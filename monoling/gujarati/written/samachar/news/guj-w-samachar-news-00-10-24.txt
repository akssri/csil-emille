<cesDoc id="guj-w-samachar-news-00-10-24" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-samachar-news-00-10-24.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Gujarat Samachar" internet news (www.gujaratsamachar.com), news stories collected on 00-10-24</h.title>
<h.author>Gujarat Samachar</h.author>
<imprint>
<pubPlace>Gujarat, India</pubPlace>
<publisher>Gujarat Samachar</publisher>
<pubDate>00-10-24</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Gujarati</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head>આસામમાં ૧૫ને ઢાળી દેતાં ઉલ્ફા ઉગ્રવાદીઓ</head></p>

<p><head>તિનસુકિયા તથા દિબ્રુગઢ જિલ્લાના ગામોમાં
મોડી રાત્રે ખેલાયેલો તાજેતરના વર્ષોનો મોટો નરસંહાર</head></p>

<p>ગુવાહાટી, તા. ૨૩</p>

<p>સિનીયર પોલિસ અધિકારીઓએ આપેલી
માહિતી અનુસાર અપર આસામના તિનસુકીયા તથા દિબ્રુગઢ જિલ્લાઓમાં ગઈ મોડી રાત્રે
ઉલ્ફાના ઉગ્રવાદીઓએ બે અલગ અલગ હુમલાઓ કરીને ૧૫ જેટલી વ્યકિતઓને રહેંસી નાખી
છે. મૃતકોમાં મોટા ભાગે નાના નાના વેપારીઓ તથા ખેડૂતોનો સમાવેશ થાય છે.
તાજેતરના ઈતિહાસમાં ઉલ્ફાનો આ મોટો હત્યાકાંડ છે.</p>

<p>તિનસુકીયાના પોલિસ સુપ્રિન્ટેન્ડેન્ટ શ્રી પી.આર.
સલોઈએ ટેલિફોન ઉપર પીટીઆઈ સાથેની વાતચીતમાં એવી માહિતી આપી છે કે જિલ્લાના
ડૂમડૂમા પોલિસ સશસ્ત્ર હદમાં આવતા કાકોઈજન નંબર ચાર નામના ગામમાં ઉલ્ફાના દસથી પણ
વધુ ઉગ્રવાદીએ લશ્કરનો ગણવેશ પહેરી જીપ લઈને ધસી ગયા હતા અને કરિયાણાની એક
દુકાન સાથે ઉભા રહેવાની માણસોએ સૂચના આપીને એકે-૫૬ રાઈફલોમાંથી ધાણીની
જેમ ગોળીઓનો વરસાદ વરસાવી દીધો હતો. દસ વ્યકિતઓ તો સ્થળ ઉપર જ મૃત્યુ
પામી હતી અને બીજી ચારને ગંભીર ઈજાઓ પહોંચી હતી. આ પૈકી એક વ્યકિતનું
હોસ્પીટલમાં લઈ જવાતાં મૃત્યુ થયું છે. બાકીના ત્રણની સ્થિતિ ગંભીર છે.</p>

<p>ઉગ્રવાદીઓએ ભાગતાં ભાગતાં પણ ગોળીબારો
ચાલુ રાખ્યા હતા. બીજા કિસ્સામાં દિબ્રુગઢ જિલ્લાના દુલિયાજાન નજીક બોરડુબી પોલિસ મથકની
હદમાં આવતા સામારિક નૌહોકિયા માર્કેટમાં ઉલ્ફાએ ત્રાટકીને ત્રણ વર્ષા એક બાળક
સહિત ચારને ઢાળી દીધા હતા અને અન્ય પાંચને ઘાયલ કરી દીધા હતા. </p>

<p>એક સ્થાનિક વેપારી રાધેશ્યામ અગ્રવાલના
મકાનમાં ઉગ્રવાદીએ મારૂતિ વાનમાં આવ્યા હતા અને ઝઘડો કર્યા બાદ વેપારીને પતાવી
દીધો હતો. તે પછી હત્યારાઓ બજારમાં આવ્યા હતા અને અન્ય ત્રણને ઢાળી દીધા
હતા.</p>

<p>બન્ને ગામોમાં સલામતિ ચુસ્ત બનાવાઈ છે અને
હુમલાખોરોને જબ્બે કરવાનું ઓપરેશન જારી કરાયું છે. સિનિયર પોલિસ તથા મુલકી
અધિકરાીએ બન્ને ગામોમાં પડાવ નાખીને સતર્ક બેઠા છે.</p>

<p>કાકોઈજન ગામમાં મોટાભાગે બિહારથી આવેલા.
નાના નાના દુકાનદારો તથા ખેતમજૂરો છેલ્લા ૨૫ વર્ષથી રહેતા હતા.</p>

<p>સેના તથા પોલિસના હાથે ખૂબ માર ખાઈ ગયેલા
ઉલ્ફાના ઉગ્રવાદીઓ વળતા પગલા તરીકે આ ફટકો માર્યો હોવાનું પોલિસ માને છે.</p>




<p><head>આંતરરાષ્ટ્રીય સ્પિડ પોસ્ટના દરોમાં
વધારાથી રૂ. ૬.૩૫ કરોડની આવક થશે</head></p>

<p>નવી દિલ્હી, તા. ૨૩</p>

<p>સરકારે આજે ઇન્ટરનેશનલ સ્પિડ પોસ્ટ (ઇએમએસ)
અને પારસલોના દરોમાં ૧લી નવેમ્બરથી અમલમાં આવે તે રીતનો વધારો જાહેર કર્યો
હતો. આ વધારાથી સરકારને વધારાના રૂા. ૬.૩૫ કરોડની આવક થશે.</p>

<p><head>૧લી નવેમ્બરથી અમલમાં આવતા ઇએમએસના દરોને
દેશ પ્રદેશના આધારે આઠ વિભાગોમાં વિભાજીત કરાયા</head></p>

<p>અત્યાર સુધી અંતરના આધારે લાગુ
પડતા ઇએમઅસના દરોને હવે દેશ અને પ્રદેશના આધારે આઠ વિભાગોમાં વિભાજીત
કરવામાં આવશે એમ એક સત્તાવાર નિવેદનમાં કહ્યું હતું. ઉલ્લેખનીય છે કે અત્યાર
સુધી જે અંતરનો આધાર લેવામાં આવ્યો છે તેમાં ૫૦૦૦ અને તેનાથી વધુ
માઇલની રવાનગીનો પણ સમાવેશ થઇ જાય છે. દરોમાં કરવામાં આવેલાં વધારાથી થનારી
નજીવી અસરને પ્રત્યેક સ્લેબ ઉપર વજનનો આધાર વધારી સરભર કરવામાં આવશે.
હાલમાં વજનનો પ્રથમ સ્લેબ ૨૦૦ ગ્રામનો છે અને ત્યારબાદના ૨૦૦ ગ્રામ માટે વધારાનો
દર લાગુ પડે છે તેના બદલે હવેથી વજનનો પ્રથમ સ્લેબ ૨૫૦ ગ્રામથી શરૂ થશે અને ત્યારબાદના
પ્રત્યેક ૨૫૦ ગ્રામ વજન માટે વધારાનો દર લાગુ પડશે.</p>

<p>એશિયાના દેશોમાં મોકલવામાં આવતાં ૨૫૦
ગ્રામ વજનના દસ્તાવેજો માટે હવે રૂા. ૪૨૫નો દર લાગુ પડશે જ્યારે ત્યારબાદના પ્રત્યેક
વધારાના ૨૫૦ ગ્રામ માટે વધારાના રૂા. ૫૦નો ચાર્જ લેવામાં આવશે. વેપારી
ચીજ વસ્તુઓ માટે પ્રથમ ૨૫૦ ગ્રામ માટે રૂા. ૪૭૫ અને ત્યારબાદના પ્રત્યેક ૨૫૦ ગ્રામ માટે
વધારાના રૂા. ૫૦નો દર લાગુ પડશે.</p>

<p>આફ્રિકા અને મધ્યપૂર્વના ઝોન માટે વેપારી
ચીજવસ્તુઓના અને દસ્તાવેજોના ઇએમએસ દરો એકસમાન રહેશે જ્યારે ઓસ્ટ્રેલિયા માટેના
પ્રથમ ૨૫૦ ગ્રામના ભાવ એકસમાન છે પરંતુ ત્યારબાદના પ્રત્યેક વધારાના ૨૫૦ ગ્રામ માટે
રૂા. ૭૫નો દર લાગુ પડશે.</p>

<p>યુરોપના દેશોમાં મોકલવામાં આવતા દસ્તાવેજો
માટે પ્રથમ ૨૫૦ ગ્રામના રૂા. ૬૭૫ અને ત્યારબાદના પ્રત્યેક વધારાના ૨૫૦ ગ્રામ માટે રૂા.
૭૫નો દર લાગુ પડશે જ્યારે વેપારી ચીજ વસ્તુઓ માટે પ્રથમ ૨૫૦ ગ્રામ વજનના રૂા. ૭૨૫ અને
ત્યારબાદના પ્રત્યેક ૨૫૦ ગ્રામ માટે રૂા. ૭૫નો ચાર્જ લેવામાં આવશે.</p>

<p>અન્ય કેટેગરીમાં દક્શિણ અમેરિકા, ઉત્તર અમેરિકા
અને શ્રીલંકા, બાંગ્લાદેશ, પાકિસ્તાન, માલ્દિવ અને નેપાળ જેવા પાંચ દેશોના ઝોનનો
સમાવેશ કરવામાં આવ્યો છે. આ દેશોના દરો પ્રથમ ૨૫૦ ગ્રામ માટે રૂા. ૩૭૫થી ૫૨૫ વચ્ચે
જુદા જુદા છે જ્યારે ત્યારબાદના વધારાના ૨૫૦ ગ્રામ માટેના ભાવો રૂા. ૨૫થી રૂા.
૧૦૦ની વચ્ચે નક્કી કરવામાં આવ્યા છે.</p>




<p><head>છોટા શકીલે છોટા રાજનને જીવતો પકડી આપવા
રૂ. પાંચ કરોડનું ઇનામ જાહેર કર્યું</head></p>

<p><head>હત્યા કરનારને બે લાખ ડોલર: કેટલાંક
લોકોને ઇ-મેઇલ કરીને લગાવેલી બોલી</head></p>

<p>મુંબઇ, તા. ૨૩</p>

<p>છોટા રાજન અને છોટા શકીલ વચ્ચેની દુશ્મની
જીવલેણ બની ચૂકી છે. છોટા શકીલના શાર્પ શૂટર દ્વારા ઘાયલ થયેલો છોટા રાજન
બેંગકોકમાં હજુ પણ જીંદગી અને મોત વચ્ચે ઝોલા ખાઇ રહ્યો છે, સાથે સાથે મુંબઇ
પોલીસ અને ભારતમાં પ્રત્યાર્પણથી બચવા મથી રહ્યો છે. છોટા શકીલે પોતાના સુધી
રાજનને જીવતો પહોંચાડનાર વ્યક્તિને પાંચ કરોડ રૂપિયાનું ઇનામ આપવાની જાહેરાત કરી
છે. કેટલાંક લોકોને ઇ-મેઇલ કરીને તેણે આ ઓફર કરી છે.</p>

<p>ગત સપ્તાહે ઇ-મેઇલનું રહસ્ય ખૂલ્યું હતું.
કેટલાંક લોકોને આની ખબર પડતા તેમણે તપાસ કરી હતી, જેમાં શકીલે કબૂલ્યું કે આ
તેનું જ ઇ-મેઇલ એડ્રેસ છે. રાજન બેંગકોકથી ફરાર થાય તો તેના ઠેકાણાની માહિતી
આપનારને એક લાખ ડોલર આપવાની જાહેરાત કરી હતી. ગત શનિવારે તેણે કરેલી જાહેરાતમાં
જણાવ્યું હતું કે, રાજનની માહિતી આપનારને એક લાખ ડોલર અને તેની હત્યા કરનારને તે
બે લાખ ડોલર આપશે. પણ જો કોઇ આ ડોનને પોતાની પાસે જીવતો પકડી લાવે તો આ
રકમ પાંચ ગણી એટલે કે દસ લાખ અમેરિકી ડોલર (રૂ. ૪.૬ કરોડ)થઇ જશે.</p>

<p>અન્ડરવર્લ્ડના ડોન અત્યાર સુધી ફોન,
મોબાઇલ અને સેટેલાઇટ ફોનની મદદથી મોડસ ઓપરેન્ડી ચલાવતા હતા,પણ છેલ્લાં કેટલાંક
મહિનાઓથી તેઓ વેબસાઇટ અને ઇ-મેઇલનો પણ સારો એવો ઉપયોગ કરે છે. છોટા
શકીલે બહુ પહેલા પોતાની વેબસાઇટનું રજીસ્ટ્રેશન કરાવી લીધું હતું.</p>

<p>અન્ડરવર્લ્ડના ડોન મુંબઇ સ્થિત પોતાના માણસોને
રોજ ઇ-મેઇલ કરે છે અને જેને ખલાસ કરવાનો હોય, કે જેની પાસેથી હપ્તા લેવાના હોય
તેની દિનચર્યા જાણવા આદેશ આપે છે. આ ઇ-મેઇલ બડા-પાઉ કે પાઉભાજી ડોટકોમ વગેરે પર
મોકલી દેવાય છે. ઇ-મેઇલની નોંધણી સાવ સરળ છે. વળી અન્ડરવર્લ્ડ માટે ઇ-મેઇલ પર
સંદેશાઓની આપ-લે સુરક્શિત છે કારણ કે તેને ટ્રેસ કરવા પોલીસ માટે મુશ્કેલ છે.
કોઇનું યુઝર્સ નેમ તો જાણી શકાય પણ પાસવર્ડ જાણવો લગભગ મુશ્કેલ છે, પણ પોલીસ ફોનને
ટ્રેસ કરી શકે છે.</p>

<p>સૂત્રોના જણાવ્યા પ્રમાણે અન્ડરવર્લ્ડના ગુંડાઓએ
મુંબઇમાં ટ્રાવેલ એજન્સીઓ અને પીસીઓ બુથમાં કમ્પ્યુટર ઇન્સ્ટોલ કરાવ્યા છે,જ્યાં
અંગ્રેજીના જાણકાર લોકો આ સંદેશાઓ વાંચે છે અને પછી અંગ્રેજીમાં જ માફિયાઓને
જવાબ મોકલાવે છે. ખાસ વાત એ છે કે તેઓ વિદેશોમા પણ પોતાની સાથે અંગ્રેજીના
જાણકારો રાખે છે. તેઓ પોતે બહુ ભણેલા નથી હોતા. છોટા શકીલે ઇ-મેઇલ પર છોટા
રાજનની બોલી લગાવી છે, એ સંદેશો વાંચીને કોઇ પણ કહી આપે કે એ શબ્દો એના
પોતાના નથી પણ બીજા પાસે લખાવડાવ્યા છે.</p>




<p><head>'સવાલ દસ કરોડ કા'ના પ્રથમ એપીસોડમાં
વૈશાલી સેમ્યુઅલે દસ લાખ મેળવ્યા</head></p>

<p><head>લોકોને કરોડપતિ બનાવવાની હોડનો પ્રારંભ
વ્યૂઅરશીપ ખેંચવા મનિષા કોઇરાલાનું ગ્લેમર ઉમેરાયું</head></p>

<p>અમદાવાદ, તા. ૨૩</p>

<p>ક્વીઝ કાર્યક્રમો દ્વારા સ્પર્ધકોની કરોડપતિ
બનાવવાની હોડમાં 'કૌન બનેગા કરોડપતિ'ની સામે ઝી ટીવીએ આજથી શરૂ કરેલા કાર્યક્રમ
'સવાલ દસ કરોડ કા'માં નાગપુરની શિક્શિકા વૈશાલી સેમ્યુઅલ દસ કરોડના સવાલ સુધી
પહોંચી શકી ન હતી અને તે રૂ. ૧૦ લાખ લઇને ક્વીટ કરી ગઇ હતી. અમિતાભ બચ્ચનની પ્રતિભાની
સામે ટક્કર લેવા અનુપમ ખેર ઉપરાંત મનિષા કોઇરાલાના રૂપમાં વધુ એક આકર્ષણ ઉમેરીને
તેને કો-હોસ્ટ બનાવવામાં આવી હતી. જો કે કાર્યક્રમની ધૂરા તો ખેરે જ સંભાળી હતી
અને મનિષા તેને આસીસ્ટ કરતી હતી.</p>

<p>પ્રારંભમાં ૨૧ સભ્યોમાંથી સૌથી વધુ ગુણ
ધરાવતા ત્રણ સ્પર્ધકોને સીલેક્ટ કરવામાં આવ્યા હતા, જેમાં સૌથી પહેલા
૧૦૦૦નો સ્કોર કરનાર વૈશાલી સેમ્યુઅલ હોટસીટ પર બેસી હતી. 'કેબીસી'માં જેમ
લાઇફલાઇન આવે છે એ રીતે અહિં તેને ટ્રમ્પકાર્ડ નામ આપવામાં આવ્યું છે. સ્પર્ધક
પહેલો સાચો જવાબ આપીને એક રૂપિયાથી ખાતુ ખોલાવે પછી દરેક સાચા જવાબે રકમમાં
એક શૂન્ય ઉમેરાય એટલે કે રકમ દસગણી થાય.</p>

<p>છેલ્લે બચેલા સ્પર્ધકને ત્રણ સવાલ પૂછવામાં
આવે છે જેમાંથી બે સાચા જવાબ આપવા પડે છે. આ રીતે દસ હજાર, લાખ, દસ લાખ, એક
કરોડ અને છેલ્લે દસ કરોડ. આજે પ્રસારિત થયેલ એપિસોડમાં લગભગ બધા સવાલો સરળ
હતા.</p>

<p>૧૦ લાખ રૂપિયા જીત્યા પછી એક કરોડ માટે આ પ્રશ્ન
પૂછાયો હતો: હોળકરના મતે આઉટડોર ગેમનો રાજા કોણ? વૈશાલી આ સવાલમાં
ગુંચવાઇ જતા ક્વીટ કરી ગઇ હતી.</p>

<p>ગેમ શોનો આ પ્રથમ એપીસોડ હતો અને તેમાં
મનિષા કોઇરાલા જેવી ગ્લેમરસ અભિનેત્રીનો સમાવેશ કરાયો હોવાથી દર્શકો એ આ
કાર્યક્રમ હોંશભેર જોયો હતો. વળી દર્શકો માટે પણ કુલ પાંચ લાખના ઇનામો રાખવામાં
આવ્યા છે, જેની વ્યૂઅરશીપ વધારવામાં મદદરૂપ સાબિત થશે.</p>




<p><head>મુક્તા-પન્ના કેસમાં સીબીઆઈનો ઉઘડો લેતી
સર્વોચ્ચ અદાલત</head></p>

<p>નવી દિલ્હી, તા. ૨૩</p>

<p>મુક્તા-પન્ના તેલક્શેત્રોના કેસમાં ગુમ થયેલી
તપાસની ફાઈલના મુદ્દે ખોટું નિવેદન આપવા માટે સુપ્રિમ કોર્ટે આજે સીબીઆઈનો
ઉધડો લઈ નાખતાં કહ્યું હતું કે આવા બનાવોથી સીબીઆઈમાંથી અદાલતનો વિશ્વાસ
ડગી જશે.</p>

<p><head>આ કેસની ગુમ થયેલી ફાઈલ અંગે ખોટું નિવેદન
આપવા બદલ સીબીઆઈ ઉપર તવાઈ</head></p>

<p>રિલાયન્સ-એનરોન કોન્સોટોરિયમને અપાયેલા
ઓઈલ ફિલ્ડના કોન્ટ્રાકટને માન્ય રાખીને ત્રણ ન્યાયમૂર્તિઓની બેંચે કહ્યું છે કે
વિશેષ તપાસ દ્વારા મદદ માટે અમે આ તપાસ-સંસ્થા ઉપર ઘણો વિશ્વાસ મૂકીએ છીએ એ
બાબતની અમે ચિંતા સાથે નોંધ લઈએ છીએ. સીબીઆઈ ઉપરનો અદાલતોનો આ
આધાર એ વિશ્વાસ ઉપર આધારીત છે જે અદાલતોએ તેનામાં મૂકેલો છે અને અમે
હાલ જે પ્રસંગનો સામનો કરી રહ્યા છીએ એવા પ્રસંગોથી અમારો વિશ્વાસ ડગી જાય એવી
શકયતા છે.</p>

<p>બેંચમાં જસ્ટીસ એસ.પી. ભરૂચા, જસ્ટીસ
એન.એસ.એસ. કાદરી તથા જસ્ટીસ એન. સંતોષી હેગડેનો સમાવેશ થાય છે. બેંચે કહ્યું
છે કે એટલા માટે અમને એવું લાગે છે કે ઘણું મોડું થઈ જાય તે પહેલાં આ તપાસ
સંસ્થા તેનું પોતાનું ઘર વ્યવસ્થિત કરે.</p>

<p>ઉપરોકત કોન્ટ્રાકટ આપવાની બાબતમાં લાંચ તથા
ગેરરીતીઓના આક્શેપોમાં કથિત તપાસમાં સીબીઆઈએ બજાવેલી કંગાળ કામગીરી સામે
અરજદારે કેટલાક આક્શેપો કર્યા હતા. જાહેર હિતની અરજી માટેનું કેન્દ્ર આ કેસમાં ચર્ચાસ્પદ
છે. બેંચ વતી ચુકાદો આપતાં જસ્ટીસ હેગડેએ કહ્યું હતું કે મુંબઈની સીબીઆઈના
તાત્કાલિક પોલિસ સુપ્રિટેન્ડેન્ટે જે ખોટી હોવાનું મનાય છે તે પાર્ટ-ટુ ફાઈલ નંબર
૧/૬૩૬, ડી/૯૫/ એપી/ હાલના અભિગમ અંગે દિલ્હી હાઈકોર્ટ સમક્શ સીબીઆઈએ અખત્યાર કરેલા
વલણ પૂરતી જ અમારી વિચારણા મર્યાદિત રાખવા સિવાય આ તમામ આક્શેપોમાં ઊંડા ઉતરવાનું
અમારે માટે જરૂરી હોય તેવું અમને નથી લાગતું. અરજદારે એવો સ્પષ્ટ આક્શેપ મૂકયો
હતો કે આ મદદ ફાઈલ મુંબઈની સીબીઆઈની ભ્રષ્ટાચાર થયેલી શાખાના સુપ્રિન્ટેડન્ટ શ્રી
વાય.બી. સિંઘે ૧૯૯૬ના માર્ચમાં જ ખોઈ હતી અને આ જ ફાઈલને સીબીઆઈના કોઈ
અધિકારીએ ભયના હેતુઓ માટે મૂળ ફાઈલથી અલગ કરીને દબાવી રાખી હતી.</p>




<p><head>ભૂતપૂર્વ ક્રિકેટર કિર્તી આઝાદનો આક્શેપ</head></p>

<p><head>ઓલિમ્પિકમાં સવા કરોડના વિદેશી હુંડિયામણની
દાણચોરી થઇ છે</head></p>

<p><head>સિડનીમાં આઇઓએના અધિકારીઓના
બાદશાહી ખર્ચા જોઇ એનઆરઆઇઓની આંખો ફાટી ગઇ</head></p>

<p>નવી દિલ્હી, તા. ૨૩</p>

<p>ભૂતપૂર્વ ભારતીય ક્રિકેટર તથા વર્તમાન સાંસદ
કિર્તી આઝાદે આક્શેપ કર્યો છે કે આઇઓએએ સિડની ઓલિમ્પિક દરમ્યાન એથ્લેટોને
ટ્રાવેલર્સ ચેકો પર સહી કરાવી અને તેના કરતાં ઓછી રકમ ચૂકવી વિદેશી હૂંડિયામણ
કૌભાંડ કર્યું છે, આમ તેઓએ રૂ. ૧.૨૫ કરોડના વિદેશી હૂંડિયામણની દાણચોરી કરવા
એથ્લેટોનો ઉપયોગ જ કર્યો છે.</p>

<p>આઝાદે દાવો કર્યો છે કે સિડની જતાં પહેલાં
૭૦ એથ્લેટો પાસે ૫૦૦૦ ડોલરના ટ્રાવેલર્સ ચેક પર સહી કરાવવામાં આવી હતી. પરંતુ
તેમને દિવસદીઠ ૩૫ ડોલર જ ચૂકવાયા હતાં. આમ એથ્લેટોએ જે ચેકો પર સહી કરી તેની
રકમ રૂ. ૧.૫ કરોડ થતી હતી જ્યારે તેની સામે તેમને માત્ર રૂ. ૨૫ લાખ જ મળ્યા છે. બાકી
નાણાંનું તેઓએ શું કર્યું તે આઇઓએ જણાવે તો જ મને આશ્ચર્ય થશે, એમ આઝાદે
પત્રકારોને જણાવ્યું હતું. તેમણે વડાપ્રધાનને આ કૌભાંડની તપાસ સીબીઆઇ દ્વારા
કરાવવાની વિનંતી કરી હતી. ઓલિ. ટીમમાં સામેલ શોટ પુટર (ગોળાફેંક ખેલાડી)
શક્તિસિંઘે પણ આઝાદના દાવાને ટેકો આપતા જણાવ્યું હતું કે તેમને તો આખા પ્રકરણની
સિડની ગયા બાદ દિવસદીઠ ૩૫ ડોલર ચૂકવાયા ત્યારે જ તેની ખબર પડી હતી. પુરાવા તરીકે
રોઝાકુટ્ટીના પાસપોર્ટની કોપીમાં સ્પષ્ટપણે જણાઇ આવે છે કે ૫૦૦૦ ડોલરની રકમ
આપવામાં આવી છે. તેમાં સીબીઆઇની સાથે એન્ફોર્સમેન્ટ ડિરેકટોરેટ પણ તપાસ કરે
તેવી માંગ કરવામાં આવી છે. તેમણે દાવો કર્યો હતો કે ઘણા એથ્લેટો આ માટે તેમને
આવીને આ અંગે મળ્યા હતા, પરંતુ તે જાહેરમાં આવવા માંગતા ન હતાં.</p>

<p>આ સિવાય સિડનીમાં ભારતીય ઓલિ. ટુકડીના
અધિકારીઓ બાદશાહી ઠાઠમાં રહ્યા હોવાના પુરાવા છે. ઓલિ. એસો.માં મહત્વનું
પદ ધરાવતા ઓમપ્રકાશ ચૌટાલાના પુત્રે લાખો રૂપિયાના ખર્ચે જહાજ પર ભવ્ય પાર્ટી
આપી હતી. </p>

<p>જોકે તેમાં એકપણ ભારતીય એથ્લેટને ભાગ લેવાની
છૂટ ન હતી. આ પાર્ટીની ભવ્યતાથી આશ્ચર્યમાં પડી ગયેલા બિનનિવાસી ભારતીયો કટાક્શમાં
કહેતા હતાં કે કોણ કહે છે કે ભારતનું ઓલિમ્પિક એસોસિયેશન ગરીબ છે. તેમના
બાદશાહી ખર્ચા જોતા તો તેઓ તેલથી સમૃદ્ધ કોઇ આરબ રાષ્ટ્રમાંથી આવ્યા હોય તેમ લાગે
છે.</p>




<p><head>રીલાયન્સ, એચડીએફસી, સુંદરમનો વીમા ક્શેત્રે
પ્રવેશ</head></p>

<p>નવીદિલ્હી, તા. ૨૩</p>

<p>વીમા ક્શેત્રે સરકારની ઇજારાશાહી તોડવાની એક
હિલચાલરૂપે ઇન્સ્યોરન્સ રેગ્યુલેટરી એન્ડ ડેવલોપમેન્ટ ઓથોરિટી (ઇરડા)એ આજે ત્રણ
ખાનગી કંપનીઓને વીમા કંપની તરીકેની નોંધણી કરાવવાન મંજૂરી આપી દીધી
હતી. જ્યારે અન્ય ત્રણ કંપનીઓને આ ક્શેત્રમાં પ્રવેશવા સૈદ્ધાંતિક મંજૂરી આપી હતી.</p>

<p><head>આઇસીઆઇસીઆઇ, મેક્સ અને ઇફકોને
નોંધણી કરાવવા માટે મળેલી સૈધ્ધાંતિક મંજુરી</head></p>

<p>વીમા ધારા ૧૯૩૮ની પેટાકલમ ૩ મુજબ જે ત્રણ
કંપનીઓને નોંધણી કરાવવાની મંજૂરી આપી હતી તેમાં રીલાયન્સ જનરલ ઇન્સ્યોરન્સ
એચડીએફસી સ્ટાન્ડર્ડ લાઈફ ઇન્સ્યોરન્સ અને રોયલ સુંદરમ ઇન્સ્યોરન્સ કંપનીઓનો સમાવેશ
થાય છે એમ સૂત્રોએ કહ્યું હતું. જ્યારે આઈસીઆઇ પ્રડેન્શિયલ ઇન્સ્યોરન્સ, મેક્સ
ન્યુયોર્ક લાઈફ ઇન્સ્યોરન્સ અને ઇફકો-ટોકીયો મરીન જનરલ ઇન્સ્યોરન્સ કંપનીને
નોંધણી કરાવવા સૈદ્ધાંતિક મંજૂરી આપવામાં આવી હતી. ઉલ્લેખનીય છે કે ઇરડાએ
અગાઉ એવી જાહેરાત કરી હતી કે દિવાળી પહેલાં લાયસંસ આપી દેવામાં આવશે. અત્યાર
સુધી ૧૦ જેટલી કંપનીઓએ વીમા કંપની તરીકેની નોંધણી કરાવવા ઇરડાને અરજીઓ
કરી દીધી છે.</p>

<p>સ્ટેટ બેંક ઓફ ઇન્ડિયા, પંજાબ નેશનલ બેંક,
બેંક ઓફ બરોડા, કેનરા બેંક, બેંક ઓફ ઇન્ડિયા અને કોર્પોરેશન બેંક જેવી જાહેર
ક્શેત્રની બેંકોએ પણ વીમા ક્શેત્રે પ્રવેશ કરવા જોરદાર તૈયારીઓ આરંભી દીધી
હતી.</p>




<p><head>સ્કાયબસમાં કિલોમીટર દીઠ માત્ર ૧૫ પૈસાનો
ખર્ચ આવશે</head></p>

<p><head>કોંકણ રેલવે દ્વારા ઘડાતો પ્રોજેક્ટ:
મહિને રૂ.૨૫૦માં એસી પ્રવાસ થઇ શકશે</head></p>

<p>નવી દિલ્હી, તા. ૨૩</p>

<p>મુંબઇ અને કર્ણાટકમાં શરૂ થનાર સામૂહિક
પરિવહનનું વાહન સ્કાય બસ એટલી બધી સસ્તી હશે ક એક કિલોમીટર પ્રવાસનો ખર્ચ માત્ર
૧૫ પૈસા જટલો નીચો આવશે એમ કોંકણ રેલવેના મેનેજિંગ ડિરેક્ટર બી રાજારામે
જણાવ્યું હતું.</p>

<p>'સામૂહિક ઝડપી પરિગમનની પધ્ધતિ' વિષય પર
અહિં સીઆઇઆઇ દ્વારા આયોજિત સેમીનારમાં સ્કાય બસના લાભો વર્ણવતા તેમણે કહ્યું કે આ
બસ એલીવેટેડ રેલ સિસ્ટમ કરતા ૫૦ ટકાથી ઓછા ખર્ચે અને ભૂગર્ભ મેટ્રો સિસ્ટમના માત્ર
૨૫ ટકા ખર્ચે ઓપરેટ થઇ શકે છે. આ અંગે તેમણે જણાવ્યું કે સ્કાય બસ મેટ્રો કોઇ
દિશામાં કલાક દીઠ ૧૫,૦૦૦થી ૫૦,૦૦૦ પ્રવાસીઓ વહન કરી શકશે અને એક વાર આ પ્રોજેક્ટ
કાર્યાન્વિત થાય પછી મેટ્રોમાં ટ્રાફિક જામની સમસ્યા નહિ ઉદભવે. આ પ્રોજેક્ટને કારણે
પ્રવાસીઓ મહિને રૂ. ૨૫૦માં એરકન્ડીશન્ડ પ્રવાસ કરી શકાશે. આ પ્રોજેક્ટ ૧૦૦ અઠવાડિયાઓમાં
અમલી બનાવી શકાય છે અને આ માટે કોંકણ રેલવે ધારો ઘડી રહી છે એમ તેમણે
વધુમાં જણાવ્યું હતું.</p>




<p><head>ઉદ્યોગપતિ ગરવારે સાથે છેતરપીંડી કરવા બદલ
રાજરથીનમને એક વર્ષની આકરી સજા ફટકારાઇ</head></p>

<p><head>કંપનીઓ હસ્તગત કરવામાં ઉસ્તાદ ઇસમને રૂ. બે
કરોડનો દંડ પણ કરવામાં આવ્યો</head></p>

<p>મુંબઇ, તા. ૨૩</p>

<p>ઉદ્યોગપતિ ચંદ્રકાંત ગરવારેની છેતરપિંડી કરવા
બદલ કંપનીઓ હસ્તગત કરવામાં ઉસ્તાદ ગણાતાં પી. રાજરથીનમને એક વર્ષની કડક શિક્શા અને
બે કરોડનો દંડ ચાર વિવિધ કેસોમાં ફટકારવામાં આવ્યો છે અને બધી સજા
સાથે ભોગવવાનો ચુકાદો વધારાના મેટ્રોપોલિટન મેજીસ્ટ્રેટ એ.કે. હોલંબે
પાટિલે આપ્યો છે.</p>

<p>પી. રાજરથીનમનની કંપની મે પીઆરએ ઇન્વેસ્ટમેન્ટ
લિ.ના મેનેજર એસ. સંપટકુમારને પણ નેગોશિયેબલ ઇન્સ્યુમેન્ટ એકટની કલમ ૧૩૮ હેઠળ એક
વર્ષની સજા ફટકારવામાં આવી છે. ગરવારે પેઇન્ટને ખરીદવા માટે રાજરથીનમનના કહેવાથી
તેમને ૪.૯૪ કરોડના ચાર ચેકો આપ્યા હતા જે બાઉન્સ થયા હતાં. બંને આરોપીઓને
પ્રત્યેક કેસમાં બે બે કરોડનો દંડ થયો છે જે એકંદરે ૧૬ કરોડ થાય છે અને તે ન ભરે
તો ત્રણ મહિનાની વધારાની ભોગવવાની રહેશે તેમજ દંડની અડધી રકમ ગરવારે
પેઇન્ટને ચૂકવવાની રહેશે.</p>

<p>સંપટકુમારને તાત્કાલિક પકડીને જેલમાં મોકલી
દેવાયા છે જ્યારે રાજરથીનમન નાસતા ફરે છે તેમની ગેરહાજરીમાં તેમના વકીલ હાજર થયા
હતા. તેમની વિરુધ્ધ કોર્ટે બિન જામીનપાત્ર વોરંટ બહાર પાડયું છે.</p>

<p>રાજરથીનમે સેશન કોર્ટ સમક્શ અપીલ કરી છે જેની
સુનાવણી કાલે થશે.</p>

<p>ન્યાયાધીશે રાજરથીનમને હાજર કરવા
સી.બી.આઇ. ડાયરેક્ટર અને તમામ રાજ્યોના પોલીસ મહાનિરીક્શકોને વાકેફ કરવા તથા ૨૦
ડિસેમ્બર પૂર્વે હાજર કરવા જણાવ્યું છે.</p>

<p>રાજરથીનમ વિરુધ્ધ ગરવારે પેઇન્ટના ચેરમેન
અને મેનેજીંગ ડિરેક્ટરે ફરિયાદ કરી હતી. ૧૯૯૩માં બાયપાસ સર્જરી કરાવ્યા બાદ તેમણે
પોતાની કંપની વેચવાનું નક્કી કર્યું હતું. ૯૪માં રાજરથીનમ સાથે તેમણે કરાર કર્યા હતા
જેમાં ગરવારે રાજરથીનમ સામે સ્પર્ધા ન કરે એવું પણ નક્કી થયું હતું અને શરૂઆતમાં
૨.૫ કરોડની રકમ બાદમાં ૪.૯૪ કરોડ સુધી વધારવામાં આવી હતી જે ચાર
હપ્તામાં ચૂકવવાનું નક્કી થયું હતું.</p>

<p>રાજરથીનમે ચાર ચેકો ચૂકવ્યા હતા જે બાઉન્સ
થતાં તેમની વિરુધ્ધ ચાર કેસો દાખલ કરાયા હતા.</p>




<p><head>કેરલમાં લઠ્ઠાકાંડનો મૃત્યુ આંક વધીને
૨૯નો થયો</head></p>

<p>કોલ્લમ, તા. ૨૩</p>

<p>કેરલનાં કલ્લુવાથુકાલ અને પાલ્લીકાલ
વિસ્તારોમાં થયેલ લઠ્ઠાકાંડનાં પગલે આજે કોલ્લમ જીલ્લાનાં પટ્ટાઝી ખાતે ઝેરી શરાબ પીતાં
ત્રણ મૃત્યુ થવાનાં અહેવાલો મળ્યાં છે. આ ત્રણ મૃત્યુ સાથે શનિવારથી આજસુધીમાં
મરનારની સંખ્યા ૨૯ની થઇ છે એમ પોલીસે જણાવ્યું હતું.</p>

<p><head>અન્ય સ્થળે પણ ઝેરી લઠ્ઠાથી વધુ ત્રણનાં મૃત્યુ
નોંધાયા</head></p>

<p>પોલીસનાં જણાવ્યાં અનુસાર કાલ્લુલાથુકાલ
ઘટનામાં અત્યાર સુધી ૧૭ વ્યક્તિઓ મરાયાં છે. જ્યારે પાલ્લીકાલમાં નવ અને
પટ્ટાઝીમાં ત્રણ વ્યક્તિઓ માર્યાં ગયાં છે. પટ્ટાઝી ખાતે મરેલાં લોકોએ બાજુનાં
પાલ્લીકાલથી ઝેરીશરાબ લાવીને પીધો હતો તે સ્પષ્ટ થવા પામ્યું નહોતું. આ
ત્રણેયને તિરૂવમંતપુરમ ખાતેની મેડીકલ કોલેજ હોસ્પિટલમાં પહોંચાડયા હતાં પરંતુ ત્યાં
પહોંચતાં સાથે જ તેમને મૃત જાહેર કરાયાં હતાં.</p>

<p>મેડીકલ કોલેજનાં સૂત્રોએ જણાવ્યું હતું કે
ઝેરી શરાબનાં ભોગ બનેલા લોકો હોસ્પીટલમાં હજુ પણ ઠલવાઈ રહ્યાં છે હોસ્પીટલનાં
સ્પેશ્યલ વોર્ડમાં ૨૦૬ વ્યક્તિઓ સારવાર હેઠળ છે.</p>

<p>૧૯૮૨થી આજસુધીમાં કેરલમાં બનેલ
લઠ્ઠાકાંડોમાંના ત્રણ સૌથી મોટા લઠ્ઠાકાંડોમાંનો આ એક છે. ૧૯૮૨માં વ્યાપીનઇર્નાકુલમ
ખાતેનાં લઠ્ઠાકાંડમાં ૭૮ લોકો મૃત્યુ પામ્યાં હતાં. ત્યાર પહેલાં ૧૯૮૧માં પુનાલુર
લઠ્ઠાકાંડમાં ૩૪ લોકો મર્યાં હતાં.</p>

<p>આ દરમિયાન ગઇકાલે રચાયેલ અધિકૃત તપાસ
સમિતિની પ્રાથમિક તપાસનો અહેવાલ અનુસાર સરકારે આજે દસ એક્સાઈઝ અધિકારીઓને
બરખાસ્ત કર્યાં છે. આ આદિવાસીઓમાં એક્સાઇઝ ખાતામાં એક સહ કમિશ્નરનો પણ સમાવેશ
થાય છે.</p>




<p><head>વાજપેયીએ સત્તાવાર રીતે કામકાજનો કરેલો
પ્રારંભ</head></p>

<p>નવી દિલ્હી, તા. ૨૩</p>

<p>વડાપ્રધાન અટલ બિહારી વાજપેયીએ આજનો
દિવસ હળવાશથી ગાળ્યો હતો. પરંતુ તેઓએ પોતાના નિવાસસ્થાને સત્તાવાર ફરજો
બજાવવાનો આરંભ કર્યો હતો. ગૃહપ્રધાન એલ. કે. અડવાણી સાથે તેઓએ અગત્યની
બેઠક યોજી હતી.</p>

<p>મુંબઈ ખાતે ઢીંચણનું ઓપરેશન કરાવ્યા બાદ
તેઓ ગઈકાલે પરત આવ્યા હતા. તેઓએ ઓપરેશન બાદ સારવારના ભાગરૂપે જરૂરી
ફીઝિયોથેરાપી અને બીજી કસરતો કરવા પૂરતો સમય ફાળવ્યો હતો. વાજપેયીના અંગત તબીબ
ડૉ. અનુપ મિશ્રાએ તેઓની દેખરેખ રાખી હતી.</p>

<p>દિવસના આરંભમાં ગૃહપ્રધાન અડધો
કલાક માટે તેઓને મળ્યા હતા. વિલંબમાં પડેલા અગત્યના મુદ્દાઓ અંગે તેઓએ ચર્ચા કરી
હોવાનું માનવામાં આવે છે. ૧લી નવેમ્બરથી ત્રણ નવા રાજ્યો ઝારખંડ, છત્તિસગઢ અને
ઉત્તરાંચલની રચનાનો કાર્યક્રમ તેમાંનો એક મુદ્દો છે.</p>

<p>શ્રીનગરમાં ગઈકાલે લશ્કરી અધિકારીઓ સાથે
રાજ્યની સ્થિતિની સમીક્શા કરવા બેઠક યોજવામાં આવી હતી, તે અંગે ગૃહપ્રધાને
તેઓને માહિતી આપી હતી. સંરક્શણ પ્રધાન જ્યોર્જ ફર્નાન્ડિઝે પણ શ્રીનગરમાં આ
બેઠકમાં હાજરી આપી હતી.</p>

<p>વાજપેયીના પગે ૧૦મી ઓક્ટોબરે ઓપરેશન થયા
બાદ કેબિનેટની પહેલી બેઠક ૩૧મી ઓક્ટોબરે મળશે. નવેમ્બરના ત્રીજા સપ્તાહમાં સંસદના
શિયાળુ સત્રનો આરંભ થઈ રહ્યો છે, તે માટે આ બેઠકમાં એજન્ડા નક્કી કરવામાં આવશે.</p>

<p>પોતાના સત્તાવાર કાર્યનો આરંભ કરવા ઉપરાંત
પોતાના રેસકોર્સ ખાતેના રહેઠાણે વડાપ્રધાને કેટલાક ટેલીફોન પણ રીસીવ કર્યા
હતા. કેટલાક કેન્દ્રીય પ્રધાનોએ દિવાળીના તહેવાર પ્રસંગે વડાપ્રધાનને પોતાના
ત્યાં બોલાવવા ઈચ્છા દર્શાવી હતી. હાલમાં વાજપેયીને સંપૂર્ણ આરામ કરવાની સલાહ
હોવાથી તેઓની વિનંતીનો અસ્વીકાર કરવામાં આવ્યો હતો. પોતાના મતવિસ્તારમાં જતા
પહેલા કેટલાક પ્રધાનો આવતીકાલે વડાપ્રધાનને દિવાળીની શુભેચ્છા આપવા ભેગા
મળશે.</p>




<p><head>અમેરિકન એમ્બેસીએ વીઝા ફીમાં કરેલા
વધારો</head></p>

<p>નવી દિલ્હી, તા. ૨૩</p>

<p>રૂપિયો-ડોલરના હૂંડિયામણ દરમાં થયેલા
ફેરફાને કારણે અમેરિકાના રાજદૂતાવાસે આજે પહેલી નવેમ્બરથી અમલી બને એ રીતે વીઝા
ફીમાં વધારાની જાહેરાત કરી છે.</p>

<p><head>રૂપિયો-ડોલર હૂંડિયામણ દર બદલાતા લેવાયેલું
પગલું</head></p>

<p>'અમેરિકન ડોલરના ચલણમાં ફી અગાઉ જટલી જ
રહેશે પણ રૂપિયાની રકમ પ્રતિ ડોલર રૂ. ૪૭ના ભાવે ગણાશે.</p>

<p>નોન ઇમીગ્રેશન વીઝા ફી રૂ. ૨૧૧૫ (૪૫ ડોલર)
અને નોન-ઇમીગ્રેશન વીઝા ઇસ્યુઅન્સ ફી રૂ.૩૫૨૫ (૭૫ ડોલર) રહેશે એમ અમેરિકન એમ્બેસીએ
જાહેર કરેલી યાદીમાં જણાવ્યુ ંહતું.</p>

<p>ઇમીગ્રન્ટ વીઝા ફી રૂ. ૧૨૨૨૦ (૨૬૦ ડોલર) અને
ઇમીગ્રન્ટ વીઝા ઇસ્યુઅન્સ ફી રૂ. ૩૦૫૫ (૬૫ ડોલર) અને રીટર્નીંગ રેસીડન્ટ્સ ફી રૂ. ૨૩૫૦ (૫૦
ડોલર) રહેશે.</p>




<p><head>ગોવા વિધાનસભામાં આજે ફ્રન્સીસ્કો
વિશ્વાસનો મત લેશે</head></p>

<p>પણજી, તા.૨૩</p>

<p>૧૧ મહિનાની મોરચા સરકારમાંથી ભારતીય જનતા
પક્શે ઉચાળા ભરી દીધા બાદ આજે ત્રીજા દિવસે પણ ગોવાની રાજકીય પરિસ્થિતિ એટલી જ
પ્રવાહી રહી છે. ભાજપે સરકાર રચવાનો દાવો કર્યો છે હવે આવતી કાલે
વિધાનસભામાં વિશ્વાસના મતનો સામનો કરવા મુખ્યપ્રધાન ફ્રાન્સીસ્કો સરદિન્સ પણ
કમર કસી રહ્યા છે.</p>

<p><head>હજુ પણ ચાલી રહેલી પ્રવાહી રાજકીય સ્થિતિ</head></p>

<p>ગોવાના ગવર્નર શ્રી અહમદ મુઝર્સ મુખ્ય
પ્રધાનને ગૃહમાં તેમની બહુમતિ પૂરવાર કરવાનો આદેશ આપેલો છે. ગવર્નરે આવતી
કાલે વિધાનસભાની આવી બેઠક પણ બપોરે આ હેતુ માટે અઢી વાગ્યે બોલાવી છે.
ગોવા પીપલ્સ કોંગ્રેસ (જીપીસી)ના વર્તુળોએ કહ્યું હતું કે આવતી કાલની નિર્ણાયક
તાકાતના મુકાલબામાં વ્યૂહ અપનાવવા માટે મુખ્યપ્રધાને દસ પ્રધાનો તથા સરખી
વિચારસરણી ધરાવતા અન્ય તેમણે આજે શ્રેણીબદ્ધ બેઠકો યોજી છે.</p>

<p>૪૦ સભ્યો ધરાવતી વિધાનસભામાં
મુખ્યપ્રધાને બહુમતિનો દાવો કરેલો છે.</p>

<p>દરમિયાનમાં પરિસ્થિતિનો તાગ મેળવવા પાંચ
સભ્યોની કોંગ્રેસ વિધાનસભા પક્શની બેઠક પણ મોડી રાત્રે મળવાની છે.</p>




<p><head>ચેન્નાઈમાં લાગેલી ભીષણ આગમાં ૧૧૬ વર્ષ
જુનું મકાન ભસ્મ</head></p>

<p>ચેન્નાઈ, તા. ૨૩</p>

<p>ચેન્નઈની જનરલ પોસ્ટ ઓફિસનાં ૧૧૬ વર્ષ જૂનાં
મકાનમાં ગઈકાલ મધ્યરાત્રીએ લાગેલી મોટી આગને લીધે મકાનનો મોટો ભાગ નાશ
પામ્યો છે. આ આગ સાત કલાકથી વધુ સમય ભભુકતી રહી હતી. જો કે આ આગને લીધે
કોઈ જાનહાનિ થવા પામી નથી.</p>

<p><head>કોઈ જાનહાની થઈ નહીં : જીપીઓનું મકાન
બળીને ખાક થઈ ગયું</head></p>

<p>ગઈકાલ મધ્યરાત્રીએ મકાનનાં ઉત્તરી ભાગમાં
ચોકીદારે આ આગ જોઈ હતી. આજે સવારે ૮.૩૦ વાગ્યે આ આગને નિયંત્રણમાં લાવવામાં
આવી હતી. એમ વડા પોસ્ટ માસ્ટર જનરલ એ.વી.બી. મેનને આજે જણાવ્યું હતું. મકાનનાં
૫૫,૦૦૦ ચોરસફીટનાં બાંધકામનો લગભગ ૩૩,૦૦૦ ચોરસફીટ ભાગ આગમાં નાશ પામ્યો
છે. આ ઉપરાંત મકાનનો બચત ખાતાનો હોલ, જાહેર કાઉન્ટરો, ૨૫ કોમ્પ્યુટરો એક સર્વર
એક મેલ અને ડીલીવરી હોલ, બેઝમેન્ટ હોલ, ફર્નિચર અને અનેક પાર્સલો નાશ પામ્યા હતાં
એમ મેનને જણાવ્યું હતું.</p>

<p>અગ્નિશમન દળોએ મહા મુશ્કેલીએ આ આગ કાબુમાં
લાવ્યા બાદ મકાનને ઠંડુ પાડવા માટે અનેક ટન પાણી ઠાલવ્યું હતું. એમ કહેતાં મેનને
ઉમેર્યું હતું કે પોસ્ટ માસ્ટર જનરલ વત્સલ રાઘવન હેઠલ એક સમિતિની રચના થઈ છે. આ
સમિતિ આગ લાગવાનાં કારણોની તપાસ કરવા ઉપરાંત પોસ્ટ ઓફિસનાં આ જુના મકાનના
સમારકામ અંગે પણ સૂચનો આપશે. આ સમિતિ એક મહિનામાં તેનો અહેવાલ આપે તેવી આશા
છે. એમ આ પોસ્ટ માસ્ટર જનરલ મેનને કહ્યું હતું. જો કે આગ લાગ્યાં છતાં મકાનનાં બચી
ગયેલાં ભાગમાં પોસ્ટ ઓફિસનું કામકાજ ચાલુ રહેશે. આવતીકાલથી કાર્ય રાબેતા મુજબ
ચાલુ કરાશે એમ મેનને કહ્યું હતું.</p>

<p> </p>






</body></text></cesDoc>