<cesDoc id="guj-w-samachar-news-01-03-02" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-samachar-news-01-03-02.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Gujarat Samachar" internet news (www.gujaratsamachar.com), news stories collected on 01-03-02</h.title>
<h.author>Gujarat Samachar</h.author>
<imprint>
<pubPlace>Gujarat, India</pubPlace>
<publisher>Gujarat Samachar</publisher>
<pubDate>01-03-02</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Gujarati</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head>પોર્ટલેન્ડ, ઓરેગન, સોલ્ટ લેઇક સીટી, ઉટાહ,
કેનેડાના કેટલાંક ભાગો અને અલ સાલ્વાડોર પણ ધ્રુજી ઉઠ્યા</head></p>

<p><head>અમેરિકામાં ૬.૮નો શક્તિશાળી ધરતીકંપ</head></p>

<p><head>સિયેટલ અને વોશિંગ્ટનમાં સંખ્યાબંધ
ઇમારતોને ૧ અબજ ડોલરનું નુકશાન : ૨૫૦ને ઇજા : હ્ય્દય બંધ પડી જતાં ૧નું
મોત</head></p>

<p>સિયેટલ,તા.૧</p>

<p>આજે અમેરિકાના ઉત્તર પશ્ચિમના કેટલાક ભાગોમાં
અને જોડે આવેલાં કેનેડાના કેટલાક વિસ્તારોમાં ભૂકંપનો એક ખુબ જ શક્તિશાળી
આંચકો આવતાં અનેક મકાનોને ભારે નુકશાન થયું હતું અને કેટલીક ઇમારતો
ધરાશયી થઇ હતી. જેના પરિણામે લોકોને ઇજાઓ થઇ હતી અને હ્ય્દય બંધ પડી જતાં
એક વ્યક્તિનું મોત થયું હતું. ભૂકંપનો આંચકો એટલો તીવ્ર હતો કે રોડ ઉપર
મોટી તિરાડો પડી ગઇ હતી અને સત્તાવાળાઓને તાત્કાલીક વીજપુરવઠો અને હવાઇ મથકો
બંધ કરી દેવાની ફરજ પડી હતી.</p>

<p>છેલ્લા બે મહિનામાં ભૂકંપના બે મોટા આંચકાના
કારણે હજારો નાગરિકો ગુમાવનાર લેટિન અમેરિકાના અલ સાલ્વાડોરમાં પણ રિચર સ્કેલ ઉપર
૬ની તિવ્રતા ધરાવતો એક આંચકો આવ્યા હતો. જો કે આજના ભૂકંપથી કોઇ જાનહાની
થઇ નહોતી.</p>

<p>અમેરિકાના ઉત્તર પશ્ચિમ વિસ્તારોમાં આજે સવારે
૧૦.૫૪ વાગ્યે (ભારતીય પ્રમાણ સમય મુજબ બુધવારે રાત્રે ૦.૫૪ કલાકે) આવેલા ભૂકંપની
તિવ્રતા રિચર સ્કેલ ઉપર ૬.૮ની નોંધાઇ હતી. ભૂકંપની તિવ્રતાથી ગભરાઇ જતાં
સિયેટલના ઉપનગરમાં એક વ્યક્તિનું હ્ય્દય બેસી ગયું હતું. ભૂકંપનું કેન્દ્રબિંદુ સિયેટલથી
૧૦૦ કિલોમીટર દક્શિણે ઓલિમ્પિયા રાજ્યના પાટનગર પાસે ૫૦ કિલોમિટર ભૂગર્ભમાં
નોંધાયું હતું.</p>

<p>વોશિંગ્ટન રાજ્યના ગવર્નર ગેરી લોકે તાત્કાલિક
ઇમરજન્સી જાહેર કરી દીધી હતી અને ફેડરલ ઇમરજન્સી મેનેજમેન્ટની ટુકડીઓ ભૂકંપગ્રસ્ત
લોકોની મદદે દોડી ગઇ હતી.</p>

<p>ભૂકંપગ્રસ્ત વિસ્તારોનું હેલિકોપ્ટરમાં હવાઇ
નિરિક્શણ કર્યા બાદ ગવર્નર લોકે એકલાં વોશિંગ્ટનની આસપાસના વિસ્તારોમાં જ અબજ
ડોલરથી વધુનું નુકશાન થયાનો અંદાજ મૂક્યો હતો.</p>

<p>અમેરિકાના ઉડ્ડયન મંત્રાલયે કોઇ નુકશાન થયું છે
કે નહીં તેની ચકાસણી કરવા થોડાં સમય માટે સિયેટલ-ટેકોમાં એરપોર્ટ બંધ કરી
દીધું હતું પરંતુ બાદમાં મર્યાદિત ફ્લાઇટ માટે પુન: ખુલ્લુ કરી દેવાયું હતું.</p>

<p>ભૂકંપના કારણે વીજળીની લાઇનો બંધ થઇ ગઇ
હતી અને ઓલિમ્પિકના પાટનગરમાં આવેલાં ૧૮૦ મીટર ઉંચા ડોમમાં એક જબરજસ્ત તિરાડ
પડી ગઇ હતી. ઉલ્લેખનીય છે કે આ વિસ્તારમાં ૧૯૪૯માં ૭.૧ની તિવ્રતાવાળો ભૂકંપ આવતાં
૮ માણસોના મોત થયા હતા ત્યારબાદ સૌ પ્રથમવાર આટલો તિવ્ર ભૂકંપ આવ્યો હતો.</p>

<p>સ્વિડીશ મેડિકલ સેન્ટરના પ્રવક્તાએ કહ્યું હતું કે
ભૂકંપના આંચકાની તિવ્રતાના કારણે જમીન ઉપર ફેંકાઇ ગયેલાં કેટલાંક લોકોને માથામાં
ઇજાઓ થઇ હતી.</p>

<p>સિયેટલના ઉપનગર રેન્ટોન ખાતેના હારબ્રોવી
મેડિકલ સેન્ટરના મહિલા પ્રવક્તા ક્રિસ્ટીન ફોલોએ કહ્યું હતું કે ભૂકંપના કારણે જુદા
જુદા પ્રકારની ઇજાઓ પામેલાં ૧૯ લોકોને હોસ્પિટલમાં લવાયા હતા જેમાંથી ચારની સ્થિતિ
ગંભીર હતી.</p>

<p>ભૂકંપનો આંચકો અમેરિકાના દક્શિણે આવેલા
પોર્ટલેન્ડ, ઓરેગન અને પૂર્વના સોલ્ટલેક સીટી અને ઉટાહમાં પણ અનુભવાયો હતો.</p>

<p>ટીવીના કેમેરામેનોએ કેટલાય બહુમાળી
બિલ્ડીંગોમાં તિરાડો પડી ગયાના અને કેટલીય ઇમારતોના જમીન ઉપર તૂટી પડેલાં ભાગોના
દ્રશ્યો દર્શાવ્યા હતા. સિયેટલના સિમાચિહ્ન રૂપ ગણાતાં સ્પેસ નીડલ કોર્પ બિલ્ડીંગમાં
પ્રવાસીઓ તાત્કાલિક લિફ્ટ મારફતે નીચે ઉતરી પડ્યા હતા. આ બિલ્ડીંગે ભૂકંપના જબરજસ્ત
આંચકાનો અનુભવ કર્યો હતો પરંતુ કોઇને ઇજા થઇ નહોતી.</p>

<p>માઇક્રોસોફ્ટના ચેરમેન બિલ ગેટ્સે કંપનીના
સિયેટલ ખાતે આવેલાં વડામથકે એક પરિષદનું આયોજન કર્યું હતું પરંતુ ભૂકંપના
આંચકાની સાથે જ લોકો હોલ છોડી બહાર ભાગી ગયા હતા અને સીએનએનના ટીવી
કેમેરામેને આ દ્રશ્યને પણ દર્શાવ્યું હતું. સિયેટલનો ભૂકંપ ૪૫ સેકંડ ચાલ્યો હતો
પરંતુ કેન્દ્રબિંદુ ૫૦ કિલોમીટર ભૂગર્ભમાં હોઇ પ્રમાણમાં ઓછું નુકશાન થયું હતું.</p>

<p>યુએસ જીઓલોજીકલ સર્વેની કચેરીમાં
ભૂસ્તરશાસ્ત્રીઓએ આરંભમાં ભૂકંપની તિવ્રતા ૭.૦ પોઇન્ટની કહી હતી પરંતુ બાદમાં આ
ગણતરી પ્રાથમિક હતી એમ કહીને તિવ્રતાનો આંક સુધારી ૬.૮ કર્યો હતો.</p>






 
  
  <p> </p>
  
 









<p><head>બાલ્કોના મુદ્દે વિપક્શોની દરખાસ્તનો
લોકસભામાં થયેલો કરૂણ રકાસ</head></p>

<p>નવીદિલ્હી, તા. ૧</p>

<p>ભારત એલ્યુમિનિયમ કંપની (બાલ્કો)ને ખાનગી
ઔદ્યોગિક જૂથને વેચી મારવાના સરકારના નિર્ણયને નામંજૂર કરતી વિરોધપક્શોએ
દાખલ કરેલી દરખાસ્તનો આજે લોકસભામાં હાથ ધરાયેલાં મતદાનમાં કરૂણ રકાસ થયો
હતો. </p>

<p><head>૨૩૯ વિ. ૧૧૯ મતે સરકારનો વિજય</head></p>

<p>મતદાનમાં સરકારનો વિજય થતાં ડિસઇન્વેસ્ટમેન્ટ
વિભાગના મંત્રી અરૂણ શૌરીએ બાલ્કોના સોદામાં લાંચ અપાઇ હોવાના વિપક્શોના
આક્શેપને ફગાવી દીધો હતો. સીપીઆઈ (એમ)ના રૂપચંદ પાલે દરખાસ્ત રજુ કરી હતી
જેને કોંગ્રેસ, સમાજવાદી પાર્ટી, રાજદ, બસપ જેવા વિપક્શોએ ટેકો આપ્યો હતો.
મતદાન વખતે ૩૬૧ સભ્યો હાજર હતા જેમાં દરખાસ્તની તરફેણમાં માત્ર ૧૧૯ મતો પડ્યા હતા
જ્યારે ૨૩૯ સભ્યોએ આ દરખાસ્તની વિરુધ્ધમાં મત આપ્યા હતાં. આરંભમાં આ દરખાસ્ત
ઉપર મૌખિક મતદાન કરવાની વાત હતી પરંતુ વિરોધપક્શોએ આ શરત ફગાવી દેતાં
સ્પિકર જીએમસી બાલયોગીએ મતદાનનો આદેશ આપ્યો હતો.</p>

<p>વિપક્શોએ બાલ્કોને ખાનગી ઔદ્યોગિક જૂથને
વેચી મારવા બદલ વડાપ્રધાનના કાર્યાલયના અધિકારીઓએ રૂ. ૧૦૦ કરોડની લાંચ
લીધી હોવાના આક્શેપને શૌરીએ પાયાવિહોણા ગણાવ્યા હતા. દરખાસ્ત ઉપર હાથ
ધરાયેલી આઠ કલાકની લંબાણપૂર્વકની ચર્ચાનો જવાબ આપતા તેમણે ક્યું હતું કે આ
સોદાના કોઈપણ તબક્કે વડાપ્રધાન કાર્યાલયના અધિકારીઓએ કોઈ માહિતી જાણવા
માંગી નહોતી. </p>

<p>છત્તીસગઢના મુખ્યમંત્રી અજીત જોગીએ મૂકેલાં આ
આક્શેપ સામે ડિસઇન્વેસ્ટમંત્રી શૌરીએ તેમને આક્શેપ અંગે મજબૂત પુરાવા રજુ કરવા
પડકાર ફેંક્યો હતો આ મુદ્દે સંયુક્ત સંસદીય સમિતિ શા માટે તપાસ કરે ? જોગીને જ
સંડોવાયેલા અધિકારીઓના નામ જાહેર કરવા દોને અમે તેઓ સામે પગલાં ભરીશું
એમ તેમણે કહેતા ઉમેર્યું હતું કે વડાપ્રધાન કાર્યાલયના અધિકારીઓ તો માત્ર આ
સોદાની પ્રક્રિયા હાથ ધરી હતી. તેઓ પૈકીનો કોઈ અધિકારી સોદામાં
સંડોવાયેલો નથી.</p>




<p><head>રીઝર્વ બેન્કે બેન્ક રેટમાં વધુ અડધા
ટકાનો ઘટાડો કર્યો</head></p>

<p><head>બેન્ક રેટ ઘટીને હવે સાત ટકા થયો</head></p>

<p>મુંબઇ, તા. ૧</p>

<p>નાણાં પ્રધાન યશવંત સિંહાએ નાની બચતના
વ્યાજદરમાં કરેલા ઘટાડાને પગલે રીઝર્વ બેન્ક ઓફ ઇન્ડિયાએ પણ બેન્ક દરમાં અડધા
ટકાનો ઘટાડો કરી બેન્ક દર ૭ ટકાનો કર્યો છે, જેનો અમલ આજ ધંધાના
કલાકો બાદ કરવામાં આવશે.</p>

<p>રીઝર્વ બેન્કે તેના વકતવ્યમાં જણાવ્યું છે કે
આંતરરાષ્ટ્રીય અને ઘર આંગણેના નાણાં બજારનું પુનરાવલોકન કરતા બેન્ક દર ૭.૫ ટકાથી
ઘટાડીને ૭ ટકાનો કરવાનો નિર્ણય લીધો છે. છેલ્લાં એક પખવાડિયામાં રીઝર્વ
બેન્કે બીજીવાર બેન્ક દરમાં ઘટાડો કર્યો છે. નવા દર આવતી કાલથી જ અમલી બનશે.</p>

<p>૧૬ ફેબ્રુઆરીના રોજ આરબીઆઇએ ૮ ટકાના બેન્ક
દરને ઘટાડીને ૭.૫ ટકાનો કર્યો હતો. જ્યારે કેશ રીઝર્વ રેશિયોમાં બે તબક્કામાં
અડધા ટકાના ઘટાડાઓ ૮ ટકાનો કરવામાં આવ્યો. યુએસ ફેડરેલ રીઝર્વે ફોડ
રેટમાં બે તબક્કામાં જાન્યુઆરીમાં એક ટકાનો ઘટાડો કર્યો ત્યારે બેન્કર્સ એક ટકા
ઘટવાની અપેક્શા રાખતા હતા. યશવંત સિંહાએ આજે વહેલી સવારે જ વ્યાજદરમાં કાપનો
ઇશારો કર્યો હતો. તેમણે કહ્યું કે તે માટેની પૂર્વભૂમિકા રચી દીધી છે અને હવે
રીઝર્વ બેન્ક પર નિર્ભર છે તે ક્યારે બેન્ક દરમાં ઘટાડો કરે છે.</p>

<p>યુનિટ ટ્રસ્ટ ઓફ ઇન્ડિયાના ચેરમેન ટી. એસ.
સુબ્રમણ્યમે જણાવ્યું કે આરબીઆઇ પગલું અપેક્શિત હતું. નાણા પ્રધાને બજેટ
પ્રસ્તાવોમાં નાની બચતના દરો અને પ્રોવિડન્ટ ફંડના દરોમાં ઘટાડો કર્યો ત્યારે સ્પષ્ટ
ઇશારો હતો કે બેન્ક દરોમાં ઘટાડો થશે.</p>

<p>સિંહાએ ફિક્કી દ્વારા આયોજિત સેમિનારમાં
જણાવ્યું હતું કે વ્યાજ દરમાં સબસિડી સામેલ હોવાથી વાસ્તવિક વ્યાજદરો ઊંચા છે. તેમણે
બજેટમાં નાની બચતના દરોમાં ૧થી ૧.૫ ટકાના ઘટાડાનું સમર્થન કરતાં જણાવ્યું કે ઉંચા
વ્યાજ દરો પાછળ કોઇ આર્થિક તર્ક નથી.</p>

<p>તેમણે ફુગાવાના દર સાથે નાની બચતના દરો તથા
પીએફના દરોનું સામજસ્ય સ્થાપિત કરવા એક નિષ્ણાત કમિશન ગઠન કરવાનો પ્રસ્તાવ
ધરાવે છે જેથી ફુગાવાના દરને અનુરૂપ આપ મેળે આ દરોમાં પણ વધધટ
થાય.</p>

<p>સ્ટેટ બેન્ક ઓફ ઇન્ડિયાના ચેરમેન જાનકી વલ્લભે
જણાવ્યું હતું કે બેન્ક દરમાં ઘટાડાને પગલે હવે એસ્ટેટ લાયાબીલીટી કમિટી મિટીંગ કરશે
અને આવતા અઠવાડિયે નિર્ણય લેશે અમારા મતે વ્યાજ દર ઘટવા જોઇએ. એએલસી તેનું
પુનરાવલોકન કરશે અને આવતા અઠવાડિયે નિર્ણય લેશે.</p>

<p>ઇન્ડિયન બેન્કસ એસોસિએશનના સીઇઓ કે. સી.
ચૌધરીએ પણ જણાવ્યું કે બેન્કો પણ રીઝર્વ બેન્કના પગલે વ્યાજ દરો ઘટાડવા માટે
કામગીરી હાથ ધરશે. તેમણે કહ્યું કે આ ઘટાડાથી ૨૦૦૧ - ૦૨માં વિકાસ દર
વધારા તરફી બનશે.એબીએન અમર બેન્કના મુખ્ય અર્થશાસ્ત્રી અજિત રાનડેએ કહ્યું કે
દરમાં ઘટાડો અપેક્શિત હતો. પરંતુ આટલો જલ્દી ઘટશે તેવું ધાર્યું ન હતું.</p>

<p>અગ્રણી બેન્કના ટ્રેઝરી મેનેજરના જણાવવા પ્રમાણે
સરકારી ડેટ માર્કેટમાં સરકારી સિકયુરીટીઝના વળતર દર કંઇક અંશે સુધર્યા હતા
તેનું કારણ બજેટમાં નાની બચતના દરમાં મુકાયેલો કાપ છે.</p>




<p><head>૧૨૫ પોઇન્ટની તોફાની વધઘટ બાદ
ભાવાંકમાં ૨૫ પોઇન્ટનો સુધારો</head></p>

<p><head>બેંક, સિમેન્ટ, સ્ટીલ, ફાર્મા, ઓટો શેરોમાં
તેજીનો ચમકારો: રિલાયન્સ ઝળક્યો</head></p>

<p>અમદાવાદ, ગુરૂવાર</p>

<p>બજેટની દરખાસ્તોને દરેક ઉદ્યોગપતિ, અને
નિષ્ણાતોએ સાર્વત્રિક આવકાર આપ્યો હોવા છતાં આજે શેરબજારમાં ફરી એકવાર ઓલ્ડ
ઇકોનોમી શેરોમાં તેજીનો ચમકારો અને સોફ્ટવેર શેરોમાં મંદીનો આંચકો
અનુભવાયો હતો.</p>

<p><head>સોફ્ટવેર શેરોએ તેજીને બ્રેક મારી:
ગોલ્ડમેન સાચ્ચનું અપગ્રેડિંગ અને બેંક રેટમાં વધુ ઘટાડો તેજીનો ભડકો કરશે</head></p>

<p>બજેટ વિકાસ લક્શી અને મૂડીબજારને પ્રોત્સાહન
આપનારૂં ગણાતા આજે બજારનો પ્રારંભ મક્કમ ટોને થયો હતો. ખાસ કરીને ઓલ્ડ ઇકોનોમી
ક્શેત્રે તેજીના ચમકારા જોવા મળ્યા હતા. બેંકિંગ શેરો, ફાર્મા શેરો, ઓટોમોબાઇલ
શેરો તથા રિલાયન્સ અને સીમેન્ટ શેરોમાં જોરદાર ઉછાળો જોવા મળ્યો હતો.</p>

<p>પરંતુ બીજી બાજુ સોફ્ટવેરની આગેવાન
સ્ક્રીપોમાં અફવાઓ વચ્ચે ગાબડા પડયા હતા. યુએસએ ખાતે ધરતીકંપ થતાં કલકત્તાનાં
ઓપરેટરોની વેચવાલી ઉપડતાં અને મોર્ગન સ્ટેન્લી સહિત ઓવરસીઝ ફંડોની જોરદાર
વેચવાલી ઉપડી હતી. ખાસ કરીને ઇન્ફોસીસ, નીટ, સત્યમ, ઝી ટેલી, રોલ્ટા વગેરે જે
બુધવારે ઉછળવા પામ્યા હતા તે ઉછાલો આજે ટકી શક્યો નહોતો.</p>

<p>બજારના સૂત્રો પ્રમાણે આગેવાન મ્યુચ્યુઅલફંડ
મોર્ગન સ્ટેન્લી સહિત બીજા ફંડોની જોરદાર વેચવાલી રહી હતી. આમાં નાસ્ડાકની સતત
નરમાઇની અસર પણ વર્તાતી હતી. ઇન્ફોસીસ રૂા. ૫૭૬ તૂટીને ૫૬૮૩, ગ્લોબલ ટેલી રૂા. ૩૨ ઘટી
૩૭૨, નીટ રૂા. ૭૩ તૂટી રૂા. ૧૧૭૦, ઝી ટેલી રૂા. ૯ ઘટી ૧૬૨.૩૦, પેન્ટામીડીયા રૂા. ૧૭.૫૦
તૂટી ૧૬૨, સીલ્વર લાઇન રૂા. ૧૨ ઘટી ૧૭૩, એચસીએલ ઇન્ફો રૂા. ૧૧.૬૦ તૂટી ૨૦૯.૫૦ અને
એપ્ટેક ૧૧.૫૦ ઘટી ૨૦૭.૬૦, એસએસઆઇ લિ. રૂા. ૬૨ તૂટી ૧૨૧૦ અને વિપ્રો રૂા. ૧૨૪ ગગડી
રૂા. ૨૪૨૦ તથા ડીજીટલ રૂા. ૩૧ ઘટી ૬૭૯.૫૦ રહ્યો હતો.</p>

<p>જો કે હિમાચલમાં કંપનીને ટેક્શમાં સારો એવો
ફાયદો થવાની ગણત્રીએ ભાવ રૂા. ૪૮ વધીને ૭૧૮ રહ્યો હતો.</p>

<p>સીમેન્ટ શેરોમાં લેવાલી વધી હતી. એસીસી
રૂા. ૧૧.૫૦ વધી ૧૮૯.૩૫, ગ્રાસીમ રૂા. ૧૩ વધી ૩૧૨.૨૫, અંબુજા સીમેન્ટ ૧૨.૨૫
વધી ૨૦૦.૪૫ અને લાર્સન રૂા. ૧૮.૨૫ ઉછળી રૂા. ૨૭૯.૬૦ રહ્યો હતો.</p>

<p>જાહેરક્શેત્રની બેંકોને કાર્યદક્શતા સુધારવા
વધુ સ્વતંત્રતા અપાતા, બેકિંગ સર્વિસ રીક્રુટમેન્ટ બોર્ડ વિખેરી નંખાતા તથા
ધિરાણમાં ઉઠાવ વધવાની ગણત્રીએ આજે બેંક શેરોના ભાવો વધુ ઉચકાયા હતા.
જેમકે કોર્પોરેશન બેંક રૂા. ૧૦.૪૫ ધી રૂા. ૧૩૯, આઇસીઆઇસીઆઇ બેંક રૂા. ૨૮
ઉછળી રૂા. ૨૦૫.૭૦, અને સ્ટેટ બેંક રૂા. ૧૭ વધી રૂા. ૨૬૧ રહ્યો હતો. મોડી સાંજે
રીઝર્વ બેંકે બેંક રેટમાં વધુ અડધા ટકાનો ઘટાડો કરતાં ખાનગીમાં સ્ટેટ બેંકનો
ભાવ વધીને રૂા. ૨૬૫-૨૬૬ જેવો બોલાતો હતો.</p>

<p>ઓટોમોબાઇલ ક્શેત્રે એકસાઇઝમાં ઘટાડાએ
પેસેન્જર કાર અને સ્કૂટરના ભાવોમાં ઘટાડા કરાઇ રહ્યા છે. આથી માંગ વધવાની
ધારણાએ ભાવો ઉચકાઇ રહ્યા છે. હિરો હોન્ડા રૂા. ૮૭ ઉછળીને રૂા. ૯૦૦, ટેલ્કો
રૂા. ૪.૪૦ વધી ૧૦૬.૭૫, બજાજ ઓટો રૂા. ૧૪.૩૦ વધી ૩૩૯ રહ્યો હતો.</p>

<p>ફાર્મા ક્શેત્રે આજે લેવાલી વધતાં સિપ્લા
રૂા. ૫૪ ઉછળી રૂા. ૧૦૬૮, ગ્લેક્સો રૂા. ૨૨ ઉછળી રૂા. ૪૮૪, રૈનબક્શી રૂા. ૩૨.૭૦ ઉછળી રૂા.
૭૨૨.૫૦ રહ્યો હતો. ઉપરાંત ડો. રેડ્ડી પણ ઉચકાયો હતો.</p>

<p>સીમેન્ટ શેરોમાં નવેસરથી લેવાલી નીકળી હતી.
એસીસી, ગ્રાસીમ, અંબુજા સીમેન્ટ, લાર્સનમાં ઉછાળો જોવા મળ્યો હતો.</p>

<p>આજે રિલાયન્સમાં તેજીનો ભડકો જોવા મળ્યો
હતો. જાણકાર વર્તુળોની ધૂમ લેવાલી અને કંપનીમાં પોલીયેસ્ટર વગેરેના ભાવોમાં
વધારો કર્યાના અહેવાલોની સારી અસર જોવા મળી હતી. આજે ભાવ ઉછળીને ૪૪૫.૫૦ થઇ
છેલ્લે રૂા. ૨૫.૫૦ જેટલો વધીને રૂા. ૪૩૯.૫૫ બંધ રહ્યો હતો. રિલા.
પેટ્રોમાં શરૂમાં આવેલ સુધારો પાછળથી ટકી શક્યો ન્હોતો.</p>

<p>એકંદરે આજે ૧૧૫ પોઇન્ટ જેટલો સેશનની
શરૂઆતમાં ઉછાળો જોવા મળ્યો હતો. પરંતુ કલકત્તા ખાતે છેલ્લાં બે દિવસ હોવાથી
તેજીવાળા ખેલાડીઓ નબળા પડી ગયા હોવાથી અને નાસ્ડાક તૂટતો આવતો રહેતાં છેલ્લે
ભાવાંક તૂટયો હતો. બીએસઇ-૩૦ ભાવાંક ૪૨૮૮ ખુલી ૪૩૮૭ અને ૪૨૧૫ વચ્ચે અથડાઇ છેલ્લે
૪૨૭૨ બંધ હતો જે આગલા બંધ ૪૨૪૭ની સરખામણીમાં ૨૫ પોઇન્ટનો ઘટાડો
દર્શાવે છે. બોલ્ટમાં ટર્નઓવર રૂા. ૫૫૯૮ કરોડ નોંધાયું હતું.</p>

<p>ટૂંકમાં બજેટને મળેલ સાર્વત્રિક આવકાર, બેંક
રેટમાં વધુ અડધા ટકાનો કરાયેલ ઘટાડો અને ગોલ્ડમેન સાચ્ચ દ્વારા બજેટની
દરખાસ્તો બાદ ભારતનું રેટીંગ અપગ્રેડ કરાયાના સમાચારો બજારમાં આગળ જતાં તેજીનો
ભડકો કરે તો નવાઇ નહિ.</p>

<p>દરમ્યાન આજે ફોરેક્સ માર્કેટમાં ડોલર સામે
રૂપિયો વધીને ૪૬.૪૯/૫૧ થવા બાદ છેલ્લે ૪૬.૫૪૫૦/૫૫૫૦ હતો. આગલો બંધ
૪૬.૫૫/૫૬ હતો.</p>

<p>ઝવેરી બજારમાં બુધવારે બજેટમાં આયાત
જકાત ઘટાડાયા બાદ સોનાનો ભાવ ૧૦ ગ્રામ દીઠ રૂા. ૧૨૦ જેટલો ગગડયા બાદ નીચા મથાળે
સાધારણ પૂછપરછ જણાતી હતી. હોંગકોંગ ખાતે ભાવ ૨૬૫.૫૦ ડોલરવાળો વધીને
૨૬૬.૩૦ ડોલર દર ટ્રોય ઔંસ દીઠ આવ્યો હતો. વર્લ્ડ ગોલ્ડ કાઉન્સિલે સોનાની આયાત
જકાત રૂા. ૪૦૦થી ઘટાડીને રૂા. ૨૫૦ કરવાની દરખાસ્તને આવકાર આપ્યો હતો. આના કારણે
આંતરરાષ્ટ્રીય અને ભારતમાં સોનાના ભાવ વચ્ચેનો તફાવત ઘટશે. આ નિર્ણયથી સરકારની આવક
ઘટવાને બદલે વધશે તેવી ધારણા વર્લ્ડ ગોલ્ડ કાઉન્સિલે દર્શાવી છે.</p>




<p><head>ભારત ૨૧૯, ઓસી. વિના વિકેટે ૪૭</head></p>

<p><head>ભારતની કંગાળ બેટીંગના લીધે પ્રથમ ટેસ્ટ ૧૦
વિકેટે જીતતું ઓસ્ટ્રેલિયા</head></p>

<p><head>ગિલક્રિસ્ટ મેન ઓફ ધ મેચ</head></p>

<p>મુંબઇ, તા. ૧</p>

<p>ભારતીય બેટસમેનોએ તેમનો નબળો દેખાવ યથાવત
રાખવા ઓસ્ટ્રેલિયાએ ત્રણ ટેસ્ટ શ્રેણીની પ્રથમ ટેસ્ટ ૧૦ વિકેટે ત્રણ દિવસથી પણ ઓછા
સમયમાં જીતી પોતાનો સળંગ ૧૬મો ટેસ્ટ વિજય મેળવ્યો હતો.</p>

<p>સચિન તેંડુલકરની ૬૫ રનની ક્લાસિક ઇનિંગનો
રિકી પોન્ટીંગે મીડ વિકેટ પર કરેલા એક અશક્ય કેચ દ્વારા અંત આવવાની સાથે જ ભારતીય
બેટસમેનોએ તુ જા હું આવું છુંની પરંપરા ચાલુ કરી દીધી હતી. પછી તો ૨૧૯
રનમાં ભારતનો ધબડકો થઇ જતા ઓસી.એ જીતવા માટે જરૂરી ૪૭ રન માત્ર સાત
ઓવરમાં જ બનાવી લઇ ભારતને કોઇપણ પ્રકારની પીચ પર હરાવવાના તેમના કેપ્ટન સ્ટીવ વોના
વિધાનને સત્ય ઠેરવ્યું હતું. સ્વાભાવિક રીતે મેન ઓફ ધ મેચ આક્રમક સદી કરી
ભારતના હાથમાંથી મેચ ખૂંચવી લેનાર અને ટેસ્ટમાં કુલે છ કેચો કરનાર વાઇસ કેપ્ટન ગિલ
ક્રિસ્ટને અપાયો હતો.</p>

<p>ગઇકાલના ૨ વિકેટે ૫૮ રનથી આજે આગળ દાવની
શરૂઆત કર્યા બાદ તેડુંલકર અને દ્રવિડે પ્રથમ સત્રમાં કોઇપણ વિકેટ પડવા ન દેતા ભારત
તેઓને સારી લીડ આપે તેવી આશા બંધાઇ હતી. આ દરમ્યાન દ્રવિડના એક પુલ
શોટનો સ્લેટરે કરેલો કેચ ત્રીજા અમ્પાયરે અયોગ્ય ઠેરવતા તેણે બિનખેલદિલીભર્યું વર્તન
કરતા અમ્પાયર વેંકટ રાઘવન અને દ્રવિડ સાથે દલીલો કરી હતી. દ્રવિડ અને સચિનની જોડીને
તોડતા છેવટે કેપ્ટન સ્ટીવ વોએ તેના ભાઇ માર્ક વોને હરભજનને મળેલી સફળતા જોઇ લગાવ્યો
હતો. તેની જ ઓવરમાં સચિનનો એક આક્રમક ફટકો શોર્ટ લેગ પર ઉભેલા લેન્ગરના ખભે
અથડાઇ મીડવીકેટ તરફ જતાં પોન્ટીંગે અશક્ય લાગતો કેચ ઝડપી લીધો હતો. પછી તો
જાણે ભારતના રકાસની ટ્રીગર ઓફ થઇ.</p>

<p>કેપ્ટન ગાંગુલી નિશ્ચિત જણાતા રનમાં રનઆઉટ થયો
જ્યારે એક જીવતદાન બાદ વોર્નને ત્રણ ચોગ્ગા ફટકારી સારા સંકેત આપનાર લક્શ્મણ માર્ક
વોના એક ઉછળેલા બોલને કટ કરવાના પ્રયાસમાં ગિલક્રિસ્ટના હાથમાં ઝીલાઇ ગયો હતો.
જ્યારે સેટ થઇ ગયેલો દ્રવિડ વોર્નના શાર્પ ટર્નમાં બોલ્ડ થયો હતો. અગરકરે ઓસી.
સામે શૂન્યની પરંપરા જાળવી રાખતા માર્ક વોની ઓવરમાં બોલ્ડ થઇ સળંગ સાતમું શૂન્ય
નોંધાવ્યું હતું. માર્ક-વો- ગિલિસ્પીએ ત્રણ-ત્રણ, મેકગ્રાથે બે તથા વોર્ને એક
વિકેટ ઝડપી હતી.</p>

<p>સ્કોરબોર્ડ</p>

<p>ભારત પ્રથમદાવ ૧૭૬</p>

<p>ઓસ્ટ્રેલિયા પ્રથમદાવ ૩૪૯</p>

<p>ભારત બીજો દાવ</p>

<p>એસ. દાસ કો. સ્ટીવ વો </p>

<p>બો. ગિલિસ્પી ૦૭</p>

<p>એસ. રમેશ કો. પોન્ટીંગ </p>

<p>બો. મેકગ્રાથ ૪૪</p>

<p>દ્રવિડ બો. વોર્ન ૩૯</p>

<p>મોંગીયા કો. ગિલક્રિસ્ટ બો. ગિલિસ્પી ૨૮</p>

<p>તેંડુલકર કો. પોન્ટીંગ બો. માર્ક વો ૬૫</p>

<p>ગાંગુલી રનઆઉટ ૦૧</p>

<p>લક્શ્મણ કો. ગિલક્રિસ્ટ બો. માર્ક વો ૧૨</p>

<p>અગરકર બો. માર્ક વો ૦૦</p>

<p>હરભજન અણનમ ૧૭</p>

<p>સંઘવી બો. ગિલિસ્પી ૦૦</p>

<p>શ્રીનાથ બો. મેકગ્રાથ ૦૦</p>

<p>વધારાના બાય-૫, નોબોલ-૧ ૦૬</p>

<p>કુલ ૯૩.૧ ઓવરમાં રનઆઉટ ૨૧૯</p>

<p>વિકેટ પડવાનો ક્રમ: ૧/૩૩, ૨/૫૭, ૩/૧૫૪, ૪/૧૫૬, ૫/૧૭૪, ૬/૧૭૪, ૭/૧૯૩,
૮/૨૧૦, ૯/૨૧૬</p>

<p>બોલીંગ: મેકગ્રાથ ૧૭.૧-૯-૨૫-૨, ફ્લેમીંગ ૧૫-૧-૪૪-૦, વોર્ન
૨૮-૧૧-૬૦-૧, ગિલિસ્પી ૧૯-૮-૪૫-૩, માર્ક વો ૧૫-૫-૪૦-૩</p>

<p>ઓસ્ટ્રેલિયા બીજો દાવ</p>

<p>હેડન અણનમ ૨૮</p>

<p>સ્લેટર અણનમ ૧૯</p>

<p>કુલ ૭ ઓવરમાં વિના વિકેટે ૪૭</p>

<p>બોલીંગ : શ્રીનાથ ૨-૦-૧૭-૦, અગરકર ૧-૦-૮-૦, હરભજનસિંઘ ૨-૦-૧૧-૦,
સંઘવી ૨-૧-૧૧-૦</p>




<p><head>એનઆરઆઇ એવોર્ડમાં સંજય, ૠત્વિક, શાહરૂખ
વચ્ચે સ્પર્ધા</head></p>

<p><head>૨૮મી એપ્રિલે આ એવોર્ડ અપાશે</head></p>

<p>ન્યુયોર્ક, તા. ૧</p>

<p>ન્યુયોર્કના બિનનિવાસી ભારતીયો દ્વારા અપાતા
બોલીવુડ એવોર્ડસમાં બેસ્ટ બોલીવુડ એક્ટરની કેટેગરીમાં શાહરૂખખાન, સંજય દત્ત અને
બોલીવુડના હોટ ફેવરિટ નવોદિત અભિનેતા ૠત્વિક રોશનનું પણ નામાંકન કરાયું છે.</p>

<p>એપ્રિલની ૨૮મીએ અપાનાર આ એવોર્ડની બેસ્ટ
એક્ટ્રેસની કેટેગરીમાં ફિલ્મ અસ્તિત્વ માટે તબ્બુ, હમારા દિલ આપ કે પાસ હૈ ફિલ્મ માટે
ઐશ્વર્યા રાય અને ફીઝા માટે કરિશ્મા કપુરના નામાંકન કરાયા છે.</p>

<p>કહોના પ્યાર હૈ, મિશન કાશ્મીર અને મહોબ્બતેને
શ્રેષ્ઠ ફિલ્મો તરીકે પસંદગી અપાઈ છે.</p>

<p>આ ઉપરાંત ભૂતકાળના મેટેની આઇડોલ
દેવઆનંદને માઈલેની પ્રજાના એવરગ્રીન સ્ટારનો પ્રતિષ્ઠિત એવોર્ડ અપાશે. જ્યારે અશોક અમૃતરાજને
ભારતના ગૌરવ (પ્રાઇડ ઑફ ઇન્ડિયા) એવોર્ડ અપાશે.</p>

<p>બિનનિવાસી ભારતીયોમાં મહોબ્બતે, કહો ના
પ્યાર હૈ, મિશન કાશ્મીર અને ફીઝા સૌથી વધુ લોકપ્રિય બની છે. એમ બોલીવુડ એવોર્ડસના
સી.ઇ.ઓ. કમલ ડાંડોનાએ ગઈકાલે કહ્યું હતું.</p>

<p>નામાંકનોમાંથી અંતિમ વિજેતા જાહેર કરવા માટે
બિનનિવાસી ભારતીયોમાં ચોઇસ પોલના પરિણામ અને નિષ્ણાતોની પેનલની પસંદગી પર
આધાર રખાશે.</p>

<p>યુ.એસ.ના બિનનિવાસી ભારતીયોમાં બોલીવુડ
ફિલ્મો ખૂબ જ લોકપ્રિય છે અને હવે તો યુ.એસ.ના અનેક સિનેમાગૃહો હિન્દી ફિલ્મો
નિયમિત બતાવે છે એમ ડાંડોનાએ કહ્યું હતું. અમારા ચોઇસ પોલમાં
એન.આર.આઇ.ઓએ અદ્ભુત પ્રતિભાવ આપ્યો છે એમ કમલ ડાંડોનાએ ઉમેર્યું હતું.</p>




<p><head>હિન્દુ સ્ત્રીઓ કર હેતુ માટે એચયુએફ રચી
શકે નહિ</head></p>

<p>નવી દિલ્હી, તા. ૧</p>

<p>સુપ્રિમ કોર્ટે ઠરાવ્યું હતું કે વારસાગત
મિલ્કતોમાંથી થતી આવક ઉપર લાગતા કરવેરામાંથી મુક્તિ મેળવવા હિન્દુ સ્ત્રીઓ કરાર
કરીને સંયુક્ત કુટુંબ ઉભું કરી શકે નહિ.</p>

<p><head>હિન્દુ પર્સનલ લૉ મુજબ પુરુષોની હાજરી
અનિવાર્ય છે : સુપ્રિમકોર્ટ</head></p>

<p>તાજેતરમાં જ આપેલાં એક ચુકાદામાં કોર્ટે
કહ્યું હતું કે હિન્દુ સ્ત્રીઓ અંદરોઅંદર ભેગી થઈ અવિભક્ત કુટુંબ ઉભું કરે એ વિચાર
હિન્દુ પર્સનલ લૉ ની વિરુદ્ધ છે કેમકે અવિભક્ત કુટુંબની રચનામાં પુરુષો હોવા જરૂરી
છે.</p>

<p>ન્યાયમૂર્તિ સર્વશ્રી એસ.પી. ભરૂચા, એન. સંતોષ
અને વાય. કે. સભરવાલની બેંચે બિહારના આવકવેરા કમિશનરે ફાઈલ કરેલી અરજીને માન્ય
રાખતા કહ્યું હતું કે હિન્દુ સ્ત્રીઓ ભેગી થઈને અવિભક્ત કુટુંબ બનાવી શકે કે કેમ તે અંગે
તેઓની ક્શમતા વિશે જ અમને ચિંતા થાય છે.</p>

<p>આ બાબતને ટેકો આપવા હજુ સુધી કોઈ
સત્તાવાળાઓનું ધ્યાન ગયું નથી અને તેઓ ટેકો આપી પણ ન શકે છતાં અહિંયા જે વિચાર
રજૂ કરાયો છે તે હિન્દુ પર્સનલ લૉ વિરુદ્ધનો છે. આ કાયદા મુજબનો સંયુક્ત કુટુંબની
રચના માટે પુરુષોની હાજરી અનિવાર્ય છે.</p>




<p><head>ભરત શાહ નિર્મિત ફિલ્મ પર પ્રતિબંધની બજરંગ
દળની માગણી</head></p>

<p>નવીદિલ્હી, તા.૧</p>

<p>બજરંગદળે આજે 'ચોરી ચોરી ચૂપકે ચૂપકે'
હિંદી ફિલ્મ માટે આઈએસઆઈ અને મુંબઇની અંધારી આલમની ધરી તરફથી નાણાં
પૂરાં પડાયાં હોવાનો આક્શેપ કર્યો હતો, તેના ઉપર પ્રતિબંધ મૂકવાની માંગણી
કરી હતી અને ફિલ્મના અભિનેતા તથા આવા રાષ્ટ્રવિરોધી સાહસો સાથે સંકળાયેલા
તમામનો વિરોધ કરવાની ધમકી ઉચ્ચારી હતી.</p>

<p>સંઘ પરિવારના આ સંગઠનના રાષ્ટ્રીય કન્વીનર
સુરેન્દ્ર જૈને અહીંયા પત્રકારોને જણાવ્યું હતું કે, અમે આ ફિલ્મ પર પ્રતિબંધ
મૂકવાની માંગણી કરીએ છીએ. અંધારી આલમના ડૉન અને દુબઇ સ્થિત દાઉદ ઇબ્રાહીમના
નિકટના સાથીદાર છોટા શકીલે આ ફિલ્મ માટે નાણાં આપ્યાં છે. આ બાબતમાં અમે જનતાને
જાગ્રત કરીશું કે આવી ફિલ્મો જોવા માટે ખર્ચાતાં નાણાં રાષ્ટ્રવિરોધી તત્વોના
ખિસ્સામાં પહોંચી જાય છે અને તે નાણાંથી દેશમાં આઈ.એસ.આઈ.ની પ્રવૃત્તિનો પ્રચાર થાય
છે.</p>

<p>તમારો વિરોધ હિંસક હશે ખરો? એમ
પૂછાતાં જૈને કહ્યું હતું કે આ ફિલ્મ ના દર્શાવાય તે જોવા અમે તમામ રીતો અખત્યાર
કરીશું. તેમણે કહ્યું કે, અમારૂં બજરંગદળ વિશ્વ હિંદુ પરિષદની યુવા પાંખ છે.
અંધારી આલમ અને બોલીવુડ વચ્ચેની ધરી સાથે સંકળાયેલાં કલાકારો અને
બીજાઓનો વિરોધ કરવામાં આવશે.</p>

<p>'ચોરી ચોરી ચૂપકે ચૂપકે'ના ફાયનાન્સર ભરત
શાહ, નિર્માતા નસીમ રઝવી અને તેમના સહાયક રહીમ અલ્લાબક્શ અંધારી આલમ સાથેના
તેમના સંબંધોને કારણે હાલ જેલમાં ધકેલી દેવાયેલા છે અને તાજેતરમાં જ
કોર્ટે આ ફિલ્મ દર્શાવવાની છૂટ આપી હતી.</p>




<p><head>બજેટ બેશક શ્રેષ્ઠ તૈયાર કરાયું છે, પરંતુ
દેશનું ભાવિ હજુ અનિશ્ચિત: દિનેશ વ્યાસ</head></p>

<p><head>સરકારી ખર્ચમાં ઘટાડો, મજદૂર કાયદામાં
સુધારા, સહિતના અનેક સરાહનીય આકરાં પગલાં લેવાયા છે.</head></p>

<p>મુંબઇ,તા.૧</p>

<p>નાણાંપ્રધાન યશવંત સિંહાએ રજૂ કરેલું
કેન્દ્રિય બજેટ બેશક સારૂં છે, પરંતુ દેશનું ભાવિ હજુ અનિશ્ચિત છે, નાણાંપ્રધાને બજેટ
કવાયત બખુબી શ્રેષ્ઠ કરી છે અને ઓપરેશન સફળતાપૂર્વક પાર પાડયું છે, પરંતુ દર્દી
(દેશ)ની હાલત નાજુક છે' એમ વરિષ્ઠ ધારાશાસ્ત્રી દિનેશ વ્યાસે આજે અહીં તેમના
બજેટ બાદના વિશ્લેષક પ્રવચનમાં જણાવ્યું હતું.</p>

<p>બજેટનું વિશ્લેષણ કરતા તેમણે વધુ જણાવ્યું
હતું કે, આ બજેટમાં યશવંત સિંહાએ આર્થિક અને રાજકીય રીતે ચાર મહત્વના આકરાં કહી
શકાય એવા પગલાં લીધા છે સરકારી ખર્ચ ઘટાડવાનો રાજકીય રીતે સંવેદનશીલ સફળ
પ્રયાસ તેમણે કર્યો છે. આ રીતે પ્રથમ વખત ગંભીર અને ચોક્કસ દિશામાં આ પગલું લેવાયું
છે. બીજું મજદૂર ધારામાં સુધારો કરી ૧૦૦ને બદલે ૧૦૦૦ સુધી
કામદારો ધરાવતા એકમોને કામદારોને છૂટા કરવાની પરવાનગી આપવાનું આકરું
પગલું લેવાયું છે. ટ્રેડ યુનિયનોને વધુ પડતા રક્શણ આપવાને કારણે ઉત્પાદકતા અને
ગુણવત્તા પર વિપરીત અસર પડતી હતી. ભારતીય બિઝનેસ અને ઉદ્યોગ માટે આ સંબંધમાં
પગલું લેનાર સિન્હા પ્રથમ નાણાંપ્રધાન છે.</p>

<p>આ ઉપરાંત ભારતીય ઉદ્યોગને વિદેશોમાં રોકાણ
કરવાની અને આંતરરાષ્ટ્રીય બજારોમાં હરિફાઈ કરવા શ્રેષ્ઠ તક આપવાનું શ્રેષ્ઠ પગલું
લેવાયું છે. એડીઆર અને જીડીઆરના ભંડોળને વિદેશી કંપનીઓ હસ્તગત કરવા ઉપયોગમાં
લેવાની છૂટ અને રૂપિયાને સંપૂર્ણ તરતો મૂકવાની દિશામાં પગલાં આવકાર્ય લેવાય છે.
ભારતીય સંસદના ઈતિહાસમાં સિન્હાએ આ વખતથી ગત વર્ષના બજેટની દરખાસ્તોનો અમલ
કેટલો થયો એનો રિપોર્ટ રજૂ કરવાનું પ્રથમ વખત શરૂ કર્યું છે. આ પ્રકારના રિપોર્ટમાં
અમલીકરણ અને નહિં અમલ થઈ શકેલી દરખાસ્તો બન્નેની કારણો સાથે રજૂઆત કરવી જોઈએ એ
મુદ્દો સૂચવ્યો હતો. કંપનીઓ માટે ડિવિડન્ડ ટેક્શમાં ઘટાડા અને ગુજરાત સિવાયના
સરચાર્જમાં નાબૂદીને કંપનીઓ અને અર્થતંત્રના વિકાસ માટે શ્રેષ્ઠ પગલું ગણ્યું હતું. આ
સિવાય વન-બાય-સીક્સ સ્કીમ સમગ્ર દેશમાં દાખલ કરવાનું અને નિકાસ ઝોન્સ માટે ઇન્ફ્રાસ્ટ્રકચર
અને ટેલિકોમ્યુનિકેશન માટે ટેક્શ હોલીડે સંબંધિત જોગવાઈના સરાહનીય પગલાં
લેવાયા છે મોટાભાગની બજેટ દરખાસ્તો સારી છે, પરંતુ દેશમાં વધતી જતી
બેરોજગારી અને દેશભરમાં વીઆરએસના અમલ અને છટણીને કારણે સામાજીક રીતે પરિસ્થિતિ
તંગ બની શકે એમ છે. આ સાથે લઘુ ઉદ્યોગો અને કેટલાક મધ્યમ કદના ઉદ્યોગો
મલ્ટિનેશનલોના આક્રમણને પરિણામે મુશ્કેલીનો સામનો કરી રહ્યા છે. આ રીતે નિયંત્રિત
અર્થતંત્રમાંથી મુક્ત બજારના અર્થતંત્ર તરફ વળવાથી તેના ગંભીર પરિણામોનો સામનો પણ
કરવો પડી શકે છે. દેશમાં આ સંજોગોમાં બેરોજગારી વધતા ગુનાખોરીનું પ્રમાણ
વધી શકે છે.</p>




<p><head>અફઘાનિસ્તાનમાં ભગવાન બુદ્ધની તમામ પ્રતિમાઓ
તોડી પાડવાનું શરૂ કરાયું</head></p>

<p>કાબુલ, તા. ૧</p>

<p>અફઘાનિસ્તાનના તાલીબાન શાસકોએ આજે કહ્યું
હતું કે તેમણે અફઘાનિસ્તાનમાં આવેલી ભગવાન બુદ્ધની તમામ પ્રતિમાઓ તોડી પાડવાનું
કાર્ય શરૂ કર્યું છે. આ બુદ્ધની પ્રતિમાઓમાં અફઘાનિસ્તાનના બામિયાન પ્રાંતમાં આવેલી વિશ્વની
સૌથી ઉંચી બુદ્ધની પ્રતિમાઓનો પણ નાશ કરાશે.</p>

<p><head>તાલીબાન શાસકોના નિર્ણયની ચોમેરથી
ટીકા: પ્રતિમાઓ ઇસ્લામ ધર્મની વિરૂદ્ધ છે</head></p>

<p>બામિયાન ખાતેની પ્રતિમાઓ તોડવાનું કાર્ય પાંચ
કલાક પહેલા શરૂ થઇ ચૂક્યું છે. પણ પ્રતિમાઓ કેટલી તોડી પડાઇ છે એ અંગે મને માહિતી
નથી એમ તાલિબાનના માહિતી અને સંસ્કૃતિ મંત્રી કુદ્રાતુલ્લાહ જમાલે જણાવ્યું હતું.
મંત્રીએ ઉમેર્યું હતું કે આ પ્રતિમાઓ કોઇપણ ભોગે તોડી પડાશે. અને એ કાર્ય ચાલુ
થઇ ગયું છે.</p>

<p>તેણે જણાવ્યું હતું કે તાલીબાનના લશ્કરી જવાનો
કાબુલ મ્યુઝિયમમાં રખાયેલા બુદ્ધની પ્રાચિન પ્રતિમાઓનો પણ નાશ કરી રહ્યા છે. આ ઉપરાંત
ગઝની, હેરાટ, જલાલાબાદ અને કંધાર પ્રાંતોમાં પણ પ્રતિમાઓ તોડવાનું શરૂ થઇ ગયું
છે. તાલિબાન મિલીશીયાના સર્વોચ્ચ વડા મુલ્લા મોહમ્મદ ઓમરે ગત સોમવારે ઇસ્લામિક
કાનૂન અનુસાર આ બધી પ્રતિમાઓનો નાશ કરવાનું એલાન આપ્યું હતું. મહોમ્મદ
ઓમરના આ હુકમથી વિશ્વભરમાં આઘાતની લાગણી ફેલાઇ છે.</p>

<p>અફઘાનિસ્તાનમાં ઇસ્લામ આવ્યા પૂર્વેનો
ઐતિહાસિક ખજાનો મનાતી પ્રતિમાઓ ને સ્મારકો આવેલા છે. પ્રાચિન રેશમના વ્યાપારમાં
અફઘાનિસ્તાન મહત્વનું વિરામ સ્થળ હતું. આ ઉપરાં મહાન વિજેતા એલેકઝાન્ડર ધ ગ્રેટ
માટે અફઘાનિસ્તાન યુદ્ધ રચનાની દ્રષ્ટિએ મહત્વનું યુદ્ધ ક્શેત્ર બન્યું હતું.</p>

<p>બામિયાન પ્રાંતમાં આવેલી બુદ્ધની બે વિશાળ
પ્રતિમાઓ અનુક્રમે ૫૦ મિટર અને ૩૪.૫ મિટર ઊંચી છે. આ પ્રતિમાઓનું નિર્માણ બીજી સદીમાં
થયું હતું. આ પ્રતિમાઓને સંભાળ માટે યુ.એસ., ફ્રાન્સ, થાઇલેન્ડ, જાપાન, શ્રીલંકા, ઇરાન
અને યુ.એન.ના જનરલ સેક્રેટકી હોકી અન્નાને અપિલ કરી હતી.</p>

<p>યુ.એન.ના શૈક્શણિક વૈજ્ઞાનિક અને સાંસ્કૃતિક
સંગઠ્ઠન (યુનેસ્કો)ના વડા કોચીરો માત્સુરાએ કહ્યું હતું કે પ્રાચિન પ્રતિમાઓના નાશથી
સાંસ્કૃતિક વિનાશ સર્જાયો અને વિશ્વ માટે એક મોટી સાંસ્કૃતિક નુકશાન થશે.</p>




<p><head>ખાનગીકરણની દિશામાંથી પીછેહઠનો સવાલ જ પેદા
થતો નથી</head></p>

<p>નવી દિલ્હી, તા. ૧</p>

<p>નાણાંમંત્રી યશવંતસિંહાએ આજે દ્રઢતાપૂર્વક
કહ્યું હતું કે નફો કરતાં કે ખોટ કરતા તમામ બિન વ્યુહાત્મક જાહેર સાહસોનું ખાનગીકરણ
કરવાની દિશામાંથી પીછેહેઠ થશે નહીં.</p>

<p><head>અલબત્ત વ્યુહાત્મક દ્રષ્ટિએ મહત્વના જાહેર સાહસોનો
દરજ્જો જાળવી રાખવા વિચારીશું</head></p>

<p>બજેટની પૂર્વ તૈયારી દરમ્યાન અમે તમામ
પાસાઓનો ખૂબ જ કાળજીપૂર્વક અભ્યાસ કર્યો હતો અને જાહેર સાહસોના ખાનગીકરણની
દિશામાં આગેકૂચ જારી રાખવાનો નિર્ણય કર્યો હતો. તેથી આ બાબતે કોઇએ શંકા
રાખવાની જરૂર નથી એમ સિંહાએ યુનિયન બજેટ અંગે ફિક્કી દ્વારા આયોજિત એક સેમિનારને
સંબોધતાં કહ્યું હતું.</p>

<p>જાહેર સાહસોના ખાનગીકરણનો અને સ્થાપિત
હિતોના પડકારોનો સામનો કરવાનો સમય પાકી ગયો છે એમ કહેતાં સિંહાએ ઉમેર્યું
હતું કે જુદા જુદા રાજકીય પક્શોને આ મુદ્દે ખોટી માહિતી અપાઇ છે. અને તેઓને
ગેરમાર્ગે દોરવામાં આવ્યા ચે માટે તેઓ ખાનગીકરણના મુદ્દે ટીકા કરે છે.</p>

<p>ેતેમણે કહ્યું હતું કે બજેટમાં ડિસઇન્વેસ્ટમેન્ટ
માટે જે રૂા. ૧૨૦૦૦ કરોડનો અંદાજ મૂક્યો છે તે વિશ્વસનીય છે. અને ખુબ જ વિચારણા
કર્યા બાદ આ અંદાજ મૂકવામાં આવ્યો છે.</p>

<p>અલબત્ત જે જાહેર સાહસો વ્યુહાત્મક દ્રષ્ટિએ અગત્યના
હશે તેનું ખાનગીકરણ નહી કરવા વિચારણા કરવામાં આવશે પરંતુ તે સિવાયના તમામ જાહેર
સાહસોનું અવશ્ય ખાનગીકરણ કરવામાં આવશે.</p>

<p>તેમણે કહ્યું હતું કે તેમનું ચોથું બજેટ
ફિક્કીની અને બહુ રાષ્ટ્રીય કંપનીઓની ભલામણોના આધારે તૈયાર કરાયું છે એવા
વિપક્શના આક્શેપો પાયાવિહોણા છે.</p>




<p><head>સંસદમાં બજેટ રજૂઆત વેળા જોવા મળેલી
વિવિધ ગતિવિધિઓ</head></p>

<p><head>કેટલાકે ચોકસાઈપૂર્વક નોંધ કરી,
કેટલાકે ઝોકા ખાધાં, કેટલાક સભ્યો પર બગાસાનો પ્રભાવ</head></p>

<p>નવી દિલ્હી, તા. ૧</p>

<p>કેન્દ્રીય નાણાંપ્રધાન શ્રી યશવંત સિંહાએ
ગઈકાલે સંસદમાં તેમનું બજેટ પ્રવચન બરાબર ૧૧-૦૦ના ટકોરે શરૂ કર્યું. આ પછી બરાબર ૧૧-૦૫
કલાકે ગૃહમાં પ્રથમ બગાસું નજરે પડ્યું. આ બગાસું સત્તાધારી પક્શનું હતું. ત્રીજી
હરોળમાં બેઠેલા નાણાંપ્રધાનના પ્રધાનમંડળના સાથીદાર શ્રી જગમોહને આ બગાસું
તાણ્યું હતું.</p>

<p>આ પછી ૧૧-૨૦ મિનિટે કૃષિપ્રધાન
નીતિશકુમાર અવાર નવાર ઝોકાં ખાતાં જોવા મળ્યા અને બરાબર આ જ સમયે ચોથી
હરોળમાંના વિદેશ પ્રધાન જસવંત સિંહ ઘસઘસાટ ઊંઘતા માલૂમ પડ્યા અને ૧૧-૪૦
સુધીમાં તો ગૃહમાંથી રવાના પણ થઈ ગયા. નાણાંપ્રધાનની સાથે જ તેમની જમણી
બાજાુ બેઠેલા મુરાસોલી મારન સ્વસ્થ અને શ્રી યશવંત સિંહાની વાકધારા ધ્યાનપૂર્વક
સાંભળી રહ્યા હતા. બીજી બાજુ સિંહાની ડાબી બાજુએ અડીને બેઠેલા સંરક્શણ પ્રધાન
જ્યોર્જ ફર્નાન્ડીઝ અંગત વિચારોમાં ખોવાયેલા જણાયા હતા.</p>

<p>નાણાંપ્રધાનની બરાબર પાછળની બાજુએ જ્યાં
આઠ વ્યક્તિઓ આરામપૂર્વક બેસી શકે તેમ હતું ત્યાં ૧૦ પ્રધાનો એક સાથે ગોઠવાઈ
ગયેલા હતા. તેઓમાં રામ વિલાસ પાસવાન પણ હતા. આ જ પાટલી ઉપર સર્વશ્રી મનોહર જોષી,
અરુણ જેટલી અને વેંકૈયા નાયડુ ખૂબ ધ્યાનપૂર્વક જરૂર નોંધ કરી રહ્યા હતા.</p>

<p>શ્રી સિંહા પોતે પણ શ્રમિત થવાની તૈયારીમાં હતા
તેમ છતાં તેઓ પ્રસંગને અનુરૂપ સ્વસ્થતા જાળવી રહ્યા હતા. આ તેમણે ચોથું બજેટ રજૂ કર્યું
હતું. કરવેરાની દરખાસ્તો રજૂ કરતાં પહેલાં તેમણે થોડી મજાક કરવાનો પ્રયાસ તો કર્યો
પણ કાંઈ જામ્યું નહીં એટલે ગ્લાસમાંથી પાણી ગટગટાવી વાર્તા પૂરી કરી.</p>

<p>વિરોધ પક્શ કોંગ્રેસની પાટલીઓ પર
આનાથી કાંઈ વિપરીત જ જોવા મળ્યું. વિરોધ પક્શના નેતા કાળજીપૂર્વક નાણાં
પ્રધાનને સાંભળવામાં સંતુષ્ટ હોય તેમ લાગ્યું. જ્યારે કોઈક કોઈક વખત કોંગ્રેસની
આગલી હરોળમાંના એકાદ બે સભ્યો થોડાઘણા વિચારો ટપકાવી લેતા નજરે પડ્યા.</p>

<p>માત્ર રેણુકા ચૌધરી એવાં હતાં જેમણે
લડાયક મૂડ જાળવી રાખેલો અને નાણાંપ્રધાન કાંઈપણ કહે તેમાં દરમ્યાનગીરી કરતા સતત
તત્પર રહેતાં સિંહાએ તેમના પ્રવચનના અંતિમ વાક્યોમાં પોતાના બજેટને 'ન્યૂ ડીલ' તરીકે
ઓળખાવ્યું ત્યારે મોટેથી બૂમ પાડીને 'રૉ ડીલ' તરીકે ઓળખાવ્યું હતું.</p>

<p>નાણાંપ્રધાને અન્ન પ્રોસેસીંગ ઉદ્યોગ માટે
એક્સાઈઝ ડ્યૂટીમાંથી સંપૂર્ણ મુક્તિની જાહેરાત કરી ત્યારે ગૃહમાં સૌથી ખુશ નીતિશકુમાર
જણાતા હતા. આવી મુક્તિ માટે સફળ પ્રયાસો કરવા બદલ શ્રી સુરેશ પ્રભુએ તેમને ધન્યવાદ
પણ આપ્યા હતા. આ પછી તો સિંહાએ તેમની કરવેરાની દરખાસ્તો પૂરી કરી ત્યારે
એનડીએના સાથી પક્શોએ ભારે કરવેરા ઝીંકાશે એમ માની બેઠેલા હોય તેમ રાહતનો દમ
ખેંચ્યો હતો. ટીડીપીના નેતા યારેન નાયડુ અને તૃણમૂલ કોંગ્રેસના નેતા સુદીપ
બંદોપાધ્યાયે પ્રથમ એકબીજાને અભિનંદન આપ્યા અને શ્રી સિંહાને તેમની કામગીરી સુંદર રીતે
પૂરી કરી તે બદલ તેમને અભિનંદન આપવા તેમની પાસે ગયા.</p>




<p><head>રાજકીય પક્શો અને નેતાઓ પાસેના</head></p>

<p><head>સરકારી આવાસોની સંપૂર્ણ વિગતો માગતી દિલ્હી
હાઇકોર્ટ</head></p>

<p>નવી દિલ્હી, તા. ૧</p>

<p>દિલ્હી હાઇકોર્ટે રાજકીય પક્શો અનધિકૃત રીતે
સરકારી મકાનોનો કબજો ધરાવી રહ્યા છે તે અંગેની સંપૂર્ણ વિગતો કેન્દ્ર પાસે
માંગી છે.</p>

<p><head>હાલ કોં. પાસે ૬, ભાજપ પાસે ૫, સામ્ય. પક્શો
પાસે ૫ બંગલા છે : ૩ એપ્રિલ સુધીમાં એફિડેવિટ કરવા આદેશ</head></p>

<p>મુખ્ય ન્યાયમૂર્તિ અરિજિત પાસાયત અને ન્યાયમૂર્તિ
ડી. કે. જૈનની બનેલી ડિવિઝન બેન્ચે કેન્દ્રના શહેરી વિકાસ ખાતા તરફથી આ અંગે પૂરી
પડાયેલી વિગતો અંગે અસંતોષ વ્યક્ત કર્યો હતો અને જણાવ્યું હતું કે, આ ખાતાએ ૩જી
એપ્રિલ સુધીમાં વિગતવાર એફિડેવિટ ફાઇલ કરવી જોઈએ અને અત્યારે રાજકીય પક્શો
અને બીજાઓ દ્વારા ગેરકાયદે સરકારી મકાનો દ્વારા કબ્જો ધરાવાઈ રહ્યો હોય તેની
ચોક્કસ સંખ્યા જણાવવી જોઈએ.</p>

<p>સુધારેલા દિશા સૂચનોનો ભંગ કરીને
સરકારી મકાનોનો કબ્જો પચાવી પાડયો હોય તેવા પક્શો અને નેતાઓ સામે મકાન ખાલી
કરાવવાની પ્રક્રિયાની વિગતોની પણ હાઇકોર્ટે માંગણી કરી હતી.</p>

<p>સરકારી વકીલ ગીતા લુથરાએ રાજકીય પક્શો
પાસેના સરકારી મકાનોની વિગતો આપીને કોર્ટને જણાવ્યું હતું કે, કોંગ્રેસ હાલ તેના
પ્રમુખ સોનિયા ગાંધીને ફાળવાયેલા મકાન સહિત છ મકાનોનો કબ્જો ધરાવે છે
તેવી જ રીતે ભાજપની પાસે પાંચ બંગલા છે, સામ્યવાદી પક્શ પાસે ૩, માર્કસવાદી સામ્યવાદી
પક્શ પાસે બે અને સમાજવાદી પક્શ અને જનતા પક્શ પાસે એક-એક બંગલો છે.</p>

<p>લુથરાએ જણાવ્યું હતું કે, સોનિયા
ગાંધીને ફાળવાયેલ ૧૦, જનપથ બંગલો, કોંગ્રેસ પક્શને અપાયેલ ૨૪, અકબરરોડ હાઉસ,
૨, તાલકટોરા રોડ તેના દિલ્હી એકમને તેમજ ભાજપને ફાળવાયેલા ૧૧, અશોકા રોડ અને
૧૪, પંતમાર્ગ બંગલાને સરકારે ગયા વર્ષે ૭ ડિસેમ્બરે બહાર પાડેલા નવા દિશા સૂચનો
હેઠળ આવરી લેવાયા છે. આ દિશાસૂચનો હેઠળ સામ્યવાદી પક્શ જનતા દળ, રાષ્ટ્રવાદી
કોંગ્રેસ પક્શ અને બહુજન સમાજ પક્શને પણ એક-એક મકાન રાખવા જણાવી દેવામાં આવ્યું
છે.</p>




<p><head>બળાત્કારના કેસમાં મૃત્યુદંડના આરોપીને
છોડી મૂકતી સુપ્રિમ</head></p>

<p>નવી દિલ્હી, તા. ૧</p>

<p>સુપ્રિમ કોર્ટે બળાત્કારના ગુનાસર મૃત્યુદંડની સજા
પામેલા એક વ્યક્તિને એવા આઘારે છોડી મૂક્યો હતો કે પોલીસે જે પદ્ધતિએ પુરાવા
રેકોર્ડ કર્યા છે તેમાં અનેક ક્શતિઓ અને ત્રૂટિઓ રહી જવા પામી છે.</p>

<p><head>આરોપી સામેના પુરાવા ગુનેગાર ઠરાવવાનો
આધાર ઉભો કરી શકતા નથી</head></p>

<p>અમને એવી નોંઘ મૂકતા અત્યંત દુ:ખ થાય છે
કે એક નાની કિશોરી ઉપર પ્રથમ બળાત્કાર કરનાર અને ત્યારબાદ તેની હત્યા કરવાના જઘન્ય
અપરાધ કરનાર તપાસ એજન્સી અને કોર્ટ કાર્યવાહીમાં રહી ગયેલી ક્શતિઓ અને
ત્રૂટિઓના કારણે શિક્શા પામ્યા વિના છૂટી જાય છે એમ ત્રણ ન્યાયમૂર્તિઓની બેંચે તાજેતરના
એક ચુકાદામાં કહ્યું હતું.</p>

<p>ન્યાયમૂર્તિશ્રી સર્વ શ્રી કે. ટી. થોમસ આર. પી.
શેઠી અને બી. એન. અગરવાલની બેંચે કહ્યું હતું કે, આરોપી કન્હાઈ મિશ્રા સામના મજબૂત
પુરાવા ગુનેગાર ઠરાવવા માટે જરૂરી આધાર ઊભો કરી શકતા ન હોઈ તેની સમક્શ
આરોપીને છોડી મૂકવા સિવાય બીજો કોઈ વિકલ્પ રહ્યો નથી.</p>

<p>આરોપી મિશ્રા અને કોર્ટ કાર્યવાહી દરમ્યાન
એવો આક્શેપ મૂકવામાં આવ્યો હતો કે તેણે એક કિશોરી ઉપર પ્રથમ બળાત્કાર કર્યો
હતો બાદમાં તેની હત્યા કરી નાંખી હતી. જો કે તેના આ ગુનાને નજરે જોનાર કોઈ સાક્શી
ન હોઈ ટ્રાયલ કોર્ટે તેને સાંયોગિક પુરાવાઓને આધારે ગુન્હેગાર ઠરાવ્યો
હતો અને હાઇકોર્ટે પણ આ ચુકાદાને માન્ય રાખ્યો હતો.</p>




<p><head>વોશિંગ્ટન- સિએટલમાં ભૂકંપનાં ઝટકા બિલગેટસ
ટેબલ નીચે ગભરાઇને સંતાઇ ગયા</head></p>

<p><head>અમેરિકામાં ભૂકંપ આવે ત્યારે કેવી રીતે
અખબારો- સરકાર અને જનતા જાગૃત થાય છે?</head></p>

<p>મુંબઇમાં ભૂકંપનાં આંચકા આવે અને ભારતનાં
સૌથી મોટા સાહસિક તેમના સીવર્લ્ડનાં ફ્લેટમાં ટેબલ નીચે સંતાઇ જાય તો નવાઇ ન
પામતા. માઇકોસોફ્ટનાં પિયર સમા સિએટસ શહેરમાં જગતનો સૌથી મોટો ધનપતિ
બીલ ગેટસ એક બહુમાળી ટાવરની ટોચે ભાષણ આપતો હતો. એ ટાવરનું નામ પણ ''સ્પેસ
નિડલ'' ટાવર હતું. ટાવરના ફ્લેટોના રૂ. ૧-૧ કરોડનાં ઝુમ્મરો ઝુલવા લાગ્યા હતા. અને
બીલ ગેટસ ગભરાઇને તેનાં ભાષણનાં ટેબલ નીચે સંતાઇ ગયા તેવા ૬-૮ રિક્ટર સ્કેલના ઝટકા
૨૮મી ફેબ્રુ.એ આવેલા. ઘણી જગ્યાએ વીજળી ગાયબ થઇ ગઇ હતી. ટ્રાફિક લાઇટ ફેઇલ જતાં
ડઝન બંધ ગાડીઓ કચડાઇ ગઇ. કરોડો રૂપિયાનું નુકસાન થયું છે. વોશિંગ્ટનનો
પાર્લામેન્ટ બેસે છે તે કેપિટોલ બિલ્ડીંગ પણ ધ્રુજી ઉઠેલું. ૧૦૦ માઇલ દૂર કેનેડાના
વાનકુવર શહેરમાં પણ ભૂકંપ આવ્યો હતો. કચ્છી જેવાં ગરીબમાં ગરીબ પ્રદેશ પછી
ધનિકમાં ધનિક અને સત્તાના કેન્દ્રને ભૂકંપે ધમધમાવ્યું છે.</p>

<p>આ શું ગજબ થઇ રહ્યો છે? દુનિયાને ખૂણે
ખૂણેથી ભૂકંપના સમાચારો આવે છે! અખબારો વધુ પડતા જાગૃત થયા છે કે કુદરત
જથ્થાબંધ ખફા થઇ છે? વોશિંગ્ટનની રાજ્ય સરકારે ત્યાંના સવારનાં ૧૦-૪૫ વાગે ભૂકંપ
પછી રાજ્યમાં ઇમરજન્સી જાહેર કરી દીધી. બોઇંગ વિમાન માટે સિએટસ શહેર પિયર
જેવું છે. તેનું પોતાનું એરપોર્ટ બંધ કરી દેવામાં આવેલું. ટ્રેનના
ઉતારૂઓને બસમાં ખસેડવામાં આવ્યા હતા. ગેસ કંપનીઓ ગભરાઇ ગઇ હતી. પાઇપ વાટે
રસોડાને ગેસ આપતી કંપનીએ ઇમરજન્સી જાહેર કરીને ગેસપાઇપ તૂટી હોય તો ફોનથી જાણ
કરવા જાહેરખબરો આપી હતી. સુપર બજારોની ખડકેલી ચીજો ઢળીને ઢગલો થઇ ગયેલી. દરેક
અમેરિકન અખબારોએ ઇન્ટરનેટની વેબસાઇટ ઉપર જાહેર કર્યું છે. તમને ભૂકંપ નડયો? તમારી
સ્ટોરી અમને લખો.'' અમેરિકાની તમામ સ્કૂલોમાં વિદ્યાર્થીઓને ખબર છે કે ભૂકંપ આવે
તો શું કરવું? રજનીશનો આશ્રમ હતો ત્યાંની ''ઓરેગોન સીટી હાઇસ્કૂલ''નાં તમામ
વિદ્યાર્થીઓ ડેસ્કનીચે સંતાઇ ગયા હતા. અમેરિકાનાં લગભગ ૧૨૫ નિષ્ણાતો ભૂજમાં શું કામ
કટકે કટકે દોડી આવ્યા હતા. તેનું રહસ્ય હવે સમજાશે. એવું બને કે એક દિવસ
સ્વામિનારાયણનાં ૧૦૦૦ સાધુઓ અમેરિકા જઇને ભૂકંપ ગ્રસ્ત અમેરિકનોને હાથે
રાંધેલી ખીચડીને કાદાબટેટાનું શાક ખવરાવશે. અમેરિકનો આજે સૌથી વધુ ભૂકંપથી
ડરે છે. ખરેખર તો મુંબઇએ ડરવા જેવું છે.</p>

<p>ચેનલ ૬૦૦૦ નામની ચેનલ શરૂ થઇ છે. સિએટલને
ભૂકંપ પછી તમામ અમેરિકનો ડરી ગયા છે. ર્ણૈહ.ર્બસ ની ચેનલ ૬૦૦૦ ભૂકંપ વિષેના
સમાચારો સતત આપે છે અને ભૂકંપ સામેની તૈયારી કેમ કરવી તે સૂચવે છે. વોશિંગ્ટન
સ્ટેટમાં ૧૮૭૨થી ૧૯૬૫ સુધીમાં ૭ ભૂકંપો આવી ગયા છે. વોશિંગ્ટન સ્ટેટના
જિઓલોજી ડીપાર્ટમેન્ટએ ૧ ડોલરની કીંમતે ભૂકંપ હવે ક્યા થશે અને ભૂકંપ આવે તો શું
કરવું તેની પુસ્તિકા વેચવા માંડી છે.</p>

<p>લીન્ડા નોસોન એન્થની કામાર અને જિરાલ્ડ
થોરસન નામનાં નિષ્ણાતો તો કહે છે કે ''હવે બીજો ટ્રાફીકનો મોટો અકસ્માત ક્યારે
થશે ત કહેવું તે મુશ્કેલ છે. છતાં ટ્રાફીક અકસ્માત ક્યાં ક્યારે થશે તેનું અનુમાન થઇ
શકે છે તેવું જ ભૂકંપનું છે. ભૂતકાળનો પેટર્ન જોઇને હવે ક્યારે ભૂકંપ આવશે તે
જનતાને કહી શકાય - કહેવું જોઇએ. આ નિષ્ણાતો કહે છે કે ૮.૩ રીક્ટર સ્કેલનો એક
આલાસ્કાનો ભૂકંપ સતત ૩૦૦ સેકન્ડ સુધી રહેલો. આ નિષ્ણાતો કહે છે કે મુંબઇમાં
ભૂકંપનાં ઝટકા આવ્યા અને ઘણા બિલ્ડીંગોમાં ક્રેક ન થયો પણ જો રિપીટેડ અને
ક્યુમ્યુલેટીવ- એક પછી એક ભૂકંપ આવે તો એ જ બિલ્ડીંગો હવે પછી પડે ખરી દરેક
બિલ્ડીંગની ભૂકંપ સામેના રક્શણની યોગ્ય ડિઝાઇન હોવી જોઇએ અને ભૂકંપ પછી તે ડિઝાઇનને
પણ મજબુત બનાવવી જોઇએ.</p>

<p>વોશિંગ્ટન સ્ટેટમાં ભૂકંપ થયો એટલે સિએટલ
ખાતે તુરંત પ્રમુખ જ્યોર્જ બુશે પોતાનો પ્રતિનિધિ મોકલ્યો. અને ખાતરી આપી કે
રાજ્યને તમામ મદદ આપીશું. યાદ રહે કે ગુજરાતમાં ભૂકંપ થયા પછી ભારત સરકાર ૯૬ કલાકે
જાગી હતી. અને રાષ્ટ્રપતિએ તેનું પ્રજાસત્તાક ડીનર પણ કેન્સલ કરેલું નહીં. વોશિંગ્ટનમાં
સવારે ૧૦-૪૫ વાગે ભૂકંપ થયો અને ૧ વાગે તો જ્યોર્જ બુશનો પ્રતિનિધિ વ્હાઇટ
હાઉસનો મેસેજ લઇને વોશિંગ્ટન સ્ટેટના ગવર્નર પાસે પહોંચી ગયેલો.</p>

<p>ઓલીમ્પીયા નગરમાં બધી જ ઓફિસો
બંધ કરાઇ. જ્યાં પાર્લામેન્ટ બેસે છે તે કેપિટોલના ગુંબજને ભૂકંપે ઇજા કરીને
તિરાડ પાડી છે! ઓલીમ્પીયા શહેરમાં સરકારી ઓફિસને જ નુકસાન થયું છે.
ઓલીમ્પીયાના કેમ્પસ ઉપરનાં તમામ સરકારી મકાનો ઉપર પીળી પટ્ટી લગાવાઇ છે તે પટ્ટીવાળા
મકાનોને તાકીદે સ્ટ્રક્ચરલ ઇજનેરોએ તપાસી લીધા છે. આ કામ ૨૪ કલાકમાં પૂરૂં
થયું અને સ્ટ્રક્ચરલ ઇજનેરોએ ઉજાગરા પછી કહી દીધુ કે ઓલીમ્પીયાની આ ઓફિસો કામ
કરવા માટે સલામત નથી.</p>

<p>સિએટલમાં હાબેરવ્યુ નામની હોસ્પિટલમાં જ
ડેમેજ થયું છે ભૂકંપનું નામ સાંભળીને ૬૬ વર્ષની વૃધ્ધાને હાર્ટ એટેક આવ્યો
અને મરી ગઇ હતી. બોઇંગ વિમાનની તમામ ફેક્ટરીઓ બંધ થઇ ગઇ છે. રાજ્યના
મેન્ટેનન્સ ઇજનેર કેન કર્કલેન્ડ કહે છે કે રાજ્યના રસ્તા, હાઇવે અને બ્રીજને કેટલું
નુકસાન થયું છે તે તપાસતા દિવસો લાગશે. વોશિંગ્ટન યુનિવર્સિટીની સેસ્મોલોજી
લેબોરેટરીનાં વડા કહે છે કે આ ભૂકંપની આગાહી થઇ શકી નહીં અને ભૂકંપની ધારણા
પણ નહોતી કારણ કે ભૂકંપ ખૂબખૂબ ઊંડેથી તેનો સ્રોત ધરાવતો હતો. પરંતુ
વોશિંગ્ટનના વુડલેન્ડ પાર્ક ઝુનાં ચોપગા પ્રાણીઓને ભૂકંપની જાણ થઇ ગયેલી. ચૅઇ નામની
હાથણીએ બચ્ચાને જન્મ આપ્યો તેના લોહીનું સેમ્પલ લેવા ગયા ત્યારે સેમ્પલ લેવાય તે
પહેલાં હાથણી એકદમ ઉચી નીચી થવા લાગી હતી અને તેને આંચકા લાગતા હોય તેવું વર્તન
કરતી હતી. તે પછી ભૂકંપ આવ્યો. ભૂકંપનો સેકન્ડો પહેલાં ઝુના ગેરીલાઓ જોર જોરથી
ચીસ પાડવા માંડયા હતા. કેટલાક ઓરાંગ- ઉરાંગ કક્શાના વાંદરાઓ ભૂકંપ પહેલાં જ
ટપોટપ કુદીને ઝાડની એકદમ ટોચની ડાળીએ પહોંચી ગયા હતા. એક નેશનલ પાર્કની
હોટલના ઉતારૂઓને હોટલ ખાલી કરાવાઇ હતી. જાપાનીસ ટુરીસ્ટ જે ટોકીયોના યોકોહામા
નામનાં પરામાં રહે છે. સિએટલમાં હતો. તેણે કહ્યું કે આનાથી વધુ કક્શાના ઝટકા આવે
છે. અને નવા ભૂકંપ માટે તૈયાર હોઇએ છીએ.</p>

<p>અમેરિકાના પેસીફીક નોર્થ વેસ્ટના સેસ્મોલોજીકલ
ખાતાને સિએટલનાં ભૂકંપ પછી ૧૦ મિનિટમા ઇન્ટરનેટ ઉપર અમેરિકન પ્રજા તરફથી સેંકડો પ્રશ્નો
પૂછાયેલા તેમાં ૨૫ પ્રશ્નોના ઉત્તરો વેબસાઇટ ઉપર તુરંત ૮ કલાકમાં પ્રસારીત થયા તેમાં
''ભૂકંપની આગાહી થઇ શકે કે નહીં'' ત્યાંતી માંડીને ''પૂલો પડી ભાંગે કે નહી'' (હા
પડી ભાંગે) તેવા પ્રશ્નો પૂછાયેલા. ''મારે ભૂકંપ સામેનું ઇન્સ્યુરન્સ લેવું જોઇએ કે
નહી?'' તેનાં જવાબમાં કહેવામાં આવ્યું ''કેટલાક ભૂસ્તર શાસ્ત્રીઓ એ પોતાના ઘરનાં
વીમા ભૂકંપ સામે ઉતરાવ્યા છે તે ઉપરથી તમે નક્કી કરી લો?'' એક છેલ્લો પ્રશ્ન પૂછાયેલો
''શું કોઇ જગ્યા કે દેશ ભૂકંપ માટે સાવ સલામત છે?'' જવાબમાં કહેવામાં આવ્યું
''કોઇ વિસ્તાર ભૂકંપથી કેમ્પલીટલી મુક્ત નથી.''</p>

<p>અને હવે મઝેદાર વાત સાંભળો ૨૩-૧-૨૦૦૧ના રોજ
લગભગ ૨૪ દિવસ પહેલાં (ભૂકંપના) ડો. બ્રાયાન શૅરોડ અને ડો થોમસ પ્રાટ નામનાં ભૂકંપ
સંશોધકોએ આગાહી કરેલી કે સિએટલ શહેર નીચેના ભૂતળમાં ફોલ્ટ છે અને ક્યારે
પણ ભૂકંપ આવી શકે છે! એટલું જ નહીં પણ સંશોધકોએ એક ''મોટો ગુંબજ''
ઉભો કરીને માત્ર ૨.૩ મેગ્નીટયુડનો ભૂકંપ આવેલો ગુંબજને કેટલું નુકસાન થાય તેનું
રિહર્સલ પણ કરેલું! આ સંશોધકોએ કહેલું કે સિએટલની ધરતી નીચે એક
કરતાં વધુ ફોલ્ટ છે અને આખુ શહેર ભૂકંપની ધરતી પર ચણાયેલું છે. અને ખરેખર ૩૪
દિવસ પછી ભૂકંપ આવ્યો!</p>

<p>કાન્તિ ભટ્ટ</p>

<p> </p>






</body></text></cesDoc>