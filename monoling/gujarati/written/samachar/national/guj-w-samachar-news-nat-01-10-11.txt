<cesDoc id="guj-w-samachar-news-nat-01-10-11" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-samachar-news-nat-01-10-11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Gujarat Samachar" internet news (www.gujaratsamachar.com), news stories collected on 01-10-11</h.title>
<h.author>Gujarat Samachar</h.author>
<imprint>
<pubPlace>Gujarat, India</pubPlace>
<publisher>Gujarat Samachar</publisher>
<pubDate>01-10-11</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Gujarati</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head>ભારતીય બજારોમાં રૂપિયો- ભાવાંક ઉછળ્યા : સોના-ચાંદી તૂટ્યા</head></p>

<p>અમદાવાદ,બુધવાર</p>

<p>અફઘાનિસ્તાન સાથે અમેરિકાના હુમલાઓ વધી રહ્યાના અને
ડાઉજોન્સ તથા નાસ્ડાક ભાવાંક તૂટીને આવી રહ્યા હોવા છતાં ભારતના શેરબજારો,
હુંડિયામણ બજાર ગગડીને જવાને બદલે સુધારા તરફી હવામાન દર્શાવી રહ્યા છે. જ્યારે
સોના-ચાંદી બજાર ઝડપથી ઉછળી જવાને બદલે સોનાના ભાવો ઉંચા મથાળેથી ઝડપી પીછેહઠ
દર્શાવી રહ્યા છે. બીજી બાજુ વિશ્વમાં ક્રુડઓઇલના ભાવ પણ યુધ્ધના ભયે વધવાને
બદલે ૨૦ ડોલરથી પણ નીચા બોલાઇ રહ્યા છે.</p>

<p>ભાવાંક ૧૦૨ પોઇન્ટ - રૂપિયો ૮ પૈસા વધ્યો : ઇન્ફોસીસ-હિંદ
લિવરના ઉત્તેજક સમાચારે તેજી તરફી વાતાવરણ</p>

<p>આજે તો ભારતના શેરબજારોમાં હિંદ લિવર ત્થા ઇન્ફોસીસ
ટેકનોલોજીના કંપની વિષયક પ્રોત્સાહક સમાચારો પાછળ સેન્ટીમેન્ટ એટલું મજબુત બની
ગયું હતું કે બધા જ નકારાત્મક સમાચારો અને અર્થતંત્ર નબળું પડી રહ્યાના
અહેવાલોની સ્હેજ પણ માઠી અસર જોવા મળી ન્હોતી.</p>

<p>ડાઉજોન્સ ૩૯ પોઇન્ટ ઘટીને ૯૦૨૯ અને નાસ્ડાક ૨૩ ૨૩ પોઇન્ટ ઘટી
૧૫૮૩ પોઇન્ટે મંગળવારે રાત્રે પહોંચી ગયાના સમાચાર છતાં અત્રે બીએસઇ-૩૦ ભાવાંક
આગલા બંધ ૨૭૯૧થી ઉછળીને ૨૯૦૫ થઇ છેલ્લે ૧૦૨ પોઇન્ટના ઉછાળા સાથે ૨૮૯૭ના મથાળે
બંધ રહ્યો હતો. બીએસઇ-૧૦૦ ભાવાંક પણ ૩૬ પોઇન્ટ વધીને ૧૩૩૨ રહ્યો હતો.
આઇટી ઇન્ડેક્સ આગલા બંધ ૯૦૪થી વધીને ૯૦૭ના મથાળે બંધ હતો.</p>

<p>આજે બજારમાં એફઆઇઆઇ તો પ્રમાણમાં શાંત હતી. પરંતુ રીટેઇલ
ઇન્વેસ્ટરો અને યુનીટ ટ્રસ્ટ સહિત લોકલ સંસ્થાઓની જોરદાર ઓલરાઉન્ડ લેવાલી ઇન્ફોટેક
અને ઓલ્ડ ઇકોનોમી શેરોમાં ઉપડી હતી. હિંદ લિવરે બોનસ-ડિબેંચર જાહેર કરવાની તથા
એફઆઇઆઇ લિમિટ ૨૪ ટકાથી વધારીને ૪૯ ટકા કરવાની દરખાસ્ત અંગે ૧૬મી ઓક્ટોબરે
બોર્ડ મીટીંગમાં નિર્ણય કરાશે તેવી જાહેરાત કરતાં આજે શેરનો ભાવ રૂા.૧૮.૮૫ ઉછળી
રૂા.૨૩૦.૨૫ રહ્યો હતો.</p>

<p>ઇન્ફોસીસ ટેકનોલોજીમાં બજારની અપેક્શા ૨૦ ટકા વૃધ્ધિ દરની
હતી તેના બદલે નેટ નફો સપ્ટેમ્બર ૨૦૦૧માં પૂરા થતાં બીજા ક્વાર્ટરમાં ૩૧ ટકા જેટલો
ઉછળીને રૂા.૨૦૧.૬૨ કરોડ આવતાં શેરનો ભાવ આજે રૂા.૨૪૫૧થી ઉછળી ૨૫૭૫ થઇ છેલ્લે
રૂા.૯૭ વધીને ૨૫૧૩ના મથાળે બંધ હતો.</p>

<p>સીએમસીનું વેચાણ તાતા જૂથને અણધાર્યું અને એકાએક કરાતા
ત્થા એચટીએલનું ડાયવેસ્ટમેન્ટ એચએફસીએલને કરવાનો કેન્દ્ર સરકારે ઝડપી નિર્ણય લેતાં
ઇન્વેસ્ટરોમાં આનંદની લહર ફેલાઈ ગઈ હતી. હવે બીજા ૧૩ પીએસયુ શેરોના વેચાણ જલ્દી
થવાના અને ત્યારબાદ ઓપન ઓફર દ્વારા શેરોના બાયબેક કરાશે ત્યારે ભાવો વધુ ઉચકાશે
તેવી ગણત્રીઓ પાછળ પીએસયુ શેરોના ભાવો પણ વધી રહ્યા છે.</p>

<p>આમ બજારનું સેન્ટીમેન્ટ સુધારા તરફી બદલાઈ રહ્યું છે. હવે
વધુને વધુ કંપનીઓના પરિણામો બહાર આવતાં રહેશે જેની ઉપર બજારની ભાવિ ચાલનો
આધાર રહેશે.</p>

<p>યુદ્ધની શરૂઆતના પ્રથમ દિવસે ફોરેક્સ બજારમાં ડોલરની ભારે માંગ
નીકળતાં જે રૂપિયો ગગડ્યો હતો તે બે-ત્રણ દિવસમાં સ્થિર બનીને સુધરી રહ્યો છે.
આજે ડોલર સામે રૂપિયો ૮ પૈસા વધીને ૪૮.૦૪/૦૫ રહ્યો હતો. એક મોટી
ટેલીકોમ્યુનીકેશન કંપનીનું રેમિટન્સ આવતાં આજે ડોલર નરમ બન્યો હતો. ડિલરના
જણાવ્યા પ્રમાણે વિશ્વમાં ક્રુડના ભાવ ઘટતાં, શેરબજારમાં ૧૦૨ પોઇન્ટનો ઉછાળો
નોંધાતા, કોલમની માર્કેટમાં લિકવીડિટી વધતાં ફોરેક્સ માર્કેટનું
સેન્ટીમેન્ટ સુધરવા પામ્યું છે.</p>

<p>ઝવેરી બજારમાં આજે મુંબઈ ખાતે વિદેશની નરમાઈ પાછળ સ્ટોકિસ્ટો
વેચવાલ બનતાં સ્ટા. સોનું રૂા. ૭૦ ઘટીને ૪૭૫૦ રહ્યું. ૧૦ તોલા બિલ્કિટનો ભાવ રૂા.
૫૬,૫૫૦થી તૂટી ૫૫,૭૦૦ રહેલ છે. ચાંદી હાજરનો ભાવ કિલોદીઠ રૂા. ૧૬૫ ઘટી રૂા. ૭૯૧૫
વાળો રૂા. ૭૭૫૦ રહ્યો છે.</p>

<p>દરમ્યાનમાં ભારતીય જનતા પાર્ટીના વાઇસ પ્રેસીડેન્ટ રામદાસ અગ્રવાલે
આજે રિઝર્વ બેંકને વ્યાજના દરોમાં વધુ ઘટાડો કરીને અર્થતંત્રને મંદીમાંથી બહાર
કાઢવામાં મદદરૂપ થવા ખુલ્લો અનુરોધ કર્યો હતો. તેઓ દિલ્હી ખાતે એક
મહાસંમેલનને સંબોધી રહ્યા હતા. જાણીતા કરવેરા નિષ્ણાત આર.એન. લખોટિયાએ
કાયદાકીય સુધારણાના પ્રોગ્રામને અગ્રતા આપવા જણાવ્યું હતું.</p>

<p> </p>




<p><head>પોલોકે પાંચ વિકેટ ઝડપતા ભારત ૨૩૩માં ઓલઆઉટ</head></p>

<p>સેન્ચુરિયન, તા.૧૦</p>

<p>દ.આફ્રિકાના કેપ્ટન પોલોકે ઝડપેલી પાંચ વિકેટોના લીધે
ભારતની ટીમ ત્રિકોણીય જંગમાં પોતાની બીજી લીગ મેચમાં ૪૮.૫ ઓવરમાં ૨૩૩ રનમાં
ઓલઆઉટ થઈ ગઈ હતી.</p>

<p>ભારતે ટોસ જીતી દાવ લેવાનું પસંદ કર્યા બાદ ગાંગુલી અગાઉની
મેચની જેમ જ આક્રમક રમતના મૂડમાં હતો તેણે ૨૭ બોલમાં બે છગ્ગાને એક ચોગ્ગાની
મદદથી ૨૪ રન કર્યા હતા. સચિને પાંચ ચોગ્ગાની મદદથી ૩૮ રન કર્યા હતા.</p>

<p>દ્રવિડે ૫૦૦૦ રન પૂરા કર્યા</p>

<p>ભારતે એક તબક્કે ૭૫ રનમાં ત્રણ વિકેટ ગુમાવી દીધી હતી
પરંતુ દ્રવિડ અને યુવરાજ વચ્ચે ચોથી વિકેટની ૯૦ રનની ભાગીદારીએ ભારતનો રકાસ
અટકાવ્યો હતો. દ્રવિડે ૭૯ દડામાં પાંચ ચોગ્ગાની મદદથી ૫૪ રન કરવા દરમ્યાન વન-ડેમાં
પોતાના પાંચ હજાર રન પૂરા કર્યા હતા. આ સિદ્ધિ મેળવનાર તે પાંચમો ભારતીય ક્રિકેટર
બન્યો હતો.</p>

<p>યુવરાજે તેને સારો ટેકો આપતાં ૫૫ દડામાં ચાર ચોગ્ગા અને
એક છગ્ગાની મદદથી ૪૨ રન કર્યા હતા જ્યારે શેહવાગે તેની આગવી આક્રમક રમત બતાવતાં ૨૭
દડામાં ચાર ચોગ્ગા અને એક છગ્ગાની મદદથી ૩૩ રન કર્યા હતાં, પરંતુ ભારતનો નીચલો ક્રમ
નિષ્ફળ જતાં ભારત આ પીચ પર સલામત ગણાતો ૨૫૦ રનનો સ્કોર કરી શક્યું ન હતું. </p>

<p>આજે ગાંગુલી અને દ્રવિડ બંનેના કેચ એક્શન રીપ્લેમાં જોતાં કેચ
લાગતા જ ન હતાં પરંતુ તેઓને બેનીફિટ ઓફ ડાઉટનો લાભ અપાયો ન હતો. જો કે
ઓછા સ્કોરને કારણે ક્રિકેટ ચાહકોમાં નિરાશા વ્યાપી ગઈ હતી.</p>

<p>સ્કોરબોર્ડ</p>

<p>ગાંગુલી કો. કાલીસ બો. પોલોક ૨૪</p>

<p>સચિન કો. નેલ બો. નિત્ની ૩૮</p>

<p>દાસ કો. કલુઝનર બો. પોલોક ૦૨</p>

<p>દ્રવિડ કો. કલુઝનર બો. પોલોક ૫૪</p>

<p>યુવરાજ બો. નિત્ની ૪૨</p>

<p>શેહવાગ કો. નિત્ની બો. નેલ ૩૩</p>

<p>દાસગુપ્તા કો. બાઉચર બો. કલુઝનર ૦૮</p>

<p>અગરકર કો. બાઉચર બો. કલુઝનર ૦૧</p>

<p>હરભજન લેગબિફોર બો. પોલોક ૧૫</p>

<p>કુંબલે અણનમ ૦૭</p>

<p>શ્રીનાથ કો. કાલીસ બો. પોલોક ૦૨</p>

<p>વધારાના લેગબાય-૨, વાઇડ-૧, </p>

<p>નોબોલ-૪ ૦૭</p>

<p>કુલ ૪૮.૫ ઓવરમાં ઓલઆઉટ ૨૩૩</p>

<p>વિકેટ પડવાનો ક્રમ : ૧/૪૪, ૨/૫૨, ૩/૭૫, ૪/૧૬૫, ૫/૧૬૭,
૬/૨૦૪, ૭/૨૦૮, ૮/૨૦૮, ૯/૨૩૧, ૧૦/૨૩૩</p>

<p>બોલીંગ : પોલોક ૯.૫-૧-૩૭-૫, નેલ ૧૦-૦-૪૯-૧, કાલીસ
૯-૦-૪૨-૦, નિત્ની ૧૦-૦-૪૨-૨, કલુઝનર ૭-૦-૩૦-૨, બોયે ૩-૦-૩૧-૦</p>

<p> </p>




<p><head>યુદ્ધના પગલે દેશના અર્થતંત્ર ઉપર રૂા.૭૫ અબજનો બોજો આવશે</head></p>

<p>મુંબઇ,તા.૯</p>

<p>અમેરિકા અને તેના સાથી રાષ્ટ્રો દ્વારા અફઘાનિસ્તાનમાં તાલિબાન
શાસકો ઉપર શરૂ થયેલા હુમલાના પગલે ભારતના અર્થતંત્ર ઉપર ટૂંકા ગાળામાં રૂ.૬૫૦૦થી ૭૫૦૦
કરોડ જેટલી વિપરીત અસર થવાની સંભાવના છે. આ રકમ ખરેખરી માથાદીઠ આવકના ૦.૨ ટકાથી
૦.૩ ટકા જેટલી છે.</p>

<p>આ અંદાજ દ્વારા સર્વિસિના કન્સલ્ટન્ટ એસ.એસ. ભંડારેએ તાજેતરમાં
ઇન્ડિયન મર્ચન્ટ્સ ચેમ્બર (આઇએમસી)ના સભ્યો સમક્શ મૂક્યો હતો.</p>

<p>અર્થતંત્રના જે ક્શેત્રોમાં ટૂંકા ગાળામાં મંદી આવવાની શક્યતા છે
તે ક્શેત્રો છે હૂંડિયામણની આવક, માલ-સામાન અને કૉમ્પ્યૂટર સૉફ્ટવેરની નિકાસ,
શૅરબજારમાં માર્કેટ કેપિટલાઇઝેશનના ઘટાડો થવાથી ખરેખરા પ્રમાણમાં મૂડીને લાગતો
ઘસારો, સરકારનો યોજનાકીય ખર્ચ અને સરકારની નાણાકીય ખાધને પૂરવા નાંખવા પડતા
વધારાના કરવેરાથી મળથી આવક.</p>

<p>ભંડારેએ જેઓ આઇએમસીના કમિટી મેમ્બર પણ છે, કહ્યું હતું કે
સારા ચોમાસાને કારણે દેશના કૃષિ ક્શેત્રને મળનારા લાભો બહારી પરિબળોના કારણે
નાબૂદ થઇ જવાનો ખતરો ઉભો થયો છે.</p>

<p>આ ક્શેત્રો ઉપરાંત ઔદ્યોગિક ક્શેત્રને પણ ફુગાવાની સ્થિતિનો
સામનો કરવો પડશે. તેલ પુલ ઍકાઉન્ટની ખાધને પૂરવા માટે પેટ્રોલિયમ પેદાશોના
ભાવમાં સંભવિત વધારો, વિમાના પ્રીમિયમમાં વધારો અને જહાજના નૂરભાડામાં
તથા વ્યાજના દરમાં થનારા વધારાને કારણે ઔદ્યોગિક ક્શેત્રને વધારાનો બોજો
સહન કરવો પડશે. આ તમામ કારણોને લીધે રૂ.૩૫૦૦-૪૫૦૦ કરોડ જેટલો બોજો
ઔદ્યોગિક ક્શેત્ર ઉપર પડશે જે દેશની માથાદીઠ આવકના ૦.૨ ટકા જેટલો છે.</p>

<p>ભંડારેએ કહ્યું કે વિદેશી નાણા સંસ્થાઓનું રોકાણ ઘટવાથી,
નોન-રેસિડન્ટો દ્વારા મોકલાતા હુંડિયામણની આવક અને વિદેશી સીધા મૂડી રોકાણ
(એફડીઆઇ)માં ઘટાડો થવાથી તથા ભારતીય કંપનીઓ દ્વારા વિદેશના બજારોમાં કરાતા
જીડીઆર, એડીઆર અને ઇસીબીના કાર્યક્રમો મુલતવી રહેવાને કારણે દેશને એકથી દોઢ અબજ
ડોલર જેટલી મળનારી આવકને ગુમાવવી પડશે.</p>

<p>જો કે, ભંડારેએ કહ્યું કે દેશની કુલ માથાદીઠ આવકમાં નિકાસનો
હિસ્સો ફક્ત ૯ ટકા છે અને અર્થતંત્ર મોટા ભાગે આંતરિક ઉદ્યોગો ઉપર નભે છે એટલે
બહારના પરિબળોની દેશના અર્થતંત્ર ઉપર મર્યાદિત અસર થાય છે. આમ છતાં, દેશની કુલ
નિકાસમાં અમેરિકામાં થતી નિકાસનો ફાળો ૨૦ ટકા અને સૉફ્ટવેરની નિકાસનો ફાળો ૬૦
ટકા છે. તેજ રીતે યુરોપિયન સમુદાયના રાષ્ટ્રોમાં થતી માલ સામાનની નિકાસનો ફાળો
૨૫ ટકાનો અને સોફટવેરની નિકાસનો ફાળો ૨૦ ટકા છે. આમાંથી ઘણા ખરા દેશો
નાટોના સભ્ય હોવાથી તેમના ઉપર ત્રાસવાદી હુમલાની શક્યતા વધી જાય છે. એટલા અંશે
ભારતની નિકાસને પણ અસર પડશે એવું ભંડારેએ કહ્યું હતું.</p>

<p>આંતરરાષ્ટ્રીય નાણા ભંડોળે (આઇએમએફ) વિશ્વના અર્થતંત્રનો
વિકાસ દર ઇ.સ.૨૦૦૦ના ૪.૮ ટકાથી ઘટીને ઇ.સ.૨૦૦૧માં ૩.૨ ટકા થવાની તથા અમેરિકાના
અર્થતંત્રનો વૃદ્ધિ દર પાંચ ટકાથી ઘટીને ૧.૫૦ ટકા થવાની ગણતરી મે ૨૦૦૧માં મૂકી હતી.
આ અંદાજોમાં ૦.૫થી ૦.૭ ટકા જેટલો નવેસરથી ઘટાડો મૂકવામાં આવ્યો છે એમ તેમણે
કહ્યું હતું. આ સંજોગોમાંથી બહાર આવવાના વિકલ્પ તરીકે ભંડારેએ ખાધવાળા
અર્થતંત્રનો આશરો લેવાનું સરકારને સૂચવ્યું હતું. અંદાજપત્રમાં ખાધ વધારવાથી
યોજનાકીય ખર્ચ થઇ શકશે, જરૂર લાગે તે ચીજો ઉપર એક્સાઇઝ ડ્યૂટી ઘટાડી શકાશે,
ડિસ-ઇન્વેસ્ટમેન્ટના કાર્યક્રમમાં આગળ વધી શકાશે અને માળખાકીય પ્રોજેક્ટ માટે નાણાની
ફાળવણી થઇ શકશે એમ ભંડારે કહ્યું હતું.</p>

<p>આ સમય અર્થતંત્રમાં વિશ્વાસ પેદા કરવાનો છે. ધિરાણ અંકુશો
ઢીલા કરવાથી કે વ્યાજ દર ઘટાડવાથી શૅરબજારમાં કે અર્થતંત્રમાં સુધારો આવશે નહીં.
આને બદલે ખર્ચ વધુ કરવાનો પ્રોત્સાહન મળે તેવા પગલાં સરકારે લેવાની જરૂર છે એમ
તેમણે કહ્યું હતું.</p>

<p>સારા ચોમાસાના તમામ લાભ ધોવાઇ જવાનો ઊભો થયેલો
ખતરો</p>




<p><head>અડવાણી, ઠાકરેની હત્યા કરવાની શકીલની યોજના હતી</head></p>

<p>મુંબઈ,તા.૧૦</p>

<p>ચેન્નાઈમાં ધરપકડ કરાયેલાં છોટા શકીલ ગેંગનાં બે કુખ્યાત
ગુંડાઓની પોલીસે કરેલી પૂછપરછમાં તેમણે કેન્દ્રિય ગૃહમંત્રી લાલકૃષ્ણ અડવાણી તથા
શિવસેના પ્રમુખ બાલ ઠાકરેની હત્યાની યોજના બનાવી હોવાનું બહાર આવ્યું છે. સાથે સાથે
એ પણ સ્પષ્ટતા થઈ ગઈ હતી કે ચેન્નાઈમાં અંડરવર્લ્ડ ડોન છોટા શકીલ નહિ પણ તેનો
સાથીદાર શકીલ મોહમ્મદ પકડાયો છે.</p>

<p>ચેન્નાઈથી પકડાયેલા બે ગુંડાઓએ આપેલી માહિતી</p>

<p>મુંબઈ પોલીસની ક્રાઈમ બ્રાન્ચનાં સૂત્રોનાં જણાવ્યા મુજબ થોડા
સમય પહેલાં ઈન્ટરપોલે પોતાની દિલ્હી શાખાને જાણ કરી હતી કે છોટા શકીલે અડવાણીની
હત્યાની યોજના બનાવી હતી. દિલ્હી પોલીસે અડવાણીનાં ઘરની આસપાસ ચુસ્ત પોલીસ
બંદોબસ્ત ગોઠવતાં બે શંકાસ્પદ શખ્સો પોલીસને હાથે ઝડપાઈ ગયાં હતાં. જેમાં અરશદ
અને મોઈનુદ્દીનનો સમાવેશ થાય છે. આ બન્ને શખ્સોએ પોલીસને પૂછપરછ દરમિયાન જણાવ્યું
કે તેઓ અડવાણીનાં ઘર નજીક ભાડેથી ઘર લેવાના હતા જોકે ત્યારપછી ઈન્ટરપોલ અને અન્ય
જાસૂસી સંસ્થાઓ વધુ સતર્ક થઈ ગઈ હતી. દરમિયાન ઈન્ટરપોલે પોતાનાં દિલ્હી
અધિકારીઓ દ્વારા તામિલનાડુનાં પોલીસ મહાનિર્દેશક બી.પી.તૈલવાલને જણાવ્યું કે
છોટા શકીલ ચેન્નાઈ આવવાનો છે. નૈલવાલે આ બાબતની માહિતી ચેન્નાઈનાં પોલીસ કમિશનર
કે. મુત્તુકુરૂપ્પમ, પોલીસ ઉપમહાનિરિક્શક (ઈન્ટેલિજન્સ) ટી.રાજેન્દ્રન, જોઈન્ટ પોલીસ
કમિશનર સી.સિલેન્દ્રબાબુ, વિશેષ શાખાનાં પોલીસ અધિક્શક તમરાઈકન્નન તથા ડી.સી.પી.
કે.પી.શાનમુગરાજેશ્વરનને આપી તેમની એક સંયુક્ત ટીમન બનાવી તપાસની જવાબદારી સોંપી
હતી.</p>

<p>બાતમીને આધારે છોટા શકીલનાં બે સાથીદારો બેંગલોરથી
નીકળતાં પોલીસે તેમનો પીછો કર્યો હતો અને ચેન્નાઈમાં તેમની ધરપકડ કરી હતી.
તેમનાં નામ જાકીર અને શકીલ મોહમ્મદ હોવાનું બહાર આવ્યું છે.</p>

<p>આરોપી શકીલ મોહમ્મદને કારણે છોટા શકીલ પકડાયો હોવાની
અફવા ફેલાઈ હતી. મુંબઈ પોલીસ શકીલ મોહમ્મદ અને જાકીરને લઈને મુંબઈ આવી ગઈ હતી. </p>




<p><head>અફઘાનિસ્તાનથી સુકા મેવાની આયાતમાં સર્જાયેલો અવરોધ</head></p>

<p>મુંબઇ, તા. ૧૦</p>

<p>અમેરિકા અને અફઘાનિસ્તાન વચ્ચે ચાલી રહેલા યુધ્ધના કારણે
અફઘાનિસ્તાનથી આયાત થતો સૂકોમેવો આવતો અટકી પડતાં ભારતમાં આગામી મહિનામાં
સૂકા મેવાની કિંમતમાં ૨૦થી ૨૫ ટકા વધારો થવાની સંભાવના છે. સપ્ટેમ્બર મહિનામાં
અમેરિકા પર આંતકવાદીઓએ હુમલો કર્યો હતો. આથી વિશ્વભરના શૅરબજારો પડી ભાંગ્યા
વિમાન કંપનીઓ, પર્યટન, વિમા કંપનીઓ, સોફ્ટવેર ઉદ્યોગો, જેવા ઉદ્યોગ
ધંધા પર વિપરીત અસર પડી હતી તેમજ સૂકા મેવાની આયાત પર પણ માઠી અસર પડી
હતી.</p>

<p>યુધ્ધને કારણે સુકામેવાના ભાવમાં ૨૦થી ૨૫ ટકા વધવાની
સંભાવના</p>

<p>અફઘાનિસ્તાન, ઇરાન તેમજ અમેરિકાથી આવનાર સૂકામેવાની ખાસ
દિવાળી અને રમઝાનના તહેવારોમાં વધુ માંગણી હોય છે. અફઘાનિસ્તાન, ઇરાન, અમેરિકા
વગેરે દેશોમાંથી ખજૂર, અંજીર, જર્દાળુ, બદામ, પિસ્તા જેવા સૂકામેવા ઓક્ટોમ્બર
મહિનાથી મોટા પ્રમાણમાં ભારતમાં આવવાની શરૂઆત થાય છે. પણ સપ્ટેમ્બર મહિનામાં વર્લ્ડ
ટ્રેડ સેન્ટર અને પેન્ટાગોન પર આંતકવાદે કરેલા હુમલાબાદ ચાલુ મહિનામાં અમેરિકા તથા
અફઘાનિસ્તાન વચ્ચે છેડાયેલા યુધ્ધના કારણે સૂકામેવાની આયાત ઘટી છે.</p>

<p>વેપારીઓ આ સંદર્ભમાં વિવિધ મત આપી રહ્યા છે. અમુક
વેપારીઓનું કહેવું છે કે અત્યાર સુધી માત્ર ૧૦થી ૧૫ ટકા માલ બજારમાં આવ્યો
છે. જ્યારે દિવાળી અને રમઝાનના તહેવાર સુધી માલની આયાત થઇ ચૂકી છે એવો
દાવો અમુક વેપારીઓ કરી રહ્યા છે. એકંદરે વિશ્વની પરિસ્થિતિને ધ્યાન રાખીએ તો સૂકામેવાના
ભાવમાં ખાસ વધારો થયો નથી. પરંતુ દિવાળી સુધી ગયા વર્ષની તુલનામાં
૨૦થી ૨૫ ટકા ભાવમાં વધારો થશે. એવો વેપારીઓનો એકમત આપી રહ્યા છે.</p>

<p>અફઘાનિસ્તાનમાંથી મુખ્યત્વે ખજૂર અંજીર, જર્દાળુ અને કિશમીસ
ભારતમાં આયાત કરાય છે. અંજીરનો ભાવ ગયા વર્ષે રૂ. ૩૧૦ કિલો હતો. આ વર્ષે રૂ. ૩૮૦
પર ભાવ પહોંચ્યો છે. આથી દિવાળી સુધીમાં તેના ભાવમાં ધરખમ વધારો
થશે. એવી સંભાવના છે.</p>

<p>બદામ મુખ્યત્વે અમેરિકાથી આવે છે. ગયા બે વર્ષ પૂર્વે એક કિલો
બદામનો ભાવ રૂ. ૪૪૦ કિલો હતો. આ વર્ષે ભાવ રૂ. ૨૪૦ સુધી ઘટી ગયો છે.
રૂપિયાની તુલનામાં ડોલર મોંઘો હોવાથી તેના ભાવમાં વધારો થવાની શક્યતા છે.</p>

<p>વાશી ખાતે માર્કેટમાં પૂરતા માલની આવક થઇ છે. હાલની
પરિસ્થિતિનો ફાયદો ઉપાડવાનો પ્રયત્ન મોટા વેપારીઓ કરીને તેનો સંગ્રહ કર્યો તો
નાના વેપારીઓને મોંઘા ભાવે સૂકો મેવો ખરીદવો પડશે. આથી ગ્રાહકોને વધુ પૈસા
આપવા પડશે. એવો અંદાજ ક્રાફડ માર્કેટ પરિસરમાં એક વેપારીએ વ્યક્ત કર્યો.</p>




<p><head>એપ્રિલ ૨૦૦૧થી અત્યાર સુધીમાં
આવકવેરા વિભાગે રૂા. ૧૪૦૦ કરોડની છૂપી આવકો શોધી કાઢી</head></p>

<p>મુંબઈ, તા.૧૦</p>

<p>આવકવેરા વિભાગે એપ્રિલ માસથી અત્યાર સુધીમાં શેરદલાલો
તેમજ અન્યોને ત્યાં પાડેલા દેશવ્યાપી દરોડામાં કુલ રૂા. ૧૪૦૦ કરોડની છૂપી આવક
શોધી કાઢી છે.</p>

<p>શેર દલાલોને ત્યાંથી જ રૂા. ૧૨૦૦ કરોડની છૂપી આવકો મળી આવી</p>

<p>શેર દલાલોને ત્યાં પાડેલા દરોડામાં રૂા. ૧૨૦૦ કરોડની છૂપી
આવકો મળી હતી અને એપ્રિલ, ૨૦૦૧થી અત્યાર સુધીમાં આવી કુલ રૂા. ૧૪૦૦ કરોડની
છૂપી આવકો શોધી કાઢવામાં આવી છે એવું આવકવેરા વિભાગના ડાયરેક્ટર જનરલ
(ઇન્વેસ્ટીગેશન) જી સારને અત્રે જારી કરેલી યાદીમાં જણાવ્યું છે.</p>

<p>આવકવેરા વિભાગે આ ઉપરાંત માધ્યમોની માધાંતા યુટીવી
જૂથ, ફેશન ડીઝાઇનર અલુ જાની અને અબુ ખોસલા, ટ્રાન્સવર્લ્ડ શીપીંગ, દેવાન હાઉસીંગ ફાયનાન્સ
કોર્પોરેશન, ડોનીયર શુટીંગ, એલ્ડર ફાર્મા તથા અન્ય દ્વારા કરવામાં આવેલી કુલ રૂા. ૮૬
કરોડ રૂપિયાની કરચોરી પકડી પાડી છે.</p>

<p>ખાસ મોડ્સ ઓપરેન્ડીની વિગતો આપતાં સારને જણાવ્યું હતું કે
જાની અને ખોસલા પાસે ગણત્રીમાં ન લેવાયેલા સ્ટોક્સ અને નહિ નોંધાયેલ રોકડ
વેચાણો હતા જ્યારે ફ્રેઇટ ફોર વર્ડીંગ એજન્ટે બનાવટી ટ્રાન્સપોર્ટ કંપનીના બિલ ઈસ્યુ
કર્યા હતા.</p>

<p>તેમણે ઉમેર્યું હતું કે પક્શકારોએ સ્વૈચ્છિક રીતે ૪૪૪ સ્થળોએ
પાડેલા દરોડાની રકમ અને લોકરની રકમ સત્તાવાળાઓને આપી દીધી હતી.</p>

<p> </p>




<p><head>સૂચિત વીઆરએસ પેકેજ વધુ આકર્ષક બનાવાયું છે</head></p>

<p>નવી દિલ્હી, તા. ૧૦</p>

<p>કેન્દ્રિય કર્મચારીઓ માટેની સૂચિત સ્વૈચ્છિક નિવૃત્તિ યોજના
(વીઆરએસ) વૂધુ પ્રમાણમાં આકર્ષક હશે. વિશેષ કરીને એક્સ ગ્રેસીયા રકમ (વળતર પેટે
ચુકવાનારી વધારાની રકમ) બાબતે સરકારે આ યોજનાને વધુ આકર્ષક બનાવી છે. આ
યોજના સ્વિકારનાર કર્મચારીને એક્સ ગ્રેસીયા રકમ માટે બે પ્રકારના વિકલ્પો આપવામાં
આવશે. એક્સ ગ્રેસીયા રકમ માટે કર્મચારીને પહેલાં વિકલ્પમાં નોકરીમાં પૂરાં કરેલા
પ્રત્યેક વર્ષ દીઠ ૪૫ દિવસના પગારના આધારે ગણતરી કરવાની પસંદગી કે પછી
નોકરીમાં પૂરા કરેલાં પ્રત્યેક વર્ષ દીઠ ૩૫ દિવસના સીધે સીધા પગારની પસંદગી
આપવામાં આવશે. જ્યારે બીજા વિકલ્પમાં નોકરીમાં બાકી રહેતાં પ્રત્યેક વર્ષ દીઠ ૨૫
દિવસના સીધેસીધા પગારની પસંદગી આપવામાં આવશે.</p>

<p>સૂત્રોએ કહ્યું હતું કે યુવાન કર્મચારીઓ આ યોજનાનો લાભ લઈ
શકે તે હેતુથી જ બીજો વિકલ્પ મૂકવામાં આવ્યો છે. અત્રે ખાસ ઉલ્લેખનીય છે કે આ
યોજના સ્વિકારનાર કર્મચારીને પેન્શન, ગ્રેજ્યુઈટી જેવા મળવાપાત્ર અન્ય લાભો ઉપરાંત એક્સ
ગ્રેસીયા રકમ મળશે. આ યોજનામાં પગારની વ્યાખ્યામાં બેઝીક વત્તા મોંઘવારીનો સમાવેશ
કરાયો છે. જોકે વર્તમાન પેટર્ન મુજબ પેન્શન અને ગ્રેજ્યુઈટી મેળવવા માટે વર્ષો જરૂરી
છે. તેમાં કોઈ ફેરફાર કરાયો નથી. ઉલ્લેખનીય છે કે હાલના નિયમો અનુસાર ૨૦ વર્ષની
નોકરી પુરી કરનાર કરનાર કર્મચારીને જ પેન્શન મળી શકે અને ૧૦ વર્ષની નોકરી પુરી
કરનાર કર્મચારી જ ગ્રેજ્યુઈટીનો લાભ મેળવવાપાત્ર બની શકે છે. સૂચિત વીઆરએસ
યોજનામાં આ વર્ષોની સંખ્યા યથાવત રાખવામાં આવી છે.</p>

<p>કેન્દ્ર સરકારે કર્મચારીઓને આકર્ષવા લીધેલો નિર્ણય</p>

<p> </p>




<p><head>બનાવટી ચલણી નોટ પાસે હોય તેથી ગુનો બનતો નથી</head></p>

<p>નવી દિલ્હી, તા.૧૦</p>

<p>બનાવટી ચલણી નોટ ધરાવનારને તેની પાસેની નોટો બનાવટી
હોવાની ખબર છે તેમ સાબિત ના થાય ત્યાં સુધી માત્ર આવી નોટ ધરાવવી તે
ગુનો ગણી શકાય નહીં એવું આજે સુપ્રિમ કોર્ટે રૂલીંગ આપ્યું હતું.</p>

<p>સુપ્રીમ કોર્ટનો મહત્વનો ચુકાદો</p>

<p>ન્યાયમૂર્તિ એસ.એસ.એમ. કાદરી અને ન્યાયમૂર્તિ એસ.એન. ફુકનની
બનેલી બેન્ચે તાજેતરમાં ચુકાદો આપ્યો હતો કે, ભારતીય દંડ સંહિતાની કલમ ૪૮૯ બી
અને ૪૮૯ સી હેઠળ વ્યક્તિને સજા કરવા માટે આવી ગેરકાયદે ચલણી નોટ ધરાવનાર
વ્યક્તિને તેની પાસેની ચલણી નોટો કે બેન્ક નોટો બનાવટી હોવાની ખબર હોય, અથવા
તેની જાણ હોવાનું કારણ હોય તે જરૂરી છે.</p>

<p>બનાવટી નોટો હોવાને કારણે જેને સેશન્સ કોર્ટે ત્રણ વર્ષની સજા
કરી હતી તેવી એક વ્યક્તિને નિર્દોષ છોડી મુકતાં સુપ્રિમ કોર્ટે જણાવ્યું હતું કે,
કોઈની પાસે બનાવટી ચલણી નોટો કે બેંક નોટો હોય અને તેનો ઉપયોગ કરવાનો
ઇરાદો હોય તો પણ નોટો બનાવટી હોવાની જાણ સાથે તેનો ઉપયોગ કરવાનો ઇરાદો ના
હોય તો કલમ ૪૮૯ સી હેઠળ કેસ બનવા માટે પૂરતું નથી.</p>

<p>હાઈકોર્ટે આ કેસમાં ગુનો માન્ય રાખતો પરંતુ સજા ઘટાડીને બે
વર્ષની કરતો હુકમ કર્યો હતો. સુપ્રિમ કોર્ટે આ હુકમને ફગાવી દઈ જણાવ્યું હતું કે,
દેશના અર્થકારણનું રક્શણ કરવાના જ નહીં પરંતુ ચલણી નોટોને અને બેંક નોટોને પૂરતું
રક્શણ આપવાના ઉદ્દેશથી કાયદામાં આ પ્રમાણેની જોગવાઈઓ રાખવામાં આવેલી છે. </p>

<p>કોર્ટે જાહેર કર્યું હતું કે, આ જોગવાઈઓ જેને જાણ જ ના હોય
તેવી નોટો ધરાવનાર કે તેનો ઉપયોગ કરનારને સજા કરવા માટે નથી.</p>

<p> </p>

<p> </p>






</body></text></cesDoc>