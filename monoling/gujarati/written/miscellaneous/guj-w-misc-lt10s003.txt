<cesDoc id="guj-w-misc-lt10s003" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-misc-lt10s003.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>unknown</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - unknown</publisher>
<pubDate>unknown</pubDate>
</imprint>
<idno type="CIIL code">lt10s003</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page unknown.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-17</date></creation>
<langUsage>Gujarati</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>
 3ફાઈલનું નામ - - LT01S003 . GTM 
 
 3પાન નં 204 
 
  નહિ તો કેવી રીતે મીરાં  અને સુરપતિ મંદિરમાં ઊભા રહી એક  
 તુમુલ ઝંઝાવત  અને વરસાદમાં ભીંના થઈ ગયાં ? ખંડેર મંદિર 
  એકદમ નાનું હતું ;  એ મંદિર તો નહોતું ;  બીજું  શું  તે પણ કહેવું 
  મુશ્કેલ હતું ; ઈંટનો સ્તૂપ કહેવો હોય તો કહેવાય ; માથા  ઉપર થોડું 
  જે આચ્છાદાન હતું  તે જ બચાવે એટલું મીરાં -  જેને સુરપતિ આ 
 કેટલાક દિવસથી રોજ પ્રથમની પત્ની તરીકે જોતો આવ્યો છે  તે 
 જ મીરાં -  તેને  ત્યારે સમજાતી નહોતી . મીરાંને કંઈક  થયું હતું ,  તે 
 દુર્યોગનો ભય માત્ર નહોતો , પણ એક  પ્રકારની વિહૂવળતા હતી  જે 
 માનવીને અચેત કરી મૂકે છે ; એવું કંઈક  સુરપતિએ અનુભવ્યું હતું 
  કે  જાણે મીરાં  કોઈ ગાઢ  સાન્નિધ્ય  માટે  વ્યાકુળ બની ગઈ હતી . 
 આ વ્યાકુળતાનો કંઈક  શેષ  રહેલો અંશ મીરાંની આંખો  અને ચહેરા 
 પર   હજુ છવાયેલો હતો .  એ વ્યાકુળતા પ્રથમની નજરે પડી હતી . 
 સુરપતિ સમજી શક્યો નથી  કે પ્રથમના મનમાં  એની પત્નીના ભીનાં 
 વસ્ત્ર  અને આંખ , મોં , ચહેરો જોઈ  કંઈ સંદેહ જાગ્યો છે  કે  કેમ . 
 બીજાંઓની દૃષ્ટિ પણ સુરપતિને ગમી નહોતી 
 
 ચા લઈ મીરાં  ફરીથી આવી . લસુરપતિ કપાળ પર  હાથ મૂકી 
 આંખો મીંચી બેઠો હતો . પલંગના અવાજથી તેણે મોં ઊંચું 
 કર્યું . 
 
 ચા પીને મીરાં થોડી વાર સામને જ ઊભી રહી ;  પછી 
 પલંગ પર  જઈને બેઠી .  તે બોલી , "  શું વિચારો છો ? " મીરાં આ 
  વખતે શાલ ઓઢીને આવી હતી . 
 
 "  ના ,  ના ,  શું  બીજું . . . ! " 
 
 " આટલી ચિંતાનું  કંઈ કારણ નથી , " મીરાં કંઈક  ઉપેક્ષા ભર્યા 
 ભાવે બોલી .  પોતાને  માટે પણ  તે  ફરીથી ચા લાવી છે . 
 " " ; " ? " 
 
 </p></body></text></cesDoc>