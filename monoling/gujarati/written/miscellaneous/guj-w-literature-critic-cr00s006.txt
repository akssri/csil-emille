<cesDoc id="guj-w-literature-critic-cr00s006" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-literature-critic-cr00s006.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>unknown</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - unknown</publisher>
<pubDate>unknown</pubDate>
</imprint>
<idno type="CIIL code">cr00s006</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page unknown.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-17</date></creation>
<langUsage>Gujarati</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>+ 
 30ફાઈલનું નામ - CR00S006 . GAL 
 30પુસ્તકનું નામ - ગુજરાતીમાં હાસ્ય  અને કટાક્ષ 
 30લેખક - મધુસુદન પારેખ 
 30પ્રથમ આવૃત્તિ : - 0 01988 
 30પ્રકાશક - નટવરસિહ  પરમાર 
 30મળવાનું ઠેકાણું - D . O . E . 
 30વિભાગ - AESTHETICS ( LITERATURE ) 
 30ઉપવિભાગ - CRITICISM 
 30કિંમત - રૂ . 17 . 00 
 30શબ્દ - 
 33 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 0 
 		306ગુજરાતીમાં હાસ્૟  અને કટાક્ષ 
 
 3પાન નં 46 
 
 3					પ્રકરણ - 4 
 
 3	  અર્વાચીન સાહિત્યમાં હાસ્યકટાક્ષનો પ્રવાહ 
 
 0કવિતામાં હાસ્યકટાક્ષ : 
 
 	 ઈ . સ . 1818 માં ગુજરાતમાં અંગ્રેજી રાજ્યઅમલ સ્થપાયો  અને 
  ધીમે  ધીમે દૃઢ થયો . મધ્યકાળમાં અશાંતિ , અંધાધૂંધી  તેમજ બિનસલામતીથી 
 કંટાળેલી પ્રજાને અંગ્રેજી રાજ્યઅમલ આશીર્વાદરૂપ લાગ્યો . અંગ્રેજોએ નિશાળો 
 સ્થાપી . મુદ્રણકળા , અંગ્રેજી ભાષા , અંગ્રેજી શિક્ષણ વગેરેનો પ્રસાર થવા માંડ્યો . 
 1857 માં મુંબઈ યુનિવર્સિટીના સ્થાપના થઈ . એ દ્વારા અંગ્રેજી સાહિત્યનો  અને 
 અંગ્રેજી સંસ્કૃતિનો સંપર્ક વધવા માંડ્યો . પ્રજાજીવનમાં નવી જાગૃતિ આવી . 
 સામાજિક  તેમજ ધાર્મિક અનિષ્ટો  સામે કેટલાક શિક્ષિત સમાજસુધારકોએ પોકાર 
 પાડ્યો  અને દેશમાં સમાજસુધારાનું મોજું  ફરી વળ્યું . 
 
 	 અંગ્રેજી સાહિત્યના સંપર્કને કારણે  ગુજરાતી ભાષામાં પણ નવલકથા , એકાંકી , 
 ટૂંકી વાર્તા , નિબંધ  વગેરે ગદ્યસાહિત્ય સ્વરૂપોનો ઉદ્ભવ થયો . કવિતામાં પણ 
 નવા નવા પ્રકારો ખેડાવા માંડ્યાં . મધ્યકાળમાં સાહિત્ય મોટેભાગે ધર્મલક્ષી હતું 
 તે 19 મા સૈકામાં સમાજસુધારાલક્ષી બન્યું . સમાજમાં  અનિષ્ટ રિવાજો  સામે 
 લોકોને જાગૃત કરવા  માટે એક  તરફ ગંભીર  , સામાજિક  અને બોધલક્ષી સાહિત્ય 
 રચાવા માંડ્યું . તે  સાથે કેટલાક સાહિત્યકારોએ હાસ્ય - કટાક્ષનો આશ્રય લઈને 
 સામાજિક  તેમ જ ધાર્મિક અનિષ્ટોની મજાક - મશ્કરી ઉડાવીને પ્રજાને અજ્ઞાન  અને 
 માનસિક જડતામાંથી  બહાર આણવાનો પ્રયત્ન કર્યો . અંગ્રેજી સાહિત્યમાં કટાક્ષ 
 ( Satire ) , હાસ્ય ( Humour ) નું સાહિત્ય  વિવિધ પ્રકારે  અને વિપુલપણે ખેડાયું હતું . 
 હાસ્યકટાક્ષયુક્્ત નવલકથા , વિનોદી ટૂંકી વાર્તાઓ , પેરડીના પ્રયોગો , કટાક્ષમય 
 કવિતા , પ્રહસનો , નિબંધિકાઓ , વગેરેના સારા એવા નમૂના અંગ્રેજી સાહિત્યના 
 ગાઢ  સંપર્કને કારણે આપણા લેખકોને સુલભ થયા હતા . હાસ્યનાં  અને કટાક્ષનાં 
 એ  વિવિધ સ્વરૂપોની પ્રેરણા  અને સહાય લઈને  ગુજરાતી ભાષામાં પણ હાસ્ય 
 કટાક્ષનું સાહિત્ય  વિશિષ્ટ રીતે  અને વિપુલપણે ખેડાવા માંડ્યું . મધ્યકાળમાં 
 હાસ્ય  અથવા વિનોદ , કૃતિમાં નાના  કે મોટાં અંશ રૂપે આવતો . હાસ્ય - 
 
 3પાન નં 47 
 
 વિનોદની કોઈ સળંગ કૃતિ  ભાગ્યે જ રચાઈ હતી . તદઉપરાંત મધ્યકાળમાં ખેડાયેલું 
 હાસ્યસાહિત્ય  બહુધા સ્થૂલ  અને સપાટી પરનું હતું . અલબત્ત , અખા  અને પ્રેમાનંદ 
 દ્વારા ઉચ્ચ કોટિના હાસ્યકટાક્ષ ખેડાયાં હતાં ,  તેમ  છતાં  સૂક્ષ્મ  અને વૈવિધ્યપૂર્ણ હાસ્ય 
 તો  અર્વાચીન યુગમાં જ નોંધપાત્ર રીતે ખેડાવું શરૂ થયું . હાસ્યરસની  અથવા તો 
 કટાક્ષની સળંગ કૃતિઓ પણ  અર્વાચીન યુગમાં પ્રકાશિત થવા  માંડી . હાસ્ય  અને 
 કટાક્ષ આલેખનમાં  વધુ તીવ્રતા  અને કલાનું ઊંડાણ આવ્યાં . હાસ્યરસ ઉચ્ચ 
 કોટી   ના કટાક્ષથી  માંડીને  ઠેઠ " નોનસેન્સ " ( અર્થહીન ) કવિતા સુધીની રચનાઓમાં 
 રસમો , ફેશનો , વૃત્તિઓ , વલણો શાસક પક્ષની  વિકૃત નીતિ - રીતિઓ , રાષ્ટ્રીય 
 દૂષણો ,  તેમજ વૈયક્્તિક વિચિત્રતાઓ , ધૂન , રોજિંદા વ્યવહારમાં પ્રગટ થતી વિલક્ષણ 
 પરિસ્થિતઓ  વગેરે હાસ્ય  અને કટાક્ષનાં નિશાન બનવા લાગ્યાં  અને હાસ્ય 
 કટાક્ષની કવિતાનો  વિપુલ પ્રવાહ વહેતો થયો . 
 
 આ તબક્કે હાસ્યરસનું વિલક્ષણ રીતે ખેડાણ કરનારી  પારસી કોમના સાહિત્યનો 
 પણ નિર્દેશ કરવો ઘટે . પારસીઓ જાનામાલની  તેમજ ધર્મની રક્ષા ખાતર વખાના 
 માર્યા પોતાનું મૂળ  વતન ત્યજીને ગુજરાતમાં આવીને વસ્યા  અને  ઠરીઠામ થયા હતા . 
 તેમણે  ગુજરાતી ભાષાને  અને કેટલીક રીતરસમોને અપનાવી લીધી . અંગ્રેજ રાજ્ય 
 અમલના પ્રભાવે ,  અને પશ્ચિમી સંસ્કૃતિના પ્રભાવે ગુજરાતની હિંદુ  તેમજ અન્ય  
 કોમોને  જેમ આંજી દીધી તે જ રીતે  પારસી કોમને પણ આંજી દીધી . અંગ્રેજી 
 શિક્ષણ  અને સાહિત્યનો પ્રસાર પામી કેટલાક  પારસી લેખકો  અને સમાજસુધારકો - 
 એ પોતાની કોમની આનંદી - રમૂજી  પ્રકૃતિ ધરાવનારી હતી .  ગુજરાતી એ તેમની 
 દત્તક લીધેલી ભાષા હતી . શુદ્ધ ,  શિષ્ટ ,  ગુજરાતી ભાષા બોલવા લખવાનું તેમને 
  માટે  મુશ્કેલ હતું . તેમની ભાષાઉચ્ચારણની મુશ્કેલીમાંથી  અને  પારસી , ઉર્દૂ , 
 અંગ્રેજી  વગેરે ભાષા સાહિત્યના અભ્યાસથી - સંપર્કથી તેમની એક  નવી લાક્ષણિક 
  પારસી ભાષા ઉદ્ભવી . આ  પારસી બોલીમાં  શિષ્ટ  ગુજરાતી શબ્દોની ઠરડમરડ0 
 અન્ય  ભાષાઓના શબ્દનું  વિચિત્ર મિશ્રણ , કેટલાક લાક્ષણિક શબ્દપ્રયોગો એ બધી 
 તેની વિશેષતાઓ હતી . અલબત્ત કેટલાક  પારસી સાહિત્યકારોએ  સતત પરિશ્રમ - 
 પૂર્વક ,  શિષ્ટ ગુજરાતીમાં બોલવા , લખવાનો મહાવરો પાડીને  ગુજરાતી સાહિત્યના 
 પ્રવાહમાં એમની  પારસી કોમના સાહિત્યનો પ્રવાહ પણ ભેળવ્યો .  તેમ  છતાં 19 માં 
 સૈકાના અંત સુધી મોટે ભાગે  પારસી લેખકોએમની લાક્ષણિક  પારસી બોલીમાં 
 કવિતા , નવલકથા નાટ્ય આદિનું સર્જન કર્યું . 
 * 
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 
 
 </p></body></text></cesDoc>