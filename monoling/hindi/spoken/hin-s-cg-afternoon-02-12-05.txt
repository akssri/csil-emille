<cesDoc id="hin-s-cg-afternoon-02-12-05" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-s-cg-afternoon-02-12-05.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Mr. Husain Adenwala)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-03</pubDate>
</publicationStmt>
<sourceDesc>
<recording type="audio">
<respStmt><resp>Location recording by</resp>
<name>Andrew Hardie</name></respStmt>
<equipment>audio tapes</equipment>
<date>02-12-05</date>
<broadcast>
<bibl><title>Afternoon Show with Navinder Bhogal</title>
<author>BBC Asian Network</author>
</bibl>
</broadcast>
</recording>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Spoken text only has been transcribed.
</samplingDesc>
</encodingDesc>
<profileDesc>
<creation>
<date>03-05-28</date>
<rs type="city">Lancaster, UK</rs>
</creation>
<langUsage>Contemporary spoken Hindi-Urdu language used in the UK, transcribed as Hindi
</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
</profileDesc>
<revisionDesc>
</revisionDesc>
<textClass>
<channel mode="s">radio broadcast</channel>
<constitution type="single"></constitution>
<derivation type="original"></derivation>
<domain type="art"></domain>
<factuality type="fact"></factuality>
<translations>
</translations>
</textClass>
<particDesc>
<person id="HF001" sex="F" age="unknown"><occupation code="1"></occupation></person>
<person id="HF002" sex="F" age="unknown"><occupation code="1"></occupation></person>
<particLinks>
</particLinks>
</particDesc>
<settingDesc>
<setting who="HF001 HF002">
<name type="country">UK</name>
<locale>radio station</locale>
<activity>presenting a radio programme</activity>
</setting>
</settingDesc>
</cesHeader>
<text>
<body>


<event desc="English news" dur="approximately 2 minutes"></event>

<event desc="announcement clip" dur="approximately 12 seconds"></event>

<u id="1" who="HF001">दो बजकर चार मिनट हुए हैं ए एम रेडियो और डिजिटल सैटेलाईट चेनल आठ सौ उनहत्तर पर ये बी बी सी एशियन नेटवर्क हैं । नम्बर <foreign lang="eng">08459 440 445</foreign> आज जुमेरात दिस्मबर की पाँच तारिख सन् दो हज़ार दो उम्मीद है आप सब खैरियत से होंगे । सबसे पहले मेरे तमाम सुनने वालो को मेरा प्यार भरा आदाब अस्सलामवलैकुम नमस्कार बंदगी और सत्यश्रीअकाल । <foreign lang="eng">and a very good after noon to you</foreign> याद रहे बिंदर साहेबा आप के टेलीफोन्स के इंतज़ार मे हैं ।<event desc="music" dur="approximately 5 seconds"></event>इस से पहले की मै प्रोग्राम शुरु करुं अपने तमामा सुनने वालों को दिल की गहराई से ईद मुबारक कहते हैं । और अल्लाह ताला से यही दुआ के ऐसी खुशियों भरी ईदें आप की ज़िंदगी मे बार बार आती रहें । आप की हर मुंह मांगी मुराद पूरी हो अगर आप हमे ईदी देना चाहते हैं तो सिर्फ इतना की नम्बर डायल करें <foreign lang="eng">08459 440 445</foreign> हमारे लिये यही ईदी होगी । या फेक्स मेसेज उस से भी ज़्यादा <foreign lang="eng">07786 202 001</foreign> अपनी ज़ुबान मे आप ईद मुबारक कहना चाहते हैं । अब तो हम आप को खुशामदीद कहेंगे । हमे आप से बात करके बहुत अच्छा लगेगा नम्बर वही डायल करना होगा <foreign lang="eng">08459 440 445</foreign> आईए इसकी दोपहर को एक यादगार दोपहर बनाए । ईद खुशियों का दिन है एक साथ मिलकर ईद मनाए और ये खुशियां बाँटें । नम्बर आपको डायल करना है <foreign lang="eng">08459 440 445 fax messages please 07786 202 001</foreign> ये चाहत ही तो कह रही हैं और सबूत क्या चाहिए ईद की ये खुशियों भरी दोपहर सिर्फ आपके नाम <foreign lang="eng">good after noon to you</foreign></u>

<event desc="song" dur="approximately 5 minutes"></event>

<u id="2" who="HF001">आवाज़े अनुराधा पोढ़वाल और मोहम्मद अज़ीज़ की । नम्बर है <foreign lang="eng">08459 440 445</foreign> ईद के पैगाम आप के चाहने वालो के नाम आप के रिश्तेदारों के नाम अगर हम से बात करना चाहें कुछ कहना चाहतें हैं तो भी बड़े शौक से चले आईए नम्बर <foreign lang="eng">08459 440 445</foreign></u>

<event desc="song" dur="approximately 4 minutes"></event>

<u id="3" who="HF001">लता मंगेशकर और मुकेश की आवाज़ें ये गाना डेडिकेट किया गया था वुलवाहेम्प्टन से फतह भाई आप को लेस्टर मे छोटा लाल भाई लेस्टर मे हरीश भाई आपको लेस्टर मे जस्सु बहन और कांति भाई आप तमाम सुनने वालो को ईद की बहुत बहुत मुबारक बाद दे रहे है दिल की गहराई से शुक्रिया अदा करते हैं प्रोग्राम की तारिफ के लिए । <foreign lang="eng">08459 440 445</foreign> आप नी ज़ुबानी किसी को कहना चाहते हैं ईद की मुबारक बाद देना चाहते हैं तो बड़े शौक के साथ <foreign lang="eng">fax messages 07786 202 001</foreign><event desc="music" dur="approximately 4 seconds"></event>खुशियों भरा दिन हैं आईये मिलकर मनाए ।</u>

<event desc="song" dur="approximately 5 minutes"></event>

<event desc="traffic control information" dur="approximately 1 minutes"></event>

<u id="4" who="HF001">वक्त है दो बजकर तेईस मिनट <foreign lang="eng">08459 440 445 your fax messages on 07786 202 001</foreign> गुरजीत सिंह गिल कोवेन्टो से आपने याद फरमाया है ईद की मुबारक बाद तमाम सुनने वालो को दे रहे हैं आप ।<event desc="music" dur="approximately 3 seconds"></event>अगर आप एयर पर आ कर अपनी ज़ुबानी किसी को मुबारक बादी देना चाहते है तो बड़े शौक के साथ चले आईये । <foreign lang="eng">08459 440 445 come rain shine dar day whatever day i want your fax messages</foreign></u>

<event desc="song" dur="approximately 6 minutes"></event>

<event desc="music" dur="approximately 4 seconds"></event> 

<u id="5" who="HF001">दोपहर के ठीक ढाई बजे हैं बी बी सी एशियन नेटवर्क से खबरें आईशा एहमद की ज़ुबानी ।</u>

<u id="6" who="HF002">आज बरतानिया के मुल्क भर मे मुसलमानों का त्योहार ईद उल फितर मनाया जा रहा है । बंग्लादेश मे एक हादसे के नतीजे मे सौलह लोग गए हैं । नेपाल मे तीन पुलिस मैन मारे गए हैं । अब आप खबरें तफसील के साथ सुनिये ।<pause dur="2"></pause>बंग्लादेश मे पुलिस का कहना है के जमाला पुर की डिस्ट्रिक मे एक लौरी के हादसे मे कम से कम सौलह लोग मारे गए हैं बताया जाता है के यह हादसा उस वक्त पैश आया जब ईद की छुट्टी से एक रात पहले हज़ारो लोग केपीटल धाका से अपने अपने गाँव जाने के लिए सफर की तैयारी कर रहे थे । पुलिस अफसरों ने बताया के उस लौरी मे सतरा लोग सवार थे हादसे के बाद ज़िन्दा बचने वाले अकेले आदमी की हालत भी खतरनाक बताई जाती है और कहा जाता है के वो मकामी हस्पताल मे ज़ेरे ईलाज हैं ।<pause dur="2"></pause>प्रसीडेंट सद्दाम हुसैन का कहना है कि यनाईटेड नेशन के हत्यारों के इंस्पेकशन्स से उन्हे अमेरिका के उस इलज़ाम को ग़लत साबित करने का मौका मिल गया है जिसमे वो कहते थे के बगदाद बड़े पैमाने पर तबाही मचाने वाले हत्थयार बना रहा हैं । पिछले हफ्ते शुरु होने वाले इस इंसपेक्शन के बारे मे पहली मरतबा बाते करते हुए उन्होने कहा के सिर्फ इस लिए के ईराक के लोगो को नुकसान ना पहुँचे वो इस इंसपेकशन के लिए राज़ी हो गए थे । याद रहे के युनाईटेड नेशन ने ईराक को इत्वार तक का वक्त दिया है के वो अपने न्युक्लीयर बायलोजीकल और केमिकल हत्थयारों के प्रोग्राम दिखा दे उनका कहना है के ईराक के लिए ज़रुरी है के वो सच्चाई बता दे वरना उन के खिलाफ फौजी कार्यवाही की जाए गी ।<pause dur="2"></pause>नेपाल के माउस बागि़यों की पुलिस स्टेशन्स और दुसरे सरकारी निशानों पर हमला करने के नतीजे मे कम से कम तीन पुलिस मैन मारे गए है ये हमले सात सलों से जारी लड़ाई को खत्म करने के सिलसिले मे की जाने वाले बात चीत के एक विधिन पूरा होने से पहले कर दिये गए है अहलकारों का तास्सुर इस बारे मे परेशान गुन था । और उन्होने कहा के वो बागियों की स्टेट मेन्ट के बारे मे फिक्र कर रहे हैं नामा निगारों ने बताया के हमेशा अमन की बातचीत के बात खून बहा हुआ हैं जिसमे दोनो पार्टियां एक दुसरे को लड़ाई शुरु करने का इल्ज़ाम देती रही हैं ।<pause dur="2"></pause>पाकिस्तान के शहर कराची मे पुलिस का कहना है के मेसदोनिया के आॅनरी काॅनसिल के दफ्तर मे एक बाॅम्ब के धमाके मे तीन लोग मारे गए है पुलिस ने बताया के ये लाशें मलबे तले दबी हुई थी और मरने वाले सभी पाकिस्तानी थे और हो सकता है के उन्हे धमाके से पहले बांध कर मार भी दिया गया हो । डाक्टर ने बताया के उनमे से दो की शहरत कटी हुई थी । अभी तक ये पता नही चल सका के इस हमले के पीछे किस का हाथ था ।<pause dur="2"></pause>अफ्गानिस्तान मे पुलिस फोर्स को ईद उल फितर की तकरीबार के मौके पर चौकस कर दिया गया है उन्हे खतरा है के तालिबान की बकिया रिजीम ईद के मौके पर फसादात करने की कोशिश करेंगे । अफ्गानिस्तान के इंटिरियर मिनिस्टर ने बताया के काबुल की एक नुमाया मस्जीद से गोला बारुद का सामान बरामत किया गया जहां एक हज़ार से ज़्यादा लोग ईद की नमाज़ पड़ने के लिए इक्ठ्ठे हुए थे । काबुल की सिक्युरिटी की चीफ ने बताया के हिफाज़त के लिए गश्त करने वाली पुलिस की तादाद पाँच गुना तक बड़ा दी गई है । ये खबरें आप बी बी सी एशियन नेटवर्क से आईशा एहमद की ज़बानी सुन रहे हैं ।<pause dur="2"></pause>युनाईटेड स्टेट्स के प्रेसीडेंट जार्ज बुश ने अफ्गानिस्तान पर अगले चार सालों मे तीन बिलियन डालर्स खर्च करने के एक बिल पर दस्तखत किये हैं । वाईट हाऊस ने बताया की ये रक्म मुल्क की मिलिटरी और दुसरी ईकोनोमि को बहाल करने के काम आएगी । युनाईटेड नेशन की सिक्युरिटी काॅनसिल ने ईराक के लिए तेल के बदले खाने के प्रोग्राम को अगले छः महिनों तक बड़ा दिया है और बग़दाद की ज़रुरत वाली चीज़ो की दरामत के बारे मे भी सोच ने का वादा किया हैं याद रहे के इस प्रोग्राम को हर छः महीने के बाद रिन्यु किया जाता हैं लेकिन वाशिंगटन इस बात पर ज़ोर दे रहा था के छः महीने के इस अरसे को कम कर देना चाहिए ।<pause dur="2"></pause>इंडोनेशिया मे बाली के नाईट कल्ब पर हमले की तहकीक करने वाली पुलिस का कहना है की इस सिलसिले मे इसी हफ्ते गिरफ्तार किये जाने वाले आदमी ने उस हमले मे शामिल होने का इलज़ाम कुबुल कर लिया है उनका कहना है के मुखलास नामी इस मशकुक आदमी ने ये मान लिया है की वो इस हमले के सिलसिले मे की जाने वाली मिटिंग्स मे शामिल हुआ था । पुलिस को शुबा है की मुखलास को हाल ही मे इसलामी तनज़ीम जामिया इसलामीया का रिजनल चीफ मुकर्रर किया गया था । याद रहे के इस तनज़ीम के बारे मे युनाईटेड स्टेट्स का कहना है की इस का तालुक अलकायदा की तनज़ीम के साथ  है ।<pause dur="2"></pause>श्रीलंका की हुकुमत और तमिल टायगर्स के बागियों के नुमाईंदो का कहना है के उन्होने अपनी ज़िम्मेदारियों और इखतेयारात को बाँटने के सिलसिले मे एक समझोता कर लिया है नामा निगारों का कहना है के इस समझोतें से मुल्क मे होने वाली सिविल वार को खत्म करने के सिलसिले मे जो बातचीत हो रही हैं उसे तकवीयत मिलेगी । याद रहे की इस जंग मे साठ हज़ार से ज़्यादा लोग मारे गए हैं ।<pause dur="2"></pause>इंडिया की हुकुमत ने इंडिया और पाकिस्तान के ज़ेरे इंतज़ाम कश्मीर को अलाहिदा करने वाली लाईन आॅफ कन्ट्रोल के करीब कारगील के पहाड़ो पर दुसरे मुल्को के एक्सपीडिशन पर लगाई जाने वाली पाबंदियों को हटा दिया हैं याद रहे के ये पाबंदी पिछले साल उस वक्त लगाई गई थी जब दोनो मुल्कों की फौजें उस ईलाके के बारे मे लड़ रही थी जो इंडिया के कहने के मुताबिक उन का था ।<pause dur="2"></pause>हाल मे एक ऐसी आदमी के बुरी तरह ज़ख्मी होने की इत्तलाह मिली है जिसके बारे मे शुबा है के उस पर हमला नसली तफर्रके की वजह से किय गया था । बताया जाता है कि चौबीस साला ईराकी कर्ड को दो आदमियों के हमला करने के बाद चहरें पर बहुत सी चौटें लगी थी । और अब आखिर मे मौसम आज मुल्क के मरकज़ी और मशरिकी हिस्सों मे बादल छाए रहेंगे और कभी कभी बारिश के छिंटे भी पड़ सकते हैं लेकिन मुल्क के मग़रिबी हिस्सों मे आम तौर पर खुश्क मौसम खुश्क और खुश गवार रहेगा । इसी के साथ बी बी सी एशियन नेटवर्क से खबरें खत्म हुई ।</u>

<event desc="announcement clip" dur="approximately 13 seconds"></event>

<u id="7" who="HF001">तीन बजने मे तेईस मिनट बाकी हैं ए एम रेडियो और डिजिटल सेटेलाईट चेनल आठ सौ उनहत्तर पर ये बी बी सी एशियन नेटवर्क है चार बजे की खबरों के बाद ड्राईव समीना अली खान के साथ याद रहे ड्राईव मे छः से ले के आठ बजे तक ईद स्पेशल प्रोग्राम नवीद अखतर के साथ पैश करेंगी समीना अली खान । और आठ से दस हिन्दी उर्दु प्रोग्राम संजय शर्मा पैश करेंगे और दस से बारह बजे तक आदिल रे के साथ । द्विया जी आपने एक्सब्रीक से याद फरमाया है याद आवरी के लिए बहुत बहुत शुक्रिया वुलवाहेम्टन से रफीक साहब आप ने ईद की मुबारक बाद तमाम सुनने वालो को दी हैं । सतवंत बहन जी और आनंद वीर जी आप ने वुलवाहेम्टन से याद फरमाया है तमाम सुनने वालो को ईद मुबारक कहा है अगला नगमा आप के नाम करते है और बक्शो बहन जी आपने याद फरमाया बहुत बहुत ईद की मुबारक बाद दे रही है और ये नग़मा आप सब के नाम ।</u>

<event desc="song" dur="approximately 3 minutes"></event>

<u id="8" who="HF001">लता मंगेशकर और मोहम्मद रफी को आवाज़े और फेक्स मेसेज कहते है <foreign lang="eng">may i be the first by all means be my guest</foreign> बहुत बहुत शुक्रिया आप का टेक्स्ट मेसेज के लिए और बाबर भट्ट आप का फेक्स मेसेज है की <foreign lang="eng">hi na eid mubarak to all the staff</foreign> बहुत बहुत शुक्रिया बाबर भट्ट आप का और सोन परी जी आप का फेक्स मेसेज मिला है ईद मुबारक हमे दे रही हैं । और कहती हैं <foreign lang="eng">to my didi jiju and fardia baji please play a song for them all</foreign> बहुत बहुत शुक्रिया सोन परी जी आप के फेक्स मेसेज का और उम्मीद करती हुं की आप कि दीदी जिजु फरिदा बाय जी सुन रही होंगी । मोहम्मद आरिफ साहब स्मोहिल से आपने याद फरमाया हैं ईद मुबारक कह रहे हैं तमाम सुनने वालों को और इसी के साथ कामरान इकबाल सादिया आयुश और सारिश को जनाब दिल की गहराई से शुक्रिया अदा करते हैं और ये नग़मा आप सब के नाम । सोन परी जी थोड़ा सा इंतज़ार करें आप की और से नग़मा कुछ ही देर मे ।</u>

<event desc="song" dur="approximately 6 minutes"></event>

<u id="9" who="HF001">ये आवाज़ कुमार शानु की । तीन बजने मे तेराह मिनट बाकी है <foreign lang="eng">08459 440 445</foreign> ईद मना रहे हैं दिल की गहराई से आप को ईद मुबारक कहते हैं । आईये एक साथ मिलकर ईद मनाए <foreign lang="eng">fax messages 07786 202 001</foreign> और अगला नग़मा सोन परी जी आप की और से आपकी दीदी जीजु और फरिदा बाजी के नाम ।<event desc="music" dur="approximately 3 seconds"></event>वेलिनबरो से रेमश सुन्दरम् साहब आप ने याद फरमाया है तमामा सुनने वालो को ईद मुबारक कह रहे हैं । जनाब बहुत बहुत शुक्रिया और लेस्टर से मिसेस लिटा आप ने याद फरमया है <foreign lang="eng">you are doing a fantastic job eid mubarak to all our friend and relatives all over the world</foreign></u>

<event desc="song" dur="approximately 4 minutes"></event>

<u id="10" who="HF001">बाबर साहब बरमिंघम से आप का नाम सुनने वालों को ईद मुबारक कह रहे हैं जनाब बहुत बहुत शुक्रिया और अली साहब बरमिंघम से आप ने याद फरमाया है तमाम सुनने वालों को ईद मुबारक कह रही हैं । रुही साहेबा ये आप के नाम ।</u>

<event desc="song" dur="approximately 5 minutes"></event>

<event desc="music" dur="approximately 30 seconds"></event>

<u id="11" who="HF001">लेस्टर से किशोर जैन जी आप ने याद फरमाया है तमाम सुनने वालों को आप ईद मुबारक कह रहे हैं ।</u>

<event desc="song" dur="approximately 3 minutes"></event>

<u id="12" who="HF001">तीन बजे हम चलते हैं खबरों की ओर मेरी आप की मुलाकात खबरों के उस पार नम्बर नही <foreign lang="eng">08459 440 445 fax messages 07786 202 001</foreign></u>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<event desc="English news" dur="approximately 4 minutes"></event>

<event desc="announcement clip" dur="approximately 8 seconds"></event>

<u id="13" who="HF001">प्रोग्राम के दुसरे और आखिरी दौर मे एक बार फिर आप का दिलो जान के साथ स्वागत खुशामदीद तिज्जी आयनो । तीन बजकर चार मिनट हुए हैं ए एम रेडियो और डिजिटल सेटेलाईट चेनल नम्बर आठ सौ उनहत्तर पर ये बी बी सी एशियन नेटवर्क हैं । पूरे चार बजे तक नविंदर जीत भौगल चार बजे की खबरों के बाद ड्राईव समीना अली खान के साथ नम्बर है <foreign lang="eng">08459 440 445</foreign> फेक्स मेसेजेस के लिए <foreign lang="eng">07786 202 001</foreign> लेस्टर से मधु जी आप तमाम सुनने वालो को ईद मुबारक कह रही है और स्कोट लैंड से शहिद जी आप ने अपने तमाम दोस्तो को और रिश्तेदारों को याद किया है और ईद मुबारक कहते हैं । बरमिंघम मे बशीर मिर्ज़ा साहब आप की और से तमाम सुनने वालों ईद मुबारक और सतनाम जी बरमिंघम मे आप ने याद फरमाया हैं । ईद मुबारक कह रहे हैं लेस्टर से अशोक पटेल साहब आप ने तमाम सुन ने वालों को ईद मुबारक कहा हैं । स्कनथोप निर्मल जी आप ने याद फरमाया ज़फर इकबाल साहब आप व्होलसोल से और रमेश भाई नोटिंघम से आप तमाम सुनने वालों को ईद की बहुत बहुत मुबारक बाद दे रहे हैं और फेक्स मेसेज नवीद जी आप की और से आया हैं जनाब पहली बात तो हम किसी से कभी खफा नही होते आप से माज़ेरत चाहतीं हुं के आप का नग़मा मेरे पास नही इसलिए बजा ना भाई उम्मीद करती हुं की अगला नग़मा आप कुबुल फरमाएंगे आप की ओर से आप के तमाम रिश्ते दारों के नाम जान पहचान वालों के नाम ।</u>

<event desc="song" dur="approximately 7 minutes"></event>

<event desc="music" dur="approximately 8 seconds"></event>

<u id="14" who="HF001">कुर्बान भाई और खतीजा बहन कपासी डेडली से आप ने याद फरमाया हैं । ईद की मुबारक बाद दे रहे है तमाम सुनने वालों को सुनिता जी आप को याद आई हमारी हमने सौचा दो हफ्तों से आप भूल गई हमे अकेला छोड़ दिया तमाम सुनने वालो को ईद मुबारक कह रही है आप और बिरजु साहब आप ने याद फरमाया है ईद कि मुबारक दे रहे हैं बाकी का क्या हुआ पंडित कौशिक जी पिटरब्रा से आप ने याद फरमाया है ईद की मुबारक बाद के साथ जनाब बहुत बहुत शुक्रिया अदा करते हैं ।</u>

<event desc="song" dur="approximately 5 minutes"></event>

<u id="15" who="HF001">सुखवींदर की आवाज़ । और ये फेक्स मेसेज एड्बास्टन से आन्नद साहब आप ने भेजा हैं कहते हैं <foreign lang="eng">a very very happy eid day to my family friends and all the listeners</foreign> जनाब दिल की गहराई से शुक्रिया अदा करती हुं । जिस नग़मे की आप ने फरमाईश की है माज़ेरत चाहतीं हुं इस वक्त वो मेरे पास नही हैं ।</u>

<event desc="song" dur="approximately 4 minutes"></event>

<event desc="traffic control information" dur="approximately 1 minutes"></event> 

<u id="16" who="HF001">तीन बजकर तेईस मिनट <foreign lang="eng">08459 440 445 and for your fax messages -07786 202 001</foreign> गुरमीत सिंह मथारु साहब आप ने याद फरमाया है तमाम सुनने वालों को ईद मुबारक कह रहे हैं ।</u>

<event desc="song" dur="approximately 5 minutes"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="17" who="HF001">कुछ ही देर मे बी बी सी एशियन नेटवर्क की खबरें मेरा आप का साथ पूरे चार बजे तक चार बजे की खबरों के बाद ड्राईव समीना अली खान के साथ और याद रहे के छः से ले कर आठ बजे तक आज ईद स्पेशल<event desc="music" dur="approximately 5 seconds"></event>आठ से दस हिन्दी उर्दु प्रोग्राम संजय शर्मा के साथ । और आठ से लेकर दस बजे तक प्रोग्राम आदिल रे पैश करेंगे ।<event desc="music" dur="approximately 3 seconds"></event>दोपहर के ठीक साढ़े तीन बजे बी बी सी एशियन नेटवर्क से खबरें आयशा एहमद की ज़बानी ।</u>

<u id="18" who="HF002">आज बरतानिया के मुल्क भर मे मुसलमानों का त्योहार ईद उल फितर मनाया जा रहा है । बंग्लादेश मे एक हादसे के नतीजे मे सौलह लोग गए हैं । नेपाल मे तीन पुलिस मैन मारे गए हैं । अब आप खबरें तफसील के साथ सुनिये ।<pause dur="2"></pause>बंग्लादेश मे पुलिस का कहना है के जमाला पुर की डिस्ट्रिक मे एक लौरी के हादसे मे कम से कम सौलह लोग मारे गए हैं बताया जाता है के यह हादसा उस वक्त पैश आया जब ईद की छुट्टी से एक रात पहले हज़ारो लोग केपीटल धाका से अपने अपने गाँव जाने के लिए सफर की तैयारी कर रहे थे । पुलिस अफसरों ने बताया के उस लौरी मे सतरा लोग सवार थे हादसे के बाद ज़िन्दा बचने वाले अकेले आदमी की हालत भी खतरनाक बताई जाती है और कहा जाता है के वो मकामी हस्पताल मे ज़ेरे ईलाज हैं ।<pause dur="2"></pause>अफ्गानिस्तान मे पुलिस फोर्स को ईद उल फितर की तकरीबार के मौके पर चौकस कर दिया गया है उन्हे खतरा है के तालिबान की बकिया रिजीम ईद के मौके पर फसादात करने की कोशिश करेंगे । अफ्गानिस्तान के इंटिरियर मिनिस्टर ने बताया के काबुल की एक नुमाया मस्जिद से गोला बारुद का सामान बरामत किया गया जहां एक हज़ार से ज़्यादा लोग ईद की नमाज़ पड़ने के लिए इक्ठ्ठे हुए थे । काबुल की सिक्युरिटी की चीफ ने बताया के हिफाज़त के लिए गश्त करने वाली पुलिस की तादाद पाँच गुना तक बड़ा दी गई है ।<pause dur="2"></pause>प्रसीडेंट सद्दाम हुसैन का कहना है कि  युनाईटेड नेशन के हत्यारों के इंस्पेकशन्स से उन्हे अमेरिका के उस इलज़ाम को ग़लत साबित करने का मौका मिल गया है जिसमे वो कहते थे के बगदाद बड़े पैमाने पर तबाही मचाने वाले हथियार बना रहा हैं । पिछले हफ्ते शुरु होने वाले इस इंसपेक्शन के बारे मे पहली मरतबा बाते करते हुए उन्होने कहा के सिर्फ इस लिए के ईराक के लोगो को नुकसान ना पहुँचे वो इस इंसपेकशन के लिए राज़ी हो गए थे । याद रहे के युनाईटेड नेशन ने ईराक को इत्वार तक का वक्त दिया है के वो अपने न्युक्लीयर बायलोजीकल और केमिकल हत्थयारों के प्रोग्राम दिखा दे उनका कहना है के ईराक के लिए ज़रुरी है के वो सच्चाई बता दे वरना उन के खिलाफ फौजी कार्यवाही की जाए गी ।<pause dur="2"></pause>बुश की इंतज़ामिया के एक नुमाया अफसर ने युनाईटेड स्टेट्स पर तनकीद कि हैं वो मुसलिम मुमालिक मे डेमोक्रेसी कायम करने के लिए कुछ नही कर रहे हैं । मिस्टर हास जिन्हे यु एस की फाॅरेन पोलिसी टीम का एहम मेम्बर समझा जाता है उनका कहना है के वाशिंगटन मुसलमान मुमालिक को तहाफ्फुज़ देने से हमेशा बचता रहा हैं लेकिन उन्होने कहा के अब युनाईटेड स्टेट्स मुसलिम दुनिया मे डिमोक्रेसी के कयाम के लिए ज़्यादा कोशिशें करेगा ।<pause dur="2"></pause>नेपाल के माउस बागि़यों की पुलिस स्टेशन्स और दुसरे सरकारी निशानों पर हमला करने के नतीजे मे कम से कम तीन पुलिस मैन मारे गए है ये हमले सात सलों से जारी लड़ाई को खत्म करने के सिलसिले मे की जाने वाले बात चीत के एक विधिन पूरा होने से पहले कर दिये गए है अहलकारों का तास्सुर इस बारे मे परेशान गुन था । और उन्होने कहा के वो बागियों की स्टेट मेन्ट के बारे मे फिक्र कर रहे हैं नामा निगारों ने बताया के हमेशा अमन की बातचीत के बात खून बहा हुआ हैं जिसमे दोनो पार्टियां एक दुसरे को लड़ाई शुरु करने का इल्ज़ाम देती रही हैं ।<pause dur="2"></pause>पाकिस्तान के शहर कराची मे पुलिस का कहना है के मेसदोनिया के आॅनरी काॅनसिल के दफ्तर मे एक बाॅम्ब के धमाके मे तीन लोग मारे गए है पुलिस ने बताया के ये लाशें मलबे तले डबी हुई थी और मरने वाले सभी पाकिस्तानी थे और हो सकता है के उन्हे धमाके से पहले बांध कर मार भी दिया गया हो । डाक्टरों ने बताया के उनमे से दो की शहरत कटी हुई थी । अभी तक ये पता नही चल सका के इस हमले के पीछे किस का हाथ था ।<pause dur="2"></pause>इंडिया के ज़ेरे इंतज़ाम कश्मीर मे पुलिस का कहना है जो शुबा है के मिलिटेन्ट्स की तरफ से किया गया था इंडिया के एक नुमाया सियासी कारकुन गुलाम मोह्युद्दीन लोन मारे गए हैं । ये हादसा कुफवारा की डिस्ट्रिक मे पैश आया था जिस मे मिस्टर लोन को उनके घर के बाहर गोली मार कर हलाक कर दिया गया था याद रहे के वो स्टेट के पुराने वज़ीरे कानुन मुशताक एहमद लोन के भाई थे और उन्हे भी इस साल सितम्बर मे होने वाली इलेक्शन की एक रैली मे मिलिटेन्ट्स ने मार डाला था । ये खबरें आप बी बी सी एशियन नेटवर्क से आयशा एहमद की ज़बानी सुन रहे थे ।<pause dur="2"></pause>युनाईटेड स्टेट्स के प्रेसीडेंट जार्ज बुश ने अफ्गानिस्तान पर अगले चार सालों मे तीन बिलियन डालर्स खर्च करने के एक बिल पर दस्तखत किये हैं । वाईट हाऊस ने बताया की ये रक्म मुल्क की मिलिटरी और दुसरी ईकोनोमि को बहाल करने के काम आएगी । युनाईटेड नेशन की सिक्युरिटी काॅनसिल ने ईराक के लिए तेल के बदले खाने के प्रोग्राम को अगले छः महिनों तक बड़ा दिया है और बग़दाद की ज़रुरत वाली चीज़ो की दरामत के बारे मे भी सोच ने का वादा किया हैं याद रहे के इस प्रोग्राम को हर छः महीने के बाद रिन्यु किया जाता हैं लेकिन वाशिंगटन इस बात पर ज़ोर दे रहा था के छः महीने के इस अरसे को कम कर देना चाहिए ।<pause dur="2"></pause>इंडोनेशिया मे बालीके नाईट कल्ब पर हमले की तहकीक करने वाली पुलिस का कहना है की इस सिलसिले मे इसी हफ्ते गिरफ्तार किये जाने वाले आदमी ने उस हमले मे शामिल होने का इलज़ाम कुबुल कर लिया है उनका कहना है के मुखलास नामी इस मशकुक आदमी ने ये मान लिया है की वो इस हमले के सिलसिले मे की जाने वाली मिटिंग्स मे शामिल हुआ था । पुलिस को शुबा है की मुखलास को हाल ही मे इसलामी तनज़ीम जामिया इसलामीया का रिजनल चीफ मुकर्रर किया गया था । याद रहे के इस तनज़ीम के बारे मे युनाईटेड स्टेट्स का कहना है की इस का तालुक अलकायदा की तनज़ीम के साथ  है ।<pause dur="2"></pause>श्रीलंका की हुकुमत और तमिल टायगर्स के बागियों के नुमाईंदो का कहना है के उन्होने अपनी ज़िम्मेदारियों और इखतेयारात को बाँटने के सिलसिले मे एक समझोता कर लिया है ।<pause dur="2"></pause>इंडिया की हुकुमत ने इंडिया और पाकिस्तान के ज़ेरे इंतज़ाम कश्मीर को अलाहिदा करने वाली लाईन आॅफ कन्ट्रोल के करीब कारगील के पहाड़ो पर दुसरे मुल्को के एक्सपीडिशन पर लगाई जाने वाली पाबंदियों को हटा दिया हैं याद रहे के ये पाबंदी पिछले साल उस वक्त लगाई गई थी जब दोनो मुल्कों की फौजें उस ईलाके के बारे मे लड़ रही थी जो इंडिया के कहने के मुताबिक उन का था ।<pause dur="2"></pause>युरोपियन सेन्ट्रल बैंक के बारे मे कहा जा रहा है के वो कुछ ही देर मे इंटरेस्ट रेट मे कमी करने का एलान करने वाले है जिससे इकोनोमी के डोलने का खतरा पैदा हो गया है । युरो का इस्तमाल करने वाले बहुत से मुमालिक को बड़ती हुई बेरोज़गारी का सामना करना पड़ रहा हैं । और युरोपियन कमीशन ने खबर दार किया है के इस सिलसिले मे बारह मुमालिक के बारे मे ख्याल है के अगले साल के शुरु मे उनकी इकोनोमी पर असर पड़ने वाला हैं । और अब आखिर मे मौसम आज मुल्क के मरकज़ी और मशरिकी हिस्सों मे बादल छाए रहेंगे और कभी कभी बारिश के छिंटे भी पड़ सकते हैं लेकिन मुल्क के मग़रिबी हिस्सों मे आम तौर पर खुश्क मौसम खुश्क और खुश गवार रहेगा । इसी के साथ बी बी सी एशियन नेटवर्क से खबरें खत्म हुई ।</u>

<event desc="announcement clip" dur="approximately 13 seconds"></event>

<u id="19" who="HF001">चार बजने मे बाईस मिनट बाकी है ए एम रेडियो और डिजिटल सेटेलाईट चेनल आठ सौ उनहत्तर पर ये बी बी सी एशियन नेटवर्क हैं । पूरे चार बजे तक नविंदर जीत भौगल नम्बर <foreign lang="eng">08459 440 445</foreign> और ये फेक्स मेसेज कहता हैं <foreign lang="eng">pyari Navinder how are you eid mubarak to you and all the BBC staff your silent listener and fans</foreign> बहुत बहुत शुक्रिया ।</u>

<event desc="song" dur="approximately 5 minutes"></event>

<u id="20" who="HF001">वुल्वाहेम्प्टन से शिव वीर जी आप ने याद फरमाया है तमामा सुनने वालों को ईद मुबारक कह रहे हैं प्रोग्राम इंजोय कर रहे है वीर जी बहुत बहुत शुक्रिया । ननील से इसमाईल जी आप ने याद फरमाया ईद की तमाम सुनने वालों को मुबारक बाद दे रहे हैं । बरमिंघम मे मुकेश जी आप ने अपने तमाम दोस्तो को ईद मुबारक कहा हैं और <foreign lang="eng">maven athony in peterbra</foreign> आप तमाम सुनने वालों को ईद मुबारक कह रहे हैं । ये फेक्स मेसेज कहते हैं <foreign lang="eng">hello this is simran from lester i would like to wish my friend rosemin a very happy eid mubarak</foreign> बहुत बहुत शुक्रिया सिमरन जी फेक्स मेसेज के लिए और नेनीटन मे हम मुबारक बाद दे रहे हैं अब्बास भाई और ज़ुबेदा बहन को जिन के आठ साल के बेटे अकील ने कुरान शरीफ माशा अल्लाह पूरा किया और तमाम रोज़े रखे यानी के पूरे के पूरे रोज़े रखे अब्बास भाई आप को और ज़ुबेदा जी अकील को और बहन ज़हेरा को बहुत बहुत मुबारक देते हैं ।</u>

<event desc="song" dur="approximately 4 minutes"></event>

<u id="21" who="HF001">मज़ाक कर रहे है हैं आप । ये कोई इतना आसान काम है और ये टेक्स्ट मेसेज कह रहा हैं । <foreign lang="eng">hi navinder assalm walaikum walaikum assalam we are listening to your excellent show on the way to my anuts in birmingham i would like to wish every one eid mubarak from Sabina and a family</foreign> बहुत बहुत शुक्रिया समीना जी आप का आप को भी ईद मुबारक <foreign lang="eng">hope you drive carefully have an excellent evening </foreign><event desc="music" dur="approximately 3 seconds"></event>चार बजने मे बारह मिनट बाकी है चार बजे की खबरों के बाद ड्राईव समीना अली खान के साथ ।<event desc="music" dur="approximately 3 seconds"></event>आप के पास सिर्फ सात मिनट और हैं । <foreign lang="eng">08459 440 445 and for your fax messages 07786 202 001</foreign><unclear cause="passing track">at only thing me</unclear><foreign lang="eng">when is raining or shining not on cloudy day</foreign> अरे वाह वाह ।</u>

<event desc="song" dur="approximately 6 minutes"></event>

<u id="22" who="HF001">और ये ई मेल नीरु नारंग चौहान की और से कहते हैं <foreign lang="eng">hi navinder let just you know that we really enjoy your programme very much keep up the good way thank you for sending me that e-mail really appreciate it</foreign> और ये फेक्स मेसेज कहता है <foreign lang="eng">tomorrow is my birthday i am sirf  21 and i am telling sirf me wish you a very happy birthday tomorrow and sirf meaners</foreign><event desc="music" dur="approximately 5 seconds"></event>और ये फेक्स मेसेज अभी अभी और आया है कहते हैं अस्सलाम वालेकुम नविंदर <foreign lang="eng">would you like to say eid greetings to my sister Nargis also a Moiz and Nasrin and all Kapasi family with love from Munira and Suleman ji in Lester</foreign> बहुत बहुत शुक्रिया आप का फेक्स मेसेजस के लिए इन सब को पैगाम मिल गया होगा । इसी के साथ मे आप से ईज़ाजत चाहुंगी शुक्रिया अदा करु बिंदर साहेबा का जिन्होने आप के पैगाम मुझ तक पहुंचाए । प्रोड्कशन टीम जिन्होने प्रोग्राम प्रोड्युस किया । और दिल की गहराई से मे शुक्रिया अदा करु आप का जिन्होने दो घंटे मेरा साथ दिया अल्लाह आप को धेर सारी खुशियां दे ये हमारी दिली दुआ हैं । अगर परवर दिगार ने चाहा तो कल दोपहर दो बजे आप से फिर मुलाकात होगी तब तक आप दुसरों का ख्याल रखें अल्लाह आप का रखेगा । मेरे ओर से यही के अल्लाह आप का निगेहबान हो <foreign lang="eng">good bye</foreign> और खुदा हाफिज़ रब राखा ते सत्यश्रीअकाल आप जो ने जय श्री कृष्णा ।<foreign lang="eng">have a wonderful evening</foreign></u>

<event desc="song" dur="approximately 5 minutes"></event>

<event desc="announcement clip" dur="approximately 8 seconds"></event>

<event desc="English news" dur="approximately 2 minutes"></event>

</body>
</text>
</cesDoc>