<cesDoc id="hin-w-literature-essay-lote11" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-literature-essay-lote11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-27</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Hindi Men Nigamdhara</h.title>
<h.author>Dinkar R.S</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - National Pub (Sahityamukhi) - New delhi</publisher>
<pubDate>1987</pubDate>
</imprint>
<idno type="CIIL code">lote11</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 130-135.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-27</date></creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;2200&gt;&lt;Dinkar R.,hindi Men Nigamdhara Es&gt;&lt;Girish Nath Jha&gt;</p>

<p>
&lt;+ ्ऎ भळ ॊो।े ःो ळ आॊ।ॉ ्ण ्ऎ।ऍ। धँ्।ॉ धँआ। ईऍ।आ। ् ँआ। ईऍ।आ। धँ्।ॉ
ळॊे सआ ऱऍ ऎोऍ ळ। ॉऎा। ।ँ। ् ळणँ ऋ।ऍँ, ो।एः, सआ ऱऍ ऎोऍ ॉ ऋ ईऍ।सआ
् ळऎ-ॉ-ळऎ ए् ँ ॉऋ ॉॊळ।ऍ ळऍँ ्ण ळ ऋ।ऍँ ळ। ईऍ।सआँऎ वऍणं ॊः ् ॉ।ऍ
ऎ।आॊँ। ळ। ईऍ।सआँऎ वऍणं ्
्ऎ।ऍ ःो ळ भळ ईऍऎऴ ॊोौँ। ए् ऋ ् ळ ए्।ण ऊॉआ ॊ।े ेव आ।आ। अऍऎण ळ
ऎ।आँ ्ण, आ।आ। ईऍळ।ऍ ळ ऋ।ौ।भण ऊेँ ्ण; ँं। ऴ।आ-ई।आ, ऍ्आ-ॉ्आ ऱऍ ऍँ-ऍॊ।
ऎण ऋ ऩआळ ऍूसए।ण ॊऋआ़आ ्ण ऋ।ऍँ ऎण धॉे।ऎ-अऍऎ ो।एः ऩॉ ॉऎए ई्णस। ं।,
ऊ ्ऍँ ऎ्ऎऎः अऍ।अ।ऎ ईऍ ऎूः ं; ऱऍ धणवेणू ळ ेव ऊ ळऍॉँ।आ ्भ,ऩॉॉ
ळन ः ॉ ॊऍौ ईूऍॊ नॉ।धएँ ळ। ईऍॊो ऋ।ऍँ ऎण ् सळ। ं। ऍंॉँऍ-अऍऎ ळ
ऎ।आआ ॊ।े ई।ऍॉ ेव नऍ।आ ॉ ऋ।वळऍ ए्।ण 936 न.ऎण दए ं ऱऍ ँऊ ॉ ॊ दआणःईूऍॊळ
धॉ ःो ळ ् थईआ। ःो ऎ।आळऍ ऍ्ँ दए ्ण ए् आ्ण, ऊेळ ्णःू-अऍऎ ळ ू।ेण
ळ ॉऎ।आ, ळह ऱऍ ऋ अऍऎ ्ण,  ्णःँॊ ॉ ॉॊँणँऍ थॉँँॊ ऍऴँ ्ण, ळणँ
आळ थआए।एएण ळ। ःो ए् ऋ।ऍँ ःो ्
ए् ळ।ऍ ् ळ ऋ।ऍँ ही ईऎ।आ ईऍ ॉ।ऍ ॉणॉ।ऍ ळ। आऎूआ। ईो ळऍँ। ्; ऱऍ सूणळ
आ।आ। अऍऎण ऱऍ आ।आ। ऋ।ौ।रण ळ। ःो ्ळऍ ऋ ऋ।ऍँ भळ ःो ऍ्। ्, थँभॊ  ेव
ॉऎवऍ ॊो़ॊ ळ भळँ। ळ ेभ ईऍए।ॉ ळऍ ऍ् ्ण, ऩआ्ण ऋ।ऍँ ळ ःऴळऍ ए् दो।
ऊणअँ ् ळ ळॉ-आ-ळॉ ःआ ॉ।ऍ। ॉणॉ।ऍ भळ ्ळऍ ऍ्व। ँं। ःआए। ऎण  थआळ
ऋ।ौ।भण, थआळ अऍऎ ऱऍ थआळ ऎँॊ।ः ईऍसेँ ्ण, ॊ ॉणॉ।ऍ ळ भळ ्आ ॉ आ्ण ऍळ
ॉळणव ॉ।ऍ ॊऋआ़आँ।रण ळ ळ।एऎ ऍऴळऍ ऋ।ऍँ आ ॉ भळँ। ळ ऩईेऊअ ळ ्,
ॊ् भळँ। ॉणॉ।ऍ ळ। अएए ् ॊऋआ़आँ।रण ळ ऊऍःॉँ ःऊ।ळऍ ए।णँऍळ भळँ। ईः।
ळऍआ ळ। ेऋ ँ।आ।ो।् ऍ।आँ ळ ्ँ। ् ऱऍ ँ।आ।ो।् ऍ।आँ ळॊे ए।णँऍळ
मळए ् ्।ॉे ळऍ ॉळँ ् ळणँ ऋ।ऍँ ळ भळँ। ए।णँऍळ आ्ण, ईऍ।ळ ्; ॊ्
पईऍ  ॉ े।ः आ्ण वए ्, ऋ।ऍँ ळ ॉ।णॉळँळ सँआ। ळ ऋँऍ ॉ ऩँईआ़आ ्न
् धॉेभ ॉऎए-ॉऎए ईऍ ऍ।आँळ ःौी ॉ ऴणूँ ् ।आ ईऍ ऋ, ॉ।णॉळँळ
ःौी ॉ ए् ॉ।ऍ। ःो भळ ऍ्। ्
ए् भळँ। ऋ।ऍँ ळ ोऋ। ् धॉ भळँ। ळ ळ।ऍ वे।ऎ ळ ःआण ऎण ऋ ॊ्, ळॉ-आ-
ळॉ थऍं ऎण, ईूआए ऎ।आ। ।ँ। ं। ऱऍ ऩॉ भळँ। ळ ळ।ऍ ॉॊँणँऍँ। ळ ऊ।ः
ॉ ऋ।ऍँ ॉ।ऍ ःआए। ळ ऍ्ॉए।ँऎळ दो। ळ। ळणःऍ ऊआ। ्द ् ऊ ऍ।ौीऍएँ।
ळ े्ऍ एूऍई ऎण ऩु, एूऍई ऴणू-ऴणू ्ळऍ थआळ सळेण ऎण ऊऴऍ वए।, ळएणळ
ए सळे अऍऎ ॉ भळ ्आ ईऍ ऋ ऋ।ौ।रण ळ ऋआ़आँ। ळ ळ।ऍ ऊणी ्भ ं ळणँ
ऋ।ऍँ ऎण ऊ ऍ।ौीऍएँ। ळ े्ऍ ऩु, ऋ।ऍँ ळ ॊऋआ़आ ऋ।व ईऍॉईऍ ऱऍ ऋ ॉऎई
द वए ॉ ऍ।ौीऍएँ। आ एूऍई ळ ीळूण-ीळूण ऎण ऊ।णी ःए।, ऩॉ
ऍ।ौीऍएँ। आ ्ऎ।ऍ भळँ। ळ ऱऍ ऋ ऎऊूँ ऊआ। ःए। ए् ळन दळॉऎळ शीआ। आ्ण
्ण ए् ऋ।ऍँ ळ ॉ।णॉळँळ ईऍॊँँ ळ। ईऍ।ऎ ् ए् ्ऎ।ऍ ऋूवे ळ ऎ्ऎ।
्; ए् ्ऎ।ऍ धँ्।ॉ ळ ःआ ् ्ऎ।ऍ। ।ँए ःऍोआ भळँ। ळ। ःऍोआ ् ्ऎ।ऍ।
।ँए ःऍोआ श। आ्ण, ईऍऎ ॉऴ।ँ। ्; नऍौए। ळ आ्ण, ऩँॉऍव ळ ऩँँआ।
ःँ। ् एूळऍॉीळ ळ।णवऍॉ ऱऍ ॉ।ऍॊऋऎ ऊःअ-ॉऎ़ऎेआ आ ए्।ण भळ ् ॉऎए
ऎण थईआ-थईआ अ।ऍऎळ ॉऎ।ऍ् ळए; ऱऍ धॉ ःो ळ आॊ।ॉ ऩआ ॉऎऎेआण ऎण धॉ
ऋ।ॊ ॉ ॉऎऎेँ ्भ, ऎ।आ ए ऩआळ थईआ ् ॉऎ।ऍ् ्ण ए् ऋ।ऍँ-अऍऎ ्
ए् ऋ।ऍँॊ।ॉएण ळ ऎ्ऎ। ् ॉॊ।ऎ ॊॊळ।आणः आ ळ्। ं। ळ ए् ळ।उ आ्ण ्
ळ थआए अऍऎण ळ ्ऎ ऊऍः।ोँ ळऍण ऩसँ ऊ।ँ ए् ् ळ ॉऋ अऍऎण ळ ्ऎ थईआ।
् अऍऎ ॉऎाण
ऍूृएण ळ। ॉऎऍंळ ऱऍ ऎ।आॊए भळँ। ळ। ऊ।अळ ्आ ळ ळ।ऍ ईहे ॉ ॊऍौण ॉ
अऍऎ, ईऍ।ए: आणःँ थॊॉं। ऎण ईू। ऍ्। ् ळणँ थऊ दॉ।ऍ मॉ ःऴँ ्ण, ऎ।आ
अऍऎ ळ आणः। ळ। एव ॉऎ।ईँ ळ ई।ॉ ् अऍऎ ळ थॊ्ेआ। धॉ ळ।ऍ ऋ ्न ळ
ॊि।आ ळ ऊृँ ळ ॉ।ं-ॉ।ं ऎआौए ळ। ए् ऋ।ॊ ऋ ऍ ईळूआ ेव। ळ ॉौी
भळ एणँऍ ् ऱऍ नो़ॊऍ ऩॉळ। धणआएऍ ् ऱऍ ॉ ्ऎ एणँऍ ळ ईऍ-ईऍ ळ।
्।े वँ ळ उ।ऍऎूेण ॉ ॉऎा ॉळँ ्ण, ऩॉ ईऍळ।ऍ ॉौी ळ ्ऍ भळ शीआ। वँ
ळ उ।ऍऎूेण ॉ ॉऎा ॉळँ ्ण, ऩॉ ईऍळ।ऍ ॉौी ळ ्ऍ भळ शीआ। वँ ळ
उ।ऍऎूेण ॉ ॉऎा । ॉळँ ् आएूीआ ॉॊएण अ।ऍऎळ ऎआौए ं; ळणँ ॉ
ॊि।आ ळ ऩआ्णआ आणॊ ू।े, ऩॉआ अऍऎ ळ ऎूे ळ ्े। ःए। ॊि।आ
ऩआ़आॉॊण ॉः ळ थणँ ँळ ँआ। ऩःअँ ं।, ऩँआ। ऩःअ ॊ् द आ्ण ् ऩआ़आॉॊण
ॉः ँळ ॊि।आ थ्णळ।ऍ ऎण ं।, ळएणळ ऩॉ ॉऎए ँळ ऩॉळ ।आळ।ऍ ंू ं ऊॉॊण
ॉः ळ। ॊि।आ ळह ॊआऎऍ ् वए। ्, ळएणळ थऊ ॊ् ऩॉ व्ऍ।न ऎण ई्णस वए। ्,
्।ण धॉ ऊ।ँ ळ आोसँ शौ। ळुआ ् ऍ् ् ळ ॉणॉ।ऍ ऎीऍ ॉ ऊआ। ् ए। ॉईऍी
ॉ, ँऍणवण ॉ आऍऎँ ् ए। ळण ॉ, थंॊ। ऩॉळ ईऍळऍए। ो।ऍऍळ ् ए। ऎ।आॉळ
द ळ ऋँळ ळ। ईऍॊ।्, आोसँ ऍूई ॉ, ऎ।आॉळँ। ळ रऍ ् ऋँळ थऊ ऩॉ
पणस।न ईऍ ई्णस वए ्, ्।ण ॉ ःऍोआ ळ। ॉऎःऍ ॉईौी ःऴ।ए ईूँ। ् आएूीआ
आ ॉ ःआए। ळ ळेईआ। ळ ं, ॊ् ळॊएण, ॉणँण ऱऍ ऍ्ॉएॊ।ःएण ळ ऊ्ँ
थआळूे आ्ण ईूँ ं आएूीआ ळ। ॊि।आ मीऎ ळ अऍ।ँे ईऍ थऊ ऋ ऩईएव, ुॉ
ऱऍ ॉँए ् ळणँ मीऎ ळ ईऍ धेळीऍआ ऱऍ ईऍीआ ळ अऍ।ँे ईऍ आएूीआ ळ
ॉःअ।णँ थएंौी ॉ।ऊँ ् ऍ् ्ण; ऱऍ अऍ-अऍ ॉणॉ।ऍ ळ  ळेईआ। ईऍळी ्
ऍ् ्, ॊ् मॉ ्, ॉऎण वँि ऱऍ ऍ्ॉएॊ।ः, ःआण ॉ।ं ऍ् ॉळँ ्ण ॊि।आ
ळ अळळ ॉ अऍऎ ळ दआौु।आळ ऋ।व (ऍसथे) ःॊ।ऍ ळ ईे।ॉीऍ ळ ॉऎ।आ ाऍळऍ
आस वऍ वए ळणँ अऍऎ ळ। ॉ।ऍँँॊ थआौु।आ आ्ण, थअए।ँऎ ्; ऱऍ ए् थअए।ँऎ
द ऋ ळ।एऎ ् ऱऍ दव ऋ ळ।एऎ ऍ्व। ॊआऊ।  आ ऊ्ँ ुळ ळ्। ् ळ  ॉऋएँ।
दव दआॊ।े ्, ऩॉऎण ॊि।आ ऱऍ थअए।ँऎ, ःआण ळ।एऎ ऍ्णव ऩॉ ॉऋएँ। ऎण
ळे। ळ। ऋ ॉं।आ पणस। ऍ्व।, ळएणळ ॊि।आ ळ। ईऍसए थअए।ँऎ ॉ ऱऍ थअए।ँऎ
ळ। ईऍसए ॊि।आ ॉ ळॊे ळे।भण ् ळऍ। ॉळँ ्
अऍऎ, थऍं ऱऍ ळ।ऎ ळ ँऍॊऍव ळ ळेईआ। ईऍ।सआण ळ ऋऍ।णँ आ्ण ं ए्
ँऍॊऍव ँऍळ।े-ॉँए ँँँॊ ् ॊ् ई्े ऋ ं।, द ऋ ् ऱऍ दव ऋ ळ।एऎ ऍ्व।
'दः ऋ ॉस, व।ः ऋ ॉस, ् ऋ ॉस ऱऍ ्ॉ ऋ ॉस' वऍू आ।आळ आ थळ।े
ळ ऊ।ऍ ए्  थेळळ ऩळँ ळ् ं, ॊ् धॉ ँऍॊऍव ईऍ ऋ े।वू ्ँ ् ॊॉँ
आ्ण ऊःेँ, ळॊे आ।ऎ ऊःेँ। ् वे।ऊ ळ स।् ॉ ळॉ आ।ऎ ॉ ईळ।ऍभ, ॊ्
्ऎो। वे।ऊ ् ऍ्व। आए एव ळ ोऊः।ॊे ऎण ळ।ऎ ऍॉ-ॉ।्ँए ऱऍ ळे।रण
ळ। ईऍए।ए ्, थऍं ॉ ऊअ ॉऎॉँ ि।आ-ॉ।्ँए ऱऍ ॊि।आण ळ। ्ँ। ् ँं। अऍऎ
थअए।ँऎ ळ। ईऍए।ए ्
ॉऋ अऍऎण ळ आवऎ ऱऍ दवऎ, ए ः ईळौ ्ँ ्ण आवऎ ॊ् वणऋऍ ऊणः ्, ्।ण
ॉ ॉऋ अऍऎण ळ ऩँईँँ ्ँ ् दवऎ थआौु।आण ळ ।े ्ण,  ईूआ ळ
ॊअएण ऱऍ ईःअँएण ळ। ॊअ।आ ळऍँ ्ण आवऎ ऎ।आॊए ऎआ ळ ॊ् थॊॉं। ्, ऊ
ऎआौए ए् ।आआ ळ ऩँळणुँ ्ँ। ् ळ आऎ ळ ई्े ऎण ळ्।ण ं। ऱऍ ऎऍआ ळ
ऊ।ः ऎण ळ्।ण ।पणव।? ए् ॉौी दई-ॉ-दई ऩहेळऍ ्ऎ।ऍ ॉऎळौ द वए ् थंॊ।
धॉळ ऍसआ। ळ ईह ळॉ थःोए ोळँ ळ। ऋ ्।ं ् ?  ळह ्ऎ ःऴँ ्ण, ॉौी
ॊ्ण ँळ ॉऎ।ईँ ् थंॊ। ऩॉळ। ईऍॉ।ऍ धणःऍएण ळ वँ ॉ दव ऋ ईूँ। ् ? आवऎ
अऍऎ ळ आऎॉं।आ ळ ळ्ँ ्ण आवऎ ॊ् व् ्, ्।ण ॉऋ अऍऎ भळ ् अऍऎ ्ण,
ॉ थआळ आ।ऎण ॉ थऋ्ँ ्आ ईऍ ऋ ईऍऎो़ॊऍ भळ ् ् आवऎ ऎूे ्, दवऎ
ो।ऴ।भण ्ण अऍऎ भळ ्, ळॊे ऩॉळ ॉ।अआ ळ ईःअँए।ण थेव-थेव ् वए ्ण थॉे
अऍऎ आवऎ ्, दवऎ ऩॉळ। पईऍ ु।ी ् आवऎ ॉऋ ऎआौएण ळ भळ ऊणः ईऍ भळँऍ
ळऍँ। ्; दवऎ अऍऎ ळ ऊ।्ए।स।ऍण ईऍ ऍ ःळऍ ऎआौए-ऎआौए ळ ईऍॉईऍ ऋआ़आ ळऍ
ःँ ्ण एः ऎआौए थईआ ळ आवऎ ॉ ऊ।णअ ऍ्, ँ दवऎ ऩॉळ ऊॉ ऎण ऍ्ँ ्ण
दवऎ ळॊे ॉ।अआ ऎ।ँऍ ्ण ऎआौए ळ। ऎऴए अएए आवऎ ळ ऩई।ॉआ। ् ळणँ ऊ ऋ
दःऎ आवऎ ॉ हीळळऍ दवऎण ळ थईआ। दऍ।अए ऊआ। ेँ। ्, अऍऎ ळ ीणी ोऍू ्
।ँ ्ण ॊःऍ् ेव ईऍ्।ऍ आवऎ ईऍ आ्ण, दवऎण ईऍ ळऍँ ्ण ळऍ।णँए।ण आवऎ
ळ ऴे।उ आ्ण दवऎण ळ ॊऍूःअ ्द ळऍँ ्ण
थईआ ःो ऎण अऍऎ ळ ळौँऍ ऎण ॉऊॉ ऊू ळऍ।णँ वँऎ ऊःअ आ ळ ं ळणँ
ऩआळ। ऋ दळऍो आवऎ आ्ण, दवऎ ळ ॊऍूःअ ं। ए् ऊ।ँ ऱऍ ् ळ धॉ ॊःऍ्
अऍऎ आ ऋ दव सेळऍ थवँ दवऎण ळ ॉौी ळऍ ू।े; ऱऍ ॉ ऊःअ ॉ ईूऍॊ ळ
्णःँॊ ऎण आवऎ व ऱऍ दवऎ ईऍअ।आ ् वए ं, ऩॉ ईऍळ।ऍ ऊःअ ऎँ ळ ऋ
्।ॉ-ळ।े ऎण आवऎ ळ ऊःे दवऎण ळ ईऍअ।आँ। ् वए
ळणँ ऋवॊ।आ ऊःअ ळ। भळ थॊः।आ मॉ। ्, ॉळ अ।ऍ। ऎण ्ऎ द ऋ ऊ् ऍ् ्ण
ऊःअ ळ दॊऍऋ।ॊ ळ ईूऍॊ ऋ।ऍँ ळ ॉणॉळँ ईऍ।ए: भळ ं, ॉ ्ऎ ॊःळ ॊोौ
ळ ॉ।ं ।आँ ्ण 'ईऍ।ए:' ऎण धॉेभ ळ्ँ। ्ूण, ळएणळ ऎऍ। थआऎ।आ ् ळ
ऎ्।ॊऍ ऱऍ ऊःअ ळ। दॊऍऋ।ॊ ऊेळे दळॉऎळ शीआ। आ्ण ं ॊःळ अऍऎ ळ
ॊऍूःअ ऎ्।ॊऍ ऱऍ ऊःअ आ  ळऍ।णँ ळ, ऩॉळ ऊ ोऍऎ-ॉणॉळँ ऎण ऎूः
ं ऱऍ ए् ॉणॉळँ दऍएण ळ दवऎआ ळ ईूऍॊ ॉ ् धॉ ःो ऎण ॊऍँऎ।आ ऍ् ं
उऍ ऋ ए् ॉँए ् ळ ॊःळ अऍऎ ळ  सआँ ऊःअ ळ ्।ंण ईऍ।ईँ ्न,
ॊॉ सआँ ऱऍ ळॉ ळ ्।ंण ईऍ।ईँ आ्ण ्न ं
ऊःअ ळऍ।णँ ळ। भळ ऊू। ऋ।ऍ ईऍ।ऎ ए् ्द ळ ऩॉळ ऊ।ः ॉ ऋ।ऍँ ळ ॉणॉळँ
ः अ।ऍ।रण ऎण ॊऋऴँ ् वए भळ अ।ऍ। ॊ् ्, ॉळ। ऎूे ॊःण ऎण ऎ।आ। ।ँ। ्,
ॉळ ो।ॉँऍ-ईऍँ। ऎआ ऱऍ ए।िॊेळए ँं। ः।ऍोआळ ोणळऍ ्ण थो़ॊशौ ळ
हूळऍ ॉणॉळँ ळ ॉऋ ळॊ ँं। ्णः ळ ऎ्।ळॊ ॊःए।ईँ, ॉूऍः।ॉ ऱऍ ँेॉः।ॉ
धॉ अ।ऍ। ळ ळॊ ्ण दअआळ एव ऎण धॉ अ।ऍ। ळ ईऍँळ ेळऎ।आए ँेळ ऱऍ ऎ्।ऎआ।
ऎःआऎ्आ ऎ।ेॊए ्भ ्ण ऱऍ ःूॉऍ अ।ऍ। ॊ् ्,  ऊःअ ळ ळऎणूे ॉ आळेळऍ
आ।व।ऍआ ऱऍ ॊॉऊणअ दः ः।ऍोआळण, ॉऍ्ई।, वऍऴ दः ॉ।अळण ँं। ळऊऍः।ॉ,
वऍ आ।आळ, ः।ःू ःए।े, ऍऊ, ऎेूळः।ॉ, ईेीूः।ॉ दः ॉणँण ळ ॊ।एण ऎण ऊ्ँ ्न
दअआळ एव ऎण ऎ्।ँऎ। व।णअ ँळ ई्णस ्
धआ ः अ।ऍ।रण ऎण ॉ ई्े अ।ऍ। ॊऍ।ोऍऎ-अऍऎ ळ अ।ऍ। ्,  दवऎण ऎण ृे।न
ळ ईॉणः आ्ण ळऍँ दवऎ वऍस ःूॉऍ अ।ऍ। ऎण ऋ ऩँईआ़आ ्भ, ळणँ ए् अ।ऍ।
आवऎ-ईऍअ।आ ् ऱऍ ऩॉळ। ऩःःोए दवऎण ळ ॊेईँ ए। ळ।उ ळऎऍ ळऍळ ऎआौए-
ऎआौए ळ भळ ऊआ।आ। ् एण-एण ॉऎए ऊँँ। ।ँ। ्, ए ःआण अ।ऍ।भण ईऍॉईऍ
ळऍऊ दँ । ऍ् ्ण ई्े ऋ ऩआ्णआ भळ-ःूॉऍ ळ ईऍऋ।ॊँ ळए। ं। ऱऍ द ऋ ॊ
भळ-ःूॉऍ ळ ईऍऋ।ॊँ ळऍ ऍ् ्ण ःळौ ळ देॊ।ऍ ळॊएण ळ ईः, थअळ।णो ऎण,
आवऎ-अ।ऍ। ळ ् ईः ्ण धॉळ ॊईऍँ, दवऎ-ऎ।ऍव ळ ॊऍअ ्आ ईऍ ऋ ळऊऍः।ॉ
ळणु ऱऍ सणःआ अ।ऍ ळऍँ ं, ॉ ्ऎ दवऎ-ऎ।ऍव ळ थआऎःआ ळ। ःौी।णँ ळ्णव
दअआळ एव ऎण ईऍऎ्णॉ ऍ।ऎळौ मॉ ॉणँ ्भ ्ण, आळ ऋँऍ धआ ःआण ् अ।ऍ।रण
ळ। ॊेए ् वए। ं। ऩआ्णआ ॊः।णँ ळ ऋ ॉ।अआ। ळ ं ऱऍ ॊ ॉवई।ॉळ ऋ ं
ए् आ्ण, ईऍँएँ ऩआ्णआ ह: ऎ्आण ँळ ऎॉेऎ।आ ळ ॉऎ।आ ॊआ ऊँ।ळऍ धॉे।ऎ
ळ ॉ।अआ। ळ ं ऱऍ ळह ळ।े ँळ ॊ नॉ।न-अऍऎ ळ ऋ ॉ।अआ। ऎण ऍ् ं
ॉ एव ऎण ्ऎ  ऍ् ्ण, ॊ् दवऎ ॉ थअळ आवऎ ळ ऎ्ऎ। ॉ ॊए।ईँ ्
धॉेभ आॊएव ळ अ।ऍऎळ ईऍॊँँ ँेॉः।ॉ ळ ईळौ ऎण ळऎ, ऎ्।ँऎ। ळऊऍ ळ
रऍ ळह थअळ ईूँ ् अऍऎ ळ ऊ।्ऍ दस।ऍ आऍ।ःँ ् वए ्ण ळणँ थअए।ँऎ
ळ आौु। द ऋ थईआ। ळ।ऎ ळऍ ऍ् ् ॉईौी ् ळ ॊि।आ-ॉऎऎँ अऍऎ ळॊे
ॊः।णँ ् ् ॉळँ। ्,  दवऎण ळ ऊ।ँ आ्ण ळऍँ।, ॉळ। ॉ।ऍ। ऍ अऍऎ ळ आवऎ-
ईळौ ईऍ ् ऊ ऍॊणःऍआ।ं आ ए् ळ्। ं। ळ 'अऍऎ ळ ईळू ऍ्, अऍऎण ळ हू
ः,' ँऊ ऩआळ। अएए ए् ऊँ।आ। ं। ळ अऍऎ ळ। आवऎ-ईळौ ् एंौी ्, दवऎण ऎण
उणॉआ ॉ ऎआौए-ऎआौए ळ ऊस ॊऋः ऩँईआ़आ ्ँ। ् ऱऍ ूळीऍ ऍ।अ।ळौआ ॉ
अऍऎ ळ 'दँऎ। ळ। अऍऎ' (ऍेआ लउ ॉईऍी) ळ्ँ ्ण, ॊ् ऋ अऍऎ ळ आवऎ-ईळौ
ळ। ् भळ आए। आ।ऎ ् 'ॊः।णँ' ोऊः ॉणॉळँ ऋ।ौ। ळ। ्; थँभॊ ए् दोणळ। ऩसँ
् ् ळ थआए अऍऎण ळ ेव धॉ ोऊः ळ ॉॊळ।ऍ आ्ण ळऍण, ळएणळ दवऎण ळ भळ
ोळौ। ए् ऋ ् ळ ईू।-ईःअँ ऎण ळॊे थईआ ् अऍऎ-~ऋ।ौ। ळ। ॊएॊ्।ऍ ळऍआ।
स।्भ ळणँ 'ॊः।णँ' आ थऍंण ळ। ॊ।सळ ्, ऩआ थऍंण ळ थणवळ।ऍ ळए ऊआ।
ॉणॉ।ऍ ळ अऍऎ भळ आ्ण ्णव ॊः।णँ ऎआौए-ऎआौए ळ ऊस भळँॊ ॉं।ईँ ळऍँ।
् ॊः।णँ ऎआौएण ळ आ।आ। ो।ऴ।रण ॉ ऩँ।ऍळऍ ऎूे ऎण भळ ळऍँ। ् ॊः।णँ ळॉ
आए अऍऎ ळ। ॊअ।आ आ्ण ळऍँ। ॊ् ॉऋ अऍऎण ळ आवऎ-ईळौ ऎण भळ ॉऎ।आ ॊए।ईँ
् ॊः।णँ ्आ ईऍ ्णःू ई्े ळ थईळौ। ोऍौु ्णःू, ऎॉेऎ।आ ई्े ळ थईळौ।
थअळ ोऍौु ऎॉेऎ।आ ऱऍ नॉ।न ई्े ळ थईळौ। थअळ ोऍौु नॉ।न ऊआ ॉळँ। ्
ए् ॊः।णँ ळ ऎ्ऎ। ् ॊि।आळ एव ऎण अऍऎ-ॉ।अआ। ळ। भळऎ।ँऍ ऎ।ऍव ॊः।णँ
ळ। ऎ।ऍव ्
ॉ अऍऎ ळ। ि।आ ।आॊऍण ळ आ्ण, ळॊे ऎआौए ळ ्ँ। ्, ऩॉ ईऍळ।ऍ ऋ।ौ।
ऋ ळॊे ऎआौए-एआ ळ। व ् ।आॊऍ ऍूॉ ऎण ऍ्ण ए। ्णःॉँ।आ ऎण ऊे ऩआळ
भळ ् ्ँ ् ळणँ ऍूॉ ऎण ऊॉआॊ।े ऎआौए ॊ् ऋ।ौ। आ्ण ऊेँ,  ऋ।ौ।
्णःॉँ।आ ऎण ऍ्आॊ।े ेव ऊेँ ्ण ऎआौए ऋ ऊ ईो ं।, ऩॉळ ई।ॉ ऋ।ौ।
आ्ण ं ऩॉळ ऋ।ौ। ळ। दऍणऋ ँऊ ्द, ऊ ॊ् ईो-एआ ॉ थेव ्आ ेव। ऋ।ौ।
ॉऋएँ। ळ। ऎूे ् ऋ।ौ। ॉणॉळँ ळ थॉे ोळँ ् ऋ।ौ। सणँआ ळ। ेऊ।ॉ ्
ॉ आॉऎू् ऎण सणँआ ऩसस ळी ळ। ्ँ। ्, ऩॉळ ऋ।ौ। ऋ ऩँआ ् ईऍऴऍ ऱऍ ऎ्आ
् ।ँ ् ऱऍ  आॉऎू् थऋ ॉणॉळँ ळ ःो। ऎण ईहू। ्द ्, ऩॉळ ऋ।ौ। ऋ
ऩॉ ईऍ।ऎ ऎण ॉंूे ऱऍ ऋणू ्ँ ् थँभॊ ्ऎ।ऍ ऋ।ौ।रण ऎण सणँआ ळ ॉॊॉं
ईऍणईऍ। ळ। ॊळ।ॉ ्आ। स।्भ, ॉॉ ऋ।ऍँ थईआ ऩॉ ॉॊऍूई ळ ॉणॊ।ऍ ॉळ, ॉळ ळ।ऍ
ॊ् वँ-ऋऍ ऎण धँआ। ॊऴए।ँ ऍ्। ्
ऋ।ौ। ळन ऋ ऊऍ आ्ण ्ँ: ऊऍ थवऍ ्ँ ्ण, ँ ऩॉळ ऊेआॊ।े ेव ्ँ ्ण
ळणँ ऴः ळ। ॊौए ् ळ ॉॊऍ।ए ळ ऊ।ः ॉ ईऍ ्णः ईऍ  थआळ दऍई ेव।ए
। ऍ् ्ण, ऩआऎण ॉ भळ ळँॉँ दऍई ए् ऋ ् ळ ्णः उळँ ्णःरण ळ ऋ।ौ।
्, ्णःँॊ ळ थँऍळँ धॉ ःो ऎण  आ।आ। अऍऎ ्ण, ऩआळ ईऍँ ॊ् ऩः।ऍ आ्ण
् ँं। धॉ ऋ।ौ। ळ। ॉॊऍ ॉ।णईऍः।एळ ऱऍ ऩॉळ ःौी ॉणळऍ ् ोऊः स।् ए
आ्ण ऍ् ्ण, ळणँ ए थंॊ। धआॉ ऎेँ-ेँ दळौई ्णः ईऍ ः-भळ ऊ।ऍ पणस-
पणस व्ण ईऍ ऋ ेव।ए वए ्ण थॊोए ् धॉ ईऍळ।ऍ दळौई ळऍआ ॊ।े ेव ऋ।ौ।
ऱऍ ॉ।्ँए ळ थअएँ। आ्ण, ऍ।आँ ळ आऍणळो ऴे।ू ्ँ ्ण, थआएं। ॊ मॉ
ऊ।ँ आ्ण ऊेँ,  ॉँए ळ ऊेळे ॊईऍँ ऱऍ आऍ।अ।ऍ ् पईऍ ऎणआ ऋ।ऍँए
ॉणॉळँ ळ आ ः अ।ऍ।रण ळ। ऩेेऴ ळए। ् ऩआळ। ँआ। ईौी ईऍऎ। ्णः ऎण
ऎेँ। ्, ऩॉॉ थअळ ईौी ईऍऎ। ळॉ ऱऍ ऋ।ौ। ऎण ऩईेऊअ आ्ण ् ्णः
ळॊे ्णःँॊ ळ ् आ्ण, धॉे।ऎ ऱऍ ॉऴ-अऍऎ ळ ऋ ऋ।ौ। ् ्णः ळॊे ॊःळ
ए। ॊऍ।ोऍऎ-अऍऎ ळ ऍूृएण ळ। ईौी-ईौ आ्ण ळऍँ, ॊ् ऩआळ ॊऍूःअ ळऍ।णँ
ळ ऋ ोळौ। ःँ ् ॊ् ळॊे दवऎ-अऍऎ ँळ ् ॉऎँ आ्ण ्, ऊेळ आवऎ-अऍऎ
ळ। ऋ दऴए।आ ऩॉआ ऊू ् आऍऋएँ। ॉ ळए। ्
्णः ळ ईऍऎऴ ळॊएण ऎण वआँ ळॊे ऩआ्ण ळ आ्ण ्ँ,  आऎ ए। अऍऎ ॉ
्णःू ं, ऊेळ ऩॉळ ोऍौॉं ळॊएण ऎण ळँआ ् ळॊ मॉ ऋ ्भ ्ण,  अऍऎ
ॉ ॉऴ ए। ऎॉेऎ।आ ं थवऍ थईऍऎऴ ळॊएण ळ वआँ ळ ।ए, ँ ऩॉ ॉऍ ऎण ः-
स।ऍ आ।ऎ नॉ।धएण ळ ऋ द ।एणव थऎऍ ऴॉऍ ऱऍ ळऊऍः।ॉ, ।एॉ ऱऍ ऩॉऎ।आ,
ऍ्ऎ ऱऍ ऍॉऴ।आ, थआ,थ्ऎः ऱऍ ळऎ।े, ँ। ऱऍ ँ।आॉआ, आॊ। ऱऍ उळऍूःःआ, देऎ
ऱऍ ोऴ ँं। ऎऊ।ऍळ ऱऍ ऍॉेआ-ए ॉऊ-ळ-ॉऊ थँएणँ ऩसस ळी ळ ळॊ ्भ ्ण ऴ।ॉ ळऍ
ळऊऍ, ।एॉ, ऍ्ऎ, ऍॉऴ।आ ऱऍ ऍॉेआ आ ्णः ळ  थईऍऎँ ॉॊ। ळ, ॊ् ्णः-
ॉ।्ँए ळ धँ्।ॉ ऎण थँएणँ वऍॊईूऍ ॉं।आ ऍऴँ ् ऱऍ धआ ळॊएण ळ। ॉऎएळ
थअएएआ ळए ऊआ। ळन ऋ ॊएळँ ्णः-ॉ।्ँए ळ। ऎऍऎि आ्ण ऎ।आ। । ॉळँ।
धॉ ईऍळ।ऍ ॉऴ वऍूरण, ॊोौँ: वऍू आ।आळ ऱऍ वऍू वॊणःॉण् ळ ऍसआ।भण ्णः-~
ॉ।्ँए ळ। थऋआ़आ थणव ्ण ऱऍ ्णः ळ ईऍ।सआ ॉ।्ँए ळ। ॉणळेआ ऩआळ ॉऎ।ॊो
ळ ऊआ। थअूऍ। ॉऎा। ।ँ। ्
दणःेआ ळ ऍूई ऎण ॉ।णईऍः।एळ भळँ। ळ ऎ।णव धअऍ ्।े ऎण दळऍ ऊॉॊण ोँ।ऊः ऎण
ऩु ्; ळणँ धॉ भळँ। ळ दॊोएळँ। ऋ।ऍँॊ।ॉ दऍणऋ ॉ ् ऎ्ॉूॉ ळऍँ द ऍ्
ं ऱऍ ्णःू-ऎॉेऎ-ऎेआ ळ। ऎऴए ळौँऍ ्आ ळ ळ।ऍ ॉ।णईऍः।एळ भळँ। ईऍ
ँआ। थअळ ऍ ्णः-ऋ।ौ ळौँऍ ऎण ःए। वए। ऩँआ। ऍ ऱऍ ळ्ण ःए। वए। ए।
आ्ण, धॉळ। एंौी ईऍऎ। ऩईेऊअ आ्ण ् ळऊऍ ॉ।्ऊ आ ईणःऍ्ॊण ॉः ऎण ्णःू-
ऎॉेऎ-~ भळँ। ळ।  आ।ऍ। ःए।, ॊ् आ।ऍ। द ँळ ्णः ऎण आ:ोऊः आ्ण ्द ्
ऱऍ धआ स।ऍ-ई।णस ॉ ॊऍौण ळ ऊस ळन ॉ ळॊ ्णः ऎण मॉ ्भ, आ्णआ दणऴ
ऎूणःळऍ धॉ आ।ऍ ळ ः्ऍ।ए। ्
्णः ळ। ॉणँ-ॉ।्ँए,  दळ।ऍ ऎण ऎ्ःअ ळ ॉऎ।आ ॊो।े ्, धॉ भळँ। ळ। ॉ।्ँए
् थॉे ऎण ए् ॉ।्ँए ऩॉ ळऍ।णँ ळ। ॉ।्ँए ्, ॉळ। ईऍॊऍँआ ऊःअ आ ळए।
ं। ए् ॉ।्ँए दःऎ ळ पणस।न ळ ।णस ऩॉळ आऎ ॉ आ्ण, ळऍऎ ॉ ळऍँ। ् ए्
ॉ।्ँए अऍऎ ळ ऊ।्ए।स।ऍण ळ ृळॉे। ॉऎाँ। ् ऱऍ ऎआौए ळ अऍऎ ळ ऩॉळ सऍँऍ
ऎण ईऍँऊणऊँ ःऴआ। स।्ँ। ् ए् ॉ्ँए थॉईोएँ। ळ। ॊऍअ ऱऍ ।ँ- थ्णळ।ऍ
ळ। ळी़ीऍ ोँऍ ् ए् ॉ।्ँए ऊुळऍ ऴ।आ ॊ।ेण ळ। ऎ।ळ ऩू।ँ। ् ऱऍ धँ ऩॉळ
ळऍँ। ्, ॉळ ्।ंण ऎण ऎ्आँ ळ शीु ए। आो।आ ्ण ए् ॉ।्ँए ऩॉ ऎ।आॊ-ॉऎ।
ळ ऋूऎळ। ँए।ऍ ळऍँ। ्, ॉळ ळेईआ। व।णअ आ ळ ं-भळ मॉ। ॉऎ। ॉऎण ोौ
आ्ण ्, अआ ळ ईू। आ्ण ्, अऍऎ ळ। दूणऊऍ आ्ण ्, थॉईोएँ। आ्ण ् ऱऍ ॉ
ॉऎ। ऎण ॉँऍ ऱऍ ोूःऍ ःऊ।ळऍ आ्ण ऍऴ ।ँ ्ण, आ ळन ॊएळँ धॉेभ थ्णळ।ऍ
ळऍँ। ् ळ ऩॉळ। आऎ पणस ए। अआ ऴ।आः।आ ऎण ्द ् ्णः ळ ॉणँ-ॉ।्ँए ळ
ॉ।ऎ।ळ ॊोौँ। ए् ् ळ थवऍ व।णअ  ळ ळेईआ। ळ। ॉऎ। थॉँँॊ ऎण द ।ए ँ
ए् ॉ।्ँए ऩॉ ॉऎ। ळ ऍ।ऎ।ए ऊआ ॉळँ। ् थंॊ। थवऍ ॉऎ।ॊ।ः ेव अऍऎ ळ ॉॊळ।ऍ
ळऍळ थईआ ळेईआ। ळ ॉऎ। ळ ॉं।ईआ। ळऍ ॉळण, ँ ए् ॉ।्ँए ऩआळ। थॉे
ऎआउॉीण ऊआ ॉळँ। ्, ऊोऍँ ळ ॊ ॉणँौ ळ ऎ्ऎ। ळ ऋ ॉॊळ।ऍ ळऍ ॉळण
+&gt;</p>

<p></p></body></text></cesDoc>