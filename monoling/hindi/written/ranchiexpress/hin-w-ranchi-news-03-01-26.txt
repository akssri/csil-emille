<cesDoc id="hin-w-ranchi-news-03-01-26" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-ranchi-news-03-01-26.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Celia Worth and Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Internet version of the "Ranchi Express" newspaper (www.ranchiexpress.com), news stories collected on 03-01-26</h.title>
<h.author>Ranchi Express Group</h.author>
<imprint>
<pubPlace>Ranchi, Jharkhand, India</pubPlace>
<publisher>Ranchi Express Group</publisher>
<pubDate>03-01-26</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>नागपुरी कला संगम का वार्षिक महोत्सव सम्पन्न</p>

<p> </p>

<p>रांची : झारखंड उच्च न्यायालय के न्यायमूर्ति गुरूशरण शर्मा ने
नागरिकों से झारखंड की संस्कृति को बनाये रखने की
अपील की। उन्होंने कहा कि संस्कृति के बिना सृजन संभव
नहीं है और न ही अच्छे समाज की नींव पड़ सकती है। न्यायमूर्ति
शर्मा आज नागपुरी कला संगम के तत्वावधान में आयोजित
वार्षिक महोत्सव के उद्घाटन के बाद सभा को सम्बोधित
कर रहे थे। नागपुरी कला संगम के कार्यकलापों की
सराहना करते हुए उन्होंने झारखंड में ऐसी संस्थाओं को सक्रिय
रूप से एकजुट होकर झारखंड के विकास के लिए कार्य
करने का आह्वान किया।इस मौके पर न्यायमूर्ति लक्ष्मण उरांव ने
संगम पत्रिका का लोकार्पण किया। उन्होंने कहा कि
नागपुरी कला संगम झारखंड की संस्कृति के संवर्द्धन में
कई वर्षों से रहा है। उन्होंने कहा कि अपने आस्तित्व को
बचाये रखने के लिए साहित्यिक- सांस्कृतिक धरोहरों को
अक्षुण्ण बताये रखने की दिशा में प्रयास करने की जरूरत है। इस
मौके पर संस्था के अध्यक्ष सत्यदेव नारायण तिवारी ने संस्था
की आतिथियों का प्रतिवेदन प्रस्तुत किया। </p><p>
संगम सम्मान : इस महोत्सव में विभिन्न लोगों को उत्कृष्ट
सेवा के लिए १२ लोगों को संगम सम्मान प्रदान किया। सम्मान
के तहत प्रशस्ति पत्र एवं शॉल प्रदान किये गए। सम्मानित होने
वालों में रतन वर्मा (पत्रकारिता), डा.सुशांत कुमार सिन्हा
(नेत्र रोग विशेषज्ञ), ब्रह्मकुमारी निर्मला (अध्यात्म द्वारा
समाज सेवा), डा.करमा उरांव (शिक्षाविद्), डा.विनोद मणि
दिवाकर (साहित्य लेखन के क्षेत्र में), डा.शीन अख्तर (नाटक एवं
साहित्य), मेघनाथ ओहदार (लोक साहित्य एवं कला),
डा.सी.के.सिंह (हिन्दी कविता लेखन), नकुल महतो (कुशल
कृषक), चन्द्रमोहन कपूर (समाज सेवा)। </p><p>
सांस्कृतिक कार्यक्रम : उद्घाटन तथा सम्मान समारोह के
बाद सांस्कृतिक कार्यक्रम आयोजित किये गए। कार्यक्रम
में नागपुरी गीत, संगीत के कार्यक्रम लब्ध प्रतिष्ठित
कलाकारों द्वारा प्रस्तुत किये गए</p>

<p> </p>

<p>कर्मचारियों को सौ
फीसदी वेतन के लिए कुलाधिपति से बात होगा</p>

<p>रांची : रांची विश्वविद्यालय
के कुलपति डा. एस.एस. कुशवाहा ने कहा है कि विश्वविद्यालय
मुख्यालय के कर्मचारियों को सौ फीसदी वेतन भुगतान
हेतु वह शीघ्र ही कुलाधिपति से बात करेंगे। विश्वविद्यालय
मुख्यालय कर्मचारियों के प्रतिनिधियों से आज सौ
फीसदी वेतन भुगतान के मामले पर वार्ता के दौरान
उन्होंने इस बात का समर्थन किया कि मुख्यालय के
कर्मचारियों को सौ फीसदी वेतन भुगतान होना ही
चाहिए। कर्मचारियों के प्रतिनिधियों को भी अपनी बात
रखते हुए वार्ता के दौरान कहा कि कुलपति कुलसचिव
सहित अन्य आधिकारियों को सौ फीसदी वेतन भुगतान हो
रहा है तब फिर उनके साथ कार्य करने वाले कर्मचारियों
को ७५ फीसदी वेतन भुगतान करना कहां तक उचित है।
कुलपति डा. एस.एस. कुशवाहा ने आज न्यायालय की अवमानना
से संबंधित मामलों पर विचार करने के लिए विश्वविद्यालय
के आधिवक्ता और संबंधित आधिकारियों के साथ
बैठक की।</p>

<p> </p>

<p> </p>

<p>झारखंड में कड़े सुरक्षा प्रबंध</p>

<p>रांची : गणतंत्र दिवस की तैयारियों
के मद्देनजर झारखंड में सुरक्षा प्रबंध और सख्त करते हुए राजधानी
रांची सहित पूरे प्रदेश में कड़ी चौकसी बरती जा रही है। राज्य
पुलिस मुख्यालय के सूत्रों ने आज यहां बताया कि पुलिस
आधिकारियों को गणतंत्र दिवस के अवसर पर सतर्कता
बरतने का विशेष निर्देश दिया गया है। सूत्रों ने बताया कि
रांची सहित जमशेदपुर, हजारीबाग, धनबाद, बोकारो और
दुमका में खास तौर पर सतर्कता बरती जा रही है। सूत्रों ने
बताया कि प्रदेश के राज्यपाल एम.रामा जोयिस कल सुबह नौ
बजे स्थानीय मोरहाबादी मैदान में झण्डोत्तोलन करेंगे जबकि
मुख्यमंत्री बाबूलाल मरांडी कल सुबह दुमका में झण्डोत्तोलन
करेंगे। सूत्रों ने बताया कि राजधानी रांची और उप राजधानी
दुमका में कड़ी सुरक्षा व्यवस्था की गई है।</p>

<p> </p>

<p>तनिष्क शोरूम से ८७ हजार रु. के
जेवर की चोरी, वीडियो कैमरे से पता लगाने का प्रयास</p>

<p>रांची : जी.ई.एल. चर्च
काम्पलेक्स स्थित टापसेल मार्केटिंग प्रा. लि. कम्पनी के शो
रूम 'तनिस्क' से ८७ हजार रुपये के जेवर चोरी चले जाने की
रिपोर्ट लोअर बाजार थाने में दर्ज करायी गयी है।
कम्पनी में कार्यरत नार्थ आफिस पाड़ा डोरंडा निवासी
गुरविन्दर सिंह ने पुलिस को दी गयी सूचना में जानकारी दी
है कि २२ जनवरी की रात स्टॉक मिलान करने के क्रम में
जेवर के दो डब्बों (एक हाफ सेट वजन ४६.८६ ग्रा. मूल्य ३३ हजार रु.
तथा एक नेकलेस वजन ६५.२७ ग्रा., मूल्य ४५ हजार रु.) एवं एक इयर
रिंग (वजन १३.३५ ग्रा., मूल्य नौ हजार रु.) गायब पाया गया। शो रूम
में कार्यरत कर्मचारियों से स बाव पूछ-ताछ करने पर
पता चला कि सिर्फ छह-सात लगों को ही आज उक्त जेवर
दिखाया गया था। बाद में शो रूम में लगे विडीयो कैमरे के
कैसेट को आन करके देखा गया जिससे पता चलता है कि
शाम ५.१८ बजे दो युवतियां, एक पुरुष के साथ शो रूम में
प्रवेश की थीं। उन्होंने जेवर खरीदने के लिए देखने के क्रम में
उक्त गायब हुए जेवर को काफी देरी तक देखा। उनमें से
पुरुष की उम्र लगभग ४५-५० वर्ष, एक महिला जिसने
सलवार-शूट पहन रखा था की उम्र लगभग २५-३० वर्ष तथा
जिंस पैंट और टी-शर्ट पहनी हुई युवती की उम्र २०-२३
वर्ष की थी। सामान देखने के क्रम में उक्त महिला को
अपनी बैग में उक्त दोनों डब्बों को डालते हुए देखा गया है।</p>

<p> </p>

<p>मोरहाबादी मैदान में राज्यपाल झंडोत्तोलन करेंगे</p>

<p> </p>

<p>रांची : राजधानी रांची में
कल गणतंत्र दिवस के अवसर पर विभिन्न सरकारी संस्थाओं
कार्यालयों तथा प्रतिष्ठानों में झंडोत्तोलन परेड तथा अन्य कार्यक्रमों
का आयोजन किया गया है। इस अवसर पर मुख्य समारोह
मोरहाबादी मैदान में आयोजित किया गया है। समारोह में
झारखंड के राज्यपाल एम. रामा जोयिस पूर्वाह्न ९ बजे झंडोत्तोलन
करेंगे। वह इस अवसर पर संयुक्त परेड का निरीक्षण करेंगे। इस
समारोह में विभिन्न कार्यक्रमों पर झांकियां भी
निकाली जाएंगी। झारखंड विधानसभा परिसर में विधानसभा अध्यक्ष
इन्दर सिंह नामधारी राष्ट्रीय झंडा पूर्वाह्न ९.१५ बजे फहरायेंगे।
झारखंड राज्य अल्पसंख्यक आयोग के अध्यक्ष मोहम्मद कलाम खां
पूर्वाह्न ९ बजे डोरंडा स्थित मिस्तरी मोहल्ला कैसर हिन्द मदरसा
में तथा पूर्वाह्न १० बजे एच.इ.सी. सेक्टर-३ स्थित जौहर एकडमी
में झंडोत्तोलन करेंगे। राजधानी स्थित विभिन्न विद्यालयों
तथा शिक्षण संस्थाओं में ५४वें गणतंत्र दिवस समारोह मनाने की
तैयारी धूम-धाम से की जा रही है। इस अवसर पर कई विद्यालयों
में झंडोत्तोलन के साथ-साथ खेल-कूद प्रतियोगिताएं तथा
सांस्कृतिक कार्यक्रम का आयोजन किया गया है।</p>

<p> </p>






</body></text></cesDoc>