<cesDoc id="hin-w-ranchi-news-02-08-30" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-ranchi-news-02-08-30.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Celia Worth and Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Internet version of the "Ranchi Express" newspaper (www.ranchiexpress.com), news stories collected on 02-08-30</h.title>
<h.author>Ranchi Express Group</h.author>
<imprint>
<pubPlace>Ranchi, Jharkhand, India</pubPlace>
<publisher>Ranchi Express Group</publisher>
<pubDate>02-08-30</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>झारखंड विद्युत
विनियामक आयोग का गदन</p><p>
नयी
विद्युत दर निर्धारित करने का मार्ग प्रशस्त                        </p>

<p> </p>

<p>              </p><p>
रांची : झारखंड सरकार ने झारखंड
विद्युत विनियामक आयोग (झारखंड इलेक्ट्रीसिटी कमीशन)
का गन कर दिया है। ऊर्जा विभाग ने इस संबंध में
आधिसूचना जारी कर दी है। आयोग के गन के साथ ही झारखंड
में नयी विद्युत दर निर्धारित करने का मार्ग प्रशस्त हो
गया है। ऊर्जा मंत्री लालचंद महतो ने उक्त जानकारी दी
इलेक्ट्रीसिटी रेगुलेटरी कमीशन एक्ट १९९८ की धारा १७ एवं
धारा २२ की उपधारा (२) के तहत आयोग का गन किया गया है।
इस आयोग का प्रधान कार्यालय रांची में होगा। आयोग राज्य
के भीतर परिचालित सत्ताओं (इन्टिटीज) को विद्युत उत्पादन,
परिषण, वितरण एवं प्रदाय के लिए निवेश विनियमित
करेगा। आयोग राज्य में विद्युत उत्पादन पारेषण, वितरण और
प्रदाय से संबंधित मामलों में राज्य सरकार की सहायता करेगा
तथा सहयोग देगा। आयोग विद्युत पारेषण, थोक प्रदाय,
वितरण या प्रदाय के लिए अनुज्ञप्ति जारी करेगा। आयोग
अनुज्ञप्तियों में सम्मिलित की जाने वाली शर्तों का
निर्धारण करेगा। आयोग अनुज्ञप्तिधारियों तथा राज्य में
विद्युत उद्योग में लगे प्राधिकृत या अनुज्ञप्त अन्य व्यक्तियों
के कार्यों को विनियमित करेगा। इसके अलावा आयोग
उनके कार्यों की दक्षता, मितव्ययिता एवं साम्यपूर्ण रीति
में समवर्तित करेगा। आयोग राज्य में विद्युत उद्योग के लिए
मानक निर्धारित करेगा। आयोग राज्य में विद्युत उद्योग में
निजी क्षेत्र की प्रतिस्पर्धा बढ़ाने तथा उनके भाग लेने का
मार्ग प्रशस्त करेगा। आयोग सुरक्षा मानको को
आधिस्थापित तथा प्रवर्तित करेगा। आयोग राज्य विद्युत नीति
बनाने में राज्य सरकार की सहायता करेगा तथा सुझाव देगा।
इसके अलावा आयोग विद्युत के उत्पादन, पारेषण, वितरण
और उपयोग से संबंधित जानकारी को संग्रहित करेगा। आयोग
राज्य में विद्युत की मांग और उसके उपयोग से संबंधित आंकड़े
तथा पूर्वानुमानों को संग्रहित एवं प्रकाशित करेगा। आयोग
राज्य में पर्यावरण नियामक एजेंसियों के साथ समन्वय
स्थापित करेगा।</p>

<p> </p>

<p>परिवहन आयुक्त
के पद पर नायक की नियुक्ति गलत प्रक्रिया के तहत की
गयी : हाईकोर्ट            </p>

<p> </p>

<p>              </p><p>
रांची : झारखंड उच्च न्यायालय के
मुख्य न्यायाधीश न्यायमूर्ति विनोद कुमार गुप्ता एवं न्यायमूर्ति
हरिशकर प्रसाद की खंडपी ने कहा है कि परिवहन आयुक्त के
पद पर चिंतू नायक की नियुक्ति गलत प्रक्रिया के तहत
की गयी। विद्वान न्यायाधीशों ने ट्रक आनर्स एसोसिएशन
की एक लोकहित याचिका पर सुनवाई करते हुए उक्त
टिप्पणी की। विद्वान न्यायाधीशों ने नियंत्रक महालेखा
परीक्षक एवं प्रमुख महालेखाकार से पूछा है कि श्री नायक को
निलंबन की अवधि का पूर्ण वेतन भुगतान कैसे किया
गया। न्यायालय ने कहा कि चिंतू नायक की नियुक्ति में
गलत प्रक्रिया अपनायी गयी परन्तु बाद में सरकार ने गलती
को सुधार लिया। न्यायालय ने कहा कि सरकार ने ९ मई २००२
को श्री नायक का निलंबन आदेश रद्द कर दिया अत: इस बारे में
उसे कुछ नहीं कहना है। प्रार्थी ने न्यायालय में जनहित याचिका
दाखिल कर न्यायालय को अवगत कराया कि परिवहन आयुक्त
के पद पर चिंतू नायक की नियुक्ति निलम्बनावधि में
हुई थी अत: नियुक्ति अवैध है। महाधिवक्ता ने पक्ष रखते
हुए कहा कि उनका निलम्बन डीभ्डा है अत: वह स्वत: ही निरस्त हो
जाता है। खण्डपी ने सरकार के पक्ष को अस्वीकार करते हुए कहा
कि आल इंडिया सर्विस रुल के रुल २ और ३ के तहत ४८ घण्टे से
आधिक न्यायिक हिरासत में रखा जाता है तो निलम्बन के
आदेश को निरस्त करने के लिए औपचारिकताएं पूरी करनी
आवश्यक है। खण्डपी ने टिप्पणी करते हुए कहा कि यह उनके
सामने ऐसा पहला मामला आया है कि निलम्बित आधिकारी
को निलंबन की अवधि में पूर्ण वेतन प्राप्त हुआ है।
उल्लेखनीय है कि जब चिन्तु नायक बिहार सरकार में कार्यरत
थे तो एक फौजदारी मामले में उनको २ फरवरी २००१ से २८
फरवरी २००१ तक हिरासत में रखा गया था। बिहार सरकार में
निलम्बन आदेश को निरस्त करने का आदेश भी नहीं दिया था
और झारखण्ड सरकार ने उनको परिवहन आयुक्त का प्रभार सौंप
दिया था।</p>

<p> </p>

<p>झारखण्ड विद्युत
बोर्ड की कार्य संस्कृति में बदलाव</p><p>
लाना जरूरी : ऊर्जा मंत्री</p><p>
ऊर्जा
क्षेत्र में सुधार पर तीन दिवसीय प्रशिक्षण कार्यक्रम का
उद्घाटन  </p>

<p> </p>

<p>               </p><p>
रांची : झारखण्ड सरकार के ऊर्जा
मंत्री लाल चंद महतो ने कहा है कि राज्य में बिजली की
स्थिति में सुधार के लिए झारखण्ड विद्युत बोर्ड में नीचे से
ऊपर तक कार्य संस्कृति में बदलाव लाने की जरूरत है।
</p><p>
श्री महतो वृहस्पतिवार
को आई-आई.सी.एम. कांफ्रेंस हाल में पावर फिनांस
कारपोरेशन लिमिटेड तथा आइ.आइ.सी.एम. के संयुक्त
तत्वावधान में पावर सेक्टर रिफार्मविद् स्पेशल इंफैसिस
आन डिस्ट्रीब्यूशन रिफार्म विषय पर आयोजित तीन
दिवसीय प्रशिक्षण कार्यक्रम का उद्घाटन करते हुए उक्त बात
कही। श्री महतो ने कहा कि झारखण्ड विद्युत बोर्ड का
नेटवर्क ५० वर्ष पुराना है जिसे बदलने की जरूरत है।
</p><p>
उन्होंने कहा कि
नीचले स्तर के कर्मचारी समस्याओं को रखते हैं परन्तु जब
नीति बनने लगती है तब हम उनके सुझावों को दरकिनार कर
देते हैं। उन्होंने कहा कि झारखण्ड में बिजली के तार जर्जर हो
गए हैं जिसके कारण संचरण की हानि होती है। इन तारों को
बदलने की जरूरत है। श्री महतो ने कहा कि झारखण्ड में तारों के
जीर्ण-शीर्ण अवस्था में रहने के कारण ३० प्रतिशत संचरण
की हानि होती है जिसे दूर करने की जरूरत है। उन्होंने कहा
कि राज्य में बिजली की चोरी एवं व्यवसायिक हानि एक
गंभीर समस्या है। इन समस्याओं को हमें दूर करना होगा। श्री
महतो ने कहा कि झारखण्ड विद्युत बोर्ड को १२० से १३०
करोड़ रुपए तक राजस्व प्राप्त होना चाहिए परन्तु मात्र ६० करोड़
का राजस्व प्राप्त हो रहा है जो चिन्ता की बात है। उन्होंने कहा
कि आखिर यह कितना दिन चलेगा। उन्होंने कहा कि जब बुनियाद
मजबूत होगा तभी भविष्य अच्छा होगा। उन्होंने कहा कि आज
पूरे देश में बिजली की भारी कमी होने वाली है। श्री महतो
ने कहा कि पूरे देश में ८० हजार गांवों का विद्युतीकरण नहीं
हो पाया है। झारखण्ड में २७ हजार गांवों का विद्युतीकरण नहीं हो
पाया है। उन्होंने कहा कि झारखण्ड सरकार ने अगले पांच छह
वर्षों में इन गांवों में बिजली पहुंचाने का लक्ष्य
निर्धारित किया है। उन्होंने स्वीकार किया कि झारखण्ड में
जिस गति से बिजली की स्थिति में सुधार होना चाहिए वह
नहीं हो रहा है। श्री महतो ने कहा कि तकलीफ तब होती है
जब भ्रष्टाचार में उपभोक्ताओं एवं कर्मचारियों-आधिकारियों
की भागीदारी हो जाती है। उन्होंने कहा कि आज केवल शहर
के लोग बिजली का उपभोग करते हैं। बिजली विलासिता
की वस्तु बन गयी है। उन्होंने बिजली की फिजुल खपत बंद
करने पर बल दिया। उन्होंने कहा कि आवश्यकता के अनुसार
बिजली की खपत कर हम काफी बिजली बचा सकते हैं। श्री
महतो ने उद्योगपतियों से झारखण्ड में पावर सेक्टर में पूंजी
निवेश करने की अपील की। उन्होंने कहा कि झारखण्ड में
थर्मल पावर प्लांट स्थापित करने के लिए सभी सुविधाएं
मौजूद हैं। उन्होंने कहा कि वस्तुत: झारखण्ड पूरे देश को रोशनी
देने की क्षमता रखता है। आइ.आइ.सी.एम. के कार्यपालक
निदेशक आर.एन. सिंह ने स्वागत भाषण करते हुए कहा कि
झारखण्ड में ऊर्जा सेक्टर के विकास की काफी संभावना है।
यहां कोयले की कोई कमी नहीं है। पावर फाइनेंस
कारपोरेशन के महाप्रबंधक विनोद बिहारी ने कहा कि
विद्युत बोर्ड के समेकित विकास के लिए निगम वचनबद्ध
है। निगम झारखण्ड विद्युत बोर्ड की आवश्यकताओं को पूरा
करने का हर संभव प्रयास करेगा। उन्होंने कहा कि झारखण्ड में
प्रचूर खनिज सम्पदा उपलब्ध है। यदि इसका उपयोग किया जाए तो
क्रांतिकारी परिवर्तन आ सकता है। एडमिनिस्ट्रेटिव स्टाफ
कालेज हैदराबाद के बलराम रेड्डी ने विद्युत बोर्डों के
आत्मनिर्भर होने की आवश्यकता पर बल दिया। उन्होंने
बोर्डों के स्वरूप में परिवर्तन लाने की आवश्यकता पर बल
दिया। इस प्रशिक्षण कार्यक्रम में चार महाप्रबंधक दस अधीक्षण
आभियंता एवं एक कार्यपालक आभियंता भाग ले रहे हैं। इस
कार्यक्रम के आयोजन में आइ.आइ.सी.एम. के आधिकारियों
ने सक्रिय सहयोग दिया। बिजली उत्पादन के लक्ष्य को पूरा
करने के लिए पुराने संयंत्रों का आधुनिकीकरण जरूरी :
राजीव रंजन झारखण्ड राज्य बिजली बोर्ड के अध्यक्ष ने कहा
कि भारत सरकार के ऊर्जा मंत्रालय ने ग्यारहवीं पंचवर्षीय
योजना में ऊर्जा उत्पादन क्षमता को दुगुना करने का
निर्णय लिया है। २०११ तक यह एक लाख मेगावाट होगा। पिछले
प्रदर्शनों पर नजर डाली जाए तो यह काफी कनि लक्ष्य है। लक्ष्य
तभी पूरा हो सकता है जब हम बीस वर्ष या उससे आधिक
पुराने संयंत्रों का आधुनिकीकरण किया जाए, संचरण एवं
वितरण प्रणाली का नवीनीकरण हो, इस्तेमाल में लायी
जानेवाली पूरी बिजली की मीटिरिंग हो और नयी क्षमता
बढ़ायी जाए। उन्होंने बताया कि भारत सरकार ने सुधार कार्यक्रम
चलाये ताकि आधिक ऊर्जा-उत्पादन किया जा सके। एक सौ
करोड़ के भीतर पूरे होने वाली परियोजनाओं को लिया गया
और उससे ज्यादा वाली परियोजनाओं को दूसरी स्कीम में रखा गया।
दोनों का उद्देश्य देश के ऊर्जा प्रक्षेत्र का तेजी से विकास
करना है। पुराने संयंत्रों के आधुनिकीकरण से दस से पंद्रह
हजार मेगावाट ज्यादा बिजली उत्पादन हो सकता है। यदि भारत
सरकार द्वारा चिह्नित पुराने संयंत्रों का नवीनीकरण कर दिया
जाये एवं संचरण एवं वितरण की हानि दस प्रतिशत कम कर दी
जाए तो पंद्रह से बीस हजार मेगावाट आतिरिक्त बिजली उत्पादन
हो सकेगा। जो कि दसवीं योजना के चालीस हजार मेगावाट
के गक्ष्य का आधा होगा। श्री राजीव रंजन ने बताया कि झारखण्ड
राज्य विद्युत बोर्ड कई योजनाओं को लागू कर रहा है।
इसमें पतरातू थर्मल पावर स्टेशन का नवीनीकरण कर २००६
तक आतिरिक्त छह सौ मेगावाट बिजली का उत्पादन करना,
संचरण एवं वितरण प्रणाली, सभी ग्यारह सर्किलों में ीक करना,
२००३ तक सभी उपभोक्ताओं को मीटर एवं ऊर्जा अंकेक्षण,
२००३ तक संचरण और वितरण की हानि की २० प्रतिशत कम
करना, २००६ तक ग्रामीण विद्युतीकरण आदि है। बोर्ड ने
संचरण-वितरण का मजबूतीकरण शुरू कर दिया है, एक लाख
मीटर लगाए हैं, शीघ्र एस.ई.आर.सी. का गन होगा, सलाहकार
के द्वारा आर.एल.ए. स्टडी एवं आर.एफ.पी. निर्माण जारी कर दिया
है।</p>

<p> </p>

<p> </p>

<p> </p>

<p>गोड्डा उप चुनाव
में फुरकान होंगे कांग्रेस के</p><p>
उम्मीदवार : किड़ो     </p>

<p> </p>

<p>              </p><p>
रांची : कांग्रेस पार्टी के मुख्य
सचेतक एवं वरिष्ठ विधायक थियोडर किड़ो ने प्रेस विज्ञप्ति
जारी कर कहा है कि कांग्रेस पार्टी गोड्डा संसदीय उप चुनाव में
अपना प्रत्याशी खड़ा करेगी। कांग्रेस पार्टी की ओर से
कांग्रेस विधायक दल के नेता फुरकान अंसारी प्रत्याशी
होंगे। उन्होंने कहा कि गोड्डा संसदीय क्षेत्र की जनता का रुझान
कांग्रेस पार्टी की ओर बढ़त जा रहा है। पूरे झारखंड की जनता
भाजपा से तंग आ चुकी है और गोड्डा की जनता कांग्रेस पार्टी
को अपना विकल्प के रूप में मान रही है। उन्होंने कहा कि
गोड्डा जनता का रुझान कांग्रेस पार्टी की ओर होने का मुख्य
कारण फुरकान अंसारी का कांग्रेस प्रत्याशी होना माना जा रहा
है। श्री फुरकान अंसारी हमारे पार्टी के विधायक दल के नेता
हैं हाल में ही विधानसभा में जिस प्रकार भारतीय जनता पार्टी
की खामिया उजागर किया वह सराहनीय है। फुरकान अंसारी
की लोकप्रियता से घबराकर भाजपा हर हाल में कांग्रेस को
हराने की कोशिश करेगी, जिसमें वह कामयाब नहीं होगी।
उन्होंने कहा कि राष्ट्रीय जनता दल सुप्रीमों लालू प्रसाद से भी
फुरकान अंसारी की बात हो चुकी है। श्री यादव ने विश्वास
दिलाया है कि यदि फुरकान अंसारी कांग्रेस के प्रत्याशी
होंगे तो राजद, कांग्रेस को समर्थन करेगी। उन्होंने कहा
कि भाजपा को हराने के लिए एकमात्र श्री फुरकान अंसारी ही
समर्थ उम्मीदवार माना है। श्री थियोडर किड़ो ने कहा कि
गोड्डा संसदीय चुनाव क्षेत्र एक अल्पसंख्यक बहुल क्षेत्र है इसलिए
भी इनकी उम्मीदवारी सार्थक सिद्ध होगी। श्री अंसारी गोड्डा
संसदीय क्षेत्र की जनता में काफी लोकप्रिय हैं। चाहे वह हिन्दू
हो या मुसलमान, गरीब या अमीर, ब्राह्मण, पिछड़ी जाति सभी
में लोकप्रिय है। </p><p>
श्री थियोडर किड़ो
ने कहा कि सभी विधायकों की एक राय है कि कांग्रेस
पार्टी को गोड्डा संसदीय चुनाव क्षेत्र से श्री फुरकान अंसारी
को ही उम्मीदवार बनाया जाए।</p>

<p> </p>

<p>बंद करने पर
सरकार को १३२३ करोड़ का</p><p>
भुगतान करना होगा</p><p>
एचइसी
को चलाने के लिए मात्र दो सौ करोड़ चाहिए : राणा संग्राम
सिंह            </p>

<p> </p>

<p>                </p><p>
रांची : हटिया प्रोजेक्ट
वर्कर्स यूनियन के महामंत्री राणा संग्राम सिंह ने कहा
कि केन्द्र सरकार यदि अभी एचइसी को बंद करती है तो केन्द्र
सरकार को १३२३.१५ करोड़ रुपये का भुगतान करना होगा,
जबकि एचइसी को चलाने के लिए मात्र २०० करोड़ रुपये चाहिए।
श्री सिंह गुरुवार को संवाददाताओं से बातचीत कर रहे थे। श्री
सिंह ने कहा कि अब समय आ गया है कि केन्द्र सरकार
औद्योगिक नीति की समीक्षा खुद करे। इसके लिए
सार्वजनिक उपक्रमों के अध्यक्षों, शीर्ष ट्रेड यूनियन
नेतागण एवं अर्थशास्त्रीयों की एक समिति बनायी जाए जो
औद्योगिक नीति की समीक्षा करे। उन्होंने कहा कि नयी
औद्योगिक नीति का बुरा नतीजा सामने आने लगा है। इस
नीति के कारण भारी संख्या में न सिर्फ कामगारों की
छंटनी हुई बल्कि देश में बेरोजगारी भी बढ़ी है। श्री सिंह
ने कहा कि केन्द्र सरकार की नयी औद्योगिक नीति का
सबसे ज्यादा खमियाजा भारत सरकार के इस सार्वजनिक
उपक्रम एचइसी को भुगतना पड़ा है। एचइसी का गन वर्ष
१९५८ में हुआ। एचइसी को १९६४ में प्रथम प्रधानमंत्री पंडित जवाहर
लाल नेहरू ने राष्ट्र को समर्पित किया। एचइसी ने भारी कास्टिंग,
फोर्जिंग सामान तथा भारी मशीन टूल्स एवं स्पेयर्स का
निर्माण कर स्टील, कोल, सिमेंट, पावर, न्यूक्लीयर, रेलवे,
अल्यूमिनियम, शिपन्यूल्डिंग, डिफेन्स जैसे कोर सेक्टर को
आपूर्ति करता है। उन्होंने कहा कि कभी-कभी तो युद्ध के
दिनों में आवश्यकता पड़ने पर कम समय में सुरक्षा के सामान
एचइसी ने आपूर्ति कर देश की सेवा की है। पूरे विश्व में एक
कैम्पस में विरले राष्ट्र के पास इस तरह का उद्योग है। उन्होंने कहा
कि जर्मनी, जापान, रूस एवं अमेरिका जैसे विकसित राष्ट्र
भी अपने देश के ऐसे बहुमुखी भारी इंजीनियरिंग उद्योग को
बीच-बीच में आर्थिक सहायता देकर जिन्दा किये हुए है। श्री
सिंह ने कहा कि ९० के दशक में एचइसी में ट्रेड यूनियनों की
बाढ़ आ गयी। जिनकी अपनी-अपनी पार्टी पहचान, विचारधारा
और उद्देश्य थे। फलस्वरूप १९८४ एवं १९८७ के लम्बे हड़ताल से एचइसी
को गुजरना पड़ा एवं कारखाने की कार्य संस्कृति खराब
हुई और यह कम्पनी को १९९२ में बीमार घोषित होकर
बीआइएफआर में चली गयी। उन्होंने कहा कि १९९६ में कम्पनी
को बीआइएफआर से पुनर्जीवन योजना पास कराने में
बीआइएफआर ने कम्पनी तथा आइडीबीआइ को भरपूर सहयोग
दिया। लेकिन इधर पांच वर्षों से एचइसी में कार्यरत कुछ
रजिस्टर्ड यूनियनों का आंदोलनात्मक भूमिका ने कम्पनी
को बदनाम किया है। श्री सिंह ने एचइसी से निगम को बचाने
के लिए कार्यशील पूंजी के रूप में २०० करोड़ रुपये की व्यवस्था
केन्द्र सरकार से करने की मांग की है। साथ ही केन्द्र सरकार
बैंकलोन समाप्त कर १७५ करोड़ रुपये का कैश क्रेडिट स्टेट बैंक
से मुहैया कराया जाय। बकाये राशि के भुगतान के लिए ३००
करोड़, आधुनिकीकरण के लिए फेज मैनर में ५०० करोड़ रुपये,
योग्य निदेशकों की नियुक्ति, सेवानिवृत्ति बकाया के
लिए १५० करोड़ रुपये की व्यवस्था, कामगारों का वेतन
पुनरीक्षण आदि की व्यवस्था केन्द्र सरकार करे। साथ ही श्री सिंह
ने केन्द्र सरकार से सार्वजनिक उपक्रमों को दूसरे कम्पनियों
को कार्यादेश देने के लिए नो आब्जेक्सन प्रमाण पत्र
एचइसी से लेना आनिवार्य करें। जैसा कि इंदिरा गांधी के
समय में था। श्री सिंह झारखंड सरकार से बकाये बिजली बिल
को माफ करने की मांग करते हुए कहा कि एचइसी की
बिजली अलग कर आवासीय दर पर देने की व्यवस्था की जाय।
साथ ही बिजली का औद्योगिक दर डीवीसी की तरह कम
करने की व्यवस्था भी झारखंड सरकार करे। सरकार सेल्स टैक्स
दस वर्ष के बाद बिना ब्याज की लेने की भी व्यवस्था
करे। राज्य सरकार एचइसी की ग्रीन लैंड, खाली भवन बाजार दर पर
ले। साथ ही अब तक जितने भी भवनों को झारखंड सरकार ने
आधिग्रहित किया है उसका भाड़ा समय पर भुगतान करने की व्यवस्था
करें। श्री सिंह ने सार्वजनिक उपक्रमों से आग्रह किया कि
वे एचइसी में निर्मित होनेवाले सामानों का कार्यादेश
बिना किसी भेदभाव के एचइसी को दे। उन्होंने कहा कि अभी
भी एचइसी जितने सस्ते दर में सामानों को तैयार कर विभिन्न
उद्योगों को आपूर्ति करता है उतने कम दर में कोई भी
उपक्रम नहीं कर सकता है। उन्होंने स्पष्ट शब्दों में कहा कि
कमीशनखोरी के कारण एचइसी को कार्यादेश नहीं मिल
रहा है। श्री सिंह ने निगम प्रबंधन से भरपूर कार्यादेश लाने
का आग्रह किया।</p>

<p> </p>

<p>प्रदेश नेतृत्व में
फेरबदल नहीं : कैलाशपति                </p>

<p> </p>

<p>              </p><p>
मधुबन : भाजपा के राष्ट्रीय उपाध्यक्ष
एवं झारखंड के प्रभारी कैलाशपति मिश्र ने आज डोमिसाइल
और आरक्षण के मुद्दे पर सरकार का पक्ष लेते हुए कहा कि
उनकी पार्टी राज्य के तृतीय और चतुर्थ श्रेणी की
नौकरियों में स्थानीय लोगों को प्राथमिकता देने की
पक्षधर रही है। श्री मिश्र यहां मधुबन में आयोजित तीन दिवसीय
कार्यकर्त्ता प्रशिक्षण शिविर के उद्घाटन के बाद आयोजित
संवाददाता सम्मेलन में बोल रहे थे। शिवार का उद्घाटन प्रदेश
भाजपा अध्यक्ष अभयकांत प्रसाद ने पार्टी का झंडा फहराकर किया।
उन्होंने प्रदेश सरकार और पार्टी संगन में फेरबदल की संभावना
से इनकार करते हुए कहा कि दोनों ही पूरी निष्ठा से
अपने-अपने दायित्वों का निर्वाह कर रहे हैं। श्री मिश्र ने
कहा कि झारखंड सरकार राज्य में अपेक्षित विकास कार्य कर
रही है। पार्टी में कार्यकर्त्ताओं की उपेक्षा के सवाल पर
उन्होंने कहा कि यह मूी बातें हैं। उन्होंने सांसदों और विधायकों
को दिये जाने वाले सालाना कोटा राशि को जहां व्यवहारिक
कहा वहीं इस बात पर अफसोस जताया कि कभी-कभी कार्यकर्त्ता
ही आभिकर्त्ता बन जाते हैं। उन्होंने जम्मू-कश्मीर की भी
चर्चा की और कहा कि उसके विभाजन के खिलाफ पार्टी
का स्टैण्ड पूरी तरह कायम है। तीन दिवसीय प्रशिक्षण शिविर
के बाबत राष्ट्रीय उपाध्यक्ष ने कहा कि शिविर में कुल १८०
प्रतिनिधि भाग ले रहे हैं जिसमें सांसद, विधायक, पूर्व
सांसद, पूर्व विधायक, राज्य के मुख्यमंत्री सहित कई
मंत्री भाग ले रहे हैं। श्री मिश्र ने शिविर के बारे में कहा कि
भाजपा में शिविर आयोजित करने की परम्परा रही है जिसमें
कोई राजनैतिक प्रस्ताव नहीं किये जाते बल्कि कार्यकर्त्ताओं
को अनुशासन और संस्कार की शिक्षा और रक्षा का प्रशिक्षण दिया
जाता है। शिविर में दल, मोर्चा और मंचों के रचनात्मक
ढांचों की जानकारी, प्रदेश और राष्ट्रीय स्तर पर राजनैतिक
चर्चा की जाती है। उन्होंने कहा कि इस बार स्वावलंबी भारत
के लिए सरकार द्वारा किये जा रहे प्रयासों पर दस सत्रों में
चर्चा होगी। प्रशिक्षण शिविर के प्रथम सत्र में जनसंघ से
भाजपा तक की विकास यात्रा विषय पर विचार व्यक्त
करते हुए कैलाशपति मिश्र ने पार्टी की राजनैतिक यात्रा में
प्रशिक्षण शिविर के महत्व पर प्रकाश डाला। श्री मिश्र ने कहा
कि पार्टी मंच से वरिष्ठ नेता अटल बिहारी वाजपेयी एवं
लालकृष्ण आडवाणी ने घोषणा की थी कि केन्द्र में
हमारी सरकार बनेगी तो अलग झारखंड राज्य गति किया जाएगा।
वर्ष १९९९ में प्रधानमंत्री श्री वाजपेयी ने अलग झारखंड राज्य
के सपने को साकार कर दिया। उन्होंने झारखंड सरकार के क्रिया-कलापों
की चर्चा करते हुए कहा कि प्रतिकूल परिस्थिति में भी
सरकार ने काफी काम किया है और कर भी रही है। भारतीय
जनता पार्टी एवं हमारी सरकार का एक ही उद्देश्य है झारखंड का
विकास करना।प्रशिक्षण शिविर में शामिल होने के लिए
झारखंड के मुख्यमंत्री बाबूलाल मरांडी, संगन मंत्री हृदयनाथ
सिंह, एस.एस. अहलूवालिया, सांसद अजय मारु, सांसद सहित सरकार
के लगभग सभी मंत्री, विधायक, पार्टी के पदाधिकारी
मधुबन पहुंच चुके हैं। प्रशिक्षण शिविर के प्रमुख भाजपा के
प्रदेश अध्यक्ष एवं सांसद अभयकांत प्रसाद हैं। जबकि दीपक
प्रकाश संचालन समिति प्रमुख, राकेश प्रसाद, कार्यक्रम
प्रमुख, दिनेश उरांव नियंत्रक, जवाहर पासवान सह नियंत्रक,
प्रवीण सिंह व्यवस्था प्रमुख तथा सुनील बरियार सह व्यवस्था
प्रमुख बनाए गए हैं।</p>

<p> </p>

<p>ब्रह्मेश्वर सिंह
पटना में गिरफ्तार  </p>

<p> </p>

<p>               </p><p>
रांची : -रणवीर सेना को बड़ा
झटका -पटना पुलिस की बड़ी उपलब्धि -बेऊर जेल भेजा गया
-पोटा लगाये जाने का संकेत पटना पुलिस ने प्रतिबंधित
उग्रवादी संगन रणवीर सेना के कमांडर-इन-चीफ ब्रह्मेश्वर सिंह
उर्फ मुखियाजी को आज दोपहर लगभग दो बजे
एक्जीविशन रोड स्थित एक मकान से गिरफ्तार कर लिया।
बिहार पुलिस पिछले आ वर्षों से ब्रह्मेश्वर सिंह को
गिरफ्तार करने का प्रयास कर रही थी लेकिन हर बार पुलिस
को मुंह की खानी पड़ी। ब्रह्मेश्वर सिंह पर पांच लाख रुपये का
इनाम भी रखा गया था। गुप्त सूचना के आधार पर ही पुलिस ने
उन्हें गिरफ्तार किया। उनके साथ चार अन्य लोगों को भी गिरफ्तार
किया गया, जिसके बारे में छानबीन की जा रही है। पिछले
एक दशक में पटना पुलिस के लिये यह बड़ी उपलब्धियों में से
एक है। वैसे पुलिस का मानना है कि यह पिछले एक दशक की
सबसे बड़ी उपलब्धि है। इस संबंध में मिली जानकारी के
अनुसार पटना पुलिस को यह गुप्त सूचना मिली कि रणवीर
सेना के संस्थापक अध्यक्ष ब्रह्मेश्वर सिंह उर्फ मुखियाजी
आज राजधानी में हैं। पुलिस को सूचना देने वाले ने वह
किाना बता दिया जहां ब्रह्मेश्वर सिंह आने वाले थे। जैसे ही वे
एक्जीविशन रोड स्थित अपने किाने पर पहंुचे, नगर पुलिस
अधीक्षक के नेतृत्व में पुलिस ने उन्हें दबोच लिया। ब्रह्मेश्वर
सिंह को राष्ट्रवादी किसान महासंघ के कार्यालय में
गिरफ्तार किया गया। तलाशी के दौरान उनके पास से किसी
तरह का कोई हथियार बरामद नहीं हुआ लेकिन उनके साथ
मौजूद चार लोगों को भी गिरफ्तार कर लिया गया जिनके
बारे में छानबीन की जा रही है। वैसे सूत्रों का कहना है कि
ब्रह्मेश्वर सिंह के साथ गिरफ्तार लोग उग्रवादी नहीं हैं। पटना के
अपर पुलिस महानिदेशक आशीष रंजन सिन्हा ने बताया कि
ब्रह्मेश्वर सिंह पर राज्य सरकार ने पांच लाख रुपये का इनाम रखा
था। यह इनाम गुप्त सूचना देने वाले को दिया जायेगा। उन्होंने
बताया कि अब तक जो जानकारी उपलब्ध हुई है उससे यह पता
चलता है कि वह २५ संगीन कांडों में शामिल रहा है, जिसमें
सर्वाधिक हत्या के मामले हैं। इसमें सामूहिक हत्या के
मामले भी शामिल हैं। वह पिछले आ वर्षों से लगातार पुलिस
को चकमा दे रहा था। इससे पुलिस हमेशा परेशानी महसूस कर
रही थी। काफी प्रयास के बावजूद ब्रह्मेश्वर सिंह को गिरफ्तार
नहीं किया जा सका था लेकिन आज पुलिस को सुनहरा अवसर
सुलभ हुआ जब ब्रह्मेश्वर सिंह को आज अपराह्न गिरफ्तार कर लिया
गया। गिरफ्तारी के बाद उस मकान की तलाशी ली गयी जहां
उन्हें गिरफ्तार किया गया। हालांकि वहां से किसी
आपत्तिजनक कागज या आग्नेयास्त्र के बरामद होने की सूचना नहीं
है। गिरफ्तारी के बाद ब्रह्मेश्वर सिंह से पूछताछ की जा रही है।
ब्रह्मेश्वर सिंह की गिरफ्तारी की खबर पूरे पटना में आग की
तरह फैल गयी और लोगों की भीड़ उन्हें देखने के लिये
पुलिस कार्यालय के समक्ष एकत्र होने लगी। समाचार लिखे
जाने तक ब्रह्मेश्वर सिंह से पूछताछ जारी थी। ब्रह्मेश्वर सिंह से
उसके संगन के शीर्ष लोगों के बारे में पूछताछ की जा
रही थी और उनका किाना जानने का प्रयास किया जा रहा था।</p>

<p> </p>

<p>झारखंड
तकनीकी शिक्षा सलाहकार समिति का गदन</p><p>
तकनीकी
एवं व्यावसायिक संस्थानों में</p><p>
गुणात्मक शिक्षा के लिए              </p>

<p> </p>

<p>              </p><p>
रांची : राज्य के इंजीनियरिंग
कालेजों, पालिटेकनिकों और व्यावसायिक शिक्षण संस्थानों
में गुणात्मक शिक्षा और निजी एवं सरकारी संस्थानों में
पन-पान का समान स्तर रखने के उद्देश्य से सरकार ने झारखंड राज्य
तकनीकी शिक्षा सलाहकार समिति का गन कर दिया है।
एक्स.एल.आर.आइ. के निदेशक डा. पी.डी. थॉमस इस समिति
के अध्यक्ष बनाये गये हैं। राज्य सरकार ने झारखंड के विभिन्न
जिलों में इंजीनियरिंग कालेज, पालिटेकनिक और व्यावसायिक
संस्थान खोलने का निर्णय लिया है। इनमें उच्चकोटि का
तकनीकी और व्यावसायिक शिक्षा स्तर बनाए रखने का जिम्मे
राज्य सरकार का है। इन संस्थानों में राज्य सरकार के मापदंडों के
अनुसार पन-पान कराना आवश्यक है ताकि निजी और सरकारी
संस्थानों में गुणात्मक शिक्षा का स्तर एक समान रखा जा सके।
इसके लिए झारखंड तकनीकी शिक्षा सलाहकार समिति के गन
का सरकार ने निर्णय लिया। यह समिति विभिन्न स्थानों
पर तकनीकी संस्थानों पालिटेकनिकों और व्यावसायिक
संस्थान खोलने के लिए परामर्श देगी। यह समिति
तकनीकी विश्वविद्यालय की स्थापना और कार्यक्षेत्र पर
सलाह देगी और सभी तकनीकी संस्थानों के उन्नयन, शिक्षा
में एकरुपता एवं सामान्य आधारभूत संरचना निर्माण के लिए
भी परामर्श देगी। अनुसूचित जाति, अनुसूचित जनजाति,
पिछड़े और ग्रामीण छात्रों को तकनीकी एवं व्यावसायिक एवं
रोजगारोन्मुखी शिक्षा प्रदान करने और तकनीकी शिक्षण के
प्रति माहौल तैयार करने हेतु परामर्श देना भी इस समिति
के कार्यक्षेत्र में होगा। तकनीकी शिक्षा, व्यावसायिक शिक्षा
गैर सरकारी संस्थानों में आरक्षण एवं संस्थानों को आन लाइन
शिक्षण सुविधा हेतु परामर्श देना भी इसी समिति के कार्यक्षेत्र
में होगी। सर्वश्री एलसीसीएन शाहदेव, पूर्व कुलपति
रांची वि.वि., एस.के. मुखर्जी कुलपति बी.आइ.टी. मेसरा,
इन्दु धान, कुलपति सिद्धू कान्हू वि.वि., यू.के. चौबे इंडियन
इंस्टीच्यूट आफ साइंस एंड मैनेजमेंट बहादुर सिंह, शिवजी सिंह,
आर.आइ.टी. के प्राचार्य, डा. प्रकाश उरांव, अहमद सज्जाद और
जितेन्द्र सिंह को समिति का सदस्य बनाया गया है। विज्ञान
प्रावैद्यिकी मंत्रालय के निदेशक इस समिति के सदस्य सचिव
होंगे।</p>

<p> </p>

<p>ललपनियां पावर
स्टेशन का काम शीघ्र पूरा</p><p>
होगा : लालचन्द महतो    </p>

<p> </p>

<p>              </p><p>
हजारीबाग
: तेनुघाट
थर्मल पावर स्टेशन ललपनिया के द्वितीय चरण का
निर्माण काम शीघ्र ही पूरा हो जायेगा। इसके लिए भेल कम्पनी
को ठेका आवंटित कर दिया गया। इसके निर्माण पर दो
हजार ९० करोड़ रुपए खर्च आयेंगे। निर्माण कार्य में
भेल कम्पनी को तीन करोड़ रुपया प्रति मेगावाट के अनुपात
भुगतान किया जाएगा। उक्त बातें कल तेनुघाट थर्मल पावर
स्टेशन ललपनियां में आयोजित वन महोत्सव समारोह में मुख्य
आतिथि के पद से झारखण्ड राज्य के ऊर्जा मंत्री लालचंद महतो
ने कही। ऊर्जा मंत्री श्री महतो ने कहा कि पूरे झारखण्ड में
३२ हजार गांव हैं, जिनमें पांच हजार गांवों में बिजली सुविधा
प्राप्त हैं। २८ हजार गांव अभी तक बिजली से वंचित हैं। उन्होंने
कहा कि प्रत्येक वर्ष पांच हजार गांव में बिजली मुहैया
कराने का मरांडी सरकार का लक्ष्य हैं। श्री महतो ने कहा कि
द्वितीय चरण में ६३० मेगावाट का निर्माण पूरा होने के बाद
तृतीय चरण में ५०० मेगावाट निर्माण करने पर बल दिया
जाएगा। उन्होंने कहा कि तेनुघाट थर्मल पावर स्टेशन ललपनियां
से कुल १५५० मेगावाट विद्युत उत्पादन करने की क्षमता बनाना
है। इस अवसर पर झारखण्ड प्रदेश के वन मंत्री यमुना सिंह ने कहा
कि पेड़ लगाने से ही छुटकारा नहीं मिलता है, बल्कि उसकी
सुरक्षा की जरूरत है। जंगलों की सुरक्षा वहां के आस-पास रहने
वाले गांवों वाले कर सकते हैं। उन्होंने कहा कि हर व्यक्ति
को जीने के लिए एक पेड़ लगाने होगा। श्री सिंह ने कहा कि
प्रदेश में १७२ पौधशाला का निर्माण किया गया है। वन
महोत्सव के दौरान ऊर्जा मंत्री लालचन्द महतो, वन मंत्री यमुना
सिंह, पूर्व विधायक छत्रुराम महतो एवं ललपनियां के
महाप्रबंधक जी.एस. वर्मा ने कई तरह के पौधे लगाएं।
इस अवसर पर टी.वी.एन.एल. के सलाहकार डा. अशोक सिन्हा,
प्रभात सिन्हा, के. राम, कोलेश्वर महतो, जदयू के नेता इन्द्रदेव
महतो, किशोर महतो, उमेश शर्मा एवं तेनुघाट मजदूर यूनियन
के सचिव धनी राम मांझी सहित बड़ी संख्या में लोग उपस्थित
थे।</p>

<p> </p>

<p> </p>

<p>कार्य में
तेजी लाएं, अन्यथा कार्रवाई : मुख्य सचिव</p>

<p>              </p><p>
रांची : राज्य के मुख्य सचिव जी.
कृष्णन सुस्त एवं लापरवाह आधिकारियों की खबर लेंगे।
विकास योजनाओं को शीघ्रता से धरातल पर लाने के क्रम
में कड़ी कार्रवाई करने का निर्णय लिया गया है।
मुख्य सचिव ने राज्य के विभिन्न जिलों में चलने वाली योजनाओं
की अद्यतन स्थिति लेने का मन बनाया है। ऐसी स्थिति में
जिस जिले में कार्य की गति धीमी पाई जाएगी उन
जिलों के पदाधिकारियों के विरुद्ध कड़ी कार्रवाई की
जाएगी। मुख्य सचिव श्री कृष्णन ने सभी उपायुक्तों,
उप-विकास आयुक्तों को कार्य प्रगति रिपोर्ट तैयार
करने तथा योजनाओं के कार्यान्वयन में शीघ्रता लाने का
निर्देश दिया है। मुख्य सचिव श्री कृष्णन ने कहा कि
विकास योजनाओं को समय सीमा के भीतर सम्पादित कराने
एवं पारदर्शिता बरतने के लिए उन्होंने सभी जिला उपायुक्तों
एवं उप विकास आयुक्तों को पत्र लिखकर निर्देश दिया हैं। जो
आधिकारी अपने दायित्वों का निर्वाह नहीं करेंगे उनके
विरुद्ध अनुशासनात्मक कार्रवाई की जाएगी। उन्होंने कहा
कि पिछले माह प्रखंड विकास पदाधिकारियों उप विकास आयुक्तों
एवं उपायुक्तों की बैठक में मुख्यमंत्री ने आधिकारियों
को कड़े निर्देश दिए थे।</p>

<p> </p>

<p>आइसीसी ने
मारी बाजी                         </p>

<p> </p>

<p>प्रायोजन अनुबंध को लेकर भारतीय क्रिकेट
बोर्ड और खिलाड़ियों के बीच चल रही तनातनी के कम
होने के आसार अब नजर आने लगे हैं। 'देश की प्रतिष्ठा' के
नाम पर भारतीय क्रिकेटरों ने नरम रुख अपनाना स्वीकार किया
है। दरअसल इस मसले पर फिलहाल भारतीय क्रिकेट बोर्ड का
पलड़ा ही भारी है इसलिए क्रिकेट खिलाड़ियों के सामने झुक
जाने के अलावा कोई दूसरा विकल्प भी नहीं था। यदि
खिलाड़ी आइसीसी के अनुबंध पर हस्ताक्षर नहीं करने पर अड़
जाते, तो बोर्ड ने भी कड़ा कदम उठाने का सारा इंतजाम
कर लिया था। खिलाड़ियों के विरोध के जवाब में बोर्ड
आगामी १२ से २९ सितम्बर तक श्रीलंका में होने वाले मिनी
विश्वकप टूर्नामेंट में नये खिलाड़ियों की टीम को
भेजने का मन बना चुका था। यदि यही टीम वहां अच्छा
प्रदर्शन कर लेती तो फिर मुख्य टीम के कई चयनित
खिलाड़ियों का पत्ता साफ होना निश्चित था। खिलाड़ियों को
बोर्ड से समझौता तो करना ही था क्योंकि आस्ट्रेलिया, इंगलैंड,
श्रीलंका और दक्षिण अफ्रीका के क्रिकेट खिलाड़ी पहले ही
इस अनुबंध पर हस्ताक्षर करने के लिए तैयार हो चुके थे।
अनुबंध के अनुसार अगले माह होने वाली चैम्पियंस ट्राफी
के दौरान खिलाड़ियों को उन कम्पनियों के विज्ञापन के
लिए मॉडलिंग वहीं करनी है जो प्रायोजन कम्पनियों की
प्रतिद्वंद्वी कम्पनियां हैं। स्टार क्रिकेटरों की मॉडलिंग के जरिए
अपना उत्पाद बेचने वाली कम्पनियों को इस अनुबंध से
काफी घाटा उठाना पड़ेगा पर उनके सामने भी दूसरा रास्ता
नहीं है क्योंकि माडलिंग के जरिए कमाई के एवज में स्टार
खिलाड़ी भी अपना खेल भविष्य दांव पर नहीं लगाना चाहेंगे।</p>

<p> </p>






</body></text></cesDoc>