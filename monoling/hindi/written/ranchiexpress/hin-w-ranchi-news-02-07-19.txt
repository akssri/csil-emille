<cesDoc id="hin-w-ranchi-news-02-07-19" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-ranchi-news-02-07-19.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Celia Worth and Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Internet version of the "Ranchi Express" newspaper (www.ranchiexpress.com), news stories collected on 02-07-19</h.title>
<h.author>Ranchi Express Group</h.author>
<imprint>
<pubPlace>Ranchi, Jharkhand, India</pubPlace>
<publisher>Ranchi Express Group</publisher>
<pubDate>02-07-19</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>झारखंड बंद के
दौरान कई स्थानों पर हिंसा</p>

<p> </p>

<p>               </p><p>
रांची :राज्य प्रशासन एवं पुलिस
का कहना है कि डोमिसाइल के विरोध में आयोजित झारखंड
बंद का रांची, जमशेदपुर, रामगढ़ एवं धनबाद में छिटपुट घटनाएं
हुई। पलामू, बोकारो, पश्चिम सिंहभूम, दुमका, गिरिडीह,
सरायकेला, चतरा, गुमला, लोहरदगा, देवघर एवं पाकुड़ में बंद
का कोई खास असर नहीं देखा गया। कुछ जिलों में व्यवसायिक
प्रतिष्ठान, बड़े वाहनों का परिचालन बंद रहा। जीटी रोड पर
वाहनों का परिचालन बाधित नहीं हो सका। राज्य की गृह
सचिव सुषमा सिंह एवं राज्य के पुलिस महानिदेशक आर.आर.
प्रसाद आज पत्रकारों को उक्त जानकारी दी। उन्होंने बताया कि
रांची, रामगढ़, हजारीबाग, धनबाद, जमशेदपुर में ामश: ९५, ५७, ३८,
३०, १००, बंद समर्थकों को गिरफ्तार किया गया। </p><p>
गृह सचिव ने बताया
कि रांची में विधायक आवास के सामने आज परिवहन मंत्री
सघनु भगत की कार पर बंद समर्थकों द्वारा पथराव किये
जाने के बाद उनके अंगरक्षक ने चार राउंड गोलियां चलायी।
उन्होंने बताया कि सेक्टर-२ गोलचक्कर के पास जमी भीड़
को तितर-बितर करने के लिए पुलिस को लगभग
सात-आठ राउंड आश्रु गैस का गोला छोड़ना पड़ा साथ ही
पुलिस ने रबर की गोलियां भी चलायी। पिस्का मोड़ पर
बर्ड के निदेशक विनोद अग्रवाल के कार्यालय में आग
लगाने का प्रयास किया गया और कार्यालय को क्षतिग्रस्त
कर दिया गया। विधानसभा के गेट के पास भी विधानसभा के
बोर्ड को बंद समर्थकों ने क्षतिग्रस्त कर दिया। उन्होंने
बताया कि सेक्टर में कई स्थानों पर बंद समर्थकों द्वारा
वाहनों के टायर जलाये गये। गृह सचिव ने बताया कि
अलबर्ट एक्का चौक के समीप जीटीवी के पत्रकार के
वाहन का शीशा फोड़ा गया। विधानसभा के निकट टेलीफोन
लाइन क्षतिग्रस्त कर दी गयी। पुलिस वाहनों पर भी पथराव किया
गया। सेक्टर-३ आभियंत्रण भवन के निकट पुलिस वाहन के
साथ अन्य वाहनों पर भी पथराव करने के बाद पुलिस को
लाठी चार्ज करना पड़ा। रातू रोड बैंक कालोनी के
निकट बंद समर्थकों ने एक गाड़ी का शीशा तोड़ डाला।
उन्होंने बताया कि बंद समर्थकों ने संयोजक राजीव रंजन
मिश्रा एवं समता पार्टी के नेता उदय शंकर ओझा सहित ९५
लोगों को गिरफ्तार किया गया। उन्होंने बताया कि
आदिवासी छात्र संघ के सुनिल मिज के आठ समर्थकों,
जगदीश लोहरा के ५० समर्थकों को एवं चमरा लिंडा के
नेतृत्व में ५० समर्थकों को रांची कालेज में मेन रोड आने
के ाम में रोका गया। उन्होंने बताया कि विधायक देवकुमार
धान भी अपने समर्थकों के साथ पिस्का मोड़ के पास
एकत्रित थे। गृह सचिव ने बताया कि धनबाद में झरिया
ओपी, रानीबाग एवं सरायकेला स्टील गेट में बंद समर्थकों
द्वारा बन्द कराने का प्रयास किया गया जिससे झड़प हो गयी।
पुलिस हस्तक्षेप के बाद स्थिति सामान्य हुई। नगर विकास
मंत्री बच्चा सिंह के आवास पर पथराव किया गया। धनबाद में
रेल यातायात को बाधित करने का असफल प्रयास किया गया।
बरमसिया में रेल पटरी पर ट्रक लगा कर रेल यातायात बाधित
किया गया, जिससे राजधानी एक्सप्रेस पांच बजकर २० मिनट
से ९.१० मिनट तक रोकी रही। राजधानी एक्सप्रेस इंजन पर भी
पथराव किया गया। पौने ग्यारह बजे के बाद रेल यातायात
सामान्य हो गया। धनबाद-चंद्रपुरा गाड़ी को थोड़ी देर के लिए
रोका गया। </p><p>
गृह सचिव ने बताया
कि जीटी रोड पर स्थिति सामान्य थी। बीसीसीएल में बंद
अप्रभावी रहा। हालाकि बाद में ३० बंद समर्थकों को
गिरफ्तार किया गया है। हजारीबाग में सुभाष चौक पर
सड़क जाम किया गया जिसमें ३८ बंद समर्थकों को
गिरफ्तार किया गया। इसके अलावा हजारीबाग में बंद
प्रभावहीन रहा। रामगढ़ में दुकानें बंद रहा और यातायात प्रभावी
रहा। वहां ५७ बंद समर्थकों को गिरफ्तार किया गया।
जमशेदपुर में १०० बंद समर्थकों को गिरफ्तार किया गया।
वहां सुबह एक घंटे तक रेलवे के परिचालन में बाधा पहंुची।
</p><p>
 </p>

<p>जमशेदपुर में
बंद असरदार, ५०० गिरफ्तार             </p>

<p> </p>

<p>                </p><p>
जमशेदपुर
: डोमिसाइल
नीति का विरोध करने वाले संगठनों की अपील पर
झारखण्ड बंद के दौरान पूर्वी सिंहभूम में बंद असरदार रहा।
गोलमुरी में छात्र संगठनों ने दो घंटों तक सड़क जाम रखी
तो मानगो पुल को युवा समता के कार्यकर्ताओं ने
तीन घंटे तक जाम किया। यों यातायात तो पहले से ही
ठप था लेकिन सड़क-पुल जाम होने से निजी वाहनों पर
घूमने वाले नागरिकों की परेशानी बढ़ गई। शहर भर में
बंद समर्थकों ने मरांडी सरकार व डोमिसाइल नीति के
खिलाफ अपना गुस्सा उतारा और बाजार में दुकानों को बंद कराया।
बस, टेंपो पहले से बंद थे, इनके स्टैण्ड सूनसान पड़े रहे। इस वजह
से स्टेशन पर बाहर से आए मुसाफिरों को दिक्कत हुई।
सुबह-सुबह स्टेशन, साकची व गोलमुरी में तोड़फोड़ की
कार्रवाई की गई। कई टेंपों के शीशे तोड़ दिए
गए। लेकिन शहर में कहीं कोई आप्रिय वारदात नहीं हुई।
साकची थाना इलाके में जेल गेट के पास झारखण्ड के वित्त
मंत्री मृगेन्द्र प्रताप सिंह के घर पर पथराव व तोड़फोड़ की
गई। बताया जाता है कि समाजवादी पार्टी व एनएसयूआई
से जुड़ेबंद समर्थकों ने यह कार्रवाई की। आवास
परिसर के बाहर लगे लोहे के फाटक को तोड़ डाला गया।
पुलिस प्रशासन भी मुस्तैद था लेकिन हंगामा भड़कने की
आशंका में सख्ती बरतने से परहेज किया गया। डोमिसाइल
नीति के समर्थन व विरोध के सवाल पर झारखण्ड में बने
हालातों के मद्देनजर शहर में निजी स्कूलों के प्रबंधकों ने
पहले ही गुरूवार को छुट्टी घोषित कर दी थी। सरकारी कार्यालय
खुले तो रहे, पर उनमें कामकाज नहीं हुआ। कमोवेश यही हाल
बैंकों का रहा, बैंक खुले जरूर लेकिन पहुंचने वाले ग्राहकों
को बैरंग लौटा दिया जाता रहा। पेट्रोल पंप, सिनेमाहाल भी
बंद रहे। पुलिस सूत्रों के मुताबिक पांच सौ बंद
समर्थकों को पकड़ा गया और शाम को रिहा कर दिया गया।
समाजवादी पार्टी के बन्ना गुप्ता, समता पार्टी के मुन्ना
शर्मा, संजय ठाकुर, कमलजीत कौर गिल, संजय सिंह
मंटू, कमलनाथ, छात्र नेता अजय सिंह को बंद कराते पुलिस ने
पकड़ा। लेकिन इस सिलसिले में पहली गिरफ्तारी शिवसेना
नेता दीनानाथ पांडे की हुई। बंद पूरी तरह सफल रहा। </p><p>
 </p>

<p>डोमिसाइल के
विरोध में झारखंड बंद के पक्ष एवं विपक्ष में अपील      </p>

<p> </p>

<p>                </p><p>
रांची : चांडिल, नीमडीह एवं ईचागढ़
प्रखंड झामुमो ने झारखंड सरकार की डोमिसाइल नीति का
समर्थन करते हुए १८ जुलाई को डोमिसाइल का विरोध
करने वाले संगठनों द्वारा आहूत झारखंड बंद का विरोध
करने का निर्णय किया है। झारखंड दिशोम पार्टी की
ईचागढ़ क्षेत्रीय इकाई ने भी १८ जुलाई को
डोमिसाइल के विरोध में झारखंड बंद का विरोध किया है।
पार्टी के क्षेत्रीय प्रवक्ता हांसदा के अनुसार यदि डोमिसाइल
नीति में लचीलापन या ठील देने की कोशिश की गयी
तो अन्य लोग आदिवासी एवं मूलवासियों से नौकरी तथा अन्य
आधिकार छीन लेंगे जिससे झारखंड राज्य के गठन के उद्देश्य
को गहरा आघात लगेगा। झारखंड मुक्ति आभियान के अमर
सेंगल के अनुसार उनका संगठन डोमिसाइल के विरोध में
१८ जुलाई को आहूत झारखंड बंद का पूरजोर विरोध करता
है। उन्होंने झारखंड विरोधियों द्वारा आहूत इस बंद को सफल
करने के लिए डोमिसाइल समर्थक झारखंड की हितैषी
संगठननों से एकजुट होकर आगे आने का आठान किया
है। दूसरी ओर १८ जुलाई को बंद समर्थकों द्वारा भी अपना
पक्ष रखते हुए बंद को सफल बनाने हेतु जोर-शोर से प्रचार किया
जा रहा है। ब्रह्मर्षि युवा मंच, झारखंड जनतांत्रिक पार्टी,
समाजवादी छात्र सभा, भारतीय राष्ट्रीय छात्र संगठन व
समाजवादी पार्टी की स्थानीय इकाइयों ने झारखंड सरकार की
डोमिसाइल नीति को भेदभाव की नीति बताते हुए इसे वापस
लेने के लिए मरांडी सरकार पर दबाव बनाने हेतु १८ जुलाई
को डोमिसाइल के विरोध में झारखंड बंद को सफल बनाने
की भी कोशिश की।</p>

<p> </p>

<p>डोमिसाइल मुद्दे
पर पार्टी और सरकार में मतभेद नहीं : मुख्यमंत्री          </p>

<p> </p>

<p>              </p><p>
रांची : मुख्यमंत्री बाबूलाल
मरांडी ने कहा है कि झारखंड से किसी को भगाया नहीं
जाएगा। यहां सभी को जीने-रहने, का समान आधिकार है।
डोमिसाइल का विरोध वही लोग कर रहे हैं जो झारखंड का
विकास देखना नहीं चाहते। डोमिसाइल के मुद्दे पर न तो
कैबिनेट में और न ही पार्टी में किसी तरह का गतिरोध है।
सरकार सबको एक गृष्टि से देख रही है। उन्होंने ऐसे लोगों को
अपनी हरकतों से बाज आने की चेतावनी देते हुए स्पष्ट किया
कि डोमिसाइल नीति पर पुनर्विचार नहीं किया जाएगा। उन्होंने
यह भी कहा कि सरकार झारखंड में वर्षों से रहने वालों में,
जिनके पास जमीन नहीं है या है उन सबकी व्यवस्था करने की
दिशा में उतनी ही गंभीर है जितनी डोमिसाइल पर। मुख्यमंत्री
ने कहा कि झारखंड के विकास का पैमाना अच्छी सड़कें,
निर्बाध विद्युत आपूर्ति नहीं माना जाना चाहिए। सही अर्थों
में राज्य की जनता खासकर वर्षों से उपेक्षित लोगों, दलितों
एवं कमजोर वर्ग के लोगों का जीवन स्तर उठाना
किसी भी सरकार का दायित्व है। इसी दिशा में उन्होंने
कार्रवाई की है। इसे वे अपराध नहीं मानते हैं।
डोमिसाइल नीति का मूल मतलब वर्षों से पिछड़ी जनता
को समाज की मुख्य धारा से जोड़ना है। डोमिसाइल नीति के
सहारे तृतीय एवं चतुर्थवर्गीय पदों पर नियुक्तियों में
स्थानीय लोगों को सिर्फ प्राथमिकता दी जाएगी। इसके
अलावा सरकार वैसे लोगों पर भी नजर रख रही है जो वर्षों से
झारखंड में तो रह रहे हैं मगर किसी कारण से वे जमीन मालिक
नहीं बन सके। </p><p>
श्री मरांडी ने कहा
कि यह सब उनके विरोधियों की चाल है। वे षड्यंत्र रच रहे
हैं। झारखंड को अशांत करना चाहते हैं। घर-घर में लड़ाई लगवा
कर समाज को बांटना चाहते हैं। मगर उनकी यह मंशा कभी
पूरी नहीं होगी। सरकार डोमिसाइल नीति पर किसी भी हालत
में पुनर्विचार नहीं करेगी। उन्होंने काफी सोच-समझकर इस
नीति की घोषणा की है। क्या इस मुद्दे पर सरकार मंत्रियों
के बीच मतभेद है। उन्होंने कहा, ऐसी बात नहीं है। कुछ लोगों
को यह तत्काल पचा नहीं। मुख्यमंत्री बाबूलाल मरांडी मानते हैं
कि डोमिसाइल नीति के विरोध में आज झारखंड बंद की
घोषणा करना नासमझी का कदम है। कतिपय स्वार्थी तत्व
जो वर्षंो से झारखंड का दोहन करते रहे हैं, मानसिकता के
सिरफिरे लोग उनका तथा डोमिसाइल नीति का विरोध कर
रहे हैं। जब उनसे पूछा गया कि क्या डोमिसाइल के मुद्दे पर
भाजपा के भीतर भी उनका विरोध हो रहा है। मुख्यमंत्री ने
कहा कि उनकी सरकार को भारतीय जनता पार्टी का पूरा
समर्थन है। कभी कभी अच्छे कार्यों का भी किसी
विशेष उद्देश्य से आलोचना की जाती है। मगर इसकी
जानकारी उन्हें नहीं है। समता पार्टी के एक वरिष्ठ नेता द्वारा
डोमिसाइल नीति पर पुनर्विचार करने तथा बिहार की जनता
की भावना का आदर करने की चर्चा करने पर मुख्यमंत्री
ने कहा कि हाजीपुर में रेल जोन की स्थापना हो। इसका
समर्थन झारखंड सरकार की इस कार्रवाई से उनकी
मंशा स्पष्ट हो जाती है। डोमिसाइल के समर्थन में २० जुलाई
की बंदी की घोषणा को किस नजर से देखते हैं? मुख्यमंत्री
ने कहा कि वे चाहते हैं कि राज्य में प्रतिदिन सुचारू रूप से
कामकाज हो। डोमिसाइल के मुद्दे पर सरकार की नीति स्पष्ट है,
इसलिए किसी खास समुदाय को बंद नहीं करना चाहिए। उन्होंने
डोमिसाइल नीति लागू कराने के लिए सरकार की मंशा को
देखते हुए आदिवासी एवं मूल निवासियों से २० जुलाई की
बंद को वापस लेने का अपील की। उन्होंने आदिवासियों से
राज्य की रक्षा करने के लिए आने का आठान किया।
डोमिसाइल नीति पर बिहार सरकार द्वारा प्रतिकूल टिप्पणी किए
जाने पर उन्होंने कहा कि उन्हें आशंका थी कि बिहार सरकार ऐसा
कर सकती है। इसलिए उन्होंने इस परिपत्र को सार्वजनिक
को दिया जिसमें डोमिसाइल के आधार पर मूल निवासियों
को प्राथमिकता देने की बात कही गई है। </p><p>
 </p>

<p>झारखंड बंद का रांची के
बाजारों पर असर रहा                                                               </p>

<p> </p>

<p>              </p><p>
रांची : डआज 'झारखंड बंद' को
लेकर रांची के सभी बाजार बंद रहे। कहीं भी कोई
कारोबार नहीं हुआ। २० जुलाई भी बंद है। २१ को रविवार है।
इसी तरह इस सप्ताह में कारोबार की स्थिति ठीक नहीं रही।
रांची में बिजली की स्थिति अभी भी पूर्ण
संतोषजनक नहीं है। कल कुछ ट्रांसफार्मर बदले गये
जिससे बिजली उपभोक्ताओं को काफी राहत मिली। अभी
बिजली के बिल समय पर अवश्य मिल जाते हैं। अपर बाजार के
बिजली उपभोक्ताओं को अभी तक बिल भुगतान करने के
लिये दूर कांके रोड जाना पड़ता है जहां जाने के लिये समय
और धन का आतिरिक्त खर्च करना पड़ता है। वैसे बैंक में
भी बिजली भुगतान की सुविधा प्रदान की गयी है। रांची में
टेलीफोन की स्थिति में अभी ठीक नहीं है। किसी का
भी टेलीफोन यदि खराब हो जाता है तो उसकी मरम्मत जल्दी से
नहीं होती। इसके लिए सभी स्तरों पर दौड़-धूप करनी पड़ती
है। फिर भी टेलीफोन की मरम्मत नहीं होती। रांची में रूक
रूक कर मामूली वर्षा हो रही है। यह वर्षा पर्याप्त
नहीं बतायी जा रही है। साग-सब्जियों के भाव के बावजूद
जितने कम होने चाहिए नहीं हो रहे हैं। रांची में कई सब्जी
बाजार हैं। अपर बाजार स्थित अर्द्ध साप्ताहिक हाट में सबसे
महंगी सब्जी बिकती है। अन्य सब्जी बाजारों में भाव कुछ सस्ते
रहते हैं। </p><p>
 </p>

<p>घूरती रथयात्रा
२० को निकलेगी        </p>

<p> </p>

<p>                </p><p>
रांची : जगन्नाथपुर मंदिर न्यास
समिति के कोषाध्यक्ष ठाकुर राधेश्याम नाथ शाहदेव ने
एक प्रेस विज्ञप्ति जारी कर बताया कि २० जुलाई को हरिशयन
एकादशी के दिन घूरती रथ यात्रा निकाली जायेगी। </p><p>
श्री शाहदेव ने रथयात्रा
के कार्याम एवं समय सारणी के बारे में जानकारी दी कि
२० जुलाई को प्रात: ६ बजे श्री बलदेव जी, माता सुभद्रा एवं
श्री श्री जगन्नाथ स्वामी का मौसीबाड़ी मंदिर में दर्शन
सर्व सुलभ, अपराह्न ३ बजे मौसीबाड़ी मंदिर में दर्शन बंद,
३.३१ बजे अपराह्न नरसिंहजी, सुदर्शन चा, गरुड़ जी, बलदेव,
माता सुभद्रा, श्री श्री जगन्नाथ स्वामी के रथ के लिए मौसी
बाड़ी से प्रस्थान, अपराह्न ४ बजे तक विग्रहों का रथारूढ़ एवं
श्रृंगार रथ के ऊपर अपराह्न ४.०१ बजे से ४.३० बजे तक श्री विष्णु
सहस्त्रनाम स्तोत्रम पाठ एवं जगन्नाथष्टकम का पाठ, पूजा एवं
आरती, ४.३१ से ५ बजे संध्या मौसीबाड़ी से रथ का प्रस्थान एवं
मुख्य मंदिर तक पहुंचने का समय है। </p><p>
इसके बाद शाम ५.०१
बजे से ५.३० बजे तक रथ के ऊपर सर्व दर्शन सुलभ
महिलाओं के लिए विशेष सुविधा, ५.३१ बजे से ६.१५ बजे संध्या
सभी रथारुढ़ विग्रहों का एक-एक कर मुख्य मंदिर में प्रवेश कराया
जायेगा। ८ बजे रात्रि में मुख्य मंदिर के अन्दर सभी विग्रहों का
एक साथ श्री श्री १०८ की मंगल आरती होगी।</p>

<p> </p>

<p>अस्पताल की
प्रशासनिक व्यवस्था में गिरावट  </p>

<p> </p>

<p>              </p><p>
रांची : राजेन्द्र मेडिकल कालेज
अस्पताल की स्थिति में निरंतर आ रहे सुधार के बावजूद
अस्पताल की प्रशासनिक व्यवस्था मे गिरावट आ रही है। अस्पताल
के प्रभारी अधीक्षक के कार्यालय में बराबर अनुपस्थित
रहने से सुधार प्रायिा में बाधा उत्पन्न हो रही है। जानकारी के
अनुसार आंख विभाग के विभागाध्यक्ष व प्रभारी अधीक्षक
एस.एन. चौधरी पिछले एक सप्ताह से अपने कार्यालय नहीं
आए हैं। इसके कारण इस माह विभागाध्यक्षों की निर्धारित
बैठक में प्रभारी अधीक्षक की अनुपस्थिति की वजह से
बैठक नहीं हो पायी। प्रत्येक माह अस्पताल के विभागाध्यक्षों
की बैठक होती है। अस्पताल सूत्रों से मिली जानकारी
के अनुसार मरीजों की समस्याओं पर भी अधीक्षक कोई ध्यान
नहीं देते हैं और न ही सुबह और शाम में अस्पताल में भर्ती
मरीजों को देखने के लिए वार्ड का निरीक्षण करते हैं।
अधीक्षक कार्यालय में उपस्थित नहीं रहने के कारण कार्यालय
के कर्मचारियों को भी परेशानी होती है आवश्यक कार्यों
का निष्पादन भी समय पर नहीं हो पाता है। बताया गया कि
अधीक्षक की अनुपस्थिति का लाभ अस्पताल के कर्मचारी
से लेकर चिकित्सा एवं नर्स भी उठाते हैं। ये लोग भी
अपने निर्धारित ड्यूटी समय से डेढ़ से दो घंटे विलंब से
आते हैं जिनसे वार्ड में भर्ती मरीजों को काफी परेशानी
होती है। विगत माह स्त्री रोग एवं प्रसूति विभाग में
लेप्रोस्कोपी उपलब्ध नहीं कराने का मामला भी प्रशासनिक
शिथिलता का प्रमाण है। चिकित्सकों के बीच आपसी
प्रतिस्पर्धा एवं द्वेष की भावना उत्पन्न हो रही है। यहां
चिकित्सकों में आपसी तालमेल का घोर अभाव है। </p><p>
आर.एम.सी.एच. में
मरीजों की संख्या में लगातार वृद्धि हो रही है। अस्पताल में
स्वीकृत बेडो की कुल संख्या ९९१ है जिसमें भर्ती मरीजों की
संख्या ९५० के आसपास है। अस्पताल के आस्थि एवं जोड़ विभाग,
न्युरो सर्जरी, सामान्य सर्जरी विभाग, स्त्री रोग एवं प्रसूति
विभाग में लगातार मरीजों की संख्या में वृद्धि हो रही है।
स्वीकृत बेडों की संख्या दुगुने-तिगुने मरीज वार्डो में भर्ती
है। बेडों के अभाव में मरीज जमीन पर लेट कर इलाज करा रहे हैं।
अस्पताल सूत्रों से मिली जानकारी के अनुसार प्रभारी
अधीक्षक तबीयत खराब का बहाना बनाकर घर पर आराम फरमाते
हैं और घर पर ही अस्पताल से संचिका मंगा कर उसका
निष्पादन करते हैं अस्पताल के विकास कार्यों की
जानकारी मांगे जाने पर वह अनभिज्ञता व्यक्त करते हैं। झारखंड
सरकार राजेन्द्र मेडिकल कालेज अस्पताल को एम्स की तर्ज
पर बनाने के लिए प्रयत्नशील है। </p><p>
े</p>

<p> </p>

<p>स्वदेशी जागरण
मंच की बैठक           </p>

<p> </p>

<p>              </p><p>
देवघर : देवघर कॉलेज के शिक्षक
और स्वदेशी जागरण मंच के देवघर जिला संयोजक प्रो. प्रदीप
गुप्ता के स्टेशन रोड स्थित आवास पर स्वदेशी जागरण मंच
पदधारियों, कार्यकर्ताओं, सदस्यों और मंच से जुड़ने
के इच्छुक स्वदेशी विचारधारा के लोगों की बैठक प्रो.
गुप्ता की अध्यक्षता में हुई। इस अवसर पर समाजसेवी और
संताल परगना स्वदेशी जागरण मंच के प्रमंडलीय संयोजक
पंकज सिंह भदौरिया (देवघर) और बन्दे शंकर सिंह (टाटा) ने
विचार व्यक्त करते हुए स्वदेशी वस्तुओं के लाभ एवं देश के
विकास पर प्रकाश डाला। इस बैठक में आगामी २१ जुलाई
को झारखंड प्रदेश स्तर की स्वदेशी जागरण मंच की बैठक
को सफल बनाने हेतु कार्य योजना बनायी गई और
जिला स्तरीय समिति के गठन करने पर विचार किया गया।
इस अवसर पर सीतावी मंडल, आर.आर.एस. विभाग संचालक, डा.
गोपाल वर्णवाल, जिला प्रचारक, आमित कुमार, कुन्दन
कुमार, शेक्षर कुमार, प्रमोद मालवीय, सच्चिदानंद केशरी, सदस्य
रीतेश कुमार, आनिल कुमार मंडल आदि उपस्थित थे। इस अवसर
पर टाटा से आये बन्दे शंकर सिंह ने स्वदेशी मंच के उद्देश्य पर
प्रकाश डालते हुए कहा कि आज विदेशी कम्पनियां भारत का
विनाश पर तुली है। भारत सरकार सबसिडी देना किसानों को
ामश: बंद करती जा रही है। विदेशी वस्तुओं के लोभ में हम
अपना आस्तित्व खोते जा रहे हैं और अगर इसी प्रकार विदेशी
वस्तुओं के लोभ में पड़ते रहे तो देश तो गुलाम होने से
कोई नहीं रोक सकता है। डा. गोपाल वर्णवाल ने कहा
कि भारतीयों में पहले जापान की महिलाओं की तरह
जागरूकता पैदा करनी होगी। जापान का उदाहरण देते हुए डा.
वर्णवाल ने कहा कि जापान ने अमेरिका का सेव खरीदने
से इंकार कर दिया। भदौरिया ने कहा कि वन विभाग द्वारा जो
वृक्षारोपण किया जाता है, उसमें विदेशी वृक्षों को नहीं
लगाने का अनुरोध किया और टी.वी. चैनल पर विदेशी
कम्पनियों द्वारा जो अश्लील विज्ञान दिये जाते हैं, उस पर रोक
लगे, जंगल और सड़क किनारे फलदार वृक्षों और साल, सखुआ,
बांस लगाये जाएं ताकि जमीन की उर्वरा शक्ति बची रहे
और गरीब जनता को मिल सके। प्रो. प्रदीप गुप्ता ने कहा
कि इस मंच का कार्य क्षेत्र सिर्फ स्वदेशी सामान
अपनाने और विदेशी सामान का बहिष्कार करने तक नहीं है
बल्कि सरकार को बाध्य करना है कि ऐसी आर्थिक नीतियां
बनाये जिनमें देशी व्यापार तथा उद्योग धंधे पर कुठाराघात
न हो। </p><p>
 </p>

<p>बैंकों के
ग्राहक सेवा केन्द्र की मासिक बैठक में विचार-विमर्श        </p>

<p> </p>

<p>                    </p><p>
रांची : बैंक आफ इंडिया (पटना
अंचल) के तत्वावधान और संयोजन में बिहार एवं झारखंड राज्यों
हेतु बैंकों के ग्राहक सेवा केन्द्र की मासिक बैठक १६
जुलाई को बैंक आफ इंडिया के रांची स्थित आंचलिक कार्यालय
में सम्पन्न हुई। बैठक की अध्यक्षता समिति के अध्यक्ष
एवं बैंक आफ इंडिया, पटना अंचल के आंचलिक प्रबंधक एवं
उप महाप्रबंधक श्री पद्म प्रकाश जैन ने की। बैठक में
भारतीय रिजर्व बैंक सहित अन्य सभी सरकारी वाणिज्यिक
बैंकों के प्रतिनिधि उपस्थित थे। समिति मूल रूप से बदलते
परिवेश में बैंकिंग उद्योग को अन्तरराष्ट्रीय स्तर तक ले जाने
हेतु तथा ग्राहकों की शिकायतों एवं परेशानियों को दूर
करने हेतु सतत् प्रयास करती रही है। अध्यक्षीय सम्बोधन में
सभी प्रतिनिधियों का स्वागत करते हुए श्री पद्म प्रकाश जैन
ने कहा कि यथासंभव नियमों और विहित प्रायिा के
अन्तर्गत कार्य करते हुए बैंकों को चाहिए कि पूरे
अपनत्व के भाव के साथ वे ग्राहकों को अबाध सेवाएं प्रदान
करें। वैश्वीकरण के कारण ग्राहकों की अपेक्षाएं काफी बढ़
गई हैं, जिससे बैंकों के समक्ष नई-नई चुनौतियां
आ खड़ी हुई हैं। उन्होंने कहा कि ग्राहकों से मिलने वाली
शिकायतें शून्य स्तर तक आ जाएं, इसके लिए समिति पूरा प्रयास
करेगी। इसके बाद बैंकवार शिकायतों की समीक्षा की
गई और उन्हें दूर करने हेतु समयबद्ध योजना पर जोर दिया गया।
</p><p>
विभिन्न बैंकों के
प्रतिनिधियों ने भी अपने-अपने विचार रखे तथा आशा व्यक्त
की कि बैंक पूरी आस्था के साथ ग्राहक सेवा देंगे। समिति
के सचिव विाम जीत मिश्र ने सभी बैंकों से ग्राहकों की
शिकायतों को निपटाने हेतु अद्यतन स्थिति की जानकारी
ली एवं बढ़ती हुई शिकायतों पर चिन्ता व्यक्त की।</p>

<p> </p>

<p>रांची
विश्वविद्यालय का वर्ष २००२-२००३</p><p>
का अनुमानित बजट     </p>

<p> </p>

<p>              </p><p>
रांची : रांची विश्वविद्यालय ने
वर्ष २००२-२००३ के अपने बजट में स्नातकोत्तर विभाग,
स्नातकोत्तर केन्द्र (चाईबासा) २८ अंगीभूत महाविद्यालयों, ६
नवांगीभूत महाविद्यालयों तथा अल्पसंख्यक महाविद्यालयों हेतु
वेतनादि के लिए अनुदान तथा सम्बद्ध प्राप्त महाविद्यालयों के
शिक्षकों के वेतन और भत्ता मद में करीब ३३.१२ करोड़ रुपये
के वार्षिक व्यय का अनुमान लगाया है। </p><p>
रांची विश्वविद्यालय
का वर्ष २००२-२००३ के अनुमानित बजट में शिक्षकों के
वेतनमद व भत्ता के एवज में खर्च होने वाली राशि का
कालेजवार विवरण </p><p>
ाम. कालेज का नाम
कार्यरत शिक्षकों का कार्यरत शिक्षकों कुल
सी.पी.एफ. कुल </p><p>
सं. मूल वेतन का
भत्ता </p><p>
२१. बहरागोड़ा कालेज,
बहरागोड़ा ५८५,९००.०० १,५११,१००.०० २,०९७,०००.०० ५८,५९०.०० २,१५५,५९०.००
</p><p>
२२. घाटशिला कालेज,
घाटशिला १,३६६,२००.०० ३,३५५,४४०.०० ४,७२१,६४०.०० १३६,६२०.०० ४,८५८,२६०.००
</p><p>
२३. काशी साहु
कालेज, सरायकेला १,२०२,१००.०० २,९६७,१५६.०० ४,१६९,२५६.०० १२०,२१०.००
४,२८९,४६६.०० </p><p>
२४. एल.बी.एस.एम.
कालेज, जमशेदपुर १,३७८,८००.०० ३,५६५,५००.०० ४,९४४,३००.०० १३७,८८०.००
५,०८२,१८०.०० </p><p>
२५. को-आपरेटिव
कालेज, जमशेदपुर ४,१७१,५००.०० १०,८४५,०९६.०० १५,०१६,५९६.०० ४१७,१५०.००
१५,४३३,७४६.०० </p><p>
२६. वर्कर्स
कालेज, जमशेदपुर २,०१६,९००.०० ५,३४०,३३६.०० ७,३५७,२३६.०० २०१,६९०.००
७,५५८,९२६.०० </p><p>
२७. जमशेदपुर वीमेंस
कालेज, जमशेदपुर २,३३३,७००.०० ६,२५६,२९५.०० ८,५८९,९९५.०० २३३,३७०.००
८,८२३,३६५.०० </p><p>
२८. बी.वी.पी. जनता
कालेज, जमशेदपुर ७७१,०००.०० १,९६२,७४४.०० २,७३३,७४४.०० ७७,१००.००
२,८१०,८४४.०० </p><p>
२९. जी.एस. कालेज
फार वीमेंस, जमशेदपुर २,०८६,५००.०० ५,३८८,३९६.०० ७,४७४,८९६.०० २०८,६५०.००
७,६८३,५४६.०० </p><p>
३०. सिंहभूम कालेज,
चांडिल ८४२,१००.०० २,१७२,७५६.०० ३,०१४,८५६.०० ८४,२१०.०० ३,०९९,०६६.०० </p><p>
नवांगीभूत कालेज
</p><p>
१. मांडर कालेज, मांडर
३,३००,०००.०० ९,८६६,१००.०० १३,१६६,१००.०० ३३०,०००.०० १३,४९६,१००.०० </p><p>
२. के.सी. भगत
कालेज, बेड़ो २,३३३,७००.०० ६,२६६,२००.०० ८,५९९,९००.०० २३३,३७०.००
८,८३३,२७०.०० </p><p>
३. बी.एन. जालान
कालेज, सिसई २,०६०,४००.०० ५,९३४,०००.०० ७,९९४,४००.०० २०६,०४०.००
८,२००,४४०.०० </p><p>
४. पी.पी. कालेज,
बुण्डू २,९७६,६००.०० ८,६९६,१००.०० ११,६७२,७००.०० २९७,६६०.०० ११,९७०,३६०.०० </p><p>
५. ए.बी.एम. कालेज,
जमशेदपुर १,९६७,४००.०० ५,९४८,९१६.०० ७,९१६,३१६.०० १९६,७४०.०० ८,११३,०५६.००
</p><p>
६. एस.एस.जे.एस.एन.
कालेज, गढ़वा ३,०१०,८००.०० ९,०९९,३००.०० १२,११०,१००.०० ३०१,०८०.००
१२,४११,१९०.०० </p><p>
अल्पसंख्यक एवं
घाटानुदान कालेजों के शिक्षकों का वेतन एवं भत्ता में होने
वाला वार्षिक अनुमानित व्यय </p><p>
१. संत जेवियर्स
कालेज, रांची २,६४५,७००.०० ७,४६३,१३६.०० १०,१०८,८३६.०० २६४,५७०.००
१०,३७३,४०६.०० </p><p>
२. गोस्समनर कालेज,
रांची २,०२४,७००.०० ५,१००,४५६.०० ७,१२५,१५६.०० २०२,४७०.०० ७,३२७,६२६.०० </p><p>
३. निर्मला कालेज,
रांची १,३८६,०००.०० ३,६३९,९८४.०० ५,०२५,९८४.०० १३८,६००.०० ५,१६४,५८४.०० </p><p>
४. योगदा सत्संग
कालेज, रांची १,५३९,९००.०० ४,२६०,३९६.०० ५,८००,२९६.०० १५३,९९०.००
५,९५४,२८६.०० </p><p>
५. मौलाना आजाद
कालेज, रांची १,०६५,३००.०० २,८८६,०८४.०० ३,९५१,३८४.०० १०६,५३०.००
४,०५७,९१४.०० </p><p>
६. करीम सिटी
कालेज, जमशेदपुर १,५८४,०००.०० ४,३५९,६९६.०० ५,९४३,६९६.०० १५८,४००.००
६,१०२,०९६.०० </p><p>
७. एस.के. बागे
कालेज, कोलेबिरा १८९,६००.००  ५११,२००.०० ७००,८००.०० १८,९६०.००
७१९,७६०.०० </p><p>
८. पी.वी.ए.इ. मेमोरियल
कालेज, चैनपुर ७२२,७००.०० १,८१५,९८४.०० २,५३८,६८४.०० ७२,२७०.००
२,६१०,९५४.००</p>

<p> </p>

<p> </p>

<p> </p>

<p>राजधानी में १००
करोड़ रुपए से आधिक का कारोबार प्रभावितए   </p>

<p> </p>

<p>              </p><p>
रांची: राजधानी की कारोबारी
और औद्योगिक गति को आज १०० करोड़ रुपए से ज्यादा का
झटका लगा। डोमिसाइल के विरोध में आहूत झारखंड बंद से
रांची की तमाम कारोबारी गतिविधियां ठप्प हो गई।
पंडरा कृषि बाजार प्रांगण, अपर बाजार अनाज पट्टी, थोक एवं
खुदरा वस्त्र व्यापार, आयरन एवं स्टील कारोबार का हरएक क्षेत्र शून्य
पर अटक गया। व्यापारिक और औद्योगिक क्षेत्र के साथ साथ
वित्तीय क्षेत्र से जुड़ी तमाम गतिविधियां २४ घंटे बाद पुनः
करोड़ों रुपए के टर्न ओवर को रोकेगी क्योंकि २०
जुलाई को डोमिसाइल के समर्थन में बन्दी है।
संगठनों की विवशता और निराशा स्पष्ट झलकती नजर
आई। इतना ही नहीं, बल्कि आम हड़ताल की घोषणा से
हड़ताल के दिन तक रोजाना कारोबार का ग्राफ घटता रहा। क्योंकि
राजधानी के इर्दगिर्द की मंडियों से ग्राहकों का आना
प्रभावित हुआ। पंडरा कृषि बाजार प्रांगण की थोक अनाज
मंडी सुनसान पड़ी रही जहां ट्रकों, व्यापारियों और मजदूरों
का कोलाहल रहता था। सूत्रों के अनुसार यहां रोजाना स्थानीय
एवं बाहरी व्यापारियों के मार्फत लगभग ३५ से ४० करोड़ रुपए
का टर्न ओवर था जो आज घटकर शून्य पर आ गया। रांची
की थोक कपड़ा मंडी, जहां सम्पूर्ण झारखंड के अलावा
बिहार, मध्यप्रदेश और उड़ीसा के व्यापारी आते थे, के शटर डाउन
रहे। विशेषज्ञों के अनुसार एक दिन में १५ से २० करोड़ रुपए
का कारोबार होता है जो आज हासिए पर आ गया। राजधानी एवं
आसपास में निर्माण गतिविधियां काफी बढ़ रही है जहां
'रॉ मेटेरियल' और लेबर चार्ज के रूप में पांच से सात
करोड़ रुपए का आदान-प्रदान होता है, पूरी तरह ठप्प रहा।
वित्तीय संस्थानों एवं सेयर कारोबार से कई करोड़ रुपए का
लेनदेन होता था वह भी शत प्रतिशत बन्द रहा। इसके अलावा हर
एक क्षेत्र में फैली दुकानों, शोरूमों, आफिसों के ताले नहीं
खुले जहां ४० से ५० करोड़ रुपए का रोजाना कारोबार होता था।
आज के बंदी के आलोक में झारखंड व्यवसायी संघ के संयोजक
प्रेम मित्तल ने प्रेस बयान में कहा कि संघ ने प्रस्तावित दोनों
बंदी में प्रतिष्ठान बंद रखने का निर्णय लिया है। जिसका
आज सभी व्यापारिक क्षेत्रों में पलायन किया गया। संघ ने १८
एवं २० जुलाई के झारखंड बंद में कारोबार बंद रखने का
निर्णय इसलिए लिया कि डोमिसाल मामले में दो भागों में
विभाजित गुटों को आपस में टकराने से रोका जा सके और
शांति बनी रहे। </p><p>
 </p>

<p>ग्रामीणों की
पिटाई से अपराधी की मौत</p>

<p>              </p><p>
रांची : चैनपुर थाना के ऊपर
डुमरी गांव में एमसीसी के नाम पर लेवी भसूलने का प्रयास
कर रहे पौलुस टोप्पो नामक एक अपराधी को स्थानीय
ग्रामीणओं ने पीट-पीटकर मार डाला। मृतक पैलुस टोप्पो रायडीह
थाना क्षेत्र के नीचे डुमरी गांव का रहने वाल था। प्राप्त
जनकारी के अनुसार मृतक पौलुस टोप्पो अपराधियों का एक
गिरोह बनाकर एमसीसी के नाम पर आसपास के गांवों में
लेवी वसूला करता था। परसो वह अपने साथियों के साथ ऊपर
डुमरी लेवी वसूलने गया था। इसी दौरान ग्रामीणों ने रणजीत
बाड़ा, जगमोहन मिंज (दोनों ग्राम चिरगांव थाना रायडीह) व
पौलुस टोप्पो को पकड़ लिया। ग्रामीणों ने पौलुस टोप्पो
की जमकर पिटायी की, जिससे उसकी मौत घटनास्थल पर ही
हो गयी। ग्रामीणों ने बाकी दो अपराधियों रंजीत बाड़ा व
जगमोहन मिंज को चैनपुर पुलिस के हवाले कर दिया। इधर
मृतक पौलुस टोप्पो की पत्नी मिलोन टोप्पो का कहना है
कि परसों चार आदमी उसके घर आये थे और उसके पति को
बुलाकर अपने साथ ले गये। उनमें से दो लोगों को उसने
पहचाना। इसमें एक का नाम एथेर दूसरे का नाम इलिया है। मृतक
पौलुस टोप्पो के विरुद्ध चैनपुर व डुमरी थाने में पहले से ही
आपराधिक मामले दर्ज हैं और दोनों थाना क्षेत्र की पुलिस
को पौलुस की तलाश थी। चैनपुर पुलिस ने गिरफ्तार
दोनों अपराधियों रंजीत बाड़ा व जगमोहन मिंज को गुमला
जेल भेज दिया। </p><p>
 </p>

<p>पाकिस्तान को
आतंकवादी देश घोषित करने की मांग                    </p>

<p></p><p>
पाकिस्तान: जम्मू में गत तेरह
जुलाई की शाम को आतंकवादियों द्वारा करीब तीन
दर्जन लोगों की हत्या पर लोकसभा में मंगलवार को दिये
गये अपने वक्तव्य में उप-प्रधानमंत्री लालकृष्ण आडवाणी ने
सरकार का संकल्प व्यक्त करते हुए कहा कि भारत अपने दम पर
आतंकवाद को कुचल देगा। लेकिन आडवाणी यह बात इतनी
बार दोहरा चुके हैं कि यह संकल्प अब अपनी अहमियत काफी
खो चुका है। अब लोग भाषण और आश्वासन नहीं आतंकवाद
और उसे संरक्षण देने वालों के विरुद्ध सीधी कार्रवाई
चाहते हैं। लेकिन दुर्भाग्य से भारत सरकार आतंकवादी हिंसा
की हर घटना पर दो कदम आगे, तीन कदम पीछे हटने की
नीति पर चल रही है। श्री आडवाणी ने आतंकवाद से अपने
बूते लड़ने का संकल्प व्यक्त करने के ाम में प्रकारान्तर से
अमेरिका से मांग की कि पाकिस्तान बार-बार वादों के
बावजूद सीमा-पार आतंकवाद नहीं रोकता है तो वह उसे
आतंकवादी देश घोषित कर दे। अमेरिका से श्री आडवाणी
की यह अपेक्षा कहीं से भी गलत नहीं है। ऐसा इसलिए कि
अफगानिस्तान में तालिबान विरोधी संघर्ष के दौरान
अमेरिका ने आतंकवाद पीड़ित विश्व के सभी देशों को
आतंकवाद से राहत दिलाने का वादा किया था। हालांकि
तालिबान विरोधी संघर्ष में भारत के एतराज के बावजूद
पाकिस्तान को शामिल करने के बाद ही यह बात स्पष्ट हो गयी
थी कि आतंकवाद के विरुद्ध भारत को अपनी लड़ाई स्वयं
लड़नी होगी। पिछले महीने अमेरिका के विदेश उपमंत्री
आर्मिटेज इस्लामाबाद होते हुए भारत आये थे। उन्होंने सीमा-पार
आतंकवाद रोकने संबंधी मुशर्रफ के वादे की बात भारत
को बतायी थी पर आर्मिटेज की भारत यात्रा के बाद भी
आतिामित कश्मीर से अलकायदा सहित विभिन्न संगठनों
के कोई २२५० आतंकवादी जम्मू-कश्मीर में घुस चुके हैं
और घुसपैठ अभी भी जारी है। इसमें अब कुछ कमी जरूर आयी
है लेकिन घुसपैठ पूरी तरह बंद नहीं हुई है, घुसपैठ
रोकने में मुशर्रफ की दिलचस्पी भी नहीं है। इसलिए श्री
आडवाणी ने अमेरिका से पाकिस्तान को आतंकवादी देश
घोषित करने की प्रकारांतर से मांग की है तो वह
गैरवाजिब नहीं है। लेकिन इसके लिए जरूरी है कि विश्व
समुदाय का समर्थन हासिल किया जाये जबकि भारत सरकार
की ओर से ऐसा कोई प्रयास दिखायी नहीं देता। दूसरी बात
यह है कि आतंकवाद से जब सरकार अपने ही दम पर लड़ने का
संकल्प व्यक्त करती है तो उससे अपेक्षा की जाती है कि
पहले वह स्वयं पाक को आतंकवादी देश घोषित करने की
पहल करे। भारत सरकार जब तक स्वयं ऐसा नहीं करती तबतक
किसी अन्य देश से ऐसी कार्रवाई की अपेक्षा करना
उचित नहीं होगा। कोई दूसरा देश हमारी लड़ाई नहीं लड़ना
चाहेगा। </p>

<p> </p>






</body></text></cesDoc>