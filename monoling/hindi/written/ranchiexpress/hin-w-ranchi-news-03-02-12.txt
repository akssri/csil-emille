<cesDoc id="hin-w-ranchi-news-03-02-12" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-ranchi-news-03-02-12.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Celia Worth and Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Internet version of the "Ranchi Express" newspaper (www.ranchiexpress.com), news stories collected on 03-02-12</h.title>
<h.author>Ranchi Express Group</h.author>
<imprint>
<pubPlace>Ranchi, Jharkhand, India</pubPlace>
<publisher>Ranchi Express Group</publisher>
<pubDate>03-02-12</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>सबके
लिए स्वच्छ जल </p>

<p>पृथ्वी का तीन चौथाई
भाग जल से पूर्ण होने के बावजूद यह एक विडम्बना ही है
कि आज के वैज्ञानिक युग में भी स्वच्छ जल का घोर अभाव
उत्पन्न हो गया है और यह क्रमशः एक समस्या का रूप धारण करता
जा रहा है, इस ओर गंभीरता से चिन्तन करने की आवश्यकता
उत्पन्न हो गयी है। संयुक्त राष्ट्र द्वारा वर्ष २००३ को 'अन्तरराष्ट्रीय
स्वच्छ जल वर्ष' के रूप में घोषित किया जाना इसी
दिशा में उठाया गया एक महत्वपूर्ण कदम है। किसी
विषय, समस्या अथवा क्षेत्र विशेष के प्रति आम जन में
जागरुकता उत्पन्न करने और वैश्विक रूप से उस दिशा में बेहतर
कार्य करने के उद्देश्य से ही विशेष वर्ष घोषित
किया जाता है। अब अन्तरराष्ट्रीय स्वच्छ जल वर्ष में भी हर
देश की सरकार और आम जनता जल संरक्षण, जल संग्रहण और जल
के कुशल और समुचित उपयोग पर ध्यान देंगी, इतनी आशा
तो की ही जानी चाहिए। इस समय स्थिति यह है कि विश्व के
अनेक प्रगतिशील और आविकसित देशों में न तो स्वच्छ जल
के महत्व को समझा जाता है और न पर्याप्त मात्रा में वहां
स्वच्छ जल ही उपलब्ध है। विकसित देशों में भी सामान्य रूप से
स्वच्छ जल की समस्या बनी हुई है। संभवतः यही कारण है कि
बोतलबंद पेयजल की मांग बढ़ी है। लेकिन विडम्बना यह है
कि बोतलबंद जल भी अब स्वच्छ और सुरक्षित नहीं। जीवन के
लिए एक महत्वपूर्ण और आवश्यक तत्व, जल जीवन के
विनाश का कारण बने और वह भी मानव जनित कारणों से,
तो यह निश्चय ही लज्जाजनक स्थिति होगी। अतः जल की
स्वच्छता के लिए हर व्यक्ति पूरी गंभीरता से प्रयासरत हो और
सबको स्वच्छ जल उपलब्ध हो, तभी स्वच्छ जल वर्ष की
सार्थकता सिद्ध होगी।</p>

<p> </p>






</body></text></cesDoc>