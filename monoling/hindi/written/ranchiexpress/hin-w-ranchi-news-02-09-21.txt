<cesDoc id="hin-w-ranchi-news-02-09-21" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-ranchi-news-02-09-21.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Celia Worth and Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Internet version of the "Ranchi Express" newspaper (www.ranchiexpress.com), news stories collected on 02-09-21</h.title>
<h.author>Ranchi Express Group</h.author>
<imprint>
<pubPlace>Ranchi, Jharkhand, India</pubPlace>
<publisher>Ranchi Express Group</publisher>
<pubDate>02-09-21</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>पीट-पीट कर छह
ग्रामीणों की हत्या                                                         </p>

<p> </p>

<p>              </p><p>
चौपारण
(हजारीबाग) :
चौपारण थाना से करीब १५ किलो मीटर दूर वृन्दावन पंचायत
अन्तर्गत ग्राम असनाचुआं के दुर्गम जंगल में २० सितंबर
को बैलगाड़ियों पर जलावन की लकड़ियां लाने गये कई
ग्रामीणों की एमसीसी उग्रवादियों द्वारा हाथ-पैर बांधकर
लाठियों से जबर्दस्त पिटाई किए जाने के कारण चार
ग्रामीणों की घटनास्थल पर ही मौत हो गई जबकि दो
ग्रामीणों ने बाद में दम तोड़ा। इस घटना में कई लोग बुरी
तरह घायल हो गए। उल्लेखनीय बात यह है कि घटना सुबह आठ
बजे घटी होने के बावजूद शाम ८ बजे तक न तो वहां पुलिस
पहुंच पायी, न ही कोई रिपोर्ट थाने में दर्ज करायी
गई। दूसरी ओर मृतक ग्रामीणों के परिवारों में मातम तथा
घायलों के परिवारों में खलबली मची हुई है। मारे गए
लोगों में इंगुनियां ग्राम के सुशील सिंह(३३) तथा खड़गधारी
साव(४२) शामिल हैं। अन्य किस गांव के हैं और कौन हैं इसकी
पहचान अभी तक नहीं हो सकी है। सभी के शव जंगल में ही
पड़े बताए जाते हैं। आशंका है कि कुछ घायल ग्रामीण भी वहां
पड़े हुए हैं। जिनके परिवार के लोग जंगल से घर नहीं लौटे हैं,
उनमें भारी घबड़ाहट तथा दहशत व्याप्त है, उनके नामों का पता
चल गया है। वे हैं कृपाल सिंह(६०) तथा उनका पुत्र डब्लू सिंह(२२)
और पच्चु सिंह(३०)। प्रथम दो लोग ग्राम बन्ने के तथा अंतिम
ग्राम केदली का है। बाद में ग्रामीणों ने बताया कि इस हादसे में
घायल कृपाल सिंह और उसके पुत्र डबलू सिंह की भी मृत्यु
हो गयी। दोनों बुरी तरह जख्मी थे। घटना स्थल से जान बचा कर
भाग आए मृतक खड़गधारी साव के १२ वर्षीय पुत्र मतल साव
ने उपयुक्त हत्याकांड की जानकारी गांव में जा कर दी। इसी
दौरान उधर से गुजर रहे पुलिस गश्ती दल को भी ग्रामीणों ने
घटना की सूचना दी। उग्रवादियों के चंगुल से भाग निकलने
वाला एक व्यक्ति प्रभु सिंह भी है। उसके अनुसार सबसे पहले
उसी का हाथ पैर बांधा गया था, लेकिन वह बंधन खोल कर
भाग निकला। उग्रवादी, जो खाकी वर्दियों में थे और हथियारों
से लैश थे, की संख्या लगभग २० बतायी जाती है। हजारीबाग
के एस.पी. आर.के. मल्लिक, बरही डीएसपी हेमन्त टोप्पो तथा
बरही के इन्सपेक्टर राजनाथ सिंह शाम सात बजे तक चौपारण
थाना में ही घटनास्थल की ओर जाने की तैयारी ही करते देखे
गए। मृतकों, घायलों तथा लापता ग्रामीणों के गांवों मातम, भय
तथा दहशत माहौल कायम है। शाम सात बजे चौपारण थाने में
फोर्स का इंतजार कर रहे हजारीबाग के एसपी ने घटनास्थल
की ओर अभी तक प्रस्थान नहीं करने का कारण स्पष्ट करते हुए
बताया कि मार्ग काफी दुर्गम है और सतगांवा की तरह
कोई वाकया रास्ते में न हो जाय इसलिए सतर्कता बरती
जा रही है। पुलिस फोर्स आ रही है। उसके पहुंचते ही
घटनास्तल की ओर प्रस्थान किया जाएगा। चौपारण थाना परिसर
में ग्रामीण भी बड़ी संख्या में जमा होकर घटनास्थल पर पुलिस
के शीघ्र से शीघ्र पहुंचने की मांग कर रहे थे।</p>

<p> </p>

<p>डीएवी हेहल में
उपद्रव, बस को क्षतिग्रस्त किया      </p>

<p> </p>

<p>              </p><p>
रांची : डीएवी स्कूल के कुछ
छात्रों ने आज कल की घटना को लेकर कठर मोड़ स्थित
डीएवी शाखा में तोड़फोड़ और नारेबाजी की। तोड़फोड़ के
क्रम में उत्तेजित छात्रों ने वहां खड़ी एक बस के शीशे तोड़
डाले और बस को बुरी तरह क्षतिग्रस्त कर दिया। यह घटना आज
दिन के लगभग साढ़े दस बजे घटी। घटना के बाद स्कूल परिसर
में अफरा-तफरी मच गयी। बाद में स्कूल शिक्षकों एवं अन्य
कर्मचारियों ने स्थिति को सम्भाला। उत्तेजित छात्र कठर
मोड़ स्थित डीएवी में तोड़फोड़ करने के बाद हेहल स्थित थ्री
वेस्टर्न पार्क स्थित डीएवी पहुंचे। नौ मौटरसाइकिलों
में तीन-तीन लड़के सवार थे। वे वहां कुछ क्षण रुके,
परिस्थिति को भांपते हुए वहां नारेबाजी करते हुए निकल गये।
कल और आज की घटना से डीएवी हेहल के छात्र-छात्राएं भयभीत
हैं। खासकर डीएवी बसों में स्कूल जाने वाले विद्यार्थी डरे सहमे
हैं। उन्होंने डर है कि किसी दिन कोई भी आप्रिय घटना घट
सकती है। स्कूल में फैले तनाव को देखते हुए हेहल डीएवी
प्रबंधन ने कल स्कूल बंद रखने की घोषणा की है तथा
डीएवी हेहल से निजी सुरक्षा प्रहरियों को हटाने का निर्णय
लिया है। उधर कल सुरक्षाकर्मी की गोली से घायल रवि प्रकाश
सिंह की हालत में सुधार हो रहा है। डीएवी हेहल स्कूल में कल हुए
गोलीकांड के कारण रांची स्थित अन्य डीएवी स्कूलों के
छात्रों में आज काफी आक्रोश देखा गया। उनका कहना है कि
सुरक्षा प्रहरी ने जानबुझकर छात्र को गोली मारी है। छात्रों ने
कहा कि उक्त सुरक्षा प्रहरी पहले भी छात्रों पर गोली चला चुका
है और यह संयोग ही है कि उसकी गोली से पहले कोई
घायल नहीं हुआ फिर भी उस समय उस सुरक्षा प्रहरी को हटाया
नहीं गया। बताया गया कि इस घटना के बाद से उक्त स्कूल में
पढ़ रहे छात्र-छात्राओं के आभिभावक भी काफी सहमे हुए हैं। वे
भी इस घटना को लेकर काफी चिन्तित हैं तथा कुछ दिनों
तक अपने बच्चों को स्कूल न भेजने का विचार कर रहे हैं।
डीएवी प्रबन्धन ने वर्तमान परिस्थिति को देखते हुए कल
विद्यालय को बंद रखने का निर्णय लिया है। डीएवी रांची
क्षेत्र के निदेशक एल.आर. सैनी ने कहा कि वर्तमान स्थिति
को देखते हुए कल विद्यालय बंद रखा जायेगा और सोमवार से
पुन: आम दिनों के तरह स्कूल जारी रहेगा। उन्होंने बताया कि
विद्यालय से निजी सुरक्षा प्रहरियों को हटा दिया गया है। अब
विद्यालय के कर्मचारी ही प्रहरी का कार्य करेंगे वे भी
बिना हथियारों के। स्कूल के शिक्षक, कर्मचारी न तो
छात्र-छात्राओं के साथ अच्छा व्यवहार करते हैं और उनके
आभिभावकों के साथ। क्रोधित लड़कों ने वहां खड़े डीएवी
हेहल के एक बस को बुरी तरह क्षतिग्रस्त कर दिया।</p>

<p> </p>

<p>सूखे के लिए
केंद्र अलग से राशि नहीं देगा             </p>

<p> </p>

<p>                 </p><p>
रांची : झारखंड में सूखे के
आकलन के लिये आये केन्द्रीय दल की प्रमुख नीता चौधरी
ने आज यहां स्पष्ट किया कि राज्य में सूखे की स्थिति से
निपटने के लिये केन्द्र सरकार द्वारा अलग से राशि उपलब्ध
कराने की न तो योजना है और न ही इस प्रकार का कोई
प्रावधान ही है। उन्होंने कहा कि राज्य सरकार को सूखे से
निपटने के लिये युद्धस्तर पर कार्य शुरू करना चाहिये,
इसके लिये राशि की कोई कमी नहीं है, क्योंकि केन्द्र
सरकार द्वारा विभिन्न मदों में दी गयी राशि को खर्च नहीं
किया गया है। राज्य के सूखा प्रभावित क्षेत्रों का दौरा कर
लौटने और प्रोजेक्ट बिल्डिंग में राज्य के वरिष्ठ आधिकारियों
के साथ बैठक के पश्चात संवाददाताओं से बातचीत करते
हुए नीता चौधरी ने उक्त बातें कही। उन्होंने स्वीकार किया
कि राज्य में सूखे की स्थिति चिंताजनक है। उन्होंने कहा कि
केन्द्रीय दल ने गोला, पेटरवार, हजारीबाग आदि क्षेत्रों के भ्रमण
के बाद पाया कि सितम्बर माह में बारिश होने के बावजूद मात्र
२५ से ३० प्रतिशत ही फसल होने की संभावना है। किसानों
को होने वाली क्षति की भरपायी खरीफ फसल की अच्छी
उपज होने पर हो सकती है। इसके लिये केन्द्र हर संभव मदद
देगा। राज्य सरकार द्वारा सूखे की स्थिति से निपटने के लिये
१५ सौ करोड़ रुपये के पैकेज की मांग के संबंध में पूछे
जाने पर उन्होंने कहा कि इस तरह पैकेज देने का कोई
प्रावधान नहीं है। उन्होंने कहा कि केन्द्र सरकार द्वारा पैकेज उस
स्थिति में दिया जाता है जब भूख से मरने की स्थिति हो,
रोजगार के अवसर सृजित करना हो। उन्होंने कहा कि केन्द्र
सरकार द्वारा विभिन्न मदों यथा ग्रामीण विकास आदि में काफी
राशि आवंटित की गयी है। उन्होंने ग्रामीण विकास का उदाहरण
देते हुये कहा कि केन्द्र सरकार द्वारा दी गयी राशि में से अभी
तक तीन-चार प्रतिशत ही खर्च किया गया है। इस प्रकार
काफी राशि पड़ी हुई है। राज्य सरकार इसका उपयोग सूखे
से निपटने में कर सकती है। सुश्री चौधरी ने कहा कि यदि
इसके बाद राशि की कमी होती है तो केन्द्र सरकार राज्य
सरकार को मदद करेगी। उन्होंने कहा कि अनाज की कमी न
हो, इसके लिये कुछ रियायत दी गयी है। पूर्व में
एफसीआइ द्वारा राज्य को आवंटित अनाज का ६० प्रतिशत खर्च
होने पर ही आगे अनाज का आवंटन होता था। अब इसमें रियायत
देते हुये ४० प्रतिशत कर दिया गया है। सुश्री चौधरी ने दल द्वारा
सूखा क्षेत्रों में किये गये निरीक्षण के बारे में जानकारी देते
हुये कहा कि स्थिति काफी चिंताजनक है। उन्होंने कहा कि
राज्य सरकार द्वारा दी गयी जानकारी के अनुसार राज्य का मात्र ८
प्रतिशत क्षेत्र ही संिचित है। दल ने राज्य सरकार को सुझाव दिया
है कि वह जल संरक्षण विधियों को तेजी से लागू करे तथा
संचिाई सुविधाएं बढ़ाने का प्रयास करे। सुश्री चौधरी ने
बताया कि दल के सदस्य कुछ गांवों में यह देखकर काफी
चिंतित हुये हैं कि वहां स्थित कई कुओं में पानी तो था
पर आसपास के खेत सूखे थे। ग्रामीणों का कहना था कि
सरकार द्वारा पम्पिंग सेट उपलब्ध नहीं कराने के कारण सिंचाई
नहीं हो सकी। उन्होंने कहा कि ग्रामीणों का पूरी तरह सरकार
पर निर्भर रहना उनकी जागरुकता की कमी का परिचायक
है। उन्होंने राज्य सरकार से कहा है कि वह स्वयंसेवी संगठनों
की सहायता से ग्रामीणों में जागरुकता लायें। इसके पूर्व
केन्द्रीय दल ने राज्य के वरिष्ठ आधिकारियों के साथ सूखे की
स्थिति से निपटने के मुद्दे पर बैठक की। बैठक में
मुख्य सचिव जी. कृष्णन, राहत सचिव नरेन्द्र भगत सहित
कई विभागों के सचिव एवं एफसीआइ के वरिष्ठ आधिकारी
शामिल थे। केन्द्रीय दल आज दिल्ली लौट गया। दल सूखे के
संबंध में उपप्रधानमंत्री लालकृष्ण आडवाणी की अध्यक्षता
वाले मंत्रियों के समूह को अपनी रिपोर्ट सौंपेगा।</p>

<p>                 </p>

<p>कुलाधिपति
ने जवाब तलब किया,वस्तुस्थिति से अवगत होंगे             </p>

<p> </p>

<p>              </p><p>
रांची : कुलाधिपति कार्यालय
ने रांची विश्वविद्यालय के अन्तर्गत विभिन्न महाविद्यालयों
में कार्यरत करीब चार हजार शिक्षकों एवं शिक्षकेत्तर
कर्मचारियों को नियम विरुद्ध निर्धारित वेतनमान से
आधिक राशि का भुगतान वेतन के रूप में किए जाने सम्बन्धी
मामले पर रांची विश्वविद्यालय से जवाब तलब करते हुए
वस्तुस्थिति की जानकारी मांगी है। मामले के सम्बन्ध
विश्वविद्यालय प्रशासन से वस्तुस्थिति की जानकारी प्राप्त
होने पर कुलाधिपति को इससे अवगत कराया जाएगा। शिक्षक
एवं शिक्षकेत्तर कर्मचारियों को निर्धारित वेतनमान
से आधिक राशि भुगतान किए जाने के सम्बन्ध में
कुलाधिपति कार्यालय ने रांची एक्सप्रेस में 'लाखों के
नुकसान के बाद रिपोर्ट ठंडे बस्ते में' शीर्षक से
प्रकाशित खबर के आधार पर मांगी है। इस सिलसिले में
कुलाधिपति सह राज्यपाल के विशेष कार्यपालक
पदाधिकारी (न्यायिक) आर.एल. शर्मा ने कुलपति को पत्र
लिखा है। सूत्र बताते हैं कि इस मामले पर कुलाधिपति कार्यालय
काफी गंभीर है। रांची विश्वविद्यालय के अन्तर्गत
विभिन्न महाविद्यालयों में कार्यरत करीब चार हजार शिक्षक
एवं कर्मचारियों को नियम विरुद्ध निर्धारित वेतनमान
से आधिक राशि का भुगतान किया जा रहा है। इससे सरकार और
विश्वविद्यालय को पन्द्रह से बीस लाख रुपये का नुकसान प्रति
माह उठाना पड़ रहा है। इस बात का खुलासा विश्वविद्यालय की
लेखा शाखा ने विश्वविद्यालय प्रशासन को समर्पित अपनी
रिपोर्ट में किया है। रिपोर्ट में सम्बन्धित कालेज के
साथ वैसे शिक्षक एवं कर्मचारियों के नामों का भी उल्लेख
कर दिया गया है, जिन्हें नियम विरुद्ध आधिक राशि का भुगतान
वेतन के रूप में किया जा रहा है। बताया जाता है कि इस सम्बन्ध
प्रभारी कुलपति ने मौखिक रूप से विश्वविद्यालय के लेखा
शाखा को आदेश दिया था कि राज्यादेश और न्यायादेश के
आलोक में कालेजकर्मियों का वेतन निर्धारित कर
भविष्य में उसी के अनुसार वेतन भुगतान की प्रक्रिया
आपनाई जाए। इसके बाद विश्वविद्यालय के लेखा शाखा ने
कुलपति के आदेशानुसार शिक्षक एवं कर्मचारियों का
वेतन निर्धारित कर रिपोर्ट जून में कुलपति को
सौंप दी। लेकिन जानकारी के अनुसार उस समय कुछ करीबी
लोगों की सलाह और दबाव में विश्वविद्यालय प्रशासन ने लेखा
शाखा के द्वारा सौंपी गई उक्त रिपोर्ट को ठंडे बस्ते
में डाल दिया। लेखा शाखा की रिपोर्ट को नजरअंदाज करते
हुए विश्वविद्यालय के वित्त पदाधिकारी को निर्देश दिया गया
कि कालेज के शिक्षक एवं कर्मचारियों को राज्य सरकार
से वेतन मद की राशि आने पर पूर्व की तरह वेतन
भुगतान किया जाए। लेखा शाखा ने अपनी रिपोर्ट में
शिक्षक एवं कर्मचारियों के वेतनमान में आवश्यक
संशोधन करते हुए कहा था कि करीब चार हजार शिक्षकों एवं
कर्मचारियों को नियम विरुद्ध वर्षों से आधिक राशि
भुगतान किए जाने के आलोक में अब उन्हें संशोधित
वेतनमान के आधार पर ही वेतन भुगतान किया जाए। लेकिन
लेखा शाखा की रिपोर्ट के साथ-साथ उसके सुझाव को भी
वि.वि. प्रशासन ने दरकिनार कर दिया। रिपोर्ट में यह भी
अनुशंसा की गई थी कि जिन शिक्षक एवं कर्मचारियों
को आधिक वेतन भुगतान किया गया है उनसे राशि वसूली
जाए। वैसे दूसरे सूत्रों का कहना है कि राज्यादेश और न्यायादेश
का सही तरीके से पालन कर कालेज शिक्षक एवं कर्मचारी
का वेतन निर्धारित कर उसी के अनुरूप भुगतान किया जाए
तो सरकार और विश्वविद्यालय को करीब पचास लाख रुपये
की बचत होगी। फिलवक्त वाइ.एस.एस. कालेज (डालटनगंज),
एस.एस. मेमोरियल कालेज (रांची), जनता शिवरात्रि कालेज,
वर्कर्स कालेज (जमशेदपुर) तथा घाटशिला कालेज में ही
शिक्षक एवं कर्मचारियों को नियम सम्मत राशि का
भुगतान वेतन के रूप में किया जा रहा है।</p>

<p>                   </p>

<p>मुख्यमंत्री के
अधीन होगा स्पेशल टास्क फोर्स             </p>

<p> </p>

<p>              </p><p>
रांची : झारखंड में अपहरण की
घटनाओं पर रोक लगाने एवं कानून व्यवस्था को पुख्ता करने
के उद्देश्य से स्पेशल टास्क फोर्स गठन की प्रक्रिया
प्रारम्भ हो गई है। आरक्षी अधीक्षक स्तर के आधिकारी
एसटीएफ के प्रभारी होंगे। स्पेशल टास्क फोर्स मुख्यमंत्री
के अधीन होगा। उच्च पदस्थ सूत्रों के मुताबिक राज्य सरकार
झारखंड में इन दिनों बढ़ रही अपहरण की घटनाओं एवं बाहर के
राज्यों से आने वाले बड़े अपराधियों के राज्य में प्रवेश की
सूचना मिलने से चिंतित है। मुख्यमंत्री के निर्देश पर
प्रशासनिक आधिकारियों ने देश के अन्य राजयों में गठित
एसटीएफ की तरह झारखंड में भी स्पेशल टास्क फोर्स
गठित करने का निर्णय लिया है। एसटीएफ के आकार
एवं कार्यक्षेत्र तय करने के लिए दूसरे राज्यों से प्रारूप
मंगाने पर विचार मंथन किया गया तथा उक्त प्रारूप को
मंगाने का निर्णय लिया गया। सूचना के मुताबिक
दिल्ली, महाराष्ट्र, आंध्र प्रदेश, कर्नाटक आदि राज्यों से प्रारूप
मंगाने पर विचार किया गया। </p><p>
जानकारी के अनुसार
उन राज्यों से प्रारूप मिलने के पश्चात शीघ्र ही झारखंड स्पेशल
टास्क फोर्स का गठन कर लिया जाएगा। एसटीएफ अपहरण,
फिरौती, रंगदारी, हत्या के मामले में स्थानीय प्रशासन द्वारा मदद
मांगने पर सक्रिय होगी। फोर्स के मिशन अलग से तय
किए जाएंगे। मुख्यमंत्री के अधीन फोर्स पूरी तरह स्वतंत्र
निकाय होगा।</p>

<p> </p>

<p>आद्रा में युवकों
के साथ दुर्व्यवहार मामले में मुख्यमंत्री गंभीर</p><p>
रेल
मंत्रालय और बंगाल सरकार से विरोध व्यक्त करेगी झारखंड
सरकार      </p>

<p> </p>

<p>              </p><p>
रांची : झारखंड के मुख्यमंत्री
बाबूलाल मरांडी ने आद्रा में राज्य के नौजवानों के साथ वहां
के उपद्रवियों द्वारा मार-पीट किए जाने को गंभीरता से लिया
है। श्री मरांडी शीघ्र ही इस संदर्भ में केन्द्रीय रेल मंत्री
नीतीश कुमार एवं पश्चिम बंगाल के मुख्यमंत्री बुद्धदेव भट्टाचार्या
को पत्र लिखेंगे तथा विरोध प्रकट करेंगे। मुख्यमंत्री ने कहा
कि झारखंड के अन्तर्गत पड़ने वाले रेल विभाग के रिक्त
पदों की नियुक्ति हर हाल में झारखंड में ही होनी चाहिए। मुख्यमंत्री
आज 'रांची एक्सप्रेस' के साथ बातचीत कर रहे थे। उन्होंने कहा
कि हटिया एवं रांची में गैंगमैन के रिक्त पदों का साक्षात्कार
आद्रा में कराने का औचित्य ही नहीं है। अगर ऐसा होता भी है
तो वहां के स्थानीय प्रशासन की जिम्मेदारी है कि वे उसे
सफलतापूर्वक सम्पादित कराएं। मगर पिछली दो बार की
घटनाओं से यह लगता है कि पश्चिम बंगाल सरकार इसे
गंभीरता से नहीं ले रही है। उन्होंने कहा कि नियुक्ति की
प्रक्रिया में आरक्षण, स्थानीयता सहित अनेक प्रावधान है
जिसके तहत ही नियुक्ति करनी है। क्षेत्रीयता को आधार
बनाकर किसी के हक को वंचित नहीं किया जा सकता।
सभी राज्यों में नियुक्ति में स्थानीय लोगों को प्राथमिकता
देने की व्यवस्था है। फिर आद्रा में जिस बात को लेकर झारखंड
के नौजवानों के साथ दुर्व्यवहार किया गया वह तो प्रथम गृष्टया
ही गलत है। मुख्यमंत्री ने कहा कि रेल मंत्री नीतीश कुमार
को वे पत्र लिखेंगे तथा इन पदों के लिए सभी प्रक्रियाएं
रांची में आयोजित करने का आग्रह करेंगे। वे कहेंगे कि
चूंकि रांची में रेलवे भर्ती बोर्ड कार्यरत है और हटिया
तथा रांची में गैंगमैन का रिक्त पदों पर नियुक्ति करनी
है, इसलिए झारखंड में ही नियुक्ति स्थल होना चाहिए। इसके
अलावा पश्चिम बंगाल के मुख्यमंत्री बुद्धदेव भट्टाचार्या को
भी पत्र लिखकर आग्रह करेंगे कि वे इस तरह की व्यवस्था
सुनिश्चित करें जिससे झारखंडवासियों को किसी तरह की
परेशानी नहीं हो तथा उनका मौलिक आधिकार का हनन नहीं
हो। यह पूछे जाने पर कि पिछले दिनों डोमिसाइल के मुद्दे पर
झारखंड में हुए उपद्रव का ही पश्चिम बंगाल अनुशरण तो नहीं कर
रहा है, उन्होंने कहा कि ऐसी बात नहीं है। श्री मरांडी ने कहा
कि वे पता करेंगे कि रेल विभाग के किस आधिकारी ने
विधि व्यवस्था को आधार बनाकर रांची के वजाय आद्रा में नियुक्ति
प्रक्रिया प्रारंभ करने का आदेश दिया।</p>

<p> </p>

<p>'मीडिया के
महत्व में वृद्धि के साथ दायित्व भी बढ़ा'            </p>

<p> </p>

<p>               </p><p>
रांची : रांची क्लब रांची ईस्ट
द्वारा आज यहां आयोजित एक समारोह में पत्रकारों को
सम्मानित किया गया। इस अवसर पर सम्मानित किये गये
पत्रकारों को क्लब की ओर से एक स्मृति चिह्न, शॉल और
पुष्प-गुच्छ प्रदान किये गए। समारोह के मुख्य वक्ता 'रांची
एक्सप्रेस' के प्रधान संपादक बलवीर दत्त ने इस अवसर पर कहा
कि वर्तमान समय में पत्रकारों का दायित्व काफी बढ़ गया
है। यह समय ज्ञान के विस्फोट का है। पत्रकारों को सभी
विषयों की जानकारी होनी चाहिए लेकिन किसी एक
विषय का उनसे विशेषज्ञ होना चाहिए। तभी वह अपने दायित्व
का सही रूप में निर्वाह कर सकते हैं। लेकिन इस संबंध में
सबसे खेदजनक बात यह है कि जिस तरह से वर्तमान समय
में पत्रकारिता के महत्व में वृद्धि हुई है उस अनुपात में दायित्व
का अनुपालन नहीं किया जा रहा है। उन्होंने कहा कि प्रेस को
पहले लोकतंत्र का चौथा स्तम्भ माना जाता था लेकिन आज
की परिस्थिति में यह पहला स्तंभ बन गया है। स्वतंत्रता के
वर्ष में इस देश में मात्र तीन हजार पत्र-पत्रिकाएं प्रकाशित
होती थी। लेकिन वर्तमान समय में लगभग पैंतालीस हजार
पत्र-पत्रिकाओं का प्रकाशन किया जा रहा है। उन्होंने कहा कि
समाचार पत्र के प्रकाशन में डेस्क पर काम करने वाले उप
संपादकों को भी काफी मेहनत करनी पड़ती है। लेकिन
नेपथ्य में रहने की वजह से लोग उनकी योग्यता को क्यों
नहीं पहचान पाते। उन्हें भी सम्मानित करने की आवश्यकता है।
मुख्य आतिथि विधि एवं न्याय मंत्री रामजी लाल सारडा ने कहा
कि समाज सेवा के क्षेत्र में पत्रकारों का योगदान काफी
महत्वपूर्ण है। उन्होंने कहा कि अपने कार्यों का निष्पादन
करते समय राष्ट्रहित एवं मानवता की रक्षा का ध्यान अवश्य रखना
चाहिए। समारोह की अध्यक्षता क्लब के अध्यक्ष विष्णु लोहिया
ने की। इस समारोह मंे शादी की सालगिरह पर पूर्व विधायक
गुलशन लाल अजमानी को उपहार देकर शुभकामनाएं दी गयी।
मंच पर राजेश गुप्ता, संजीव पोद्दार आदि उपस्थित थे। धन्यवाद
ज्ञापन ललित केडिया ने किया। इस समारोह में पंकज वासल,
दीवाकर प्रसाद, विजय पाठक, संजय थापकर, ब्रजकिशोर
पांडेय, शमीम अंसारी, नीरज सिंहा, रूपेश, पूरनचंद, रजिंदर सिंह
दीपक, योगेश किसलय, गिरिजा शंकर ओझा, संजय सहाय
तथा धर्मवीर सिन्हा सम्मानित किये गये।</p>

<p> </p>

<p>अधर में लटकी
है चतुर्थवर्गीय कर्मचारियों की किस्मत             </p>

<p> </p>

<p>              </p><p>
पाकुड़ : पाकुड़ समाहरणालय के
कर्मचारियों की बहाली का मामला अभी तक अधर में
लटका हुआ है। इस संबंध में झारखंड उच्च न्यायालय ने दो माह के
अन्दर निष्कासित चतुर्थवर्गीय कर्मचारियों की
बहाली की प्रक्रिया पूरी कर सूचना देने का पाकुड़ के उपायुक्त
को आदेश दिया गया था। इस संबंध में जिला प्रशासन दो माह
के बाद भी हाथ पर हाथ धरे बैठा है। इधर निष्कासित
कर्मचारी और उनके परिवार के समक्ष भुखमरी की स्थिति
उत्पन्न हो गई है। उल्लेखनीय है कि पाकुड़ समाहरणालय में
वर्षों से कार्यरत चतुर्थवर्गीय दैनिक मजदूरी पर
काम करने वाले कर्मचारियों काम से हटा दिया गया था।
कर्मचारियों ने इसके खिलाफ झारखंड उच्च न्यायालय में १९९८
में एक मुकदमा (संख्या ८९१/९८ सी.डब्ल्यू.जी.सी.) दायर किया।
मुख्य न्यायाधीश ने जिला प्रशासन पाकुड़ को आदेश दिया कि
पाकुड़ समाहरणालय में सभी दैनिक वेतनभोगी कर्मचारियों
का वरीयता सूची बनाकर छह माह के अन्दर सरकारी नियमानुसार
भी प्रक्रिया पूरी कर रिक्त पदों पर बहाली की जाये। आदेश
के कई साल गुजर गये पर जिला प्रशासन के काम पर जूं
तक रेंगी। कर्मचारियों ने पुनः उच्च न्यायालय में अवमानना
का मुकदमा दायर किया। झारखंड उच्च न्यायालय के न्यायमूर्ति
एस.जे. मुखोपाध्याय ने जिला प्रशासन को कोर्ट में हाजिर
होने का आदेश जारी किया। जिला प्रशासन की ओर से
निदेशक, जिला ग्रामीण विकास आभिकरण ने हाजिर होकर
हलफनामा दायर किया कि दो माह के भीतर उच्च न्यायालय के
आदेश का अक्षरशः पालन किया जाएगा। आज दो माह से भी
आधिक गुजर गये पर जिला प्रशासन उच्च न्यायालय के आदेश
को ठंडे बस्ते में डालकर कुंभकर्णी निन्द्रा में लीन हो गया
है। वहीं दूसरी ओर कर्मचारी दाने-दाने का मोहताज हो गए
हैं।</p>

<p> </p>

<p>केन्द्रीय मानव
संसाधन विकास राज्यमंत्री से मिलेंगे मुख्यमंत्री      </p>

<p> </p>

<p>              </p><p>
रांची : मुख्यमंत्री बाबूलाल
मरांडी कल अपराह्न एक बजे मुख्यमंत्री आवासीय कार्यालय
में श्रीमती रीता वर्मा (केन्द्रीय मानव संसाधन विकास राज्यमंत्री)
के साथ भेंट करेंगे एवं अपराह्न ३.३० बजे देवदास आप्टे, सांसद के
सम्मान में आयोजित नागरिक आभिनंदन समारोह में भाग लेंगे।
उक्त समारोह नेहरू सांस्कृतिक केन्द्र जवाहरनगर, कांके रोड में
सम्पन्न होगी।</p>

<p> </p>

<p>संघ के जिला
प्रचारक पर पशु तस्करों ने जानलेवा</p><p>
हमला किया     </p>

<p> </p>

<p>              </p><p>
पाकुड़ : राष्ट्रीय स्वयं सेवक संघ
के जिला प्रचारक जैनेन्द्र कुमार पर महेशपुर प्रखंड के वेलियापतरा
गांव में कातिलाना हमला किया गया जिसमें वह गम्भीर रूप से
घायल हो गये। इस संबंध में छक्कधारा गांव के पशु तस्कर
अहमद अंसारी एवं मतवर अंसारी नामक दो लोगों पर महेशपुर
थाना कांड सं. ९७/२००२ भादवि के ३२३, ३२४, ३०७, ३७९/३४ के तहत
मुकदमा दायर किया गया है। समाचार लिखे जाने तक किसी
की गिरफ्तारी नहीं हुई थी। घटना के सम्बन्ध में बताया गया
कि पाकुड़ जिला आर.एस.एस. के जिला प्रचारक महेशपुर
ब्लॉक वेलियापतरा गांव अपने प्रवास में गये हुए थे। प्रवास
के दौरान पशु स्तकरी का विरोध करने पर पशु तस्कर अहमद
अंसारी एवं मतवर अंसारी ने घातक हथियार से उन पर वार किया।
जिला प्रचारक जैनेन्द्र कुमार जान बचाकर भाग रहे थे तभी
हसुआ चलाया गया, जिससे श्री कुमार का बांया पैर जख्मी हो
गया। जिला प्रचारक महेशपुर थाने में प्राथमिकी दर्ज कराने
गये तो प्राथमिक दर्ज नहीं की गयी। आर.एस.एस. के
समर्थक की भीड़ जुट जाने पर महेशपुर के थाना प्रभारी ने
लाचार होकर प्राथमिकी दर्ज की पर अभी तक किसी की
गिरफ्तारी नहीं जा सकी है। विगत दिन महेशपुर प्रखंड के
पोखरिया हाट में झारखंड मुक्ति मोर्चा के नेता दुर्गा
मरांडी ने जिला प्रचारक श्री कुमार को पकड़ कर एक पेड़ में
बांध कर अमानुषिक यातानाएं दी थीं। गांव वाले के प्रबल
विरोध कर झामुमो के नेता ने इस शर्त पर छोड़ा कि
इलाके में नजर नहीं आना चाहिये।</p>

<p>                                                                                                          </p>

<p>टेलीफोन सेवा
शुरू होने से ग्रामीणों में हर्ष                                     </p>

<p>              </p><p>
जामताड़ा
: भारत संचार निगम
लिमिटेड द्वारा ग्रामीण क्षेत्रों में टेलीफोन सेवा डब्ल्यू.एल.एल.
चालू किये जाने से इन दिनों सुदुर गांव देहातों में खुशी की
लहर दौड़ गयी है। जरुरी बातें, सम्पर्क व्यवसाय आदि मामले
में ग्रामीणों को पहली दफा फोन सेवा की आवश्यकता
महसूस होने लगी है। विदित हो कि जिले के नाला प्रखंड के
उपरांत पिछले दिनों कुण्डहित में प्रखंड मुखयालय में विभागीय
मंडल आभियंता द्वारा दूरभाष एक्सचेंज का उद्घाटन किया गया।
इस अवसर पर प्रमंडलीय आभियंता एक्सचेंज का उद्घाटन किया
गया। इस अवसर पर प्रमंडलीय आभियंता पी.के. सिंह ने
जानकारी दी कि नाला प्रखंड में २८२ एवं कुण्डहित के २५४ गांवों
में डब्ल्यू.एल.एल. फोन सेवा प्रथम चरण में उपलब्ध कराया गया है।
उन्होंने बताया कि थाना एवं सरकारी दफ्तर समेत एक लक्ष्य
सीमा के अंदर इच्छुक व्यक्तियों को भी आन डिमांड पर पांच
सौ रुपये सेक्यूरिटी मनी एवं दस हजार के इंश्योरेंस के जरिये
व्यक्तिगत फोन सेवा मुहैया करायी जाएगी।</p>

<p>              </p>

<p>चुनाव आयोग
की अनुचित सलाह                                                      </p>

<p> </p>

<p>सर्वोच्च न्यायालय के सामने
चुनाव आयोग का संविधानेतर सत्ता की तरह पेश आना उसकी
निष्पक्षता पर कई सवाल खड़े करता है। गुजरात मामले
की सुनवाई करने वाले पीठ को आयोग ने सलाह दी
है कि सर्वोच्च न्यायालय राष्ट्रपति को कोई सलाह न दे।
सलाह मांगने वाला पत्र राष्ट्रपति को निरुत्तर लौटा दे। न्यायालय
ने चुनाव आयोग को इस सलाह के लिए नहीं बुलाया था। इस
तरह की सलाह की जरूरत उसे है भी नहीं। सर्वोच्च न्यायालय
चाहता है कि गुजरात मामले पर संवैधानिक मर्यादाओं के
भीतर रहते हुए राष्ट्रपति को समुचित जवाब देने में संबंधित
पक्ष उसे सहयोग करे ताकि वह किसी निष्कर्ष पर
पहुंच सकें। चुनाव आयोग जिन कारणों से गुजरात मामले में
किसी निष्कर्ष पर पहुंचा है और वह वहां छह माह के
भीतर चुनाव कराने के बजाय राष्ट्रपति शासन चाहता है, इस बारे
में सर्वोच्च न्यायालय को अपनी राय देनी चाहिए थी। आखिर
संवैधानिक संकट के समय सर्वोच्च न्यायालय से ही राष्ट्रपति
सलाह मांगते हैं। सलाह मांगने का यह काम भी संवैधानिक है,
मगर चुनाव आयोग इसे भी अनदेखा कर रहा है। उसने तो अनुच्छेद
१७४ के तहत विधानसभा भंग होने की स्थिति में छह माह के
भीतर चुनाव कराने की संवैधानिक व्यवस्था की भी
अनदेखी की और अब वह सर्वोच्च न्यायालय को सीमा के बाहर
जाकर सलाह दे बैठा है। अत: सर्वोच्च न्यायालय को उसकी इस
सलाह पर अपनी टिप्पणी देकर आयोग की मर्यादाओं का ध्यान
दिलाने की जरूरत आ पड़ी है। संविधान के अनुच्छेद ३२४ के
तहत चुनाव आयोग को किसी भी राज्य में चुनाव कराने का
आधिकार प्राप्त होता है। इस अनुच्छेद में कहीं भी आयोग को
चुनाव न कराने का आधिकार प्राप्त नहीं है। बेशक उसे चुनाव
निष्पक्ष हों, इसकी निगरानी करने और जहां निष्पक्ष
चुनाव नहीं हुआ हो या कोई गड़बड़ी हुई हो तो वहां
दोबारा चुनाव कराने का आधिकार है। मगर गुजरात मामले में
चुनाव आयोग ने कानून व्यवस्था का सवाल खड़ा कर वहां
राष्ट्रपति शासन की सिफारिश कर दी। सर्वोच्च न्यायालय को
इस पर भी विचार करना चाहिए कि क्या चुनाव आयोग को
किसी भी राज्य में राष्ट्रपति शासन लागू करने की सिफारिश
करने का आधिकार है। मुख्य न्यायाधीश बी.एन. किरपाल की
अध्यक्षता वाले इस पीठ को यह भी फैसला देना है कि
किसी न किसी कारणवश चुनाव आयोग को किसी भी राज्य
में चुनाव टालने का हक है या नहीं। उम्मीद की जानी चाहिए,
फैसला जो भी होगा सभी पक्षों को मान्य होगा।</p>

<p> </p>






</body></text></cesDoc>