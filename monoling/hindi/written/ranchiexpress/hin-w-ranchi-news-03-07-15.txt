<cesDoc id="hin-w-ranchi-news-03-07-15" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-ranchi-news-03-07-15.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Celia Worth and Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Internet version of the "Ranchi Express" newspaper (www.ranchiexpress.com), news stories collected on 03-07-15</h.title>
<h.author>Ranchi Express Group</h.author>
<imprint>
<pubPlace>Ranchi, Jharkhand, India</pubPlace>
<publisher>Ranchi Express Group</publisher>
<pubDate>03-07-15</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>बिजली
बोर्ड आम जनों की सुविधाओं का ध्यान रखे          </p>

<p></p><p>
रांची: झारखंड उच्च न्यायालय के न्यायाधीश
न्यायमूर्ति एम.वाइ. इकबाल ने आज झारखंड राज्य विद्युत बोर्ड
के अध्यक्ष बी.के. चौहान को हिदायत दी कि वह बिजली
आपूर्ति के मामले में केवल विशिष्ट व्यक्तियों की सुविधा
का ख्याल न रखे बल्कि आम जनता की सुविधा को भी ध्यान
में रखे। न्यायालय ने अदालत में व्यक्तिगत रूप से उपस्थित
बोर्ड अध्यक्ष से दो टूक शब्दों में कहा कि आम जनता
सर्वोपरि है इसलिए उनकी सुविधा का ध्यान रखा जाए। न्यायमूर्ति
इकबाल ने कहा कि न्यायालय आम जनता की असुविधा को
नजर अंदाज नहीं कर सकता। उल्लेखनीयहै कि न्यायालय ने
पूर्व में सो मोटो लेते हुए झारखंड के अध्यक्ष बी.के.
चौहान, ऊर्जा सचिव सुधीर प्रसाद, महाप्रबंधक विद्युत
आपूर्ति, रांची मंडल, आर.आर.डी.ए. के उपाध्यक्ष, प्रशासक,
रांची नगर निगम का न्यायालय में हाजिर होने का निर्देश दिया।
प्रशासक नगर निगम के स्थान पर उप प्रशासक न्यायालय में
हाजिर हुए। मामले की सुनवाई के दौरान झारखंड विद्युत
बोर्ड की ओर से न्यायालय को जानकारी दी गयी कि
विशिष्ट व्यक्तियों की सुविधा के लिए २४ घंटे बिजली की
आपूर्ति करनी पड़ती है। शेष जगहों पर समान रूप से
बिजली की आपूर्ति की जाती है तथा इसमें भेदभाव नहीं
बरता जा जाता। बोर्ड की ओर से दायर शपथ पत्र में कहा गया
कि बिहार राज्य विद्युत बोर्ड से झारखंड विद्युत बोर्ड
को जर्जर संरचना मिली है तथा पतरातू थर्मल पावर प्लांट
में बिजली का उत्पादन भी कम हो रहा है। शपथ पत्र में कहा गया
कि बोर्ड घाटे में चल रहा है। उत्पादन कम होने की बात
सुनकर न्यायमूर्ति इकबाल ने महाधिवक्ता आनिल कुमार
सिन्हा से पूछा कि ऊर्जा मंत्री घोषणा करते है कि राज्य
में आतिरिक्त बिजली का उत्पादन हो रहा है और वह बिजली
बेचने की बात भी कहते हैं। इस पर महाधिवक्ता ने कहा कि
बोर्ड द्वारा प्रस्तुत रिपोर्ट के अनुसार ऊर्जा मंत्री का बयान
गलत है। बोर्ड द्वारा न्यायालय को जानकारी दी गयी कि
बोर्ड का सरकारी विभागों एवं सार्वजनिक क्षेत्र के
प्रतिष्ठानों के पास ४०० करोड़ रुपये बकाया है। केवल एच.इ.सी.
पर ३५० करोड़ रुपया बकाया है। जिसके कारण बोर्ड को
आर्थिक कठिनाइयों का सामना करना पड़ रहा है। इसके
कारण विकास कार्य भी प्रभावित हो रहा है। इसके बावजूद
बोर्ड सीमित संसाधन रहते हुए भी ठीक ढंग से विद्युत
आपूर्ति करने के लिए कृतसंकल्प है। न्यायालय बोर्ड की
दलीलों को सुनने के बाद उसे निर्देश दिया कि बकाए की
वसूली के लिए कानून के तहत कार्रवाई करे। स्ट्रीट
लाइट इसके साथ ही अदालत ने रांची नगर निगम को निर्देश दिया
कि सबी क्षेत्रों में स्ट्रीट लाइट की व्यवस्था करे और इसके
झारखंड विद्युत बोर्ड को नियमानुसार चौथे फेज के लिए
आवेदन दे। अदालत ने कहा कि नगर निगम के अधीन जो
सड़कें और गलियां हैं उनकी देखरेख ठीक ढंग से करे जहां
आवश्यकता हो मरम्मत कार्य करे। अदालत ने आर.आर.डी.ए.
को निर्देश दिया कि सड़कों का सुधार करे एवं पुरानी टूटी
फूटी सड़कों को बनाए।</p>

<p> </p>

<p>पशुपालन
घोटाला, राजो सिंह की नियमित जमानत याचिका खारिज</p>

<p></p><p>
रांची: पशुपालन घोटाले से
सम्बंधित मुकदमे मेंं सीबीआइ के विशेष न्यायाधीश
आलोक सेन गुप्ता की अदालत ने आज सांसद राजो सिंह की
नियमित जमानत याचिका को खारिज करते हुए उन्हें २१
जुलाई तक न्यायिक हिरासत में भेज दिया। यह मुकदमा
डोरंडा कोषागार से अवैध तरीके से सरकारी राशि की
निकासी से सम्बंधित है। जिसमें राजो सिंह के खिलाफ
आरोप है कि उन्होंने घोटाले के आरोपी डा. बी.एन.शर्म
के स्थानान्तरण को लेकर तत्कालीन मुख्यमंत्री के नाम एक
पत्र जारी किया था। दूसरा आरोप पशुपालन घोटाले के सरगना
श्याम बिहारी सिन्हा से राजो सिंह की टेलीफोन वार्ता से
सम्बंधित हैं। तीसरा आरोप एयर लाइंस कर्मचारी जे.पी.
वर्मा द्वारा राजो सिंह को हवाई यात्रा का टिकट मुहैया
कराने से सम्बंधित है। जे.पी.वर्मा की पत्नी और पुत्र
दोनों पशुपालन विभाग में आपूर्तिकर्त थे।</p>

<p> </p>

<p>आपदा प्रबंधन
प्राधिकार के गठन को मंजूरी</p>

<p></p><p>
रांची: झारखंड में गुजरात के
तर्ज पर आपदा प्रबंधन प्राधिकार का गठन होगा। मुख्यमंत्री
ने इसे अपने स्वीकृति दे दी है। कैबिनेट की स्वीकृति
होनी बाकी है। भीषण आपदाओं से निपटने के लिए इस
प्राधिकार का गठन होगा। भूकम्प, तूफान, सूखा, विमान
दुर्घटना, ट्रेन दुर्घटना, नरसंहार, जैसे विषम
परिस्थितियों से निपटने की जिम्मेवारी इसी प्राधिकार की
होगी। केन्द्र सरकार द्वारा इस बाबत निर्णय लिया गया। केन्द्रीय
गृह मंत्रालय द्वारा राज्य सरकार को इस सम्बन्ध में हिदायतें दी गयी
हैं और कहा गया है कि विचार कर इस दिशा में निर्णय लें।
अब तक यह स्थिति है कि आपदा आने के बाद उससे निपटने की
तैयारी शुरु की जाती है जिसमें काफी वक्त लग जाता है।
आपदा जिस प्रकार की होती है उससे निपटने में सक्षम
विभागों के लोगों को जुटाया जाता है फिर राहत का काम
शुरु होता है। अब केन्द्र ने निर्णय लिया है कि राज्य में भी
राहत के लिए प्रशिक्षित टीम रहे जो आपदाओं में तुरन्त सक्रिय
होकर उस पर नियंत्रण पाएं या त्वरित सहायता दे सकें। झारखण्ड
में आपदा प्रबंधन प्राधिकार के गठन का केन्द्र ने निर्देश दिया
है। यहां जो राहत एवं पुनर्वास विभाग है उसका नाम भी
बदलने का निर्णय लिया गया है। विभाग का नाम आपदा
प्रबंधन विभाग कर दिया जायेगा। राज्य सरकार ने इसके लिये
अपनी स्वीकृति दे दी है। पुलिस अर्द्धसैनिक बल, स्वास्थ्य
आदि विभागों से लोग आपदा प्रबंधन की टीम में लिये जायेंगे।
प्राधिकार में मुख्यमंत्री अध्यक्ष तो होंगे पर संबद्ध विभागों के
मंत्री और मुख्य सचिव भी इस प्राधिकार के सदस्य होंगे।
कृषि गृह, सिंचाई स्वास्थ्य आदि विभागों के मंत्रियों
को आपदा प्रबंधन प्राधिकार का सदस्य बनाया जायेगा। आपदा
प्रबंधन की टीम के प्रशिक्षण पर खास तौर पर जोर दिया जाएगा।
स्वयंसेवी संस्थाओं की भी सहायता ली जाएगी। केन्द्र ने
आपदा प्रबंधन के लिए योजना तैयार करने की भी अनुशंसा
तैयार की है। जिला एवं राज्य स्तर पर योजना तैयार की जाएंगी
और आपदा प्रबंधन का प्रशिक्षण टीम को दिया जाएगा।
सी.बी.एस.ई. के पाठ्यक्रम में भी आपदा प्रबंधन
विषय रखने की जरूरत बतायी गयी है। राज्य के पुनर्वास
आयुक्त नरेन्द्र भगत ने बताया कि गुजरात, उड़ासी में क्रमश:
भूकम्प और तूफान से भयंकर विनाश का अनुभव किया जा
चुका है। विभिन्न अनुभवों के बाद यह जरूरत महसूस की गयी
कि आपदा प्रबंधन के लिए राज्यों में प्राधिकार का गठन
हो। उड़ीसा और गुजरात में प्राधिकार का गठन कर दिया गया
है। गुजरात मॉडल को झारखण्ड में अपनाने का निर्णय लिया
गया है। इसके लिए गुजरात सरकार को लिखा गया है। वहां से
सारी जानकारियां मंगायी गयी है। धनबाद निगम के स्पेशल
अफसर और 'माडा' के प्रबंध निदेशक को अहमदाबाद में
परामर्श सम्मेलन में भाग लेने के लिए भी कहा गया है।
अहमदाबाद में भूकंप पर काफी काम हुआ है इसकी वे वहां
जानकारी प्राप्त करेंगे। मानव द्वारा उत्पन्न आपदाओं जैसे नरसंहार
आदि को भी इस प्रबंधन के अधीन लाने पर सैद्धांतिक
सहमति हो चुकी है। हवाई दुर्घटना, ट्रेन दुर्घटना से
निपटना आदि आपदा प्रबंधन प्राधिकार के दायरे में होगा। सभी
विभागों से यह आग्रह किया गया है कि वे समन्वय करें ताकि
इस प्राधिकार के गठन की प्रक्रिया में तेजी लायी जा
सके। उप प्रधानमंत्री लालकृष्ण आडवाणी पंद्रह अगस्त को
इंडिया डिजास्टर रिसोर्स नेटवर्क (आइ.डी.आर.एन.) के
वेबसाइट का उद्घाटन करेंगे। राज्य के पास आपदाओं से निपटने
के क्या उपकरण हैं इसकी भी फेहरिस्त तैयार की जाएगी।
सर्च एंड रेस्क्यू टीम (खोज एवं बचाव दल) को प्रशिक्षण दिया
जाएगा। प्रशिक्षण की योजना राज्य एवं जिलावार बनाने के लिए
कहा गया है। राज्य के आपदा राहत फंड से इस टीम के लिए राहत
उपकरण खरीदे जा सकेंगे। यू.एन.डी.पी. के एक प्रतिनिधि ने
आधिकारियों के समक्ष प्रेजेंटेशन दिया है। खोज एवं बचाव दल
में पुलिस, अर्द्धसैनिक बलों, स्वास्थ्य विभाग एवं कुछ अन्य
विभागों के लोग होंगे। झारखंड में पाकुड़-साहेबगंज के
इलाके सिस्मिक जोन तीन में रखे गये हैं। ये इलाके और
धनबाद के कुछ इलाके भूकम्प के गृष्टिकोण से सम्वेदनशील
तो है पर उतने सम्वेदनशील नहीं।</p>

<p> </p>

<p>सिल्ली में
सड़क हादसा, दो ईसीएल आधिकारियों की मौत</p>

<p></p><p>
रांंंची: सिल्ली में सिल्लीडीह के
निकट आज प्रात: लगभग छह बजे एक कार और एक ४०७ मिनी
बस(डब्ल्यू बी५५-१९८३) के बीच हुई सीधी टक्कर में
ईस्टर्न कोलफील्ड लिमिटेड के दो आधिकारियों की
मृत्यु हो गयी जबकि दो अन्य आधिकारी गम्भीर रुप से घायल
हो गये। मृतकों में ईसीएल के डिप्टी चीफ सिस्टम मेनेजर
मधुप कुमार वर्मा(४९) और सीनियर माइनिंग इंजीनियर
सुरेश मिश्र(४५) हैंं। दुर्घटना मे घायल हुए
पी.के.बी.गुप्ता(डिप्टी चीफ मेनेजर सेल्स) एवं बी.के
वर्मा (सिविल इंजीनियर) को उपचार के लिये हाजी
अब्दुर्रज्जाक अंसारी मेमोरियल अस्पताल इरबा में भर्ती कराया
गया है जहां उनकी हालत चिंताजनक बतायी गयी है। बताया गया
कि मधुप वर्मा की दुर्घटना स्थल पर ही मृत्यु हो गयी
जबकि सुरेश मिश्र की मृत्यु अपोलो अस्पताल पहुंचते ही हो
गयी। कार को बी.के. वर्मा चला रहे थे। कार भी उनकी
ही थी। </p><p>
प्राप्त जानकारी के
अनुसार उक्त मारे गये तथा घायल हुए सभी लोग पूर्व में
सीसीएल एवं सीएमपीडीआइ रांची में कार्यरत थे लेकिन
दो वर्ष पूर्व उनका तबादला ईसीएल के सेन्टोरिया
स्थित मुख्यालय में कर दिया गया था। बताया गया कि रांची में
ही इनके मकान और परिवार के अन्य सदस्य रहते हैं। इसलिये
अक्सर वे शनिवार को रांची लौट आते थे और सोमवार को
अहले सुबह यहां से सेन्टोरिया के लिये प्रस्थान कर जाते थे।
आज सुबह वे सेन्टोरिया के लिये एक अल्टो कार (जेएच-०१
ई-१२६४) से चले थे कि रास्ते में सिल्ली के निकट एक लाइन
होटल के सामने उनकी कार विपरीत दिशा से आ रही एक
मिनी बस से टकरा गयी। इस जबर्दस्त टक्कर में मधुप
वर्मा की घटना स्थल पर ही मारे गये। दुर्घटना में मिनी
बस भी पलट गयी और बस पर सवार जोन्हा निवासी सूरज
मुंडा,जयराम मंाझी, श्री राम वर्मा एवं ठाकुर दास
कोईरी को चोटे आयी हैं।</p>

<p> </p>

<p>पुलिस की
ट्रेनिंग से ज्यादा सशक्त है उग्रवादियों की ट्रेनिंग</p>

<p></p><p>
रांची: पुलिस बलों की ट्रेनिंग
से कहीं भी कम नहीं है उग्रवादियों की ट्रेनिंग। उग्रवादियों
के ट्रेनिंग मैनुअल से इसका खुलासा हुआ है। झारखंड में चार
महीने तक चले विशेष आभियान में शामिल केन्द्रीय बल
के आधिकारियों ने बताया कि उग्रवादियों के प्रशिक्षण कहीं
से भी उनसे (पुलिस बल) कम नहीं है। एक व्यवस्थित पुलिस
बल की ट्रेनिंग की तरह ही उनकी ट्रेनिंग कारगर और सशक्त
है। उग्रवादियों की ट्रेनिंग में अस्त्र-शस्त्र चालन के साथ-साथ
गुरिल्ला लड़ाई की अत्याधुनिक तकनीक और कौशल
की जानकारी दी जाती है। झूमरा पहाड़ी में छापामारी के
दौरान केन्द्रीय बलों को प्राप्त ट्रेनिंग मैनुअल से उग्रवादियों
की ताकत और व्यवस्था को देख पुलिस आधिकारी अचम्भित
हैं। सूत्रों के अनुसार उग्रवादियों की ट्रेनिंग मैंनुअल से पता
चला है कि कैडरों को अस्त्र-शस्त्र चलाने के साथ-साथ लैण्ड माइन
लगाने, रिमोट से उड़ाने तथा अस्त्र-शस्त्र नहीं रहने पर भी दुश्मनों
को पस्त करने की तरकीब बतायी जाती है। झारखंड में
लगभग चार महीने तक चले विशेष छापामारी आभियान में ३२
उग्रवादियों के मारे जाने तक एक सौ से ज्यादा लोगों को
गिरफ्तार किए जाने की सूचना है। लेकिन केन्द्रीय बल
छापामारी में स्वचालित अस्त्र-शस्त्र नहीं मिलने को आभियान की
बड़ी विफलता मानते हैं। केन्द्रीय बलों को अफसोस है कि
उन्हें सही समय पर झुमरा पहाड़ी पर नहीं भेजा गया। झुमरा पहाड़ी
पर २६०० से ज्यादा उग्रवादियों ने इसी फरवरी में पन्द्रह दिनों की
ट्रेनिंग ली लेकिन झुमरा पहाड़ी पर अंतिम समय में जून में
आभियान के लिए भेजा गया। केन्द्रीय बलों का मानना है कि
राज्य पुलिस को झुमरा पहाड़ी पर चलने वाली ट्रेनिंग की
जानकारी थी और जानबूझ कर उस उपयुक्त मौके का फायदा
नहीं उठाया गया। इस ट्रेनिंग में १६०० से ज्यादा महिलाएं थीं।
भरोसेमंद सूत्रों का कहना है कि झुमरा पहाड़ी पर आभियान से
पहले ही उग्रवादी वहां से खिसक गए थे लेकिन बड़ी मात्रा में
अस्त्र-शस्त्र के भंडारगृह को नष्ट करने में पुलिस सफल रही। बताया
गया कि स्वचालित शस्त्रों को छोड़कर अन्य आग्नेयास्त्र भारी
मात्रा में बरामद किए गए। कहा गया कि उग्रवादियों को मिल रहे
व्यापक जन समर्थन और राज्य पुलिस के कुछ आधिकारियों
की उग्रवादियों से सांठ-गांठ के चलते ही उग्रवादियों
को पकड़ पाना मुश्किल हो रहा है। सूत्रों का कहना है कि
स्थिति का अंजादा इसी से लगाया जा सकता है कि एक एरिया
कमांडर ने ही केन्द्रीय पुलिस बल को पानी पिलाया और
रास्ता दिखाया। पानी पीने के बाद जब पुलिस बल वापस
लौटने लगी तो उन्हें बताया गया कि पानी पिलाने वाला व्यक्ति
ही एरिया कमांडर था। केन्द्रीय पुलिस बल ने दो ट्रक से भी ज्यादा
मात्रा में जिलेटीन बरामद की। इन जिलेटीन बाक्सों पर गोमिया
एक्सप्लोसिव फैक्टरी की मुहर लगी हुई थी। एक हजार से
ज्यादा मिलिट्री यूनीफार्म तथा कई बाक्सों में भरे
राइफल बरामद किए गए।</p>

<p> </p>

<p>रतिलाल महतो
की हत्या पूंजीपतियों ने करायी ः शैलेन्द्र भट्टाचार्य</p>

<p></p><p>
सरायकेला:
मजदूर नेता
रतिलाल महतो की चौथी पुण्य तिथि पर गम्हरिया प्रखंड स्थित
ऊपरबेड़ा मैदान में शहीद रतिलाल स्मृति रक्षासमिति तथा इण्डेन
बोटलिंग प्लान्ट विस्थापित प्रभावित श्रमिक सहयोग समिति
लिमिटेड व झारखंड मुक्ति मोर्चा के तत्वावधान में ५००
गरीब लोगों के बीच वस्त्र वितरण किया गया। </p><p>
वस्त्र वितरण झामुमो
के केन्द्रीय उपाध्यक्ष सह पूर्व विधायक चंपाई सोरेन
और शहीद रतिलाल महतो की पत्नी रेखा महतो ने किया। पुण्य
तिथि पर आयोजित आमसभा को संबोधित करते हुए किया
झामुमो के राष्ट्रीय कार्यालय सचिव शैलेन्द्र भट्टाचार्य ने
कहा कि मजदूरों के शोषण के खिलाफ आवाज उठाने
के कारण पूंजीपति उद्यमियों ने रतिलाल महतो की हत्या
करा डाली। झामुमो के केन्द्रीय महासचिव सुनील महतो ने
कहा कि शहीद परिवार को अब तक सरकार से कुछ सुविधा
नहीं मिली है। यहां तक की अंगरक्षक भी वापस ले लिया गया
है। झामुमो के केन्द्रीय उपाध्यक्ष सुधीर महतो ने कहा कि
अपनी सत्ता होने पर झारखंड के शहीद परिवार को सरकार द्वारा हर
सुविधाएं दी जायेंगी। शहीद रतिलाल स्मृति रक्षा समिति के
सचिव शत्रुघ्न महतो ने कहा कि मजदूर हित की लड़ाई
लड़ते ही रहे रतिलाल महतो। झामुमो केन्द्रीय महासचिव मोहन
कर्मकार ने कहा कि राजग सरकार को हटाने के बाद ही
मजदूर हित की बात हो सकती है, क्योंकि राजग सरकार
पूंजीपति व व्यापारियों की पार्टी है। झामुमो के सरायकेला
खरसावां जिलाध्यक्ष सुधीर महतो ने कहा कि झारखंड मुक्ति
मोर्चा के मजदूर विंग 'झारखंड औद्योगिक मजदूर यूनियन'
के संस्थापक थे। वे मजदूरों के हित की लड़ाई सदैव
लड़ते रहे। इसके लिये उन्हें जेल भी जाना पड़ा। उन्होंने इण्डेन
बोटलिंग प्लांट विस्थापित प्रभावित श्रमिक सहयोग समिति
लिमिटेड के माध्यम से लगभग २०० परिवारों के लिए स्थायी
रोजी- रोटी की व्यवस्था की। श्री महतो से प्रेरित होकर अन्य
कंपनियों ने भी मजदूर हित में श्रमिक सहयोग समिति
बनाकर नौकरी दिलाने की प्रक्रिया शुरू की। कुछ असामाजिक
तत्वों ने १२ जुलाई १९९९ को गोली मारकर उनकी हत्या कर
दी।</p>

<p> </p>

<p>दुर्लभ सपनों
को साकार करने का प्रयास : आर.ए.के. वर्मा</p>

<p></p><p>
रांची: पुन्दाग स्थित इंडियन इंस्टीच्यूट
आफ साइंस एण्ड मैनेजमेंट आज झारखंड के द्वितीय सर्वश्रेष्ठ
प्रबंधन संस्थान के रूप में जाना जाता है। वर्ष १९८७ में इस
संस्थान की शुरूआत कडरू में एक भाड़े के मकान में डॉ.
सच्चिदानंद, पूर्व कुलपति, रांची विश्वविद्यालय के
मार्गदर्शन में आर.ए.के.वर्मा ने एक प्रीमियर इंस्टीच्यूट
बनाने के दुर्लभ सपने को लेकर की थी। आज प्रबंधन
के क्षेत्र में यह संस्थान प्रतिवर्ष सैकड़ों छात्रों को
रोजगार मुहैया करा रहा है। संस्थान के १८वे स्थापना दिवस के
अवसर पर एक विशेष बातचीत में श्री वर्मा ने बताया
कि इसके स्थापना के पूर्व यहां एक भी होटल प्रबंधन
संस्थान नहीं था। एल. एन. मिश्रा कॉलेज आफ बिजनेस मैनेजमेंट,
मुजफ्फरपुर से वर्ष १९७९ में एमबीए करने के पश्चात श्री
वर्मा इंडियन इंस्टीच्यट आफ बिजनेस मैनेजमेंट में दो
वर्ष तक कार्यरत रहे। यहां कार्य करने के बाद
प्राप्त अनुभवों को उन्होंने पूर्ण रुप से इस संस्थान के
विकास में लगा दिया। उन्होंने बताया कि यहां त्रिवर्षीय
डिप्लोमा इन होटल मैनेजमेंट एंड कैटरिंग टेक्नोलॉजी एवं
पोस्ट ग्रेजुएट डिप्लोमा इन बिजनेस मैनेजमेंट का शुभारंभ
वर्ष १९८८ में किये गये। झारखंड बनने के बाद उक्त
पाठ्यक्रमों को स्टेट बोर्ड आफ टेक्निकल एजुकेशन,
झारखंड सरकार द्वारा मान्यता दे दी गयी। श्री वर्मा को
माइकोम इंडिया एवं एवीसी कन्सल्टेंट्स जैसे प्रभावशाली संस्थानों
में कार्य करने का भी अनुभव है। मुख्य आतिथि राज्यपाल
होंगे आइआइएसएम के १८वां स्थापना दिवस समारोह पूर्वाह्न ११
बजे से शुरू होगा। इसके मुख्य आतिथि झारखंड के राज्यपाल
होंगे। इस अवसर पर संस्थान के विद्यार्थियों द्वारा कई कार्यक्रम
भी प्रस्तुत किये जायेंगे।</p>

<p> </p>

<p>नेत्रदान
जागरूकता पर कार्यशाला का आयोजन</p>

<p></p><p>
रांची: न्यू सेन्चूरी लायन्स क्लब
आफ रांची ग्रीन एवं कश्यप मेमोरियल आई बैंक के संयुक्त
तत्वावधान में नेत्रदान जागरूकता कार्यशाला का आयोजन
होटल चिनार में किया गया। कार्यशाला में कश्यप आई
बैंक की मेडिकल डायरेक्टर डा. भारती कश्यप मुख्य आतिथि
के रूप में उपस्थित थी। अपने आभिभाषण में डा. भारती कश्यप
ने नेत्रदान जागरूकता पर विशेष बल देते हुए इसे आगे बढ़ाने
का आह्वान किया। क्लब के पांच सदस्यों ने आई डोनेशन
अवेयरनेस क्लब (आई.डी.ए.सी.) की सदस्यता ग्रहण करते हुए
अपने मरणोपरान्त नेत्रदान करने हेतु शपथ पत्र भरे। क्लब के
अध्यआ लायन मुकेश जाजोदिया ने स्वागत भाषण देते हुए
अपनी पूरी टीम के साथ पूरे कार्यकाल के दौरान नेत्र
एवं स्वास्थ्य संबंधी अनेक कार्यशाला का आयोजन करने
का संकल्प लिया। नेत्रदान के लिए आई.डी.ए.सी. की सदस्यता
ग्रहण करने वाले पांच सदस्यों में मुकेश जाजोदिया, संदीप
केडिया, नारायण वीरेन्द्र, आनिल सिंघानियां एवं राजेश गर्ग
शामिल हैं। इसके अलावा मरणोपरान्त नेत्रदान करने के लिए
आवेदन पत्र भरने वालों में नेयाल अहमद, अमरनाथ गुप्ता, आमित
पोद्दार, नितिन सरावगी, राजीव रंजन, घनश्याम राजगढ़िया,
विकास मोदी, विमोर डागा, विकास कुमार, दीपक कुमार
शामिल हैं। उल्लेखनीय है कि डा. भारती कश्यप विगत वर्षों
में अनेक सामाजिक कार्य विभिन्न सामाजिक संगठनों
के माध्यम से करते आ रही है जिनमें नेत्र प्रत्यारोपण, आई.
स्क्रीनिंग कैम्प, मोतियाबिन्द एवं अन्य सेवाएं गरीबों को
नि:शुल्क दी गयी। क्लब की ओर से लायन नारायण बीरेन्द्र ने
डा. भारती कश्यप को प्रतीक चिह्न भेंट किया। अन्त में धन्यबाद
ज्ञापन लायन आमित पोद्दार ने दिया।</p>

<p> </p>

<p>राजस्थान के
तर्ज पर कार्यक्रम बनाये जायेंगे : डीइओ</p>

<p></p><p>
रांची राजस्थान के विभिन्न
जिलों में चलाये जा रहे अनीमिया नियंत्रण कार्यक्रम की
तकनीकी पहलुओं की जानकारी प्राप्त करने के बाद वहां गयी
जिला शिक्षा पदाधिकारी अरविन्द विजय विलुंग की टीम
उर्जावान होकर लौटी है। यह टीम रांची जिले में भी वहां
सफल हुए कई कार्यक्रमों को शुरू करेगा। श्री विलुंग
ने बताया कि सात जुलाई को राजस्थान की राजधानी जयपुर
पहुंचकर उसकी टीम ने महिला एवं बाल विकास निदेशालय गयी
तथा निदेशक जगदीश चन्द्र एवं प्रोजेक्ट अफसर श्रीमती प्रमीला
संजया के साथ बैठक की। बैठक में संपूर्ण
राजस्थान में इस विभाग द्वारा चलाये जा रहे स्वास्थ्य एवं पोषण
संबंधी कार्यक्रमों की जानकारी मिली। इसके बाद यह
टीम टोंक जिले का भ्रमण किया। यहां स्थानीय ग्रामीण
महिलाओं, किशोरियों व संबंधित कार्यकारियों से
मुलाकात की गयी। श्री विलुंग ने बताया आठ जुलाई
को जयपुर शहर की गंदी बस्तियों में स्थित आंगनबाड़ी केन्द्र
इंदिया बस्ती पंेटर नगर का अवलोकन कर राजकीय माध्यमिक
कन्या विद्यालय का भ्रमण किया। श्री विलुंग ने बताया कि
इस क्षेत्र भ्रमण में आइसीडीएस के सेवानिवृत्त आधिकारी डॉ.
एस.एल.शर्मा ने प्रारंभ से अंत तक टीम का सहयोग तथा
मार्गदर्शन किया। इस टीम में श्री विलूंग के अलावा
जिला सहयोगी सदस्य सुचन्द्रा पंडा, शारदा सिंह व लॉजिस्टिक
अशोक प्रसाद सिंह शामिल थे।</p>

<p> </p>

<p>अयोध्या मुद्दे पर
फिर बमचख </p>

<p> </p>

<p>              </p><p>
भाजपा के नीति
निर्धारक नेता और उपप्रधानमंत्री लाल कृष्ण आडवाणी
ने अयोध्या विवाद के समाधान के लिए कानून बनाने के
मार्ग की उलझनों का खुलासा करते हुए स्पष्ट कर दिया है कि
मौजूदा परिस्थिति में मंदिर विवाद के समाधान के लिए संसद
में विधेयक लाना संभव नहीं है। विधेयक मार्ग की
बाधाओं से कांग्रेस भलीभांति वाकिफ है, इसलिए विधेयक
लाने पर पार्टी कार्यकारिणी में विचार करने के संबंध में
भाजपा द्वारा विहिप और संघ प्रमुख को दिये गये आश्वासन पर
उसकी (कांग्रेस की) कोई प्रतिक्रिया नहीं आयी।
लेकिन आश्चर्य है कि गठबंधन (राजग) में शामिल दलों
समता पार्टी अकाली दल और शिवसेना को भाजपा के
आश्वासन पर राजनीति करने का मौका मिल गया हैं। समता
पार्टी के अध्यक्ष और राजग के संयोजक जार्ज फर्नांडीस जो
सरकार के संकट मोचक भी रहे हैं, का हालांकि इस संबंध में
कोई वक्तव्य नहीं आया है लेकिन उनकी पार्टी के
प्रवक्ता शिव कुमार सिंह का कहना है कि भाजपा द्वारा मंदिर
विवाद के समाधान के लिए संसद में विधेयक लाया गया तो
इसका विरोध किया जायेगा। श्री सिंह का कहना है कि शाहबानो
प्रकरण में जो गलती राजीव गांधी ने की थी, उसे दुहराने
की कोशिश हुई तो इससे न केवल एक गलत परम्परा
बनेगी बल्कि नयी उलझनें भी पैदा होंगी। लेकिन कैसी
उलझनें पैदा होंगी यह न तो शिव कुमार सिंह ने स्पष्ट किया
और न पूर्व प्रधानमंत्री चन्द्रशेखर ने इन नेताओं का कहना
है कि बातचीत से मंदिर विवाद का हल यदि नहीं हो तो इसे
अदालत के ऊपर छोड़ देना चाहिए। लेकिन हमारे ख्याल से सर्वोच्च
न्यायालय में इस संबंध में अर्जी दाखिल कर उससे पूछा जाना
चाहिए कि आस्था का यह मुद्दा उसके विचार का विषय है या
नहीं। यदि उसके विचार का यह विषय है तो उससे एक समय
सीमा के भीतर इस मुद्दे को निपटाने के लिए आग्रह किया
जाना चाहिए या फिर अलग से अदालत गठित कर इस मुद्दे के
समाधान का प्रयास किया जाना चाहिए। लेकिन आश्चर्य है
कि दशकों से अयोध्या का मंदिर विवाद अदालत में लंबित है
लेकिन अलग से अदालत गठित कर विवाद के शीघ्र
समाधान के प्रति न तो सरकार सचेष्ट है और न विपक्ष इसके
लिए सरकार पर दबाव डालने को उत्सुक है। सही बात यह है कि
सभी पार्टियां मंदिर विवाद को लंबे समय तक जीवित रखना
चाहती हैं ताकि राजनीतिक स्वार्थ साधने का उन्हें बार-बार
मौका मिलता रहे। लेकिन भारत को अगर विकसित राष्ट्र की
जमात में शामिल होना है, तो उसे लोगों को आपस में बांटने
वाले ऐसे तमाम विवादों का हल तलाशना ही होगा। इस विवाद
का समाधान सिर्फ हिन्दू ही नहीं चाहते बल्कि बकौल श्री
आडवाणी मुसलमान भी चाहते हैं। लेकिन श्री आडवाणी का इस
संदर्भ में यह कहना बिल्कुल वाजिब है कि समस्या के
समाधान के लिए व्यावहारिक गृष्टिकोण अपना होगा यानी हिन्दू
और मुसलमान दोनों ही समुदायों को कट्टरपंथी सोच से
मुक्ति पानी होगी।</p>

<p> </p>






</body></text></cesDoc>