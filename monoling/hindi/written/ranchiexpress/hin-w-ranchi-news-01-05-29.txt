<cesDoc id="hin-w-ranchi-news-01-05-29" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-ranchi-news-01-05-29.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Celia Worth and Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Internet version of the "Ranchi Express" newspaper (www.ranchiexpress.com), news stories collected on 01-05-29</h.title>
<h.author>Ranchi Express Group</h.author>
<imprint>
<pubPlace>Ranchi, Jharkhand, India</pubPlace>
<publisher>Ranchi Express Group</publisher>
<pubDate>01-05-29</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>विधायक ने निर्माण
कार्यों का शिलान्यास किया</p>

<p>बनासो
(हजारीबाग), 28 मई : झारखंड मुक्ति
मोर्चा के नेता एवं मांडू विधायक विधायक टेकलाल
महतो ने अपने विकास मद से विष्णुगढ़ प्रकंड के
सुदूरवर्ती गांवों में पिछले दिन 12 लाख 21 हजार 7 सौ रुपये लागत के
निर्माण कार्यों की आधारशिला रखी। इसमें विष्णुगढ़
सरकारी अस्पताल को एक नया एम्बुलेंस की उपलब्धता भी
शामिल है। इस आशय की जानकारी देते हुए प्रखंड झामुमो नेता
सुशील कुमार ने बताया कि झामुमो नेता एवं मांडू विधायक
श्री महतो द्वारा प्रखंड के बेड़म गांव में 1 लाख 40 हजार की लागत से पथ मरम्मत, कोल्हू गांव में 90 हजार की लागत से बनने वाले गार्डवाल, बुच्चादयाल
गांव में 1 लाख 31 हजार की लागत से चेक डैम के निर्माण, डूमर में 80 हजार की लागत से पुल निर्माण, मंदर गढ़वा में 50 हजार की लागत से बनने वाले चेकडैम, मंगरो एवं
चानो गांव में 22-22 हजार रु. लागत के 20 फीट ब्यास वाले कूपों के निर्माण, उपर बेड़वा में 25 हजार रु. की लागत वाले चेकडैम, 8 हजार की लागत से डाडी निर्माण, नंदूडीह में 55 हजार रु. लागत से चेकडैम के निर्माण, नागी गुड़ियागढ़ा
में 50 हजार की लागत से चेक डैम के
निर्माण एवं विष्णुगढ़ स्थित विवेकानंद सेवाश्रम में 75 हजार की लागत से पीसीसी पथ के निर्माण की
आधारशीला रखी गई। श्री कुमार ने बताया कि इसके
अलावा राज्यसभा सदस्य आर.के. आनंद के विकास मद के 4 लाख रुपये से मायागढ़ा गांव में चेकडैम निर्माण
कार्य का भी शिलान्यास किया गया। </p>

<p>कोयला भवन आग्निकांड : तथ्य
छुपा रहा है बी.सी.सी.एल. प्रबंधन</p>

<p>धनबाद,
28 मई: कोयला भवन में कल लगी आग के
संबंध में भारत कोकिंग कोल लिमिटेड प्रबंधन चालाकी से
तथ्यों को छुपाने की कोशिश कर रहा है। प्रबंधन इस आगजनी
के संबंध में पुलिस को भी गुमराह करने के प्रयास में
लगातार लगा हुआ है। कोयला भवन में आग लगी के संबंध में
सरायढेला थाना के प्रभारी के नाम से कल दो पत्र भेजे गए।
दोनों पत्र प्रबंधन की ओर से उप मुख्य कार्मिक प्रबंधन
सुभाष चन्द्र सुनेजा ने दिया है। पुलिस को दिये पहले पत्र
में श्री सुनेजा ने लिखा है कि कोयला भवन के लेबल दो में
कल दोपहर एक 1.15 बजे आग भड़क उठी।
पत्र में थाना प्रभारी को टेलीफोन से दी गई सूचना का
हवाला देकर लिखा गया है कि उन्हें (थाना प्रभारी को) सूचना
देने के बाद करीब 2.30 बजे आग्निशामक दल
पहुंचा। थाना प्रभारी चंद्रेश्वर प्रसाद यादव ने आज जब घटना की
जांच पड़ताल शुरू की तो उन्हें पता चला कि 'कोयला भवन' में
लगी आग को सबसे पहले केन्द्रीय औद्योगिक सुरक्षा बल के
हेड कांस्टेबल महेन्द्र सिंह ने देखा था। थाना प्रभारी श्री यादव ने
आज हेडकांस्टेबल महेन्द्र सिंह का बयान कलम बंद किया। श्री
सिंह के बयान और बीसीसीएल प्रबंधन की ओर से पुलिस
को आग भड़कने के समय के संबंध में दी गई सूचना में
अंतर है। प्रबंधन ने आग भड़कने का समय 1.15 बजे बताया है जबकि हेड कान्सटेबल महेन्द्र सिंह का बयान
है कि उन्होंने करीब पौने एक बजे कोयला भवन के लेबल-टू
से धुआं उठते हुए देखा। धुआं उठते देख कर उन्होंने 'कोयला
भवन' के मुख्य द्वार पर स्थित सी.आइ.एस.एफ. के 'कंट्रोल' रूम
में इस बाबत तुरंत सूचना दी। इस तरह स्पष्ट होता है कि प्रबंधन
की ओर से आग लगने के समय के संबंध में दी गई
सूचना सही नहीं है। पुलिस सूत्रों का कहना है कि कायदे से
आग लगने की पुलिस को सूचना लेबल-टू पर आग से
प्रभावित विभाग के प्रमुख को देनी चाहिए थी। आग से पूरी
तरह नष्ट हुए प्रोजेक्ट एण्ड प्लानिंग तथा एसोसिएट फाइनांस विभाग
के प्रमुख प्रोजेक्ट एण्ड प्लानिंग के निदेशक बी.एन. मिश्रा है।
पुलिस सूत्रों का कहना है कि कायदे से आग लगने की
सूचना श्री मिश्रा को पुलिस को देनी चाहिए थी। उन्होंने इस
बात से इंकार किया कि आग्निशामक संयंत्र और आग बुझाने
के इरादे से लगाई गयी पाइपों से पानी नहीं निकला। श्री
खेरा ने यह समझाने का भरपूर प्रयास किया कि आग लगने से
वास्तव में कोई महत्वपूर्ण कागजात नष्ट हुआ है। श्री खेरा
की बातों का निचोड़ यह था कि एक तरह से 'कोयला भवन'
का प्रोजेक्ट एण्ड प्लानिंग विभाग सी.एम.पी.डी.आइ.एल. का
एक्सटेंशन है। यहां जितना भी 'प्रोजेक्ट एण्ड प्लानिंग' से
संबंधित नक्शा था सभी सी.एम.पी.आइ.डी.एल. में उपलब्ध है।
उन्होंने यह समझाने का प्रयास किया कि कोई मूल फाइल या
नक्शा आग से नष्ट हुआ है। श्री खेरा ने यह भी कहा कि आग
शार्ट सर्किट के कारण लगी है। पुलिस अभी तक इस नतीजे
पर नहीं पहुंच सकी है। प्रबंधन ने पुलिस को दी गई
लिखित सूचना में भी आग लगने के कारण के संबंध में कुछ
भी नहीं बताया है। श्री खेरा ने बताया कि आग लगने के
कारण और इससे हुए नुकसान का डाइरेक्टर टेक्निकल की अध्यक्षता
में गठित 'चार-पांच' लोगों की उच्चस्तरीय कमेटी जांच
कर रही है। श्री खेरा पूछने पर यह नहीं बता सके कि डायरेक्टर
टेक्निकल की अध्यक्षता में गठित कमेटी के अन्य
चार-पांच सदस्य 'कौन-कौन' पदाधिकारी है। उन्होंने बताया
कि कमेटी ने जांच और नुकसान के आकलन का काम शुरू
कर दिया है। कमेटी अगले दो तीन दिनों में रिपोर्ट दे
देगी। श्री खेरा ने धनबाद के उपायुक्त विनोद किस्पोट्टा
प्रशासन के सभी आधिकारियों सहित आग बुझाने में सहयोग
करने वाले सभी लोगों को धन्यवाद दिया। उन्होंने बताया कि
उपायुक्त के प्रयास से ही चंद्रपुरा और सिन्दरी से आग्निशामक
वाहन पहुंच गया। दूसरी ओर चर्चा है कि आग दोपहर बारह
बजे के आस-पास ही लगी। आग जब काफी तेज पकड़ चुकी
थी और काफी धुआं बाहर निकलने लगा था तब करीब पौने
एक बजे आग लगने की सी.आइ.एस.एफ. को जानकारी मिली।
आग लगने के करीब ढाई घंटे यानी 2.30 बजे आग पर काबू पाने का वास्तविक प्रयास शुरू किया
गया। श्री खेरा से हुई बातचीत से ऐसा प्रकट हुआ है कि
प्रोजेक्ट एण्ड प्लानिंग विभाग में बीसीसीएल की किसी भी
परियोजना से क्या लाभ और क्या घाटा होगा, कितना 'मैन
पावर' लगेगा इसका आकलन किया जाता है। श्री खेरा का यह
कहना कि कोई भी 'मूल' कागजात नष्ट नहीं हुआ है, उससे
जानकार इंकार करते है। </p>

<p>राजेश मिंज की शहादत रंग लायी </p><p>
सहज
हो गई है लोटवा के आदर्श ग्राम बनने की संभावना</p>

<p>हजारीबाग
: जम्मू कश्मीर के कुपवाड़ा सेक्टर में देश की सरहद की रक्षा
करते हुए 29 अप्रैल 2000 को अपने प्राण न्यौछावर कर देने वाले शहीद राजेश
मिंज का सदा से उपेक्षित गांव लोटवा आज अपनी बदलती
किस्मत पर चकित है। अंतत: राजेश मिंज की शहादत रंग लायी
और इस गांव की निराशा ही टहनियों पर विकास एवं
निर्माण के फूल खिलने लगे। गत 29 अप्रैल 2001 को राजेश मिंज की पुण्य तिथि पर उनकी सद्य:
स्थापित प्रतिमा के निकट आयोजित समारोह में हजारीबाग
के उपायुक्त अमरेंद्र प्रताप सिंह ने जो वादा किया था, उससे वह
पीछे नहीं हटे और लोटवा गांव की जरूरतों का सर्वेक्षण करा कर
विकास एवं निर्माण के कई द्वार खोल दिए। मुख्य
मार्ग से गांव को जोड़ने के लिए नदी पर पुल, सड़क, उद्वह
सिंचाई योजना, स्पील वे आदि के निर्माण के कार्य
तो शुरू किए ही गए हैं, सामाजिक एवं कल्याणकारी कार्यक्रमों
के तहत 32 लोगों को इंदिरा आवास, 17 लोगों को वृद्धावस्था पेंशन, 9 लोगों
को अन्नपूर्णा कार्यक्रम का लाभ, 6 को बालिका समृद्धि योजना का लाभ, एक को मातृत्व
योजना का लाभ, 30 लोगों को स्वर्ण जयंती
ग्राम स्वरोजगार योजना का लाभ तथा 15 व्यक्तियों
को किसान क्रेडिट कार्ड आदि की सुविधाएं उपलब्ध
कराने की व्यवस्था भी की गई है। गत २५ मई को केंद्रीय
वित्तमंत्री यशवंत सिन्हा के लोटवा ग्राम के दौरे से इस गांव
के लोगों को अपना उज्जवल भविष्य साफ नजर आने लगा है।
निराशा ने उत्साह का रूप धारण कर लिया है। </p><p>
लोटवा हजारीबाग
जिला अन्तर्गत इचाक प्रखंड का एक छोटा सा गांव है। जिला
मुख्यालय से इसकी दूरी करीब 14 कि.मी. है। गांव में 87 परिवार हैं जिनमें 20 आदिवासी तथा शेष
हरिजन हैं। इस गांव की ओर सबसे पहले 1985 में होलीक्रास समाज सेवा केंद्र की निदेशक सिस्टर
रोसिली का ध्यान गया और तब वहां बालबाड़ी एवं प्राथमिक
विद्यालय की स्थापना हुई। नतीजतन आज गांव की हर
आदिवासी लड़की साक्षर है। यही नहीं होलीक्रास समाज सेवा
केंद्र द्वारा वहां कुछ स्वरोजगार उन्मुखी कार्यक्रम भी चलाए जा
रहे हैं। कृषि विकास एवं सिंचाई सुविधा की दिशा में
भी कार्य किए गए हैं और अब तो सरकार एवं प्रशासन की
भी छत्र-छाया इस गांव को प्राप्त है जिसने इस गांव को एक
आदर्श ग्राम बनने की संभावनाओं को और सहज बना दिया
है। </p>

<p>कुल 230 सीटें बढ़ेंगी, 50 फीसदी सीट झारखंड के युवकों के लिए </p><p>
बी.आइ.टी.
ने राज्य सरकार को अंतरिम रिपोर्ट सौंपी</p>

<p>रांची, 28 मई : बी.आइ.टी. मेसरा प्रबंधन ने सीटें बढ़ाने के
सम्बंध में अपनी अंतरिम रिपोर्ट आज राज्य सरकार को
सौंप दी है। राज्य सरकार के प्रस्ताव पर बी.आइ.टी. ने कुल 230 सीटें बढ़ाने के सम्बंध में अपनी मंजूरी दी है। इन
सीटों के बढ़ने तथा इसके लिए आधारभूत संरचना तैयार करने
में लगभग २३ करोड़ रुपए खर्च होने की सम्भावना है। प्रथम
किस्त के तौर पर 10 करोड़ रुपए की राशि
बी.आइ.टी. ने 15 जून के पहले भुगतान
किए जाने की मांग की है। उन्होंने कहा कि बढ़ी हुयी सीटों
में 50 प्रतिशत झारखंड के युवकों के लिए
आरक्षित रहेंगी। बी.आइ.टी. के कुलपति डा. एस.के मुखर्जी ने
कहा कि 15 जून तक राशि मिलने पर
आतिरिक्त सीटों के लिए एजुकेशन कैम्पस तथा हास्टल बनाए
जाएंगे। अगले सत्र की सीटें तभी बढ़ायी जा सकती हैं जब
तुरन्त छात्रावास तथा अन्य बिल्डिंग बनाने का काम शुरू कर दिया
जाए। डा. मुखर्जी ने विज्ञान एवं प्रौद्योगिकी सचिव को
अंतरिम रिपोर्ट सौंप कर आग्रह किया है कि 10 करोड़ रुपए की पहली किस्त शीघ्र निर्गत कर दी
जाए। अन्य तीन सालों में बी.आइ.टी. ने राज्य सरकार से क्रमशः
साढ़े पांच करोड़, पांच करोड़ तथा ढाई करोड़ रुपए की
मांग की है। इस प्रस्ताव को यदि राज्य सरकार स्वीकृत करती
है तो स्नातक पाठ्यक्रमों सूचना तकनीक में 80, कम्प्यूटर में 20, इलेक्ट्रिक एंड इलेक्ट्रानिक्स इंजीनियरिंग में 45, इलेक्ट्रानिक्स कम्यूनिकेशन में 30 तथा पालीमर में पांच सीटें बढ़ेंगी। स्नातकोत्तर
पाठ्यक्रम के एम.सी.ए. में 30, एम.एस.सी. (इन्फोर्मेशन
साइंस) में 15 तथा बायो मेडिकल में 5 सीटें बढ़ेंगी। मालूम हो कि अभी बी.आइ.टी. मेसरा
में स्नातक पाठ्यक्रमों में 320 तथा स्नातकोत्तर पाठ्यक्रमों
में 121 सीटें निर्धारित हैं। बताया गया कि
सीट बढ़ाने तथा झारखंड के लोगों के लिए 50 प्रतिशत सीटें आरक्षित करने का प्रस्ताव विज्ञान एवं
प्रौद्योगिकी मंत्री समरेश सिंह ने अपने भ्रमण के दौरान दिया।
विज्ञान एवं प्रौद्योगिकी मंत्री के निर्देश पर बी.आइ.टी. ने
आज राज्य सरकार को रिपोर्ट सौंप दी है। अब राज्य सरकार
इसे अमल में लाती है तो बी.आइ.टी. में सीटें बढ़ सकती हैं।</p>

<p> </p>

<p>संक्षिप्त</p>

<p>आग के पीछे 'कुछ' है</p>

<p>धनबाद,
28 मई : धनबाद की सांसद सह केन्द्रीय
ग्रामीण विकास राज्य मंत्री प्रो. रीता वर्मा ने कोयला
भवन में लगी आग पर अत्यंत चिंता व्यक्त की है। उन्होंने कहा
है कि इस आग से कितनी संपत्ति की क्षति हुई है तथा
कितनी जरूरी चीजें जली इसका आकलन तो बाद में ही हो
सकता है। इस आग के पीछे 'कुछ' है, इसकी संभावना से भी
इंकार नहीं किया जा सकता। </p>

<p>ट्रेकर-डम्पर भिड़ंत में 4 मरे, 14 घायल</p>

<p>वेस्ट
बोकारो, 28 मई : वेस्ट बोकारो
ओ.पी. अन्तर्गत घाटो-रामगढ़ मार्ग के सारूबेड़ा में आज
संध्या हुई ट्रेकर एवं डम्पर की भिड़ंत में चार व्यक्तियों
की मृत्यु हो गई एवं चौदह व्यक्ति घायल हो गए। पुलिस
सूत्रों के अनुसार सारूबेड़ा स्थित मस्जिद के निकट आज संध्या
चार सवारियों से भरी ट्रेकर बी.एच.एम. 9651 से टकरा गयी। ट्रेकर पर सवार चार व्यक्तियों की
घटना स्थल पर ही मृत्यु हो गई। उक्त ट्रेकर केदला से सवारियों
को लेकर रामगढ़ जा रही थी। समाचार लिखे जाने तक मृतकों
की शिनाख्त नहीं हो सकी है। किन्तु मृतक केदला, लइयो
आदि ग्रामीण क्षेत्रों के निवासी बताए जाते हैं। दुर्घटना में
घायल हुए अन्य 14 व्यक्तियों को रामगढ़
के नई सराय स्थित जुबली अस्पताल में दाखिल कराया गया
है। </p>

<p>गोला में भीषण संघर्ष,
एक मरा</p>

<p>रामगढ़
कैंट, 28 मई : गोला प्रखंड के बेटुलकला
गांव में आज भूमि विवाद को लेकर हुए भीषण खूनी
संघर्ष में एक व्यक्ति मारा गया, जबकि तीन अन्य
गम्भीर रूप से घायल हो गये। घटना की सूचना मिलते ही
पुलिस गांव पहुंच गयी। खूनी संघर्ष के बाद गांव में
तनाव कायम है और लोग भय व दहशत से अपने-अपने घरों में
दुबक गये है। घायलों में दो की स्थिति चिन्ताजनक बतायी
गयी है। घायलों को गोला राजकीय अस्पताल में प्राथमिक
उपचार के बाद रांची भेज दिया गया है। पुलिस ने शव को
अपने कब्जे में ले लिया है। इस सिलसिले में पुलिस ने तीन
लोगों को गिरफ्तार किया है। संघर्ष में घायल शिव दयाल
प्रजापति ने गोला राजकीय अस्पताल में पत्रकारो को बताया
कि आज सुबह उनके परिवार के लोग अपनी रैयती व
खतिहानी जमीन पर हल जोतने के लिये गये। अचानक परशु
राम प्रजापति के आतिरिक्त दर्जन भर पुरुष व महिला
लाठी, डंडा, भाला आदि हथियारो से लैस होकर आये और
खेत जोत रहे लोगों पर हमला कर दिया। हमलावरों ने राम दयाल
प्रजापति (55 वर्ष) की पेट में
भाला घुसेड़ दिया, जिससे भाला पेट को चिरता हुआ पीठ
की ओर निकल गया। इससे उसकी घटनास्थल पर ही मृत्यु हो गयी।
हमले में ललकु राम प्रजापति,सोना राम प्रजापति व शिव दयाल
प्रजापति घायल हो गये । ललकु राम प्रजापति की छाती, सिर
व पैर में गम्भीर चोटे हैं, जबकि सोना राम प्रजापति का दोनों
हाथ तोड़ दिया गया है और सिर पर भी हमला किया गया है।
दोनों घायलों की स्थिति चिन्ताजनक बतायी गयी है। गांव
में भूमि विवाद को लेकर कल से ही तनाव कायम था और
संघर्ष की पृष्ठभूमि बन रही थी। </p>

<p>विश्व में एक्यूप्रेशर चिकित्सा
पद्धति पाटलिपुत्र की देन</p>

<p>पटना, 28 मई : विश्व एक्यूप्रेशर दिवस के अवसर पर एक्यूप्रेशर
योग एवं अल्टरनेटिव मेडिसीन का एक दिवसीय राष्ट्रीय
सम्मेलन यहां पिछले दिन संपन्न हुआ। इस कार्यक्रम का
उद्घाटन बिहार के राज्यपाल विनोद चंद्र पांडेय ने दीप
प्रज्ज्वलित कर किया। यह कार्यक्रम गोपालगंज जिले के
नेचुआ जलालपुर स्थित भारतीय एक्यूप्रेशर योग परिषद,
बिहार एक्यूप्रेशर योग कालेज एवं इंटरनेशनल बोर्ड आफ
अल्टरनेटिव मेडिसीन एंड नेचुरोपैथी के संयुक्त तत्वावधान
में किया गया। इस कार्यक्रम में डा. सी.बी. लाभा (बंगाल),
डा. अशोक राय (सिक्किम), डा. जी.एस. भल्ला (चंडीगढ़), डा.
भगत प्रसाद नारायण (झारखंड), डा. एस.के. सिन्हा (झारखंड), डा.
कोमल तेनुसराय तथा डा. ऊषा भारती (यू.पी.) सहित एक्यूप्रेशर
चिकित्सा पद्धति के अन्य विशेषज्ञ मौजूद थे। वक्ताओं ने यह
भी रेखांकित किया कि 6000 वर्ष पुरानी यह
पद्धति विश्व को पाटलीपुत्र की देन है। प्रसिद्ध चिकित्सक डा.
फणि भूषण प्रसाद ने एक्यूप्रेशर से संबंधित अपने
अनुभवों को विस्तार से बताया। उन्होंने कहा कि शारीरिक
दर्द से निदान पाने में एक्यूप्रेशर चमत्कारिक लाभ देता है।
सम्मेलन में आतिथियों का स्वागत करते हुए भारतीय एक्यूप्रेशर
योग परिषद के अध्यक्ष डा. सवदेव प्रसाद गुप्त ने बताया कि
गठिया, जोड़ों का दर्द, साइनोसाइटिस जैसे बीमारियों
में एक्यूप्रेशर पद्धति से काफी लाभ पहुंचता है। उन्होंने कहा
कि इस पद्धति को सर्वमान्य बनाने के लिए युवक-युवतियों
को प्रशिक्षित किया जा रहा है। उन्होंने जनहित में कहा कि
पद्धति को मान्यता प्रदान करने की मांग राज्य सरकार से की
है।</p>

<p> </p>

<p>चीन को जार्ज बुश का
कूटनीतिक झटका</p>

<p>वाशिंगटन
में दलाईलामा का अभूतपूर्व स्वागत कर अमेरिका ने
कूटनीतिक युद्ध में चीन को करारा झटका दिया है। चीन
कभी स्वीकार नहीं कर सकता कि उसके आंतरिक मामले में
कोई देश हस्तक्षेप करे। पचास वर्ष पूर्व तिब्बत पर
आधिकार कर चीन ने वहां के आध्यात्मिक नेता दलाईलामा
को आत्मनिर्वासन की सजा भोगने को विवश कर दिया
था। तबसे दलाईलामा भारत की शरण में हैं, मगर उन्हें कोई
राजनीतिक गतिविधियां करने की आजादी नहीं है।
दलाईलामा को शरण देने की सजा के तौर पर 1962 में चीन ने भारत पर हमला कर और हजारों एकड़ जमीन
पर कब्जा करके अपना बदला ले लिया है। भारत के सीने पर
लगा वह जख्म अब भी यदाकदा हरा हो जाता है जब कभी तिब्बत
की आजादी का सवाल उभरता है। भारत से मदद की जो उम्मीद
दलाईलामा ने की थी, पचास वर्षों के बाद भी उसके पूरे
होने की कोई उम्मीद नहीं दिखती। लगभग नाउम्मीद हो
चुके दलाईलामा को विश्व बिरादरी से और खासकर
अमेरिका से बहुत उम्मीदें हैं जो मानवाधिकारों के मामले में
ज्यादा चौकसी बरतता है। हालांकि दलाईलामा कई बार
अमेरिका गये, पर उन्हें वह भरोसा नहीं मिला जो इस बार
राष्ट्रपति जार्ज बुश ने उन्हें दिया है। दलाईलामा कोई
राष्ट्राध्यक्ष नहीं है। इसके बावजूद ह्वाइट हाउस में उनका स्वागत करने
राष्ट्रपति जार्ज बुश खुद वहां मौजूद थे। ह्वाइट हाउस की परम्परा
के विपरीत दलाईलामा के स्वागत के पीछे अमेरिका की
गहरी कूटनीति छलक कर बाहर आ गयी है और चीन तिलमिला
उठा है। यही कारण था कि चीन ने इसका विरोध किया
और बीजिंग में अमेरिकी राजनयिक को बुलाकर अपना
प्रतिवाद दर्ज करा दिया। चीन को अपना यह विरोध दर्ज
कराने की जरूरत नहीं पड़ती अगर राष्ट्रपति बुश ने
दलाईलामा के स्वागत में अपने आधिकारियों को भेज दिया
होता। एक राष्ट्राध्यक्ष के रूप में दलाईलामा का अमेरिका में
स्वागत होने पर चीन का एतराज स्वाभाविक था। किन्तु
जार्ज बुश की यह सोची-समझी राजनीति का एक हिस्सा
था। दलाईलामा का यह स्वागत चीन के जख्म पर मिर्च की
तरह लगा है। जार्ज बुश यही चाहते भी हैं, कि चीन
तिलमिलाए और मंसूबों का अपना ही खंभा नोचने को
विवश हो जाये। ह्वाइट हाउस से दलाईलामा का सीधे भारतीय
राजदूत ललित मान सिंह के आवास पर भोजन के लिए जाना भी
चीन की दुश्वारियां बढ़ा गया होगा। इस कूटनीतिक भोज में
तिब्बत मामले की अमेरिकी आधिकारी पाउला ठोब्रियान्सकी
की उपस्थिति की सूचना भी चीन को बेहद कष्टदायी लगी
होगी। राष्ट्रपति जार्ज बुश ने तिब्बती नेता दलाईलामा
के संघर्ष में अमेरिकी मदद का भरोसा दिलाकर चीन
को शायद बौखला दिया होगा। हालांकि दलाईलामा ने
स्वीकार किया है कि वह तिब्बत की आजादी के संघर्ष
में सहयोग मांगने अमेरिका नहीं आये हैं। जार्ज बुश के
ह्वाइट हाउस से जारी वक्तव्य में भी तिब्बत की आजादी जैसी
शब्दावली के प्रयोग से बचा गया है। कहा गया है कि
दलाईलामा की तिब्बत की धार्मिक और सांस्कृतिक
आस्मिता की सुरक्षा की लड़ाई में अमेरिका उनके साथ है।
राष्ट्रपति ने आशा व्यक्त की है कि चीन धार्मिक नेता
दलाईलामा से बात करेगा और उनकी मांगों को गंभीरता
से लेगा। लगता है अमेरिका तिब्बत मामले को लेकर चीन
के सामने कठिन स्थितियां पेश करने की तैयारी कर
रहा है। इसकी पुष्टि चीन के विरोध प्रदर्शन के बावजूद
ताइवान के राष्ट्रपति चेन-शुई-बेन को उसी समय वाशिंगटन
आने की अनुमति देने से भी होती है। चीन और ताइवान के
सम्बन्ध इस समय तनावपूर्ण हैं और एकाध अवसर पर चीन ने
ताइवान पर सैनिक आभियान की चेतावनी दे रखी है। ऐसी
स्थति में ताइवान के राष्ट्रपति का वाशिंगटन में स्वागत स्पष्ट
करता है कि चीन के सामने उसके साम्राज्यवादी सपनों की
बहुत आसान राह नहीं है। चीन के प्रति अमेरिका के इस बदले
रुख से भारत राहत की सांस ले सकता है क्योंकि चीन कई
बार भारत के हिस्सों पर भी अपना आधिकार जता चुका है
जिसके कारण वाजपेयी सरकार को भी उलझनपूर्ण स्थितियों
से गुजरना पड़ा है। अभी हाल में ही अरुणाचल प्रदेश के कुछ
भूभाग पर चीनी सैनिकों की कवायद से भारत के रक्षा
मंत्रालय के होश उड़ गये थे। बेशक चीन के प्रति वर्तमान
अमेरिकी नीति से भारत को राहत मिल सकती है और चीन
तिब्बत और ताइवान को लेकर उलझा रह सकता है। हालांकि
उसकी संघर्ष क्षमता बहुआयामी है।</p>

<p> </p>






</body></text></cesDoc>