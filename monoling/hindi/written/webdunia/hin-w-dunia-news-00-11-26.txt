<cesDoc id="hin-w-dunia-news-00-11-26" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-dunia-news-00-11-26.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker, Celia Worth and Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Webdunia" internet news (www.webdunia.com), news stories collected on 00-11-26</h.title>
<h.author>Webdunia</h.author>
<imprint>
<pubPlace>Madhya Pradesh, India</pubPlace>
<publisher>Webdunia</publisher>
<pubDate>00-11-26</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>थाई पुलिस द्वारा राजन से रिश्वत लेने का
खंडन       </p>

<p>                                                                              </p>

<p> बैंकाक। थाईलैंड
में कड़ी सुरक्षा वाले अस्पताल से माफिया डॉन छोटा राजन के भाग निकलने से,
यहाँ की पुलिस ने इस बात का खंडन किया है कि छोटा राजन भागने के लिए एक
पुलिस अधिकारी को रिश्वत दी थी।</p><p>
</p><p>
पुलिस के
लेफ्टीनेंट जनरल हेमराज तरीथाई ने कहा कि शुक्रवार को छोटा राजन के फरार
होने से वरिष्ठ पुलिस अधिकारियों का कोई सरोकार नहीं है। छोटा
राजन पर पहरा दे रहे जूनयर अधिकारियों का इस घटना में दोष रहा।</p><p>
</p><p>
राष्ट्रीय पुलिस
प्रमुख पोर्नाक दुरोेंगाविबुल्या और वरिष्ठ अधिकारियों के साथ बैठक के
बाद हेमराज ने संवाददताओं से कहा- सात अधिकारियों को आपस में इस घटना
की जिम्मेदारी लेनी चाहिए। कृपया इसमें वरिष्ठ अधिकारियों को दोष मत
दीजिए।</p><p>
</p><p>
स्थानीय अखबारों
के अनुसार राजन ने अपने पूर्व वकील को टेलीफोन पर बताया कि भागने लिए उसने
मेजर जनरल रैंक के एक अधिकारी को ढाई करोड़ बाट यानी छह लाख डॉलर
की रिश्वत दी थी।</p><p>
</p><p>
पुलिस जाँच दल
के एक सदस्य ने पहले कहा था कि राजन ने चौथी मंजिल से उतरने क लिए चट्टान पर चढने
वाले औजार का इस्तेमाल किया और वहाँ पहरा दे रहे सात पुलिसकर्मियों को
इसकी कतई भनक नहीं लगी।</p><p>
</p><p>
अखबार के
अनुसार राजन ने कहा - इस तरह की कहानियाँ तो हिंदी फिल्मों में ही देखने को
मिलती हैं, मैंने तो भागने के लिए अस्पताल के इमरजेंसी दरवाजे का इस्तेमाल किया
था।</p><p>
</p><p>
ब्यौरे के
अनुसार राजन ने अपने पूर्व वकील को बताया कि वह नौका से थाईलैंड से निकलेगा
और दक्षिण पूर्व एशिया के किसी देश जाएगा इसके बाद शायद वह पश्चिम एशिया
जाएगा।</p><p>
</p><p>
हेमराज ने कहा
कि उनके ख्याल से राजन कम्बोडिया पहुँच चुका है,क्योंकि यही वह सबसे नजदीक
का देश है जहाँ वह छिप सकता है।</p><p>
</p><p>
अखबार ने अस्पताल
के एक सुरक्षा गार्ड के हवाले से कहा कि भारतीय दिखने वाले एक व्यक्ति ने टैक्सी
लाने के लिए उसे 500 बाट की टिप दी थी।</p><p>
</p><p>
सितम्बर में
बैंकाक में अपने ऊपर हुई गोलीबारी में छोटा राजन जख्मी हो गया था और तभी से
वह अस्पताल में इलाज करा रहा था।</p><p>
</p><p>
थाईलैंड में इस
बात पर सुनवाई चल रही थी कि हत्या और अन्य मामलों में वांछित माफिया डॉन को
भारत के हवाले किया जाए या नहीं। अगस्त में एक प्रमुख उद्योगपति अशरफ पटेल की
हत्या के मामले में भी पुलिस उसकी तलाश कर रही थी</p><p>
</p><p>
छोटा राजन को
भारत के हवाले करने में हुई देरी के लिए हेमराज ने भारतीय अधिकारियों
को जिम्मेदार ठहराते हुए कहा- भारतीय अधिकारियों ने इस बात का जवाब देने
में बहुत समय लिया कि राजन को उनके हवाले किया जाए या नहीं शायद अंदरूनी
दिक्कतें इसके लिए जिम्मेदार रही होंगी।</p><p>
</p><p>
राजन को
माफिया सरगना दाउद इब्राहीम का कट्टर दुश्मन माना जाता है जिसके बारे में कहा
जाता रहा है कि वह दुबई या कराची में है।          </p>

<p> </p>

<p>लेबनानी छापामारों
द्वारा इजरायली सैनिकों पर हमले                            </p>

<p>                                                                                                                                                                                                                                </p>

<p> यरूशलम। उत्तरी
मोर्चे पर लड़ाई छेड़ते हुए लेबनानी छापामारों ने रविवार को इजरायली सैनिकों
पर हमले किए जबकि इजरायल ने दो महीने के रक्तपात के कारण फिलिस्तीन के साथ
भंग हुई शांति वार्ता शुरू कराने के लिए मिঃ्र का सहारा लिया है।</p><p>
</p><p>
सीरिया समर्थित
हिजबुल्ला छापामारों ने सीमा के पास शेबा फार्म क्षेत्र में बम हमले में तीन
इजरायली सैनिकों को मार गिराने की जिम्मेदारी ली है। इजरायली लड़ाकू
विमानों ने सीमा पार जाकर बमबारी की लेकिन किसी के हताहत होने की तत्काल
कोई खबर नहीं है।</p><p>
</p><p>
हिजबुल्ला नेता
शेख हासन नसरूल्ला ने शनिवार को कहा कि शेबा फार्म को इजरायल के कब्जे से
छुड़ाने का एकमात्र उपाय युध्द है जिसे इजरायल ने 1967 में पश्चिम एशिया लड़ाई के
दौरान सीरिया से छीन लिया था। हिजबुल्ला को ईरान का भी समर्थन मिल रहा
है।</p><p>
</p><p>
इस बीच पश्चिम
एशिया की कूटनीति का अखाड़ा मिঃ्र बना हुआ है जहाँ, राष्ट्रपति होस्नी मुबारक ने
इजरायली प्रधानमंत्री एहूद बराक के एक वरिष्ठ सलाहकार से पश्चिमी तट और
गाजा पट्टी में हिंसा रोकने के उपायों पर बातचीत की है ।</p><p>
</p><p>
अरब देशों में
सिर्फ मिঃ्र ही ऐसा देश है जिसने इजरायल के साथ शांति संधि की है। एक
सप्ताह पहले ही मिঃ्र ने इजरायल से अपना राजदूत बुला लिया था और आरोप लगाया था
कि इजरायल फिलिस्तीनियों पर हमले कर रहा है इसके बाद इजरायल ने अपने
अधिकारी डेनी यातोम को मिঃ्र भेजा है।</p><p>
</p><p>
इजरायली
राजनीतिक सूत्रों ने कहा है कि उनके मंत्री और एक पूर्व इजरायली जासूस ने
शनिवार की रात गाजा पट्टी में फिलिस्तीनी नेता यासेर अराफात से गुप्त मुलाकात की
लेकिन फिलिस्तीन के एक वरिष्ठ अधिकारी ने इसका खंडन किया है।</p><p>
</p><p>
इजरायली सूत्रों
ने कहा कि मंत्री अमनोन लिपकिन शाहाक और इजरायली गुप्तचर सेवा शिन बेट के
पूर्व प्रमुख आमी अयालोन बराक के कहने पर अराफात से मिल।े काहिरा में
शनिवार को अराफात ने रूसी राष्ट्रपति ब्लादिमीर पुतिन के साथ शुक्रवार को हुई
बातचीतकी जानकारी मुबारक को दी।</p><p>
</p><p>
फिलिस्तीनियों
की मंशा है कि इजरायल के सहयोगी अमरीका का मुकाबला करने के लिए रूस को
पश्चिम एशिया शांति प्रक्रिया में अधिक भूमिका निभानी चाहिए।</p><p>
</p><p>
अराफात ने
जार्डन के टेलीविजन से कहा कि उन्हें इसकी उम्मीद है कि राजनयिक
गतिविधियों से उस हिंसा को रोकने का कोई रास्ता निकलेगा जिसमें अब
तक करीब तीन सौ जानें जा चुकी हैं।</p><p>
</p><p>
उन्होंने कहा-
मुझे उम्मीद है कि राजनयिक प्रयासों तथा अरब एवं अंतरराष्ट्रीय समुदाय की पहल के
सकारात्मक परिणाम सामने आएँगे और फिलिस्तीनियों के खिलाफ इजरायली हमले
रूकेंगे, लेकिन गाजा और पश्चिमी तट में मौतें बढ़ने का सिलसिला जारी है।
शनिवारको इजरायली सैनिकों ने चार फिलिस्तीनियों को मार डाला यह जानकारी
अस्पताल के सूत्रों ने दी।                 </p>

<p> </p>

<p>भारत में अंतरराष्ट्रीय
निवेशकों के दायित्व                                            </p>

<p>                                                                                                                                                                                                                                </p>

<p> 16वें भारत आर्थिक शिखर सम्मेलन में
प्रधानमंत्री</p><p>
</p><p>
नई दिल्ली।
प्रधानमंत्री अटल बिहारी वाजपेयी ने आर्थिक सुधार और वैश्वीकरण के
प्रति सरकार की वचबद्धता को दोहराते हुए रविवार को अंतरराष्ट्रीय निवेशकों के
समक्ष दायित्वों की नौ सूत्री सूची रखी जिसमें भारत में लम्बे समय तक काम करने
की प्रतिबद्धता प्रमुख है ।</p><p>
</p><p>
वाजपेयी ने यहाँ
भारतीय उद्योग परिसंघ (सीआईआई)और दावोस,स्विट्जरलैंड के वर्ल्ड इकोनामिक
फोरम (डब्लूईएफ) के तत्वावधान में आयोजित 16वें भारत आर्थिक शिखर
सम्मेलन का उद्घाटन करते हुए कहा कि विश्वस्तर पर कारोबार करने का अवसर और
अधिकारपाने के साथ-साथ कुछ जिम्मेदारियाँ भी उठानी होंगी ताकि वैश्वीकरण
की प्रक्रिया सबके लिए लाभदायक और सर्वग्राह्य बन सके।</p><p>
</p><p>
इसी संबंध
में वाजपेयी ने सरकार और निजी क्षेत्र के बीच आर्थिक और सामाजिक दोनों
क्षेत्रों के विकास में सहभागिता पर बल देते हुए देशी और विदेशी निवेशकों के
लिए नौ सूत्री दायित्वों की सूची गिनाई और कहा कि सबसे प्रथम खासकर विदेशी
निवेशकों को भारत में लम्बे समय तक टिकने का इरादा लेकर आना होगा।</p><p>
</p><p>
उद्योगों के
समक्ष दायित्वों की सूची रखते हुए प्रधानमंत्री ने कहा कि - विदेशी निवेशक
लंबे समय के लिए आएँगे तभी विश्वास का संबंध बन पाएगा।</p><p>
</p><p>
-उद्योगों को
एकाधिकार कायम करने या दुरभिसंधि करने की बजाय उपभोकताओं के
हित में स्वतंत्र और निष्पक्ष प्रतिस्पर्धा की भावना से काम करना चाहिए।</p><p>
</p><p>
-कंपनियाँ
कारोबार के अच्छे मानक अपना कर निवेशकों का विश्वास जीतेें व उद्योग अपने
कर्मचारियों और उनके बाल बच्चों के शिक्षण-प्रशिक्षण के जरिए मानव संसाधन
विकास को प्राथमिकता दें।</p><p>
</p><p>
-प्रौद्योगिकी
विकास के लिए अनुसंधान विकास और नवप्रवर्तन पर विशेष ध्यान दिया जाए।</p><p>
</p><p>
-भारत को
केवल सूचना प्रौद्योगिकी प्राप्त करने का स्थान न मान कर इसे उत्पादन का कंेद्र
बनाया जाए।</p><p>
</p><p>
-पर्यावरण संरक्षण
और प्रदूषण नियंत्रण और प्रबंध के उच्च मानक अपनाए जाएँ।</p><p>
</p><p>
-ग्रामीण क्षेत्र
के लोगों की जरूरतों का भी बराबर ध्यान रखा जाए तथा शिक्षा और स्वास्थ्य
सेवाओं के विस्तार में सरकार का हाथ बँटाया जाए।</p><p>
</p><p>
-प्रधानमंत्री
ने सवाल किया कि क्या सीआईआई की सदस्य 4000 कंपनियाँ और इस सम्मेलन में
उपस्थित तीन सौ विदेशी कंपनियाँ भारत में एक प्राथमिक विद्यालय और प्राथमिक
स्वास्थ्य केंद्र की जिम्मेदारी उठा सकती है।</p><p>
</p><p>
वाजपेयी ने कहा
कि इस प्रकार की भागीदारी भारत का नक्शा बदल सकती है और देश के लोगों के
जीवन में गुणात्मक परिवर्तन ला सकती है।      </p>

<p> </p>

<p>भारत में अंतरराष्ट्रीय
निवेशकों के दायित्व                                            </p>

<p>                                                                                                                                                                                                                                </p>

<p> 16वें भारत आर्थिक शिखर सम्मेलन में
प्रधानमंत्री</p><p>
</p><p>
नई दिल्ली।
प्रधानमंत्री अटल बिहारी वाजपेयी ने आर्थिक सुधार और वैश्वीकरण के
प्रति सरकार की वचबद्धता को दोहराते हुए रविवार को अंतरराष्ट्रीय निवेशकों के
समक्ष दायित्वों की नौ सूत्री सूची रखी जिसमें भारत में लम्बे समय तक काम करने
की प्रतिबद्धता प्रमुख है ।</p><p>
</p><p>
वाजपेयी ने यहाँ
भारतीय उद्योग परिसंघ (सीआईआई)और दावोस,स्विट्जरलैंड के वर्ल्ड इकोनामिक
फोरम (डब्लूईएफ) के तत्वावधान में आयोजित 16वें भारत आर्थिक शिखर
सम्मेलन का उद्घाटन करते हुए कहा कि विश्वस्तर पर कारोबार करने का अवसर और
अधिकारपाने के साथ-साथ कुछ जिम्मेदारियाँ भी उठानी होंगी ताकि वैश्वीकरण
की प्रक्रिया सबके लिए लाभदायक और सर्वग्राह्य बन सके।</p><p>
</p><p>
इसी संबंध
में वाजपेयी ने सरकार और निजी क्षेत्र के बीच आर्थिक और सामाजिक दोनों
क्षेत्रों के विकास में सहभागिता पर बल देते हुए देशी और विदेशी निवेशकों के
लिए नौ सूत्री दायित्वों की सूची गिनाई और कहा कि सबसे प्रथम खासकर विदेशी
निवेशकों को भारत में लम्बे समय तक टिकने का इरादा लेकर आना होगा।</p><p>
</p><p>
उद्योगों के
समक्ष दायित्वों की सूची रखते हुए प्रधानमंत्री ने कहा कि - विदेशी निवेशक
लंबे समय के लिए आएँगे तभी विश्वास का संबंध बन पाएगा।</p><p>
</p><p>
-उद्योगों को
एकाधिकार कायम करने या दुरभिसंधि करने की बजाय उपभोकताओं के
हित में स्वतंत्र और निष्पक्ष प्रतिस्पर्धा की भावना से काम करना चाहिए।</p><p>
</p><p>
-कंपनियाँ
कारोबार के अच्छे मानक अपना कर निवेशकों का विश्वास जीतेें व उद्योग अपने
कर्मचारियों और उनके बाल बच्चों के शिक्षण-प्रशिक्षण के जरिए मानव संसाधन
विकास को प्राथमिकता दें।</p><p>
</p><p>
-प्रौद्योगिकी
विकास के लिए अनुसंधान विकास और नवप्रवर्तन पर विशेष ध्यान दिया जाए।</p><p>
</p><p>
-भारत को
केवल सूचना प्रौद्योगिकी प्राप्त करने का स्थान न मान कर इसे उत्पादन का कंेद्र
बनाया जाए।</p><p>
</p><p>
-पर्यावरण संरक्षण
और प्रदूषण नियंत्रण और प्रबंध के उच्च मानक अपनाए जाएँ।</p><p>
</p><p>
-ग्रामीण क्षेत्र
के लोगों की जरूरतों का भी बराबर ध्यान रखा जाए तथा शिक्षा और स्वास्थ्य
सेवाओं के विस्तार में सरकार का हाथ बँटाया जाए।</p><p>
</p><p>
-प्रधानमंत्री
ने सवाल किया कि क्या सीआईआई की सदस्य 4000 कंपनियाँ और इस सम्मेलन में
उपस्थित तीन सौ विदेशी कंपनियाँ भारत में एक प्राथमिक विद्यालय और प्राथमिक
स्वास्थ्य केंद्र की जिम्मेदारी उठा सकती है।</p><p>
</p><p>
वाजपेयी ने कहा
कि इस प्रकार की भागीदारी भारत का नक्शा बदल सकती है और देश के लोगों के
जीवन में गुणात्मक परिवर्तन ला सकती है।      </p>

<p> </p>

<p>बांग्लादेश के कपड़ा
कारखाने में आग, 45 मरे                                    </p>

<p>                                                                                                                                                                                                                                </p>

<p> ढाका। बांग्लादेश
में एक कपड़ा कारखाने में आग लगने के बाद भगदड़ मचने से 45 कर्मचारियों की
मृत्यु हो गई। मृतकों में ज्यादातर महिलाएँ व बच्चे हैं। कोई 100 घायलों
को अस्पताल में भर्ती कराया है। घायलों में अनेक गंभीर हैं।</p><p>
</p><p>
यह हादसा ढाका
से 40 कि.मी. पूर्व में स्थित चौधरी निटविअर गारमेंट्स फैक्टरी में हुआ।
फैक्टरी की इमारत चार मंजिली है। फैक्टरी में 900 कर्मचारी काम करते हैं।</p><p>
</p><p>
आग की शुरुआत
चौथी मंजिल पर हुई। कर्मचारी दहशत में भागने लगे। सँकरी सीढ़ियों पर भगदड़
में अनेक कुचल गए। भूतल पर एकमात्र निकासी द्वार सुरक्षा कारणों से बंद था।
दमकल आने पर द्वार तोड़ा गया। आग लगने का कारण शार्टसर्किट बताया जा रहा
है। सूत्रों के अनुसार मृतकों की संख्या और भी बढ़ सकती है।</p>

<p>                                                                                                               </p>

<p>आतंकवादियों के हमले
में चार सैनिक शहीद                                       </p>

<p>                                                                                                                                                                                                                                </p>

<p> श्रीनगर। पाकिस्तान
समर्थक आतंकवादियों ने सेना की एक गश्ती टुकड़ी पर हमला कर 4 सैनिकोें की
जान ले ली और 5 अन्य को घायल कर दिया। कश्मीर में संघर्ष विराम की घोषणा
के उपरांत सुरक्षा बलों को भड़काने की नीयत से किया गया यह दूसरा घातक हमला
है। इसके अलावा आतंकवादी दो नरसंहारों में दस नागरिकों को भी मौत के
घाट उतार चुके हैं।</p><p>
</p><p>
अधिकारियों
ने बताया कि आतंकवादियों ने यह हमला शनिवार शाम कश्मीर घाटी के कुपवाड़ा जिले
के पथुशाही क्षेत्र में किया। घात लगाकर किए गए इस हमले में राष्ट्रीय रायफल्स के
4 सैनिक शहीद हो गए।</p><p>
</p><p>
घायलों को
अस्पताल में भर्ती करवाया गया है, जहाँ उनकी दशा नाजुक बताई जाती है। हमले के
बाद आतंकवादी जंगलों में भागने में कामयाब रहे। भागने से पूर्व वे एक एके-47
रायफल तथा एक लाइट मशीनगन भी अपने साथ ले गए। हमलावर आतंकवादियों की तलाश
जारी है। चौंकाने वाला तथ्य यह भी है कि इस हमले की जानकारी
अधिकारियों ने वरिष्ठ अधिकारियों को एक दिन बाद दी है।                                                         </p>

<p> </p>

<p>हिज्बुल और हुर्रियत की
टूट पर टिका है मिशन कश्मीर                      </p>

<p> --उषा डुग्गर                                                                                                </p>

<p> श्रीनगर। भारत सरकार
ने कश्मीर समस्या के हल की खातिर जो 'मिशन कश्मीर' संघर्ष विराम के सहारे आरंभ
किया है, वह अब टूट-फूट की आस पर टिका है। यह टूट-फूट हिज्बुल मुजाहिदीन
तथा सर्वदलीय हुर्रियत कॉन्फ्रेंस में नजर आ रही है। भारत सरकार इसका लाभ उठाना
चाहती है।</p><p>
</p><p>
केंद्र सरकार के
'मिशन कश्मीर' को पाकिस्तान समर्थक आतंकवादियों ने घातक हमलों तथा नरसंहारों
के सहारे तोड़ने का प्रयास किया है मगर भारत सरकार अपने इरादों से टस से मस नहीं
हुई है।</p><p>
</p><p>
विश्वसनीय सूत्रों
के अनुसार उसकी इस आस का कारण आतंकवादी दलों तथा हुर्रियत कॉन्फ्रेंस में होने
जा रही टूट है। एकाध दिन में हिज्बुल मुजाहिदीन के दो हिस्सों में बँट जाने
की घोषणा सामने आ सकती है। बताया जाता है कि हिज्बुल मुजाहिदीन के कश्मीर
घाटी केकमांडर मजीद डार हिज्बुल को दो हिस्सों में बाँटने के लिए तैयार हो गए
हैं। यह बात अलग है कि हिज्बुल मुजाहिदीन के सुप्रीम कमांडर सैयद सलाहुद्दीन द्वारा
ताजा संघर्ष विराम को मानने के लिए शर्तों पर शर्तें रखी जा रही हैं, परंतु इस सचाई
से इंकार नहीं किया जा सकता कि मजीद डार ने इसका स्वागत किया है।</p><p>
</p><p>
अगर सूत्रों पर
विश्वास करें तो भारत सरकार ने मजीद डार को उस समय ही अपने पक्ष में कर लिया था,
जब जुलाई में हिज्बुल मुजाहिदीन ने संघर्ष विराम घोषित किया था। केंद्रीय दल के
साथ वार्ता के प्रथम दौर के उपरांत ही मजीद डार ने अपने इस रुख से सभी को अवगत
करवाया था। यह इसी से स्पष्ट होता है कि मजीद डार को कश्मीर में सभी सरकारी
सुविधाएँ उपलब्ध करवाई जा रही हैं।</p><p>
</p><p>
जम्मू-कश्मीर में
सक्रिय आतंकवादियों में से 80 प्रतिशत आतंकवादी हिज्ब से संबंधित हैं। केंद्र
को अपने मिशन कश्मीर की सफलता हुर्रियत के बयानों के कारण भी कामयाब होती
दिखाई दे रही है।</p>

<p>                                                                                                               </p>

<p>डीएचआर विश्व को
समर्पित                                                                  </p>

<p>                                                                                                                                                                                                                                </p>

<p> दार्जिलिंग। रेलमंत्री
सुश्री ममता बनर्जी ने रविवार को 119 वर्ष पुराने दार्जिलिंग हिल रेलवे (डीएचआर) जो
यूनेस्को का विश्व विरासत स्थल है, को विश्व समुदाय को समर्पित किया। इस अवसर
पर यहाँ एक रंगारंग कार्यक्रम आयोजित किया गया।</p><p>
</p><p>
डीएचआर 1881 में
खोला गया था। यह विश्व की दूसरी नैरो गेज रेलवे थी, जिसने विश्व विरासत का
ओहदा पाया। इसके पहले हालाँकि ऑस्ट्रिया की सीमरिंग हिल रेलवे को यह
ओहदा दिया जा चुका है।</p>

<p>                                                                                                               </p>

<p>बुश ने चार काउंटियों
के खिलाफ मुकदमा दायर किया                        </p>

<p>                                                                                                                                                                                                                                </p>

<p> वॉशिंगटन ।
अमेरिका के राष्ट्रपति चुनाव के 124 सालों के इतिहास में यह पहला अवसर है जब
चुनाव अधिकारियों को विजयी उम्मीदवार घोषित करने में अड़चन आ रही है। उधर
फ्लोरिडा की एक काउंटी में रिपब्लिकन उम्मीदवार जॉर्ज बुश का बहुमत भी पुनर्मतगणना
में 930 से घटकर 464 पर पहुँच गया। बुश ने चार काउंटियों के खिलाफ मुकदमा
दायर करके सैनिकों के अवैध मतपत्रों की जाँच कराने की माँग की है।</p><p>
</p><p>
नई स्थितियों
को देखते हुए अनुमान लगाया जा रहा है कि नतीजों का विवाद दिसंबर तक खिंच
जाएगा।अमेरिकी राष्ट्रपति चुनाव के नतीजों की उलझी गुत्थी अभी भी सुलझती दिखाई
नहीं दे रही है।</p><p>
</p><p>
फ्लोरिडा में
मतों की हाथ से गिनती को लेकर डेमोक्रेटिक पार्टी के उम्मीदवार अल गोर और
रिपब्लिकन उम्मीदवार जॉर्ज बुश आमने-सामने आ गए हैं। हाथ से मतों की गिनती के
नतीजों को अंतिम परिणाम में शामिल न करने की माँग बुश शुरू से कर रहे हैं,
जबकिगोर इसके पक्ष में है। फ्लोरिडा के सुप्रीम कोर्ट ने भी गोर के पक्ष में ही
फैसला दिया, जिसके विरुद्ध बुश अमेरिकी सुप्रीम कोर्ट में चले गए थे।</p><p>
</p><p>
फ्लोरिडा की
ब्रोवार्ड काउंटी में शनिवार रात गिनती समाप्त हो गई। यहाँ बुश की बढ़त 930 से
घटकर अब 464 हो गई है। पाम बीच तथा केनवेसर्स में रविवार रात तक मतगणना का
काम समाप्त हो जाने के आसार हैं।</p><p>
</p><p>
गोर का दावा
है कि उनके मतों की संख्या में करीब 100 की बढ़ोतरी हुई है।उधर
रिपब्लिकन जॉर्ज डब्ल्यू बुश ने चौदह अन्य काउंटियों के खिलाफ अपने मुकदमे वापस
लिए जाने के कुछ ही घंटों के बाद फ्लोरिडा के चार काउंटियों के खिलाफ
मुकदमा दायर किया है। इनमें सैनिकों के अवैध मतपत्रों की समीक्षा की
माँग की गई है, जो राष्ट्रपति चुनाव में निर्णायक साबित हो सकते हैं।</p><p>
</p><p>
बुश के चुनाव
अभियान प्रवक्ता नेशनिवार को कहा कि वे पाँचवीं काउंटी के खिलाफ भी मामला दायर
करेंगे, ताकि तकनीकी आधार पर अवैध करार दिए गए मतपत्रों की दोबारा
समीक्षा की जा सके। उन्होंने पोल्क पासको, हिल्सबोरो और ओकालूसा
काउंटी के खिलाफमामला दायर किया है। एक और मामला ओरैंज काउंटी के
खिलाफ दायर किया जाएगा।</p><p>
</p><p>
बुश के चुनाव
अभियान के प्रवक्ता ने कहा कि सैनिकों के मतपत्रों की दोबारा जाँच कराने की
माँग पर पुनर्विचार किया जाना चाहिए। इससे पूर्व शनिवार को टालाहासी में सर्किट
अदालत में मामला दायर करते समय बुश के वकील ने कहा कि रिपब्लिकन उम्मीदवार
फ्लोरिडा की अधिकतर मतगणना से संतुष्ट हैं, लेकिन उनका आग्रह है कि
सैनिकों के अवैध मतपत्रों की समीक्षा होना चाहिए।</p><p>
</p><p>
इस बीच पश्चिम
पाम बीच से प्राप्त समाचारों के अनुसार फ्लोरिडा की दो काउंटियों में पुनर्मतगणना
का काम शनिवार को भी जारी रहा। फ्लोरिडा के सुप्रीम कोर्ट ने हाथों से
दोबारा मतगणना का काम रविवार तक निपटा देने की मोहलत दी थी। अमेरिका के
सुप्रीम कोर्टके बुश द्वारा दायर याचिका को सुनवाई के लिए मंजूर कर लेने से यह
विवाद अब दिसंबर तक खिंच जाएगा।</p><p>
</p><p>
बुश और गोर
को प्राप्त हुए मतों का अंतर बहुत कम है। इस बीच गोर के समर्थकों ने फ्लोरिडा
की एक काउंटी मियामी डेड के नतीजों को चुनौती देने का फैसला किया है।
इस कारण कानूनी विवाद और गहरा जाएगा। उनके सहयोगियों का कहना है कि
गोर संभवतः पाम बीच के नतीजों को भी चुनौती दे सकते हैं।                                                                                        </p>

<p>भारत विरोधी
हरकतों का संचालन स्थल बनता नेपाल                        </p>

<p>                                                                                                                                                                                                                                </p>

<p>                                                                                                               नई दिल्ली ।
पाकिस्तान की खुफिया एजेंसी इंटर सर्विसेज इंटेलिजेंस ने नेपाल से लगे भारत के
सीमावर्ती इलाकों में पिछले कई वर्षों से भारत विरोधी अभियान छेड़ रखा
है। साथ ही उसने नेपाल के अस्तित्व के लिए भी गंभीर खतरा उत्पन्न कर दिया है।</p><p>
</p><p>
आईएसआई भारत
में आतंकवाद, तस्करी एवं जाली मुद्रा के जरिए अर्थव्यवस्था को प्रभावित करने के लिए
नेपाल की भूमि का उपयोग कर रही है। नेपाल में लगातार बढ़ रही मुस्लिम आबादी
तथा मस्जिद, मदरसों ने वहाँ गंभीर खतरा पैदा कर दिया है। वहाँ मुस्लिम संगठनों
की बढ़ती तादाद भी चिंता का कारण है।</p><p>
</p><p>
सीमा सुरक्षा बल
के महानिदेशक वीरेंद्र कुमार गौड़ ने अपनी नई पुस्तक 'आईएसआई और भारत में
आतंकवाद' में यह विचार व्यक्त किया है। उन्होंने व्यक्तिगत अनुभवों, मीडिया
रिपोर्टों तथा 'वेबसाइट' पर उपलब्ध प्रधानमंत्री के प्रधान सचिव बृजेश
मिश्र द्वारा नेपाली अधिकारियोंको सौंपी गई रिपोर्ट में वर्णित तथ्यों के
आधार पर यह पुस्तक लिखी है।शिल्पायन प्रकाशन से प्रकाशित इस पुस्तक की रचना
में बल के एक अन्य महानिरीक्षक एवं जाने-माने लेखक विभूति नारायण राय ने भी सहयोग
किया है।</p><p>
</p><p>
पुस्तक मे कहा
गया है कि नेपाल में मुस्लिम आबादी वर्ष 1981 में साढ़े 3 प्रतिशत थी, वहीं अब यह
आँकड़ा पाँच से अधिक और संभवतः 10 प्रतिशत की सीमा-रेखा पार कर
चुका है। इसी प्रकार गैर सरकारी मुस्लिम संगठनों की संख्या में भी बढ़ोतरी हुई
है और अभी ऐसे 26 संगठन वहाँ सक्रिय हैं।</p><p>
</p><p>
पाकिस्तान के
खिलाफ वर्ष 1965 तथा 1971 में सक्रिय भूमिका अदा कर चुके गौड़ ने लिखा है कि
उत्तर प्रदेश, बिहार और पश्चिम बंगाल से लगते नेपाल के वांके, कपिलवस्तु, रूपनदेही,
नेवलपाशी, परसा, रौताहद, सिराहा, सप्तारी, सुनसारी एवं उदयपुर जिलों में मस्जिदों
एवंमदरसों की संख्या बढ़कर इन दिनों 451 हो चुकी है।                                                                      </p>

<p> </p>

<p>बिन लादेन के खिलाफ
अमेरिका और रूस का साझा अभियान         </p>

<p>                                                                                                                                                                                                                                </p>

<p> लंदन । अमेरिका ने
रूस के साथ मिलकर इस्लामी आतंकवादी ओसामा बिन लादेन के खिलाफ जवाबी
कार्रवाई करने की योजना बनाई है। इस आतंकवादी को अफगानिस्तान की
तालिबान सरकार का समर्थन मिल रहा है।</p><p>
</p><p>
अमेरिका का
मानना है कि पूर्वी अफ्रीका में उसके दूतावासों पर हमले तथा अमेरिकी जहाजों पर
बमबारी के पीछे बिन लादेन का ही दिमाग है।</p><p>
</p><p>
लंदन के अखबार
'टाइम्स' के अनुसार अमेरिका तथा रूस के इस संयुक्त अभियान के मुखिया जनरल हैनरी
शेल्टन ने बताया कि बिन लादेन के खिलाफ कार्रवाई में क्रूज मिसाइल समेत किसी भी
हथियार का उपयोग किया जा सकता है।</p><p>
</p><p>
इस बारे में रूसी
सूत्रों का कहना है कि अमेरिका के साथ साझा जवाबी कार्रवाई का फैसला इसलिए
किया गया, ताकि इस्लामी जेहाद को समय रहते दबाया जा सके।</p>

<p>                                                                                                               </p>

<p>एवरेस्ट पर विमोचित
राजस्थानी पुस्तक रूसी वेबसाइट पर                  </p>

<p>                                                                                                                                                                                                                                </p>

<p> बीकानेर। एक युवा
साहित्यकार की 8 वर्ष पूर्व हिमालय पर्वत की एवरेस्ट चोटी की 18100 फीट पर
विमोचित राजस्थानी भाषा की पुस्तक 'बेलू रा मोती' कथा संग्रह को हाल ही में रूस
की वेबसाइट पर स्थन मिला है।</p><p>
</p><p>
पुस्तक के
लेखक व पर्वतारोही बीरकानेर निवासी जगमोहन सक्सेना ने बताया कि बेलू रा मोती
का विमोचन देश के माने हुए पर्वतारोही मगन बिस्सा ने 8 मई 1992 को एवरेस्ट शिखर
पर चढ़ाई के दौरान किया था। लेखक जगमोहन सक्सेना भी पुस्तकों का सेट पीट
पर लादे इस दुर्गम स्थल पर मौजूद थे।                                                                                                    </p>

<p> </p>

<p>जॉर्ज बुश 537 मतों से
विजेता घोषित</p>

<p>                                                                                                               </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>टालाहासी, (फ्लोरिडा)। मतदान के
करीब तीन सप्ताह बाद फ्लोरिडा राज्य के चुनाव अधिकारियों ने रविवार की रात
को यहाँ राष्ट्रपति चुनाव में रिपब्लिकन उम्मीदवार जॉर्ज डब्ल्यू बुश को 537 मतों से
विजेता घोषित कर दिया।</p><p>
</p><p>
इस चुनाव
परिणाम के बारे में डेमोेक्रेट प्रत्याशी और उपराष्ट्रपति अल गोर के समर्थकों का
कहना है कि वे इस परिणाम को चुनौती देंगे। गोर के वकीलों ने इस चुनाव
परिणाम के कानूनी चुनौती देने की तैयारी पूरी कर ली है।</p><p>
</p><p>
स्थानीय समय के
अनुसार रात को सेक्रेट्री ऑफ स्टेट कैथरीन हैरिस ने घोषणा की कि फ्लोरिडा
की 67 काउंटीन के मतों की गिनती के अनुसार राज्य में रिपब्लिकन उम्मीदवार जॉर्ज
बुश ने बहुमत प्राप्त किया है। शाम 5 बजे तक हाथों से गिने गए मतपत्रों के परिणाम
स्वीकार किए गए।</p><p>
</p><p>
हालाँकि पाम
बीच काउंटी की ओर से निर्धारित समय सीमा समाप्त होने से पहले कहा गया कि
गिनती के लिए 1 हजार से भी कम मत रह गए हैं, इसलिए चुनाव अधिकारियों को
देर से अंतिम परिणाम बताने की इजाजत दी जाए, पर ऐसा नहीं करने दिया गया।</p><p>
</p><p>
हैरिस ने अपनी घोषणा
में कहा कि राज्य चुनाव कैनवासिंग कमीशन और फ्लोरिडा राज्य के नियमों के तहत
वे टेक्सास के गवर्नर जॉर्ज बुश को फ्लोरिडा के 25 निर्वाचन मंडल मतों का विजेता
घोषित करती है। उनके अनुसार बुश को राज्य में 2,912,790 और अल गोर को
2,912,253 मत मिले।                                                                                                         </p>

<p> </p>

<p>इराकी उप राष्ट्रपति भारत
के लिए रवाना                                               </p>

<p>                                                                                                                                                                                                                                </p>

<p> बगदाद। इराक के
उप राष्ट्रपति ताहा यासीन रमदान रविवार को भारत के लिए रवाना हो गए। पिछले 25
वर्षों मंे इराक के शीर्ष स्तर के किसी इराकी अधिकारी की यह पहली यात्रा
है।</p><p>
</p><p>
इराकी संवाद
समिति ने बताया कि उप राष्ट्रपति के साथ तेल मंत्री अमीर मुहम्मद राशिद और विदेश
मंत्रालय में वरिष्ठ अवर सचिव नूरी इस्माइल भी रवाना हुए।</p><p>
</p><p>
रमदान ने बताया
कि इस यात्रा का उद्देश्य आर्थिक, राजनीतिक, वाणिज्यिक, वैज्ञानिक एवं सांस्कृतिक
सहयोग के तरीकों पर विचार विमर्श करना है।</p><p>
</p><p>
रमदान भारतीय
अधिकारियों को इराक पर पिछले 10 वर्षों से चल रहे संयुक्त राष्ट्र
प्रतिबंध के प्रभाव से अवगत कराएंगे।                                     </p>

<p> </p>






</body></text></cesDoc>