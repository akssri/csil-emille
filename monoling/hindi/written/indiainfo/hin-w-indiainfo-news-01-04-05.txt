<cesDoc id="hin-w-indiainfo-news-01-04-05" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-indiainfo-news-01-04-05.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Indiainfo" internet news (www.indiainfo.com), news stories collected on 01-04-05</h.title>
<h.author>Indiainfo.com Ltd.</h.author>
<imprint>
<pubPlace>India (Mumbai, Hyderabad, Bangalore)</pubPlace>
<publisher>Indiainfo.com Ltd.</publisher>
<pubDate>01-04-05</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>माधवपुरा
बैंक अध्यक्ष ने किया आत्मसमर्पण </p>

<p>मंंुबई ।
बिग बुल केतन पारेख को १३७ करोड़ रुपए के पे-आर्डर जारी करने वाले
माधवपुरा को-ऑपरेटिव बैंक के अध्यक्ष रमेश पारेख को आज सीबीआई के
सामने आत्मसमर्पण किया ।</p>

<p>सीबीआई का कहना
है कि आज सुबह पारेख खुद सीबीआई के कार्यालय में आए और उन्होंने आत्मसमर्पण
किया । आत्मसमर्पण के बाद सीबीआई उनसे लगातार पूछताछ कर रही
है । </p>

<p>आधिकारिक सूत्रों
का कहना है कि पारेख अपने एक संबंधी के यहां भावनगर गए हुए थे जिसकी वजह से
उन्हंंे गिरफ्तार नहीं किया जा सका था । </p>

<p>वर्मा
के खिलाफ आडियो सबूत</p>

<p>नई
दिल्ली । सीबीआई का कहना है कि उसके पास सीबीईसी के बर्खास्त अध्यक्ष बी.पी.
वर्मा के खिलाफ ऑडियो टेप्स हैं जो यह दर्शाते हैं कि उन्होंने चेन्नई
स्थित कंपनी ए.के. एंटरप्राइजेज के के. विजय प्रताप के बीच अवैध धन की
सौदेबाजी होती थी ।</p>

<p>सीबीआई सूत्रों का
कहना है कि हमारे पास ऐसे ऑंडियो टेप्स हैं जो यह दर्शाते हैं कि विजय
प्रताप, वर्मा और उनके पुत्र सिद्धार्थ वर्मा के साथ लेनदेन करता था । वर्मा पर यह
आरोप है कि भारी वित्तीय लाभ की एवज में वे ए.के. एंटरप्राइजेज को लाभ
पहंुचाते थे । वर्मा पर यह आरोप भी है कि वे विजय प्रताप को वित्तीय
बिचौलिये के रुप में उपयोग करते थे । विजय प्रताप के जरिये विदेश से आने
वाले कंसाइन्मेंट क्लियर किए जाते थे ।</p>

<p>विजय प्रताप ने
पूछताछ के दौरान यह स्वीकार किया है कि उसने वर्मा को कंसाइन्मेंट
क्लियर करने की एवज में धन दिया ।</p>

<p>गौरतलब है कि
राजीव कुमार ने अपनी गिरफ्तारी के बाद बुधवार को यह स्वीकार किया कि विजय
प्रताप ने उसे वर्मा को देने के लिए दो लाख रुपए अदा किए थे । सीबीआई का कहना
है कि अब वर्मा के एक और एजेंट मोहन गुप्ता की तलाश की जा रही है,जो कि
पाउच बनाने के कारोबार से जुड़ा हुआ है । सीबीआई को आशंका है
कि गुप्ता के नाम की सम्पत्तियां असल में वर्मा की ही हैं ।</p>

<p>भावना पांडे
के बारे में सीबीआई का कहना है कि वह नैनीताल की उम्रदराज महिला है और वह
वर्मा की अच्छी मित्र है । सीबीआई के मुताबिक जो अधिकारी अच्छी जगह
नियुक्ति चाहते थे वे भावना के जरिये वर्मा तक पहंुचते थे । अचरज की बात है
कि सीबीआई ने एफआईआर में भावना का जिक्र नहीं किया है । </p>

<p>मुशर्रफ
ने राष्ट्रपति बनने का संकेत दिया </p>

<p>इस्लामाबाद ।
पाकिस्तान के सैनिक शासक परवेज मुशर्रफ ने संकेत दिया है कि उच्चतम न्यायालय
द्धारा पाकिस्तान में लोकतंत्र की बहाली की मियाद समाप्त होने के बाद वे
राष्ट्रपति बनेंगे । </p><p>
</p><p>
मुशर्रफ ने अपने एक साक्षात्कार में कहा सरकार वर्ष २००२ में मेरे राष्ट्रपति बनने
को लेकर सभी पहलुओं की जांच कर रही है । न्यायालय ने १२ अक्टूबर २००२
तक देश में चुनाव करवाने की अंतिम समय-सीमा दी है । </p><p>
</p><p>
उन्होंने कहा कि हम लोग प्रस्ताव के लाभ-हानि पर गौर कर रहे हैं  और उचित समय
आने पर फैसला करेंगे । सैन्य शासक ने कहा कि सरकार की कार्रवाईयों को
जारी व सुरक्षित रखने के लिए उनके द्धारा बतौर राष्ट्रपति पद संभालना जरुरी
है । </p><p>
</p><p>
उन्होंने कहा कि यदि कोई भी दल जिनमेंं पाकिस्तान मुस्लिम लीग और पाकिस्तान
पीपुल्स पार्टी शामिल है, यदि चुनाव जीत जाती है तो उसे सत्ता का हस्तांतरण कर
दिया जाएगा । </p><p>
</p><p>
हालांकि उन्होंने संकेत दिया कि अपदस्थ प्रधानमंत्री नवाज शरीफ और बेनजीर
भुट्टो को देश दूसरी बार शासन करने का अवसर नहीं देगा । सउदी अरब
में नवाज शरीफ की गतिविधियों के बारे में परवेज ने कहा कि यह देखना सउदी सरकार
का काम है । </p><p>
</p><p>
बेनजीर के पाकिस्तान लौटने की बात पर उन्होंने कहा कि उनके वापस
लौटने पर उन्हें गिरफ्तार कर लिया जाएगा और अदालत द्धारा उन्हें उनके अपराधों
के लिए सजा दी जाएगी । </p>

<p>लामा,
ताईवान के राष्ट्रपति से मिले </p>

<p>ताइपेई ।
धार्मिक तिब्बती नेता दलाई लामा गुरुवार को स्वतंत्रता के समर्थक ताईवान के
राष्ट्रपति चेन-शुई-बायन से मिले । यह महत्वपूर्ण मुलाकात बीजिंग के गुस्से
को जरुर भड़काएगी । </p><p>
</p><p>
दलाई लामा ने आज सुबह ताईवान के नेता से उनके राष्ट्रपति कार्यालय में
मुलाकात की । लामा ने चीन के पारंपरिक दिवस टॉम्बस्वीपिंग के उपलक्ष्य में
ताईवान नेता से मुलाकात की । हालांकि दोनों हस्तियों के बीच क्या बातें
हुईं, फिलहाल इसका खुलासा नहीं किया गया है । </p><p>
</p><p>
लेकिन चीन ने आरोप लगाया है कि तिब्बती नेता के इस दौरे का उद्देश्य ताईवान
में अलगाववादी ताकतों के साथ सहयोग करना और अलगाववादी गतिविधियों के लिए धन
जुटाना है । लामा यहां शनिवार को नौ दिन के धार्मिक दौरे पर आए
हैं । </p><p>
</p><p>
इस बीच, लामा ने चीन के इस आरोप को नकार दिया है कि वे ताईवान की आजादी
की गतिविधियों में शामिल हैं । उन्होंने साफ किया कि मैं आजादी के बजाय
तिब्बत की बृहद स्वायत्ता चाहता हूं । </p>

<p>'भारत
सीटीबीटी पर हस्ताक्षर करें' </p>

<p>स्टॉकहोम ।
यूरोपीय संघ ने भारत से परमाणु परीक्षण संधि (सीटीबीटी) पर हस्ताक्षर कर
उसका अनुसमर्थन करने को कहा है । जबकि भारत का कहना है कि उसका
परमाणु परीक्षण कार्यक्रम अन्य देशों की परमाणु नीतियों की ही तरह है । </p><p>
</p><p>
स्वीडन के विदेश मंत्री अन्ना लिंध ने कहा कि हमंे खेद है कि भारत ने
सीटीबीटी पर हस्ताक्षर करने का फैसला नहीं किया है । लिंध के मुताबिक
सीटीबीटी ही एक मात्र ऐसा मुद्दा है, जिस पर यूरोपीय संघ भारत के साथ
सहमत नहीं है । </p><p>
</p><p>
उन्होंने यह भी कहा कि यूरोपीय संघ चाहता है कि भारत-पाकिस्तान कश्मीर समस्या
को हल करने के लिए पुनः बातचीत शुरु करें । </p><p>
</p><p>
लिंध का कहना है कि भारत द्धारा भविष्य में परमाणु परीक्षण के ऋण स्थगन की घोषणा
करना महत्वपूर्ण है । </p>

<p>क्लिंटन
की पुनर्वास योजना पर बैठक </p>

<p>अहमदाबाद ।
पूर्व अमरीकी राष्ट्रपति बिल क्लिंटन ने गुरुवार को गुजरात के भूंकप
पीड़ित इलाकों में पुनर्वास कार्य की योजना को लेकर राहत संगठनों के
साथ एक बैठक की । </p><p>
</p><p>
उल्लेखनीय है कि इस पुनर्वास कार्य के लिए अमरीका-भारत फाउंडेशन द्धारा पांच
करोड़ डॉलर की राशि जुटाई गई है , इसके अध्यक्ष क्लिंटन
हैं । बुधवार को क्लिंटन ने गुजरात के गंभीर रुप से प्रभावित भूकंपग्रस्त
क्षेत्र भुज, रतलाम और अंजार का दौरा कर मृत लोगों को श्रद्धांजलि अर्पित की ।
अपने इस पहले दिन के दौरे में क्लिंटन जहां भी गए जनसमूह ने उनका तहेदिल से
स्वागत किया । </p><p>
</p><p>
क्लिंटन ने कहा कि फाउंडेशन की निधि को छोटी और बड़ी
योजनाओं के लिए इस्तेमाल किया जाएगा और इस कार्य में जुटी हुई
गैर-सरकारी संस्थाएं और राहत संगठन सहयोग करेंगे । </p><p>
</p><p>
अपने सप्ताह भर के दौरे के दौरान क्लिंटन कोलकाता स्थित मदर टेरेसा
के आश्रम भी जाएंगे । दिल्ली  में वे प्रधानमंत्री अटलबिहारी वाजपेयी के साथ
रात्रिभोज पर बातचीत करेंगे । </p>

<p>अंतिम
लड़ाई की जोरदार तैयारी</p>

<p>मारगोवा ।
भारत और आस्ट्रेलिया के बीच चालू एक दिवसीय श्रंृखला के अंतिम मैच की
जोरदार तैयारी दिखाई दे रही है । दोनों टीमें इस मैच को जीतने के लिए
अभ्यास के अलावा रणनीतिक तैयारी में लगी हुई है । </p>

<p>आस्ट्रेलिया इस मैच
को जीतकर जहां टेस्ट मैच श्रंृखला में हुई अपनी भारी पराजय के गम को
दूर करने के प्रयास में है वहीं भारत इस मैच को जीतकर आस्ट्रेलिया को पूरी
तरह धुल चटाने के मूड में है ।</p>

<p>मारगोवा में होने
वाले पांचवें और आखिरी एक दिवसीय के मैच बेहद रोचक और रोमांचक होने की
संभावना है । हालांकि भारत इस मैदान पर अब तक कोई भी मैच नहीं जीत सका
है ।</p>

<p>भारत वर्ष १९९० और
१९९७ में श्रीलंका के खिलाफ यहां हुए मैच में हारा था जबकि न्यू जीलैंड के
खिलाफ वर्ष १९९४ में खेला गया मैच बारिश के कारण धुल गया था । </p>

<p>तरफ,
आस्ट्रेलिया ने वर्ष १९८९ में यहां श्रीलंका के खिलाफ खेला मैच जीता ।
तथाति, भारतीय टीम के कोच जॉन राइट ने पिछले इतिहास पर ध्यान देने
के बजाय कहा कि भारतीय टीम पूरे उत्साह में है और मुझे विश्वास है कि
टीम यहां अच्छा खेलेगी और इस इतिहास को धो डालेगी । </p>

<p> </p>






</body></text></cesDoc>