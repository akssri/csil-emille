<cesDoc id="hin-w-indiainfo-news-01-03-20" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-indiainfo-news-01-03-20.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Indiainfo" internet news (www.indiainfo.com), news stories collected on 01-03-20</h.title>
<h.author>Indiainfo.com Ltd.</h.author>
<imprint>
<pubPlace>India (Mumbai, Hyderabad, Bangalore)</pubPlace>
<publisher>Indiainfo.com Ltd.</publisher>
<pubDate>01-03-20</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Hindi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>अमिताभ
बॉलीवुड के सबसे लोकप्रिय सितारे </p>

<p>सनफ्रांसिस्को ।
'फोर्ब्स डॉट कॉम' द्वारा जारी पांच सबसे लोकप्रिय भारतीय फिल्म
सितारों की सूची में अमिताभ बच्चन प्रथम आए हैं, जबकि ऋृतिक रोशन, आमिर खान,
ऐश्वर्या राय और माधुरी दीक्षित क्रमश: दूसरे, तीसरे, चौथे और पांचवें स्थान पर
रहे हैं । </p><p>
</p><p>
यह सूची फोर्ब्स द्वारा वेतन की राशि, 'प्रेस क्लिप्स' की संख्या और 'गुगल
डॉट कॉम' द्वारा लोकप्रियता के शोधों के आधार पर तैयार की गई
है । </p><p>
</p><p>
इन सभी वर्गों में अमिताभ आगे रहे हैं । </p>

<p>नदीम
पर भारत में मुकदमा जारी रहेगा </p>

<p>मुंबई ।
इंग्लैंड की अदालतों और दि हाऊस ऑफ लॉर्ड्स द्वारा संगीतकार नदीम
सैफी को दोषमुक्त कर दिए जाने के बावजूद मुंबई पुलिस नदीम पर लगे आरोपों
पर आगे कार्रवाई जारी रखेगी । नदीम पर गुलशन कुमार हत्याकांड में प्रमुख
अभियुक्त की भूमिका निभाने का आरोप है । </p><p>
</p><p>
विशेष अधिवक्ता उज्जवल निकम ने कहा, 'नदीम पर भारत की अदालतों में चल रहे
मामले दि हाऊस ऑफ लॉर्ड्स के फैसले से बेअसर रहेंगे ।' </p>

<p>शेरॉन
की हिंसा समाप्ति की मांग </p>

<p>येरुशलम ।
इजरायली प्रधानमंत्री एरियल शेरॉन ने फिलीस्तीनियों से शांति वार्ता आरंभ करने से
पूर्व यहां जारी हिंसा को समाप्त करने की मांग की है । सोमवार को हुए एक
संघर्ष में हमलावारों नेे एक यहूदी और एक इजरायली सुरक्षा अधिकारी को मार
डाला । </p><p>
</p><p>
शेरॉन इस शांति वार्ता का विचार अमरीकी राष्ट्रपति जॉर्ज डब्ल्यू. बुश के
सामने भी रखने वाले हैं । </p><p>
</p><p>
शेरॉन ने वाशिंगटन से जारी अपने बयान में कहा है, ''अराफात के
फिलिस्तीनी शासक हिंसा और आतंक को बढ़ाने में लगे हुए हैं । उधर,
येरुशलम में हुए संघर्ष को लेकर पड़ो़सियों का आरोप है कि बेथलेहम के
पास यात्रा में प्रतिबंध पर शेरॉन द्वारा दी गई ढील के फैसले के कारण यह हमला
हुआ है । </p>

<p>तालिबान
की 'नौरोज' मनाने पर पाबंदी </p>

<p>काबुल ।
अफगानिस्तान के शासक तालिबान मिलीशिया ने ईरानियों के नए साल को मनाने पर
रोक लगा दी है  । </p><p>
</p><p>
एक अधिकारिक रेडियों ने मंगलवार को कहा कि इस त्योहार को मनाने वाले
पर विधर्मी की छाप लगा दी जाएगी । </p><p>
</p><p>
कार्यवाहक पालक मंत्री मोहम्मद सलीम हक्कानी ने कहा, ''अफगानियों को
पारसी नववर्ष या नौरोज से 'घृणा' करनी चाहिए ।' </p><p>
</p><p>
रेडियो शरियत ने हक्कीक के बयान के हवाले से कहा है कि नौरोज का उत्सव
नास्तिकों से जुड़ा हुआ है, जो हमें उनके समान नास्तिक बनाने से रोकता
है । </p>

<p>डीएमके
व पीटी में सीटों का समझौता </p>

<p>चेन्नई ।
तमिलनाडु में आगामी विधानसभा चुनाव में सीटों के बंटवारे के लिए
डीएमके और पुथिया तमिजागम (पीटी) राजी हो गए हैं । पीटी को
दस सीटें आबंटित की गई हैं, जबकि नवगठित टीएमसी
डेमोक्रेटिक फोरम को दी जाने वाली सीटों के बारे में फैसला बाद
में किया जाएगा । </p><p>
</p><p>
अपने संगठन को भेजे एक संकेत के मुताबिक डीएमके कम से कम १५५
सीटों पर चुनाव लड़ेगी । लिहाजा अब सीटों के बंटवारे का
मुद्दा लंबा नहीं खिंचेगा । </p><p>
</p><p>
हालांकि पार्टी प्रमुख करुणानिधि ने टीएमसी फोरम को दी जाने वाली
सीटों के फैसले में बताने से इनकार कर दिया । जबकि उसे तीन सीटें
दे दी गई थीं । </p><p>
</p><p>
करुणानिधि ने कहा, ''वैसे कोई भी समस्या नहीं है । फैसला एक-दो
दिनों में कर लिया जाएगा ।'' </p>

<p>अमरीका
ने तालिबान की दलील रद्द की </p>

<p>वाशिंगटन ।
अमरीका ने अफगानिस्तान में प्राचीन बौद्ध प्रतिमाओं को ध्वस्त करने की तालिबान की
दलील को नकार दिया है । अमरीका ने इस दलील को 'स्वयं को
संतुष्टि' पहुंचाने वाली और अशुद्ध करार दी है । </p><p>
</p><p>
राज्य विभाग के प्रवक्ता रिचर्ड बुचर ने तालिबान के विशेष दूत सैय्यद रहमतुल्ला के
बयान पर प्रतिक्रिया व्यक्त करते हुए कहा, 'ये सारी दलीलें अशुद्ध और स्वयं को
संतुष्टि प्रदान करने वाले हैं ।' रहमतुल्ला ने अपने बयान में कहा कि जब
यूनेस्को दल ने भूखे अफगानियों को सहायता के बदले निधि देने की पेशपश की,
तब इससे क्रोधित होकर मिलिशिया ने कार्रवाई की । </p><p>
</p><p>
बुचर ने कहा कि पश्चिमी अनुदानकर्ताओं द्वारा अफगान को उसकी स्थिति बेहतर बनाने
के लिए वर्षों से सहायता दी जा रही है । </p><p>
</p><p>
रहमतुल्ला ने सोमवार को अमरीकी राष्ट्रपति जॉर्ज डब्ल्यू. बुश को पेश किए
एक पत्र में दोनों देशों के बीच संबंध बेहतर बनाने और संवाद को जारी रखने का
आग्रह भी किया है । </p>

<p>फिजी
में मंदिर अपवित्र हुआ </p>

<p>सुवा ।
पश्चिमी फिजी में एक मंदिर को कथित रूप से तोड़ने व अपवित्र करने को
लेकर लउटोका के हिंदू समुदाय ने वहां पुलिस सुरक्षा लगवा दी है । </p><p>
</p><p>
गंगम्मन कोविल मंदिर के कार्यवाहक दविनेश रेड्डी ने कहा कि मंदिर
को अपवित्र किए जाने से यहां छोटे समूह में मौजूद इस समुदाय को काफी
धक्का लगा है । </p><p>
</p><p>
उन्होंने बताया कि मंदिर में स्थापित मूर्तियों के गहने और वस्त्र भी लूटेरों ने
लूट लिया है । </p>

<p>तेजपाल
सैनिक अदालत में पेश होंगे </p>

<p>नई
दिल्ली । रक्षा सौदे घोटाले के आरोप की जांच कर रही थलसेना अदालती
जांच ने 'तहलका' डॉट कॉम के प्रबंध निदेशक तरुण तेजपाल को अदालत
के सामने प्रस्तुत होने का आग्रह किया है । </p><p>
</p><p>
थलसेना सूत्रों का कहना है कि तेजपाल ने अदालती जांच के सामने पेश होकर इस
प्रक्रिया में सहायता पहुंचाने की बात कही है । </p><p>
</p><p>
तीन सदस्यीय इस अदालती जांच की अध्यक्षता लेफ्टिनेंट जनरल एस.के. जैन कर रहे
हैं और इस जांच की प्रक्रिया सोमवार से शुरू भी कर दी गई है । इस
घोटाले की बारीकी से जांच के लिए उक्त पोर्टल से संबंधित टेप उपलब्ध
करवाने का आग्रह भी किया गया है । </p><p>
</p><p>
थलसेना कानून के अंतर्गत गठित इस जांच में नागरिकों को 'सम्मन' जारी
करने का अधिकार नहीं है । लेकिन सूत्रों का कहना है कि आग्रह के आधार पर
नागरिकों की गवाही भी ली जा सकेगी । </p>

<p>एनडीए
सरकार को हटाना प्राथमिकता : मुलायम </p>

<p>नई
दिल्ली । लोक मोर्चा अध्यक्ष मुलायम सिंह यादव ने मंगलवार को कहा कि केंद्र में
कांग्रेस के सहयोग से वैकल्पिक सरकार बनाने का मुद्दा वाजपेयी सरकार के
इस्तीफे के बाद ही उठेगा । </p><p>
</p><p>
यादव ने कहा कि मोर्चा के सामने फिलहाल सर्वप्रथम प्राथमिकता एनडीए सरकार
को बेदखल करना है । </p><p>
</p><p>
वैकल्पिक सरकार बनाने के मामले में कांग्रेस से गठजोड़ के मुद्दे पर
यादव ने कहा 'पहले सरकार को जाने दीजिए ।' </p><p>
</p><p>
उन्होंने कहा कि मोर्चा सरकार की नीतियों के बारे में जनता में जनजागृति
फैलाएगी कि ये पूरी तौर पर गलत है । </p><p>
</p><p>
समाजवादी पार्टी के अध्यक्ष ने कहा कि आगामी दिनों में दिल्ली में रक्षा सौदे
घोटाले के खिलाफ मोर्चा नेता प्रदर्शन भी करेंगे । </p>

<p>फिजी
में चुनाव २७ अगस्त को </p>

<p>सुवा ।
फिजी की कार्यवाहक सरकार में देश में २७ अगस्त को चुनाव कराने की घोषणा की
है । फिजी में महेंद्र चौधरी की सरकार के अपदस्थ होने के बाद सेना ने शासन
संभाल लिया । फिलहाल वहां एक कार्यवाहक सरकार गठित कर दी गई है और
महेंद्र चौधरी को बर्खास्त कर दिया गया है । </p><p>
</p><p>
कार्यवाहक सरकार ने कैबिनेट की बैठक के बाद मंगलवार को यह घोषणा
की है । </p>

<p>पीएमओ
से विवादित अफसरों को हटाएं : ठाकरे </p>

<p>मुंबई ।
भारतीय जनता पार्टी की प्रमुख सहयोगी शिवसेना ने प्रधानमंत्री कार्यालय के दो
विवादित अफसरों को तत्काल हटाने की मांग की है । तहलका पर्दाफाश के
बाद कई बार इन अफसरों का नाम उछल चुका है । भाजपा की सहयोगी समता
पार्टी भी पहले इन अफसरों को हटाने की मांग की थी । </p><p>
</p><p>
शिवसेना के मुखपत्र 'सामना' में शिवसेना प्रमुख बाल ठाकरे ने पी.एम.ओ. के दो
अफसरों बृजेश मिश्रा व एन.के. सिंह को तत्काल हटाने की मांग की है ।
उन्होंने कहा कि राजकीय कार्यों में प्रधानमंत्री के दामाद रंजन भट्टाचार्य के दखल
पर भी रोक लगनी चाहिए । </p><p>
</p><p>
'सामना' के मंगलवार के अंक में शिवसेना प्रमुख ने कहा कि यदि बृजेश मिश्र,
एन.के. सिंह व रंजन भट्टाचार्य पर उंगलियां उठ रही हैं तो उन्हें तत्काल
हटाया जाना चाहिए । उन्होंने कहा कि जांच चलने तक तीनों को पीएमओ से
बेदखल कर देना चाहिए । </p><p>
</p><p>
ठाकरे ने कहा कि भट्टाचार्य प्रधानमंत्री के दत्तक दामाद हैं उन्हें पीएमओ से
नहीं निकाला जा सकता किंतु उनको शासकीय कार्यों में दखल देने से रोका जा
सकता है । उन्होंने कहा कि यह हस्तक्षेप वैसा ही जैसे श्रीमती इंदिरा गांधी के
कार्यकाल में पीएमओ में संजय गांधी का प्रभुत्व था । </p>

<p>कोयना
क्षेत्र में भूकंप के झटके </p>

<p>पुणे ।
महाराष्ट्र के कोयना क्षेत्र में सोमवार को भूकंप के हल्के-झटके महसूस
किए गए । इससे पुणे के आसपास के लगभग १४० किलोमीटर का क्षेत्र प्रभावित
हुआ । भूकंप की तीव्रता रिक्टर पैमाने पर ३.७ मापी गई । </p><p>
</p><p>
सोमवार को प्रात:१०.०९ मिनट पर आए भूकंप से किसी जनधन हानि की सूचना नहीं
है । कोयना हायड्रो इलेक्ट्रिक प्रोजेक्ट को भी इससे कोई
नुकसान नहीं हुआ । कोयना डैम भी पूरी तरह सुरक्षित है । </p>

<p>लेखानुदान
पारित, संसद फिर ठप </p>

<p>नई
दिल्ली । लोकसभा ने चालू वित्त वर्ष के लिए ३७ हजार, ६४१ करोड़ रुपए की
अंतिम अनुपूरक मांगों व लेखानुदान को अपनी स्वीकृति दे दी । वित्तमंत्री ने
सदन के पटल पर सोमवार को ही लेखानुदान रखा था किंतु शोर-शराबे के बीच
उसे पास नहीं किया जा सका था । </p><p>
</p><p>
मंगलवार को प्रतिपक्ष से वार्ता कर लोकसभा अध्यक्ष ने लेखानुदान मांगे पारित करवाने
का रास्ता निकाला । लेखानुदान पारित होने के बाद लोकसभा की कार्रवाई
स्थगित कर दी गई । राज्यसभा भी भारी शोरशराबे के चलते स्थगित करनी पड़ी ।
इस प्रकार ६ठें दिन भी दोनों सदनों की कार्रवाई नहीं चल सकी । संसद में
विपक्षी सदस्यों ने 'गली-गली में शोर है एनडीए सरकार चोर है' जैसे नारे
लगाकर अपना आक्रोश प्रकट किया । राज्यसभा में १० मिनट लगातार नारे
बाजी के बाद सभापति कृष्णकांत ने बैठक ४ बजे तक के लिए स्थगित कर
दी । </p>

<p>फेमा
के तहत राकेश रोशन से पूछताछ </p>

<p>मुंबई ।
फिल्म निर्माता-निर्देशक राकेश रोशन फेमा (फारेन एक्सचेंज मैंनेजमेंट एक्ट)
के तहत प्रवर्तन निदेशालय में पेश हुए । </p><p>
</p><p>
आज के सुपर स्टार ऋृतिक रोशन के पिता राकेश रोशन से प्रवर्तन निदेशालय के
अफसरों ने अपने नरीमन प्वाइंट, मित्तल चेंबर स्थित कार्यालय में लगभग ६ घंटे
पूछताछ की । </p><p>
</p><p>
सोमवार को राकेश रोशन लगभग ११.३० बजे दिन में वहां पहुंचे तथा १७.३० बजे उन्हें
छुट्टी मिल सकी । प्रवर्तन निदेशालय के सूत्रों के मुताबिक रोशन ने
पूछताछ के दौरान सहयोग किया । 'फेमा' दो साल पूर्व
'फेरा' के स्थान पर लागू किया गया था । </p><p>
</p><p>
फिल्म स्टार अमरीश पुरी भी पूछताछ के लिए बुलाए गए थे, पर इस मामले
की पूरी जानकारी नहीं मिल सकी । </p>

<p>नितीश
को रेलमंत्रालय का अति. प्रभार </p>

<p>नई
दिल्ली । कृषि मंत्री नितीश कुमार को रेल मंत्रालय का अतिरिक्त प्रभार दिया गया
है । </p><p>
</p><p>
राष्ट्रपति के.आर. नारायणन ने प्रधानमंत्री अटलबिहारी वाजपेयी की सलाह पर
नितीश को रेल मंत्रालय का प्रभार संभालने के निर्देश दिए । रेलमंत्री ममता बनर्जी
के त्यागपत्र के बाद यह पद रिक्त था । </p><p>
</p><p>
नितीश कुमार पहले वाजपेयी की दूसरी बार बनी सरकार में रेलमंत्री रह चुके हैं ।
१९९९ में राजग के सत्ता में आने पर उन्हें कृषि मंत्री बनाया गया । </p>

<p>केरल
फिल्मोत्सव में ४० देशों की फिल्में </p>

<p>तिरुवनंतपुरम् ।
केरल में होने वाले ६ठे अंतरराष्ट्रीय फिल्म उत्सव में ४० देशों की १२५
फिल्में प्रदर्शित की जाएंगी । फिल्मोत्सव ३० मार्च को प्रारंभ हो रहा है । </p><p>
</p><p>
केरल के संस्कृति मंत्री टी.के. रामकृष्णन के मुताबिक ८ दिनों तक चलने
वाले इस आयोजन की एकमात्र समस्या यह है कि इस वर्ष फिल्मोत्सव का बजट १
करोड़ से घटाकर ६५ लाख कर दिया गया है । </p><p>
</p><p>
उन्होंने कहा कि यह फिल्मोत्सव निरंतर अपने स्तर को लेकर आगे बढ़ा
है । अन्य होने वाले फिल्मोत्सवों के बीच इसकी अपनी अलग पहचान बन रही
है । </p><p>
</p><p>
समारोह में सर्वश्रेष्ठ निर्देशक, सर्वश्रेष्ठ निर्माता, सर्वश्रेष्ठ फिल्म, सर्वश्रेष्ठ
लघु फिल्म श्रेणी के पुरस्कार दिए जात हैं । साथ ही अंतरराष्ट्रीय सिनेमा में
खास पहचान वाली एक फिल्म को विशेष सम्मान दिया जाता है । </p>

<p>'मर्यादा
पुरुषोत्तम' में अमिताभ बनेंगे दशरथ ! </p>

<p>मुंबई ।
'द स्वार्ड ऑफ टीपू सुल्तान' तथा 'जय हनुमान' जैसे
ऐतिहासिक/धार्मिक धारावाहिकों से लोकप्रिय हुए संजय खान अब छोट पर्दे से
बड़े पर्दे पर एक महत्वाकांक्षी 'प्रोजेक्ट' के साथ आ रहे हैं । संजय
खान अब 'रामायण' पर एक बड़े बजट की फिल्म बनाना चाहते हैं और अब
उन्हें 'राम' की तलाश है । </p><p>
</p><p>
खान ने अपनी इस महत्वाकांक्षी फिल्म 'मर्यादा पुरुषोत्तम' के नाम की घोषणा तो
कर दी है, पर अभी यह तय नहीं है कि फिल्म में राम की भूमिका कौन निभाएगा ।
बताया जाता है कि खान चाहते हैं कि राम की भूमिका 'आज के सुपर स्टार'
ऋृतिक रोशन निभाएं और दशरथ के रूप में अमिताभ बच्चन पर्दे पर हों । लेकिन
इन चर्चाओं में कितना सच है यह अभी सामने आना है । </p><p>
</p><p>
हालांकि अमिताभ बच्चन ने माना कि खान ने अपनी फिल्म के लिए उनसे दशरथ का चरित्र
निभाने की चर्चा की है लेकिन बात अभी प्रारंभिक चरण में है । वहीं संजय खान के
दामाद ऋृतिक रोशन से इस 'प्रोजेक्ट' पर चर्चा नहीं की गई है । </p><p>
</p><p>
ऋृतिक के पिता राकेश रोशन इस बात को गलत बताते हैं कि उनका बेटा खान
की फिल्म में काम करने वाला है वे कहते हैं- 'ऋृतिक यह फिल्म नहीं कर
रहा, मुझे नहीं पता यह अफवाह कैसे फैली । वह तो वैसे ही अगले २ सालों के
लिए व्यस्त है ।'' </p><p>
</p><p>
रोशन परिवार के एक निकट सूत्र के अनुसार 'यह मुंबई के प्रेस का किया
धरा है जो अमिताभ और ऋृतिक को दशरथ व राम के रूप में देखना चाहता है ।
किंतु ऋृतिक के पास २००२ तक कोई समय नहीं है । और न ही वह परिवार के
लिए अपने अन्य वायदों से मुकर सकता है । वह बहुत प्रोफेशनल है । खान भी
इन चीजों को समझते हैं और बहुत स्वाभिमानी हैं । यदि वे ऋृतिक से फिल्म
करने को कहेंगे तो सुपर स्टार को न कि अपने दामाद को ।' </p><p>
</p><p>
ऐसे में लगता है कि अपने ससुर (संजय खान) की फिल्म में ऋृतिक का काम करना
संभव न हो पाएगा । खान ने इस बीच अभिषेक बच्चन को भी 'राम' के पात्र
निभाने के लिए संपर्क किया है । अभिषेक के मुताबिक 'यह सच है कि
खान ने मुझे राम के रोल के लिए ऑॅफर किया है पर उन्होंने मुझसे नहीं मेरे
पिता जी से बात की थी ।' </p><p>
</p><p>
अमिताभ के मुताबिक 'खान ने मुझसे दशरथ के रोल को लेकर बात की है और
बातचीत चल रही है ।' अभिषेक को पिता के काम करने के मामले पर लोगों
का मानना है कि अमिताभ अपने होम प्रोडक्शन की फिल्म के लिए दोनों की साथ
उपस्थिति चाहते हैं । वे नहीं चाहते कि अन्य फिल्म में दोनों के साथ जाने से
उनके खुद के प्रोजेक्ट पर असर आए । जाहिर है संजय खान को अमिताभ के
रूप में दशरथ तो मिल गए हैं पर राम की तलाश जारी है । </p>

<p>अभिषेक
बनेंगे चंद्रशेखर आजाद </p>

<p>मुंबई ।
बॉलीवुड के सुपर स्टार अमिताभ बच्चन के पुत्र अभिषेक बच्चन जल्द ही चंद्रशेखर
आजाद की भूमिका में नजर आएंगे । अंग्रेजों के खिलाफ लड़ाई लड़ने
वाले आजाद को पर्दे पर जीवंत करने के लिए प्रख्यात फिल्म निर्देशक प्रियदर्शन तैयार
हैं । </p><p>
</p><p>
मूलत: अंग्रेजी में बनने वाली इस फिल्म मे स्वतंत्रता आंदोलन के दौरान की
पृष्ठभूमि और उसमें चंद्रशेखर आजाद की भूमिका को रेखांकित किया
जाएगा । प्रियदर्शन द्वारा निर्देशित इस फिल्म में आजाद की भूमिका अभिषेक बच्चन
निभाएंगे । </p><p>
</p><p>
फिल्म वैसे तो अंतरराष्ट्रीय स्तर पर प्रसारित करने के लिए अंग्रेजी में ही बनाई
जा रही है किंतु स्थानीय दर्शकों के लिए इसे हिंदी में भी डब किया जाएगा ।
अभिषेक इन दिनों इस ऐतिहासिक चरित्र को पर्दे पर साकार करने के लिए चंद्रशेखर
आजाद के बारे में उपलब्ध साहित्य व जानकारियां पढ़ रहे हैं । </p><p>
</p><p>
अभिषेक कहते हैं ''मैं एक ऐतिहासिक चरित्र को निभाने का अवसर पाकर
खुद को बहुत रोमांचित महसूस कर रहा हूं ।'' </p><p>
</p><p>
कुछ वर्ष पहले अभिषेक के पिता अभिताभ को भी एक प्रख्यात स्वतंत्रता संग्राम
सेनानी मंगल पाण्डेय के जीवन पर बनने वाली फिल्म में काम करने का अवसर मिला
था । निर्देशक केतन मेहता की यह फिल्म कुछ कारणवश मूर्त रूप न ले सकी
और 'कारतूस' नामक इस फिल्म का प्रोजेक्ट ठंडे बस्ते में चला
गया । </p><p>
</p><p>
जल्द ही अभिषेक की 'शरारत' फिल्म रिलीज हो रही है । उसे लेकर वे बहुत
आशान्वित हैं । नए निर्देशक गौरव भल्ला की फिल्म 'शरारत' के बारे में
अभिषेक कहते हैं यह अलग सी फिल्म है । वे कहते हैं 'लोग कुछ अलग
किस्म का सिनेमा चाहते हैं । मैं इस जरूरत को महसूस करता हूं ।' </p><p>
</p><p>
देखना है 'शरारत' और चंद्रशेखर आजाद की भूमिका उनके लिए बॉलीवुड में
सफलता के नए आयाम खोलेगी या उनकी पुरानी फिल्मों की तरह उनके संघर्ष को
और लंबा खींचेगी । </p>

<p>भारत-पाक
आपस में खेले -आईसीसी </p>

<p>चेन्नई ।
अंतरराष्ट्रीय क्रिकेट परिषद (आईसीसी) के अध्यक्ष माल्कम ग्रे ने कहा कि
क्रिकेट खेलने वाले देश जैसे भारत या पाकिस्तान की विदेश नीति क्या हो इसका
निर्णय आईसीसी नहीं कर सकती । </p><p>
</p><p>
दिल्ली जाने के रास्ते चेन्नई में रूककर भारत-ऑस्ट्रेलिया के बीच खेले जा रहे
तीसरे टेस्ट मैच को देखते हुए संवाददाताआंे से बातचीत करते हुए ग्रे ने
कहा कि आईसीसी सिर्फ दोनों देशों (भारत-पाकिस्तान) के बीच खेल से होने वाले
फायदे की बात कर सकती है । </p><p>
</p><p>
यह पूछे जाने पर कि जब कोई देश आईसीसी के लम्बित कार्यक्रम का पालन नहीं
करता तब उस देश को क्या दंड दिया जायेगा ? ग्रे ने कहा यदि ऐसा
क्रिेकेट बोर्ड के अधिकारों के दायरे सेे बाहर होता है तब आईसीसी
कुछ भी नहीं कर सकती । </p><p>
</p><p>
उन्होंने कहा यदि बोर्ड आईसीसी के कार्यक्रमों का पालन स्वेच्छा से नही
करता तब आईसीसी उस बोर्ड की सम्बद्धता को खत्म कर दे सकती है । </p><p>
</p><p>
माल्कम ग्रे ने कहा कि वे भारत को आईसीसी चैंपियनशिप जीतता हुआ देखना चाहते
हैं । ग्रे ने कहा कि पिछले माह घोषित की गई टेस्ट चैंपियनशिप
लोगों में क्रिकेट के प्रति गहरी रुचि जगायेगी । </p>

<p>भारत
ने पाक को हरा कप जीता </p>

<p>ढाका ।
प्राइम मिनिस्टर गोल्ड कप हॉकी टूर्नामेंट के तहत खेले गये फाइनल
में भारत ने पाकिस्तान को हरा दिया  । मैच के निर्धारित ७० मिनट के समय
में दोनों ही टीमें ३-३ गोल से बराबर थीं । इससे पहले हाफ टाइम में
मुकाबला २-२ से बराबर था । </p><p>
</p><p>
उपलब्ध अतिरिक्त १५ मिनट के समय में भी दोनों ही टीमें एक-दूसरे के
खिलाफ गोल नहीं दाग पाईं । </p><p>
</p><p>
पेनाल्टी शूट आउट में भारत के कप्तान बलजीत सिंह ढिल्लन और अर्जुन
हलप्पा ने पेनाल्टी के जरिये गोल दाग टीम को २-० से बढ़त
दिलाई । लेेकिन इसके बाद पाकिस्तान ने हैदर हुस्सैन और वसीम अहमद की मदद
से गोल दाग परिणाम २-२ से बराबर कर दिया । </p><p>
</p><p>
लेकिन फिर पाकिस्तान के मुहम्मद नदीम के शॉट के पोस्ट से टकरा कर
लौट जाने के बाद भारत के दलजीत सिंह ने टीम के पांचवें पेनाल्टी
शूट से गोल दाग भारत को जीत दिला दी । </p><p>
</p><p>
भारत की तरफ से २३वें और २५वें मिनट में राधा कृष्णन और ६८वें मिनट में
दिलीप तिर्की ने गोल दागे । </p><p>
</p><p>
पाकिस्तान की तरफ से १७वें और ३०वें मिनट में मुहम्मद सरवार ने और मुहम्मद
नदीम ने ६०वें मिनट गोल दागे । </p><p>
</p><p>
टाई ब्रेकर में भारत की तरफ से बलजीत ंसिंह ढिल्लन, अर्जुन हलप्पा और
दलजीत ंसिंह ने तथा पाकिस्तान की तरफ से हैदर हुसैन और वसीम अहमद ने गोल
दागे । </p><p>
</p><p>
टूर्नामेंट में पेनाल्टी के जरिये जापान को ७-६ से हरा इजीप्ट की
टीम तीसरे स्थान पर रही । दोनों ही टीमों ने निर्धारित समय में ३-३ गोल
दागे थे । </p><p>
</p><p>
भारत ने सेमीफाइनल में इजीप्ट और पाकिस्तान ने जापान को हराया था । </p>

<p>आनंद
ने पीटर लीको को हराया </p>

<p>मोंट
कार्लो । दसवीं अम्बर ब्लाइंडफोल्ड एंड रैपीड चेस टूर्नामेंट
के तीसरे चक्र में विश्व चैंपियन भारत के विश्वनाथन आनंद ने हंगरी के पीटर
लीको को हरा दिया । दूसरी तरफ टोपालोव १२ खिलाड़ियों की स्पर्धा
में में बढ़त पर हैं । उन्होंने रूस के व्लादीमिर क्रामनिक को हराया । </p><p>
</p><p>
आनंद ने सफेद मोहरों से गेम की शुरुआत करते हुए पहली ही चाल में लीको को
अचंभित कर दिया । आनंद हंगरियन वैरियेशन का सहारा लेते हुए लीको पर दबाव
बनाने में सफल हो गये । </p><p>
</p><p>
लीको की रणनीति खराब रही । जरूर के समय भी उनके प्यादे राजा की रक्षा में ही
लगे रह गये । इस स्थिति का आनंद के प्यादों ने पूरा फायदा उठाया ।
३६ वीं चाल में लीको को अपने एक मोहरे से हाथ धोना पड़ा । इसका
फायदा आनंद को मिला और उन्होंने बाजी जीत ली । </p><p>
</p><p>
ब्लाइंड फोल्ड गेम में आनंद ने फ्रेंच रक्षात्मक शैली अपनाई, लेकिन दोनों
ही खिलाड़ी एक दूसरे के मोहरों की बलि लेते गये । बिसात से दोनों की
रानियां हट जाने के बाद आनंद ने अपने राजा को रानी की तरफ लाकर संभावित
सफेद मोहरों के हमले को टाल दिया । </p><p>
</p><p>
लेकिन ३३वीं चाल में दोनों ही खिलाड़ियों को ड्रॉ के लिए राजी होना
पड़ा । </p>

<p>भारत
को ऑस्ट्रेलिया पर ८९ रनों की बढ़त </p>

<p>चेन्नई ।
शृंखला के तीसरे और अंतिम टेस्ट के तीसरे दिन का खेल समाप्त होने के
समय भारत ने ऑस्ट्रेलिया पर ८९ रनों की बढ़त ले ली है । साईंराज
बहुतुले ४ और निलेश कुलकर्णी शून्य पर खेल रहे हैं । </p><p>
</p><p>
इससे पूर्व तीसरे दिन का खेल जब शुरू हुआ तब बिना कोई रन जोड़े भारत ने
दास का पहला विकेट खोया । उन्हें मैक्ग्रा ने ८४ रन पर पगबाधा आउट
किया । फिर तेेंदुलकर और लक्ष्मण ने मिलकर तीसरे विकेट के लिए २६ रनों
की भागीदारी की । लक्ष्मण को मैक्ग्रा ने मार्क वॉॅ के हाथों लपकवाया । ६५
रनों की पारी में लक्ष्मण ने ग्यारह चौके लगाए । इसके बाद तेंदुलकर और
गांगुली ने मिलकर चौथे विकेट के लिए ४७ रन जोड़े । गांगुली को
को मैक्ग्रा ने २२ रन पर गिलक्रिस्ट के हाथों लपकवाया । </p><p>
</p><p>
२११ पर दो विकेट के बाद भारत का स्कोर २८४ पर चार विकेट हो
गया । लेकिन फिर तेेंदुलकर और उपकप्तान राहुल द्रविड़ ने मिलकर सावधानी
के साथ सूझबूझ का परिचय देते हुए पांचवें विकेट के लिए उपयोगी भागीदारी
की । दोनों ने मिलकर १६९ रन जोड़ भारत को ऑस्ट्रेलिया पर
बढ़त दिला दी । </p><p>
</p><p>
आउट होने से पूर्व द्रविड़ ने १४० गेंदों का सामना कर १२ चौकों और एक
छक्के की मदद से ८१ रनों की पारी खेली । उन्हें गिलेस्पी ने विकेट के
पीछे गिलक्रिस्ट के हाथों लपकवाया । </p><p>
</p><p>
जल्दी ही तेेंदुलकर को भी गिलेस्पी ने गिलक्रिस्ट के हाथों लपकवा दिया ।
तेंदुलकर ने २३० गेंदों का सामना कर १२६ रनों की पारी में १५ चौके और दो
छक्के उड़ाये । यह तेंदुलकर के टेस्ट कैरियर का २५वां
टेस्ट शतक है । ४६८ रन पर तेंदुलकर के रूप में छठा विकेट
खोने के बाद भारत ने ४७० पर दिघे, ४७५ पर जहीर खान और ४७७ पर हरभजन सिंह के
विकेट खो दिये । </p><p>
</p><p>
ऑफ स्पिनर कोलिन मिलर ने अपने एक ही ओवर में जहीर खान और हरभजन सिंह को
आउट किया । ऑस्ट्रेलिया की तरफ से अभी तक मैक्ग्रा ने तीन और
गिलेस्पी, मिलर तथा वार्न ने दो-दो विकेट लिये हैं । </p>

<p>भारत
के नौ पर ४८० रन </p>

<p>चेन्नई ।
सचिन तेंदुलकर की शतकीय पारी और दास (८४), लक्ष्मण (६५) तथा द्रविड़ (८१) की
बदौलत भारत ने तीसरे दिन का खेल समाप्त होने पर नौ विकेट पर ४८० रन बना
लिये । साईंराज बहुतुले (४) और निलेश कुलकर्णी (०) खेल रहे हैं । </p><p>
</p><p>
भारत को पहली पारी के आधार पर ऑस्ट्रेलिया पर ८९ रनों की बढ़त मिल
गई है । </p>

<p>५००
विकेट लेने वाले पहले खिलाड़ी वाल्श </p>

<p>पोर्ट
आफ स्पेन । वेस्ट इंडीज के तेज गेंदबाज कोर्टनी वाल्श ने दक्षिण
अफ्रीका के विरुद्ध दूसरे क्रिकेट टेस्ट के तीसरे दिन ५०० वां
विकेट लिया । इस तरह वाल्श दुनिया के पहले ऐसे गेंदबाज बन गए हैं जिन्होंने
अपने क्रिकेट जीवन में ५०० विकेट लिए  हैं । </p><p>
</p><p>
इससे पूर्व वाल्श ने भारतीय गेंदबाज कपिलदेव और न्यूजीलैंड के हेडली के
विकेट लेने के रिकॉर्डस को तोड़ा था । </p><p>
</p><p>
दक्षिण अफ्रीकी खिलाड़ी गेरी क्रिस्टन और कालिस के विकेट
झटककर वाल्श ने अपने ५०० विकेट पूरे किए । </p>

<p> </p>






</body></text></cesDoc>