<cesDoc id="ori-w-science-maths-oma04" lang="ori">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ori-w-science-maths-oma04.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ବୀଜଗଣିତ.</h.title>
<h.author>ଓଡିଶା...</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book.</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">oma04</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0866.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-12</date></creation>
<langUsage>Oriya</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;ଆହୁରି ମଧ୍ଧ ସାନ , ବଡ ସବୁପ୍ରକାର ଆଯତ୍ତକ୍ଷେତ୍ରର କ୍ଷେତ୍ରଫଳ ସ୍ଥିର କରିବା
ପାଇଁ ଏହି ନିଯମଟିକୁ ବ୍ଯବହାର କରିହେବ । ତେଣୁ ନିଯମ - 1 ଭଳି ଏହି
ନିଯମଟି ମଧ୍ଧ ସମସ୍ତ ପ୍ରକାର ଏକକ ପଦ୍ଧତି ଏବଂ l ଓ b ର ଭିନ୍ନ ଭିନ୍ନ ମାପ
ପାଇଁ ସତ୍ଯ । ଅଧିକନ୍ତୁ ନିଯମ - 2 କୁ ନିଯମ - 1 ପରି ଅଧିକ ବ୍ଯାପକ ଅର୍ଥରେ
ପ୍ରଯୋଗ କରାୟାଇପାରିବ ।</p>

<p>ଉପରୋକ୍ତ ଦୁଇଟି ଉଦାହରଣରୁ ସ୍ପଷ୍ଟ ହୁଏ ୟେ ସଂଖ୍ଯାକୁ ବୁଝାଇବା ନିମନ୍ତେ
ଅକ୍ଷର ବ୍ଯବହାର କଲେ ତାହା ଅଧିକ ବ୍ଯାପକ ଭାବରେ ଚିନ୍ତା କରିବାରେ
ସହାଯକ ହୁଏ । ଅନ୍ଯପ୍ରକାରେ କହିବାକୁ ଏହାଦ୍ବାରା ଗାଣିତିକ ସମସ୍ଯାର
ସମାଧାନ କରିବା ଓ ସାଧାରଣ ନିଯମ ଗଠନ କରିବା ସମ୍ଭବ ହୋଇଥାଏ ।
ଏହି ସାଧାରଣ ନିଯମଗୁଡିକୁ ସୂତ୍ର ବୋଲି କୁହାୟାଏ । ସୂତ୍ରଗୁଡିକ ସାହାୟ୍ଯରେ
ହଜାର ହଜାର ଗାଣିତିକ ସମସ୍ଯାର ସମାଧାନ କରାୟାଇପାରେ ।</p>

<p>ସଂଖ୍ଯାକୁ ବୁଝାଇବା ପାଇଁ ଏହି ଅକ୍ଷରଗୁଡିକୁ ଆକ୍ଷରିକ ସଂଖ୍ଯା ବା କେବଳ
ଆକ୍ଷରିକ ବା ବୀଜ ବୋଲି କୁହାୟାଏ । ଏଣିକି ଆମେ x , y , z , p , s ,
a , b , l ଇତ୍ଯାଦି ବୋଲି କେଉଁଠି ବ୍ଯବହାର କଲେ ଆମକୁ ବୁଝିବାକୁ ହେବ
ୟେ ଏମାନେ ଗୋଟିଏ ଗୋଟିଏ ସଂଖ୍ଯା । ଏହି ସଂଖ୍ଯାମାନଙ୍କର ନିର୍ଦ୍ଦିଷ୍ଟ ମୂଲ୍ଯ
ଆମକୁ ଜଣା ନ ଥାଏ ବୋଲି ଏଗୁଡିକୁ ଅଜ୍ଞାତ ସଂଖ୍ଯା ବୋଲି ମଧ୍ଧ କୁହାୟାଏ ।</p>

<p>ସଂଖ୍ଯା ସାଙ୍ଗକୁ ଆକ୍ଷରିକ ବା ବୀଜଗୁଡିକୁ ବ୍ଯବହାର କରି
ୟେଉଁ ଗଣିତ ସୃଷ୍ଟି କରାଗଲା ତାହାକୁ ବୀଜଗଣିତ କୁହାୟାଏ ।</p>

<p>ପୃଥିବୀର ସବୁ ଦେଶର ଲୋକେ ବୀଜଗଣିତରେ ଲାଟିନ୍୍ ଓ ଗ୍ରୀକ୍୍ ଭାଷାର
ଅକ୍ଷରଗୁଡିକୁ ବ୍ଯବହାର କରିଥାନ୍ତି । ତୁମେ ବ୍ଯବହାର କରୁଥିବା ଇଂରାଜୀ
ଅକ୍ଷରଗୁଡିକ ପ୍ରକୃତରେ ଲାଟିନ୍୍ ଅକ୍ଷର ।</p>

<p>ପ୍ରଶ୍ନମାଳା 4 - ( a )</p>

<p>1. ଗୋଟିଏ ବୃତ୍ତର ବ୍ଯାସ , ତା'ର ବ୍ଯାସାର୍ଦ୍ଧର ଦୁଇଗୁଣ । ବ୍ଯାସ ପାଇଁ d
ଓ ବ୍ଯାସାର୍ଦ୍ଧ ପାଇଁ r ନେଇ ସୂତ୍ରଟି ଲେଖ ।</p>

<p>2. ବୀଜ ବ୍ଯବହାର କରି ନିମ୍ନଲିଖିତ ଉକ୍ତିଗୁଡିକୁ ପ୍ରକାଶ କର । କେଉଁଥିପାଇଁ
କେଉଁ ବୀଜ ବ୍ଯବହାର କଲ ଲେଖ ।
( a ) ସମବାହୁ ତ୍ରିଭୁଜର ପରିସୀମା ତା'ର ପ୍ରତ୍ଯେକ ବାହୁର ତିନିଗୁଣ ।
( b ) ତୁମ ଶ୍ରେଣୀର ପିଲାସଂଖ୍ଯା , ପ୍ରତ୍ଯେକ ଧାଡିରେ ବସିଥିବା ପିଲାସଂଖ୍ଯା
ଓ ଧାଡି ସଂଖ୍ଯାର ଗୁଣଫଳ ସଙ୍ଗେ ସମାନ ।
( c ) ଗୋଟିଏ ଆଯତ୍ତଘନାକାର କୋଠରୀର ଘନଫଳ ତା'ର ଦୈର୍ଘ୍ଯ ,
ପ୍ରସ୍ଥ ଓ ଉଚ୍ଚତାର ଗୁଣଫଳ ସହିତ ସମାନ ।</p>

<p>4.2 ବୀଜଗଣିତର ଇତିହାସ -</p>

<p>ବ୍ରହ୍ମଗୁପ୍ତ ( ଜନ୍ମ - 598 ଖ୍ରୀ: ଅ: ) ଙ୍କ ଲିଖିତ " ବ୍ରହ୍ମଗୁପ୍ତ ସିଦ୍ଧାନ୍ତ " ନାମକ
ପୁସ୍ତକକୁ ପୃଥିବୀର ପ୍ରଥମ ବୀଜଗଣିତ ପୁସ୍ତକ କୁହାୟାଇପାରେ । ଏହି
ଶାସ୍ତ୍ରରେ ଅଜ୍ଞାତ ସଂଖ୍ଯାମାନଙ୍କୁ ନେଇ ଓ ଶୂନକୁ ବିନିୟୋଗ କରି ତଥ୍ଯମାନ
ଉପସ୍ଥାପନା କରାୟାଇଛି । ଏହି ପୁସ୍ତକଟି ଉଜ୍ଜଯିନୀର ` କାଙ୍କ ' ନାମକ ଏକ ବ୍ଯକ୍ତି
770 ମସିହାରେ ବାଗ୍୍ଦାଦ୍୍ର ଖଲିଫା ଅଲ୍୍ମନସୁର ( Almansoor 712 -
775 ) ଙ୍କ ରାଜଦରବାରରେ ପେଶ୍୍ କରିଥିଲେ । ଏହି ପୁସ୍ତକଟି ପ୍ରଥମେ ଆରବୀଯ
ଭାଷାରେ ସିନ୍ଦ୍୍ ହିନ୍ଦ୍୍ ( Sind Hind ) ନାମକ ପୁସ୍ତକରେ ପ୍ରକାଶ କରାଗଲା
ଏବଂ ଏହି ପୁସ୍ତକ ଜରିଆରେ ଭାରତୀଯ ଶୂନ , ଦଶମିକ ସ୍ଥାନାଙ୍କ ପଦ୍ଧତି ଓ
ବୀଜଗଣିତ ପ୍ରଥମେ ଆରବ ଓ ପରେ ୟୁରୋପରେ ପ୍ରସାର ଲାଭ କଲା ।</p>

<p>ବ୍ରହ୍ମଗୁପ୍ତଙ୍କ ପୂର୍ବରୁ ମଧ୍ଧ ପ୍ରାଚୀନ ଭାରତୀଯ ଗାଣିତିକମାନେ ଅଜ୍ଞାତ ସଂଖ୍ଯାକୁ
ଚିହ୍ନାଇବା ପାଇଁ ସଙ୍କେତମାନଙ୍କର ବହୁଳ ବ୍ଯବହାର କରିଅଛନ୍ତି । ସେମାନେ
ଅଜ୍ଞାତ ରାଶିକୁ ` ୟାବତ୍୍ ତାବତ୍୍ ' , ` ବର୍ଣ୍ଣ ' , ` ବୀଜ ' ଆଦି ଦ୍ବାରା ଚିହ୍ନିତ କରିଥିଲେ । //
ତୁମେ ଦେଖ ୟେ ( 1 ) ଠାରୁ ( 10 ) ପର୍ୟ୍ଯନ୍ତ ଗାଣିତିକ ଉକ୍ତିଗୁଡିକରେ
` = ' ଚିହ୍ନ ବ୍ଯବହୃତ ହୋଇଛି । ୟେଉଁ ଗାଣିତିକ ଉକ୍ତିରେ ` = ' ଚିହ୍ନ ବ୍ଯବହୃତ
ହୋଇଥାଏ ତାହାକୁ ଏକ ସମାନତା ଉକ୍ତି କିମ୍ବା କେବଳ ସମାନତା ବୋଲି
କୁହାୟାଏ । ତେଣୁ ( 1 ) ରୁ ( 10 ) ୟାଏଁ ପ୍ରତ୍ଯେକ ଉକ୍ତି ଗୋଟିଏ ଗୋଟିଏ
ସମାନତା ଅଟନ୍ତି ।</p>

<p>ଲକ୍ଷ୍ଯକଲେ ଦେଖିବ ୟେ ( 1 ) ଓ ( 2 ) ସମାନତା ଦ୍ବଯରେ କୌଣସି ବୀଜ
ବ୍ଯବହୃତ ହୋଇନାହିଁ । ମାତ୍ର ( 3 ) ରୁ ( 10 ) ୟାଏଁ ସମାନତାଗୁଡିକରେ ଏକ
ବା ଏକାଧିକ ବୀଜ ବ୍ଯବହୃତ ହୋଇଛି । ୟେଉଁ ସମାନତା ଉକ୍ତିରେ ଏକ
ବା ଏକାଧିକ ବୀଜ ବ୍ଯବହୃତ ହୋଇଥାଏଁ ତାହାକୁ ଗୋଟିଏ ସମୀକରଣ
କୁହାୟାଏ । ତେଣୁ ( 3 ) ରୁ ( 10 ) ୟାଏଁ ସମସ୍ତ ସମାନତା ଉକ୍ତି ଗୋଟିଏ
ଗୋଟିଏ ସମୀକରଣ ଅଟନ୍ତି ।</p>

<p>ଲକ୍ଷ୍ଯ କର ୟେ ପ୍ରତ୍ଯେକ ସମୀକରଣର ଦୁଇଟି ପାର୍ଶ୍ବ ଅଛି , ବାମ ପାର୍ଶ୍ବ
( ବା. ପା. ) ଏବଂ ଦକ୍ଷିଣ ପାର୍ଶ୍ବ ( ଦ. ପା. ) । ` = ' ଚିହ୍ନର ବାମରେ ଥିବା
ଅଂଶକୁ ବାମ ପାର୍ଶ୍ବ ଓ ତା'ର ଡାହାଣରେ ଥିବା ଅଂଶକୁ ଦକ୍ଷିଣ ପାର୍ଶ୍ବ କୁହାୟାଏ ।
ସମୀକରଣ ( 3 ) ରେ a( b+c ) ହେଉଛି ବା. ପା. ଏବଂ ab+ac ହେଉଛି
ଦ. ପା. । ତୁମେ ଆହୁରି ଦେଖୁଛ ୟେ ସମୀକରଣ ( 3 ) ଟି a , b , c ର
ୟେକୌଣସି ମୂଲ୍ଯ ପାଇଁ ସତ ଅଟେ ।</p>

<p>(4) ରୁ (10) ପର୍ୟ୍ଯନ୍ତ ସମୀକରଣଗୁଡିକ କିନ୍ତୁ ବୀଜମାନଙ୍କର ୟେକୌଣସି
ମୂଲ୍ଯ ପାଇଁ ଠିକ୍୍ ନୁହନ୍ତି । ଏ ସମୀକରଣରେ ଥିବା ବୀଜଗୁଡିକୁ ଅଜ୍ଞାତ ରାଶି
ବୋଲି କୁହାୟାଏ । ଅଜ୍ଞାତ ରାଶିଗୁଡିକୁ ସାଧାରଣତଃ ଲାଟିନ୍୍ ବର୍ଣ୍ଣମାଳାର
ଶେଷଆଡର ଅକ୍ଷରଗୁଡିକ ଦ୍ବାରା ପ୍ରକାଶ କରାୟାଏ , ୟଥା :- x , y , z , u , v, r</p>

<p>ତୁମେ ଦେଖ ୟେ ସମୀକରଣ (8) ଓ (10) ରେ ଦୁଇଟି ଅଜ୍ଞାତ ରାଶି
ଅଛି ମାତ୍ର ସମୀକରଣ (4) , (5) ଆଦିରେ ଗୋଟିଏ ଲେଖାଁଏ ଅଜ୍ଞାତ ରାଶି
ରହିଛି । ସମୀକରଣ (4) , (5) , (6) , (8) ଓ (10) ରେ ଅଜ୍ଞାତ ରାଶିର ସର୍ବୋଚ୍ଚ
ଘାତ 1 ଅଟେ । ୟେଉଁ ସମୀକରଣରେ ଅଜ୍ଞାତ ରାଶିର ସର୍ବୋଚ୍ଚ ଘାତ 1 ଅଟେ
ତାହାକୁ ଏକ ରୈଖିକ ସମୀକରଣ ବା ଏକ ଘାତ ସମୀକରଣ କୁହାୟାଏ । ତେଣୁ
ସମୀକରଣ (4) , (5) , (6) , (8) ଓ (10) ଗୋଟିଏ ଗୋଟିଏ ରୈଖିକ
ସମୀକରଣ ବା ଏକଘାତ ସମୀକରଣ ।</p>

<p>ଏହି ଅଧ୍ଧାଯଟିରେ କେବଳ ଏକ ଅଜ୍ଞାତ ରାଶିବିଶିଷ୍ଟ ରୈଖିକ ସମୀକରଣ କଥା
ଆଲୋଚନା କରାୟାଇଛି ।</p>

<p>ତୁମ ଦୈନନ୍ଦିନ ଜୀବନରେ ଅନେକ ଘଟଣା ଦେଖାୟାଏ ୟାହାକୁ କି ଗୋଟିଏ
ରୈଖିକ ସମୀକରଣ ରୂପରେ ପ୍ରକାଶ କରାୟାଇପାରେ । ନିମ୍ନ ଉଦାହରଣଟି
ଦେଖ ।</p>

<p>ଉଦାହରଣ - ଗୋଟିଏ ଧାନଭଙ୍ଗା କଳ ଘଣ୍ଟାଏ ଚାଲିବାକୁ 1/4 ଲିଟର ତେଲ
ଦରକାର କରେ । ଦିନେ କଳଟି x ଘଣ୍ଟା କାମ କରିବାରୁ 3 ଲିଟର ତେଲ
ଖର୍ଚ୍ଚ ହୋଇଗଲା । 1 ଘଣ୍ଟା ଚାଲିବା ପାଇଁ ଦରକାର ତେଲର ପରିମାଣ = 1/4 ଲିଟର
x ଘଣ୍ଟା ଚାଲିବାକୁ ଦରକାର ତେଲର ପରିମାଣ = 1/4 ଲିଟର ଗୁଣନ x
= 1/4 x ଲିଟର
ମାତ୍ର ଜଣା ଅଛି ୟେ ସେଦିନ 3 ଲିଟର ତେଲ ଖର୍ଚ୍ଚ ହୋଇଗଲା । //
5.3 ସମୀକରଣର ସହଜ ସମାଧାନ ପ୍ରଣାଳୀ -</p>

<p>ପୂର୍ବ ସମାଧାନ ପ୍ରଣାଳୀରୁ ଜଣାୟାଏ ଏହା ଅଧିକ ସମଯସାପେକ୍ଷ
ସମୀକରଣର ମୂଳ ବଡ ହୋଇଥିଲେ ଏହି ପ୍ରଣାଳୀରେ ସମାଧାନ ଅଧିକ
କଷ୍ଟସାଧ୍ଧ । ତେଣୁ ସମାଧାନର ଏକ ସହଜ ପ୍ରଣାଳୀ କିପରି ବାହାର କରିହେବ
ତାହା ଏଠାରେ ଆଲୋଚନା କରିବା ।</p>

<p>ସମୀକରଣ ଏକ ସାଧାରଣ ନିକିତି ସହ ତୁଳନୀଯ । ଏହାର ଦୁଇ ପାର୍ଶ୍ବ
ନିକିତର ଦୁଇ ପଲା ସଦୃଶ । ସମାନ (=) ଚିହ୍ନ ବାମ ପଲାର ବଟକରା ଓ
ଦକ୍ଷିଣ ପଲାର ଜିନିଷ ଓଜନର ସମାନତାକୁ ବୁଝାଏ ।</p>

<p>ବାମ ପଲାର ବଟକରା ଓ ଦକ୍ଷିଣ ପଲାର ଜିନିଷ ସମାନ ଓଜନର
ହୋଇଥିଲେ ନିକିତି ଦଣ୍ଡଟି ସମତୁଲ ଅବସ୍ଥାରେ ଭୂମି ସହ ସମାନ୍ତର ଭାବେ
ରହେ । ବାମ ପଲାରେ ଅଧିକ ବଟକରା ପକାଇଲେ ଦକ୍ଷିଣ ପଲାରେ
ସମାନ ଓଜନର ଜିନିଷ ନେବାକୁ ହୁଏ । ତେବେୟାଇ ଦଣ୍ଡଟି ସମତୁଲ
ଅବସ୍ଥାକୁ ଆସେ । ସେହିପରି ସମାନ ଓଜନର ବଟକରା ଓ ଜିନିଷ ବାହାର
କରିନେଲେ ସମତୁଲ ଅବସ୍ଥା ଅପରିବର୍ତ୍ତିତ ରହେ । ଏହା ମଧ୍ଧ ସମୀକରଣ
କ୍ଷେତ୍ରରେ ଘଟିଥାଏ ।</p>

<p>ଏହି ନିଯମଗୁଡିକୁ ସମୀକରଣ ସମାଧାନ ଦିଗରେ ବିଶେଷ ସହାଯକ ।
ଏଗୁଡିକୁ ପ୍ରଯୋଗ କରି କିପରି ସହଜରେ ସମାଧାନ କରାୟାଏ , ପରପୃଷ୍ଠାର
ଉଦାହରଣଗୁଡିକୁ ଲକ୍ଷ୍ଯକଲେ ଭଲଭାବେ ବୁଝିପାରିବ ।</p>

<p>5.4 ସମୀକରଣ ସମାଧାନର ପ୍ରଯୋଗ -
ଆମ ଦୈନନ୍ଦିନ ଜୀବନରେ ଅନେକ ପ୍ରଶ୍ନର ସମାଧାନ ବୀଜଗଣିତ ପ୍ରଯୋଗ
କରି କରାୟାଇପାରିବ । ନିମ୍ନ ଉଦାହରଣଟିକୁ ଦେଖ ।</p>

<p>ଉଦାହରଣ -
ତୁମ ସାଙ୍ଗ ପାଖରେ ୟେତେ ଟଙ୍କା ଅଛି , ତା'ର ଦୁଇ ଗୁଣରେ
5 ଟଙ୍କା ୟୋଗ କଲେ 25 ଟଙ୍କା ହୁଏ । ତେବେ ତୁମ ସାଙ୍ଗ ପାଖରେ
କେତେ ଟଙ୍କା ଅଛି ?</p>

<p>ଏହି ପ୍ରଶ୍ନଟି ଗୋଟିଏ ଗାଣିତିକ ସମସ୍ଯା । ସମସ୍ଯାଟିର ସମାଧାନ କରି
ପ୍ରଶ୍ନଟିର ଉତ୍ତର ବାହାର କରିବାକୁ ହେବ । ଏହା କିପରି କରାୟିବ ସେ କଥା
ଆଗ ଟିକେ ଆଲୋଚନା କରିବା ।</p>

<p>ପ୍ରଶ୍ନଟିକୁ ଭଲକରି ପଢ । ଏବେ ଠିକ୍୍ କର ପ୍ରଶ୍ନଟିରେ କ'ଣ ବାହାର
କରିବାକୁ ହେବ । ତୁମ ସାଙ୍ଗ ପାଖରେ କେତେ ଟଙ୍କା ଅଛି , ଏକଥା ବାହାର
କରାୟିବ । ତୁମ ସାଙ୍ଗ ପାଖରେ ଥିବା ଟଙ୍କା ବିଷଯରେ କିଛି ନ ଜାଣିଲେ
ସେ ଟଙ୍କାର ପରିମାଣ କେତେ ବାହାର କରାୟାଇ ପାରିବ ନାହିଁ । ତେଣୁ
ପ୍ରଶ୍ନଟିରେ ନିଶ୍ଚିତ ଭାବରେ ତୁମ ସାଙ୍ଗ ପାଖରେ ଥିବା ଟଙ୍କା ଦୁଇ ଗୁଣରେ
5 ଟଙ୍କା ୟୋଗ କରାୟାଇଛି । ଫଳରେ ସବୁ ମିଶି 25 ଟଙ୍କା ହୋଇଛି । '</p>

<p>ତୁମ ସାଙ୍ଗ ପାଖରେ ଥିବା ଟଙ୍କାର ପରିମାଣ ଜଣା ନାହିଁ । ତାହାର
2 ଗୁଣରେ 5 ଟଙ୍କା କିପରି ମିଶାଇବା ? ଜଣା ନ ଥିବା ଟଙ୍କାର ପରିମାଣ
ଏକ ଅଜ୍ଞାତ ରାଶି । ଅଜ୍ଞାତ ରାଶିକୁ ତୁମେ କିପରି ଲେଖିବ ? ` x ' ଭାବରେ
ଲେଖିବ । ତା'ର ଦୁଇଗୁଣରେ ପୁଣି ପାଞ୍ଚ ଟଙ୍କା ମିଶାଇବ । ହେବ -
2x+5 ଟଙ୍କା । ତୁମେ ଜାଣ ୟେ ଏସବୁ ଟଙ୍କା ମିଶିଲେ 25 ଟଙ୍କା ହେବ । //
ଏହି ଉଦାହରଣରୁ ଆମକୁ ନିମ୍ନ କେତେକ ସୂଚନା ମିଳୁଛି । ସମାଧାନ
କଲାବେଳେ ସେଗୁଡିକର ପ୍ରଯୋଗ ଏକାନ୍ତ ଆବଶ୍ଯକ ।</p>

<p>ମନେରଖ
(a) ୟାହା ନିର୍ଣ୍ଣଯ କରାୟିବ ତାହାକୁ ଅଜ୍ଞାତ ରାଶି ଦ୍ବାରା ସୂଚିତ
କରାୟାଏ ।
(b) ଅଜ୍ଞାତ ରାଶି ପାଇଁ x , y , z ଆଦି ୟେକୌଣସି ଏକ ଅକ୍ଷର
ନିଆୟାଏ ।
(c)ଅଜ୍ଞାତ ରାଶି ସହିତ ସଂପୃକ୍ତ ଉକ୍ତିକୁ ବୀଜଗାଣିତିକ ରାଶି ରୂପରେ
ପ୍ରକାଶ କରାୟାଏ ।
(d) ସେହି ବୀଜଗାଣିତିକ ରାଶି ସହିତ ସମାନତା ଦର୍ଶାଉଥିବା ସଂଖ୍ଯା
ବା ରାଶିକୁ ନେଇ ସମୀକରଣ ଗଠନ କରାୟାଏ ।
(e) ଏହାପରେ ଅଜ୍ଞାତ ରାଶି ପାଇଁ ସମାଧାନ କଲେ ଆବଶ୍ଯକ ଉତ୍ତର
ମିଳେ ।
(f) ସମୀକରଣ ଉଭଯ ପକ୍ଷରେ ଅଜ୍ଞାତ ରାଶିର ମାନ ବସାଇ
ଉତ୍ତର ଠିକ୍୍ କି ନା ପରୀକ୍ଷା କରାୟାଏ । ସମୀକରଣଟି ସେହି
ମାନ ଦ୍ବାରା ସିଦ୍ଧ ହେଲେ ଉତ୍ତରଟି ଠିକ୍୍ ବୋଲି ଜଣାୟାଏ ।
ଏହି ସୂଚନାଗୁଡିକର ପ୍ରଯୋଗ କିପରି କରାୟାଏ ଭଲ ଭାବରେ ବୁଝିବା ପାଇଁ
ତଳେ ଦିଆୟାଇଥିବା ଉଦାହରଣଗୁଡିକୁ ଲକ୍ଷ୍ଯ କର ।	</p>

<p>ପ୍ରଶ୍ନମାଳା 5 (d)</p>

<p>1. ଦୁଇଟି ସଂଖ୍ଯାର ସମଷ୍ଟି 27 , ଗୋଟିଏ ଅନ୍ଯଠାରୁ 3 ବେଶୀ ହେଲେ
ସଂଖ୍ଯା ଦୁଇଟି ସ୍ଥିର କର ।</p>

<p>2. କେଉଁ ସଂଖ୍ଯାକୁ 8 ଦ୍ବାରା ଗୁଣିଲେ ଗୁଣଫଳ 40 ହେବ ?</p>

<p>3. ଗୋଟିଏ ସଂଖ୍ଯାକୁ 12 ଦ୍ବାରା ଭାଗକଲେ ଭାଗଫଳ 4 ହୁଏ । ତେବେ
ସଂଖ୍ଯାଟି କେତେ ?</p>

<p>4. ତୁମ ଶ୍ରେଣୀରେ ୟେତେଜଣ ବାଳିକା ପଢନ୍ତି ତା'ର 3 ଗୁଣ ବାଳକ
ପଢନ୍ତି । ବାଳକ ଓ ବାଳିକା ସଂଖ୍ଯା ମିଶି 44 ହେଲେ ପ୍ରତ୍ଯେକଙ୍କ ସଂଖ୍ଯା
କେତେ ?</p>

<p>5. କେଉଁ ସଂଖ୍ଯାର ଏକ ତୃତୀଯାଂଶ ତା'ର ଏକ ପଞ୍ଚମାଂଶଠାରୁ 4 ବେଶୀ ?</p>

<p>6. 20 ବର୍ଷ ପରେ ତୁମର ବଯସ ୟେତେ ହେବ ତାହା ତୁମର ବର୍ତ୍ତମାନ
ବଯସର 3 ଗୁଣ । ତେବେ ତୁମର ବର୍ତ୍ତମାନ ବଯସ କେତେ ?</p>

<p>7. ବାବୁର ଟଙ୍କା ବେବି ଟଙ୍କାର 3 ଗୁଣରୁ 15 ଟଙ୍କା କମ୍୍ । ବାବୁ
ପାଖରେ 45 ଟଙ୍କା ଥିଲେ ବେବି ପାଖରେ କେତେ ଟଙ୍କା ଅଛି ?</p>

<p>8. ଗୋଟିଏ ପରୀକ୍ଷାରେ ମୀନା ଇଂରାଜୀରେ ପାଇଥିବା ନମ୍ବର ତା'ର ଗଣିତ
ନମ୍ବରର ଅଧାରୁ 15 ନମ୍ବର ବେଶି । ୟଦି ଇଂରାଜୀରେ ସେ 55 ନମ୍ବର
ପାଇଥାଏ ତେବେ ତା'ର ଗଣିତ ନମ୍ବର କେତେ ?</p>

<p>9. ଖଣ୍ଡିଏ ଗଣିତ ବହିର ଦାମ୍୍ 6 ଟଙ୍କା । ଜଣେ ବହି ବିକ୍ରେତା କେତେ
ଖଣ୍ଡ ଗଣିତ ବହି କିଣିଲା ପରେ ତାଙ୍କ ପାଖରେ 8 ଟଙ୍କା ବଳିଲା । ୟଦି
ବହି କିଣିବା ପୂର୍ବରୁ ତାଙ୍କ ପାଖରେ 158 ଟଙ୍କା ଥାଏ , ତେବେ ସେ
କେତେ ଖଣ୍ଡ ଗଣିତ ବହି କିଣି ଥିଲେ ?</p>

<p>10. ଗୋଟିଏ ସଂଖ୍ଯା ଅନ୍ଯ ଏକ ସଂଖ୍ଯାର ଛଅ ଗୁଣ । ସେହି ସଂଖ୍ଯା ଦୁଇଟିର
ଅନ୍ତର ଫଳ 15 ହେଲେ ସଂଖ୍ଯା ଦୁଇଟି ନିର୍ଣ୍ଣଯ କର ।</p>

<p>11. ଦୁଇଟି କ୍ରମିକ ସଂଖ୍ଯାର ୟୋଗଫଳ 45 ହେଲେ ସଂଖ୍ଯାଦ୍ବଯ ସ୍ଥିର କର ।</p>

<p>12. ଗୋଟିଏ ପରିବାରରେ ମାସକୁ ୟେତିକି ଗହମ ଖର୍ଚ୍ଚ ହୁଏ ତା'ର ତିନିଗୁଣ
ଚାଉଳ ଖର୍ଚ୍ଚ ହୁଏ । ଗୋଟିଏ ମାସରେ ଉଭଯ ଚାଉଳ ଓ ଗହମ ଖର୍ଚ୍ଚର
ପରିମାଣ 60 କିଲୋଗ୍ରାମ ହେଲେ କେଉଁଥିରୁ କେତେ ଖର୍ଚ୍ଚ ହୋଇଥିଲା ?
 +&gt;*
               0 
</p></body></text></cesDoc>