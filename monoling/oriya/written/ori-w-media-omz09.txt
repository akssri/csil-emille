<cesDoc id="ori-w-media-omz09" lang="ori">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ori-w-media-omz09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ବାଣୀଚ୍ଚି</h.title>
<h.author>ପରୀକ୍ଷିତ</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1981</pubDate>
</imprint>
<idno type="CIIL code">omz09</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0600.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-12</date></creation>
<langUsage>Oriya</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;ଆପଣ ୟାହା ପଚାରି ଥିଲେ
ବୈଜ୍ଞାନିକ କେଦାର ଚରଣ ରାଉତ , ବରେହିଁପୁର , ଚାନ୍ଦବାଲି
ଜିଲ୍ଲା - ବାଲେଶ୍ବର ।
ପ୍ରଶ୍ନ - ସ୍ବାମୀ ସ୍ତ୍ରୀର ସବୁ ଦେଖିପାରେ କିନ୍ତୁ କଣ ଦେଖିପାରେ ନାହିଁ ?
ଉତ୍ତର - ବୈଧବ୍ଯ ।
ଶ୍ରୀ ବୀର କିଶୋର ମହାନ୍ତି , 4 ର୍ଥ ବାର୍ଷିକ କଳାଛାତ୍ର ,
ସରକାରୀ ସାଂଧ୍ଧ ମହାବିଦ୍ଯାଳଯ , ବାରିପଦା ।
ପ୍ରଶ୍ନ - ଶେଷ ଶ୍ରାବଣର ସାଫଲ୍ଯତା ପରେ ପ୍ରଶାନ୍ତ ନନ୍ଦ ଆଉ ଚିତ୍ର ନିର୍ମାଣ
କରୁଛନ୍ତି କି ?
ଉତ୍ତର - ବର୍ତ୍ତମାନ ସେ ବମ୍ବେରେ ଏକ ହିନ୍ଦି ଚିତ୍ରର ସହ ନିର୍ଦ୍ଦେଶନା
ଦେବାରେ ବ୍ଯସ୍ତ ।
ପ୍ରଶ୍ନ - ପ୍ରଶାନ୍ତ ନନ୍ଦ ଓ ସୁରେଶ ମିଶ୍ରଙ୍କ ୟୋଗ୍ଯତା ଓ ଠିକଣା
ଜଣାଇବେ କି ?
ଉତ୍ତର - ଶ୍ରୀ ପ୍ରଶାନ୍ତ ନନ୍ଦ ଏମ.ଏ. ଏଲ.ଏଲ.ବି. , କେଶରପୁର , କଟକ - 1
ଶ୍ରୀ ସୁରେଶ ମିଶ୍ର , M.sc. ଅଧ୍ଧାପକ , ଷ୍ଟୁଆର୍ଟ କଲେଜ , କଟକ - 1
ପ୍ରଶ୍ନ - ଓଡିଆ ସିନେମା ଜଗତରେ କୃତିତ୍ବ ଅର୍ଜ୍ଜନ କରିଥିବା ଅଭିନେତା
ମାନଙ୍କ ମଧ୍ଧରେ ଶ୍ରୀ ରାମ ପଣ୍ଡା ଜଣେ । ଏହା ୟଥାର୍ଥ କି ?
ଉତ୍ତର - ଆପଣଙ୍କ ମତାମତ ଭୁଲ୍୍ ନୁହେଁ ।
ଶ୍ରୀ ପ୍ରଶାନ୍ତ କୁମାର ପଣ୍ଡା , ଇସ୍ପାତ ମାର୍କେଟ , ରାଉରକେଲା ।
ପ୍ରଶ୍ନ - ସାଗର ବେଳାରେ ଉଲଗ୍ନ ନରନାରୀ କେଉଁଠାରୁ ଉଷ୍ମତା ପାଇଥାନ୍ତି
ଉତ୍ତର - ଭଭଯଙ୍କ ଦେହର ଉଷ୍ମତାରୁ ।
ପ୍ରଶ୍ନ - ଗାଯକ ମୂକେଶ ଶେଷଥର କେଉଁ ଫିଲ୍ମ ପାଇଁ ଗାଇଥିଲେ
ଜଣାଇବେ କି ?
ଉତ୍ତର - ସତ୍ଯମ୍୍ , ଶିବମ୍୍ , ସୁନ୍ଦରମ୍୍ ରେ ଲତାମଙ୍ଗେସ କରଙ୍କ ସହ ୟୁଗ୍ମ
ସଂଗୀତର ରିହରସଲ ପରେ ପରେ ବିଦେଶରେ ଆକସ୍ମିକ ମୃତ୍ଯୁ ବରଣ
କରିଥିଲେ ।
ଶ୍ରୀ ସରୋଜ କୁମାର ନନ୍ଦ , ଇଉନିଟ - 9 , ଭୁବନେଶ୍ବର
ପ୍ରଶ୍ନ - ହିନ୍ଦୀ ଚିତ୍ର ଅଭିନେତ୍ରୀ ହେମାମାଳିନୀଙ୍କର ପ୍ରଥମ ଚଳଚ୍ଚିତ୍ର କଣ
ଓ ତାଙ୍କର ଠିକଣା ଜଣାଇବେ କି ?
ଉତ୍ତର - ସପନ କା ସୌଦାଗର
ଠିକଣା - ରୁମ୍୍ ନଂ , 58 ଭିଲ୍୍ ପାର୍ଲେ ବମ୍ବେ - 400056
ଶ୍ରୀ ସୀତାଶଂ କୁମାର ରଣା ( ଟୁନା ) ଦେଉଳ ସାହି , ବିରପଦା ।
ପ୍ରଶ୍ନ - ମୁଁ ୟେଉଁ ଝିଅକୁ ଭଲପାଏ ସେ ବି ମୋତେ ଭଲ ପାଏ । କିନ୍ତୁ
ଆଉ ଉଭଯଙ୍କ ବିବାହ ନିମନ୍ତେ ଝିଅର ବାପା ରାଜି ନୁହନ୍ତି ।
ଏହାର ପ୍ରତିକାର କଣ କରାୟାଇ ପାରେ ?
ଉତ୍ତର - ରେଜିଷ୍ଟ୍ରୀ ମ୍ଯାରେଜ
ପ୍ରଶ୍ନ - ମଣିଷ ପ୍ରେମିକାକୁ ବିବାହ କଲେ ତାର ଜୀବନ ଦୁଃଖମଯ ହୁଏ
ବୋଲି ଲୋକେ କହନ୍ତି , ଏହା କଣ ସତ୍ଯ ?
ଉତ୍ତର - ଏହା ହତାଶ ପ୍ରେମିକ ମାନଙ୍କର ଏ ନିନ୍ଦା ମାତ୍ର । ସ୍ବାମୀ ସ୍ତ୍ରୀଙ୍କର
ଜୀବନ ଦୁଃଖମଯ ହୋଇଥାଏ କେବଳ ପରସ୍ପର ଭୁଲ୍୍ ବୁଝାବଣାକୁ ନେଇ
ମହମ୍ମଦ ଆଖତର ଆଲମ୍୍ ଇଉନିଟ୍୍ - 9 , ଫ୍ଳାଟ , ଶହୀଗ ନଗର
ଭୂବନେଶ୍ବର - 7
ପ୍ରଶ୍ନ - ମୁଁ ଚିତ୍ର ତାରକାଙ୍କ ସହ ପତ୍ରାଳାପ ରଖି ପାରିବି କି ?
ଉତ୍ତର - କରି ପାରନ୍ତି ।
ପ୍ରଶ୍ନ - ହେମାମାଳିନିଙ୍କ ପ୍ରଥମ ଚିତ୍ର କଣ ଏବଂ ସେଥିରେ ମୁଖ୍ଯନାଯକ
କିଏ ଥିଲେ ।
ଉତ୍ତର - ସପନ କା ସୌଦାଗର । ମୁଖ୍ଯ ନାଯକ ଥିଲେ ରାଜକାପୁର
ଶ୍ରୀ ଶୁକ ଦେବ ରାଉତ , କଳ୍ପନା ଛକ , ଭୁବନେଶ୍ବର
ପ୍ରଶ୍ନ - ବେଶ୍ଯା ଅର୍ଥଲୋଭୀ ନା କାମ ଲୋଭୀ ।
ଉତ୍ତର - ପରିସ୍ଥିତି ଲୋଭୀ ।
ପ୍ରଶ୍ନ - ଚୁମ୍ବନ କଣ ଅଶ୍ଳୀଳ ?
ଉତ୍ତର - ନା , ବରଂ ମଧୁର ।
ଶ୍ରୀ ଗଯାଧର ବଳ , ମୁନୁ ଡ୍ରିଙ୍କସ୍୍ , ଭୁବନ୍ଶ୍ବର - 6
ପ୍ରଶ୍ନ - ବିବାହ ପୂର୍ବରୁ ବାସ୍ତବ ପ୍ରେମ ଅଭିଜ୍ଞତାର କୌଣସି ଆବଶ୍ଯକତା
ଅଛି କି ?
ଉତ୍ତର - ଅଭିଜ୍ଞତା ଥିଲେ ସାର୍ଥକତା ଅଛି ।
ପ୍ରଶ୍ନ - ନିଜକୁ ଚରିତ୍ରବାନ ନ ରଖି ଏକ ସୁଚରିତ୍ର ସ୍ତ୍ରୀ କାମନା କରିବା
କେତେ ଦୂର ଫଳପ୍ରଦ ?
ଉତ୍ତର - କୋର କୋ ସାରେ ନଜର ଆତେ ହେଁ ଚୋର ।
ଏଥର ଆପଣ ବିଚାର କରନ୍ତୁ ।
ଶ୍ରୀ ପ୍ରଣବ କୁମାର ଦାସ , ଚରମ୍ପା ହାଇସ୍କୁଲ , ଜିଲ୍ଲା ବାଲେଶ୍ବର
ପ୍ରଶ୍ନ - ମୁଁ ଗୋଟିଏ ଝିଅକୁ ଭଲ ପାଉଛି କିନ୍ତୁ ତାର ସାନ ଭଉଣୀ
ବାଧା ଦେଉଛି , କହିପାରିବେ କି ଏହାର ପ୍ରତିକାର କଣ ?
ଉତ୍ତର - ପ୍ରଥମେ ସାନ ଭଉଣୀକୁ ଭଲ ପାଇବା ଉଚିତ୍୍ ଥିଲା ।
ପ୍ରଶ୍ନ - ଶ୍ରୀ ରାମ ପଣ୍ଡା ଓ ମହାଶ୍ବେତାଙ୍କ ଶିକ୍ଷାଗତ ୟୋଗ୍ଯତା ଜଣାଇବେକି ?
ଉତ୍ତର - ଶ୍ରୀ ରାମ ପଣ୍ଡା , ବି.ଏ ( ଅଭିନଯ ପ୍ରବୀଣ )
ମହାଶ୍ବେତା ଆଇ.ଏସ.ସି.
ଶ୍ରୀ ସୁରେନ୍ଦ୍ର କୁମାର ପାଣିଗ୍ରାହୀ , ବାଲିଗୁଡା , ଫୁଲବାଣୀ
ପ୍ରଶ୍ନ - ଗଛ ଦିଏ ଫଳ , ଆକାଶ ଦିଏ ଜଳ , ସୂର୍ୟ୍ଯ ଦିଏ ଆଲୋକ କିନ୍ତୁ
ଆଜିର ପ୍ରେମିକ କଣ ଦିଏ ?
ଉତ୍ତର - ହଳା ହଳ ।
ଶ୍ରୀ ସୁକାନ୍ତ କୁମାର ଦାସ ସହସଂପାଦକ , ଲକ୍ଷୀନାରାଯଣ ୟୁବକ ସଂଘ
ଜୋଦାଳା , ପାହାଳ , ପୁରୀ ।
ପ୍ରଶ୍ନ - ମୁଁ ସବୁ ଝିଅ ଓ ପୁଅଙ୍କ କଥା ଭାବେ କିନ୍ତୁ ସେମାନେ ମୋ କଥା
ଭାବନ୍ତି କି ନାହିଁ କେମିତି ଜାଣିବି ?
ଉତ୍ତର - ଆପଣ ସେମାନଙ୍କ କଥା ଭାବୁଛନ୍ତି ବୋଲି ସେମାନେ ପ୍ରଥମେ ଜାଣନ୍ତୁ
ପ୍ରଶ୍ନ - ଅକ୍ଷଯ ମହାନ୍ତି ଓ ପ୍ରଫୁଲ୍ଲ କରଙ୍କ ଠିକଣା କଣ ?
ଉତ୍ତର - ଶ୍ରୀ ଅକ୍ଷଯ ମହାନ୍ତି ମାନସିଂ ପାଟଣା , କଟକ
ଶ୍ରୀ ପ୍ରଫୁଲ୍ଲ କର , ବକ୍୍ସି ବଜାର , କଟକ - 1
ପ୍ରଶ୍ନ - ଶେଷ ଶ୍ରାବଣ ପରେ ପ୍ରଶାନ୍ତ ଓ ମହାଶ୍ବେତା କେଉଁ ଚିତ୍ରରେ ଏକାଠି
ଅଭିନଯ କରୁଛନ୍ତି ?
ଉତ୍ତର - ଏ ପର୍ୟ୍ଯନ୍ତ ଜଣାନାହିଁ ।
ପ୍ରଶ୍ନ - ମୋ ସହିତ ବହୁ ଝିଅ କଥାବାର୍ତ୍ତା କରିବାକୁ ଖବର ଦେଲେ ବି
ମୁଁ ଲଜ୍ଜ୍ଯାରେ ୟାଇ ପାରେନି ଏହାର କାରଣ କଣ ?
ଉତ୍ତର - ସେମାନଙ୍କ ପ୍ରତି ଆପଣଙ୍କର ଭଯ ଥିବା ଦୃଷ୍ଟିରୁ ।
ଶ୍ରୀ ଲବ ନାଯକ , ଶଙ୍ଖସାହି ପୋଷ୍ଟ - ଭଞ୍ଜନଗର , ଜିଲ୍ଲା - ଗଞ୍ଜାମ
ପ୍ରଥମ - ଗଞ୍ଜାମ ଜିଲାର କେଉଁ କେଉଁ ଅଞ୍ଚଳରେ କେଉଁ କେଉଁ ଚିଳଚ୍ଚିତ୍ର
ସୁଟିଂ କରାହେଉଛି ଓ କରାହେବ ?
ଉତ୍ତର - ତାଜା ସମ୍ବାଦ ଅନୁୟାଯୀ ଏବେ ବ୍ରହ୍ମପୁର ସହରରେ ଓଡିଆ ଚିତ୍ର
` ରଜନୀଗନ୍ଧା ' ସୁଟିଂ ଚାଲିଛି , ଅନ୍ଯାନ୍ଯ ଚିତ୍ରର ସୁଟିଂ ପାଇଁ ସ୍ଥାନ
ଜଣା ପଡିନାହିଁ ।
ପ୍ରଶ୍ନ - ଗପ ହେଲେବି ସତ ଚିତ୍ର ପାଇଁ ମୁଖ୍ଯ ଅଭିନେତା ଓ ଅଭିନେତ୍ରୀ
କେତେ ଟଙ୍କା ଦାବୀ କରିଥିଲେ ?
ଉତ୍ତର - ସେ କଥା ସଂପୂର୍ଣ୍ଣ ନିଜସ୍ବ । ୟଦି ଜରୁରୀ ଦରକାର ତେବେ ତାଙ୍କୁ
ପଚାରନ୍ତୁ ।
ଶ୍ରୀ ଭାସ୍କର ବେହେରା , 2 ଯ ବର୍ଷ ବାଣିଜ୍ଯ ଛାତ୍ର , ଭଦ୍ରକ କଲେଜ
ପ୍ରଶ୍ନ - ମୁଁ ୟେଉଁ ଝିଅକୁ ଭଲ ପାଉଛି ସେ ମୋତେ ଭଲ ପାଏ କି ନାହିଁ
ଜାଣେନା । ସେ ମୋତେ ଦେଖିଲା ମାତ୍ରେ ମୁହଁପୋତି ଚାଲି ୟାଉଛି
ସେ ମୋତେ ଭଲ ପାଏ କି ନା କେମିତି ଜାଣିବି ?
ଉତ୍ତର - ଆପଣଙ୍କ ମନ କଥା ତାକୁ ଥରେ ଜଣାନ୍ତୁ ନା ।
ପ୍ରଶ୍ନ - ମୁଁ ୟେଉଁ ଝିଅକୁ ଭଲ ପାଉଥିଲି ତାକୁ ପ୍ରେମିକା ସମ୍ବୋଧନ
କରୁଥିଲି । ଏବେ ସେ ଅନ୍ଯ ଜଣଙ୍କୁ ବିବାହ କରିୟାଇଥିବା ଦୃଷ୍ଟିରୁ
ତାକୁ ମୁଁ କି ସମ୍ବୋଧନ କରିବି ?
ପ୍ରଶ୍ନ - ସଂପର୍କୀଯା ଉଭଣୀ ।
ଶ୍ରୀ ନନ୍ଦ କିଶୋର ସାହୁ , ଭଦ୍ରକ , ଜିଲ୍ଲା ବାଲେଶ୍ବର ।
ପ୍ରଶ୍ନ - ବର୍ତ୍ତମାନ ଧର୍ମେନ୍ଦ୍ର ଓ ହେମାମାଳିନୀଙ୍କ ସପର୍କ କିପରି ଅଛି ?
ଉତ୍ତର - ପୂର୍ବ ପରି ନାହିଁ ।
ପ୍ରଶ୍ନ - ବର୍ତ୍ତମାନ ଚିତ୍ର ଜଗତରେ କେଉଁ ଅଭିନେତ୍ରୀ ପ୍ରଥମ ସ୍ଥାନ ଅଧିକାର
କରିଛନ୍ତି ?
ଉତ୍ତର - କେଉଁ ଚିତ୍ର ? ହିନ୍ଦୀ ଓଡିଆ ନା ଅନ୍ଯ କିଛି ...
ପ୍ରଶ୍ନ - ମଣିଷ ପାପ କରି ସୁଦ୍ଧା ମୁକ୍ତି ପାଇଁ କାହିଁକି ଭାବନ୍ତି ନାହିଁ ?
ଉତ୍ତର - ପାପରୁ ମିଳୁଥିବା ଲାଭକୁ ତ୍ଯାଗ କରିପାରୁ ନଥିବା ଦୃଷ୍ଟିରୁ ।
ପ୍ରଶ୍ନ - ମୁଁ ବାଣିଚିତ୍ର ପଢିବାକୁ ଚାହେଁ , କେମିତି ପାଇ ପାରିବି ?
ଉତ୍ତର - ଆପଣଙ୍କ ସହରରେ ପତ୍ରିକା ଦେକାନରେ ପାଇ ପାରିବେ ଅଥବା
ଡାକ ୟୋଗେ ଉପୟୁକ୍ତ ମୂଲ୍ଯ ଓ ଡାକଖର୍ଚ୍ଚ ପଠାଇଲେ ପାଇ
ପାରିବେ ।
ଶ୍ରୀ ଅନିରୁଦ୍ଧ ଗିରି , ଘର ନଂ - j2A/7 ସାଉଥ ବଲଣ୍ଡା , ତାଲଚେର
ପ୍ରଶ୍ନ - ` ସମଯ ' ର ` ବତୀଘର ' କୁ କେନ୍ଦ୍ର କରି ମୋ ଜୀବନ ନାଟକର ୟୌବନ
ପର୍ବରେ ` ୟାୟାବର ' ପରି ନିରାଶ ହୋଇ ୟେତେବେଳେ ଏ ମନମନ୍ଦିରକୁ
` ଶେଷ ଶ୍ରାବଣ ' ର ପ୍ରଖର ସୁଅରେ ` ଆହୁତି ' ଦେବାକୁ ୟାଉଥିଲି
ଠିକ୍୍ ` ଶୀତରାତି ' ରେ ` ଅଦିନ ମେଘ ' ର ` ସୂର୍ୟ୍ଯମୁଖୀ ' ପରି ସେ ଫେରିଆସି
ମୋ ` ନାଗଫାଶ ' ରେ ` ବନ୍ଧନ ' ହୋଇଥିବା ` ସୁନା ସଂସାର ' କୁ
` ପୁନର୍ମିଳନ ' କରିବ କହିଲା ଅଥଚ ଦୀର୍ଘ ଦୁଇମାସ ପୂର୍ବରୁ ଜଣକ ଲାଗି
ମସ୍ତକର ` ସିନ୍ଦୁରବିନ୍ଦୁ ' ପରିଧାନ କରି ସୁଦ୍ଧା ସେ ଆଜି ମୁକ୍ତ କଣ୍ଠରେ
କହିପାରିଲା ମୋର ହେବ । କିନ୍ତୁ ଏହା ତାର ` ନୂଆବୋଉ ' ପରି
` ଅଭିମାନ ' ନୁହେଁ ତ ? ୟଦି ଏହି ତାଙ୍କର ମନର କଥା ହୋଇଥାଏ
ତାହେଲେ ଆପଣ କୁହନ୍ତି ମୋର ଏ ` ପିପାସା ' କୁଳିତ ପ୍ରାଣରେ ଏ
ସମାଜର ପ୍ରକାଣ୍ଡ ନୀତିନିଯମକୁ ଭାଙ୍ଗି ତାଙ୍କୁହିଁ କଣ ପ୍ରିଯତମା ପରି
ଆଦରି ନେବି ?
ଉତ୍ତର - ସମାଜର ` ବାଟ ଅବାଟ ' ଆପଣ ନିଶ୍ଟଯ ଚିହ୍ନିଛନ୍ତି । ତେଣୁ ସେ
ଅନ୍ଯ ଜଣଙ୍କର ସ୍ତ୍ରୀ ହୋଇଥିବା ଦୃଷ୍ଟିରୁ ଆପଣ ଆଉ ` ଅମଡାବାଟ '
ମାଡିବା ଠିକ୍୍ ହେବନାହିଁ । ହୁଏତ ପରେ ` ଅନୁତାପ ' କରି ପାରନ୍ତି ।
ପ୍ରଶ୍ନ - ମୁଁ ଜଣେ ମଧ୍ଧବିତ୍ତ ପରିବାରର ସନ୍ତାନ । ଜଣେ ଧନୀ କନ୍ଯାକୁ
ବହିଟିଏ ସାହାୟ୍ଯ ଦେବାକୁ ୟାଇ ପରେ ପରେ ଉଭଯେ ପରସ୍ପରକୁ
ନିବିଡ ଭାବେ ଭଲ ପାଇ ବସିଲୁ । ଏବେ ତା ବାପା ଏକ ନୂଆ ଫିଅଟ୍୍
କାର କିଣିଛନ୍ତି । କିନ୍ତୁ ଦୁର୍ଭାଗ୍ଯର କଥା ୟେ ତା ଉଦ୍ଦେଶ୍ଯରେ ଗନ୍ତବ୍ଯ
ସ୍ଥଳକୁ ୟାଇ ତାକୁ ନ ପାଇ ଫେରୁଛି ଆପଣ କହନ୍ତୁତ , ପ୍ରେମ ଅପେକ୍ଷା
କଣ ତାଙ୍କ ଫିଆଟ୍୍ ଖଣ୍ଡିକ ବଡ ? ଅନ୍ଯ କ୍ଷେତ୍ରରେ ମୁଁ କଣ
ତା ପରି ଧନୀର କନ୍ଯାକୁ ଭଲ ପାଇ ଭୁଲ୍୍ କରିଛି ?
ଉତ୍ତର - ପ୍ରେମ ଅପେକ୍ଷା ୟଦିଓ ଫିଆଟ୍୍ ବଡ ନୁହେଁ , ତେଣୁ ଅନେକ ଦିନ
ଧରି ଆପଣଙ୍କୁ ଭଲ ପାଇ ଆସିଥିଲେ ଏବେ ଫିଆଟ୍୍ ଟିଏ ଆସିଥିବାରୁ
ତାହା ପ୍ରତି ଭଲ ପାଇ ବସିଛନ୍ତି ଆପଣ ଧୈର୍ୟ୍ଯ ଧରନ୍ତୁ । ପୁଅ ହୁଏତ
ଆପଣଙ୍କୁ ଭଲ ପାଇ ପାରନ୍ତି । ଭଲ ପାଇଲା ବେଳେ କେହି ଭୁଲ୍୍
କିମ୍ବା ଠିକ୍୍ ବିଚାରି ପାରନ୍ତି ନାହିଁ ।
ଶ୍ରୀ ବିଶ୍ବମ୍ଭର ନାଏକ , ରୁଗୁଡିପଡା ବଲାଙ୍ଗୀର
ପ୍ରଶ୍ନ - ମୁଁ ୟଦି ଟଙ୍କା ପଠାଏ ତାହେଲେ ଆପଣ ମୁକ୍ତି ସଂଖ୍ଯାଟି
ପଠାଇବେ କି ?
ଉତ୍ତର - ପୁରୁଣା କପି ବିକ୍ରୀ ପାଇଁ ଆମ ପାଖରେ ଗଚ୍ଛିତ ରୁହେନା ।
ଶ୍ରୀ ଅଭିମନ୍ଯୁ ଦାସ , ଇଟାମାଟି , ନଯାଗଡ ଜିଲ୍ଲା ପୁରୀ ।
ପ୍ରଶ୍ନ - ଆପଣ କଣ ସବୁ ଚଳଚ୍ଚିତ୍ର ଦେଖିଛନ୍ତି ଓ ଦେଖୁଛନ୍ତି ?
ଉତ୍ତର - ବିଶେଷତଃ ଓଡିଆ ଚିତ୍ରକୁ ଉତ୍ସାହ ଦେବା ନିମନ୍ତେ କେବଳ ଓଡିଆ
ଚଳଚ୍ଚିତ୍ର ସବୁ ଦେଖିଥାଏ ।
ପ୍ରଶ୍ନ - ଆପଣଙ୍କୁ ୟେ କୌଣସି ପ୍ରଶ୍ନ ପଚାରିଲେ ଆପଣ ତାହାର ୟଥାର୍ଥ
ଉତ୍ତର ଦିଅନ୍ତି ?
ଉତ୍ତର - କେବଳ ଅବାନ୍ତର ପ୍ରଶ୍ନ ବିନା ଅନ୍ଯ ସମସ୍ତ ପ୍ରଶ୍ନ ସନ୍ତୋଷ ଜନକ
ଉତ୍ତର ଦେଇ ପାଠକ ମାନଙ୍କୁ ସନ୍ତୁଷ୍ଟ କରିବା ତ ଆମର କର୍ତ୍ତବ୍ଯ ।
ପ୍ରଶ୍ନ - ଆପଣଙ୍କର ବାଣୀଚିତ୍ରର ଏଜେଣ୍ଟ ନଯାଗଡ ଅଞ୍ଚଳରେ କେହି
ଅଛନ୍ତି କି ?
ଉତ୍ତର - ସିଧା ସଳଖ ଆମର କେହି ଏଜେଣ୍ଟ ନଥିଲେ ମଧ୍ଧ ଆପଣ
ଖୋଜିଲେ ପାଇ ପାରିବେ ।
ଶ୍ରୀ ଗୋବିନ୍ଦ ଚନ୍ଦ୍ର ସାହୁ , ବଡ ବିରୁଆଁ , କଟକ
ପ୍ରଶ୍ନ - ସ୍ତ୍ରୀର ପ୍ରେମ ଓ ମାତାଙ୍କର ସ୍ନେହ ଏ ଦୁଇଟି ମଧ୍ଧରେ କେଉଁଟି ବଡ ?
ଉତ୍ତର - ମାତାଙ୍କର ସ୍ନେହ ।
ପ୍ରଶ୍ନ - ଦୁଇଜଣଙ୍କ ଘନିଷ୍ଠ ବଂଧୁତା ମଧ୍ଧରେ ୟଦି ଜଣେ ଅନ୍ଯ ଜଣଙ୍କ ଠାରୁ
ଦୂରେଇ ୟାଏ ତେବେ ଅପର ବ୍ଯକ୍ତିର ଏପରିସ୍ଥିତିରେ କର୍ତ୍ତବ୍ଯ କଣ ?
ଉତ୍ତର - ନୀରବରେ ବଂଧୁଙ୍କର ଗତିବିଧି ଲକ୍ଷ୍ଯ କରିବା ।
ପ୍ରଶ୍ନ - ଜନ୍ମ ମୃତ୍ଯୁ ଏ ଦୁଇ ମହାଶକ୍ତିରୁପ ୟେଉଁ ତୃତୀଯ ମହାଶକ୍ତିର ସୃଷ୍ଟି
ହୁଏ ତାହା କଣ କହି ପାରିବେକି ?
ଉତ୍ତର - ପ୍ରଳଯ ।
ଶ୍ରୀ ରବୀନ୍ଦ୍ର ନାଥ ଦାସ , ବାରୋ , ତିହିଡି , ବାଲେଶ୍ବର
ପ୍ରଶ୍ନ - ଅବହେଳା ଓ ଅବସାଦ ଦୂର ପାଇଁ ସାହିତ୍ଯରେ କି ଔଷଧ
ଅଛି କହିବେ କି ?
ଉତ୍ତର - ବିବେକର ଚାବୁକ୍୍ ଓ ଭବିଷ୍ଯତର ବିଧା ଚାପୁଡା ଭଯ ଆପଣଙ୍କ
ସ୍ବତନ୍ତ୍ର ଫାର୍ମାସୀରୁ ପର୍ୟ୍ଯାପ୍ତ ଭାବେ ମିଳିବ , ତେବେ ଆପଣ ମହାପୁରୁଷ
ମାନଙ୍କ ଜୀବନୀ ପଢି ଦେଖନ୍ତୁ ।
ପ୍ରଶ୍ନ - କୌଣସି ଭାଷା , କୌଣସି ଜାତି ବା କୌଣସି ଧର୍ମକୁ ସର୍ବ ପ୍ରାଚୀନ
ଓ ସୁଦୂର ପ୍ରସାରି ବୋଲି କିପରି ଜାଣିହେବ ?
ଉତ୍ତର - ୟଦି ଆପଣ ଆଦି ପୁରୁଷ ମନୁ ଆଦାମ ଓ ଆଦିମାତା
ମାନବୀଙ୍କୁ କୌଣସି ଭାଷାର ବା ଜାତିର ଅଥବା ଧର୍ମର ବୋଲି
ପ୍ରମାଣିତ କରି ପାରନ୍ତି ।
ଶ୍ରୀ ଅଶୋକ କୁମାର ସାହୁ ଗୋପାଳବନ୍ଧା , ଶୁକ୍ଳେଶ୍ବର ଚାନ୍ଦବାଲି , ବାଲେଶ୍ବର
ପ୍ରଶ୍ନ - ମୁଁ ଜାଣିବାରେ ଜଣେ ୟୁବକ ଗୋଟିଏ ଝିଅକୁ ଭଉଣୀ ଭଳି ଭଲ
ପାଏ ଏବଂ ଝିଅଟି ଭାଇ ଭଳି କରେ କିନ୍ତୁ ଲୋକମାନେ ତାଙ୍କର
ସଂପର୍କକୁ ଖରାପ ଦୃଷ୍ଟିରେ ଦେଖି ବଦନାମ କରନ୍ତି କାହିଁକି ?
ଉତ୍ତର - ଲୋକମାନେ ସେମାନଙ୍କର ସଂପର୍କକୁ ଭଲ ଦୃଷ୍ଟିରେ ସହ୍ଯ
କରିପାରନ୍ତି ନାହିଁ ବୋଲି ।
ପ୍ରଶ୍ନ - ଆଜି କାଲି ଶିକ୍ଷିତ ୟୁବକ ମାନେ ଚାଷ କରିବାକୁ କୁଣ୍ଠିତ କାହିଁକି ?
ଉତ୍ତର - କାଳେ ତାଙ୍କୁ କେହି ଅଶିକ୍ଷିତ କହିଦେବ ବୋଲି ।
ପ୍ରଶ୍ନ - ପଲ୍ଲୀ ଅଞ୍ଚଳ ପିଲା ଶିକ୍ଷା , ଖେଳ ପ୍ରଭୃତି କ୍ଷେତ୍ରରେ ଏତେ ଅନୁନ୍ନତ
କାହିଁକି ? ଏହା କିପରି ଦୂର ହୋଇ ପାରିବ ।
ଉତ୍ତର - ସୁବିଧା ଓ ସୁୟୋଗର ଅଭାବ ଦୃଷ୍ଟିରୁ । ଆପଣଙ୍କ ଭଳି ସମୂହ
ଚେଷ୍ଟା ଫଳରେ ଏହା ଦୂର ହୋଇ ପାରିବ ।
ପ୍ରଶ୍ନ - ଚରିତ୍ରହୀନା ଝିଅଙ୍କୁ ବିବାହ କଲେ କ୍ଷତି କଣ ? ୟଦି ଗୋଟିଏ ପୁଅ
କୌଣସି ଝିଅକୁ ଜବରଦସ୍ତ ଭଲ ପାଏ ତେବେ କଣ ଝିଅର ଚଳଚ୍ଚିତ୍ରରେ
କୌଣସି କଳଙ୍କ ଲାଗେ ?
ଉତ୍ତର - କ୍ଷତି ନାହିଁ । କିନ୍ତୁ ଭଲ ପାଇବାରେ ଜବରଦସ୍ତିର ପ୍ରଶ୍ନ ଉଠୁଛି
କେଉଁଠି ? କଳଙ୍କ ତ ଦୂରର କଥା ।
ଶ୍ରୀ ପ୍ରଶାନ୍ତ କୁମାର ଜେନା ସାମନ୍ତ , ଗ୍ରାମ - ମଝି ଗାଁ , ପଦ୍ମପୁର , ବ୍ରହ୍ମପୁର
ପ୍ରଶ୍ନ - ମୋର ଦୁଇଟି ପ୍ରଶ୍ନର ଉତ୍ତର ଦଯାକରି ଦେବେ ବୋଲି ଆଶାକରେ
ଉତ୍ତର - ଆପଣଙ୍କ ଚିଠି ସହ କୌଣସି ପ୍ରଶ୍ନ ଆସିନାହିଁ ପ୍ରଶ୍ନ ପଠାଇଲେ
ନିଶ୍ଟଯ ଉତ୍ତର ପାଇ ପାରିବେ । //
ଶିକାର - ଭଗବତୀ ଚରଣ ପାଣିଗ୍ରାହୀ
ସେ ଅଞ୍ଚଳରେ ଘିନୁଆର ନାମ ବିଖ୍ଯାତ - ଶିକାରୀ ହିସାବରେ ।
ସେ ବନ୍ଧୁକ ମାରି ଜାଣେ ନାହିଁ । ତାର ପ୍ରଧାନ ଅସ୍ତ୍ର ନିଜର ହାତ ତିଆରି
ଘନୁଶର । ସେ ତୀର ମାରିଲାବେଳେ ଚିତ୍୍ ହୋଇପଡେ , ବାମ ପାଦଟି
ଧନୁରେ ଲଗାଇ କାନ ପର୍ୟ୍ଯନ୍ତ ତୀରଟି ଟାଣି ନେଇ ଛାଡିଦିଏ । ଦେଢ କିଲୋ~
ମିଟର ଦୂରରୁ ତୀର ମାରି ସେ ଲାଖ ବିନ୍ଧିପାରେ । ଏଇ ଧନୁଶର ସାହାୟ୍ଯରେ
ସେ ହରିଣ , ସମ୍ବର , ବାରହା , ଭାଲୁ ମାରିଛି ଅଗଣନ , ଚିତାବାଘ ବି ମାରିଛି
ଅନେକ । କିନ୍ତୁ ମହାବଳ ମାରିଛି ମୋଟେ ଦୁଇଟି । ମହାବଳ ଦୁଇଟି ମାରି
ସେ ଡେପୁଟି କମିଶନରଙ୍କ ଠାରୁ ପୁରସ୍କାର ପାଇଛି ।</p>

<p>ସେଦିନ ଭୋରୁ ସେ ଏକ ଅଦ୍ଭୁତ ଶିକାର ନେଇ ଡେପୁଟି କମିଶନରଙ୍କ
ବଙ୍ଗଳା ପାଖରେ ହାଜର । ଗୋଟିଏ କାନ୍ଦରେ ତାର ଧନୁଟି ଝୁଲିଛି , ହାତରେ
ଦି ତିନିଟା ତୀର । ଆର କାନ୍ଧରେ ରହିଛି ଟାଙ୍ଗୀ । ଘିନୁଆକୁ ଏ ବେଶରେ
ଦେଖି ଅର୍ଦ୍ଦଳି ପଚିରିଲା -</p>

<p>" କିରେ ଆଜି କି ଶିକାର ଆଣିଛୁ ? " ଘିନୁଆ ସଙ୍ଗେ ତାର ଭଲ
ଜଣାଶୁଣା । କେତେଥର ସେ ତାର ବକ୍୍ସିସ୍୍ର ଅଂଶୀଦାର ହୋଇଛି ।
ଉତ୍ତରରେ ଘିନୁଆ ତାର ମଇଳା ଦାନ୍ତ ଦୁଇ ଧାଡି କେବଳ ଦେଖାଇଲା । ସେ
ହସିଲା କି ଖେଁକିଲା ଜାଣିବାର ଉପାଯ ନାହିଁ । ପ୍ରକୃତରେ ହସ ବୋଲି ୟାହାକୁ
କୁହାୟାଏ , ଘିନୁଆଠାରେ କେହି କେବେ ଦେଖିନାହିଁ । ବେଳେ ବେଳେ ସେ
ଏହିପରି ଦାନ୍ତ ଦେଖାଏ , ତାହାର ହସ ନୁହେଁ - କେବଳ ଦାନ୍ତ ଦେଖା ।</p>

<p>ଅର୍ଦ୍ଦଳି ପଚାରିଥିଲେ - " କିବେ କି ଶିକାର ଆଣିଛୁ ? " ଘିନୁ ତାର
ଗାମୁଛାରେ ବନ୍ଧା ହୋଇଥିବା ଗୋଟିଏ ପଦାର୍ଥକୁ ଦେଖାଇ କହିଲା , ସେ ଆଜି
ଏକ ମସ୍ତ ଜାନୁଆର ଶିକାର କରି ଆଣିଛି ।</p>

<p>ଅର୍ଦ୍ଦଳି ପଚାରିଲା ` ବାଘ ? ' ଘିନୁ ମୁଣ୍ଡ ହଲାଇ ନାଇଁ ଜଣାଇଲା ।
" ତେବେ କଣ ଚିତା ... ଭାଲୁ ... ବାର୍ହା ? " ଘିନୁଆ କେବଳ ମୁଣ୍ଡ ହଲାଉ
ଥାଏ । ` ଆଉ ତେବେ କଣ ବେ ? '</p>

<p>ଗୋଳମାଳ ଶୁଣି ସାହେବ ବଙ୍ଗଳା ଭିତରୁ ବାହାରି ଆସିଲେ ।
ଘିନୁଆ ମୁଣ୍ଡିଆ ମାରି ପୁଣି ସାହେବଙ୍କୁ ସେହିପରି ଦାନ୍ତ ଦେଖାଇଲା ।
ସାହେବ ଶିକାରର ଚେହେରାଟା ଦେଖିବାକୁ ଔତ୍ସୁକ ପ୍ରକାଶ କରିବାକୁ
ଘିନୁଆ ଗାମୁଛା ଭିତରୁ ବାହାର କରି ସାହେବଙ୍କ ପାଦତଳେ ଥୋଇଲା
ଗୋଟିଏ ସତକଟା ମଣିଷ ମୁଣ୍ଡ ।</p>

<p>ସାହେବ ଚମକି ଉଟି ପାହୁଣ୍ଡେ ଦି ପାହୁଣ୍ଡ ଗଲେ । ଘିନୁଆ
ହାତ ବଢାଇ ମାଗିଲା - " ସାହେବ ବକ୍୍ସିସ୍୍ ? " କିଛି କ୍ଷଣ ପରେ ମନକୁ ସମ୍ଭାଳି
ନେଇ ଘିନୁଆକୁ ବକ୍୍ସିସ୍୍ ପାଇଁ ଅପେକ୍ଷା କରିବାକୁ ଇସରା କଲେ ଓ ଭିତରକୁ
ୟାଇ ଫୋନ ଦ୍ବାରା ସଶସ୍ତ୍ର ପୋଲିସ ଫୌଜ ଡକାଇଲେ । ଏହା ବ୍ଯତୀତ
ଘିନୁଆକୁ ଜବତ କରିବାର ଉପାଯ ନାହିଁ । ଦେହରେ ତାର ଗୋଟିଏ ଅସୁରର
ବଳ , ହାତରେ ପୁଣି ତାର ଧନୁଶର ଓ ଟାଙ୍ଗୀଟା ରହିଛି । ୟେତେବେଳେ
ହାତ କଡି , ଗୋଡ କଡି ପିନ୍ଧି ଘିନୁଆକୁ ହାଜତରେ ରହିବାକୁ ହେଲା , ସେ
କିଛି ବୁଝି ପାରିଲା ନାହିଁ , ତାକୁ ଏପରି ଭାବରେ ଅଟକାଇ ରଖିବାର
ମତଲବ କମ ? ସୁବିଧା ପାଇଲେ ସେ କାହାରି କାହାରିକି ଏ ବିଷଯ
ପଚାରେ । କିଏ କହେ ତାର ଫାଶୀ ହେବ , କିଏ କହେ ସେ ୟିବ କଳାପାଣି ।
କାହିଁକି ? ସେ ଏମିତି କି ଅପରାଧଟାଏ କରିଛି କି ? କିଛି ବୁଝି ନ ପାରି
ସେମାନଙ୍କ କଥାକୁ ସେ ବିଶ୍ବାକ କରେ ନାହିଁ । ଶେଷରେ ଡେପୁଟି କମିଶନର
ଜେଲ ପରିଦର୍ଶନ କରିବାକୁ ଆସିଥିଲେ , ସେ ତାଙ୍କୁ ହିଁ ସବୁ ହାଲ ପଚାରିଲା ।
ସେ କହିଲେ ୟେ ପୂର୍ବରୁ ବାଘ ଭାଲୁ ମାରୁଥିଲା ବୋଲି ସାଙ୍ଗେ ସାଙ୍ଗେ ବକ୍୍ସିସ୍୍
ପାଉଥିଲା , ଏବେ ମାରିଛି ମଣିଷ । ଏଥର କି ବକ୍୍ସିସ୍୍ ପାଇବ ତାହା ପାଞ୍ଚ
ଜଣ ତ ଫେରେ ବୁଝି ବିଚାରି ଠିକ୍୍ କରିବେ । ଏଇ କଥା ହିଁ ଘିନୁଆ
ମନକୁ ମାନିଲା ।</p>

<p>ୟେଉଁଦିନ ତାର ବିଚାର ହେଲା , ଘିନୁଆ ମନେ ମନେ ଭାବିଲା
ସେଦିନ ତାକୁ ବକ୍୍ସିସ୍୍ ମିଳିବ । ସେ ଉତ୍ସାହିତ ହୋଇ ଜଜ୍୍ଙ୍କ ଆଗେ ସବୁକଥା
କହିଗଲା - " ସେ ୟେ ଗୋବିନ୍ଦ ସରଦାରକୁ ହାଣିଲା , ସେଥିପାଇଁ ତାକୁ କମ୍୍
କଷ୍ଟ କରିବାକୁ ପଡିନାହିଁ । ଆଉ ଅନେକ ଲୋକ ତାକୁ ମାରିବାକୁ ଛକି~
ଥିଲେ ; କିନ୍ତୁ କେହି ପାରି ନ ଥିଲେ । ଗୋବିନ୍ଦ ସରଦାର ସବୁବେଳେ ମଟର
ଚଢି ୟିବାଆସିବା କରେ । ସେ ତାର ଧନ ସମ୍ପତ୍ତି କମେଇଛି ଅନ୍ଯ ସମସ୍ତଙ୍କୁ
ଲୁଟ କରି । ବଡ ସଇତାନ ଲୋକ ଥିଲା ସେ । କେତେ ଲୋକଙ୍କୁ ଉଚ୍ଛନ୍ନ
କରିଛି , କେତେ ଘରେ କଳା କନା ବୁଲେଇଛି ତାର ଠିକଣା ନାହିଁ । ଘିନୁଆର
ସବୁ ଜମିବାଡି ସେହିପରି ନେଇ ମଧ୍ଧ ନିଜର କରିଛି । ସେଦିନ ସନ୍ଧ୍ଯାରେ
ଘିନୁଆର ସ୍ତ୍ରୀକୁ ବି ଅପମାନ ଦବାକୁ ଆସିଥିଲା । ଏଡେ ବଡ ବହପ ! ଘିନୁଆକୁ
ଦେଖି ମଟରରେ ପଳାଉଥିଲା , ଭାବିଥିଲା ତା ହାବୁଡରୁ ଖସି ଚାଲିୟିବ । ତାର
ମଟର ଚକକୁ ତୀରମାରି ଘିନୁଆ ମଟରକୁ ଅଚଳ କରିଦେଲା । ତାପରେ
ଟାଙ୍ଗୀଆରେ ତା ମୁଣ୍ଡଟି କାଟିନେଇ ସିଧା ଦୌଡିଲା ରାତାରାତି ବଣ ଜଙ୍ଗଲ
ଭିତର ଦେଇ ପଚାଶ କିଲୋମିଟର ରାସ୍ତା । ଏକାନିଃଶ୍ବସକେ ଦୌଡି ସେ
ଡେପୁଟି କମିଶନରଙ୍କ ବଙ୍ଗଳା ପାଖରେ ହାଜର ହେଲା । ଗୋବିନ୍ଦ ସରଦାର
ସାମାନ୍ଯ ଲୋକ ନୁହେଁ । ହାତରେ ତାର ଥାଏ ସବୁବେଳେ ବନ୍ଧୁକ । ବାଘ
ଭାଲୁ ଅପେକ୍ଷା ଲୋକ ତାକୁ ବେଶି ଭଯ କରନ୍ତି । ବାଘ , ଭାଲୁଙ୍କ ଅପେକ୍ଷା
ସେ ଲୋକଙ୍କର କ୍ଷତି କରେ ଢେର ବେଶି । ତାକୁ ମାରିବାରେ ଘିନୁଆ କମ୍୍
ସାହସ ଓ ବିଚକ୍ଷଣ ଦେଖାଇ ନାହିଁ ।</p>

<p>କିଛି ବର୍ଷ ତଳେ ମେଳିଆ ଝପଟ ସିଂର ମୁଣ୍ଡ କାଟିଥିବାରୁ ସାହେବ
ଡୋରାକୁ ପାଞ୍ଚ ଶହ ଟଙ୍କା ବକ୍୍ସିସ୍୍ ଦେଇଥିଲେ । ଝପଟ ସିଂ ତ ଏକରକମ
ଭଲ ଲୋକ ଥିଲା । ସେ କେବଳ ଖଜଣାଖାନା ଲୁଟି କରିଥିଲା । ଆଉ କେତେ~
ଜଣ ସିପାହୀକୁ ମାରିଥିଲା । ଗୋବିନ୍ଦ ସରଦାର କିନ୍ତୁ ବଡ ଭଯଙ୍କର ଲୋକ ।
ତାକୁ ମାରିଥିବାରୁ ଅଧିକ ବକ୍୍ସିସ୍୍ ମିଳିବା ଉଚିତ୍୍ ।</p>

<p>ଘିନୁଆର ୟୁକ୍ତି ଶୁଣି ସମସ୍କେ ଘୋ ଘୋ ହୋଇ ହସି ଉଠିଲେ ।
ଜଜ୍୍ ସାହେବ ହସି ହସି କହିଲେ , " ହଁ , ଉପୟୁକ୍ତ ବକ୍୍ସିସ ଦିଆୟିବ । "
ସରକାରୀ ଓକିଲ କହିଲେ , " ତଳେ ଏଠିକି ଖାସ୍୍ ଅଣାୟାଇଛି ବକ୍୍ସିସ
ଦିଆୟିବା ଲାଗି ।</p>

<p>ଘିନୁଆ ଏହାକୁ ପରିହାସ ନ ମଣି ସତ ବୋଲି ମନେକଲା । କାରଣ
ହସ କଉତୁକ ଥଟ୍ଟା ପରିହାସ ସେ ଜାଣେ ନାହିଁ । ତାର ସେ ନିରାଟ ରସ
ଶୂନ୍ଯ ପ୍ରକୃତି ।</p>

<p>ଶେଷରେ ରାଯ ଦିଆଗଲା - ପ୍ରାଣ ଦଣ୍ଡ । ଘିନୁଆ ଏହାର କୌଣସି
ଅର୍ଥ ବୁଢିଲା ନାହିଁ । ପୁଣି ଜେଲକୁ ଫେରାଇ ନେଲାବେଳେ ତାକୁ ବୁଢାଇ
ଦିଆଗଲା ୟେ ତାର ବକ୍୍ସିସ ପାଇବାର ଦିନ ଆସୁଛି ।</p>

<p>ଘିନୁଆ ସେ ପର୍ୟ୍ଯନ୍ତ ବୁଝିଲା ନାହିଁ ୟେ ସେ ଜଣେ ଅପରାଧୀ ଓ
ସେଥିଲାଗି ତାକୁ ପ୍ରାଣଦଣ୍ଡ ଆଦେଶ ହୋଇଛି ।</p>

<p>ଝପଟ ସିଂକୁ ମାରିବା ଓ ଗୋବିନ୍ଦ ସରଦାରକୁ ମରିବା ୟେ ଏକା
କଥା ନୁହେଁ , ତାହା ସେ ବୁଝିବ ନାହିଁ ? ସେ ଜାଣିଲା ନାହିଁ ୟେ ଗୋଟିଏ
ଗୌରବର ବିଷଯ , ଅନ୍ଯଟି ଦୋଷାବହ । ଆଇନର ସୂକ୍ଷ୍ମଜାଲ ଭିତରେ
ପଶିବାକୁ ତାର ମୁଣ୍ଡ କାହିଁ ? ସେ ବଣିଆ ସାନ୍ତାଳ ।</p>

<p>ସେ ମନେ ମନେ ଭାବେ ଡୋରା ପାଆଁଶ ଟଙ୍କା ପାଇଛି , ଢପଟ ସିଂକୁ
ମାରି । ସେଥିରୁ ବେଶି ନ ଦେଲେ ସେ କାହିଁକି ନେବ ? ସବୁ ଫେରାଇ ଦେଇ
କହିବ - " କିଛି ନ ଦିଇ ପଛେ ସାହେବ । ଡୋରା ଠାରୁ ବେଶି ପାଇବା
ମୋର ହକ । "</p>

<p>ଜେଲର ଅନ୍ଧାରିଆ ନିର୍ଜନ ଗୁମ୍ଫାରେ ରହି ସେ କେତେ କଣ ଭାବେ ।
କଥାବାର୍ତ୍ତା କରିବାକୁ ସେ କାହାରିକି ଦେଖେ ନାହିଁ । କଥାବାର୍ତ୍ତା କରିବାକୁ
ତାର ଆଗ୍ରହ ନ ଥାଏ । କେବଳ ବକ୍୍ସିସ୍୍ ପାଇ ଘରକୁ ଫେରିବାକୁ ତାର
ମନ ଛଟପଟ ହେଉଥାଏ ।</p>

<p>ଶେଷରେ ତାର ଫାଶୀର ଦିନ ଆସିଲା । ତାକୁ ପଚରା ଗଲା ,
ତାର ଶେଷରେ କଣ ଦରକାର । ସେ କହିଲା ମୋର ବକ୍୍ସିସ୍୍ ।</p>

<p>" ଆଚ୍ଛା ବକ୍୍ସିସ୍୍ ପାଇବୁ ଆ " କହି ତାକୁ ନେଇଗଲେ । ମୁଣ୍ଡରେ
ତାର ଗୋଟିଏ କନାର ଖୋଳ ପିନ୍ଧାଇ ଦିଆଗଲା । ଘିନୁଆ ମନେ ମନେ
ବିଚାରିଲା ଆଖିରେ ଅନ୍ଧପୁଟୁଳି ବାନ୍ଧି ମୋ ହାତରେ ସୁନା ରୂପା ଢାଳି
ଦିଆୟିବ । ସରକାର ଘରର କେତେ ଫାନ୍ଦି ଫିକର , କାଇଗଦା କଟକଣା ଅଛି ।
ଖାଲି ସେମିତି କଣ ବକ୍୍ସିସ୍୍ ଦିଆୟାଏ ? ସେ ଘରକୁ ଫେରି ସବୁ ସ୍ତ୍ରୀକୁ
ଦେଖାଇବ । କି ଖୁସି ହେବ ସ୍ତ୍ରୀ ତାର ସେ ସବୁ ଦେଖି । ଭଲ ଭରଦ୍ବାର କରି
ଜମିବାଡି ଚଷି ସେ ସୁଖରେ ରହିବ । ଆଉ ତ ଗୋବିନ୍ଦ ସରଦାର ନାହିଁ ସେ
ସବୁ ଲୁଟି କରି ନେବ ।
ହଠାତ୍୍ କଣ ଗୋଟାଏ ଆସି ତା ବେକରେ ବାଜିଲା । //
ଜ୍ଞାନଦନ୍ତ
ଥରେ ଜଣେ ଜ୍ଞାନୀ ପଣ୍ଡିତ ନିଜ ଜ୍ଞାନର ଉତ୍ପୀଡନରେ ଏପରି ଅତିଷ୍ଠ
ହୋଇ ଉଠିଲେ ୟେ , ତାଙ୍କର ପ୍ରତିକାର ପାଇଁ ସେ ରାଜ୍ଯର ସର୍ବବିଧ ଭେଷଜ
ବିଦ୍ବାନମାନଙ୍କର ସାହାୟ୍ଯ ପ୍ରାର୍ଥନା କରି ସୁଦ୍ଧା ବିଫଳ ମନୋରଥ ହୋଇଥିଲେ ।
ସେପରି ପ୍ରଯାସରେ ସେ ଏତେଦୂର ହତୋତ୍ସାହିତ ହୋଇ ପଡିଲେ ୟେ ଦାଣ୍ଡ
ପିଣ୍ଡାରେ ବସି ବସି ନାନା ଦୃଶ୍ଚିନ୍ତାରେ ସଢୁଥିବା ବେଳେ ଦାଣ୍ଡରେ ଶିଶୁ , ବୃଦ୍ଧ
ନିର୍ବିଶେଷରେ ୟେ କୌଣସି ପରିଚିତ ଅପରିଚିତ ପଥିକ ୟାଉଥିବା ଦେଖିଲା
ମାତ୍ରେ ସେ ବିକଳ ହୋଇ ସେଇ ଗୋଟିଏ ମାତ୍ର ପ୍ରଶ୍ନ ପଚାରୁଥିଲେ -</p>

<p>' ଭୋ ଭୋ ପଥିକ ! ଜ୍ଞାନ ପୀଡାରୁ ଉଦ୍ଧାର ପାଇବାର ପନ୍ଥା କଣ ? "</p>

<p>ପଥିକମାନେ ମହାପଣ୍ଡିତଙ୍କ ଦୁଃଖ ବୁଝୁଥିଲେ କି ନା କିଏ ଜାଣେ !
ଅଧିକାଂଶ ନିଜର ଅଜ୍ଞତା ସୂଚକ ଚାହାଣୀରେ ପଣ୍ଡିତଙ୍କୁ କେବଳ ସମ୍ମାନ ସୂଚକ
ନମସ୍କାରଟିଏ ଜଣାଇ ଘଟଣା ସ୍ଥଳରୁ ଦ୍ରୁତ ଗତିରେ ବିଦାଯ ନେଇ ୟାଉଥିଲେ ।
ଅବଶ୍ଯ ଜଣେ ଜଣେ ବଦମାସ୍୍ ଲୋକ ପଣ୍ଡିତଙ୍କ ଦୁଃଖର ମାତ୍ରା ଲେଶ ମାତ୍ର
ଅନୁଭବ ନ କରି କେ0ବଳ ସହାନୁଭୂତି ସୂଚକ ହସଟିଏ ହସି ଦେଉଥିଲେ ।
ଜଦ୍ବାରା ପଣ୍ଡିତଙ୍କ ଦୁଃଖ ଦୂର ହବ କଅଣ ତାଙ୍କର ଅନ୍ତରାତ୍ମା ଆହୁରି
ବିଷାଦରେ ଭରପୁର ହୋଇ ୟାଉଥିଲା । ଅସଂପୃକ୍ତ ବ୍ଯକ୍ତିମାନଙ୍କ ହସକୁ
ଉପହାସ ମଣି ସେ ସେତେବେଳେ ଏତେ କ୍ଷୁବ୍୍ଧ ହୋଇ ପଡୁଥିଲେ ୟେ
ସବୁରି ଦେଖନ୍ତିରେ ନିଜ ଗାଲରେ ନିଜେ ଚଟକଣା କେତୋଟି ଖାଇବାର ଦୃଶ୍ଯ
ପ୍ରଦର୍ଶନ କରି ସେ ଓଲଟି ଅନ୍ଯମାନଙ୍କୁ ଆପ୍ଯାଯିତ କରୁଥିଲେ ।</p>

<p>ଦିନେ ରାତିରେ ପଣ୍ଡିତଙ୍କ ଅଜାଣତରେ ତାଙ୍କ ପାଟିରୁ ତାଙ୍କର ଅବ~
ଶେଷ ଜ୍ଞାନଦନ୍ତଟି ପାନ ପିକ ସହିତ ଅକସ୍ମାତ ଗୋଟିଏ ପିକଦାନୀ ଭିତରକୁ
ଗଳି ପଡିଗଲା । ସକାଳରୁ ହଠାତ୍୍ ଦୀର୍ଘ ଦିନର ବିଷାଦର ବୋଝ ତାଙ୍କ ମୁଣ୍ଡରୁ
ଓହ୍ଲାଇ ୟାଇଛି । ଏହିପରି ଅନୁଭୂତି ନିଜ ହୃଦଯରେ ଉଦଯ ହେବା ଜାଣି
ପଣ୍ଡିତଙ୍କ ଆନନ୍ଦର ସୀମା ରହିଲା ନାହିଁ , କିନ୍ତୁ ଦୀର୍ଘ ସମଯ ପର୍ୟ୍ଯନ୍ତ ସେ
ଆଦୌ ସେ ଘଟଣାର ରହସ୍ଯଭେଦ କରିପାରିଲେ ନାହିଁ । ସେଇ କାରଣରୁ
ଦାଣ୍ଡ ପିଣ୍ଢାରେ ସେ ବସି ମହା ଖୁସିରେ ରାସ୍ତାକୁ ଚାହିଁ ଗଲା ଅଇଲା ଲୋକଙ୍କୁ
ଆହୁରି ଗୋଟିଏ ଅଦ୍ଭୁତ ପ୍ରଶ୍ନ ସେ ପଚାରି ଲାଗିଲେ -</p>

<p>" ଭୋ ଭୋ ପଥିକ ! ମୋର ଆଜିର ଆନନ୍ଦର କାରଣ ସଂପର୍କରେ କେହି
କିଛି ଆଲୋକପାତ କରି ପାରିବ କି ? "</p>

<p>ପଥିକମାନେ ପୂର୍ବପରି ହୁଏତ ନମସ୍କାରଟିଏ କରି କିମ୍ବା ହସଟିଏ ହସି
ଦେଇ ବିଦାଯ ନେଲେ ; କିନ୍ତୁ ପଣ୍ଡିତଙ୍କ ଦୁଃଖ ଓ ଆନନ୍ଦର ରହସ୍ଯଭେଦ
କରିବା କାହାରି ପକ୍ଷରେ ସମ୍ଭବ ହେଲା ନାହିଁ ।</p>

<p>ଏତିକିବେଳେ ପଣ୍ଡିତଙ୍କ ଘରର ଚାକରାଣୀ ବିଚାରୀ ଧଇଁ ସଇଁ ହୋଇ
କୁଆଡୁ ଧାଇଁ ଆସିଲା । ହାତରେ ତାର ଥାଏ ସେଇ ଜ୍ଞାନଦାନ୍ତଟି । ୟାହାକୁ
ପଣ୍ଡିତେ ଅଜ୍ଞାତସାରରେ ବିସର୍ଜନ କରିଥିଲେ ଗତ ରାତିରେ ।</p>

<p>" ମହାଶଯ ! ଆପଣଙ୍କ ଜ୍ଞାନଦାନ୍ତଟି ପଲଙ୍କତଳ ପିକଦାନୀରେ ପଡି
ୟାଇଥିବା ସମ୍ବାଦ ହୁଏତ ଏପର୍ୟ୍ଯନ୍ତ ଆପଣ ନିଜେ ମଧ୍ଧ ଜାଣି ନାହାନ୍ତି ।
ସକାଳେ ପିକଦାନୀ ସଫା କରୁ କରୁ ଏ ଦାନ୍ତଟିକୁ ଆବିଷ୍କାର କରିଛି ମୁଁ । "</p>

<p>ଏତକ କହି ଚାକରାଣୀଟି ବର୍ଜିତ ଜ୍ଞାନଦାନ୍ତକୁ ପଣ୍ଡିତଙ୍କ ଗୋଚରାର୍ଥେ
ତାଭ୍ର ପାଦତଳେ ରଖିଦେଲା ଓ ପୁଣି କହିଲା -</p>

<p>" ଜୀବନ ସାରା ଏ ଆପଣଙ୍କୁ ସମସ୍ତ ବୁଦ୍ଧା ଓ ଜ୍ଞାନ ଦେଇ
ଆସିଛି । ଏହାର ଅନ୍ଯ ସଙ୍ଗୀମାନେ କେଉଁଦିନୁ ଆପଣଙ୍କ ପାଟି ଛାଡି କୁଆଡେ
ଚାଲିଗଲାଣି । କେବଳ ଏଇ ଗୋଟିକ ମାତ୍ର ଅବଶିଷ୍ଟ ଜ୍ଞାନଦାନ୍ତ ୟୋଗୁଁ
ଆପଣଙ୍କ ସବୁ ଖ୍ଯାତି ସୁନାମ ବଜାଯ ରହିଛି । ଅତଏବ ଏ ଦାନ୍ତକୁ ଫୋପାଡି
ନ ଦେଇ ପାଖରେ ରଖିବା ହୁଅନ୍ତୁ ମହାଶଯ । "</p>

<p>ଚାକରାଣୀ ହାତରୁ ନିଜର ବର୍ଜିତ ଜ୍ଞାନଦନ୍ତକୁ ବିସ୍ମଯ ସହକାରେ
ଗ୍ରହଣ କରିବା ପାଇଁ ପଣ୍ଡିତେ ହାତ ବଢେଇ ଦେବା ମାତ୍ରେ ହଠାତ୍୍
ହୃଦଯରେ ଦାରୁଣ କଷ୍ଟ ଅନୁଭବ କଲେ । ସହସା ତାଙ୍କର ଜ୍ଞାନୋଦଯ ହେଲା ।
ସେ ବୁଝି ପାରିଲେ ୟେ ଗତ କେତେ ବର୍ଷ ଧରି ୟେଉଁ ୟନ୍ତ୍ରଣା ଅନୁଭବ କରି
ଅହରହ ଛଟରଟ ହେଉଥିଲେ ତାର କାରଣ ସେଇ ଭଯଙ୍କର ପଦାର୍ଥଟି ଛଡା
ଅନ୍ଯ କିଛି ହୋଇ ନ ପାରେ ।</p>

<p>" ଦୂ଱୍୍ ଦୂ଱୍୍ ! " ପଣ୍ଡିତଙ୍କ ଗର୍ଜନରେ ରାସ୍ତା ମୁଖରିତ ହୋଇ
ଉଠିଲା । ଚାକରାଣୀଟି ଚମକି ପଡିଲା । ତା ହାତରୁ ପଣ୍ଡିତଙ୍କ ଜ୍ଞାନଦାନ୍ତଟି
ଖସିପଡିଲା ।</p>

<p>" ବଦମାସ୍୍ । " ପଣ୍ଡିତେ ଆହୁରି ଜୋରରେ ଗର୍ଜି ଉଠିଲେ ।
ଚାକରାଣୀକୁ ଚାହିଁ ଆଦେଶ ଦେଲେ ସେ , ସେ ରାକ୍ଷାସକୁ ଶୀଘ୍ର ଅନ୍ତର କର
ମୋ ସାମ୍ନାରୁ ମୂର୍ଖ ! ୟାବତୀଯ ଦୁର୍ଦ୍ଦଶା ୟାବତୀଯ ପୀଡାର କାରଣ ଏଇ
ତେବେ । - କିଏ ବା ଜାଣିଥିଲା । ନେ-ନେ ଫୋପାଡ୍୍ ଫୋପାଡ୍୍ ସେ
ବଦ୍୍ମାସ ଜିନିଷକୁ ଶୀଘ୍ର । "</p>

<p>ଚାକରାଣୀଟି ପଣ୍ଡିତଙ୍କ ଘରେ ବହୁ ବହୁ କାର୍ୟ୍ଯ କରି ଆସିଥିବା ୟୋଗୁଁ
ବା ପଣ୍ଡିତଙ୍କ ପ୍ରକୃତି ସହିତ ଉତ୍ତମ ପରିଚଯ ଥିବା ହେତୁ ସେ ତତ୍୍କ୍ଷଣାତ
ଘଟଣାଟିର ଅର୍ଥ ବୁଝି ନେଇ ପାରିଲା । ତେଣୁ ପଣ୍ଡିତଙ୍କ ଗାଳିକୁ ଖାତିର ନ
ଦେଇ ତାକୁ ପୁଣି ଥରେ ପିକଦାନୀରେ ରଖିଦେବାକୁ ମନସ୍ଥ କଲା ।
 +&gt;*
               0 
</p></body></text></cesDoc>