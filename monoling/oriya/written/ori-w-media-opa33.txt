<cesDoc id="ori-w-media-opa33" lang="ori">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ori-w-media-opa33.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ପ୍ରଗତିବା</h.title>
<h.author>ପ୍ରଦ୍ଯୁମ</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Paper</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">opa33</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0602.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-12</date></creation>
<langUsage>Oriya</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;54 ବର୍ଷ ପରେ ପୁନରାବୃତ୍ତି
ଭାରତରେ ସମୁଦାଯ ଉତ୍ତର ପୂର୍ବ ଅଞ୍ଚଳରେ ଏବଂ
ନେପାଳରେ ଗତ ରବିବାର ଦିନ ହୋଇଥିବା ଭୂମିକମ୍ପ ଫଳରେ
କେବଳ ବିହାର ଓ ନେପାଳରେ ପ୍ରାଯ 1000 ବ୍ଯକ୍ତିଙ୍କ ମୃତ୍ଯୁ
ହୋଇଛି । ହଜାର ହଜାର ଲୋକ ଆହତ ହୋଇଛନ୍ତି । ବହୁ କୋଠା~
ବାଡି ଓ ଘରଦ୍ବାର ଭାଙ୍ଗି ୟାଇଛି ।</p>

<p>54 ବର୍ଷ ପୂର୍ବେ 1934 ମସିହା ଜାନୁଆରୀ ମାସରେ ହୋଇ~
ଥିବା ପ୍ରଳଯଙ୍କରୀ ବିହାର ଭୂମିକମ୍ପ ୟେଉଁ ଅଞ୍ଚଳରେ ହୋଇଥିଲା
ଏଥର ମଧ୍ଧ ଠିକ୍ ସେହି ଅଞ୍ଚଳରେ ଏହି ଭୂମିକମ୍ପ ଅନୁଭୂତ ହୋଇଛି ।
କିନ୍ତୁ 1934 ପରିମାଣର ଧନଜୀବନ କ୍ଷତି ଏଥର ଘଟି ନ ଥିଲେ
ମଧ୍ଧ ବିଗତ ଅର୍ଦ୍ଧଶତାବ୍ଦୀ ଭିତରେ ଏହା ଭାରତର ଏବଂ ନେପାଳର
ସବୁଠାରୁ ଭଯାବହ ଭୂମିକମ୍ପ ବୋଲି ବିଚାର କରିବାକୁ ପଡିବ ।
30 ସେକେଣ୍ଡ ଠାରୁ ପ୍ରାଯ 1 ମିନିଟ ହୋଇଥିବା ଏହି ଭୂମିକମ୍ପ
ବିହାରର ଦରଭଙ୍ଗାରେ ସବୁଠାରୁ ଅଧିକ ଅନୁଭୂତ ହୋଇଛି ।
କିନ୍ତୁ ଏହି ଭୂମିକମ୍ପ ବିହାର ଓ ନେପାଳ ଛଡା ପଶ୍ଚିମବଙ୍ଗ , ସିକିମ ,
ଆସାମ , ଅରୁଣାଚଳ ପ୍ରଦେଶ , ମିଜୋରାମ , ମଣିପୁର , ତ୍ରିପୁରା ,
ମେଘାଳଯ , ଉତ୍ତର ପ୍ରଦେଶ ଓ ଓଡିଶାରେ ମଧ୍ଧ ଅନୁଭୂତ ହୋଇଛି ।
ଦିଲ୍ଲୀ , କଲିକତା , ପାଟନା ଓ ରାଉରକେଲା ସମେତ ବହୁ ସହରରେ
ଏହି ଭୂକମ୍ପନ ଫଳରେ ଆତଙ୍କ ସୃଷ୍ଟି ହୋଇଛି । ରବିବାର ଦିନ ରାତି
ପ୍ରାଯ 4 ଟା 40 ମିନିଟରେ ହୋଇଥିବା ଏହି ଭୂମିକମ୍ପ ସମଯରେ ବହୁ
ଲୋକଙ୍କର ଶୋଇବା ଅବସ୍ଥାରେ ମୃତ୍ଯୁ ହୋଇଛି । ଏହି ଭୂମିକମ୍ପ
ଫଳରେ ନଦୀଶୟ୍ଯା , ନଦୀବନ୍ଧ ସେତୁ ଏବଂ ଅନ୍ଯାନ୍ଯ ସ୍ଥାଯୀ ନିର୍ମାଣ
ଗୁଡିକର କି କ୍ଷତି ହୋଇଛି ତାହା ସହଜରେ ଏବେ କୁହାୟାଇ
ପାରୁ ନାହିଁ । କିନ୍ତୁ ହିମାଳଯର ପାଦଦେଶରେ ଥିବା ଏହି ଅଞ୍ଚଳରେ
ଅଧିକ ଭୂମିକମ୍ପର ଆଶଙ୍କା ବିଶେଷଜ୍ଞମାନଙ୍କ ପକ୍ଷରୁ ପ୍ରକାଶ ପାଇଛି ।</p>

<p>ଏହି ଭୂମିକମ୍ପ ବିଷଯରେ ପୁନେସ୍ଥିତ ଭୂ-କମ୍ପନବିତ୍ ଶ୍ରୀ ଅରୁଣ
ବାପତ୍ ବହୁ ଆଗରୁ ସତର୍କ ବାଣୀ ଶୁଣାଇଥିଲେ ମଧ୍ଧ ଜନସାଧାରଣଙ୍କୁ
ଏ ବିଷଯରେ କିଛି କୁହାୟାଇ ନ ଥିଲା । ମନୁଷ୍ଯ ବିଜ୍ଞାନ କ୍ଷେତ୍ରରେ ବହୁ
ଅଗ୍ରଗତି କରିଥିଲେ ମଧ୍ଧ ଭୂକମ୍ପନର ଆଗୁଆ ସତର୍କବାଣୀ ଠିକ୍ ଭାବରେ
ଦିଆୟାଇ ପାରୁ ନାହିଁ ।</p>

<p>ଭାରତର ଏହି ଉତ୍ତର-ପୂର୍ବ ଅଞ୍ଚଳରେ ବିଗତ 300 ବର୍ଷ
ଭିତରେ 15 ଟି ବଡ ଧରଣର ଭୂମିକମ୍ପ ହେଲାଣି । ଭବିଷ୍ଯତରେ ଅଧିକ
ଭୂମିକମ୍ପର ସମ୍ଭାବନା ରହିଛି । 1950 ସ୍ବାଧୀନତା ଦିବସରେ
ଆସାମରେ ହୋଇଥିବା ଭୂମିକମ୍ପରେ ପ୍ରାଯ 1500 ଲୋକ ପ୍ରାଣ
ହରାଇଥିଲେ ।</p>

<p>କିନ୍ତୁ ହିମାଳଯ ପାଦଦେଶରେ ଥିବା ଭାରତର ଏହି ବିସ୍ତୃତ
ଉତ୍ତର ପୂର୍ବ ଅଞ୍ଚଳର ଗୁରୁତ୍ବ ଦୃଷ୍ଟିରୁ କ୍ରମାଗତ ଭୂମିକମ୍ପର କାରଣ ଓ
ତାହାର ପ୍ରତିକାର ବିଷଯରେ ଗବେଷଣାର ଆବଶ୍ଯକତା ରହିଛି ।
ବିହାରର ଭୂମିକମ୍ପ ଫଳରେ ବାସହୀନ ହୋଇ ପଡିଥିବା
ହଜାର ହଜାର ଶିଶୁ ଓ ଲୋକମାନଙ୍କ ପାଇଁ ତୁରନ୍ତ ରିଲିଫ୍ ଓ
ଥଇଥାନ ବ୍ଯବସ୍ଥା କରାୟିବା ଆବଶ୍ଯକ ।</p>

<p>ଭୋକ ଉପାସରେ କିମ୍ବା ଚିକିତ୍ସା ଅଭାବରୁ ଜଣେ ହେଲେ ବ୍ଯକ୍ତିର
ୟେପରି ମୃତ୍ଯୁ ନ ହୁଏ ତାହା ସରକାରଙ୍କୁ ଦେଖିବାକୁ ପଡିବ ।
ସ୍ବେଚ୍ଛାସେବୀ ଅନୁଷ୍ଠାନ ମାନଙ୍କର ସାହାୟ୍ଯ ସରକାର ଏଥିପାଇଁ ଗ୍ରହଣ
କରିବା ଉଚିତ୍ । ଭାରତର ଏକ ବ୍ଯାପକ ଅଞ୍ଚଳରେ ଭୂମିକମ୍ପର ଆଶଙ୍କା
ସବୁବେଳେ ରହିଥିବାରୁ ସେସବୁ ଅଞ୍ଚଳରେ ଘରଦ୍ବାର , କୋଠାବାଡି
ଓ ରାସ୍ତାଘାଟ ନିର୍ମାଣର ନୂତନ କୌଶଳ ଇଞ୍ଜିନିଅର ଓ
ବୈଜ୍ଞାନିକମାନେ ଚିନ୍ତା କରିବା ଉଚିତ୍ । ଭୂମିକମ୍ପର ଠିକଣା ସମଯ
ୟେପରି ଆଗୁଆ ରେଡିଓ ଏବଂ ଟେଲିଭିଜନ ଜରିଆରେ ଲୋକମାନଙ୍କୁ
ଜଣାଇ ଦିଆୟାଇ ପାରିବ ସେଥିପାଇଁ ପଦକ୍ଷେପ ଗ୍ରହଣ କରାୟିବା
ଉଚିତ୍ । ଭୂମିକମ୍ପ ପରି ପ୍ରାକୃତ୍ରିକ ବିପର୍ୟ୍ଯଯ ବିରୁଦ୍ଧରେ ସଂଗ୍ରାମ କରିବା
ନିଶ୍ଚିତ ଭାବେ କଷ୍ଟକର ।</p>

<p>ପ୍ରଦେଶ କଂଗ୍ରେସ ଇ କମିଟି ପୁନର୍ଗଠନ
ଶ୍ରୀ କାହ୍ନୁ ଚରଣ ଲେଙ୍କା ରାଜ୍ଯ କଂଗ୍ରେସ ଇ କମିଟିର
ଆଡହକ ସଭାପତି ଭାବେ 6 ମାସ ତଳେ ନିୟୁକ୍ତ ହୋଇଥିଲେ ମଧ୍ଧ
ପୁନର୍ଗଠିତ ପ୍ରଦେଶ ଆଡହକ୍ କମିଟିର ତାଲିକା ଏ.ଆଇ.ସି.ସି. ଇ
ପକ୍ଷରୁ ପ୍ରକାଶ ପାଇଁ ନାହିଁ । ଫଳରେ ଶ୍ରୀଲେଙ୍କା ବିଧିବଦ୍ଧ ପ୍ରଦେଶ
କଂଗ୍ରେସ ଇ କମିଟି ନ ଥାଇ ତାହାର ସଭାପତି ଭାବରେ ଏକାକୀ
କାର୍ୟ୍ଯ କରୁଛନ୍ତି । ମୁଖ୍ଯମନ୍ତ୍ରୀ ଶ୍ରୀ ପଟ୍ଟନାଯକ ଗତ ରବିବାର ଦିନ
ଦିଲ୍ଲୀରୁ ଫେରି ସମ୍ବାଦିକମାନଙ୍କୁ କହିଛନ୍ତି ୟେ , ଖୁବ୍ ଶୀଘ୍ର ପୁନର୍ଗଠିତ
ପ୍ରଦେଶ କଂଗ୍ରେସ କମିଟିର ନାମ ତାଲିକା ଏ.ଆଇ.ସି.ସି. ଇ
ଘୋଷଣା କରିବେ । ପ୍ରଦେଶ କଂଗ୍ରେସ ଇ ସଭାପତି ଶ୍ରୀ ଲେଙ୍କା
ପୂର୍ବରୁ ବହୁବାର ପ୍ରଦେଶ କଂଗ୍ରେସ ଇ କମିଟି ଗଠନ ବିଷଯରେ
ବିଭିନ୍ନ ସମଯ ସୀମା ଘୋଷଣା କରିଛନ୍ତି । କିନ୍ତୁ ତାଙ୍କର ସମଯ ସୀମା
ଅନୁସାରେ ପ୍ରଦେଶ କଂଗ୍ରେସ ଇ କମିଟି ଗଠିତ ହୋଇ ନାହିଁ । ଫଳରେ
ଶ୍ରୀ ଲେଙ୍କା ଓ ମୁଖ୍ଯମନ୍ତ୍ରୀ ଶ୍ରୀ ପଟ୍ଟନାଯକଙ୍କର ଏ.ଆଇ.ସି.ସି. ଇ
ମହଲରେ ବିଶେଷ ପତିଆରା ନ ଥିବା ଜଣାପଡୁଛି ।</p>

<p>ସେହି ଦୁଇଜଣଙ୍କ ଭିତରେ ଉତ୍ତମ ସମ୍ପର୍କ ଥିଲେ ମଧ୍ଧ
ଏକ ସର୍ବସମ୍ମତ ଆଡହକ୍ ପ୍ରାଦେଶିକ କମିଟି ଏ.ଆଇ.ସି.ସି. ରୁ
ଅନୁମୋଦନ କରାଇବାରେ ସେମାନେ ବିଫଳ ହୋଇଛନ୍ତି । ଅପର
ପକ୍ଷରେ ଶ୍ରୀମତୀ ନନ୍ଦିନୀ ଶତପଥୀଙ୍କର କଂଗ୍ରେସ ଇ ରେ ୟୋଗଦାନ
ପରେ ଓଡିଶା କଂଗ୍ରେସ ଇ ର ପରିସ୍ଥିତି ଭିନ୍ନ ମୋଡ ନେଇଛି । ଏପରି
ଅବସ୍ଥାରେ ଏ.ଆଇ.ସି.ସି. ପକ୍ଷରୁ ପ୍ରଦେଶ କଂଗ୍ରେସ ଇ କମିଟି
ପୁନର୍ଗଠନ ଖୁବ୍ ଶୀଘ୍ର ହୋଇୟିବ ବୋଲି ମୁଖ୍ଯମନ୍ତ୍ରୀ ଦେଇଥିବା
ସୁଚନାକୁ ବିଶ୍ବାସ କରା ୟାଇ ନ ପାରେ । ମୁଖ୍ଯମନ୍ତ୍ରୀ ଶ୍ରୀ ପଟ୍ଟନାଯକ ଓ
ଶ୍ରୀ ଲେଙ୍କା ଉଭଯ ନନ୍ଦିନୀ ଶତପଥୀଙ୍କୁ ରାଜ୍ଯ କଂଗ୍ରେସ ଇ ରେ
କୌଣସି ପଦପଦବୀ ଦିଆୟିବାର ବିରୋଧ କରୁଥିବା ସମଯରେ ଅସନ୍ତୁଷ୍ଟ
ଗୋଷ୍ଠୀ ପୁରାପୁରି ଭାବେ ଶ୍ରୀମତୀ ଶତପଥୀଙ୍କୁ ସମର୍ଥନ ଜଣାଇଛନ୍ତି ।
ଏପରି ଅବସ୍ଥାରେ ପ୍ରାଦେଶିକ କମିଟି ପୁନର୍ଗଠନ କରିବା
ଏ.ଆଇ.ସି.ସି. ଇ ପାଇଁ ସହଜସାଧ୍ଧ ନୁହେଁ । କର୍ଣ୍ଣାଟକ ଓ ବିହାର
ପ୍ରଭୃତି ଗୁରୁତ୍ବପୂର୍ଣ୍ଣ ରାଜ୍ଯରେ ମଧ୍ଧ ଏ.ଆଇ.ସି.ସି. ଇ ପ୍ରାଦେଶିକ କମିଟି
ଚୂଡାନ୍ତ କରି ପାରି ନାହାଁନ୍ତି । //
ବିଳାସ ବ୍ଯସନରେ ବ୍ଯଯ
ୟୋଜନା କମିଶନ ତରଫରୁ
ପ୍ରକାଶିତ ହୋଇଥିବା ଅଷ୍ଟମ
ପଞ୍ଚବାର୍ଷିକ ଅର୍ଥନୈତିକ ପରିକଳ୍ପନା
ଆଭିମୁଖ୍ଯରେ ସୂଚିତ ହୋଇଛି ୟେ
ବିଳାସବ୍ଯସନ ସାମଗ୍ରୀର ଉତ୍ପାଦନ
ସଙ୍କୋଚନ କରାୟିବ । କାରଣ
ଦେଶରେ ସଞ୍ଚଯ ପରିମାଣ ବୃଦ୍ଧି
ପାଇବା ଦରକାର । ସଞ୍ଚଯ ବୃଦ୍ଧି
ହେଲେ , ୟୋଜନା ଲାଗି ଆବଶ୍ଯକ
ସମ୍ବଳ କେତେକ ପରିମାଣରେ
ମିଳିୟିବ । ବର୍ତ୍ତମାନ ସଞ୍ଚଯ ହାର
ହେଲା ଜାତୀଯ ଆଯର 18
ଶତାଂଶ । ଅଷ୍ଟମ ପଞ୍ଚବାର୍ଷିକ
ୟୋଜନା କାଳରେ ଅର୍ଥନୈତିକ
ଅଭିବୃଦ୍ଧି ହାର 6 ଦଶମିକ 5 ଲକ୍ଷ
ଧାର୍ୟ୍ଯ ହୋଇପାରେ । ସେଥିନିମିତ୍ତ
ସଞ୍ଚଯ ହାର ପ୍ରତିଶତ 28 ଦଶମିକ
5 ହେବା ବିଧେଯ ।</p>

<p>ପୁଞ୍ଜି ବିନିୟୋଗ ଲାଗି ସମ୍ଭଳ
ବୃଦ୍ଧି ନିମିତ୍ତ ବିଳାସ ସାମଗ୍ରୀରେ
ଅର୍ଥବ୍ଯଯ ସୁୟୋଗ ସୀମିତ
ରଖାଗଲେ , ବିତ୍ତଶାଳୀ ଲୋକଙ୍କର
ସଞ୍ଚଯ ସ୍ପୃହା ବଳବତୀ ହୋଇ~
ପାରେ । ସେଇ କାରଣରୁ ବିଳାସ
ସାମଗ୍ରୀ ଉତ୍ପାଦନ ଲାଗି ନୂଆ
କଳକାରଖାନା ବସାଇବାକୁ
ଅନୁମତି ଦିଆୟିବା ଉଚିତ୍ ନୁହେଁ
ବୋଲି ପ୍ଲାନିଂ କମିଶନ ପରାମର୍ଶ
ଦେଇଛନ୍ତି । ବର୍ତ୍ତମାନ ସେସବୁ
ଜିନିଷ ୟାହା ଉତ୍ପାଦିତ ହେଉଛି ,
ସେଥିରୁ ଅଧିକାଂଶ ବିଦେଶକୁ
ରପ୍ତାନି କରିବାକୁ ସରକାର
ପ୍ରୋତ୍ସାହନ ଦିଅନ୍ତୁ ବୋଲି
ୟୋଜନା କମିଶନ କହିଛନ୍ତି ।</p>

<p>ସରକାରଙ୍କର ଗୋଟିଏ ଅଙ୍ଗ
ୟାହା ଚିନ୍ତା କରେ , ଅନ୍ଯ ଅଙ୍ଗ
ସେଥିପ୍ରତି ଧ୍ଧାନ ଦେବା ସହଜ
ହୋଇ ନ ଥାଏ । ସରକାରୀ କଳ
ଏପରି ଜଟିଳ ଓ ଗହନ ୟେ ତା'
ଭିତରେ ସମନ୍ବଯ ସାଧନ ମୁସ୍କିଲ୍ ।
ୟୋଜନା କମିଶନ କହୁଛନ୍ତି
ଗୋଟିଏ କଥା ; ସରକାରଙ୍କ ଅନ୍ଯ
ଦପ୍ତର ସେକ୍ରେଟେରୀମାନେ ଅନ୍ଯ
ପ୍ରକାରେ ନିଷ୍ପତ୍ତି ନେଉଛନ୍ତି ।</p>

<p>ଭାରତ ସରକାର ଗଠନ
କରିଥିବା ସ୍ବତନ୍ତ୍ର ` କମିଟି ଅଫ୍
ସେକ୍ରେଟେରିଜ୍ ' ସୁପାରିଶ୍ କରିଛି
ୟେ , ଦେଶରେ ଭିଡିଓ କ୍ଯାସେଟ୍
ରେକର୍ଡର ( ଭି.ସି.ଆର ) ଏବଂ
ଭିଡିଓ କ୍ଯାସେଟ୍ ପ୍ଲେଯାର
( ଭି.ସି.ପି. ) ଉତ୍ପାଦନ ବୃଦ୍ଧି ଲାଗି
ଗୋଟିଏ ପବ୍ଲିକ୍ ସେକ୍ଟର ସଂସ୍ଥା
ଏବଂ ତିନିଗୋଟି କମ୍ପାନୀଙ୍କୁ
ଲାଇସେନସ୍ ମଞ୍ଜୁର କରାୟିବ ।
କମ୍ପାନୀଗୁଡିକ ହେଲେ ବାଙ୍ଗାଲୋରର
ବ୍ରିଟିଶ୍ ଫିଜିକାଲ୍ ଲେବୋରେ~
ଟୋରୀ ` ବି.ପି.ଏଲ୍. ' ବମ୍ବେର
` କଲ୍ଯାଣୀ ସାର୍ପ ' ଏବଂ ଔରଙ୍ଗା~
ବାଦର ` ଭିଡିଓକନ୍ ' ।</p>

<p>ରଙ୍ଗୀନ୍ ଟି.ଭି. , ଭି.ଡି.ଓ.
ସେଟ୍ , ଏଯାର କଣ୍ଡିସନର ,
ଵାସିଙ୍ଗ୍ ମେସିନ୍ ବିକ୍ରୀ ଭାରତରେ
ବଢି ବଢି ଚାଲିବ । ଉଚ୍ଚ ମଧ୍ଧବିତ୍ତ
ପରିବାରରେ ଏ ସାମଗ୍ରୀ ଲୋଡା
ପଡିଲାଣି । ବଜାରକୁ ଏସବୁ
ୟୋଗାଇଦେବା ସଙ୍ଗେ ସଙ୍ଗେ
ସଞ୍ଚଯ ହାର ବୃଦ୍ଧି ହେଉ ବୋଲି
କିପରି ଆଶା କରିବା ? ପ୍ଲାନିଂ
କମିଶନ କ'ଣ ପରାମର୍ଶ ଦେଉଛି ,
ସେକ୍ରେଟେରୀ ପ୍ଯାନେଲ୍ କି ନିଷ୍ପତ୍ତି
ନେଉଛନ୍ତି , ସେଥିରୁ କେଉଁଟି
ୟୁକ୍ତିୟୁକ୍ତ ? କେଉଁଟିରୁ କେତେ
ଅଂଶ ଗ୍ରହଣୀଯ ? ଭାରତୀଯ ଧନୀକ
ଗୋଷ୍ଠୀର ମନୋବୃତ୍ତି ହେଲା
ବିଦେଶୀ ସୌଖିନ ସାମଗ୍ରୀ ଲୁଚା~
ଛପାରେ କିଣି ଆଣିବା । ଦେଶରେ
ବିଳାସ ସାମଗ୍ରୀ ଉତ୍ପାଦନ ସୀମିତ
ରଖିବାକୁ ହେଲେ , ବିଦେଶୀ ଜିନିଷ
ଆସିବାଟା ବନ୍ଦ କରିବାକୁ ହେବ ।
ସଞ୍ଚଯ ହାର ବୃଦ୍ଧି କରିବା ନିମିତ୍ତ
ସୁପରିକଳ୍ପିତ ବ୍ଯବସ୍ଥା ଲୋଡା । //
ମୁଖା ପିନ୍ଧିଥିବା ମଣିଷର ଅନ୍ଯ ରୂପ
ସେଦିନ ହଠାତ୍ ଭଦ୍ର
ଲୋକଙ୍କ ସହିତ ଏମିତି ସାକ୍ଷାତ
ହୋଇୟିବ ବୋଲି ମୁଁ କଳ୍ପନା
କରି ନ ଥିଲି । ଭଦ୍ରବ୍ଯକ୍ତି କଥା
କହିବାରେ ଓସ୍ତାଦ୍ ।
ୟେ କୌଣସି ପରିସ୍ଥିତିରେ ସେ କଥା
କହି ଉଦ୍ଧାର ପାଇ ୟାଆନ୍ତି । କଥା
ପଡୁ ପଡୁ ଜଗନ୍ନାଥଙ୍କଠାରୁ ଦିଲ୍ଲୀ
ପର୍ୟ୍ଯନ୍ତ ଉଡିୟାଏ । ତାଙ୍କୁ ମୁଁ
ଅନେକ ଥର ଜଣେ ପାର୍ଲାମେଣ୍ଟ
ସଦସ୍ଯଙ୍କ ଘରେ ବସିଥିବାର
ଦେଖିଛି । ସେ ସେଠାକୁ ଗାଆଁରୁ
ଆସିଲେ ଖାଲି ହାତରେ ଆସନ୍ତି ।
କିଛି ଲୋକ ୟେଉଁମାନଙ୍କର ଏମ୍.ପି.
ସାହେବଙ୍କ ପାଖରେ କିଛି କାମ
ଥାଏ - ଲୁଙ୍ଗିଟିଏ , ନ ହେଲେ
ଖୋର୍ଦ୍ଧା ଗାମୁଛାଟିଏ କିଣି ନିଅନ୍ତି ।
ଏମ.ପି. ମହୋଦଯ ୟେ ତାଙ୍କୁ
ଭଲ ପାଆନ୍ତି ଏମିତି କିଛି
ଗ୍ଯାରେଣ୍ଟି ନାହିଁ । ମାତ୍ର ସେ ବି
ନାଚାର । କାରଣ ଏମାନେ ହେଉ~
ଛନ୍ତି ଗୁଜବର ସମ୍ରାଟ । ଗୋଟିଏ
ତୁଣ୍ଡରେ ଛେଳିକୁ ମେଣ୍ଢା , ମେଣ୍ଢାକୁ
ବାଘ କରିଦେଇ ପାରନ୍ତି । ତେଣୁ
ତାଙ୍କର କିଛି କାମ କରିବାକୁ
ପଡେ ।</p>

<p>ମୁଁ ଅନେକ ଥର ଦେଖିଛି ଉକ୍ତ
ମହାଶଯ ତାଙ୍କର ଗୁରୁ ୟାହା
କହନ୍ତି , ସାଙ୍ଗେ ସାଙ୍ଗେ ନିଜସ୍ବ
ରୀତିରେ ସମର୍ଥନ କରନ୍ତି । ଗୁରୁ
ୟଦି କହନ୍ତି ଦିନକୁ ରାତି , ଭଦ୍ରବ୍ଯକ୍ତି
କହନ୍ତି ତାରା ଦେଖ ୟାଉଛନ୍ତି ।
ଏଇ ରୀତିରେ ପଣସକୁ ଆମ୍ବ , ଆମ୍ବକୁ
ଭେଣ୍ଡି , ମାଛକୁ ମାଂସ ଖରାପକୁ
ଭଲ , ଭଲକୁ ଅତି ଖରାପ କହି
ପକାନ୍ତି । ଏଇ ୟୂପକାଠିରେ
ଅନେକ ସରକାରୀ ଲୋକ କେବେ
କେବେ ବଳି ପଡନ୍ତି ।</p>

<p>ହଠାତ୍ ଆଉ ଏକ ନିର୍ମଳ
ପ୍ରଭାତରେ ମୁଁ ଲକ୍ଷ୍ଯ କଲି ଉକ୍ତ
ଭଦ୍ରବ୍ଯକ୍ତି ସ୍ଥାନୀଯ ବିଧାଯକଙ୍କ
ଘରେ ଆସ୍ଥା ଜମେଇଛନ୍ତି । ଏଥିରେ
ଆଶ୍ଚର୍ୟ୍ଯ ହେବାର କାରଣ ଥିଲା ।
କାରଣ ସମସ୍ତେ ଜାଣନ୍ତି ଏମ.ପି.
ଓ ଏମ.ଏଲ.ଏଙ୍କ ମଧ୍ଧରେ
ଅହିନକୁଳ ସମ୍ପର୍କ । ତେବେ ଦୁଇ
ବର୍ଷ ଆଗରୁ ସେ କ୍ଯାମ୍ପରେ ଥିବା
ଲୋକଟି ଏଇ କ୍ଯାମ୍ପକୁ କେମିତି
ଚାଲି ଆସିଲା ମୁଁ ଆଶ୍ଚର୍ୟ୍ଯ ହେଲି ।
ଏକୁଟିଆ ଦେଖି ପ୍ରଶ୍ନ କଲି -
: ଆପଣ ଏଇଠି କେମିତି ?
ସେ ହସିଲେ । କହିଲେ -
: ଆମେ ଆଜ୍ଞା ସବୁ ୟାତ୍ରାପାର୍ଟି
ଲୋକ । ପୂର୍ବରୁ ମୁହଁରେ ରଙ୍ଗ
ବୋଳି କେବେକେବେ ମୁଖା ପିନ୍ଧି
ପାର୍ଟ କରୁଥିଲୁ । ଏବେ ବଯସ
ନାହିଁ । ରୂପ ନାହିଁ । ତେଣୁ ୟାତ୍ରା
ଛାଡି ରାଜନୀତିରେ ମିଶିଛୁ । ଆପଣ
ତ ଜାଣନ୍ତି , ଆଜି ରାଜନୈତିକ
ପାର୍ଟି ଠିକ୍ ୟାତ୍ରା ପାର୍ଟି ଭଳି ।
ଗୋଟିଏ ପାର୍ଟି ବନ୍ଦ ହେଲେ
ଅଥବା କାଟତି ହରାଇଲେ
ଆମେ କ୍ଯାମ୍ପ ବଦଳାଉ । ଏଣୁ
ଆଶ୍ଚର୍ୟ୍ଯ ହେବାର କାରଣ କିଛି
ନାହିଁ ।
କେବେକେବେ ଏମିତି ବି ହୁଏ ।</p>

<p>ସମ୍ବାଦ ସମୀକ୍ଷା
ବର୍ଷକ ପାଁଇ ସବୁ ଥାନା ବନ୍ଦ
କରାୟାଉ
ତାମିଲନାଡୁର ଏକ ଯୁନିଅନ୍
ପକ୍ଷରୁ ବର୍ଷକ ପାଇଁ ରାଜ୍ଯର ସବୁ
ଥାନା ବନ୍ଦ ରଖି ସବୁ ପୋଲିସ୍
କର୍ମଚାରୀଙ୍କୁ ସବେତନ ବାଧ୍ଧତାମୂଳକ
ଛୁଟିରେ ପଠାଇବାକୁ ରାଜ୍ଯସର~
କାରଙ୍କୁ ପରାମର୍ଶ ଦିଆୟାଇଛି ।
ଗତ ଗୁରୁବାର ଦିନ ଅନୁଷ୍ଠିତ ଏକ
ସାଧାରଣ ସଭାରେ ଏ ସଂକ୍ରାନ୍ତରେ
ଏକ ପ୍ରସ୍ତାବ ଗୃହୀତ ହୋଇଛି ।</p>

<p>ସିଭିଲ୍ ଲିବର୍ଟି ଯୁନିଅନ୍
ପକ୍ଷରୁ ଥାନାଗୁଡିକ ବନ୍ଦ
କରାୟାଇ ସବୁ ପୋଲିସ୍ କର୍ମଚାରୀଙ୍କୁ
ବର୍ଷେ ବାଧ୍ଧତାମୂଳକ ଛୁଟିରେ
ପଠାଇଦେଲେ ରାଜ୍ଯର ଅପରାଧ ହାର
ଶତକଡା 10 ଭାଗ କମିୟିବ ବୋଲି
କୁହାୟାଇଛି ।</p>

<p>ଏହି ସଂଗଠନ ପକ୍ଷରୁ ସର~
କାରଙ୍କୁ ଦିଆୟାଇଥିବା ପରାମର୍ଶ
ଖୁବ୍ ତାତ୍ପର୍ୟ୍ଯପୂର୍ଣ୍ଣ । ଆଇନ୍~
ଶୃଙ୍ଖଳା ଦାଯିତ୍ବରେ ଥିବା
ପୋଲିସ୍ କର୍ମକର୍ତ୍ତାମାନେ ୟଦି
ଆଇନ୍ଶୃଙ୍ଖଳା ବ୍ଯାହତ କରନ୍ତି
ତେବେ ସେମାନଙ୍କୁ ରଖିବାରେ
ଆବଶ୍ଯକତା କ'ଣ ଅଛି ? ବିଭିନ୍ନ
ଅପରାଧରେ ସଂପୃକ୍ତ ଥିବା ବ୍ଯକ୍ତିମାନେ
ପୋଲିସ୍ କର୍ମକର୍ତ୍ତାଙ୍କ ସହ ସୁସମ୍ପର୍କ
ରଖୁଛନ୍ତି । ପୋଲିସ୍ କର୍ମକର୍ତ୍ତା~
ମାନେ ବି ସେମାନଙ୍କ ଠାରୁ ଲାଞ୍ଚ
ନେଇ ସେମାନଙ୍କ ଅପରାଧ ପ୍ରତି
ଆଖିବୁଜି ଦେଉଥିବା ଜଣାୟାଉଛି ।
ତେଣୁ ପରୀକ୍ଷାମୂଳକ ଭାବେ
ପୋଲିସ୍ କର୍ମକର୍ତ୍ତାଙ୍କୁ ବର୍ଷେ
ଛୁଟିରେ ପଠାଇଦେଲେ ଜନସାଧା~
ରଣ ନିଜକୁ ନିଜେ ସଚେତନ
ହେବେ ଓ ଲାଞ୍ଚ ମିଛ ଲୋପ ପାଇ~
ୟିବ । ୟେଉଁସବୁ ପୋଲିସ୍ ଅଫି~
ସରଙ୍କ ନାମରେ ଦୁର୍ନୀତି ଅଭିୟୋଗ
ରହିଛି ସେମାନଙ୍କ ବିରୁଦ୍ଧରେ ଓ
ସେମାନଙ୍କ ଜମାଥିବା ଧନ ସମ୍ପତ୍ତିର
ତଦନ୍ତ କରିବାକୁ ମଧ୍ଧ ଏହି ସଂସ୍ଥା
ପକ୍ଷରୁ ଦାବି କରାୟାଇଛି ।</p>

<p>କେବଳ ତାମିଲନାଡୁ ନୁହେଁ ,
ବିଭିନ୍ନ ରାଜ୍ଯରେ କେତେକ ପୋଲିସ୍
କର୍ମଚାରୀ ଏ ପ୍ରକାର ଅସାଧୁ ଉପାଯ
ଅବଲମ୍ବନ କରୁଥିବା ଦେଖାୟାଉଛି ।
ସେଥିପାଇଁ କଥାରେ କହନ୍ତି ଚାକିରି
କରିବ ପୁଲିସ , ମାଛ ଖାଇବ ଇଲିଶି ।
ପୋଲିସ୍ ଚାକିରି ଅର୍ଥ ଉପାର୍ଜନ
ପାଇଁ ପ୍ରକୃଷ୍ଟ କ୍ଷେତ୍ର ବୋଲି ଏବେ
ଧାରଣା ମଧ୍ଧ ହେଲାଣି । ଏ ପରି~
ପ୍ରେକ୍ଷୀରେ ସିଭିଲ୍ ଲିବର୍ଟି ଯୁନି~
ଅନର ଦାବି ୟୁକ୍ତିୟୁକ୍ତ ନୁହେଁ କି ?
 +&gt;*
               0 
</p></body></text></cesDoc>