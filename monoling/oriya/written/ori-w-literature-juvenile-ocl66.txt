<cesDoc id="ori-w-literature-juvenile-ocl66" lang="ori">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ori-w-literature-juvenile-ocl66.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ଜହ୍ନମାମୁ</h.title>
<h.author>ଚକ୍ରପାଣି</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">ocl66</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 1009.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-12</date></creation>
<langUsage>Oriya</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;କା'ର ସର୍ବନାଶ , କା'ର ପୁଷମାସ
ମଧ୍ଧପ୍ରଦେଶର ଗାବଡି ନାମକ ଗାଁରେ ଚଡକା ପଡି
ଗୋଟିଏ ଶିଶୁର ପ୍ରାଣ ଗଲା , କିନ୍ତୁ ପାଖରେ ଥିବା
ଗୋଟିଏ ଆଜନ୍ମ ବଧୀର ପିଲା ଶ୍ରବଣ ଶକ୍ତି ପାଇଗଲା ।</p>

<p>ଗନ୍ତାଘରିଆଙ୍କ କବର
ମିଶର ( ଇଜିପ୍୍ଟ ) ର ବିଖ୍ଯାତ ଫାରୋ ବା ସମ୍ରାଟ ତୁତାନ୍~
ଖାମୁନଙ୍କ ବିଷଯ ନିଶ୍ଚଯ ଶୁଣିଥିବ । ତାଙ୍କ ଗନ୍ତାଘରିଆ
ତଥା ପୁରୋହିତଙ୍କ ନାମ ଥିଲା ମାଯା । ମାଯା ଓ
ମାଯାଙ୍କ ସ୍ତ୍ରୀଙ୍କ କବରର ସନ୍ଧାନ ଏବେ ମିଳିଛି । ହଜାର
ହଜାର ବର୍ଷ ତଳର ସେ କବର ଭିତରେ ବହୁତ
ଧନରତ୍ନ ଥିବାର ସମ୍ଭାବନା ।</p>

<p>ଅତିଥି ବୃକ୍ଷର ଗାରିମା
ଭାରତରୁ ୟାଇ ମସ୍କୋର ବଟାନିକାଲ୍ ଗାର୍ଡନରେ
ରୋପିତ ଗୋଟିଏ ଦେବଦାରୁ ଚାରା ସମଯ କ୍ରମେ
ଶହେ ଫୁଟ ୟାଏ ବଢି ମସ୍କୋ ନଗରୀର ଉଚ୍ଚତମ ବୃକ୍ଷ
ହେବାର ଗୌରବ ଅର୍ଜନ କରିବ ବୋଲି ବିଶ୍ବାସ
କରାୟାଏ ।</p>

<p>ସାଇବେରିଆଠୁ ଭଲ ?
ଦ୍ବିତୀଯ ମହାୟୁଦ୍ଧ ଲଢିଥିବା ଜଣେ ଉକ୍ରାଇନ୍୍ବାସୀ
ସୈନିକ ଆଜିକୁ 42 ବର୍ଷ ହେଲା ନିଜ ଘରର ଗୋଟିଏ
କୋଣରେ ଲୁଚି ରହିଥିଲା । ଜର୍ମାନ୍୍ମାନେ ରୁଷ୍ ଭିତରକୁ
ପଶିଆସିବା ବେଳେ ସେ ଆତ୍ମସମର୍ପଣ କରିଥିଲା ।
ଜର୍ମାନୀର ପରାଜଯ ପରେ ତାର ଭଯ ହେଲା କି ରୁଷ୍
ସରକାର ତା'କୁ ତା'ର ଭୀରୁତା ପାଇଁ ଦଣ୍ଡ ହିସାବରେ
ସାଇବେରିଆ ତୁଷାର ମରୁକୁ ପଠାଇ ଦେବେ । ଏବେ
ତାର ସନ୍ଧାନ ମିଳିଛି ଓ ସେ ପଦାକୁ ଆସିଛି ।</p>

<p>ଏକଦା ଭରତ ନାମରେ ଜଣେ ରାଜା ଥିଲେ । ପରିଣତ ବଯସରେ ସେ ରାଜ୍ଯଭାର ପୁତ୍ରମାନଙ୍କୁ
ଦେଇ ତପସ୍ଯା ନିମନ୍ତେ ବଣକୁ ଚାଲିଗଲେ ।
ଦିନେ ସେ ନଇରେ ସ୍ନାନ କରୁଥାନ୍ତି , ଦେଖିଲେ , ଗୋଟିଏ ମା' ହରିଣ ତା' ଶାବକକୁ
ଛାଡି ମରିଗଲା । ଭରତ ଛୁଆ ହରିଣକୁ ନିଜ କୁଡିଆକୁ ନେଇୟାଇ ପାଳିଲେ । କ୍ରମେ ସେ
ହରିଣ ଛୁଆ ତାଙ୍କୁ ଏମିତି ମାଯାଡୋରିରେ ବାନ୍ଧିଲା ୟେ ସେ ଆଉ ସବୁ କଥା ଭୁଲିୟାଇ
ତାହାରି ୟତ୍ନ ନେବାରେ ମନ ଦେଲେ ।</p>

<p>ମୃତ୍ଯୁ ସମଯକୁ ସେ ସେହି ହରିଣ କଥା ଭାବୁଥିବାରୁ ପର ଜନ୍ମରେ ହରିଣ ହୋଇ ଜନ୍ମ
ନେଲେ । କିନ୍ତୁ ପୂର୍ବ ଜନ୍ମ କଥା ତାଙ୍କର ମନେ ଥାଏ । ସେ ଅନୁତାପ କରୁଥାନ୍ତି । ହରିଣ
ଜୀବନ ପରେ ସେ ଗୋଟିଏ ବ୍ରାହ୍ମଣ ପରିବାରରେ ଜନ୍ମ ନେଲେ । ଦୁଇ ପୂର୍ବଜନ୍ମ କଥା
ସୁମରି ସେ ସର୍ବଦା ବିତସ୍ପୃହ ରହୁଥାନ୍ତି । ଲୋକେ ତାଙ୍କୁ ବୋକା ଭାବି ଜଡ ଭରତ ବୋଲି
ଡାକୁଥାନ୍ତି । ସେ ଘରଛାଡି ଏଣେ ତେଣେ ବୁଲୁଥିବା ବେଳେ ଦିନେ ରାଜକର୍ମଚାରୀମାନେ
ତାଙ୍କୁ ଧରି ନେଇ ରାଜାଙ୍କ ସବାରି ବୋହିବା କାମରେ ଲଗାଇଲେ ।</p>

<p>ସେ କାଳେ କୌଣସି ପ୍ରାଣୀଙ୍କୁ ମାଡି ପକାଇବେ , ତେଣୁ ସାବଧାନରେ ଚାଲୁଥାନ୍ତି । ଫଳରେ
ସବାରୀର ଗତିରେ ବାଧା ପଡୁଥାଏ । ରାଗିୟାଇ ରାଜା ଥରେ ତାଙ୍କୁ ଗୋଇଠାଏ ପକାଇଲେ ।
ବାହକ କିନ୍ତୁ ଶାନ୍ତ କଣ୍ଠରେ ତାଙ୍କୁ ଶାସ୍ତ୍ର ଉପଦେଶ ଦେବାକୁ ଲାଗିଲେ । ରାଜା ବିସ୍ମଯରେ
ସବାରୀରୁ ଓହ୍ଲାଇ ତାଙ୍କୁ ପ୍ରଣାମ କଲେ । ରାଜା ତାଙ୍କୁ ବହୁ ଭାବରେ ସମ୍ମାନିତ କରିବାକୁ
ଚାହୁଁଥିଲେ । କିନ୍ତୁ ସେ ମୋଟେ ଅପେକ୍ଷା ନକରି ବଣକୁ ୟାଇ ତପସ୍ଯା କରି ମୁକ୍ତି ଲାଭ
କଲେ । //
ଚିକିତ୍ସା ରହସ୍ଯ
ପ୍ରାଯ ପଚାଶ ବର୍ଷ ତଳର କଥା ।
ମଙ୍ଗଳପୁର ଗାଁରେ ଜଣେ ଜମିଦାର
ଥିଲେ । ନାମ ବିଷ୍ଣୁ ଗୁପ୍ତ । ତାଙ୍କ ଜମିଦାରୀ
ସେପରି ବିଶେଷ ବଡ ନ ଥିଲା , ତେବେ
ଜମିଦାରୀ ସାଙ୍ଗକୁ ତାଙ୍କର କିଛି ବ୍ଯବସାଯ
ମଧ୍ଧ ଥିଲା ।</p>

<p>ମଙ୍ଗଳପୁର ଗାଁରେ ଜଣେ ବିଶିଷ୍ଟ ବୈଦ୍ଯ
ଥିଲେ । ବିଷ୍ଣୁ ଗୁପ୍ତଙ୍କର ଦେହ ପା' ଖରାପ
ହେଲେ ସେ ଦେଖୁଥିଲେ । ବୈଦ୍ଯଙ୍କର
ପରଲୋକ ହୋଇ ୟିବାରୁ ବିଷ୍ଣୁଗୁପ୍ତ ବହୁତ
ଦୁଃଖିତ ହେଲେ ।</p>

<p>ଏହା ପରେ ଥରେ ବିଷ୍ଣୁଗୁପ୍ତଙ୍କୁ ଜ୍ବର
ହେଲା । ଅଳ୍ପ ଦୂରରେ ଥିବା ଶ୍ଯାମପୁର ଗାଁର
ବୈଦ୍ଯ ଜଯରାମଙ୍କୁ ଡକା ହେଲା । ଜଯରାମ
ଆନନ୍ଦରେ ଆସି ବିଷ୍ଣୁଗୁପ୍ତଙ୍କୁ ଦେଖିଲେ ।
କାରଣ ଏଡେ ବିଶିଷ୍ଟ ବ୍ଯକ୍ତିଙ୍କୁ ଚିକିତ୍ସା
କରିବାର ସୁୟୋଗ ତାଙ୍କୁ ପୂର୍ବରୁ କେବେ ମିଳି
ନ ଥିଲା । ସେ ଆସି ଓଷଧ ଦେଲେ ଓ
ସବୁଦିନ ଆସିଲେ । ତିନି ଚାରିଦିନରେ ଗୁପ୍ତ
ପୁରାପୁରି ସୁସ୍ଥ ହୋଇଗଲେ । ସେ ବୈଦ୍ଯଙ୍କୁ
ଶହେଟଙ୍କା ଧରାଇ ଦେଲେ ।</p>

<p>ବାସ୍ତବିକ୍ ବିଷ୍ଣୁଗୁପ୍ତଙ୍କର ବିଶେଷ କିଛି
ହୋଇ ନ ଥିଲା । ସାମାନ୍ଯ ଜ୍ବର । ହୁଏତ
ସେ ବିନା ଚିକିତ୍ସାରେ ହିଁ ଆପଣାଛାଏଁ ଭଲ
ହେବାକୁ ପସନ୍ଦ କରିଥାନ୍ତେ । କିନ୍ତୁ ତାଙ୍କୁ
କୌଣସି ଜରୁରୀ କାମରେ ସହରକୁ ୟିବାକୁ
ପଡୁଥିବାରୁ ସେ ଓଷଧ ଖାଇ ଶୀଘ୍ର ଆରୋଗ୍ଯ
ହେବାକୁ ଚାହୁଁଥିଲେ । ବୈଦ୍ଯଙ୍କ ଔଷଧ ଭଲ
କାମ କରିବାରୁ ସେ ଖୁସି ହେଲେ । ଏତେ~
ଗୁଡିଏ ଟଙ୍କାଦେବାର ତାହାହିଁ ମୁଖ୍ଯ କାରଣ ।
ତେବେ ଅନ୍ଯ କାରଣ ବି ଥିଲା । ଗୁପ୍ତ
ଜାଣିଥିଲେ ୟେ ବୈଦ୍ଯ ଅଭାବୀ ଲୋକ ।</p>

<p>ବୈଦ୍ଯ ଆବାକ୍ ହୋଇଗଲେ । କାରଣ
ଏଭଳି ସାଧାରଣ ରୋଗର ଚିକିତ୍ସା ପାଇଁ
ତାଙ୍କୁ କେହି ପାଞ୍ଚ ଦଶ ଟଙ୍କାରୁ ବେଶି ଦେଉ
ନ ଥିଲେ । ସେ ଗୁପ୍ତଙ୍କୁ କି ଭାଷାରେ ଧନ୍ଯବାଦ
ଦେବେ ବୁଝିପାରିଲେ ନାହିଁ । ଆନନ୍ଦରେ
ଗଦ୍ ଗଦ୍ ହୋଇ ଚାଲିଗଲେ ।</p>

<p>ଏହାର ଦୁଇଦିନ ପରେ ସେ ଆସି
ହଠାତ୍ ପହଞ୍ଚିଗଲେ ଗୁପ୍ତଙ୍କ ଘରେ । ଗୁପ୍ତ
ପଚାରିଲେ , " କ'ଣ ବୈଦ୍ଯ ମହାଶଯ !
ଆମ ଗାଁ ରାସ୍ତା ଦେଇ ଆଉ କୁଆଡେ
ୟାଉଥିଲେ କି ? "
" ନା , ଖାସ୍ ଆପଣଙ୍କ ସହ କଥା ଅଛି
ବୋଲି ଆସିଛି । " ବୈଦ୍ଯ କହିଲେ ।
" ବେଶ୍ , ବେଶ୍ । କୁହନ୍ତୁ । " ଗୁପ୍ତ
ବୈଦ୍ଯଙ୍କୁ ବସିବାକୁ ଚୌକି ଦେଖାଇ ଦେଲେ ।
ବୈଦ୍ଯ କଣ୍ଠସ୍ବର ଧୀର କରି କହିଲେ ,
" ଗୋପନ କଥା । "</p>

<p>ଗୁପ୍ତ ଆଶ୍ଚର୍ୟ୍ଯ ହୋଇ ବୈଦ୍ଯକୁ ଗୋଟିଏ
ଭିତର କୋଠରୀକୁ ନେଇ ଗଲେ । ବୈଦ୍ଯ
ଥରେ ୟୋରରେ ନିଃଶ୍ବାସ ନେଇ କହିଲେ ,
" ମହାଶଯ , ଆପଣଙ୍କୁ ଚିକିତ୍ସା କଲାବେଳେ
ମୁଁ ହଠାତ୍ କେତୋଟି ଉପସର୍ଗ ଦେଖିଲି ।
ସେଥିରୁ ବୁଝିଲି ଆପଣଙ୍କ ଭିତରେ ଗୋଟିଏ
ଭୀଷଣ ବେମାରି ଦେଖା ଦେଇଛି । ତାହାର
ଚିକିତ୍ସା ନ ହେଲେ ... "
" ନ ହେଲେ ? " ଗୁପ୍ତ ପଚାରିଲେ ।
" ଆପଣ ଛଅମାସ ପରେ ଇହଧାମ
ପରିତ୍ଯାଗ କରିବେ । " ବୈଦ୍ଯ ସଙ୍କୋଚ ଛାଡି
କହିଲେ ।
" ୟଦି ସେ ରୋଗର ଚିକିତ୍ସା ଅଛି
ତେବେ ଚିକିତ୍ସା ନ ହେବା କିଆଁ ? " ଗୁପ୍ତ
ପଚାରିଲେ ।
" କଥା କ'ଣ କି ସେ ଚିକିତ୍ସା ଟିକିଏ
ବ୍ଯଯସାପେକ୍ଷ । ଅର୍ଥାତ୍ ମୋତେ ବହୁତ
ମୁଲ୍ଯବାନ୍ ଔଷଧ ତିଆରି କରିବାକୁ ପଡିବ ।
ସୁନା ଭସ୍ମ ସେ ଔଷଧରେ ପଡିବ । ହଜାର
ହଜାର ଟଙ୍କା ଖର୍ଚ୍ଚ ହେବ । "
" କେତେ ପ୍ରକାର ? "
" ସମୁଦାଯ ପ୍ରାଯ ବାର ହଜାର ପଡିବ । "</p>

<p>ଗୁପ୍ତ ମୁଣ୍ଡ ହଲାଇ କହିଲେ , " ବୈଦ୍ଯଜୀ ,
ମୋତେ ବଅସ ଆସି ସତୁରୀ ହେଲାଣି । ମୁଁ
ଜୀବନରେ ୟାହା କରିବାର କରିଛି । ଏଣିକି
ମୋ ପୁଅ ଜମିଦାରୀ ତଥା ବେପାର ବୁଝୁଛି ।
ମୁଁ ଭଗବତ୍ ଚିନ୍ତାରେ କିମ୍ବା ପୋଥି ପୁରାଣ
ପଢି ସମଯ ବିତାଉଛି । ମୋର ଆଉ ଦୀର୍ଘ
ଜୀବନ ଦରକାର କ'ଣ ? ଭଗବାନ୍ ୟଦି
ଚାହିଁବେ , ତେବେ ମୁଁ ପରଲୋକ ଗମନ ନ
କରିବି କିଆଁ ? ନା ବୈଦ୍ଯଜୀ , ମୁଁ ବାର
ହଜାର ଟଙ୍କା ଦେଇ ପାରିବି ନାହିଁ । "</p>

<p>ବୈଦ୍ଯ ଗମ୍ଭୀର ହୋଇ କହିଲେ , " ଆପଣ
ମୋ କଥାରେ ଗୁରୁତ୍ବ ଦେଉ ନାହାଁନ୍ତି । ମୁଁ
ଠିକ୍ କହୁଛି । ଚିକିତ୍ସା ନ ହେଲେ ଛଅ
ମାସରେ ଆପଣ ୟିବେ । ସଙ୍ଗେ ସଙ୍ଗେ ଚିକିତ୍ସା
ଆରମ୍ଭ ହେବା ଦରକାର । "</p>

<p>" ମୋର ଚିକିତ୍ସା ଦରକାର ନାହିଁ , ବୈଦ୍ଯ
ମହାଶଯ ! ଦୁନିଆରେ କେତେଜଣ ଏପରି
ଦୁର୍ମୁଲ୍ଯ ଚିକିତ୍ସାର ଆଶ୍ରଯ ନେଇ ପାରିବେ ? "
ଗୁପ୍ତ କହିଲେ ।</p>

<p>" କିନ୍ତୁ ଚିକିତ୍ସା ନ ହେଲେ ଆପଣଙ୍କ ବଞ୍ଚିବାର
ଆଶା ନାହିଁ ୟେ । "
" କହିଲି ପରା , ମୋର ବଞ୍ଚିବାର ଆଗ୍ରହ
ନାହିଁ । ତେବେ ଆପଣ ୟଦି ଚାହାଁନ୍ତି , ଚିକିତ୍ସା
କରି ପାରନ୍ତି ମୁଁ ବର୍ଷକ ପରେ ଟଙ୍କା ଦେବି ।
ବର୍ତ୍ତମାନ ମୋଠିଁ ଟଙ୍କା ନାହିଁ । " ଗୁପ୍ତ
କହିଲେ ।</p>

<p>" ବର୍ଷକ ପରେ ଆପଣ ବାର ହଜାର
ପାଇବେ କେଉଁଠାରୁ ? " ବୈଦ୍ଯ ପଚାରିଲେ ।
" ଆପଣଙ୍କ ପରି ଜଣେ ସାଧାରଣ ବୈଦ୍ଯ
ୟଦି ଛଅ ମାସରେ ବାର ହଜାର ଟଙ୍କା ଆଯ
କରି ପାରିବେ ବୋଲି ଭାବିଲେ , ମୋ ଭଳି
ଜଣେ ଜଣାଶୁଣା ଜମିଦାର ଓ ବ୍ଯବସାଯୀ
ବର୍ଷକରେ ବାର ହଜାର ୟୋଗାଡ କରି ପାରିବ
ନାହିଁ ? " ଗୁପ୍ତ ହସି ହସି ପଚାରିଲେ ।</p>

<p>ବୈଦ୍ଯ ଅପ୍ରତିଭ ହୋଇ ଟିକିଏ ଚିନ୍ତା କରି
କହିଲେ , " ଆଚ୍ଛା , ତାହାହିଁ ହେଉ । ମୁଁ
ଆଗାମୀ କାଲିଠୁ ଚିକିତ୍ସା ଆରମ୍ଭ କରୁଛି । "</p>

<p>ପରଦିନ ବୈଦ୍ଯ ଔଷଧ ଧରି ଆସି
ସେଠାରେ ପହଞ୍ଚିଲେ । ଗୁପ୍ତ ତାଙ୍କୁ ଧନ୍ଯବାଦ
ଦେଲେ । ପ୍ରତି ସପ୍ତାହରେ ବୈଦ୍ଯ କିଛି କିଛି
ଔଷଧ ନେଇ ଆସୁଥାନ୍ତି ଓ ତାହା କିପରି
ସେବନ କରିବାକୁ ହେବ , ସେକଥା କହି
ୟାଉଥାନ୍ତି । //
କିନ୍ତୁ ରାଜୁ କଥା ଅଲଗା । ସେ ଧ୍ବଂସାବ~
ଶେଷ ପ୍ରତି ତା'ର ଥାଏ ପ୍ରବଳ ଆକର୍ଷଣ ।
ସେ ଏକାକୀ ସେଠାରେ ୟାଇ ଦୀର୍ଘ ସମଯ
ଧରି ବିଚରଣ କରୁଥାଏ । ଦିନେ ହୁଏତ
ସେ ପଡିଥାନ୍ତା ବାଘ କବଳରେ ; କିନ୍ତୁ
ଠିକ୍ ସମଯରେ ସେ ଗୁମ୍ଫା ଭିତରେ ଲୁଚି
ୟାଇ ପାରିଥିଲା । ଦିନେ ହୁଏତ ବିଷଧର
ସର୍ପଟିଏ ଚୋଟ ମାରିଥାନ୍ତା ; କିନ୍ତୁ ଠିକ୍
ସମଯରେ ସେ ତା' ପାଦଟି ଅପସାରଣ
କରି ନେଇଥିଲା ।</p>

<p>ରାଜୁ ପିଲାବେଳେ ମା'କୁ ହରାଇ ଥିଲା ; ଆଉ
ତା' ବାପା ଥିଲେ ଜଣେ ଦକ୍ଷ ସ୍ଥପତି । ସେ
ସର୍ବଦା ନିଜ କାମରେ ବ୍ଯସ୍ତ ଥାନ୍ତି ।</p>

<p>ସେଦିନ ସୂର୍ୟ୍ଯାସ୍ତ ସମଯରେ ସବୁ କିପରି
ଅସ୍ବାଭାବିକ ଭାବରେ ସ୍ତବ୍୍ଧ ଲାଗୁଥାଏ ।
ଆକାଶ ଦିଶୁଥାଏ ଘନ ଗେରୁଆ ଓ ବିଷାଦପୂର୍ଣ୍ଣ ।
ସାରା ଉପତ୍ଯକାରୁ ସବୁ ପବନ ୟେପରି କେହି
ଶୋଷି ନେଇ ୟାଇଥିଲା । ଲୋକେ ଅଶ୍ବସ୍ତି
ଅନୁଭବ କରୁଥିଲେ ।</p>

<p>ହଠାତ୍ ଅରଣ୍ଯର ସବୁ ପଶୁପକ୍ଷୀ ରଡି
ଆରମ୍ଭ କରିଦେଲେ । ତା'ପରେ ଆସିଲା
ପ୍ରଚଣ୍ଡ ଉତ୍ତପ୍ତ ପବନ । କ୍ଷେତବାଡି ପଥ~
ଘାଟରେ ଲୋକେ ସନ୍ତ୍ରସ୍ଥ ଭାବରେ ଦୌଡିବାକୁ
ଲାଗିଲେ । ପର୍ବତମାଳା ଭିତରୁ ଶୁଭିଲା
କର୍ଣ୍ଣ-ବିଦାରକ ଶବ୍ଦ । ପରେ ପରେ ଘରଦ୍ବାର
ଦୋହଲିବାକୁ ଲାଗିଲେ । ଅବଶ୍ଯ ସେ ଭୂମିକମ୍ପ
ବେଶିକ୍ଷଣ ସ୍ଥାଯୀ ହେଲା ନାହିଁ କି ବିଶେଷ
ଅନିଷ୍ଟ କଲା ନାହିଁ , କିନ୍ତୁ ପର୍ବତର ମଧ୍ଧଭାଗରୁ
ପରିତ୍ଯକ୍ତ ପୁରୁଣା ପ୍ରାସାଦର ଏକ ଅଂଶ ଛିଟିକି
ଆସି ନୂଆ ପ୍ରାସାଦର ଶିଖର ଭାଙ୍ଗି ଦେଲା ।</p>

<p>ରାଜା ସଙ୍ଗେ ସଙ୍ଗେ ଅତି ଜ୍ଞାନୀ ମଣ୍ଡଳିର
ବୈଠକ ଡକାଇଲେ । " ୟଦି ପ୍ରାସାଦର ଶିଖର
ଭୂତଳଶାଯୀ ହେଲା , ତେବେ ତାକୁ ନିର୍ମାଣ
କରିଥିବା ମିସ୍ତ୍ରୀମାନଙ୍କ ମୁଣ୍ଡ ମଧ୍ଧ ଭୂତଳସାଯୀ
ହେବା ଦରକାର । " ସେ ଗର୍ଜନ କରି
କହିଲେ ଓ ପଚାରିଲେ , " ଠିକ୍ ନୁହେଁ ? "</p>

<p>" ହେ ରାଜକୂଳ-ଶାର୍ଦ୍ଦୁଳ , ସେମାନେ
ଦୁଇପୁରୁଷ ତଳର ମିସ୍ତ୍ରୀ । " ମନ୍ତ୍ରୀ କ୍ଷମାପ୍ରାର୍ଥୀ
ଭଳି କହିଲେ ।
" ହଇହେ ମନ୍ତ୍ରୀ ! ଆମ୍ଭେ ବୋକା ? "
ରାଜା ଗମ୍ଭୀର ହୋଇ ପ୍ରଶ୍ନ କଲେ ।
" ମହାରାଜ , ଆପଣ ଅତିଜ୍ଞାନୀ ମଣ୍ଡଳୀର
ସ୍ରଷ୍ଟା । ଆପଣ ତେଣୁ ଅତି ଅତି ଜ୍ଞାନୀ ।
ଆମ ସମସ୍ତିଙ୍କ ଜ୍ଞାନର ପରିମାଣ ଉଇହୁଙ୍କା
ଭଳି ହେଲେ ଆପଣଙ୍କର ଜ୍ଞାନ ପର୍ବତ ପ୍ରମାଣ । "</p>

<p>" ଠିକ୍ । ଦୁଇପୁରୁଷ ତଳେ ମରିୟାଇ
ମିସ୍ତ୍ରୀମାନେ ଆମ୍ଭକୁ ଠକି ପାରିବେ ନାହିଁ ।
ଜଣେ ମରିଗଲେ ତା'ର ଋଣ ପରିଶୋଧ
କରେ କିଏ ? " ଏ ପ୍ରଶ୍ନ କଲାବେଳେ ରାଜାଙ୍କ
ନଯନ ବେନି ଖଦ୍ଯୋତ ଭଳି ଚମକୁ ଥିଲା ।
ତାଙ୍କ ୟୁକ୍ତି ଅନୁସାରେ , ରାଜୁର ପଣି~
ଜେଜେବାପା ନୂଆ ପ୍ରାସାଦର ମୁଖ୍ଯ ମିସ୍ତ୍ରୀ
ହୋଇଥିବାରୁ ତାଙ୍କ ଉତ୍ତରାଧିକାରୀ ରୂପେ
ରାଜୁର ବାପା ଗିରଫ ହେଲେ ।</p>

<p>" ମୁଣ୍ଡ କାଟ । " ହେଲା ରାଜାଙ୍କ ସଂକ୍ଷିପ୍ତ
ହୁକୁମ୍ ।
" ମହାରାଜ , କାହିଁ ପ୍ରାସାଦର ଶିଖର , କାହିଁ
ଏ ଟିକି ମୁଣ୍ଡ । ବରଂ ଏହାକୁ ହଜାରେ ବର୍ଷ
ପାଇଁ କାରାରୁଦ୍ଧ କରାୟାଉ । " ସ୍ଥପତି ପ୍ରତି
ଦଯାବାନ୍ ଜଣେ ଅମାତ୍ଯ କହିଲେ ।</p>

<p>ହଜାରେ ବର୍ଷ କଥାଟା ରାଜାଙ୍କର ହୃଦଯ~
ଗ୍ରାହୀ ହେଲା । ସ୍ଥପତି କାରାରୁଦ୍ଧ ହେଲେ ।
ରାଜୁ କ୍ରୋଧ ଓ ଦୁଃଖରେ ଭାଙ୍ଗି ପଡିଲା ।
ବନ୍ଧୁମାନେ ତାକୁ ଧୈର୍ୟ୍ଯ ଧରିବାକୁ କହିଲେ ।
କିନ୍ତୁ କାରାଗାରରେ ମାତ୍ର ଏକମାସ ପରେ
ରାଜୁର ବାପା ମୃତ୍ଯୁ ବରଣ କଲେ ।</p>

<p>" ତା' ହେଲେ ତ ବନ୍ଦୀ ଆମ୍ଭେ ଦେଇଥିବା
ନଅଶହ ଅନେଶତ ବର୍ଷ ଏଗାର ମାସ କାଳ
ଦଣ୍ଡ ଫାଙ୍କି ଦେଲା । " ରାଜା ଅତିଜ୍ଞାନୀ ମାନଙ୍କ
ଆଗରେ ନିଜ ହତାଶା ବ୍ଯକ୍ତ କଲେ ।</p>

<p>" ମହାରାଜ , ୟଦି ପୁଅ ବାପାର ସମ୍ପତ୍ତି
ଭୋଗିବାକୁ ହକ୍୍ଦାର , ତେବେ ସେ ବାପାର
ଦଣ୍ଡ ନ ଭୋଗିବ କିଆଁ ? " କହିଲେ
ସର୍ବଦା ଦୁଇ ହାତ ଦଳି ହେଉଥିବା ଜଣେ
ଅମାତ୍ଯ ।</p>

<p>" ବାଃ , ବାଃ । ତମ ବୁଦ୍ଧି ବିଲୁଆ ଭଳି ।
ଏଣିକି ତମ ଉପାଧି ହେଲା ବିଲୁଆ ।
ହଁ , ଧରିଆଣ ବନ୍ଦୀର ପୁଅକୁ କର ତାକୁ
କାରାରୁଦ୍ଧ । " ରାଜା ନିର୍ଦ୍ଦେଶ ଦେଲେ ।</p>

<p>କିନ୍ତୁ ସିପାହୀମାନେ ରାଜୁଠିଁ ପହଞ୍ଚିବା
ପୂର୍ବରୁ ତାଠିଁ ଖବର ପହଞ୍ଚି ୟାଇଥିଲା ।
ସେ ଖସି ପଳାଇଲା ।</p>

<p>ଜହ୍ନରାତି । ରାଜୁ ୟାଇ ପର୍ବତର ମଧ୍ଧ
ଦେଶରେ ପୁରୁଣା ପ୍ରାସାଦର ଭଗ୍ନାବଶେଷ
ଭିତରେ ପହଞ୍ଚିଲା । ସେ ବଣବୁଦା ଓ ଶିଳା~
ଖଣ୍ଡମାନ ତା'ର ଅତି ପରିଚିତ । ସେ ଖଣ୍ଡିଏ
ସୁପରିଚିତ ସଫେଦ୍ ପଥର ଉପରେ ବସି
ପଡିଲା । ସେଇଠି ବସି ସେ କେତେଥର ଫୁଲ
ମାନଙ୍କୁ ଗୀତ ଶୁଣାଇଛି , ଗୁଣ୍ଡୁଚି , ନେଉଳ
ଓ ପ୍ରଜାପତିଙ୍କୁ ଗପ କହିଛି ।</p>

<p>ବାପା କଥା ମନେ ପଡି ତା ଆଖିକୁ ଲୁହ
ଆସିଲା । ଖାଲି ଝିଙ୍କାରି ଓ ଗୋଟିଏ ପେଚାର
ସାମଯିକ ଧ୍ବନି ଛାଡିଦେଲେ ଚତୁର୍ଦ୍ଦିଗ ଥିଲା
ନିସ୍ତବ୍୍ଧ । ବିତିଗଲା ଘଣ୍ଟାଏ ।</p>

<p>ହଠାତ୍ ତାର ମନେହେଲା , ପଛରେ
କିଛି ଅବା ଉଜ୍ଜଳ ଦିଶୁଛି । ସେ ବୁଲି
ଅନାଇଲା । ୟେଉଁଠି କେବଳ ଅନ୍ଧାର
ପାଷାଣ ଛଡା ସେ କେବେ କିଛି ଦେଖି
ନ ଥିଲା , ସେଠି ସେ ଜହ୍ନ ଆଲୁଅରେ ଦେଖିଲା
ଏକ ଅପୂର୍ବ ନାରୀ ମୂର୍ତ୍ତି । ବାରମ୍ବାର
ଅନାଇବା ପରେ ୟାଇ ସେ ନିଶ୍ଚିତ ହେଲା
ୟେ ଏହା ଦୃଷ୍ଟିଭ୍ରମ ନୁହେଁ ।</p>

<p>ସେ ଉଠିପଡି ସେ ଆଡେ ଅଗ୍ରସର ହେଲା ।
ପ୍ରତି ପଦପାତ ପରେ ସେ ଅନୁଭବ କରୁଥାଏ
ଅଧିକରୁ ଅଧିକ ବିସ୍ମଯ । କି ଦୀପ୍ତିମାନ୍
ସୌନ୍ଦର୍ୟ୍ଯ ! ମୂର୍ତ୍ତି ହାତରେ ଥିଲା ପଦ୍ମଫୁଲଟିଏ ।
ବେଶି ପାଖକୁ ଗଲେ ମୂର୍ତ୍ତି ଚମକି ପଡିବ ଓ
ପଦ୍ମଫୁଲଟି ତଳେ ପଡିୟିବ କି ? ରାଜୁ ମନରେ
ଦେଖା ୟାଉଥାଏ ଏହିଭଳି ଆଶଙ୍କା । ତା'ର
ମନେ ହେଉଥାଏ , ଆଖି ସତେ ଅବା ତା'ର
ଉଛୁଳି ଉଠୁଛି । କିଛି ଦେଖିବାରେ ଏଭଳି
ଆନନ୍ଦ , ଏଭଳି ଶିହରଣ ଥାଇପାରେ ବୋଲି
ସେ କେବେବି କଳ୍ପନା କରି ନ ଥିଲା ।</p>

<p>ମୁହୂର୍ତ୍ତକ ପରେ ତାର ମନେ ପଡିଗଲା ,
କନକ ଉପତ୍ଯକାର କିମ୍ବଦନ୍ତୀ - ଅତୀତର
ଶିଳ୍ପୀ-ରାଜା ଏକ ସୁବର୍ଣ୍ଣ ପ୍ରତିମା ଗଢିଥିବା
କଥା । ଭୂମିକମ୍ପ ନିଶ୍ଚେ ଲୁକ୍୍କାଯିତ ପ୍ରତିମାକୁ
ଦୃଶ୍ଯମାନ କରିଛି । ତା'ର ଆହୁରି ବି ମନେ
ହେଲା ସେ ୟେପରି ସେ ମୂର୍ତ୍ତିକୁ ପୂର୍ବରୁ
ଦେଖିଛି । ଆଶ୍ଚର୍ୟ୍ଯ ! କାରଣ ସେ ମୂର୍ତ୍ତିର
କୌଣସି ଛବି ଦେଖିବାର ପ୍ରଶ୍ନ ବି ନ ଥିଲା ।
ନୂଆ ରାଜବଂଶ ଅମଳରେ ରାଜ୍ଯରୁ ଶିଳ୍ପ ଅଙ୍କନ
ଇତ୍ଯାଦି ବିଦ୍ଯା ଲୁପ୍ତ ହୋଇଥିଲା ।</p>

<p>ସେ ୟାଇ ମୂର୍ତ୍ତିଟି ଛୁଇଁଲା । ଆନନ୍ଦ ଓ
କୃତଜ୍ଞତାରେ ଭରିଗଲା ତା'ର ହୃଦଯ । ସେ
ବୁଝିପାରିଲା ନାହିଁ କାହା ପାଇଁ ସେ କୃତଜ୍ଞ
ଅନୁଭବ କରୁଛି । ହୁଏତ ୟେଉଁ ଶକ୍ତି ତାକୁ
ବିପଦ ଭିତରୁ ଏ ସୁନ୍ଦର ପରିବେଶକୁ ନେଇ
ଆସିଲା , ତାହାରି ପାଇଁ ।</p>

<p>ରାଜୁ ସେ ପଦ୍ମ ସ୍ପର୍ଶ କଲା ଓ ତା'ପରେ
ପଦ୍ମ ଧାରଣ କରିଥିବା ସୁନାର ସୁନ୍ଦର ଆଙ୍ଗୁଳି
ଗୁଡିକୁ । ମୂର୍ତ୍ତିର ଏକ ଆଙ୍ଗୁଳିରୁ ମୁଦିଟିଏ
ଖସି ଆସି ତା' ନିଜ ଆଙ୍ଗୁଳିରେ ପିନ୍ଧି ହୋଇ~
ଗଲା । ତୀବ୍ର ପୁଲକ ଓ ବିସ୍ମଯରେ ସେ
ୟେପରି ଅଚେତ ହୋଇୟିବ । ସେ ବସି
ପଡିଲା । ତାର ମନେ ହେଲା ସେ ତନ୍ଦ୍ରାଚ୍ଛନ୍ନ
ହୋଇୟାଉଛି ।</p>

<p>" ଏ ମୂର୍ତ୍ତିରେ ଜୀବନ ସଞ୍ଚାର ହେବା
ଚାହଁ ? " ସେହି ଅବସ୍ଥାରେ ସେ ଶୁଣିଲା ଏହି
ପ୍ରଶ୍ନ । " ହଁ , ଚାହେଁ । " ସେ ଉତ୍ତର ଦେଲା ।
" ତେବେ ମୂର୍ତ୍ତି ୟେପରି ସୁରକ୍ଷିତ ରହିବ
ସେତକ ବ୍ଯବସ୍ଥା କରି ତମେ ଏ ଝରଣା ଟପି
ଚାଲିୟାଅ । ତମେ ପହଞ୍ଚିବ ଏକ ଭିନ୍ନ
ରାଜ୍ଯରେ । ଭେଟିବ ଦେବୀଙ୍କୁ ।
ଜୀବନ୍ଯାସର ରହସ୍ଯ ସେ କହିଦେବେ । ଆଉ ,
ଏ ମୂର୍ତ୍ତି ଜୀବନ ପାଇଲେ ତମେ ୟଦି ଏ
କନ୍ଯାକୁ ବିଭା ହେବାକୁ ଚାହଁ , ସେ ବର ବି
ମାଗି ନେଇପାର । " କହିଲା ସେ ଅପୂର୍ବ
କଣ୍ଠସ୍ବର । //
ସଚ୍ଚା ବିଦୂଷକ
କୋଶଳ ଦେଶର ରାଜା ଶୈଳପ୍ରତାପ
ଦିନେ ତାଙ୍କ ବିଦୂଷକ କିଶୋର
ଉପରେ ରାଗିଲେ । କାରଣ ରାଜା ୟାହା
କହନ୍ତି , କିଶୋର ତା'ର ବିପରୀତ କଥା
କହେ ।</p>

<p>" ତମେ ମୋ ଦରବାର ଛାଡି ବାହାରି
ୟାଅ ! ଏହିକ୍ଷଣି । ତମ ମୁହଁ ମୋତେ
ଦେଖାଇବ ନାହିଁ । " ରାଜା ଆଦେଶ ଦେଲେ ।
ବିଦୂଷକ , " ୟେ ଆଜ୍ଞା , " କହି ଅଭିବାଦନ
କରି ସଙ୍ଗେ ସଙ୍ଗେ ବାହାରିଗଲା ।</p>

<p>ସେଦିନ ଜଣେ ସାମନ୍ତ ରାଜାଙ୍କ ବାର୍ତ୍ତା
ନେଇ ଚନ୍ଦ୍ରଶେଖର ନାମକ ତାଙ୍କ ବିଦୂଷକ
କୋଶଳ ବାହାରକୁ ଆସିଥାନ୍ତି । ରାତିରେ
ରାଜା ମନ୍ତ୍ରୀଙ୍କୁ କହିଲେ , " ମୋର ମନେହେଲା
ଚନ୍ଦ୍ରଶେଖର ଖୁବ୍ ଭଲ ବିଦୂଷକ । ମୁଁ ଭାବୁଛି
କିଶୋର ସ୍ଥାନରେ ତାକୁ ନିୟୁକ୍ତି ଦେବି । "</p>

<p>ମନ୍ତ୍ରୀ କହିଲେ , " ମହାରାଜ , ଅତୀତରେ
ବି ଆପଣ କେତେଥର କିଶୋର ଉପରେ
ରାଗି ୟାଇଛନ୍ତି , କିନ୍ତୁ ପରେ ପୁଣି ଶାନ୍ତ
ହୋଇଛନ୍ତି । ଏଥର ବି ଏଡେ ଶୀଘ୍ର ତାକୁ
ପଦଚ୍ଯୁତ କରିବା ଉଚିତ୍ ହେବ ନାହିଁ । "</p>

<p>ରାଜା ଚୁପ୍ ରହିଲେ । ରାତାରାତି ମନ୍ତ୍ରୀ
କିଶୋରକୁ ଡାକି ରାଜାଙ୍କ ମନୋଭାବ ଜଣାଇ
ଦେଲେ । ସକାଳେ ରାଜା ଦରବାରକୁ ୟିବେ
ବୋଲି ବାହାରିଛନ୍ତି ଓ ମନ୍ତ୍ରୀଙ୍କ ସହ
କିଛି ଆଲୋଚନା କରୁଛନ୍ତି , ସେତିକିବେଳେ
କିଏ ଜଣେ ନିଜ ମୁହଁ ଖଣ୍ଡିଏ କନାରେ
ଲୁଚାଇ ତାଙ୍କୁ ଅଭିବାଦନ କଲା ।</p>

<p>ରାଜା ଆଶ୍ଚର୍ୟ୍ଯ ହୋଇ ପଚାରିଲେ , " ଏ
ଜୀବ କିଏ ? " ବିଦୂଷକ କିଶୋର କହିଲା ,
" ମହାରାଜ ! ମୁଁ ଆପଣଙ୍କ ଆଦେଶ ମାନି
ଚଳୁଛି । ମୋ ମୁହଁ ଦେଖାଉ ନାହିଁ । "</p>

<p>ରାଜା ହସ ଚାପି କହିଲେ , " ହୁଁ ! କିନ୍ତୁ
ମୁଁ ଭାବୁଛି ତମ ସ୍ଥାନରେ ବିଦୂଷକ ଚନ୍ଦ୍ର~
ଶେଖରକୁ ନିୟୁକ୍ତ କରିବି । "</p>

<p>" ମହାରାଜ ! ସେଥିରେ ଅସୁବିଧା କ'ଣ ?
ସେ ତ ଆପଣଙ୍କ ସାମନ୍ତ ରାଜାଙ୍କ ଦରବାରର
ଅମାତ୍ଯ । ଆପଣଙ୍କ ଅଧୀନ । " ବିଦୂଷକ
କିଶୋର କହିଲା ।
ରାଜା ପଚାରିଲେ , " ସେ ଭଲ ବିଦୂଷକ ଟି ? "
" ଖୁବ୍ ଭଲ । " କିଶୋର କହିଲା ।
ରାଜା ଆହୁରି ଆଶ୍ଚର୍ୟ୍ଯ ହୋଇ ପଚାରିଲେ ,
" ତମେ ଏକଥା କହୁଛ ? "
" ମହାରାଜ ! ଆପଣ ୟାହା କହିବେ ମୁଁ
ତା'ର ବିପରୀତ କଥା କହିବାରୁ ଆପଣ
ଅସନ୍ତୁଷ୍ଟ । ତେଣୁ ମୁଁ ସର୍ବଦା ଆପଣଙ୍କୁ
ସମର୍ଥନ କରିବି ବୋଲି ଠିକ୍ କରିଛି । "
କିଶୋର କହିଲା ।</p>

<p>ରାଜା ଟିକିଏ ଭାବି କହିଲେ , " ଦେଖ ,
କିଶୋର , ତମେ ୟଦି ଗୋଟିଏ ସର୍ତ୍ତ ପୂରଣ
କରିପାରିବ , ତେବେ ତମ ପଦ ରହିବ ।
ବର୍ତ୍ତମାନ ଦରବାରରେ ତମେ ନିଜକୁ ନ୍ଯୁନ
ନ କରି ଚନ୍ଦ୍ରଶେଖରକୁ ଶ୍ରେଷ୍ଠ ବିଦୂଷକ
ବୋଲି କହିପାରିବ ? ହଁ , ତମେ ମୁହଁ ଦେଖାଇ
ପାର । " " ୟେ ଆଜ୍ଞା , ମହାରାଜ । " କିଶୋର
କହିଲା ।</p>

<p>ରାଜା ଦରବାରକୁ ଗଲେ । କିଛି ସମଯ
ପରେ କିଶୋର ସେଠାକୁ ୟାଇ କହିଲା ,
" ମହାରାଜ , ଆପଣଙ୍କୁ ଗୋଟିଏ ପରାମର୍ଶ
ଦେବାର ଅଛି । ୟଦି କେବେ ଆପଣଙ୍କର
ଏପରି ଦୁର୍ଭାଗ୍ଯ ହେବ ୟେ ଆପଣ ମୋ ଭଳି
ୟୋଗ୍ଯ ବିଦୂଷକର ସେବାରୁ ବଞ୍ଚିତ ହେବେ ,
ତେବେ ଚନ୍ଦ୍ରଶେଖରଙ୍କୁ ବିଦୂଷକ ପଦରେ
ନିୟୁକ୍ତ କରିବେ । କାରଣ ମୁଁ ୟେତେ
ବିଦୂଷକଙ୍କୁ ଜାଣିଛି , ସେମାନଙ୍କ ଭିତରେ
ସେ ଶ୍ରେଷ୍ଠ । "</p>

<p>ରାଜା କିଶୋରର ବାକ୍୍ଚାତୁରୀରେ ଖୁସି
ହୋଇ ତାକୁ ପୁରସ୍କାର ଦେଲେ ।
 +&gt;*
               0 
</p></body></text></cesDoc>