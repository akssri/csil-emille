<cesDoc id="ori-w-literature-bio-obr23" lang="ori">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ori-w-literature-bio-obr23.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ସତ୍ଯବାଦୀ</h.title>
<h.author>ଉଦଯନାଥ..</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - book.</publisher>
<pubDate>1985</pubDate>
</imprint>
<idno type="CIIL code">obr23</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 1001.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-12</date></creation>
<langUsage>Oriya</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;ଆଜିକାଲି ଅନେକ ସତ୍ଯବାଦୀ ସ୍କୁଲ କଥା କହି ଥାଆନ୍ତି । ଲେଖା~
ଲେଖିରେ , ବକ୍ତୃତାରେ ଖୁବ୍ ପ୍ରଶଂସା କରନ୍ତି ।</p>

<p>ଏହି ସ୍କୁଲ ପ୍ରତିଷ୍ଠା କରିଥିଲେ ପଣ୍ଡିତ ଗୋପବନ୍ଧୁ ଦାସ 1911
ମସିହାରେ । ପଣ୍ଡିତ ନୀଳକଣ୍ଠ ଦାସ , ପଣ୍ଡିତ ଗୋଦାବରୀଶ ମିଶ୍ର , ପଣ୍ଡିତ
କୃପାସିନ୍ଧୁ ମିଶ୍ର , ପଣ୍ଡିତ ଲିଙ୍ଗରାଜ ମିଶ୍ର , ଆଚାର୍ୟ୍ଯ ହରିହର ଦାସ , ଶ୍ରୀୟୁକ୍ତ
ନନ୍ଦ କିଶୋର ଦାସ ପ୍ରମୁଖ ଓଡିଶାର ଜଣାଶୁଣା ପଣ୍ଡିତ ଜ୍ଞାନୀ ଦେଶଭକ୍ତ
ଲୋକମାନେ ଶିକ୍ଷକ ଥିଲେ । ଓଡିଶାର ସବୁ ଅଞ୍ଚଳରୁ ବହୁ ଦୂରଦୂରାନ୍ତରୁ
ପିଲାମାନେ ଆସି ପଢୁଥିଲେ । ହସି ଖେଳି ଆନନ୍ଦ ଉଲ୍ଲାସରେ ଦିନ କାଟୁଥିଲେ ।</p>

<p>ଭାରତର ଚାରିଆଡେ ଏହି ସ୍କୁଲର ଖ୍ଯାତି ବଢିଗଲା । ସେ ସମଯର ବଡ
ବଡ ଶିକ୍ଷାବିତ୍ ମାନ୍ଯଗଣ୍ଯ ବ୍ଯକ୍ତି ସ୍କୁଲ ଦେଖିବାକୁ ଆଗ୍ରହରେ ଧାଇଁ ଆସୁଥିଲେ ।
ସ୍କୁଲ ପରିଚାଳନା କାର୍ୟ୍ଯ ଦେଖି ପ୍ରଶଂସା କରି ଧନ୍ଯ ଧନ୍ଯ କହୁଥିଲେ ।</p>

<p>ତତ୍କାଳୀନ ବିହାର-ଓଡିଶାର ଇଂରେଜ ଶାସକ ସାର ଏଡଵାଡ ଗେଟ
ଶିକ୍ଷା ବିଭାଗର ଡିରେକ୍ଟର ଡିଉକ , କଲିକତା ବିଶ୍ବବିଦ୍ଯାଳଯର ସ୍ବନାମଧନ୍ଯ
କୁଳପତି ସାର ଆଶୁତୋଷ ମୁଖାର୍ଜୀ ( ଦେଶବାସୀ ତାଙ୍କୁ ଆଖ୍ଯା ଦେଇଥିଲେ )
ଅଧ୍ଧାପକ ୟୋଗେଶ ଚନ୍ଦ୍ର ରାଯ " ବିଦ୍ଯାନିଧି "
( ଓଡିଶାର ପ୍ରଖ୍ଯାତ ଜ୍ଯୋତିଷୀ ମହାମହୋପାଧ୍ଧାଯ ଚନ୍ଦ୍ରଶେଖର ସାମନ୍ତ ସିଂହ
ଓ ତାଙ୍କ ପ୍ରଣୀତ ପ୍ରସିଦ୍ଧ ` ସିଦ୍ଧାନ୍ତ ଦର୍ପଣ ' ପୁସ୍ତକକୁ ୟିଏ ଲୋକ ଲୋଚନକୁ ଆଣିଥିଲେ )
ପ୍ରମୁଖ ମନୀଷିମାନେ ଏହି ସ୍କୁଲର ପ୍ରଶଂସାରେ ଶତମୁଖ ହୋଇ ଉଠିଥିଲେ ।</p>

<p>1921 ମସିହାରେ ମହାତ୍ମା ଗାନ୍ଧୀ ଅସହୟୋଗ ଆନ୍ଦୋଳନ ଆରମ୍ଭ କଲେ
ପରାଧୀନ ଭାରତକୁ ସ୍ବାଧୀନ କରିବା ପାଇଁ । ଏ ଆନ୍ଦୋଳନର ପ୍ରଥମ ସୂତ୍ର
ଥିଲା ବିଦେଶୀ ଶାସନ ତଥା ଇଁରେଜ ସରକାରଙ୍କଠାରୁ ସମସ୍ତ ସଂପର୍କ ଛିନ୍ନ କରିବା ।
ସେ ଶାସନକୁ କେହି କୌଣସି ପ୍ରକାର ସହୟୋଗ କରିବେ ନାହିଁ ।</p>

<p>ପଣ୍ଡିତ ଗୋପବନ୍ଧୁ ଗାନ୍ଧୀଜୀଙ୍କର ଜଣେ ପଟ୍ଟଶିଷ୍ଯ ହୋଇଗଲେ ।
ଓଡିଶାରେ ସ୍ବାଧୀନତା ଆନ୍ଦୋଳନର ମୁଖ୍ଯ ହେଲେ ସେ ବିହାର-ଓଡିଶା କାଉ~
ନସିଲର ମାନ୍ଯବର ସଭ୍ଯ ଥିଲେ । ସେ ପଦ ତ୍ଯାଗ କଲେ । ସଙ୍ଗେ ସଙ୍ଗେ
ତାଙ୍କ ପ୍ରିଯ ସ୍କୁଲଟିକୁ ମଧ୍ଧ ସେ ଏହା ଦେହରେ ମିଶାଇ ଦେଲେ ।</p>

<p>ଅସହୟୋଗ ଆନ୍ଦୋଳନରେ ଭାରତର ବହୁ ଧନୀ , ମାନୀ ନେତା ଓ
ବୁଦ୍ଧିଜୀବୀଙ୍କୁ ସର୍ବସ୍ବ ତ୍ଯାଗ କରିବାକୁ ପଡିଥିଲା । ଧନସଂପତ୍ତି ହରାଇଲେ ,
ଶାରୀରିକ ୟନ୍ତ୍ରଣା ଭୋଗିଲେ , ଜେଲ ଦଣ୍ଡ ପାଇଲେ । ପଣ୍ଡିତ ଗୋପବନ୍ଧୁ
ମଧ୍ଧ ତାଙ୍କୁ ଅତି ଆଦରର ମାନସ ସନ୍ତାନ ସତ୍ଯବାଦୀ ସ୍କୁଲକୁ ଏହି ୟଜ୍ଞରେ
ଆହୁତି ଦେବାକୁ କୁଣ୍ଠିତ ହୋଇ ନ ଥିଲେ ।</p>

<p>ସ୍କୁଲର ଉନ୍ନତି ଦେଖି ବିହାର-ଓଡିଶା ସରକାର ଖୁସି ହୋଇ ୟାଇଥିଲେ ।
ଏହାକୁ କଲେଜରେ ପରିଣତ କରିବା ପାଇଁ ପ୍ରସ୍ତାବ ଦେଲେ । ସେଥିପାଇଁ
ତିରିଶ ହଜାର ଟଙ୍କା ମଞ୍ଜୁର କରିଥିଲେ । ଏହା କଲେଜରେ ପରିଣତ
ହୋଇଥିଲେ ଓଡିଶାର ଦ୍ବିତୀଯ ଉଚ୍ଚ ଶିକ୍ଷାନୁଷ୍ଠାନ ହୋଇଥାନ୍ତା । ମାତ୍ର
ଗୋପବନ୍ଧୁ ସେଥିରେ ଭୁଲିଲେ ନାହିଁ । ବିଶ୍ବବିଦ୍ଯାଳଯ ସଂପର୍କ ଛିନ୍ନ କରି~
ଦେଲେ । ସ୍କୁଲ ହୋଇଗଲା ଜାତୀଯ ସ୍କୁଲ । ନାମ ଦିଆଗଲା ସତ୍ଯବାଦୀ ବିହାର ।
ବିହାରର ଅର୍ଥ ବୌଦ୍ଧ ମଠ ବା ଋଷି ଆଶ୍ରମ । ସେଠାରେ ପିଲାମାନେ
ସବୁ ପ୍ରକାର ଜ୍ଞାନ ଆହରଣ କରନ୍ତି ଅତି ସରଳ ଭାବରେ ମିଳିମିଶି
ଚଳନ୍ତି । ମନରେ କୌଣସି ପ୍ରକାର ଭେଦଭାବ ରଖିବେ ନାହିଁ , କାହାକୁ ହିଂସା
କରିବେ ନାହିଁ , ପରର ଉପକାର କରିବେ , ଦୀନ ଦୁଃଖୀ ରୋଗୀଙ୍କ ସେବା
କରିବେ , ଭବିଷ୍ଯତରେ ଚଳିବା ପାଇଁ ନାନା ପ୍ରକାର କାମଧନ୍ଦା ଶିଖିବେ ।
ପାଠପଢା ସଙ୍ଗେ ସଙ୍ଗେ ଏ ସବୁରେ ତାଲିମ ପାଆନ୍ତି । ପାଠ ପଢି ସାରିଲେ
ଚାକିରି କରିବି , ଅନ୍ଯର ହୁକୁମ ମାନି ଚଳିବି - ଏ ଭାବନା କାହାରି ନ ଥାଏ ।</p>

<p>ବିଶ୍ବବିଦ୍ଯାଳଯ ସଂପର୍କ ତୁଟିଗଲା । ପିଲାମାନେ ଆଉ ପରୀକ୍ଷା
ଦେବେ ନାହିଁ । ପରୀକ୍ଷା ନ ଦେଲେ କଲେଜରେ ପଢି ପାରିବେ ନାହିଁ । କୌଣସି
ଡିଗ୍ରୀ ମିଳିବ ନାହିଁ । ଡିଗ୍ରୀଟିଏ ନ ଥିଲେ ଚାକିରୀଟାଏ ବା ମିଳିବ କିପରି ?</p>

<p>ଆର୍ୟ୍ଯ ଭାରତର ଲକ୍ଷ ଥିଲା ପିଲାମାନଙ୍କୁ ଶିକ୍ଷା ଦିଆୟିବ ଜ୍ଞାନାର୍ଜନ ପାଇଁ ।
ଶିକ୍ଷା ସେ ସମଯରେ ବ୍ଯବସାଯ ନ ଥିଲା । ଇଂରେଜ ଶାସନରେ ଶିକ୍ଷାର ସେ
ଉଦ୍ଦେଶ୍ଯ ସଂପୂର୍ଣ୍ଣ ବଦଳିଗଲା । ଏହା ହୋଇଗଲା ବ୍ଯବସାଯିକ ଅର୍ଥକାରୀ ।
ପିଲା ପାଠ ପଢିବ କେବଳ ଗୋଟିଏ ଚାକିରି କରି ଟଙ୍କା ରୋଜଗାର କରିବ ।
ତେଣୁ ଅଭିଭାବକମାନେ ମୂଳଧନ ସ୍ବରୂପ ଶିକ୍ଷାଦାତାଙ୍କ ଉଦ୍ଦେଶ୍ଯରେ ଅର୍ଥ
ବ୍ଯଯ କରିବେ । ପିଲା ପାଠ ପଢି ସେହି ମୂଳଧନକୁ ବଢାଇ ଅର୍ଥ ସଂଗ୍ରହ
କରିବ । ଜ୍ଞାନାର୍ଜନ ହୋଇଗଲା ଗୌଣ । ତେଣୁ ପିଲାର ଲକ୍ଷ ରହିଲା କେବଳ
ଗୋଟାଏ ଡିଗ୍ରୀ ହାସଲ କରିବା । ଡିଗ୍ରୀ ହିଁ ଆଣିଦେବ ଚାକିରି । ସେହି ଡିଗ୍ରୀ
ସଂଗ୍ରହ ପାଇଁ ସେ ପରୀକ୍ଷାରେ ନାନା ଅସାଧୁ ଉପାଯର ଆଶ୍ରଯ ନେବାକୁ କୁଣ୍ଠିତ
ହେଲା ନାହିଁ । ଶେଷରେ ଡିଗ୍ରୀ କାଗଜ ଖଣ୍ଡେ ମିଳିଲା ; ମାତ୍ର ଜ୍ଞାନ ଗାରିମା
ବୁଦ୍ଧି ବିବେକ , ବିଚାର ଶକ୍ତି , ମୋଟ ଉପରେ ମାନବିକତା ଘର ଶୂନ୍ଯ ପଡିଗଲା । //
ଆମ ଭିତରୁ ଜଣେ ଚୁଡାକଦମ୍ବାର ଅତି ବଡ ଭକ୍ତ ହୋଇ ଉଠିଥିଲେ ।
ବାରମ୍ବାର ଆମ ଆଗରେ ତାର ବହୁଳ ପ୍ରଶଂସା କରି ସନ୍ତୋଷ ଲାଭ କରୁଥିଲେ ।
ଏବଂ ଆମ୍ଭମାନଙ୍କୁ ପ୍ରଲୋଭିତ କରାଉଥିଲେ । ବର୍ତ୍ତମାନ ସେ କେବଳ ଆମ
ଓଡିଶା ନୁହେଁ , ଭାରତର ମଧ୍ଧ ଜଣେ ଖ୍ଯାତିସଂପନ୍ନ ତୁଙ୍ଗ ବ୍ଯକ୍ତି । ପୃଥିବୀର
ପ୍ରାଚ୍ଯ ପାଶ୍ଚାତ୍ଯ ସବୁ ଅଞ୍ଚଳରେ ବାରମ୍ବାର ଆତିଥ୍ଯ ଗ୍ରହଣ କରି ବଡ ବଡ
ହୋଟେଲରେ ନାନା ସ୍ବାଦୁର ମୁଖରୋଚକ ଖାଦ୍ଯ ଖାଇବାର ସୁୟୋଗ
ପାଇଛନ୍ତି । ତଥାପି ସାକ୍ଷୀଗୋପାଳର ଚୁଡାକଦମ୍ବା ନାମପଡିଲେ ଏବେ
ମଧ୍ଧ ତାଙ୍କ ଜିଭ ଆର୍ଦ୍ର ହୋଇୟାଇଥିବ ।</p>

<p>ଅଧିକାଂଶ ପିଲା ସେହି ଚୁଡାକଦମ୍ବା ଖାଆନ୍ତି । ଫଳରେ କାଛୁକୁଣ୍ଡିଆ
ରୋଗ ଭୋଗିବାକୁ ପଡିଥାଏ । ସେହି ରୋଗରେ ଏକ ସମଯରେ ବହୁସଂଖ୍ଯକ
ପିଲା ଆକ୍ରାନ୍ତ ହୁଅନ୍ତି ଓ ଅନେକ ଦିନ ଧରି ଘାଣ୍ଟି ହୁଅନ୍ତି । ସେତେବେଳେ
ଆଚାର୍ୟ୍ଯ ମହାଶଯଙ୍କ ପରିଶ୍ରମ ବହୁତ ବଢିୟାଏ ।</p>

<p>ସକାଳେ ଖାଇବା ହଲରେ ପ୍ରାଯ କେହି ନ ଥାନ୍ତି । ସେହି ସମଯରେ
ଆଚାର୍ୟ୍ଯ ମହାଶଯ ନିଜେ କାଛୁକୁଣ୍ଡିଆ ରୋଗୀଙ୍କ ଚିକିତ୍ସା କରି ଥାଆନ୍ତି ।</p>

<p>ସେବକ ପ୍ରତ୍ଯୁଷରୁ ଗୋଟିଏ ବଡ ପାତ୍ରରେ ନିମ୍ବଛେଲି ଓ ନିମ୍ବପତ୍ର
ସିଝା ଗରମ ପାଣି ରଖିଥାଏ । ଆଗରୁ ସିଝା ଚିରେଇତା ପାଣି ଓ ଡାକ୍ତରଖାନାରୁ
ମଲମ ଆସି ରହିଥାଏ । ଚିକିତ୍ସକ ଜଣଜଣଙ୍କୁ ଡାକି ନିମ୍ବସିଝା ଗରମ ପାଣିରେ
ତା ହାତ ଗୋଡର ଘା ଧୋଇ ଦିଅନ୍ତି । ଖଣ୍ଡିଏ ବାଉଁଶ ପାତିଆରେ କାଛୁ~
କୁଣ୍ଡିଆର ଉପର ଚମ ଖଣ୍ଡିଆ କରି ପୂଜ ରକ୍ତ ଚିପି ବାହାର କରିଦିଅନ୍ତି ।</p>

<p>ଘରେ ମା ଏହି କାମ କରିବାକୁ ବସିଲେ ପିଲା ହୁଏତ ଛାଟିପିଟି ହୋଇ
ପଳାଇୟିବାକୁ ଚେଷ୍ଟା କରିବ । କେହି ମାଡିବସି ଧରିଲେ କାନ୍ଦ ଗର୍ଜନରେ ସେ
ଅଞ୍ଚଳ କମ୍ପି ଉଠିବ । ମାତ୍ର ଏ ମା ନୁହଁନ୍ତି , ରାଗିବେ କି ଆଖି ଦେଖେଇବେ ।
ଏ ୟେ ପିଲାମାନଙ୍କର ଅତି ଆପଣାର ଗୁରୁଜନ ଆଚାର୍ୟ୍ଯ ମହାଶଯ । ଛାଟିପିଟି
ହୋଇ ପଳାଇୟିବାର କଥା କଳ୍ପନାକୁ ଆସିବ କିପରି ? ୟେତେ କଷ୍ଟ ହେଉ
ପଛକେ ମୁହଁ ବୁଲାଇ ଓଠ କାମୁଡି ସହିବାକୁ ପଡିବ । ପୁଣି କାନ୍ଦ । ଅତି
ଲାଜ କଥା । ଆଚାର୍ୟ୍ଯ ମହାଶଯ ତ ହସିବେ , ସାଙ୍ଗସାଥୀଙ୍କ ମଧ୍ଧରେ ପ୍ରଚାର
ହୋଇଗଲେ ମୁହଁ ଉପରକୁ ଟେକି ଚାଲିବା ସମ୍ଭବ ହେବ ନାହିଁ କିଛି ଦିନ ୟାଏ ।</p>

<p>ଘାଆ ସଂପୂର୍ଣ୍ଣ ଶୁଖି ୟିବା ପର୍ୟ୍ଯନ୍ତ ରୋଗୀର ଏହି ଦଶା । ଅବଶ୍ଯ
ବେଶିଦିନ ଏ କଷ୍ଟ ଭୋଗିବାକୁ ପଡେ ନାହିଁ । ଚିକିତ୍ସକଙ୍କ ସସ୍ନେହ ସହାନୁଭୂତି~
ଭରା ଚିକିତ୍ସା କୌଶଳରେ ଅଳ୍ପ ଦିନ ମଧ୍ଧରେ ରୋଗୀ ରୋଗମୁକ୍ତ ହୋଇୟାଏ ।</p>

<p>ତା ପରେ ଆସନ୍ତି ନୂଆ ନୂଆ ରୋଗୀ । ଆଚାର୍ୟ୍ଯ ମହାଶଯଙ୍କ ଚିକିତ୍ସା
ସରେ ନାହିଁ - ଲାଗି ରହିଥାଏ ।</p>

<p>ସରସ୍ବତୀ ପୂଜାର ପ୍ରାଯ ଦୁଇ ସପ୍ତାହ ଆଗରୁ ଅନ୍ତେବାସୀଙ୍କର
ବୈଠକ ବସେ । ପୂଜା ପରିଚାଳନା ପାଇଁ ଗୋଟିଏ କମିଟି ଗଢାୟାଏ ।
ସେଥିରେ ସଭାପତି , ଉପସଭାପତି , ସଂପାଦକ , କୋଷାଧ୍ଧକ୍ଷ ନିର୍ବାଚିତ
ହୁଅନ୍ତି । ଶିକ୍ଷକମାନଙ୍କ ସହୟୋଗ ଲୋଡାୟାଏ ନାହିଁ ।</p>

<p>ଦଶମ ଶ୍ରେଣୀର ଓ ତଳଶ୍ରେଣୀର ଆଗ୍ରହୀ ପିଲାମାନଙ୍କୁ ସ୍ବେଚ୍ଛାସେବକ ରୂପେ
ବଛାୟାଏ । ଏମାନେ ଚାନ୍ଦା ସଂଗ୍ରହରେ ସାହାୟ୍ଯ କରିବେ , ମଣ୍ଡପ ସଜାଇବେ ,
ବଜାରରୁ ସଉଦାପତ୍ର ଆଣିବେ , ରନ୍ଧାରନ୍ଧିରେ କାମ କରିବେ । ବାଳଭୋଗ
ଓ ଭୋଜିରେ ଆଯୋଜନ ବି କରିବେ । ଶିକ୍ଷକମାନଙ୍କ ସମେତ ଆମନ୍ତ୍ରିତ ବ୍ଯକ୍ତିଙ୍କୁ
ଚର୍ଚ୍ଚା କରିବେ । ଅତିଥିଙ୍କ ମନୋରଞ୍ଜନ ପାଇଁ ସଙ୍ଗୀତ ଆସର ବା ଏକାଙ୍କିକା
ଅଭିନଯର ଆଯୋଜନ ସମଯ ସମଯରେ ବି କରିବାକୁ ପଡିଥାଏ ।</p>

<p>କମିଟି ଗଢା ହୋଇଗଲା ପରେ ଚାନ୍ଦା ଆଦାଯ ଧୁମଧାମ ଚାଲେ । ପୂର୍ବରୁ
ଗୋପନ ଆଲୋଚନା ପରେ ସଂପାଦକଙ୍କ ସହ ଚାରି ପାଞ୍ଚ ଜଣ କୁହାଳିଆ ସ୍ବେଚ୍ଛା~
ସେବକ ବାହାରି ପଡନ୍ତି । କୋଠରୀ ଭିତରେ ପଶି ଅପେକ୍ଷାକୃତ ଧନୀ ପରିବାରରୁ
ଆସିଥିବା ଛାତ୍ରକୁ ସଂପାଦକ ଚାନ୍ଦା ଖାତା ବଢାଇ ଦିଅନ୍ତି । ସେ ଖାତା ଦେଖିବା
ଆରମ୍ଭ କରିବା ମାତ୍ରକେ ଜଣେ ସହୟୋଗୀ କହିଉଠେ , " ଏ ବର୍ଷର ପୂଜାଟାକୁ
ମାଟି କରିଦେଲ ସିନା । ଏ ତ ବଡ କଞ୍ଜୁସ୍ । ଏହା ପାଖରୁ ଅନୁକୂଳ କଲେ
ଆଉ ପଇସା ମିଳିବ ତ 	? ଛି , ଛି । ଅନ୍ଯ ଜଣେ ଧମକ ସ୍ବରରେ
କହିଉଠେ , " ତୁମେ କିଛି ଜାଣ ନାହିଁ ! ନ ଜାଣି ନ ଶୁଣି ଏଭଳି କଥା କେବେ
କହିବ ନାହିଁ । ମୁଁ ଭଲରୂପେ ଜାଣେ , ତାଙ୍କ ଜେଜେ ସେ ଅଞ୍ଚଳର ଜଣେ ଅତି
ଦଯାଳୁ ଲୋକ ଥିଲେ । ସେ ପ୍ରତିବର୍ଷ ଜଳଛତ୍ର ଦେଉଥିଲେ । ତାଙ୍କ ବାପା ମଧ୍ଧ
କମ୍ ନୁହଁନ୍ତି । କେହି କେବେ ତାଙ୍କ ଘରୁ ନିରାଶରେ ଫେରେ ନାହିଁ । ଜଣେ କେହି
ଆସି ଟଙ୍କାଟିଏ ମାଗିଲେ ସେ ଦଯାବହି ଦୁଇଟଙ୍କା ଦେଇ ଦିଅନ୍ତି । ଏ କଣ ତାଙ୍କ
ବଂଶର ମର୍ୟ୍ଯାଦା ବୁଡାଇଦେବେ ? ଏଇଟା ତାଙ୍କ ପକ୍ଷରେ ନିନ୍ଦା କଥା ନୁହଁ ।</p>

<p>ଏହି କଥା କଟାକଟିରେ ପିଲାଟି ବିବ୍ରତ ହୋଇଉଠେ । ଦୁଇଟଙ୍କା
ଦେବାକୁ ଇଚ୍ଛାଥିଲେ ମଧ୍ଧ ଖାତାରେ ପାଞ୍ଚଟଙ୍କା ଲେଖେ ବଂଶ ନିନ୍ଦାକୁ ଡରି ।</p>

<p>ତା ପରେ ସୁବିଧା ହୋଇୟାଏ । ଅନ୍ଯ ପିଲାଙ୍କୁ କହିବାକୁ ହୁଏ , ସେ
ଦେଲା ପାଞ୍ଚ ଟଙ୍କା , ତୁ କଣ ତା ଠାରୁ କମ୍୍ ଦେବୁ ? ତୋତେ ଲାଜ ଲାଗିବ ନାହିଁ ?
ପାଞ୍ଚଟଙ୍କାରୁ ଆରମ୍ଭ କରି ଆଠ ଅଣା ପର୍ୟ୍ଯନ୍ତ ଚାନ୍ଦା ମିଳିଥାଏ । ୟେତିକି
ମିଳେ , ସେଥିରେ ସମସ୍ତ ଆଯୋଜନ କରାୟାଏ ।</p>

<p>ସେତେବେଳେ ଟଙ୍କା ପଇସା ଥିଲା ଦୁଷ୍ପ୍ରାପ୍ଯ ; ମାତ୍ର ନିତ୍ଯ ବ୍ଯବହାର୍ୟ୍ଯ
ଦ୍ରବ୍ଯର ମୂଲ୍ଯ ଥିଲା କମ୍ । ସରୁ ଅରୁଆ ଚାଉଳ ଟଙ୍କାକୁ ପାଞ୍ଚରୁ ଛଅ ସେର ( କିଲୋ
ନୁହେଁ , ) ଘିଅ ସେର ଦେଢଟଙ୍କାରୁ ଦୁଇଟଙ୍କା - ଖାଣ୍ଟି ମଇଁଷି ଘିଅ , ବନସ୍ପତି
ବା ଚର୍ବି ମିଶ୍ରଣର ଉଦ୍ଭବ ହୋଇ ନ ଥିଲା । କ୍ଷୀର ଟଙ୍କାକୁ ଆଠ ସେର । //
କୋଣାର୍କ ଭ୍ରମଣ
ଆଗରୁ ସତ୍ଯବାଦୀ ସ୍କୁଲର ପିଲାମାନେ କୌଣସି ନା କୌଣସି
ଅଞ୍ଚଳକୁ ପଦୟାତ୍ରାରେ ଶିକ୍ଷା ଭ୍ରମଣରେ ୟାଉଥିଲେ । ପ୍ରଧାନ ଶିକ୍ଷକ ପଣ୍ଡିତ
ନୀଳକଣ୍ଠ ଓ ଆଉ କେତେକ ସେମାନଙ୍କ ନେତୃତ୍ବ ନେଉଥିଲେ ।</p>

<p>ମାତୃଭୂମିର ବିଭିନ୍ନ ଅଞ୍ଚଳ ସଂପର୍କରେ ପ୍ରତ୍ଯକ୍ଷ ଜ୍ଞାନ ଅର୍ଜନ କରିବା ,
ଦୂରଦୂରାନ୍ତ ମଫସଲ ବାସୀଙ୍କ ସହ ମିଶି ସେମାନଙ୍କ ସହ ଭାବ ବିନିମଯ
କରିବା , ଇତିହାସ ପ୍ରସିଦ୍ଧ ସ୍ଥାନମାନଙ୍କ ସଂପର୍କରେ ତଥ୍ଯ ସଂଗ୍ରହ କରିବା ସଙ୍ଗେ
ସଙ୍ଗେ ଦର୍ଶନୀଯ ସ୍ଥାନମାନଙ୍କର ପ୍ରାକୃତ୍ରିକ ସୌନ୍ଦର୍ୟ୍ଯ ଉପଭୋଗ କରିବା ଏ
ଭ୍ରମଣର ପ୍ରଧାନ ଲକ୍ଷ ଥିଲା । ଏହା ଫଳରେ ଓଡିଶାର ପୁରପଲ୍ଲୀ ସବୁ ଅଞ୍ଚଳରେ
ଏହି ବନ ବିଦ୍ଯାଳଯର ଖ୍ଯାତି ପ୍ରଚାରିତ ହୋଇଥିଲା ।</p>

<p>ସୂକ୍ଷ୍ମ କାରୁକାର୍ୟ୍ଯ ଖଚିତ ଇତିହାସ ପ୍ରସିଦ୍ଧ , ଓଡିଆ ଜାତିର ଗର୍ବ ଓ
ଗୌରବ ଭଗ୍ନ କୋଣାର୍କ ମନ୍ଦିର ପ୍ରତ୍ଯେକ ଛାତ୍ରର ମନକୁ ଆକୃଷ୍ଟ କରିଥାଏ ।
ପଣ୍ଡିତ କୃପାସିନ୍ଧୁ ମିଶ୍ରଙ୍କ ପ୍ରଣୀତ କୋଣାର୍କ ପୁସ୍ତକ କମ ଉଚ୍ଚାଟନ କରେ ନାହିଁ ,
ତେଣୁ ନବାଗତ ଛାତ୍ରମାନଙ୍କର ପ୍ରଥମ ଆକାଂକ୍ଷା କୋଣାର୍କ ଦର୍ଶନ ।</p>

<p>ଓଡିଆ ସାହିତ୍ଯର ଅମର କାବ୍ଯ ପଣ୍ଡିତ ନୀଳକଣ୍ଠଙ୍କ ରଚିତ କୋଣାର୍କେ
ଛାତ୍ରମାନଙ୍କର ଏହି କୋଣାର୍କ ୟାତ୍ରାର ପୃଷ୍ଠଭୂମି ଉପରେ ପରିକଳ୍ପିତ । ତାହାର
ପ୍ରଥମ ଭାଗ ` ରାମଚଣ୍ଡୀଠାରେ ରାତି ' ସତ୍ଯବାଦୀ ସ୍କୁଲର ଛାତ୍ରମାନଙ୍କ କୋଣାର୍କ
ଭ୍ରମଣର ସରସ ବିବରଣୀ ୟୁଗ ୟୁଗଧରି ପାଠକମାନଙ୍କୁ ମୁଗ୍୍ଧ କରୁଥିବ ।</p>

<p>ଆମ ସମଯକୁ ସେ ରାମ ନ ଥିଲେ କି ସେ ଅୟୋଧ୍ଧା ବି ନ ଥିଲା ।
ଶିକ୍ଷକମାନେ ବଯୋବୃଦ୍ଧ । ପୁରାତନ ଛାତ୍ର ୟେଉଁ କେତେଜଣ ଥିଲେ , ସେମାନେ
ଉତ୍ସାହହୀନ । ଅନୁଷ୍ଠାନର ପରିସ୍ଥିତି ସମସ୍ତଙ୍କୁ ଏକ ପ୍ରକାର ମ୍ରିଯମାଣ କରି~
ପକାଇଥାଏ । ସେ ବର୍ଷ ଆମେ ଦୂରଦୂରାନ୍ତରୁ ଆସିଥିବା ଛାତ୍ରମାନେ
କୋଣାର୍କ ଦର୍ଶନରେ ୟିବା ପାଇଁ ଉତ୍କଣ୍ଠିତ ହୋଇ ଉଠିଲୁ । କେତେଜଣ ୟାଇ ଆଚାର୍ୟ୍ଯ
ମହାଶଯଙ୍କୁ ଅନୁମତି ମାଗିଲୁ । ପ୍ରଶ୍ନ ହେଲା , " ତୁମ ସାଙ୍ଗରେ ୟିବ କିଏ ? "
" ଆମେ ତ ଆଉ ଛୋଟ ପିଲା ନୋହୁଁ , ଦଶମ ଶ୍ରେଣୀରେ ଆସି
ପଢିଲୁଣି । ପୁଣି ଏତେ ଜଣ ଅଛୁ । "</p>

<p>ଆମ ସଂଖ୍ଯା ପଚିଶରୁ କମ ନ ଥିଲା ।
" ନା , ଜଣେ ମୁରବୀ ଭଳି କେହି ନ ରହିଲେ ବୋଡିଂରୁ ତୁମକୁ ଛାଡି~
ପାରିବି ନାହିଁ । ବହୁତ ଦୂର , ଅଗମ୍ଯ ବାଟ , ପୁଣି ବଣ-ଜଙ୍ଗଲ-ପୂର୍ଣ୍ଣ ନିର୍ଜନ ।
ପିଲାଗୁଡିକ ଅସୁବିଧାରେ ପଡିବ ହଇରାଣ ହେବ । "</p>

<p>ଆଜିକାଲି କୋଣାର୍କ ୟାତ୍ରା ୟେପରି ସୁଗମ ହେଲାଣି ସେତେବେଳେ
ତାହା ସ୍ବପ୍ନାତୀତ ଥିଲା । ଆଖପାଖରେ ତ କୌଣସି ଗ୍ରାମ ନ ଥିଲା ଖାଲି ଗୋଟିଏ
କୁଡିଆ ଚାଳିଘର ମଧ୍ଧ ତିଆରି ହୋଇ ନ ଥିଲା । ଚାରିଆଡେ ଘନ ଜଙ୍ଗଲ ।
ସମଯ ସମଯରେ ବାଘ ଉପଦ୍ରବର ସମ୍ବାଦ ମିଳିଥାଏ । ଗଧିଆ ପଲପଲ ।
ବାଟ କେବଳ ଚାଷ ବିଲ ଭିତରେ । ସ୍ଥାନେ ସ୍ଥାନେ କିଆବୁଦା ବା ବାରମାସିଆ
ବୁଦାର ପ୍ରାଚୀର । ସେଥିପାଇଁ ଆଚାର୍ୟ୍ଯ ମହାଶଯଙ୍କ ଆପତ୍ତି ।</p>

<p>ଆମର ସେହି ବଯସଟା ଜୀବନର ଗୋଟିଏ ଘଡିସନ୍ଧି ସମଯ କହିଲେ
ଅତ୍ଯୁକ୍ତି ହେବ ନାହିଁ । କେହି ପିଲା କହିଦେଲେ ମନରେ ଖଟକା ଲାଗେ ।
କଥାବାର୍ତ୍ତାରେ ଟିକିଏ ଗୁରୁଗମ୍ଭୀର ଭାବ ପ୍ରକାଶ କଲେ ବଯସ୍କମାନଙ୍କର ବ୍ଯଙ୍ଗର
ପାତ୍ର ହେବାକୁ ପଡିଥାଏ । ଶୁଣିବାକୁ ହୁଏ ଏପରି ମୁରବିପଣିଆ ଦେଖାଅ ନାହିଁ ।</p>

<p>" ପିଲାଗୁଡିକ ଅସୁବିଧାରେ ପଡିବ " - ଆଚାର୍ୟ୍ଯ ମହାଶଯଙ୍କ ଏହି
ମନ୍ତବ୍ଯରେ ଆମ ଅଳି ବଢିଗଲା । ସମସ୍ତେ ଜିଦ୍ ଧରି ଅନୁନଯ କଲୁ । ତଥାପି
କଥା ନ ରହିବାରୁ ମନରେ ଅଭିମାନ ଜାତ ହେଲା ।</p>

<p>" ଆମେ ଆସି ଦଶମ ଶ୍ରେଣୀରେ ପଢିଲୁଣି । ଆସନ୍ତା ବର୍ଷକୁ
ବିଶ୍ବବିଦ୍ଯାଳଯ ପରୀକ୍ଷା ଦେବୁ - ମାଟ୍ରିକ୍ଯୁଲେସନ ପାଶ କରିବୁ , ହେବୁ କଲେଜ
ଛାତ୍ର , ତଥାପି ଆମକୁ ପିଲାତଳେ ଗଣା ୟାଉଛି । "</p>

<p>କେତେଜଣ ସହପାଠୀଙ୍କ ଏହି କଥୋପକଥନ ସଂସ୍କୃତ ଶିକ୍ଷକ
ଶ୍ରୀ ଗଙ୍ଗାଧର ମହାପାତ୍ର ଶୁଣିପାରିଲେ । ହସିହସି ପାଖକୁ ଆସି କହିଲେ -</p>

<p>" ସ୍ନେହଟା ମୁରବୀମାନଙ୍କୁ ସବୁବେଳେ ଅନ୍ଧ କରି ରଖିଥାଏ । ପୁଅ
ୟେତେ ବଯସ୍କ ହେଉ ପଛକେ , ୟେତେ ବଳିଷ୍ଠ , ସାହସୀ , କର୍ମୀ ହୋଇଥାଉ ନା
କାହିଁକି , ବାପା ମାଆଙ୍କ ଆଖିରେ ଅତି ଦୁର୍ବଳ କୁନି ପିଲା । କୌଣସି ବାପା-ମାଆ
ନିଜ ପିଲା ବୁଢା ହୋଇଗଲାଣି ବୋଲି କଳ୍ପନା କରି ପାରନ୍ତି ନାହିଁ । "</p>

<p>ସାଧାରଣରେ ଦେଖାୟାଏ ମୁରବୀ ସାଧାରଣତଃ ଦୁଇ ଶ୍ରେଣୀର । ଗୋଟିଏ
ଶ୍ରେଣୀର ହେଉଛନ୍ତି , ନିଜର ଉପୟୁକ୍ତ ପ୍ରତିଭା , କ୍ଷମତା ବା ବୋଧଶକ୍ତି ନ ଥିଲେ
ମଧ୍ଧ ସେମାନେ ସବୁଠାରେ ସବୁ ସମଯରେ ନିଜର ମୁରବିପଣିଆ ଜାହିର
କରିବାକୁ ଲାଳାଯିତ , ତତ୍ପର । ଅଧସ୍ତନମାନଙ୍କୁ ବିନା କାରଣରେ କୃତ୍ରିମ ମନଗଢା
କାରଣ ଦର୍ଶାଇ ସେମାନଙ୍କୁ ଡରେଇ ହଲେଇ ଭୀତତ୍ରସ୍ତ କରି ରଖିବା ସେମାନଙ୍କ
ସ୍ବଭାବସିଦ୍ଧ ଚରିତ୍ର । ସମସ୍ତଙ୍କର ସେ କେବଳ ଭଯର ପାତ୍ର । //
କିଂବଦନ୍ତୀ କହେ , ଦେବମନ୍ଦିର ବିଦ୍ବେଷୀ କଳାପାହାଡକୁ ବୋକା ବନାଇ
ନିଜର ପବିତ୍ରତା ସେ ବଜାଯ ରଖିଥିଲେ । ୟେଉଁ ପ୍ରବଳ ପ୍ରତାପୀ କଳା ପାହାଡ
ଶ୍ରୀ ଜଗନ୍ନାଥ ମୂର୍ତ୍ତିଙ୍କୁ ନେଇ ଗଙ୍ଗା କୂଳରେ ଦାହ କରିବା ପାଇଁ ସମର୍ଥ ହୋଇଥିଲା , ସେ
ଏହି ରାମଚଣ୍ଡୀଙ୍କଠାରେ ପରାଜଯ ମାନିଥିଲା । ସେ ମୂର୍ତ୍ତି ଭାଙ୍ଗିବାକୁ , ମନ୍ଦିର ମଧ୍ଧକୁ
ପଶିଲା ବେଳେ ଦ୍ବାର ମୁହଁରେ ଦେଖିଲା ଜଣେ ସୁନ୍ଦରୀ ୟୁବତୀ ପାଣି କଳସ
କାଖରେ ଧରି ମନ୍ଦିର ଭିତରୁ ବାହାରକୁ ଆସୁଛି । କଳାପାହାଡକୁ କହିଲା ,
" ପାଣି ଆଣିବାକୁ ୟାଉଛି । ଆସିଲେ ଦେବୀଙ୍କର ମାର୍ଜନା କରିବି । ଏଠାରେ
ଅପେକ୍ଷା କରି ବସିଥାଅ । "</p>

<p>କଳାପାହାଡ ଅପେକ୍ଷା କରି ବ୍ଯସ୍ତ ହୋଇ ଉଠିଲା । ସେ ଆଉ
ଫେରୁ ନାହାଁନ୍ତି । ଶେଷରେ ଅତି ବିରକ୍ତ ହୋଇ କବାଟ ଭାଙ୍ଗି ଭିତରକୁ ଗଲା ।
ମନ୍ଦିର ଶୂନ୍ଯ । କୌଣସି ମୂର୍ତ୍ତି ନାହିଁ । ପଥର ମୂର୍ତ୍ତିଟିକୁ କିଏ କିପରି ତା
ଆଖି ଆଗରେ ହରଣଚାଳ କରି ନେଇଗଲା ! ତାହା ସେ ବୁଝି ପାରିଲା ନାହିଁ ।</p>

<p>ମନ୍ଦିରରେ କେବଳ ଦୁଇଜଣ ସେବକ ନ ଥାନ୍ତି । ସେମାନଙ୍କ ସହ
ବାସ କରୁଥାନ୍ତି ଆଉ ଦୁଇଟି ପ୍ରାଣୀ - ଦୁଇଟି ଅସମ୍ଭବ ରକମର ହୃଷ୍ଟପୁଷ୍ଟ
ବିଲେଇ ।</p>

<p>ଅଳ୍ପ ଦୂରରେ ସମୁଦ୍ର ସୀମାକୁ ଲାଗି ପତ୍ର କୁଡିଆଟିଏ । ତା ଭିତରେ
ଚାରି ଜଣ ନୋଳିଆ - ମାଛ ଧରାଳି ।</p>

<p>ସେ ସମଯରେ ତାହା କୌଣସି ଜମିଦାରଙ୍କ ସଂପତ୍ତି ନ ଥିଲା । ମାଛ
କୁତ ଧରିଥିବା ନୋଳିଆମାନେ ରାତିରେ ସେ କୁଡିଆରେ ରହନ୍ତି । ପ୍ରତ୍ଯୁଷରୁ
ପାଣିରେ ନାଆ ମେଲାଇ ମାଛ ଧରିବାକୁ ୟାଆନ୍ତି ।</p>

<p>ପ୍ରଥମ ଖେପାରେ ୟେତେ ମାଛ ଧରା ପଡିବେ , ସେ ସମସ୍ତ ରାମଚଣ୍ଡୀଙ୍କ
ପ୍ରାପ୍ଯ । ତାଙ୍କରି ପାଖରେ ଭୋଗ ଲାଗିବ । ସେବକ ଦୁହେଁ ତାର ଅଧିକାରୀ ।
ତାପରେ ୟେତେ ମାଛ ଧରାୟିବ , ସେ ସବୁ ପୁରୀ ସହରକୁ ଚାଲିୟିବ ବିକ୍ରି ପାଇଁ ।</p>

<p>କୁଳମଣି ବାବୁ ସେମାନଙ୍କ ପାଖକୁ ୟାଇ କିଛି ମାଛ ଧରି ଦେବାକୁ
କହିଲେ । ଏତେ ପିଲା ଖାଇବେ କଅଣ ? ସାଙ୍ଗରେ ଅଛି ଖାଲି ଆଳୁ ଓ ଡାଲି ।
ବିଶେଷତଃ ସମୁଦ୍ର କୂଳରେ ରହି ମାଛ ନ ଖାଇବେ ବା କିପରି ?</p>

<p>କୋହଲା ପାଗ , ଜୋର ପବନ , ପୁଣି ଘୋର ଅନ୍ଧକାର ; ଉତ୍ତର
ମିଳିଲା , " ଏପରି ଅବସ୍ଥାରେ ସମୁଦ୍ର ଭିତରକୁ ୟିବା ଅସମ୍ଭବ । "</p>

<p>" ଗୁଡିଏ ପିଲା ଆସିଛନ୍ତି । ସତ୍ଯବାଦୀ ସ୍କୁଲର ଛାତ୍ର । ରାତିରେ
କଅଣ ଉପାସ ରହିବେ ? ତୁମର ନିନ୍ଦା ହେବ ନାହିଁ ? "</p>

<p>କୁଳମଣି ବାବୁଙ୍କର ଏ କଥାରେ ନୋଳିଆମାନଙ୍କର ମନ ଦବିଲା । କହିଲେ ,
" ଟଙ୍କାରେ ତିନି ବିଶାରୁ ବେଶି ଦେବୁ ନାହିଁ । "
ବିଶାକ କଟିକୀ ସେରରେ ଦେଢ ସେର । ସେରକ ଏକଶହ ପାଞ୍ଚ
ତୋଳା ଓଜନ ।
କୁଳମଣି ବାବୁ ସମ୍ମତି ଜଣାଇଲେ । ଆଗତୁରା ତାଙ୍କୁ ଟଙ୍କାଟିଏ
ବଢାଇଦେଲେ ।
ନୋଳିଆମାନେ ଦୁଇଟି ମଶାଲ ଧରି ସମୁଦ୍ରରେ ଡଙ୍ଗା ମେଲିଦେଲେ ।
ଅଳ୍ପ ସମଯ ଭିତରେ ସେମାନେ ପାଣି ଭିତରେ ଅଦୃଶ୍ଯ ହୋଇଗଲେ ।
ଅଧଘଣ୍ଟାକ ପରେ ସେମାନେ ଫେରି ବାଲି ଉପରେ ଜାଲ ଝାଡିଦେଲେ । ୟେତେ ମାଛ
ଜମା ହୋଇଗଲା , ତା ଦେଖି ଆମ ଆଖି ଖୋସି ହୋଇଗଲା । ମନ କୁରୁଳି ଉଠିଲା ।
ସେମାନେ ବିଶାରେ ଓଜନ କଲେ । ତିନି ବିଶା ହେଲାପରେ ବାକି
ୟାହା ବାଲି ଉପରେ ପଡି ରହିଲା , ତାହା ଆଉ ତିନି ବିଶାରୁ ଅଧିକ ହେବ ।
" ଏତେ ରାତିରେ ଆମେ ଏହାକୁ କଅଣ କରିବୁ ? ସବୁତକ
ନେଇ ୟାଆନ୍ତୁ । "</p>

<p>ଟଙ୍କାଟିଏ ବଦଳରେ ଛଅବିଶା ବା ନଅସେର ମାଛ ମିଳିଲା ।
ସବୁ ୟାକ ଏକ ଗୋଷ୍ଠୀର । ଚାନ୍ଦୀ ଜାତୀଯ ଚେପ୍୍ଟା ଓ ମଧ୍ଧମ ଧରଣର ।</p>

<p>ଖାଇସାରି ମଧ୍ଧରାତ୍ରି ସୁଦ୍ଧା ସମସ୍ତେ ଶୋଇ ପଡିଲୁ । ଖାଦ୍ଯ ଗରମ
ଗରମ ଭାତ ଓ ପ୍ରଚୁର ମାଛ ତରକାରୀ । ସେ ମାଛରେ ଏତେ ସରୁ କଣ୍ଟା ୟେ
ଅଧାଅଧି ପତ୍ରରେ ଛାଡିୟିବାକୁ ବାଧ୍ଧ ହେଲୁ ।</p>

<p>ବର୍ଷା ପୁରାପୁରି ଛାଡିୟାଇଛି । ଆକାଶ ନିର୍ମଳ ।
ସକାଳ ଛଅଟା ବେଳକୁ ୟାଇ ନିଦ ଭାଙ୍ଗିଲା । ପଦାରେ କୁହୁଡିଘେରା
ଆଲୁଅ । କୁଆ କାଆ କାଆ ରାବ ଦେଲାଣି । ପାଣିଧାର ନିକଟସ୍ଥ କଣ୍ଟାବୁଦା
ଗହଳିରୁ ନାନା ପ୍ରକାର ପକ୍ଷୀଙ୍କର କୋଳାହଳ ।</p>

<p>ଦେଖାଗଲା , ସାଙ୍ଗରେ ୟାଇଥିବା ଭାରୁଆ , ପୂଜାରୀ ଓ ଦେବୀ~
ସେବକ ସେପର୍ୟ୍ଯନ୍ତ ବସି ଖାଉଛନ୍ତି । କହିଲେ , " ଟଙ୍କା ଦିଆୟାଇ ମାଛ
କିଣାହୋଇଛି ; ଆଉ କଣ ଫୋପାଡି ଦିଆୟିବ ? ସବୁ ଆଦାଯ କରିଦେଲୁ ।
ତେଲରେ କଡା ଭଜା କରିଦେଲାରୁ ସରୁ କଣ୍ଟା ଚୋବେଇ ହୋଇଗଲା ।
କିଛି ଅସୁବିଧା ହେଲା ନାହିଁ କି ମାଲ ନଷ୍ଟ ହେଲା ନାହିଁ । " //
ଶୀଘ୍ର ଗାଡି ଷ୍ଟେସନରୁ ବାହାରିଗଲେ ଷ୍ଟେସନ ମାଷ୍ଟର ଆଶ୍ଚର୍ୟ୍ଯ ହେବେ ।
ସେଥିପାଇଁ ପ୍ଲାଟଫର୍ମରେ ଏପଟ ସେପଟ ହେଉଥିଲେ । ପିଅନଟି ତାଙ୍କୁ
ଡାକିଆଣିଲା । ଗାର୍ଡ ସେହି ପାଖ ଦେଇ ନିଜ ଡବା ଆଡକୁ ୟାଉଥିଲେ , ତାଙ୍କୁ
ମଧ୍ଧ ଡକାହେଲା ।</p>

<p>ଉଭଯଙ୍କ ପାଖରେ ହାକିମି ମିଜାଜରେ ଅଭିୟୋଗ ହେଲା - " ଗୁଡିଏ
ବିନା ଟିକେଟ ୟାତ୍ରୀ ଡବା ଭିତରେ ପଶି ଗୋଳମାଳ କରୁଛନ୍ତି । ବୋଧ ହୁଏ
ଉଦ୍ଦେଶ୍ଯ ଭଲ ନୁହେଁ । ଚୋର କି ଡାକୁ ହୋଇ ପାରନ୍ତି । ଏମାନଙ୍କୁ ନିକାଲି
ଦେବାକୁ ହେବ । "</p>

<p>ସେ ଉଭଯେ ଡବା ଭିତରକୁ ଚଢି ଆସିଲେ । କୌଣସି କଥା ଶୁଣିବାକୁ
ନାରାଜ । ଟାଣି ଓଟାରି ସମସ୍ତଙ୍କୁ ବାହାର କରିଦେଲେ ।</p>

<p>ଜିଲ୍ଲା ମାଜିଷ୍ଟ୍ରେଟଙ୍କ ଟେଲିଗ୍ରାମ ଓ ପାଶ ଦେଖାଇଲୁ ।
ଗାର୍ଡ ତାଙ୍କ କାର୍ୟ୍ଯରେ ଲାଗିଗଲେ । ଷ୍ଟେସନ ମାଷ୍ଟର ଆମ ପାଇଁ ସ୍ଥାନ
ଖୋଜିବାକୁ ବ୍ଯସ୍ତ ହୋଇ ପଡିଲେ । ଇଞ୍ଜିନଠାରୁ ଶେଷ ଡବା ପର୍ୟ୍ଯନ୍ତ
ଦଉଡାଦଉଡି କଲେ , ମାତ୍ର ବୃଥା ପରିଶ୍ରମ ।</p>

<p>ହୁଇସିଲ ହେଲା । ଗାଡି ଚାଲିବାକୁ ଆରମ୍ଭ କଲା । ଷ୍ଟେସନ ମାଷ୍ଟର
ଦଉଡିଆସି ସେହି ଡବା ଭିତରକୁ ଉଠି ୟିବାକୁ ନିର୍ଦ୍ଦେଶ ଦେଲେ । ନିଜେ କବାଟ
ପାଖରେ ଠିଆହୋଇ ଜଣେ ଜଣେ କରି ହାତ ଧରି ଉଠାଇଲେ । ଜଣେ
ଅଳ୍ପବଯସ୍କ ପିଲା ଚଳନ୍ତା ଗାଡିରେ ଉଠି ପାରିଲା ନାହିଁ । ତଳକୁ ଆସି ତାକୁ
କୋଳ କରି ନେଇ ଡବାରେ ଚଢାଇ ନେଲେ ।
ସେତେବେଳକୁ ଡେପୁଟି ଦମ୍ପତିଙ୍କ ମୁହଁ କଳା ପଡିଗଲାଣି ।
ଷ୍ଟେସନ ମାଷ୍ଟର ତଳେ ଠିଆ ହୋଇ ତାଙ୍କୁ ଆଶ୍ବାସନା ଦେଲେ । ଆପଣଙ୍କର
କୌଣସି ଅସୁବିଧା ହେବ ନାହିଁ । ସେମାନେ ସତ୍ଯବାଦୀ ସ୍କୁଲର ଛାତ୍ର ।</p>

<p>ପୁରୀ ଷ୍ଟେସନରେ ସବୁ ଡବା ଖାଲି ହୋଇଗଲା । ଆମେ ଓହ୍ଲାଇବା~
ପୂର୍ବରୁ ତାଙ୍କ ପାଖକୁ ୟାଇ ନମସ୍କାର କଲୁ । ସେ ମୁହଁ ଟେକି ଚାହିଁ ନ ଥିଲେ କି
ଗାମ୍ଭୀର୍ୟ୍ଯର ପରିବର୍ତ୍ତନ ଘଟି ନ ଥିଲା ।</p>

<p>ଭଗବତୀ ଚରଣ ପାଣିଗ୍ରାହୀ ନମସ୍କାର ଜଣାଇଲାବେଳେ କହିଦେଲେ -
" ପୁରୀ ଜିଲ୍ଲା ମାଜିଷ୍ଟ୍ରେଟ ଆମକୁ ନିମନ୍ତ୍ରଣ କରି ଅଣାଇଛନ୍ତି । ପୁରୀରେ ହଇଜା
ଲାଗିବା ଭଯ ଅଛି । ଆମେ ରୋଗୀମାନଙ୍କୁ ସେବା କରିବୁ । ଜଗନ୍ନାଥେ କରନ୍ତୁ
ଆପଣଙ୍କ ପାଖକୁ ଫେରି ୟିବାକୁ ନ ପଡେ । "</p>

<p>ଗୋଟିଏ ନାଁ ବାଳକ ପିଲାର ଔଦ୍ଧତ୍ଯ ତାଙ୍କ ମୁହଁର କି ପରିବର୍ତ୍ତନ
କରିଥିବ , ତାହା ଦେଖିବାର ସୁୟୋଗ ଆମକୁ ମିଳି ନ ଥିଲା ।
 +&gt;*
               0 
</p></body></text></cesDoc>