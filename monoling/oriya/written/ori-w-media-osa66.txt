<cesDoc id="ori-w-media-osa66" lang="ori">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ori-w-media-osa66.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ସମ୍ବାଦ..</h.title>
<h.author>ସୌମ୍ଯ...</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Paper</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">osa66</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0800.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-12</date></creation>
<langUsage>Oriya</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;ଉଦ୍ଦୀପନା ସହ ସାଧାରଣତନ୍ତ୍ର
ଦିବସ ପାଳିତ
ଭୁବନେଶ୍ବର - ଏଠାରେ
ଗତକାଲି ପ୍ରବଳ ଆନନ୍ଦ ଓ ଉଦ୍ଦୀପନା
ମଧ୍ଯରେ ସାଧାରଣତନ୍ତ୍ର ଦିବସ ପାଳିତ
ହୋଇୟାଇଛି । ସ୍ଥାନୀଯ ପ୍ଯାରେଡ୍୍
ପଡିଆଠାରେ ରାଜ୍ଯପାଳ ଶ୍ରୀ ବିଶ୍ବମ୍ବର
ନାଥ ପାଣ୍ଡେ ଅଭିବାଦନ ଗ୍ରହଣ କରିଥିଲେ
ଏବଂ ଏହି ପରେଡ ଦେଖିବାକୁ ପ୍ରବଳ
ଜନସମାଗମ ହୋଇଥିଲା ।</p>

<p>ଶ୍ରୀ ପାଣ୍ଡେ ଗତକାଲି ସକାଳ
ଆଠଟା ଅଠେଇଶି ମିନିଟ୍୍ରେ ପରେଡ
ପଡିଆଠାରେ ପହଞ୍ଚିବା ମାତ୍ରେ , ରାଜ୍ଯ
ମୁଖ୍ଯ ଶାସନସଚିବ ଶ୍ରୀ ନଳିନୀକାନ୍ତ ପଣ୍ଡା
ତାଙ୍କୁ ସ୍ବାଗତ ଜଣାଇ ମଞ୍ଚ ଉପରକୁ
ପାଛୋଟି ନେଇଥିଲେ । ବିଭିନ୍ନ ସାମରିକ
ଓ ବେସାମରିକ ସ୍କୁଲ ଓ କଲେଜ , ରେଡକ୍ରସ
ଓ ଅଗ୍ନିଶମ ସଂସ୍ଥା ଏହି ପରେଡରେ
ଅଂଶଗ୍ରହଣ କରିଥିଲେ । ବିଭିନ୍ନ ଶିଳ୍ପସଂସ୍ଥା
ପକ୍ଷରୁ ପ୍ରଜ୍ଞାପନୀ ମେଢ ମଧ୍ଯ ଏହି
ପରେଡରେ ଭାଗ ନେଇଥିଲେ ।
ପି.ଏମ୍୍.ଜି. ଅଫିସରୁ ଏହି ପରେଡ
ବାହାରି ଏ.ଜି.ଛକ , ରାଜମହଲ ଛକ ,
ମାଷ୍ଟର କ୍ଯାଣ୍ଟିନ୍୍ ଦେଇ ପି.ଏମ୍୍.ଜି.
ଅଫିସକୁ ଫେରିଥିଲା ।</p>

<p>ରାଜଧାନୀର ବିଭିନ୍ନ ଅନୁଷ୍ଠାନ
ବିଶେଷ କରି ଶିକ୍ଷାନୁଷ୍ଠାନ ଗୁଡିକରେ
ସାଧାରଣତନ୍ତ୍ର ଦିବସ ପାଳିତ
ହୋଇଥିଲା ।</p>

<p>କଂଗ୍ରେସ ଭବନଠାରେ ପି.ସି.ସି.
ସଭାପତି ଶ୍ରୀ ନିତ୍ଯାନନ୍ଦ ମିଶ୍ର ପତାକା
ଉତ୍ତୋଳନ କରିଥିଲେ । ସାଧାରଣ
ସମ୍ପାଦକ ଶ୍ରୀ ଗୋକୁଳା ନନ୍ଦ ବିଶ୍ବାଳ ,
କୋଷାଧ୍ଯକ୍ଷ ଶ୍ରୀ ବସନ୍ତ କୁମାର ବିଶ୍ବାଳ ,
ପୂର୍ବତନ ମନ୍ତ୍ରୀ ଶ୍ରୀ ଗଙ୍ଗାଧର ମହାପାତ୍ର ,
ବିଧାଯକ ସୁରେଶ କୁମାର ରାଉତରାଯ ,
ଭୁବନେଶ୍ବର ଜିଲ୍ଲା କଂଗ୍ରେସ କାର୍ୟ୍ଯକାରୀ
ସଭାପତି ଶ୍ରୀ ବିଧାନଚନ୍ଦ୍ର ପଟ୍ଟନାଯକ ,
ସର୍ବଶ୍ରୀ ନାରାଯଣ ରଥ , ଭବାନୀ ଚରଣ
ପଟ୍ଟନାଯକ , ଦେବରାଜ ପାଠୀ , ବାନାମ୍ବର
ପାତ୍ର ପ୍ରମୁଖ ଉତ୍ସବରେ ୟୋଗ
ଦେଇଥିଲେ ।</p>

<p>ଭାରତର ସମୃଦ୍ଧି ପାଇଁ ଗଣତାନ୍ତ୍ରିକ
ଶାସନ ପଦ୍ଧତି ଏକମାତ୍ର ପନ୍ଥା ବୋଲି
ମୁଖ୍ଯମନ୍ତ୍ରୀ ଶ୍ରୀ ଜାନକୀ ବଲ୍ଲଭ ପଟ୍ଟନାଯକ
ଉଲ୍ଲେଖ କରିଛନ୍ତି ।</p>

<p>ଗତକାଲି କଟକ ବାରବାଟୀ
ଷ୍ଟାଡିଯମଠାରେ ସାଧାରଣତନ୍ତ୍ର ଦିବସ
ଉପଲକ୍ଷେ ଅନୁଷ୍ଠିତ ପ୍ଯାରେଡରେ
ଅଭିବାଦନ ଗ୍ରହଣ କରି ମୁଖ୍ଯମନ୍ତ୍ରୀ
କହିଲେ ୟେ ଗତ 38 ବର୍ଷର
ଗଣତାନ୍ତ୍ରିକ ଶାସନ ଭିତରେ ଭାରତ
କୃଷିକ୍ଷେତ୍ରରେ ସ୍ବାବଲମ୍ବୀ ହେବା ସଙ୍ଗେ
ସଙ୍ଗେ ଶିଳ୍ପକ୍ଷେତ୍ରରେ ବିଶ୍ବରେ ଦଶମ
ସ୍ଥାନ ଅଧିକାର କରିପାରିଛି ।
ଏହାବ୍ଯତୀତ ନିରପେକ୍ଷ ରାଷ୍ଟ୍ରମାନଙ୍କ
ନେତୃତ୍ବ ନେଇ ଭାରତ ପୃଥିବୀରେ ଶାନ୍ତି
ପ୍ରତିଷ୍ଠା କରିପାରିଛି । ତେବେ ୟେତେଦିନ
ପର୍ୟ୍ଯନ୍ତ ଦାରିଦ୍ର୍ଯ ଦୂରୀକରଣ ହୋଇପାରି
ନାହିଁ ସେତେଦିନ ପର୍ୟ୍ଯନ୍ତ ସମ୍ବିଧାନର
ଲକ୍ଷ୍ଯ ପୂରଣ ହୋଇପାରିବ ନାହିଁ ।</p>

<p>ଦିଲ୍ଲୀରେ ବର୍ଣ୍ଣାଢ୍ଯ
ଜନରାଜ୍ଯ ପରେଡ
ନୂଆଦିଲ୍ଲୀ -
39ତମ ଜନରାଜ୍ଯ ଦିବସ ଉପଲକ୍ଷେ
ବର୍ଣ୍ଣାଢ୍ଯ ପରେଡ ଦିଲ୍ଲୀରେ ଅନୁଷ୍ଠିତ
ହୋଇଥିଲା । ରାଷ୍ଟ୍ରପତି ଶ୍ରୀ ରାମସ୍ବାମୀ
ଭେଙ୍କଟରମଣ ଅଭିବାଦନ ଗ୍ରହଣ
କରିଥିଲେ । ତିନିବାହିନୀର ଜବାନଙ୍କ
ସମେତ ସମରଶିକ୍ଷାର୍ଥୀ ବାହିନୀର
କ୍ଯାଡେଟ୍୍ ଓ ସ୍କୁଲ ବାଳକ ବାଳିକା ଏହି
ପରେଡରେ ଅଂଶଗ୍ରହଣ କରିଥିଲେ ।</p>

<p>ଗତକାଲିର ଉତ୍ସବରେ ସମ୍ମାନିତ
ଅତିଥିଭାବେ ଶ୍ରୀଲଙ୍କା ରାଷ୍ଟ୍ରପତି
ଜଯବର୍ଦ୍ଧନେ ୟୋଗ ଦେଇଥିଲେ ।
ଅପରାହ୍ନରେ ରାଷ୍ଟ୍ରପତି ଭବନଠାରେ
ବିଶିଷ୍ଟ ନାଗରିକମାନଙ୍କୁ ସମ୍ବର୍ଦ୍ଧନା ଜ୍ଞାପନ
କରାୟାଇଥିଲା । ରାଷ୍ଟ୍ରପତି ଜଯବର୍ଦ୍ଧନେ
ଶ୍ରୀମତୀ ଏଲିନା ଜଯବର୍ଦ୍ଧନେ , ପ୍ରଧାନମନ୍ତ୍ରୀ
ଶ୍ରୀ ରାଜୀବ ଗାନ୍ଧୀ , ଉପରାଷ୍ଟ୍ରପତି ଡକ୍ଟର
ଏସ୍୍.ଡି.ଶର୍ମା , ବାଚସ୍ପତି ବଳରାମ
ଜାଖର ପ୍ରମୁଖଙ୍କ ସମେତ ବିଶିଷ୍ଟ ବ୍ଯକ୍ତି
ଏଥିରେ ୟୋଗ ଦେଇଥିଲେ ।</p>

<p>ରାଷ୍ଟ୍ରପତି ଅନ୍ଯ ଏକ ଉତ୍ସବରେ
ବିଭିନ୍ନ କ୍ଷେତ୍ରରେ ଉଲ୍ଲେଖୟୋଗ୍ଯ
ଅବଦାନ ଦେଇଥିବା ସାମରିକ
ଅଫିସରମାନଙ୍କୁ ସ୍ବୀକୃତି ସ୍ବରୂପ
ପଦକମାନ ପ୍ରଦାନ କରିଥିଲେ । //
ଶ୍ରୀଲଙ୍କାରେ ପ୍ରବଳ ସଂଘର୍ଷ ,
14 ଶାନ୍ତିସେନା ନିହତ
କଲମ୍ବୋ -
ଶ୍ରୀଲଙ୍କାର ଉତ୍ତର ଏବଂ ପୂର୍ବାଞ୍ଚଳରେ
ଏଲ୍୍.ଟି.ଟି.ଇ. ଗରିଲା ଏବଂ ଶାନ୍ତିବାହିନୀ
ମଧ୍ଯରେ ବଡ ଧରଣର ସଂଘର୍ଷ ଘଟିବା
ଫଳରେ 14 ଜଣ ଶାନ୍ତିସେନା ଏବଂ
16 ଜଣ ଗରିଲାଙ୍କ ସମେତ ମୋଟ 35
ଜଣ ବ୍ଯକ୍ତି ନିହତ ହୋଇଛନ୍ତି । ଶ୍ରୀଲଙ୍କା
ସରକାରଙ୍କ ପ୍ରତି ଭାରତର ସମର୍ଥନକୁ
ବିରୋଧ କରି ଗଣତନ୍ତ୍ର ଦିବସ ଠାରୁ
ଏହି ଦୁଇ ପ୍ରଦେଶରେ ଏଲ୍୍.ଟି.ଟି.ଇ.
ପକ୍ଷରୁ 24 ଘଣ୍ଟିଆ ସାଧାରଣ ଧର୍ମଘଟ
ପାଳନ କରାୟାଇଛି ।</p>

<p>ମିଳିଥିବା ସୂତ୍ରରୁ ପ୍ରକାଶ ୟେ ,
ଗତ 48 ଘଣ୍ଟା ଧରି ଏଲ୍୍.ଟି.ଟି.ଇ.
ଗରିଲାମାନେ ଭାରତୀଯ ଶାନ୍ତିସେନା
ଜବାନ୍୍ଙ୍କ ଉପରେ ବଡ ଧରଣର
ଆକ୍ରମଣ ଆରମ୍ଭ କରିଛନ୍ତି । ଗତକାଲି
ପୂର୍ବ ବାଟ୍ଟିକାଲୋଆ ସହର ଠାରେ ତାମିଲ୍୍
ଗରିଲା ଏବଂ ଶାନ୍ତିସେନାଙ୍କ ମଧ୍ଯରେ
ବହୁ ସମଯ ଧରି ଗୁଳିଗୋଳା ବିନିମଯ
ହୋଇଥିଲା । ଗତକାଲି ସନ୍ଧ୍ଯାରୁ ଉକ୍ତ
ସହରରେ 36 ଘଣ୍ଟା ପାଇଁ କର୍ଫ୍ଯୁ ଜାରି
କରାୟାଇଛି ।</p>

<p>ବାଟ୍ଟିକୋଲୋଆ ପୋଲିସ୍୍ ସୂତ୍ରରୁ
ଜଣାପଡିଛି ୟେ , ନାଉତକୁଲି ଠାରେ
ତାମିଲ ଗରିଲାମାନେ ପହରାରତ
ଶାନ୍ତିସେନା ଉପରେ ଅତର୍କିତ ଆକ୍ରମଣ
କରିବା ଫଳରେ ଉଭଯ ପକ୍ଷ ମଧ୍ଯରେ
ଦୀର୍ଘ ସମଯ ଧରି ସଂଘର୍ଷ ଘଟିଥିଲା ।
ଏଲ୍୍.ଟି.ଇ. ପକ୍ଷରୁ ଏକ ଲ୍ଯାଣ୍ଡମାଇନ୍୍
ବିସ୍ଫୋରଣରେ ଜଣେ ଶାନ୍ତିସେନା ଜବାନ୍୍
ନିହତ ହୋଇଥିଲେ । ଉତ୍ତରପୂର୍ବ
ମୁକାଇତିଭୂ ଜିଲ୍ଲାରେ ଘଟିଥିବା
ସଂଘର୍ଷରେ 8 ଜଣ ଶାନ୍ତିସେନା ଜବାନ
ପ୍ରାଣ ହରାଇଥିବା ଏଲ୍୍.ଟି.ଟି.ଇ. ପକ୍ଷରୁ
ଦାବୀ କରାୟାଇଛି । ହିଂସାକାଣ୍ଡ ପରେ
ସମ୍ପୂର୍ଣ୍ଣ ଜିଲ୍ଲାରେ 24 ଘଣ୍ଟିଆ କର୍ଫ୍ଯୁ
ଜାରି କରାୟାଇଛି ।</p>

<p>ଗତ ସୋମବାର ଦିନ
ବାଟ୍ଟିକାଲୋଆ ଜିଲ୍ଲାର ଭାଲାକେନାଲ୍୍
ଠାରେ ଶାନ୍ତିବାହିନୀର ପାଲଟା
ଆକ୍ରମଣରେ 5 ଜଣ ତାମିଲ୍୍ ଗରିଲା
ନିହତ ହୋଇଥିବା ବେଳେ ଶାନ୍ତିସେନାର
ଜଣେ ଜୁନିଅର କମିଶନର ଅଫିସର ନିହତ
ହୋଇଥିଲେ । ଏତଦ୍୍ବ୍ଯତୀତ
ଏଲ୍୍.ଟି.ଟି.ଇ.ର ବିଶିଷ୍ଚ ନେତା ରିଗାନ୍୍
ଆହତ ହୋଇଥିବା ଉକ୍ତ ସୂତ୍ରରୁ ପ୍ରକାଶ ।</p>

<p>ଆଜି ଜାନକୀଙ୍କ ଅଗ୍ନିପରୀକ୍ଷା
ନୂଆଦିଲ୍ଲୀ -
ଆସନ୍ତାକାଲି ତାମିଲନାଡୁରେ
ମୁଖ୍ଯମନ୍ତ୍ରୀ ଶ୍ରୀମତୀ ଜାନକୀ ରାମଚନ୍ଦ୍ରନ
ଆସ୍ଥାଭୋଟର ସମ୍ମୁଖୀନ ହେଉଛନ୍ତି । ଏହି
ପରିପ୍ରେକ୍ଷୀରେ ବିଭିନ୍ନ ମହଲରେ
ତତ୍ପରତା ଓ କଳ୍ପନାଜଳ୍ପନାମାନ ପ୍ରକାଶ
ପାଇଛି ।</p>

<p>ପ୍ରଧାନମନ୍ତ୍ରୀ ରାଜୀବ ଗାନ୍ଧୀଙ୍କ
ଦ୍ବାରା ପ୍ରଦର୍ଶିତ ଏକ ବାର୍ତ୍ତାରେ ଆଜି
କୁହାୟାଇଛି ୟେ କଂଗ୍ରେସ (ଇ) ଦଳ
ତାମିଲନାଡୁରେ ସ୍ବର୍ଗତ ଏମ.ଜି.~
ରାମଚନ୍ଦ୍ରନଙ୍କ ଅବିଭକ୍ତ ଦଳକୁ ହିଁ ସମର୍ଥନ
ଦେବ । ସ୍ବର୍ଗତ ରାମଚନ୍ଦ୍ରନ ତାଙ୍କ
ଉଇଲରେ ଦଳ ୟେପରି ବିଭକ୍ତ ନ ହୁଏ
ତାହା ଇଚ୍ଛା ପ୍ରକାଶ କରିଥିଲେ । ଦଳ
ଖଣ୍ଡଖଣ୍ଡ ହେଲେ ରାଜ୍ଯର ଦୁର୍ବଳ , ଦରିଦ୍ର
ଓ ନିର୍ୟାତିତ ଜନସାଧାରଣଙ୍କ ପ୍ରତି
ଘୋର ଅନ୍ଯାଯ କରାୟିବ କାରଣ
ସେହିମାନଙ୍କ ଲାଗି ଆନ୍ନା.ଡି.ଏମ୍୍.କେ.
ଗଠନ କରାୟାଇଥିଲା ବୋଲି ସ୍ବର୍ଗତ
ରାମଚନ୍ଦ୍ରନ ଲେଖିୟାଇଛନ୍ତି । ତେଣୁ
ଆନ୍ନ.ଡି.ଏମ୍୍.କେ.ର ଉଭଯ ଗୋଷ୍ଠୀ
ମତାନୈକ୍ଯ ଭୁଲି ଏକାଠି ହେବାପାଇଁ
ଶ୍ରୀ ଗାନ୍ଧୀ ନିବେଦନ କରିଛନ୍ତି ।</p>

<p>ଆଜି ଏଠାରୁ ତିନିଜଣ କେନ୍ଦ୍ର ମନ୍ତ୍ରୀ
ମାନ୍ଦ୍ରାଜ ଅଭିମୁଖେ ୟାତ୍ରା କରିଥିବା
ଜଣାପଡିଛି । ସେମାନେ ହେଲେ ସ୍ବରାଷ୍ଟ୍ର
ମନ୍ତ୍ରୀ ବୁଟା ସିଂହ , ନିଖିଳ ଭାରତ କଂଗ୍ରେସ
(ଇ) କମିଟୀର ସାଧାରଣ ସମ୍ପାଦକ
କେ.ଏନ୍୍.ସିଂହ ଓ ସ୍ବରାଷ୍ଟ ରାଷ୍ଟ୍ରମନ୍ତ୍ରୀ ପି.~
ଚିଦାମ୍ବରମ୍୍ । ଏହି ନେତାମାନେ
ମାନ୍ଦ୍ରାଜଠାରେ ରାଜ୍ଯ କଂଗ୍ରେସ (ଇ)
ନେତାମାନଙ୍କୁ ପରାମର୍ଶ ଦେବେ ବୋଲି
ପ୍ରକାଶ ।</p>

<p>ଅପରପକ୍ଷରେ ମଦୁରାଇ
କଂଗ୍ରେସ (ଇ) ସଭାପତି ଏମ୍୍.କେ.~
ରାମକୃଷ୍ଣନ ପ୍ରଧାନମନ୍ତ୍ରୀଙ୍କ ନିକଟକୁ ଏକ
ଟେଲିଗ୍ରାମ ପଠାଇ ଦଳର
ବିଧାଯକମାନଙ୍କୁ ଜାନକୀ ରାମଚନ୍ଦ୍ରନଙ୍କ
ସରକାର ବିରୁଦ୍ଧରେ ଭୋଟ ଦେବାଲାଗି
ଅନୁମତି ଦେବାକୁ ନିବେଦନ କରିଛନ୍ତି ।
ସେ କହିଛନ୍ତି ୟେ ଶ୍ରୀମତୀ ରାମଚନ୍ଦ୍ରନଙ୍କ
ସରକାରରେ ଶ୍ରୀ ବିରାପାନ , ଶ୍ରୀ କଲିମୁଟ୍ଟୁ
ଓ ଶ୍ରୀ ରାଘବାନନ୍ଦନମ୍୍ ଅଛନ୍ତି । ସେମାନେ
ଏମ୍୍.ଜି.ଆରଙ୍କ ଜୀବିତାବସ୍ଥାରେ କଂଗ୍ରେସ
(ଇ) ର ନୀତି ଓ ଆଦର୍ଶର ଘୋର
ସମାଲୋଚନା କରୁଥିଲେ । ତେଣୁ
ଶ୍ରୀମତୀ ରାମଚନ୍ଦ୍ରନଙ୍କ ସରକାରକୁ
ସମର୍ଥନ ନ ଦେବା ଉଚିତ ।</p>

<p>ଆଜି ତାମିଲନାଡୁ ବିଧାନସଭା
ବୈଠକରେ ଜଯଲଳିତା ଗୋଷ୍ଠୀର
ବିଧାଯକମାନେ ୟୋଗ ଦେଇନଥିଲେ ।
ସେମାନେ ସ୍ବର୍ଗତ ଏମ୍୍.ଜି.ରାମଚନ୍ଦ୍ରନଙ୍କ
ସମାଧିପୀଠରେ ଏକତ୍ରୀତ ହୋଇ
ତାଙ୍କ ମୃତ୍ଯୁରେ ଶୋକପ୍ରକାଶ
କରିଥିଲେ । ଆଜି ତାମିଲନାଡୁ
ବିଧାନସଭାରେ ସ୍ବର୍ଗତ ଏମ୍୍.ଜି.ଆରଙ୍କ
ପାଇଁ ମଧ୍ଯ ଶୋକ ପ୍ରସ୍ତାବ ଗୃହୀତ ହୋଇ
ବୈଠକ ମୁଲତବୀ ରହିଥିଲା । //
କେଳୁଚରଣଙ୍କୁ
ପଦ୍ମଭୂଷଣ
ଭୁବନେଶ୍ବର - ଓଡିଶୀ
ନୃତ୍ଯର ପ୍ରବୀଣ ଗୁରୁ କେଳୁଚରଣ
ମହାପାତ୍ରଙ୍କୁ 1987 ର ରାଷ୍ଟ୍ରୀଯ ସମ୍ମାନ
ପଦ୍ମଭୂଷଣ ମିଳିଛି । ଓଡିଶୀ ନୃତ୍ଯ
ସାଧନାରେ ଗୁରୁ କେଳୁଚରଣଙ୍କ
ଉଲ୍ଲେଖନୀଯ ଅବଦାନ ସାଙ୍ଗକୁ ଏହି
ଶାସ୍ତ୍ରୀଯ ନୃତ୍ଯକଳାକୁ ଲୋକପ୍ରିଯ
କରାଇବାରେ ତାଙ୍କ ସୁଦୀର୍ଘ ଭୂମିକା ପ୍ରତି
ସ୍ବୀକୃତି ସ୍ବରୂପ ଏହି ପଦ୍ମଭୂଷଣ ଉପାଧି
ମିଳିଛି ।</p>

<p>ସେହିଭଳି ପଟ୍ଟଚିତ୍ର କଳାରେ
ବିଶେଷ ଅବଦାନର ସ୍ବୀକୃତି ସ୍ବରୂପ ଶିଳ୍ପୀ
ସୁଦର୍ଶନ ସାହୁଙ୍କୁ ପଦ୍ମଶ୍ରୀ ଉପାଧି ପ୍ରଦାନ
କରାୟାଇଛି ।</p>

<p>ବି.ଡି.ଓ.ଙ୍କୁ ମାଡ ଅଭିୟୋଗରେ
ବିଧାଯକଙ୍କ ବିରୁଦ୍ଧରେ ମକଦ୍ଦମା
ପୁରୀ - ପିପିଲି
ବି.ଡି.ଓ.ଙ୍କୁ ଅଶ୍ଳୀଳ ଭାଷାରେ ଗାଳିଗୁଲଜ
କରିବା ସଙ୍ଗେ ସଙ୍ଗେ ମାଡମାରିବା
ଅଭିୟୋଗରେ ପୋଲିସ ବିଧାଯକ
ଶ୍ରୀ ପ୍ରଦୀପ୍ତ କୁମାର ମହାରଥୀଙ୍କ ସମେତ
ଆଠଜଣଙ୍କ ବିରୁଦ୍ଧରେ ମକଦ୍ଦମା ରୁଜ୍ଜୁ
କରିଥିବା ଜଣାପଡିଛି । ଏହି ଘଟଣାରେ
ପୋଲିସ ପିପିଲି ଏନ୍୍.ଏ.ସି.
ଚେଯାରମ୍ଯାନ୍୍ଙ୍କ ସାନଭାଇ ଶ୍ରୀ ଶରତ
ମହାନ୍ତି ଓ ଅନ୍ଯ ତିନିଜଣଙ୍କୁ ଗିରଫ କରି
ଆଜି ପୁରୀ କୋର୍ଟକୁ ଚାଲାଣ କରିଛନ୍ତି ।
ଗିରଫ ଭଯରେ ବିଧାଯକ ଶ୍ରୀ ମହାରଥୀ
ଘଟଣାସ୍ଥଳରୁ ଫେରାର ହୋଇୟାଇଥିବା
ପୋଲିସ ସୂତ୍ରରୁ ପ୍ରକାଶ ।</p>

<p>ଘଟଣାରୁ ପ୍ରକାଶ ୟେ , ଗତକାଲି
ବିଧାଯକ ଶ୍ରୀ ମହାରଥୀଙ୍କ ନେତୃତ୍ବରେ
ପ୍ରାଯ କୋଡିଏ ଜଣ ଲୋକ ବ୍ଳକ ଅଫିସ
ଭିତରେ ଜୋରଜବରଦସ୍ତ ପ୍ରବେଶ କରି
ତାଙ୍କ ( ବିଧାଯକଙ୍କ ) ସମର୍ଥକମାନଙ୍କୁ
ଆର.ଏଲ.ଇ.ଜି.ପି. ୟୋଜନାରେ କାମ
ୟୋଗାଇଦେବାକୁ ବାଧ୍ଯକରି 15
ଜଣଙ୍କ ନାମ ଦେଇଥିଲେ । ମାତ୍ର ବି.ଡି.ଓ.
ଉକ୍ତ ନାମଗୁଡିକ ଗ୍ରାମ୍ଯକମିଟୀ
ସୁପାରିଶକ୍ରମେ ପଠାଇବାକୁ କହିଥିଲେ ।
ଏଥିରେ ଉତ୍୍କ୍ଷିପ୍ତ ହୋଇ ବିଧାଯକ
ଶ୍ରୀ ମହାରଥୀ ଅଶ୍ରାବ୍ଯ ଭାଷାରେ
ଗାଳିଗୁଲଜ କରିବା ସଙ୍ଗେ ସଙ୍ଗେ ତାଙ୍କୁ
ମୁଥ ମାରିଥିଲେ ବୋଲି ବି.ଡି.ଓ.
ଶ୍ରୀ ମହାନ୍ତି ପିପିଲି ଥାନାରେ ଲିଖିତ
ଅଭିୟୋଗ ଦାଯର କରିଛନ୍ତି । ଏହାପରେ
ପୋଲିସ ଭାରତୀଯ ପିଙ୍ଗଳକୋର୍ଡର
452 , 294 , 353 , 506 ଓ
34 ଦଫାରେ ମକଦ୍ଦମା ରୁଜ୍ଜୁ କରିଥିବା
ଜଣାୟାଇଛି ।</p>

<p>ଏହି ଘଟଣାର ପ୍ରତିବାଦ କରି
ଆଜି ପିପିଲି ବ୍ଳକ କର୍ମଚାରୀମାନେ
କଲମଛାଡ ଆନ୍ଦୋଳନ କରି କାର୍ୟ୍ଯରେ
ୟୋଗ ଦେଇନଥିଲେ । ଖବର ପାଇ
ସଦର ଏସ୍୍.ଡି.ଓ. ଶ୍ରୀ ଦିଗମ୍ବର ମହାନ୍ତି ଓ
ଭାରପ୍ରାପ୍ତ ଅଧିକାରୀ ଶ୍ରୀ ନିବାରଣ ସାମଲ
ଘଟଣାସ୍ଥଳରେ ପହଞ୍ଚି ସମ୍ପୃକ୍ତ
ବ୍ଯକ୍ତିମାନଙ୍କ ବିରୁଦ୍ଧରେ କାର୍ୟ୍ଯାନୁଷ୍ଠାନ
ଗ୍ରହଣ କରାୟାଉଛି ବୋଲି ଦର୍ଶାଇବା
ପରେ ସେମାନେ ଆନ୍ଦୋଳନ ପ୍ରତ୍ଯାହାର
କରିଥିଲେ ।</p>

<p>ଏହି ଘଟଣାରେ ଗଭୀର ଉଦ୍୍ବେଗ
ପ୍ରକାଶ କରି ଆଜି ପୁରୀ ଜିଲ୍ଲା ଓ.ଏ.ଏସ୍୍.
ଅଫିସର ଆସୋସିଏସନର ଜରୁରୀ
ବୈଠକ ଡକାୟାଇଥିବା ପ୍ରକାଶ ।</p>

<p>ଦିଲ୍ଲୀରେ ଓକିଲ ଧର୍ମଘଟ
ସବୁ କୋର୍ଟ ଅଚଳ
ନୂଆଦିଲ୍ଲୀ -
ଆଜି ଏଠାରେ ଓକିଲମାନେ
ଦିନିକିଆ ପ୍ରତୀକ ଧର୍ମଘଟ ପାଳନ
କରିଥିବାରୁ ସୁପ୍ରିମକୋର୍ଟ , ହାଇକୋର୍ଟ ଓ
ସବୁ ଅଧସ୍ତନ କୋର୍ଟ କାର୍ୟ୍ଯ ଅଚଳ ହୋଇ
ପଡିଥିବା ଜଣାପଡିଛି । 21 ତାରିଖରେ
ତିସ୍୍ହଜାରୀ କୋର୍ଟର ଜଣେ ଓକିଲଙ୍କୁ
ପୋଲିସ ଦ୍ବାରା ହାତକଡି ପକାଇ ଗିରଫ
କରାୟିବାର ପ୍ରତିବାଦ କରି ଗତ
ଗୁରୁବାର ଦିନ ଓକିଲମାନେ ବିକ୍ଷୋଭ
ପ୍ରଦର୍ଶନ କରିଥିବାବେଳେ ପୋଲିସ୍୍ ଲାଠି
ଚଳାଇବାରୁ ଏହାର ପ୍ରତିବାଦ ସ୍ବରୂପ
ଆଜିର ଦିନିକିଆ ଧର୍ମଘଟ ପାଳନ
କରାୟାଇଛି । ଲାଠିମାଡର ବିଚାର
ବିଭାଗୀଯ ତଦନ୍ତ ପୋଲିସ୍୍ ଉପ
କମିଶନରଙ୍କୁ ସସ୍୍ପେଣ୍ଡ କରାୟିବା ଓ
ଉପ-କମିଶନରଙ୍କ କାର୍ୟ୍ଯାଳଯକୁ
ତିସ୍୍ହଜାରି କୋର୍ଟରୁ ଅନ୍ଯତ୍ର ଉଠାଇ
ଦେବା ଦାବୀ କରି ପ୍ରାଯ 2 ହଜାର ଓକିଲ
ଆଜି ଶୋଭାୟାତ୍ରାରେ ବୋଟ୍୍ କ୍ଳବକୁ
ୟାଇଥିଲେ ଓ ଦାବୀ ପୁରଣ ନ ହେଲେ
ଦେଶବ୍ଯାପୀ ଧର୍ମଘଟ କରାୟିବ ବୋଲି
ଚେତାବନୀ ଦେଇଥିଲେ । //
11 ଜଣ ଛାତ୍ର ଛାତ୍ରୀଙ୍କ
ଅନଶନ ଧର୍ମଘଟ
ରୁପ୍୍ସା - ଭୋଗରାଇ
ସ୍ଥିତ ଶ୍ଯାମ ସୁନ୍ଦର ସଂସ୍କୃତ
ମହାବିଦ୍ଯାଳଯର 11 ଜଣ ଛାତ୍ରଛାତ୍ରୀ
ପରିଚାଳନା ସମିତିର ଦୁର୍ନୀତି ମୁଳକ
କାରବାର ବିରୁଦ୍ଧରେ ଏବଂ 17 ଦଫା
ଦାବୀ ଉପରେ ସମ୍ପ୍ରତି କଲେଜ ପରିସର
ମଧ୍ଯରେ ଅନଶନ କରିଥିଲେ । ଏମାନଙ୍କ
ମଧ୍ଯରୁ 4 ଜଣଙ୍କ ଅବସ୍ଥା
ଉଦ୍୍ବେଗଜନକ ହୋଇ ପଡିଥିଲା ।
ନିଜର ଦାବୀ ନେଇ ଛାତ୍ରମାନେ
ଜିଲ୍ଲାପାଳଙ୍କ ଠାରୁ ଆରମ୍ଭ କରି
ଶିକ୍ଷାମନ୍ତ୍ରୀଙ୍କ ପର୍ୟ୍ଯନ୍ତ ସବୁ କର୍ତ୍ତୃପକ୍ଷଙ୍କୁ
ଜଣାଇବା ପରେ ମଧ୍ଯ କୌଣସି
ପ୍ରତିକାର ନ ହେବାରୁ ସେମାନେ
ଶେଷରେ ଏହି ପନ୍ଥା ଗ୍ରହଣ
କରିଥିବା ବିଷଯ କହିଛନ୍ତି ।</p>

<p>ଶିକ୍ଷାଧିକାରୀଙ୍କ କାର୍ୟ୍ଯାଳଯ
ଆଗରେ ବିକ୍ଷୋଭ ପ୍ରଦର୍ଶନ
ବାଲେଶ୍ବର -
ବାଲେଶ୍ବର ଜିଲ୍ଲା ଅଣଆନୁଷ୍ଠାନିକ ଶିକ୍ଷକ
ସଂଘ ପକ୍ଷରୁ ଗତ 19 ତାରିଖ ଦିନ ଜିଲ୍ଲା
ମଣ୍ଡଳ ଶିକ୍ଷାଧିକାରୀଙ୍କ କାର୍ୟ୍ଯାଳଯ
ସମ୍ମୁଖରେ ବିକ୍ଷୋଭ ପ୍ରଦର୍ଶନ
କରାୟାଇଥିଲା । ସି.ଟି. ତାଲିମ
ପ୍ରାପ୍ତ ଅଣଆନୁଷ୍ଠାନିକ ଶିକ୍ଷକ
ଶିକ୍ଷଯିତ୍ରୀମାନଙ୍କୁ ବିନା ସାକ୍ଷାତକାର ତଥା
କୌଣସି ବଯସ ସୀମା ନ ରଖି ପ୍ରାଥମିକ
ବିଦ୍ଯାଳଯରେ ନିୟୁକ୍ତି ଦେବା ଏବଂ
ଅଣତାଲିମପ୍ରାପ୍ତ ଶିକ୍ଷକ ଶିକ୍ଷଯିତ୍ରୀମାନଙ୍କୁ
ପୁନଃ ତାଲିମ୍୍ ଦିଆୟିବାକୁ ଦାବୀ କରି
ନିଖିଳ ଓଡିଶା ଅଣଆନୁଷ୍ଠାନିକ ଶିକ୍ଷକ
ସଂଘର ସଭାପତି ଶ୍ରୀ ପୂର୍ଣ୍ଣଚନ୍ଦ୍ର ପୁହାଣଙ୍କ
ନେତୃତ୍ବରେ ଏହି ବିକ୍ଷୋଭ ପ୍ରଦର୍ଶନ
କରାୟାଇଥିଲା ।</p>

<p>ପରିବେଶ ସୁରକ୍ଷା
ସଚେତନ ଅଭିୟାନ
ବ୍ରହ୍ମପୁର - ଓଡିଶା
ବିଜ୍ଞାନ ପ୍ରସାର ପରିଷଦ ପକ୍ଷରୁ
ନଭେମ୍ବର 19ରୁ ଚାଲିଥିବା ପରିବେଶ
ସୁରକ୍ଷା ସଚେତନ ଅଭିୟାନ ଗତ 9
ତାରିଖ ଦିନ ଉଦ୍୍ୟାପିତ ହୋଇୟାଇଛି ।
ସ୍ଥାନୀଯ ଟାଉନହଲ୍୍ରେ ଅନୁଷ୍ଠିତ
ଉଦ୍୍ୟାପନ ଉତ୍ସବରେ ବ୍ରହ୍ମପୁର
ମେଡିକାଲ୍୍ କଲେଜ ଭେଷଜତତ୍ତ୍ବ ବିଭାଗ
ପ୍ରଫେସର ଡକ୍ଟର କରିନୁଦ୍ଦିନ୍୍ ଅହମ୍ମଦ
ଅଧ୍ଯକ୍ଷତା କରିଥିଲେ । ମୃତ୍ତିକା ସଂରକ୍ଷଣ
ବିଭାଗର ପୂର୍ବତନ ନିର୍ଦ୍ଦେଶକ
ଶ୍ରୀ ଚୈତନ୍ଯ ପାତ୍ର ମୁଖ୍ଯ ଅତିଥି ଏବଂ
ରାଜ୍ଯ ଶିକ୍ଷା ଓ ପ୍ରଶିକ୍ଷଣ ଗବେଷଣା
ପରିଷଦ ବିଜ୍ଞାନ ଓ ଗଣିତ ବିଭାଗ
ପ୍ରଧ୍ଯାପକ ଡକ୍ଟର ନରେନ୍ଦ୍ର ପ୍ରସାଦ ଦାସ
ମୁଖ୍ଯବକ୍ତା ରୂପେ ୟୋଗଦେଇ ବିଜ୍ଞାନର
ସଦୁପୟୋଗ ଓ ପରିବେଶ ସୁରକ୍ଷା
ସମ୍ପର୍କରେ ଆଲୋଚନା କରିଥିଲେ ।
ସାଧାରଣ ସମ୍ପାଦକ ଡକ୍ଟର ଦୁର୍ଗାଚରଣ
ଜେନା ବିବରଣୀ ପାଠ କରିଥିଲେ ।
ସରକାରୀ ବାଳିକା ଉଚ୍ଚ ବିଦ୍ଯାଳଯର
ଛାତ୍ରୀମାନେ ପରିବେଶ ସୁରକ୍ଷା ସମ୍ପର୍କରେ
ପ୍ରାରମ୍ଭିକ ସଙ୍ଗୀତ ଗାନ କରିବା ପରେ
ମୁଖ୍ଯ ଅତିଥି ବିଭିନ୍ନ ପ୍ରତିୟୋଗିତାର କୃତୀ
ଛାତ୍ରଛାତ୍ରୀଙ୍କୁ ପୁରସ୍କାର ବିତରଣ
କରିଥିଲେ ।</p>

<p>ହାତୀ , ଭାଲୁ ଓ ମାଙ୍କଡ
ଉପଦ୍ରବ , ଜଣେ ମୃତ
ବେଲଗୁଣ୍ଠା - ବେଲଗୁଣ୍ଠା
ଓ ଭଞ୍ଜନଗର ବ୍ଳକର କଣ୍ଢେଇପଲ୍ଲୀ ,
ସିରିକୋଇ , ଷଡଙ୍ଗାପଲ୍ଲୀ , ଲେମ୍ବେଇ ଓ
କାଦଲୁଣ୍ଡି ଜଗନ୍ନାଥ ପ୍ରସାଦ ବ୍ଳକ୍୍ର
ଗଯାଗଣ୍ଡା , ସମ୍ବରବନ୍ଧା ଏବଂ ବେଲଗୁଣ୍ଠା
ବଜାରରେ ହାତୀ , ଭାଲୁ ଓ ମାଙ୍କଡ
ଉପଦ୍ରବ ବୃଦ୍ଧି ପାଇଛି । ଗତ ଏକ ମାସ଼
ଭିତରେ ସିରିକୋଇ ଗ୍ରାମର ଶ୍ରୀ ତ୍ରିନାଥ
ବିଷୋଯୀ , କଣ୍ଢେଇପଲ୍ଲୀ ଗ୍ରାମର ଜଣେ
ଆଦିବାସୀ ୟୁବକ , ଷଡଙ୍ଗୀ ପଲ୍ଲୀ ଓ
କାଦଲୁଣ୍ଡି ଗ୍ରାମର ଦୁଇ ଜଣ ବଯୋବୃଦ୍ଧ
ବ୍ଯକ୍ତି ଭାଲୁ ଆକ୍ରମଣରେ ଗୁରୁତର ଭାବେ
ଆହତ ହୋଇଛନ୍ତି । ଭାଲୁ ଉପଦ୍ରବ ୟୋଗୁଁ
ଏସବୁ ଅଞ୍ଚଳ ସନ୍ଧ୍ଯା ହେଲେ ଶୁନ୍୍ଶାନ୍୍
ହୋଇୟାଉଛି ।</p>

<p>ଗତ କିଛିଦିନ ମଧ୍ଧରେ ଜଗନ୍ନାଥ
ପ୍ରସାଦ ବ୍ଳକ ଅନ୍ତର୍ଗତ ସମ୍ବରବନ୍ଧ ଗ୍ରାମ
ନିକଟରେ ରାଜୁ ନାଯକ ନାମକ ଜଣେ
ହରିଜନ ବ୍ଯକ୍ତି ହାତୀ ଆକ୍ରମଣରେ ପ୍ରାଣ
ହରାଇଛନ୍ତି । ତାଙ୍କୁ ସରକାରୀ ସାହାୟ୍ଯ
ପ୍ରଦାନ ପାଇଁ ଗ୍ରାମବାସୀମାନେ ସମ୍ପୃକ୍ତ
କର୍ତ୍ତୃପକ୍ଷଙ୍କ ନିବେଦନ କରିଛନ୍ତି । ହାତୀ
ଉପଦ୍ରୁତ ୟୋଗୁଁ ଭଞ୍ଜନଗର
ସବ୍୍ଡିଭିଜନ୍୍ର ବହୁ ଅଞ୍ଚଳରେ
କୋକୁଆ ଭଯ ସୃଷ୍ଟି ହୋଇଛି ।</p>

<p>ମଙ୍ଗଳବାର ଦିନ ବେଲଗୁଣ୍ଠା
ହାଟପଦାରେ ମାଙ୍କଡ ଆକ୍ରମଣରେ ଜଣେ
ମହିଳା ଓ ଦୁଇଜଣ ବାଳକ ଆହତ
ହୋଇଛନ୍ତି । ତା ପୂର୍ବଦିନ ଏହି ମାଙ୍କଡ
ଦ୍ବାରା ନିକଟସ୍ଥ ରଙ୍ଗଣୀ ପାଟଣାରେ ଜଣେ
ମହିଳା ମଧ୍ଯ ଗୁରୁତର ଭାବେ ଆହତ
ହୋଇଥିଲେ ।
 +&gt;*
               0 
</p></body></text></cesDoc>