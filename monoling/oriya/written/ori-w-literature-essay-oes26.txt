<cesDoc id="ori-w-literature-essay-oes26" lang="ori">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ori-w-literature-essay-oes26.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ଓ.ଓଡିଆ..</h.title>
<h.author>ଚିତ୍ତରଞ୍</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book.</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">oes26</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 1001.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-12</date></creation>
<langUsage>Oriya</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;ଓଡିଶା ଗୋଟିଏ ଭୂମି ଓ ଓଡିଆ ହେଉଛି ଏକ ଜନଗୋଷ୍ଠୀର ନାମ ।
ପୃଥିବୀରେ ବାସ କରି ଆସିଥିବା ୟାବତୀଯ ଜନଗୋଷ୍ଠୀ ପରି ଏହି
ଜନଗୋଷ୍ଠୀଟିର ମଧ୍ଧ ଏକ ସଂହତି ରହିଛି । ଏକ ନିଯତି ମଧ୍ଧ ରହିଛି । ଏହି
ସଂହତି ଏବଂ ନିଯତି କେବଳ ଭାବଗତ ନୁହେଁ , ତାହା ଆହ୍ବାନଗତ ।
ଓଡିଆ ଏକ ଜନଗୋଷ୍ଠୀର ନାମ , ଏକ ଭାଷାର ମଧ୍ଧ ନାମ । ସେହି ଭାଷାର ଏକ
ସାହିତ୍ଯ ରହିଛି , ଏକ ଐକ୍ଯବିଧାଯକ ଆବେଦନ ମଧ୍ଧ ରହିଛି । ଓଡିଶାରେ
ଆମେ ୟେଉଁମାନେ ଓଡିଆ ଭାଷା କହୁ , କେବଳ ସେହିମାନଙ୍କୁ ହିଁ ଓଡିଆ
ବୋଲି କୁହାୟିବ ନା ଅନ୍ଯ ଭାଷାଭାଷୀମାନଙ୍କ ସମେତ ଏହି ରାଜ୍ଯରେ ବାସ
କରୁଥିବା ସମସ୍ତଙ୍କୁ ଓଡିଆ ବୋଲି କୁହାୟାଇ ପାରିବ ? ଘରେ ଓଡିଆ
କହୁଥିବା ବହୁସଂଖ୍ଯାବିଶିଷ୍ଟ ଓଡିଆମାନଙ୍କୁ ଆପଣା ଭିତରେ ଏହି ପ୍ରଶ୍ନଟିର
ଏକ ସମାଧାନ କରିବାକୁହିଁ ପଡିବ । ପୁରାତନ କାଳରୁ ଆରମ୍ଭ କରି ଏବ
ପର୍ୟ୍ଯନ୍ତ କୌଣସି ସମଯରେ ହିଁ ଓଡିଶାଭୂମିର ସମସ୍ତେ କଦାପି ଓଡିଆ
କହୁ ନ ଥିଲେ । ଓଡିଆ ପୂର୍ବରୁ ଏଠାରେ ଏକାଧିକ ଆଦିବାସୀ ଭାଷା ରହିଥିଲା ,
ଏବ ମଧ୍ଧ ରହିଛି ।</p>

<p>ଏହି ଭୂମିର ` ଓଡିଶା ' ନାମଟି ମଧ୍ଧ ଆଦୌ ସବୁଦିନର ନୁହେଁ । ଆହୁରି
ୟଥାର୍ଥ ଭାବରେ କହିଲେ , ଷୋଡଶ-ସପ୍ତଦଶ ଶତାବ୍ଦୀ ବେଳକୁ ୟେତେବେଳେ
ମୋଗଲମାନେ ବଙ୍ଗଳାକୁ ଆସି ବର୍ତ୍ତମାନ ଓଡିଶାର ଉପକୂଳ-ଅଞ୍ଚଳଟିକୁ
ଅଧିକାର କଲେ , ସେତେବେଳେ ସେମାନେ ତାହାକୁ ଓଡିଶା ବୋଲି
କହିଲେ । ସେତେବେଳେ ସେହି ମୋଗଲବନ୍ଦୀ ଅଞ୍ଚଳ ବ୍ଯତୀତ ଓଡିଆ ଭାଷା~
ଭାଷୀ ଗଡଜାତମାନେ ମଧ୍ଧ ରହିଥିଲେ । ମୋଗଲମାନଙ୍କ ପୂର୍ବରୁ କଳିଙ୍ଗ
ଥିଲା , ତତ୍୍ କଳିଙ୍ଗ ଥିଲା କୋଶଳ ଏବଂ ତୋଷାଳି ମଧ୍ଧ ଥିଲା , ମାତ୍ର ଓଡିଶା
ନଥିଲା । ଏମାନେ ପରସ୍ପର ମଧ୍ଧରେ ୟୁଦ୍ଧ କରୁଥିଲେ , ପରସ୍ପରର ଦିଅଁମାନଙ୍କୁ
ସାନ ବୋଲି ଦେଖାଇ ନିଜ ଦିଅଁଙ୍କୁ ସେମାନଙ୍କଠାରୁ ଅଧିକ ମହିମାର ଓ
ଅଧିକ ଗରିମାର ବୋଲି ଦେଖାଉଥିଲେ । ତିନୋଟି ବା ତତୋଧିକ ଖଣ୍ଡ~
ରାଜ୍ଯକୁ ଏକାଠି କରି ୟେଉଁମାନେ ଏଠାରେ ନିଜକୁ ସକଳ-କଳିଙ୍ଗ-ଅଧିପତି
ବୋଲି ଘୋଷଣା କରିଥିଲେ , ସେମାନେ ମୂଳତଃ ଓଡିଶାରେ ନଥିଲେ ,
ସେମାନେ ଦକ୍ଷିଣରୁ ଆସିଥିଲେ ଏବଂ ରାଜ୍ଯ ବିଜଯର କ୍ରମରେ ଏହି ଭୂମିଟିକୁ
ଏକ ରାଜନୀତିକ ବିସ୍ତୃତତର ରୂପ ଦେଇଥିଲେ । ଆଗ କାଳରେ ରାଜ୍ଯ ରାଜ୍ଯ
ମଧ୍ଧରେ ୟେଉଁସବୁ ୟୁଦ୍ଧ ହେଉଥିଲା , ଅଧିକତର ୟଥାର୍ଥତା ସହିତ ତାହାକୁ
ଜାତି ଜାତି ମଧ୍ଧରେ ହୋଇଥିବା କୌଣସି ୟୁଦ୍ଧ ବୋଲି ନ କହି ରାଜବଂଶ
ସହିତ ରାଜବଂଶର ୟୁଦ୍ଧ ବୋଲି କୁହାୟାଇ ପାରିବ ।</p>

<p>ଓଡିଶାରେ ଜାତୀଯତାବାଦର ସଂକ୍ରମଣ ହେବା ପରେ ୟାଇ ଆମେ
ଏଠି ମଧ୍ଧ ନିଜକୁ ଗୋଟିଏ ଜାତି ବୋଲି କହିବାକୁ ଏବଂ ଅନୁଭବ କରିବାକୁ
ଆରମ୍ଭ କଲୁ । ଏହି ଉନବିଂଶ ଶତାବ୍ଦୀର ଶେଷ ଭାଗର କଥା ।
ସେହି ସଂକ୍ରମଣ ଓ ସେହି ପ୍ରେରଣାରୁ ତତ୍୍କାଳୀନ ଓଡିଶାରେ ଏକ ସ୍ବତନ୍ତ୍ର
କିସମର ନେତୃତ୍ବ ମଧ୍ଧ ଆବିର୍ଭୁତ ହୋଇଥିଲା , ୟାହାର କି ପୂର୍ବତନ କୌଣସି
ୟୁଗ ପର୍ୟ୍ଯାଯରେ ଏହି ସମାନ୍ତର ଦେଖିବାକୁ ମିଳିବ ନାହିଁ । ତାପରେ
ଏହି ଭୂମିର ସାହିତ୍ଯକୁ ଏକ ବିଶେଷ ବ୍ଯଞ୍ଜନ ଦେଇ ଓଡିଆ ସାହିତ୍ଯ ବୋଲି
କୁହାଗଲା । ଏକ ସମୂହ ଭାବରେ ଆପଣାର ଉନ୍ନତି ବିଧାନ ଲାଗି ଆମ
ଆଗୁଆମାନେ ୟେତେବେଳେ ୟେଉଁ ଆନ୍ଦୋଳନ କରିଥିଲେ , ତାହାକୁ ଓଡିଆ
ଆନ୍ଦୋଳନ ବୋଲି କୁହାଗଲା । ଏଠାରେ ସର୍ବ ପ୍ରଧାନ - ସମ୍ମିଳନୀ , ଉତ୍କଳ
ସମ୍ମିଳନୀ ବୋଲି ଅଭିହିତ ହେଲା । ତଥାପି ସ୍ମରଣ କରିବାକୁ ହେବ ୟେ ,
ସେତେବେଳେ ଏହି ୟାବତୀଯ ସମ୍ମିଳନ , ଅଭିୟାନ ଓ ଆନ୍ଦୋଳନ
ୟେଉଁମାନଙ୍କ ଦ୍ବାରା ହୋଇଥିଲା ସେମାନେ ଓଡିଶାର ପ୍ରଧାନତଃ
ଓଡିଆ ଭାଷାଭାଷୀମାନଙ୍କୁହିଁ ଆଗରେ ରଖି ଏହି ସବୁକିଛିରେ ଆଗ୍ରହୀ
ହେଉଥିଲେ । ରାଜନୀତିକ ଓଡିଶା ଡିଭିଜନର ସୀମା ବାହାରେ ତତ୍୍କାଳୀନ
ଅନ୍ଯାନ୍ଯ ପ୍ରଦେଶମାନଙ୍କରେ ୟେଉଁ ସବୁ ଓଡିଆ ଭାଷାଭାଷୀ ରହିଥିଲେ , ସେହି
ସମସ୍ତଙ୍କୁ ଏକାଠି କରି ଓଡିଶା ବୋଲି ସ୍ବତନ୍ତ୍ର ପ୍ରଦେଶଟିଏ ଲାଭକରିବାରେ
ବିଚାର ତଥା କାର୍ୟ୍ଯକ୍ରମ ସେତିକିବେଳେ ଆରମ୍ଭ ହୋଇଥିଲା । ଉତ୍ସାହ ଓ
ଉଦ୍ଯମଗୁଡିକ ସେତେବେଳେ ଏପରି ଏକାଗ୍ର ଏବଂ ଏକୋଦ୍ଦିଷ୍ଟ ହୋଇ
ରହିଥିଲା ୟେ ଏହି ଭୂମିରେ ବାସ କରୁଥିବା ଅନ୍ଯ ଭାଷାଭାଷୀମାନଙ୍କ ବିଷଯରେ
ଆମ ତତ୍କାଳିନ ନେତାମାନେ ହୁଏତ ଆଦୌ କୌଣସି ବିଚାର କରିନଥିଲେ ।
ତେଣୁ ସେହି ଅନ୍ଯ ଭାଷାଭାଷୀମାନେ ମୁଖ୍ଯ ସ୍ରୋତଟିର ବାହାରେ ବାହାରେ
ରହିଥିଲେ । //
ମୁସଲମାନମାନେ ପ୍ରଥମେ ଏହି ଦେଶକୁ ବହିଃରାକ୍ରମଣକାରୀ ହିସାବରେ
ଆସିଥିଲେ । ସେମାନେ ଏହି ଦେଶର ଅନେକାଂଶକୁ ଜଯ କଲେ ଓ ଏଠାରେ
ଦୀର୍ଘକାଳ ଲାଗି ସାମ୍ରାଜ୍ଯମାନଙ୍କର ବି ପ୍ରତିଷ୍ଠା କଲେ । ମାତ୍ର ସେମାନେ କାଳ~
କ୍ରମେ ଏହି ଭୂମିରେ ହିଁ ରହିଗଲେ । ଭାରତୀଯ ସ୍ଥପତି , ଭାରତୀଯ ସଂଗୀତ ,
ଭାରତୀଯ ଦର୍ଶନ ଓ ଏପରିକି ଭାରତୀଯ ଜୀବନଦୃଷ୍ଟିକୁ ମଧ୍ଧ ସେମାନେ ପ୍ରଭାବିତ
କଲେ , ଏହି ଘରଟିରେ କେତେ ନୂଆ ଝରକାମାନ ଫିଟାଇଲେ । ପାରସୀ
ଓ ସଂସ୍କୃତି - ଏହି ଦୁଇ ଭାଷାର ସମ୍ମିଶ୍ରଣରେ ଉର୍ଦ୍ଦୁଭାଷାର ଉଦ୍ଭବ ହେଲା ଓ
ତାହାରି ମାଧ୍ଧମରେ ଭାରତୀଯ ସାହିତ୍ଯରେ କେତେ ନୂତନ ଆଙ୍ଗିକର ବି ଜନ୍ମ
ହୋଇ ପାରିଲା । ମୁସଲମାନ ରାଜତ୍ବ ସମଯରେ ହିଁ ଭାରତୀଯ ପ୍ରାଚୀନ ଏକାଧିକ
ଆଧ୍ଧାତ୍ମାଶାସ୍ତ୍ରର ତର୍ଜମା କରାଗଲା ଓ ଏହିପରି ଭାବରେ ତାହା ବିଶ୍ବବ୍ଯାପୀ ହେଲା ,
ବିଶ୍ବସାହିତ୍ଯରେ ପରିଣତ ହେଲା । ଏକଦା ୟେପରି ବୌଦ୍ଧଧର୍ମ ସମ୍ରାଟ ଅଶୋ~
କଙ୍କୁ ଜନ୍ମ ଦେଇଥିଲେ , ଭାରତବର୍ଷର ସେହି ସନାତନ ଘରଟିରେ ମୁସଲମାନୀ
ସଂସ୍ରବ ବାଜି ଫିରୋଜ ତୋଗଲକ ସମ୍ଭବ ହେଲେ , ସମ୍ରାଟ ଆକବର ସମ୍ଭବ
ହେଲେ , ଦାରା ଶିକୋଦ୍୍ ମଧ୍ଧ ସମ୍ଭବ ହେଲେ । ହୁଏତ ଏକ ୟୁକ୍ତିୟୁକ୍ତ
ସ୍ବାଭିମାନ ସହିତ ଏଭଳି କୁହାୟାଇ ପାରିବ ୟେ , କେବଳ ଭାରତବର୍ଷରେ ହିଁ
ତାହା ସମ୍ଭବ ହୋଇ ପାରିଲା ।</p>

<p>ଯୁରୋପୀଯମାନେ ମଧ୍ଧ ପ୍ରଥମେ ଭାରତବର୍ଷକୁ ବାଣିଜ୍ଯ କରି ଆସିଥିଲେ ।
ମୂଳତଃ ବାଣିଜ୍ଯ କରି ଆସିଥିବା ମାର୍କୋପୋଲୋଙ୍କୁ ଚୀନ ଦେଶ ୟେପରି
ଅନ୍ଯାନ୍ଯ ନାନା ଭାବରେ ଆକୃଷ୍ଟ ଓ ପ୍ରଭାବିତ କରିଥିଲା , ଠିକ୍୍ ସେହିପରି ଭାବରେ
ଯୁରୋପୀଯମାନେ ଭାରତବର୍ଷକୁ କେବଳ ସୁବର୍ଣ୍ଣର ଅନୁସନ୍ଧାନରେ ଆସିଥିଲେ
ମଧ୍ଧ ସବୁକାଳ ସକାଶେ ଖାଲି ୟେତିକି ଭିତରେ ମୋଟେ ସୀମାବଦ୍ଧ ହୋଇ
ରହିପାରି ନାହିଁ । ଯୁରୋପ ଓ ଭାରତବର୍ଷ ମଧ୍ଧରେ କେତେ ଅଭିନବ ଆଦାନ
ପ୍ରଦାନର ଦ୍ବାର ଖୋଲିଗଲା । ବୃହତ୍ତର ପୃଥିବୀଟା ୟେ ଆମର ୟାବତୀଯ
ବୃଦ୍ଧିଲାଳସାଠାରୁ ଅନେକ ଅଧିକ ବଡ , ଏହି ସଂସ୍ରବଟି ବଳରେ ଉଭଯ
ଯୁରୋପ ଏବଂ ଭାରତବର୍ଷ ସେହି ସତ୍ଯଟିର ପରିଚଯ ପାଇଥିଲେ ଓ
ତାହାକୁ ଅଙ୍ଗେ ନିଭାଇବାକୁ ବାଧ୍ଧ ହୋଇଥିଲେ । କେବଳ ଯୁରୋପ ଓ ଭାରତ~
ବର୍ଷ ନୁହେଁ , ସମଗ୍ର ବିଶ୍ବ-ପୃଥିବୀ ନିମନ୍ତେ ମଧ୍ଧ ସେହି ସଂସ୍ରବ ୟେ
କେତେ ଉଦଯନର କାରଣ ହୋଇଛି , ଭାରତବର୍ଷ ରାଜନୀତିକ ସ୍ବାଧୀନତା ଲାଭ
କରି ସାରିଲା ପରେ ୟାଇ ସଂପୃକ୍ତ ଆମେ ସମସ୍ତେ ତାହାର ମର୍ମୋପଲବ୍୍ଧି
କରିବାକୁ ସମର୍ଥ ହେଲୁ ।</p>

<p>ଊନବିଂଶ ଶତାବ୍ଦୀର ମଧ୍ଧଭାଗରେ ଜନ୍ମଗ୍ରହଣ କରିଥିବା ଭାରତବର୍ଷର
ତିନି ବରପୁତ୍ର - ରବୀନ୍ଦ୍ର ନାଥ , ମହାତ୍ମାଗାନ୍ଧୀ ଓ ଶ୍ରୀଅରବିନ୍ଦ - ଏମାନଙ୍କୁ ଆମେ
ଭାରତବର୍ଷ ଓ ଯୁରୋପ ମଧ୍ଧରେ ଘଟିଥିବା ସେହି ବହୁ ସମ୍ଭାବନାମଯ ସଂସ୍ରବର
ତିନୋଟି ସର୍ବୋତ୍ତମ ଫଳ ବୋଲି କହିପାରିବା । ଭାରତୀଯ ତଥା ଯୁରୋପୀଯ
ସଂସ୍କୃତିର ଏକ ଗୁଣାତ୍ମକ ମୂଲ୍ଯାଯନ କରିବାର ଭୂମି ଉପରେ ଏହି କଥାଟିକୁ
ଆଦୌ କୌଣସି ବିବାଦର ଅବତାରଣା ନକରି ଅବଶ୍ଯ କୁହାୟାଇ ପାରିବ ୟେ ,
ଉଭଯ ସଂସ୍କୃତି ମଧ୍ଧରେ ୟାହା କିଛି ସ୍ଥାଯୀ ଆକୃତି ଏବଂ ଆହ୍ବାନମାନ ରହିଛି ।
ରବୀନ୍ଦ୍ରନାଥ , ଗାନ୍ଧୀ ଓ ଶ୍ରୀଅରବିନ୍ଦ ନିଜର ଜୀବନ ତଥା ସନ୍ଦେହ ମଧ୍ଧରେ
ଅତ୍ଯନ୍ତ ମୂର୍ତ୍ତ ଭାବରେ ତାହାରି ପ୍ରତିନିଧିତ୍ବ କରିୟାଇଛନ୍ତି । ରବିନ୍ଦ୍ରନାଥ କ'ଣ
କେବଳ କବି ଥିଲେ , ଗାନ୍ଧୀ କ'ଣ ଏକ ରାଜନୀତିକ ସଂଗ୍ରାମର ନେତୃତ୍ବ ଦେବାକୁ
ଆସିଥିଲେ ବା ଶ୍ରୀଅରବିନ୍ଦ କେବଳ ତଥାକଥିତ ଆଧ୍ଧାତ୍ମାବାଦୀମାନଙ୍କର ଏକ
ଅଲଗା ସମ୍ପ୍ରଦାଯ , ଗଢିବାକୁ ଆସିଥିଲେ ? ଅନେକ ଏକଦର୍ଶୀ ଅଥବା
ଅଳ୍ପଗ୍ରାସୀ ସମୀକ୍ଷକ ସେମାନଙ୍କୁ କେବଳ ଏତିକି ବୋଲି ଚିହ୍ନାଇ ଦେବାରେ
ହୁଏତ କେତେ ପ୍ରସନ୍ନତା ଅନୁଭବ କରିଥାନ୍ତି , ମାତ୍ର ସେମାନେ ସେତିକିଠାରୁ
ଅନେକ ଅଧିକ ଥିଲେ । ସେମାନେ ଅନ୍ନଠାରୁ ଆନନ୍ଦ ପର୍ୟ୍ଯନ୍ତ ପ୍ରଲମ୍ବିତ ହୋଇ
ରହିଥିବା ଏକ ବୃହତ୍ତର ଜୀବନର ସନ୍ଦେହବାହକ ଥିଲେ । ସେମାନେ ଏହି
ଭାରତବର୍ଷରେ ଜନ୍ମଗ୍ରହଣ କରିଥିଲେ , ମାତ୍ର ଆମେ ସେଥିଲାଗି ସେମାନଙ୍କୁ
କେବଳ ଭାରତବର୍ଷର ବୋଲି କହିପାରିବା କି ? ଯୁରୋପୀଯ ସମାଜମୂଲ୍ଯ ଏବଂ
ବାସ୍ତବମୂଳ ଜୀବନବୃଦ୍ଧି ଦ୍ବାରା ସେମାନଙ୍କର ବିଚାର ଏବଂ ବାକ୍ଯଗୁଡିକ ବହୁ~
ପରିମାଣରେ ଅନୁପ୍ରେରିତ ହୋଇ ରହିଥିଲା ।</p>

<p>ସେମାନେ ତିନିଜଣୟାକ ଯୁରୋପୀଯ ଏକଦର୍ଶୀମାନଙ୍କ ଆଖିରେ ସତେ~
ଅବା କେବଳ ଭାରତୀଯ ପରି ଦିଶୁଥିଲେ ଓ ଏକଦର୍ଶୀ ଭାରତୀଯମାନଙ୍କ ଆଖିରେ
କେବଳ ୟୁରୋପୀଯମାନଙ୍କ ପରି ଦିଶୁଥିଲେ । ଏବେ ମଧ୍ଧ ସେହିପରି ଦିଶୁଛନ୍ତି ।
ଜର୍ମାନୀର ମହାକବି ଗୁଏଟେ ଏକଦା ତାଙ୍କର ଗୋଟିଏ ଗ୍ରନ୍ଥରେ ଘୋଷଣା କରି
କହିଥିଲେ ୟେ , ୟେଉଁ ବ୍ଯକ୍ତି ଆପଣାକୁ ତଥା ଅନ୍ଯମାନଙ୍କୁ ୟଥାର୍ଥ ଭାବରେ
ଚିହ୍ନିବ , ସିଏ ୟୁଗପତ୍୍ ଭାବରେ ଏହି କଥାଟିକୁ ମଧ୍ଧ ଉପଲବ୍୍ଧି କରିପାରିବ
ୟେ ପ୍ରାଚ୍ଯକୁ ପାଶ୍ଚାତ୍ଯଠାରୁ ଅଥବା ପାଶ୍ଚାତ୍ଯକୁ ପ୍ରାଚ୍ଯଠାରୁ ଅଲଗାକରି କଦାପି
ଦେଖି ହେବନାହିଁ । ସେହି ଦୁଇଟି ମଧ୍ଧରୁ କୌଣସିଟିକୁ କେବଳ ତାହାରି
ମଧ୍ଧରେ ବିଚ୍ଛିନ୍ନ ଏବଂ ସ୍ବଯଂସମ୍ପୂର୍ଣ୍ଣ ଭାବରେ ବିଚାର କରାୟାଇ ପାରିବ ନାହିଁ ।
ପ୍ରାଚ୍ଯର ହୃଦଯକୁ ଭେଦ କଲେ ଆମେ ସେଠାରେ ପାଶ୍ଚାତ୍ଯର ହୃଦଯକୁ ମଧ୍ଧ
ଅବଶ୍ଯ ଭେଟିବା ଏବଂ ସେହିପରି ପାଶ୍ଚାତ୍ଯର ହୃଦଯକୁ ଭେଦକଲେ ଆମେ
ସେଠାରେ ପ୍ରାଚ୍ଯର ହୃଦଯଟିକୁ ମଧ୍ଧ ଦେଖିବାକୁ ପାଇବା । //
ସମନ୍ବଯର ଅସଲ ଅର୍ଥ ହେଉଛି ଅସାରକୁ ଛାଡି
ଅସଲ ସାରକୁ ଲୋଡିବା । ନିଜର ପିତୁଳାଟିକୁ ପ୍ରଧାନତଃ ନିଜର ଏକ ସମ୍ପତ୍ତି
ବୋଲି ଭାବି ତାହାକୁ ହିଁ ସର୍ବସାର ବୋଲି ଦାବୀ କରିବା - ତାହା କଦାପି
ସମନ୍ବଯ ନୁହେଁ । ଅଥବା , ବିଭିନ୍ନ ଆଡୁ ବିଭିନ୍ନ ସନ୍ତକ ଆଣି ନିଜର ପିତୁଳାଟି
ସହିତ ଏକାଠି କରି ବାନ୍ଧିଦେବା ; ସେଇଟି ମଧ୍ଧରୁ ୟଥାର୍ଥ ସମନ୍ବଯଠାରୁ
ଅନେକ ଦୂରକୁ ନେଇ ୟାଇଥାଏ ।</p>

<p>ତନ୍ତ୍ରଶାସ୍ତ୍ରରେ ମନ୍ମାଥ ଓ ଜଗନ୍ନାଥର ତୁଳନାତ୍ମକ ଆଲୋଚନା କରା~
ୟାଇଛି । ତନ୍ତ୍ରଶାସ୍ତ୍ର ଅର୍ଥାତ୍୍ ତାନ୍ତ୍ରିକ ସାଧନାର ଶାସ୍ତ୍ର । ଚେତନାର ଏକ
ଉତ୍ତରୋତ୍ତର ଅଗ୍ରଗତି ଅର୍ଥାତ୍୍ ସଂପ୍ରସାରଣକୁ ହିଁ ସାଧନା କୁହାୟାଏ । ଚେତନା
ସଂପ୍ରସାରିତ ହେଲେ ପୌତ୍ତଳିକତାର ୟାବତୀଯ ମୋହ ତୁଟିୟାଏ । ଅର୍ଥାତ୍୍ ,
ପିତୁଳାମାନେ ରହନ୍ତି ସତ , ମାତ୍ର ସେମାନେ ଆଦୌ ବାନ୍ଧି ପକାନ୍ତି ନାହିଁ ।
ମନ୍ମାଥ ଜଗନ୍ନାଥରେ ପରିଣତ ହୁଅନ୍ତି । ପିଣ୍ଡ ଭିତରେ ବ୍ରହ୍ମାଣ୍ଡ ଦେଖାୟାଏ ।
ମନ୍ମାଥର ମାଧ୍ଧମରେ ଜଗନ୍ନାଥ ୟାଏ ପହଞ୍ଚି ପାରିବା , ହୁଏତ ତାହାକୁ ହିଁ ଆମେ
ଜଗନ୍ନାଥ ଚେତନା ବୋଲି କହିପାରିବା । ସେହି ଚେତନାକୁ ଆମେ ଉପଲବ୍୍ଧିର
ଅସଲ ଆକାର ରୂପେ ଗ୍ରହଣ କରି ନିଅନ୍ତି । ଓଡିଶାର ଏକ ଜଗନ୍ନାଥ-ସାଧନାର ମଧ୍ଧ
କଳ୍ପନା କରାୟାଇପାରେ । ତନ୍ତ୍ରଶାସ୍ତ୍ରରେ ତାହାରି ସୂଚନା ଦିଆୟାଇଛି । ମାତ୍ର
ସେ ଜଗନ୍ନାଥ ଏ ଜଗନ୍ନାଥ ନୁହଁନ୍ତି । ଓଡିଶାର ଏକ ବଜ୍ରୟାନପୋଥିରେ
ଗ୍ରନ୍ଥବନ୍ଦନାରେ ୟାହାଙ୍କୁ ସର୍ବଜିନବରାର୍ଚ୍ଚିତ ଜଗନ୍ନାଥ ବୋଲି କୁହାୟାଇଛି ,
ସିଏ ମଧ୍ଧ ଆମ ଶ୍ରୀକ୍ଷେତ୍ରର ଜଗନ୍ନାଥ ନୁହଁନ୍ତି । ଦକ୍ଷିଣ ଭାରତର ରୁଦ୍ରଭଟ୍ଟ
ଖ୍ରୀଷ୍ଟ ଦ୍ବାଦଶ ଶତାବ୍ଦୀରେ କନ୍ନଡ ଭାଷାରେ " ଜଗନ୍ନାଥ ବିଜଯ " ବୋଲି ୟେଉଁ
ଗ୍ରନ୍ଥ ଲେଖିଛନ୍ତି , ସେଥିରେ ମଧ୍ଧ ଆମ ଘରର ଏହି ଜଗନ୍ନାଥଙ୍କ ସହିତ ଆଦୌ
କୌଣସି ସମ୍ବନ୍ଧ ନାହିଁ । ଆମର ଏଇ ସାନଘରଟିର ସାଧନା ତଥା ଆଗ୍ରହକୁ
ସମଗ୍ର ବିଶ୍ବନୀଡଟି ଲାଗି ଉଦ୍ଧିଷ୍ଟ କରି ରଖିବା , ତାହାହିଁ ବିଶ୍ବଚେତନା ଓ ଏହି
ବିଶ୍ବଚେତନାର ଅନ୍ଯ ଏକ ନାମ ହେଉଛି ଜଗନ୍ନାଥ-ଚେତନା । ସେହି
ଚେତନାକୁ ନେତ ପିନ୍ଧାଇ ପଣ୍ଡା ପଢିଆରୀ ଖଟାଇ ୟିବ ଏକ ବିଗ୍ରହ କରି
ବସାଇଦେବ ଓ ତାକୁ ଆମର ଏହି ସାନ ଡିହଟିର ସବାବଡ ସାମନ୍ତ ବୋଲି
ଏବେ ବଡ ପାଟିରେ ୟିଏ କହିବାରେ ଲାଗିଛି , ସିଏ ଅସଲ ଜଗନ୍ନାଥ-ଚେତନାର
କୌଣସି ପତ୍ତା ହିଁ ପାଇବ ନାହିଁ । ଜଗନ୍ନାଥ-ଚେତନା କୌଣସି ଜାତି ବା
ଜନପଦର ସମ୍ପତ୍ତି ନୁହେଁ , ତାହାକୁ କଦାପି ଏକ ଦେଉଳ ବା ଦେବସ୍ଥାନ
ଭିତରେ ଆବଦ୍ଧ କରି ରଖାୟାଇ ପାରିବ ନାହିଁ । ଭିତରେ ଅନେକ ଦେଉଳ
ଭାଙ୍ଗିଲେ ୟାଇ ଅସଲ ସମଗ୍ରତାଟିର ଦର୍ଶନ ମିଳିପାରିବ । ବାଟକାନ୍ଥର ଅନେକ
ମୂର୍ତ୍ତିକୁ ଡେଇଁ ପାରିଲେ ୟାଇ ଅସଲ ମର୍ମଟି ସହିତ ହୃତ୍୍ସ୍ପର୍ଶ ସମ୍ଭବ ହୋଇ
ପାରିବ ।</p>

<p>ଏହି ଅବକାଶରେ ୟାବତୀଯ ଆଲୋଚନାରେ ଓଡିଶାର ପଞ୍ଚସଖା ୟେ
ଏକ ବିଶେଷ ସ୍ଥାନ ଅଧିକାର କରିବେ , ସେ କଥା ଅତ୍ଯନ୍ତ ସ୍ପଷ୍ଟ ହୋଇୟିବା
ଉଚିତ । ଓଡିଶାର ସନ୍ଥ-ପରମ୍ପରା ଭାରତୀଯ ସନ୍ଥ-ପରମ୍ପରା ଲାଗି ୟେ ଏକଦା
ଏକ ବଳିଷ୍ଠ ଅବଦାନ ୟୋଗାଇ ଦେଇଥିଲା , ସଂସ୍କୃତିକୁ ପ୍ରଧାନତଃ ଏକ
ଚିଦ୍୍ବିସ୍ତାର ବୋଲି ବୁଝୁଥିବା ପ୍ରତ୍ଯେକ ଓଡିଶାବାସୀ ସେକଥାକୁ ଅବଶ୍ଯ
ସ୍ମରଣ ରଖିଥିବ । ଓଡିଶାର ସନ୍ଥମାନେ ଏହି ଭୂମିର ଥିଲେ , ତଥାପି ସେମାନେ
କେବଳ ଏହି ଭୂମିର ନ ଥିଲେ । ସେମାନଙ୍କର ଚେର ଗୁଡିକ ଆମର ସ୍ଥାନୀଯ
ଏହି ଭୂମିଟିକୁ ଭେଦ କରି ରହିଥିଲା ; ମାତ୍ର ଚେର ୟେତିକି ଗଭୀରକୁ ଭେଦି
ପାରିଲେ ତାହାରିଦ୍ବାରା ଏକ ବିଶ୍ବ-ଜନତାର ସ୍ତରକୁ ସ୍ପର୍ଶ କରି ହୁଏ ଏବଂ ସେହି
ବିଶ୍ବଜନତାର ପରିପ୍ରେକ୍ଷୀରେ ହିଁ ୟାବତୀଯ ସ୍ଥାନୀଯତା ଓ ସ୍ଥାନୀଯ ଭୂମିର ବିରାଟ
ବ୍ଯଞ୍ଜନା ରୂପଟିକୁ ଆଘ୍ରାଣ କରିହୁଏ , ଓଡିଶାର ସନ୍ଥମାନେ ନିଜର ଚେରଗୁଡିକୁ
ସେତିକି ଗଭୀରକୁ ଭେଦାଇ ପାରିଥିଲେ । ସେମାନେ ତୁଚ୍ଛା ବାହ୍ଯିକତା ବ୍ଯତୀତ
ଆଉ କୌଣସି କିଛିକୁ ଅସ୍ବୀକାର କରି ନଥିଲେ । ମୂର୍ତ୍ତି ସେପାଖକୁ ଦେଖି
ପାରୁଥିବା ମଣିଷ କଦାପି ମୂର୍ତ୍ତିକୁ ଭଯ କରେ ନାହିଁ । ଓଡିଶାର ସନ୍ଥମାନେ
ବାଟରେ କେତେ କେତେ ଖଣ୍ଡ କଳ୍ପନା ଓ ଖଣ୍ଡ ଦେବତାମାନଙ୍କୁ କାଟି
ଦେବନାଶନ ବଟୟାଏ ପହଞ୍ଚି ପାରିଥିଲେ । ଖ୍ରୀଷ୍ଟ ପଞ୍ଚଦଶ ଓ ଷୋଡଶ
ଶତାବ୍ଦୀରେ ସମଗ୍ର ଭାରତବର୍ଷରେ ପ୍ରଚଳିତ ବାହ୍ଯିକତା ଗୁଡିକ ବିରୁଦ୍ଧରେ
ସନ୍ଥମାନେ ୟେଉଁ ବଳିଷ୍ଠ ପ୍ରତିବାଦ ଜଣାଇଥିଲେ , ଓଡିଶାର ସନ୍ଥମାନେ ସେହି
ଉଦ୍୍ବେଳନ ଲାଗି ଆପଣାର ଏକ ଗୌରବମଯ ଅବଦାନ ଆଣି ପହଞ୍ଚାଇ
ପାରିଥିଲେ । ସେମାନେ ରାଜଧର୍ମର ୟାବତୀଯ ବାଡକୁ କାଟି ଦେଇଥିଲେ ,
ତତ୍କାଳୀନ ସମାଜର ଅନେକ ଅସୁନ୍ଦରତାକୁ ଅସ୍ବୀକାର କରି ମନୁଷ୍ଯକୁ ,
ପ୍ରତ୍ଯେକ ମନୁଷ୍ଯକୁ ତାର ଦିବ୍ଯ ଅର୍ଥାତ ସମ୍ମାନିତ ପରିଚଯରେ ଚିହ୍ନିବା
ନିମନ୍ତେ ଆହ୍ବାନ ଦେଇଥିଲେ । ସାଧକର ସାଧନା ୟେ ଏକ ଲୋକ ଜାଗରଣର
ମଧ୍ଧ କାରଣ ହୋଇପାରେ , ଜଣେ ଆସଲ ସନ୍ଥ ୟେ ସତେଅବା ଏକ ଆଧ୍ଧାତ୍ମିକ
ପ୍ରୟୋଜକ ଦ୍ବାରା ପ୍ରେରିତ ହୋଇ ଏକାବେଳେ ବହୁଜନଙ୍କର ଉଦଯ ଲାଗି
ସାମୂହିକ ଜୀବନର ସର୍ବନିମ୍ନ ସ୍ତରକୁ ଓହ୍ଲାଇ ଆସିପାରେ ଓଡିଶାର ପଞ୍ଚସଖା
ସେହି କଥାଟିର ଉଜ୍ଜ୍ବଳ ଦୃଷ୍ଟାନ୍ତ ହୋଇ ସବୁଦିନ ରହିଥିବ ଏବଂ ଆମକୁ
ପ୍ରେରଣା ଦେବାରେ ଲାଗିଥିବ । //
ସେହି ଅଞ୍ଚଳର କୁଜି ରାଜା ଓ ଜମିଦାରମାନେ ମଧ୍ଧ ସେହି ଦୂରୂହତାକୁ ଆହୁରି ବଢାଇ
ଦେବାରେ ସାହାୟ୍ଯ କରୁଥିଲେ । ତେଣେ ସମ୍ବଲପୁର ଖଣ୍ଡଟା ଖାସମାହାଲ
ଥିଲା ଏବଂ ସୁଦୂର ନାଗପୁରଠାରୁ ତାହାର ଶାସନ ହେଉଥିଲା । ସେତେବେଳେ
ଓଡିଶା କେଉଁଠି ଥିଲା ।</p>

<p>ପ୍ରାଚ୍ଯ ବା ଭାରତବର୍ଷ ପାଖରେ ୟାହା ନ ଥିଲା , ସେତେବେଳେ
ପାଶ୍ଚାତ୍ଯର ଖାସ୍୍ ତାହାହିଁ ରହିଥିଲା ବୋଲି ପ୍ରତୀତ ହୋଇଥିଲା । ପାଶ୍ଚାତ୍ଯ
ପାଖରେ ଜ୍ଞାନ ଥିଲା , ବିଜ୍ଞାନ ଥିଲା , ପୃଥିବୀୟାକ ସେମାନଙ୍କର ସାମ୍ରାଜ୍ଯ ରହିଥିଲା
ଓ ଆଧୁନିକ ସଭ୍ଯତା କହିଲେ ସେହି ପାଶ୍ଚାତ୍ଯକୁ ହିଁ ବୁଝାଉଥିଲା । ଏଠି
ଭାରତରେ ୟେଉଁମାନେ ପାଶ୍ଚାତ୍ଯର ସେହିସବୁ ଆବେଦନ ଦ୍ବାରା ଉଦ୍୍ବୁଦ୍ଧ
ହେଲେ , ସେମାନେ ତାହାର ସେହି ନୂତନ ପ୍ରାଣ ପବନଗୁଡିକୁ ଗ୍ରହଣ କରି~
ନେଇ ପାରିଲେ ୟାଇ ଭାରତର ଉଦ୍ଧାର ବା ନିଦ୍ରାଭଙ୍ଗ ହେବ ବୋଲି ଭାବିବାକୁ
ଆରମ୍ଭ କଲେ । ପାଶ୍ଚାତ୍ଯର ଆବିଷ୍କାର ଭାରତବର୍ଷର ଅତୀତ ଭିତରେ ରହିଥିବା
ସ୍ଥାଯୀ ରତ୍ନଗୁଡିକୁ ଆବିଷ୍କାର କରିବା ଲାଗି ମଧ୍ଧ ଉଦ୍୍ବୋଧନ ଦେଲା । ଏକା~
ବେଳକେ ଦୁଇଟିୟାକ କଥା ଘଟିଲା ; ବାହାର ଆଡକୁ ଗବାକ୍ଷଗୁଡିକ ଖୋଲିଗଲା
ଏବଂ ଏଣେ ଏକ ନୂତନ ଗୌରବବୋଧ ତଥା ଗଭୀରତା ଦେଇ ନିଜକୁ
ଚିହ୍ନିବା ଲାଗି ମଧ୍ଧ ଅବସର ମିଳିଥିଲା । ଭାରତବର୍ଷରେ ଆଧୁନିକ ଉଦ୍୍ବେଳନ~
ଗୁଡିକର ମୂଲଦୁଆ ପଡିଲା ।</p>

<p>ସେତେବେଳକୁ ଓଡିଶାରେ ଦୁଇ ତିନୋଟି ଇଂରାଜୀ ସ୍କୁଲ ଆରମ୍ଭ
ହୋଇ ଗଲାଣି । ଥିଲାଘରର ପିଲାମାନେ କଲିକତା ପର୍ୟ୍ଯନ୍ତ ୟାଇ ଏଫ.ଏ. ,
ବି.ଏ. ପ୍ରଭୃତି ଉଚ୍ଚ ପାଠ ପଢି ଇଂରାଜମାନଙ୍କର ଦପ୍ତରରେ ବଡ ହାକିମ
ହେବାକୁ ମଧ୍ଧ ମନ କଲେଣି । ରକ୍ଷଣଶୀଳମାନେ ତାହାର ପ୍ରତିରୋଧମାନ
କରିବାକୁ ଆରମ୍ଭ କଲେଣି । ନୂଆର ପ୍ରତିରୋଧ କଲେ ତଥାପି ତାକୁ ପାରି
ହେବ ନାହିଁ ବୋଲି ଉତ୍ତମ ରୂପେ ଜାଣିଥିଲେ ମଧ୍ଧ ପୁରୁଣା ସତେଅବା ସ୍ବସଂସ୍କାର~
ବଶତଃ ତାହାର ପ୍ରତିରୋଧ କରୁଛି । ସେତିକି ବେଳେ ଓଡିଶାର ପୁରୋଗାମୀ
ସମ୍ବାଦପତ୍ର ` ଉତ୍କଳ-ଦୀପିକା ' ଜନ୍ମଲାଭ କଲା । ତରୁଣ ଶ୍ରୀ ପ୍ଯାରୀମୋହନ ଆଚାର୍ୟ୍ଯ
` ଉତ୍କଳପୁତ୍ର ' ବାହାର କଲେ । ସେତେବେଳେ ଓଡିଶାର ସର୍ବୋଚ୍ଚ ଭବିଷ୍ଯ~
ଦୃଷ୍ଟି ସମ୍ଭବତଃ ତାଙ୍କରି ଲେଖନୀରୁ ହିଁ ବ୍ଯକ୍ତ ହେଉଥିଲା । ପ୍ଯାରୀମୋହନ
ତାଙ୍କର ନିରନ୍ତର ସଂଘର୍ଷରତ ଅଥଚ ପୁରୋଦୃଷ୍ଟି-ସମୁଜ୍ଜ୍ବଳ ସ୍ବଳ୍ପ
ଜୀବନକାଳ ମଧ୍ଧରେ ଆଗକୁ ଅନାଇ ୟେତିକି ଦୂରକୁ ଦେଖି ପାରୁଥିଲେ ଓ
ତାହାକୁ ବାସ୍ତବରୂପ ଦେବାକୁ ୟାଇ ୟେତିକି କରିପାରିଥିଲେ , ସେକଥା
ଭାବିଲେ ବର୍ତ୍ତମାନ ମଧ୍ଧ ବିସ୍ମିତ ହେବାକୁ ପଡେ । ସିଏ ଏକାଧାରାରେ ଉଦ୍ଯମ
ଓ ସାହସର ପ୍ରତୀକ ଥିଲେ । ଗୋଟିଏ ପ୍ରସଙ୍ଗରେ ସେ ଲେଖିଛନ୍ତି ; " ୟାହା
ମନୁଷ୍ଯ କରିଅଛି , ତାହା ଅବଶ୍ଯ ମନୁଷ୍ଯ କରିପାରିବ । ଦିନରେ ନ ହେବ ,
ସହସ୍ର ଦିନକେ ହେବ । ସହସ୍ର ଦିନେ ନ ହୁଏ , ସହସ୍ର ବର୍ଷରେ ହେବ ,
ୟତ୍ନ କଲେ ଅବଶ୍ଯ ହେବ । " ଭାଗ୍ଯବାଦୀ ତତ୍କାଳୀନ ଓଡିଶାରେ ଏହି କଥା~
ଗୁଡିକ ସେତେବେଳେ ସତେ ଅବା ଶାପ ମୋଚନକାରୀ ହରିନାମ ଭଳି
ଶୁଭିଥିବ । ଓଡିଆ ସାହିତ୍ଯରେ ମଧ୍ଧ ଏକ ନୂତନ ଉତ୍ତରଣର ୟୁଗ ଆସୁଛି
ବୋଲି ପ୍ଯାରୀମୋହନ ଅନୁଭବ କରିଥିଲେ । ସେ ଶିକ୍ଷାର ନୂତନ ସଂଜ୍ଞା ନିର୍ଣ୍ଣଯ
କରିଥିଲେ । ପରିସ୍ଥିତି ଦ୍ବାରା ୟେତେ ବିଚ୍ଛିନ୍ନ ଓ ସ୍ବଳ୍ପ ପରିଧି ହୋଇ ରହିଥିଲେ
ମଧ୍ଧ ଓଡିଶା ୟେ ବୃହତ୍ତର ଭାରତବର୍ଷ ମଧ୍ଧରେ ଅବସ୍ଥିତ ରହିଥିଲା ,
ସମସାମଯିକ ଅନ୍ଯ ସମସ୍ତଙ୍କ ତୁଳନାରେ ପ୍ଯାରୀମୋହନ ସେହି ବିଷଯରେ
ସମ୍ଭବତଃ ସବୁଠାରୁ ଅଧିକ ସଚେତନ ଥିଲେ । ସିଏ ଓଡିଶାକୁ ଭାରତୀଯ
ମହାସ୍ରୋତ ମଧ୍ଧରେ ହିଁ ଏକ ଅଙ୍ଗ ରୂପେ ଦେଖିପାରୁଥିଲେ ଏବଂ ସେହିପରି
ଦେଖିବାକୁ ହିଁ ୟଥାର୍ଥ ଦେଖିବା ବୋଲି ଅନୁଭବ କରୁଥିଲେ ।</p>

<p>ଏହି ଶତାବ୍ଦୀର ଆରମ୍ଭ ବେଳକୁ ଭାରତୀଯ ଜାତୀଯ କଂଗ୍ରେସ ନିଜର
ଶାସନ ଛାଡି ବାହାରି ଆସିଲେଣି । ବଙ୍ଗ-ବିଚ୍ଛେଦ-ବିରୋଧୀ ଆନ୍ଦୋଳନ
ପରେ ସେହି ଗୋଟିଏ ସଂସ୍ଥା ଭିତରେ ମଧ୍ଧ ଚରମପନ୍ଥୀ ଓ ନରମପନ୍ଥୀଙ୍କ
ମଧ୍ଧରେ ସଂଗ୍ରାମର ପନ୍ଥା ନେଇ ବିବାଦ ହେଲାଣି । ଭାରତକୁ ଇଂରେଜମାନଙ୍କ
ହାତରୁ ମୁକ୍ତ କରିବା ଲାଗି ଦଳେ ସଂତ୍ରାସବାଦର ଆଶ୍ରଯ ମଧ୍ଧ ନେଲେଣି ।
ଓଡିଶାରେ ସତେଅବା ତାହାର ଆଦୌ କୌଣସି ଖବର ପହଞ୍ଚି ନାହିଁ ।
ଓଡିଶାରେ ସାଧାରଣ ରାଜନୀତିର ସ୍ତରରେ ୟାହାକିଛି ହେଉଛି , ତାହା ସତେ
ୟେପରି ଅତ୍ଯନ୍ତ ଏକୁଟିଆ ଭାବରେ ହେଉଛି । ବାହାର ସହିତ ସତେ ଅବା
କୌଣସି ସଂସ୍ରବ ନାହିଁ । 1904 ମସିହାରେ ୟେତେବେଳେ ଏସିଆର
ଜାପାନ ଦେଶ ଯୁରୋପର ଅତିକାଯ ରୁଷିଆକୁ ପରାସ୍ତ କରିଥିଲା , ସେତେବେଳେ
ତାହା ଭାରତର ସ୍ବାଧୀନତାକାମୀମାନଙ୍କୁ କେତେ ଉତ୍ସାହ ଓ ବଳ
ଆଣିଦେଇଥିଲା , କେତେ ଆଶାର ସଞ୍ଚାର କରିଥିଲା ଓଡିଶାରେ ସେହି
ସମ୍ବାଦଟି ସତେ ଅବା ଆଦୌ ପହଞ୍ଚି ନ ଥିବା ପରି ମନେ ହୋଇଥାଏ । //
ଭାଷା , ଜାତି ଅଥବା ସଂସ୍କୃତି ଅନୁସାରେ ପୃଥିବୀରେ ୟେତେଠାରେ ରାଜ୍ଯ
ଅଥବା ଦେଶ ଗୁଡିକର ସୀମାନିର୍ଦ୍ଧାରଣ କରାୟାଇଛି ତାହା କଦାପି ଗଣିତର
ଗାର ଟାଣିଲା ପରି ଗୋଷ୍ଠୀଗୁଡିକ ଅନୁସାରେ ସୀମାଗୁଡିକର ନିର୍ଦ୍ଧାରଣ
କରିପାରି ନାହିଁ । ଭାରତରେ ମଧ୍ଧ ଭାଷା ସୂତ୍ରରେ ରାଜ୍ଯଗୁଡିକର ଗଠନ
କରାୟାଇଛି । ତାହାର ଅର୍ଥ ନୁହେଁ ୟେ ଓଡିଶାରେ କେବଳ ଓଡିଶା ଭାଷାଭାଷୀ
ରହିଛନ୍ତି ଓ ଆନ୍ଧ୍ରରେ କେବଳ ତେଲୁଗୁ-ଭାଷାଭାଷୀମାନେ ରହିଛନ୍ତି ।
ହରିଆନାରେ କେବଳ ହିନ୍ଦୀଭାଷୀ ରହିଛନ୍ତି ଓ ପଞ୍ଜାବରେ କେବଳ ପଞ୍ଜାବୀ
ରହିଛନ୍ତି । ଓଡିଶାର ଜଙ୍ଗଲରୁ ତେଲୁଗୁମାନେ କାଠ କାଟି ନେଇଗଲେ ,
ଆନ୍ଧ୍ର ସରକାର ଓଡିଶା ସୀମାରେ ଭିତରେ ପଶି ଖଜଣା ଆଦାଯ କଲେ , ବିହାର
ସରକାର ଓଡିଆଙ୍କ ପ୍ରତି ଅବିଚାର କଲେ , ଆମର ଖବର କାଗଜମାନଙ୍କରେ
ୟେତେବେଳେ ଏହିସବୁ ଖବର ବାହାରେ ଓ ଏକ ନିର୍ଦ୍ଦିଷ୍ଟ ପ୍ରକାରର ଉତ୍ତାପ
ସୃଷ୍ଟି କରେ , ଭାଷାର ସୂତ୍ରରେ ରାଜ୍ଯର ଗଠନ ହୋଇଥିଲେ ମଧ୍ଧ ଭାଷା ତଥା
ଗୋଷ୍ଠୀ-ସମ୍ବନ୍ଧୀ ସମସ୍ଯାଗୁଡିକର ସଂପୂର୍ଣ୍ଣ ସମାଧାନ ୟେ ତଥାପି ହୋଇ
ନାହିଁ , ସେତେବେଳେ ସେସବୁ ତାହାରି ସୂଚନା ଦେଇଥାଏ । ଆମ
ପୃଥିବୀରେ ମାନବିକ ମୂଲ୍ଯଗୁଡିକର ସାଦର ପ୍ରତିଷ୍ଠା ହୋଇପାରିବା ପର୍ୟ୍ଯନ୍ତ ଆମେ
ଆପଣାର ପ୍ରାଯ ସକଳ ଅନୁରକ୍ତି ଏବଂ ଅନୁରାଗଗୁଡିକୁ ଆମର ସାନ ସାନ ଗୋଷ୍ଠୀ
ସହିତ ରହିଥିବା ମୁଗ୍୍ଧ ତଥାକଥିତ ଅଧିକ ମହତ୍ତ୍ବପୂର୍ଣ୍ଣ ସମ୍ବନ୍ଧଗୁଡିକ
ସହିତ ବାନ୍ଧି ରଖିବା ସକାଶେ ହିଁ ଆମର ଚାରିପାଖରୁ ଅପେକ୍ଷାକୃତ ଅଧିକ
ପ୍ରରୋଚନା ଏବଂ ମନ୍ତ୍ରଣା ନିଶ୍ଚଯ ପାଉଥିବା ଓ ଅଧିକତର ଭାବରେ ସେହିଗୁଡିକ
ଦ୍ବାରା ହିଁ ପରିଚାଳିତ ହେଉଥିବା ।</p>

<p>ଓଡିଶା ବାହାରେ ୟେଉଁ ଓଡିଆମାନେ ଅନ୍ଯ ରାଜ୍ଯଗୁଡିକରେ ରହିଛନ୍ତି ,
ସେମାନେ କଣ ବିଦେଶରେ , ଶତ୍ରୁହାତରେ ବା କୌଣସି ବନ୍ଦୀଶାଳାରେ
ଅଛନ୍ତି ? ଓଡିଶାର ସୀମା ଭିତରେ ଉପାନ୍ତ ଅଞ୍ଚଳମାନଙ୍କରେ ୟେଉଁ ବଙ୍ଗଭାଷୀ
ବିହାରୀ ଅଥବା ତେଲୁଗୁମାନେ ଅଛନ୍ତି , ସେମାନେ କଣ ଆମଘରେ ବନ୍ଦୀ
ହୋଇ ରହିଛନ୍ତି , ଶତ୍ରୁହାତରେ ଅଛନ୍ତି ? ଗଣତନ୍ତ୍ରର ଆଲୋଚନା କରିବାକୁ
ୟାଇ ଜଣେ ଇଉରୋପୀଯ ଗବେଷକ ପଣ୍ଡିତ ମତ ଦେଇ କହିଛନ୍ତି ୟେ ,
କୌଣସି ରାଜ୍ଯ ବା ରାଜ୍ଯଖଣ୍ଡରେ ବାସ କରୁଥିବା ଏକ ସଂଖ୍ଯାଗରିଷ୍ଠ ଗୋଷ୍ଠୀ
ସେହି ରାଜ୍ଯରେ ରହିଥିବା ସଂଖ୍ଯାଲଘୁ ମାନଙ୍କ ପ୍ରତି ୟେଉଁ ପ୍ରକାରର
ବ୍ଯବହାର ଦେଖାଏ , ତାହାରି ଦ୍ବାରାହିଁ , ସେଠାରେ କାର୍ୟ୍ଯକାରୀ ହେବ ବୋଲି
ସ୍ବୀକୃତି ହୋଇଥିବା ଗଣତନ୍ତ୍ରର ଅସଲ ମାନ ଓ ମର୍ୟ୍ଯାଦାଟି ଅନେକାଂଶରେ
ନିରୂପିତ ହୋଇଥିଲା । ଆମ ରାଜ୍ଯରେ ୟେଉଁ ସଂଖ୍ଯାଲଘୁ ଅନ୍ଯ-ଭାଷାଭାଷୀ
ଅଛନ୍ତି , ଅଥବା ଆମ ଭାଷାଭାଷୀ ୟେଉଁମାନେ ଅନ୍ଯ କୌଣସି ରାଜ୍ଯରେ ସଂଖ୍ଯା
ଲଘୁ ଭାବରେ ରହିଛନ୍ତି , ସେମାନେ ଓଡିଶା , ବଙ୍ଗ , ଅଙ୍ଗ ଅଥବା ଆନ୍ଧ୍ର ୟେଉଁ
ରାଜ୍ଯରେ ବାସ କରୁଥାନ୍ତୁ ପଛକେ , ସେମାନେ ସମସ୍ତେ ଏହି ଭାରତବର୍ଷରେ ହିଁ
ଅଛନ୍ତି । ଭାରତବର୍ଷର ୟେ କୌଣସି ରାଜ୍ଯରେ ୟେ କୌଣସି ଭାଷାଭାଷୀ
ରହିଥାଉ ପଛକେ , ଆମର ଅସଲ ସାଧାରଣ ଭୂଇଁଟିରେ ତାର ଚେର ଅବଶ୍ଯ
ରହିଛି ଏବଂ ତେଣୁ ଆମେ ସଂପୃକ୍ତ ସମସ୍ତେ ତାକୁ ମୂଳତଃ ସେହି ପରିଚଯରେ
ହିଁ ଆମ ନିଜ ସହିତ ସାମିଲ କରିନେବା । ଆମର ସର୍ବୋତ୍ତମ ସାଂସ୍କୃତିକ
ମୂଲ୍ଯଗୁଡିକ ଆମର ସାମାଜିକ ଜୀବନ ବ୍ଯବସ୍ଥାଗୁଡିକୁ ସତକୁସତ ଭେଦକରି
ରହିଥିଲେ ହୁଏତ ଆମେ ଅତ୍ଯନ୍ତ ସ୍ବାଭାବିକ ଭାବରେ ଠିକ୍୍ ତାହାହିଁ ଅନୁଭବ
କରୁଥାନ୍ତୁ । ମାତ୍ର , ଭାରତବର୍ଷ ୟେପରି ତିଆରି ହେବା କଥା , ତାହା ସତକୁ ସତ
କଣ ସେହିପରି ତିଆରି ହେଲାଣି କି ? ଏବଂ , ଭାରତବର୍ଷ ୟଦି ତାହା ହୋଇ
ପାରି ନାହିଁ , ତେବେ ଓଡିଶା ତିଆରି ହୋଇ ସାରିଲାଣି ବୋଲି କିଏ କହି
ପାରିବ ।</p>

<p>ଭାଷା ସୂତ୍ରରେ ରାଜ୍ଯଗୁଡିକର ଗଠନ ହୋଇଥିବା ସତ୍ତ୍ବେ ପ୍ରତ୍ଯେକ
ରାଜ୍ଯରେ ୟେ ଅନ୍ଯ ଭାଷାଭାଷୀମାନେ ତଥାପି ରହିଛନ୍ତି , ଆଉ ଏକ ଦୃଷ୍ଟିରୁ
ବିଚାର କଲେ ତାହା ହୁଏତ ଆମ ସମସ୍ତଙ୍କ ଲାଗି ଏକ ବିଶେଷ ସୁୟୋଗ ବୋଲି
ଅନୁଭୂତ ହେବ । ରାଜ୍ଯ ଉପାନ୍ତର ୟେଉଁ ଭାବରେ ଏପାଖ ସେପାଖ ହୋଇ
ଦୁଇଟି ଭାଷାଭାଷୀ ବାସ କରୁଛନ୍ତି , ହୁଏତ ଶତାବ୍ଦୀ ଶତାବ୍ଦୀ ଧରି ବାସ କରି
ଆସିଛନ୍ତି , ସେହି ଭାଗଟି ଦୁଇ ରାଜ୍ଯ ମଧ୍ଧରେ ଉପୟୁକ୍ତ ସାଂସ୍କୃତିକ ଆଦାନ
ପ୍ରଦାନ ଲାଗି ଏକ ସ୍ବାଭାବିକ ସେତୁପରି କାର୍ୟ୍ଯ କରି ପାରିବ । ଭାରତବର୍ଷରେ
ୟେଉଁମାନେ ଶିକ୍ଷିତ ହୋଇଥିବାରୁ ଆପଣାକୁ ଶିଷ୍ଟ ମଧ୍ଧ ବୋଲାନ୍ତି , ସେମାନେ
ନିଜକୁ ଯୁରୋପ ସହିତ ଆହୁରି ନିର୍ଦ୍ଦିଷ୍ଟ କରି କହିଲେ ଇଂରାଜୀ ଭାଷା ସହିତ
ୟେତେ ପରିମାଣରେ ଛନ୍ଦି କରି ରଖିଛନ୍ତି ଆପଣାର ପ୍ରତିବେଶୀ ଭାଷା ଓ
ତାହାର ବାହକ ଜୀଅନ୍ତା ମଣିଷମାନଙ୍କ ସହିତ ତଥା ସେମାନଙ୍କର ସାଂସ୍କୃତିକ
ବୈଚିତ୍ରଗୁଡିକ ସହିତ ସେତେ ପରିଚିତ ହୋଇ ପାରି ନାହାନ୍ତି , ଉଦାହରଣ
ସ୍ବରୂପ , ୟେଉଁମାନେ ଓଡିଆରେ ସାହିତ୍ଯ କରୁଛନ୍ତି ଓ ଆପଣାର ଧାତୁଟିକୁ
ସର୍ବୋପରି ଏକ ସାଂସ୍କୃତିକ ଧାତୁବୋଲି ମନେକରିଛନ୍ତି , ସେମାନେ ଆନ୍ଧ୍ରର
ସାହିତ୍ଯ ସହିତ କୌଣସି ପରିଚଯ ଲାଭ କରି ନାହାନ୍ତି ।
 +&gt;*
               0 
</p></body></text></cesDoc>