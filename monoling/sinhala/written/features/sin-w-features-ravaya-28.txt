<cesDoc id="sin-w-features-ravaya-28" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-ravaya-28.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p> </p>

<p>ish ú;fha mßmQ¾K;ajhg m;ajQ
ñksiaiq ish ú;fhka fndfyda foa Wlyd.kakg ÿyqkka fjkqfjka fodrj,a yer ;n;s'
tlaflda th lrkafka iudc jHdmdrj,g iïnkaOfjñka tajd yryd ;j;a msßilg w;foñks'
ke;fyd;a ;ukau iudc jHdmdrh fjñka fndfyda is;a.;a wh ú;hg noaO lr.ksñka ish
nqoaê NdKavd.drh mq¿,a lr.ekSug w;ys; fo;s'</p>

<p> </p>

<p>wfma hq.fha isá tjeks uyÛ=
mqoa.,fhls uydpd¾h irÉpkaø' Tyq f.da,hka yeÿfõ ish f.da, ;k;=f¾ iodld,slj
weiqre lsÍfï mrud¾:fhka fkdfõ' ish f.da,hkag ;ud W.;a" w;aoelSï ,enQ
ish,a, m%odkh lrñka msÈh hq;af;la njg nqoaê fmdaIKh lrjd iudkhl= ñ;=frl=
lr.;af;ah' tl, irÉpkaøhkaf.a fldKavh fuka fldKavh mSrE wh" Tyqf.a fudag¾
r:h ñ,oS f.k tys .uka ìuka .sh" Tyqf.a f,i we÷ï me</p>

<p> </p>

<p>Made in I.A.S. නමැති සෞන්දර්යය විශ්ව විද්යාලයේ
සිසුන් දහසය දෙනකුගේ චිත්ර ප්රදර්ශනයක් 706 ගැලරියේ දී නැරඹීමට ගියෙමි. ඒ ප්රදර්ශනය දක්වා
ඔවුන් මෙහෙයවූ ගුරුවරයා වූයේ ජගත් වීරසිංහයන්ය. ඔහු ලංකාවේ චිත්ර විෂය පිළිබඳ
භාවිතාව අංශක අනූවකින් වෙනස් කළ පුද්ගලයකු ලෙස ගොඩයා අදහයි. චිත්ර විෂය කෙරේ තිබූ
ප්රභූ මානය බිඳ දීැමූ ජගත් එම විෂය පිළිබඳ නව කතිකාවතක් සම්පාදනය කරයි. ජගත්ට පෙර
යුගයේ කලාභවන ලයනල් වෙන්ටඩ් කලාගාරය අත්පත් කරගෙන විකුම් පෑ පුද්ගලයෝ මෙම කතිකාවතට
සටේ වීමට නොහැකිව අර්බුදයකට පත්වී සිටී. චිත්රය ඇඳීමට පෙරම එහි මිළ ගැන හා ඒ
මිළෙන් මිලදී ගන්නා භාණ්ඩ පිළිබඳ විශ්වාසයක් තිබූ චිත්ර ශිල්පීන් වෙනුවට කලාව යනු
දීැනුම පරම්පරාවකි යන්න වටහා ගනිමින් ඒ තුළ සිය දැක්ම විනිවිදින චිත්ර ශිල්පී
පරම්පරාවක් ජගත් විසින් බිහි කරයි. ප්රභූ පැළැන්තිවලට අයත් නොවූ ඔවුන්ගේ ප්රකාශන
ශක්තිය පැරණි චිත්ර රටාවමත් අර්බුදයට යවයි. ජගත් සිය ජීවිතයේ දොර හැර තැබූ
නිසාම, සිය දැනුම ගබඩාවේ යතුර පොදු කළ නිසාම චිත්ර ශිල්පී සංස්කෘතියම වෙනසකට
භාජනය කළ හැකිවිය.</p>

<p> </p>

<p>විශ්ව විද්යාල අතර ඉතා කුඩාම
ආයතනය වන සෞන්දර්යය විශ්ව විද්යාලයේ ඇති මේ උදාහරණය වෙනත් විශ්ව විද්යාල එකකින්වත්
අපට දකින්නට ලැබෙන්නේ නැත. දීැනුම තමන් නැග සිටින සුව පහසු අට්ටාලය උස්කර ගැනීමට
හේතු වනවා විනා එය ප්රදානය කළයුතු ප්රපංචයක් ලෙස බොහෝ විශ්ව විද්යාල ඇදුරන් නොසිතයි.</p>

<p> </p>

<p>නිදහස් මනසකින් ජීවිතය හා සමාජය
දෙස බලන්නට හැකි මිනිසුන් විරල සමාජයක හැටි මේ යයි කියන්නට හොඳම රට ශ්රී ලංකාව යයි
ගොඩයා සිතයි. ජේ.ආර්. ජයවර්ධන විසින් විවෘත කරන ලද ආර්ථිකය තුළ ආර්ථිකය විවෘත වූවා
සේ ම පුද්ගල ඒකකයන් තමන්ගේ කොටු පවුරු තුළ ගාල් වෙන්නට පටන් ගත්තේය.
අනෙකාගේ ගෙයි බැම්ම උඩින් එබිකම් කරමින් එහා ගෙදර සිටින මිනිසා නිරුවතින් සිටින්නේ යයි ද
එය තම නිවසේ පැවැත්මට හානියකැයි පොලිසිය කැඳවන ඊර්ෂ්යාව මුසු කුහකකම ජේ.ආර්.
ජයවර්ධනට පළවා හැරිය නොහැකි විය. ධනය ගොඩ ගන්න සල්ලිකාරයන් බිහිවූවා සේ ම ඒ
ධනයට බය නැති තරුණ පරපුරක් ද ජේ.ආර්. හරහාම බිහිවිය.</p>

<p> </p>

<p>ගොඩයාට පුද්ගලයකු වශයෙන්
ගොඩනැගීමට සිය ජීවිතයේ, දීැනුම ගබඩාවේ දෙීාර හැර දැමූ මිනිසුන් බොහෝ දෙනකු
පිහිට විය. ඔවුන්ගෙන් අප උගත් දෙය නම්, අපගේ ජීවිතවල ද දොර හැර තබමින් අලූත්
පරපුරකට ලැගුම් දිය යුතු බවය. එය කෙතෙක් වෙනවාද, එය ප්රමාණවත්ද යන්න ගොඩයා
නොදනියි.</p>

<p> </p>

<p>සිය ජීවිතයේ එළිපත්ත මත ලැගුම්
දී පසුකලෙක එයට කප්පම් ඉල්ලන මිනිසුන් ගොඩයාට මුණ ගැසී තිබේ. මිතුරුකම්,
සහෝදරකම්, ප්රේමයන්, මානව සම්බන්ධතා විෂයයක් බව නොසිතා මිත්ර සම්බන්ධතා
ජාලය වෙනුවෙන් කර්තෘ භාගය සොයන මිනිසුන් ගොඩයාට හමුවී තිබේ. මිල ගෙවිය නොහැකි,
මේ විෂයයන් නිසා ගොඩයා මහත් සිත් වේදනාවටත් කළකිරීමටත් පත්වී සියල්ල අතහැර දැමිය
යුතු යයි සිතන තැනකට පත්වු වකවානු තිබිණ.</p>

<p> </p>

<p>කුහකභාවය සමාජ ගර්භය තුළ සඟවා
ගෙන සිටින සාම්ප්රදායික මිනිසුන් හා ආයතන දීක තිබේ. එහෙත් ඒවා හඳුනාගැනීම අපහසු නැති
නිසා ඉන් වන හානිය සුන්ථ පටුය. නමුත් ප්රගතිශීලී ව්යාපාර හා ආයතන ප්රගතිශීලී සිනාවකින්
මුව සරසාගෙන කුහකභාවය ගර්භයේ දරාගෙන කරන හානිය සුන්ථ පටු නොවේ.</p>

<p> </p>

<p>ස්ත්රීවාදය අගයමින් ගෙදර ගොස්
ස්ත්රීන්ට තලන ස්ත්රීවාදීන් ගැන ගොඩයා අසා තිබේ. විෂම ලිංගිකත්වය වෙනුවෙන් පෙනී
සිටිමින් කුරිරු සමලිංගිකත්වයක් වෙනුවෙන් තරුණන් මරා දැමුණු දේශපාලනඥයන් අප දැක
තිබේ. මේ කුහකභාවයට උත්තර සොයාගන්නා තෙක් අපට මං හසරක් පෑදේ ද?</p>

<p> </p>

<p>ඒ වගත් මෙසේම</p>

<p>ගොඩයා </p>






</body></text></cesDoc>