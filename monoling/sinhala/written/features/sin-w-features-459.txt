<cesDoc id="sin-w-features-459" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-459.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>දැයේ  ඔද තෙද පුබුදු කළ</p>

<p>බ්රහ්මචාරී වලිසිංහ
හරිශ්චන්ද්ර</p>

<p>මෙම ජූලි 9
වෙනිදාට  යෙදෙන බ්රහ්මචාරී වලිසිංහ
හරිශ්චන්ද්රයන්ගේ 125 වෙනි සැමරුම වෙනුවෙනි.</p>

<p>සීමිත ප්රදේශයක තම
අණසක පතුරා ගෙන සිටි පෘතුගීසි, ලන්දේසි ආදී විදේශීය  ආක්රමණිකයන්ගෙන් පසු අපගේ මවුබිම  බි්රතාන්ය අධිරාජ්යවාදීන්ගේ ග්රහණයට නතුවිය.</p>

<p>පෙර සිටි
ආක්රමණිකයන්ගේ බලපෑම් හේතුවෙන් එතෙක් අප 
,nd ;snq msßySu ;j ;j;a bÈßhg .uka lrùug  th m%n, rel=,la úh' fmr l, isg wm i;=j mej;s wd¾h isxy, isß;a
úß;a bj; oeuQ we;eï wh ta fjkqjg ngysr  isß;a
úß;a w.hñka hqfrdamSh ixialD;shla furg 
f.dvke.=ug  odhl jQy' </p>

<p>සිංහල නම්
වෙනුවට  පෘතුගීසි, ලන්දේසි සහ
ඉංග්රීසි නම්  භාවිතා කළ ඔවුහු බුදු දහම
අත්හැර ලාභ ප්රයෝජන  අපේක්ෂාවෙන්
ක්රිස්තියානි ආගම අදහන්නට පටන් ගත්හ.</p>

<p>ඉදින් මේ නිසා
වූයේ  උතුම් සංස්කෘතියකට උරුමකම්  හිමි ජාතියකට අත්වීමට  යන්නාවූ ඛේදනීය වූ ඉරණම ගැන 
wjÈfhka n,d isá msßia tlaùuh' wmf.ka .s,syS hk cd;sl  wd.ñl" ixialD;sl Wreuhka  wdrlaId lsÍfï jHdmdr Èh;a ùuh' thska cd;sl
msì§ulg uQ,sl wä;d,u jeàuh'</p>

<p>මෙවන් පසුබිමක්
පැවති සමයෙක දීය 1876 ජුලි  මස නව වෙනි දින
මීගමුව මහ හුනුපිටිය ග්රාමයේ වලිසිංහ හරිශ්චන්ද්ර තුමාගේ උපත සිදු වූයේ.  එහෙත් එතුමාගේ මුල් නම වූයේ වලිසිංහ
හරිශ්චන්ද්ර  නොවේ.  එඩ්වඩ් ද සිල්වාය. වසර පහ සපිරුණු කුඩා එඩ්වඩ්
නිවස අසල පිහිටි විහාරස්ථානයෙන් මූලික අධ්යාපන කටයුතු ඇරඹීය. පසුව සෙන් මේරිස්
විද්යාලයටද, ඉන් අනතුරුව ලක්දිව 
m%lg  mdGYd,djlajQ fjia,s  úÿy,go iïnkaO jQfha ;u  wOHdmk lghq;= jv jvd;a ÈhqKq lr .ekafï wruqK
we;sjh'</p>

<p>එඩ්වඩ් ද සිල්වා යන
නම අගට වලිසිංහ යන්න යොදා භාවිතා කිරීම 
werUqfha o fï iufha§ neõ i|yka fõ'</p>

<p>දේශපාලනයත්,
ජාතික නිදහසත් වෙනුවෙන්  කිසියම් සේවාවක්
කිරීමට  නම් පළමුව නීතිය  හැදෑරීම මූලික සුදුසුකමක් ලෙස එකල  පිළිගෙන තිබිණි. පාසල් අධ්යාපනයෙන් අනතුරුව
එම අදහස පෙරදීරිව  නීතිය හැදෑරීමේ කටයුතුවල
යෙදුණු තරුණ එඩ්වඩ් පසුව එයින් ඉවත්  වූයේය.
එයට හේතු වූයේ උසාවියේ  පවතින නීතියට
වඩා  යුක්ති ධර්මය උතුම් දෙයක් ලෙස
ඔහුට  වැටහීමය.</p>

<p>නීතිය ඉගැන්වූ
ගුරුවරයා  දිනක් එඩ්වඩ් සොයා පැමිණියේය.
කාමරයට පිවිසි විට දුටුයේ බලාපොරොත්තු නොවූ දසුනකි. ඔහුට දර්ශනය වූයේ නීති පොත්
හදාරන  සිසුවෙක් නොවේ. ඉතාමත් ශාන්ත
විලාසයකින් යුතුව සිටිනා භාවනානුයෝගියෙකි. නීති අධ්යාපනය අත්හැර දීමූ එඩ්වඩ්,
මිනිසුන් අතරට ගොස්  මහජනතාවට සේවයක්
කිරීමට  අදිටන් කර ගත්තේය.</p>

<p>සිංහලයෙනි
නැගිටිව්, බුද්ධ ශාසනය බේරා ගනිවි, යන කාහල නාදය සිරිලක පුරා  පතුරුවමින් එතෙක් කුම්භ කර්ණ  වූතය 
iudokaj  isá isxy,hd wjÈ lrjQ
wk.dßl  O¾umd, ;=udf.a iydhlfhla njg
wjik Tyq m;a jQfhah' B  uyd fndaê
iud.fï Wm f,alï flfkl= f,i Tyq lghq;= lf  1899 ckjdß ui m 
yslalvqfõ  Y%S iqux.,  kdysñhka uq,iqk fynjQ iNdjl§ uq,ska tâjâ
o  is,ajd  hk kñkao miqj j,sisxy 
yßYapkaø  hk kñkao ye¢kajQ  fuu i;amqreIhdKka yg n%yaupdß hk f.!rj  kduho m%odkh lrkq ,enqfõh' n%yaupdÍ f.!rj
kduh ,enqfuka  wk;=rej Tyq lf  bj; ±óuh'
W;=re i¿jla ord .ksñka Wmdilhl=f.a iajrEmfhka hqla; jQ j,sisxy  yßYapkaø ;=ud W;=ï jQ n%yaupdß ms  ms</p>

<p>ඊළඟට පැමිණියේ
ඔහු දිවියේ තවත් සුවිශේෂ අවදියකි. මහජනතාව වෙත ගිය වලිසිංහ හරිශ්චන්ද්ර තුමා ඔවුන්ගේ
ජීවිත සැපවත් කිරීමට  පියවර ගත්තේය. ගමක්
ගමක් පාසා ඇවිද යමින් විශේෂයෙන් ළමා පරපුරේ අනාගතය පිළිබඳව  සොයා බලා 
lghq;= lrkakg úh' t;=ud meje;a jq foaYkj,g ijka §u msKsi nd,"
uyÆ"  ;reK" fn!oaO wfn!oaO iefjdu  meñKs nj 
i|ykaj  we;' thska fmfkkqfha  Tyqf.a foaYk  iEu whl=gu fmdÿfõ m%fhdackj;ajQ njls' ;jo wk.dßl O¾umd,;=ud yg
úfoaYSh rgj,a lrd  hdug isÿjQQ wjia:dj,§
O¾u m%pdrK j, fmruqK  .;af;ao
yßYapkaøhkah'</p>

<p>ස්වල්ප  කලකට දඹදිව 
n,d msg;a jQ n%yaupdÍ j,sisxy yßYapkaø ;=ud  blaì;sj l,algd uyd fndaê iud.fï lghq;= .ek;a  oUÈj 
fn!oaO ;;a;ajh .ek;a iEfyk  wjfndaOhla
we;sj mia uilg miqj kej; ,laÈjg 
meñKsfhah'</p>

<p>නැවත වරක් සිංහල
බෞද්ධයන්ගේ අයිතිය පෙන්වමින් දේශය පුරා සුපුරුදු දේශන පවත්වමින් චාරිකාවෙහි
යෙදුණේය. හෙතෙමේ විසින්  පොත් පත්
ගණනාවක්ද රචනාකර තිබේ.  සිංහල ජාතියේ
පටන් ගැන්ම දෙවනපෑතිස් නරේන්ද්ර චරිතය 
iqrdmdkh" wkqrdOmqr Y=oaO k.rh" mqrd úoHdj ta w;r  jeo.;a ;ekla ysñ lr .kS' óg wu;rj  iqrdmdkfhys wd§kj  olajk Ñ;% iy m%pdrl m;%sld wdÈho uqøKh  fldg ck;dj w;r fnod yeÍug 
fya lghq;= lf</p>

<p>පෙර සිංහල රජ දවස
සමෘද්ධිමත්ව තිබූ රජරට ප්රදේශය මෙදා වනගතව තිබීම ගැනද  ඔහු  මහත් කම්පා වූයේය.
එනිසා අනුරාධපුරය  යළිත් ශ්රී විභූතියෙන්
සපිරි  නගරයක් බවට පත් කෙරුමට හේ
පුරෝගාමීව කටයුතු කළේය.</p>

<p>එහෙත් එතුමාට බාධක
අවහිරතා එල්ල නොවූවා නොවේ. ඇතැම් පුද්ගලයන්ගේ විරෝධතාවන් සහ නොයෙකුත් බාධා
කිරීම  අටෝරාශියකටද  අනුබුදු මිහිඳු හිමියන් වැඩි මිහින්තලාව  නැවත වරක් පූජනීයත්වයට  පත් කෙරුවේ ව්යාපාරයේදී ඔහුට  මුහුණ දෙන්නට වූයේය.  එහෙත් ඒ සියල්ලමද  අනුරාධපුරය යළිත් ශුද්ධ නගරයෙක් බවට පත්වීමේ ආදි කර්තෘවරයා වන්නේ බ්රහ්මචාරී
වලිසිංහ හරිශ්චන්ද්ර  තුමාණන් බව පෙනේ.
එසේම වරෙක 1903 පොසොන් පොහෝදින අනුරාධපුරයේදී ඇතිවූ කැරැල්ලක් එතුමා මෙහෙයවු
බවට  චෝදනා ලැබ බන්ධනාගාර ගතවීමටද  ඔහුට 
isÿúh' miqj  thska  ksoyi ,enQ t;=ud kej;;a iqmqreÿ mßÈ  ksr; jQfha 
rg" cd;sh" wd.u ms</p>

<p>එකල
රජය මගින් පනවා තිබූ ඉතාම අසාධාරණ වූ මුඩුබිම් 
mk;g tfrysj  f.k .sh  igfka m%Odk 
fikam;sfhla  jQfhao  j,sisxy yßYapkaø ;=udh' isxy,hka ;uka Wmka
ujqìu ;=  ;;a;ajh bka ÿreúh' tfy;a ;u ujq ìu fjkqfjka uy;a mß;Hd.YS,Sj  lghq;= l  frda.hla iE§ 1913 cQ,s
ui ;siajeks od T;am, úh' blaì;sj  ta
olajd lrk ,o  cd;sl wd.ñl  fiajd w;ru. kej;=fKah' olaI ffjoHjrekaf.a
m%;sldrj,skao" m,la fkdùh' frda.h ;j ;j;a W.% ;;a;ajhg m;aúh' fufia wikSmj
ál l,la isá Tyq il,  fn!oaO  ck;dju 
ÿla ihqrl .s,ajñka ioygu fofk;a mshd .;af;ah' ñksfila yg ysñ mQ¾K  wdhq m%udKh .; flrejg yßYapkaø ;=ud yg
fkd,enqfKah' tfy;a t;=ud ckaufhka f.kd wdhq m%udKfhka ;u rg cd;sh iy wd.u
fjkqfjka  l  w;s uy;ah' 
tneúka  n%yaupdÍ j,sisxy  yßYapkaøhkaf.a Y%S kduh isß,la jdis
fndÿ  ne;su;=kaf.a is;a i;kays iodl,a
jecfUkq ksh;h'</p>






</body></text></cesDoc>