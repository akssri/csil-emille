<cesDoc id="sin-w-features-455a" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-455a.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>බරවා රෝගය</p>

<p> </p>

<p> f,dalfha >¾u l,dmSh yd Wm >¾u l,dmSh rgj, nrjd frda.h
jHdma;j w;' bkaÈhdj" ngysr bka§h" fldfoõ ¥m;a ol=Kq Ökh"
cmdkh" ngysr yd uOHu wms%ldj  hk
rgj,a ñka lsysmhls'YS% ,xldj ;=</p>

<p> fuh ñksidf.a jid moaO;sh ;=</p>

<p> fuu frda. ldrl mrfmdaIs; mKqjd jqjf¾ßhd
nekafl%d*Aá hk kñka ye¢kafõ' frda. jdylhd f,i lsHQf,laia laúkals*eisfhagia
j¾.fha  .eyeKq uÿrejka ls%hd lrhs' Tjqka
frda.h wka whg m;=rejhs'</p>

<p> frda. Odrlhska fofofkls' ñksid ksh; Odrlhd jk
w;r uÿrejd w;r ueÈ Odrlhd fjhs' </p>

<p>කි්යුලෙක්ස්
ක්වීන්කිෆැසියේටස් වර්ගයේ පරිණත ගැහැණු මදුරුවන් 
ì;a;r oukafka wêl f,i ¥Is; jQ wmsßisÿ c,h /÷k ia:dkj,h' tkï jeisls</p>

<p>පරිණත පිරිමි මදුරුවෙක්
සතියක් පමණ ජීවත් වන අතර පරිණත ගැහැණු 
uÿrefjla i;s 3-4la muK ld,hla j;a fõ'</p>

<p> reêrh Wrd fndkafka .eyeKq uÿrejd úiska muKs'
Tjqkaf.a ì;a;r fïÍug reêrh wjYH fõ' msßñ uÿrejka me  ;u ì;aa;r fudarK f;la .ia
m¾re" wi, jid isák .eyeKq uÿrejd jirlg ì;a;r 100-200 la muK ouk w;r ;u ú;
ld,h ;=</p>

<p>  බරවා රෝගය බෝ කරන ගැහැණු මදුරුවන් විසින්
මිනිසා දෂ්ඨ කරනු ලබන්නේ සාමාන්යයෙන් රාතී්ර 8.00 සිට අන්ථයම 4.00 දක්වා කාලය තුළය.</p>

<p>දිවා කාලයේ ගැහැණු මදුරුවා
නිවෙස් තුළ එල්ලා ඇති ඇඳුම්, දොර රෙදි ආදියෙහිද මේස, පුටු ඇඳන් වැනි
දෑ යටද වසා සිටියි.  </p>

<p>මදුරුවෙකු දෂ්ඨ කරන විට
රෝග  කාරක පණුවා මිනිසාගේ සමට යටින් දිවෙන
පියවි ඇසට නොපෙනෙන වසා ප්රණාල තුළට ඇතුන්ථ වී ප්රණාළ ඔස්සේ වසා ගැටිතිවලට
පැමිණේ. විශේෂයෙන්ම  උදරයේ  සහ ඉකිලිවල තිබෙන වසා ගැටිතිවලට ඇතුන්ථ වන
මොවුන් ඒවා තම වාසස්ථාන කරගනී. වසා පද්ධතියේ පෝෂණය ලබමින් මාස 06 සිට 09ක  කාලයක් තුළදී මොවුන් වැඩුණු පණුවන් බවට
පත්වේ.</p>

<p> j¾Okh jQ .eyeKq mKqjl= È.ska fi'ó' 8 isg 10
olajdo msßñ mKqjl= È.ska fi'ó' 2'5 isg 4 olajdo fõ'fuu .eyeKq yd msßñ mKqjka
;j;a w,q;a laIqø mKqjka mrmqrla fndalrhs' Tjqka reêrhhg we;=¿  ù by; whqßka ;u ú; pl%h iïmQ¾K lrhs'</p>

<p> jevqKq mKqjl= jid .eá;s ;=  isg 15 la muK ld,hla j;a jk nj fidhdf.k
we;'</p>

<p> jid .%ka:s j, l%ufhka jefvk mKqjdg;a  Tjqka msglrk wmøjH iy ridhksl øjH j,g;a YÍrh
;=  reêrh 
ffi,j,ska wdl%uKh fõ' ^l=oaoá bÈóu;a" fõokdldÍ ùu;a we;sfjkafka fï
fya;=fjks'&amp;</p>

<p>මීට ඉහත දී පැහැදිලි කළ
පරිදි පළමුව සිදුවන අසාත්මිකතාවය හේතුවෙන්ද දෙවනුව සිදුවන වසා ප්රණළ අවහිර වීම
නිසාද රෝග ලක්ෂණ ඇතිවේ. රෝග පණුවන් ජීවත් වන අවයව අනුව රෝග ලක්ෂණ වෙනස්විය
හැකිය. එසේම රෝගය වැළදුනු සියලූම පුද්ගලයන් එක හා සමාන රෝග ලක්ෂණ නොපෙන්වයි.
බරවා රෝගය බහුලව පවතින ප්රදේශයක් තුළ ජීවත් වන සමහර රෝගීන්ගේ රෝග ලක්ෂණ
මතුවීමට බොහෝ කල් ගතවේ. </p>

<p>පියයුරු ගැටිති, වෘෂණ
කෝෂ ඉදිමීම (අණ්ඩවාතය) හන්දිපත් රුදාව බරවා රෝගයේදී ඇතිවන සුලභ රෝග
තත්ත්වයන්ය. දීර්ඝ කාලීනව රෝගය ශරීරයේ කි්රයාත්මක වීමෙන් ''අලි කුකුල්" ලෙස
හඳුන්වන පාද ඉදිමීම හා විරුපි වීම ඇතිවේ. එම තත්ත්වය රෝගයේ උත්සන්නම  අවස්ථාවයි. මුල් අවස්ථාවලදී මස් පිඩුවල
වේදනා  සමෙහි ඇතිවන වේදනාකාරී රතුලප,
උණ ගතිය, වසා ප්රණාළවල ආසාදන දැකිය හැක.</p>

<p> fuys§ iug háka r;=mdáka Èiafjk kd</p>

<p>වසා ග්රන්ථිවල ඉදිමීම ප්රධාන
වශයෙන් කිහිළිවල සහ ඉකිලිවල ඇතිවන වසා ග්රන්ථවල සිදුවෙයි. මෙය කුද්දෙටි ඉදිමීමක්
ලෙස ඇතිවෙයි. ආසාදිත අවයව ආශි්රත ඉදිමීම සහ වේදනාව ප්රධාන වශයෙන් අත් කකුල් සන්ධි
පයෝධර වෘෂණ කෝෂ වැනි අවයවවලට බලපායි. </p>

<p>වෘෂණ කෝෂවල වසා තරලය
පිරීම නිසා විශාලවීම, පියයුරු අත් කකුල් සන්ධි ආදියේ ඉදිමුම සිදුවෙයි. කල් යෑමේදී
වරින් වර රෝගයට භාජනය වීමත් බැක්ටිරියා මගින් එම පටක ආසාදනය වීමත් නිසා මෙම පටක වල
ස්වභාවික බාහිර පෙනුම වෙනස් වී විරුපිතා ඇතිවිය හැකිය. </p>

<p>අමතරව ක්ෂුද්ර පණුවන් පෙනහන්ථ
තුළ ජීවත්වීම නිසා ''ට්රොපිකල් පල්මොනරි ඉයොසිනොෆිලියා (ටී.වී.ඊ.)'' නම්
රෝගි තත්ත්වය ඇතිවේ. ඉන් පහත සඳහන් රෝග 
,CIK we;súh yelsh'</p>

<p> uo WK .;sh lEu wreÑh rd;S% ld,fha we;sjk
úh</p>

<p>(මෙය බොහෝ විට තරුණ
වියේ සිටින අයට වැළඳෙන රෝගී තත්ත්වයකි.)</p>

<p>රෝග ලක්ෂණ හඳුනාගැනීමෙන්
සහ  රුධිර පරික්ෂාව මගින් රෝගය හඳුනාගත
හැකිය. රෝගය පැතිරීම වැළැක්වීමට නම් රෝගය හර්නාගෙන ප්රතිකාර ගැනීම වැදගත්ය.</p>

<p> reêr mg, .ekSfuka laIqø mKqjka reêrfha
isàoehs mÍCId flf¾' fudjqka reêrhg meñfKkafka rd;S% ld,fha§ muKla neúka reêr
mg, mÍCIdj l  fidhd.; yelsh' </p>

<p>බරවා රෝගය මිනිසාට පමණක්
වැළඳෙන රෝගයක් බවත්, රෝගය බෝ කරනුයේ මදුරු විශේෂයක් මගින් බවත්
කලින් සඳහන් කළෙමු. මේ අනුව නිරෝගි පුද්ගලයින් පමණක් සිටීමට නම් රෝගීන් සුව
කිරීමත් මදුරුවන් දුරු කිරීමත් යන දෙකම කළ යුතුය.</p>

<p> 1' uÿrejka md,kh lsßu'</p>

<p>2. මදුරු දෂ්ඨණයෙන්
වැළකී සිටීම.</p>

<p>3. රෝග ලක්ෂණ සහිත
රෝගීන් හා රෝග ලක්ෂණ නොපෙන්වන 
frda.Ska y¾kdf.k m%;sldr lsÍu'</p>

<p>වැසිකිලිවළවල්  මතයොදා ඇති කොන්කී්රට් හෝ වෙනත් ආවරණවල
සිදුරු තිබේ නම් එම සිදුරු හරහා මදුරුවන්ට ගමන් කිරීමට නොහැකි වන ලෙස වසා දීමිය යුතුය.</p>

<p> j;=r tl;=úh yels fmd,a lgq" áka"
Ndck" ;eô,s "l=reïnd fldaïn hkdÈh ;=</p>

<p> c,h m,ajk ldKq msßisÿ lrkak' ksjiska neyerjk
c,h tl/ia fkdjk f,i nei hdug ie,eiaùu yd mdÆ j  hq;=h'</p>

<p>අවුරුදු 50කට අධික කාලයක්
මුන්ථල්ලේ සිට බරවා රෝගයට ප්රතිකාර කිරීම සඳහා 
f,dalfha fndfyda rgj, m%Odk jYfhka Ndú;d jkafka ã'B'iS' ^vhs B;hs,a
ldnuiska fyÜg%idka&amp;  kue;s T!IOhhs' ñf,ka
,dNodhs ùu;a fndfyda frda.Ska úiska id¾:l m%;sM, fmkaùu;a óg fya;= fõ'</p>

<p>රෝගියෙකුගේ බර කි.ග්රැ.
වලින් ගත් විට බර කි.ග්රැ.01කට දිනකදි ඩී.ඊ.සී.මි. ග්රෑම්. 06ක් වන පරිදි දෙනු ලබන පෙති
ප්රමාණය  ගණනය කරනු ලැබේ. මෙම පෙති උදේ සහ
රාති්ර ලෙස වාර දෙකකට බෙදා කෑමෙන් පසු ගැනීම වඩාත් උචිතය.  සාමාන්යයෙන් ප්රතිකාර මාලාව සති දෙකක් පමණ වන අතර රෝග ලක්ෂණ අනුව මෙම
කාල සීමාව වෙනස් විය හැකිය.</p>

<p> ã'B'iS fm;s .kakd frda.Skaf.ka jeä fokl= yg
lsisÿ w;=re wdndOhla we;s fkdjk nj wjOdrKh l</p>

<p> idudkHfhka oelsh yels w;=re wdndO kï lEu
wreÑh" Tlaldrh" ysiroh' ksÈu; .;sh wdÈhhs iuyr úg laIqø mKqjka ñh hEu
ksid ksmofjk wmøjH fya;=fjka ysiroh uia msvqj, fõokd ifuys mÆ oeóu" leiSu
jid .%ka:s bÈóu wdÈh we;súh yelsh' merissgfuda,a jeks ir, m%;sldrhlska fïjd
fndfyduhla wju lr.; yelsh'</p>

<p> </p>






</body></text></cesDoc>