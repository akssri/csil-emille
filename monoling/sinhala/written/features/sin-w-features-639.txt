<cesDoc id="sin-w-features-639" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-639.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p> wekaf.da,dfõ isú,a
hqoaOh wjika jk ,l=Kq@ </p>

<p> </p>

<p>පසුගිය
සිකුරාදා ඇන්ගෝලාවේ මොක්සිකෝ පළාතේදී යුනිටා සංවිධානයේ නායක ජෝනාස්
සව්බ්බි රජයේ හමුදා ප්රහාරයකින් මියයෑම නිසා 1975 සිටම ඇන්ගෝලාවේ පැවති කුරිරු සිවිල්
යුද්ධයේ නිමාවක් දීකිය හැකි බවට බලාපොරොත්තු රටේ හැම දෙසින්ම පළ වී ඇත.
ඇන්ගෝලාව පෘතුගීසි යටත් විජිතයක්ව පැවති 1960 ගණන්වලදී සවිම්බිගේ නායකත්වයෙන් ආරම්භ
කළ යුනිටා සංවිධානය 1975 දී ඇන්ගෝලාවේ පෘතුගීසි පාලනය බිද වැටීමෙන් පසු පිහිට වූ
එම්.පී.එල්.ඒ. සංවිධානය නායකත්වයෙන් යුත් රජයටද එරෙහිව සටන් කළේය. එම්.පී. එල්.
ඒ සංවිධානයට එවකට පැවති සෝවියට් සංගමයෙන් යුද හා ආර්ථික ආධාර ලැබුන අතර යුනිටා
සංවිධානයට සී.අයි. ඒ. සංවිධානයේ අරමුදල් හා යුධ ආධාර ලැබිණ. එය ඇමරිකාව සෝවියට්
සංගමයට එරෙහිව ගෙන ගිය නිරවි යුද්ධයේම විකාශනයක් ලෙස බොහෝ විචාරකයෝ හැඳින්වූහ.</p>

<p>දකුණු
දිග අප්රිකාවේ ස්වාභාවික සම්පත් වලින් ධනවත්ම රාජ්යය ලෙස හැඳින්විය හැකි ඇන්ගෝලාව ඛණිජ
තෙල්, ස්වාභාවික ගෑස් මෙන්ම දියමන්ති වලින්ද බෙහෙවින් පොහොසත්ය.  දශක තුනකට අධික කාලයක් කුරිරුතම ගරිල්ලා යුද්ධයක්
පැවතීමට ඒක හේතුවක් වූයේද මෙම ස්වාභාවික සම්පත්වල අයිතිය ලබා ගැනීම සඳහා වූ
අරගලයයි. වර්ගවාදී දකුණු අප්රිකාව යුනිටා කැරලිකරුවන්ට සහයෝගය ලබා දුන්නේ දකුණු
අප්රිකානු සමාගම් වලට ව්යාපාරික කටයුතු පවත්වාගෙන යෑමට පදනමක් සකස්කර ගැනීම සඳහාය.
සටන් විරාම ගිවිසුම් තුනක්ම බිඳ වැටීමෙන් පසු එක්සත් ජාතීන්ගේ ආරක්ෂක මණ්්ඩලය යුනිටා
සංවිධානය කෙරේ වෙළෙඳ හා ආර්ථික සම්බාධක පැන වුවද සවිබ්බිට ගරිල්ලා යුද්ධය ගෙන යෑමට
හැකිවූයේ රහසින් කෙරුණ දියමන්ති වෙළෙඳාමෙනි. එම ජාවාරම්වලට සවිම්බිට එවකට සයරේ
රාජ්යය නමින් හැඳින්වු කොංගෝ ප්රජා තන්ත්රවාදී ජනරජයේ මොබුටුගේ සහාය ලැබිණ මොබුටුගේ
පාලනය බිඳ  වැටීමත් සමග කොංගෝව හරහා
කෙරුණු වසරකට ඩොලර් කෝටි 400ක් පමණ වූ දියමන්ති වෙළෙඳාමද බිඳ වැටිණ.</p>

<p> oYl ;=klg wdikak
ld,hla ;siafia mej;s .ß,a,d hqoaOh ksid wekaf.da,dfõ wd¾:slh fukau ck ú;ho
iïmQ¾Kfhkau mdfya wvd,ù we;'  tlai;a
cd;Skaf.a jd¾;d wkqj wekaf.da,dj mqrd w;=rd we;s ìïfndaïn m%udKh oY,laI yhls'
fï ksid Èkm;du ìï fndaïn msmsÍfuka ú; yd w;amd wysñ jk ixLHdjo b;d by</p>

<p>1977 දී එඩුවාඩෝ ඩොස් සැන්ටොස් ඇන්ගෝලාවේ ජනාධිපති
පදවියට පත්වීමෙන් පසු යුනිටා සංවිධානය සමග සටන් විරාමයක් ඇති කර ගැනීමට දීඩි උත්සාහයක්
ගත්තේය. ඇම්.පී. එල්.ඒ. සංවිධානයට ආධාර සැපයූ සෝවියට් සංගමය බිඳ  වැටීමත් ඇන්ගෝලාවේ රජයේ  හමුදාවල සහායට පැමිණි  කියුබානු භටයින් ඉවත්  කර ගැනීමත් නිසා  යුනිටා සංවිධානය සමග සටන් විරාමයක් ඇති කර ගැනීමටත් ජාතික එකමුතු
රජයක් පිහිටු වීමටත් ඇති ඉඩ ප්රස්ථා වැඩි විය. ඒ අනුව 1989 දී ඇම්.පී.එල්.ඒ. රජය හා
යුනිටා කැරලිකරුවන් අතර බහු පක්ෂ ප්රජාතන්තවාදී පාලන ක්රමයක් පිළිබඳ එකඟතාවයක්
ඇති කකර ගැනීමට හැකි විය ඉන් පසුව මහා මැතිවරණයක් පැවැත්වීමටත් සටන් විරාමයක් ඇති
කර ගැනීමටත් එකඟතාවයක් ඇති කරගන්නා ලදී. එහෙත් යුනිටා නායක ජොනාස්් සවිම්බිට 1992 දී
පැවැත්වූ ජනාධිපති වරණයෙන් ජය ගැනීමට නොහැකි වීමත් ඉන් අනතුරුව පැවති පාර්ලිමේන්තු
මැතිවරණයෙන්ද ඔහුගේ යුනිටා සංවිධානයට බහුතරයක් දිනා ගැනීමට නොහැකිවීමත් නිසා සටන්
විරාමය බිඳ වැටිණ. සටන් විරාම කාලය තුළ ශක්තිමත් වූ යුනිටා සංවිධානයට කෙටි කාලයක දී
මුන්ථ භූමි ප්රදේශයෙන් සියයට 70කම බලය තහවුරු කර ගැනීමට හැකිවිය.</p>

<p>bka miqj kej;
jrla 1994 § tlai;a cd;Skaf.a ueÈy;aùfuka 
wekaf.da,d rch yd iúïì w;r igka úrdu .súiqula we;s lr .; yels úh'  Æidldys§ w;aika ;nk  ,o 
tu .súiqug  hqksgd .ß,a,d Nghska
rcfha yuqodjg we;+  fiiq wm%sldkq rgj, iydh ,nd.;a ckdêm;s iekafgdaia ler,slrejka
m%Odk k.r j,ska mrdch fldg ;;a;ajh md,kh lf</p>

<p>සටන් විරාම ගිවිසුම් කීපයක්ම කඩ කිරීම නිසාත් බලය ලබා ගැනීමේ
ඒකායන  අපේක්ෂාව නිසාත් සවිම්බිට රට තුළ
මෙන්ම ජාත්යන්තරව ලැබුණු සහයෝගය පසුගිය 
jir lSmh ;= 
1980 § weursldkq ckdêm;s frdk,aâ f¾.ka Oj ukaÈrfha§ Tyq ms</p>

<p>මේ තත්ත්වය යටතේ යුනිටා සංවිධානය පසුගිය වසර තුළ දීඩි
පසුබෑමකට ලක්ව තිබිණ. මගී දුම්රිය පුපුරුවා දහස් ගණනක් මරණයට පත් කිරීම වැනි  මිනිස් ඝාතන රැසක් යුනිටා සංවිධානය සිදු කළද
ලූවන්ඩා රජයා රටේ භූමි ප්රදේශයෙන් සියයට 90කම බලය තහවුරු කරගත හැකි විය. යුධමය
ජයග්රහණයක් අත්කර ගත් ජනාධිපති සැන්ටෝස් එකඟත්වයකට පැමිණෙන ලෙස නැවත වරක් කැරලිනායක
සවිම්බිගෙන් ඉල්ලා සිටියද ඔහු එය පිළිගත්තේ නැත. එකඟතාවකට පොළඹවා ගැනීම සඳහා
සවිම්බිට හෝ ඔහුගේ සහායකයින්ට විරුද්ධව 
lsisu kS;suh mshjrla fkd.ekSug;a foaYmd,k n,h fnod .ekSug;a l</p>

<p> iúïìf.a urd
±óu;a iu. .ß,a,d hqoaOh jydu k;r jkq we;ehs lsj fkdyel' tfy;a iúïìf.a
kdhl;ajfhka f;dr hqksgd ixúOdkh mj;ajd .ekSu fnfyúka ÿIalr ld¾hhla jkq we;ehs
fndfyda fokd mji;s' miq.sh jif¾§ hqksgd ixúOdkfha m%Odk fmf</p>

<p> wfkla w;g ckdêm;s tvqjdfvda fvdia iekafgdaiaf.a rchg miq.sh jir
lSmh ;=  isg o  njg m;aúh' fï ksid iúïìf.a urKh;aiu. wekaf.da,dfõ
foaYmd,k ia:djr Ndjhg m;aùug we;s bvlv fnfyúka jeä ù we;s nj fndfyda ksÍlaIlhskaf.a
u;hhs'</p>

<p> </p>

<p> </p>






</body></text></cesDoc>