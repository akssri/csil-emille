<cesDoc id="sin-w-features-silumina-241" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-silumina-241.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ඵ28ටදඹ03</p>

<p>චඵ</p>

<p>නූතනවාදී
ප්රබන්ධ කලාවේ</p>

<p>පුරෝගාමියා</p>

<p> </p>

<p>අයර්ලන්ත
ජාතික ජේම්ස් ඔගස්ටීන් ඇලෝසියස් ජොයිස්  James
A.A. Jayee) වර්ෂ 1882 පෙබරවාරි මස 02 වැනි දින ඩබ්ලීන් නගරයේ දී මෙලොව උපත
ලැබුවේ ය. සාමාජිකයන් දහසය දෙනෙකු ගෙන් යුත් පවුලක හත් වැනියා වූ හෙතෙම, දීප්තිමත්
පාසල් ශිෂ්යයෙකි. යොවුන් විය එළැඹි කල විදේශීීය භාෂා ඉගෙන ගැනීමේ අටියෙන් ජේසුයිට් පාසලට
ඇතුළත්වූ ඔහු එහි දී සාහිත්ය පිළිබඳ ව පෘථුල අධ්යාපනයක් ලැබුවේ ය. 1900 දී ජොයිස්
ඩබ්ලීන් නගරයේ පිහිටි රෝයල් විශ්ව විද්යාලයට ඇතුළත් වී දර්ශනවාදයත්  ඉතිහාසයත් වැඩිදුරට
හැදෑරීය. 1903 දී මොහු ලේඛන කලාව ප්රගුණ කරනු වස් ප්රංශය බලා පිටත් වී ගියේ ය. එහෙත්
අවාසනාවකට තම අධ්යයන කටයුතු නිසි ලෙස කරගෙන යෑමට ඔහුට අවකාශ නො ලැබිණි. සිය මව ගේ
මරණය නිසා ඔහු යළිත් පෙරළා ඩබ්ලීන් නගරය වෙත පැමිණියේ ය. 1904 දී නෝරා බානැක්ලි ව්ධපච
ඕචපදචජතඥ) සමඟ විවාහ වුණු ජොයිස් ඈ සමගින් යුරෝපයට ගොස් ලේඛන කලාව ප්රගුණ කළේ
ය.</p>

<p> »pùY SJ~p »[ (Henrik Ibsen) නාට්ය
විචාරය කරමින් විචාරකයෙකු ලෙස සාහිත්ය ලෝකයට පිවිසි ජොයිස්, වඩාත් ම ප්රකට වූයේ ප්රබන්ධ
කතාකරුවකු වශයෙනි. ඔහු 1907 දී තම පළමු කෘතිය ලෙස ඛ්ඩචථඡඥප ර්භඵඪජ ප්රකාශයට පත් කළේ ය.
මෙය කාව්ය සංග්රහයකි.</p>

<p>rv¨ »z¤Y v£ ~¹[²£vx R£yKuþvf vl»lp
yap£ Yyp zn Dubliners - 1914 pK {« »YÑYl£ ~¹[²x v[p »v£¨ »YÑYl£Yy¥{Yª »z~
»àyf {¥Õ»x x. »K »YÑYl£ ~¹[²x {p£ WY »»|zxÃp WÃ»pY t¥¼ãj© Yªh£ Ylpny
{¥zÃ. RplM[lx  R£Y¯Üx ~ ~¹»Yl£lvY [¥w¨yÃp »x£np zn vo³v r¹ÜY cpx£»[ aùl
R¥~¨»yp A{£ yap£ Yy R¥l.</p>

<p>ඔහුගේ ඊළඟ
කෘතිය වූයේ ඒ. Portrait of the Artist As a Young Man  1916 වූ නවකතාව යි.
මෙය විචාරකයන් ගේ මහත් පැසසුමට ලක්වූයේ... බටහිර සාහිත්යයේ නූූතනවාදී ව්යාපාරය
විසින්් බිහිකළ වඩාත් ම පරිපූර්ණ නිර්මාණය ලෙසට යි. මෙය ස්ටෙපන් ඩැඩලස් (Stephen
Dadalus) යන කලාකරුවාගේ චරිතයේ අධ්යාත්මය වචනයෙන් විචිත්රවත් කිරීමකි.</p>

<p>ජොයිස් ගෙවී
ගිය විසිවන සියවසෙහි පහළ වූ අග්රගණ්යය සාහිත්යධරයෙකු ලෙස ප්රකට වූයේ... මොහු විසින්
රචනා කරන ලද මහා ප්රබන්ධ යුගළ වන උතරඵඵඥඵ - 1922 සහ ජ්ඪදදඥඨචදඵ එචඬඥ - 1939 මගිනි.</p>

<p>ඔහු
උතරඵඵඥර නැමැති ප්රබන්ධය උදෙසා හෝමර්ගේ The Odyssey නම් මහා
කාව්යයේ ප්රධාන චරිතය වන Ulysses යොදා ගනී. එසේ
යොදාගනු ලබන්නේ ... එම වීර ග්රීීකයා පුතෙකු - පමේවතෙකු - ස්වාමිපුරුෂයෙකු - පියෙකු -
සෙබළෙකු - ලෙස ජීවිතයේ විවිධ චරිතවලට පණ පොවන බැවිනි. හෝමර්ගේ මහා කාව්යයේ දී මෙන් ම
මෙහි දී එකැස් යෝධයකු ලෙස චරිතයක් මතුවෙයි. ඒ තැනැත්තා ඉතා පිළිකුල් සහගත ය. අධිරාජ්ය
විරෝධියෙකි. උග්ර සුචරිතවාදියෙකි. මෙම මහා ප්රබන්ධය රචනා කිරීමට තමා පොළඹවන ලද්දේ... ප්රංශ
නවකතාවක් යැ යි කියා වරක් ජොයිස් පුවත්පත් වාර්තාකරුවන්හට ප්රකාශකර ඇත. මෙය විසිවන සිය
වසේ දී බිහිවූ විශිෂ්ටතම ප්රබන්ධය ලෙස සැලැකේ.</p>

<p>ඊළඟට
ජොයිස් Finnegans Wake - 1939 නමැති ප්රබන්ධය මගින් ස්ති්ර - පුරුෂ
ලිංගිකත්වය ගැන ගැඹුරින් කතා කරයි. මානව ඉතිහාසයේ සිට පැවත එන ස්තී්ර මූූලය  පුරුෂ
මූූලය අතර ඇති ආදරය සහ අනුරාගය වඩාත් සංකීර්ණ ව විවරණය කරනුවස් ඩබ්ලීන් නගරයේ වෙසෙන
හම්ප්රි නම් පුරුෂ චරිතයත් ඇනා නම් ස්තී්ර චරිතයත් යොදා ගනී. මෙම ප්රබන්ධයේ කි්රයාදාමය දිවා
කාලයේ වුවද, ජ්ඪදදඥඨචදඵ එචඬඥ දිවයන්නේ රාති්ර කාලය තුළදී ය. රාති්ර සිහිනමය භාෂාවක්
බිහි කරනු පිණිස හෙතෙම විවිධ භාෂා 50 කට අධික සංඛ්යාවක ගේ් උපකාරය මීට යොදාගනියි. වඩාත්
සංකීර්ණ වූ ද වඩාත් ම ගැඹුරු වූ ද මේ ප්රබන්ධය ඔහු ගේ අවසාන නිර්මාණය ලෙස සැලැකේ.</p>

<p>උතරඵඵඥඵ  සහ ජ්ඪදදඥඨචදඵ එචඬඥ නම් මහා ප්රබන්ධ පළවීමෙන්
අනතුරු ව ප්රබන්ධකරණයට නැවුම් වූ වාජීකරණයක් සහ සංකේත භාවිතයේ අපූර්වතම අත්දැකීම ජොයිස්
විසින් හඳුන්වා දෙනු ලැබූූ බව විචාරකයන් ගේ මතය වේ. විඥාානධාරා රීතිය (Stream
of Consciouness) හාපුරා කියා ප්රබන්ධ කලාවට හඳුන්වාදෙන ලද්දේ මොහු විසිනි.</p>

<p>ප්රබන්ධ
කලාවේ අද්විතීය ප්රාතිහාර්යයක් කළ ජොයිස්ගේ පෞද්ගලික ජීවිතය මහත් දුක්ඛ දෝමනස්සයන්ගෙන්
පිරුණු කටුක එකක් විය. මහත් ආර්ථික දුෂ්කරතා මැද්දේ දැඩි කායික සහ මානසික පීඩා විඳිමින්
කල්ගෙවූ හෙතෙම, ට්රයිස්ට් නගරයේ පිහිටි පාසලක ඉංග්රීීසි භාෂාව ඉගැන්වීමෙන් ලද සුන්ථ
වැටුපෙන් දිවි ගෙවී ය. අවසානයේ දී මහත් සේ පීඩා විඳි මොහු ගේ දිවිය ඉතාමත් කටුක
දුක්මුසු වූවක් බවට පත් විය.</p>

<p>"විසිවන සියවසේ
ප්රබන්ධ කලාව නූූතනවාදයට තල්ලු කළ මේ ශ්රේෂ්ඨ ලේඛකයා" වර්ෂ 1941 දී ට්රයිස්ට් නගරයේ දී
මෙලොව හැර දා ගියේය.</p>

<p>මහදිසාවේ
බංඩාර ඇටිපොල</p>

<p> </p>






</body></text></cesDoc>