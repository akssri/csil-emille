<cesDoc id="sin-w-features-divayina-184" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-divayina-184.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head>iodpdrh j,a jefoa</head></p>

<p><head>wjkS;sh rc lrhs</head></p>

<p><head>miajdka oyia l,a mj;akd iqmsßiqÿ nqÿ oyu rlakd th jeä
ÈhqKq lrk O¾uoaùmfha jdih lrk b;du;a YsIag iïmkak" bmerKs iNH;ajhlg Wreulï
lshk cd;shla jYfhka wms fuf;la l,a wdvïnr ùuq' i;=ka ueÍu" fidrlï
lsÍu" ldufhys jrojd yeisÍu" fndre lSu iy iqrdmdkh lsÍu hk mxp
mdml¾uhkaf.ka je</head></p>

<p><head>tfy;a wm wjg wo isÿ jk foa foi ne,Sfï§ wmg ;jÿrg;a wdvïnr
úh yelso@ O¾uoaùmfha jeishka f,i f,dalhd yuqfõ fmks isáh yelso@ wo wmg isÿ ù
we;af;a ,Êcdfjka uqyqK iÕjd .ekSugh' wo Y%S ,xldj wmrdOlrejkaf.a"
ñkSurejkaf.a" ia;S% ¥Ilhkaf.a" fínoaokaf.a" u;al=vq
cdjdrïlrejkaf.a" jxpdldrhkaf.a" uxfld,a,lrejkaf.a mdrd§ihla njg m;aj
;sfí'</head></p>

<p><head>.s,kqkag Wmia:dk lsÍu uy;afia w.h l</head></p>

<p><head>Y%S ,xld fg,sfldï wdh;kfha ¥IKhka fyd;kh lsÍug ta wdh;kfhau we;eï mqoa.,fhda ñkSure fldka;%d;alrejka fhdoj;s'
fï w;r orejka meyerf.k f.dia lmamï .ekSfï isoaëka .Kkdjls' úúO wdh;khkag jegqma
f.ùug nexl=j,ska uqo,a /f.k hoa§ T;a;= n,d tu uqo,a meyerf.k hEfï isoaëka
fndfydah' wo l=vd fjf</head></p>

<p><head>fï ish,af,ka meyeÈ,s jkafka wo wm rfÜ iodpdrh j,aje§ we;s
njh' kS;sh .ek lsis .reirejla ke;s njh' yeu fokdu úhre jegqKjqka fia uqo,a
miafia yUd hk njh' ñkS urd fyda uqo,a fiùug fm  kS;sh </head></p>

<p><head>/lSug isák wdrCIl wxYj, we;eï ÿ¾odka; mqoa.,hka mjd fuu
l%shdjkag iyNd.s ùuh' wú wdhqO mqyqKqj ,en yuqodfjka mek .sh mqoa.,hka fndfyda
fokl= fuu m%pKav l%shdjkays fhfok nj o oelal yelsh'</head></p>

<p><head>miq.sh oilhl muK ld,fha fï rfÜ isÿ jQ foaYmd,k m%pKav
l%shdjka fya;=fldg uerlïj,g we;s ìh iel ÿrej hEfuka fuu wmrdO /,a, ;j ;j;a
by</head></p>

<p><head>flfia fj;;a fuu wmrdO u¾okh lsÍug oekauu úêu;a mshjrla
fkd.;fyd;a fï rfÜ wkd.;h;a" wkd.; mrmqr;a uqyqK fokafka wk;=reodhl
;;a;ajhlgh' wvq .Kfka rfÜ kS;sh yßhdldrj l%shd;aul flfrk jevms</head></p>






</body></text></cesDoc>