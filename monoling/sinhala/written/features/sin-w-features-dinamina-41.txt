<cesDoc id="sin-w-features-dinamina-41" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-dinamina-41.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>වැඩි මිලක් වෙනුවට වැඩි නිපැයුමක්</p>

<p> </p>

<p>සිය නිෂ්පාදනයන්ට වැඩි මිලක් ලබා ගැනීමට කාගේවත්
අකැමැත්තක් නැත. නමුත් නිෂ්පාදනවලට වැඩි මිලක් ලබා ගැනීමේ හැකියාව එන්න එන්නම
අභියෝගයන්ට ලක්වෙමින් ඇත. මේ නිසා වැඩි මිලක් වෙනුවට යෙදො පිරිවැය තුළින්ම වැඩි
නිෂ්පාදනයක් ලබා ගැනීම සඳහා වැඩි අවධානයක් යොමු කිරීම යුග අවශ්යතාවයක් බවට පත්වෙමින්
පවතී.</p>

<p>      úfYaIfhka u
wo fï rfÜ ù ksIamdokh fuu wNsfhda.hkag uqyqK § we;' ù ksIamdokh i|yd wdfhdackh
jk fhojqïj, ñ, fndfyda fia by</p>

<p>      f.dúhdg
mdvqjla fkdjkakg;a mdßfNda.slhdg ßoùula fkdjkakg;a ù ñ, hï idOdrK uÜgul
mj;ajdf.k hd hq;=h' fome;a;u i;=gq lrñka fuu ñ, uÜgu mj;ajdf.k hdu fnfyúkau
wiSre lghq;a;ls'</p>

<p>      rcrg uyje,s
l=Uqre lÜáhla ^wlalr 2 1$2la&amp; j.d lsÍug yd wiajkq ,nd .ekSu olajd remsh,a
;sia oyil ;rï úhoula orkakg isÿ fõ' fuu l=Uqre lÜáhlska ±ka ,efnk idudkH wiajkq
m%udKh ù lsf,da 4000 la muK fõ' ù lsf,dajla remsh,a 10 ne.ska wf,ú wf,ú
lf</p>

<p>      ù lsf,da
4000 la ksmojk l=Uqfrka ù lsf,da 5000 la ,nd .; yels jqjfyd;a ù ñ, jeä fkdlr
f.dúhdg tu ,dNhu ,nd .ekSug mq¿jkalu ;sfí' fï i|yd Wiia ì;a;r ù Ndú;h yd
fldïfmdaiaÜ fmdfydr Ndú;h jeo.;a jk nj lDIs m¾fhaIK ks,OdÍyq lsh;s' </p>

<p>      fyd| ì;a;r
ù Ndú;fhka wiajkq m%udKh ishhg úis mylska jeä lr .; yels nj m¾fhaIK j,ska
fidhdf.k ;sfí' ta ksid ù ñ, i|yd igka lrk f.dùkag kj ux fm;la fy</p>

<p>      úfYaIfhka ù
j.dj iq,Nj flfrk rcrg .;a úg - 1998 uy lkakfha§ rcrg l=Uqre wlalr 37079 l ù j.d
lrk ,o w;r fï i|yd ì;a;r ù nqi,a 741580 la wjYH úh' fï wjYH;djhg lDIsl¾u
foamd¾;fïka;=fjka iemhsh yels jQfha ì;a;r ù nqi,a 14500ls' fï wkqj fuu lkakfha
rc rg ì;a;r ù ysÕh ù nqi,a 668730 la úh' 
fuhska ì;a;r ù nqi,a 280917 la lDIsl¾u fomd¾;fïk;=fjka imhk ,o w;r
fm!oa.,sl wxYfhka ì;a;r ù nqi,a 88864 la imhk ,§' túg ì;a;r ù nqi,a 2499219 l
ysÕhla we;s úh' fuu ì;a;r ù m%udKh f.dùka w;g m;a l  rcrg ù ksIamdokh ishhg
úismylska jeä jkafkah'</p>

<p>      ù ñ,
w¾nqohg tla úi÷ula f,i ì;a;r ù ksIamdokh úYd, jYfhka mq¿,a lsÍug;a - f.dùka
iqÿiq ì;a;r ù  Ndú;hg yqre lsÍug;a  rch l%shdud¾. kj l%shd ud¾. .; hq;=j ;sfí'</p>

<p>      fuys § rcrg
Y%S ,xld uy nexl= YdLdj m%foaYfha úúO fiajd iuqmldr iñ;s yjq,a lrf.k ì;a;r ù
ksIamdokh lsÍfï jevigyka Èh;a lsÍu wdrïN lr ;sfí' l,l isg ì;a;r ù ksIamdokfhys
kehe</p>

<p>      tmamdj, úúO fiajd iuqmldr iñ;sfha ì;a;r ù
ksIamdok jHdmD;sh ms  lkakhlg ì;a;r ù nqi,a 20000
la muK ksmofjkjd' fï m%udKh l</p>

<p>      fuu ì;a;r ù
jHdmD;shg w; ys; ÿka Y%S ,xld uy nexl=fõ m%dfoaYSh l</p>

<p>      wkd.;fha§
muKla fkdj ±kgu;a we;s fjñka mj;akd úúO wNsdfhda.j,g uqyqK §ug yels f.dú
wd¾Ólhla jy jyd ie,iqï l</p>

<p>      rc rg
lDIsl¾u - mY= iïm;a - mßir yd ëjr lghq;= wud;H weia' weï' pkaøfiak uy;d fï
ms</p>

<p>      wvqu jYfhka
rcrgg wjYH ì;a;r ù ál rcráka u ksmoúh 
hq;= w;r yeu rcrg f.dúfhla u ;ukaf.a j.djg iy;sl l</p>

<p> </p>

<p>බණ්ඩාර ඒකනායක.  </p>






</body></text></cesDoc>