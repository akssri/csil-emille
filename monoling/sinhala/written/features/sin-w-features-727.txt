<cesDoc id="sin-w-features-727" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-727.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>පොම්පියා
නගරය පොළවට යට කළ විසුවියස් යළි ගොරවයි</p>

<p> </p>

<p>පොම්පියා
නගරය පිහිටා ඇත්තේ විසුවියස් නමැති මහා කඳු පාමුල ය. මීට අවුරුදු සිය ගණනාවකට ඉහත දී
විසුවියස් කන්ද පුපුරා යෑම නිසා ඉන් ඇද හැන්ථනු ලෝදිය හා දසත විහිදී ගිය අඵහුණු
හේතුවෙන් පොම්පියා නගරයත් එහි ජීවත්වුන මිනිසුනුත් විනාශ වු බව ඉතිහාසය කියයි.</p>

<p>පොම්පියා
නොහොත් පොම්පෙයි නගරය මෙවන් විනාශයකට පත්වුයේ දේව ශාපයක් නිසා යැයි සමහරු මත
පළ කරති. ඇත්ත වශයෙන්ම පොම්පියා නගරය විනාශ වුනේ දේව ශාපයක් නිසාද? පැරණි
රෝමන්වරුන්ගේ දේව විශ්වාස හා කතා වලින් කියවෙන්නේ පොම්පියා නගරයේ විනාශයට දේව
ශාපයක් මුලික වී ඇති බවයි. එය මිත්යා විශ්වාස හා මිත්යා ප්රබන්ධ පදනම් කරගෙන ගොතන ලද්දක්
බව සමහරු කියති.</p>

<p>කවුරු
මොනවා කීවත් පොම්පියා නගරය විනාශ වී ඇත්තේ දේව ශාපයකින් නොවන බව සක්සුදක් සේ
පැහැදිලි ය. එය ස්වභාව ධර්මයාගේ බලපෑමක් මත සිදුවුවකි. අප එසේ කියන්නේ පොම්පියා
නගරයත්, එහි විනාශවී ඇති ගොඩනැගිලිත් සියසින් දැක බලා ගැනීමේ අවස්ථාවක්
පසුගියදා අපට ද උදාවු බැවිනි.</p>

<p>රුදුරු
ඛේදවාචකයකට මුහුණ දුන් ඓතිහාසික පොම්පියා නගරය පිහිටා ඇත්තේ ඉතාලියේ නාපෝලි නගරයේ
සිට මයිල පහළොවක පමණ දුරිනි.</p>

<p>පොම්පියා
නගරය අද එකම නටබුන් ගොඩකි. ගරා වැටුන මහා මන්දිරද ගෙවල් දොරවල් ද කඩ සාප්පුද
අබලන් වු මංමාවත්ද හැම තැනින්ම දිස් වේ. ලෝදියෙන් පිළිස්සී අන්ථහුණුු තැවරුණ
මළමිනී තවමත් තැන තැන දැක්ක හැක. නටබුන් නගරයක් බවට පත්වී ඇති ඓතිහාසික
පොම්පියාව දැනට ආරක්ෂා කරනු ලබන්නේ එම ප්රදේශයේ ඇති නගර සභාව මඟිනි.</p>

<p>පොම්පියා
නගරයේ ආරම්භය ක්රි.ව. 8 දී පමණ සිදුවුවක් බව පොත පතේ සඳහන්  වේ. ග්රීක ජාතිකයකු වන හෙරක්ලීස් මෙම නගරයේ
නිර්මාතෘවරයා ය. මෙහි මුල් පදිංචි කරුවන් ග්රීකයන් බව ඉතිහාස සටහන් වැඩිදුරටත් තහවුරු
කරයි. පසුව එය ඔස්කන් නමැති ජාතිකයකුට යටත්ව තිබී ක්රි.ව. 89 දී පමණ රෝම අධිරාජ්යයට
අත්පත් විය. එතැන් සිට මෙම නගරය ග්රීක හා රෝම සභ්යත්වයට අනුව හැඩ ගැසුණ බවත් කියති.
දේවශාපයක් නිසා පසු කලකදී විනාශ වී ගියා යැයි කියනු ලබන පොම්පියා නගරය එකල අනේක
වර්ගික ජනයාගෙන් පිරී සමෘද්ධියෙන් අගතැන් පත්වුන රාජ්යයක් වී තිබුණ බව ඓතිහාසික
තොරතුරු හෙළිදරව් කරයි. මෙම කාලය රෝම අධිරාජ්යයේ ස්වර්ණමය යුගයක් හැටියටද සලකනු
ලැබිණ. හැම වීදියක්ම නෘත්ය ශාලාවලින් ද, බීමහල්වලින් ද, අවන්හල් වලින්
ද, විනෝදාගාර වලින් ද, දුකෙළිහල් වලින් ද, වෙසඟනන් පිරි රසහල්
වලින්ද පිරි පැවතිණිි. සෞභාග්යයෙන් සියල්ල ආඪ්ය වීම හේතුකොට ගෙන මිනිස් ගුණ ධර්ම
පිරිහී ගොස් මුඵ නගරයම දුරාචාරයට, දුෂණයට ගොදුරු වී අවිචාර සමයකට පාතබා තිබුණ බව
ඉතිහාස ගත තොරතුරු හෙළි කරයි. හැම විටකම හැම තැනකම දකින්නට ලැබුණේ කමින්,
බොමින්, නටමින්, ගයමින්, විනෝදයෙන් හා කෙළි දෙළෙන් පසුවුණ
මිනිසුන් ය. කෙළි දෙළෙන් පසුවු පොම්පියාවේ ජනතාව කෙරෙහි කෝපවු දෙවියෝ
ක්රි.ව. 63 දී භයානක භූමිකම්පාවක් ඇති කොට නගරයේ ගොඩනැගිලි ගණනාවක් විනාශ කරමින්
මිනිස් ජීවිත සිය ගණනක් බිලිගත් බවද එක් පුරාවෘත්තයකින් කියවේ.</p>

<p>ලෝක
ප්රකට විසුවියස් ගිනිකන්ද පිහිටා ඇත්තේ පොම්පියා සහ හර්කියුලේනියුම් යන නගර අයිනේය.
සරළව කියහොත් මෙම නගරද්වය පිහිටා ඇත්තේ විසුවියස් කන්ද පාමුලය. විසුවියස් කන්ද
කොපමණ උස එකක් දැයි කිසි තැනක සඳහනක් නැතත් එය අපගේ හුන්නස්ගිරි කඳු වැටිය තරම් උස
එකකි. මෙම මහා කන්ද කිසියම් දිනක පුපුරා ගිනි ගෙන මහා විනාශයක් පොම්පියා නගරයට
සිදුකරනු ඇතැයි කිසිවෙක් සිහිනෙකින් වත් නොසිතන්නට ඇත. එහෙත් සිදුවුයේ නොසිතු
නොපැතු දෙයකි. මහා විනාශයකි.</p>

<p>පොම්පියා
නගරයට මහා විනාශයක් සිදු කරමින් ක්රි.ව.63 දී ඇතිවු මහා පිපිරීම එතැන් සිට සුන්ථ වශයෙන්
කීප විටක්ම සිදුවී ඇති බව වාර්තා විය. එහෙත් අර තරම් මහා විනාශයක් නම් සිදුවී නැත.</p>

<p>දැන්
අපි යළිත් විසුවියස් කන්දේ වර්තමාන තත්වය දෙසට නෙත් යොමු කරමු. විසුවියස් කන්දට
බොහෝ කලකට පසු නැවත වරක් මහා පිපිරීමකට සුදානම් වෙන බවක් ඉතාලියේ භූගර්භ
විද්යාඥයෝ අනතුරු හඟවති. මෙම මහා පිපිරීම කොයි මොහොතක හෝ සිදුවිය හැකි බවත් මෙම
පිපිරීමෙන් විසුවියස් කන්ද පාමුල ඇති පොම්පියා සහ හර්කියුලේනියම් යන නගරද්වය විනාශ වී
දහස් ගණනකගේ ජීවිත නැතිවීමට ඉඩ ඇති බවත් ඔවුහු වැඩිදුරටත් කියා සිටිති.</p>

<p>උගෝ
කොරාටි, විසුවියස් කඳු වැටියේ ම ජීවත්වෙමින්, එහිම වැඩ පලක යෙදී සිටි සාමාන්ය
වැසියෙකි. 1944 දී විසුවියස් කන්ද පුපුරා යන විට ඔහු දොළොස් වියේ පසු වු කොලූ
ගැටයෙකි. මෙම පිපිරීමෙන් ගලාගිය ලෝදිය ගංගාව හේතුකොට ගෙන නිවාස අටසියයක් පමණ
විනාශ වුන බවත් මිනිස් ජීවිත සිය ගණනක් නැතිවුන බවත් කොරාටි කියා සිටියි. ශාන්ත
සෙබස්තියානෝ සහ මස්ස, ද සොම්මා යන ගම්මාන දෙක මෙම පිපිරීමෙන් විනාශ වී ගිය බවද
කොරාටි කියා සිටියි.</p>

<p>හතළිහේ
දශකයේදී සිදුවු මෙම විනාශයෙන් පසු ඉතාලියේ භූගර්භ විද්යාඥයෝ විසුවියස් කන්දේ පිපිරීම්
ගැන දැඩි සැලකිල්ලක් දැක්වීමට පටන්ගත්හ. යළි කන්ද පිපිරේ දැයි සොයා දැන ගැනීම
සඳහා ඔවුහු නවීන පන්නයේ තාක්ෂණ උපකරණ විසුවියස් කන්දේ තැන තැන වළලා නොයෙක්
විධියේ පර්යේෂණවල නිරත වීමට පටන් ගත්හ. දැනුදු මෙම පර්යේෂණ  දිනපතා සිදුවේ. හදිසියක් වුවහොත් කඳුපාමුලින් ඉවත් වෙන ලෙස මහජනතාවට
අනතුරු හඟවන සංඥා යන්ත්ර ද තැන තැන සවිකොට ඇති බව කොරාටි කියා සිටියි. එහෙත් මේ
මොන විධියේ යන්ත්ර සුත්ර සවිකොට තිබුණත් විසුවියස් කන්ද යළි කොයි මොහොතක හෝ
කාටත් හොරා පුපුරා යාහැකි බවත් මෙම පිපිරීමෙන් තමන් ඇතුන්ථ සිය දහස් ගණනක් ලාවා
ඕගයට හසුවී මිය පරලොව යාහැකි බවත් කොරාටි මත පළ කරයි.</p>

<p>මේ
අතර භූගර්භ විද්යාඥයන් කියා සිටින්නේ මෑතකදී සිට විසුවියස් කන්දේ හැසිරීමේ රටාවේ කිසියම්
විපර්යාශයක් හා අමුත්තක් දකින්නට ඇති බවයි. කන්ද ඇතුලේ කිසියම් චලනයක් සිදුවෙමින්
පවතින බවද එය පිපිරීමේ සලකුණ කිසියම් මාර්ගයකින් කියා පෑමක් බවද ඔවුහු වැඩිදුරටත් කියා
සිටිති. තව කොයි මොහොතක හෝ සිදුවීමට යන්නේ 1944 දී සිදුවුණු පුංචි පිපිරීම වැනි
පිපිරීමක් නොවේ. අවුරුදු දෙදහකට පසු මෙවර සිදුවීමට යන පිපිරීම ඉතාමත් බිහිසුණු දරුණුම
පිපිරීම විය හැකි යැයි භූගර්භ විද්යාඥයෝ වැඩිදුරටත් සඳහන් කරති. </p>

<p>මිලියන
දෙකක ජනතාවක් දැනට විසුවියස් කන්ද පාමුල හා ඒ අවට නිවාස තනාගෙන පදිංචි වී සිටිති. තවත්
ලක්ෂ ගණනක් මේ අවට රැකියා වල යෙදී සිටින බවද කියති. විසුවියස් කන්ද කොයි මොහොතක හෝ
පිපිරී ගොස් මහා විනාශයක් සිදුවිය හැකි බව මොවුනට ඉව වැටී තිබේ. </p>

<p>ළඟදී
සිදුවිය හැකි යැයි අනුමාන කරන මහා පිපිරීමෙන් මිලියනයක ජනතාව මොහොතකින් ජීවිතක්ෂයට
පත්විය හැකි බව ඉතාලියේ භූගර්භ පර්යේෂණවල යෙදී සිටින මහාචාර්ය එඩෝ ආඩෝ ඩෙල්
පෙෂෝ කියා සිටියි. 'එපමණක් නොවෙයි විසුවියස් කඳු පාමුල ඇති විශාල භූමි ප්රමාණයකුත්
මොහොතකින් දුවිලි බවට පත්විය හැකිය.' පෙෂෝ අනතුරු අඟවයි.</p>

<p>1037
වර්ෂය වෙන තෙක්, අවුරුදු සියයට වරක් විසුවියස් කන්ද පුපුරා ගියේ ය. ඉන්පසු
අවුරුදු 600 ක් ගතවෙන තෙක් විසුවියස් නිහඩ විය. එහි යළිත් වරක් පිපිරීමක් හට ගත්තේ
1931 දීය. මෙම පිපිරීමෙන් මිනිසුන් හාරදාහක් පමණ ජීවිතක්ෂයට පත්වුහ.</p>

<p>යළි
විසුවියස් ගිනි කන්ද පිපිරී ගියහොත් සිදුවිය හැකි මහා විනාශය අවම කර ගැනීම සඳහා දැනටමත්
අවශ්ය ආරක්ෂක පියවර ගෙන ඇති බව දැනගන්නට ලැබෙයි. එහෙත් ගෙන ඇතැයි කියන එවන්
ආරක්ෂක ක්රියාදාමයන් ගෙන් වැඩක් වේද?</p>

<p>විසුවියස්
කන්දේ මහා පිපිරීමක් හෝ සුන්ථ පිපිරීමක් හෝ ඇතිවන්නේ නම් ඒ බව කල්තියා දැනගත
හැකි බව භූගර්භ විශේෂඥ මහාචාර්ය ඩෙල් පෙෂෝ සඳහන් කරයි. හිරු එළියේ තද කමට
භූගර්භය තුළ ගල්කැට උණු වෙයි. මෙම ගල්කැට වතුර සමග මිශ්රවී ලෝදිය (ලාවා) බවට
පෙරළෙයි. භූගර්භය රත්වීමෙන් ගෑස් වර්ගයක් හටගනී. මෙය ඉතා සැර ගෑස් වර්ගයකි. මෙම
ගෑස් වර්ගය නිසා භූගර්භය තුළ එකතු වෙමින් පවතින ලෝදිය උණු වී කැකෑරීමට පටන් ගනී.
භූගර්භයට දරා සිටීමට බැරිවු විට මෙම ලෝදිය දහර කඳු ශිඛරය පුපුරා ගෙන එළියට පනී.</p>

<p>විසුවියස්
ගිනි කන්ද පිපිරීමට පෙර එය තුළ විශාල භූ චලනයක් ඇතිවෙන බව මහාචාර්ය ඩෙල් පෙෂෝ
කියා සිටියි. මෙම භූ චලනය කල්තියා දැන ගැනීම සඳහා දැනටමත් විසුවියස් භූ ගර්භය තුළට
විද්යුත් උපකරණ ටික බස්සවා ඇති බවත් එම උපකරණ වලට සවිකොට ඇති රේඩා චන්ද්රිකා
මාර්ගයෙන් එම චලනයන් පිළිබඳ තොරතුරු ලබා ගන්නා බවත් මහාචාර්ය ඩෙල් පෙෂෝ සඳහන්
කරයි. කෙසේ වෙතත් කොයි මොහොතක හෝ විසුවියස් ගිනිකන්ද යළි පුපුරා ගොස් මහා
විනාශයක් ඇති වීමට ඉඩඇති බැවින් ඒ ගැන ජනතාවට කල්තියා දැනුම් දී සිදුවිය හැකි යැයි
සිතන මහා අනතුර අවම කර ගැනීමට රේඩා චන්ද්රිකා සංඥා බෙහෙවින්ම උපකාර විය හැකි බවත්
මහාචාර්ය ඩෙල් පෙෂෝ මත පළ කරයි.</p>

<p>විසුවියස්
කඳු වැටිය ආශ්රිතව ජීවත්වීම බෝම්බයක් ළඟ තබාගෙන සිටීම වැනි වැඩකි. කොයි මොහොතක
හෝ එය පිපිරී මහා විනාශයක් ඇති විය හැකි බැවිනි. එසේ නම් එවැනි බිහිසුණු අනතුරක් දැන
දැනම සිය දහස් ගණනක් විසුවියස් කඳු වැටිය ආශ්රිතව ජීවත් වෙන්නේ ඇයි?</p>

<p>විසුවියස්
මහා කඳු වැටිය ගොඩ නැගී ඇත්තේ ඉතා සරු සාර පසක් එක්වීමෙනි. මෙම සරුසාර පස සෑදී
ඇත්තේ ගිනි කන්ද පුපුරා යෑමේදී සතර වටේ විසිරී යන ලාවා අන්ථ හුණු කැටි මිශ්ර වීමෙනි.
අන්ථහුණු මිශ්ර මෙම පස හඳුන්වනු ලබන්නේ තෙප්රා සොයිල් යනුවෙනි. අන්ථහුණු මිශ්රිත මෙම
සරුසාර පොළොවේ ඕනෑම බෝග ජාතියක් ඉතාමත් සරුවට වැවෙන බවත් විශාල අස්වැන්නක්
ලබා ගැනීමට පුඵවන් බවත් විසුවියස් කඳු වැටිය අසල ජීවත්වෙන ගොවියෝ කියා සිටිති. කොපමණ
අනතුරු දායක වුවත් ඔවුහු මෙම කඳු වැටියේ ම ජීවත් වෙන්නේ ගොවි තැනට සරු බිමක් එහි
ඇති බැවිනි. මිදි, තක්කාලි, බෝන්චි, මල්ගෝවා, න්ථෑනු,
දොඩම්, ලෙමන්, ඔසු පැළෑටි සහ මල් වර්ග ඉතාමත් සරුවට මෙහි වගා වෙන බව
කියති. මොවුන් ඉසුරු බර පීවිතයක් ගත කරන්නේ මෙම සරුසාර පොළවෙන් ලැබෙන සරු
අස්වැන්න අලෙවියෙන්  සොයා ගන්නා රන්
කාසිවල පිහිටෙනි.</p>

<p> </p>

<p> </p>






</body></text></cesDoc>