<cesDoc id="sin-w-features-divayina-377" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-divayina-377.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head> </head></p>

<p><head> </head></p>

<p><head>cd;sldNsudkh
yuqfõ ;dkdm;s Oqrh</head></p>

<p><head>oyih jk ishji
mqrdu Y%S ,xldfõ my;rg ;u n,h ;yjqre lr.;a mD;=.SiSkag fodka cqjka O¾umd, rcq
úiska ;u rdcHh ;E.s Tmamqjlska ,shd ÿkafka 1580 jif¾h' mD;=.SiSkaf.a w,af,a kgk
rElvhla muKla jQ ta rcq 1597 § ñh.sfhah' 1592 § isÿjQ iS;djl rdcisxyhkaf.a
urKska miq tu rdcHho flá l,lska wNdjhg .sfhah' tfia fyhska Èjhsfka W;=re"
ol=Kq iy ngysr fmfoiaj, wiydh iajdóka njg m;aùug mD;=.SiSkag yels úh'</head></p>

<p><head>fcrksfudao
wifõÿ kïjQ oreKq mD;=.Sis wdKavqldrhd b;d kskaÈ; f,i isxy,hka fm  wifõÿ kr rl=id isxy, daId lsÍug Tyq ;u fin¿kag wK
lf</head></p>

<p><head>;u fld,a,ldÍ
mrx.s fin</head></p>

<p><head>fï oreKq  wdl%uKslhka yuqfõ ;u foam</head></p>

<p><head>/l.;a  ta w;S; úrejkaf.a pß; fkdfmfka' ke;fyd;a jqjukdfjkau
ta foi fkdn,d isá;s'</head></p>

<p><head>mD;=.SiSka
fmrÈ.§ ,;a oreKqu mrdch Tjqkag uq,af,aßhd fjf,a§ w;alr ÿka iS;djl rdcisxyhkaf.a
msßySug fya;= jQfha wßÜG lS fjKavq kï úcd;sl WmfoaYlhdh' rdcisxyhka isxy,
ck;djf.a;a" uyd ix>r;akfha;a wm%idohg ,la ùfuka we;sjQ kdhl;ajfha ysvei
msrùug Wvrg fldkmamq nKavdr bÈßm;aúh' rdcisxy rcq úiska ;u mshd uerùfuka miq
f.dajg .sh fldkmamq nKavdr" lvq Ys,am olajd mD;=.SiSkaf.a is;a Èkdf.k
fodka fcdaka kñka fmkS isáfhah' 1590 § h</head></p>

<p><head>m  fodak
l;sßkd l=ußho ;uka i;= lr.;af;ah' tu.ska Wvrg isyiqkg lr,a,shoafoa mjq, i;=j
;snQ Wreuho úu,O¾uiQ¾h rcq i;= úh' i;=rkaf.a ksrka;r ;¾ckhg ,lajQ l=vd rdcHhl
md,lhd jqjo fï rc;=ud i;=j fld;rï wNsudkhla f;acila ;snqfKao hkak t;=ud ngysr
;dkdm;sjrekag ie,l+ wdldrfhka ms</head></p>

<p><head>Y%S ,xldjg
meñKs m%:u ,kafoais kej jQ ~~,d fn%ìia~~ 1602 uehs 30 jk Èk uvl,mq jrdfha
kex.=rï ,Efõh' tys fk!ldêm;s jQ fcdßia jEka iams,an¾cka úu,O¾uiQ¾h rcq yuqjg
meñKsfhah' ñka ikd: jk ;j;a lrekla  kï
uvl,mqj wh;a jk j;auka kef.kysr m</head></p>

<p><head>iams,an¾cka
ms</head></p>

<p><head>flfia jqjo
1603 § furgg meñKs iSn,aâ o pd¾Ü kï ,kafoais ;dkdm;shd iams,an¾cka ;rï m%{jka;
fkdùh' hqo Yla;sfhka m%n, ´,kaofha ;dkdm;shd jk ;ud lrk lshk ´kEu fohla fï l=vd
wdishd;sl rfÜ rcq bjid isá;ehs Tyq is;kakg we;' rc;=ud úiska ;uka ms  rcq iy ;dkdm;s w;r idlÉPdj isÿjQfha fofokdgu jegfyk nila jQ
mD;=.Sis nisks' rc;=udf.a wdrlaIl ks,OdÍyqo t;=ud rcùug m%:u mD;=.SiSka fj; isá
ld,fha mgkau t;=udg fiajh l</head></p>

<p><head>o pd¾Üf.a
urKska miq Tyqf.a kefõ isá wfkla ,kafoaiSka fj; rc;=ud fufia ,shd heùh' ~~iqrdj
fndkakd jrog fm  rglg tk
ksfhdað;hd hym;a pß;hla we;a;l= ñi mia 
mfjys .eÆKq whl= fkdúh hq;= nj t;=ud hejQ ,smsfhka meyeÈ,s flf¾' Tyq
´,kaofhka meñKsho fkda¾fõ ráka meñKsho th tfiauh'</head></p>

<p><head>úfoaYhl§ ;u rg
ksfhdackh lrk ;dkdm;shd ;u cd;sfha f.!rjh wdrlaId lrk" pß;j;a"
m%{jka; whl= úh hq;= nj;a Tyq ksod.; hq;af;a ;ks hykl nj;a fl!á,H mçjrhd mjid
we;' ngysrg fïjd wuq;= jqjo fmrÈ. ;dkdm;s fiajfha moku fuhhs' </head></p>

<p> </p>






</body></text></cesDoc>