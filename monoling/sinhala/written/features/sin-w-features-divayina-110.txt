<cesDoc id="sin-w-features-divayina-110" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-divayina-110.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>නවක වදයත්
රටට වදයක්</p>

<p> </p>

<p>රටේ  විධායක සියන්ඵ බලතල එකම
අතක පවතින අප රටේ අධ්යාපන අංශය භාරව කටයුතු කරන ගරු අධ්යාපන අමාත්යතුමා ඇතුන්ඵ
වගකිවයුතු පිරිසගේ අවධානය යොමු  කිරීමට මේ
කරුණු කීපය ලියන්නට අදහස් කළෙමි.</p>

<p> </p>

<p>1 වසරේ සිට 13 වසර දක්වා අධ්යාපනය ලබන ශිෂ්යයා 14 වන වසර ඉගෙනීම ලබන
ආයතනයට අන්ඵත් නමක් භාවිත කරයි. පළල් අරුතක් ඇති එය විශ්ව විද්යාලයයි. ගෝලීය
කරණය ගැන කථා කරන මේ යුගයේ රටක් යනු 
úYajfha tla .ïudkhls'  me;=reKq
nqoaêhlska úYaj ±kqulska is;k úg tfia lsj yel' fï jeks hq.hl ld,hg .e</p>

<p> </p>

<p>ශ්රී වික්රම රාජසිංහ රජතුමා මේ රටේ අම්මා කෙනෙක් ලවා ඒ අම්මගේම දරුවන්
වංගෙඩියේ දමා කෙටීමට අණ කළ හැටි අපේ ඉතිහාසය අපට කියයි. වරදක් කළා නම් ඒ අය
සොයා ගැනීමට බැරි තැන කරපු හරියයි ඒ.</p>

<p> </p>

<p>අද යුගය ගැන සිතමු. පසුගිය මෑත යුගයක අගනුවර පාරක පාසල් ශිෂ්යාවක්
පෞද්ගලික බසයකට හසුවී මියගිය වෙලාවේ රියදුරා පණ බේරාගෙන සැඟවී ඇත. කෝපයට පත්වූ
ගම්වාසීහු බසය පොඩිකර ගිනි තබා විනාශ කළා පමණක් නොව එම මාර්ගයේ සියන්ඵම පෞද්ගලික
බස්වලට අලාභ හානිකර සියන්ඵම බස් ගමන් නතර කළහ. කාගේ හෝ දරුවකුගේ ජීවිතය
දෙමවුපියන්ට අගනේය.  අප්රාණික බස් කුමක්
කරන්නද?</p>

<p> </p>

<p>බල්ලෙකුට ගලකින් ගැසූ විට බල්ලා සපා කන්නේ ඒ ගලයි. ගල ආ හැටි
නොබලයි. ඒ සතාගේ තරමයි ඒ.</p>

<p> </p>

<p>නවකවදය දුන් අය ඉගෙනීම අවසන් කර ගොස්ය. ඒ  පලිය ගන්නේ අන්ඵතෙන්  එම
විශ්ව විද්යාලයට ඇතුන්ඵවන කිසිත් නොදන්නා පිරිසගෙනි. එයත් රාජසිංහ රජුගේ නියෝගය
මෙනි.</p>

<p> </p>

<p>මානව හිමිකම් ගැන මිනිස් අයිතිවාසිකම් ගැන කථාකරන මේ යුගයේ මේ අන්ඵතෙන්
ඇතුන්ඵවන සිසුන්ගේ ඒ හිමිකම් ඔවුන්ට නැද්ද? අනුන්ට රිදවා සතුටුවීමේ අවිනීත චර්යාවකි
එය.</p>

<p> </p>

<p>මේ විධියට අනුන්ට හිරිහැර වන ලෙස හැසිරෙන විශ්ව විද්යාල සිසුන් ආපසු හැරී
තමන් ඒ තැනට ආ ගමන් මග ගැන සිතුවොත් නොමිලයේ නිල ඇඳුම් නොමිලයේ පොත්පත්
නොමිලයේ අධ්යාපනය 1 වසරේ සිට 13 වසර දක්වාම ලැබූ සැටිත් ගොඩනැගිලි පහසුකම් විද්යාගාර
පහසුකම්, ගමන් පහසුකම් ආදී අපමණ සේවා රැසක් පවතින  රජයෙන් භුක්ති විඳ මේ තැනට පැමිණි සැටිත් පෙනෙනු ඇත. එසේ මේ විශ්ව
විද්යාලයට ඇතන්ඵ වූ ගමන් 13 වසර අවසන් කර 14 වසර ගෙවීමට සූදානම් වනවිට ඇතිවන මේ මානසික
වෙනස පදනමක් නැති අනවශ්ය පෙරළියක් බව වටහාගත යුතුය.</p>

<p> </p>

<p>'අපට නේවාසික පහසුකම් දියව්', 'අපට ශිෂ්යාධාර දියව්', 'වහාම
විශ්වවිද්යාල අරිව්' යනුවෙන් පවතින රජයට නියෝග දෙන්නේ එසේ විශ්ව විද්යාලවලට ඇතුන්ඵ වූ
රටේම බුද්ධිමත් පිරිසයි. මෙය රටේ අධ්යාපන ක්රමයේ වරදක් විය නොහැකිද? </p>

<p> </p>

<p>මේ අය විශ්ව විද්යාලවලට එන්ට පෙර 1 වසරේ සිටම යැපුණේ පවතින රජයෙන් හා
තම දෙමවුපියන්ගෙනි. </p>

<p> </p>

<p>මිරිස් පැලයක් වත් සිටවා ලැබූ අත්දීකීම් ඇති අය හෝ කුළී වැඩක් හෝ
කර මුදලක් සොයාගත් අය  මේ පිරිසෙත් සිටිනවා
නම් ඒ ඉතා සුන්ඵ පිරිසකි. විශ්ව විද්යාල ජීවිත කාලය තුළ දේශනවලින් සටහන් කරගත් දීනීම
හැර වෙනත් නිර්මාණශීලී නිෂ්පාදනය කළ අන්ඵත් සොයා ගැනීමක් කළ අය සිටිනවා නම් ඒ
කීපදෙනෙකි.</p>

<p> </p>

<p>තමන්ට දේශන පවත්වන්නට එන කථිකාචාර්යවරුන්ට ගරු කරනවා තබා ඔවුන් නිවාස
අඩස්සියේ තබාගෙන කටයුතු කරන ඔවුන්ගේ මානසික තත්ත්වයේ තරම තේරුම්ගත හැක.</p>

<p> </p>

<p>පොදු දේපළවලට හානි කරමින් ආයතනයේ පරිපාලන රටාව වෙනස් කරමින් ගෙන
යන මේ අවිනීත කි්රයාවන් විශ්ව විද්යාල තුළින් ඉවත් කළ යුතුය.</p>

<p> </p>

<p>ජනවාර්ගික ප්රශ්න විසඳීමට කලක් ගත විය හැක. නමුත් මේ ප්රශ්න විසඳීමට රජයට
හැකි විය යුතුය.</p>

<p> </p>

<p>නිදහස වුවත් පමණ ඉක්ම වන්නේ නම් එහි අහිතකර බවක් දීකිය හැක. </p>

<p> </p>

<p>විශ්වවිද්යාල අධ්යාපනය පෞද්ගලීකරණය කළොත් තත්ත්වය කුමක් විය හැකිද?
තමන්් පැමිණි කාරණය මැනවින් කර ගැනීමම ප්රධාන අරමුණයි.  නවක සිසුන් සුහද ලෙසින් පිළිගෙන කටයුතු කළොත් ඉන් වන හොඳක්
මිස හානියක් නැත.</p>

<p> </p>

<p>සාකච්ඡාවෙන් ප්රශ්න විසඳීමටත්, නීති මගින් අයිතිය ලබා ගැනීමටත්
බැරිකමක් නැත. දේශපාලනය හැමදෙනාටම අවශ්ය නැත. එය බලෙන් පැටවීමද නොකළ යුත්තකි. </p>

<p> </p>

<p>අවසාන වශයෙන් කියමි. 'බැරිම තැන  බැරිවෙන කිසි දෙයක් නැත' කියා
කියමනක් තිබේ. ඒ වගේම 'ඇසේ අගය දීනෙන්නේ ඇස නැතිවූ දාටය.'</p>

<p> </p>






</body></text></cesDoc>