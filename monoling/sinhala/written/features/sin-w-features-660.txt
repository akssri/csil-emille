<cesDoc id="sin-w-features-660" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-660.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ර්</p>

<p> </p>

<p>දෙපක්ෂයක අරගලය නිමාව සඳහා</p>

<p>යෝජිත
සාම සාකච්ඡාව සඳහා අවශ්යවන න්යාය පත්රය විදේශ නියෝජිතයින් හා ප්රභාකරන් පමණක් සාකච්ඡා
කොට සකස් කිරීම සාධාරණද? එසේම එය මෙබර් බරපතල ප්රශ්නයක් නිරාකරණය කරනු සඳහා
යෝග්යයැයි නිගමනය කළ හැකිද? සාම්ප්රදායිකව පිළිගත් අයුරු හා ජාත්යන්තර ප්රජාව
වුවද පිළිගන්නා අයුරු මෙවැනි වැදගත් සාකච්ඡාවක් සඳහා ඒකපාක්ෂික නිගමනයක්
ලෝකයේ කොයි කවුරු වුවද පිළිගනිත්ද?</p>

<p>මෙහිදී
විශේෂයෙන්ම අවුරුදු විස්සක් පමණ අනේකවිධ පීඩාවට පත් කරන ලද බහුතර කොටස වන
සිංහලයින්ටත් එසේම වර්තමාන රජයටත් නිහඬව මේ ප්රභාකරන්ගේ ඊනියා අභීත නාදයෙන්
ප්රකාශයට පත්කරන ලද නිගමනය පිළිගත හැකිද? මේ ඊනියා න්යාය පත්ර සාකච්ඡාව මෙසේ හොර
රහසේ ඉරණමඩුවේදීම පැවැත්වූයේ ඇයි? මේ වැදගත් සාකච්ඡාවට රජය විසින් නියෝජනය
නොකළේ ඇයි? යුද්ධයට මැදිහත් වූ කෲරතම ප්රතිමල්ලවයා විසින් තමන්ගේ සිය අණසක
කිසිම යුක්ති ධර්මයක නොපිහිටා ප්රකට කිරීම යුක්ති සහගතද? එපමණක් නොව මේ ප්රබල
සාකච්ඡාවට සහභාගි වූයේ එක් අතකින් මේ රටේ පුරවැසිභාවයවත් නොමැති ඊනියා ඇන්ටන්
බාලසිංහම් නමැති ඊනියා න්යායාචාර්යයාත් එසේම ඔහුගේ බිරිඳත් අනිත් අතින් නෝර්වීජියානු
නියෝජිතයනුත් වීම සැළකිය යුතු සැක හා චකිතය දනවන්නක් නොවේද? විශේෂයෙන්ම
සළකතොත් බාලසිංහම්ගේ බිරියට ඒ සුදු නෝනාට මෙවැනි තීරණාත්මක සාකච්ඡාවකට නොබියව
සහභාගිවීමට ඇති බලය, වගකීම මෙන්ම අයිතිය කුමක්ද?</p>

<p>මේවා
පෙනුමට සැහැල්ලූ ප්රශ්න වූවාට මේ ගුප්ත ක්රියා මගින් කවදත් ප්රභාකරන් යටිකුට්ටු අයුරින්
මහජාතිය වූ සිංහලයින් රවට්ටා ඇති නිසාත් එසේම තැබූ විශ්වාසය තුන් වරක්ම කඩකළ
නිසාත් සුන්ථකොට තැකිය හැකිද? අපේ රාජ්ය නායකයන් මේ වනවිටත් මේ ඊනියා න්යාය පත්ර
සාකච්ඡාව පිළිබඳව කිසිදු ප්රකාශයක් නොකළේ ඇයි? ප්රභාකරන් නමැති සයිලොක්ගේ
අණසක මෙන්ම නිර්දේශ ඒ අයුරින්ම පිළිගනු සඳහා මුනිවත රකින නිසාද? එක කරුණක්
මෙහිදී මතුකොට දීක්වීම අතිශයින්ම වැදගත්ය. ප්රභාකරන්ගේ මායාකාරී යටිකුට්ටු රැවැටීම්වලට
ගොදුරු වී ඇති හෙළ සිංහළයා තවදුරටත් එසේවීමට කිසිසේත් සූදානම් නොමැති බව
අවධාරණයෙන්ම පැවැසිය යුතුය. ගිණිපෙල්ලෙන් බැට කෑ පුද්ගලයා කණාමැදිරියාටත් බිය විය
යුතුය යන වදන සිත්හි දරාගෙන සිංහලයා තවදුරටත් රවට්ටන කූට උපක්රමවලට ගරු අගමැති රනිල්
වික්රමසිංහ මහතා කසිසේත් අනුකුලතාව නොදීක්විය යුතු බව අපේ සිංහල බහුතරයේ එකම
අධිෂ්ඨාණය බව පැහැදිළිවම සඳහන් කළ යුතුය. ප්රභාකරන් ආඥාදායකයකුමෙන්
පුරවැසියෙකුවත් නොමැති ඇන්ටන් බාලසිංහම්ගේ අනුදීනුමද ඇතිව තීරණය කරන ලද ඊනියා
න්යාය පත්රය කුණුකූඩයට දමා අපක්ෂපාතී සාකච්ඡාවක් සිදු කරන්නට කැමැති නම් දෙපක්ෂයේම
පුර්ණ සහභාගිත්වයෙන් එසේ පළමුවෙන්ම සාධාරණවත් එසේම යුක්තිසහගතවත් එවැන්නක් තීරණය
කිරීම යෝග්යය. එසේ නොමැතිව මේ මස විසිහය වැනිදා කළ සාකච්ඡාව කෙළින්ම
ප්රතික්ෂේප කිරීම කවදත් නිර්භීතව වැඩ කළ සිංහල ජාතියේ එකම අපේක්ෂාව බව සෘජු
ලෙසම අවධාරණය කළ යුතුය. සටන් විරාමයක් ප්රකාශ කොට එක සමානව දෙපාර්ශ්වයම එකඟ වූයේ
නම් යෝජිත සාම සාකච්ඡාව දෙපැත්තේම පුර්ණ සහයෝගයෙන් සාධාරණව පැවැත්විය යුතු බව
අමුතුවෙන් පුනපුනා කිව යුතු නොවේ. මේ සිදුවන දෙයින් පෙනී යන්නේ මේ යෝජිත සාකච්ඡාව
සාධාරණවත් යුක්තිසහගතවත් පැවත්වේදීයි දීවැන්ත සැකයකි. විමතියකි. තවත් වැදගත් දෙයක්
මනාව මතුවේ. මේ සකස් කරන ලද ඊනියා න්යාය පත්රයේ කුමක් අඩංගු වුවාදීයි රජය වත් අඩු
වශයෙන් දන්නේද?</p>

<p>න්යාය
පත්රයේ අඩංගු දේ පිළිබඳව කිසිම ප්රකාශයක් ප්රභාකරන් හෝ නොර්වීජියානු නායකයින්
සිදු නොකළේ ඇයි? මෙවැනි ගැටන්ථසහගත ප්රශ්නයක ස්වභාවය හා එය විසඳන ආකාරය
පිළිබඳ සැකැසුණු න්යාය පත්රයේ අඩංගු වන දේ සිංහලයින් ප්රධාන වශයෙනුත් හමේබත් වූ
ද්රවිඩ ජනතාව සාමාන්ය වශයෙන් දීන නොගත යුතුද?  
b;d újD;j idlÉPd l</p>

<p>ඔහුගේ
එම ප්රකාශවලින් පෙනෙන්නේ ඔහු දීනටමත් ජය ලබා සව්දිය බොන්නාක්සේය. මේ නිසා අපේ
අගමැතිතුමා ඇතුන්ථ වගකිවයුතු සියන්ථ දෙනාම මේ ප්රශ්නය සඳහා න්යාය පත්රය සකස් කළ
යුත්තේත් අවසාන වශයෙන් සාම සාකච්ඡාව පැවැත්විය යුත්තේ දීඩි වගකීමෙන් හා අනාගත
අපේක්ෂා පිළිබඳ නිවැරදි අවබෝධයෙන් බව අවධාරණය කළ යුතුය.</p>

<p>න්යාය
පත්රය පිළිබඳව නිශ්චිත යමක් ප්රභාකරන් සඳහන් කර නැතත් ඔහු අවුරුදු විස්සක පමණ කාලයක
සිට නිබඳව නිරන්තරයෙන් සිත තුළ කිතිකැවුනු ඔහුගේ නිජබිම පිළිබඳ වූ ඊනියා
සංකල්පය පහත සඳහන් අයුරු ආයෙත් වරක් නිශ්චිත වචනවලින් මෙසේ ප්රකාශයට පත්කර ඇති
ආකාරය ප්රවෘත්ති පත්රවලත් එසේම විද්යුත් මාධ්යවලත් සඳහන් විය. ඒ ප්රකාශයේ ප්රධාන
අරමුණු පහක් මෙසේ පෙළගස්සා තිබුණු බව නොදන්නා කෙනෙකු නැත. ඒ ඔහුගේ කවදත්
සුපුරුදු තීරණ බව ඉතා පැහැදිළිය.</p>

<p> </p>

<p>1.
එල්.ටී.ටී.ඊ. සංවිධානය වෙත පනවා ඇති තහනම ඉවත්කිරීම</p>

<p>2.
උතුරු නැගෙනහිර  ධීවරයින්ට නිදහසේ මසුන්
මැරීමට අවස්ථාව සළසා දීම</p>

<p>3.
මසුන් මැරීම සඳහා දීනට පනවා තිබෙන තහනම ඉවත්කිරීම,</p>

<p>4.
උතුරු නැගෙනහිර පළාත්සභාවේ පාලනය එල්ටීටීඊ. සංවිධානයට ලබාදීම </p>

<p>5.
රජය සමග සාම සාකච්ඡාවට අවතීර්ණවීමට නම් දීනට එල්.ටී.ටී.ඊ සංවිධානයට පනවා තිබෙන තහනම
ඉවත්කිරීම</p>

<p> fï ;SrK ke;fyd;a wfmalaId myu tlfia
m%Ndlrkaf.a l,la meje;s wNsu;d¾: m,.kajd.kq i|yd bÈßm;a l  ,xldfõ ;ykïfldg we;s t,a'à'à'B ;yku
bj;alrjqjfyd;a wm rg wdfh;a jrla m%Yakh W.%j meje;s ;ekg h  m%Ndlrkag wjYHj
we;af;a idu idlÉPdjg jvd Tyqf.a f,a msmdihg 
mdol jQ Tyqf.a ixúOdkh hg i|yka ;ykñka bj;alrjd .ekSu nj ;¾ldkql=</p>

<p>ප්රභාකරන්
කියන අයුරු සමානව මේ යෝජිත සාම සාකච්ඡාවට සහභාගිවීම අවශ්ය නැත. උසාවියක වුවද වරද
කළ අයෙකු එම වරදින් නිදහස්කොට පසුව වරද කළ පක්ෂය නිදහස් කිරීමේ පිළිවෙතක්
කොහෙත්ම නැත. තහනම ඇතිව හෝ නැතිව මේ සාම සාකච්ඡාවට ප්රභාකරන් ඇතුන්ථ කොටි  පිරිස එන්නේ ශ්රී ලාංකික සිංහලයාගේ මෙන්ම ද්රවිඩයන්ගේද
යහපත සඳහාය. සාමය ගොඩනැගුණොත් ආයෙත් තහනමක් පිළිබඳ කථාවක් හෝ අවශ්යතාවක්
කෙසේවත් නැත. ඒ නිසා සාමය උදාවනවාත් සමගම තහනම ඉවත්වීමත් සමස්ත ශ්රී ලංකාවටම සාමය
උදාවීමත් ඉර පායන්නාක් වැනි සත්ය සිද්ධියකි. මේ නිසා සාධාරණවත් අපක්ෂපාතීවත් සිතනවිට
ප්රභාකරන් දතට දත යන කියමන සේ කෲර ලෙස කි්රයා නොකොට ජාත්යන්තර ප්රජාව වුවද
සක්සුදක්සේ පිළිගෙන ඇති අයුරු පවතින තත්ත්වයෙන්ම යෝජිත සාකච්ඡාවට
ඒකාන්තයෙන්ම සහභාගිවිය යුතුය. ප්රභාකරන් අවංකවම යෝජිත සාම සාකච්ඡාවට අවතීර්ණ වූයේ
නම්  මේ අනවශ්ය ප්රශ්න නගා තවත් දුරට මහ ජාතිය
නොහොත් සිංහලූන් නොපෙළවිය යුතුය. ඔහු හෘද සාක්ෂියටම අනුව මේ රටේ හැට
දහසකට  අධික අහිංසක ජනතාවක් ඝාතනය කොට
එසේම මේ රටේ විවිධ ගණයේ භෞතික  සම්පත්
විනාශ කළ කෙනෙකු බව නිරනුමානයෙන්ම දන්නෙකි. පොඩි ළමයකු මෙන් මෙවැනි උදාර
එසේම පණ්ඩිතවේදනීය වූ සාකච්ඡාවකට කෙසේ හෝ සහභාගි විය නොහැකි  බව තරයේම සිතිය යුතු බව අවධාරණය කළ යුතුය.</p>

<p> ;j;a jeo.;a lreKla u;= lsÍu w;sYhskau
jeo.;ah' hg i|yka Tyqf.a .=ma; wfmalaIK j,ska fmfkkafka Tyq;a Tyqf.a i.hd;a fï
±jeka; udrdka;sl hqoaOh ;snqKq ;ekgu wdfh;a yrjkak ±hs ielhla u;=fjhs' Tyqf.a
;%ia;jd§ ixúOdkh f,dalfha m%n, rgj,a úiska ;ykï fldg we;' fmrfuka ta iydh jQ
rgj,a j,ska uqo,a .,d tkafkao ke;' ta ksid Tyq wfyakshlg ke;fyd;a idudkH
miqnEulg m;aù we;s nj Y%S ,dxlslhd;a tfia c.;a m%cdj;a fyd¢ka oks;s' fï
;;a;ajfhka ñ§ug m%Ndlrka .kakd  ;j;a l+g
Wmdhla±hs fï ms</p>

<p> ;j;a jeo.;a fohla flf¾ mdGl wjOdkh fhduq lrùu
jeo.;ah' wjqreÿ úiaila muK È.= ld,hla ñksia f,a msmdifhka uqim;aj w;s oreKq f,i
ls%hd l  wfma w.ue;s;=ud;a
md,l m¾Ioh;a  tfiau fï ms</p>

<p> rfÜ fmdÿ wdrlaIdj i|yd;a tfiau wjdikdjka;
wjia:djla hï fyhlska Woa.;jQfjd;a tu wjia:djg WÑ; f,i Yla;su;a f,i fukau ks¾NS;
f,i uqyqK§u i|yd wfma .=jka yuqodj  mdn,
yuqodj kdúl yuqodj yd úúO jQ iafõÉPd 
fiakdxl iSrefjka ;eîu b;d m%«f.dapr nj i|yka l  fomd¾Yajhu tl;=j ;ju
WÑ; kHdh m;%hla j;a ilid .ekSug WÑ; jQ wjxl idOdrK fiau hqla;siy.; jd;djrKhla
f.dvke.=fKa kï fï bÈß wkd.; fhdað; ie  ksYaÑ; wkdjels m</p>

<p>bkaÈhdfõ fyda ;dhs,ka;fha fyda fï fhdað; idu idlÉPdj meje;aùug
m%Ndlrkag wjYHj we;af;a wehs@ igka úrduhla fyd¢ka ls%hdlrk fï Y%S ,xldfõu
fujeks idlÉPdjlg iqÿiq ;ekla keoao@ nKavdrkdhl iïuka;%K Yd,dj fyda mokï wdh;k
Yd,dj fï idlÉPdjg fkd.e</p>

<p>මහාචාර්ය
හෙන්රි වීරසිංහ,</p>

<p>කොළඹ විශ්වවිද්යාලය.</p>






</body></text></cesDoc>