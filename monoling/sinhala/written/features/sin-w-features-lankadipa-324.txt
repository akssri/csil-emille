<cesDoc id="sin-w-features-lankadipa-324" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-lankadipa-324.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p> </p>

<p>සරසවි තුළ යහපත් ම තත්ත්වයක් අඛණ්ඩව තබා ගන්න බැරි ද?</p>

<p>රුහුණු විශ්ව විද්යාලයේ සිසුන් දෙපිරිසක් අතර මේ නොබෝදා ඇති වූ
ගැටුම්වලින් තුවාල ලැබූ ශිෂ්යයෝ තිදෙනෙක් මාතර රෝහලට ඇතුළත් කරනු ලැබ සිටිති
යි ඊයේ වාර්තා වූයේ ය. මේ ගැටුම්වලින් එකක් හට ගෙන ඇත්තේ මාතර එස්. කේ. ටවුම
මුහුදු වෙරළේ දී ය. අනෙක සරසවි බිමේ දී ය. වාර්තා වන පරිදි තුවාල ලත් සිසුන් දෙදෙනකු
සරසවි මහා ශිෂ්ය සංගමයේ සාමාජිකයන් වන අතර අනෙක් සිසුවා ස්වාධීන ශිෂ්ය සංගමයේ
සාමාජිකයෙකි.</p>

<p>ගැටුම්වලට හේතුව හෝ හේතු හෝ කවරේ දී යි වාර්තා වී නොතිබිණ.
පෞද්ගලික කොන්තරයක් ද ? සරසවි දෙක අතර මත භේදයක් ද ? දේශපාලන ස්ථාවරයන්
පිළිබඳ  ප්රශ්නයක් ද? සරසවි ශිෂ්ය
ප්රශ්නයක්් හා සම්බන්ධ කරුණක් ද?</p>

<p>හේතුව කුමක් වූවත් රෝහල් ගත කරන්නට සිදු වන තත්ත්වයට ගැටුම්
ඇති කර ගත යුතු ද? සරසවි පිවිසෙනුයේ දියුණු යැයි 
lsj yels nqoaê uÜgula we;s wh hs' tjka msßila fufia ldhsl jYfhka ydks jk
;;a;ajhla olajd wek fldgd .; hq;= he hs wms fkdis;uq' th we;a; jYfhka u nqoaêhg
ks.dfjls' lsisfia;a m%«f.dapr fkd fõ'</p>

<p>මේ දෙපිරිස අතර කිසියම් ප්රශ්නයක් හෝ ප්රශ්න කීපයක් හෝ ඇති බව
පැහැදිලිය. එහෙම නම් ඒවා ගුටි බැට හුවමාරු කැරැ ගනිමින් විසඳා ගත හැකි ද? නොහැකි ය. ඒ
වග තේරුම් ගන්නට එතරම් බුද්ධි මට්ටමක් අවශ්ය යැයි අපි නොසිතමු.</p>

<p>ගුටි බැට හුවමාරු කර ගන්න තැන ඇති වෛරය වැඩි වෙයි. නැති වෛර
ඇති වෙයි. ප්රශ්න ඇත්නම් ඒවා තව තවත් සංකීර්ණවීම එහි අනිවාර්ය ප්රතිඵලය යි. එවන්
තත්ත්වයක් මතු වන තන්හි තව තවත් ගැටුම්වලට දොර විවෘත වෙයි. එයින් සරසවි වසා දමන්නට
සිදු වන තත්ත්වයක් ද බොහෝ විට උද්ගත වෙයි. සරසවි තාවකාලික ව හෝ වසා දමන විට
කෙළින් ම පාඩුව බහුතර ශිෂ්ය ශිෂ්යාවන්ට ය. අන්තිමේ රටට ය.</p>

<p> iriú wOHdmkh i|yd
jd¾Isl j remsh,a fldaá .Kkska jeh fõ' ta j. YsIH ix.ïj,g ryila úh fkdyels h' tu
wdfhdackh úmq, M,odhs wdfhdackhla le/ .; hq;= h' .y neK .kakg ie/fik ;ek th
úmq, M,odhs wdfhdackhla njg m;a le/ .ekau wiSre h'</p>

<p> fndfyda YsIH YsIHdjka
iriú jrï ,nkqfha by</p>

<p>සරසවි සිසුන් සිසුවියන් තමන් කැමැති දේශපාලන මත නොදීරිය යුතු  යැ යි මෙයින් අදහස් නො වේ. තමත් කැමැති
දේශපාලන අදහසක් මාර්ගයක් දර්ශනයක් දරන්නට ඔවුන්ට ඇත්ත වශයෙන් ම පූර්ණ නිදහස තිබිය
යුතුම ය. හරි හැටි කල්පනා කරන  ඕනෑම පක්ෂයකට
සම්බන්ධ වන්නට ද ඔවුනට පූර්ණ අයිතියක් තිබිය යුතුම ය. එය මූලික අයිතියෙකි. එහෙත් අනුව
අයුරින් ක්රියා කොට  තමන් ගේ අධ්යාපනය
කඩකප්පල් කැරැ ගත යුතු නැත. එපමණක් නොව අනෙක් සහෝදර සිසුන් සිසුවියන්ගේ අධ්යාපනය
ද කඩාකප්පල් වන තැනට ක්රියා කළ යුතු නැත. එයින් රටට සිදුවනුයේ ලොකු පාඩුවෙකි.</p>

<p>සමහර සරසවි සිසුන් අනුන් ගේ බළල් අත් බවට පත් වෙතොත් ඔවුන්
ගේ  බුද්ධියෙන් ඇති යහපත කිමෙක් ද? සරසවි
ශිෂ්යාවන් ජාතික ප්රශ්න ගැනත් අන්තර් ජාතික ප්රහ්න ගැනත් මනා අවබෝධයකින් සිටිය යුතු
බව ඇත්තය. ඒවා සාකච්ඡා කළ යුතු බවත් ඇත්ත ය. එයින් අයිතිවාසිකම් හා ප්රශ්න ආදිය
ගැනත් උචිත හැඟීම් සහිත ව සිටිය යුතු බවත් ඇත්තය. එහෙත් එසේ අවබෝධයකින් ප්රබුද්ධ ව
හැසිරීමත් බැහැරි ආත්මාර්ථකාමී සමහර දේශපාලන බලවේගවල අතකොන්ථ හෝ බළල් අත් හෝ
බවට  දීන හෝ නොදීන හෝ පත්වීමත් අතර
ලොකු වෙනසක් ඇත. අප මේ බව සඳහන් කරනුයේ ඒ පිළිබඳ ඉඟි වරින් වර දක්නට ලැබෙන
නිසා ය. ඒ වූ කලී ශිෂ්ය පරපුරට ඉතා ම අනර්ථකාරී තත්ත්වයකි. ඒ තුළ ගබ්ව ඇත්තේ
අනතුරෙක් ම ය. විනාශයක් ම ය. දශක කිහිපයක සිට සමහර සරසවි තුළ දක්නට ලැබුණු
අර්බුදවලින් ඒ වග තේරුම් ගැන්ම එතරම් අසීරු නො වේ.</p>

<p> iriú ;=  Ndrldr wd§kag;a fmdÿfõ rgg;a fyd|h' wkjYH l,n, fyda m%Yak fyda
we;s fkdjk ;ekg l%shd lsÍu tla w;lska iriú n,OdÍka f.a hq;=luhs' j.lSu hs'
we;eï iriú n,OdÍka isiqka f.a m%Yak 
f;areï .kakd njla fkdfmfka' f;areï .kakg yok njlao fkdfmfka' ovínrùu
kqjKg yqre ke;' iqLkuHNdjh wjYH h' nqoaêh fufyhjd ¥ro¾YSùu m%uqL;ajh ,eìh
hq;af;ls' wjq,a we;s lrkakg udk n,k ndysr n,fõ. ke;ehs is;d l%shd lrkafka kï
wkqjklfuls'</p>

<p>සරසවිවල යහපත් තත්ත්වයක් අඛණ්ඩව පවත්වා ගන්නට ශිෂ්ය පිරිස ද අවංක
උත්සාහයක් දීරීම අතිශය වැදගත්කමකින් යුක්ත ය. එසේ නොවන විට ඔවුන්ගේ ම අධ්යාපනය
කඩාකප්පල් වෙයි. මත්තෙහි සරසවි පිවිසෙන්නට අපේක්ෂා කරන තවත් ලක්ෂ ගණනකට බාධා සිදුවිය
හැකි බව ද මොහොතකට හෝ කල්පනා කළ යුතුව ඇත. එහෙම වුණොත් එයත් අපරාධයෙකි.
කවර විධියෙන් හෝ සරසවි තුළ යහපත් ම තත්ත්වයක් ජනිත කැරැ ගෙන එය සුරක්ෂිත කැරැ
ගැන්ම පාලක හා ශිෂ්ය දෙපිරිසේ ම යුතුකම යි.</p>

<p> </p>






</body></text></cesDoc>