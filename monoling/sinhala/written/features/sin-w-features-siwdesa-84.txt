<cesDoc id="sin-w-features-siwdesa-84" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-siwdesa-84.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p>විශ්වය පුරා
පවතින ක්ෂුද්ර තරංග පසුබිම් විකිරණය සොයාගැනීමට මහා පිපිරුම් වාදය සනාථ කිරීමට සාක්ෂ්යයක්
ලෙස යොදාගත්තද ස්ථාවර තත්ව න්යායවාදීන් කියා සිටින්නේ ක්ෂුද්ර තරංග පසුබිම් විකිරණය මහා
පිපුරුම් න්යායෙන් හැරුණුවිට වෙනත් ආකාරවල විලිනුත් සිදුවිය හැකි බවයි. උදාරණයක් ලෙස
තාරකා පුපුරායාම්වලදී (සුපර්නෝවා පිපිරීම්) නිපදවන යකඩ තන්තුවැනි දේවල්වලට
තාරකාවලින් විහිදෙන ආලෝකය උරාගෙන එන නැවත ක්ෂුද්ර තරංග විකිරණයක් ලෙස පිටකිරීමට
හැකියාව ඇත. </p>

<p> </p>

<p>තවද
ක්ෂුද්ර තරංග විකිරණයේ සුන්ථ සුන්ථ වෙනස්කම් ඇති බවද ස්ථාවර තත්ව න්යායවාදීහු පෙන්වා දෙති.
ක්ෂුද්ර තරංග විකිරණයේ එකී සුන්ථ සුන්ථ වෙනස්කම් බොහොමයක් උපකරණවල සංවේදීතාවයේ සීමාවල
ඇති ඒවා නිසා ඒවා පිළිබඳව ලබාගෙන ඇති තොරතුරුවල එතරම් විශ්වාසයක් නොමැති බව
ඔවුන්ගේ අදහසයි.</p>

<p> </p>

<p>úYajfha Wm; ms</p>

<p>මහා
පිපිරුම් පිළිතුරු දිය නොහැකි ප්රශ්න තුනක් තිබේ. විශ්වය මෙතරම් සමජාතීය හා
ස්වසාර්වදිශා (සෑමතැනකම එක හා සමාන හා හැම දෙසටම ඒකාකාරීව ප්රසාරණය) වූයේ මන්ද ?
විශ්වය තරමක් වයස් ගත වූවත් එහි ශක්ති සාමාන්ය ශක්ති ඝනත්වය මෙතරම් අධික වූයේ
ඇයි? ප්රතිපදාර්ථයට වඩා පදාර්ථ ප්රමාණයක් තිබෙන්නාක් මෙන් පෙනෙන්නේ මක්නිසාද? විශ්වයේ
දක්නට ලැබෙන මන්දාකිණිය සහ වෙනත් ව්යූහ නිර්මාණය කිරීමේ බීජ ලෙස අන්තිවේදී ක්රියාකරන
ඝනත්ව උච්චාවචන බිහිවූයේ  කෙසේද?
විශ්වයේ හැසිරීම් ගැන නව අදහස් වර්ධනය කර ගැනීමට හැත්තෑව දශකයේදී විශ්ව සම්භව
න්යායවාදීහු මූලික අංශු විද්යාවන්ගේ නව සොයා ගැනීම් ප්රයෝජනයට ගත්හ. ඒ අනුව ඔවුන්
පෙන්වා දෙන්නේ විශ්වයේ මුල් යුගවලදී තිබූ ඉතා ඉහළ ගණත්ව සහ ඉහළ උෂ්ණත්ව
යටතේදී සාමාන්ය භෞතික විද්යාත්මක නිමාවලට වඩා වෙනස් ආකාරයට ද්රව්යයන්ගේ හැසිරීම සිදුවන
බවයි. එවැනි තත්ත්ව යටතේදී විශ්වය හැසිරෙනු ඇත්තේ පරමාණුවල අභ්යන්තරයේ අංශු හැසිරෙන
ආකාරයටම බැව් විද්යාඥයෝ කියති. </p>

<p>ද්රව්යමය
ලෝකය අංශු වර්ග දෙකකින් සමන්විත වී ඇතැයි 1970දී පමණ සොයා ගැණින. පළමුව ඒවා
ඉලෙක්ට්රෝන වැනි අංශුව ලෙප්ටන්, සහ ඒවායේ නියුටෝනෝය. දීන් අපි ලෙප්ටන්
හයක් (ඉලෙක්ට්රෝන, මියොන් Muon) ටාවූ අංශු :ඔ්ම ච්රඑසජකැි-
සහ ඒවා හා බැඳුනු නියුට්්රනෝය. දීන්ගෙන ඇති අතර සැබෑ මූලික ඒවා ලෙස තවමත් සැලකේ.
දන්නා වූ අනෙක් අංශු ප්රෝටෝන්, නියුට්ට්රෝන සහ වෙනත් සියයක පමණ  හැඩ්රොන් යනුවෙන් හැඳින්වේ. ස්වභාවයේ  ප්රබල බල වශයෙන් හැඟෙන ඒවා නියම ප්රමාණයක් සහිත
සංකීර්ණ වස්තූන්ය. </p>

<p>මූලික
අංශු භෞතික විද්යාඥයෝ අන්තර් ක්රියාකාරීත්වය (Interactton) යන්නෙන් අදහස් කරන්නේ ප්රකීරණය :ීජ්එඑැරසබට- කක්ෂය
(Decay) අංශු උච්ඡෙදනය (Particle
Annihilation) අංශු ඇතිවීම :ඡ්රඑස්කැ ක්රු්එසදබ- වැනි මූලික අංශූන්ගෙන් සිදුවන ඕනෑම ක්රියාවලියෙකි. ස්වභාවයේ
සියලූ දක්නා අන්තර් කි්රයාවන් වර්ග හතරකට බෙදා දැක්විය හැකිය. එනම් පෘථිවිය සූර්යයා වටා
යවමින් තැබීම වැනි ක්රියාකරණ ගුරුත්ව බලය (Gravitation) ප්රෝටෝන තුළ ක්වාක් රඳවා තබන ප්රබල
ක්වාක් රඳවා තබන ප්රබල න්යාෂ්ටික බලය (Strong
Nutlear tnter actions)
පරමාණුක න්යෂ්ටිය වටා ඉලෙක්ට්රෝන යාමට සලස්වන විද්යුත් චුම්බක බලය :ෑකැජඑරදප්ටබැඑසිප- සහ න්යුටෝන ජීර්ණය හෙයත් බීටා ජීර්ණය යන නමින් දන්නා ක්රියාවට
හේතුවන දුර්වල න්යාෂ්ටික බලය (Weak nuclear
Interaction - වේ.</p>

<p>wxY= f,dalfha l=ula
isÿjkafka±hs úia;r lsrSug;a" .Kkh lrSu i|yd l%shdldÍ mokula f,i mdúÉÑhg
fhdojd.;a iïu; wdlD;shla wxY= fN!;sl úoHdfõ kQ;k f;dr;=re u.ska ksmojd we;'
f,magka jeks" lajdlaia j,ska ixhqla; ù we;ehs i.lk yâfrdaka we;a; jYfhka
Y+kH ;ru iys; jQ uQ,sl wxY+kah' yhj¾. f,magka we;sjdla fuka lajdlaia o yh
j¾.hla y÷kdf.k we;' .Kfol w;r fï ixl,kh iïu; wdlD;sfha j,x.= Ndjfha jeo.;a ikd:
lsrSula fia ie,lsh yelsh' lafjdkagï fN!;sl úoHdfõ ks.ukhka f.dvke.+ iólrK
iuqÞhhla úiska wdfrdams; wxY= w;/;s úoHq;a pqïNl l%shdjla b;d fyd¢ka úia;r fldg
;sfí' th lafjdkagï úoHq;a .;s úoHdj fyda qED hkqfjka ye¢kafõ' </p>

<p>lafjdkagï úoHq;a .;s
úoHdj iu. m%;siu;dj úiska j¾Okh l QCD hkqfjka jHjydr fõ' úoHq;a ÿ¾j, n,h f,iska tla iólrK fmk jQ úYajfhys ;ks taldnoaO n,hla jHdma;j meñK' tfy;a úYajh
m%%idrKh fj;au th isis,a ù .sh w;r iuñ;sl wx.kh kue;s l%shdj,sh fya;= fldgf.k
tla tla n,hla ;ukag rsis mrsÈ mqmqrd .sfhah' </p>

<p>විද්යුත්
චුම්බක හා දුර්වල අන්තර් ක්රියා ඒකී භවනය පළමුවෙන්ම වාදයක් සේ ඉදිරිපත් වූයේ 1960
ගණන්වලදීය එහෙත් පර්යේෂණාත්මකව ඔප්පු කරන ලද්දේ 1970 ගණන්වලදීය. ඒ ජිනීවාහි සර්න්
(න්යෂ්ටික පර්යේක්ෂණ සඳහා වූ යුරෝපීය සංවිධානය CERN නමැති ආයතනයේ යෝධත ත්වරකයෙන් කරන ලද
පර්යේෂණයන් ගෙනි. දුර්වල බලය නමින් හැඳින්වෙන අංශු සොයා ගැනීමෙන් පසුය. අතිශයින් කුඩා
වූ වස්තූන් පිළිබඳ ලෝකයෙහි රහස් අනාවරණය කිරීමට භෞතික විද්යාඥයන් විසින් යොදාගනු
ලබන ප්රධාන උපකරණය වන්නේ අංශු ත්වරකය හෙවත් 'පරමාණුක විකණ්ඩකය' 'පරමාණුක විකණ්ඩකය'
යනු පරමාණුක අංශු ඇදහිය නොහැකි වේගයෙන් ත්වරණය කරවා, වෙනත් අංශු සමග ගැටීමට
සලස්වා එයින් ඇතිවන පරමාණුක අවශේෂ පරීක්ෂා කිරීමට විද්යාඥයනට උපකාරීවන යෝධ යන්ත්රයන්ය.
සර්න් ආයතනයේ පරමාණු විකණ්ඩකය ලෝකයේ ප්රබලතම ත්වරණයකි. මෙහිදී ප්රෝටෝන සහ
ප්රතිප්රෝටෝන කදම්භ වොල්ට් මිලියන  පන්සිය
හතළිස් දහසක් දක්වා මුන්ථ ශක්තියක් ගැවීමට සැලැස්විය හැකිය. දුබල බලය ලෙන යන උ හා
න්අංශු මෑතකදී සොයාගත්තේ මෙහිදීය.</p>

<p>m%n, yd ÿ¾j, n, úia;r
lrk .Ks; iQ;% w;r iudk;ajh ksid fN!;sl úoHd{fhda fïjd uyd msmqrefuys wdÈ;u
wjia:djkays meje;s ;kstalS N+; n,hlska lsã fjkaù .sh foj¾.fhlehs iel l</p>

<p>මහා
ඒකාබද්ධ න්යායේ ඇතැම් ලක්ෂණ ආදී විශ්වයට බෙහෙවින් අදාළ වේ. මහා ඒකාබද්ධ න්යායෙන්
වර්ධනය කිරීමට කලින් ප්රෝටෝන ද, පරමාණුවද එමඟින් පදාර්ථයද නිරවශේෂයෙන්ම
ස්ථාවර යයි අදහස් කරන ලද නමුත් දුර්වල - අන්තර් ක්රියා න්යුටෝනයේ ජීරණයට හේතුවන්නාක්
මෙන් මහා ඒකාබද්ධ නා්යායේ අන්තර් ක්රියා ප්රෝටෝනයේ ජීර්ණය වීම අඟවයි. </p>

<p> </p>






</body></text></cesDoc>