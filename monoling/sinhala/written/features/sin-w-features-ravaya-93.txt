<cesDoc id="sin-w-features-ravaya-93" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-ravaya-93.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>ප්රභවවාදය</p>

<p> </p>

<p>මිචෙල් ෆූකෝගේ බොහෝ ශාස්තී්රය ගවේෂණවලට කායිකත්වය පිළිබඳ
මනෝගතය මුල්වී තිබේ. බලය කේන්ද්ර කොටගත් සංසිද්ධීන් ප්රකට වන්නේ මිනිස් කය තුළින්
සමාජයෙහි බලය හිමිකොට ගත් කොටස් සිය ආධිපත්යය තහවුරු කර ගන්නේ සෙස්සන්ගේ දේහයන්
දැඩි පාලනයකට ලක් කිරීමෙනි. මෙහිදී පාලනය යන්න එහි වාච්යාර්ථයෙන් මෙන්ම රූපකාර්ථයෙන් ද
සැලකුව මනාය. මිනිස් දේහයක්, බලයක්, ඉතිහාසයක් නිෂ්පාදනයක් අතර ඇත්තේ
සියුම් සම්බන්ධයකි. බටහිර සමාජවල අපරාධකරුවන් යැයි සම්මත පුද්ගලයන්ට දඬුවම් පමුණුවා
ඇති ආකාරය විවිධ යුගවලදී වෙනස්වූ අයුරු ඔහු නිරූපණය කරන්නේ මෙම සම්බන්ධය මුල් කොට
ගෙනය.</p>

<p> </p>

<p>ප්රභවවාදය නමැති සංකල්පයෙහි සංකීර්ණත්වය වටහා ගැනීමට නම් මිනිස් දේහය මත
බලය මූලික වන අයුරු විමසිය යුව් වේ. පුරාතන යුගවලදී ප්රභූ පක්ෂය සිය බලය ප්රදර්ශනය
කළේ සමාජයෙහි වෙසෙන සෙසු පුද්ගලයන්ගේ සිරුරු භෞතික වශයෙන් පාලනයට ලක්
කිරීමෙනි. එහෙත් වර්තමාන යුගයෙහි ෆූකෝ පවසන පරිදි, ප්රභූ පක්ෂය සිය බලය
විදහාපාන්නේ සමාජයෙහි ප්රසාරණය කරන විවිධ සංකථන මාර්ගයෙනි. උදාහරණයක් වශයෙන්
මනෝවිශ්ලේෂණය පිළිබඳ සංකථනය සමාජයෙහි ව්යාප්තවීමත් සමග මිනිස් සිරුර පාලනය කිරීමේ
බලය මනෝවිශ්ලේෂකයන්ට හිමිවූ අයුරු ඔහු පෙන්නුම් කරයි. වර්තමාන සමාජය ඔහු හඳුන්වන්නේ
ශික්ෂණාත්මක සමාජයක් ලෙසටය. මෙහි අර්ථය නම් වර්තමානිකයන්ගේ සිරුර මෙන්ම  දිවි
පෙවෙතද විවිධ ශික්ෂණ මගින් හසුරුවනු ලබන බවය. මෙම විෂය අරබයා ෆූකෝ මෙසේ ප්රකාශ කරයි.
මිනිස් සිරුර දේශපාලන ක්ෂේත්රය සමග සෘජුව සම්බන්ධව ඇත. බලය හා සම්බන්ධතා එක එල්ලේම
සිරුර කෙරෙහි බලපායි. එම සම්බන්ධතා මිනිස් සිරුර සලකුණු කරයි. පුහුණු කරයි.
වධහිංසාවට ලක් කරයි. විවිධ කාර්යයන් ඉටු කිරිමට බලකරයි. සංඥා නිකුත් කිරීමට
පොළඹවයි.</p>

<p> </p>

<p>මිචෙල් ෆූකෝ ඉතිහාසය දුටුවේ විවිධ ජන කොට්ඨාශ අතර ආධිපත්යය සඳහා කෙරෙන
අරගලයක් හැටියටය. එබැවින් බලය නමැති සංකල්පය ඔහුගේ විවරණවලට අතිශයින් වැදගත් වේ. ඔහු
සිය ඓතිහාසික ගවේෂණ පැවැත්වූයේ වර්තමානය මුල්කොටගෙනය. වර්තමාන සිදුවීමක් හෝ
අර්බුදයක් හෝ සමාජ පරිවර්තනයක් සිය ඓතිහාසික ගවේෂණයන්හි ආරම්භක ස්ථානය ලෙස සැලකීම
ෆූකෝගේ සාමාන්ය පරිචිතිය විය. එහෙත් සාම්ප්රදායික ඓතිහාසික විමර්ශකයන් මෙන් වර්තමාන
සිදුවීම් ඓතිහාසික බලවේගයන්හි තර්කානුකූල ඵලයක් ලෙස සැලකීම ඔහුගේ චින්තනයට පටහැනිය.</p>

<p> </p>

<p>මිචෙල් ෆූකෝගේ අරමුණ ඉතිහාසය නිෂ්පුද්ගල වූත් සදාකාලික වූත් සත්යයන්ට
අනුව විකාශනය නොවන බවත්, එය ආධිපත්යය සඳහා විවිධ ජන කොටස් අතර සිදුවන සටනෙහි
ප්රතිඵලයක් අනුව විකාශනය වන බවත්් පෙන්නුම් කිරීමටය. ඓතිහාසික සිදුවීම් සන්තතිවල ප්රභවය
සෙවීම ඉතා සංකීර්ණ කර්තව්යයක් බවත්. එය බලය, ඥාන නිෂ්පාදනය, අඛණ්ඩත්වය වැනි
සංකල්ප සමග දැඩි ලෙස බැඳී පවත්නා බවත් ෆූකෝ නිතර නිතර පෙන්වා දෙයි. ඉතිහාසය හා
සම්බන්ධ විවිධ යුගවල විවිධ සංකල්පවලට හා ආයතනවලට නව අර්ථ ආරෝපණය වන ආකාරය ඔහු සිය
ඓතිහාසික අධ්යයන වළින් පැහැදිලි කොට ඇත. මෙම අර්ථ බිහිවන්නේ වෛෂයික විද්යාත්මක
සිද්ධාන්තයන්ට අනුව නොව විවිධ ජන කොටස් අතර ඇතිවන බලය පිළිබඳ තරගයෙහි
ප්රතිඵලයක් වශයෙන් බව විදහා දීක්වීම ප්රභවවාදයෙහි එක් අභිමතාර්ථයකි.</p>

<p> </p>

<p>පසුගිය දශක දෙක තුළ ප්රභවවාදය ඉතා සඵල ලෙස ඓතිහාසික අධ්යයනයන්හි
මෙන්ම සමාජ ගවේෂණයන්හි ද උපයෝගී කොට ගෙන ඇත. මෙම සංකල්පය ඉතා සාර්ථක ලෙස
සාහිත්යය හා චිත්රපටි විශ්ලේෂණයන්හිදී යොදා ගෙන තිබේ. උදාහරණයක් වශයෙන් ලෙස්ටර් ජේම්ස්
පීරිස්ගේ ප්රමුඛත්වයෙන් මෙරට කලාත්මක චිත්රපටි සම්ප්රදායක් බිහි වූ ආකාරය ප්රභවවාදී
විග්රහයකට ලක් කළ හැකිය. දකුණු ඉන්දියානු චිත්රපටි කලාවෙන් හා කර්මාන්තයෙන් මිදෙන්නට
දීරූ තැත, දේශීීීීයත්වය පිළිබඳ සංකල්පයෙහි සංසරණය, ද්විභාෂික මධ්යම
පන්තියෙහි බලපෑම, ජාත්යන්තර අවධානය, සාහිත්යය හා නාට්ය ක්ෂේත්රයන්හි
ප්රබෝධය, ගැමි පසුබිමින් බිහිවූ බුද්ධිමතුන්ගේ කි්රයාමාර්ගය එබඳු ප්රභවවාදී විග්රහයක
වැදගත් තැනක් ගනු ඇත.</p>

<p> </p>






</body></text></cesDoc>