<cesDoc id="sin-w-features-silumina-269" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-silumina-269.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>ප්රස්තාවනා</p>

<p> </p>

<p>පොත් මිල සහ කියවීම</p>

<p> </p>

<p>දරුවන් සාහිත්ය පොත් කියවීමට යොමු කරන්නැ'යි අපි යන යන
තැන වැඩිහිටියන්ට කියමු. 'ඔය දරුවො හොඳ සාහිත්ය පොත් කියවන්ඩ පුරුදු වෙන්ඩ ඕනෑ' යයි
දරුවන්ට නිතර උපදෙස් දෙන්නෙමු. මෙවැනි අවස්ථාවල දෙමව්පියන් බොහෝ දෙනෙකු කියන්නේ
"පොත් කියවන එක නම් හොඳ තමයි ඒත් පොත් අරන් දෙන්නෙ කොහොමද, පොත් හරි ගණන්
නෙ..." කියා ය.</p>

<p>එය ඇත්තකි. පොත්වල මිල බෙහෙවින් ඉහළ ගොස් ඇති බව
ඇත්තකි. එසේ කියන දෙමව්පියන්ගෙන්් අපි පෙරළා මෙසේ ඇසුවොත්, හොඳයි අද මිල ඉහළ
ගිහින් තියෙන්නෙ පොත්වල විතරද? හාල්, පොල්, අල, පරිප්පු, සීනි, කිරිපිටි, මස්, මාන්ථ
මේවායේ මිල ඉහළ ගියා කියල ඔයගොල්ල නොකා ඉන්නව ද?? ඇයි පොත්වල මිල වැඩිකම ගැන විතරක්
කතා කරන්නෙ?</p>

<p>tzxf rl {« r~¨ »n|r£zpeõp Rpp£ »~
W{¥ë r±|p R¥~»vp »K [¥fû{ ý~¼à [l »p£¥Y. »r£l{z ñz S »[£~ R¥l»l n
R»pYªl u£jh ñz S p¥[ R¥Ü »N[x Rp¨{ v x. W»l »r£l »p£Ãx{£ ~Ñxl »p£Y£
»p£ð Sppf t¥ù t{ Rr nëv¨. A ë~£ Rr R»r ~£l³ »r£l r»l ñz Rh¨ Yyppf t¥ùn
xp Y£yjx ÑYY ýv~£ tzv¨.</p>

<p>අප රටට සාපේක්ෂව අපේ අසල් වැසි ඉන්දියාවේ පොත්වල මිල
බෙහෙවින් අඩුය. මිල පමණක් නොව පොත්වල බර ද අඩුය. අද අප රටේ පොත් මුද්රණ කර්මාන්තය
කැපී පෙනෙන දියුණුවක් අත්පත් කරගෙන සිටී.</p>

<p>අද සාහිත්ය පොත්වලින් බොහොමයක් ඉතා සෝබමාන ලෙස මුද්රණය
වෙයි. එහෙත් පිටු 300 යක නවකතා දෙකක් අතේ තබාගෙන ගියහොත් අත කැක්කුම හැදෙන තරමට ඒ
පොත් බරින්් වැඩිය. මීට හේතුව ඒ පොත් මුද්රණය කිරීම සඳහා පාවිච්චි කරන කඩදාසි වර්ගය මිලෙන්්
වාගේ ම බරෙන් ද වැඩි වීම ය.</p>

<p>ඉන්දියාවේ සාහිත්ය පොත පත මුද්රණය කිරීම සඳහා පාවිච්චි කරන
කඩදාසි බෙහෙවින් සැහැල්ලුය. යුරෝපා රටවල ද එසේ ය. පොත් ප්රකාශනය පිළිගත් කර්මාන්තයක්
බවට පත් කොට ප්රකාශකයන්ට පොත් මුද්රණය සඳහා සුදුසු සැහැල්ලු මෙන්ම මිලෙන් ද අඩු කඩදාසි
ගෙන්වා දෙන්නැයි ලේඛකයන් සේ ම ප්රකාශකයන් ද, ආණ්ඩු ගණනාවකට ඉල්ලීම් ඉදිරිපත් කළ
නමුත් අද දක්වා එය ඉටු වී නැත. අඩුම වශයෙන් දැනට පාවිච්චි කරන කඩදාසි වර්ගය හෝ සහන මිලකට
සපයා දීමට බලධාරීන් ගේ අවධානය යොමු වන්නේ් නම් බරින් වැඩි වුවත් මිලෙන් අඩු සාහිත්ය
පොත් සාප්පුවලින් මිල දී ගත හැකි වනු ඇත.</p>

<p>මෙසේ කාරණයේ එක් පැත්තක් පෙන්වා දුන් විට තම දරුවන්ට
පොත් මිල දී නො ගන්නා දෙමාපියන්ට සිත් සැනසිල්ලක් ලැබෙනවාට සැකයක් නැත. නමුත් ඒ
එක පැත්තකි. අනෙක් පැත්ත නම් දරුවන් පුස්තකාල පරිහරණය සඳහා යොමුකරවීමක් බොහෝ
දෙමාපියන් අතින් සිදු නොවීමයි. තම පෞද්ගලික වියදමින් දරුවන්ට පොත් අරන් දෙන්නට නොහැකි
වුව ද දරුවන් ළඟපාත ඇති පුස්තකාලයට හුරු කරවන්නේ් නම් ඒ මගින් ලොකු වියදමක් නොකර
දරුවා සාහිත්යයට යොමු කළ හැකිවෙයි. පුස්තකාලවලට හොඳ පොත් සැපයීමේ වැඩ පිළිවෙළක් හැකි
කමේ ගැටලුව පසුගිය සතියක කතා කළ නිසා, මෙතැනට අදාල වුව ද එය නැවත කතා කරන්නේ නැත.</p>

<p>පොත් කියවන්නේ නැත්තේ ඇයි ද යන ප්රශ්නය පාසල් දරුවන්
පිරිසකගෙන් ඇසූූ අවස්ථාවක එක් යොවුන් සිසුවෙක් මෙසේ කීවේය.</p>

<p>"සිංහල නවකතා විසිපහක් තිස්පහක් කියවේවත් ඒ හැම එකකම
තියෙන්නෙ එක වගේ ම කතන්දර නෙ..."</p>

<p>මේ යොවුන් පාඨකයාගේ ප්රකාශයෙන් මම ද ලජ්ජාවට පත්වීමි.
ලේඛකයන් වන අප මේ කාරණය ගැන බරපතල ලෙස කල්පනා කළ යුතුව ඇත. අපේ නවකතා බොහොමයක්
එක ම අච්චුවක නිර්මාණය වූ ඒවා ලෙස පෙනෙන බව අසත්යයක් නොවේ. මේ නිසාම අද බොහෝ පාඨකයෝ
විදේශ සාහිත්ය කෘතිවල පරිවර්තන කියවීමට වැඩි කැමැත්තක් දක්වති. මෙයින් පෙනී යන්නේ පොත්
කියවීමේ රුචිය යොවුන් හා තරුණ පරපුර තුළ වර්ධනය කිරීමට ලේඛකයන් ගෙන් ද ඉටු විය යුතු
කාර්යභාරයක් ඇති බවයි.</p>

<p> </p>

<p>ජයතිලක කම්මැල්ලවීර </p>






</body></text></cesDoc>