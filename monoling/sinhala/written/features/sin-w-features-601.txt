<cesDoc id="sin-w-features-601" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-601.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>කුණු වී සැරව ගලන තුවාලයක් බවට පත්ව ඇති
දේශපාලනය</p>

<p>ශ්රී ලංකාවේ නූතන පාර්ලිමේන්තු දේශපාලනය
මජර ගලන කුණු වූ තුවාලයක් ලෙස සැලකිය හැකි නම් ඒ තුළ සිටින සියලූ දේශපාලන පක්ෂ
(පොදු පෙරමුණ, එක්සත් ජාතික පක්ෂය හා ජනතා විමුක්ති පෙරමුණ ඇතුන්ථ) එම කුණු
වූ තුවාලයෙන් ගලා යන මජර බී ජීවත් වන ඉහඳ පණුවන් ලෙස සැලකිය හැකි ය.</p>

<p>පොදු පෙරමුණේ පිරිසක් විපක්ෂයට එකතු
වීම, ඔවුන්ට එරෙහිව ආණ්ඩු පක්ෂය නගමින් තිබෙන චෝදනා නැවත ඔවුන් ආණ්ඩු පක්ෂයට
එල්ල කරමින් තිබෙන චෝදනා, ඒ ගැන එක්සත් ජාතික පක්ෂය හා ජනතා විමුක්ති පෙරමුණ
කරමින් තිබෙන විචාර රටේ දේශපාලනය කුණු වී තිබෙන තරම මනා ලෙස පෙන්නුම් කරයි. </p>

<p>කලක් එස්.බී. එජාපයේ කෝපයට හේතු වී සිටි
ලොකුම දුෂ්ඨයා වූයේ ය. එසේ ම ඔහු ජනාධිපතිනියටත් පොදු පෙරමුණු ආණ්ඩුවටත් සිටි
හොඳම වීරයා වූයේ ය. දීන් එක්සත් ජාතික පක්ෂයට දුෂ්ඨයා වීරයා වී සිටින විට ආණ්ඩුවට වීරයා
දුෂ්ඨයෙකු වී සිටින්නේ ය.</p>

<p>කලක් ජනතා විමුක්ති පෙරමුණට විජය කුමාරතුංග
පමණක් නොව ඔහුගේ බිරිඳ චන්ද්රිකා කුමාරතුංග පෙනුණේ ද ජීවත් නොකළයුතු තරමේ
ද්රෝහීන් ලෙස ය. චන්ද්රිකා කුමාරතුංග ජනතා විමුක්ති පෙරමුණ දීක්කේ ද අනුකම්පා විරහිතව
විනාශ කළ යුතු ව්යාපාරයක් වශයෙනි. එහෙත් අද ඒ දෙපිරිසම කිරිපැණි තරමට යාන්ථ ය. </p>

<p>ජනතා විමුක්ති පෙරමුණට පොදු පෙරමුණ අත්හැර
ගිය සියලූ දෙනා පෙනෙන්නේ දොෂිත තක්කඩින් ලෙස ය. එහෙත් ආණ්ඩුව තුළ සිටින දොෂිත
තක්කඩින් පිළිබඳව ඔවුන් මුන්ථමණින් නිහඬ ය. මැතිවරණ දොෂණවලදී එස්.බී. රඟපෑ භූමිකාව
සඳහා ඔවුන් පුන පුනා කථා කරතත්, අනුරුද්ධ රත්වත්ත, මහීපා හේරත් වැනි අය
ගැන මුන්ථමණින්ම නිහඬය. බැංකු පොලූකාරයින් ගැන කථා කරන විට ඔවුන් කථා කරන්නේ ද
යශෝධා ගැන පමණය. ජනාධිපතිනියගේ විශ්වාසවන්තම මිතුරා ලෙස සැලකිය හැකි රොනී පීරිස්
ගැන ඔවුන් නිහඬය.</p>

<p>පොදු පෙරමුණෙන් මන්ත්රීවරුන් 9 දෙනෙකු
විපක්ෂයට ගිය අවස්ථාවේ දී මංගල සමරවීර මහතා නිවේදනයක් නිකුත් කරමින් කියා සිටින්නේ
''පොදුජන වරමින් පාර්ලිමේන්තුවට පැමිණ මෙරට අඩු ආදායම් ලාභී ජනතාවගේ ආර්ථික සමෘද්ධිය
සඳහා වෙන් කළ මුදල් ගසා කමින් සුවිශාල මාළිගා ඉදිකර ඇති පුද්ගලයින් ද රටේ
ස්වාභාවික සම්පත් විනාශ කරමින් ස්වාභාවික වනාන්තර දීව ජාවාරම්කාරයන්ට අලෙවි කරමින් කෝටි
ප්රකෝටි ගණනින් ඉපැයූ මුදලින් කොළඹ හතේ පමණක් නොව විදේශයන්හි ද
සුඛෝපභෝගී නිවාස මිලදී ගෙන ඇති පුද්ගලයින් ද ඇඟලූම් කම්හල්වලට ලබාදිය යුතු ඇඟලූම්
කෝටා කන්ථ කඩයට විකුණමින් අයුතු ලාභ ලැබූ පුද්ගලයින් ද ජාවාරම්කාරයින්ගේ පාර්ලිමේන්තු
නියෝජිතයන් බවට පත්ව ඇති බව ය.''</p>

<p>ඒ මගින් මංගල සමරවීර ඇමතිවරයා කියන්නේ
පොදු පෙරමුණෙන් ඉවත්ව ගිය හිටපු ඇමතිවරුන් තිදෙනාම හොර තක්කඩින් බව ය. </p>

<p>තවත් දිනෙක රූපවාහිනී සාකච්ඡාවක දී මංගල
සමරවීර ඇමතිවරයා කිව්වේ අහසින් ගමන් කරන්නෙකුට උඩරට කඳුකරයේ මාළිගා දෙකක් දීකිය
හැකි බවත් ඉන් එකක් නුවර දළදා මාළිගාව බවත් අනෙක් මාළිගාව ඇත්තේ
හඟුරන්කෙත බවත් ය. එස්.බී. දිසානායක මහතා විවාදයට හේතු වී තිබෙන සිය නිවෙස ගැන කීවේ
තමන් හඟුරන්කෙත නිවසක් ඉදිකළ බව සත්යයක් බවත් එහෙත් තමාට නයිල් ගඟ ඉවුරේ
සුඛෝපභෝගී මාළිගා නැති බවත් කාමර 4කින් යුතු සිය නිවස ඉදිකිරීමට තමාට ලක්ෂ
150ක් වියදම් වූ බවත් ය. ඪ</p>

<p>ඉන් පසු මංගල සමරවීර කීවේ ලක්ෂ 150ක් වැය වී
ඇතැයි කියන එස්.බී. දිසානායක මහතාගේ හඟුරන්කෙත නිවස ඉදිකිරීමට අඩුම වශයෙන් රුපියල්
ලක්ෂ 600කට වඩා වැයවන බවත් රුපියල් 17,000ක වැටුපක් ලබන ඇමතිවරයකු ගෙයක්
සාදන්න රුපියල් ලක්ෂ 600ක් වැය කළේ කෙසේද යන්න ප්රශ්නයක් බවත් ය. </p>

<p>එස්.බී. දිසානායකගේ මාළිගාව ගැන
චෝදනා කරන ජනාධිපතිනිය ඇතුන්ථ ආණ්ඩු පක්ෂයේ සියලූ දෙනාට එස්.බී., ආණ්ඩුව
අත්හැර යන තුරු ඒ මාළිගාව ප්රශ්නයක් වූයේ නැත. 2000 ජනාධිපතිවරණය පැවති කාලයේ
දී ජනාධිපතිනිය ද ඒ මාළිගාවට ගියා ය. මංගල සමරවීර ඇතුන්ථ බොහෝ ඇමතිවරුන් ද
අවස්ථා ගණනාවක එහි ගොස් කා බී ප්රීති විය. එහෙත් ඒ කිසිවෙකුට ඒ කාලයේ එය ප්රශ්නයක්
නොවී ය. </p>

<p>තමන් සමග සිටින තෙක් දොෂණයට ඉඩ දෙන හා
වෙන්වීමක් ඇතිවුවහොත් පමණක් ඒ දොෂණ පසුපස හඹා යන ධර්මතාවක් රටේ කි්රයාත්මක වන බව
ඉන් පෙනී යයි. </p>

<p>වෙන්ව ගිය ඇමතිවරුන් ජනාධිතිනියට එරෙහිව
ඉදිරිපත් කරන චෝදනා ද ඉතාමත් බරපතල ය. ඇගේ ප්රමාදය ගැන එස්.බී. කරන විග්රහය මෙසේ ය.</p>

<p>''චන්ද්රිකා කුමාරතුංග ජනාධිපතිනිය වෙලාවට
වැඩ කිරීමට නොදනී. කාලය කළමනාකරණය කිරීමට නොදනී. ප්රමාදය විනාඩි 10ක් 15ක් නොව
පැය 3ක් හෝ 4කි. මහරැජිනටත්, ඇමරිකාවේ ජනාධිපතිටත් ඉන්දියාවේ අගමැතිටත් ප්රමාද වී
ගොස් ඇය ලෝක වාර්තා තබා ඇත. එහෙත් ඇය ප්රමාද වන බව නොපිළිගනී. </p>

<p>ජනාධිපතිනියගේ අමාත්යාංශ ප්රගති සමාලෝචන
රැස්වීම් හැමදාම කල් තබන කාලය මරන විකාරයකි. උදේ 10ට වෙලාව දුන්නොත් රැස්වීම්වලට එන්නේ
සවස 4.00ට ය. සවස 4.00ට වෙලාව දුන්නොත් එන්නේ රාත්රී 10.00ට ය. නිලධාරීන් ටයි කෝට්
දාගෙන රටේ නායිකාව හමුවීමට ගොස් පැය 5-6ක් ඇය ප්රමාද වන විට සාකච්ඡා පවත්වන
මානසිකත්වයක නිලධාරින් නැත. සාකච්ඡා සිදුවන්නේ ද නැත. ජනාධිපතිනිය සුපුරුදු ලෙස ඇගේ
තීක්ෂණ බුද්ධිය හා කාර්යාක්ෂමතාව ගැන සම්පූර්ණයෙන් අසත්ය වූ ආත්ම වර්ණනා දේශනයක් පවත්වා
සභාව අවසන් කරයි.''</p>

<p>ජනාධිපතිනියගේ ප්රමාදය මුන්ථ රටටම රහසක්
නොවූවත් ඇමති මංගල සමරවීර කියන්නේ මෙතෙක් රටේ පහළ වූ සියලූ නායකයින් අතරින්
වඩාත්ම කාර්යක්ෂම නායිකාව ඇය බව ය. සුන්ථ සුන්ථ ප්රමාද තිබිය හැකි වුවත් රටේ අභිවෘද්ධිය
පිණිස විශාල වෙහෙස මහන්සියක් දරන බව ය. </p>

<p>රටේ ජනාධිපතිනිය සියලූ වැදගත් ආයතන තමන්
අත තබාගෙන සිටින්නේය යන්න ඇයට එරෙහිව ඔවුන් ඉදිරිපත් කර ඇති තවත් චෝදනාවකි. </p>

<p>ඒ ගැන එස්.බී. කරන විග්රහය මෙසේ ය.</p>

<p>''ජනාධිපතිනිය අමාත්යාංශ රැසක් තමන් අත
තබාගෙන සිටී. රටේ වැදගත්ම අමාරුම ලොකුම අමාත්යාංශය වූ මුදල් ක්රමසම්පාදන හා ආරක්ෂක
අමාත්යාංශය ඇය යටතේ ය. නාගරික සංවර්ධන, නිවාස, මහජන උපයෝගිතා
සමෘද්ධි, ක්රීඩා හා ජනමාධ්ය අමාත්යාංශය ඇය යටතේ ය. </p>

<p>මීට අමතරව ඇමතිවරුන්ට කැපී පෙනෙන වැඩක්
කළ හැකි සෙවණ අරමුදල, සේවා නියුක්තිකයන්ගේ භාරකාර අරමුදල,
සංස්කෘතික අරමුදල වැනි ඒවා ද ඇය යටතේ ය. රාජ්ය ආයතන 80ක් පමණ ඇය යටතේ පාලනය
වන අතර හිල්ටන් හෝටලයත්, සත්තු වත්තත්, ඇය යටතට ගෙන ඇත. මීටත් අමතරව
විශාල ව්යාපෘති, ටෙන්ඩර්, විදේශ ආයෝජන, පෞද්ගලීකරණ කටයුතු,
රාජ්ය දේපළ පැවරීම් ආදිය මොන අමාත්යාංශයට අයත් වුවත් ඇගේ තනි පාලනය යටතේ සිදු
වේ. මෙම ආයතනවල කටයුතු සහමුලින්ම ඇණහිට ඇත. ලිපිගොනු දහස් ගණනක් මාස ගණන්
පෝලිමේ තිබේ. මාස හය හතක් ප්රමාද වී අතුරුදහන් වන ලිපිගොනු අනන්තය. අප්රමාණ ය.
රාජ්ය නායිකාවට අවශ්ය චිත්ත විවේකය වෙනුවට සන්සුන් බව වෙනුවට ජනාධිපති මනස අවුල්
ජාලයකි. එය යකාගේ කම්මලටත් අන්ත ය.''</p>

<p>සියලූ වැදගත් විශාල ටෙන්ඩර් හා පෞද්ගලීකරණ
කි්රයාවලිය ජනාධිපතිනිය තමා වටා සිටින හිතමිතුරන් කිහිප දෙනෙකු සමග මෙහෙයවමින් සිටින බවට
එල්ල වී තිබෙන චෝදනා, ජනාධිපතිනියට එරෙහිව එල්ල වී තිබෙන චෝදනා අතර තිබෙන
බරපතලම චෝදනාව ලෙස සැලකිය හැකි ය. එස්.බී. දිසානායක මහතා ප්රකාශකර තිබෙන ආකාරයට
ජනාධිපතිනියගේ කැබිනට් සංදේශ ඇමති මණ්ඩලය තුළ සාකච්ඡාවට ලක් කෙරෙන්නේ නැත.
සිරිතක් වශයෙන් කැබිනට් සාකච්ඡා අවසාන වන අවස්ථාවේ දී ජනාධිපතිනියගේ කැබිනට් සංදේශයක්
හෝ දෙකක් ඇති බව අගමැතිවරයා ප්රකාශ කරන අතර ඒවා සාකච්ඡාවට ලක් කිරීමකින් තොරව
ඒවා අනුමත කරන ධර්මතාවක් කැබිනට් මණ්ඩලය තුළ කි්රයාත්මක වේ. </p>

<p>ඇමරිකවේ ඉවාන්ස් ඉන්ටර්නැෂනල් සමාගමට
නීත්යනුකූලව ලැබිය යුතුව තිබුණු ටෙන්ඩරයක් අවසාන මොහොතේ වෙනත් සමාගමකට ලබාදීම
හේතුකොටගෙන ඉවාන්ස් ඉන්ටර්නැෂනල් සමාගමට රුපියල් කෝටි 25ක වන්දියක් ගෙවීමට
ලංකාණ්ඩුවට සිදුවිය. ලොකෝමෝටිව් ටෙන්ඩරයත් ජනාධිපතිනිය මැදිහත් වී අවසාන මොහොතේ
ප්රංශ සමාගමකට ලබාදීම ආශ්රයෙන් මතු වූ අර්බුදයක් නිසා මුදල් අමාත්යාංශයේ බී.සී. පෙරේරා
මහතාට සිය තනතුර ද අත්හැර යාමට සිදුවිය. කොළඹ කටුනායක අධිවේගී මාර්ගයට අදාළව
ටෙන්ඩර් කි්රයාවලිය ද විවාදයට හේතු වී තිබුණි. කොළඹ කටුනායක අධිවේගී මාර්ග ටෙන්ඩරය
පිටුපස සිටින ප්රධාන පුද්ගලයා බවට චෝදනා එල්ල වී තිබුණේ රොනී පීරිස්ට ය. දීන් එය
හොඳින් තහවුරු වූ සත්යයක් බවට පත් වී තිබේ. </p>

<p>frdkS mSßia ckdêm;skshf.a
úYajdijka;u ñ;=rd h' úch l=udr;=x. >d;kfhka miqj ckdêm;sksh wdrCIdj i|yd
úfoaY.; ù isá ld,fha wehg wdOdr l</p>

<p>ලංකා බැංකුවේ ලන්ඩන් ශාඛාව මේ පුද්ගලයාට
ස්ටර්ලින් පවුම් 650,000ක (රුපියල් ලක්ෂ 845) ණයක් ලබා දී තිබෙන අතර එම ණය
ආපසු නොගෙවා සිටින තත්ත්වයක් තිබියදී බැංකුවට අයවිය යුතු ලක්ෂ 150ක පොලියක් කපා
හැරෙන ආකාරයට එම ණය මුදල ප්රතිලේඛන ගත කරවා ගැනීමට ද ඔහු සමත් වී සිටී. </p>

<p>හිරු ගුවන් විදුලි සේවය පවත්වාගෙ යන ඒබීසී
පුද්ගලික සමාගමේ ප්රධාන හිමිකරුවා ද රොනී පීරිස් ය. එම සමාගම සතු මුන්ථ කොටස්
8562673කින් කොටස් 3425068ක් රොනී පීරිස්ට අයත් බ්ලු ක්ලාස් ඉන්ටර්නැෂනල් සමාගම සතු
ය. එම කොටස් අයිතියෙන් පමණක් 1999 වසර සඳහා හිමිකරගෙන තිබෙන ලාභය රුපියල්
15789568කි. </p>

<p>ජනාධිපතිනියට එරෙහිව කැරලිකාර කණ්ඩායමක්
ඉදිරිපත් කරන චෝදනා ඉතාමත් බරපතල වුවත් ඒ සියලූ බරපතල වැරදි ඉදිරියේ අවුරුදු 7ක්
නිහඬව බලා සිටියේ ඇයිද යන්න ප්රශ්නයක් පවතී. ජනාධිපතිනිය තමාට අවශ්ය මහා පරිමාණයේ
ටෙන්ඩර් කැබිනට් සාකච්ඡාවකට ඉඩ නොතබා කැබිනට් මණ්ඩලය ලවා අනුමත කරවා ගැනීමට සමත් වී නම්
හා ඒ හැම අවස්ථාවකදී ම බරපතල වරදක් සිදුවන බව දීන දීනම කැබිනට් මණ්ඩල සාමාජිකයන් නිහඬව
ඊට ඉඩ හරින ප්රතිපත්තියක් අනුගමනය කළේ නම් එවැනි ඇමති මණ්ඩලයක් සැලකිය හැක්කේ
නපුංසක ඇමති මණ්ඩලයක් වශයෙන් පමණ ය. </p>

<p>වික්ටර් අයිවන් </p>

<p> </p>






</body></text></cesDoc>