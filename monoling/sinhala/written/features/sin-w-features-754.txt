<cesDoc id="sin-w-features-754" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-754.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ඩබ්ලියු.
ඒ. අබේසිංහ</p>

<p>දිට්ඨමංගලිකලා,
ශිෂ්ට මංගලිකලා සහ දුෂ්ටමංගලිකලා</p>

<p>අප
මහ බෝසත් මාතාංග පණ්ඩිතයින් දිට්ඨමංගලිකාවගේ ජාතිමදමානය නමැති අත්තටු කැපුවේ
දෙපැත්ත කැපෙන කඩුවකිනි. මාතංග පණ්ඩිතයින්, ඒ කඩුවේ අනිත් පැත්තේ මුවහත තබා
ගත්තේ, බමුණන්ගේ තටු කපන්නටය.</p>

<p>බුදු
හාමුදුරුවන්ගේ දේශනයෙහි වඩාත්ම ඉදිරිගාමී, විප්ලවකාරී, මානව හිතවාදී ස්වරය
මෙන්ම ස්ථාවරය රැඳී ඇත්තේ, උන්වහන්සේ බමුණන් පිළිබඳ දීක් වූ ආකල්පය හා
ඔවුන් ගැන කළ අර්ථකථනයෙහිය. බමුණන් පිළිබඳ බුදුහාමුදුරුවන්ගේ මතවාදය ලොවේ
කිසිම ආගමක, ධර්මයක හෝ දර්ශනවාදයක, ඒ තරම් තියුණු අයුරින් නොපැනෙයි.</p>

<p>ධම්මපදයේ
බ්රාහ්මණ වග්ගයෙන් ඇද ගත් මෙම වැකිකඩ කිහිපය දෙස බැලූව මැනවි.</p>

<p>න
ජටාහි න ගොත්තේන</p>

<p>න
ජච්ච හෝති බ්රාහ්මණේ</p>

<p>ජටා
මඩුලූයෙන් හෝ නම් ගොත්වලහි නැති නම්, ජාතියෙන් බමුණකු නොවන්නේය. න චාහං
බ්රාහ්මණං බ්රෑම් යෝනිජං මත්ති සම්භවං</p>

<p>උපන්නේ
මුත් බමුණු කුලයෙක</p>

<p>උපන්නේ
මුත් බමුණු මව්කුස</p>

<p>ඔහුට
නොකියමි බමුණෙකැයි මම</p>

<p>කෙනෙකු
උප්පත්තියෙන්ම බමුණකු නොවන වග බුදුහාමුදුරුවන්ගේ ස්ථාවර මතය හෙවත් මතවාදය විය. මෙය
වඩාත් පළ වන්නේ වාසටේඨ සහ භාරද්වාජ යන මිත්ර මාණවකයින් දෙදෙනාට උන්වහන්සේ කළ
විවරණයෙනි. ඒ අරුතින් ගත්් කල වාසටේඨ සූත්රය, ලොවේ බමුණුවාදය මතු නොවෙ සියලූ
මාදිලියේ ජාතිවාදයන්ද, වර්ගවාදයන්ද උඩු යටිකුරු කරන අග්රේෂ්වර සූත්ර ධර්මයකි; ප්රඥා
ප්රභාවෙන් බබලන චින්තාවකි.</p>

<p>මෙම
තරුණයින් දෙදෙනාට දීන ගන්ට අවශ්ය වූයේ, බමුණකු වන්නේ කෙසේද යන කාරණයයි.</p>

<p> tys§ nqÿyduqÿrejka ;¾l lrk ú,dih b;d ukyrh'
mqÿu t</p>

<p>බුදුහාමුදුරුවන්
වදාරන සේක්, තණකොළවලට, බස්වලට ජාතිමය ලිංග ඇති බවය. ඒ
කියන්නේ, එක් තෘණ වර්ගයක් තවත් තෘණ වර්ගයකින් වෙනස් බවය. එක වෘක්ෂ වර්ගයක් තවත්
වෘක්ෂ වර්ගයකින් වෙනස් බවය. එතැන ජාතිමය ලිංගයක් හෙවත් ජාති භේදයක්  ඇත.</p>

<p>එපරිදිම,
කුරාකුහුඹුවෝ පණුවෝ පළගැටියෝ නානාප්රකාරයහ. සිවුපා සතුන් ද එසේමය. ඔවුන්ට
ජාතිභේද ඇත. උරගුන්ට, මසුන්ට මෙන්ම පියාපත් ඇති ලිහිණි ආදී පක්ෂීන්ටද
ජාතිමය ලිංග හෙවත් ජාතිභේදය ඇත.</p>

<p>බුදුන්ගේ
තර්ක න්යාය අනුව, ඒ හැම තැනකම ජාතිභේදය ඇති බැව් වටහා ගැනීම අසීරු නැත. දීන්
මෙහෙම  ගනිමු.</p>

<p>තුත්තිරි,
බැල තණවලින් වෙනස්ය. බැල තණ කුසතණවලින් වෙනස්ය.</p>

<p>කොස්ගහත්,
පොල් ගහත් සමාන නැත. කජු ගසත් අඹ ගසත් සමාන නැත. කොළ වලින්ද,
ඵලවලින්ද, දීවයෙන්ද මේවා එකිනෙකට වෙනස් බැවිනි. කූඹියා, කුරුමිණියාට වෙනස්ය.
කුරුමිණියා පළ ගැටියාට වෙනස්ය. කඩියා, දිමියාට වෙනස්ය.</p>

<p>නයාට
පෙණය ඇති මුත් පොළඟාට පෙණ ගොබයක් නැත. උරගුන්ට විස දළ ඇතත් ගැරඬියාට එහෙම
නැත. තමාට පෙණයක් ඇතැයි පෙන්වන්ට අවශ්ය නම් ඌ කැට කැබිලිත්තක් ගෙන එසවිය යුතුමය. (ජන
සමාජයේ ඉන්න ගැරඬින් කරන්නේත් බොරුවට කැට කැබිලිති එසවීම පමණි. උන්ට පුප්පන්ට
පෙණයක් නැති බැවිනි). පිඹුරා මේ සියලූම උරගුන්ට වඩා විශාල නමුත් ඌටද පෙණ ගොබයක්
නැත. මෝල් ගස් ගිලපු පිඹුුරන් රාජ්ය තන්ත්රය අරා සිටින තකතීරු අහංකාර නිලධාරීන්ට
සමකළ හැකි වුවත්, ඒ පිඹුුරන්ට ද පුප්පන්ට පෙණයක් නැත.</p>

<p>උඩ
ඉගිලෙන ලිහිණි කොබෙයියෝද එකිනෙකාට සමාන නොවෙති. රාජාලියාත් කපුටාත් සමාන
කරන්නේ කෙසේද? මොනරාත් ඇටිකුකුළාත් කෙසේ නම් සමාන කර දක්වන්ටද?</p>

<p>සිව්පා
සතුන්ගේ ජාති භේදය, ඒ සැමටම වඩා බලවත්ය; ව්යාප්තය කකුල් හතර හේතු කොටගෙන
සිවුපා වුවද, හැම සිවුපාවුන්ටම වලිගය නැත. තියුණු අං තට්ටු ඇති මුවන්ට,
ගෝනුන්ට මෙන්ම, බෝසත්ව සිට ඇඟ මස් දන් දුන්්නු හාවාටද, වලිගයක් නැත.
සෙසු බොහෝ සතුන්ට මෙන්ම හරකාටද වලිගයක් ඇත. නුවණ නැතත් ගොනා, තම ජාමේ
බේරා ගන්නේ අං දෙක නිසාය. නුවණ නැතිව, සමාජයේ ඉහළට ගිය මෝඩයින්ටද පසුව
අං එයි. ඔවුන්ගේ තකතීරුකම් වැහෙන්නේ පසුව එන අං තට්ටුව නිසාය. දේශපාලකයින්ට සහ
ඔවුන්ගේ කැචර්ලාට ලැබෙන මේ අං තට්ටු ගැලවී වැටෙන්නේ මැතිවරණයක නිමාවත් සමඟය. ඊට
පසුව, අලූත් පිරිසක් සබයට ඇවිත් ඒ අං තට්ටුවලට අයිතිවාසිකම් කියති. කොහොමටත්
ගොනෙකුට අං නැතත් වලිගයක් නම් නැතිවම බැරිය. වලිගය, මැස්සන්  පන්නන්ටය.</p>

<p>වලිගය
ඇති ගොනා මැස්සන්ට බැට දුනි</p>

<p>දුප්පත්කමත්
වලිගය නැති ගොනා වැනි</p>

<p>යයි
අපේ ජනකවියා කීවේ, ඒ නිසාය. තට්ටුමාරු ක්රමයට රටත් ගමත් දෙකම වැළඳූ අපේ
දේශපාලන පක්ෂ විසින් මුන්ථ මහත් ජාතියම වලිගය නැතිි ගොන් රැළක් බවට පත්කොට ඇත.
අවුරුද්දේ ණය වාරිකත්, පොළියත්් ගෙවන්ට මුන්ථ ජාතික ආදායමත් මදිය. තවත්
අමතර කෝටි පන්දහසක් අවශ්යය. රටක් වශයෙන් අප කොතරම් දුගී දුප්පත් වෙලාද
කිවහොත්, අපට පිටේ වහන මැස්සෙකුටත් පන්නා ගන්ට බැරිය. අපේ ජනකවියා කියා
ඇත්තේ කටේ මසුරන් දමන්ට වටින කතාවක් වුණත්්, ඒ සඳහා මසුරන් හොයා ගන්නේ
කොහෙන්දීයි මා නම් දන්නේ නැත.</p>

<p>කොහොම
නමුත් අපේ බුදු හාමුදුරුවන්ගේ විග්රහය අනුව, හැම සිව්පාවෝ්ම එක සමාන නොවෙති.
සිංහයාත් කොටියාත් දෙදෙනාම නපුරුය. භයානකය. එහෙත් සමාන නැත. සිංහයාට ඇත්තේ
කේසරය. කොටියාට ඇත්තේ පුුල්ලිය. කැලේ මාරු කළාට, කොටියා පුල්ලි මාරු
කරන්නේ නැත. මේ මොහොතේ සිංහ කොටි ගැන කතා කිරීම ජාති භේදවාදය ඇවිස්සීමක් විය
හැකි නිසා, වගවලසුන් ගැන කතාකිරීම යෙහෙකැයි සිතේ. කෙසේ නමුත්, සිව්පා
සතුන් අතර ජාතිමය ලිංගය හෙවත් ජාතිභේදය ඇත.</p>

<p>බුදු
බණ එහෙමය. </p>

<p>බුදු
බණ අනුව, මෙකී ජාතිමය ලිංගය හෙවත් ජාතිභේදය නැත්තේ, කකුල් දෙකේ සතුන්
වන මානවයින්ට හෙවත් (මානව විද්යාඥයින්ගේ යෙදුමකින් කියනවා නම්)
හෝමෝසේපියන්ස්ලාට (Homosapiens )
පමණි. </p>

<p>යථා
ඒතාසු ජාතීසු</p>

<p>ලිංගං
ජාතිමයං පුථු</p>

<p>ඒවං
නත්ථි මනුස්සේසු</p>

<p>ලිංගං
ජාතිමයං පුථූ</p>

<p>යම්
පරිදි එකී ජාතියෙහි වෙන වෙන ජාතිමය ලිංග ඇත්ද, එපරිදි මිනිසුන් කෙරෙහි වෙන වෙන
ජාතිමය ලිංග නැත්තේය.</p>

<p>සෙසු
සියලූම ජීව ගුණය කෙරෙහි පවත්නා වූ ජාති භේදය මිනිසුන් කෙරෙහි නැති බව බුදුන් වහන්සේ
පහදා දෙන්නේ ඉතාම තියුණු ආකාරයෙනි.මේ බැලූව මැනවි.</p>

<p>මිනිසුන්
අතර ''ජාතිභේදයක් කෙශයෙන් නැත, හිසින් නැත, කනින් නැත,
ශ්රෝණියෙන් නැත, සම්බාධයෙහි නැත. මෛථුුනයෙහි නැත. අතින් නැත, පයින්
නැත, ඇඟිල්ලෙන් නැත නියෙන් නැත, දඟින් නැත. කලවෙයින් නැත,
පැහැයෙන් නැත, ස්වරයෙන් අන්ය ජාතීන්හි සෙයින්, ජාතිමය ලිංගි නැත්මැයි".</p>

<p>වාසටේඨ
සූත්රයෙහි දී බුදු හාමුදුරුවන් තර්ක කරන්නේ, ඒ විලාසයෙනි.</p>

<p>අවුරුදු
දෙදහස් පන්සියයකටත් වඩා පැරණි ඒ බුදුබණ, මේ වන තෙක්ම අභියෝගයට ලක් වුණේ
නැත. මීට වඩා දිගු පුන්ථලින් යුතු මානව හිතවාදයක් ලොවේ කොතැනක මුත්කුමන දර්ශනයක
මුත්, තිබිය හැකිද? ආගම් මිශ්ර කොට අච්චාරු හදන්ට වෑයම් කරන තී්රවීල්කාර ඇන්.ජී.ඕ.
කාරයින්ගෙන් අපට ඉගෙන ගන්ට තියෙන මානව හිතවාදය මොකක්ද?</p>

<p>මීට
අවුරුදු දෙදහස් පන්සියයකටත් පෙර, අපේ බුදු හාමුදුරුවන් තර්ක කළ ආකාරය මොන
තරම් අපූරුද?</p>

<p>මිනිහකු,
කවරකු මුත් වන්නේ කි්රයාවෙනි, කරන වැඩ නිසාවෙනි. මේ බැලූව මැනවි.</p>

<p>''කස්සකෝ
කම්මනා හෝතිඪ</p>

<p>සිප්පකෝ
හෝති කම්මනා</p>

<p>වාණිජෝ
කම්මනා හෝති</p>

<p>පෙස්සිකෝ
හෝති කම්මනා"</p>

<p> </p>

<p>ගොවියකු
වන්නේ කි්රයාවෙනි</p>

<p>ශිල්පියකු
වන්නේ කි්රයාවෙනි</p>

<p>වෙළෙන්දකු
වන්නේ කි්රයාවෙනි</p>

<p>කි්රයාවෙන්ම
හාම්පුතෙක්ද වන්නේය</p>

<p>ඉදින්,
බ්රාහ්මණයකු වන්නේත් නොවන්නේත් කිරියෙන්ම නොවේද? උපතින් බමුණකු වන්නේ කෙසේද?</p>

<p>වාසටේඨ
දේශනයේ කූට ප්රාප්තියේදි බුදුහු මෙසේ වදාරති.</p>

<p>''න
ජච්චා බ්රහ්මණෝ හෝති</p>

<p>න
ජච්චා හෝති අබ්රාහ්මණෝ</p>

<p>කම්මනා
බ්රාහ්මණෝ හෝති</p>

<p>කම්මනා
හෝති අබ්රාහ්්මණෝ්"</p>

<p>ගයා
ශීර්ෂයෙහි ඇසතු රුක මුල බුදුව, මේ පණිවිඩය ලොවට දෙන්ට පාරමී දම් පුරන අප මහ
බොසතුන්, මාතංග පණ්ඩිතයන්ට සැඩොල් කුල ඉපිද, බරණැස් නුවර ඒ සියුමැලි
දීරී දිට්ඨමංගලිකාවන් කරින් සිය කුප්පායමට ගිය ගමන, ලෝකයේ සමාජ ප්රතිසංස්කරණ
පිළිබඳ දිග ගමනක මුල් පියවරක් නොවන්නේද?</p>

<p> fndai;=ka weh iu. isáfha ojia y;la fyda wgla
muKlehs .=re¿f.daóyq lsh;s' ta ojia y; wgo .;lf</p>

<p>දිට්ඨමංගලිකාවන්
රොඩී කුප්පායමට ගෙන ගිය තැන් පටන් සිදුවන දේවල්, ගුරුන්ථගෝමීන් විස්තර කරන්නේ
මහත් අභිරුචියෙනි. මාතංගයන්ගේ බෝසත් සිතත්, බෝසෙත් වතත්, බෝසෙත්
ගුණයත් අප දකින්නේ  මෙතැන්හිදීය.</p>

<p>මාතංගයන්ගේ
මුල් අදිටන වුයේ තමා පිරිමියෙකු නම්, දිට්ඨමංගලිකාවන් තම පාද පරිචාරිකාව කර ගැනීමය.
දීන් එය ඉටුව ඇත. දීන් මතු වන්නේ, ඔහුගේ පිරිමිකම නොව, බෝසත්කමය.
මාතංගයන් තමන්ම අමතන විලාසය ගුරුන්ථ ගෝමීන්ගේ රමණීය ගැදි බසින් මෙසේ පළ වෙයි.</p>

<p>''ඉදින්
මෙ කුල දො මා නිසා මහත් යශස් නොලදු නම්, මා සුවිසි බුදුන් අතවැසියෙක් නම් නොවෙයි.
මැය පා දවේ දිය මුන්ථ දඹදිවැ රජුනට අභිෂේකෝදක කළ මැනවැ"</p>

<p>මාතංගයන්
කර පිටින් ගෙන ආ වන්දිය, දිට්ඨමංගලිකාවන් ලබන්නේ එයාකාරයෙනි. ඉදිරි කාලයේදී මුන්ථ
මහත් ජම්බුද්වීපයෙහිම රජවරුන්ට අභිෂේක මංගල්ලයේදී හිස් සෝදා නාන්ට වන්නේ
දිට්ඨමංගලිකාවන්ගේ කකුල් සේදො වතුරෙනිෟ</p>

<p>එහෙත්
ගිහිව සිට එය ඉටුකළ හැකි නොවෙයි. එහෙයින් පැවිදිවන්ට තීරණය කරන මාතංගයෝ
දිට්ඨමංගලිකාවන් අස්වසා වනගතව සොහොනේ වැරහැලි සොයා සිවුරු සාදා පඬු පොවා පැවිදි
බවට පත්වෙති. දින හතක් ඇතුළත මාතාංගයෝ කසිණ භාවනා කොට අෂ්්ට සමාපත්තිද පංච
අභිඥා ද උපදවා ගත්හ.</p>

<p>මෙතෙක්
කල් දීයටත් සිතින් දුටු දුටුවන් වදමින්" ගිය සැඬොලා දීන් යන්නේ අහසිනි.</p>

<p>දිට්ඨමංගලිකා
වස්තුවේ ඉතාම තියුණු තැනකට පැමිණ සිටින මෙම මොහොතේ, සිය අවධානය වෙනත් කරුණකට
යොමු කරන්ට සිදුවීම ගැන පාඨකයෝ මා කමත්වා.</p>

<p>මෙ
සමයෙහි අප සමාජයේ දේශපාලන බලපුලූවන්කාරකම් ඇති බොහෝ අය අහසින් යන්ට,
ගුවන් යානාත් හෙලිකොප්ටරත් යොදා ගනිති. එසේ ඉගිලෙන්ට තටු ලබන්නට පෙර ඔවුනට
දේශපාලන බලය ලබා ගන්ට මහ පොළොව මත බොහෝ ජරමර කරන්ට සිදුවෙයි. අනතුරුව මේ
උදවිය අහසෙහි පියාඹන්නේ, එසේ කරන ලද වැඩ අවැඩ දෙකේම ආඥාවෙනි,
ආනුභාවයෙනි. බලයෙන් ඇද වැටුණු කල ඒ ආඥාවත්, ආනුභාවයත් නැතිව යන්නේ හරියට
තටුකැපූ කුරුල්ලකු බිම ඇද වැටෙන්නාක් මෙනි.බලයෙන් පිරිහුණු ඇතැම් ඇමතිවරු,
ඉඳහිට රිමාන්ඩ් සිර මැදිරිවල සිට අධිකරණය වෙත අහසින් පියාඹා යන නමුත්, එද සදහටම
නොවෙයි. කොහොමටත් අහසින් යන බොහෝ දේශපාලන බලවතුන් බිම ඇද වැටෙන්නේ,
තටු කැපූ ගිජු ලිහිණියන් මෙනි.</p>

<p>එහෙත්
මාතංග පණ්ඩිතයන් අහසින් යන්ට කළ එකම ව්යායාමය සතියක් කසිණ භාවනා කිරීම පමණි.
සතියක ඒ බවුන් වැඩුම ඔවුන්ට සමාපත්ති අටක්් ලබන්ටත් අභිඥා පහක් ලබන්ටත් හේතු
වාසනා විය. එය, ධර්මයේ බල මහිමය නම්, මාතංග පණ්ඩිතයන් අහසින් පොළවට
වට්ටවන්ට කිසි ජගතකුට දීන් පුන්ථවන්කමක් නැත.</p>

<p>දිට්ඨමංගලිකාව
ඔසොවා තබන්ට නිසි වෙලාව මේ යැයි සිතන මාතංගයෝ අහසින් ගොස් සැඩොල් ගම් දොරටුවේ
බැස, පිඬු සිඟා යමින්, දිට්ඨමංලිකාවන්ගේ දෙපැල ඉදිරිපිටද සිටගනියි.</p>

<p> uq,a ieKfha§u ÈÜGux.,sldj ish ieñhd y÷kd
fkd.;a;dh' ~~jvj jykafi" iefËd,a f.he¶ hkqfjka weh ;jqidg lshkakSh'
wk;=rej ish ieñhd y÷kd .kakd y;</p>

<p>''
හො පුන පුනා බලන්නී, හැඳිනැ අතින් ළෙහි පැහැරු හඬා පාමුලැහි දී හිමි ,
තොපට මෙවැනි සිතක් ඇති බැවින් මා මහත් යශසින් පිරිහෙළා අනාථ කුමට කළාව" යි
නොඑක් සෙ වැහැසැ ඇසැ කඳුන්ථ පිසැ පියා නැඟී, අතින් පය ගෙනැ, ඇතුන්ථ ගෙයි
වඩා හිඳුවා, බත් දුනි."</p>

<p>තම
අරමුණ මහානුභාව සම්පන්න තවුසකුු වන්නට නම්, මෙ පරිදි දිට්ඨමංගලිකාවන් අනාථ කිරීම
ගැන මහ බෝසතුන්ට සමාව දිය නොහැක. එසේ හෙයින්, දිට්ඨමංගලිකාවගේ මුවට නැගුණු
දීමා කුමට අනාථ කෙළෙහිද?" යන වදන් සැබැවින්ම නිරුත්සාහීය; අව්යාජය; සාධාරණය.
එතෙකුදු වුව, ඇතුන්ථ ගෙයි වඩා හිඳුවා බෝසතුන්ට බත් දෙන විට, ඇගේ පාදපරිචාරිකා
ගුණය මහත්ව නැඟී එයි.</p>

<p>ජාති
මදමානයෙන් නැගී සිටි ඒ  කුල දීරිය දීන්
කොහිද? ඇය තුළින් දීන් එබිකම් කරන යසෝදරාවන්ගේ සෙනෙහැති පිළිඹිබුව ඔබට
නොපෙනේදෟ</p>

<p>කාරණාව
බුදු වෙනවා ලකුණු දීනේ </p>

<p>නෑර
කැටුව ආවා මම සරණ ගෙනේ</p>

<p>බාවනාව
තොර නොකරමි සිතින් අනේ</p>

<p>මාලිගාව
අද අඳුරුයි සුවාමිනේ</p>

<p>දිට්ඨමංගලිකාව,
යසෝදරාවන් මෙන් එසේ නොකී බව සැබෑය. එතෙකුදු, ඇය තුළින් මතුව එන්නේ
යසෝදරා පිළිබිඹුුවමැයි.</p>

<p> yËd je,fmk ÈÜGux.,sldjka wiajik fndai;ayq
fufia f;m,;s'</p>

<p>''නුඹගේ
පා සේදො ජලය, මුන්ථ මහත් දඹදිව රජුන්ගේ අභිෂේක ජලය බවට පත් කරන්ට මට හැකිය.
නුවරට ගොස්, මගේ සැමියා සැඩොලෙක් නොවෙයි, මහා බ්රහ්මයාය යනුවෙන්
කියවදී.</p>

<p>එබසට
පිළිවදන් දෙන දිට්ඨමංගලිකාවන්ගේ කටට රන් මසු අහුරක්ම දීමීමත් මදිය. ඇගේ
පිළිතුර, සිය රුචිරතර ගැදියෙන් 
,shd ;nk .=re¿f.daóka iqr;g rka mkays|la ÿkak;a uÈh' ukaoh;a" ta
ms</p>

<p>දීන්
ඉතින් දිට්ඨමංගලිකාවන්ගේ පිළිවදන, ගුරුන්ථගෝමීන්ගේ වදන් වලින්ම ඇසුව මැනවි.</p>

<p>දීමම්,
මාගේ මුව දොසින්මැ දුක්පත්වීමි. එසේ කියන්නට නොනිස්සෙමි".</p>

<p>ගුරුන්ථගෝමීන්ගේ
කුලවත් වියත් ගැදි බස, අමු හිංගලට නැගුවොත්, කියවෙන්නේ මෙ පරිදිය.</p>

<p> ~~ uu keyqfKa" uf.a lg ksid' uu wudrefõ
jegqfKa" uf. lg ksid' b;ska ug" ±ka ta jf.a fndrejla lshkag neye¶'</p>

<p>''
දිට්ඨමංගලිකා නම්, පාඩමක් ඉගෙන ගත්තාය.</p>

<p>තමන්
''දුක්පත්්" වූයේ හෙවත් දුකට පත් වූයේ, කට නිසා බැව් ඉගෙනගත් තැන් සිට,
ඕදී ශිෂ්්ටමංගලිකාවක් වූවාය. </p>

<p>එතෙකුදු
වුවත්, බලයෙන් දිස්ටි වන ඇතැම් දුෂ්ටමංගලිකාවන්, ඒ පාඩම ඉගෙන ගන්නේම නැත.</p>

<p> </p>

<p> </p>

<p> </p>






</body></text></cesDoc>