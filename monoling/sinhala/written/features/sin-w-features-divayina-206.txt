<cesDoc id="sin-w-features-divayina-206" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-divayina-206.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>idys;H
wgqjdj</p>

<p>il=
lúuÕ y÷kajdfok iqfndaOd,xldr ikakh</p>

<p>iqfndaOd,xldrh
md,s niska ,sheù we;s tlu w,xldr .%ka:hhs' jq;af;daoh md,s niska ,sheù we;s tlu
Pkaoia .%ka:hhs' fï .%ka: fol ,sheù we;af;a ud.O NdIdj fyj;a md,s NdIdj muKla
o;a;jqkag ixialD; w,xldrh yd tys Pkaoi y÷kajd§u msKsih' .%ka: folu rpkd fldg
we;af;a oUfoKs hq.fhys úiQ wiydh meúÈ úh;dKka jQ ix>rlaIs;  uyd iajdñmdohka úisks'</p>

<p>ixialD;
w,xldr Ydia;%h;a PkaoiaYdia;%h;a wkqj hñka md,sfhka ldjH rpkd jkq oelSu
ix>rlaIs; uyd iajdñmdohkaf.a wruqK jQ nj meyeÈ,sh'</p>

<p>tfy;a
oUfoKs l;sldj; mjikafka NslaIQka úiska ldjH kdgl kQ.; hq;= njh' kQ.ekaúh hq;=
njh' tfia ;sìh§ NslaIQkag kdgl rpkh i|yd flfia fj;;a ldjH rpkh i|yd Èß foñka
iÕrc moúh fydnjñka isá uyf;r flkl=ka nuqKq lú u.g nisñka fujeks .%ka: rpkd lsÍu
tla w;lska l=yq, okjk lreKls'</p>

<p>oUfoKs
l;sldj; iïmdokh jQfha fojk merl=ïnd rcq ojih' tjl ~~uyd iajdó moúh fyj;a
ix>rdc Oqrh~~ oerefjda kï wdrKHl ^jkjdiS&amp; fïOxlr f;rKqfjdah' t;=fuda Bg
fmr" tkï fojk merl=ïnd rcqf.a mshd jQ úchndyq  rcq oji lrlaIs; uyd iajdñmdofhdah' t;=uka wh;a
jQfha .%dujdiS NslaIq ms</p>

<p>ud fï
lreKq bÈßm;a lf</p>

<p>~~mqrd;kfhys
rduY¾ud§ka úiska lrk ,o fYdaNkjQ w,xldr .%ka: we;;a Y=oaO ud.êlfhda tajd Ndú;hg
fkd.ks;s' taksid Tjqka iqÿiq w,xldrhlska i;=gq lrkafka kï uekú' udf.a fï mßY%uh
ta fjkqfjks'~~</p>

<p>ix>rlaIs;
udysñfhda iqfndaOd,xldrh wrUñka tfia i|yka lr;s'</p>

<p>~~iqoaOud.êld~~
hk iqfndaOd,xldr md,s mdGhg tys ikakh wre;a mjid we;af;a ~~Y=oaO jQ ud.O
jHjydrj;ayq~~ hkqfjks' ~~msßisÿ ud.O NdId jHjydrh fyj;a md,s NdId jHjydrh
we;af;da~~ hkq tys woyihs' fï lshk Y=oaO ud.OH kï nuqKkaf.a il= jyßka yd il= lú
u.ska fkdflf</p>

<p>~~Y=oaO
ud.êlfhda ;uka msßisÿ fyhska w,xldrhka úkHdifhka fYdaNk jqjo mqrd;k fyhska
wmßY=oaOyhs wj{fhka fkdj,¢;s'~~</p>

<p>fï
iqfndaOd,xldr ikak mdGfhkao by; i|yka woyi ;yjqre fjhs'</p>

<p>furg
ixialD; w,xldrjd§ lúu. ;yjqre jkakg jQfha 10 jk ishjfia isgh' oUfoKs hq.h 13 jk
ishjig wh;a fjhs' ta jk ;=re;a we;eï NslaIq msßila Bg tfrysj igka jefoñka isá
whqre oeka meyeÈ,sh' Tjqyq kï jkjdiS NsCIQyqh' .%dujdiS NsCIQyq ljo;a f,!lsl
úIh flfrys ie,ls,su;a jQy' tksid nuqKq lúuÕ wkqj hñka md,sfhka .oH moH .%ka:
rpkhg Tjqyq miqng fkdjQy' 10 jk ishjfia fyda 11 jk ishjfia fyda ,sheù we;ehs
ie,lsh yels md,s uyd fndaê jxih;a" uyÆ merl=ïnd rc jkakg u| l,lg fmr ,sheù
we;s ~~kd,xldr kï md,s ldjHh;a ta njg idOl t</p>

<p>ix>rlaIs;
ix>rdchkaf.a wNsm%dh jQfha Bkshd Y=oaO ud.êlhkaf.a yKñá neyer lrùuh'
~~w,xldr~~ hkq Tjqka is;k ;rï wY=oaO fkdjk nj;a" th je  flfiao hkq;a fmkakd§uh' th fkdmsrlaIs; uyd iajdñmdofhda fufia mji;s'</p>

<p>~~fkdfhl=;a
Ydia;% úIfhys mqreÿ l</p>

<p>iqfndaOd,xldrh
jQl,s w,xldr Ydia;%h ms</p>

<p>tfy;a
ix>rlaIs;  uyd iajdñmdofhda nuqKka
mejeiQ yeu woyilau" Tjqka oerE yeu wdl,amhlau ysia uqÿKska
fkdms</p>

<p>nuqfKda
ldjHfhys wd;auhla .ek l:d lr;s' ta wd;auh oajksh" rih" jfl%dala;sh
wdÈ jYfhkao y÷kaj;s' nqÿ oyug wkqj wd;auhla ke;' tksid ldjHfhys wd;auhla .ek
l:d lsÍugo ix>rlaIs; uyd iajdñmdofhda hqyqiqÆ fkdfj;s' rih nuqKkag n%yau;aj
ifydaor jqjo ix>rlaIs; udysñhkag ldjH rihg jvd O¾u rih W;=ïh' ixialD; w,xldr
.%ka:j, iq,n YDx.drd,dm ix>rlaIs; udysñhkag yqre ke;' ta fjkqjg t;=ukaf.ka
nqÿ.=Ku mejfihs'</p>

<p>~~iqfndaOd,xldr
mqrdK ikakh~~ Ndr;Sh w,xldr Ydia;%h ye±Ífuys§ iqfndaOd,xldrhg;a jvd wmg
m%fhdackj;a fjhs' iqfndaOd,xldrfhka fláfhka mejefik foa tys§ iúia;rj;a jHla;
f,i;a újrKh fjhs' oajksjdofhys ;s%úO Yío jHdmdr" úNdj" wkqNdj"
id;aúlNdj" jHNsjdßNdj yd riksIam;a;sh ms</p>

<p>iqfndaO,xldr
ikak l;=jrhd ixialD; w,xldr Ydia;%h .eUqßka yod, úhf;ls' ;u ku .u i|yka
fkdl</p>

<p>mod¾:
lSu" ldjHdo¾Yh" oajkHdf,dalh" kdgH Ydia;%h jeks ixialD;
ldjHdkqYdik .%ka: weiqqre lrñka lreKq úia;r lsÍu yd ksrela;s oelaùu fï ikak
.%ka:fhys biau;= ù fmfkk rpkd ud¾.h fjhs' my; oelafjkafka iqfndaOd,xldr moHhlg
mod¾: olajd myod fok whqre i|yd ksoiqkls'</p>

<p>~~ÿlaLrEfmhudkkafod
- l:kakq lreKdÈfl</p>

<p>ishd
fidk+kudkkafod - fidfld fjiaika;riai ys~~ whx wdkkafod" idudÊldkudkkafod
hkd§ka lshk ,o fï ikaf;daIh( ÿlaLrEfm" ÿlaL,CIK jQ( lreKdÈfl" lreKdh
fr!øh hk úfYaIfhys l:kakq flfia fõo@ hk fï fodaIh ke;s ys" tfiauehs'
fjiaika;riai" úYajka;r fndaêi;ajhkaf.a" fidfld" mq;%odr úryfhka
jQ fYdalh" fidk+kx" oeka wikakd jQ iNHhkag" wdkkafod ishd"
wdkkaohg ldrKd fyhska ld¾fhdampdrfhka ikaf;daI kï jkafkahs'</p>

<p>jla;Dyqf.a
idud¾:Hfhka lreKd Èh ridÈhg muqKqjd m%ldY lrk ,o fYdaldÈh f;fï ÿlaL iajrEmfhka
isáfhka wikakjqkag m%S;s Wmojkafkah hk wNsm%dhhs'~~</p>

<p>iqfndaOd,xldrfhys
fláfhka mjik ,o lreKq úia;r lsÍug ikak l;=jrhd ork W;aidyh .%ka:h mqrdu oelsh
yelshs' my; oelafjkafka ta i|yd ksoiqka lSmhls'</p>

<p>~~fuys
ldjH kï uqla;l l=,ld§ ldjH jYfhka wjhj iajNdjhg tu uq;a;l l=,ld§ jQ wkH;r jdlH
iuQyfhka msreKq jD;a; úfYaIfhka Nskak jQ yqÿ moHuh jQ fyda pïmqh hk kï we;s
moH.oHuh jQ fyda uyd jdlH iajNdjfhka isáfkahs' kdgl kï Nr;dÈ kdgHYdia;%.; kdkd
m%ldrfhka olajk ,o iajNdj we;s ldjHho fuysu ,laIKfhka tl foaYhlska hqla; m%lrK
NdKdÈ kjrEmlfhdao kdáld .kafkahs'~~</p>

<p>~~jH[ack
iajNdj jHdmdrhg jHx.Hhhs lshkq ,efí' fï ;D;Sh jHdmdrhg úIh tn÷ jdlHuhhs o;
hq;=hs' fuys jHx.Hd¾: m%Odk jQ nkaOh W;a;uhhso" jHx.Hd¾: m%Odk fkdjkafka
uOHu hhso wjHx.H nkaOh wOuhhso lsh;a~~</p>

<p>~~máNdk
máNd hkq m%{jg ku' thska jQ ldjHh;a máNdhhs lshkq ,nk fyhska fï m%{ úfYaIh
rpkdfhys w;Hkaf;damldrhhs o; hq;='~~</p>

<p>ix>rlaIs;
udysñhka ixialD; w,xldr Ydia;%h y÷kajd §fuys§ fn!oaO Ñka;khg ydkshla fkdùug
j.n,d.;a whqre by; i|yka úKs' ikak l;=jrhdo ix>rCIs; udysñhkaf.a tu wdl,amh
uekúka jgydf.k isáfhah' ridjfndaO kïjQ miajk mßÉfÔohg ikak lsÍu wrUñka mjik fï
fldgiska ta nj meyeÈ,s jkq we;'</p>

<p>~~fufia
m%;s{; jQ kshdfhka m%ia;+; kjm%ldr jQ YDÚ.drdÈ rih ili ixidr ÿlaL ksiairKhg
wiydh ldrK jQ úuqla;s rifhka talri jQ w;sm%Õ; jQ ioaO¾uduD; rifhysu f,d,a jQ
Y%oaOd ndyq,Hh we;s Y=oaO ixhókag úuqL jqj;a ,laIK muKlska f,dal jHjydrfhys
wiïfudayh msKsi ixfCIam l%ufhka olajkq leue;sj oeka máNdkj;d hkdÈ lsh;a'~~</p>

<p>iqfndaOd,xldr
ikakfhys NdId jHjydrh fln÷o hkq fuys Wmqgd olajk ,o fldgiaj,ska meyeÈ,s jkq
we;' ta jkdys oUfoKs hq.fha fndfyda jHdLHdklrejkag ms%h jQ ixialD; jpk fnfyúka
ñY% isxy,hu nj wuq;=fjka lsjhq;= fkdfõ'</p>

<p>isß
;s,lisß</p>

<p> </p>






</body></text></cesDoc>