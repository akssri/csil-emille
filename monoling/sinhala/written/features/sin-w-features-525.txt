<cesDoc id="sin-w-features-525" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-525.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>පිසූ අහර උණුසුම්ව තබා ගත හැකි තාපබරණි
උදුන</p>

<p>මිනිත්තු 9-10 ක් වැනි කෙටි කාල පරාසයක්
තුළ ගෑස් එසේත් නැතිනම් දර හෝ ලී කුඩු වැනි අන් ඉන්ධනයක් භාවිතා කර ජලය නැටවෙන
තෙක් භාජනයකට දමා බතක්, ධාන්ය වර්ගයක්, අල වර්ගයක් හෝ ව්යංජනයක් පිස,
අනතුරුව ''තාපබරණී" නමැති අග්නි තාප උදුන නොහොත් ගින්දර නොමැති සුන්දර උදුන යැයි
විරුදාවලියකින් හඳුන්වන උදුන තුළ බහා තැබුමෙන් එතුළ මුන්ථමනින්ම තැබූ බත හෝ අන්
ආහාර ඉදෙන අතර නැවතත් රස්නයෙන් යුතුවම පාවිච්චියට ගත හැකි බැව් ප්රදර්ශනය කෙරෙන වැඩ
මුන්ථවක්් පසුගිය බ්රහස්පතින්දා මහරගම ජාතික තරුණ සේවා සභා ශ්රවණාගාරයේදී පැවැත්විණි. </p>

<p>ඉන්දියානු කේරළ ප්රාන්තයේ උපත ලද
''තාපබරණී" උදුන පරිසර සුරැකීම පිළිබඳ උනන්දුවක් දීක් වූ ශී්ර ලාංකිකයන් තිදෙනෙකුට
හඳුන්වාදීම උදෙසා පෙළඹුණේ පොදු රාජ්ය මණ්ඩලීය වැඩසටහන් සම්පාදකවරයකු වූ ආචාර්ය
ජයතිස්ස පෙරේරා මහතා විසිනි. රන්ජිත් දිසානායක, අනුර වීරක්කොඩි හා උපාලි වික්රමසිංහ
යන තිදෙනා මේ අනුව කේරළයේ සංචාරයක යෙදෙමින් උදුන පිළිබඳ අධ්යයනයක යෙදිණි.
කේරළයේ පිහිටුවන ලද බලශක්ති කළමනාකරණ ම්යධස්ථානය පොදු රාජ්ය මණ්ඩලීය හා
ඒකාබද්ධ වෙමින් කේරලයේ පරිසර හා බලශක්ති මධ්යස්ථානයේ නිලධාරීන් තිදෙනෙක් ලංකාවට
පැමිණීමෙන් පසු උදුන් නිපැයුම පිළිබඳ එකඟතාවකට පිවිස මෙම වැඩමුන්ථව පැවැත්විණි.
කේ්රළය නියෝජනය වූයේ අධ්යක්ෂ දර්ශන ගුණිතාත්, අධ්යාපන නිලධාරී උදයභානු සහ
ග්රාමීය ක්ෂේ්ත්ර වැඩසටහන් නිලධාරී අජිත් ප්රභූ යන නිලධාරී මහතුන්ගෙනි.</p>

<p>වැඩමුන්ථව ඇමතූ දර්ශන ගුණිතාත් මහතා ප්රකාශ
කළේ කේරළ ප්රාන්තයේ ප්රධාන තේමාවක් වන්නේ පරිසර හිතකාමී වීමත් බලශක්තිය
සංරක්ෂණය කිරීමත් බවය. ලෝකයේ අනාගතය දෙස බලන විට බල ශක්තිය නිරන්තර පිරිහීමකට පත්වන
බවත් ඉදිරියේදී හීනතාවකට මුහුණ පෑමේ අවදානමකට මුහුණ දෙන බවත්ය. මේ අනුව බලශක්තිය
අපතේ් යැවීම පිළිබඳ දීනුවත් වුවහොත් එය අපතේ නොයවා සංරක්ෂණය කරගත හැකි
බවත්, සදාතනික පාරිභෝජනය සඳහා බලශක්තිය සංරක්ෂණය කළ යුතු බවත් සමස්ත
කේරළය පුරාම ප්රචලිත කෙරෙන බවත් ඔහු කීවේය. මේ පිළිබඳව ෂීධ - 14000 ප්රමිතියද ඔවුනගේ වැඩ සටහන් සඳහා
ලැබී තිබෙන බැව්ද ප්රකාශ කෙරිණ. </p>

<p>''තාපබරණී" උදුන සඳහා ඔවුන් යොදාගෙන
ඇත්තේ ඉතා සරල මූලධර්මයක් බවත් ඒ අනුව නිමැවූ එහි මිල කාටත් දීරිය හැකි තත්ත්වයක
පවතිනා බවත් උදුනේ තාක්ෂණය පිළිබඳ පැහැදිලි කිරීමක් කළ අධ්යාපන නිලධාරී උදයභානු
මහතා පැවසීය. කේරලයේ බලශක්ති කළමනාකරණ කේන්ද්ර උදුනේ නිර්මාණය හා වැඩිදියුණු
කිරීමට කැප වී කි්රයාත්මක වෙයි.</p>

<p>තාපය මාරු කරලීමේ මූලධර්මය පාදක කොටගෙන
උදුන නිර්මාණය කර තිබේ. කිරණ විහිදීම හා තාපය හීනවීම වැළැක්වෙන අයුරින්ද තාප විකිරණයට
විශිිෂ්ට ලෙස ඔරොත්තු දිය හැකි ප්රසාරණය කළ පොලිස්ටිරින් යොදා උදුන නිමවා ඇත. මේ
හැර තෙතමනයට ඔරොත්තු දෙන ලැමිනේට් කළ සියුම් වැස්මකින්ද යුතුය. මනා ඇසුරුම්
සැකැස්මකින් හා පරිහරණ පහසුවකින් යුතුවීමත්් සැහැල්ලූ බවත් උදුව පාවිච්චිය පහසු කරයි.</p>

<p>උදුන භාවිතයෙන් සිදුවන ඉන්ධන පිරිමැසුම 70%
පමණ වන බවත් ආහාර පිසීම සඳහා කුස්සියේ ගත කළ යුතු කාලය බෙහෙවින් අඩුවන බවත්
වැඩිදුරටත් පැවසූ ඔහු මෙය ගෘහණියකට වෙනත් කටයුත්තක යෙදීම සඳහා කාලය ඉතිරි කර දීමක් බවත්
ඕනෑම ආකාරයක කෑලිවලං මේ සඳහා යොදාගත හැකි බවත් පරිහරණයේදී පරිස්සම් සහගත වන බවත්
පැහැදිලි කළේය. </p>

<p>පිසූ ආහාර වර්ග පැය 8 ක් දක්වා කාලයක්
තුළ උණුසුම්ව තබා ගත හැකි වීමත් විශේෂ සිද්ධියකි. එනම් උදේට ඉවූ දීය දවල් වන තුරුත්
දවල් පිසූ ආහාර රාතී්ර පරිභෝජනයට ගත හැකි බවත් ඉන් ගම්ය වෙයි.</p>

<p>සති දෙකක පමණ ඇවෑමෙන් ශී්ර ලාංකික
වෙළඳ පළට ඉදිරිපත් කිරීමට නියමිත ගින්දර නැති සුන්දර උදුන, ''තාපබරණීය"
පිළිබඳ වැඩි විස්තර රාජගිරිය, වැලිකඩ ප්ලාසාහි අංක 17 ස්ථානයේ පිහිටි ස්ටෙෆ්
ලංකා ලිපිනයෙන් හා දුරකථන අංක 888799 වෙතින්ද විමසිය හැක.</p>

<p>ආර්.බී. දිසානායක </p>

<p> </p>






</body></text></cesDoc>