<cesDoc id="sin-w-features-713" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-713.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head>ශ්රී
පාද වන්දනාව ජීවන වන්දනාව කරගනිමු</head></p>

<p> </p>

<p> </p>

<p>උඳුවප්
පොහෝ දවස සිංහල බෞද්ධයාට සේම සමස්ත බෞද්ධයාටම වැදගත් වූවකි. වෙසෙසින් මෙරට
ජනතාවට කරුණු කිහිපයක් ඔස්සේ වැදගත්වේ. ශ්රීපාද වන්දනා සමයේ ආරම්භය එකකි. මෙරට
භික්ෂුණී ශාසනයේ සමාරම්භය සනිටුහන් කරමින් සංඝමිත්තා තෙරණිය මෙරටට පැමිණීම තවත්
කාරණයකි. බෞද්ධාගමේ ව්යාප්තියට ඉවහල් වූ ශ්රී මහා බෝධියේ ශාඛාවක් මෙරටට  ගෙන ඒම තෙවන කාරණයයි.</p>

<p> Y%Smdo jkaokh W÷jma fmdfyda ojiska wdrïN lr
fjila fmdfyda ojiska wjika flf¾' jia iufha wjidkh;a iu.ska Wodjk fuu fmdfydh
Ys; foaY.=K hym;a ;;a;ajh óg fya;= fõ È.= l,la lE,Ejgu ;snQ Y%S mdoia:dkh
ksYaYxlu,a, rdcH iufha§ iqoaO mú;% fldg uyck;djg jkaokh i|yd ilia l</p>

<p> ls%ia;shdks uqia,sï iy yskaÿ wd.ñlfhda
;ukaf.a  úYajdihkag wkqj lghq;= lr;s'
fuu jkaokd iuh ksudjkqfha iuka foajd,fha uyd fmryersks' tfiau foaY.=Kh b;d
hym;aj mej;Su fuys jkaokdj myiq lrhs' wêl j¾Idj fjk;a wdmod wdÈh ksid jif¾ fiiq
ld, iSudjkays§ Y%S mdo jkaokdjka isÿlrkafka ke;' ckm%jdo j,g wkqj ta ld,
iSudjkays idudkH ck;dj jkaokdfõ fkdhkafka foú foaj;djqka tu ld,fha jkaokd udk
isÿlrk ksidh hk u;hla mj;S' flfia jqj;a j¾;udkfha Y%Smdo jkaokh wd.ñl ixl,amh
fj;ska neyerg hñka úfkdaodiajd§ wruqKq fj; mßj¾;kh lr.kakg W;aidy flfrk
wdldrh;a ±lsh yelsh' fmr oji fn!oaO ck;dj Y%Smdo jkaokdfõ fhÿfKa wiSñ; mskanr
is;sú,s fj;ska is;a mqrjdf.kh' ßoauhg .ehQ .S;j, lúj, ienE isxy, fn!oaO YslaIKh
yeÈhdj yd ixhuh leáj ;sìKs' is;a joka fndÿ is;sú,s iy .d:d O¾u ldjHdÈfhka
w,xldr lr.;a fn!oaOhdf.a ls%hdldÍ;ajho Bg idfmalaIj ixjr úkS; njla bis,S lDIs
wd¾:sl rgdfjka msßmqka Tjqkaf.a mßir ys;jd§ wd¾:sl meje;au ;=</p>

<p> j¾;udkfha ukaokh úfkdaoh njg m;aj ;sfí' fmr
oji md .ukska .uka l</p>

<p> Ydiksl b;sydihSh f,i fuu fmdfyda oji fnfyúka
jeo.;a lula Wiq,hs' ix>ñ;a;d f;rKshf.a meñu yd ta wdY%s; isÿùï ud,dj b;d
jeo.;ah' </p>

<p>ශ්රී
පාද වන්දනාව කේන්ද්ර කරගත් උඳුවප් පොහොය මේ නිසා බොහෝ අංශයන් වෙතින් බෞද්ධයාට
ප්රයෝජනවත් වේ. ශාසනික ලෙසත් ඉතාම වැදගත්ය. ලංකාවේ ශ්රී මහා බෝධිය පිහිටුවීම
භික්ෂුණී ශාසනයේ ආරම්භය ආදී කාරණා නිසා මෙහි ඉහළ වටිනාකමක් වේ. ශාසනික වටිනාකම
සහිත මෙම පොහෝ දවස බෞද්ධ වත් පිළිවෙත්වලින් චර්යා ධර්මවලින් පිරිපුන් දිවියක්
සකසා ගන්නට සෑම බෞද්ධයෙක්ම අදිටන් කරගත මනාය.</p>

<p>සටහන</p>

<p> isisr pdñkao .uf.a</p>

<p> </p>






</body></text></cesDoc>