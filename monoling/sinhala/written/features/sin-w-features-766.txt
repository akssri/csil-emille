<cesDoc id="sin-w-features-766" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-766.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>පශ්චාත්-යථාර්ථවාදී
ප්රබන්ධ කලාවක් කරා - </p>

<p>(විතණ්ඩ
සමය නවකතාව ඇසුරින් කෙරෙන විවරණයකි)</p>

<p>ලියනගේ
අමරකීර්ති</p>

<p> </p>

<p>''හොඳ"
සාහිත්ය කෘති ඉතා ප්රයෝජනවත් සංවාද ජනනය කිරීම උදෙසා සුසමාදර්ශ ලෙස භාවිත කළ
හැකිය. එරික් ඉලයප්ආරච්චිගේ විතණ්ඩ සමය නවකතාව ද ''එක තනි තේරුමකට" වැඩි අර්ථ හෝ
අර්ථාංග නිෂ්පාදනය කළ හැකි සංඥා, රූපක, සංකල්ප රූප, චරිතාංග ආදිය
අඩංගු කරගත්තකි. එහෙත් ඒ කෘතිය සිංහල නවකතාවට පොදුවේ අදාළ වැදගත් ගැටලූවක් ද එය
තුළ දරා සිටියි. මෙම ලිපියේ අරමුණ එම කෘතියත්, ඒ ගැටලූවත් සවිස්තරාත්මකව
සාකච්ඡා කිරීමයි.</p>

<p>සාහිත්ය
කෘතියක අර්ථ උපදින්නේ තියුණු, සංවේදී සහ බුද්ධිමත් පාඨකයන් ඒ කෘතිය අපේක්ෂා
කරන තරම් කැපවීමකින් යුතු කියවීමක් වෙත කෘතිය කිති කවා, අවදි කර, පොළඹවා
ගත් විටය. කෘතිය විසින් ඉල්ලා සිටිනු ලබන කැපවීමක් කිරීමට අවශ්යතාව, වගකීම,
හැකියාව, දැනුම සහ අන්තර්ඥානය නැති විචාරකයෝ''එකතනි-අර්ථයක්" පසුපස ඒකමානීය
බැල්මක් හෙළා බුද්ධිමය ක්ෂණික මෝචනයකින් මෙන් විචාරාත්මක සම්භාෂණය අවසන් කරති.
භාෂාව වූකලි කතුවරයකුගේ අණසකට යටත් කර තනි අර්ථයක් වෙත තබා ඇණ ගැසිය නොහැකි
ද්රවශීලී ප්රවාහයක් සෑම අතටම ගලා බස්නා ප්රවාහයක්-බව පශ්චාත් ව්යුහවාදය මැනවින් පැහැදිලි
කර තිබේ. සාහිත්ය කෘතියක් යනු විශේෂ ආකාරයකට භාෂාව හසුරුවනු ලබන තැනකි.
සාහිත්යකරුවා යනු භාෂාවේ මෙම ප්රවාහාත්මක ස්වභාවය, පාලනය කළ නොහැකි බව
හොඳින් දන්නෙකි. ඒ නිසා ඔහු භාෂාවෙහි පවත්නා ලිස්සන සුන්ථ බව සිය වාසියට යොදා
ගනියි. එහිලා ඔහු කරන්නේ භාෂාවෙහි ඇති බහුවිධ අර්ථජනන ශක්තියට සිය සාහිත්ය කෘතියෙහි
අර්ථ කලාපය පළල් සහ විසිතුරු කරන්නට ඉඩහැරීමයි.</p>

<p>භාෂාව
වූ කලි මෙවැනි ලිස්සන සුන්ථ දෙයක් බව සාහිත්යකරුවා දැනගත යුතු සේ ම පාඨකයා ද දැනගත
යුතු ය. සාහිත්ය කෘතියක් සමඟ පාඨකයෙකු පවත්වන අන්තර් සම්බන්ධය ද පළල් සහ විසිතුරු
අර්ථ කලාපයක් නිපදවිය යුතුය.</p>

<p>රචකයෙකු
නිපදවන අර පළල් සහ විසිතුරු අර්ථකලාපයට ''රචිතය" යැයි කිව හැකිය. පාඨකයෙකු නිපදවන
පළල් සහ විසිතුරු අර්ථකලාපයට ''පඨිතය" යැයි කිව හැකිය. රචකයාගේ රචිතය සහ
පාඨකයාගේ පඨිතය ආයුබෝවන් කීමට එකක් මත එකක් තබන ලද අත් දෙක සේ ගැලවී
නොසිටින්නට ඇති ඉඩ වැඩිය. සාහිත්ය රචකයා සහ පාඨකයා යන දෙදෙනා රචනයෙහි සහ පඨිතයෙහි
තියුණු කුසලතා ඇත්තන් වන තරමට රචිතය සහ පඨිතය අතර නොගැලපීම වැඩිවේ. මෙම නොගැලපීම
නිර්මාණය කිරීම ඉතිහාස රචනාව, ආර්ථික විද්යා විශ්ලේෂණය, දේශපාලන විග්රහය
යනාදියට වඩා තියුණු සහ ආස්වාදජනක ලෙස කළ හැක්කේ සාහිත්ය රචනාවටය. </p>

<p>''සහෘදයා"
යන පැරණි සංකල්පය ඒ නිසා ප්රතිඅර්ථ ගැන්වීමකට ලක් කළ යුතු යැයි හැඟේ. සහෘදයා යනු
රචකයෙකුගේ කෘතියක රචකයා අදහස් කළ අර්ථය යැයි පෙන්විය හැකි තනි අර්ථයක්
අල්ලාගන්නෙකු නොවිය යුතුය. පශ්චාත්-ව්යුහවාදයෙන් සහ එහි නිවුන් සහෝදරයා
විසංයෝජනවාදයෙන් පසු සහෘදයා යනු රචකයෙකුගේ රචිතයක් මත එය හා ගැලපෙන සහ නොගැලපෙන
විසිතුරු පඨිතයක් නිපදවන්නෙකු විය යුතුය. සාහිත්ය කෘති කියවීමෙහි ඉමහත් ආනන්දය වූ කලි
ඒ සංකීර්ණ පඨිතයයි; අර්ථ නිෂ්පාදන ක්රියාවලියයි; සම්භාෂණයයි. </p>

<p>මෙම
ලිපියෙහි අරමුණ ඉලයප්ආරච්චිගේ ''විතණ්ඩ සමය" නවකතාව ඇසුරින් සාහිත්ය රචනයට සහ පඨිතයට
අදාළ ගැටලූ කිහිපයක් වෙත අවධානය යොමු කිරීමයි. මෙය ප්රබන්ධ කතාවේ ඇතැම් ප්රශ්න
ගැන ''විතණ්ඩ සමය" පාදක කරගෙන කරන සාකච්ඡාවක් ලෙස වැඩි වශයෙන් ද, එම කෘතිය
පිළිබඳ විචාරයක් ලෙස මඳ වශයෙන් ද සැලකිය හැකිය.</p>

<p>''විතණ්ඩ
සමය" බගන්දරා නවකතාවේ ම දිගුවක් වුවද මෙම ලිපියේ දී අපි එය ස්වාධීන කෘතියක් ලෙස
සලකමු. මෙම නවකතාව සමීපව කියවන පාඨකයෙකු මුලින්ම හඳුනාගන්නා දෙයක් නම් එක්තරා
අස්වාභාවික යුගල සමූහයකි. පළමු පරිච්ඡෙදයේ මාතෘකාවෙන්ම පටන් ගනිමු. එය ''ලූණු කැඳ
හා ලූණු දෙහි" නම් වේ. ලූණු කැඳ සහ ලූණු දෙහිවල ලූණු පොදුවේ තිබීම හැරුණු විට වෙන පොදු
දෙයක් නැත. එය සමාන යැයි බැලුබැල්මට පෙනෙන අසමානතා දෙකක එකතුවකි. මෙම නොගැලපෙන
අස්වාභාවික යුගලකරණය වෙත අවධානය යොමු කරන මෙන් රචකයා ඉල්ලා සිටින්නේ ගුණසේන සූප
ශාස්ත්රය පොතෙහි ලූණු දෙහි සහ ලූණු කැඳ නිර්වචන උපුටා පෙන්වමිනි. ''විතණ්ඩ සමය" සමීප
කියවීමකට ලක් කරන්නෙකුට මෙම අස්වාභාවික සහ නොගැලපෙන යුගලකරණය කෘතියේ රචිතයට ඇතුන්ථ
වන යතුර ලෙස භාවිත කළ හැකිය. ඒ යතුරෙන් අපි රචිතය විවර කරමු.</p>

<p>''ලූණු
කැඳ සහ ලූණු දෙහි" යන නොගැලපෙන වාග්මය යුගලකරණය එක්තරා පුද්ගල සම්බන්ධයක් වෙත
එළිය විහිදුවයි. නවකතාවේ චරිත දෙකක් අපට මුලින්ම මුණ ගැසේ. තරුණ විවාහක කතක සහ
හෙදියක වන මද්දුමී මහලූ ජරාජීර්ණ, ඔඩුදුවන කුෂ්ටයක් ඇති, විවාහයන් දෙකක් අත්හැර
දමන ලද, 71 කැරැල්ලේ සාමාජිකයෙකුව සිටි මාඕ මාස්ටර් සමඟ පෙමක පැටලී සිටියි. ලූණු
කැඳ සහ ලූණු දෙහි අතර දෘශ්යමාන සාම්යයක්, පොදු බවක් ඇති සේ ම මද්දුමී සහ මාඕ
මාස්ටර් අතර දෘශ්යමාන සාම්යයක් සහ පොදු බවක් ඇත. එනම් මද්දුමී ගැහැනියකි. මාඕ පිරිමියෙකි.
තවද මද්දුමී හෙදියකි. මාඕ ලෙඩෙකි. එමෙන්ම මද්දුමී ශිෂ්යාවය; මාඕ ගුරුවරයාය. ලූණු කැඳ සහ
ලූණු දෙහි අතර අභ්යන්තරික විසම්බන්ධය හෝ විකාර සම්බන්ධය මද්දුමී සහ මාඕ යුවළ වෙත ද
ඇත. කතුවරයා මෙසේ කියයි. ''ඔවුන් දෙදෙනා අතර ඇත්තේ නොගැලපෙන අඹු සැමි
සම්බන්්ධයක් වැනි ප්රේම සම්බන්ධයකි." (පිටුව 14) ඇගේ සැමියාට ද, මහ ගෙදරට
ද හොරෙන් ගෙනයන මෙම සම්බන්ධය ගැන මද්දුමී පසුවන්නේ දෙගිඩියාවෙනි. එහෙත් ඇය ඔහුගේ
මහලූ සිරුරට ආසා කරයි. ''ඔහුගේ මහලූ සිරුර ඇගේ සිත පුබුදුවාලයි." (පිටුව 16)</p>

<p>නවකතාව
විවරවන්නේ ස්ත්රී පුරුෂ සම්බන්ධය පිළිබඳ මෙම සංකීර්ණ සාහිත්යයික චිත්රයෙනි. එය
ලිංගිකමය වශයෙන් සංකීර්ණය; ආචාර ධාර්මිකව සංකීර්ණය; ලිංගික දේශපාලනමය වශයෙන්
සංකීර්ණය; මනෝ විද්යාත්මකව සංකීර්ණය. එය කෙතරම් සංකීර්ණ මිනිස් සබඳතාවක් ද යත්
ඉලයප්ආරච්චිම පවා ඒ සංකීර්ණතාවෙහි සාහිත්යයික විභවතාව වටහා නොගනියි. සාහිත්යයික
විභවතාව යනුවෙන් මා අදහස් කරන්නේ එම සංකීර්ණ සබඳතාව බහුවිධ අර්ථ ජනනය කරවිය හැකි
සාහිත්යයික රූපකායක් ලෙස රචිතයක් මත භාවිතවීමට ඇති ශක්තියටයි. ඉලයප්ආරච්චි එම ශක්තිය
නොදැකීමේ ගැටලූව වෙත අපි මෙහි ලිපියෙහි අන්තැනකදී ආපසු එමු.</p>

<p>ඊළඟට
තවත් චරිත දෙකක් අපට මුණ ගැසේ. ඒ මාඕ සහ මහේෂ්ය. මාඕ හැත්තෑ එක කැරැල්ලේ
නායකයන්ගේ හුදකලා, ජරාජීර්ණ සහ මහලූ ශේෂයකි. මහේෂ් ව්යාපාරික පවුලක ප්රසන්න
තරුණයෙකි. මාඕ පර්චස් හතරක කුඩා ලයින් කාමරයක් වැන්නක වසන දුප්පතෙකි; මහේෂ්''බෙල්ලන
මැදුරේ" එකම පුත්රයායි. මෙම ධනවත් තරුණයා දුප්පත් මහල්ලාගේ සමාජවාදයේ අනුගාමිකයා වී
සිටියි. මෙම මිනිස් සබඳතාවෙහි ද ලූණු කැඳ සහ ලූණු දෙහි විකාර සම්බන්ධයක හෝ විසම්බන්ධයක
ලක්ෂණ තිබේ. ඒ අතරම මේ පිරිමි දෙදෙනාට ම පොදු දෙයක් ද තිබේ. එනම් ඔවුහු දෙදෙනාම
මද්දුමීගේ සිරුරෙහි පහස ලබා තිබීමයි. ඒ දෙදෙනාම ඇයගේ සිරුර සූරාකා ඇතැයි ඉතා සරල
ඒකමානීය අර්ථකථනයක් දීම අමාරුය. මද්දුමී යනු සූරාකනු ලැබීම වැනි ගනුදෙනුවකට ඉඩ නොතබන
බලවත් ලිංගිකත්වයක් ඇත්තියකි. ජරපත් මහල්ලාත්, නහඹු තරුණයාත් දෙදෙනාම අතර
එහා මෙහා යා හැකි සංකීර්ණ ලිංගික කලාපයක් ඇය සතුව ඇත. එහෙයින් ඇය''විතණ්ඩ සමයේ"
සංකීර්ණතම චරිතය යැයි කෙනෙකුට තර්ක කළ හැකිය. මාඕ ද එවැනි සංකීර්ණ ලිංගිකත්වයක්
ඇත්තෙකි. ඔහු කලෙක ගැහැනු සහ පිරිමි දෙකොටස සමගම ලිංගික සබඳතා පැවැත්වූ ද්විලිංගිකත්වයක්
ඇත්තෙකි. අපට මෙහිදී වැදගත්වන්නේ මද්දුමී සහ මාඕ යුගලකරණය සේ ම මාඕ සහ මහේෂ්
යුගලකරණය ද විසම්බන්ධයක් ලෙස කතුවරයා විසින් රචනය කර තිබීමයි.</p>

<p>නවකතාවේ
දෙවැනි පරිච්ඡෙදය තුළ ද විසම්බන්ධතා යුගලකරණයක් දැකගත හැකිය. ඒ මෙසේය: ''මහේෂ්
විසින් පදවන ලද මෙටැලික් ග්රේසනිය වෙල්යායට පෙරළී බොනට්ටුව චප්ප විය. (පිටුව
26) මෙය දෙවැනි පරිච්ඡෙදයේ ආරම්භක වැකියයි. එය වැකියක් පමණක් නොව තියුණු
සංඥාවක් බවට පත්වන සාහිත්යයික රූපයකි. වෙල්යායකට ජපන් මෝටර්රථයක් වැටී ඇත. මෙය මෙම
නවකතාවේ චරිතයන්ට අදාළ භූගෝලයීය පරිසරය ඉඟි කරවන සංඥාවකි. මෙය ගම්මානයකි. එහෙත්
ඒ වෙත නූතනත්වය පැමිණෙන්නට පටන්ගෙන තිබේ. නූතනත්වය පැමිණ ඇත්තේ එය දරාගත නොහැකි
භූගෝලයීය පරිසරයකටය. මෙටැලික් ග්රේ සනිය නූතනත්වයේ ප්රකාශනයක් නම් එය වැටී ඇති
වෙල්යාය සාම්ප්රදායිකත්වයේ ප්රකාශනයක් සේය. මහේෂ් විසින් පදවන ලද ''ග්රේ සනිය" ගමන්
කොට ඇති පරිසරය ඉක්බිති කතුවරයා විස්තර කරයි: කාරය ගමන් කළ පාරේම ගොන් කරත්තයක්
ද ගමන් කර තිබේ. කාරය කරත්තයේ කඩ ඇණයේ ද වැදෙන්නට ගියේය.  ඉලයප්ආරච්චි මෙටැලික් ග්රේ සනිය එක්
පසෙකත්, ගොන් කරත්තය සහ වෙල්යාය තව පසෙකත් තබා විසම්බන්ධ යුගලකරණයක් නිමවයි.
අපි දැන් වෙල්යායකට වැටී එරුණු නවීන ජපන් මෝටර් රථයක ඡායාරූපයක් සිතින් මවා ගනිමු.
එය සංඥාවකියි
(sign) සලකමු. සෑම සංඥාවකටම කොටස් දෙකක් ඇත. එක හඟවනය
ද, දෙවැන්න හැඟවුම ද ලෙස ගත හැකිය. අප මෙම විස්තර කරන සංඥාවේ හඟවනය වෙල්යායට වට
සනියේ දෘශ්ය රූපයයි. ඊට අපගේ සිත් තුළ නොයෙක් අරුත් දනවන්නට ශක්තිය ඇත.
එමඟින් හඟවනු ලබන අර්ථ හැඟවුමයි. හැඟවුම යනු එක් තනි අර්ථයක් නොවේ. අපිට දැනෙන එක්
හැඟවුමක් මෙසේ විය හැකිය. මේ කාරය ගමන්ගත් මග ඇත්තේ ගමකය; එහි වැසියන් තවමත්
උදැල්ල බිම තබා නගරයට පැමිණ නැත. එක්කෝ ඔවුන් තවමත් නූතනත්වය ගමට පැමිණෙන පාර සකසා
නැත. එහෙත් ගම වෙනස්වෙමින් පවතී. තවද මෙම හඟවනය හඟවන තවත් හැඟවුමක් නම් රිය අනතුරක්
සිදුවී ඇති බවය. එය වූයේ කෙසේදැයි හඟවන්නට හඟවනයට හැකියාවක් නැත. එහෙත් අනතුර
සිදුවූයේ මන්දැයි දැනගැනීම උදෙසා කුතුහලයක් අප තුළ නිපදවන්නට ඒ හඟවනයට පුන්ථවන.
නොනවතින අර්ථ නිෂ්පාදන ක්රියාවලියක් පණගන්වන මේ සංඥාවේ කැපීපෙනෙන හෙවත් ප්රධාන
ලක්ෂණයවන්නේ''ග්රේ සනිය" කුඹුරට වැටීමෙන් සිදුවී ඇති ඓතිහාසික උත්ප්රාසයයි.</p>

<p>මා
එය ඓතිහාසික උත්ප්රාසයකියි කීවේ ධනේශ්වර-නූතනත්වය සංකේත කරන''ග්රේ සනිය" කුඹුර
විසින්''අඩක්ම ගිලදමා" ඇති බැවිනි. මෙහිදී අප සංඥාවකියි සැලකූ රූපය-වෙලට වැටුණු ග්රේ
සනියේ රූපය- මෙම නවකතාව පුරාම දැකිය හැකි, ඉදිරියට තවත් වර්ධනය වන විසම්බන්ධ
යුගලයන්හි සහපැවැත්ම අඟවයි. ඓතිහාසික වශයෙන් මෙම සංඥාව සරල රේඛීය සමාජ විකාශනයකට වැඩි
සංකීර්ණ, ඓතිහාසික අවස්ථාවක් අඟවයි. ධනේශ්වර ක්රමයත්, ගැමි අර්ථ ක්රමයත්
(මම ඊට වැඩවසම් යැයි නොකියමි) එකට පවතියි. නූතනත්වයත්, සම්ප්රදායත් එකට
පවතියි. මෙය පහසු සම්බන්ධයක් නොවේ. කොයි මොහොතක හෝ එකක් අනික මත කඩා වැටිය
හැකි සම්බන්ධයකි.''ලූණු කැඳ සහ ලූණු දෙහි" අතර වැනි ම වූ විසම්බන්ධයක් මෙහිද ඇත. ඊට
මෙහිදී ද සංඥාවේදමය ශක්තියක් ඇත.</p>

<p>කුඹුරට
වැටුණු කාරය මෙම නවකතාවේ කළාලයට අදාළ පාරිසරික, භූගෝලයීය,
ඓතිහාසික සංකීර්ණතාව හඟවන අතර කාරය ඇතුළේ මිනිස් සබඳතාව ද සංකීර්ණ යුගලකරණයක්
හඟවයි. අනතුරට පත් කාරය තුළ ධනවත් බෙල්ලන පවුලේ එකම පුත්ර මහේෂ් සහ ඔවුන්ගේ
මැදුරේ මෙහෙකාරිය පෙමක පැටලී සිටියි. ඔවුන්ගේ සම්බන්ධයෙහි''විසම්බන්ධය" තියුණු කරන්නේ
ඇය දෙමළ ජාතික සෙල්වි වීමයි. ඊටත් වඩා, ඇය මහේෂ්ගේ පියාගේ ද''පමේවතියක"
වීමයි. පන්තිභේදය වැනි භෞතික භේදයකුත්, ජනවාර්ගික භේදය වැනි දෘෂ්ටිවාදාත්මක
භේදයකුත්, පියා සහ පුතා එකම ගැහැනියට පමේ කිරීම වැනි ආචාරධාර්මික සහ
සංස්කෘතික ගැටලූවකුත් කුඹුරට වැටුණ සනිය ඇතුළේ තිබේ. සනිය වෙලට වැටීමේ
ඓතිහාසික අනතුර සංකීර්ණ මිනිස් සබඳතාවක් අර්බුදයට යවයි.</p>

<p>යථාර්ථවාදී
නවකතාව එක් අතකින් කරන්නේ ඉතිහාසයේ ප්රවාහය ඉදිරියේ ඒකපුද්ගල මිනිසා''සංකීර්ණ ලෙස"
ජීවත්වීමට දරන උත්සාහයේදී ඇතිවන ගැටුම, නොගැලපුම, දිසා මාරුව,
බලතුලනය ආදිය චිත්රනය කිරීමයි. ඇතැම් නවකතාකරුවෝ සිය නවකතා පසුපස ඇති
ඓතිහාසිකත්වය ඉතා ඈත පසුබිමට තල්ලූ කරති. නොඑසේ නම් නවකතාව ඉතිහාස කතාවක් බවට
පත්වේ. එය සරල රේඛීයය. ඒකමානීය. පැතලිය. ඩොස්ටයවේස්කි වැන්නෙකු ඓතිහාසිකත්වය
ඈතට තල්ලූ කරන්නේ සිය චරිතයන්ගේ මනස් විග්රහයට සුවිශේෂ තැනක් දෙමිනි. ටොල්ස්ටෝයි
වැන්නෙකු එය කරන්නේ ඓතිහාසික ප්රවාහය සහ පුද්ගලජීවිත ප්රවාහය ගෙත්තමක් මෙන් එකක් මත
එකක් ගෙතීමෙනි. මේ දෙදෙනාම අඩු වැඩි වශයෙන් යථාර්ථවාදී නවකතාකරුවෝ වෙති.</p>

<p>ඉලයප්ආරච්චිගේ''විතණ්ඩ
සමය" නවකතාව එහි ඓතිහාසිකත්වය හසුරවන්නේ කෙසේද යන කාරණය, නවකතාවක් ලෙස එහි
අගය තීන්දු කරයි.''බගන්දරා" නවකතාවෙන් ද පෙන්නූ පරිදි ඉලයප්ආරච්චි ඓතිහාසිකත්වය
පිළිබඳ අතිශයින් සංවේදීය. එහෙයින් ඔහුගේ නවකතාවේ ඓතිහාසික මානය සියල්ල ඉක්මවා
මතුවෙයි. ඔහුගේ චරිත, (මද්දුමී වැන්නියක හැර) සියල්ලෝම ඉතිහාසයේ ප්රවාහයට
අනුගත වෙති. ඇතැම්විට ඔවුහු නවකතාකරුවාගේ තේමාත්මක සැලැස්මක දෙබස් කියන්නෝ බවට
පත්වෙති. ඉතා සංකීර්ණ කතාවක් බවට පත්වීමේ විභවය ඇතිව ඇරඹෙන නවකතාව ඉතා සරල,
සරල රේඛීය, ඒකමානීය තේමාත්මක අදහසක් කියයි. එය මෙසේ සංක්ෂිප්ත කළ හැකිය.
ලංකාවේ වමේ ව්යාපාරයේ ක්රමික භින්නය සහ ඛාදනය සිංහල ජාතිවාදයේ නැග්මට තුඩු දෙයි. ඒ
ජාතිවාදය මතුවන්නේ එක්තරා අසංස්කෘතික, සංස්කෘතික වශයෙන් පරාජිත වෙළෙඳ
පන්තියක් තුළිනි. පැරණි වමේ ව්යාපාරය තුළින් ශිෂ්යත්වයක් ලැබ සෝවියට් දේශයට
ගිය මහේෂ් සිංහල ජාතිවාදයේ සාර්ථක හඬ බවට පත්වෙයි. </p>

<p>යථාර්ථවාදය
තුළ සිට අර ඓතිහාසික මානය ඈතට එළවන ශිල්පක්රම භාවිතයට නොගැනීම නිසා
පාඨකයාගේ ඇල්ම, බැඳීම, කුතුහලය ගැන නොසලකන නවකතාවක් බවට''විතණ්ඩ සමය"
පත්වෙයි. ඒ නිසා නවකතාවත්, පාඨකයාත් අතර ඇත්තේ ද''ලූණු කැඳ සහ ලූණු දෙහි"
අතර වැනි සම්බන්ධයකි. නවකතාව බිත්තියක එල්ලා ඇති, දුරස්ථ චිත්රයක් ලෙස මට දැනිණ.
එනයින් ද එය තාර්කික ඉතිහාස රචනයක් පාඨකයකු හා ඇති කරගන්නා සම්බන්ධයට සමානය. එය අපට
දුර සිට, වාස්තවිකව බලන්නැයි ඉල්ලා සිටිනු වැනිය. දුරස්ථීකරණය සහ වාස්තවිකත්වය
ප්රබන්ධ කතාවට අනවශ්ය වියළි බවක් දෙතියි මට සිතේ. නවකතාව වාස්තවිකවීමට තැත් දැරීම
එක අතකින් බොළඳ ක්රියාවකි. භාෂාවට වාස්තවික විය නොහැකි බව පශ්චාත්-ව්යුහවාදය
යළි නොබිඳිය හැකි සේ පෙන්නා දී තිබේ.</p>

<p>ඉසබෙල්
අය්යන්දේ නවකතා ලිවීමට පෙර පුවත්පත් කලාවේදිනියකි. ඇයගේ වාර්තාවක් කියවූ නොබෙල්
ත්යාගලාභී කිවි පැබ්ලෝ නෙරූදා මෙසේ කීය:''ඔබ ඔබේ වාර්තාවලට ඔබව රැගෙන එනවා වැඩියි;
වාර්තාව දුරස්ථවෙන්න ඕනෑ. ඔබ කළයුත්තේ නවකතා ලිවීමයි". ඉන්පසු අය්යන්දේ ලියූ
නවකතාව ඇය ලෝක සාහිත්ය වෙත ගෙන ගියේය.</p>

<p>දීවිතණ්ඩ
සමයේ" තෙතමනයක් නැති දුරස්ථභාවය වාස්තවිකත්වයේ ප්රතිඵලයක් නොවේ නම් එය නවකතාකරුවා
විසින් සැලසුම් කරන ලද හෝ අහම්බයකින් ඇතිවූ කලාත්මක උපක්රමයක් ලෙස ද කියවිය හැකිය.
විතණ්ඩ සමයේ කළාලය ආදරයෙන් තොර වියළි බිමක් සේ ය. එක් එක් විතණ්ඩයන්හි ගිලී
සිටින ඔවුහු ගනුදෙනුකරුවෝය. නවකතාව තුළ සිදුවන සියලූ විවාහයන් තුළ මේ ආදරයෙන්
තොර ගනුදෙනු ස්වභාවය දැක්ක හැකිය. කිසිදු විතණ්ඩවාදයක් හිසින් නොගත් ආදරයේ,
තෙතමනයේ සහ මිනිස් ගුණයේ ගැහැනිය වූ මද්දුමී කොටි බෝම්බයකින් මියගිය පසු ආදර
නියඟයක් නවකතාව වසාගනියි.</p>

<p>නවකතාවේ
ඓතිහාසික මානය ඈත් කර කියවන කල විතණ්ඩ සමය තුළ සහෝදරියක සහ සහෝදරයෙකු අතර
අපූරු සම්බන්ධයක් පිළිබඳ නොකියන ලද කතාවක් ද හඳුනාගත හැකිය. මද්දුමී සහ කුලසිරි එකම
දේශපාලන ව්යාපාරයකට (ජවිපෙ 1971) විවිධ හේතු නිසා සම්බන්ධ වූවෝය. ඒ දේශපාලන
ව්යාපාරයේ ශේෂයන්ගෙන් සහ මතකයන්ගෙන් මිදෙන්නට සහ ඒවා සමඟ ජීවත්වන්නට තැත් කරන
සහෝදර යුවළකගේ සංකීර්ණ කතාවක් ලෙස ද විතණ්ඩ සමය මතුකර ගැනීමට කතුවරයා
අසමත්වන්නේ ඓතිහාසිකත්වය සමඟ ගනුදෙනුව වැරදියට කරන හෙයිනියි මට සිතේ. ලේඛකයන්ට
ලියන සැටි ඉගැන්වීමට වඩා තමන් ඉදිරියේ ඇති කෘතිය නිර්මාණශීලීව කියවීම නූතන සාහිත්ය
විචාරයේ ප්රධාන ලක්ෂණයයි. එහෙයින් මෙම නවකතාව සහෝදර යුවළක් ගැන කතාවක් ලෙස
කියවීමට තවමත් අවකාශ ඇත.</p>

<p>මද්දුමී
මෙම නවකතාවේ වඩාත්ම සජීවි චරිතය බව ඉහතින් කියන ලදි. ඇය ආදරයේ ගැහැනියයි.
ලිංගිකත්වයේ ගැහැනියයි. ඇය සියලූ සම්මත ආචාරධර්ම බිඳගෙන ජීවිතය එහි මුන්ථ මහත්
පරිමාවෙන්ම භුක්ති විඳියි. ඇය පාසල් දැරියකව සිය දේශපාලන ගුරුවරයා සහ උපවිදුහල්පති වූ
මාඕට සිය සිරුරෙහි කන්යා පහස ලබාදෙයි. එය ආදරයේ සහ ලිංගිකත්වයේ ක්රියාවක් බව ඇය
බිරිඳක සහ මවක වී සිටියදීද ඔහු වෙත යෑමෙන් පෙනේ. ඇය සිය සැමියාට ද එකසේ ආදරය කරයි.
ඔහු හෘදයාබාධයකින් බේරා ජීවත් කරවීමට ඇය ඔහු රැගෙන ඉන්දියාවට ද යයි. ඔහු සමඟ ටජ්මහල ද
නරඹයි.</p>

<p>සාන්ත
බි්රජට් කන්යාරාමයේ උගත් තමාලිට නැති නූතන අදහස් සමුදායක් මද්දුමීට ඇත. මේ බලන්න:''ඇය
සිතූ පරිදි ඇඳුම පැළඳුම යනු ශ්රමය වෙනුවෙන් ලබන ජය සංකේතයකි. ඇඳුම් විලාසිතා ගැන ඇය
තුළ දැඩි කැමැත්තක් තිබිණ. පොල් කඩන තරුණයෙකු වුව දවසේ මෙහෙවර අවසන් කර හොඳ
කලිසමකින්, ටෙරලින් කමිසයකින් හා අව් කණ්ණාඩියකින් සැරසී මගතොට ඇවිදීම ඇයට
සතුටක් වියදී (පිටුව 114). ඇය මෙහිදී කල්පනා කරන්නේ නූතන සහ නාගරික සංස්කෘතියකට
අයත් ගැහැනියක ලෙසිනි. ඇය සංස්කෘතියක් විසින් තමන් මත පටවා ඇති දෘෂ්ටිවිද්යාත්මක
බෙදීම් විනිවිදින්නට සමත්ය:''ලංකාවේ සමාජය ගැහැනු පිරිමි වශයෙන් මිනිස් වග දෙකඩ" කර ඇති
බවත්, ඉන්දියාව, චීනය වැනි වෙනත් සමාජ-අතරමැදි ලිංගිකත්වයකට ඉඩදී ඇති බවත්
ඇය වටහා ගනියි; ඇය එවැනි දේ හඳුනාගැනීමෙහිලා සංවේදීය. (පිටුව 121). එසේම
මනුෂ්ය චරිතයේ සංකීර්ණත්වය ගැන ද ඇයට අවබෝධයක් තිබේ.''සාධු චරිත, මෝහ
චරිත, බුද්ධි චරිත ආදී වශයෙන් මිනිස් වග බෙදන්නට මද්දුමී අපොහොසත්ය." (පිටුව
125). මේ ඇය ඉන්දියා ගමනින් ලබන එක් අත්දැකීමකි. බහුවාර්ගික, බහුජාතික පරිසරයක
හැදෙන වැඩෙන මහේෂ් සෝවියට් දේශයට ගොස් පැමිණ ජාතිවාදයේ කොඩිය අතට ගන්නා සැටි සමඟ
සසඳා බලන කල මද්දුමීගේ අර විවෘත හදවත වඩාත් හොඳින් වටහාගත හැකිය.</p>

<p>කෝටි
බෝම්බයෙන් මියයන්නේ ඒ ආදරයේ ගැහැනියයි. විවෘතභාවයේ ගැහැනියයි; පරිත්යාගයේ
ගැහැනියයි. ඇගේ මරණය එතෙක් මුන්ථමනින්ම සිංහල ජාතිකවාදය සමග අනන්ය වී නොසිටි කුලසිරි
ද ඒ වෙත තල්ලූ කරයි. ඔහුගේ කඩයේ ද කොබ්බෑකඩුව පෝස්ටරයක් ප්රදර්ශනය වෙයි.</p>

<p>සංස්කෘතිය
විසින් පටවන ලද දෘෂ්ටිවාදාත්මක සීමා පවුරු බිඳ ජීවිතය එහි පූර්ණත්වය කරා රැගෙන යාමට
මද්දුමීට ඇති ක්රියාශීලී උනන්දුව නවකතාව අවසානයේදී කුලසිරි සතුවන බවක් ඉඟි කෙරේ. ඔහු
ලන්සි තරුණියක සහ මෝස්තර නිර්මාණ ශිල්පිනියක වන මැඩෝනාට පමේ බඳියි. ඉංග්රීසි බස
වාහනය කොට ගෙන විවිධ සමාජ ස්ථර හරහා ද ජාතිකත්වයේ සීමාව හරහා ද යන ඇය ද
නූතනත්වයේ සංඥාවකි. නවකතාව අවසන්වන්නේ කුලසිරි හොලිවුඩ් චිත්රපටියකින් ෆැසිස්ට්වාදය
පරදින සැටි යළිත් බලන දර්ශනයකිනි.  මේ
වූකලී ඔහුගේ දේශපාලන වැඩිවිය පත්වීමද? නැතිනම් වර්ගවාදයට ඔහුගේ ප්රවේශයද? නැතිනම් ඒ
දෙකම අතර තවමත් තීන්දු නොවුණ අනාගතයක් ඇති මිනිසෙකුගේ දෙගිඩියාවද?</p>

<p>ඒ
ප්රශ්නයට පිළිතුර කුමක් වුවත් මෙම නවකතාවේ අර්ථ පද්ධතිය සෑම විටෙකම ඓතිහාසිකත්වය
වෙත ආපසු එයි. සහෝදර යුවළකගේ කතාවක් ලෙස වුවද එය අනවශ්ය තරම් ළංවූ
ඓතිහාසික මානයක් ඇතිකර ගනියි. නවකතාවක් එසේ නොවිය යුතුයැයි කීමට කාටවත්
පුන්ථවන්කමක් නැත. නවකතාව වූකලි දේශපාලනය, ඉතිහාසය, ලිංගිකත්වය ආදී විවිධ
දේ පිළිබඳ සම්භාෂණ එක කෘතියකට රැස් කළ හැකි මාධ්යයක්  බව, සල්මාන් රුෂ්ඩිගෙන් පසු ඉන්දියාවේ
පහළ වූ හොඳම නවකතාකරුවා යැයි බොහෝදෙනෙකු විසින් සලකනු ලබන අමිතාව් ගෝෂ්
සිය''ග්ලාස් පැලස්" නවකතාව සම්බන්ධයෙන් ප්රකාශ කරයි.''ග්ලාස් පැලස්" වූකලි තියුණු
ඓතිහාසික මානයකුත්, තියුණු මානුෂික මානයකුත් මනා ලෙස සංකලනය වූ අගනා
නවකතාවකි. එවන් සංකලනයක් සිදුවන්නේ කෙසේද යන ප්රශ්නය වෙත අපි මේ ලිපියේ ඊළඟ
කොටසින් එළඹෙමු.</p>

<p>මතු
සම්බන්ධයි</p>

<p> </p>

<p> </p>

<p>වින්දනය
ලැබීම නොව ලබාගැනීම</p>

<p>කිනම්
ආකාරයකින් හෝ අපට දැනෙන ලිංගික තෘප්තිය ප්රේමයේ ප්රතිඵලයක්ද? නැත්නම් එය හුදු
රාගය මතුවීමක්ද? වඩා පිවිතුරු මේ අතුරින් කුමක් විය හැකිද?</p>

<p>ගැහැනියක
හා පිරිමියෙකු එක්වී ලබන ලිංගික සතුට හුදු ජීවවිද්යාත්මක අවශ්යතාවන් (බඩගින්න, පිපාසය
වැනි) ඉටුකිරීමක් පමණක්ද?</p>

<p>ලිංගික
ක්රියාවක නිරතවීම ප්රජනනය හෙවත් වර්ගයා බෝ කිරීමේ අරමුණ සඳහා කේන්ද්ර වූ කාර්යයක්
පමණක්මද?</p>

<p>මානව
ලිංගිකත්වය තුළ ලිංගික තෘප්තිය, ප්රජනනය නම් වූ ජීවවිද්යාත්මක අවශ්යතාව ඉක්මවූ
ස්වයං පැවැත්මක් ඇති අත්දැකීමක් බව පැහැදිලි කිරීම මේ ලිපියේ අරමුණයි.</p>

<p>දෙදෙනෙකු
අතර ඇතිවන ආකර්ශනයේ උපරිම ඵලයක් ලෙස ලිංගික එක්වීමක් සිදුවීමත් එයින් ලිංගික
තෘප්තියක් ලැබීමත් දැක්විය හැකිය. දෙදෙනෙකුට ලැබිය හැකි උපරිම සන්තෘෂ්ටිදායක හැඟීමක්
මෙමඟින් ජනනය විය හැකිය. එහෙත් මෙයම කනස්සල්ලට මුල ද වියහැක්කේ අප අපගේ ලිංගික
හැඟීම් ගැන හදාරන්නට උනන්දුවක් නොදක්වන නිසාය. බොහෝදෙනෙකු සිතන්නේ ලිංගික
සුවයක් ලැබීමේ ප්රීතිය ස්වාභාවිකවම අප නොසිතාම ලබාගත හැක්කක් ලෙසිනි. එහෙත් ඇත්ත එය
නොවේ. එය උගත යුත්තකි; අභ්යාස කළයුත්තකි.</p>

<p>ලිංගික
සතුට දෙස ලැබිය හැකි ඉතාම ප්රාථමික ආකාරය නම් එය ප්රජනනය සඳහා හෙවත් අපගේ වර්ගයාගේ
ස්ථිර පැවැත්ම තහවුරු කිිරීමට අප පොළඹවන සාධකය ලෙසයි. එම ජීවවිද්යානුකූල දෘෂ්ටිය
අනුව වෙනස් ගති ලක්ෂණ දරන පිරිමියකු හා ගැහැනියක එක්ව ඒ ගති ලක්ෂණ මුහුකොට අලූත්
ජීවයක් තනයි. ලිංගික එක්වීම යනු එකිනෙකාගේ සිරුර තුළ වෙන වෙනම තැන්පත්ව තිබූ
ජීවයක් බිහිකිරීමට අවශ්ය ජන්මාණුවලට මුණගැසීමට අවස්ථාවක් සැලසීමයි. එසේ බලනවිට එහිදී
ලැබෙන තෘප්තිදායක හැඟීම අතුරුඵලයකි. ජීවවිද්යාත්මකව අපගේ විශේෂයේ පැවැත්මට ප්රජනනය
කළයුතු බවට විවාදයක් නැත. වෙනස් ගතිලක්ෂණ ඇති අය මුහුවීමෙන් වඩාත් හොඳ ලක්ෂණ
මතුකර ගනිමින් පරිණාමය තුළ ඉදිරියට යෑමට අවශ්ය ප්රමුඛතම කාර්යය ලෙස මේ අනුව ලිංගික
එක්වීම දැක්විය හැකිය. අපට ලිංගික ආසාවක් දැනෙන්නේත්, ලිංගික ආකර්ශනයක්
ඇතිවන්නේත් එක්වීමේ උවමනාවක් ඇතිවන්නේත් පෙර කී ලෙස ජන්මාණු මුණගස්වා පරම්පරාව
ඉදිරියට ගෙනයෑමටය. </p>

<p>සත්ව
ලෝකයේ බොහොමයක් දෙනා සම්බන්ධයෙන් ඉහත විග්රහය සත්ය විය හැකි නමුත් මිනිසා විෂයෙහි එය
පූර්ණ සත්යයක් නොවේ. මිනිසා සම්බන්ධයෙන් ද ප්රජනනය හෙවත් වර්ගයා බෝකිරීමේ අවශ්යතාව
ලිංගික එක්වීම පිළිබඳ ප්රබල හේතුවක් නමුත් එය නියත වශයෙන්ම පිරිමි සිරුරක ඇති
ජන්මාණු ගැහැනු සිරුරක් තුළ පරෙස්සමෙන් තැන්පත් කිරීමක් ඉක්මවා යයි. </p>

<p>ඇත්ත
වශයෙන්ම ජීවයක් බිහිකිරීමට අවශ්ය ප්රජනන ක්රියාවලියේ බොහොමයක් කටයුතු අප සිරුරු
තුළ නිහඬවම සිදුවන්නේ අපගේ මැදිහත්වීමකින් තොරවමය. ප්රජනනයට අවශ්ය ඉන්ද්රියයන් කලල
අවස්ථාවේ සිට ක්රමයෙන් වර්ධනයවන්නේ අප සිතාමතා කරන බලපෑමකින් ස්වායත්තවය. ගැහැනු
සිරුර තුළ ජන්මාණු වර්ධනය වී ඇත්තේත් පිරිමි සිරුර තුළ ඒවා වර්ධනයවන්නේත් අප
නොදැනුවත්මය. එය එතරම්ම සියුම්ය. ශරීරය තුළ වන රසායනික ක්රියාවලීන් ඒවා පාලනය
කරයි. ස්වාභාවික රිද්මයකට අනුව ඒවා සිදුවේ. ගැහැනියකට සිතාමතා උවමනා දිනයකට
ජන්මාණුවක් මුදාහැරීමට අවස්ථාවක් නැත. පිරිමියකුට තම අවශ්යතාව අනුව වැඩිපුර හෝ
අඩුවෙන් සිතාමතා ජන්මාණු නිපදවීමට ද හැකියාවක් නැත.</p>

<p>ලිංගිකව
එක්වූ දෙදෙනෙකුට තමන් මුදාහළ ජන්මාණු නියතවම හමුවන බව නිසැක වශයෙන්ම සහතික
කිරීමට කළහැකි දෙයක් නැත. ජන්මාණු එක්වූයේ නම් එහි වර්ධනය ද අපට පාලනය කළ
නොහැකි සෛල මට්ටමින් ම සිදුවේ.</p>

<p>ප්රජනනය
අළලා වන සමස්ත ක්රියාදාමය තුළම අපට සිතාමතා කළ හැකි එනම් අපගේ පාලනය
තුළ ඇති එකම සිදුවීම කායිකව එක්වීමයි. මෙය, තමන් පතන දරු සම්පතක් වෙනුවෙන්
දුටු සුබ සිහිනයක් සැබෑ වන ආශ්චර්යමත් මොහොතක්වීමට කෙනෙකුට ඉඩ ඇත. එහෙත්
යුවළකට ලබාගත හැකි උපරිම දරුවන් ගණන ඉක්මවන වාර ගණනක් ඒ යුවළ ලිංගිකව එක්වන
බව නම් කාටත් ඉතාම පැහැදිලිය. ජීව විද්යාත්මක අර්ථයෙන්''ඵල රහිත" ඒ එක්වීම් ගැන බලද්දී අප
ලිංගිකව එක්වීමට පොළඹවන්නේ හුදු ප්රජනන අවශ්යතාවයම නොවන බව තහවුරු වේ.</p>

<p>බොහෝදුරට
සියලූම පිරිමින් තුළත්, ගැහැනුන්ගෙන් 98%ක පමණක් තුළත් ජීවිතයේ කෙදිනක හෝ
ලිංගික අවදිවීමක් ගැන අත්දැකීමක් ලැබේ. ලිංගිකාසාව අපගේ මොළය ඇතුන්ථ ස්නායු
පද්ධතිය හා තදින් බැඳී පවතී. ඇත්තවශයෙන්ම පිරිමි හා ගැහැනු දෙපිරිසටම පොදු ප්රධාන
ලිංගික අවයවය ලෙස මොළය හැඳින්විය හැක්කේ එහෙයිනි. ලිංගික ආසාව ඇති
කිරීම, බාහිර උත්තේජවලට ප්රතිචාර දැක්වීමට ලිංගික අවයවවලට දැනුම්දීම,
රාගාන්විත සිතුවිලි ජනිත කිරීම, ඒවා මතක තබාගැනීම ආදී කාර්යයන් රැසක් ඉන් සිදුවේ.
එහෙත් මොළයේ බලපෑම මිනිසා විෂයෙහි කිසිසේත් දරුවන් තැනීම වෙනුවෙන්ම සැකසී නැත.
පිරිමි චිම්පන්සියකුට ලිංගික හැඟීමක් ඇතිවී, අවයව ප්රාණවත් වී, ජන්මාණු පහකිරීම
යන මුන්ථ ක්රියාදාමයටම ගතවන්නේ තත්පර 90ක් වැනි කාලයකි. එහෙත් මිනිසාට ප්රේමාලිංගනයේ
යෙදෙමින් ඒ ක්රියාව සඳහා ඉතා කෙටි කාලයක සිට විනාඩි 15ක හෝ පැයක් දක්වා වුවත්
ගතකිරීමට හැකියාව ඇත.  ජීව විද්යාත්මක
අවශ්යතාව ඉටුකිරීමට එතරම් කාලයක් අවශ්ය නැති බව පැහැදිලිය.</p>

<p>ලිංගික
සතුටේ උපරිමයට ළඟාවීම ආශ්රිත යාන්ත්රණය ලිංගික සම්බන්ධතා ගැන කිසියම් හෝ හැඟීමක්
අප තුළ ජනිතවීමට පෙරාතුවම ඇතිවේ. ලිංගිකමය නොවන උත්තේජන හමුවේ ප්රාණවත් වන
පිරිමි අවයව කුඩා දරුවන්ගේත් දැකිය හැකිය. පිරිමි දරුවෙකු නාවන විටක ප්රාණවත් වූ
අවයවයක් දැකීම මවකට අරුමයක් නොවේ. වැඩිවියට පත්වීමට පෙර (එනම් ජන්මාණු පිටකිරීමක්
සිදුකිරීමේ හැකියාවක් ලැබීමට පෙර) තනිවම ලිංගික සතුටේ උපරිම අවස්ථාවට ළඟාවීමට
ක්රියා කිරීම ගැහැනු පිරිමි දෙපිරිසටම හැකිවීම හා ඊට පෙළඹීම සුලභය. එහිදී ලබන''සුවය"
ප්රජනනය හා සම්බන්ධතාවක් නැත.</p>

<p>පිරිමින්
සතු ලිංගික අවයවයට සම්බන්ධ ස්නායු අන්ත රාශියකි. එක්වීමකදී උත්තේජනය වන මේ ස්නායු
මගින් ප්රමෝදජනක හැඟීමක් ඇති කර අවසානයේ පිරිමි ජන්මාණු නිකුත් කිරීම උද්වේගකර
ලෙස සිදුවේ. පිරිමි මේ මොහොතේ ඉතා උත්කර්ෂවත් ප්රමෝදයක් අත්විඳිති.</p>

<p>ගැහැනුන්ගේ
ලිංගික අවයව ආශ්රිතවත් මෙසේ ස්නායු අන්තවලින් ගහන පිහිටීම් ඇත. මෙයින් ප්රධානම
පිහිටීම අවයවවල බාහිරින් පිහිටා ඇත. මෙය උත්තේජනය කිරීමෙන් පමණක් වුවද ගැහැනියකට
උපරිම ලිංගික තෘප්තියකට ළඟාවිය හැකිය. එහෙත් මිනිසුන්, චිම්පන්සීන් හා සමහර
වඳුරු විශේෂ හැරුණු කොට අනෙක් සතුන් සැබැවින්ම මෙවැනි උපරිම වින්දනයක මිහිර විඳින්නේ ද
යන්න අපැහැදිලිය.</p>

<p>ගැහැනුන්ගේ
ඉහත කී සුවිශේෂ බාහිර පිහිටීම චිම්පන්සී ගැහැනු සතකුට සාපේක්ෂව පිහිටා ඇත්තේ
ලිංගික විවරයේ පිටතට වන්නටය. එහි ප්රතිඵලයක් ලෙස පොදුවේ සාමාන්යයෙන් සිදුකෙරෙන
ස්වරූපයේ කායික එක්වීමකදී පිරිමි අවයවය මගින් ගැහැනියගේ ලිංගික තෘප්තියට වඩාත්
තීරණාත්මක එකී පිහිටීම ප්රමාණවත් තරම් උත්තේජනය නොවන තරම්ය. ගැහැනියකට පූර්ණ
තෘප්තියක් සඳහා අමතර උත්තේජනයක් ලබාදීම වඩාත් ඵලදායීවන්නේ ඒ නිසාය.</p>

<p>අනෙක්
අතට, පිරිමියකුට මෙන් නොව ගැහැනියකට ඉතා කෙටි කාලයක් තුළ උත්කර්ෂවත්
ප්රමෝදයකට කිහිපවාරයක් පත්වීමේ හැකියාව ඇත. එසේම ඇය ලිංගික තෘප්තියේ ඉහළටම පැමිණ
ඉන් සැනහීමත් ඇයගේ සිරුර තුළ ජන්මාණුවක් පිටවීමත් අතර සම්බන්ධතාවක් නැත.
(පිරිමියකුට ජන්මාණු පිටකිරීමේදී තෘප්තියක් දැනෙනවා මෙන්)</p>

<p>කෙසේවෙතත්
ලිංගික සුවයක් ලැබීම ජන්මාණු හමුවකට මගපෑදීමට අපට කරන පොළඹවීමක් ඉක්මයි. දෙදෙනෙකු
අතර ඇතිවන බැඳීම තීව්ර කරන්නේ ලිංගික ආකර්ශනයයි. එය දෙමව්පියභාවය,
පීතෘත්වය, දරුවන් රැකබලා ගැනීම,''පවුලක්" ලෙස ජීවත්වීම ආදී අලූත් අත්දැකීම්
රාශියක පදනම් සාධකය විය හැකිය. එය දෙදෙනෙකු අතර ඇතිවන බැඳීම පිළිබිඹු කරයි.
දෙදෙනෙකුට එකිනෙකා වෙත ආමන්ත්රණය කිරීමට ඇති හෘදයංගම හා සංවේදී මාධ්ය ලෙස ලිංගික
හමුව හා ඉන් ලැබෙන තෘප්තිදායක හැඟීම දැක්විය හැකිය.</p>

<p>ලිංගික
ආසාව සෑමවිට, එහි කෙළවර ලැබෙන උත්කර්ෂවත් ප්රමෝදයම කේන්ද්ර කොට ගෙන
නොපවතී. හදිසියෙන් සොරාගන්නා උණුසුම් හාදුවක්, ආදරයෙන් අතිනත වෙලා ගැනීමක්
සුන්දර මතකයන් වන්නා සේ ම ප්රියජනක ලිංගික අත්දැකීමකි. ලිංගික ආසාව, රාගය
ලෙස කොන්කිරීමට අදහස් කළත් දෙදෙනෙකුගේ ආකර්ශනයේ මුල් පියවර එය බවත්, එය
වර්ගයා බෝකිරීමේ අවශ්යතාව තුළ පමණක් නොපවතින බවත් පැහැදිලිය. රාගයෙන් ප්රේමය
වෙන් කිරීම හෝ රාගයෙන් තොර පවිත්ර ප්රේමයක් ගැන මානව ලිංගිකත්වය තුළ වෙන්කොට
දැක්වීම දුෂ්කර විය හැකිය. ඒවා එතරම්ම තදින් බැඳී පවතී. එහෙත් සම්බන්ධතාවක් තුළ රාගය
පිළිබඳ ඇල්ම ක්රමයෙන් අඩුවී අන්යොන්ය අවශ්යතාව, සුහදකම, තනිකමට සගයකු
වැනි සංකීර්ණ තලවලට ආදරයට ගමන් කළ හැකිය. ඒ ගැන විවාදය දිගටම යද්දී අවධාරණය
කළයුතු කාරණය වන්නේ මානව ලිංගිකත්වය තුළ ලිංගික තෘප්තිය අපිරිසිදු හෝ දුෂ්ට
සංකල්පයක් නොවන බවයි. එසේම ලිංගික තෘප්තියක් ලැබීම හෝ හුදු ලිංගික තෘප්තියක්
වෙනුවෙන්ම දෙදෙනෙකු එක්වීම''වැරැද්දක්" නොවේ. අස්වාභාවික නොවේ.</p>

<p>අනෙක්
අතට ලිංගික සතුට ලැබීම වෙනත් අරමුණක් කරා අප යන ගමනේ අපට ඉබේ ලැබෙන දෙයක් නොවේ.
පිරිමින්ට නම් අඩුම ගණනේ ලිංගික හැඟීමක් ඇති කරගෙන උත්තේජනයක් ලබා ජන්මාණු පිටවන
විට තෘප්තියක් ලැබුණත් ගැහැනුන්ට එය එතරම්ම පහසුවෙන් නිරුත්සාහිකව සිදු නොවේ.
ලිංගිකව එක්වීමෙන් තමන් ලබන ලිංගික සතුටේ ස්වභාවය හා තීව්රභාවය අවස්ථානුකූල සාධක මත
වෙනස් වේ. එය වර්ධනය කළ හැකිය. එය වඩාත් තීව්ර කළ හැකිය. ඒ වෙනුවෙන් ගතහැකි
පියවර රාශියකි. කිසිදු බාහිර උපකරණයකින්, ඕෂධයකින් හෝ රසායනික ද්රව්යයකින්
තොරවත් තමන්ටත්, අදාළ යුවළකටත් කළ හැකි දේ බොහෝය.</p>

<p>එහෙයින්
බොහෝදෙනා සිතන්නාක් මෙන් කායිකව එක්වීම අමුතුවෙන් සිතන්නට දෙයක් නැති,
ස්වාභාවික ක්රියාවලියක් සිදුවන විට  ඉබේ
ලැබෙන්නක් නොවේ. එය ලබාගත යුතුය.</p>

<p>ගාමිණී
වනසේකර </p>

<p> </p>

<p> </p>

<p>පිවිතුරු
ආදරයේ සංකේතය ලෙස බොහෝදෙනා සලකන ටජ්මහල ඇත්තවශයෙන්ම ලිංගික ඇල්ම
(රාගය?) පිළිබඳවත්, දරුවන් වෙනත් කිරිමව්වරුන් මගින් පෝෂණය
කිරීමත්, දරුවන් බිහිකිරීමේ අවදානම ගැනත් වන අපූර්ව සිහිවටනයකි. මුම්ටාස්,
ෂාජෙහාන් අධිරාජ්යයාගේ සුවපහසුවට සිටි කාන්තාවන් අතර, තවත් යුවතියකි. ඇය
අධිරාජයාගේ පළමු බිරිය නොවූවාය. එහෙත් ෂාජෙහාන්ගේ අන්තඃපුරයේ සිටි සියලූ අඟනන්
පරයා මුම්ටාස් අධිරාජයාගේ ප්රියතම තේරීම වූවාය. ඇත්තවශයෙන්ම තරුණ නාටිකාංගනාවන්ගෙන්
හා පළපුරුදු මෙහෙසියන්ගෙන් පිරුණු අන්තඃපුරයක් තිබුණත් ෂාජෙහාන් සැමවිටම
තෝරාගත්තේ කුඩා දෑසින් යුතු මුම්ටාස්මය. අවුරුදු 16දී සිය පළමු බිරිඳ ප්රසුත්ර
කළ ඇය 1631 ජූනි 28 වැනි දින සිය දාහතරවන දරු ප්රසුතියෙන් පසු මියයනවිට සෑම වසරකටම
වරක් පාහේ දරුවකු බිහිකර තිබුණි. ඒ සියලූ දරුවන් සප්පායම් වී ඇත්තේ කිරිමව්වරුන්ගේ
මව්කිරිවලිනි. බොහෝවිට ඇය මියයන්නට ඇතැයි සැලකෙන්නේ විශාල දරුවන් සංඛ්යාවක්
බිහිකළ කාන්තාවක් තුළ සුලබ, දරු උපතට පසු සිදුවූ රුධිර වහනයෙන්  වෙතැයි සැලකේ. ෂාජෙහාන් ඇය අමාරුවෙන් සිටින
බව අසා තමා වෙත කැඳවන ලදුව අධිරාජයාගේ දෙඅත් මත මියයෑමට ඇයට අවස්ථාව ලැබී ඇත.
මෙයින් සන්තාපයට පත් අධිරාජයා වසර 2ක් යනතුරු මනා සන්ථපිළිවලින් හා ආහාර
පානාදියෙන් වැළකී සිිටියේලූ. යමුනා ගඟ අසබඩ උද්යානයක මුම්ටාස් වෙනුවෙන් සිහිවටනයක්
ඉදිකරන්නට ෂාජෙහාන් තීරණය කරන්නේ ඉන්අනතුරුවය. ඉස්ලාම් දහමට අනුව දරුවකු ලබන අතරතුර
මරණය වීරත්වයට කාරණයක්වීමත් මේ තීරණයට හේතුවන්නට ඇත. (මිනිසුන් 20,000කට
අනර්ඝ මැණික් ඔබ්බ වූ සුදු කිරිගරුඬ සොයා මේ නිර්මාණය කිරීමට වසර 20ක් ගතවිය. අද
ඉන්දියාවේ ජනගහනය දිනකට 20,000කින් පමණ වැඩිවන නිසා සෑම පැය 24කට වරක් අලූත්
ටජ්මහලක් සදන්නට තරම් අතිරික්ත ශ්රම බලකායක් එරටට ඇත.)</p>

<p> </p>

<p> </p>

<p> </p>






</body></text></cesDoc>