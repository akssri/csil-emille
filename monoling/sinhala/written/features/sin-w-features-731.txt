<cesDoc id="sin-w-features-731" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-731.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>අමෙරිකාවේ
හිසේ රුදාව සදාම් හුසේන්</p>

<p> </p>

<p>j¾. ie;mqï .Kkdjla mqrd me;sr we;s brdl ckdêm;s iodï yqfiakaf.a
ud,s.d  90 l uyd úm;a;sodhl ridhksl hqo
wú .nvdlr we;s njg wfußldj yd ì%;dkHh fpdaokd k.kafka fndfyda ld,hl isghs'
Tjqka fuh f,da jeishka bÈßfha  y÷kajd
fokafka Nhdkl ;¾ckhla yeáhgh' brdlfha wiaila uq,a,la kEr ridhksl wú mßlaIdlsÍu
i|yd tlai;a cd;Skaf.ka jfrka;=jla ,nd .ekSug fuu rdcHhka fol W;aidyhl fhfoñka
isà'</p>

<p>1991
සිට 1998 දක්වා කාලය තුළ සදාම් හුසේන් විසින් 
wú ksÍlaIK lKavdhï uq</p>

<p>අමෙරිකාව
දැන් අනුගමනය කරන්නේ චූදිතයාට පළමුව දඬුවම් ලබාදී දෙවනුව නඩු විභාග කිරීමේ
න්යායයකි. එක්සත් ජාතීන්ගෙන් ඉරාක ආක්රමණයට අවසර ලබාගැනීම අපහසු වන්නේ ආරක්ෂක
මණ්ඩලයේ ස්ථිර සාමාජිකත්වය දරණ රුසියාව, චීනය හා ප්රංශය මීට එකඟත්වයක් නොපෙන්වන
හෙයිනි. ඔවුන් දරන්නේ වෙනස් මතයක්.</p>

<p>සදාම්
හුසේන් තමන්ගේ මාලිගා සංකීර්ණය තුළ විස අවි සඟවා ඇතැයි සිතීමට හෝ පැවසීමට
හේතු ඇතත්, එහි සත්යතාවය තහවුරු කරන කිසිදු සාක්ෂියක් සැපයීමට කිසිවෙකු සමත්
වී නැත. එහෙත් සදාම් හුසේන්ගේ අතීතය දිහා බලන කවරෙකුට වුවද ඔහුට විරුද්ධව නැගෙන
මෙවැනි චෝදනා නොපිළිගෙන සිටීමට නොහැකිය.</p>

<p>iodï W;=re ueo brdlfha ÿmam;a mjq,l Wm; ,o flfkla' fudyq wh;a f.da;%fha
msßi b;d oreKq yd olaI igka ldóyq fj;s' 1950 § 
nd;a iudcjd§ mlaIhg iodï 
ne÷fkah' fuu mlaIfha mrud¾:h jqfka uq¿ wrdìlrfha ck;dj w;r tl yd iudkj
iïm;a fnodf.k fyd| jk uÜgulg ta msßi f.k tauhs' l=vd ld,fha isgu iodï m; fmd;
lshùug oeä reÑhla oelajQ flfkla' tfiau olaI l:slfhls' fï w;r ck;d wdl¾IKhl=;a
Tyq i;=j mej;sKs' fï .;s.=K ksid brdlh kj u.lg fhduqlrùug iu;a kdhlhl= jYfhkq;a
Tyqg ms</p>

<p>1968
දී බාත් පක්ෂය ඉරාකයේ පාලන බලය අල්ලා ගත් අවස්ථාවේදී, එම විප්ලවකාරීන් මෙහෙයවූ
ප්රධාන බලවේගය වුනේ සදාම් හුසේන්ය. 1970 වනවිට ඔහු විප්ලවකාරී බාත් පක්ෂයේ නියෝජ්ය
නායක පදවියට පත්වී හමාරය. මේ කාලය ඉරාකයේ රන් යුගය වශයෙන් හැඳින්වුණු සමයකි. සදාම්ගේ
මඟ පෙන්වීම යටතේ ජාතික අධ්යාපන ක්රමයක් සැකසීම, අලූතින් පාසැල් ඉදිවීම ජනතා නිවාස
රෝහල් හා මහා මාර්ග තැනීම ආදිය සිදුවිය.</p>

<p>මේ
වකවානුවේදීම සම්පූර්ණ බලය තමන් අතට ගැනීමේ අභිප්රායක් සදාම්ගේ සිත තුළද පැවතියත් ඔහුගේ
සගයන් ඔහුට මුල්තැන දීමට කැමැත්තක් දැක්වූයේ නැත. ඔහුට බලය දෙනවාට වඩා ඡන්දයකින්
සුදුස්සෙකු තෝරා ගැනීමට පක්ෂයේ බල මණ්ඩලය තීරණයකට ආහ. මේ බව දැනගත් සදාම්
තමන්ගේ බලය තහවුරු කර ගැනීමට ක්ෂණික පියවර ගත්තේය.</p>

<p>1979
ජුලි 22 දින පක්ෂ ප්රධානීන් සිය දෙනෙකුගෙන් සමන්විත රැස්වීමක් පැවැත්වීමට ඔහු කටයුතු
කළේය. රැස්වීම පැවැත්වූනේ බැග්ඩෑඩ් නගර ශාලාවකදීය. සියලූ දෙනාම රැස්වීම් ශාලාවට
රොක්වූවාට පසු හමුදා නිල ඇඳුමින් සැරසුණු සදාම් ශාලාවේ වේදිකාවට නැග 'මෙතැන පාවාදීමක්
සිදු වී තිබෙනවා. රාජ්ය ද්රෝහීන් පිරිසක් මොවුන් අතර ඉන්නා බව මා දන්නවා' යි පවසා
බාත් පක්ෂයේ මහලේකම් අල් හුසේන් මෂ්හාඩ් වේදිකාවට කැඳවේවේය.</p>

<p>දින
කීපයකට පෙර මොහු සිර අඩස්සියට ගෙන වධහිංසා පමුණුවා, ද්රෝහීන්ගේ නම් ඔහු ලවා
ප්රකාශකරවා ගැනීමේ වැඩ පිළිවෙලක් සකස් කෙරී තිබුණි. මේ අනුව මෂ්හාඩ් එක් එක්කෙනා
වෙන වෙනම හඳුන්වා දුන්නේය. සන්නද්ධ හමුදා භටයින් නම් කරන ලද 60 දෙනකු ඒ මොහොතේම
සිරභාරයට ගෙන එදිනම මරා දමන ලදී. අනතුරුව සියලූ පාලන බලය වෙහසකින් තොරව තමා වෙත පවරා
ගැනීමට සදාම්ට හැකි විය.</p>

<p>අවුරුදු
දෙකක් යාමට මත්තෙන් ඉරාකයේ යුද ශක්තිය දියුණු කළ සදාම් අසල්වැසි රටවල් ආක්රමණය
කිරීමට සැලසුම් සකස් කළේය. ඉරාන හා ක්වේට් ආක්රමණ මීට ඇතුළත් වී තිබිණි. </p>

<p>සදාම්
පුහු ආශාවන්ගෙන් පිරුණු පුද්ගලයෙකි. වෙන්ථම් 19 කින් යුත් ඔහුගේ ජීවිත කතාව සෑම රජයේ
නිලධාරියකු විසින්ම කියවීම අනිවාර්ය කෙරිණි. ඔහුගේ ජීවිත කතාව රචනා කළ ලේඛකයාට ඔහු
පැවසුවේ.</p>

<p>තමා
උනන්දුවන්නේ මේ කාලයේ මිනිසුන් සිතන දේවල් ගැන නොව වසර 500 ක් ඉක්ම ගිය පසු
මිනිසුන් කියන දේවල් ගැන පමණක් සිතීම බවයි.</p>

<p>වරක්
රූපවාහිනී හා ගුවන් විදුලි ප්රධානියා කැඳවූ සදාම්, තමාගේ ගුණ වර්ණනාකර ඉරාක ජනතාව
එවන සියලූ කව් ගී ආදිය ප්රචාරය නොකරන්නේ මන්දැයි විමසා සිටියේය. ආධුනිකයින් විසින්
එවනු ලබන බාල වර්ගයේ පබැඳුම් ප්රචාරය කිරීම තමා නතර කළ බව රූපවාහිනී ප්රධානියා
පැවසීය. කෝපයට පත් සදාම් පැවසුවේ තමන් ගැන වර්ණනා කිරීමට ජනතාවට නිදහස ලබාදිය යුතු
බවයි. රූපවාහිනි ප්රධානියාට මරණ දඬුවම හිමි නොවුනත් රැකියාව අහිමි වී ගිය බව වාර්තා
විය.</p>

<p>කෲර
පාලකයෙකුට වරද හෝ නිවරද හෝ තේරුම් ගැනීමට නොහැක. සාරධර්ම හෝ නීතිය කෙරෙහි
හෝ කිසිම ගරුසරුවක් ඔහු තුළ නොමැති නිසයි. 1980 දී නිවාස අධිකාරියේ නිලධාරීන්
කිහිප දෙනෙකුට අල්ලස් චෝදනා එල්ලවූ අවස්ථාවේදී ඔවුන් එල්ලා මැරීමට නියම කළේ
සදාම් හුසේන්ය. කාර්යාලයේ සේවකයින් සියලූ දෙනාම එල්ලා මරණු නැරඹීමට පැමිණෙන ලෙසත්
නියෝග කළේය. 1981  සහ 1982 වර්ෂවලදී
සදාම්ගේ නියෝග මත මරණ දණ්ඩනය නියමවූ ඉරාක වැස්සන්ගේ ප්රමාණය 3000 කට අධික බව
වාර්තාවේ.</p>

<p>සදාම්ගේ
දොසක් පැවසීමේ වරදට හසුවූ උසස් හමුදා නිලධාරියකු වූ ඕමාර් අල් හසාට මරණ දඬුවම නියම
විය. එපමණක් නොව මරණයට පෙර ඔහුගේ දිව කපා දැමීමටද සදාම් නියෝග කළේය. ඔහුගේ
කෝපය මෙයින් නැවතුණේ නැත.හසාගේ පුතාට මරණ දඬුවම දී හසාගේ බිරිඳ හා දරුවන් මහ මගට
ඇද දමා ඔවුන්ගේ නිවසද විනාශ කළේය.</p>

<p>ඉරාක
වාසී කර්ඩ් ගෝත්රිකයින් 5000 ක් පමණ දෙනා විස දුම් ප්රහාරයකට ගොදුරු කර පවුල් පිටින් මරා
දැමීමේ කි්රයාවට අණ ලැබුණේත් සදාම්ගෙන් බව වාර්තා විය.</p>

<p>ඔහුගේ
ජීවිතයට තර්ජන එල්ල වූ අවස්ථා බොහෝයි. දෙවියන්ගේ ආදරය දිනාගත් තමා විනාශ කිරීම පහසු
කාර්යයක් නොවන බව ඔහු පවසයි. ශුද්ධවූ මොහොමඩ් නදි නායකතුමන්ගේ දියණිය වූ පාතිමා
කුමරියගේ පරම්පරාවට තමන් සම්බන්ධ කර විවරණයක් කරන ලෙස ඉතිහාසඥයින්ගෙන් ඔහු ඉල්ලා
සිටියේය.</p>

<p>සදාම්ට
අවශ්යව ඇත්තේ පුන්ථවන් තාක්කල් ඉරාකයේ රාජ්ය නායකයාව සිටීමයි. ඔහු දැන් 65 වැනි වියේ
පසුවෙයි. ශාරීරික අබල දුබලකම් වසාගෙන, තරුණ පෙනුමකින් යුතුව සිටීමට ඔහු ගන්නේ
අපමණ වෙහෙසකි.</p>

<p>ඉරාකයේ
ඔහු වටා රොක්වන පිරිස එසේ ඉදිරිපත් වන්නේ ඔහුට දක්වන ගෞරවය නිසා නොව ඔහුට ඇති
බිය නිසාය. අමෙරිකානු ජනාධිපති ජෝර්ජ්් බුෂ් මේ බව හොඳින් දනී. සදාම් හුසේන්ගේ බලය
පිරිහී යන බව ඉරාක ජනතාවට ඒත්තු ගිය දිනක ඔවුන් විකල්ප නායකයකු සොයා යෑම
වැළැක්විය නොහැක.</p>

<p>අනෙකුත්
ආරබි රටවල් සදාම් හුසේන් කෙරෙහි එතරම් පැහැදීමකින් පසුවෙන්නේ නැත. ඊට හේතුව වන්නේ
ඔහු සතු රසායනික අවි භාවිත වුවහොත් එයින් තමන්ගේ රටවලට බලපෑ හැකි විනාශය නිසයි.
අමෙරිකානු ආක්රමණයට විරෝධයක් ඔවුන් තුළද නැති බවයි විචාරක මතය. එක්සත් ජාතීන්ගේ
අනුමැතිය ඇතිව හෝ නැතිව නොවැම්බර් මැද භාගයේදී පමණ අමෙරිකාව ඉරාකය ආක්රමණය කිරීමට
බොහෝ දුරට ඉඩ ඇත. අමෙරිකාවේ මූලික පරමාර්ථය වන්නේ සදාම් හුසේන් බලයෙන් පහ
කිරීමයි.</p>

<p> </p>

<p> </p>

<p>පී.බී.</p>






</body></text></cesDoc>