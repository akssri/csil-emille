<cesDoc id="sin-w-features-lankadipa-346" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-lankadipa-346.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>ජේසු සමිඳුන්</p>

<p>ජේසු සමිඳුන් භාරතයේ විසුවේය යන ප්රබන්ධය ක්රි.ව. 1894 පමණ
කාලයේ සිට අද දක්වා නිකොලොයි නොටොවීච්, එස්. ඔබර්මයියර්, ටී. ජේ.
ප්ලාන්ප්, ෆාබර් කයිසර් හා හෝල්ගර් කර්ස්ටන් යන අය විසින් විවිධ ආකාරයෙන් ඉදිරිපත්
කරන ඇත. යුරෝපය තුළ ශාස්ත්රීය හා විද්යාත්මක විවේචනයන් ඉදිරියේ අසාර්ථක වූ මෙම
ප්රබන්ධය දීන් ආසියානු රටවල පතළ වෙමින් පවතී. ඉතා මෑතකදී සිංහලයට පරිවර්තනය කර ඇති
එච්. කර්ස්ටන්ගේ දීජේසු තුමන් දඹදිව විසූ වග" යන පොත ශ්රී ලාංකේය සමාජයේ හොල්මන්
කරන්නට පටන්ගෙන ඇත.</p>

<p>හෝල්ගර් කර්ස්ටන් තම ප්රබන්ධය ගොඩනගාගන්නේ පහත සඳහන්
කරුණු මුල්කරගෙනය. එනම්</p>

<p>¬ ජේසුස් වහන්සේගේ ජීවිතයේ වයස 12-30 දක්වා කාලය
ඇතුළත සිදුවීම් කිසිවක් ශුභාරංචිවල සඳහන් නොවීම.</p>

<p>¬ විවිධ ලේඛන හා සමහරුන්ගේ මත අනුව උන්වහන්සේ මෙම කාලය
ඇතුළත දඹදිව විසූබව.</p>

<p>¬ උන්වහන්සේ දඹදිවින් නැවත පලස්තීනයට පැමිනියේ
බ්රහ්මනයන්ගේ විරෝධය නිසා බව.</p>

<p>¬ පලස්තීනයට පැමිණීමෙන් පසු උන්වහන්සේගේ උගැන්වීම් නිසා
උන්වහන්සේ කුරුසියක ඇණ ගසා මරා දීමූ බව.</p>

<p>¬ උන්වහන්සේ කුරුසියේ මරණයෙන් ගැළවී ආපසු දඹදිවට පැමිණ
බොහෝ ආයුෂ වළඡ් එහි මියගිය බවත් උන්වහන්සේගේ සොහොන එයට සාක්ෂියක් බව.</p>

<p>ජේසුස් වහන්සේගේ වයස 12-30 දක්වා කාලය</p>

<p>ජේසුස් වහන්සේගේ වයස 12-30 දක්වා කාලයේ කිසිවක් සඳහන්
නොවීමට හේතුව කුමක්දීයි අසන කර්ස්ටන් අවාසනාවකට මෙන් සුභාරංචි අවබෝධ කරගෙන ඇත්තේ
ඒවා දීජේසුගේ ජීවිත වාර්තා" ලෙසටය. (පිටු 15-16) දේව ධර්ම විශාරදයකු ලෙස පරිවර්තකයා
විසින් හර්න්වනු ලබන ඔහුට සුභාරංචි පිළිබඳ මූලික දීනුමක්වත් නැති බව පෙනී යාම අප මවිතයට
පත් කරවන සුන්ථය. ඇත්ත වශයෙන්ම සුභාරංචි ජේසුස් වහන්සේගේ වචන හා ක්රියා පදනම් වූ
දේව වේදීය කෘතීන් මිස උන් වහන්සේගේ ජීවිත කථාව නොවේ. ජේසුස් වහන්සේ සම්බන්ධයෙන්
සුවිශේෂ කතුවරුන්ගේ දෘෂ්ටි කෝණයත්. ඔවුන්ගේ පරමාර්ථයත් අනුව ඒවා විවිධ ලෙස සැකසී
ඇත. එම නිසා ඒවායේ විවිධත්වයත් තිබිය යුතුමය. මාක් හා ජොහාන් සුභාරංචි ජේසුස්
වහන්සේගේ උපත හා බාලකාලය සඳහන් නොකරන්නේ ඒවා එම කතුවරුන් දෙදෙනාගේ දේව වේදීය
දෘෂ්ටියට අදාල හෝ වැදගත් හෝ නොවූ නිසාය. එම නිසා ජේසුස් වහන්සේගේ වයස 12-30
දක්වා කාලයේ හිඩස පිරවීම සඳහා ප්රබන්ධයන් ගොඩනැගීම අනවශ්ය බව කිව යුතුය.</p>

<p>ජේසුස් වහන්සේ දඹදිව විසුවේ යැයි කියනු ලබන මුසාව</p>

<p>ළමා ජේසුස් වහන්සේ සේද මාර්ගයේ වෙළරේන් සමග
දඹදිවට සැපත්ව එහිදී බුදු දහම හා වේද ඉගෙන ගත් බව හෝල්ගර් කර්ස්ටන් පවසන්නේ
නිකොලොයි නොටොවීව් විසින් ක්රි.ව. 1894 දී ලයන ලද පොතක් හා කුම්භාශ්රිත ලියැවිල්ල යන
ලේඛන පදනම් කරගෙනය. නොටොව්විගේ ඉදිරිපත් කිරීම අනුව ඔහු ටිබටයේ හේම්ස් හි මහායාන
බෞද්ධ ආශ්රමයකට පැමිණ එහි ප්රධාන ලාමාවරයා හමුවී ඇත. එම ලාමාවරයා එහි පුස්ථකාලයේ
තිබුණේ යැයි කියනු ලබන ජේසුස් වහන්සේ පිළිබඳ පොතක් ඔහුට පෙන්වා එහි අන්තර්ගතය
විස්තර කර දී ඇතැයි පැවසේ. නොටොවීව් ආපසු පැරිසියට පැමිණි පසුව ඒ කරුණු සියල්ල
සහිත ''ජේසුස් වහන්සේගේ සැඟවුන දිවිය"නමින් පොතක් පළ කර ඇත.</p>

<p>මෙම පොත සම්බන්ධයෙන් කිව යුතු කරුණු කිහිපයක් ඇත. එනම්
නොටෝවීව්ගේ පොතෙහි සඳහන් වන කරුණු වල සත්යාසත්යභාවය සනාථ කර ගැනීම සඳහා හේමිස්
ආරාමයේ තිබුණා යැයි කියන මූලික ලේඛනය අත්යාවශ්යය. එහෙත් දීන් එය සොයාගත හැකි බවක්
නොපෙනේ. එය සොයාගත හැකි වුවද, එහි සඳහන් කරුණුවල විශ්වාසනීයත්වය ඔප්පු
කළ යුතුවේ. මෙය කිනම් ශත වර්ෂයකට අයත් ලියැවිල්ලක්ද යන්න දීනගැනීමද අවශ්යය. ඉන්දු
විද්යාඥයන් පවසන අන්දමට ටිබටේ බස ලියැවෙන්නට පටන් ගෙන ඇත්තේ ක්රි.ව. 8 වන ශත වර්ෂය
පමණ කාලයේය. නොටොච්විගේ පොතෙහි සඳහන් කරුණු බොහොමයක් අසත්යයැයි කියන විද්යාඥයෝ
හේමිස්හි ආරාමයේ ලියැවිල්ල ගැන සැක පළ කරති. මේ සම්බන්ධයෙන් ආචාර්ය පී.
ගෝන්බ්රෝල්ට්ගේ ජේසු සමිඳුන් භාරතේ (සිංහල පරිවර්තනය) පිටු 32-54 කියවන්න.</p>

<p>''කුම්භාශ්රිත ශුද්ධ ලියැවිල්ල"</p>

<p>ජේසුස් වහන්සේ දඹදිව විසුසේකැයි යන්නට කර්ස්ටන් සාක්ෂි
වශයෙන් ඉදිරිපත් කරන්නේ ක්රි.ව. 1908 පමණ ලීවේ යැයි එච්. ඩඩ්ලිගේ විසින් ලියන ලද
කුම්භාශ්රිත ශුද්ධ ලියැවිල්ල නැමැති විකාර කෘතියයි. කර්ස්ටන් විස්තර කරන ආකාරයට එය
පෞද්ගලික එළිදරව්වකි. මෙවැනි කෘතීන් අද එමට ඇති නමුත්, ඒවායේ ඓතිහාසික
හෝ ශාස්ත්රීය හෝ වැදගත්කමක් නැත. සමහර විට ඒවා සම්බන්ධ ආගමික නිකායන්ට හෝ එසේ
නැතහොත්, ඒවායේ කතුවරුන්ගේ චරිත තේරුම් ගැනීම සඳහා මනෝ විද්යාඥයන්ට හෝ
ප්රයෝජනවත් විය හැකිය. කතෝලික සභාව තම අධිකාරි බලය යොදවමින් අහම්බෙන් හා හිතුවක්කාර
ලෙසත් නව ගිවිසුමේ සම්මත පොත් තෝරා ගත්තායැයි දොස් නගන කර්ස්ටන් (පිටු 13-14)
දීකුම්භාශ්රිත ලියැවිල්ල" ශුද්ධ බයිබලය මෙන් පූජනීය බවට පත්කර ඇත්තේ කුමන මිණුම්
දණ්ඩක් පදනම් කරගෙන ද යන්න ප්රශ්න කළ යුතුය. කර්ස්ටන්ට මෙන් නව ගිවිසුමේ පොත්
සම්බන්ධ ප්රශ්නයක්, විවිධ ක්රිස්තියානි සභාවන්ට නැති බව සඳහන් කළ යුතුවේ. ඉහත
සඳහන් කරුණු අනුව කර්ස්ටන්ගේ ප්රබන්ධය පදනම් වන මුලාශ්ර ඓතිහාසික මෙන්ම ශාස්ත්රීය
වශයෙන් ද සැක සහිතය.</p>

<p>ජේසුස් වහන්සේ කුරුසිය මත නොමළේය යන ප්රලාපය</p>

<p>ජේසුස් වහන්සේ කුරුසියේ ඇණ ගසන ලද නමුත් උන්වහන්සේ
මරණයෙන් ගැළවී නැවත දඹදිවට සැපත්ව එහි මියගිය සේකැයි කර්ස්ටන් කියයි. මේ සඳහා ඔහු
පදනම් කර ගන්නේ ටුයිරින්හි ඇති ශ්රවවස්ත්රය (මළ වැස්ම) සම්බන්ධයෙන් ඔහු විසින්ම
මවාගත් කරුණු (පිටු 132-173) හා ශ්රී නගර් ප්රදේශයේ ඇතැයි කියනු ලබන ජේසුස්
වහන්සේගේ යැයි සමහරුන් විසින් විශ්වාස කෙරෙන සොහොනය. (පිටු 200-206)</p>

<p>ශ්රව වස්ත්රය (මළ වැස්ම)</p>

<p>ඉතාලියේ ටුරින්හි දක්නට ලැබෙන ශ්රව වස්ත්රයේ ඇති රූප
සටහන මගින් කර්ස්ටන්, ජේසුස් වහන්සේ කුරුසියේ ඇණගසනු ලැබීමෙන් මරණයට පත්
නොවුයේ යැයි ඔප්පු කිරීමට අසාර්ථක උත්සාහයක් දරා ඇත. පළමුකොට මෙම වස්ත්රය
ජේසුස් වහන්සේගේ ශරීරය තැන්පත් කිරීමට හෙයාදාගත් බවට මේතාක් කතෝලික සභාව නිල
ප්රකාශයක් කර නැති බව සඳහන් කළ යුතුය. එහෙත් කර්ස්ටන් එසේ කිරීම අප හිනස්සන සුන්ථය.
හාන්ස් හේබර් හෙවත් කර්ට් බර්නා කවරකු වුවත් විවිධ ක්ෂේත්රවල ලෝක ප්රසිද්ධ විද්යාඥයන්
විසින් ශ්රවවස්ත්රය සම්බන්ධයෙන් කර ඇති පර්යේෂණ දහස් ණනකින් හෙළිවී ඇත්තේ එය
කුරුසියේ ඇණගසා මරාදමුවකුගේ මළ සිරුර එතීමට යොදාගත් එකක් බවයි.
කර්ස්ටන්, මළ වැස්ම පිළබඳ විද්යාඥයකු ලෙස හර්න්වන හාන්ස් නේබර් ශ්රව වස්ත්ර
සම්බන්ධ පර්යේසණයන්ට සහභාගි වූවකු නොවේ. හාන්ස් හේබර් ජේසුස් වහන්සේ නොමළ බව
සඳහන් කරන්නේ (කර්ස්ටන් පවසන අන්දමට) ඔහුට ජේසුස් වහන්සේ දර්ශනය වී මෙම
එළිදරව්ව කළ නිසාය. (පිටුව 148) මෙවැන්නක් විද්යාඥයකුගේ ප්රකාශයක් විය
නොහැකිය.</p>

<p>කර්ස්ටන් ද මේ කිසිදු පර්යේෂණයකට සහභාගි නොවී ශ්රවවස්ත්රය
සිහි මුර්ච්ජා වූ ක්රිස්තු ශරීරය භුමිදාන කිරීමට යොදාගත් බව පවසන්නේ පහත සඳහන් කිසිදු
පදනමක් නැති කරුණු අනුවය. කුරුසියේ ඇණගසා සිටියදී සිහි මුර්ජාවීම සඳහා උන්වහන්සේට
''සෝමා" නමැති ඉන්දීය පානයක් දුන් බව කියන කර්ස්ටන් සුභාරංචිවල සඳහන් විනාකිරි (මාක්
15, 36) තේරුම් කරන්නේ මේසය. තවද කුරුසිය අසල සිටි එසීන් වරයෙකු එසේ
කළැයි කියන (156-157) කථාව විකාරයකි. මෙහිදී කරුණු දෙකක් සඳහන් කළ යුතුව
තිබේ. පළමුකොට කුරුසියේ ඇණ ගසනලද අපරාධ කරුවකුට කිසිවක් දීමට රෝම හේවායකුට
හැර වෙනත් කිසිවකුට ඉඩ නොදිනි. දෙවනුව, එසීන් වරයකු කුරුසිය ළඟ සිටි බවත්
සෝමා පානය ගැන දීන සිටි බවත් ඔප්පු කළ යුතුය. දීටෙරපවේටේස්" නමින් ෆීලෝ නැමැති
ජූදා චින්තකයා විසින් හඳුන්වනු ලැබූ එසීන්වරුන් සතු අභිරහස්ය (ගුප්ත) සුවකිරීමේ
ක්රමයක් තිබුණු බවට ඔවුනට ආවේණික ලේඛනවලින් කිසිසේත් ඔප්පු කළ නොහැකිය. තවද
ශ්වවස්ත්රය සම්බන්ධයෙන් සඳහන් කළ යුත්තේ ක්රි.ව. 10 වෙනි ශත වර්ෂයෙන් ඔබ්බට යන
පැහැදිළි ඉතිහාසයක් එයට නොමැති බවයි.</p>

<p>ජේසුස් වහන්සේගේ යැයි නම් කෙනෙර සොහොන</p>

<p>ජේසුස් වහන්සේ කුරුසිය මත මරණයට පත් නොවී යැයි විද්යාත්මක
ලෙස ඔප්පු කිරීමට මෙතෙක් කිසිදු සාධකයක් ඉදිරිපත් කර නැත. එසේ නම් උන්වහන්සේගේ යැයි
කියනු ලබන ශ්රී නගර්හි සොහොන කාගේද? ඒ සොහොන උන්වහන්සේගේ යැයි කීමට කර්ස්ටන්
ඉදිරිපත් කරන්නේ පහත සඳහන් කරුණුය. එනම් 11 වැනි ශත වර්ෂයේ පටන් එහි වන්දනාකරුවන්
පැමිණීම එහි වසන ගෝත්රිකහන් එය සනාථ කරන බව, මහාචාර්ය හස්නෙන් ෆීඩා එය
උනව්හන්සේගේ යැයි කීම ආදී කරුණුය. ඉහත සඳහන් කිසිදු කරුණක් සනාථ කිරීමට ඓතිහාසික
හා පුරා විද්යාත්මක සාක්ෂි නැත. ඊශ්රායලයේ පවා යම් පුද්ගලයන්ගේ යැයි බොහෝ කාලයක්
පුරා පිළිගෙන තිබුණු සොහොන් එසේ නොවේයැයි පුරා විද්යා පර්යේෂණවලින් සනාථ කර
ඇත. උදාහරණයක් වශයෙන් ලාසරස්ගේ සොහොන, සෙකරියාගේ සොහොන පෙන්විය හැකිය.
එසේ නම් ටිබෙටයේ ඇතැයි කියන සොහොන ගැන කුමක් කිව හැකිය.</p>

<p>කර්ස්ටන්ගේ ක්රමවේදය සම්බන්ධ විමර්ශනයක්</p>

<p>හෝල්ගර් කර්ස්ටන් තම කරුණු ඉදිරිපත් කිරීමේදී ප්රධාන වශයෙන්
භාවිතා කරන්නේ ''විය හැකිය. ඉඩ ඇත. මා විශ්වාස කරන අන්දමට, මා සිතන අන්දමට"
ආදී අනුමානයන් සහිත ව්යවහාරයන්ය. ඔහු නිගමනයන්ට එළැඹෙන්නේ මෙවැනි ව්යවහාරයන්
පදනම් කරගෙනය. ශාස්ත්රීය විග්රහය අනුව මෙය කිසිසේත් කළ නොහැකිය. එනම් මූලික කරුණු
අනුමාන සහිත නම් ඒවා පදනම් කරගෙන ස්ථීර නිගමනයන්ට එළැඹිය නොහැකි බව තර්ක
ශාස්ත්රයේ මූලිකම සිද්ධාන්තයකි. කර්ස්ටන් මෙවැන්නක් නොද්නනේ ඇයිද යන්න අප මවිත
කරයි.</p>

<p>කර්ස්ටන්ගේ ක්රමවේදයේ ඇති විශාලම අඩුපාඩුවක් වන්නේ
මුලාශ්ර පටලවා ගැනීමයි. ඓතිහාසික වැදගත්කමක් නැති මුලාශ්ර සාමණ්යයෙන් වැදගත් යැයි
පිළිගැනෙන ඒවා සමග එක මට්ටමකින් ඉදිරිපත් කිරීමයි. මෙසේ නව ගිවිසුමේ අසම්මත ලේඛන
(පේදුරුතුමාගේ සුභාරංචිය, තෝමස් තුමාගේ සුභාරංචිය, එබියන්වරුන් අනුව
සුභාරංචිය, කුම්භාශ්රිත ලියැවිල්ල ආදිය) පිළිගත් සුභාරංචි හා එක්ව ඉදිරිපත්
කරයි. සම්මත සුභාරංචිවලින් ජේසුස් වහන්සේගේ සැබෑ ජීවිතය ගැන දීනගත නොහැකියැයි පවසන
කර්ස්ටන් (පිටු 14-15) තම ප්රබන්ධය ගොඩනැගීම සඳහා විශාල වශයෙන් යොදා ගන්නේ
සම්මත සුභාරංචි සතරය. විශේෂයෙන් ජොහාන් සුභාරංචියයි. මෙය හාස්යයට කරුණකි.</p>

<p>කර්ස්ටන් සුභාරංචි මෙන්ම ශුද්ධ බයිබලය ද අර්ථ කථනය කරන්නේ
තමන්ට වාසිදායක ලෙසින්ය. මෙසේ පුනරුත්පත්තිය සම්බන්ධ ඉගැන්වීම සුභාරංචි තුළ
ඇතැයි ඔප්පු කිරීමට අසාර්ථක උත්සාහයක යෙදෙයි. (පිටු 107-112) මෙයින් පෙනී
යන්නේ ඔහු සුභාරංචි පිළිබඳව ගැඹුරු හැදෑරීමක් කර නොමැති බවයි. උදාහරණයකින් මෙය
පැහැදිලි කර හැකිය. ලුක් 1, 13-17හි ස්නාවක ජොහාන් තුමා ''එලියාගේ
ආත්මානුභාවයෙන් සහ බලයෙන් උන්වහන්සේට පෙරටුව යන්නේය" යන කියමනෙන් කර්ස්ටන් කියන
ආකාරයට පුනරුත්පත්තියක් අදහස් නොකෙරේ. මුන්ථ පැරණි ගිවිසුම පුරාම දීආත්මානුභාවය"
යන්නෙන් හර්න්වනු ලැබුයේ දිවැසිවරුන් හා 
kdhlhka u; l%shdldÍ jQ foaj fufyhùuhs' foaj l%shdldÍ;ajhhs' fuys§ iakdjl
fcdydka" t,shd jeks Èjeisjreka .Khg jefgk nj;a" uyd Èjeisjrhl= nj;a
weÛfõ' t,shd ±kgu;a meñK we;ehs hkafkka iakdjl fcdydka ;=ud ms</p>

<p>කර්ස්ටන් තම ප්රබන්ධයේ ශාස්ත්රීය බවක් හුවා දීක්වීම සඳහා
පුදුමුාකාර වාග් විග්රහයක යෙදේ. පළමුවෙන් කිව යුත්තේ ඔහු සෙමිතික් (හෙබ්රවේ,
අරමීය, අසිරියන්, බැබ්ලෝනි සහ අරාබි) හා ඉන්දු යුරෝපීය භාෂාවන් අතර
ඇති විශාල පරතරය තේරුම් ගැනීමට අපොහොසත් වී ඇති බවයි.</p>

<p>එක් උදාහරණයක් මේ සඳහා සෑහේ. දීමනු, මානේස්,
මිනෝස් හා මෝසෙස් යන නාම සතරේම පොදු සංස්කෘත ආරම්භයක් ඇති බව කර්ස්ටන් කියයි.
(38 පිටුව) ඔහු පවසන අන්දමට දීමනු" යන්න දීඋතුම් මිනිසාදී", දීනීතිය දෙන්නා"
වැනි අරුත් ඇති සංස්කෘත වචනයකි. එහෙත් මෝසෙස් යන්න දී(කෙනෙකුගෙන්) උපත ලද"
නමැති තේරුමක් සහිත ඊජිපතු වචනයකි. මනු සහ මෝසෙස් යන දෙදෙනාම නීති වේදීන් වුවත්
ඔවුන්ගේ නම් අතර සමාන ශබ්ධයක් ඇත්තේ මිස සමාන අර්ථයක් කිසිසේත් නැත. වචන එක්
බසෙකින් තවත් බසකට බිඳී ඒම විස්තර කිරීමේදී අනුගමනය කළ යුතු මූලික සිද්ධාන්ත ඇත.
එනම්,</p>

<p>¬ ව්යාංජනාක්ෂර හා ඒවායේ ගණන ඒ ආකාරයෙන් තිබිය යුතුය.
සෙමිතික් භාෂාවක එම ගණන තුනකි. එය දෙකක් විය හැකි අවස්ථාද තිබේ.</p>

<p>¬ එම අක්ෂර පිහිටා ඇති ආකාරයද වෙනස් නොවිය යුතුය.</p>

<p>¬ වචනයක නිරුක්තිවීම සෙවිය යුත්තේ එකම ආකාරයේ භාෂාවන්
අතරය. එනම් හෙබ්රවේ වචනයක නිරුක්තියසෙවියයුත්තේ සෙමිතික් භාෂාවන් අතරේ මිස ඉන්දු
යුරෝපීය භාෂාවන් අතර නොවේ.</p>

<p>¬ මෙසේ කිරීමට නොහැකි අවස්ථාවල පමණක් වෙනත් ආකාරයක
භාෂාවන් වෙත යොමුවිය හැකිය. මෙසේ කිරීම අවසාන ප්රයත්නය පමණක්විය යුතුය.</p>

<p>මේ කිසිවක් නොදන්නා කර්ස්ටන්, මෝ.අබ්, නෙබෝ
කන්ද, පිෂ්ගා, බෙත්-ෂෙයෝර්, හෙෂ්බෝන් (පිටු 45-46) ආදී වචන
තේරුම් කරන්නේ අශාස්ත්රීය ලෙස දඹදිව ඇති ස්ථාන නාම ලෙසටය. මෙය පුදුමයකි.</p>

<p>ආචාර්ය හෙන්රි සිල්වා පියතුමා</p>

<p> </p>






</body></text></cesDoc>