<cesDoc id="sin-w-features-silumina-257" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-silumina-257.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>හින්දු
සංස්කෘතිය හඳුනා ගැනීම</p>

<p> </p>

<p>ශිව රාති්රය</p>

<p>පාලම</p>

<p> </p>

<p>මෙවර පාලම
විශේෂාංගයෙන් ඔබ වෙත ශිව රාති්රය නම් හින්දු ආගමික උත්සවය පිළිබඳ තොරතුරු ඉදිරිපත්
කෙරෙයි. මෙය හින්දු ධර්මය පිළිපදින ශිව භක්තිකයින් හට විශේෂිත වූ උපවාස දිනයකි. එසේම
විශාල ගණනක් උපවාස අතර ලොව පුරා හින්දු භක්තිකයින් මහත් භක්තියකින් පිළිපදින ප්රධානතම
උපවාසය මෙය වෙයි.</p>

<p> </p>

<p>ලොවම නිහඬ,
නිසල නිදන හෝරා කිහිපය රාති්රය  නම් වෙයි. එම
කාලයෙහි සියලු සත්වයෝ තම තමන්ගේ සියලු කටයුතුවලින් නිදහස්ව විවේක සුවයෙන් පසු වෙති.
එම කාලයේදී ලෝක පාලන ශිව දෙවියෝ ලෝ දනන් වෙත 
»vl ~l r£ X{§p»[ xrl U»n~£ ÷»x »z£{ ~¥ù~yÜ. W{p {« |{ »nýxp f
Y¯l[ªj ~zYp y£Ü²x |{ y£Ü²x pK »{õ. "y£l²" xp¨ "rnv" x.
"|{ y£l²" xp¨ |{ »nýxp r§np y£Ü²xõ. A Rp¨{ |{ y£Ü²x ~vyjx Ãú»v
Ryv¨j {p»p ~xû »z£ {¥~xp ~xû rh£{p»[p v¨à R£yY}£ »Y£f ~{ »nýxp £
AY£lòx þvx.</p>

<p>වාර්ෂිකව
නවම් මහේ අවපස සඳෙහි යෙදෙන ශිව රාති්රය ලොව පුරා හින්දු සිව (සෛව) බැතිමත්හු විසින්
මහත් අභිමානයෙන් යුක්තව ආගමානුකූලව සමරති.</p>

<p>මෙදින රැය
පහන් වන තුරා ශිව දේවාලයන් හි ශිව දෙවියන් පිදීමේ පූජාවන් ද, බජන් ගායනා, ආගමික හා
සංස්කෘතික වූ කලාංගයන් ද පැවැත්වීම සිරිතයි.</p>

<p>හින්දු
බැතිමතුන් පිළිපදින උපවාසයන් අතර වඩාත්ම වැදගත් උපවාසයද මෙයයි. මෙලොව සියල්ලම බිහි
වූයේ උත්පත්තිය ලැබූයේ 'ලිංග' මූලිකවය යන්න ශිව රාති්රයේදී අවධාරණය කෙරෙයි.</p>

<p>ශිව රාති්රය
හා එහි ආගමික හා ආධ්යාත්මික වැදගත්කම පිළිබඳ කියැවන සුවිශේෂ වූ ඓතිහාසික ග්රන්ථ
කිහිපයක් ද වෙයි. ඒ අතුරින් "ශිව මහා පුරාණය" "කන්දපුරාණම්"
"අරුනාචලපුරාණම්" ආදිය විශේෂ තැනක් හිමිකර ගනියි. මේ සියල්ලෙන්ම ශිව රාති්රය
පිළිබඳ කියවෙන්නේ එක් පොදු කථාන්තරයකි. එනම් මෙම දිනයේ දී ආගම දහමෙහි කියැවෙන
අයුරින් ශිව පූජාවක් පවත්වන්නේ  නම් සියලු
යහපත සිද්ධ වෙන අතරම එකී පුද්ගලයා ශිව දෙවියන් සරණ යන බව ය. මෙසේ හින්දු බැතිමතුන්
විසින් සිදු කරනු ලබන ශිව පුජාවෙන් හින්දු ආගමානුකූලව සිදු කරන්නාවූ 'අශ්වමේදම්' (සුදු
අශ්වයින් මුල් කරගෙන දිවා රාති්ර වශයෙන් දින ගණනාවක් සිදු කරනු ලබන ශාන්ති කර්මයකි) වලින්
අත්වන ප්රතිඵලවලට වඩා වැඩි යහපතක් සිදුවන බව ද කියවේ.</p>

<p>මේ නිසාම
හින්දු බැතිමතුන් පිළිපදින සෙසු උපවාස සියල්ලටම වඩා උතුම් හා පූජනීය වූ ශිව රාති උපවාසය
සැමරීමෙන් මෙළොවටත් එළොවටත් යහපතක්ම සිදුවන බවත් 
»v¤Y}xf v[ r¦»np t{l pã R£[v »rp{£ »nõ.</p>

<p>මෙම දිනයේ
දී ශිව දෙවොල්වල ජාමයන් හතරේදී ම ශිව දෙවියන් උදෙසා විශේෂ දේව මෙහෙයන් හා පූජාවන්
පැවැත්වෙයි. එදින රැය පහන් වන තුරු නිරාහාරව හිඳිමින් ශිව දෙවියන් හට පූජා උපහාර
පවත්වමින් නිදි වැරීමෙන් එසේ කරන්නාට මහඟු ඵල අත්වන බව ද කියැවෙයි.</p>

<p>ශිවරාති්රය
පිළිබඳ ශී්ර ල ශී්ර ආරුමුගනාවලර් "සිය සෛව විනා විඩයි" නම් (සෛව පැන -
පිළිතුරු) කෘතියෙහි දක්වා ඇත්තේ මසේය. "හිමිදිරි උදයේ අවදි වී දිය නා ශිව
දෙවොලට ගොස් ශිව දෙවියන් වැඳ, ශිව දෙවියන් උදෙසා වූ ස්තෝත්ර ගයමින් නිරාහාරව
උපවාසයෙහි යෙදී සිටිය යුතුය. දහවල අවසන සවස් යාමයේ නැවතත් දිය නා ශිව දෙවොලෙහි
ජාමයන් හතරෙහිම නිදි වරමින් පූජාවන් පැවැත්විය යුතුය. එසේ ශිව දෙවියන් හට පූජා
පැවැත්වීම අපහසු නම් ශිව දෙවොලට ගොස්  එහි
පැවැත්වෙන පූජා  R£nx áY tz£ [ëñp »p£ëà
~Ññp |{ »nýxp U»n~£ [xp ~»l¤l² [£xp£ Yyñp |{ »nýxp »v»p Yyñp Wv Y£zx
[l Y x¨lªx." xp¨»{ë. Sr¥÷Ú R£[ñY [²pm{z n |{ y£Ü²x rtq ~qp »vxv
»{õ.</p>

<p>ශිව
රාති්රයෙහි ගුණ වරුණ කියවෙන ඉතිහාස කථාන්තරයක ශිව රාති්ර දිනයේ නිදිවරා සිටිමින් ශිව
දෙවියන් සිහිකර පවත්වන උපවාසයේ ආනිසංස පිළිබඳ 
»v»~ ~qp »{õ. "Wnpf |{ y£Ü²x »xn R¥Ü t{ »p£vnl {p»x ~l{
nhx»K [x {¥nnYª y£Ü²x Wôvl ~v[v {p ~{§r£{§p»[p lv Ìýlxf £ëxY »{l¥õ
~l£ R~ {« [~Yf p¥[[l t{l, [»~ S RllY {£Õ þ ~Ñ X¨ X¨fv »p£áì ëpn
x¦»vp Wv [~p {¥Ò ñx x£ýx xp ï»xp v ÷x rp {p lªy£ Wv [»~ »Y£ WÃ»pY Yh£
ïv nvñp ~Ñ t{l, y£Ü²x rp þ»vl ~v[v X¨f |{ »nýxp»[ R£|M{£nx z¥ð»vp
»v¤Y}xf [x t{l Ãx »N. |{ y£Ü²x t{ »p£áp ~Ñ {¥nnYª Wnp ÷x r§y£ ëy££y{,
ën {MËl{ ~Ò»vp »v£Y ~¨{x z¥t¨»N pK A rtq áp¨{l{ Wv Yfx¨l»l ëx¥z»vp
vl R£ë~¹~ Rl{p t{ »võp Ãx»{õ.</p>

<p>තිස්තුන්
කෝටියක් දෙවිවරුන් පිළිබඳ කියැවෙන ආගමක් වශයෙන් හින්දු ආගම දෙවියන් පිළිබඳ සංකල්පය
මත පදනම් වී ඇත. එසේම සකල විද දෙවිවරුන් අතර මූලික හා ප්රධානම දෙවියන් ලෙස හින්දු ආගම
විසින් පෙන්වා දෙන්නේ ශිව දෙවියන් ය. මේ නිසාම සෙසු සියන්ථ දෙවිවරුන් වෙනුවෙන්
පවත්වන්නාවූ පුද පූජාවලට වඩා අතිශයින්ම වැදගත් පූජාවක් හා උපවාසයක් වශයෙන් හින්දු
ආගම "ශිව රාති්රය" හා බැඳුණු පූජාවන් හා උපවාසය පෙන්වා දෙයි.</p>

<p>රවී
රත්නවේල්</p>






</body></text></cesDoc>