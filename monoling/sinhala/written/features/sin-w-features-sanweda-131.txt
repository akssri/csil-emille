<cesDoc id="sin-w-features-sanweda-131" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-features-sanweda-131.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>ආදරයේ ජීව රසායන විද්යාව</p>

<p>W;a;Í;r" mru msú;=re wdorh' Ôú;h ñysß isyskhla lrk
wdorh' ke; f,dfõ wka ri÷kd wdorh fia iqjfokd' fï fkdfhl=;a wdldrfha jr
ke.Sïj,ska hq;a ienE wdorh hkqfjka hula b;sßù weoao@ tfia kï th l=ulao@
úoHd{hska f.a u;h wdorh hkak ìys ù we;af;a mßKduh" ðj úoHdj yd ridhk úoHdj
u;hs' Tjqka fmkajd fok wkaoug wdorh uq,skau ìysjqfKa óg wjqreÿ ñ,shk 04lg tmsg§
wm%sldfõ ;eks;,dj, h' ñksia j¾.hdf.a uq,au hq.hkays§ Tjqkaf.a ysfiys isg uq,au
iakdhql ridhksl Odrdj" reêrhg .,d wdfõ fï hq.fha§' fï yeÕSï Odrdj m%o¾Ykh
jkakg mgka .;af;a uq,skau foll=,a Tijd ñksid weúÈkakg" fõ.fhka Èj hkakg
mqreÿ jQ miqjhs' ,sxf.kaøshhka iïmQ¾Kfhka osiajkakg jqKd' iEu mqoa.,fhl=g u
úfYaIs; jQ is;a weo.kakd iq¿ iajNdjhla ;snqKd' ta .;s ,CIK yd YÍr ,CIK
úysfokakg mgka .ekSu;a iu.u wdorh lsÍfï w¨;a l%u ìysjkakg mgka .;a;d' uqyqKg
uqyqK ,d i;=gqúu fndfyda i;=ka yd ñksiqka w;r m%p,s; jkakg mgka .;a;d' fmkqu iy
fm!oa.,sl wdl¾IKh fï p¾hd;aul iQ;%fha fY%aIaG;u fldgi njg m;a jqKd'</p>

<p>ස්ත්රීන් සහ පුරුෂයින් දිගු කාලීන සහකරුවන් බවට පත් කිරීම සඳහා වූ
ආකර්ෂණය පරිණාමය වෙන්නට බල පෑ කරුණක් වුණේ රොමැන්ටික් හැගීම් මාලාවක්. එය දරුවන්
ලබා ගැනීමේ ක්රියාවලියට බෙහෙවින් අවශ්ය වුණා. ආදිම ස්ත්රීන් හා පුරුෂයින් දිගු කලක්
තිස්සේ එකතුව සිටියේ එක් දරුවකු බිලිඳු වියේ පටන් හදා වඩා ගැනිම පිණිස. ඉක්බිති
දෙදෙනාම අලුත් සහකරුවන් සොයා යන්නට වුණා. ඒ අනුව ආදිතම මිනිසා රොමැන්ටික් තත්වයකට
පත් වුණේ හුදෙක් දරුවකු වෙනුවෙන් බව හෙලන්න් ිෂර් සිය .ඇනටොන්මි ඔ ලව්. ග්රන්ථයේ කියා
සිටිනවා.</p>

<p>ස්වභාව ධර්මයේ සැලසුම අනුව රොමැන්ටික් ආදරය සදාකාලික එකක් නෙවෙයි.
එසේම සුවිශේෂ දෙයකුත් නෙවෙයි. සාමාන්යයෙන් ආදිතම සමාජවල පටන් මිනිසාගේ රටාව වුණේ
රහසිගත පරදාර සේවනයත් සමග ම ප්රසිද්ධියේ ඒක භාර්යා, ඒක පුරුෂ සේවනය. හෙලන්න්
ිෂර් පෙන්වා දෙන අන්දමට පූර්ව ඓතිහාසික ස්ත්රීන් තමන්ගේ සැමියා සමග පවත්වන ප්රේමයට
අමතරව රහසේ පවත්වා ගෙන යන ප්රේමය පවත්නා තාක් ඔවුන්ට අතිරේක සම්පත් ලබා ගත හැකි
වුන බව.</p>

<p>සුප්රකට හොලිවුඩ් නිලියක් වූ ගී්රටා ගාර්බෝ වරක් මෙල්වින් ඩග්ලස්ට යැවූ
ලිපියක: ..ආදරය කියන්නේ සාමාන්ය ජීව විද්යාත්මක එහෙම නැත්නම් රසායනික යැයි කියමු. ඔව්.
රසායනික ක්රියාවලියක්. මේ ගැන විකාර කතා හරියට ලියවිලා තියෙනවා... </p>

<p>පමේවතුන් බොහෝ විට මෙය පිළිගන්නට කැමති නැහැ. ආදරය යනු ශරීරයේ
රසායනික හා හොර්මෝන ක්රියාකාරිත්වයක් නිසා හටගන්නා තත්වයක්. පර්යේෂණ මගින් පෙන්වා දී
ඇති අන්දමට දෑසට දෑසක් මුණ ගැසුන විට, අතක් ස්පර්ශ කළ විට, විලවුන් හෝ
දහදිය සුවඳක් දීනුන විට මොළයේ ස්නායූන් ක්රියාශීලි වන්නට පටන් ගන්නවා. එය ස්නායු
හා රුධිරය හා ගමන් කරන විට එහි ප්රතිඵලය වන්නේ රත් පැහැ ගත් සම, හුස්ම ගැනීම
වේගවත් වීම, අත්ල දහදියෙන් තෙමීම, දෑස් අඩවන් වීම ආදිය. ඊට හේතුව ඉතා සරල
එකක්. රසායනික ගමන් මාර්ග හේතුකොට ගෙනයි, ඒ වෙනස් වීම් ඇති වන්නේ. මේ සියල්ලටම
වඩා ආදරයෙන් බැඳීමේ චමත්කාර ජනක හැඟීම් ධාරාවක් ඇතිවෙනවා. එය එතරම් විශ්මය ජනක ප්රති
ක්රියාවක් නොවේ. මේ ප්රහාරයට ලක්වී සිටින ද්රව්යය ඇම්පිටැමයින් විශේෂයක්. ඊට
ඩොපැමයින්, නොරිපිනොප්රින් සහ පීනයිල්තිලැමයින් (ඡෑ්) අඩංගුයි. තරුණියක්
දුටු තරුණයෙක්: ..ඔයා දීකපු ගමන් මට පිස්සු ඩබල් වුණා.. යනුවෙන් පවසන කියුම හට
ගන්නේ ඒ තරුණිය දුටු සැනින් තරුණයාගේ මොළයේ හට ගන්නා පීනයිල්තිලැමයින්
හෙවත් ඡෑ් ධාරාව නිසා. ඒ බැල්ම විසින් මොළයේ ඡෑ් ගබඩාවට සංඥාවක් ලැබෙනවා.
එසැනින් ඡෑ් ධාරාව ක්රියාත්මක වෙනවා. නමුත් මේ සරාගී, රෙමැන්ටික් ආදරය දිගු
කලක් පවතින්නේ නැහැ. ඕනෑම ඇම්පිටැමයින් විශේෂයක් ඇතිවූ සැනින් මේ ඡෑ් ඇතිවීම
ඉවසා ගත හැකි තත්වයක් ශරීරයේ හට ගන්නවා. ඒ අනුව ආදර බැල්මකින් හදවතට වදින මල්සර
හීය තවත් තීව්ර කරන ද්රව්යයක් නිෂ්පාදනය වෙනවා. අවුරුදු තුන හතරකින් පසු ශරීරයට අවශ්ය තරම්
ඡෑ් නිපදවීමේ හැකියාව නැතිව යනවා. මෙසේ අඩු වී ගෙන යන රසායනික ද්රව්ය නිසා ආදරයෙන්
කෙනෙකු උමතු වීමේ කාලය කෙළවර වෙනවා.</p>

<p>නමුත් තවමත් ඇතැම් රොමැන්ටික් ප්රේම සම්බන්ධතා මුල් අවුරුද්දෙන්
පසුවත් දිගින් දිගටම ගලා යනවා. එවිට දිය හැකි තේරුම කුමක්ද? මේ ගැන විද්යාඥයින්
කියන්නේ තවත් රසායනික කට්ටලයක් ක්රියාත්මක වන බවයි. මොළයේ එන්ඩොපීන් දිගටම
පවත්නා අතර එහි නිෂ්පාදන වේගය ඉහළ යන්නට පටන් ගන්නවා. මෙය ස්වාභාවික වේදනා
නාශකයක්. එමගින් පමේවතුන්ට සාමය, ආරක්ෂාව හා සෞම්ය හැඟීම් ලබා දෙනවා.</p>

<p>අප ඉහත සඳහන් කළ පීනයිල්තිලැමයින් හෙවත් ඡෑ් හා අනිකුත්
ඇම්පිටැමයින් වැනි රසායනික අතර වෙනසක් පර්යේෂණ මගින් හෙලිවී තිබෙනවා. මේ ඡෑ් පමේ
සිත් මෝහනයෙන් මත් කරවනවා. වඩාත් යාවජීව ආදරයක් ඇති කරන්නේ එන්ඩොපීන් මගින්.
එය දිගටම පවත්වා ගෙන යන්නෙත් එමගින්. ප්රථම ප්රේමය මල් ඵල ගන්නේ ඔබ ආදරය කරන විට.
අනික ඔබට ආදරය කරන ආකාරය අනුව. පරිණත පේ්රමය යනු යමෙක් ඔබ වටහා ගෙන කරන ආදරයයි.
සරාගී ආදරය හා රොමැන්ටික් ආදරය අතර වෙනස ඒ අනුව වටහා ගත හැක. ආදරය හා සම්බන්ධ
තවත් රසායනික ද්රව්යයක් වන්නේ ඔක්ෂිටොසින්. මොළය මගින් නිපදවන අතර එයින් ස්නායු
සංවේදීකරණය කරන අතර මාංශ පේශී හැකිලවීමට අවශ්ය උත්තේජනය ලබා දෙනවා. වියපත් ස්ත්රීන්
හා පිරිමින් අතර .පසුවියේ. ප්රේමයක් ඇති වීමට මේ රසායනිකය ඉවහල් වන බව විද්යාඥයින්
පවසනවා. විවිධ ප්රතිඵල ලබා දෙන මේ එන්සයිමය සුරතාන්තය</p>

<p> j¾Okh lsÍugo bjy,a úh yels
nj fmkajd § ;sfnkjd' ,sx.sl l%shdj,sfha§ iqr;dka;h lrd ,Õ ùfï§ TCIsfgdaka
ksmehqu ;=ka .=Khl isg mia .=Khla olajd by  fhduq fkdfj;ehs lsj yelafla ldgo' úúO
l%shdldß;ajfhka hq;a ridhksl iqr;dka;h by  ;=ka .=Khl isg mia .=Khla olajd j¾Okh jk nj
fy</p>

<p>සමලිංගිකතාවය වනාහි කලලය වර්ධනය වීමේ දී හට ගන්නා ජීව රසායනික විෂමතාවයක්
නිසා ඇති වන්නක් බව ඇතැම් පර්යේෂකයින් පෙන්වා දෙති. නමුත් එයින් සමලිංගිකයින් අතර
ආදරයක් ඇති වීම වැලකෙන්නේ නැත. සරාගී සහ සානුකම්පික හැඟීම් ඇති වීම රසායනික මගින්
පහදා දිය හැක. නමුත් එක් සහකරුවකු තවත් එක් සහකරුවකුට පමණක් ප්රේම කරන්නේ ඇයිද යන
ප්රශ්නය නැවතත් පැන නගී. නැවත වරක් එය ජීව විද්යාවේ සහ පරිණාම ක්රියාවලියේ සකාර්යය
කොටසක් බව හෙලිවේ. පිරිමි තම සහ කාරියන්ගෙන් උපරිම 
idm,H ;djla n,dfmdfrd;a;= fj;s' jhi wjqreÿ 17 isg 18 olajd jQ ld,h
;=  ia;%Ska orejka ìys lsÍfï ,d
±ä  Yla;shla m,lrhs' fï jhfia ia;%Skag
msßñka ;=</p>

<p>ස්ත්රීන් ආදරය කරන්නට පටන්ගන්නේ සෙමෙන්ය. හේතුව ඔවුන්ගේ අවශ්යතා
බෙහෙවින් සංකීර්ණ වීමයි. තම සහකරුවා කවුරුන්ද යන්න පිළිබඳව පරීක්ෂා කිරීම සඳහා ඔවුන්ට
කාලය අවශ්ය වේ. පුරුෂයා වියපත් පුද්ගලයෙකු වූවාට ගැටන්ථවක් නැත. අවශ්ය වන්නේ ඔහු
ස්ත්රියට ආරක්ෂාව ලබාදීමට, දරුවන්ට යහපත් පියෙකු වීමට සහ සම්පත් බෙදා හදා ගැනීමට
හැකියාවක් ඇද්ද යන්නයි. </p>

<p>පිරිමියෙක් හෝ ගැහැණියක් එකවර
දෙදෙනකුට ආදාරය කරයි යන්න ඇතැම් ලිංගික විද්යාඥයින් පිළිගන්නේ නැත. සෑම
පුද්ගලයෙක්ම  තම පරමාදර්ශී සහකරුවා හෝ
සහකාරිය පිළිබඳ සිතියමකින් යුත් මනසක් ඇති අය බැව් මේ විද්යාඥයින් පෙන්වා දෙති.
ඇසුරුකරණ පුද්ගලයින් සහ බාලවියේ අත් දීකීම්වලින් ඔවුන්ගේ මේ සිතියම සකස් වේ.
බාලවියේ පටන් අපේ මව්වරු සහ පියවරුන් කියා දී ඇති ඇතැම් ශරීර ලක්ෂණ වියපත් වූ පසු සහකරුවකු
හෝ සහකාරියක සොයා යෑමේදී ඉස්මතුවේ.</p>






</body></text></cesDoc>