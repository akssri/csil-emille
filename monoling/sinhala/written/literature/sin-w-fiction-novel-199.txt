<cesDoc id="sin-w-fiction-novel-199" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-fiction-novel-199.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>බෝදිලිමාගේ සාපය</p>

<p> </p>

<p>භද්රජී
මහින්ද ජයතිලක</p>

<p>ලියූ</p>

<p>භයානක
රසය දනවන  නවකතා ති්රත්වයේ දෙවැන්න</p>

<p>1</p>

<p>හබරවැව හන්දියේ අන්තිම බස්රියෙන්  බැස්ස පසු මා  ලීලාවතී මැණිකේගේ නවාතැන් පළ වෙත සැතපුම් තුන ගමන් ගත යුත්තේ
පයිනි. හන්දියේ කඩ කීපයේත්, බැංකුවේත් 
úÿ,s t 
ud hk uÛ f;dfÜ  úÿ,s myka fkdue;'</p>

<p>මුල්ම 
i;sfha ud nexl=fõ  /lshdj lf  f.or
ojila .; fldg" wïud Whd fok n;a fõ,la 
ri lrñka  ld" yjia jrejl
reúkS oel .kakg hEu .ek uu isysk ueõfjñ' uq,a i;sfha isl=rdod iji jev wjika
l 
yïnq  fjÉÑ  tfla 
wudrefjka  ÿla  ú|f.k 
yß Tfydu ÿr  n%dkaÉ  tll 
wjqreoaola  jev  lr, 
udrej  yod.kak" wfka ~~hehs
reúkS ug lSjdh' ~~uq,skau yïn Wk riaidj' tmd lshkako@ lS fofkla fï riaidjg j,s
lkjo@~~ wE lS fkdfhl=;a joka ;ju;a ud is; fyd,auka lf  ú|f.k yß ynrjej wjqreoaola .; lrkjd hehs uu
bgd .;sñ'</p>

<p>හබරවැව කුඩා ගමකි. නුවර බදුලූ පාරේ විශ්ව
විද්යාල භූමිය පසුකොට සැතපුම් විස්සක් තරම දුර යද්දී හබරවැවට යන අතුරුපාර  වමතට මුණ ගැසේ. වරින් වර මහවැලියේ අතුගඟක
රළ රඟනු හබරවැව පාරේ ගමන්  කරන්නෙකුට  පෙනෙන්නේය.</p>

<p>අවසන් බස් රථය අඳුර කපා ගෙන කර් වංගු
නඟිමින් බසිමින් හබරවැව හන්දියට  එන්නේ රාති්ර
දහයට පමණය. මේ රැයේ  යම් යම් ප්රමාද දෝෂයන්
නිසා මා බසයෙන් බසින විට රාති්ර දහයමාරට පමණ විය.</p>

<p>හන්දිය සියල්ල නිදා ගත්තාක් වැන්න. දිනසිරි
හෝටලයේ  දොර පන්ථ දෙකක් පමණක් ඇර
තිබිණි.  ඒ දෙසින් යන්තමට ගුවන් විදුලියක
චුරු චුරුවක්  ඇසිණි. පාරෙහි තැන තැන අලස
ලෙස වැතිර ගත් බල්ලෝ හාමතෙන්දෝ මා ගැන තැකීමක් නොකරති. බස්රියෙන් බැස්ස කීප
දෙනා කොහේ ගොස්  සැඟවුණාදැයි නැත. බස්
රියැදුරාද, කොන්දොස්තර ද හෝටලයෙන් 
f;a tlla î fmr 
uu ;ksju ,S,dj;S ueKsflaf.a  f.or
w;g weúo tkakg jqfKñ'</p>

<p> rd;s% wyfia kshfmd;a;la jeks i|ls' tfyka fufyka  j,dl=f  legj, o kshfmd;= if|ys o wdf,dalhla we;akï
th wyfiau k;r jqkd fiah' uÛ fomiu Wi .iaje,ah' fhdaO w÷re wj;drh'</p>

<p>මම ගමන් මල්ලෙන් ගත් විදුලි පන්දම දල්වා
ගතිමි. මගේ සිතෙහි  එතරම් බියක් නැත.
රාති්රයෙහි  තනිව ගමන් කොට මට නොපුරුදුවා
නොවේ. පාසලෙන් අස්වූ පසු මම නාට්යවල රඟපෑමට ගියෙමි. බොහෝ රාති්රයක නාට්ය
රංගනයක්  අවසන් වී මා ගෙදර එන්නේ රෑ දෙගොඩ
හරියේය. සමහර රාති්රයක නුගේගොඩින් අන්තිම බස්රියකින් බැස්ස පසු මිරිහානට ඉතිරි දුර මා
ගමන් ගෙන ඇත්තේ තනිවම පයිනි. ඉතා 
blaukska wä ;nk ug isf;a ìhla we;s fkdjkafka o fkdfõ' wE; hfula weúo hkq
olsk ug ta fyd,auklaoehs isf;a' tfy;a u| fõ,djlska ta pls;h is£ hkafkah'</p>

<p> fï rd;s%fha o ud ;=  kï fkdùh' uf.a ju;g mdfrka  my; nEjqfï 
,÷ le,E w;ßka jßka jr fmkqfKa .,d nisk .x.dfõ Èh r  wdldrhhs' fumsáka  keÛ=Kq fhdaO WK m¾rej, n| ßls,s iq</p>

<p>මෙසේ මඳ දුරක් ආ මට ගස් අතු  අතරින් අමුතු 
.|la  keÛ tkq oekqKs'  fï .| ud 
óg  fmro ú| we;af;ñ'  we;efula 
th  y÷kajkafka fmf÷; .|
hkqfjks'  u  hehs we;eïyq lsh;s' ud ;¾l lf</p>

<p>තවත් මඳක් තැන් යද්දී මේ ගඳ අතුරුදන් විය.
මම විදුලි  පන්දමේ  එළිය 
.iaje,aj,g o  t,a, lf  jjqf,la ig ig .d mshdm;a i,d
by</p>

<p>මඳක් දුර ආ මට දකුණු පස කන්ද උඩින්
ගැඹුරු  කට හඬක්  ඇසෙන්නට විය.</p>

<p>''මා ........'' කියමින් නැඟුණු
අඳෝනා  හඬක්  බඳු  ඒ හඬ මා ගත හිරිගඩු
නංවමින් හද කීරි ගැස්විය. මම සිටිතැනම නතර වුණෙමි.</p>

<p>''මා...... විල වෙලේ මානා කොක්  රඟන්නේ...... '' සුළගේ ඇදී ආවේ කන්ද උඩ
අතු පැලක තනිවූ ගැමියකුගේ  කට හඬයි.</p>

<p>මමද ඒ අනුව හඬ නඟා ගී හඬක් නැඟුවෙමි.
මෙයින්  පසු ගමන පහසු විය. මම  මඟක් 
ÿr isjqrejï lrñka  wdfjñ'  .eñhdf.a lú yË wE; w÷f¾ ;=kS ù keiS .sfhah'</p>

<p>ගස් අතු සොලවමින් සුළඟ නඟන  කෙඳිරිය පමණි මා  අවට වූයේ. සීත මීදුමක් කෙමෙන් 
mßirh  jid .;s'  tlajku ud bÈßfhka u l=vd w÷re  i;l= ÿj hkakdla fiah ug fmkqfKa' fudfyd;lg
uu ;s.eiaiqfKñ' b;a;Ejl=" fyda;Uqjl= fyda W!re óhl= úh hq;= hehs is;
yod.;a uu wäh blauka lf  ud w;jQ úÿ,s  mkaou w;ska .s,syS ju;g jQ m,a,ug jegqKs' .Û
w;g  fmr  th fudfyd;lg t  oyrla úysÿjd  ksù .sfhah' th jegqK ;ek fmkqK o oeka me;sreK w÷frka ug
yßyeá  ;ekla  fidhd .;  fkdyelsúh' nE.h
lf¾ ,d .;a .uka ìug my;a jqKq uu u|la 
m,a,ug hñka w÷f¾u ;K m÷re w;.dkakg jqfKñ' f;;ukfhka hq;= Wia  ;K m÷re ud 
;= 
msks fmdo ud w;a, .efgoa§ " ud w;eÛs,s jeÿk iS;, úÿ,s mkaou ;j;a
my  úÿ,s mkaou mK we;s oÛldrhl= fia 
i,ld  thg l:d  lrñka uu ;j u|la  my;g  weúo hkakg  wäh ;enqjd muKhs' mh  ;enQ ;ek ìu isÿrla  jQjd fia mh  mels  my</p>

<p>මෙවර මම උමතු වූවෙකු මෙන් අඳුරු තෙත් තණ
බිම අතපත ගාන්නට වුණෙමි. විදුලි පන්දම කොතැනද? එය ඇති තැනක  තිබෙන්නට හැර අඳුරේම නවාතැනට ගොස් හෙට දින හිරු
පායා ආ පසු ඇවිත් එය සොයා ගන්නට ද නොසිතුණා නොවේ. මේ මොහොතේ මා අතැඟිලිවල
යම්කිසි දියාරු සෙවල දෙයක් ගැටිණි. ජුගුප්සාවෙන් 
weo.;a w; uu isU neÆfjñ' thska keÛ=Kq 
jiqre .| ud ;=  kxjk
iqÆúh'</p>

<p>''චීඃ'' කියු මම වෙන තැනක තණ ගොල්ලේ අත
පිස දැමුවෙමි.  වාසනාවට මෙවර මගේ අතට
නොසිතාම, නොදැනීම විදුලි පන්දමේ යකඩ 
iS;,h oekqKs'  th w;g  .;a;o oeka tys  wdf,dalhla kï fkdùh'  w÷r
w;ßka ~~Ö# Ö# Ñ#~~ lshk uQisl yËla weisKs'</p>

<p>බිය 
ueË.;a  uu úÿ,s mkaou;a w;ska f.k
) wdf,dalh fkdue;s úÿ,s mkaou ;ju;a  ÿ.|
yuk w;ska f.k ) mdrg f.dv jqfkñ' kshfmd;a;la jeks i| wyig uqÿkaj n,d isáfhah'
uf.a uÛ t</p>

<p>මඳක් දුර ගමන් ගත් මම මගේ වමතින්වූ බෑවුම
දෙස බැලූවෙමි. මීදුම කැටිවෙන ගස්වැල්  අතරින් ගඟ
දිය යන්තමට  පෙනිණි. එයින්  මෙපිට මා නෙත ගැටුණේ  දිලිසෙන විශාල දෑසකි. ඒ දෑස  මිනිස් 
oEila  kï fkdùh' fnda;,  wä fuka 
rjqï jQ fld</p>

<p>උණ හපුලූවෙක් යැයි  සිත හදාගත් 
ud b;sß  uÛ wdfõ ÿjk  fomhsks' </p>

<p>මා ලීලාවතී මැණිකේගේ  ගෙදරට 
  Ññks
,dïmqj oe,a fjñka ;snqKs'</p>

<p> </p>

<p> </p>

<p>2 </p>

<p>''අපි මේ බලන් හිටියා අයියා එනකල්''
ඉස්තෝප්පුවේ අඩ අඳුරේ බලා සිටි 
wurisß  nxvdrf.a lg yË uu
ye¢kafkñ'</p>

<p>දහනව හැවිරිදි අමරසිරි බංඩාර, ලීලාවතී
මැණිකේගේ එකම පුතායි. ඔහු ළඟම බලා සිටියේ 
keÛKsh ) tfldf</p>

<p>''අනේ 
cklhsh tkl,a ux nfha  ysáhd~~ wE
lSjdh' ~~ug yßhg  lkak;a  neßjqKd'~~</p>

<p>''ඔන්න අම්මා බත් පංගුවකුත් බෙදලා වහලා
තිබ්බා.'' අමරසිරි බණ්ඩාර හිඳ සිටි කෙටි බිත්තියෙන් 
ñÿ,g mek ud w;jQ u,a, Wÿrd .;af;ah' ~~fudloao whsfh fï .|@~~</p>

<p>''මඟදී එද්දී හරි වැඩක් උනා මල්ලියෙ.
ඔක්කොටම ඉස්සෙල්ලා අතපය සෝද ගන්න ඕනෑ'' කියමින් මම  ඉස්තෝප්පුවට ගොඩ වුණෙමි.</p>

<p>''මොකද්ද උනේ කියන්නකො ජනකයියා?'' මා
ළඟට ආ නයනා ''හැබෑට ගඳ නං තමා උරුලෑවෙක් වගේ''යි කීවාය.</p>

<p> ~~uf.a wf;a ;snqK fgdaÉ tl jegqKd' tal wyq,kak nEjqfï ;K f.d,af,
w;.Ejd' fudllao crdjla wf;a .EjqKd' oeka .|hs'~~</p>

<p>''ඉතිං ටෝච් එක හම්බ උනාද අයියා ?'' </p>

<p>''ඒක හම්බ උනා. මේං. දැන් ඒක පත්තු
වෙන්නෙත්  නෑ.''</p>

<p>''කෝ දෙන්න මට බලන්න'' අමරසිරි බංඩාර
විදුලි පන්දම ගනිද්දී අපේ කතා බහ අසමින් ගෙතුලේ සිටි ලීලාවතී මැණිකේත්,  ඇගේ ලොකු දුවණිය වජිරාත් ඉස්තෝප්පුවට ආහ.</p>

<p>විසිතුන් හැවිරිදි වජිරා පසුගිය සතිය තුළ
මා දුටුවේ එතරම් කතා නොකරන විළියෙන් පිරි 
;reKshl f,isKs'  tfy;a  wo rd;s% ud uqyqKmE w;aoelSu weiQ wE fmr
lsis Èfkl fkdjQ ú,i fodvuÆ jqKdh' ~~cklg ;sfhkafk fld  hkak'  t;fldg ? wdmyq tkfldg  tafl 
t;Els~~ wE fhdackd l</p>

<p>''ඒකට ඉතිං බයිසිකල් එකක් ගන්න එපෑ නේද
අයියෙ?'' නයනා තර්කයක් ගෙනාවාය.</p>

<p>''කොයි හරියෙ ද ජනක පුතේ ඕක උණේ?''</p>

<p>ලීලාවතී මැණිකේ ඉස්තෝප්පුවේ අත්
පුටුවකට බර දුන්නාය.</p>

<p>''වංගුව පහුකළාට පස්සෙ දකුණු අත
පැත්තෙ කන්දක්  තියෙන්නෙ, ඕං එතැන්දි.
මට එතන උඩහ පැලකින් කවුදෝ කවියක් 
lshkj;a weyqKd"~~ ud ta lS miq 
fudfyd;lg yeu ;ek jqfha 
ksyeähdjls'</p>

<p>''ඔය කියන තැන කොයින් ද පැලක්?''
ලීලාවතී මැණිකේ  හඬ බර කොට කීවාය.
''කලකට ඉස්සර නම් බක්කා  ඔතන පැල්  රැක්කා. අවුරුදු  ගාණකට ඉස්සර. ඒ පැල ගිනි ගත්තා නෙව...''</p>

<p>''එහෙනං අයියට තැන වැරදිලා, ඊට එහා
වෙන්න ඇති''  අමරසිරි බංඩාර හේතුවක්
සෙවීය.</p>

<p>''බක්කගෙ අවතාරයක්  තියෙනව කියනවනෙ'' වජිරා දෑත ළමැද හරහා බැඳ
අත්තල වලින් උඩු අත්ගොබ පිරිමැද ගත්තාය. ''මේන් බලන්න මගේ ඇඟේ හිරි ගඩු
පිපුණා.''</p>

<p>''කවුද බක්කා කියන්නෙ?'' මම විමසිලිමත්
වුනෙමි.</p>

<p>''බක්කා කියන්නෙ මේ ගමේ හිටි මහ විසාල
මිනිහෙක්.'' අමරසිරි බංඩාර කතාව පටන් ගනිත්ම ''සේරටම ඉස්සරවෙලා අතපය සෝදා ගන්න්න
ඕනෑ'' යි කී මම ඇතුන්ථ ගෙට ගියෙමි.</p>

<p>ලීලාවතී මැණිකේ ද, වජිරා ද මා අනුව
ආහ. අම්මාගේ ඇණවුමෙන් වජිරා කුස්සියේ වූ වතුර බාල්දියක් පිටුපස පිලේ වූ බේසමට
වත් කළාය. ඈ මට සබන් කැටයක් ද, තුවායක් ද ගෙනවුත් දී ''අයියා ඉතින් බය
වෙන්න ඇති නේදැ? ''යි  ඇසුවාය.</p>

<p> fuf;la ug ckl hehs weue;+ wE ug whshd hehs lSfõ wÆ; Wmka
ióm;ajhlska fkdfõo@ wef.a oEfia o ta j. lsheúKs' ud wo rd;s%fha uqyqK ÿka ìhlre
isÿùu ksid  wE ud y÷kd ug ióm jQjd fiah'</p>

<p>''මට ඒ තරං බයක් දැනුනෙ නෑ නංගී''</p>

<p>''අයියා රෑ පානෙ තනියමේම  එන එක ගැන මං  හිතුවා. අප්පච්චිට අමාරු වචේච වෙලාවක මටයි, මල්ලිටයි සැරයක්
දුවන්න උනා හන්දියට. අපි දෙන්නා එක පිම්මේ 
ÿj, .sfha'~~ fufia lS wE nd,aÈfhka fcda.a.=jlg msrjQ j;=r uf.a oE;g j;a
l</p>

<p>''අනේ, අත් දෙක ගඳ නම් තමා. අත් දෙක
සෝදාගෙන ඉන්නකො, බේසමට දාන්ඩ කලින්. කහළ ගොඩේ ලැගල ඇවිල්ලා.'' ඈ දැන්
කතා කළේ මවක දරුවකුට කතා කරන දයා කරුණාවකිනි. </p>

<p>මෙයින් පසු  මම මුහුණ සෝදන්නට  වුනෙමි.
බේසමෙන් හිස ඔසවා බලන විට වජිරා  පෙනෙන අසලක
නොවීය. ඈ පිලේ  මිටි බංකුව මත තබා ගිය
තුවායෙන් මුහුණ පිසදාගත් මම කෑම මේසය වෙතට ගියෙමි. ඇතුන්ථ කාමරයක අක්ක නඟෝ පහත්
හඬින්  කතා කරනු ඇසිණි.  ලීලාවතී මැණිකේ  මගේ බත් පිඟාන හැර දුන්නාය. බතුත් පොළොස් ඇඹුලත්, පරිප්පු
ටිකත්, මගේ කුසගින්න අලූත් කළේය. මා කෑම අරඹත්ම, ''පුතා රෑ එනකොට
පරිස්සමෙන්'' කී ලීලාවතී මැණිකේ ද මා තනිකොට ඉවත්ව ගියාය.</p>

<p>මා මඳක් බියවූ බව සැබෑය. එහෙත්
දැන්නම්  මා සිතට කිසිදු බියක් නොවීය. මම
හිතේ හැටියට කැකුන්ථ බතුත්, වෑංජනත් කෑවෙමි. 
fjkodg ud ? lEu lkafka  wurisß
nxvdr iu.hs' wo Tyq;a fmfkk wy,l fkdùh' ? jQ ksid hehs uu  is;=fjñ'</p>

<p>ඔවුන් නිදාගත්නා වේලාවත් පසුවෙලාය. ඔවුන්
මෙතෙක්  අවදිව සිටියේ මා එනතුරුය. දැන් හැම
දෙනාටම සැනසිල්ලේ නිදාගත හැකි නොවේ දැයි මට සිතිණි. ඇතුන්ථ කාමරයේ කෙල්ලන්
දෙදෙනා  දැන් නිහඬය.  මුන්ථතැන්  f.hska wefikafka ,S,dj;S ueKsfla 
tys wiamia  lrk yËh'  ud ld wyjrù w; fidaod f.k wd,skaoh w;g  tkúg 
id,fha ol=K;g jQ  ldurfhka wurisß
nxvdr msg;g wdfõh'</p>

<p>''මට නිදාගන්න බෑ අයියා.... අර කතාවට බය
හිතෙනවා.''</p>

<p>''මොන කතාවද?''</p>

<p>මා අනුව ඔහු ඉස්තෝප්පුවට ආ හෙයින් මම
ද මගේ කාමරයට නොයා නැවතුණෙමි. මා නිදා ගත්තේ ඉදිරි කාමරයේයි.  එහි දොර හැරුණේ ඉස්තෝප්පුවටයි.  වෙනදා නම් රාති්ර කෑමෙන් පසු  අමරසිරි බංඩාර මඳ වේලාවක් කතා බහක  පැටලෙන්නේ 
uf.a ldurhg ú;a we| fl  Tyq biaf;damamqfjka 
f.;=  nh ys;=K~~ lshñks' ~~whshd okafk kEfk ta
ñksyf. me, .sks .;a;d' B 
jeá,d .Ûg  mekakd' Th fiaru jqfka
nlalf. .Eks </p>

<p>''ඒ කොච්චර කල්ද?''</p>

<p>''දැන් අවුරුදු ගාණක් උනා අයියෙ. මම පොඩි
දවස්වල. බක්කගෙ ගෑනි සිසිලින් මැරුණට පස්සෙත් මං හරි හරි කතා ඇහුවා.''</p>

<p>හදිසියේම දොරකඩ මුහුණක් මතුවිය. අමරසිරි
බංඩාර  තිගැස්සුණේය. ''අම්මා! මං බය උන
තරම.''</p>

<p>ලීලාවතී මැණිකේ ගෙයි තිබූ චිමිනි ලාම්පුව
මා අතට දෙමින් ''ඔන්න ඔය කතාව නවත්තල පුතාල නිදගන්නකො'' යි කීවාය.</p>

<p>''දැන් මොකටද ඔය හොල්මන් කතා කරන්නෙ.?
''</p>

<p>''මං අයියට පස්සෙ කියන්නංකො'' රහසින්
මෙන් කී අමරසිරි බංඩාර අම්මාත්  සමග ගෙට
ගියේය.</p>

<p>මා චිමිනි ලාම්පුවත් ගෙන මගේ කාමරයට යන අතර
මහදොර වැසෙන හඬ ඇසිණි. ලීලාවතී මැණිකේ පුතාට යමක් කීවාය. කාමරයට ආ මම  දොර ඇතුළතින්  වැසුවෙමි. මගේ ඇඳ තිබුණේ දොරෙන් වම්පස ඉදිරි බිත්තයේ කවුන්ථවට පහතිනි.
දොරට මුහුණලු එහා බිත්තියේද එවැනිම යකඩ කූරු සහිත කවුන්ථවක්  විය. 
fï fudfydf;a  cfk,a folu yer
ou,dh' msg; w÷r oeäh' iS;, iq</p>

<p>මගේ බෑග් මල්ල කාමරයට ගෙන ආවේ  නයනා 
úh hq;=h'  nE.fhka .;a iru
ye|.kakg l,ska uu fodr;a" cfka,;a jid oeuqfjñ'  cfk,a  mshka jioa§ hka;ï
ìhla ud is; ;=  jeiqfKa wmyiqjlsks' th wÈkakg w; msg;g ouoa§
hfula uf.a w;ska w,a,kakd fiah yeÛ=fKa''''''th f;;ukfhka hq;= iq  uejqk
udhdjlao@</p>

<p>සරම හැඳගත් මම පහන නිවා ඇඳෙහි
ඇළවුනෙමි.  පිටත හුම් හුම් හඬක් නැගෙන්නට
විය. ''බකමූණෙකැ''යි මම අනුමාන කළෙමි. සිහින් සිරි පොදයක් සුළඟකුත් සමග හමා
ගියේය. මම හුම් හුම් හඬ අසමින්  නින්දට
වැටුණෙමි.</p>

<p>එහෙත් එය කෙටි නින්දක් විය. තවමත් ගත
වෙහෙස  දැනුණද මට ඇඳෙහි ඇළවී සිටීම
අපහසු විය. මා අඳුරු පාර දිගේ ඇවිද එද්දී දැනුණ පෙරේත ගඳ දැන් මේ කාමරය තුළ විය.
''ගස් ගඳ'' යැයි මම සිතුවෙමි.</p>

<p>ඇයි මට නින්ද නොයන්නේ? ඇඳෙහි
පෙරළෙමින් මම නොයෙක් දේ ගැන සිතුවෙමි. බක්කා.....සිසිලින්....බක්කාගේ
හොල්මන.....උන පඳුරු අතර දිලිසුන දෑස කාගේද? උණ හපුන්ථවෙක්? හෝතඹුවෙක්? ඒ
නැත්නම්...විකාර....මම රුවිනී ගැන සිතන්නට වුනෙමි.රුවිනී....මා ආදරණීය රුවිනී.....මම
ඇගේ මුහුණ සිත මවා ගතිමි. මා ගත හැඟුමෙන් පුබුදුවන්නට විය. මුන්ථ සිරුරම රත් යවටක්
සේ රත්වෙයි. ඇදෙහි නිදාගත නොහී මම එහි කෙළවරක හිඳ  ගත්තෙමි. මගේ සිත කියන්නේ බිම හිඳ ගන්නයි. කිසිම හේතුවක්  නැතිව මම ඇඳ අසලම බිම හිඳගත්තෙමි.  මගේ සිරුර තවමත්  රත් යවටකි.  මගේ ඉඳුරන්
පිබිදී ඇත්තේ වෙන කිසිදාක නොවූ විරූ අයුරකිනි. මහා  බැම්මක් බැඳ සිර කරන ලද ගංගාවකි මා තුළ තෙරපෙන්නේ. සිමෙන්ති
පොළවේ සීතල පහස විඳිමින් මම එහි වැතිරුණෙමි. 
fufia ìu  je;sr  .;a uu oeä fjfyiska  jqjo" msìÿkq b÷rka  iam¾Y lf  reúkS ) reúkS ) uf.a oE; 
läkï úh' neñ ì| uyd Èh l|la msgdr .eÆjd fiah' uu fjfyiska ìu je;sr .;a
;eku kskaog jegqfKñ' </p>

<p>මා උදෑසන අවදිවූයේ ද ඒ අයුරෙන් බිම
වැතිරුණ ගමන්මය.</p>

<p>මෙතරම් ඇඳක් තිබියදී මා එසේ බිම නිදාගත්තේ
ඇයි? හේතුවක් නම් නොවීය.</p>

<p>මට ඉමහත්  කෙඩෙත්තු ගතියක් දැනිණ. මුහුණ ඉදිමුණාක් මෙනි. කවුන්ථව හැරිය මම ඒ
ළඟම එල්ලා තිබුණු කැඩපතට එබී බැලූවෙමි. මගේ දකුණු කම්මුල හරහා සීරීම්  ලකුණු තුනක් විය. හරියටම කවුදෝ නියපොතු තුනකින්
සූරා දැම්මාක් මෙනි. මිදුණු සිහින් ලේ පාරවල් එතරම් ගැඹුරක්  නොවුවද මුන්ථ මුහුණේම වූයේ  ඉදිමුණු ස්වභාවයකි.</p>

<p>කැරපොත්තෙක්  හෝ වෙනත් කුරුමිණි සතෙක් හෝ මගේ මුහුණ හරහා ඉරි අඳිමින්  ඇවිද ගියාද? තද නින්දේ සිටි මට එය නොදැනෙන්නට
ඇති.මේ හැරෙන්නට මේ සීරීම් ලකුණුවලට වෙන පිළිතුරක් නම් නොවීය.</p>

<p>මා දොර හැරගෙන ඉස්තෝප්පුවට යත්ම
අමරසිරි බංඩාර මා එනතුරු බලා සිටියාක් මෙන් ''අයියට පරක්කු වෙනව නේද?'' යි ඇසීය.</p>

<p>''මට හරියට නින්ද ගියෙ නැහැ'' තුවාය කර
ලාගෙන  ළිඳ ළඟට යමින් මම කීවෙමි.</p>

<p>කඩිනමින් නා හැඳ පැළඳ ගත් මම ලීලාවතී
මැණිකේත්,  වජිරාත් එක්ව පිළියෙල
කළ පිට්ටුත්, කිරිහොදිත්  ගිල දමා
පාරට ආවෙමි. මා අනුව ආවේ සුදු යුනිපෝමයක් හැඳගත් නයනායි. මේ ගෙදරින් පාසලට යන්නේ
නයනා පමණි. වජිරා පාසලෙන් අස්වී වසර කීපයකි. අමරසිරි බංඩාර දෙවැනි වතාවටත් උසස්
පෙළ විභාගයට හිඳ දැන් ගෙදර කල් මරයි. අප්පච්චි මිය යාමෙන්  තමාගේ ඉගෙනීම කඩාකප්පල්  වීයැයි ඔහු කියන්නේ වෝදනාවක් ලෙසිනි.</p>

<p>මාත් 
khkd;a  lkao nei mdrg  tkúg 
wurisß nxvdr mdr woaor n,d isáfhah'</p>

<p>''අයියා ගෙදර ආවම මං අර කතාව
කියන්නංකො'' ඔහු කීවේය.</p>

<p>නයනා හෝ ඔහු හෝ මගේ මුහුණේ සටහන්වූ
සීරීම්  සලකුණු නොදැකීම එක් අතකට සහනයක්
විය. මා සිතූ තරමට සීරීම් දරුණු නොවන්නට ඇත. නයනාත්, මමත් මඳක් දුරක් කතාබහක්
නොකර හන්දිය  අතට ආවෙමු.  අප යන මඟ, පාසල් යන දරු දැරියන්  කීප දෙනකුද, රැකියා සඳහා  යන කීපදෙනකුද දැකිය හැකි විය.</p>

<p>මගේ විදුලි පන්දම පෙරදා රාති්රයේ වැටුණ
තැනට ළංවෙත්ම වමතට අත දිගු කළ නයනා ''අයියෙ, පැල් කවි කියනව ඇහුණෙ අර
කන්දෙන්ද?'' යි ඇසුවාය.</p>

<p>''ඔව්''</p>

<p>''එතැන පැලක් නැහැ අයියෙ. අර බලන්න නිකං
මානා පඳුරු වැවුණ කැලේ ''</p>

<p>''ඇත්ත තමයි නංගියෙ.''</p>

<p>''ඉතිං අයිය කොහොමද එතැනින්  කවි අහන්නෙ?''</p>

<p>''සමහර විට කවුරු  හරි කන්දෙ ඉන්න ඇති''</p>

<p>''ඒ 
fudk u</p>

<p>මට පිළිතුරු නොමැති විය.</p>

<p>''අයිය මීට පස්සෙ රෑ එනකොට පරිස්සමෙන් එන්න.
හොඳ අයියනෙ.''</p>

<p>ඉන්පසු හන්දියට එනතෙක් අප අතර කතා බහක්
නොවීය.  නයනා පාසල අතට ගිය අතර  මම බැංකුවට පිය නැඟුවෙමි.</p>

<p>මේ කුඩා බැංකු ශාඛාවේ සේවය කළේ සුන්ථ
පිරිසකි.  කළමනාකරු සිරි ජයතුංග හැරෙන්නට
මෙහි සිටියේ. අතුලත්, චිත්රාත්, අනෝමාත්, මාත්, කාර්යාල කාර්ය
සහායකයන් වූ ජයසිරිත්,  ටිකිරි බංඩාත්ය.</p>

<p>මගේ 
uqyqfKa iSÍï i,l=Kq uq,skau ÿgqfõ chisßh'  ~~fï fudflda uy;a;h uQK yQrf.k@~~</p>

<p>ඔහුගේ විශ්ම පිරි කටහඬ අසමින් ටිකිරි  බංඩා ද මා සිටි මේසය වෙතට ආවේය. ''ඔව්මයි
මහත්තයා, මේ මොකද? ඊයෙ රෑ කවුදෝ එක්ක මොනවද කරල තියෙනව.''</p>

<p>තව ටිකකින් අනෝමා ද එතැනය. ''ඇත්තට ජනක
මේ මොකද?'' මා මෙහි ආ දා පටන්  ඈ මා හා
ඉතා කුන්ථපගය. ''කවුද අනේ ඔයාගෙ මූණ හීරුවෙ? ඔයාට කවුද ඉන්නව නේද?'' අනෝමා
උවමනාවටත් වඩා මිතුරු වෙන්නට තැත් කරතැයි මට කීපවතාවක්ම සිතුණි. ඇය මෙයට පෙර වතාවක
මගේ පමේ සබඳතා ගැන හාරා අවුස්සන්නට ආ විට ද මම ඇයව මඟ හැරියෙමි.</p>

<p>''ඊයෙ රෑ මොනවද  උනේ අනේ?'' ඈ යළිත් ඇසුවාය.</p>

<p>''මං දන්නෙ කොහොමද? ඊයෙ කොළඹින් මං
ආපහු ආවෙ රෑ වෙලා. බෝඩිමට යන මඟදිනං ඔන්න මොකෙද්ද පාර හරහට දිව්වා. ඊ ළඟට කවුද
කවි කියනව ඇහුණ. මගේ අතේ තිබුණ ටෝච් එක පල්ලමේ වැටිලා හොයා ගන්න වෙලා ගියා. ...''</p>

<p>මා විස්තරය කියාගෙන  යද්දී අනෙක් අය  උවමනාවෙන් අසා සිටියහ. 
~~wfma fndaäfï f.or wh kx lshkafk ux fyd,aukla oel,Æ'~~</p>

<p>''ඔය කියන තැන බක්කගෙ  හොල්මන තියෙනව කියනවනේ'' ජයසිරි කීවේය.</p>

<p>''ඔව් බක්කගෙ කතාව මං ඇහුව.'' මම කීවෙමි.
''ඒ උනාට  මං ඇහැට දැක්කෙ නෑනෙ. කවුද කවියක්
කිව්ව වගේ ඇහුණෙ''</p>

<p>''අනේ මගේ ඇඟේ මයිල් කෙළින්
හිටියා.'' අනෝමා ගත සලිත කළාය.</p>

<p>''මහත්තය ඕක බලා ඉන්න එපා. මොකක් හරි
දිෂ්ටියක් මහත්තයට බැල්ම දාලා තියෙනවා.'' ටිකිරි බංඩා එසේ කියත්ම එහා මේසය වෙත  හිඳ සිටි අතුල ''මං දන්නව දිස්ටිය මොකද්ද
කියා''යැයි හඬගා කියා සිනාසුණේය.</p>

<p>අනෝමාත් ඒ අනුව ජයසිරිත් ඉවතට ඇවිද
ගියහ. ටිකිරි බංඩාද තවත් මඳ වේලාවක්  දිස්ටි
ගැන කතා කරමින් සිට වෙනතක ගියේය. ''මහත්තය මොකක් හරි ආරස්සාවක්  කරගන්නම ඕනෑ. අඩුම තරමින්  මහපිරිත් පොතවත්  කොට්ටෙ යට තියාගන්න නිදාගන්න යනකොට.'' ටිකිරි බංඩා අන්තිමට කීවේ එයයි.</p>

<p>කළමනාකරු සිරී ජයතුංග කාර්යාලයට ආවේ
මඳක් පමාවීය. එහෙත් අනෙක් අයගේ කතා බහ ඇසු නිසාදෝ ඔහු මගේ  මුහුණ දකින්නට  නොපමාව පැමිණියේය.</p>

<p>''මේක නිකං දිස්ටියක් නෙමෙයි ජනක'' ඔහු
දැනුමැත්තෙකු සේ කීවේය. ''ඔය  වගේ
හූරන්නෙ - බෝදිලිමා.''</p>

<p>''බෝදිලිමා?''</p>

<p>''ජනක අහල නැතැයි  බෝදිලිමා ගැන.''</p>

<p>''අහල තියෙනවා. පෙරේත දිස්ටියක් නේද?''</p>

<p>''සමහරු නං කියන්නෙ. කටුස්සෙක් වගේ  දිස්ටියක් කියල'' සිරී තමා දන්නා දෙයක් විස්තර
කළේය. ''ඉස්සර මං පානදුර බ්රාන්ච් එකේ වැඩකරන කොට එතැන ක්ලාක්  කෙනකුට ඔය වගේ වැඩක් වුණා.  මිනිහගෙ මූණ විතරක් නෙමෙයි පිටෙත්  ඉරි 
;=kla yQr, ;snqKd' lSm /hlau ´l Th úÈhg isoaO WKd' wka;sfï§ Tkak u;=r,
kQ,a  nekaod' Bg  miafi ;uhs yß .sfh'~~</p>

<p>''මං නං හිතන්නෙ මගෙ මූණෙ කැරපොත්තෙක්
දුවන්න ඇති'' මම ගණනක් නොගෙන කීවෙමි. ''ආයෙත් උනොත්  බලන්න  බැරියැයි''</p>

<p>''මහත්තයගෙ මූණ නං ඒ හැටි  හොඳ නෑ'' දවල් වරුවේද ටිකිරි  බංඩා කීවේය. ''අසනීප ගතියක් තියෙනවද?''</p>

<p>මා  ''නැහැ'' යි කීව ද තවමත් මා ගතට කෙඩෙත්තු ගතිය
දැනිණි. මා බලා සිටියේ  කොයි වේලේ  නවාතැනට යනවාද කියායි. හවස  වැඩ ඇරුණ සැනින් මම ගෙදර බලා ආවෙමි.</p>



<p> </p>



<p> </p>

<p> </p>

<p>තරු ගැ''ම</p>

<p>''නක්ෂත්රය
බලන්නාවූ අඥානයාගේ අර්ථය ඉක්මෙන්නේ ය. අර්ථයට අභිෂ්ටාර්ථ ප්රතිලාභයම නක්ෂත්ර නමි.
තාරකා රූපයෝ කවර නම් අර්ථයක් සාදද්දැ'' යි කී සේක.</p>

<p>- නක්ඛත්ත ජාතකය
- 49</p>

<p>විවාහ මංගල්යය සඳහා සියලු
කටයුතු සූදානම් වෙමින් තිබිණ. සැවැත්නුවර ප්රසිද්ධ ඉඩම්හිමි ගොවි මහතෙකු වූ  ගෙහිමියා 
;u tlu ÈhKsh fjkqfjka  l</p>

<p>අන්ථත් වැඩියාවෙන්
කලඑළිවූ නිවස ඉදිරිපිට  තොරණ බැඳ මාර්ගය
ද සරසා තිබිණ. නගරයේ ප්රසිද්ධම සංගීත කණ්ඩායම, මැනවින් සකසා තිබූ මංගල
වේදිකාව  ඉදිරියේ  තම මෙහෙය මගින් අමුත්තන්ගේ 
ijka mskjQfhah' wiqmsáka" wiaßfhka yd md .ukska wdrdê;  wuq;a;ka iy {d;Syq uÛ=,a f.org  meñfKñka isáhy' leú,s  fmú,s j,ska ix.%y lrk fm/;a;fhka yd isky
yçka uq¿ m</p>

<p>නියමිත වේලාව ලංවත්ම
කාගේත් දෙනෙත් මහමග දෙසට  යොමුවන්නට විය.
කාගේත් ආශාව වූයේ මනාලිය කැන්දාගෙන යාම සඳහා 
w,xldr r:hlska  {d;s msßjr  iuÛ meñfKk ukd,hdf.a uqyqK n,d .ekSug h'
ld,h úkdä  .Kkska  miq ù h;au f.ysñhdf.a  is;a ;=  fiahdjla o Wmk' ~~uqka wmj /jeÜgqjdj;a o@~~ fya is;ska  m%Yak flf</p>

<p>       ~~f.dhs
uy;a;hd" wehs fï f.d,af,d  fï ;rï
mudfjkafk" u. lrorhlaj;a fj,d o@~~tc "&amp;#9;f.dhsuy;a;hd\"wehsfïf.d,af,dfï;rïmudfjkafk\"u.lrorhlaj;afj,do@"</p>

<p>සමහර අමුත්තෙකු නැගූ
ප්රශ්නයට නිගමනයකින් තොරවම ගෙහිමියා 
hï;ñka jpk .,md .;af;ah'</p>

<p>       ~~Tõ
ud;a ys;kafk tfyu fjkak we;s~~tc "&amp;#9;Tõud;ays;kafktfyufjkakwe;s"</p>

<p>ඊළඟ මොහොතේ  ඔහුගේ මුවට, තිරලෙස පෙනුණු එක් අදහසක්
පහළ විය.</p>

<p>       ~~kE'
lula kE' álla myq jqkdg ta f.d,af,d tkafk wE; m%;Hka; .ul b|,hs'
fldfydug;a  myqfj,d yß ths' wmg Th
kele;a fõ,djla lsh, fohla kEfka ` ~~tc "&amp;#9;kE'lulakE'államyqjqkdgtaf.d,af,dtkafkwE;m%;Hka;.ulb|,hs'fldfydug;amyqfj,dy¹ths'wmgThkele;afõ,djlalsh,fohlakEfka!"</p>

<p>අසල්
වැසියෙකුගේ මාර්ගයෙන් සිදුවූ මේ මංගල යෝජනාව 
ls%hd;aul  lsÍu i|yd ;uka úiska
fhdað; Èkh ms  md¾Yjh
tlfy,d leue;a; m  ixúOdkh lrkakg o ks.ukh úh' Tjqka yd tlÛ jQ
fydardj o blau f.dia yudrh' ;ud fõ,dj kshulr .;af;a  ldf.a;a  myiqj fukau
foaY.=Ksl ;;a;ajh o i,ld ne,Sfukah hkak Tyq isyshg  ke.Sh'tc "wi,a jeisfhl=f.a udr®.fhka
isjQ fï ux., fh`ckdj  ls%hd;aul  ls¯u i|yd ;uka úiska fh`; kh ms  mdr®Yjh tlfy,d leue;a; m 
ixúOdkh lrkakg o ks.ukh úh' Tjqka yd tlÛ jQ fy`rdj o blau f.dia yudrh'
;ud fõ,dj kshulr .;af;a  ldf.a;a  myiqj fukau foaY.=Ksl ;;a;ajh o i,ld ne,Sfukah
hkak Tyq isyshg  ke.Sh'"</p>

<p>,මේ මිනිස්සු අපි
අමාරුවෙ දැම්මද? නැතිනම් වෙන හතුරෙකුගෙ උපක්රමයක් ද?, පසෙකට කැඳවාගත් සිය බිරිඳට
අවස්ථාවේ බරපතළ කම පැහැදිලි කළ හේ, ඇගෙන්වත්  සුදුසු 
ms  ,efíoehs
n,dfmdfrd;a;= m</p>

<p>රහසිගතව පැවැති
සාකච්ඡාවට තවත් ළඟම නෑයන්  කීපදෙනෙක් ද
එක්වූහ. තම සොහොයුරාත් බිරිඳගේ  මවත් ආදී
හිතවත් පිරිසේ දැඩි  උනන්දුව ගැන ගෙහිමියා ට
දැනුනේ  මහත්  සැනසිල්ලකි.</p>

<p>       ~~fï
;rï uqo,l=;a úhoï lr," ta Tlafldgu jeäh oeka .fï  rfÜ ñksiaiqka weú,a," fïl uy kskaodjla'''''''''''fï .fï kï
bkak ,efnkafk keye''''''''''~~ tc "&amp;#9;fï;rïuqo,l=;aúhoïlr,\"taTlafldgujeähoeka.fïrfÜñksiaiqkaweú,a,\"fïluykskaodjla'''''''''''fï.fïkïbkak,efnkafkkeye''''''''''
"</p>

<p>නැන්දණිය
උද්වේගයෙන්  තෙපළාය. කෙසේ හෝ
අවස්ථාවට මුහුණ දීමට කිසියම්  පිළියමක්
වහාම කළ යුතුය, යන්න කාගේත්  නිගමනය
විය. අවසානයේ සියලුදෙනම තීරණාත්මක 
uqyqKqj,ska  hq;=j" we;=¿
.en /£ isá  ukd,sh  fj; .uka .;ay'  f.ysñhd wuq;a;kag tlaúh'tc "kekaoKsh Woafõ.fhka  f;m  ms  ks.ukh úh'
wjidkfha ish,qfoku ;SrKd;aul 
uqyqKqj,ska  hq;=j\" we;=ý
.en /£ isá  ukd,sh  fj; .uka .;ay'  f.ysñhd wuq;a;kag tlaúh'"</p>

<p>       ~~oeka
fï mKsjqvhla wdj' ta f.d,af,d u. tkjÆ' fjÉp mudj .ek ljqre;a iudfjkak
´k''''''''''~~tc
"&amp;#9;oekafïmKsjqvhlawdj'taf.d,af,du.tkjÆ'fjÉpmudj.ekljqre;aiudfjkak´k''''''''''"</p>

<p>හෝරාවක් පමණ  ගතවුණු තැන මනාලයා සිය ඤාතීන් කිහිපදෙනෙකු සමඟ
මනාලියගේ නිවසට පැමිණියේය. කාගේත් මුහුණු 
i;=áka  msÍ mej;sK' kshñ; mßÈ
pdß;% úê  bgq flreKq miq kj hqj,g fi;a
me;+ wuq;af;da wdmiq  hkakg jQy'
miqÈk  olajd  ish,a, idudkH f,iska 
isÿúh'  bruqÿka  ùug fydard 
follg muK fmrd;=j meñKqkq wuq;a;ka msßil u.ska  ksjfia iduhg ndOd jQ njla fmksK'</p>

<p>අමුත්තන්  පිරිස හඳුනාගනු ලැබීමෙන් පසු ගෙහිමියා විසින් ඔවුන්
පිළිගනු ලැබිණ. සුදුසු  ආගන්තුක
සංග්රහයෙන් පසුව හේ සන්සුන්ව  පිරිස
ඇමතීය.</p>

<p>       ~~wms
ÿj újdy lr,d ÿkakd' ta Tlafldu lghq;= 
wjika jqfka Bfha' fmdfrdkaÿ jqkq úÈhg Bfh Tfya, wdfõ ke;s tl .ek wms ld
;=  mqÿu ;ryla kï we;sjqKd ;uhs'
t;a oeka ta Tlafldu bjrhs'  wms miafi
fhdod.;a; jev ms  isoao
jqkd'~~tc
"&amp;#9;wmsjújdylr,dkakd'taTlafldulghq;=wjikajqfkaBfha'fmdfrdkajqkqúhgBfhTfya,wdfõke;stl.ekwmsld;=</p>

<p>       ~~fudkjd
ÿj újdy lr, ÿkak@ t;fldg wmg fjÉp fmdfrdkaÿj@~~ wuq;a;ka msßfia m%Odkshd fõ.j;aj
ke.S isáfhah'tc
"&amp;#9;fudkjdjújdylr,kak@t;fldgwmgfjÉpfmdfrdkaj@ wuq;a;ka ms¹fia m%Odkshd
fõ.j;aj ke.S isáfhah'"</p>

<p>       ~~Tõ"
ta foa isoaO jqkd' Th f.d,af,d fj,djg wdfj ke;sksid ÿj .ek nd,fmdfrd;a;= fjka
ysáh  fjk;a ;reKfhl=g wms lghq;a; lr,
ÿkak' tfyu fkdl  lr,'  wmg wuq;a;kag uqyqK
fokak;a neß fjkjd' fldfydu jqk;a wmsg ;du;a m%Yakhla wehs" Thf.d,af,d
fj,djg wdfj ke;af; lshk tl~~tc "&amp;#9;Tõ\"tafoaisoaOjqkd'Thf.d,af,dfj,djgwdfjke;sksidj.eknd,fmdfrd;a;=fjkaysáhfjk;a;reKfhl=gwmslghq;a;lr,kak'tfyufkdl</p>

<p>ගෙහිමියාගේ  බිරිඳ කරුණු පැහැදිළි කරන්නට විය.  සිදු වී ඇති හරිය තේරුම් යාමෙන් ආගන්තුක පිරිස
දැඩිලෙස කෝපයට පත් වුවද මනාලයා ලෙසින් පැමිණ තරුණයා හැමදෙනාම සන්සුන්ව තබන්නට
ප්රයත්න දැරීය.</p>

<p>       ~~fõÉp
foaj,a .ek l,n, lr.kak jqjukdjla kE' oeka ;d;a;d lshkak wfma me;af;ka mudfjkak
fya;=j jqfka fudllao lsh,~~tc "&amp;#9;fõÉpfoaj,a.ekl,n,lr.kakjqjukdjlakE'oeka;d;a;dlshkakwfmame;af;kamudfjkakfya;=jjqfkafudllaolsh,"</p>

<p>මනාලයාගේ  පියා කරුණු පැහැදිලි කරන්නට පටන් ගත්තේය.tc "ukd,hdf.a  mshd lreKq meye,s lrkakg mgka
.;af;ah'"</p>

<p>       ~~wms
tod Èfka  ;SrKh lr.;a; nj we;a;' ta;a
wfma mrïmrdfju isß;la ;uhs yeu fohlau fyd|g fydh, n,, kele;a fj,djlu lrk tl'
wer;a wfma mq;df. úf;a jeo.;au isÿùu fyd|u fj,djl  fkd.;af;d;a  ljodj;a ta
jefâ id¾:l fjkafk kE'  ta ksid  uu wfma .fï fcHda;sId pd¾h;=ud yïnfj,d
ldrfKa lsõj' t;=ud .;a  lggu lsõj Th
lshk Èfkag fyd| fj,djla we;af;u kE" fj,dj tkafk miafikaoguhs lsh,' ta ksid
uu ;SrKh l 
fodaIdfrdamkh ltc "&amp;#9;wmstodfka;SrKhlr.;a;njwe;a;'ta;awfmamrïmrdfjuis¹;la;uhsyeufohlaufyd|gfydh,n,,kele;afj,djlulrktl'wer;awfmamq;df.úf;a
jeo.;au isùu fyd|u fj,djl 
fkd.;af;d;a  ljodj;a ta jefâ
idr®\:l fjkafk kE'  ta ksid  uu wfma .fï fcHda;sId pdr®h;=ud yïnfj,d
ldrfKa lsõj' t;=ud .;a  lggu lsõj Th
lshk fkag fyd| fj,djla we;af;u kE\" fj,dj tkafk miafikaoguhs lsh,' ta
ksid uu ;SrKh l fo`Idfr`mkh l</p>

<p>       ~~wms
kï Th kele;a fj,dj,a .ek ta ;rï .Kka .ekSula kE' wms ne,qfj ldf.;a  myiqj' ta ksid Bfhg Tlafldu iQodkï lr.;a;'
fyd|hs b;ska  Th oji fjkia ùu .ek wehs
wmg l,ska fkdoekakqfj@~~ f.ysñhd m%Yak flftc "&amp;#9;wmskïThkele;afj,dj,a.ekta;rï.Kka.ekSulakE'wmsne,qfjldf.;amyiqj'taksidBfhgTlaflduiQodkïlr.;a;'fyd|hsb;skaThojifjkiaùu.ekwehswmgl,skafkdoekakqfj@
f.ysñhd m%Yak flf</p>

<p>       ~~l,a;shd
oekqï §ula  .ek wms ta ;rï nr m;,la  ys;=fj kE' lghq;a;  ;Skaÿ jqkdkï wmg ;sfhkafk ÿj tlalka hkakhs' wfma isß; lekao.k
.syska  ukd,hf. me;af; fmdä W;aijhla
.kak tlhs' talhs" oekakqfj ke;=j wou wdfj' kele;a fj,djg lekaoka hkak'
wer;a Tfya, jqk;a ys;kak jákj fldfyduo fï jf.a jeo.;a  lghq;a;la  lrkafk iqÿiq
fj,djla n,kafk ke;=j lsh,'''''~~ wuq;a;kaf.a 
me;af;ka  lsheúKs' Tjqka ld
;=tc "&amp;#9;l,a;shdoekqï°ula.ekwmsta;rïnrm;,lays;=fjkE'lghq;a;;Skajqkdkïwmg;sfhkafkjtlalkahkakhs'wfmais¹;lekao.k.syskaukd,hf.me;af;fmdäW;aijhla.kaktlhs'talhs\"oekakqfjke;=jwouwdfj'kele;afj,djglekaokahkak'wer;aTfya,jqk;ays;kakjákjfldfyduofïjf.ajeo.;alghq;a;lalrkafkiqiqfj,djlan,kafkke;=jlsh,'''''
wuq;a;kaf.a  me;af;ka  lsheúKs' Tjqka ld ;=</p>

<p>       fï
w;r f.ysñhd ;j;a woyila f.kyer olajkakg úh' ~~ug fmak yeáhg fu;k jev jroao,
;sfhkafk Tfya,f. ys;j;a kele;a .=re;=ud' fyd| fj,djla  yokak lsh, Èkh;a fjkia lr, uy wjq,la we;sl 
weyeõfj ke;s tl .ek' ta ;ryg Tfya,f.ka m  wjq, yokak we;af;' fldfydug;a fïl;a mdvula~~tc "&amp;#9;fï w;r f.ysñhd ;j;a woyila
f.kyer olajkakg úh'
ugfmakyeáhgfu;kjevjroao,;sfhkafkTfya,f.ys;j;akele;a.=re;=ud'fyd|fj,djlayokaklsh,kh;afjkialr,uywjq,lawe;sl</p>

<p>නැකැත් ගුරුතුමා ගැන
සැකමුසු අදහස පළවෙත්ම අමුත්තන් සිත් ද විපිළිසර වන්නට විය. ඉදිමුණු හිසින් යුතු
ඔහුගේ  ආඩම්බර කි්රයා කලාපය ගමම දන්නා
කරුණක් විය. ඔහුගේ අදහස් නොවිමසා ගත්  මේ
කි්රයා මාර්ගය පිළිබඳ පළවුනු අප්රසාදය ද වැදගත් සාධකයක්ව පවතියි.  මෙය ඔහුගේ ම පළිගැනීමේ  උපක්රමයක් විය නොහැකි ද? අමුත්තෝ සිතිවිලි බරව
මොහොතක්  නිහඬව සිටියහ.</p>

<p>තම නිවස දෙසට පියනගන
මිනිස් රුව දුටු ගෙහිමියා හුනස්නෙන් නැගිට ඉදිරියට ගියේය. අමුත්තා හඳුනාගන්නට වේලාවක්
ගත නොවිණ. ''එන්න ආචාරිනි එන්න,.........................!'' ගෙහිමියාගේ
උණුසුම් ආරාධනය කාගෙත්  අවධානයට  යොමුවිය. සැරයටිය පසෙක තබා උස්ව බැඳුනු තලප්පාව  අතට ගත් 
wdpdÍyq f.;= 
W;a;=x. isrer;a  nqoaê m%Ndjfhka  hq;= uqyqK;a" meiqKq jhfiys Ydka;
fmkqu;a úiska  ujd,Q f.!rj iïm%hqla;
o¾Ykh bÈßfha  ish,qu wuq;af;da
wiqkaj,ska  ke.S ysi kud  wdpdr l</p>

<p>නගරයේ ශාස්ත්ර ශාලාවේ
නියමු ඇදුරුපාණන්  වශයෙන් පමණක් නොව
සිල්වත්, ගුණවත් වැඩිහිටියෙකු වශයෙන් ද මේ බ්රාහ්මණ පඬිවරයා කාගේත්  ගෞරවයට පාත්ර වී සිටියේය. දිනය පසු වී හෝ
එතුමන්  ;u ksjig iem;aùu .ek f.ysñhd
;=  i;=gla Wmk'</p>

<p>       ~~Bfha
ux.f,da;aijhg tkak ug wjia:djla  ,enqfk
kE' ta ksihs" wo wdfj iqN m;, hkak~~tc "&amp;#9;Bfhaux.f,`;aijhgtkakugwjia\:djla,enqfkkE'taksihs\"wowdfjiqNm;,hkak"</p>

<p>එළඹුණු මාතෘකාව
ඔස්සේ පිළිසදර කථාව මැනවින් සිදුවිය. 
kele;a fõ,dj wrNhd isÿ ú we;s wn.a.h ldg;a fyd| mdvula úh hq;= neõ
mäjrhdf.a woyi úh'</p>

<p>       ~~kel;
lshkafk wyfi ;sfhk  ;drld wkqj wfma jev
lghq;= md,kh lr.kak tlhs" ta wkqj fyd| krl ;SrKh lr.kak tlhs' W;aidyh
;sfhk  wêIaGdkYS,S  ukqiaifhl=g yeu fõ,dju kele;a fõ,djla' fyd|
jevla mgka .kak ;re .ek .ek bkak ´k kE' tal iuyr lïue,s ñksiaiqkaf. Wmdhla
ú;rhs' ;uka lrk jefâ .ek wd;au úYajdihla ke;s ñksiaiq tajdfha id¾:l wid¾:l Ndjh
kele;a ;rej,g Ndr fokj' tfyu lr, jev jerÿkdu kel;g neK neK bkakj ñil ;ukaf. wvq
mdvqj fudllao lsh, ys;kak bÈßm;a fjkafk kE' ~~uf.a krl fj,dj" .%y
wm,''''''~~ lsh lshd is;ska mrdch Ndrf.k úf;a 
ld,lkaks lr.kakj' ta ksid kel;g jeäh 
uq,a;ek fokak ´k ;uka lrk jefâghs' oeka fï isoaêfha § jqk;a fj,djg
weú,a, kshñ; lghq;a; ltc "&amp;#9;kel;lshkafkwyfi;sfhk;drldwkqjwfmajevlghq;=md,khlr.kaktlhs\"tawkqjfyd|krl;SrKhlr.kaktlhs'W;aidyh;sfhkwêIaGdkYS,Sukqiaifhl=gyeufõ,djukele;afõ,djla'fyd|jevlamgka.kak;re.ek.ekbkak´kkE'taliuyrlïue,sñksiaiqkaf.Wmdhlaú;rhs';ukalrkjefâ.ekwd;auúYajdihlake;sñksiaiqtajdfhaidr®\:lwidr®\:lNdjhkele;a;rej,gNdrfokj'tfyulr,jevjerkdukel;gneKneKbkakjñil;ukaf.wvqmdvqjfudllaolsh,ys;kakb¹m;afjkafkkE'uf.a
krl fj,dj\" .%y wm,''''''lshlshdis;skamrdchNdrf.kúf;a  ld,lkaks lr.kakj' ta ksid kel;g jeäh  uq,a;ek fokak ´k ;uka lrk jefâghs' oeka fï
isoaêfha ° jqk;a fj,djg weú,a, kshñ; lghq;a; l</p>

<p>සැවැත් නුවරින් බැහැර
ඈත ගම් පියසට නික්මුණු අමුත්නත් පිරිස වෙත 
neyeßka ysia njla fmkqk o  .ug
f.khdug ;rï yrj;a hula  Tjqkaf.a is;a
;=</p>

<p> </p>

<p>අනුගමනය</p>

<p>''...............අස්සලයාගේ
පය කොර කොට ඔබන නිසා මුත් ඒ නිසාම ඌ පමණක් විනා ව්යාධියක් නැතැයි ..........
නිර්ව්යාධි අස්සලයෙකු අසු බලන්නට සලස්වන්නා අසුගේ පය කොර ඇරෙයි.......''</p>

<p>- ගිරිදත්ත ජාතකය
- 183</p>

<p>       tc "&amp;#9;"</p>

<p>''නෑ වෙන්න බෑ, මේ වගේ දුබලතාවක් ඔය සතාට
කලින් තිබුනෙ නෑ.''tc
"kEfjkaknE\"fïjf.an,;djlaThi;dgl,ska;snqfkkE'"</p>

<p>සාම
රජතුමා අවධාරණයෙන් ප්රකාශ කෙළේය. 
ue;s  weu;sjreka we;=¿ ish,q  wuq;a;kaf.a 
oEia ta wkqj uÛ=,a  wiq fj; fhduq
ù ;sìK'tc
"idu rc;=ud wjOdrKfhka m%ldY flf 
ue;s  weu;sjreka we;=ý ish,q  wuq;a;kaf.a 
oEia ta wkqj uÛ=,a  wiq fj; fhduq
ù ;sìK'"</p>

<p>රාජකීය අස් හමුදාවේ
වාර්ෂික  පෙළපාලිය නරඹනු වස්  මහරජතුමන්ගේ ප්රධානත්වයෙන් සියලු  ආරාධිත පිරිස රාජාංගනයේ විශේෂ වේදිකාවේ ද,
සෙසු මහජනයා  මහ මන්ථව පුරා ද රැස්ව සිටියහ.
අස් හමුදාවේ  පෙරමුණු  බල ඇණිය පියමන් වූ පසු විශේෂ ආරක්ෂක අසුන්
පිරිවරකොට මංගල  අශ්වයා ගමන් කරවනු
ලැබිණ.  පරිවාර අශ්වයන්  අතර උත්තුංග 
ldh iïm;a;sfhka  hq;=j Oj,
j¾Kfhka  nen,qkq cjdêlhdf.a m%:u o¾Ykh
is;a m%fndaOhg  m;a l  ld ;=  W! .uka l 
wdldrhhs'</p>

<p> wYajhd fj; fhduqjqKq uyckhdf.a fidamydi ne,au
B   uyrc;=uka  fj;h' 
fya  uy;a  fia 
wiykhg  m;aj isáfha h'</p>

<p>''කොතනක හෝ බරපතළ
වැරැද්දක්  සිද්ද වෙලා තියෙනවා. මේ සතාට
අසනීපයක්වත් දැ'' යි විමසිලිමත් වූ රජතුමා 
wi,skau  wiqkaf.k isá uy
weu;sjrhdf.ka  ms  fj;ska
lsisÿ jpkhla fkdke.sK' wiqfj; fhduq jQ ne,au;a iu.u Tyqf.a ysi o
is;sú,af,ka  nrjkakg úh'</p>

<p>වංග දේශයේ  සුප්රසිද්ධ අස් වෙළඳ ආයතනයෙන් රන්මසු
දසදහස් ගණනක් ගෙවා මිළට ගැනුණු මේ සත්වයා උභය කුල පාරිශුද්ධ විය. තවත් විශාල ධනයක්
වැයකොට විශේෂඥ ආචාරින්  යොදවා  පුහුණු කිරීමෙන් පසුව මංගල තානාන්තරයෙහි තබනු
ලැබූ අසු රාජ්ය ඉස්තාලයෙහි  දෛනික  පුහුණු 
lghq;=j, fhfoñka  úfYaI ks,OdÍka
iuQyhlau hgf;a  /ljrKh ,eîh' ish,q  ;;= 
uq, isg  isysl  fï ish,q  j.lSï ;ud Ndrfha u mj;akd fyhsks'
wiqf.a  ÿn,;djg fya;=j  ;u j.lSï ksisf,i bgq fkdlsÍu fkdfõo@ wod  yßhdldrj wëlaIKh l  fujekakla 
isÿfkdjkq  we;' ta iu.u  Tyq is; NS;sfhka we  t,a,jk rdc Woyi .ek isysm;a ùfuks'</p>

<p>       ~~iudfjkak
uyrc;=uks" ug;a f;afrkafk kE fudllao 
fïlg fya;=j lsh,d" uu blaukska f;dr;=re fidhd n,kakï'~~tc "&amp;#9;iudfjkakuyrc;=uks\"ug;af;afrkafkkEfudllaofïlgfya;=jlsh,d\"uublaukskaf;dr;=refidhdn,kakï'"</p>

<p>නමස්කාර
පෙරටුව නැගී සිටි මහ ඇමති  මහරජතුමන්  වෙත යාඥා ස්වරයෙන්  තෙපළීය.tc "kuialdr fmrgqj ke.S isá uy
weu;s  uyrc;=uka  fj; hd{d iajrfhka  f;m</p>

<p>       ~~Tõ
jydu fï .ek ug jd¾;d ltc "&amp;#9;Tõjydufï.ekugjdr®;dl</p>

<p>වේගයෙන්
නැගී සිටි රජතුමා මාළිගය වෙත පියනගත්ම ආචාර පෙළපාලිය අතර මග නතර කරන ලදී. මහ
ඇමති සිය කාර්ය මණ්ඩලය ද සමඟ විශේෂ රාජකාර්යය සඳහා යුහුසුන්ථ විය.tc "fõ.fhka ke.S isá rc;=ud
ud</p>

<p>මහ ඇමතිවරයා පළමුව
කෙළේ රාජ්ය අස්හලට පැමිණ මගුලසු පරීක්ෂා කර බැලීමයි.</p>

<p>අශ්වයාගේ සියලු
ශාරීරීක  තත්ත්වයන් සාමාන්ය මෙන්ම නීරෝගි
ද  විය. මේ නිසා ඉදිරි පාදයක් ඇදයට තැබීම
රෝගී බවක සළකුණක්  නොව පුරුද්දක්
වශයෙන් අනුගමනය වන්නක් බව  මහ ඇමතිවරයාට
පසක් වන්නට වැඩිකල් නොගියේ ය.  මීළඟට
අස්හලේ  පාලක තැන ඇතුන්ථ කාර්ය මණ්ඩලය
ඔහුගේ පරීක්ෂාවට යොමුවූයේය.</p>

<p>       "fï
.ek ug kï lsis fohla ys;d .kak nE' wms n,uq .sßo;a;f.ka  wy,'"tc "&amp;#9;\"fï .ek ug kï lsis
fohla ys;d .kak nE' wms n,uq .s¹o;a;f.ka 
wy,'\""</p>

<p>අස්හලේ
පාලක තැන මහ ඇමතිතුමන්ට යෝජනා කෙළේය.tc "wiayf,a md,l ;ek uy weu;s;=ukag
fh`ckd flf</p>

<p>ගිරිදත්ත යනු තුරුණුවියේ
පසුවුනු අස්හල සේවකයෙකි. ඔහුගේ කාර්යක්ෂම භාවය හා අවංක සේවය පිළිබඳ වඩාත්
ප්රසාදයට පත් පාලක තෙමේ, ගිරිදත්ත මගුලසුගේ මෙහෙවර සඳහා යෙදවීය. තුරුණු
සේවකයා  තම වගකීම මැනවින් ඉටුකරලීම සඳහා
දිවා රෑ වෙහෙසවනු දැක්ම පාලකයාගේ සතුටට හේතුවිය. අසුගේ අහරවට කලට වේලාවට
පිළියෙල කිරීම,  තෙල් ගැල්වීම,
අදාළ පුහුණුවීම් නැවත නැවතත් ප්රගුණ  කරවීම
ආදියෙහි දී ගිරිදත්ත වෙහෙස නොබලා කාලය කැප කෙළේය. වැටහීමේ  ශක්තිය අතින් ද  අනිකුත් අසුන්  අභිබවා සිටි
මගුලසු ද තම ආරක්ෂකයා කෙරේ දැක්වූයේ ඉමහත් කුන්ථපග බවකි.  තමා වෙත පැමිණෙන ගිරිදත්ත ඈත තියාම හඳුනාගන්නා
අශ්වයා හේෂාරවය කරමින් සතුටින් ඉපිල යනු දක්නා ලදී.</p>

<p>අසු හා සම්බන්ධ
ගිරිදත්තගේ  චරියාවන්  හැදෑරූ මහ ඇමතිතුමා තුළ ද හටගත්තේ
සතුටකි. එහෙත් තවමත් මූලික ගැටන්ථව විසඳුනේ  නැත.</p>

<p>පාලක තැන සමඟ කතා බහේ
යෙදෙන අතරවාරයෙහි අස්හලේ  ඈත කෙළවරක
සිට පැමිණෙන ගිරිදත්තගේ  රුව මහඇමතිගේ
ඇස ගැටිණ. ඔබින් පැමිණෙන රුව දෙස බලාසිටි ඔහුගේ දෑස්  හදිසි අවධානයකින් දැඩිලෙස විවර විය. මදක් ඉදිරියට නැඹුරුව හදිසි
සුපරික්ෂාවකින් කැළඹුණු මහ ඇමතිවරයාගේ ස්වරූපය පාලකයාට විමතියක් විය.</p>

<p>       ~~wehs"
Tn;=ud fudl=;a  oelal o@ ta ;uhs uf.a
f.da,hd wr lshdmq .sßo;a;~~ fya úuis,su;aj mstc "&amp;#9;\"wehs\" Tn;=ud
fudl=;a  oelal o@ ta ;uhs uf.a f.`,hd wr
lshdmq .s¹o;a;\" fya úuis,su;aj ms</p>

<p>       ~~Tõ
uu oelal" tal wuq;a;la ;uhs' yßhgu yß oeka ug f;afrkj fudllao ldrfKa
lsh,'''''''''''''~~tc "&amp;#9;\"Tõ uu oelal\"
tal wuq;a;la ;uhs' y¹hgu y¹ oeka ug f;afrkj fudllao ldrfKa
lsh,'''''''''''''\""</p>

<p>හුනස්නෙන් නැගී සිටිමින් පැවසූ මහ ඇමතිවරයා,tc "yqkiafkka ke.S isáñka mejiQ uy
weu;sjrhd\""</p>

<p>       ~~wehs
fmakafk keoao .sßo;a;f. .uka ú,dfi" wr ll=,la wefoag ;sh ;shd wx. úl,fhla
jf.a ''''''''''''''~~hs kej; o md,l ;ekg lshd isáfha h'tc "&amp;#9;\"wehs fmakafk keoao
.s¹o;a;f. .uka ú,dfi\" wr ll=,la wefoag ;sh ;shd wx. úl,fhla jf.a
''''''''''''''\"hs kej; o md,l ;ekg lshd isáfha h'"</p>

<p>මහ ඇමතිවරයාගේ නිගමනය
තහවුරු කරවන හේතු සාධක තවදුරටත් දැන ගැනීමට සෙසු සියලු දෙනාම උත්සුක වූහ. ඒ අනුව
කරුණු එකිනෙක නිරාවරණය වන්නට විය.</p>

<p>උදේ සවස  හෝරා 
lSmhla WoHdkh jgd weúohdu u.=,a wiqf.a ld,igyfkys wksjd¾hh wx.hla úh'
wiqf.a  lgl,shdj w,a,d.kakd .sßo;a;
Èkm;d  tu  fufyjr  bgqlf 
l:dnyl o fhfoñka hy¿jka  fom,la
úfõlj weúohkakd  fihska isÿ flreKq fï
ld¾h jQ l,s fofokdf.au  ;Dma;sh msKsi
úh' wiq o ;u ñ;%hdf.a jpk f;areï .ksñka" Tyqf.a yeÍï"
kej;Sï"  .ukaú,dY wd§ bßhõ o
wkq.ukh lrñka  fï wNHdih uekúka  isÿ flf  lgld,shdfõ iïmg w,a,d .;ajku .uka .;a .sßo;a;f.a  tla mdohla ;rula fldgj ;snQ fyhska
Tyqf.a  .uk iunr;djhlska f;drúh' mfilg
uola we,fjñka Tyq wäh ;nk ;nk fudfyd;la mdid wiqf.a uqj ne÷kq iïmg o mfilg
wefokakg úh' .ufka § isÿjqkq  fï
ls%hdj,sh uola  we,j hdug fok  ix{djla " úhhq;= hehs  wiq o is;kakg we;' ta wkqj" l,ahdfï §
fï .uka ;d,h fldr.eiSula ;rug u ÈhqKq jQfha wYajhdf.a  idudkH .uka ú,dYh njg m;aúh'</p>

<p>දෙසතියක ඇවෑමෙන්
මහරජතුමන්  ඉදිරියේ මංගල අශ්වයා නැවත දර්ශනය
කෙරිණ. අන්ථත් අස්සලයා සමඟ රාජාංගනයට පිවිසි අසු අකම්ප්ය පද පාතයෙන්  සෘජු ලෙස දක්වන ගමන් විලාශය රජතුමන්ගේ  ඉමහත් 
i;=gg fya;=úh' </p>

<p>මහ
ඇමතිතුමන්ගේ  තියුණු  බුද්ධිය අරභයා කාගේත් පැසසුම යොමු වන්නට
වූයේ ය.</p>






</body></text></cesDoc>