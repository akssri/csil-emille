<cesDoc id="sin-w-fiction-story-divayina-151" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-fiction-story-divayina-151.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head>oeka fl,af,la fkfuhs</head></p>

<p><head>mjq,a lk .Eksfhla</head></p>

<p><head> </head></p>

<p><head>iS.sß .Shla Tfí yojf;a ,shkak
fokjd o</head></p>

<p><head>i|lv mykla Tfí yojf;a
fk</head></p>

<p><head>rkauiq Whkla Tfí yojf;a ;kkak
fokjdo</head></p>

<p><head>ñfhk ;=rd ug Tfí yojf;a /f|kak fokjdo</head></p>

<p><head> </head></p>

<p><head>weh y÷kajd §ug wuq;=fjka joka fm</head></p>

<p><head>újdyfhka miqj ksfrdaId ,nk wÆ;a w;aoelSï ms</head></p>

<p><head>b;ska ksfrdaId fudkjo fï ojiaj, lrkafka''''''''</head></p>

<p><head>ï''''ï'''' uu wÆf;ka kï isxÿ lsõfj kE' ta;a relauKs foaú
we;=¿ wfma me/Ks .dhsldjka lsysm fofkl=f. .S; w¿;a ;d,hg .dhkd lrñka leiÜ mghla
lrkjd' ud¾;= w. tal ksl=;a lrkjd'</head></p>

<p><head>me/Ks .dhsldjkafka .S; .hkafk wkjirfhkao@</head></p>

<p><head>kE wkque;sh we;sjhs .hkafka' jeäu .S; m%udKhla relauKS
foaúh úiska .ehQ .S; ix.S; wOHCIKh lrkafka iageka,s mSßia'</head></p>

<p><head>ta wefrkak kj ks¾udK we;=</head></p>

<p><head>ksfrdaId újdyfhka miafia yßu ld¾h nyq, fj,d fkao@</head></p>

<p><head>oeka b;ska biair jf. nEfka' mjq,la úÈyg jev l</head></p>

<p><head>ksfrdaId" k,skao .ek lsõjd ljqo fï k,skao@ fldfyduo
Tyq yuq jqfKa''''''@</head></p>

<p><head>ksfrdaId wef.a u;lh w;S;hg f.k .shd' weh iskduqiqj w;S;h
isysm;a l</head></p>

<p><head>k,skao iïm;a b,x.fldaka uf.a wdorKSh ieñhd thd hqo yuqod
lemagka flfkla' fg,s kdgHhl uqyqr;a W;aijhl§ wms fokakd uq,skau yuq jqKd' úlis;
ñ,skh frdc¾ fifkúr;ak oñ;d wfír;ak rÕmE tys miqìï .S .ehqfj uu wOHCIlf. ñ;=frla
úÈyg k,skao weú;a ysáhd'</head></p>

<p><head>idudkHfhka .eyeKq   fome;af;u wdYs¾jdoh ueoafoa wms újdy jqKd'</head></p>

<p><head>fyd|hs .dhsldjla jYfhka ckm%sh;ajfha ysKs fm;af;a isák Tn
ìß|la" .DyKshla f,i ,nk w;aoelSï i|yka lf</head></p>

<p><head>wfka wð;a oekg kï f,dl= w;aoelSula ,eì,d kE' ;ju újdyh
lshkafka fudloao lsh, wms f;areï .ksñka bkakjd' oekg m%Yak kE' iq¿ iq¿ m%Yak
mek ke.=k;a wms fokakd l;d lr,d tajd úi|d .kakjd' wkd.;h wog jvd fyd| fõú'
k,skao;a ld¾h nyq, wfhla' fï ojiaj, kï fld</head></p>

<p><head>ksfrdaId t;fldg f.or jevm</head></p>

<p><head>újdyhg fmr kï b;ska wïuf. w;ska jqfKa ta;a oeka uuu ta
foaj,a l</head></p>

<p><head>ksfrdaId oeka mÈxÑh;a fjkia lr,d k,skao;a tlal fjkuu
leoe,a,lg mshUd weú;a' ta .ek i|yka lrkak;a weh wu;l lf</head></p>

<p><head>oeka uu bkafka uf.a ksjfia' ^ta;a u|la l,amkd l</head></p>

<p><head>mqoa.,sl f;dr;=re .ek ksfrdaId l;d lroa§ uu wehg ndOd
l</head></p>

<p><head>f,dafl    yeurglu
me/Ks .S; /ma lrkjd' ta;a wfma rfÜ ;sfhkafka ir, ix.S;hg yqrejQ .S;' b;ska wfma
fï .S; /ma ix.S;hg .e,fmkafka kE' /ma lsÍu je/È fohla fkdfõ' ta;a oeka ;sfhk
/ma .S; ug ri ú¢kak nE' /ma lrkak mq¿jka .S; fudkjo neß .S; fudkjo lsh,d f;areï
fkd.ekSu ksid fyd| fudloao krl fudloao lsh,d      jegfykafk kE' /ma lsh,d wo l;d l   ;rï fohla /ma tfla lr, kE' .S;hla f;dar.kak
fmr tys ix.S;h mo rpkh /ma l%uhg uqiq lrkak iqÿiqo lsh, f;areï .kak ´fk'</head></p>

<p><head>fydfrka wrka /ma lrkak mq¿jka úosyg uu .S; .h, kE' uf.a
ks¾udK jeä yßhlau kS;suh /ljrKhla u; lr, ;sfhkafka' tksid uf.a leue;a; ke;sj ta
foa fldfyduj;a lrkak nE' ta;a uf.a .S;hg iqÿiq úÈyg /ma l%uh fhdokjd kï bÈßfha§
uf.a leue;a; ta i|yd ,nd fokak bv ;sfnkjd'</head></p>

<p><head>t;fldg Tfí .S; ldka;d yËlska msßñfhla .ehqfjd;a''''''@</head></p>

<p><head>tal ta whf. yelshdj' ta;a kkaod ud,kS" iqcd;d
w;a;kdhl jir .Kkdjla ÿla ú|, f.dvk.d.;a;= m%;srEmh fï .eyeKq yçka .hk msßñ
.dhlhka úiska tl ;;amrhlska úkdY lr, odkjd'</head></p>

<p><head>Tjqka ;ukaf.a yelshdjka fhdod .kak wdldrfha je/oaola
;sfhkjd' fïlg leiÜ ksIamdoljre;a j. lshkak ´kE' uqo,a ksid wfma m%ùk
.dhsldjkaf.a m%;srEmh hful= úkdY lrkjdkï tal mjqldr jevla'</head></p>

<p><head>ksfrdaId óú; yd l</head></p>

<p><head>uu újdy Wkd lsh,d risl m%;spdrj, wvqjla kï kE' uu widOdrK
úÈyg bÈßhg tkak W;aidy fkdl</head></p>

<p><head>ksfrdaIdf.a wÆ;a ksjfia ,smskh 99$80" mrK ã' wd¾' ´'
mdr l|dk weh risl m%;spdrj,g m%uqL ;ekla fok njo lSjd'</head></p>

<p><head>wð;a w,yfldaka</head></p>

<p> </p>






</body></text></cesDoc>