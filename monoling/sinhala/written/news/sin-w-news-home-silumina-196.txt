<cesDoc id="sin-w-news-home-silumina-196" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-home-silumina-196.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>නැවුම්
සුවඳින් අඩුවක් නැති</p>

<p>විචාර
ලිපි සරණිය</p>

<p> </p>

<p>-
එදා මෙදා තුර</p>

<p>(සාහිත්ය
සමාජ දේශපාලන විමර්ශන)</p>

<p>කර්තෘ
- ඩබ්ලියු. ඒ. අබේසිංහ</p>

<p>ප්රකාශනය
- ඇස්. ගොඩගේ සහ සහෝදරයෝ</p>

<p>මිල
- රුපියල් 150 යි.</p>

<p>සුප්රකට
සාහිත්යධරයකු මෙන් ම සාහිත්ය සමාජ හා දේශපාලනය ද සම්බන්ධයෙන් ප්රවීන  විචාරකයකු වූ ඩබ්ලියු. ඒ' අබේසිංහ සූූරීන්ගේ
ග්රන්ථාවලියට අලුතෙන් ම එක් වූ විචාර ග්රන්ථය 
'Wà »và lªy' pñp ¥¼np»{õ. Wv Rûl [²pmx ~Y~ { R¥l»l '61 ~f '89
nY{£ {« Y£z ry£~x lª {ùp {y R»J~¹xp ý~p zxp zã{ r§{l rl ~ ~`y£{p
r {« zr ~yÚ»xÃ. W»l Wv zr{z r{Üpp£ {« p¥{§K ~¨{q Rh¨{Y p¥Ü{ v á»pp
»õp Rûl »r£lY »z~ ¥¼npþ»K {y»nY »p£ »r»p. Wv zr Ãx{£ R¥Ü Rxfl, »p£
Ãx{£ R¥Ü Rxfl WY »~ y~£~{£nx rÚ~ v {p »õp »r£»l Rûl t{ {h£l á»p.
»t£»¤ ýa£yYxp ` »v {¥ë Y»zY »vv zr ~yÚ»x {Ñp£Yv »t»ýp {¥Õ »{õ.</p>

<p>මෙම
ලිපි සරණියෙහි එන 'යළි උපන්නෙමි' නවකතාව ගැන අබේසිංහයන් 1962 දී ලියා 'රසවාහිනි'
සඟරාවේ පළ වූ විචාරය අදටත් වලංගු යැ යි සිතමි. එය යළි උපන්නෙමි නවකතාව ලියූූ ගුණදාස
අමරසේකරයන් ද අද අනුමත කරනු ඇතැයි සිතිය හැකි ය. හෙන්රි ජයසේන ගේ 'අපට පුතේ මගක්
නැතේ' යන සුප්රකට නාට්යය විචාරය කරමින් අබේසිංහයන් 63 අගෝ: 23 දා සිරිලක පත්රයට ලියූූ
'අපට පුතේ මඟක් නැද්ද' යන විචාරය මෙම ලිපි සරණියට ඇතුළත් කොට ඇත. ඒ ගැන විශේෂ
සටහනක් කරන අබේසිංහයෝ "මා අතින් ලියැවුණු එම ලේඛනය පිළිබඳව මා තුළ ඇති
වන්නේ බලවත් ලජ්ජාවකි" යි සඳහන් කරයි.</p>

<p>එම
ලැජ්ජාව ඇති වූයේ ඒ කාලයේ අබේසිංහයන් ම සඳහන් කරන පරිදි තමා 'උන්මත්තක මාක්ස්
ලෙනින් වාදියකු වීම' නිසා නම් එය වෙන මා කාරණයක් වන නමුදු එදා ජයසේන ගේ නාට්යය ගැන
ලියැවුණු විචාරය ගැන නම් අදත් අබේසිංහයනට ලැජ්ජා වීමට තරම් සාධාරණ හේතුවක් ඇතැයි නො
සිතමි. එම විචාරයේ දිස්නය දෙන අන්තගාමි ස්වරූපය බැහැර කළ විට සමස්ත විචාරය ම අදටත්
බැහැර කළ හැකි යැ යි මා නම් සිතන්නේ නැත.</p>

<p>අබේසිංහයන්
'63 'සරසවිය' පත්රයට ලියූූ 'සිංහලයේ ශෘංහාර කවි කිහිපයක්' යන විචාරාත්මක රචනය මෙ කලට
වුවද බෙහෙවින් ප්රයෝජනවත් වන්නකි. එය ඉතා ම ආශ්වාදජනක විචාර ලිපියක් බව නො මසුරු ව
සටහන් කළ යුතු ය. පත්ර සංස්කාරකයන් විසින් අවිචාරශීීලීව පුම්බා මහත් කරන ලද ගුරුකුල
විචාරකයන් කිහිප දෙනෙකු නිසා සිංහල පද්ය සාහිත්යය ඉතා අභාග්ය සම්පන්න ඉරණමකට මුහුණ පා ඇති
මෙ කල අබේසිංහයන් ශෘංගාර කවි කිහිපයක් ගැන දැනට දශක තුනකටත් ඉහතදී කළ ප්රබුද්ධ විචාරය,
ඊනියා කාව්ය විචාරකයන් ගේ පමණක් නොව පත්ර සංස්කාරකයන්ගේ ද ඇස් පාද වන්නකි. අබේසිංහ
මහතා වැනි ශ්රේෂ්ඨ අපක්ෂපාතී විචාරකයකු අතින් මෙ සමයෙහි පද්ය රචනා ගැන විචාර ග්රන්ථයක්
කැරෙන්නේ නම් එය මහත් ඵල ආනිශංස ගෙන දෙන්නක් වනු ඇත.</p>

<p>'එදා
මෙදා තුර' සාහිත්ය සමාජ දේශපාලන විමර්ශන ග්රන්ථයෙහි එන සියලු ලිපි ගැන යමක් සඳහන්
කරන්නට මෙයින් අවකාශයක් නැත. එහෙත් මෙම ලිපි සරණියෙහි එන හැම  විචාර ලිපියක් ම නවීන ප්රවීන කවර කෙනකු වුව ද කියවා
බැලිය යුතු තරම් අගනා බව කිව යුතුම ය.</p>

<p>-
නිමල් හොරණ</p>






</body></text></cesDoc>