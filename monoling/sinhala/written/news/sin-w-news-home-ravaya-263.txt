<cesDoc id="sin-w-news-home-ravaya-263" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-home-ravaya-263.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>රුපියලේ අගය ක්රමිකව පහළ වැටෙමින් පවතී.
පවුම් හා යුරෝ අගයයන් කලින් සතිවලට වඩා විශාල ලෙස අවප්රමාණයවීමක් නිරීක්ෂණය කළ
හැකිය. නමුත් එම මුදල්වලින් දේශීය වෙළෙඳ පොළට ඩොලර් තරමට බලපෑමක් නැත.</p>

<p>දත්ත-මර්කන්ටයිල් මර්චන්ට් බැංකුව</p>

<p>ඇදුම රෝගය පිළිබඳව ව්යාපාරික අවධානය</p>

<p>මැයි මස 03 වැනි දිනට යෙදෙන ''ලෝක ඇදුම
දිනය" නිමිති කොට ඇදුම රෝගය පිළිබඳව ශ්රී ලාංකිකයන් දීනුවත් කිරීමේ අරමුණින්
පසුගිය සතියේ කොළඹ දී මාධ්ය හමුවක් පවත්වන්නට යෙදිණි.</p>

<p>ඇදුම රෝගය පිළිබඳ ශ්රී ලංකා
විශේෂඥයන්ගෙන් සමන්විත ශ්වසන රෝග අධ්යයන කණ්ඩායමේ අනුමැතිය මත, ග්ලැක්සෝ
ස්මිත් ක්ලයින් සමාගම විසින් මෙම මාධ්ය හමුව සංවිධානය කර තිබිණි.</p>

<p>මෙහිදී අදහස් දීක්වූ බදුල්ල රෝහලේ වෛද්ය
කීර්ති ගුණසේකර මහතා ඇදුම රෝගය, පුද්ගල ජානයන්හි පවත්නා අන්තර්
ක්රියාකාරීත්වයක් හා පරිසරයෙහි පවත්නා බලපෑ හැකි වෙනත් සාධක නිසා හට ගන්නා රෝගයක්
බැව් දීනට සොයාගෙන තිබෙන බැව් කීවේය.</p>

<p>දීනට ශ්රී ලංකාව තුළ ඇදුම රෝගීන් දස
ලක්ෂයකට වඩා සිටින බැව් රෝහල්වලදී කරන ලද සමීක්ෂණ මගින් හෙළිදරව් වී තිබෙන බවත් 1990
දී වාර්තාගතව සිටි ඇදුම රෝගීන් 80,454 දෙනා දශකයක් තුළදී 163,353ක්
වී, දෙගුණයක් බවට පත්ව ඇතැයි පැවසුවේය.</p>

<p>තවද මේ කාලසීමාව තුළ ඇදුම රෝගය නිසා
සිදුවූ වාර්තාගත මරණ සංඛ්යාව ද 4 ගුණයකට ආසන්නව ඉහළ ගොස් ඇති බැව් ද කීවේය.</p>

<p>වත්මන, ලෝක ජනගහණයෙන් දස ලක්ෂ 100
සිට 150 දක්වා ජනයා ඇදුම රෝගයට ගොදුරු වී ඇති බවත්, වසරක් පාසා එම සංඛ්යාව
වැඩි වන ප්රවණතාවක් පවත්නා බවත් සෑම ළමුන් 10 දෙනෙකුගෙන් එක් ළමයෙක් ද,
වැඩිහිටියන් 07 දෙනෙකුගෙන් එක් අයෙක් ද ඇදුම රෝගියකු බවට පත්ව ඇතැයි ද වැඩිදුරටත්
පැවසුවේය.</p>

<p>ඇදුම රෝගය නිසා වසරක් පාසා ලොවෙහි මියැදෙන
සංඛ්යාව ලක්ෂ 1.8කට ආසන්නව පවතියි. රෝගය පිළිබඳ දීනුවත්භාවය දුබල වීම, නිසි
ඕෂධ භාවිත කිරීමට නොපෙළඹීම, පිළිවෙත් නොපිළිපැදීම, පිළියම්
කරන වෘත්තිකයන් රෝගයෙහි ඇති බරපතලකම පිළිබඳ නිසි සැලකිල්ලක් නොදීක්වීම මරණ
සංඛ්යාව වැඩිවීම කෙරෙහි බලපා ඇති බව හෙළිදරව් වී ඇත.</p>

<p>ඇදුම රෝගය ලෝක සෞඛ්ය තලයේ පොදු
වැදගත්කමින් යුතු ප්රධාන රෝගයක් ලෙස ලෝක සෞඛ්ය සංවිධානය පිළිගෙන ඇති
අතර, රෝගය වැළඳීම හා ව්යාප්තියට ඇති හේතු සාධක අනාවරණය කරගැනීමට කටයුතු
කරගෙන යන බැව්ද, අසාත්මික කළමනාකරණය මගින් රෝගය මැඩ පැවැත්වීමට ගතහැකි
පියවර පිළිබඳ උපාය මාර්ගයන්හි නියැලෙන බව ද වැඩිදුරටත් ඔහු පැවසුවේය.</p>

<p>ලෝක ඇදුම දිනය පිළිබඳ සාකච්ඡාවට සහභාගි
වූ වෛද්යවරුන් පිරිස</p>

<p>2001 වසර සඳහා හොඳම ගුවන් සේවාව එමිරේට්ස්</p>

<p>නව මස් කාලසීමාවක් තුළ ලෝ වටා පවත්වනු
ලැබූ ගුවන් මගීන් ලක්ෂ 27ක් පාවිච්චි කළ ඡන්දයකින් ඩුබායි කේන්ද්ර කොටගත් ජාත්යන්තර
ගුවන් සේවා සමාගමක් වූ එම්රේට්ස් 2001 වසරේ හොඳම ගුවන් සේවාව ලෙස තෝරාගෙන ඇත.</p>

<p>ගුවන් මගී ඡන්දදායකයිනට තම රට මූලික
කොටගත් ක්රියාත්මක ගුවන් සේවාවක, ගුවන් තොටුපළ පිළිබඳවත් යානයේ
ආසනවල තත්ත්වයත්, ගුවන් ගමනේදී හා ඒ තුළ දී ආහාරයෙන් සංග්රහය
කිරීමත්, කැබින් ම'ුල්ලේ ආගන්තුක සේවා තත්ත්වයත් ඇගැයීමට භාජනය කරන මෙන්
ඉල්ලා සිටින ලදී. </p>

<p>27 ලක්ෂයේ ඡන්දයේ 32%ක් නිතිපතා ගුවන්
ගමනේ යෙදුණු හෝ පළමු පන්තියේ ව්යාපාරික කොටසට ද, 24%ක් පිරිවැසුම්
පන්තියේ සංස්ථාපිත හේතූන් සඳහා ගමනේ යෙදුනවුන්ද, 44%ක් විවේකය සඳහා පිරිවැසුම්
පන්තියේ ගුවන් ගත මගීන්ගෙන් ද සමන්විත විය.</p>

<p> </p>

<p>ලින්ක් නැචුරල් 9001 දිනා ගනී</p>

<p>ආයුර්වේද ඕෂධ නිෂ්පාදන ක්ෂේත්රයේ ප්රමුඛතම
දේශීය ආයතනයක් වන සීමාසහිත ලින්ක් නැචුරල් ප්රොඩක්ට්ස් සමාගම ෂීධ 9001 ජාත්යන්තර
තත්ත්ව කළමනාකරණ පද්ධති සහතිකය දිනාගෙන ඇත.</p>

<p>නව නිෂ්පාදන ප්රවර්ධනය, නිෂ්පාදන
ක්රියාවලිය, තත්ත්ව කළමනාකරණය, පාරිභෝගික සේවාව වැනි අංශයන් පරීක්ෂා
කර ඒවාහී විශිෂ්ටත්වය සලකා බලා ෂීධ 9001 සහතිකය ලින්ක් නැචුරල් ප්රොඩක්ට්ස් ආයතනය
වෙත පිරිනමා ඇත්තේ මහා බි්රතාන්යයේ ප්රධානතම තත්ත්ව ඇගැයුම් ආයතනයක් වන ීඨී යාර්ස්ලි
ඉන්ටර්නැෂනල් සර්විසස් ආයතනය හා යුනයිටඩ් කින්ඩම් ඇක්රඩිමේෂන් සර්විස් :ඹණ්ී- ආයතනය
විසිනි.</p>

<p>දශක දෙකක පමණ ඉතිහාසයක් ඇති ලින්ක්
නැචුරල් ප්රොඩක්ට්ස් සමාගම ආයුර්වේද ඕෂධ විශාල ප්රමාණයක් නිෂ්පාදනය කරන අතර,
මහා බි්රතාන්ය, ජපානය හා මැද පෙරදිග රටවලට ද සිය නිෂ්පාදන අලෙවි කරයි. මීට අමතරව
ලින්ක් නැචුරල් ප්රොඩක්ට්ස් (ඉන්දියා) සමාගම ආයතනයේ නිෂ්පාදන ඉන්දියාවේ අලෙවි
කරයි.</p>

<p> </p>

<p>පොලී අනුපාත ඉහළ යාම සහ බදුබර වැඩි කිරීම</p>

<p>රජයේ ආදායම වැඩිකර ගැනීම අද ප්රධාන සාධකයක්
වී ඇත. රජයේ වියදම දිනෙන් දින ඉහළ යන වාතාවරණයක් යටතේ රාජ්ය අංශයේ වියදම් බොහෝ
දුරට සීමා කරමින් පවතින අතර ඉදිරි සය මාසය තුළදී රාජ්ය ප්රාග්ධන වියදම් ණය බොහෝ දුරට
සීමා කිරීමට ඉඩ ඇත.</p>

<p>රාජ්ය අංශයේ සේවකයන්ගෙන් උපයන විට බදු අය
කිරීම, මන්ත්රීවරුන් ඇතුන්ථ සම්භාවණීය බදු නොගෙවන අය බදු බර යටතට ඇතුළත් කිරීම
යනාදී බොහෝ යෝජනා විමර්ශනයට ලක්වෙමින් පවතී.</p>

<p>නැවත වතාවක් උතුරේ යුද්ධය ආරම්භ වී ඇති
නිසා අලිමංකඩ ප්රහාරය වැනි ප්රහාරයක් එල්ල වී යැයි සැකයක් ද ආරක්ෂක අංශ තුළින්
මතුවී ඇත.</p>

<p>මෙම තත්ත්වය යටතේ ආරක්ෂක අංශ සඳහා වෙන්
කරනු ලබන වැය ශීර්ෂවල කිසිදු කපා හැරීමක් බලාපොරොත්තු විය නොහැකි අතර, ශ්රී
ලංකා රජයේ දීනට පවතින අධික ණය බර පියවා දීමීම සඳහාත්, ඩොලරයේ ශීඝ්ර අවප්රමාණය
වීම මත රාජ්ය ණය ප්රමාණය රුපියල්වලින් ගණන් ගැනීමේදී ඉහළ යාමත් බලවත් ප්රශ්නයක්ව
ඇත.</p>

<p>ශ්රී ලංකා මහ බැංකුව මගින් භාණ්ඩාගාර බිල්පත්
සහ භාණ්ඩාගාර ඇපකර සඳහා ඉහළ පොළියක් නියම කිරීමත්, සීමාරහිතව
භාණ්ඩාගාර බිල්පත් සහ ඇපකර නිකුත් කිරීම නිසා වාණිජ බැංකු මගින් ඉතා සුරක්ෂිත
ආයෝජනයක් ලෙස රජයට ණයදීමට මූලික ස්ථානයක් දී ඇත.</p>

<p>මෙම තත්ත්වය නිසා මූල්ය වෙළෙඳ පොළේ
පොළී අනුපාත නොනැවතී ඉහළ ගොස් ඇත. තැන්පතු පොළී අනුපාතයන් ඉහළ
යනවිට අත්තිකාරම් පොළී අනුපාතයන් ද ඉහළ යන අතර, කොටස් වෙළෙඳ
පොළේ පවතින අවපාත තත්ත්වය මත බොහෝ ආයෝජනයන් සිය කොටස් අලෙවි කරන ලද
ආදායම කෙටිකාලීන මූල්ය වෙළෙඳ පොළේ ආයෝජනය කරනු ලබයි.</p>

<p>සමහර රාජ්ය සංස්ථා පවා තම කොටස් අලෙවි කර එම
මුදල් භාණ්ඩාගාර ඇපකර සඳහා යොදවා ඇත.</p>

<p>වාණිජ බැංකු විසින් ද සිය ප්රධාන පරමාර්ථයන්
අමතක කර, එනම් සාමාන්ය ජනතාවට ණය අත්තිකාරම් ලබාදීම දෙවෙනි ස්ථානයේ තබා රජයට
ණයදීම සිය ප්රධාන ආදායම් මාර්ගය කරගෙන ඇත.</p>

<p>සමහර පෞද්ගලික බැංකු ණයකර පවා නිකුත්
කරමින් ලබාගන්නා මුදල් රාජ්ය ණය සඳහා යොදවන බව නොරහසකි.</p>

<p>මෙම තත්ත්වයෙන් ගොඩ ඒම සඳහා රජයේ
කෙටිකාලීන ක්රමෝපාය වී ඇත්තේ ඩොලර් මිලියන 253ක් ජාත්යන්තර මූල්ය අරමුදලෙන්
ලබාගැනීමය. රුපියල් කෝටි 3000ක මුල් වාරිකය ලැබීමත් සමග පොළී අනුපාතයන් අඩු වී
කොටස් වෙළෙඳ පොළ ක්රියාකාරීත්වය ඉහළ යා හැකි බව විශ්වාස කරයි.</p>

<p>මෙමගින් නැවත වතාවක් පොළී අනුපාතයන්
අඩු වීමත් සාමාන්ය තත්තවයට පත්වීමත් ඩොලරයට සාපේක්ෂව ශ්රී ලංකාවේ රුපියලේ අගය
ස්ථාවර විය හැකි බවත් අපේක්ෂා කරයි.</p>

<p>ඩොලරය නිදහස් කිරීමෙන් අනතුරුව ප්රථම වරට
පසුගිය සිකුරාදා ඩොලරයේ විකුණුම් මිල රුපියල් 91.20 දක්වා ඉහළ ගොස් ඇත.</p>

<p>විශාල වශයෙන් ආනයන මත යැපෙන රටක ඩොලරයේ
අග ඉහළ යාම එදිනෙදා ජන ජීවිතයට දීඩි බලපෑමක් කරනු ලබයි. ඒ හා සමගම ඉහළ යන භාණ්ඩ
මිල ගණන්වලට අනුරූපීව භාණ්ඩ සහ සේවා බද්ද ඉහළ ගොස් භාණ්ඩ මිලට එකතුවීම නිසා
පාරිභෝගිකයන් මෙන්ම ආයෝජකයන් ද දීඩි අපහසුතාවට පත්වන බව අමුතුවෙන් කිවයුතු
නොවේ.</p>

<p>අයවැය හිඟය අවම තත්ත්වයක පවත්වා ගැනීම
මූලික කරගෙන බදු පදනම තවදුරටත් ඉහළ දීමීමත්, බදු ආදායම් ඉහළ දීමීමත් යන සාධක
මත නොව, වෙනත් විකල්ප මාර්ග කෙරෙහි යොමුවීමට රජයට සිදුවෙනවා ඇත.</p>

<p>ආයෝජන මණ්ඩලය යටතේ නව කර්මාන්ත සහ
ව්යාපාර ආරම්භ කිරීමට සැලසුම් සකස් කරනු ලැබුවේ පරමාර්ථ කිහිපයක් පදනම් කරගෙනය.</p>

<p>(1) විදේශ ආයෝජන රට තුළට ගෙන්වා
ගැනීම සහ නව තාක්ෂණය ලබාගැනීම.</p>

<p>(2) දේශීය අමුද්රව්ය පරිහරණය කරමින් අමුද්රව්ය
වශයෙන් ආනයනය කළ භාණ්ඩ සඳහා වටිනාකම එකතු කිරීම</p>

<p>(3) අපනයන ආදායම ඉහළ නැංවීම</p>

<p>(4) රැකියා අවස්ථාවන් සුලභ කිරීම යන
කරුණු මතය.</p>

<p>එහෙත් අද මේ පරමාර්ථයන්ට පටහැනිව
වෙළෙ)ඃ960තම, සේවා ස්ථාන ආරම්භ කිරීම, ආපනශාලා සහ රෙස්ටොරන්ට් යනාදී
ව්යාපාර සඳහා මෙන්ම දේශීය වශයෙන් ඉතා හොඳින් පවත්වාගෙන ගිය කර්මාන්ත ද ආයෝජන
මණ්ඩල ව්යාපෘති වශයෙන් ආරම්භ කිරීමට පසුගිය කාලයේ අවසර දී ඇත. මෙම තත්ත්වය නිසා බදු
ගෙවමින් කර්මාන්ත පවත්වාගෙන යන කර්මාන්තකරුවන් දීඩි අපහසුතාවට පත්වන අතර, රජයට
ලැබිය යුතු බදු ආදායම ද අහිමි වනු ඇත.</p>

<p>විශාල ප්රමාණයේ සාප්පු සංකීර්ණ :ීයදචසබට
ඵ්කක- ආදිය පවා අද ආයෝජන මණ්ඩල ව්යාපෘති බවට පත්වී ඇත. මොවුන් විසින් කරනු
ලබන්නේ පිටරටින් මහා පරිමාණයෙන් භාණ්ඩ ආනයනය කර හෝ දේශීය වශයෙන් නිපදවා භාණ්ඩ
අලෙවි කිරීමය. මෙවැනි ව්යාපෘති සඳහා ආයෝජන මණ්ඩලයට වරප්රසාද ලබාදීම නිසා රජයේ
ආදායම විශාල වශයෙන් ඇහිරී යයි.</p>

<p>ආයෝජන මණ්ඩල ව්යාපෘති ගණන වැඩිකර වාර්තා
සැකසීම සඳහා මෙවැනි ක්රමයක් උචිත වන අතර, අප විසින් ඉහත සඳහන් කළ හැකි එකම
පරමාර්ථයක් හෝ ඉෂ්ට කර ගත හැකිද යන ප්රශ්නය නිතැතින්ම මතුවේ. මේ තත්ත්වය යටතේ
ඉදිරියේ දී ජාත්යන්තර මූල්ය අරමුදල විසින් කරනු ලබන යෝජනා පිළිගැනීමට රජයට සිදු
වුවහොත් සාමාන්ය ජන ජීවිතය විශාල අර්බුදයකට ගමන් කරනු ඇත.</p>

<p>එබැවින් රජය විසින් කළ යුතුව ඇත්තේ
කෙටිකාලීනව රජයේ ආදායම් මාර්ග වැඩි කර ගැනීමේ ක්රියාමාර්ග සඳහා යොමුවීමය. උදාහරණයක්
ලෙස වරාය අධිකාරිය විසින් හෝ රේගුව විසින් තහනමට ලක් කර ඇති අතිවිශාල වාහන ඇතුන්ථ
භාණ්ඩ තොග නිදහස් කරගැනීම, රජයේ සංස්ථා සහ දෙපාර්තමේන්තු සතුව පවතින පරිහරණය
කළ නොහැකි වාහන, උපකරණ ආදිය අලෙවි කිරීම, පෞද්ගලිකකරණය කරන ලද
ආයතනවලින් ලැබිය යුතු මුදල් ලබාගැනීම සහ විදේශ විනිමය කන්ථ කඩය වසා දීමීම ආදියයි.</p>

<p>ඊට අමතරව ඇත්ත වශයෙන්ම බදු ගෙවීම පැහැර හැර
ඇති මහා පරිමාණ ව්යාපාරිකයන් බදු දීලට හසුකර ගැනීම සඳහා දීර්ඝකාලීන වැඩපිළිවෙලක්
සකසා ගැනීමය. එසේ නොමැතිව තවදුරටත් සමාන්ය ජනතාව මත බදු බර පැටවීම සුදුසු නොවනු ඇත.</p>

<p>ලංකා හෝටල් සංස්ථාවෙන් දේශීය සංචාරකයන්ට
වැඩි පහසුකම්</p>

<p>වසර 25ක් පුරා ලංකා හෝටල් සංස්ථාවේ සේවයේ
නියුතුව සිටි සේවකයන්ට උපහාර පිදීම හා වාර්ෂික ක්රීඩා උත්සවය පැවැත්වීමත් ඉදිරි කාලසීමාව
තුළදී සංස්ථාවේ අපේක්ෂිත නව්යකරණ කටයුතු පිළිබඳ පැහැදිලි කිරීමත් අරබයා පසුගිය
සතියේ කොළඹදී මාධ්ය හමුවක් පැවැත්විණි.</p>

<p>මේ පිළිබඳ අදහස් දීක්වූ හෝටල් සංස්ථාවේ
සභාපති නාවලගේ බැනට් කුරේ මහතා, වසර 34ක කාලයක් තුළ දෙස් විදෙස් සංචාරකයන්
සඳහා නවාතැන් හා ප්රණීත ආහාර පාන සපයමින් සංස්ථාව පැසසුමට ලක්ව ඇති ආයතනයක්
බවත්, වත්මන් සංකල්පයක් අනුව දේශීය සංචාරකයන් සඳහා නව සහනදායක මිල ගණන් යටතේ
පළල් ලෙස සේවාවන් රැසක් එළිදීක්වීමට ක්රියාකරගෙන යන බවත් පැවසුවේය.</p>

<p>මෙම අරමුණ මුදුන් පමුණුවා ගැන්ම සඳහා සමූහ
වශයෙන් පැමිණෙන කණ්ඩායම්වලට නවාතැන් හා ආහාරපාන විශේෂ වට්ටම් සහිතව අඩු මුදලකට ලබාදීමට
කටයුතු කරන බවත් කීය. බීම වර්ගවල මිල දීනටමත් පහත හෙළා ඇත.</p>

<p>ප්රතිපත්තියක් වශයෙන් දීනට පවතින සංස්ථාවේ
හෝටල් හා තානායම් කාලීන අවශ්යතාවනට ගැලපෙන පරිදි නවීකරණය කිරීමට පියවර ගන්නා බවත්
විශේෂයෙන්ම දඹුල්ල, හබරණ හා සීගිරිය වැනි අන්තර්ජාතික ක්රිකට් ක්රීඩාංගණය පිහිටි
පෙදෙසේ ඇති තානායම් ඉල්ලූමට සරිලන පරිදි සංවර්ධන කෙරෙන බවටත් ඇල්ල,
බෙලිහුල්ඔය, කිතුල්ගල, සේරුවා හෝටලය හා තිස්සමහාරාම ආදි ස්ථානවල පිහිටි
හෝටල් හා තානායම් දීනටමත් නවීකරණය කර ඇති බවත් කියා සිටියේය.</p>

<p>මේ හැර, මෙතෙක් අඩුපාඩුවක් ලෙසින්
පැවති නුවරඑළිය නගරයේ නිවාඩු නිකේතනයක් හා තේ මධ්යස්ථානයක් පිහිටුවීම මගින්
විදේශීය සංචාරකයන් වැඩි ප්රමාණයක් ගෙන්වා ගැනීමට අපේක්ෂා කරන බවත්, වන
සංරක්ෂණයට ආපසු බාර දුන් දීෆාර් ඉන්" තානායමේ අලාභය පියවා ගැන්ම සඳහා තැන්න මත
හෝටලයක් ඉදිකිරීමට සැලසුම් කරන බවත් පින්නවල අලි අනාථාගාරය සඳහා පැමිණෙන සංචාරකයන්
සඳහා ද හෝටලයක් ඉදිකිරීමට කටයුතු කරගෙන යන බවත් පැවසුවේය.</p>

<p>ලංකා හෝටල් සංස්ථාව කිසිම බැංකුවකට ණය
නොවී, භාණ්ඩාගාර මුදල් ලබා නොගෙන ස්වකීය ඉපැයුම් මගින් යැපෙන ආයතනයක් බැව් ද
2000 වසර සඳහා ලබා ඇති ලාභය රුපියල් මිලියන 18ක් වන්නේ යැයි ද කියා සිටියේය.</p>

<p>සාකච්ඡාව සඳහා ආනන්ද සිරිසේන, වසීර්
ඩෝල්, රංජිත් බාලසූරිය හා එස්.යූ. කාසිම් යන මහත්ම, මහත්මීන් ඇතුන්ථ සංස්ථා
නිලධාරීන් පිරිසක් ද සහභාගි වූහ.</p>

<p>කොටස් වෙළෙඳ පොළ තවමත් දුර්වල මට්ටමක
පවතී. ගනුදෙනු සීමිතය. විදේශ උනන්දුව අඩුය. පිරිවැටුම සාමාන්ය මට්ටමක පැවතියත්,
මිලදී ගැනීම සඳහා දේශීය සුන්ථ ආයෝජකයන්ගේ ඉල්ලීම් සීමා වෙන සෙයකි. සියලූ අවස්ථාවන්
සමග සසඳන විට කොටස් මිල ගණන් අඩු වී තිබෙන මේ සමය ආයෝජකයන්ට දීර්ඝ සහ මධ්ය කාලීන
ආයෝජන ලෙස මුදල් යෙදවිය හැකිය.</p>

<p> </p>

<p> </p>

<p> </p>






</body></text></cesDoc>