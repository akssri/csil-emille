<cesDoc id="sin-w-news-home-dinamina-5" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-home-dinamina-5.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head>ÈkñK 2000 iema;eïn¾ 20 </head></p>

<p> </p>

<p>උපාලි
ඇස් ධර්මබන්දු </p>

<p> </p>

<p>වීසා
බලපත්ර ලබා ගැනීමේදී තැරැව් කාරයන්ට අසුවීමේ හෝ අයදුම්කරුවන් බොහෝ වේලාවක් පෝලිම්වල
රස්තියාදු වීමේ හෝ අවස්ථාව තුරන් කොට වීසා අයදුම්පත් වඩාත් ඉක්මණින් හා පහසුවෙන්
සැකසීමේ ක්රමයක් ලබන 26 වැනිදා පටන් ශ්රී ලංකාවේ අමෙරිකානු තානාපති කාර්යාලය ආරම්භ
කරයි. </p>

<p>නව
ක්රමය යටතේ වීසා අයදුම්කරුවන්ට හෝ ඔවුන්ගේ නියෝජිතයන්ට සදුදා පටන් සිකුරාදා දක්වා
සවස 1 සිට 3 තෙක් වීසා අයදුම්පත් ඉදිරිපත් කොට වීසා පරීක්ෂණය සඳහා දිනයක් ලබාගත හැකි වනු
ඇත. වීසා අයදුම්පත් සැකසීම සඳහා අමෙරිකානු ඩොලර් 45ක් හෝ ශ්රී ලංකා මුදලින් රුපියල්
3600ක් හෝ අය කෙරේ. </p>

<p>වීසා
අයදුම්පත භාරදී යථෝක්ත වීසා අයදුම්පත්ර ගාස්තුව ගෙවූ පසුව වීසා පරීක්ෂණය පැවැත්වෙන දිනය
හා වේලාව දීක්වෙන අංකනය කළ (නොම්මරයක් සහිත) ටිකට් පතක් අයදුම්කරු වෙත නිකුත්
කෙරේ. ශ්රී ලංකාවේ අමෙරිකානු තානාපති කාර්යාලයේ ප්රදොත (කොන්සියුලර්) අංශයේ අධිපති
විලියම් ඇම්.හෝව් මෙම නව ක්රමය හඳුන්වා දීම පිළිබඳව ඊයේ උදේ අමෙරිකානු තොරතුරු
මධ්යස්ථානයේදී පැවති පුවත්පත් සාකච්ඡාවේදී කරුණු දක්වමින් නව ක්රමය යටතේ වීසා අයදුම් පත්ර
ගාස්තුව ගෙවීම, අයදුම්පත ඉදිරිපත් කිරීම හා වීසා පරීක්ෂණයට දිනයක් හා වේලාවක් ලබා
ගැනීම, අයදුම්කරුට පෞද්ගලික හෝ පණිවුඩකරුවකු ගමන් නියෝජිතයකු මිතුරකු හෝ
පවුලේ සාමාජිකයකු ලවා හෝ කරවා ගත හැකි බව පැවසීය.</p>

<p>එහෙත්
වීසා පරීක්ෂණය සඳහා අයදුම්කරු පෞද්ගලිකව පෙනී සිටිය යුතු වේ. අනුමත කරන ලද අයදුම් පත්
වෙනුවෙන් වීසා බලපත්ර වීසා පරීක්ෂණය පැවැත්වෙන දිනයේදීම නිකුත් කෙරේ. </p>

<p>ප්රදොත
අධිපති නෝවේ මහතා පවසන අන්දමට නව ක්රමය ක්රියාත්මක කරනු ලැබීමට හේතු වූයේ වීසා
පෝලිම්වල තැන් මුදලට ලබාදීමට තැරැව් කරුවන් ඉදිරිපත් වී අයදුම්කරුවන් ගසා කෑමට දරණ
උත්සාහයන් වැළැක්වීමට යැයිද කිය. එමෙන්ම අයදුම්කරුවන්ට තම තමන්ගේ වීසා බලපත්ර
ලබාගැනීමට ගත වන කාලය නව ක්රමය යටතේ විශාල වශයෙන් අඩු වන බවද නෝවේ මහතා කීය. තම
තමන්ගේ වීසා අයදුම්පත්ර අනුමත කරනු ලැබූ අයදුම්කරුවන්ට වීසා බලපත්රද අතැතිව තනාපති
කාර්යාලයෙන් පිටව යෑමට හැකිවන බවත් ඔහු කීය.</p>

<p>වලංගු
විදේශ ගමන් බලපත්රයක් හා අයදුම්කරුගේ ඡායාරූපයක් සහ අමෙරිකානු තානාපති කාර්යාලයෙන්
සතියේ ඕනෑම දිනයක ලබා ගත හැකි අයදුම්පතක් පුරවා ඉදිරිපත් කොට වීසා සැකසීමේ ගාස්තුව වන
අමෙරිකානු ඩොලර් 45ක් හෝ එහි ශ්රී ලාංකික සමානුපාතිකය වන රුපියල් 3600/- ක් හෝ
ගෙවීමෙන් වීසා ගමන් බලපත්රයක් ඉල්ලුම් කළ හැකිය.</p>

<p>වීසා
අයදුම්පත ප්රතික්ෂේප කරනු ලබුව හොත් එසේ ප්රත්ක්ෂේප කිරීමට හේතුව දක්වා ප්රදොත
නිලධාරියා විසින් ලිපියක් නිකුත් කෙරෙනු ඇත. එහෙත් වීසා අයදදුම්පත් ගාස්තුව ආපසු
ගෙවනු නොලැබේ. මෙය ඇමරිකාවේ පමණක් නොව ලෝකයේ ඕනෑම රටක සිරිත බවත් නෝවේ මහතා
කීය. </p>

<p>ශිෂ්ය,
වෙළඳ ව්යාපාරික හා සංචාරක යන අංශ වෙනුවෙන් වීසා ගමන් බලපත් ඉල්ලා මේ වසරේ අයදුම්පත්
15,000ක් ශ්රී ලංකාවේ අමෙරිකානු තානාපති කාර්යාලය වෙත ඉදිරිපත් කොට තිබූ බවත් මෙය
පසුගිය වසරට වඩා සියයට 25 ක වර්ධනයක් පෙන්නුම් කරන බවත් නෝවේ මහතා කීය. අමෙරිකාවේ
සංචාරය කිරීමට බලාපොරොත්තු වන පුද්ගලයකු නව වැඩපිළිවෙල යටතේ කළ යුත්තේ
කොළඹ 03, ගාලු පාරේ අංක 44 දරණ ස්ථානයේ පිහිටි ශ්රී ලංකාවේ අමෙරිකානු
තානාපති කාර්යාලයේ (ටැලිපෝන් 421270, 421919) ප්රදොත අංශය වෙත ගොස්
සිය  විදේශ ගමන් බලපත්රය හා වීසා අයදුම්පත
භාරදී වීසා අයදුම්පත සැකසීමේ ගාස්තුව ගෙවා වීසා පරීක්ෂණය සඳහා දිනයක් හා වේලාවක් ලබා
ගැනීම ය. </p>

<p> </p>






</body></text></cesDoc>