<cesDoc id="sin-w-news-home-340" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-home-340.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>මහගමසේකර අනුස්මරණය ජනවාරි 15 දා</p>

<p>මෙරට විසූ කීර්තිමත් ගීත රචකයෙකු,
කවියෙකු, චිත්ර ශිල්පියෙකු, සිනමාකරුවෙකු හා සාහිත්යකරුවෙකු වූ මහගමසේකර
වියෝවී 26 වසරක් සපිරෙන්නේ ජනවාරි 14 වැනිදාටය. මෑතකදී පිහිටුවා ගන්නා ලද මහගමසේකර
පදනම මගින් මෙම අනුස්මරණ දිනය වෙනුවෙන් ජනවාරි 15 දා සවස 3.30ට මහගමසේකර අනුස්මරණ
දේශනයක් කොළඹ බණ්ඩාරනායක ජාත්යන්තර සම්මන්ත්රණ ශාලාවේ (කමිටු ශාලාව ඒ :්-) දී
පැවැත්වීමට කටයුතු සංවිධානය කොට ඇත. කාව්ය ශාස්ත්රය හා කාව්ය නිර්මාණය යන මැයෙන් මෙම
දේශනය පවත්වනු ලබන්නේ මහාචාර්ය ඒ.වී. සුරවීර විසිනි. මහගමසේකර අනුස්මරණ දිනය නිමිති
කොට ගෙන පාසල් හා විවෘත මට්ටමින් පවත්වන ලද කව්, ගී තරගාවලියේ ජයග්රාහකයනට හිමි
සම්මානය හා සහතික පත් ප්රදානය ද මෙදින සිදු කෙරේ.</p>

<p>මෙම උත්සවය අතරතුරේදී මහගමසේකර විසින් රචිත
ගීත කිහිපයක් නුගේගොඩ කලාභූමි ආයතනයේ සිසු සිසුවියන්ගේ හඬින් හා රෝහණ
වීරසිංහයන්ගේ සංගීත නිර්මාණයෙන් යුතුව ඉදිරිපත් කිරීමට ද සේකර පදනම කටයුතු යොදා ඇත.</p>

<p> </p>

<p>වැනීසියේ වෙළෙන්දා නව මුහුණුවරකින්</p>

<p>බන්දුල විතානගේ විසින් නිෂ්පාදනය කරන ලද
ෂේක්ස්පියර්ගේ වැනීසියේ වෙළෙන්දා වේදිකා නාට්යය නව මුහුණුවරකින් නැවත වේදිකා ගත
කෙරේ. නව නිර්මාණයෙහි විශේෂත්වය වන්නේ නවීන රංග වස්ත්රාභරණයෙන් නව ශිල්පීන් යොදවා
ඉදිරිපත් කිරීම ය. ශ්රී ජයවර්ධනපුර සහ කැලණිය විශ්ව විද්යාලයන්හි නාට්යකරණය හදාරණ සිසුන්
පිරිසක් මෙහි රඟපෑම සඳහා තෝරාගෙන තිබේ.</p>

<p> </p>

<p> </p>

<p>My Life චිත්ර දීක්ම</p>

<p>කැලණිය සෞන්දර්යය පීඨයේ ප්රථම වසර
ශිෂ්යයකු වන සත්සර ඉලංගසිංහගේ ඵහ Life
චිත්ර ප්රදර්ශනය ජනවාරි 12-14 ජාතික කලා භවනෙහිදී පැවැත්වේ.</p>

<p> </p>

<p> </p>

<p>හැඟුම්</p>

<p>සමිත රත්ගම සිව්වැනි ඒක පුද්ගල චිත්ර
ප්රදර්ශනය වූ ''හැඟුම්" ජනවාරි 12. 13 ජාතික කලා භවනෙහිදී පැවැත්වේ.</p>

<p> </p>

<p>මනකල නර්තන ප්රසංගය</p>

<p>නැටුම් ආචාරිණී යමුනා පීරිස්ගේ රද්දොන්ථගම
මනකල රංග පීඨය අට වැනි වරට ඉදිරිපත් කරන නර්තන ප්රසංගය ජනවාරි 12 ප.ව. 5.00ට බණ්ඩාරනායක
ජාත්යන්තර සම්මන්ත්රණ ශාලාවේහිදී පැවැත් වේ. සිසුවියන් 200ක් පමණ සහභාගී වන මෙම
ප්රසංගයෙහි ආරාධිතයන් වන්නේ ජැක්සන් ඇන්තනි යුවළ යි.</p>

<p> </p>

<p>අපට පුතේ මඟක් නැතේ</p>

<p>ජයවර්ධනපුර සරසවියෙහි කලා කවයට ආධාර පිණිස
හෙන්රි ජයසේනගේ ''අපට පුතේ මඟක් නැතේ" වේදිකා නාට්යය ජනවාරි 14 දා ප.ව. 6.30ට
කොළඹ ජෝන් ද සිල්වා සමරු රඟහලෙහි දී වේදිකා ගත කෙරේ.</p>

<p> </p>

<p>කාටුන් වැඩසටහන් සඳහා පිටපත් ඉදිරිපත්
කිරීමේ අවස්ථාවක්</p>

<p>úoHq;a udOH jevigyka iïmdokh
iïnkaOfhka f,dj lS¾;s kduhla ysñlr f.k isák ì%;dkHfha flaïì% weksfïIka
isiagïia iud.u iuÛ yjq,a j fgla Y%S ,xld wdh;khg wkqnoaO fgla úIka ,xld iud.u
úiska </p>

<p> </p>

<p>නිව්ටන් ගුණසේකර ලියූ</p>

<p>''වතු සුදු මල" දොරට වැඩුම</p>

<p>නිව්ටන් ගුණසේකර ලියූ පසළොස්වන
නවකතාව, ''වතු සුදු මල" දොරට වැඩීමේ උත්සවය 2002 ජනවාරි මස 15 වැනි අඟහරුවාදා
උදේ 10.00ට කොළඹ මරදානේ දයාවංශ ජයකොඩි සහ සමාගමයේ පොත් ප්රදර්ශනාගාරයේ දී
පැවැත්වේ.</p>

<p>''සකුරා මලක්, ''මුතු වරුසා",
''පවනක් වී එන්නම්", ''අහසයි ඔබ මට", ''ඉරබටු තරුව" සහ ''සරා සහ සුබා"
ඇතුන්ථ නවකතා රැසක් ඔහු විසින් මීට පෙර ලියා පළ කර ඇත.</p>

<p>''වතු සුදු මල" නවකතාව හඳුන්වාදීමේ දේශනය
මහාචාර්ය අනුර වික්රමසිංහ මහතා විසින් පවත්වනු ලබයි.</p>

<p>නිව්ටන්
ගුණසේකරගේ අත්සන සහිතව ''වතු සුදු මල" නවකතාව ජනවාරි 15 වැනි අඟහරුවාදා දවස පුරා
විශේෂ අඩු මිලකට පාඨකයිනට ලබාගැනීමට අවස්ථාව සලසා ඇති අතර, එදින දහවල් 12.00
දක්වා ඒ මහතා හමුවීම සඳහා ද කටයුතු පිළියෙල කර ඇති බව පවසන ප්රකාශක දයාවංශ
ජයකොඩි මහතා විශේෂ ආරාධනාවලින් තොර මෙම උත්සවය සියලූ සාහිත්ය රසකාමීන්ට විවෘත බව ද
පවසයි.</p>






</body></text></cesDoc>