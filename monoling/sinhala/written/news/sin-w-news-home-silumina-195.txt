<cesDoc id="sin-w-news-home-silumina-195" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-home-silumina-195.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>ප්රස්තාවනා</p>

<p> </p>

<p>ඉසිවර
දැක්ම</p>

<p> </p>

<p>ඉතා
දක්ෂ වේදිකා නාට්ය අධ්යක්ෂවරුන් ද, නන්ථ නිළියන් ද සෑහෙන පිරිසක් වර්තමානයේ අප රටේ සිටින
බව අපි දනිමු. මෑතකදී ස්වතන්ත්ර නාට්ය පිටපත් හිඟයක් පෙන්වූ නමුත් දක්ෂ පිටපත් රචකයෝ
කිහිප දෙනෙක් ද අප නාට්ය ක්ෂේත්රයේ සිටිති. වර්තමානයේ වේදිකා නාට්ය ක්ෂේත්රයේ ප්රවීණයකු
හෝ නවකයකු සමග කතා කළහොත් අසන්නට ලැබෙන මැසිවිලි, පැමිණිලි මෙවැනි ය.</p>

<p>"අපට
නාට්ය කලාව පවත්වාගෙන යන්න කාගෙන්වත් උදව්වක් නෑ. අඩුම වශයෙන් අපට නාට්යයක්
පුහුණුවන්න තැනක් නෑ. කොළඹ නගරෙම වුණත් නාට්යයක් පෙන්නන්ඩ සුදුසු නාට්ය ශාලාවකට
තියෙන්නෙ. එකයි. ඒකෙ ශාලා ගාස්තු අපට දරන්ඩ බෑ. අපේ රටේ තවමත් නාට්ය පාසලක් නෑ.
වෘත්තීය නාට්යකරුවො හැටියට නම් අපේ රටේ කොහොමටවත් පවතින්න බෑ. ණය වෙලා නාට්යයක්
හැදුවම ඒක ප්රේක්ෂකයා ගාවට ගෙනියන්ඩ නිසි යන්ත්රනයක් නෑ. ආණ්ඩුව නාට්ය කලාව වෙනුවෙන් කරන්නෙ
මොනවද? අපට දක්ෂකම් තියෙන රචකයො, අධ්යක්ෂවරු, නන්ථ නිළියො ඕන තරම් ඉන්නව. ඒ අයගෙ
දක්ෂකම්වලින් ප්රයෝජනයක් ගන්න කිසිම වැඩ පිළිවෙළක් නෑ.."</p>

<p>මේ
ආදි වශයෙන් තව බොහෝ මැසිවිලි පැමිණිලි නාට්ය ක්ෂේත්රයෙන් නිතර නිතර ඇසෙයි. නාට්යකරුවන්
කියන මේ කතා සියල්ල ඇත්තය. අපේ නාට්ය ලෝකයේ දුප්පත්කම ආණ්ඩුවට අයිති ජෝන් ද
සිල්වා රඟහලේ කැන්ටිමෙන් ම ප්රදර්ශනය වෙයි. එය තොට ළඟ පටේටි කඩයක් වැනිය. නාට්යවල මංගල
දර්ශන සඳහා වැඩි දෙනෙකු යන ලුම්බිණි රඟහල ඇතුළේ පැය දෙකක් ගත කිරීම දඬුවමකි. ප්රාදේශීීයව
නාට්ය රඟ දැක්වෙන ශාලාවලින් සමහර ඒවා කිරි වවුල් ගුහාවල්ය. සමහර ශාලාවල නන්ථවකු අඩි දෙකක්
උඩ පැන්නොත් හිස බාල්කයක හැපෙයි. ප්රාදේශීීය නාට්ය සංවිධායකයන් නාට්ය කරුවන්ට
'පොලුතියා' මාරු වෙන අවස්ථා ද සුලබය. අපේ නාට්ය ලෝකයේ දුප්පත්කම කියන්නට තව ඕනෑ තරම්
නිදසුන් ගෙනහැර පෑ හැකිය.</p>

<p>මේ
අතර අපට ආණ්ඩුව පාලනය කරන ටවර් හෝල් පදනමක් ද ආණ්ඩුව පත් කරන නාට්ය අනු මණ්ඩලයක්
ද තිබේ. ඒවාද මොන මොනවා හරි කරමින් නාට්ය කරුවන්ගෙන් බැණුම් අසයි. සෑම ආණ්ඩුවකම
ඇමතිවරු නාට්ය කලාව දියුණු කිරීම වෙනුවෙන් ආකර්ශනීය කතා පවත්වති. එහෙත් අපේ නාට්ය ලෝකය
දවසින් දවස දුප්පත් වෙයි.</p>

<p>අපේ
නාට්ය කලාව ද සාහිත්යය ද දියුණු කිරීම සඳහා කළ යුත්තේ කුමක් ද යන්න දැවෙන ප්රශ්නයක් වී
ඇත්තේ වර්තමානයේ ය. එවැන්නක් ගැන නාට්යකරුවන් අතර පවා සැලකිල්ලක් ප්රදර්ශනය නොවූ
කාලයක මාර්ටින්  වික්රමසිංහ නම් මහා ප්රාඥයා
ලියූූ දෙයක් මෙහි සටහන් කරන්නට සිතුවේ වර්තමාන නාට්යකරුවන්ගේත්   නාට්ය අනුමණ්ඩල සාමාජිකයන්ගේත් මැති ඇමැති,
නිලධාරීන්ගේත් අවධානයට දැන්වත් එය යොමු වනු ඇතැයි අපේක්ෂාවෙනි.</p>

<p>"දැනට
උවමනා කරන්නේ නාටක රචනයටත්, සම්පාදනයටත්, නන්ථකමටත් ශක්තිය ඇතියන් එක් කොට ඔවුන්ගේ
ශක්තීන් සංවිධානයෙන් සිංහල නාටකය ජාතික කලාවක් සේ දියුණු කළ හැකියන්ගේ ආයතන එකක්
නොවේ නම් දෙක තුනකි.</p>

<p>ආණ්ඩුව
හා සබඳකම් ඇති ආයතනයකින් නම් සිංහල නාටකය ජාතික කලාවක් සේ දියුණු කළ නොහැකිය.  අප රට පමණක් නොව අනික් සමහර රටවල ද ජාතික කලාව
දිමි කෙහල්, දිමි ගොටු,, මැලවුණු අතු පතර හා එල්ලුණු කුරුමිනි සතුන් ද ඇති රුකක් හා
සමානය. මේ සියල්ල පෑහීමෙන් ඒ  රුකෙහි
මල්ඵල හට ගැන්විය හැකිය. මැතියන්ට හා ඇමැතියන්ට ඇඟිලි ගසන්ට ඉඩ නොතබා ආණ්ඩුවේ
උපකාර ඇතිව පිහිටුවන ආයතනයක් හා කලාවන්ගේ දියුණුව පතන ධනවතුන්ගේ උපකාර ඇතිව
පිහිටුවන අදීන ආයතනයක් ද අර කාරිය සඳහා වුවමනාය."</p>

<p>මේ
කුඩා ඡේද දෙක බරපතල, සාකච්ඡාවකට ලක් කරන්නේ නම් වර්තමාන නාට්යකරුවා මුහුණ දෙන
ප්රශ්නවලින් කිහිපයකටවත් විසඳුම් සොයා ගත හැකි වනු ඇතැයි අපට සිතේ.</p>

<p> </p>

<p>ජයතිලක
කම්මැල්ලවීර</p>






</body></text></cesDoc>