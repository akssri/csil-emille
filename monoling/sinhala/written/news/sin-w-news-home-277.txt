<cesDoc id="sin-w-news-home-277" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-home-277.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>වසර 54ක් තරම් වූ ඉතා දීර්ඝ
ඉතිහාසයක් හා විශ්වවිද්යාල මට්ටමේ එනම් වාණිජවේදී උපාධිය හා සමානකමක් ඇති බව ගැසට් මගින්
නීතිගතකර ඇති උසස් ජාතික වාණිජ ඩිප්ලෝමා පාඨමාලාව සඳහා 1995/96 අධ්යයාපන වර්ෂය
සඳහා මම මහනුවර කාර්මික විද්යාලයට ඇතුළත්වීමි. එම පාඨමාලාව පූර්ණකාලීන වර්ෂ 04ක
පාඨමාලාවක් වන අතර එය දෙහිවල, මහනුවර, ගාල්ල, බදුල්ල හා යාපනය යන
දිස්ති්රක්ක 5ක පමණක් හැදෑරීය හැකි පාඨමාලාවකි. </p>

<p>1995 වන විට කම්කරු අමාත්යාංශයට
යටත්ව තිබූ මෙම පාඨමාලාව 1993 වන තෙක් උසස් අධ්යාපන අමාත්යාංශයට යටත්ව තිබූ
අතර, 1994 නව රජය පත්වීමත් සමගම එය කම්කරු අමාත්යාංශය යටතට පත්විය. එහි තිබූ ගැටන්ථ
හේතුකොටගෙන ගෙනගිය ඉල්ලීම් හා උද්ඝෝෂණවල ප්රතිඵලයක් ලෙසට යළිත් වරක් මෙම
පාඨමාලා 1997 සිට නැවත වරක් උසස් අධ්යාපන අමාත්යාංශය යටතට පත්කරගෙන ශී්ර ලංකා උසස්
තාක්ෂණ ආයතනය නමින් අන්ථත් ආයතනයකට අනුයුක්ත කරන ලදී. එයට සියන්ථම ඩිප්ලෝමා පාඨමාලාවන්
අයත් කරන ලදී. </p>

<p>එහිදී, ඒ යටතට උසස් ජාතික
වාණිජ ඩිප්ලෝමාවද හසුවූ අතර, ඉන් පසුව සිදුවූ ප්රතිසංස්කරණයන් නිසා අපි ඉමහත්
අපහසුතාවයකට පත්වීමු. එනම් අපගේ පාඨමාලාව 1947 සිට පැවත ආ අතර එහිදී 1993 වසරේදී එහි
විෂය නිර්දේශය වෙනස් කර නව නිර්දේශය ලෙසින් නම් කරන ලදී. පළමු හා දෙවන වසර විභාග
අදාළ ප්රාදේශීය ආයතන මගින්ද, තෙවන හා අවසන් වසර විභාග පොදු විභාගයක් ලෙස
විභාග දෙපාර්තමේන්තුව මගින්ද පවත්වනු ලැබිණි. </p>

<p>එහෙත්, නැවතවරක්
උ.ජා.වා.ඩි. පාඨමාලාව 1998දී පමණ එහි නම, උසස් ජාතික කළමනාකරණ ඩිප්ලෝමා ලෙස
නම් කර නව විෂය නිර්දේශයක් හා නීති පද්ධතියක් යටතට පත්කර තිබේ. එහි සියන්ථම විභාගයන්ද
උසස් තාක්ෂණ ආයතනය මගින් පවත්වනු ලබන බව සඳහන් කර තිබේ. </p>

<p>මේ හේතුව නිසා උ.ජා.වා.ඩි.
හැදෑරූ සිසුන්ගේ තෙවන හා අවසන් වසරවල විභාග මේ වනතුරුත් පැවැත්වූයේ නැත. ඒ
පිළිබඳව කිහිප විටක්ම උසස් තාක්ෂණ ආයතනයේ අධ්යක්ෂතුමා මගින් විමසුවද ඒවා යථා
කාලයේදී පවත්වන බව දීන්වූ අතර, 2000 දෙසැම්බර් 11 වන දින උසස් තාක්ෂණ ආයතනයේ
අධ්යක්ෂතුමා මහනුවර උසස් තාක්ෂණ ආයතනයට පැමිණිවිට සාකච්ඡාවක් පැවැත්වූ අතර, එහිදී
එතුමා කියා සිටියේ විභාග දෙපාර්තමේන්තුව හා සාකච්ඡා කර එම සියන්ථ විභාග 2001 මැයි මස
අවසන් වන විට හෝ මැයි මස ඉක්ම යාමට පෙර පවත්වා අවසන් කරන බවයි. </p>

<p>එහෙත් දීන් අපට ඇති ගැටන්ථව
මෙයයි. දීන් 2001 අප්රේල් අවසාන වී මැයි ආරම්භ වී තිබේ. එහෙත් මෙම උ.ජා.වා.ඩි. විභාග
නොපැවැත්වීම නිසා අසරණවී පාඨමාලාව අවසන්කරගත නොහැකිව අතරමංව සිටින දිවයින පුරා විසිරී
සිටින සියලූම සහෝදර සහෝදරියන් වෙනුවෙන් මා උසස් තාක්ෂණ අමාත්යතුමාගෙන්ද, විභාග
කොමසාරිස්තුමාගෙන් හා උසස් තාක්ෂණ ආයතනයේ අධ්යක්ෂතුමාගෙන්ද ඉල්ලා සිටින්නේ මේ ගැන
ඔබගේ අවධානය යොමුවී හැකි ඉක්වමිනි අපව මෙම පාඨමාලාවෙන් නිදහස් කරන ලෙසය. </p>

<p>බුදු දහම වනසන ලංකාවේ......</p>

<p>බුදු දහම අවිච්චින්තව පැවත ආවේ
සිල්වත් භික්ෂුන් හා භික්ෂුනින් වහන්සේලා නිසාය. බුදු දහම අනුව හැඩ ගැසුනු නිසාම ශී්ර
ලංකා ද්වීපයට ධර්මද්වීපය යන නම ලැබුණි. </p>

<p>මෑතක සිට ශී්ර ලංකාවේ භික්ෂුන්
වහන්සේලාගේ වැඩි දෙනෙක් වාණිජකරණයට හසුවිය. එනිසාම බුදු දහමේ හරය පසෙක තබා
සුඛෝපභෝගී ජීවිත ගත කිරීමට පෙළඹුණි. බුද්ධ ප්රතිමාවන් කඩා බිඳ දමා නිදන් වස්තුන්
සොරාගන්නේ එනිසාමය. මෙතෙක් එවන් දේ කළ සොරුන් ගැන දීනගැනීමට නොහැකි වූයේ
වැටත් නිරත් ගොයම් කෑ නිසා විය හැකිය. </p>

<p>බුදුන් වහන්සේ දේශනා කළ චාම්
දිවිය වෙනුවට අද ඇතැම් භික්ෂුන් වහන්සේලා ගිහියන්මෙන් කොණඩ රැවුල් වවාගෙන
නරුමයන්මෙන් ජීවිත ගතකරති. එමෙන්ම ගිහියන් ලබාදෙන දානයෙන් යැපෙමින් අඹුදරුවන් පෝෂණය
කරති. තම භාර්යාවද සුඛෝපභෝගි වාහනයේ හිර්වාගෙන යන්නේ කිසිම ලජ්ජාවකින් තොරවය. </p>

<p>සමහර තලේබාන්වරු කරනුයේ
පන්සල් තුළ කානිවලා් පෙන්නා මුදල් ගැරීමය. එමෙන්ම විහාරයක් තුළ තිබිය යුතු
ශාන්ත පරිසරය පසෙක තබා ශබ්ද විකාශන යන්ත්ර සවිකර එකම ගෝෂාවක් පතුරුවා ගරානා ව්යාපාරයේ
යෙදීමය. </p>

<p>අන්ය ආගමිකයන් බුද්ධ ප්රතිමා විනාශ
කරද්දී විරෝධයපාන ගෞරවණීය ස්වාමින් වහන්සේලා ලංකාවේ බුදු දහමට වින කරන චීවරධාරී
තලේබාන්වරුන් ගැනද විරෝධය පාන්නේ නම් බුද්ධ ශාසනයේ විරස්තිථිය පිණිස එයම
හේතුවන්නේය. </p>

<p> </p>

<p>'මනුස්සකම අමතක කළවුන්ට උසාවියෙන් පාඩමක්"</p>

<p>දේපොළ හිමිකම උදෙසා බිරිඳට එරෙහිව සැමියා දික්කසාද නීතිය
වැරදි ලෙස උපයෝගී කර ගැනීම පිළිබඳ විශේෂිත නඩුවක් පසුගියදා අභියාචනාධිකරණයේ
විභාග විය. අභියාචනාධිකරණ විනිසුරුවරුන් වන නිහාල් ජයසිංහ හා ඩී. ජයවික්රම මහත්වරුන්
ඉදිරියේ විභාග වූ මෙම නඩුවට පාදක වූ කතාව කලාතුරකින් අසන්නට ලැබෙන වෘතාන්තයක් බර්ය.
එම නඩු තීන්දුව පාදක කරගෙන සැකසෙන ලිපියකි මේ. මෙහි එන නම් ගම් මනඃකල්පිතය</p>

<p> </p>

<p>චන්ද්රානි ගුණසේකර කිරිබත්ගොඩ ප්රදේශයේ විසූ පාසල්
ගුරුවරියෙකි. ඇය විවාහ වී සිටියේ රජයේ දෙපාර්තමේන්තුවක උසස් නිලයක් දීරූ විමලසේන
ගමගේ සමගය. මේ දෙදෙනා විවාහ වී වසර 25ක් පමණ ඉතාම සතුටින් හා සමාදානයෙන් වාසය
කළේ සැමියා ගේ නිවසේය. එහෙත් මේ දෙදෙනාට එක් කනස්සල්ලක් තිබිණි. ඊට හේතුව
දරුවන් නොමැති කමයි.</p>

<p>චන්ද්රානිට සහ විමලසේනට සෑහෙන දේපලක් තිබිණි. චන්ද්රානිගේ
පවුලේ සහෝදර සහෝදරියන් සිටියේ නැත. එහෙත් විමලසේනගේ පවුලේ සහෝදර
සහෝදරියන් කිහිප දෙනෙක් සිටියහ. චන්ද්රානි සහ විමලසේන යුවලගේ පවුල් ජීවිතයේ සැනසුම
පලාගියේ විමලසේන හදිසියේ රෝගාතුරවීම නිසාය. චන්ද්රානි තමන්ට හැකි පමණ ඉහළ වෛද්ය
ප්රතිකාර ඔහුට ලබාදීමට කි්රයා කළාය. ඇය අත තිබූ මුදලින් සෑහෙන ප්රමාණයක් සැමියා ගේ
අසනීපය වෙනුවෙන් වියදම් විය. එහෙත් චන්ද්රානි ඒ සියල්ල සතුටින් ඉටුකළේ සැමියාට තිබූ
භක්තිය නිසාය.</p>

<p>එහෙත් සියල්ල අවසන් වූයේ ඉතා ඛේද ජනක ආකාරයටයි.
විමලසේනට වැළඳී තිබූ රෝගයෙන් ඔහු මරණයට පත්විය. ඒ ගැන පශ්චාත් මරණ පරීක්ෂණයක්
පැවති අතර චන්ද්රානි සැමියාගේ මෘත දේශය භාරගෙන තමන්ට හැකි තරම් උසස් ලෙස සැමියාගේ
අවසාන කෘත්ය සිද්ධ කළාය. ඒ සඳහා චන්ද්රානිට සෑහෙන මුදලක් වියදම් වී තිබිණි. සැමියාගේ
මරණයෙන් පසුව පින්කම් කටයුතු කොට සැමියාට පින් පැමිණවීම් ආදිය සඳහාද චන්ද්රානි අවශ්ය
කටයුතු කළාය. ඉන් පසුව සිය නිවසේ චන්ද්රානි ජීවත්වූයේ තනිවමය. ගමේ පාසලේම ඇය
ගුරුවෘත්තියේ යෙදී සිටි නිසා ඇයට රාජකාරිය පහසුවෙන් කරගෙන යාමට අවකාශ ලැබිණි.</p>

<p>සැමියාගේ මරණින් සති කිහිපයකට පසුව දිනක් පළාතේ
පොලිසියේ කොස්තාපල්වරයකු ඇයගේ නිවසට පැමිණ අසවල් දින පොලිසියට පැමිණෙන්නැයි
චන්ද්රානිට දීනුම් දුන්නේය. නියමිත දිනට පොලිසියට ගිය චන්ද්රානිට පොලිස් ස්ථානාධිපතිවරයා
පැවසූයේ තරු විසිවන කතාවකි. චන්ද්රානි සැමියාගේ නිවසේ අනවසරයෙන් පදිංචිව සිටින බවට
පැමිණිල්ලක් ලැබී ඇති බව ස්ථානාධිපතිවරයා ඇයට කීවේය.</p>

<p>එම පැමිණිල්ල කර තිබුණේ ඇයගේ මියගිය ස්වාමි පුරුෂයා වූ
විමලසේනගේ සහෝදරියක වූ සුධර්මා නමැත්තියයි. තමන් විවාහයෙන් පසුව ගත වූ අවුරුදු 25 ම
සැමියා සමග ඉතාමත් සතුටින් හා සමාදානයෙන් ජීවත්වූ බවත් ඔහුගේ මරණයෙන් පසුව කසාද
උරුමය අනුව ඔහුගේ නිවසේ පදිංචිවී සිටීම වරදක් වන්නේ කෙසේ දීයි චන්ද්රානි පොලිස්
ස්ථානාධිපතිවරයාගෙන් ප්රශ්න කළේය.</p>

<p>ඒ අවස්ථාවේ එතැන සිටි පැමිණිලිකාරිය වූ සුධර්මා කීවේ
චන්ද්රානිගේ කතාව අමූලික බොරුවක් බවය. සැමියා මිය යන විට චන්ද්රානි ඔහුගෙන් දික්කසාද වී
සිටි අතර ඊට අදාල දික්කසාද සහතිකය තමන් ළඟ තිබෙන බවත් සුධර්මා කීවාය. පසුව ඇය
දික්කසාද සහතිකයේ පිටපත පොලිස් ස්ථානාධිපතිවරයා අත තැබීය. එම දික්කසාදය ලබාදී තිබුණේ
පළාතේ දිස්ති්රක් උසාවියෙනි. විමලසේන මිය යන්නට වසර 8 කට පමණ පෙර මේ දික්කසාදය
ලබාගෙන තිබිණි. එය පළාතේ දිසා අධිකරණය විසින් නීත්යානුකූලව ලබාදුන් සහතිකයක් මගින්
තහවුරු කර තිබීම නිසා චන්ද්රානි ප්රකාශ කරන්නේ අසත්යයක් බව පොලිස් ස්ථානාධිපතිවරයාගේ
විශ්වාසය විය.</p>

<p>තමන්ගේ හිසමත කඩා වැටුන මේ මරාලය නිසා බෙහෙවින් වික්ෂිප්ත
භාවයට පත්ව සිටි චන්ද්රානි හඬන්නට වූවාය. මෙය වංචාවක් බව ප්රකාශ කළ ඇය තමන් සැමියා
ගේ නිවසින් ඉවත්ව නොයන බවත් මීට එරෙහිව අධිකරණයට යන බවත් පොලිසියට දීන්වීය. මේ
ආරවුල බේරීමට තමන්ට හැකියාවක් නැති නිසා උසාවියෙන්ම ගැටන්ථව විසඳා ගැනීම හොඳ බව
පොලිස් ස්ථානාධිපති වරයාගේ අදහස විය.</p>

<p>ඒ අනුව චන්ද්රානි පළාතේ දිස්ති්රක් උසාවියට ගොස් තමන්ට
සිදුව තිබෙන අසාධාරණය පිළිබඳව පැමිණිලි කළාය. වංචාවකින් තමන් දික්කසාද කළ බව
කී ඇය සැමියාගේ දේපොළ ලබා ගන්නට ඔහුගේ සොහොයුරිය මේ උප්පරවැට්ටිය සිදුකර ඇති
බව සඳහන් කළේය.</p>

<p>නඩුව විභාගයට ගැනීමට නියමිතව තිබියදී චන්ද්රානි විශ්රාම වැටුප්
දෙපාර්තමේන්තුවට ගියේ සැමියාගේ විශ්රාම වැටුප වැන්දඹුව වන තමන්ට හිමි නිසා එය ලබා
ගැනීම පිණිසය. විශ්රාම වැටුප් දෙපාර්තමේන්තුවේ විෂය භාර ලිපිකරු මහතා සඳහන් කළේ
මියගිය විමලසේනගේ සොයුරිය වූ සුධර්මා මේ ගැන පැමිණිල්ලක් කර ඇති බවය. ඒ අනුව
චන්ද්රානි සහ විමලසේන දික්කසාද වී තිබෙන නිසා විශ්රාම වැටුප චන්ද්රානිට අහිමිවන බව ඔහු
පැවසීය. දික්කසාද සහතිකයේ පිටපතක්ද සුධර්මා විශ්රාම වැටුප් දෙපාර්තමේන්තුවට ලබාදී තිබූ
නිසා චන්ද්රානි එතැනදීත් අසරණ විය.</p>

<p>මේ නිසා ඇය සහනයක් අපේක්ෂා කළේ දිස්ති්රක් උසාවියෙනි.
නඩුව විභාගයට ගත් දිසා විනිසුරු තුමාට දික්කසාද සහතිකය ඉදිරිපත් කළ පසුව චන්ද්රානිගේ
නීතිඥයන් කීවේ මේ දික්කසාදය ලබාගෙන ඇත්තේ චන්ද්රානිගේ අනු දීනුමක් නොමැතිව බවය.
එහෙත් උගත් විනිසුරුතුමාට එය පිළිගන්නට තරම් හේතු නොතිබිණි. එතුමා සඳහන් කළේ
මේ ගැන කි්රයා කරන්ට දිස්ති්රක් උසාවියට බලයක් නැති නිසා ඇපැල් උසාවියට
(අභියාචනාධිකරණයට) ගොස් සහනයක් ලබාගන්නා ලෙසයි. ඒ අනුව චන්ද්රානි
අභියාචනාධිකරණයට පෙත්සමක් ඉදිරිපත් කරමින් සහන අයැද සිටියාය.</p>

<p>සුධර්මාද සටන අතහැරියේ නැත. ඇය මේ සටන ගෙනයන්නේ
චන්ද්රානිගේ සැමියාට (ඇයගේ සොහොයුරාට) අයත්ව තිබූ ලක්ෂ ගණනක දේපොළ
තමන්ට අයිතිකර ගැනීම සඳහාය. ඇය සුපුරුදු ලෙස අභියාචනාධිකරණයට පැමිණ චන්ද්රානි සහ
විමලසේන දික්කසාද වූ දික්කසාද සහතිකය ඉදිරිපත් කළේය. එය නඩුවේ ප්රබලතම සාක්ෂියයි.
අභියාචනාධිකරණය මේ සහතිකය නීත්යානුකූලව පළාතේ දිසා අධිකරණයෙන් නිකුත් කළ
සහතිකයක් බව පිළිගත්තේය.</p>

<p>එහෙත් චන්ද්රානි ගේ පාර්ශ්වය වෙනුවෙන් ඉදිරිපත් කරන කරුණු සහ
ලේඛන පිළිබඳව සැලකිල්ලෙන් විමසා බැලීය. අසාධාරණයක් සිදුව තිබේ නම් එය නිවැරදි
කිරීමට ඇපැල් උසාවියට බලය තිබිණි. </p>

<p>චන්ද්රානි සැමියාගෙන් දික්කසාද වූවායයි දික්කසාද සහතිකයේ
සඳහන් දිනයට පසුව දෙදෙනා එකට ජීවත්වූ බව තහවුරු කෙරෙන ලිපි ලේඛන ගණනාවක් චන්ද්රානිගේ
පාර්ශ්වය ඉදිරිපත් කළේය. සැමියා ගේ දේපොළක් වෙනත් කෙනෙකුට විකුණූ අවස්ථාවේ
බිරිඳ ලෙස එම ඔප්පුවේ සාක්ෂියට අත්සන් කර තිබුණේ චන්ද්රානිය. සැමියාගේ මරණ සහතිකයේ
භාර්යාවගේ නම ලෙස සඳහන් වූයේද ඇයගේ නමය. පශ්චාත් මරණ පරීක්ෂණයේ බිරිඳ ලෙස සාක්ෂි
'' විමලසේනගේ මරණය භාරගෙන තිබුණේද චන්ද්රානි විසිනි. මේ සියල්ල වාර්තාගත නිල ලේඛන
මගින් තහවුරු කළේය. ඒවාට අමතරව විමලසේනගේ මරණය පිළිබඳව පළකළ මරණ
දීන්වීම්වලද බිරිඳ ලෙස නම සඳහන් වූයේ චන්ද්රානිගේය. එසේම සැමියා සහ බිරිඳ එකම නිවසේ
ජීවත්වූ බව තහවුරු කෙරෙන ඡන්ද නාම ලේඛනය ඇතුන්ථ තවත් ලේඛන ගණනාවක්ම ඉදිරිපත් විය. ඉන්
වඩාත් විශේෂ ලේඛනය වූයේ චන්ද්රානි ගෙන් විමලසේන දික්කසාද වූවාට පසුව දිනෙක ඇයට
දරුවන් නොලැබීම පිළිබඳ වෛද්ය පරීක්ෂණයක් කිරීම සඳහා විමලසේන කි්රයාකර තිබූ බවට
සඳහන් වෛද්ය වාර්තාය.</p>

<p>එහෙත් එක් ස්ථානයක විමලසේන තමන් අවිවාහකයකු බව ප්රකාශ කර
ඇති බවට ලිපියක් විමලසේනගේ සොහොයුරිය වන සුධර්මා උසාවියට ඉදිරිපත් කළේය. එම
ලිපිය විමලසේන ලියා තිබුණේ විශ්රාම වැටුප් දෙපාර්තමේන්තුවටය. දික්කසාද සහතිකය ලබා
ගත්තාට පසුව එය ලියා තිබිණි.</p>

<p>අභියාචනාධිකරණය ඉදිරියේ ඉතාමත් කලාතුරකින් ඉදිරිපත්වන මෙවැනි
නඩු දීඩි ලෙස පරීක්ෂා කිරීමත් සියුම්වත් විමසා බැලේ. ඉදිරිපත්වූ කරුණු අනුව දික්කසාද
සහතිකය නිවැරදි ලේඛනයක් වුවත් මෙහි කිසියම් සැඟවුණ දෙයක් ඇති බව වටහා ගත්
අභියාචනාධිකරණයේ උගත් විනිසුරු දෙපොළ මේ ගැන විශ්ලේෂණාත්මක විග්රහයක් මෙන්ම
පරීක්ෂණයක්ද කළේය.</p>

<p>පරීක්ෂණය අවසානයේ හෙලිදරව් වූයේ අපූරු කරුණකි. විමලසේනට
හා චන්ද්රානිට දරුවන් නැති නිසා විමලසේනගේ ඇවෑමෙන් ඔහුගේ දේපොළ බිරිඳට හිමිවීම
වැලැක්වීමට විමලසේන සහ සුධර්මා අනුගමනය කර ඇත්තේ කදිම උපායකි. විමලසේන සිය
කැමැත්තෙන් පළාතේ දිසා අධිකරණයේ දික්කසාද නඩුවක් පවරා තිබුණේ බිරිඳගෙන් දික්කසාද
වීමටයි. එහෙත් විමලසේන සහ ඔහුගේ බිරිය වූ චන්ද්රානිගේ ලිපිනයන් ලෙස ලබාදී තිබුණේ ව්යාජ
ලිපිනයන්ය.</p>

<p>දික්කසාද නඩුව උසාවියේ විභාගයට ගන්නා දිනයට විමලසේන උසාවියට
පැමිණෙයි. බිරිඳ මේ ගැන නොදන්නා නිසා ඇය උසාවි ආවේ නැත. වාර ගණනාවක් නඩුව කැඳවූ
පසුව විනිසුරු තුමාගෙන් ආයාචනයක් කළ විමලසේන සිතාසි භාරදී ඇතත් බිරිඳ ඒ ගැන
නොතකා උසාවි නොපැමිණි බැවින් ඒක පාර්ශ්විකව නඩුව අසා තමන්ට දික්කසාදය ලබා දෙන ලෙස
ඉල්ලා සිටියේය. උසාවිය එම ඉල්ලීම පිළිගෙන විමලසේනට දික්කසාදය ලබාදුනි. නමුත්
ඉන්පසුවද විමලසේන හා චන්ද්රානි සතුටින් හා සමාදානයෙන් එකම නිවසේ වාසය කළහ.
විමලසේන මේ ගැටය ගැසුවේ සිය මරණයෙන් පසුව දේපොළ සහෝදරියට ලබාදීමටයි.</p>

<p>මේ කරුණු සියල්ල නිසිලෙස එළිදරව් කරගත් ඇපැල් උසාවිය
චන්ද්රානිගේ සියලූ අයිතිවාසිකම් තහවුරු කළේය. සැමියාගේ විශ්රාම වැටුප ඇයට හිමිකර
දුනි. ඔහුගේ දේපොළ හිමිකම ද බිරිඳට පැවරුනි. එම තීරණයෙන් පසුව චන්ද්රානි උසාවියෙන්
පිටව ගියේ යුක්තිය ඉටුකර ගත් ගැහැනියක ලෙසය. නීතිය සාධාරණවත් විචක්ෂණවත්
කි්රයාකළ නිසා චන්ද්රානිට සහනයක් ලැබිණි. එහෙත් දේපොළ හා වස්තු සම්පත් ඉදිරියේ
විනාශවන මනුස්සකම නගා සිටුවන්නට නීතියට පුන්ථවන්ද යන්න ප්රශ්නයකි.</p>

<p> </p>

<p>කරන්දෙණියේ
මරණ හතර</p>

<p>කන්ථගඟට
පැන  මියගිය සිව්දෙනා</p>

<p>කරන්දෙණිය අන්ධකාරයේ ගිලී
තිබුණේය. පාරවල් පාන්ථයි. මිනිස් පුන්ථටක්වත් සොයාගත නොහැකි තරමට ගමම පාන්ථවට ගොස්ය.
ගම් වැසියෝ කිසිවක් නොවූ නියායෙන් තම ගමේ සිව් දෙනෙකු කන්ථතර'' කන්ථ ගඟට පැන්න කතාවේ
සමීප දසුන් දොරන්ථගුලාගෙන රූපවාහිනී නාලිකා ඔස්සේ කරඹති.</p>

<p>අපි එසේ  මියගිය සිව් දෙනාගේ මහගොඩ නිවසේ ආලින්දයට වී
බලා සිටියෙමු.</p>

<p>ඒ මරණ පමණක් නොව ඒ ජීවිතද මේ
ගමට ආගන්තුක වී තිබෙන්නේ යැයි අපට සිතෙයි. නැතහොත් ඒ නිවස මෙතරම් පාන්ථවට යන්නට විදියක්  නැත.</p>

<p>''ඒ ගෙදර රෑට නම් යන්න එපා. ගමේ
කිසි කෙනෙක් යන්නෙත් නෑ. ගෙදර හොල්මන් තියෙනවලූ. ළමයි අඬන ශබ්ද විලාප තියන ශබ්ද
ඇහෙනවලූ. ඒ වගේම වත්තේ නයි හත් දෙනෙක් ඉන්නවලූ. කොයි වෙලාවේ කොහෙන් මතු වෙයිද
කියන්න බැරිලූ'' ඒ ගේ  ගැන අපට ලැබුණු
ආරංචි  එසේය.</p>

<p>එහෙත් සිත් සසල කර නොගෙන මහරෑම
අපි ඒ නිවසට ගියෙමු.</p>

<p>ඔබ විශ්වාස කළ යුතුය. ඒ
මළ ගෙදර කිසිවකු හෝ සිටියේ නැත. දොරගුන්ථ ලා තිබිණි. ජීවත්ව සිටියදී ඇසුරු
නොකළවුන් මළ පසු කුමට ආශ්රය කරන්න දීයි'' ඔවුන් සිතුවාදීයි අපට සිතෙයි.</p>

<p>අපි යළිත් පාරට ආවෙමු. අසල
නිවෙසකට කතා කළෙමු. මැදිවියේ ගැහැනියක් ගෙයින් එළියට ආවාය. මළවුන්ගේ
නිවසේ ආලින්දය ඒ නිවෙසට පෙනෙයි.</p>

<p>''ඒ අය අපේ ළඟම නෑදෑයෝ
තමයි. ඒත් කොහේ ගියාද මක්වුණාද කියලා අපි නම් දන්නේ නෑ. අපිට  ඒ අය කියන්නෙත් නෑ. අපි දීක්කෙත් පත්තරෙන්"
එසේ කී ගැහැනිය ඉක්මණින්ම ගෙට ගොස් දොර වසා ගත්තාය.</p>

<p>මේ තරමටම අනුවේදනීය වූ
ඛේදවාචකයක් සම්බන්ධයෙන් මෙවැනි ආකල්පයක් නිර්මාණය වීමට බලපෑ හේතුව කුමක්ද?</p>

<p>නිසැක වශයෙන්ම ඒ සිව් දෙනාගේම
මරණයට බලපෑ හේතුවක් ඇත්තේ එහිම යැයි අපට සිතෙයි. ඒ හේතුව සොයා යා යුතු යැයි
අපි තීරණය කළෙමු.</p>

<p>ඒ අම්මාගේ නම රූපා බියටි්රස්
පෙරේරාය. වයස 60 කි. ගුණමුුනි විජයසිරි විල්ෆ්රඩ් ඒ පවුලේ තුන්වැනියාය. වයස 30 ක
තරුණයෙකි. හතරවැනි පුතා ගුණමුනි විජේන්ද්ර විල්ෆ්රඩ් පෙරේරාය. වයස 28 කි. බඩ පිස්සී
ගුණමුනි නිරූෂා නිලන්තිය. ඈ වයස 26 ක තරුණියකි.</p>

<p>මේ පවුලට වැඩිමහල් දියණියක් සිටී.
අකීකරු විවාහයක් කරගත්තේ යැයි කියා ඒ දෙමාපියෝ ඒ දියණිය සමග සබඳතා අත්හළහ.
දෙටු පුතා රැකියාව පිණිස ගාල්ලෙන් කටුනායකට ආවේය. සති අන්තයේ ගෙදර පැමිණෙන
ඔහු  හැකි ඉක්මනින් ආපසු රැකියාවට යයි.</p>

<p>''මගේ බිරිඳ හා දරුවන් තුන් දෙනා
මාව ගෙදරින් පැන්නුවා'' ඒ ගෙදර ගෘහ මුලිකයා වන විශ්රාමික හමුදා මේජර්වරයා ප්රකාශ කරයි.
ඒ අනුව අංක 36, මහගොඩ, කරන්දෙණිය යන ලිපිනයේ පදිංචිව සිට ඇත්තේ පෙරකී
සිව්දෙනා පමණය.</p>

<p>ඒ සිව්දෙනා පසුගිය 20 වැනිදා
ආවරණය කරන ලද වෑන් රියකින් ගමෙන් පිටවනු ඇතැම් ගැමියෙක් දුටුවේය.</p>

<p>ඒ ගිය ඔවුන් කෙළින්ම පැමිණ
ඇත්තේ කන්ථතරට යැයි කියති.</p>

<p>බැඳගෙන ආ බත්මුල් බෝධිය
පාමුලදී ලෙහාගෙන කා පසුව ඔවුහු එහි බොහෝ වේලා රැඳී සිට ඇත. රෑ බෝ වනතුරුම ඔවුන්
එහි සිට ඇත.</p>

<p>පසුව සිව්දෙනාම රේල් පාර ඔස්සේ
කන්ථ ගඟමතට පිවිසියහ. ඇතැම්විට ඔවුන් එසේ එන්නට ඇත්තේ නිශ්චිත තීරණයකට එළැඹීමෙන්
අනතුරුව  විය හැකිය. සියලූ දෙනාගේ රත්රන්
බඩු ගලවා බෑගයක දීමූ ඔවුහු එම බෑගය හා මුදල් පාලම මත තැබූහ.</p>

<p>බැස ගිය වෙසක් සඳ කිරණේ අවසන්
රැස් වළලූ කන්ථගඟ දිය මතට වැටී තිබිණි. අනුහස් සහිත කන්ථතර බෝධිරාජයාණන් වහන්සේගේ
බෝපත් පිස එන මතක බණපද කියන මල් සුළඟ සිත් නිවී සැනහීම් කරන්නට සමත්ය.</p>

<p>එහෙත් ඔවුන්ගේ සිත් ගිනිගෙන
දීවෙමින් තිබිණි. ආත්මගත අග්නියෙන් දීවෙන ජීවිතයට සදාකාලික විමුක්තියක් කන්ථගඟ පතුලේ සැඟවී
තිබේ යැයි කවුරු හෝ ඔවුන්ට කියා තිබුණේද?. මහන්ථ මවුවරුන්ගේ පින්සාර සාධු නාදය
ඈතින් ඇසේ. එහෙත් ඔවුන්ගේ සවන්පත් මත නින්නාද වන්නේ පෙරදා රැයේ නිවසේ නැටූ
තොවිලයේ බෙර පදය ඒ සිත් නොසන්සුන් වෙමින් තිබෙන්නට ඇත. ඒ අත් එකිනෙකට
වෙළිණි. සංසාරයේ පතාගෙන ආ පරිදි එක නිවසක උපන්  ඔවුහු ඒ නිසරු සසරේ සැබෑ අරුත කියා දෙමින් එකම මොහොතක කන්ථගඟ
පතුළට පැන්නාහ. </p>

<p>දින දෙකක වෙහෙසකර වැයමකින්
අනතුරුව සිරුරු හතරම ගොඩ ගත්හ. ඔවුන් කාගේ කවුදීයි  හර්නා ගැනීමට නිශ්චිත සලකුණක් තිබුණේ නැත. ''මේ ගින්දරෙන් අප බේරා
දෙන්න'' එසේ සටහන් කළ කොල  කැබැල්ලක්
ඔවුන්ගේ බෑගයක තීබී හමුවිය.</p>

<p>මළ සිරුරුවල ඡායාරූප
පුවත්පත්වල පළ විය. ලංකාදීප පත්තරයක්ද රැගෙන උතුරු කන්ථතර පොලිස් ස්ථානාධිපති
පොලිස් පරීක්ෂක රෙක්ස් ජැක්සන් මහතා හමුවූ ගුණමුනි විල්බට් මහතා කියා සිටියේ ''දිවි
නසාගෙන තිබෙන්නේ තම බිරිඳ හා දරුවන් තිදෙනෙකු'' බවය.</p>

<p>ඔහු තවදුරටත් මෙසේද කීවේය.</p>

<p>''මගේ බිරිඳ බලි තොවිල්,
ශාස්ත්ර යන්ත්ර, මන්ත්ර, ගුරුකම්, අංජනම් එළිවලට පුදුම විදියට ඇබ්බැහි
වුණා. මගේ බාලම දරුවන් තුන්දෙනත් ඒවාටම නැඹුරු වුණා. මම ඒවට ඒ තරම් කැමැති
වුණේ නෑ. මම ඒවට විරුද්ධව කතා කළාම බිරිඳ හා 
 
ú;r by;~~ ud f.oßka tf  j;a jqKd~~</p>

<p>ඒ මහා ඛේදවාචකය එක් අඳුරු
පැතිකඩකින් අනාවරණය  වූයේ  එසේය. ඒ පියා කියන පරිදි දිවි නසා ගැනීමට තරම්
වෙනත් හේතුවක් ඔවුන්ට තිබී නැත. ඔවුන් යන්ත්ර මන්ත්ර ගුරුකම් විශ්වාස කළ නිසා ඒ
මරණ සිදුවී යැයි ඔහු විශ්වාස කරයි.</p>

<p>අපි ඒ ගමට ගියේ ඒ කතාවේ
ඇත්ත නැත්ත විපරම් කිරිමටය. මහ රෑ වුවද ඒ නෑදෑයින් අවදි කරවා ඔවුන්ගෙන් තොරතුරු ලබා
ගැනීමට අපි සමත් වුණෙමු.</p>

<p>''ඔය  ගෙදරට පිට කිසිම කෙනෙක් යන්නේ නෑ. ඒ ගෙදර අය මගේත් ළඟම ඥාතීන්
තමයි. නමුත් මීට අවුරුදු හත අටක කාලේ ඉඳලා නෑදෑයන්, ගමේ අය මේ ගෙදරට එනවට ඒ
අය කැමැති වුණේ නෑ. අඩුම තරමේ ගමේ අය එක්ක හිනාවෙන්නෙවත් නැතිව ගියා. ඒ ගෙදරට
ආව ගිය එකම පිටස්තර තැනැත්තා තමයි ශාස්තරකාරි. ඒ ගෑන විතරයි ඒ ගෙදර ආවේ ගියේ"
රූපනි බියුටි්රස් මහත්මියගේ ළඟම ඥාතිවරයකු ද වන හා ළඟම අසල්වැසියා වන පී. ඩී.
රත්නපාල  අප සමග කීවේය.</p>

<p>රූපලාගේ නිවෙස මෙසේ හදිසි
විපර්යායකට ගොදුරුවී තිබෙන්නේ මෙයට වසර අට නවයකට පෙර සිට යැයි ප්රදේශවාසීහු මෙන්ම
ඥාතීහුද කියති. මෙයට වසර 10 කට පමණ පෙර රූපාගේ මව හා පියා මියගොස් තිබේ. යන්ත්ර
මන්ත්ර ගුරුකම්වලට ටිකෙන් ටික නැඹුරුවී සිටි රූපා සහ දරුවන්  සිව්දෙනා යන්ත්ර මන්ත්ර ගුරුකම් වැඩ 
nod je</p>

<p>''මිය ගොස් තිබෙන්නේ මගේ
අක්කා හා දරුවන් තුන් දෙනෙක්. මේ අය ඉතාම හොඳට ජීවත් වුණා. ගමේ කොයි කා අතරත්
හොඳ හිත දිනාගෙන ජිවත් වුණා. අපේ අම්මයි තාත්තයි මැරුණට පස්සේ අක්කා යන්ත්ර
මන්ත්රවලට වැඩියෙන් නැඹුරු වුණා. එයා නිතරම ශාස්ත්ර අහන්න ගියා. ඒ ශාස්ත්ර අහපු
තැන්වලින් කියලා තියෙනවා. නෑදෑයන් හා ගමේ අය ගෙදරට කොඩිවින හදි හූනියම් කරලා තියෙනවා.
ඒ නිසා ඒ අයගෙන් බේරිලා ඉන්න කියලා. අන්තිමට අක්කයි දරුවෝ තුන් දෙනයි
නෑදෑයන්ගෙන් ඈත් වුණා. නෑදෑයෝ ගෙදරට ගියාම කෙළින්ම කිව්වා 'මේ ගෙදරට ආපහු
එන්න එපා' කියලා. ඒ නිසා නෑයන් ඒ අයත් එක්ක තිබුණු සබඳතා අත්හැරියා. ඒ පුත්තු
දෙන්නාගෙන් එක පුතෙක් බයිසිකලයෙන් කඩේ එහෙම යනවා අපි දීකලා තියෙනවා. ඒත් ඔන්ථව
උස්සලා කතා කරන්නේ නෑ. අනිත් පුතා නම් වත්තෙ සීමාවෙන් එළියට ඇවිත් නෑ. ඒ අය
හුදකලා ජීවිතයක් තමයි ගත කළේ"</p>

<p>ගම්වැසියන් හා නෑදෑයින් කියන්නේ
එහෙම කතාවකි. ''රූපා අක්කාට අංශභාග රෝගය ඇවිත් තියෙනවා. දුවට ආතරයිටීස් රෝගය
තිබුණැයි කියලා කියනවා. තව පුතෙකුට ඔන්ථවෙ කැක්කුමක්  තිබිලා තියෙනවා. ශාස්ත්රකාරී කියලා තියෙනවා. ඒවාට බොහෙත් අරගෙන වැඩක්
නෑ. ශාන්ති කර්ම කරන්න'' කියලා. අන්තිමට ඒ ගෙදර තොවිලයක් නටපු නැති දවසක් නැති
තරම්. එහෙම කියන්නේ රූපා මහත්මියගේ කිට්ටුවර ඥාතියකුවන මහගොඩ පදිංචි කේ. එම්. ජයවර්ධන
මහතාය.</p>

<p>රූපා බි්රයුටි්රස් ඇතුන්ථ දරුවන්
ජීවත්වූයේ මනා ලෙස සැකසූ නිවහනකය. ඔවුන්ට අක්කර හතක පමණ විශාල ඉඩමක්ද තිබේ යැයි
පවසන ඥාතීහු එයින් විශාල ආදායමක් ලැබූ බවද පෙන්වා දෙති.</p>

<p>''දිවි නසා ගන්න තරම් ආර්ථික ප්රශ්න
ඔවුන්ට තිබුණේ නෑ'' ඥාතීහු එසේ කියති.</p>

<p>එහෙත් මියගිය සොහොයුරන් දෙදෙනාගේ
හා සොහොයුරියගේ වැඩි මහල් සොහොයුරියක වන ඩොරින් කැට්ලින් පෙරේරා හමුවීමට ගිය මිය
ගිය සොහොරෙක් හා සොහොයුරිය ඇගෙන් රුපියල් පනස්  දහසක් ණයට ඉල්ලාගෙන  ඇතැයි
කියති.</p>

<p>''මගෙන් රුපියල් පනස් දහසක් ණයට
ඉල්ලාගත්තේ තොවිලයකට කියලයි කිව්වේ" ඩොරින් කැට්ලින් උතුරු කන්ථතර පොලිස් ස්ථානාධිපති
පොලිස් පරික්ෂක රෙක්ස් ජැක්සන් මහතාට කට උත්තරයක් දෙමින් කියා ඇත.</p>

<p>''අපේ ගමේ හැම රාත්රියකටම වගේ
බෙර ශබ්ද ඇහෙනවා. ඒ කියන්නේ ගමේ කොතැනක 
yß f;dú,hla kgkjd' ta jf.au 
ryis.; wd.ula .fï me;sß,d o lshk ielhla wmg ;sfhkjd~~  tfyu lsõfõ .=re WmfoaYljrhl= jk fla' tï'
chj¾Ok uy;dh'</p>

<p>මේ අතර තවත් සිද්ධියක් වාර්තා වේ.
එනම් පිරිස දිවි නසා ගැනීමට පෙර දිනයේ රාත්රියේ එම නිවසේ තොවිලයක් තිබූ බවය.</p>

<p>''රීරි යකා වරෝ....... මරන්
කන්න වරෝ........'' කිය කියා කෑ ගසන ශබ්දයක් රාත්රී 12 ට පමණ ඒ නිවසින් ඇසුණු බව
ප්රදේශවාසීහු කියති. </p>

<p>පසුවදාට පහන් විය.</p>

<p>තිර දමා ආවරණය කරන ලද වෑන්
රියක් ඒ නිවෙසට පැමිණ තිබේ. මව හා දරු තිදෙනා ඒ රියට ගොඩ වී තිබේ.</p>

<p>ඒ රිය සැනින් පිම්මේ ඉඟිලී
ගියේය. ඉන් පසු වාර්තා වූයේ එකම පවුලේ 
isjq fofkla l¿.Ûg mek Èú kid .;a njh'</p>

<p>ඒ පුවත වාර්තා වූ පසු ගම්මු ඒ නිවෙසට
ගියහ.</p>

<p>ඒ නිවෙසේ දොරවල් විවෘත කර තබා
ඔවුන් ගොස් තිබෙන බව අනාවරණය වූයේ ඒ මොහොතේය.</p>

<p>ඔවුන් දිවි නහ ගත්තේ ඇයි?</p>

<p>ඒ සඳහා ඔවුන් පෙළඹ වූ සාධකය
කුමක්ද?</p>

<p>යන්ත්ර මන්ත්ර ගුරුකම්, බලි
තොවිල් අංජනම් එළි පස පසු ගිය ගමනක අවසාන ප්රතිඵලය එයද?</p>

<p>අප සමග ගම් වැසියෝ මෙසේ කීහ.</p>

<p>''සමහර විට ශාස්ත්රකාරයො
''උඹලාගේ ආත්මය වටා මහා ගිනි ජාලාවක් එතෙනවා. ඒ ගින්නෙන් මිදෙන්න ශුද්ධ වූ
වතුරකට පනින්න'' කියලා කිව්වද දන්නේ  නෑ
ඒ නිසා ඒ අය ගඟට පැන්නද දන්නෙ නෑ''</p>

<p>''මේ ගින්නෙන් අප
බේරා දෙන්න'' යැයි මියගිය දිවුදෙනා ළඟ තිබූ පුංචි කොළ කැබැල්ල ඒ මහා
ඛේදවාචකයේ සුලමුල අනාවරණය කරන්නකැයි අපට සිතේ.</p>






</body></text></cesDoc>