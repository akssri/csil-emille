<cesDoc id="sin-w-news-home-dinamina-14" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-home-dinamina-14.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p><head>ÈkñK 2000 lajQ iema;eïn¾ 20 </head></p>

<p> </p>

<p>ශ්රේණි
එකසිය ගණනක් තිබූ ගුරු ක්ෂේත්රය කණ්ඩායම් 5 කට සම්පිණ්ඩනය කර ගුරු සේවාව
ඇතිකර, තමන්ටම කියා නිවසක් හිමිකර ගැනීමට ඉඩමක් ගැනීමට අඩු පොලී ක්රමයට ණය
පහසුකම් උදාකර, තම වැටුප මෙන් දහ ගුණයක් ආපදා ණය ක්රමයක් ලබා ගැනීමේ අවස්ථා
උදාකර ඇතැයි ද මේ නිසා මෙතෙක් ලැබූ වැටුප් ප්රමාණය වැඩිවී ආත්ම ගරුත්වයක් ඇති පිරිසක්
ලෙස කටයුතු කිරීමට ගුරු පරපුරට අවස්ථාව ලැබී ඇතැයි ද ශ්රී ජයවර්ධනපුර කලාපීය අධ්යක්ෂ
එස්. ගලප්පත්ති මහතා පැවැසීය.</p>

<p>ඒ මහතා එසේ පැවසූවේ
මහරගම බුවනෙකබා විද්යාලය තුළ නිම කැරුණු ව්යාපෘති රැසක් සිසු අයිතියට පැවැරීම
නිමිත්තෙන් පැවැති උත්සවයකදීය. </p>

<p>එම ව්යාපෘතීන් අතර
බස්නාහිර පළාතේ මහ ඇමැති වරයා සිය විමධ්යගත මුදලින් ලබාදුන් රුපියල් ලක්ෂ තුනක්
වැය කර නවීකරණය කළ ශාලාව, විද්යාලීය තාප්පයේ නිම කළ චිත්ර ඇඳීමේ
ව්යාපෘතිය, ජලය අත්හිටවූ දිනයන්හිදී ප්රයෝජනයට ගැනීමට සරත් ජයසූරිය මහතාගෙන්
පිරිනැමුණු ජල ටැංකිය, ඩබ්ලිව්.පෙරේරා මහතා පිරිනැමූ ආරක්ෂක මුර කුටිය,
බස්නාහිර පළාත් සභා මන්ත්රී ලක්ෂ්මන් නිපුනආරච්චි මහතාගේ විමධ්යගත අරමුදලින් ඉදි
කැරුණු වැසිකිළි හා විදුහල් දරුවන්ගේ ප්රයෝජනය සඳහා මහරගම ප්රාදේශිය සභා
අනුග්රහයෙන් සැකසෙන ක්රීඩා පිටියද විය. චාරිත්රානූකූලව පොල්තෙල් පහන දීල්වීමෙන් අනතුරුව
තවදුරටත් කතාකළ ගලප්පත්ති මහතා මෙසේද කීය.</p>

<p>නූතන නවීන තාක්ෂණික දියුණුව
සමගම ලෝකය එකම ගම්මානයක් බවට පත්වීමේ ක්රියාවලිය ක්රියාත්මක වෙමින් පවතියි. මේ නිසාම
ඊට ගැලපෙන ආකාරයට අපේ අනාගත පරපුරට හැඩ ගැස්වීම අවශ්යව ඇත. අභියෝගවලට මුහුණ දිය
හැකි අනාගතයට නිර්භයව මුහුණ දීමට හැකි සංවර්ධන පෞර්ෂයක් ඇති දරුවන් ඒ අනුව හැඩ
ගැස්වීම අවශ්ය නිසයි.  නව අධ්යාපන
ප්රතිසංස්කරණ වැඩ පිළිවෙළ රජය මගින් ආරම්භ කළේ.</p>

<p>ඉදිරියේදී ඇතිවන දීනුම
පුපුරා යාමේ න්යාය අනුව දියුණුව ලබා ගැනීමට නම් විද්යුත් මාධ්ය ජනමාධ්ය හා නොයෙකුත්
සංනිවේදන මාධ්ය හරහා සොයා යන්නට කියවන්නට අසන්නට දරුවන්ට සිදුවෙයි. එහි දී
ගුරුවරයාට ඉගැන්වීමට නොව මග පෙන්වීමටයි සිදු වෙන්නේ. සමාජ විරෝධී නොවන සමාජයක් බිහි
කිරීමේදී එම වගකීම නිවැරදිව ඉටු කළයුතු වෙනවා.</p>

<p>මේ සඳහා අවශ්ය පහසුකම් සපයා
ඇති අතර බස්නාහිර පළාතේ මහ ඇමතිවරයා ඉතා උනන්දුවකින් කටයුතු කිරීම අගේ කළ
යුතුයි. ප්රදේශයේ පාසල් සඳහා භෞතික සම්පත් විශාල වශයෙන් ලබාදීම හා විෂය ඉගැන්වීමේදී
හරවත් බව ගුණාත්මක බව වැඩි කරන්නට කළයුතු වැඩිකිරීම් ඉටු වී තිබෙනවා. මේ අනුව
ගුරුවරුනට සිය යුතුකම් ඉටු කිරීම පහසු වී තිබෙනවා. ආයුධ අතට ගන්නා පරපුරක් නොව
ප්රාථමික අධ්යාපනයේ සිට හික්මුණ, දීනුමැති රටගැන හැඟීමක් ඇති අනාගත පරපුරක් බිහි
කිරීමේ මග පෙන්වීමේ වගකීම ගුරු පරපුරට හිමිව ඇති බව කිව යුතුයි. ඊට අවශ්ය මූලික පදනම
රජය සපයාදී තිබෙනවා. </p>

<p>බස්නාහිර පළාත් සභා
මන්ත්රී ඉසුර දේවපි්රය, මහින්ද කුරුවිට, යන මහත්වරුද මහරගම ප්රාදේශීය
සභාවේ සභාපති ඩබ්ලිව්.සමරපාල පෙරේරා, විදුහල්පති කේ.යූ.බි.රත්නායක යන මහත්වරුද
කතා කළහ.</p>

<p>චිත්ර ව්යාපෘතිය සඳහා දායක
වූ පාසල් සඳහාද විද්යාලයේ දස්කම් දීක්වූ දරුවන් සඳහාද ත්යාග පිරිනැමීමක්ද සිදු කැරිණි.</p>

<p> </p>

<p>  </p>






</body></text></cesDoc>