<cesDoc id="sin-w-news-foreign-71" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-foreign-71.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>සදාම් හුසේන් ඉරාකය පාලනය කරන්නේ
හරියට  මාෆියා නායකයෙකු තමාගේ දාමරික ව්යාපාරය පාලනය කරන ආකාරයෙනි. දාමරික නායකයින් හැමවිටම කල්ලියේ ප්රධාන තැන් දෙන්නේ සිය
පවුලේ අයටයි. රසායනික අවි ආයුධවලින් සන්නද්ධව සිටින සදාම් බලයෙන් පහකිරීමට අමෙරිකාව දරන උත්සාහය රහසක් නොවේ. අමෙරිකාව සිතන ආකාරයට සදාම් හුසේන් සමස්ථ ලෝක ජනතාවටම මහා හිසරදයක් වී ඇත. ඉතින් අමෙරිකාව කරන්නට යන්නේ හිසරදයට කොට්ට මාරුකිරීමක් නොවේ දැයි මධ්යස්ථ මතධාරීන් නඟන ප්රශ්නයකි. ඊට සාධාරණ හේතුද ඇත.</p>

<p>සදාම් හුසේන්ගෙන් පසුව ඉරාකයේ බලය පැවරෙන්නේ  කාටද? අනිවාර්යෙන්ම හිමිකම් කියන්නේ ඔහුගේ පුතුන් දෙදෙනාගෙන්
කෙනෙකි. විශේෂයෙන් බාලපුතා තාත්තාගෙන් පසුව ඉරාකයේ පාලකයා වන බවට බොහෝ දෙනෙක් විශ්වාසය පළ කරති. මොවුන් තාත්තාට වඩා
කෲර පාලකයින් වන බවට පෙනෙන්නට ඇති සාධක බොහෝය. </p>

<p>සදාම්ගේ පුතුන් පිළිබඳ අවට ලෝකයාට දැනගන්නට ලැබෙන්නේ ඉරාකයෙන් පැන අනෙක් රටවල
වාසය කරන ඉරාක ජාතිකයින් ගෙනි.</p>

<p>ලොකු පුතා උදේය. බාලපුතා කුසේ ය. උදේගේ වයස 38
ක් වන අතර කුසේ 36 හැවිරිදිය.
සෙසු රටවල වාසය කරන ඉරාක ජාතිකයින් විසින් කරනු ලබන විස්තර අතිශයෝක්ති විය හැකි
වුවද ඒවා සත්යයට එතරම් ඈත නොවන බව අමෙරිකානු රහස් ඔත්තු සේවය පවත්වාගෙන යනු ලබන ලිපි ගොනු
පෙන්නුම් කරයි. උදේ පිළිබඳ සෑහෙන විස්තර ලබා ගැනීමට හැකිවුවත්, කුසේ
පිළිබඳ එතරම් විස්තර වාර්තා
ලබා ගැනීමට නොහැක. කෙසේ වුවද ආරක්ෂක හමුදා සේනාංක සියල්ල ඔහු යටතේ,
පවතී. මේ හේතුවෙන් පිය උරුමය පැවරෙන්නේ කුසේ ට බව කවුරුත් විශ්වාස කරති.</p>

<p>සදාම්ගේ පුතුන් පිළිබඳ විස්තර අසන විට අපට නිතැනින්
සිහියට නැඟෙන්නේ අපේ දේශපාලනඥයින්ගේ පුතුන් පිළිබඳ විස්තර කතාවලටයි. කුඩා
කාලයේ සිට උදේ සහ කුසේ බිහිසුණු  දාමරිකකම් දැක දැක දැඩිවුණු සොහොයුරන් දෙදෙනෙකි. කුඩා
කාලයේදී
පටන් වධකාගාර නැරඹීමට පියා සමගින් මේ පුතුන් දෙදෙනාද ගිය බව පෙනේ. ඒ කාලයේදී ''බාත්'' පක්ෂය රට පාලනය කළ වකවානුවයි. ජ්යෙෂ්ඨ
හමුදා නිලධාරියෙකු
වශයෙන් සදාම්  මේ වධකාගාර පවත්වාගෙන ගිය
අතර මෙහිදී
ඉතාම කුරිරු
වධ හිංසාවලට ඉරාක ජාතිකයින් මුහුණ දුන් බව වාර්තා ගතය.</p>

<p>අවසානයේ මාලිගාව</p>

<p>1960 දී බාත් පාක්ෂිකයින් බලයෙන් පහවු අවස්ථාවේ දී මේ වධකාගාර පිළිබඳ තොරතුරු හෙළිවන්නට විය.
''අවසානයේ මාලිගාව'' වශයෙන් එක් වධකාගාරයක් හැඳින්වේ. 1958 දී  පයිසාල්
රජු මරා දමන ලද්දේ මේ ස්ථානයේදීය. හමුදා පාලනයට යටත් වු ඉරාකය හඳුන්වනු ලැබුවේ ''භීතියේ
ජනරජය'' ලෙසටයි. උල්කරන ලද යකඩ කූරු මත වාඩිවීමට සැලැස්වීම, විදුලි බලයෙන් කි්රයා කරන බුරුමවලින් ශරීරයට විදීම, ඇඟිලි කපා දමන යන්ත්ර මේ වදකාගාරවල තිබී හමුවී
ඇත. සදාම්, වදදී
මරා දමනු ලැබු මිනිසුන්ගේ සිරුරු රටේ පාලකයා වශයෙන් නෑයින්ට භාරදී ඇත. ජර්මනියේ හිට්ලර් රුසියාවේ ස්ටාලින් වැනි ආඥාදායකයින්ගේ කි්රයා කලාප
අනුගමනය කිරීමට
සදාම් රුචිකත්වය දැක්වූ බව රහසක් නොවේ. පියාගේ කෲර කම් දැක දැක හැදී වැඩුණු උදේ සහ කුසේ කෲරත්වයේ සංකේත බවට පත්වීම
පුදුමයට කාරණයක් වශයෙන් සැලකිය යුතු නැත. </p>

<p>උදේ පස්වරක් විවාහ වු කෙනෙකු බව පැවසේ. වීදියකදී ලස්සන කාන්තාවක් මුණ ගැසුනොත් තමාගේ ආරක්ෂක භටයින්
යොදවා ඒ අවාසනාවන්ත කාන්තාව සමාජ ශාලාවකට බලයෙන් ගෙන ගොස් දුෂණය කිරීම ඔහුගේ එක් විනෝදාංශයකි.</p>

<p>වරක් උද්යානයකදී තරුණ අඹුසැමි යුවළක් ඔහුට  හමුවී ඇත. සැමියා කැප්ටන් වරයෙකි. උදේ එම
කාන්තාව තමා ළඟට කැඳවා ගැනීමට උත්සාහ කර ඇතත් එය හරි නොගිය තැන ඔහු එම කාන්තාවගේ අතින් ඇද ''මේ වගේ
මිනිහෙකුට ඔයා හොඳ වැඩිය'' පවසා ඇත. පෙරදින හමුදා නිලධාරියා සමග විවාහ වු බව කාන්තාව බැගෑපත්ව පැවසු නමුදු උදයගේ රැකවල්ලූ එම කාන්තාව හෝටල් කාමරයකට කැඳවාගෙන ගොස් උදයට භාර දුන්හ. දුෂණයට ලක්වු එම කාන්තාවගේ මළ සිරුර පසුදා හෝටලයේ හයවැනි තට්ටුවේ සිට බිමට පතිත වී ඇති බව දක්නට
ලැබුණු බව සඳහන් වේ. හමුදා කැප්ටන්වරයා සදාම් හුසේන් ජනාධිපතිවරයාට ප්රසිද්ධියේ
අපහාස කිරීමේ
වරදට වෙඩි තබා මරා දමා ඇත. මේ සිද්ධිය පමණක් පුවත්පත් වල පළවී ඇත. </p>

<p> </p>






</body></text></cesDoc>