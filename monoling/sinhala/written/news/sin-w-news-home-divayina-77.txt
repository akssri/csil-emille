<cesDoc id="sin-w-news-home-divayina-77" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-news-home-divayina-77.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p>සහන සැලසීමට සහල් සළාකයක්</p>

<p>ලබන ජූලි මාසයේ සියන්ථ රජයේ සේවකයන්ගේ
වැටුප් වැඩි කරන බවත් එතෙක් අත්යවශ්ය භාණ්ඩවල මිල පාලනයක් ඇති කිරීමට රජය පියවර ගනිමින්
සිටින බවත් කම්කරු ඇමැති අලවි මවුලානා මහතා ප්රකාශ කර තිබේ.  සිංහල අන්ථත් අවුරුද්දට පෙර එය ක්රියාත්මක වන බව
කී ඇමැතිවරයා දැනට ද ඇතැම් භාණ්ඩ මිල පාලනයට යටත් කර ඇතත් එය හරිහැටි ක්රියාත්මක
නොවන බව ද පෙන්නා දුන්නේය.</p>

<p>අනපේක්ෂිත ලෙස ඩීසල් පැට්රල් හා භූමිතෙල්
ආ යේත් ගෑස්වලත් මිල ඉහළ යෑම හේතුකොට මෙරට ප්රවාහන වියදම් වැඩි වීමෙන් භාණ්ඩ
හා ආහාර ද්රව්ය ප්රවාහනයට වැඩි මුදලක් වැය කරන්නට සිදු වීමෙන් හා ආහාර ද්රව්ය සැකසීමට වැඩි
වියදමක් දැරීමට සිදු වීමෙන් සාමාන්ය ජනතාවගේ බත් පතේත් ආප්පයේ ඉඳිආප්පයේ සිට තේ
කෝප්පය තෙක් හැම දෙයකම මිල ඉහළ නැංගේය. එහෙත් අවස්ථාවෙන් ප්රයෝජන ගැනීමට බලා
සිටින කූට වෙළෙඳුන් නිසා එම මිල ගණන් අසාධාරණ ලෙස ඉහළ නංවා තිබේ.</p>

<p>මේ අතර රුපියල පාවෙන්නට ඉඩ හැරීම නිසා
කිරිපිටි, ඕෂධ වර්ග ඇතුන්ථ අනෙකුත් අත්යවශ්ය භාණ්ඩවලත් එ නෙදා පාරිභෝගික
භාණ්ඩයන්ගේත් මිල ඉහළ ගොස් තිබේ. මේ හේතුකොට මෙරට සාමාන්ය ජනතාවට එදිනෙදා කෑම
බීම හා දරුවන් නඩත්තු කිරීම පවා අසීරු තත්ත්වයකට පත්ව ඇත්තේය. ඊට පිළියමක්
වශයෙන් රජයේ සේවකයන්ට ජූලි මාසයේ සිට වැටුප් වැඩි කිරීමට ගන්නා ලද තීරණය පැසසිය
යුත්තකි.</p>

<p>එහෙත් මේ රටේ බහුතරය රජයේ සේවකයන් නොවීම
නිසා එයින් සම්පූර්ණ ප්රශ්නය විසඳෙන්නේ නැත. පෞද්ගලික අංශයේ සේවය කරන සංඛ්යාව හා
වෙනත් ස්වයං රැකියාවන්ගෙන් ජීවිකාව උපයා ගන්නා සංඛ්යාව විශාලය. ගොවිතැන් හා කම්කරුවෝ ද
ඒ අතර වෙති. ඔවුන්ට ද මෙහිදී සහනයක් සැලසිය යුතුව ඇත්තේය.</p>

<p>මෙහිදී පාරිභෝගික භාණ්ඩවල මිල පාලනයක් ඇති
කිරීමට ගන්නා ලද තීරණය අගය කළ යුත්තකි. එහෙත් එය විධිමත්ව කාටත් සාධාරණයක් ඉටුවන
පරි  ක්රියාත්මක විය යුතු බව පෙන්නා දිය යුතුව තිබේ. මීට පෙරාතුව, පසුගිය
දෙවැනි ලෝක මහා යුද්ධය පැවැති කාලයේ සිටම මේ රටේ මිල පාලනයක් සළාක ක්රමයට
පාරිභෝගික ද්රව්ය සැපයීමක් සිදු වූ බවත් එයින් මෙරට දිළිඳු ජනතාවට මහඟු සේවයක් ඉටු
වූ බවත් මෙහිදී සිහිපත් කළ යුතුය.</p>

<p>සළාක ක්රමය යටතේ 1970 ගණන්වල පවා මෙරට
ජනතාවට හාල් සේරුව සත 25 ටත් සීනි රාත්තල සත 72 ටත් ඊට අමතරව පිටි,
උම්බලකඩ, පරිප්පු හා මිරිස් ආදිය සහන මිලටත් ලබාගත හැකි විය. එදා කිසියම් රැකියාවක්
නොමැති පවුලකට වුවද යන්තම් වේල පිරිමසා ගැනීමට පුන්ථවන් තත්ත්වයක් පැවතිණි.</p>

<p>එහෙත් 1977 වසරෙන් පසු කාලයේ පැවැති රජය
විසින් හාල් සළාක ක්රමය ඇතුන්ථ සහන මිලට පාරිභෝගික ද්රව්ය සැපයීමේ ක්රමය අහෝසි කරන
ලදී. මෙය දිළිඳු ජනතාවට වැදුණු විශාල පහරකි. එහෙත් හාල් නිෂ්පාදනය අතින්
ස්වයංපෝෂිත තත්ත්වයේ සිටින ඉන්දියාව එහි සිටින කෝටි 100 ක් ජනතාවගෙන් අඩු ආදායම්
මට්ටමේ සිටින හැම දෙනාටම අදත් සළාක ක්රමයට සහන මිලට හාල් ලබා දෙයි. කිලෝව
ඉන්දියානු රුපියල් 3.75 ගණනේ මසකට හාල් කිලෝ 20 බැගින් එම ජනතාවට ලබා දෙයි.</p>

<p>ශී්ර ලංකාවේද ජනතාවගේ ප්රධාන ආහාරය බත්ය.
එමෙන්ම මේ රටේ ජනතාවට සෑහෙන තරම් සහල් දැනට මෙරට ගොවියා විසින් නිෂ්පාදනය කෙරෙයි. එම
නිසා ඔවුන්ට ඕනෑ කරන සහල් ප්රමාණය විදේශ විනිමයක් වැය නොකර ලබාදීම පහසු කාර්යයකි.
එහෙත් අද සහල් කිලෝවක් රු. 25 සිට 36 තෙක් මිල ඉහළ ගොසිනි. දිළිඳු ජනතාවට
එදිනෙදා අවශ්යතාවය ලබා ගැනීම අසීරු තත්ත්වයකට පත්ව තිබේ.</p>

<p>සහන මිලට සළාක ක්රමයට මෙරට ජනතාවට සහල් ලබා
දීමට රජය පියවර ගත්තොත් එය මේ රටේ දිළිඳු ජනතාවට විශාල සහනයක් වනු ඇත. එමෙන්ම එය
මේ රටේ ගොවියා දිරිගැන්වීමක් ද වන්නේය. සහල් මිල අඩු වූ සැණින් අනෙකුත් පාරිභෝගික
භාණ්ඩවල මිලද නිතැතින්ම පහළ බසින්නේය.</p>

<p> </p>






</body></text></cesDoc>