<cesDoc id="sin-w-editorial-silumina-34" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-silumina-34.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p> </p>

<p>එජාප නායකයාට ඡන්දදායකයන් රැවැටිය හැකි ද?</p>

<p>            l;= jelsh'</p>

<p> </p>

<p>එජාපය පක්ෂයක් හැටියට මෙම මහ මැතිවරණයේ දී තරග බිමට පිවිස
සිටින නමුදු මෙ රට මහ ජනතාව ඉදිරි සය අවුරුදු කාලය සඳහා එජාප නායකත්වය ප්රතික්ෂේප කැර
දීමුවේ 1999 අවසාන භාගයේ පැවැති ජනාධිපතිවරණයේ දී ය. එම ජනාධිපතිවරණයේ දී රනිල්
වික්රමසිංහ එජාප නායකයා රටේ නායකත්වය සඳහා ඉල්ලා සිටි ජනවරම බහුතර ඡන්දදායකයෝ
ප්රතික්ෂේප කළහ. මහ ජනයා රනිල් ප්රතික්ෂේප කරන බව දැන සිටි ප්රභාකරන් අවසාන
මොහොතේ චන්ද්රිකා බණ්ඩාරනායක කුමාරතුංග ජනාධිපතිනිය ජීවිතක්ෂයට පත් කොට තරග බිමෙන්
ඉවත් කරන්නට දීරූ ප්රයත්නය ද අසාර්ථක විය. පොදු ජනතාව එජාප නායකත්වය ප්රතික්ෂේප කොට
ඉදිරි ආණ්ඩු කාලය උදෙසාත් තෝරා ගත්තේ ජනාධිපතිනිය ගේ රාජ්ය නායකත්වය යි.  මේ අනුව දීනටමත් රනිල් වික්රමසිංහ රාජ්ය
නායකත්වයෙන් බැහැර කොට ඇත. ඉදිරි කාලයේත් රාජ්ය නායිකාව චන්ද්රිකා බණ්ඩාරනායක
ජනාධිපතිනිය යි. මෙ මහ මැතිවරණයේදී රනිල් වික්රමසිංහ එජාප නායකයාට තරග කරන්නට සිදුව
තිබෙන්නේ අගමැති ධූරය සඳහා පමණකි. එයින් ප්රත්යක්ෂ වන්නේ එජාපයට තරගයට එන්නට
මහජනයා වරම් දී ඇත්තේ අර්ධ පරාජයක් සමග බව 
h'</p>

<p>      fï h:d¾:h
Pkaoh fok ck;dj wjfndaO fldg f.k isák nj tcdm kdhlhd fydÈka u okS' ta ksid u
ck;dj fkd u. hjd w,af,ka ú,s jid .kakg fyf;u Pkao fõÈldfõ fnd</p>

<p>      Pkaoodhlhka tcdm kdhl;ajh m%;slafIam lsÍu
mqÿu ke;' tcdmfha kshu Wreulaldrhka jQ fcHIaGfhda rks,a f.a kdhl;ajh m%;slafIam
fldg bka bj;aj isá;s' bka jeä u fokd iyfhda.h olajkafka ckdêm;skshf.a
kdhl;ajhghs' tcdmfha fcHaIaG mS;Djreka jQ fÊ' wd¾' chj¾Ok yd wd¾' fm%auodi hk
uy;=kf.a kshu mqfrda.dó wkq.dñl odhl;ajh wo tcdmh ;=, wo ke;' fÊ' wd¾ f.a wd¾Ól
jev ms  ke.=K muKska ;ukaf.a mlaI idudðl;ajh ;ykï
l  orkafka flfia±hs úfÊmd,
uekaäia uy;d m%Yak lrhs' fmd't'fm'  kj
wdKavql%u jHjia:djg ishhg wkQ mylg;a jvd tlÕ jQ tcdm kdhlhd wka;su fudfydf;a Bg
tfrys jQ wjia:djd§ m%;sm;a;sh fy,d ÿgq tcdm fcHIaG kdhlhl= jQ yf¾kaø fldrhd
uy;d tcdmfhka b,a,d wia ù fmdÿ fmruqKg tla úh' tcdmfha fu ;rï ìysiqKq kdh hEula
b;sydifha lsisod isÿ ù ke;' tcdm kdhl;ajhg tfrysj m%dfoaYslj fmdÿ fmruqKg tlajk
ixLHdj Èfkka Èk by</p>

<p>      tlai;a
cd;sl mlaIh tl fiai;a ler .; fkd yels kdhlhd rgla md,kh lrkakg n,h b,a,Su
úys¿jls hs tcdmfha l%shdldÍyq u lsh;s' ckdêm;sjrKfha § ud, j  m%ldY yqfola úfkdao cklh' Tlaf;dan¾ 10 jk od
n,hg m;ajkafka kï" mdrg nei úma,jhla lrk nj Tyq fkdfndaod /iaùul§  lSfõh' fudkjo ta l;d@ Tlaf;dan¾ 10 jk od
n,hg m;ajkafka kï" mdrg nei úma,j lrkafka l=ulg o@  Tlaf;dan¾ 10 odg miqj mdf¾ flfia fj;;a tcdmh
;=</p>

<p>      ck;dj b,a,d
isákafka rgg kiame;a;shla jQ hqoaOh k;r fldg iduh Wod lsÍu ñi" nhsisl,a
lgqjla" jEka lgqjla fkdfõ' iduh Wod lsÍu msKsi wem lem jQ b;du iqÿiaid
ckdêm;sksh nj ck;dj miq.sh ckdêm;sjrKfhka ;Skaÿ lr yudrh' tcdm kdhlhd ta i|yd
iyfhda.hj;a fkd fok neúka fu jr uy ue;sjrKfha § Pkao wmf;a fkdhjd fmdÿ fmruqKg
ish¨u Pkao §fuka ckdêm;skshf.a n,h jvd mq¿,a f,i Yla;su;a lsÍu ck;dj f.a
wêIaGdkh jkjd isl=reh' idlal=fjka t   </p>

<p> </p>

<p>රනිල් ප්රභාකරන් එකම කාසියේ දෙ පැත්ත</p>

<p> </p>

<p>      lsishï
foaYmd,k mCIhl tys mqfrda.dó kdhlhka mj;sk kdhl;ajhg tfrysj tu mCIfhka bj;aj
hkafka kï" th mdCIslhkg muKla fkdj rfÜ ck;djgo ;on, f,i wjOdkh fhduq
lrkakg isÿ jkq we;' tcdm fha fÊIaG kdhlhka jQ isßfiak l=f¾ uy;df.a isg úchmd,
fukaäia" yf¾kaø fldrhd" iqis,a uqKisxy" frdkS o ue,a"  r;akisß rdcmCI" ir;a wuqKq.u" ir;a
fldaka.yf.a" wd§ tcdm fha fÊIaG;u iy ls%hdldß msßila rks,a úl%uisxy
uy;df.a kdhl;ajhg tfrysj mCIh yer f.dia isá;s' tfia mCIh yer f.dia isák
m%dfoaYSh kdhlhkaf.a ixLHdjo úYd,h' tfiau Èkm;du mCIh yer hk msßio jeä fj;s'
isßfiak l=f¾ uy;d yer wfkla ish,af,dau pkaøsld nKavdrkdhl l=udr;=x.  ckdêm;skshf.a kdhl;ajh ms</p>

<p>      tcdm uyd
mßudK fï kdh hEu .ek w|f.d¿ ìysß ms</p>

<p>      fmdtfm rch
bÈßm;a l 
m%Ndlrkag fmdfrdkaÿ ù we;s tcdm kdhlhd ;ud n,hg m;ajqjfyd;a
ckdêm;skshg  hqoaOh lerf.k hdug bv
fkdfok nj;a lshd we;' </p>

<p>      B,dï iyskh
ienE ler .ekSu i|yd f,a .x.djl mSkk m%Ndlrka keue;s ;%ia;jdÈhdg mCImd;s;ajh
olajk tcdm kdhlhd j¾Ordcd fmreud,a talSh ,xldjla iy cd;sl iuÕh Wfoid o fmkS
isák úg th fy  jkjdg m%Ndlrka
úreoaOh' Bg tcdm kdhlhd úreoaO jkafka wehs' m%Ndlrka i;=gq lsÍu i|yd h' tcdm
kdhlhdg w;S;h wu;l ùu lk.dgqodhlh' m%Ndlrkag tfrysj igka lsÍu i|yd yudodjla
f.dv ke.Sug j¾Ordcdg wdOdr Wmldr lf tijQfha B,dï fldäh fkdj cd;sl
fldähhs'  Tyq bkaÈhdjg mek .sfha
m%Ndlrkaf.ka Ôú;h fírd .ekSu i|ydhs' fï ish,a, okakd rks,a úl%uisxy  tcdm kdhlhd ta j¾Ordcdg wo myr .ikafka ;u
ñ;% m%Ndlrka i;=gq fldg Tyqf.a iyfhda.h ,nd .ekSu i|yd h' </p>

<p>      tcdm
kdhlhdf.a foaYfødayS ls%hd l,dmh u.ska m%Ndlrkaf.a mQ¾K iyfhda.h tcdm
kdhlhdg;a" tcdm wfmaCIlhkag;a ,eî we;' tcdm Èkj, f,i fõ¿ms,af, m%Ndlrka
W;=re kef.kysr fou  isÿj we;af;a m%Ndlrkaf.a urK ;¾ck b;sßfhah' m%Ndlrkaf.a urdf.k
uefrk fldáhka ol=Kg tjd we;af;ao fmdtfm weue;sjreka >d;kh ler oukakgh' tcdm
kdhlhdf.a;a" tcdm wfmaCIlhkaf.a;a uhs,a .ylgj;a fldá ;%ia;jd§kaf.ka
wk;=rla ke;'  fï lreKq ish,a, i,ld n,k
l  w.ue;sjrhd lshd we;s mßÈ u tcdm
wfmaCIlhka ish,a,kao fldá T;a;=lrejka yd .e;a;ka nj i;Hfhls'   </p>

<p> </p>

<p> </p>

<p> </p>

<p>      </p>

<p>දොෂිත වැඩ පිළිවෙලකින් දොෂණ මැඩිය
හැකි ද ?</p>

<p> </p>

<p>      fvdkfuda¾
jHjia:dj hgf;a 1931 we/ô rdcH uka;%K iNd b;sydifha;a" fida,anß jHjia;dj
hgf;a 1947 we/ô md¾,sfïk;= b;sydifha;a ue;sjrK ks,Odßfhl= fyda ue;sjrK
flduidßia jrhl= fyda ms  ÈhqKq lrkakg úh'</p>

<p>      ue;sjrK
¥IK je,elaúu i|yd ue;sjrK flduidßiajrhd ;=      ¥IKhla
j,lajkq i|yd hehs lshñka iaálrhla uqøkh lrkakg ue;sjrK flduidßia jrhd ls%hdfldg
;sfnk wdldrh u uq¿ukskau ¥Is;h' fuf;la ish¿ ue;sjrK flduidßia jreka Pkao uqøK
lghq;= j,§ ryiH Ndjh wdrCId lsÍu i|yd wkq.ukh l, iel idxld rys; ms</p>

<p>      iaálrh
uqøKh lsÍfïÈ ryiHNdjh rlskakg ue;sjrK flduidßiajrhd f;dard f.k we;af;a iq.;odi
lS%vdx.kfha .Dyia: fydag,hhs' iq.;odi ls%vdx.kh hk kdu ud;%h weiqKq ieKska ;;=
o;a ck;dj bÈßfha ueú fmkkafka fld</p>

<p>      wfkla jeo.;a m%Yakh fuu iaálrh uqøKh
flf</p>

<p>       ue;sjrK flduidßia jrhd Pkaoh §fï jHdc Pkao
m;s%ld u.ska Pkao §u je,elaùu i|yd kï isálrh we,úh hq;af;a Pkaoodhlhd Pkaoh
i,l=kq lrk Pkao m;%ldfõh' tfy;a Tyq lshkafka fjk;a l;djls' ¶;eme,a fomd¾;fïka;=
n,OdÍka fj; Pkao ldâ ndr §ug fmr tu isál¾ tys we,ùu ue;sjrK fomd¾;fïka;=fõ woyi
úhe¶ hs Tyq lshhs' ;eme,a fomd¾;fïka;=fjka Pkao odhlhd fj; tjk Pkao m;
we;;a" ke;;a Pkao odhlhdg Pkaoh Èh yelsh' fndfyda fokd ;emEf,ka tk Pkao
mf;ka m%fhdackhla fkd.ks;s' ´kE ;rï tfy fufy úisler ou;s' tjeks Pkaoh §u i|yd
ksh; f,i mdúÉÑ fkdlrk Pkao m;l fldaÜ .kka jeh ler isálrhla w,jkafka wyj,a
u.=,lg ±hs Pkao odhlfhda m%Yak lr;s' ue;sjrk flduidßiajrhd Pkaoodhlhka f.a
nqoaêhg iuÉp,a lrkakg W;aidy fkdl</p>

<p>      ;emEf,ka
tjk ks, Pkao m;%sldj, iaálrhla we,ùfuka uev meje;aúh yels Pkao ¥IKhla ke;s nj
fï wkqj fmks hhs' wfkla w;ska ±kgu;a fï iaálrh ljr wdldrhlska fyda msgia;r
mqoa.,hka w;g m;aj ke;ehs ldg lsj yelso @ 
rysis.; iaálrh ljr wdldrhlska fyda msgia;r mqoa.,hka w;g m;aj ke;shs ldg
lsj yelso @ ryis.; iaálrh uqøKh lrkakg uqøKh lrkakg ndr ÿka wdlrh;a" th
ndr § we;s mqoa.,hka yd Tjqka ls%hd ler we;s wdldrh;a" ue;sjrK flduidßia
jrhd b;sydifha m%:u j;djg fï ;rï iel iys; wkaoñka ls%hd ler ;sìh §
tcdmh;a" fÊùmsh;a yjqf,a Tyq wdrCId lrkakg ñi isÿ ù ;sfnk iel lghq;= jev
ms  wdKavqj neÈ isák njo
u;la ler fokq leue;af;uq'  </p>

<p> </p>






</body></text></cesDoc>