<cesDoc id="sin-w-editorial-lankadipa-125" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-lankadipa-125.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p> </p>

<p>20.2.02
editorial</p>

<p>විපක්ෂ
නායකගේ පිළිවෙත අගයමු</p>

<p>රටේ සාමය ඇති කිරීම සඳහා රජය මේ අවස්ථාවේ
අනුගමනය කරන වැඩ පිළිවෙළ කෙරෙහි විපක්ෂය අවධානයෙන් සිටින බවත්, වසා තිබුණු
මාර්ග විවෘත කිරීම හා මාර්ග බාධක ඉවත් කිරීම 
.ek ;uka úreoaO fkdjk kuq;a thska idudkH ck;djf.a wdrCIdjg lsishï fyda
;¾ckhla we;s fkdjkakg rch j.n,d .;hq;= nj;a úmCI kdhl uyskao rdcmCI uy;d m%ldY
lr we;ehs jd¾;d ù ;sfí' ck;djf.a m%Yak úi£fuys,d md¾,sfïka;=fõ úreoaO md¾Yajh
fj; mejeÍ ;sfnk j.lSu rcfha ta j.lSug jvd lsisfia;a wvq tlla fkdfõ' úmCI kdhl
uyskao rdcmCI uy;df.ka b;d meyeÈ,sj m%ldY ù we;af;a úmCIfha ta j.lSu ms</p>

<p>අද අප රටේ ප්රමුඛතම අවශ්යතාව සාමයයි. පූර්ණ හා ස්ථාවර සාමයක්
ඇති වුව හොත් එයින් ආර්ථික හා සමාජයීය වශයෙන් මුන්ථ මහත් රටටම අත් වන ප්රතිලාභය අති
විශාලය. යුද්ධය නිසා මේ වන තෙක් සිදුවූ ආර්ථික විනාශය නතර වීමත් ජීවිත හානිය නිම වීමත් යන
ප්රධාන අංශ දෙකෙන් පමණක් වුවද රටට අත්වන ශාන්තිය ඉමහත්ය. ඒ දේශ සේවයේ අව්යාජ
කොටස්කාරයකු වන්නට තම කණ්ඩායම සූදානම් බව විපක්ෂ නායකවරයාගෙන් මේ තීරණාත්මක අවස්ථාවේ
ප්රකාශවීම සැබැවින්ම ප්රසංශාත්මකය. රජය සාම ක්රියාදාමයට මුල් තැන දී තිබෙන බව දීන්
නොදන්නෙක් නැත. එය රටේ ප්රමුඛතම අවශ්යතාව සපුරාලීමේ ප්රයත්නයක් බව නොකිවමනාය. ඒ
සඳහා රටත් ජනතාවත් වෙනුවෙන් පලදායක හා වර්ධනාත්මක සහයෝගයක් විපක්ෂයෙන් රජයට ලැබෙන
බව විපක්ෂ නායකවරයාගේ ප්රකාශයෙන් පැහැදිලි වෙයි. එය සුබපෙර නිමිත්තකි. විපක්ෂයේ
සහයෝගය යන්නෙන් මෙහිදී නිර්වචනය කැරෙන්නේ විපක්ෂය හුදෙක් විවේචනයෙන් විචාරයෙන්
විමර්ශනයෙන් හා විරෝධයෙන් තොරව නිහඬව සිටින බව නොවේ. දීන් ආරම්භ වී තිබෙන සාම
ක්රියාදාමය පමණක් නොව රජයේ කවර වැඩ පිළිවෙළක් වුවද සාර්ථක කර ගැනීම සඳහා
විරුද්ධ පාර්ශ්වයේ නිර්දය විවේචනයත්, මධ්යස්ථ විචාරයත්, තීක්ෂණ විමර්ශනය මෙන්ම
සාධාරණ විරෝධයත් අත්යවශ්යය. විවේචනය කඩාකප්පල්කාරී නොවිය යුතුය. විචාරය කුහකත්වයෙන්
තොර විය යුතුය. විමර්ශනය අව්යාජ විය යුතුය. විරෝධය යුක්ති සහගත විය යුතුයි. විපක්ෂ
නායකවරයාගේ ප්රකාශයෙන් දීක්වෙන පරිදි රජයේ සාම වැඩපිළිවෙළ කෙරෙහි එතුමා දක්වන
අවධානය යථොක්ත අංශවලින් සමන්විත වුව හොත් රටේ ප්රමුඛතම අවශ්යතාව රජයේත් විපක්ෂයේත්
සමගි සහයෝගයෙන් ඉටු කර ගැනීම උගහට නොවනු ඇත. </p>

<p>jid ;snqKq ud¾. újD; lsÍu .ek;a" ud¾. ndOl bj;a lsÍu .ek;a ;ud
úreoaO fkdjk kuq;a thska idudkH ck;djf.a wdrCIdjg l=uk fyda ;¾ckhla t,a, fkdjk
;ekg j.n,d .ekSu rcfha hq;=lula njo úmCI kdhljrhd i|yka lr we;' fuho ;uka fj;
ck;dj fjkqfjka mejeÍ ;sfnk j.lSu ms</p>

<p>මෙතෙක් විපක්ෂය අනුගමනය කළ අන්දමට, රජය කරන හැම දේටම
විරුද්ධ  වීමේ ප්රතිපත්තියෙන් බැහැරවීමට තමන්
අදහස් කරන බවද විපක්ෂ නායකවරයා ප්රකාශ කර ඇත. මෙයද විපක්ෂනායකයකුගෙන් මෙතෙක් අසන්නට
නොලැබුණු අන්දමේ වැදගත් කතාවකි. ජනතා සුබ සෙත පිළිබඳ හැඟීමකින් නොව දේශපාලන
වාසිය පිළිබඳ දුෂ්ටි කෝණයෙන්ම රජයේ වැඩ පිළිවෙළවල් දෙස බැලීමේ දේශපාලන
දුර්වලතාවක් විපක්ෂ කණ්ඩායම් තුළ දිග කලක් තිස්සේ පැවැති බව නොරහසකි. වත්මන් විපක්ෂ  නායකවරයා රටේ මෙවැනි තීරණාත්මක අවස්ථාවකදී ඒ
දුර්වලතාව දුරලීමට අධිෂ්ඨාන කර ගෙන ඇති බව අසන්නට ලැබීමද සැබැවින්ම සුබ පෙරනිමිත්තකි. </p>

<p>විපක්ෂ
නායකවරයාගේ තවත් වැදගත් ප්රකාශයක් මෙසේ වාර්තා වෙයි. දීමේ සාම වැඩ පිළිවෙළේදී
රජය මෙන්ම අනෙක් පාර්ශ්වද අවංක විය යුතුයි. එක් පාර්ශ්වයක් සාමයට කැප වෙද්දී
අනෙක්  පාර්ශ්වයක් සාමයේ මුවාවෙන් වෙනත්
අරමුණු ඉෂ්ට කර ගන්නට කටයුතු කරනවාද යන්න ගැන විමසිලිමත්වීම රජයේ මෙන්ම විපක්ෂයේද
කාර්යභාරයක්. මේ අවස්ථාවේ සාමයට බාධාවක් වන කිසිම දෙයක් කතා කරන්න වත් අප සූදානම්
නැහැ. එහෙත් මේ ගැන අප අවධානයෙන් පසුවෙනවා." රට ඉතා වැදගත් සන්ධිස්ථානයකට පැමිණ සිටින
මේ මොහොතේ විපක්ෂ නායකවරයා රටත් ජනතාවත් වෙනුවෙන් තමන් වෙත පැවරී තිබෙන ඉතා වැදගත්
වගකීම පිළිබඳ පූර්ණ අවබෝධයකින් කතා කර ඇති බව පෙනේ. එතුමාගෙනුත් පොදුවේ
විපක්ෂයෙනුත් ඒ වගකීම් ඉෂ්ඨ වුවහොත් එය සාමය ප්රයත්නයේ සාර්ථකත්වයට ප්රබල රුකුලක් වනු
ඒකාන්තය.</p>

<p> </p>






</body></text></cesDoc>