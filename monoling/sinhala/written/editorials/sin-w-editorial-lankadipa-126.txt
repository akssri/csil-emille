<cesDoc id="sin-w-editorial-lankadipa-126" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-lankadipa-126.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>21.2.02
editorial</p>

<p>ඇමැතිවරයා
රටට දුන්</p>

<p>ප්රතිඥාව
කාලෝචිතයි</p>

<p> </p>

<p>කොටි
සංවිධානය සමඟ තිරසාර සටන් විරාමයකට එළැඹීම සඳහා අවබෝධතා ගිවිසුමක් සකස් කිරීමේ
කටයුතු තවම අවසන් වී නැති බවත් ගිවිසුම සකස් වුණු වහාම එය රටටත් පාර්ලිමේන්තුවටත්
ඉදිරිපත් කරන බවත් කර්මාන්ත ප්රතිපත්ති හා ආයෝජන ප්රවර්ධන ඇමැති මහාචාර්ය ජී.එල්.
පීරිස් නොබෝදා පාර්ලිමේන්තුවේදී ප්රකාශ කළ බව වාර්තා වී තිබේ. නව රජය  බලයට පත්වීමෙන් පසු ආරම්භ වී තිබෙන සාම ප්රයත්නය
දෙස මුන්ථ රට බලා සිටින මෙවැනි තීරණාත්මක අවස්ථාවක ඇමැතිවරයාගෙන් ප්රකාශ වී ඇත්තේ
ඉතා කාලෝචිත වැදගත් අදහසකි. එක් අතකින් එය වැදගත් වන්නේදී විනිවිද භාවය" පිළිබඳ
සංකල්පය මේ රටේ දේශපාලනයට එක් කළ පාලක පක්ෂයක ඇමැතිවරයකුගෙන් ඒ අදහස් ප්රකාශ
වීම නිසාය. අනෙක් අතින් එය වැදගත් කොට සැලකිය යුත්තේ කොටි සංවිධානය සමග හොර
ගිවිසුමක් ගසා ගෙන ඇතැයි සනාථ නොවුණු චෝදනාවට මැතිවරණ කාලයේ ලක් වුණු පක්ෂයක්
විසින් පිහිටුවන ලද ආණ්ඩුවේ ඇමැතිවරයකු වශයෙන් යථෝක්ත පුද්ගලයාගෙන්ම  ඒ ප්රකාශය නිකුත් වී ඇති හෙයිනි.</p>

<p> fï jk úg ú; oi oyia .Kkla ì,s f.k we;s oreKq
hqoaOhlg fya;= ldrl jqKq jeo.;a cd;sl m%Yakhla úi£u i|yd óg l,ska foj;djl§u
h;ak ±rE w.ue;sjreka fofofkl=g fydr .súiqï fpdaokd t,a, jQ fyhska fï j;dfõ
tjekaklg bvla fkd,efnk mßÈ" tlS wjfndaO;d .súiqu ilia jqKq jydu  th rgg;a md¾,sfïka;=jg;a bÈßm;a lrk nj
weue;sjrhd f.ka m%ldY ùu ldf.a;a iel ÿre ùugo fya;=jla jkq ksielh' úfYaIfhkau
ìysiqKq  úkdYhlg ux mE¥ hqoaOh ksujd rgg
iduh Wod lsßfï jev ms</p>

<p>කොටි
සංවිධානය සමග අවබෝධතා ගිවිසුමක් ඇති කර ගැනිමෙන් හෝ වෙනත් කවරම ආකාරයකින්  හෝ විසඳන්නට රජය ප්රයත්න දරන්නේ මෙරටට නිදහස
ලැබුණු වකවානුවේ සිටම නොයෙක් ආකාරයෙන් ජාතික දේහයෙහි ඔඩු දිවූ ගැටලූවකටය. සටන්
විරාමය ඇති නොවන තෙක්ම විශේෂයෙන් ම සාමාන්ය ජනතාව අන්ත පීඩනයට පත් කළ ගැටලූවකටය.
ආර්ථික වශයෙන් රට අන්ත  පරිහානියකට පත්
කළ ගැටලූවකටය. අම්මලා තාත්තලා  දස දහස්
ගණනකට දො පුතුන්  ද දො පුතුන් දස දහස් ගණනකට
අම්මලා තාත්තලා ද පවුල් ලක්ෂ සංඛ්යාත ගණනකට උන් හිටිතැන්ද අහිමි කළ ගැටලූවකටය. තවමත්
නිශ්චිත  විසර්මක් පේන තෙක් මානයක නැති
ගැටලූවකටය. එහෙයින් හ් සඳහා සැකසෙන ගිවිසුම්, ඉදිරිපත් වන යෝජනා, එළැඹෙන
නිගමන  හා ගනු ලබන තීරණ යනාදී  සියල්ල අඩු ලූහුුඩු නැතිව දීන ගන්නට ඒ ගැටලූවෙන්
දිගු කලක් තිස්සේ දුක් විඳින ජනතාවට අයිතියක් තිබේ. එහෙයින් ඇමැතිවරයාගේ යථෝක්ත
ප්රකාශය ඒ අතින්ද වැදගත් වෙයි.</p>

<p> fmdÿ fmruqKq rch n,hg m;a lsÍfï§ fukau th
mrojd j;auka tlai;a cd;sl fmruqKq rch n,hg m;a lsÍfï§o" hqoaOfhka fyïn;aj
wd¾:sl wfyaksfhka  mSä;j isá furg
ck;djf.a uQ,sl wNsm%dh jQfha hqoaOfha ksudj;a idufha Wodj;a njg lsisu ielhla
ke;' ck;djf.a ta wNsm%dh bIag lrk njg j;auka rch ue;sjrK m%pdrl iufha§
ck;djg  m%;s« ÿka njo ryila fkdfõ' ta
i|yd rch ork iEu m%h;akhlau úksúo Ndjfhka 
hqla; úu" by; i|yka jqKq mßÈ" ck;djf.a iel ÿre lsÍug muKla
fkdj Tjqkaf.a woyia Woyia ,nd .ekSug WmldÍ jk fyhska hï hï W!k;d ÿr,Sugo th
msgqn,hla fjhs" wfkla w;g ta woyia Woyia ys;¨ fyda lgl;d u; fkdj h:d¾:
o¾Ykh u; mokï jk fyhska tajd fndfyda 
ÿrg iqnjd§ ùugo bv ;sfí'</p>

<p>''මේ
සම්බන්ධයෙන් අපට තවම ගිවිසුමක් නැහැ. පැවැති සාකච්ඡාවලදී අදාළ කරුණු අධ්යයනය කළා.
සාකච්ඡා වට කිපයක්ම පැවැත්වුණා. එහි ප්රතිඵලයක් වශයෙන් බොහෝ ලිපි ලේඛන
පිළිවෙළ වුණා. මෙය ක්රියාවලියක් පමණයි. නිශ්චිත ගිවිසුමක් තවම නැහැ" යනුවෙන්ද
ඇමැතිවරයා  ප්රකාශ කර ඇත. දීනට පවත්නා
තත්ත්වය එසේ අනාවරණය කළ  ඇමැතිවරයා
අවසන් ගිවිසුම සැකැසුණු විට   එය රටටත්
පාර්ලිමේන්තුවටත් ඉදිරිපත් කරන බවද අවධාරණය 
lrhs' fuf;la ieleiqKq fujeks .súiqï md¾,sfïka;=jg bÈßm;a jk f;la ta
ms  fudfyd;la t  m%;s«j b;d jeo.;ah' ldf,daÑ;h' th ta
wdldrfhkau bIag ù rgg iduh Wod fõjd hs wms m%d¾:kd lruq'</p>

<p> </p>






</body></text></cesDoc>