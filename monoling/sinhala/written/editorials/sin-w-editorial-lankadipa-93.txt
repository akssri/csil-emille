<cesDoc id="sin-w-editorial-lankadipa-93" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-lankadipa-93.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p> </p>

<p>වැසි
වැටුණත් දිවා කාලයේදී විදුලිය කපා හැරීමේ තීරණයක් මෙතෙක් ගෙන නැති බව විදුලි බල හා
බලශක්ති ඇමැති දේශබන්ධු කරු ජයසූරිය මහතා ප්රකාශ කර ඇතැයි වාර්තා වී තිබේ. යම්
හෙයකින් දිවා කාලයේදී විදුලිය කැපුණ හොත් කර්මාන්ත ශාලා වෙළෙඳ සල් හා වෙනත්  නොයෙක් ව්යාපාරික හා නිෂ්පාදන ආයතනවල කටයුතු
බොහෝ දුරට නතර වීමට ඉඩ ඇති හෙයින් ඒ ආයතනවලටත් ඒවායෙහි සේවය කරන අයටත් පමණක්
නොව පොදුවේ ජනතාවට ද බරපතල ගැටලූවලට මුහුණ පෑමට සිදු වනු නිසැකය. එහෙයින් දිවා
කාලයේ විදුලිය කැපීමට ඇමැතිවරයා මෙතෙක් තීරණයක් ගෙන නැති බව අසන්නට ලැබීම විදුලි බලය
පාවිච්චි කරන කාගේත් සතුටට කරුණකි. එහෙත් ''මෙතෙක් තීරණයක් ගෙන නැතැ" යි කීමෙන්  ''ඉදිරි කාලයේදී තීරණයක් ගන්නට සිදුවනු ඇත"යි
යන්නද ධ්වනිත වන හෙයින් විදුලි පාරිභෝගිකයන්ගේ එකී සතුට මඳක් තුනී වී යනවාද ඇත.</p>

<p> </p>

<p> ta flfia jqjo iEfyk ld,hla ;siafia rd;%S
ld,fha§ ±kgu;a úÿ,sh lefmk meh foll fyda fol yudrl ld,h úÿ,s mdßfNda.slhka ±ä
mSvdjlg m;a lrkakla nj wuq;=fjka lsj hq;a;la fkdfõ' úÿ,sh lefmk ld,h mefha isg
fï jk úg meh fol;a fol yudr;a olajd jeäùfuka tlS mSvdj jeä jqKd ñi wvq
fkdjQfhah' bÈßfha§ fï ld,h ;j;a Èla fõf×a hk ìhla o ck;dj ;=</p>

<p> </p>

<p> Èk tlish wiQjlska rfÜ úÿ,s n, w¾nqoh úi|k njg
úÿ,s n, yd n, Yla;s weue;sjrhd úiska fok m%;s«jla ck;dj yuqfjys ;sfí' úÿ,s
mdßfNda.slhka ta fmdfrdkaÿj flfrys ±ä úYajdihla  ;ndf.k isákjdg lsisu ielhla ke;' tfyhska Tjqkaf.a úYajdih lv
fkdlrkakg úÿ,s n, yd n,Yla;s weue;sjrhd;a rch;a j. n,d .kakjd we;ehs wms ;rfha
úYajdi lruq' kshñ; Èk tlish wiQfjka miqj ck;dj ;jÿrg;a lrejf,a fkd;eîug weue;s
lre chiQßh uy;d ksis mshjr .kakjd we;ehso wms úYajdi lruq'</p>

<p> </p>

<p> c, úÿ,s n,d.dr wdY%s; c,dYj, c, uÜgu my;
neiSu úÿ,s w¾nqofha tlu fya;=j jYfhka rcfhka m%ldY jk kuq;a fjk;a nrm;, fya;=o
Bg idOl ù we;ehs lshefjkq o wikakg ,efí' úÿ,s n, uKav,fha úÿ,s ckl hka;% §¾>
ld,hla ;siafia ksis mßÈ kv;a;= fkdlsÍu;a wÆ;ajeähd l  ck;djg fokakg we;af;ao tlS ishÆ W!k;d yd wvqmdvqlï fidhd ne,Sfuka
miqjhehs úYajdi l</p>

<p> </p>

<p> miq.sh rch meje;s ld,fha f.kajk ,±hs lshk ;dm
úÿ,s ckl hka;% lSmhla fï jkf;la mdúÉÑhg f.k ke;s njlao wikakg ,efí' ñka bÈßhg
tl úkdählaj;a úÿ,sh fkdlefmk nj;a ;dm úÿ,s ckl hka;% f.kakd ;sfnk fyhska
fkdlvjd úÿ,sh iemfhk nj;a fmdÿ fmruqKq rcfha w×</p>

<p> </p>

<p> úÿ,s w¾nqoh i×gu úi£fï wruqK we;sj .,a wÛ=re
;dm úÿ,s n,d.dr bÈlsÍfï uyd mßudK jHdmD;s ms  fyhska th m%udo jk fudfyd;la mdid ck;d
wm%idoho j¾Okh jk nj wjOdrKh l</p>






</body></text></cesDoc>