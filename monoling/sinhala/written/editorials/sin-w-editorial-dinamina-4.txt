<cesDoc id="sin-w-editorial-dinamina-4" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-dinamina-4.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>දිනමිණ 2000 සැප්තැම්බර් 19</p>

<p>පිටුව
06</p>

<p>කතුවැකිය</p>

<p>ප්රභාකරන්ට
ඕනෑ දේ ඉටු කරන්නෝ</p>

<p>කොටි
ත්රස්තවාදීන් ඉකුත් සතියේ කොළඹදී පිපිරැවූ බෝම්බයේ දෝංකාරය, අෂ්රොප්
ඇමැතිවරයාගේ හදිසි අභාවය නිසා යටපත් වූ සෙයක් පෙනේ. එහෙත් එය එලෙසින් යටපත්
වන්නට ඉඩ හැරීම කීප අතෙකින් ම අනතුරුදායකය.</p>

<p>මරාගෙන
මැරෙන කොටි සාමාජිකයකු විසින් පිපිරැවූ එම බෝම්බය ගෙනෙන්නට ඇත්තේ පොදු පෙරමුණු
ආණ්ඩුවේ ඇමැතිවරයෙකු හෝ ඊටත් ඉහළ කෙනෙකු හෝ ඝාතනය කිරීම සඳහා විය හැකිය.
එහෙත් එම ඉලක්කය සපුරා  ගැනීමට අවස්ථාවක්
නොලැබ ම බෝම්බය පුපුරුවා ගැනීමට කොටියාට සිදුවූයේ ය. අහිංසක ජීවිත ගණනාවක් එයින්
විනාශ වූ අතර තවත් බොහෝ දෙනෙක් තුවාල ලැබූහ. </p>

<p>කොටි
ත්රස්තවාදීන් කොළඹ දී බෝම්බ පිපිරැවූ පළමු අවස්ථාව මෙය නොවේ. තම අරමුණු ඉටු
කැරැ ගැනීමේ එක ම මාධ්යය ත්රස්තවාදය බව විශ්වාස කරන ඔවුන් මෙවැනි කෲර මිනිස් ඝාතන
සිදු කිරීම පුදුමයක් නොවේ. එබැවින්, එවැනි අපරාධයක් සිදු කළ විට, ඒ ගැන
අඬා දොඩා ටික කලෙකින් ම එය අමතක කැරැ දීමීම සෙසු අයගේ සිරිත බවට පත් වී තිබේ. </p>

<p>එය
ඉතාම අයහපත් තත්ත්වයෙකි. එසේම අවාසානාවන්ත තත්ත්වයෙකි. මුන්ථ රටට ම ශාපයක් ව තිබෙන
කොටි ත්රස්තවාදයේ අවසානයක් දීකීමේ ක්රියා මාර්ගයකට පිවිසීමට, එම ''සෝඩා
බෝතල්'' පිළිවෙත හෙවත්, යම් දෙයක් සම්බන්ධයෙන් සුන්ථ මොහොතක් උනන්දුවක්
දක්වා, ඉන්පසු ඇල් මරුණු තත්ත්වයකට පත්වීමේ පිළිවෙත විශාල බාධාවක් හෙයිනි.
කොටි සංවිධානයේ උපාය හා උපක්රම නිවැරැදිව තේරුම් නොගෙන ත්රස්තවාදය පරාජය නොකළ
හැකිය. ඒ උපාය හා උපක්රම තේරුම් ගැනීමට නම් ත්රස්තවාදීන්ගේ සෑම පියවරක් ගැනම
විමැසිල්ලෙන් සිටිය යුතුය.</p>

<p>කවුරුන්
කවර දේ කීවත්, කොටි සංවිධානයේ අවසාන අරමුණ දෙමළ ඊළමක් අටවා ගැනීම ය.
එය සාමකාමී සාකච්ඡා මඟින් කිසිකළෙකත් ඉටු නොවන බව සෙසු අය මෙන්ම කොටි නායකයෝද
දනිති. එම නිසා එය තුවක්කුවේ බලයෙන් ඉටුකැරැ ගැනීම ඔවුන්ගේ ස්ථාවරයයි. එම ස්ථාවරය
අනුව ක්රියා කිරීමට ඔවුනට දෙමළ ජනතාවගේ සහ අන්තර්ජාතික ප්රජාවේ ආධාර අනිවාර්ය
යෙන්ම අවශ්යය. එම ආධාර කොටි සංවිධානයට ලැබෙන්නේ ලාංකීය දෙමළ ජනතාවට සාධාරණ ය
ඉටු  කිරීමට සිංහල ආණ්ඩු කවරදාවත් ක්රියා
නොකරන බව දෙමළ ජනතාවටත් සෙසු ලෝකයාටත් පෙන්වීමෙනි.</p>

<p>එහෙත්
චන්ද්රිකා බණ්ඩාරනායක කුමාරතුංග ජනාධිපතිනියගේ නායකත්වයෙන් යුත් පොදු පෙරමුණු
ආණ්ඩුව නව ආණ්ඩුක්රම ව්යවස්ථා මගින් ඉදිරිපත් කරන විසඳුම සම්මත වුවහොත් දෙමළ
ජනතාවගේ සහයෝගය හා අන්තර් ජාතික ප්රජාවේ සහයෝගය කොටි සංවිධානයට අහිමිවීම වැලැක්වීමට
කොටි සංවිධානය සෑම උත්සාහයක්ම දරයි. ඒ සඳහා ඔවුන් අනුගමනය කරන උපක්රමය චන්ද්රිකා සහ
ඇගේ ආණ්ඩුව විනාශ කොට කොටි පදේට නැටැවිය හැකි ආණ්ඩුවක් ලබයට පත්  කැරැ ගැනීමයි. </p>

<p>කොටි
සංවිධානය මොන අපරාධය කළත් ඒවා හෙළා දීකීමට එක වචනයක් පවා නොකියන එජාප
නායකත්වය පොදු පෙරමුණු ආණ්ඩුවේ දේශපාලන යෝජනාවලියත්, එය පදනම් කැරැගෙන
සැකැසුණු නව ව්යවස්ථා කෙටුම්පතත් සම්බන්ධයෙන් අනුගමනය කරන කල් මැරීමේ හා කඩාකප්පල්
කිරීමේ පිළිවෙත බලවත් සැකයට පාත්ර වන්නේ එය කොටි සංවිධානයට ද අවශ්ය ව තිබෙන දේ
බැවිනි. ඉකුත් සතියේ කොළඹදී පිපිරැවුණු කොටි බෝම්බය සම්බන්ධයෙන් පවා කොටි
සංවිධානය හෙළා දීකීමට එජාප නායකයා අපොහොසත් වීම ජනතාව තුළ ජනිත කරනුයේ
සාධාරණ සැකයෙකි.</p>

<p>එම
නිහැඬියාව ගැන ගුවන් විදුලියේ සුභාරතී වැඩ සටහනට ප්රකාශයක් කළ එජාප මාධ්ය ප්රකාශක
ආචාර්ය කරුණාසේන කොඩිතුවක්කු මහතා කීවේ, එම බෝම්බ සිද්ධිය ගැන එජාපය කිසිවක්
නොදීන සිටි නිසා බව ය. එසේ නම් පසුගිය අප්රේල් මාසයේ දී කොටි ත්රස්තවාදීන් හිටි අඩියේ
ප්රහාරයක් අරඹා අලිමංකඩ අල්ලාගත් සැණින් යාපනය නගරයද කොටින් අතට පත්වී ඇතැයි එජාප
නායක රනිල් වික්රමසිංහ මහතා දෙස් විදෙස් මාධ්යවේදීන්ට ප්රකාශ කෙළේ ඒ බව දීනැ සිටි
නිසාදීයි කොඩිතුවක්කු මහතා කීවේ නැත. </p>

<p>රනිල්ට
හැකි නම්, ප්රභාකරන් මිනීමරුවෙකැයි කියන ලෙස එජාප විකල්ප කණ්ඩායමේ නායක විජේපාල
මෙන්ඩිස් මහතා අභියෝග කරන්නේ එබැවිනි.</p>

<p> </p>






</body></text></cesDoc>