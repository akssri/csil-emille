<cesDoc id="sin-w-editorial-lankadipa-79" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-lankadipa-79.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ජනතා අයිතිය නසන්නට මැරයන්ට ඉඩ
දෙමුද?</p>

<p>නාවලපිටිය ප්රදේශයේ නොබෝදා පැවැති එක්සත් ජාතික පක්ෂ පොකුරු රැස්වීම්
කීපයකට සහභාගි වූ එ.ජා.ප. ආධාරකරුවන්ට ප්රදේශයේ පොදු පෙරමුණු දේශපාලනඥයකු සහ
ඔහුගේ මැර පිරිසක් විසින් පහර දෙන ලදැ යි කියන සිද්ධිය ප්රජාතන්ත්රවාදයත් ශිෂ්ටත්වයත්
අගයන සෑම කෙනකු ගේ ඉමහත් පිළිකුලට ලක් වනු නොඅනුමානය. එතරම් ම පහත් කි්රයාවක්
නිසාය.</p>

<p>මැර
පිරිස පහර දීමෙන් ම නොනැවතුණහ. ඔවුන් රැස්වීම් පැවැත්වූ ස්ථානවල මේස පුටු කඩා දමා කොඩි
වැල් ද කඩා දැමූ බවද වාර්තා වී ඇත. නාවලපිටියේ එ.ජා.ප. සංවිධායකවරයා ආරක්ෂාව ඇතිව
ඔහුගේ නිවාසයට කැඳවාගෙන ඒමට පොලීසියට සිදු වූ බව ද තවදුරටත් වාර්තා වුණේය.</p>

<p>මැර
පිරිසේ කි්රයාවේ තරම තිරම එයින් ම වටහා ගැන්ම අසීරු නොවේ. සිද්ධියට සම්බන්ධ යැයි
කියන දේශපාලකයා පමණක් නොව පොදු පෙරමුණ ම ලජ්ජා විය යුතුය. දේශපාලන සංවිධානයක්
හැටියට පොදු පෙරමුණ මෙවන් කි්රයා ගැන කියන්නේ කුමක් ද? අනුමත කරන්නේ ද? නැද්ද?
මෙවැනි දඩබ්බරකම් සිදු වූ පළමු වැනි වතාව මෙය නොවේ. පසු ගිය වයඹ ඡන්ද සමයෙන්
උදාහරණ ගණනාවක් ලැබෙයි. එ.ජා.ප.යේ ජයග්රහණය පතා වැඩ කළ කාන්තාවක නිරුවත් කොට
මහ මඟ ගෙන යන ලද්දේ යැයි කියන අසික්කිත සිද්ධිය වාර්තා වුණේ ද වයඹ පළාතින් ය.</p>

<p>මේ
පොකුරු රැස්වීම්වලට සහභාගිවූවන්ට පහර දෙන ලද්දේ ඇයි? කවර වරදක් කළාට ද?
දේශපාලන රැස්වීමක් පැවැත්වීමත් ඊට සහභාගිවීමත් වරදක්ද? නැත. එහෙම නම් පහර දුන්නේ ඇයි?
රැස්වීමට සහභාගි වූවන් අතින් වරදක් සිදු වුණත් ඔවුන්ට පහරදීමට කිසිම දේශපාලකයකුට
අයිතියක් නැත. නීතියෙන් අවසරයක් ද නැත.</p>

<p>නිදහස්
මත දරන්නටත් නිදහස් ලෙස අදහස් දක්වන්නටත් මේ රටේ සෑම පුද්ගලයකුට ම අයිතියක් ඇත.
ඒ මූලික අයිතියකි. ඒ එක්කම රැස්වීම් පවත්වන්නට ද අයිතියක් ඇත. රැස්වීම් කඩාකප්පල්
කිරීමත් රැස්වීම්වලට සහභාගිවූවන්ට පහරදීමත් එකී අයිතිවාසිකම්වලට පහරදීමක් වෙයි. එය ඉතා ම
බරපතල වරදෙකි.</p>

<p>සමහර
දේශපාලකයෝ ඒ වග නොදනිත්ද? නොදනිති යි සිතිය නොහැකි ය. නොදන්නේ නම් ඔවුන්
දේශපාලනයට ජනතා සේවයට සුදුසු වන්නේ කෙසේදැයි අපි නොදනිමු.</p>

<p>ඔවුන්
ගැන තීන්දුවක් ගැනීම දේශපාලන පක්ෂ ප්රධානීන් ගේ වගකීමයි. ඔවුන්ට ඒ සඳහා ශක්තියක්
නැත්නම් ඡන්ද දායකයන්වත් ඒ ගැන කල්පනා කළ යුතුය.</p>

<p>මහජන
වරමකින් තොරව ඕනෑම පුද්ගලයකුට දේශපාලනය කරන්නට සවියක් නොලැබෙයි. මන්ත්රී ධුර ද ලද
නොහැකි ය. වරම් දිය යුත්තේ අනුවණයන්ට නොවේ. හැකියාවකින් තොර පුද්ගලයන්ට ද නොවේ.
දඩබ්බරයන්ට ද නො වේ. සොරුන්ට හා දොෂිතයන්ට ද නොවේ. එවැනි පුද්ගලයන්ට වරම් ලැබෙන
තරමට රටට ම සිදු වනුයේ ලොකු පාඩුවකි.</p>

<p>ඡන්ද
දායක දායිකාවන් නිසි ලෙස කල්පනා කළ යුතුව ඇත. දේශපාලකයන්ට ජනතාව වරම් දෙනුයේ
ඉහළ ම බලාපොරොත්තුවක් ඇතිව ය. ඒ බලාපොරොත්තුව නම් ඉහළ ම වාසනාව ඔවුන්
මාර්ගයෙන් ලබා ගැනීම යි. නොසුදුස්සන්ට වරම් දීමෙන් කිසි දා ඉහළ ම වාසනාව ලද
නොහැකි ය. බරපතල ආකාරයෙන් කල්පනා කළ යුතු ව ඇති ප්රධාන හේතුවක් වන්නේ එය
යි.</p>

<p>යථොක්ත
සිද්ධිය වැනි සිද්ධිවලින් ප්රකට වන්නේ සමහර දේශපාලකයන්ගේ දේශපාලන නොදියුණුකමයි.
නොමේරුකමයි. පටු ආකල්පයයි. පටු දෘෂ්ටියයි. ප්රජාතන්ත්ර විරෝධිභාවය යි. අන්යයන්ගේ
අදහස් හෝ මත හෝ ඉවසීමට ඇති නොහැකියාවයි. එවැන්නවුන් ප්රජාතන්ත්රවාදි දේශපාලන
ක්රමයටත් එහි වැදගත් අංගයක් වන පක්ෂ දේශපාලන ක්රමයටත් සුදුසු වන්නේ කෙසේ දැයි අපි
නොදනිමු.</p>

<p>දේශපාලන
දුබලයන් ගෙන් රටට ලැබෙන්නේ නරක ආදර්ශයෙකි. ඔවුන් ගෙන් සිදු වන හානිය සුන්ථ පටු නොවේ.</p>

<p>පසුගිය
මැතිවරණ කාලවල දී මෙවැනි සිද්ධි සිදු විණි නම් දැනුදු සිදුවන්නේ නම් ඉදිරි මැතිවරණ කාලවල දී
නොසිදු වෙතියි කාට කිව හැකි ද? මැතිවරණයක් නැති අවස්ථාවක දී මෙවැනි දෙයක් සිදු විණි
නම් එය සුබ ලකුණක් නො වේ.</p>

<p>මේවා පෙරහුරු විය හැකි
ය. මැතිවරණ සමයෙක දී සිදු වුණොත් නිදහස් හා සාධාරණ ඡන්ද පිළිබඳ ව ජනතාව තුළ
ඇති අවංක බලාපොරොත්තු සුන් වෙයි. එහෙම වුණොත් ඉහළ ම වාසනාව ළඟා කැරැ
ගැන්ම අසීරු ය.</p>

<p>ස්වාධීන මැතිවරණ
කොමිසමකත් ස්වාධීන පොලිස් කොමිසමකත් වැදගත්කම මෙවැනි සිද්ධිවලින් සක් සුදක් සේ
පෙනේ. ප්රජාතන්ත්රවාදයත් මූලික මිනිස් අයිතිවාසිකම් ආදියත් තහවුරු කැරැ ගත හැකි වන්නේ
යහපත් තත්ත්වයක් පවත්වා ගන්නට රට ම ඊට සූදානම් වුණොත් පමණි.</p>

<p> </p>






</body></text></cesDoc>