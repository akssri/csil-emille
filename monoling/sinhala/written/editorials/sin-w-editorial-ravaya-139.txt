<cesDoc id="sin-w-editorial-ravaya-139" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-ravaya-139.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ගොයම
කන්නේ වැටත් නියරත් නම් කළ හැක්කේ කුමක් ද?</p>

<p>17
වැනි ව්යවස්ථා සංශෝධනය යටතේ පිහිටුවීමට නියමිත ස්වාධීන කොමිෂන් සභාවලට පත් කළ
යුතු පුද්ගලයින්ගේ නාමලේඛනය ව්යවස්ථා සභාව විසින් ජනාධිපතිනිය වෙත යවා දැන් මාසයකට
ආසන්න කාලයක් ගතවී ඇතත්, ජනාධිපතිනිය එම නාමලේඛනය අනුමත නො කිරීම නිසා
ස්වාධීන කොමිෂන් සභා ක්රමය අවුල් ජාලාවක් බවට පත්වී තිබේ.</p>

<p>17
වැනි ව්යවස්ථා සංශෝධනය දේශපාලන පක්ෂවල අභිලාෂයක් නො වී ය. එය යහපත් ආණ්ඩුකරණයක්
සඳහා උනන්දුවක් දක්වන සිවිල් සමාජ සංවිධානවල බලවත් ඉල්ලීමට දේශපාලන පක්ෂවලින් ලැබුණු
එකඟත්වයක් පමණක් විය. එය ප්රමුඛ තනතුරුවලට පුද්ගලයන් පත්කිරීමේදී රාජ්ය නායකයා හෝ
නායිකාවට තිබෙන අභිමත බලය වැරදි ලෙස පාවිච්චි කිරීම වළකා එවැනි තනතුරුවලට පටු
දේශපාලන භේදයෙන් තොරව සුදුසුම පුද්ගලයන් පත් කිරීමට බලකෙරෙන ව්යවස්ථා සංශෝධනයක්
ලෙස සැලකිය හැකිය. මෙම ව්යවස්ථා සංශෝධනය නීතිගත කරන අවස්ථාවේදී මුල් අරමුණ විකෘති
කෙරෙන ආකාරයේ විකෘති කිරීම් ගණනාවකට ලක්විය. ව්යවස්ථා සංශෝධනය සම්මත වී තවත් පසුකලෙක
ව්යවස්ථා සභාවක් බිහිවූයේ ඒ විකෘති කිරීම් මැද්දේ ය.</p>

<p>අවසානයේ
බොහෝ ප්රමාද වී හෝ ව්යවස්ථා සභාව යටතේ ඇති කිරීමට නියමිත ස්වාධීන කොමිෂන් සභාවලට
පත්කළ යුතු ප්රධානීන් හා කොමසාරිස්වරුන්ගේ නාමලේඛනයක් ව්යවස්ථා සභාව විසින්
අවසාන අනුමැතිය සඳහා ජනාධිපතිනිය වෙත යවා දැන් මාසයක් තරම් කාලයක් ගතවී ඇතත්,
ජනාධිපතිනිය එම ලැයිස්තුව හිරකරගෙන සිටින පිළිවෙතක් අනුගමනය කරමින් සිටී.</p>

<p>මෙම
ව්යවස්ථා සංශෝධනය විවිධ විකෘති කිරීම් මැද එක් දිනක් තුළ කඩිමුඩියේ සම්මත කරගන්නා
ලද්දකි. සමහර අර්ථ නිරූපණවල නිශ්චිත බවක් නැති අතර, අර්ථ නිරූපණයක් නැති අවස්ථා
ද පවතී. ඒ සියලූ සීමිතකම් තිබියදීත් ව්යවස්ථා සභාව නිර්දේශ කරන නාමලේඛනයක් අප්රමාදව
අනුමත කිරීමට ජනාධිපතිනිය ව්යවස්ථානුකූලව බැඳී සිටී. එසේ නොකිරීම ව්යවස්ථා
උල්ලංඝණය වරදක් ලෙස සැලකිය හැකි ය.</p>

<p>ckdêm;sksh jHjia:d iNdjg ks¾foaY lr ;sfnk kduf,aLkh wkqu; lsrSu ys;du;d
u m%udo lrhs kï" ta ksid we;sjk w¾nqoh ch.; hq;af;a flfiao hk ldrKh i,ld
ne,Sfï j.lSu we;af;a md¾,sfïka;=jg h' th uyck;djf.a fyda ckudOHj, lrmsg oud weÛ
fírd.; fkd yels h' md¾,sfïka;=jla ;sfnkafka kS;s iïmdokhg yd m%;sm;a;s iïmdokhg
h' ;uka yok kS;s l%shd;aul fkdjk ;;a;ajhla mj;S kï tajd l%shd;aul lrk wdldrfha
w;=re kS;suh úê úOdk we;s lsrSu o md¾,sfïka;=fõ j.lSuls'</p>

<p> </p>

<p> </p>






</body></text></cesDoc>