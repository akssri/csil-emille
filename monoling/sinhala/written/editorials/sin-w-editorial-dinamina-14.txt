<cesDoc id="sin-w-editorial-dinamina-14" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-dinamina-14.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>නැවතත් කොටි උගුලක වැටිය යුතු නැත.</p>

<p> </p>

<p>වසර 20 ක් තිස්සේ ශ්රී ලංකාවේ ජන ජීවිතය විනාශ
කිරීමෙහි යෙදුණ, 60000 කගේ  ජීවිත විනාශ
කළ කොටි ත්රස්තවාදීන්ගේ ජාතිවාදී බෙදුම්වාදී අරගලය නිමා කිරීම සඳහා 1994 වසරේ සිට
පොදු පෙරමුණු රජය කොටි සංවිධානය සමඟ කීප වතාවක්ම සාකච්ඡා පැවැත්වූ නමුත් ඒ සියල්ල
අසාර්ථක විය. ත්රස්තවාදියා නැවත නැවතත් ත්රස්තවාදයෙහිම යෙදුණේය.</p>

<p> </p>

<p>මෙහිදී යුදමය ක්රියාදාමයකින් ත්රස්තවාදය මෙරටින්
තුරන් කිරීමේ අරමුණින් යුදමය පදනමක පිහිටා සටන් වැදීමෙහි ශ්රී ලංකා රජය නිරත විය.
බෙලහීන වන විට අවි ආයුධ ලබා ගැනීමේ මං ඇහිරෙන විට, පරාජය වළකා ගැනීම සඳහාත්
යළි ශක්තිමත් වී සටනට බැසීමේ අරමුණෙන් යුතුවත් සාමයට කතා කිරීම ඕනෑම සතුරු පක්ෂයක
මානසිකත්වයයි. කොටි සංවිධානය කීප වතාවක්ම මෙම උපාය අනුගමනය කළේය.</p>

<p>දීන් නැවත ද කොටි නායක ප්රභාකරන් සාම
සාකච්ඡාවකට කැමැත්ත පළ කර තිබේ. මීට පෙරාතුව ද කොටි සංවිධානය සාම සාකච්ඡාවකට සහභාගි
කරවීමට නෝර්වේ රජය උත්සාහ කළ නමුත් එය ව්යර්ථව ගියේය. මෙවරත් කොටි නායක
ප්රභාකරන්ගේ ඉල්ලීම පිට නෝර්වේ විශේෂ නියෝජිත එරික් සෝල්හයිම් ඇතුන්ඵ නියෝජිත
පිරිසක් මුලතිව් වනයට ගොස් පසුගිය 1 වැනිදා ප්රභාකරන් මුණගැසී තිබේ. එම
නියෝජිතවරයාත් ශ්රී ලංකාවේ නෝර්වීජියානු තානාපතිවරයාත් ජනාධිපතිනි චන්ද්රිකා බණ්ඩාරනායක
කුමාරතුංග මහත්මිය හමුවී ප්රභාකරන්ගේ යෝජනා ජනාධිපතිනිය හමුවේ තැබූහ. එම යෝජනා
කවරේදීයි මාධ්යයන්ට හෙළිදරව් කිරීම නෝර්වීජියානු තානාපතිවරයා විසින්් ප්රතික්ෂේප කර
ඇත.</p>

<p> </p>

<p>මෙම යෝජනා ගැන ජනාධිපතිනිය අද ඇමැති
මණ්ඩලය සමඟ සාකච්ඡා කිරීමට නියමිතය. මේ අතර කොටි සංුධානය සමඟ සාකච්ඡා කිරීමට එක්සත්
ජාතික පක්ෂය සහ අනිකුත් දෙමළ කණ්ඩායම් එකමීත්වය පළ කර ඇතැත් ජනතා ුමුක්ති
පෙරමුණ හා සිහළ උරුමය ුරෝධය ප්රකාශ කර සි'. ඊ.පී.×.පී. පක්ෂය කියන්නේ කොටි
සංුධානය කෙරෙහි ුශ්වාසයක් තැබිය නොහැකි බවයි.</p>

<p>මේ අතර නොවැම්බර් 9 වැනිදා සිට 27 වැනිදා
දක්වා කොටි සංුධානයේ වීර සතිය පවත්වන බව ප්රභාකරන් විසින්් ප්රකාශයට පත් කර ඇතැයි ද
තොරතුරු ලැණී තිබේ. සාමාන්යයෙන් ම වීර සතිය තුළ මහා ප්රහාරයන්හි යෙදීම කොටි
සංුධානයේ පුරුද්දය. එම නිසා සාකච්ඡා මුවාවෙන් හැම දෙනාගේ ඇස් වසා කිසියම් කූට
උපාය මාර්ගයක යෙදීමට කොටියා අරඅඳින්නේ ද යන්න ගැන ද පරෙස්සම් විය යුතුව ඇත්තේය.
කපන්නට බැවි අත සිඤුන සිවිතක් පවත්නා බව අමතක කළ යුතු නැත.</p>

<p>තවත් කරුණක් නම්, කිසිදු කොන්දේසියකින්
තොරව සාකච්ඡා මඟට එළැඹීමට කොටි සංවිධානය සූදානම් යැයි නෝර්වීජියානු නියෝජිතයන්
කියතත්, සාකච්ඡා ඇරඹීම සඳහා කොටි සංවිධානය කොන්දේසි කීපයක් ඉදිරිපත් කර ඇතැයි
ලන්ඩනයේ කොටි කාර්යාලය නිවේදනය කරයි. එම කොන්දේසිවලට අනුව, උතුර - නැගෙනහිර
ගැටුම්කාරී තත්ත්වය පාලනය කළ යුතුය. එදිරිවාදුකම් අත්හිටුවීමේ එකඟතාවක් ඇති කළ
යුතුය. උතුර - නැගෙනහිර හමුදා ක්රියාකාරකම් සීමා කළ යුතුය. </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p>යාපනයේ අල්ලාගත් ප්රදේශවලින් හමුදාව ඉවත්
විය යුතුය. ආර්ථික සම්බාධක ඉවත් කළ යුතුය. දෙමළ ජනතාව ජීවත්වන ප්රදේශවල සිුල්
පවිපාලනය තහවුරු කළ යුතුය.</p>

<p>මේ තත්ත්වය මෙසේ පවතිද්දී බි්රතාන්ය රජය විසින්්
එල්.ටී.ටී.ඊ. සංවිධානය තහනම් කළ යුතු යැයි ශ්රී ලංකා විදේශ ඇමැති ලක්ෂ්මන් කදිර්ගාමර්
මහතා බි්රතාන්ය රජයෙන් ඉල්ලා සි'. කොටි සංවිධානය විසින්් මුලතිව් මුහුදේදී රුසියානු
නැවක් පැහැරගෙන යෑම ගැන රුසියානු රජය විරෝධය පළ කර සිටී. මේ අතර පසුගිය මහ
මැතිවරණයේදී අගමැති රත්නසිරි වික්රමනායක මහතා රටේ මුන්ඵ මහත් ජනතාව හමුවේ ප්රතිඥා
දුන්නේ යුදමය ක්රියාදාමයෙන් කොටි ත්රස්තවාදයේ මුල් සුන් කරන බවටය. එහෙත් ජනාධිපතිනිය
ප්රකාශ කරන්නේ තමා ජනවරමක් ලබා ඇති බැවින් මීට පෙර ඉදිරිපත් කළ ව්යවස්ථාව කවර
ක්රමයකින් හෝ ක්රියාත්මක කරන බවය.</p>

<p> </p>

<p>මෙය ඇත්ත වශයෙන්ම පසුගිය මහ මැතිවරණයේදී
ඡන්දය ප්රකාශ කර පොදු පෙරමුණු රජය බලයට පත් කළ කෝටි සංඛ්යාත ශ්රී ලාංකික ජනතාවට
අන්තෝ ජටා බහි ජටා තත්ත්වයකි. රජයේ අරමුණ යුද්ධ කර සාමය ස්ථාපිත කිරීම ද,
එසේ නැත්නම් අනාගතය ගැන නොසිතා කවර ක්රමයේ හෝ නාමික සාමයක් ඇති කිරීම ද යන්න
පැහැදිලි නැත. එසේ නාමික සාමයක් ඇති කිරීමෙන් නොබෙල් සාම ත්යාග ලැබ ලෝක පූජිත වූ
රාජ්ය නායකයන් ගැනත් පසු කලෙක එම රටවල් තව තවත් අවුල් තත්ත්වයකට පත් වූ බවත් අප අසා
ඇත. එම නිසා අරමුණ විය යුත්තේ නොබෙදුණු ශ්රී ලංකාවක් යටතේ සාමය ස්ථාපිත කිරීමය. එය
නව රජයේ වගකීමය.</p>

<p> </p>

<p> </p>

<p> </p>






</body></text></cesDoc>