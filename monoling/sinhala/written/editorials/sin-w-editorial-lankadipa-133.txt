<cesDoc id="sin-w-editorial-lankadipa-133" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-lankadipa-133.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>රටේ
ලොකුම රෝහලේදී වත් රෝගීන්ට ආරක්ෂාවක් නැද්ද?</p>

<p>2003
ජනවාරි මස 07</p>

<p> </p>

<p>කොළඹ
ජාතික රෝහලේ 50 සහ 54 වාට්ටුවල ප්රතිකාර ලබමින් සිටි රෝගීන් දෙදෙනකු සාහසිකයන්
තිදෙනකු විසින් පිහියෙන් ඇන මරා දමන ලදීයි කියන සිද්ධියක් ඊයේ වාර්තා වූයේ ය. තවත්
රෝගියකුට තුවාල සිදු වී ඇතැයි ද කියති.</p>

<p>වාර්තා වී ඇති
පරිදි ඝාතනයට ලක් වූ මේ අය රෝහල් ගත කරනු ලැබ සිටියේ නව වසර උදාවීම නිමිත්තෙන්
රතිඤ්ඤා පත්තු කිරීමක් සම්බන්ධයෙන් දෙපිරිසක් අතර ඉකුත් 31 වැනි දා ඇති වූ ගැටුමකින්
තුවාල ලැබීමෙන් පසුව ය. මේ සිද්ධියේ තුවාල ලැබූවන් හය දෙනෙකුගෙන් තිදෙනකු වෛද්ය
ප්රතිකාර ලබා රෝහලෙන් පිටව ගොස් සිටි අතර ඝාතනයට ලක් වූ දෙදෙනා හා තවත්
පුද්ගලයෙක් ප්රතිකාර ලබමින් රෝහලේ නැවතී සිටිය හයි ද යථෝක්ත වාර්තාවේ සඳහන් වෙයි.</p>

<p> .egqug fudk fudk fya;= ksshñka yd wkshñka
n,mEfõ ±hs wmg lsj fkdyels h' ta fjku ldrKhls' fuys § fl</p>

<p>සාමාන්යයෙන්
ගැටුමක දී තුවාල ලබන අය ප්රතිකාර ලබා ගනු පිණිස පැමිණෙන්නේ රජයේ මහ රෝහල් වෙතට ය.
එවැන්නවුන් බාර ගන්නට බොහෝ පෞද්ගලික රෝහල් මැලිකම් දක්වන නිසාය. ගැටුම සිදු වී
ඇත්තේ කොළඹ හෝ ඒ අවට ප්රදේශයක හෝ නම් තුවාල කරුවන් කොළඹ මහ රෝහලට
රැගෙන ඒම  සාමාන්ය සිරිතය. එසේ තුවාලකරුවන්
රැගෙන එනුයේත් තුවාලකරුවන් පැමිණෙනුයේත් ලොකු විශ්වාසයකින් යුතුවය. එක් අතෙකින්
තමන්ට හොඳ ප්රතිකාර ලැබී ඉක්මන් සුවයක් ලැබේ යැයි ඔවුන් බලාපොරොත්තු වනු සිකුරුය.
ප්රතිකාර ලබන අතර රෝහල් වාට්ටුවක් තුළදී පිහියා ඇනුම් කා මිය යන්නට සිදු වෙති යි
ඔවුහු කිසි විටෙක බලාපොරොත්තු නොවෙති. </p>

<p>රෝහල් පරිශ්රය
තුළට පැමිණි තමන්ට හානියක් සිදුකළ හැකි ඉඩකඩක් රෝහල තුළ ඇතැයි ඔවුන්ට
හැඟී ගියානම්  ඔවුන් එම රෝහල තුළ
නැවතී තවදුරටත් ප්රතිකාර ගැනීම වෙනුවට වෙනත් කි්රයාමාර්ගයක් ගනු නොඅනුමානය. මෙසේ මරු
මුවට පත්වන්නට කිසි කෙනෙකු කැමැති නොවන නිසා ය.</p>

<p>මේ සිද්ධියෙන්
එකක් පැහැදිලිය. කොළඹ මහ රෝහලේ ආරක්ෂක විධි විධාන ප්රමාණවත් නොවන බව යි.
අඩුපාඩුවලින් ගහණ බවයි.</p>

<p> jd¾;d ù we;s mßÈ ñkSurejka frday,a mßY%h
;=  wfõ,dfõh'
w;awvx.=jg .kq ,enQ m%Odk iellref.ka m%Yak lsÍfï§ fy</p>

<p> fï j. fmkS .sh jdr fndfyda h' óg l,ska wjia:d
lsysmhl § u yÈis wk;=re wxYfha jdÜgqjl m%;sldr ,nñka isá fujka we;euqkag
Tjqkaf.a i;=rka úiska ydks meñKjqKq nj jd¾;d ù ;snqK" wmg u;lh' ta
wjia:dj, § o fkajdisl frda.Skaf.a wdrlaIdj ms</p>

<p> fld 
fyda WÑ; wdrlaIdjla ie,iS ;sìKsoe hs iy;sl fldg lshkakg mq¿jkao@ mq¿jka
fj;shs wms fkdis;uq'</p>

<p> f.aÜgqfõ wdrlaIlhl=g w,a,ila § frdayf,a
mßY%hg msúishd kï >d;lhka m%Yakhlska f;drj jdÜgqj,g we;=¿ jQfha flfiao@
jdÜgqj,g wfõ,dfõ meñfKkakka .eko úuid n,kakg wdrlaIlhl= ke;slulskao@ úh yelsh'</p>

<p> f,vqka n,kakg tk mqoa.,hkag ~~mdia¶ ksl=;a
lsÍfï § l%uhla o ;snqKd wmg u;lh' ta l%uh wNdjhg f.dia we;s ieáhls' tjka l%uhla
ksis f,i ls%hd;aul fjf;d;a fujka ieyeis ls%hdjkag tla;rd ndOdjls'</p>

<p> fld</p>

<p> </p>

<p> </p>






</body></text></cesDoc>