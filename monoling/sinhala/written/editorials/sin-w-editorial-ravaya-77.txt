<cesDoc id="sin-w-editorial-ravaya-77" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-ravaya-77.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>''වෘක වත"</p>

<p>උතුරු
නැගෙනහිර ප්රදේශවල හැර රටේ අනිකුත් ප්රදේශවල පළාත් පාලන ඡන්ද විමසීම් මේ වනවිට
හමාර වී තිබේ. මෙම මැතිවරණවලදී බලයේ සිටින එක්සත් ජාතික පෙරමුණට විශිිෂ්ට ජයක් අත්කර
ගැනීමට හැකි වී ඇත. ඇතැම් ප්රදේශවල පැවැති ඡන්ද විමසීම්වලින් එජාපෙරමුණ ලැබූ
ජයග්රහණයන්හි සුජාතභාවය පිළිබඳව බරපතල සැක මතු වී ඇතත් පොදුවේ මුන්ථ රටේම ඡන්ද
කොල්ලයක් පැවැතිණැයි කීම එතරම් යුක්ති සහගත වෙතැයි මම නොසිතමි.</p>

<p>කෙසේ වෙතත්
ප්රධාන දේශපාලන පක්ෂ ප්රජාතන්ත්රවාදී මැතිවරණ පීල්ලට තවම වැටී නොමැත යන්නට මම ද එකඟ
වෙමි. නව දේශපාලන සංස්කෘතියක් ඇති කිරීමේ පූර්විකාව ලෙසින් හැඳින්වෙන සාධාරණ ඡන්ද
තවමත් අපේ දේශපාලන සංස්කෘතියට ආගන්තුක බැව් පසුගියදා පැවැති පළාත් පාලන
මැතිවරණ කි්රයාවලිය තුළින්ම පෙන්නුම් කෙරේ.</p>

<p>m  Pkao odhlfhl=f.a lg lmd
ìh joaod Tyqf.a Pkao whs;sh Wÿrd .;a;d úh yelsh' Pkao odhlhl= Pkao fmd</p>

<p> fuu ;;a;ajh tlai;a cd;sl fmruqK kdhlhka fukau
tu mCIfha fiiq j.lsjhq;= fldgia o oks;s'</p>

<p> tfia kï miq.sh m</p>

<p>මෙය රටේ
පාලන බලය වෙනස් කෙරෙන තීරණාත්මක මැතිවරණයක්ද නොවේ. කිසියම් විදියකින් පොදු පෙරමුණට
පළාත් පාලන ආයතන කිහිපයක හෝ බලය හිමිවූවා නම් එය ආණ්ඩුවේ ධෛර්යය වැට්ටවීමට
සාධකයන් වනවා මිස එය මූලික බලයේ වෙනසක් කිරීමට අපොහොසත් වේ.</p>

<p>මෙමගින්
ප්රජාතන්තී්රය සමාජය වෙත සන්නිවේදනය වන අනතුරුදායක සංඥාව නම් මෙවැනි නො වැදගත්
අවස්ථාවකදී ඡන්ද දොෂණ හා බලහත්කාරී කි්රයාවල නිරත වන පක්ෂ කි්රයාකාරීන් කිසියම් තීරණාත්මක
අවස්ථාවක දී කෙසේ හැසිරෙත්්ද යන්නය. නො එසේ නම් පාලන බලය වෙනස් කළ හැකි මහ
මැතිවරණයක දී පාලක එක්සත් ජාතික පෙරමුණ (පක්ෂය) කෙසේ හැසිරෙත්ද යන්නය.
පාර්ලිමේන්තු නිල කාලය අවසන්ව මහමැතිවරණයකට ගිය විට පසුගිය ප. පා. ඡන්දයේ සිදුවූ දොෂණ
හා බලහත්කාරී කි්රයා උච්චස්ථානයකට පත් නොවෙතැයි කෙසේ නම් කිවහැකිද? </p>

<p> fuys§ md,l tcdmfha n,OdÍka jro ;ukaf.a
wfmaCIlhka yd iq¿ ks,OdÍka fj; mjrd w;fidaod .ekSug h;ak orkq o fmfkhs' fuh
iïmQ¾Kfhkau jerÈ iy.;h' wvqu ;rñka jro l</p>

<p> kuq;a ud okakd ;rñka m</p>

<p>මෙහිදී
එක්සත් ජාතික පක්ෂයේ ප්රධානීන් කරන තවත් වරදක් ද පෙන්නුම් කළ යුතුය. එනම් සිදුවූ
හෝ සිදුකළ වරද කලින් පැවති මැතිවරණවලට සාපේක්ෂ කර ඒවායේ බර හෑල්ලූ කිරීමය. එය
ද ප්රජාතන්්ත්රවාදී මැතිවරණ ක්රමය වෙත නිකුත් කෙරෙන රතු සංඥාවකි.</p>

<p> </p>

<p> </p>

<p> </p>






</body></text></cesDoc>