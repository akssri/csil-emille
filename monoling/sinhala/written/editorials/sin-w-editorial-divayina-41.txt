<cesDoc id="sin-w-editorial-divayina-41" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-divayina-41.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p><head> </head></p>

<p><head>jelsh</head></p>

<p><head>w,a,ia fldñiu - ilS%h úh hq;=hs</head></p>

<p><head>w,a,ia fldñiu jvd;a ld¾hCIu njg m;a lsÍfï;a" ks;sm;d
isÿjk nÿ f.ùï meyer yeÍï w,a,d .ekSfï;a wruqKska tys ls%hdldÍ;ajh ms</head></p>

<p><head>w,a,ia yd ¥IK w,a,d .ekSug wjYH l=i,;djka fukau ks,OdÍka
ixLHdjla fmd,sish i;=j ke;ehs o" nÿ jxpd w,a,d .ekSug Tjqkag Yla;shla
ke;ehs o fuys§ fmkakd fok ,o w;r" w,a,ia fldñiug tu Yla;sh yd ks,OdÍka
,nd h hq;=j we;ehs o i|yka lrk ,§' we;eï j;= iud.ï .sKqï fmd;a folÜgqjla
;ndf.k we;ehs o" tfia fkdlr ksje/È .sKqï bÈßm;a lrkafka kï" tu j;= iud.ï
,nk wdodhñka j;= lïlrejkag re' 400$- l jeämqr jegqm f.ùu wmyiqjla ke;ehs o
wdodhï nÿ fomd¾;fïka;=fõ m</head></p>

<p><head>miq.sh jljdkqfõ lsisu rchla hgf;a w,a,ia fldñiu úêu;aj
ls%hd;aul fkdjQ nj fï rfÜ ck;djf.a ms</head></p>

<p><head>flfia fj;;a" w,a,iska" ¥IKfhka msß wo iudcfha
w,a,ia fldñiula ls%hd;aulj ;sîu b;du;a jeo.;ah' tfy;a th foaYmd,k n,mEïj,ska
f;dr iajdëk fldñiula úh hq;=h' fl;rï n,iïmkak mqoa.,hl= jqjo" Bg yiqlr
.ekSfï n,;, fldñiu i;=úh hq;=h' fuys§ ue;s weue;slï ie</head></p>

<p><head>wi,ajeis bka hdfõ ;ñ,akdvq uy weue;skshj isá chrdï
ch,,s;d mjd w,a,ia yd ¥IK fpdaokd hgf;a rcfha uqo,a kdia;s lsÍfï fpdaokd hgf;a
wêlrKh yuqjg m;a lrkq ,enqjdh' ;j;a bka hdfõ weue;sjreka .Kkdjla tjeks
fpdaokd u; wêlrKh yuqjg m;a lrkq ,enQy' iuyre nkaOkd.dr.; lrkq ,enQy'</head></p>

<p><head>bkaÿkSishdfõ ckdêm;s wíÿ,a ryauka jdys kS;s úfrdaë f,i
uqo,a ,nd .ekSfï fpdaokd hgf;a oekg wêlrKh yuqjg m;a lrkq ,en isà' fï wkaoñka
ljr ;rd;srfï mqoa.,hl=g jqjo tfrysj fpdaokd t,a, lsÍug;a" ta ms</head></p>

<p><head>tu ksid rfÜ iïm;a iq/lSug;a" tajd úêu;aj wdfhdackh ùu
iy;sl lsÍug;a" w,a,ia fldñiul ls%hdldÍ;ajh w;HdjYHh' uyck uqo,a ta ta
mqoa.,hkaf.a ´kEtmdlï i|yd m%fhdackhg .ekSu je</head></p>

<p><head>fuu ld¾hfha§ foaYmd,k n,;,j,g hg fkdù iajdëkj lghq;= lrk
w,a,ia fldñiul ls%hdldÍ;ajh w;sYh jeo.;ah'</head></p>

<p><head> </head></p>

<p><head> </head></p>






</body></text></cesDoc>