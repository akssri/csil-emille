<cesDoc id="sin-w-editorial-lankadipa-103" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-lankadipa-103.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>බන්ධනාගාර
මැරයන්ගේ  රජදහනක් වූයේ කෙසේද?</p>

<p> </p>

<p>පුද්ගලයකු ඝාතනය  කිරීම හා තවත් අපරාධ රැසක් සම්බන්ධයෙන් කොළඹ
රිමාන්ඩ් බන්ධනාගාරගත කරනු ලැබ සිටි සැකකරුවන් දෙදෙනකුට තවත් සිරකරුවකු  විස ද්රවයක් අඩංගු යැයි  සැලකෙන සිරින්ජරයකින් ඇන ඇති බවත් එසේ ඇනුම්
කෑ සිරකරුවන් දෙදෙනා කොළඹ  ජාතික
රෝහලට ඇතුළත් කැරුණු බවත් බොරැල්ලේ පොලිසිය පැවැසූහයි වාර්තා වී තිබේ. ඇනුම්
කෑ සිරකරුවන් දෙදෙනාගේ සිරුරුවලට විස ඇතුන්ථ වී ඇති  බවක් අනාවරණය වී නැතැයිද 
jd¾;dfjka lshefjk kuq;a" flfkl=g wekSu i|yd lsisjl= isßkacrhla
mdúÉÑ  lrkafka ta u.ska úila YÍr.; lsÍu
i|yd hehs wkqudk l</p>

<p>බාහිර කිසිම කෙනෙකුට සිය
හිතුමතයේ ඇතුන්ථවිය හැකි නොවන තරමට දැඩි රැකවල් යෙදුණු රිමාන්ඩ් බන්ධනාගාරයෙහි සිටින
සැකකරුවන් අතට වෛද්ය උපකරණයක් වන සිරින්ජරයක් හා කෙනකුට ක්ෂණිකව මරු කැඳවන සයනයිඩ්
වැනි විස ද්රවයක් පත්වූයේ කෙසේද යන්න මෙහිදී මතු වන බරපතලම ප්රශ්නයයි. රිමාන්ඩ් බාරයේ
සිටින සැකකරුවන් බැලීමට එන පිරිස් බන්ධනාගාරයට ඇතුන්ථ කැරෙන්නේ ඔවුන් දොරටුවෙහිදී
දැඩි පරීක්ෂණයකට භාජන කිරීමෙන් පසුවය. ඔවුන් සැකකරුවන් සඳහා රැගෙන එන කෑම් බීම් ඇඳුම් යනාදී
සියලූ දේ ද දැඩි පරීක්ෂණයකට ලක් කැරෙයි. සැකකරුවන් බලන්නට එන සියලූදෙනා එක වරම රොත්ත
පිටින් ඇතුන්ථ කිරීමේ  සිරිතක්ද නැත. එක් එක්
සැකකරුවා බැලීමට එන්නන්ගේ වාරය අනුව විනා හිතුමතයට එහි ඇතුන්ථ වන්නටද ඔවුනට අවසර
නොලැබේ. </p>

<p>එසේ නම් එක් සැකකරුවකුට
හෝ කීප දෙනකුට හෝ යම් අනතුරක් සිදු කළ හැකි අවියක් හෝ වෙන යම් ද්රව්යයක්
බන්ධනාගාරය තුළට රැගෙන යන්නටත් එය එහි රඳවනු ලැබ සිටින තවත් සැකකරුවකු අතට පත්
කරන්නටත් කෙනකුට මද ඉඩකඩක් හෝ ලබාගත හැක්කේ කෙසේද? සියුම් වෛද්ය උපකරණයක් වන
සිරින්ජරයක් කුඩා  ද්රව්යයක් බව ඇත්තකි.
එහෙත් සැකකරුවන් බැලීමට පැමිණෙන්නන් නිසි පරීක්ෂණයකට ලක්  කැරෙන්නේ නම් එවැනි කුඩා දෙයක් වුවද එහි රැගෙන යෑමට කිසිවකුට ඉඩකඩ
ලබාගත හැකි නොවන බව කිව යුත්තක් නොවේ. </p>

<p>එහෙත් සැබැවින්ම  එහි පවත්නා තත්ත්වය එය නොවන බවත්  සිරින්ජරයක් වැනි කුඩා උපකරණයක් පමණක් නොව ඊට
වඩා බෙහෙවින් විශාල පිස්තෝල වැනි ගිනි අවි පවා බන්ධනාගාරය තුළට යැවිය හැකි මං පාදාගත
හැකි බවත් ඒ පිළිබඳ තතු දන්නෝ කියති. මත් කුඩු ජාවාරම පවා රිමාන්ඩ්
බන්ධනාගාරයේ පමණක් නොව මහ සිපිරි ගෙදරද ශීඝ්රාකාරයෙන් සිදුවන බවට රාවයක් පවතී.
බන්ධනාගාර තුළ සිරකරුවන් අතර හටගත් නොයෙක් ගැටුම්වලදී එහි කිසිසේත් තිබිය
නොහැකි අවි ආයුධ පාවිච්චි කැරුණු අවස්ථාද වාර්තා වී නැත්තේ නොවේ. </p>

<p>ඇතැම් බන්ධනාගාර නිලධාරීන්
අල්ලසට යටවීම ඉහත කී කනගාටුදායක තත්ත්වයට ප්රධාන වශයෙන්ම හේතුවී ඇතැයි අසන්නට ලැබේ.
යහමින් සල්ලි වියදම් කළහැකි කෙනකුට 
nkaOkd.drh ;=  ,la fkdjQfhao hkak;a m%Yakhls' </p>

<p>අධිකරණයෙන්
වරදකරුවන් වන පුද්ගලයන් යළි වැරදිවලට නොපෙළැඹෙන සේ පුනරුත්ථාපනය කිරීම
බන්ධනාගාරයෙන් අපේක්ෂිත අරමුණකි. අධිකරණයෙන් දඩුවම් නියමවන තෙක් හෝ චෝදනාවලින්
නිදහස් වනතෙක් හෝ ඇප නියම කැරෙන තෙක් හෝ සැකකරුවන් රඳවනු ලබන රිමාන්ඩ්
බන්ධනාගාරයේ වගකීම වනුයේ ඔවුන් රැකබලා ගැනීමය. ඒ දෙතැනින් කෙතෙැනකදී වුවද කිසියම්
සිරකරුවකුට හෝ පිරිසකට අනතුරක් එල්ල වේ නම් ඔවුන් එයින් මුදා ගැනීම සඳහා අවශ්ය
ආරක්ෂක විධිවිධාන සැලැසීම  බන්ධනාගාර
කොමසාරිස්වරයා ඇතුන්ථ සියලූ නිල බලධාරීන්ගේ යුතුකමකි වගකීමකි. එහෙත් ඇතැම් දොෂිත
නිලධාරීන් ඇල්ලසට යටවීම නිසා හෝ වෙනත් කවරම හේතුවක් නිසා හෝ සිරකරුවනට
සැකකරුවනට අනතුරක් එළැඹෙන්නේ නම් එය බන්ධනාගාර නිලධාරීන්ගේ පාලන දුර්වලතා ප්රකට
කරන සාධකයක් වන්නේය. ඉහත දැක්වුණු සිද්ධියම ඒ පිළිබඳ නිදසුනකි. මේ තත්ත්වය
වෙනස්කොට සිරකරුවන්ගේ ජීවිතාරක්ෂාව තහවුරු කරනු පිණිස කොමසාරිස්වරයා ඇතුන්ථ සියලූ
නිලධාරීන් වගබලා ගන්නවා ඇතැයි අපි විශ්වාස කරමු.</p>

<p> </p>






</body></text></cesDoc>