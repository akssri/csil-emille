<cesDoc id="sin-w-editorial-lankadipa-80" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-lankadipa-80.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>විදුලිය කපන
වේලාවේ  වෙනසක් කළ නොහැකිද?</p>

<p> </p>

<p>විදුලිය කපා හරින කාලය එන්ට එන්ටම වැඩි කරනු පෙනේ. ඉදිරි සතිය තුළ දී
විදුලිය බලාගාර ආශ්රිත ප්රදේශවලට ප්රමාණවත් තරමින් වැසි නොලැබුණෙහොත් විදුලිය කපා
හැරෙන කාලය දිනකට පැය 10 1/2ක් කරන්නට සිදු විය හැකියැයිි ඊයේ පළ වූ වාර්තාවෙකින්
දීක්විණි.</p>

<p>එසේ
කරන්නට සිදු වනුයේ වෙන කළ හැකි දෙයක් නැති නිසා ය. දීනට විදුලිය කපා හරිනුයේ රට
කලාපවලට බෙදා වෙන් කර ගනිමිනි. ඒ ඒ කලාප අනුව විදුලිය කපන වේලාව වෙනස් වෙයි. මෙසේ
කපන වේලාවේ කිසියම් වෙනසක් කළ නොහැකි දී යි විමසනු කැමැත්තෙමු. අප මෙසේ විමසනුයේ
විශේෂයෙන්ම සමහර පළාත්වලට බලපාන පරිදි පෙරවරුවේ 4.30 සිට 7.30 පමණ දක්වා කපන
වේලාව ගැනය.</p>

<p>මෙය
ඉතා විශාල සංඛ්යාවකට බලපාන වේලාවකි. විශේෂයෙන් පාසල් ශිෂ්ය ශිෂ්යාවන්ට බලපාන වේලාවකි.
එපමණක් නොව රැකියා සඳහා පිටත්ව යන උදවියට ද බලපාන වේලාවකි. පාසල් ශිෂ්ය ශිෂ්යාවන්
බහුතරය පාසල් යන්නට ලක ලැහැස්ති වීමට සාමාන්යයෙන් පටන් ගනුයේ මේ වේලාව තුළ දී ය.
හැම නිවසකම පාහේ වැඩිහිටියනුත් 
  iúia;rj fmkakd §u wjYH hehs fkdis;uq'</p>

<p>පාසල්
ශිෂ්ය ශිෂ්යාවන්ට පමණක් නොව නිවසේ සිට කිලෝ මීටර් විසිපහ, තිහ තිස්පහ පමණ දුර
ගෙවා රැකියා ස්ථාන කරා යන උදවියට ද දුෂ්කරතා මතුවෙයි. ඒ අයට ද ගමනට සුදානම් වීමට
සිදුවනුයේ පාන්දර 4.30ට පමණ සිට ය. ඒ වෙලාවට විදුලිය කපා හැරීම ගමනට ලක ලැහැස්ති
වීමටත් ගමනටත් බාධාවකි. මේ පිරිස දහස් ගණනක් නොව ලක්ෂ ගණනක් වන බව සිකුරු ය. </p>

<p>ඒ නිසා පෙරවරු 4.30
පමණ සිට පෙරවරු 7.30 පමණ දක්වා විදුලිය කපා හැරීමේ වේලාවේ කිසියම් වෙනසක් කළ
නොහැකි දීයි බලනු වටී. බැලිය යුත්තේ බහුතර ජනතාවට විඳින්නට සිදු වන දුෂ්කරතා අවම කැර
ගැනීමට ය. අප ඒ බව අවධාරණය කරනුයේ අති විශාල පිරිසකට විශේෂයෙන් ම පාසල් ශිෂ්ය
ශිෂ්යාවන්ට දුෂ්කරතා විඳින්නට සිදුවන බව වාර්තා වන නිසා ය.</p>

<p>මේ
පියවර විදුලිය කැපීමේ එක් කාල පරිච්ඡෙදයක් නිසා ලක්ෂ ගණනකට සිදුවන දුෂ්කරතාවක් අවම කැර
ගැන්ම සඳහා පමණි. එයින් විදුලි බල අර්බුදයට විසඳුමක් නොලැබෙන බව ඇත්තය. ඊට ස්ථිර විසඳුම්
සෙවීම වෙනම කාරියෙකි. එය අවශ්යයෙන්ම සිදුවිය යුත්තකි. </p>

<p>පැලැස්තරවලින්
වැඩක්  නැත අවශ්ය වන්නේ ස්ථිර විසැඳුමකි.
දේශපාලනඥයන් කුමක් කීවත් විදුලිය බල අර්බුදයට පෙනෙන තෙක් මානයෙක විසැඳුමක් නැතැ යි
ලංකා විදුලිබල මණ්ඩලයේ ඉංජිනේරු සංගමය පවසන බව ඊයේ වාර්තා වූයේ ය. පෙරේදා කොළඹ
දී පැවැති ප්රවෘත්ති සාකච්ඡාවෙක දී ඔවුන් කියා තිබුණේ රටේ වැඩි වන විදුලි බල ඉල්ලූම
සපුරාලීමට විධිමත් වැඩ පිළිවෙළක් නොයෙදුවොත් වසර 2004 දී දීනටත් වඩා දරුණු විදුලි
බල අර්බුදයක් මතුවිය හැකි බව යි. ස්ථිර විසැඳුමක් සොයන්නට 2004 එළැඹන තුරු බලා
සිටිමු ද? කටින් බතල එහිදීත්් කොළ සිටවමුද?</p>

<p>වගකිවයුතු
අංශ විවිධ යෝජනා ඉදිරිපත් නොකළයි කීමට තරම් අපි සැහැසි නොවෙමු. යම් යම් ව්යාපෘති ද
යෝජනා කැරිණි. සැලැසුම් ද සැකසිණි. පෙරේදා පැවැති ප්රවෘත්ති සාකච්ඡාවේ දී විදුලිබල
මණ්ඩලයේ හිටපු ඉංජිනේරුවරයකු වන ආචාර්ය තිලක් සියඹලාපිටිය මහතා කියා තිබුණේ
අර්බුදයට මූලික හේතුව කලට වේලාවට වැසි නොලැබීම නොව විදුලිබල මණ්ඩලයට යෝජනා කරන
ව්යාපෘති කලට වේලාවට ඉදි නොකිරීම බවයි. මෙයින් පෙනෙන්නේ සැලැසුම් නිසි පරිදි කි්රයාත්මක
කොට නැති බවකි. ඊට වගකිවයුත්තෝ කවුරුද?</p>

<p>ගල් අඟුරු බලාගාර ගැනද යෝජනා ආවේය. එහෙත් සමහරුන් ගේ
විරෝධතා මත ඒවා යටගියේය. එවන් විරෝධතා මතු වන්නේ නම් ඔවුන් සමග සාකච්ඡා කොට
විකල්ප ස්ථාන සොයා ගත නොහැකි ද? සාකච්ඡාවලට හවුල් වී විකල්ප ස්ථාන හෝ ප්රදේශ සොයා
ගන්නට උදවු වීම විරෝධතා දක්වන්නවූයේගේ යුතුකමයි. විරෝධතා දීක්වීම  ප්රමාණවත් නොවේ. මෙය ජාතික ප්රශ්නයකි. ඒ වග
අමතක කළ නොහැකිය.</p>

<p> </p>






</body></text></cesDoc>