<cesDoc id="sin-w-editorial-dinamina-7" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-dinamina-7.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>දිනමිණ
2000 අගෝස්තු 28</p>

<p> </p>

<p>කතුවැකිය</p>

<p> </p>

<p>දේශපාලන
දෙපිළ බෙදීම</p>

<p> </p>

<p>මැතිවරණය ළඟාවෙත්ම
දේශපාලන බලවේගවල පෙළගැස්ම තීව්රවෙයි. එය බොහෝ දුරට දෙපිළ බෙදීමෙක ස්වරූපයද
ගනී. මේ දිනවල අප රටෙහි සිදුවෙමින් තිබෙන්නෙද එයයි.</p>

<p>එහෙත් මෙම
පෙළගැස්මෙහි යම් නොපැහැදිළි බවක්ද බැලූ බැල්මට ම තිබේ. ඊට හේතුව මැතිවරණයට
ඉදිරිපත්වීමේ අපේක්ෂාවෙන් මැතිවරණ කොමසාරිස්වරයාගේ දෙපාර්තමේන්තුවේ ලියා පදිංචි
දේශපාලන සංවිධාන සංඛ්යාවේ විශාලත්වයයි. වාර්තාවන අන්දමට මේ වන විට ලියාපදිංචි  දේශපාලන පක්ෂ සංඛ්යාව හතලිහට අධිකය ය. එබැවින්
දේශපාලන බලවේගවල පෙළගැස්මෙහි ව්යාකූල බවක් දකින්නට තිබීම අරුමයක් නොවේ.</p>

<p>ඒ,
මතුපිටට පෙනෙන සැටි ය. නියම තත්ත්වය එය නොවේ. දේශපාලන පෙළගැස්මෙහි නියම
තත්ත්වය නිවැරැදිව වටහා ගැනීමට හැකි වනුයේ ඒ පිළිබඳව මඳ විමැසීමෙකිනි. </p>

<p>මෙම
මහමැතිවරණයටද ඉදිරිපත් වනුයේ ප්රධාන බලවේග දෙකෙකි. පොදු පෙරමුණ සහ එජාපය ඒ බලවේග
දෙකයි. ඉදිරි සයවසරෙක කාලය තුළ රට පාලනය කළ 
hq;af;a tu folska ljr n,fõ.ho fyj;a ljr mlaIho hkq ck;dj ;SKh lrkq we;'
ta ;SrKh l=ula jqj;a bÈß ih jir ;=</p>

<p>එම
ජනාධිපතිවරණයේදී ඇය ජනතාවගෙන් ඉල්ලා සිටි සිටි වරම වූයේ රටට තිර සාමය උදාකිරීම
පිණිස, බලය බෙදාගැනීම පදනම් කැරැගත් සාමකාමී දේශපාලන විසඳුමක් මඟින් ජනවාර්ගික
ප්රශ්නය නිරාකරණය කිරීමේ වරමයි. ඇය ඉල්ලා සිටි ඒ වරම, ජනතාව ඉතාම පැහැදිලි
තීන්දුවෙකින් ඇයට දුන්හ මෙම මහ මැතිවරණයෙන් ඇය ඉල්ලා සිටින්නේ එම ජන වරම ක්රියාවට නැඟීමට
පාර්ලිමේන්තුවේ බලයයි.</p>

<p>මෙම
මැතිවරණයේදී දේශපාලන බලවේගවල පෙළගැසීම දෙස බැලිය යුත්තේ එම ප්රශ්නය තුළිනි.
තිර සාමය පිළිබඳ ප්රශ්නය නොවිසඳා රටේ අනෙක් කිසිම ප්රශ්නයක් සාර්ථකව, විසැඳීමට
නොහැකි බැවිනි. ආර්ථික සමාජමය හා සදාචාරමය ඈ සියලු ක්ෂේත්රයන්හි පවතින පිරීහීමට
ප්රධානම හේතුව සැපයුණේ ජනවාර්ගික ප්රශ්නයෙනි. එය විසැඳීමට මේ දක්වාම අපොහොසත් වමේ
වන්දිය ගෙවීමට සිදුවී තිබෙන්නේ ලෙයින් හා කඳුලිනි. එබැවින් ඒ විසඳුම තව දුරටත් පමා
නොකළ හැකිය.</p>

<p>දේශපාලන
අවස්ථාවාදය පසුගිය වතාවලදී මෙන් මෙවරත් උත්සාහ කරන්නේ විසැඳුම් ප්රයත්නය ව්යවර්ථ කිරීමට
ය. එය මුන්ථ රටට ම පැහැදිළි වී මහාරය. එබැවින් දේශපාලන බලවේග මේ මැතිවරණය සඳහා
පෙළගැසෙන්නේ කවර ආකාරයටද යනු තේරුම් ගැනීමද පහසුය.</p>

<p>පොදු පෙරමුණ තම
විසඳුම ආණ්ඩුක්රම ව්යස්ථා කෙටුම්පතක් ලෙසින් ඉදිරිපත් කොට තිබේ. එජාප නායකත්වයද වසර
ගණනාවක් තිස්සේ කල් මර මරා සිට අන්තිම මොහොතේ සහභාගිවී එම කෙටුම්පත සකස් කීරීමට
දායක වූයේය. එහෙත් එය සම්මත කොට ක්රියාවට නැඟීමට සහාය නොදුන්නේ ය. සහයෝගය දීමට
කැමැත්තෙන් සිටි එජාප මන්තී්රන් පිරිසක් හදිසියේම රට පැටවූයේය. </p>

<p>එජාප නායකත්වය
එම අවස්ථාවාදය නිසා කලකිරුණු දේශප්රේමී කොටස් එජාපයෙන් ඉවත්වී පොදු පෙරමුණට එක්වී සිටිති.
එයද වත්මන් දේශපාලන පෙළගැස්මේ කැපී පෙනෙන සාධකයෙකි. සෙසු දේශපාලන බලවේගද ඒ
අනුව මැතිවරණයේ ප්රධානම ප්රශ්නය වන නව ව්යවස්ථාව මුල්කැරැගෙන දෙපිළබෙදෙමින් සිටින
සැටි දීක ගැනීමට හැකිය. එක් පිළක් මෙහෙය වන්නේ පොදු පෙරමුණ මඟිනි. රට නොබෙදා
බලය බෙදා ගැනීම ජනවාර්ගික ප්රශ්නයට එකම විසඳුම බව එම පිළේ ස්ථාවරයයි. අනෙක් පිළ
මෙහෙයවන්නේ එජාපය විසිනි. එහෙත් එජාපයට නොසැලෙන ස්ථාවරයක් නැත. බලය බෙදා ගැනීමේ
පදනම පිළිගන්නා බව ප්රකාශ කරමින් ම, බලය බෙදාගැනීමක් කිසිසේත්ම අවශ්ය නැති
බවත්, එකම විසැඳුම යුද්ධය බවත් කියා සිටින ජාතිවාදී බලවේග සමඟ එජාපය දීන් සන්ධාන
ගතවී සිටී. ජනතා විමුක්ති පෙරමුණ තමන් තුන්වැනි බලවේගය බව කියමින් කරන්නේ ද බලය
බෙදාගැනීමේ පොදු පෙරමුණු ස්ථාවරයට එරෙහිව යමින් එජාපයේ අවස්ථාවාදයට මුක්කු ගැසීමය.
මෙවර මහ මැතිවරණයේදී දේශපාලන බලවේග වල දෙපිළ බෙදීම සිදුවෙමින් තිබෙන්නේ එලෙසිනි.
</p>

<p>මහජන එක්සත්
පෙරමුණ පොදු පෙරමුණ සමඟ එක්වීම මෙම දෙපිළ බෙදීමේ ප්රවනතාවේ සුවිශේෂ සංධිස්ථානයකි.
දිනේෂ් ගුණවර්ධන මහතාගේ නායකත්වයෙන් යුත් ම.එ. පෙරමුණ ඊනියා ජාති ආලයේ පිරුවටයේ
ඔතා එජාපයට පාවාදීමට එජාපයේ ගිහි පැවිදි ඒජන්තලා දීරූ කුමන්ත්රණකාරි උත්සාහය ව්යවර්ථ
වූයේ ය. ඔවුන් කපන්නේ බොරු වලක් බව ම.එ.පෙ. නායකයෝ මැනැවින් තේරුම් ගත්හ. අවංක
ජාතිමාමක බවත්, බොරු ජාති ආලයත් අතර වෙනස මඑපෙ නායකත්වය රටට ම පැහැදිලි කොට
තිබේ.</p>

<p>යෝජිත නව
ව්යවස්ථාවේ අඩුපාඩු තිබෙනවා නම් ඒවා පෙන්වාදී සංශෝධනය කැරැ ගැනීමට අවස්ථාව සළසා
දී තිබියදී එම අවස්ථාව පැහැර හරිමින් මුන්ථ ව්යවස්ථාවම ප්රතික්ෂේප කිරීම ජාති ආලය නොව
ජාති ආලයේ වෙස්මුහුණින් වසා ගත් එජාප ආලය බව රට, දීය සමයට අවංකව ම ආදරය
කරන සියලු දෙනාම දීන් දනිති.</p>

<p> </p>

<p> </p>






</body></text></cesDoc>