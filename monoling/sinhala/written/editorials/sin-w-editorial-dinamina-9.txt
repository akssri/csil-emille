<cesDoc id="sin-w-editorial-dinamina-9" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-dinamina-9.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>කතුවැකිය</p>

<p> </p>

<p>රනිල් ප්රභාකරන් එකම කාසියේ දෙ පැත්ත</p>

<p> </p>

<p>      lsishï
foaYmd,k mCIhl tys mqfrda.dó kdhlhka mj;sk kdhl;ajhg tfrysj tu mCIfhka bj;aj
hkafka kï" th mdCIslhkg muKla fkdj rfÜ ck;djgo ;on, f,i wjOdkh fhduq
lrkakg isÿ jkq we;' tcdm fha fÊIaG kdhlhka jQ isßfiak l=f¾ uy;df.a isg úchmd,
fukaäia" yf¾kaø fldrhd" iqis,a uqKisxy" frdkS o ue,a"  r;akisß rdcmCI" ir;a wuqKq.u" ir;a
fldaka.yf.a" wd§ tcdm fha fÊIaG;u iy ls%hdldß msßila rks,a úl%uisxy
uy;df.a kdhl;ajhg tfrysj mCIh yer f.dia isá;s' tfia mCIh yer f.dia isák
m%dfoaYSh kdhlhkaf.a ixLHdjo úYd,h' tfiau Èkm;du mCIh yer hk msßio jeä fj;s' isßfiak
l=f¾ uy;d yer wfkla ish,af,dau pkaøsld nKavdrkdhl l=udr;=x.  ckdêm;skshf.a kdhl;ajh ms</p>

<p> </p>

<p>      tcdm uyd
mßudK fï kdh hEu .ek w|f.d¿ ìysß ms</p>

<p>      fmdtfm rch
bÈßm;a l 
m%Ndlrkag fmdfrdkaÿ ù we;s tcdm kdhlhd ;ud n,hg m;ajqjfyd;a ckdêm;skshg  hqoaOh lerf.k hdug bv fkdfok nj;a lshd we;' </p>

<p> </p>

<p>      B,dï iyskh
ienE ler .ekSu i|yd f,a .x.djl mSkk m%Ndlrka keue;s ;%ia;jdÈhdg mCImd;s;ajh
olajk tcdm kdhlhd j¾Ordcd fmreud,a talSh ,xldjla iy cd;sl iuÕh Wfoid o fmkS
isák úg th fy  jkjdg m%Ndlrka
úreoaOh' Bg tcdm kdhlhd úreoaO jkafka wehs' m%Ndlrka i;=gq lsÍu i|yd h' tcdm
kdhlhdg w;S;h wu;l ùu lk.dgqodhlh' m%Ndlrkag tfrysj igka lsÍu i|yd yudodjla
f.dv ke.Sug j¾Ordcdg wdOdr Wmldr lf  tijQfha B,dï fldäh fkdj cd;sl
fldähhs'  Tyq bkaÈhdjg mek .sfha
m%Ndlrkaf.ka Ôú;h fírd .ekSu i|ydhs' fï ish,a, okakd rks,a úl%uisxy  tcdm kdhlhd ta j¾Ordcdg wo myr .ikafka ;u
ñ;% m%Ndlrka i;=gq fldg Tyqf.a iyfhda.h ,nd .ekSu i|yd h'</p>

<p> </p>

<p>      tcdm
kdhlhdf.a foaYfødayS ls%hd l,dmh u.ska m%Ndlrkaf.a mQ¾K iyfhda.h tcdm
kdhlhdg;a" tcdm wfmaCIlhkag;a ,eî we;' tcdm Èkj, f,i fõ¿ms,af, m%Ndlrka
W;=re kef.kysr fou  isÿj we;af;a m%Ndlrkaf.a urK ;¾ck bossßfhah' m%Ndlrkaf.a urdf.k
uefrk fldáhka ol=Kg tjd we;af;ao fmdtfm weue;sjreka >d;kh ler oukakgh' tcdm
kdhlhdf.a;a" tcdm wfmaCIlhkaf.a;a uhs,a .ylgj;a fldá ;%ia;jd§kaf.ka
wk;=rla ke;'  fï lreKq ish,a, i,ld n,k
l  w.ue;sjrhd lshd we;s mßÈ u tcdm
wfmaCIlhka ish,a,kao fldá T;a;=lrejka yd .e;a;ka nj i;Hfhls'   </p>






</body></text></cesDoc>