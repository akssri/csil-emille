<cesDoc id="sin-w-editorial-lankadipa-99" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-editorial-lankadipa-99.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ර්මිනුවන්ගමුව
විදුහල ගැන</p>

<p> ksis mshjrla
fkd.kafka wehs @</p>

<p> </p>

<p>උඩදුම්බර,
මිනුවන්ගමුව ප්රාථමික විද්යාලයෙහි උගෙනීමට සිසු සිසුවියන් නැති නිසා එහි විදුහල්පතිවරයාටත්
ගුරු මහත්මීන් තිදෙනකුටත් උදේ 8 සිට සවස 2 වන තෙක් පාසලට වී නිකරුණේ කාලය ගත
කරන්නට සිදුවී ඇතැයි  කියන පුවතක් වාර්තා වී
තිබේ. අධ්යාපනයේත් එය ලබන සිසු දරු දීරියන්ගේත් උන්නතිය අරමුණු කොට ගත්
ප්රතිසංස්කරණ, ප්රතිශෝධන හා පරිවර්තන ගණනාවක් අධ්යාපන ක්ෂේත්රයෙහි මෑත කාලය
තුළදීම ක්රියාත්මක වුණු රටක ඈත පිටිසර ප්රාථමික විද්යාලයක් සම්බන්ධයෙන් අසන්නට ලැබෙන
ඒ කතාව සැබැවින්ම  කනගාටුදායකය. අධ්යාපන
බලධාරීහු නව අධ්යාපන ප්රතිසංස්කරණ හා එයින් සිසු දරු දීරියනට අත් විය හැකි යැයි බලධාරීන්
කල්පනා  කරන දේද  ගැන කොතෙකුත් කතා කරති. නොයෙක් මත
ප්රකාශ  කරති. ඒ අතරම  උඩදුම්බර ප්රදේශයේ  ප්රාථමික විදුහලක අධිපතිවරයා  සහ
ගුරුවරියෝ තුන් දෙනෙක් සේවයක්් නොකර වැටුප් ලබති. එහෙත් එය කිසිසේත් ඔවුන්ගේ
වරදක් නොවන බව සඳහන් කළ යුතුය. එසේ වුවද මේ සිද්ධිය අධ්යාපන අමාත්යාංශයේ විශේෂ
සැලැකිල්ලට භාජන විය යුත්තකි.</p>

<p>ඒ
විදුහල්පතිවරයාටත් ගුරු මහත්මීනටත් පාසලට වී නිකරුණේ කාලය ගත කරන්නට සිදුවී ඇත්තේ එහි
උගෙනීමට ශිෂ්ය ශිෂ්යාවන් නැති හෙයිනි. ශිෂ්ය ශිෂ්යාවන් නැති  වී ඇත්තේ එම විදුහලෙහි සිටි ළමයින් විසි දෙනා ඔවුන්ගේ මවු පියන්
විසින් වෙනත්  පාසල්වලට ඇතුළත් කැරුණු
හෙයිනැයි වාර්තා වී තිබේ. මේ විදුහලට අත් වී ඇති ඉරණම සම්බන්ධයෙන් මෙහිදී ප්රශ්න කීපයත්
මතුවෙයි. මෙහි  මුලදී සිටි සිසු පිරිස කොපමණ
වීද? ආරම්භයේ සිටම එය විස්සකට සීමා වූයේද? මුලදී සිටි විශාල සිසු පිරිසක් කෙමෙන් විසි
දෙනා දක්වා අඩු වූයේ නම් ඒ පිරිහීමට හේතුවූ කරුණු කවරේද? ප්රාථමික විදුහලක එවැනි
පරිහානිදායක තත්ත්වයක් ඇති වුයේ නම් විදුහල්පතිවරයා ඒ බව අදාළ අධ්යාපන  බලධාරීනට දීනුම් දුන්නේද? එසේ  නම් ඒ බලධාරීහු විදුහල සම්බන්ධයෙන් නිසි පියවරක්
නොගත්තෝද? අද මේ විදුහලට අත්වී තිබෙන ඉරණම අධ්යාපන බලධාරීන් ඒ සම්බන්ධයෙන් නිසි
පියවර නොගැනීමේ විපාකයක්ද? කොතෙක් ඈත පිිටිසර පෙදෙසක වුවද ප්රාථමික විදුහලක වුවද
පරිහානිය පිළිබඳව අධ්යාපන අමාත්යාංශය මූලික වශයෙන් වගකිව යුතු හෙයින් ඉහත දීක්වුණු
ප්රශ්න සඳහා උත්තර ලබා ගැනීම දීන් වුවද ප්රයෝජනවත් වනවා ඇත.</p>

<p>මේ
සම්බන්ධයෙන් මතු වන අනෙක් වැදගත් ප්රශ්නය නම්, මේ ප්රාථමික විදුහලේ හතර දෙනකුගෙන්
යුත් ගුරු  මණ්ඩලය උදේ අටේ සිට සවස  දෙක 
jk f;la úÿyf,ys kslrefKa ld,h .; lroa§ th wOHdmk wud;HdxYfha  fyda fomd¾;fïka;=fõ fyda wjOdkhg ,la  fkdjQfhao hkakhs'  wvqu jYfhka ta .=re uKaav,hg jegqma f.jk wod  jQfha kï fï
m%d:ñl úÿyf,a ;;a;ajh ±kg jvd fjkia jkakg bv ;sìKehs is;sh yelsh'</p>

<p>නියමිත
සේවය ලබා නොගෙන ගුරුවරුන් හතර දෙනකුට වැටුප් ගෙවීමේ වරදද මෙහිදී සිදුවී ඇති බව
පෙනේ. එසේ වී නම් එය අදාළ ගුරුවරුන්ගේ වරදක් නොව ඔවුන්ගෙන් අදාළ සේවය ලබා
ගැනීමට කටයුතු නොයෙදො අධ්යාපන බලධාරින්ගේ දුර්වලතාවකි. මවුපියන් ඔවුන්ගේ දරුවන් මේ
ප්රාථමික විදුහලෙන් අස් කර ගෙන වෙනත් විදුහල්වලට 
we;=  fï
.=rejrekg kslrefKa ld,h f.jkakg  isÿ jQfha
kï Tjqka mqrmamdvq we;s fjk;a úÿy,aj,g hjd Tjqkaf.a fiajh ,nd .kakg
m%dfoaYSh  wOHdmk fomd¾;fïka;=jg mqýjka
lula fkd;ssìKo@</p>

<p>මිනුවන්ගමුව ප්රාථමික විදුහලින් සිය දරුවන් ඉවත් කර ගන්නට
මවුපියන් පෙළැඹී ඇත්තේ විදුහල වසා දීමීමේ කුමන්ත්රණයක් ක්රියාත්මක වෙමින් පවතින නිසායැයි
ප්රදේශවාසීන් කියන බවද වාර්තා වී තිබේ. එය කාගේ කුමන්ත්රණයක්ද යන්න අනාවරණය වී නැතත්
ප්රාථමික විදුහලත් අධ්යාපන අමාත්යාංශය  සතු
සම්පතකි. එය ප්රදේශයේ දරුවන්ගේ යහපත සඳහා ආරක්ෂා කර ගැනීමේ වගකීමක් හා යුතුකමක්
අමාත්යාංශයට  තිබේ. විදුහල වැසී ගිය හොත් එය
ප්රදේශයේ දරුවන්ගේ අධ්යාපනයට පහරක් විය හැකි හෙයින් දීන්වත් මේ ගැන අධ්යාපන
අමාත්යාංශය සැලකිලිමත් වනු ඇතැයි අපි 
úYajdi lruq' mdi,la wefrk úg ysr f.hla jeiS h;shs mrK l;djla  ;sfí' mdi,la jefik úg ysr f.hla wefr;shs
wfkla w;g  fkdlsj yels kuq;a  
ksid ÿmam;a orejkg úYd, mdvqjla isÿ jkakg bv ;sfí'</p>

<p> </p>

<p> </p>






</body></text></cesDoc>