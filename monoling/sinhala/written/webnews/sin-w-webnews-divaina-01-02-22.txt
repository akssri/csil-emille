<cesDoc id="sin-w-webnews-divaina-01-02-22" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-webnews-divaina-01-02-22.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus, dated01-02-22</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>01-02-22</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>මැති ඇමැතිවරැන්ගේ සහ කාර්ය මණ්ඩලවල වියදම් අකුරටම කපන්න
ජනාධිපතිනිය නියෝග කරයි </p>

<p>කැබිනටි
ඇමැතිවරැ නියෝජ්ය ඇමැතිවරැ සහ ඇමැති කාර්ය මණ්ඩලවල වියදම් කපා හැරීමට සකස්
කර ඇති වැඩපිළිවෙල අකුරටම කි්රයාත්මක කරන ලෙස ජනාධිපතිනි චන්ද්රිකා බණ්ඩාරනායක
කුමාරතුංග මහත්මිය අමාත්යාංශ ලේකම්වරැන්ට නියෝග කර තිබේ.</p>

<p>වාර්ෂික
ඇස්තමේන්තුවන්ට අනූව එක් එක් වියදම් සඳහා ඇස්තමේන්තු කර ඇති මූදල හැරෙන්නට
වෙනත් කිසිදු හෝ මූදලක් ඇමැතිවරැ නඩත්තුව සඳහා යොදා නොගත යූතු යැයිද
ජනාධිපතිනිය තරයේ උපදෙස් දී තිබේ.</p>

<p>ඇමැති
වියදම් කපා හැරීමේ වැඩපිළිිවෙලට අනූව කැබිනටි ඇමැතිවරයකු පරිහරණය කරන වාහන
සංඛ්යාව 05 දක්වා අඩූ කිරීමට නිියෝග කර ඇත. කාර්ය මණ්ඩලයට ලබාදෙන වාහන
සංඛ්යාව 03 කට සීමා කෙරෙන අතර මේ අනූව ඇමැතිවරයාට හා කාර්ය මණ්ඩලයට හිමිවන
වාහන සංඛ්යාව 08 කි.</p>

<p>නියෝජ්ය
ඇමැතිවරයකුට හිමිවන වාහන සංඛ්යාවද තුනකට සීමා කෙරෙන අතර අදාළ නියෝජ්ය
ඇමැතිවරයාගේ කාර්ය මණ්ඩලයට හිමි වන වාහන සංඛ්යාව 02 කි.</p>

<p>දැනට
ඇමැතිවරැ නියෝජ්ය ඇමැතිවරැ හා ඕවූනගේ කාර්ය මණ්ඩල පරිහරණය කරන අමතර වාහන
සියල්ලක්ම මූදල් අමාත්යාංශය වෙත ආපසූ භාරදිය යූතු අතර එසේ භාරදෙනූ ලබන වාහන
දැනට වාහන නොමැති අනෙකුත් අමාත්යාංශ වෙත ලබාදීමට පියවර ගැනේ.</p>

<p>මෙම
පියවර ගැනීමට පෙර එක් ඇමැතිවරයෙක් හා ඕහූට අයත් කාර්ය මණ්ඩලය වාහන 10 ත් 15 ත්
අතර සංඛ්යාවක් සිය පරිහරණයට යොදා ගනූ ලැබූහ.</p>

<p>ඇමැතිවරයකුගේ
පූද්ගලික කාර්ය මණ්ඩලයද 08 දක්වා අඩූ කිරමට උපදෙස් දී ඇති අතර ඒ අනූව පූද්ගලික
ලේකම්වරයා සහ සම්බන්ධීකරණ ලේකම්වරැ සතරදෙනාට අමතරව තවත් තිදෙනකු පමණක්
ඇමැතිවරයකුට සිය කාර්ය මණ්ඩලයට ඇතුළත් කර ගත හැක.</p>

<p>නියෝජ්ය
ඇමැතිවරයකුට හිමිවනූයේ පස්දෙනකුගෙන් යූත් කාර්ය මණ්ඩලයක් පමණි.</p>

<p>මෙයට
පෙරදී කැබිනටි ඇමැතිවරයකුට විසිදෙනකුට වැඩි පිරිසකගෙන් සමන්විත කාර්ය මණ්ඩලයක්
පවත්වාගෙන යෑමේ අවසරය ලැබී තිබිණි.</p>

<p>කැබිනටි
ඇමැතිවරැන්ගේ ඉන්ධන දීමනාවන්ද සීමා කර ඇති අතර බස්නාහිර පළාත නියෝජනය කරන
ඇමැතිවරයකුට පැට්රල් දීමනාව ලෙස මසකට හිමිවන්නේ රැපියල් 50,000 කි. ඩීසල් දීමනාව
ලෙස ඕහූට හෝ ඇයට රැපියල් 10,000 ක් හිමි වේ.</p>

<p>බස්නාහිර
පළාතෙන් පිටත ජීවත්වන ඇමැතිවරයකුට ඩීසල් දීමනාව ලෙස රැපියල් 20,000 ක් හිමිවන අතර
පැට්රල් දීමනාව ලෙස රැපියල් 75,000 ක් හිමිවේ.</p>

<p>මෙයට
පෙරදී ඇමැතිවරැන්ට සීමා රහිත ලෙස ඉන්ධන දීමනාවක් ලබාගැනීමේ අවසරය ලැබී තිබිණි.</p>

<p>මෙයට
අමතරව කැබිනටි ඇමැතිවරයකුගේ නිල දුරකථන සංඛ්යාවද තුනක් දක්වා සීමා කෙරේ.
අදාළ ඇමැතිවරයාගේ කාර්ය මණ්ඩලයට හිමිවන දුරකථන සංඛ්යාවද 02 දක්වා අඩූ කෙරේ.</p>

<p>කැබිනටි
ඇමැතිවරයකුගේ නිල දුරකථන ගාස්තුව රැපියල් 10,000 කට සීමා කෙරෙන අතර නිවසේ
දුරකථන සඳහා ගෙවනූ ලබන මූදල රැපියල් 5000 කට සීමා කෙරේ. මේ අතර සෙලියූලර් දුරකථන
ගාස්තුවද රැපියල් 2000 දක්වා අඩූ කෙරේ.</p>

<p>නියෝජ්ය
ඇමැතිවරයකුගේ නිල දුරකථන ගාස්තුව රැපියල් 5000 කට සීමා කෙරෙන අතර ඕහූගේ හෝ
ඇයගේ නිවසේ දුරකථන සඳහා ගෙවනූ ලබන්නේ රැපියල් 2000 ක මූදලක් පමණි. මේ අතර
පූද්ගලික කාර්ය මණ්ඩලයන්ට හිමි දුරකථනවල ගාස්තු පියවනූ ලබන්නේ මාසික ගාස්තුව
රැපියල් 750 නොඉක්මවන පරිදිය.</p>

<p>මෙයට
පෙරදී මැති ඇමැතිවරැන්ගේ දුරකථන ගාස්තු සඳහා සීමාවන් පනවා නොතිබිණි.</p>

<p>මෙම
සීමාවන් දැඩි ලෙස කි්රයාත්මක කරන ලෙසත් වෙනත් අරමූදල් යොදාගනිමින් වියදම් පියවීමට
පියවර නොගන්නා ලෙසත් ජනාධිපතිනිය අමාත්යාංශ ලේකම්වරැන්ට උපදෙස්දී ඇත. මෙම
නියෝග කි්රයාත්මක නොකරන්නේ නම් ඒ පිළිබඳව තමන්ට සොයා බැලීමට සිදුවන බවද
ජනාධිපතිනිය දන්වා තිබේ.</p>

<p>බදුල්ලේ පූද්ගලික බැංකුවක දෙකෝටියක මූදල් වංචාවක් </p>

<p>බදුල්ල
නගර මධ්යයේ පිහිටි ප්රධාන පෙළේ පූද්ගලික බැංකුවක රැපියල් දෙකෝටි තිස් ලකෂයක
මූදල් වංචාවක් සිදුවී ඇති බව විගණන පරීකෂණයක දී හෙළිවී ඇත.</p>

<p>මෙම
වංචාව හෙළිවීමත් සමග එම බැංකු ශාඛාවේ කළමනාකරැ ආගියඅතක් නැතැයි පැවසේ.</p>

<p>මෙම
මූදල් වංචාව පිළිබඳව එම බැංකුවේ අභ්යන්තර විගණන අංශය මගින් පරීකෂණ පවත්වාගෙන යන
අතර එම බැංකුවේ නූවරඑළිය ශාඛාවේ දෙවන නිලධාරි දැනට බදුල්ල ශඛාවේ කළමනාකරැ ලෙස
වැඩ බලයි.</p>

<p>මෙම
බැංකුවේ ඇති අවිනිශ්චිත ගිණූමෙන් බැංකු ශාඛා අතර මූදල් ගණූදෙනූ කරන මූවාවෙන් මෙම
වංචාව සිදුකර ඇතැයි එහි ප්රකාශකයෙක් දිවයින"ට පැවසීය.</p>

<p>මෙම
බැංකු ශාඛාවට වෙනත් බැංකු ශාඛාවලින් ෆැක්ස් මගින් හා දුරකථන පණිවූඩ වලින් මාරැකරනූ
ලබන මූදල් අදාල වවූචරයේ මූල්පිටපත ලැබෙන තෙක් ගනූදෙනූකරැගේ ගිණූමට තාවකාලිකව
මූදල් බැර කිරම සඳහා මෙම අවිනිශ්චිත ගිණූම පවත්වාගෙන යනූ ලබයි.</p>

<p>ෆැක්ස්
යන්ත්රය කි්රයාවිරහිතවූ අවස්ථාවක දුරකථනයක් මගින් මූදල් මාරැ කිරමේදී ආරම්භක
ශාඛාවේ කළමනාකරැ විසින් රහස්ය සංකේත ක්රමයක් මගින් අදාල පනිවූඩය දැනූම් දුන්
පසූ එම බැංකු ශාඛාවේ කළමනාකරැ යොදනූ ලබන සටහනක් මගින් මෙම අවිනිශ්චිත ගිණූමේ
මූදල් අදාල ගණූදෙනූකරැගේ ගිණූමට බැර කරනූ ඇත.</p>

<p>මෙසේ
දුරකථන පණිවූඩ මගින් ගනූදෙනූ කළ ආකාරයෙන්ම මෙම වංචාව සිදුකර ඇති බවත් එම
ප්රකාශකයා කියා සිටියේය.</p>

<p>පසූගිය
බ්රහස්පතින්දා බදුල්ල ශාඛාවට තවත් නිලධාරියෙක් සමග පැමිණි ප්රාදේශීය කළමනාකරැ බැංකු
ශාඛාවේ කටයූතු පරකෂාකර ආපසූ යන අතරතුරදී නිලධාරිය සොයාගත් තොරතුරැ පිළිබඳව
විමසා ඇත.</p>

<p>එම
අවස්ථාවේ අදාල නිලධාරියා විසින් මෙම බැංකුවේ අවිනිශ්චිත ගිණූමේ මූදල් විශාල
ප්රමාණයක් සටහන් මගින් ගෙවා ඇති බවත් එම ගෙවීම් දීර්ඝ කාලයක් තිස්සේ තුලනය කර
නොමැති බවත් ප්රකාශකර ඇත.</p>

<p>එම
ආරංචියත් සමග හාලිඇල ප්රදේශයේදී වාහනය ආපසූ හැරවූ ප්රාදේශීය කළමනාකරැ යළිත්
බදුල්ල ශාඛාවට පැමිණ මේ ගැන සොයා බැලීමේදී සටහන් විශාල ප්රමාණයක් මහනූවර
ශාඛාවෙන් ලැබූණූ ඒවා බව සටහන් කර තිබූණූ බවත් ඒ ගැන විමසීමේදී අදාල වවූචර්
ගෙන්වා ගතයූතු බව කළමනාකරැ ප්රකාශ කර ඇත.</p>

<p>මේ අතර
යළිත් කොළඹ බලා පිටත්ව ගිය ප්රාදේශීය කළමනාකරැ අදාල සිද්ධිය බැංකුවේ අභ්යන්තර
විගණන අංශයට ප්රකාශ කර ඇත.</p>

<p>මේ අනූව
පැමිණි බැංකුවේ අභ්යන්තර විගණන අංශයේ නිලධාරන් විසින් අවිනිශ්චිත ගිණූම
පරකෂාකර සටහන් යොදා ඇති ගණූදෙනූ පිළිබඳව වවූචර් එවිය යූතු බැංකුවලින් විමසීමේදී
එසේ එවීමට එම ශාඛාවල වවූචර් නොමැති බව ප්රකාශකර ඇත.</p>

<p>මේ අනූව
සොයා බැලීමේදී මෙසේ සටහන් යෙදීමෙන් පමණක් රැපියල් දෙකෝටි තිස්ලකෂයක මූදලක්
වංචා කර ඇති බව හෙළිවී තිබේ.</p>

<p>මේ
අතරතුර සූඦ මූදල් අඩූවක් ඇති බවත් එම මූදල සොයාගෙන එන බවත් එම බැංකු ශාඛාවේ
දෙවන නිලධාරියාට පැවසූ එහි කළමනාකරැ ඒ සමගම අතුරැදහන් වී ඇතැයි එම ප්රකාශකයා
වැඩිදුරටත් කියා සිටියේය.</p>

<p>මගේ බිරියව සූජී මැරැවේ මගේ ඉදිරියේදීමයි
පොලිස් කොස්තාපල් කියයි</p>

<p> </p>

<p>ඉකුත් 19
වැනිදා රාති්ර 8.55 ට උතුරැ කඦතර පොලිස් ස්ථානාධිපති පොලිස් පරකෂක රෙක්ස් ජැන්සන්
මහතා හමූවීමට පොලිසියට පැමිණියේ පොලිස් මූලස්ථානයේ සේවය කරන උප පොලිස් කොස්තාපල්
30728 තුෂාර අනූර බණ්ඩාරය.</p>

<p>සර් මගේ
නෝනා අසනීපයෙන් බෙහෙත් ගන්න කොළඹ මහ රෝහලට යන්න ගියා. මරදානේදී තී්රවිල් එකක්
හම්බ වූණා. බිරිඳව තී්රවීල් එකේ නංවලා රෝහලට පිටත් කළා. බිරිඳ රෝහලට ගිහින්
නැහැ. අතුරැදන් වෙලා යැයි පැවසීය.</p>

<p>පොලිස්
කොස්තාපල්ගේ හැසිරීම ගැන සැකසිතූ පොලිස් පරීකෂකවරයා දිගින් දිගටම ප්රශ්න කළේය.
ඇත්ත කියනවා නැත්නම් මම තමූසෙට කරන දේ දන්නවා යැයි පොලිස් පරීකෂක පොලිස්
කොස්තාපල්වරයාට දැඩිව අවවාද කළේය. </p>

<p>සර් මම
ඇත්ත කියන්නම් මාව සර් බේරගන්න ඔනෑ. කොස්තාපල්වරයා බැගෑපත් විය. </p>

<p>සර් මගේ
බිරිඳව සූජී මැරැවේ මං ඉදිරියේදි. තී්රවිල් එකේ යන ගමන් සූජී බිරිඳගේ බෙල්ලට එක
පාරටම ලණූවක් දමා තද කළා. බිරිඳ මියගියා. වැලිවේරිය පාර අයිනේ බිරිඳගේ මළ සිරැර
දමා අප ආවා. යැයි පොලිස් කොස්තාපල්වරයා කියා සිටියේය.සර් මගේ නෝනා නිතරම
අසනීපයෙන් මූත්රාශයේ ලෙඩක් ඇයට තියෙනවා. ප්රතිකාර කොපමණ කළත් සූවයක් නැහැ. අපට
සිව් හැවිරිදි දියණියක්ද සිටිනවා මං බිරිඳගේ අසනීපය ගැන නිතරම කල්පනා කළා. මේ
අතරතුරදී මට ඩන්කං සූජී හමූවූණා. සූජීට මං තොරතුරැ කීවා.</p>

<p>බිරිඳව
මරා දමන්න යැයි සූජී කීවා. මරා දැමීමේ කොන්ත්රාත් එක සූජී භාර ගත්තා. කොන්ත්රාත්
මූදල රැපියල් 15,000 යි. සූජී, සමඟ කතා කරගත් පරිදි 19 වැනිදා කොළඹදී නිලන්තිව
මරන්න තීරණය කළා. 19 වැනිදා උදේ 7.21 ට උතුරැ කඦතරින් කොළඹ යන දුම්රියේ මමත්
නිලන්තිත් නැග්ගා. පෙරවරැ 10 ට මරදානෙන් අපි බැස්සා.</p>

<p>එහිදී අප
දෙදෙනා එනතුරැ තී්රවීල් රථයක් තබාගෙන සූජී හා ඕහූගේ මස්සිනා වන රෝසි සිටියා. අප
දෙදෙනා තී්රවීල් රථයට නැග්ගා.</p>

<p>අතරමගදී
මත්කුඩූ දැමූ ජින්ජබියර් බෝතලයක් නිලන්තිට දුන්නා. එය බීමෙන් පසූ ඇය නිදන්නට
පටන් ගත්තා. වැලිවේරිය දෙසට තී්රවීල් රථය ධාවනය කළා. වැලිවේරිය ප්රදේශයේදී සූජී
නිලන්තිගේ ගෙළට ලණූවක් දමා තද කළා. හූස්ම හිරවීමෙන් ඇය මියගියා. මළ සිරැර
වැලිවේරිය පාර අයිනේ දමා අපි ආපසූ ආවා. </p>

<p>සැකකාර
පොලිස් නිලධාරියා කූඩූවට දමන්නට නියෝග කළ රෙක්ස් ජැන්සන් මහතා වත්තල පොලිසියට
දුරකථන ඇමතුමක් ගත්තේය. </p>

<p>එහි සිටි
පොලිස් පරීකෂක ලෝරන්ස් මහතාට සූජී හා රෝසි අත්අඩංගුවට ගැනීමට උපදෙස්
දුන්නේය. පැය කීපයකින් සැකකරැවන් හා මළ සිරැර සොයාගැනීමට පොලිසියට මේ අනූව
හැකි විය.</p>

<p>හොරණ
සිට පොලිස් මූලස්ථානයේ රාජකාරියට යන තුෂාර අනූර බණ්ඩාරට පිඹූර අගලවත්තේ පදිංචි
කලුආරච්චිගේ නිලන්ති හමූවූයේ බස් රථයේදී. ඇය වත්තල මාබෝලේ ඇඟලුම් කම්හලක
සේවිකාවක් වූවාය.</p>

<p>පෙමින්
බැඳුන දෙදෙනා වසර දෙකක පමණ ඇසූරකින් පසූ විවාහ වූයේ දහසක් පැතුම් පොදි බැඳගෙනය.
1994 වසරේ විවාහ වූ මෙම යූවළට දැන් සිව්හැවිරිදි දැරියක් ද සිටී. දියණිය ප්රසූත
කරන විටදී රෝගී වූ ඇය පසූව දිගින් දිගටම රෝගී තත්ත්වයේ පසූවීම සැමියාට
කරදරයක් වූවාය.</p>

<p>එහි
අවසන් ප්රතිඵලය වූයේ නීතිය දන්නා නිලධාරියකු වන පොලිස් නිලධාරියකු විසින් සිය 31
හැවිරිදි බිරිඳ ඝාතනය කිරීමය.</p>

<p>වසර තුන් දහස් ගණනකට පෙර ජනාවාසයක නටබූන් මතුවෙයි </p>

<p>කුරැණෑගල
දිස්ති්රක්කයේ යාපහූව පර්වතය මතින් මීට වසර තුන්දහසකටත් දෙදහස් පන්සියයකටත් අතරතුර
කාලයක පැවැති පූර්ව ජනාවාසයක නටබූන් පූරාවිද්යා දෙපාර්තමේන්තුව මගින් අනාවරණය
කරගෙන තිබේ.</p>

<p>මෙම
ජනාවාස ශී්ර ලංකාවේ පර්වතයක් මත තිබී මෙතෙක් අනාවරණය කොටගෙන ඇති පැරණිතම ජනාවාසය
වේි.</p>

<p>පසූූගිය
දෙසැම්බර් මාසයේ පටන් යාපහූව ප්රදේශයේ පූරාවිද්යා දෙපාර්තමේන්තුව මගින්
ආරම්භ කරනූ ලැබූ විද්යාත්මත ගවේෂණ මගින් මෙම ස්ථානය අනාවරණය කරගෙන ඇත.</p>

<p>මරාගෙන මැරෙන කොටියා කූඩූවේදී බල්බයකින් බෙල්ල කපා ගනී </p>

<p>පොලිස්
කූඩූවක පසූවූ මරාගෙන මැරෙන කොටි සාමාජිකයකු එහි සවි කර තිබූ විදුලි බූබූළක් බිඳ
වීදුරැ කැබැල්ලකින් ගෙළ කපාගෙන දැන් රෝහලේ ප්රතිකාර ලබයි.</p>

<p>මොහූ
සමග අත්අඩංගුවට ගත් තවත් මරාගෙන මැරෙන කොටි සාමාජිකයකු කොටි සංවිධානයේ මීළඟ
සැලසූම් කීපයක් පිළිබඳව පොලිස් කණ්ඩායම්වලට තොරතුරැ හෙළි කරගෙන යන බව පොලිස්
ආරංචි මාර්ග කියයි.</p>

<p>රේගුව මාඦ බායි </p>

<p>මෙරටින්
වඳවීයාමේ තර්ජනයට ලක්ව ඇති මත්ස්ය විශේෂයක් ආයෝජන මණ්ඩලයේ ලියාපදිංචි
එක්තරා සමාගමක් විසින් නීති විරෝධී ලෙස අපනයනය කිරීමට සූදානම් කර තිබියදී රේගු
ජෛව විවිධත්ව ඒකකයේ නිලධාරීන් විසින් අත්අඩංගුවට ගෙන ඇත.</p>

<p>දෙපූල්ලියා
යනූවෙන් හඳුන්වන මෙරට ඇල, දොළවල ජීවත්වන මෙම මත්ස්ය විශේෂය මෙරටින්
වඳවීයාමේ තර්ජනයට ලක්ව සිටින මත්ස්යයකු බව රේගු ජෛව විවිධත්ව ඒකකයේ
නිලධාරියෙක් පැවසීය.</p>

<p>මෙම
මත්ස්යයන් 160 ක් ගුවන් මගින් හොර රහසේ කුවේටි රටට යැවීමට සූදානම්කර තිබූ
බවද හෙතෙම සඳහන් කළේය.</p>

<p>රේගු
ජෛව විවිධත්ව ඒකකයේ රේගු අධිකාරි සමන්ත ගුණසේකර මහතාගේ මෙහෙයවීම මත වැඩිදුර
පරීකෂණ කරගෙන යයි.</p>

<p>ගුවන් පාලම සහ වටරවූම නිසා කෝටි දෙකක පමණ පාඩූවක් වෙනවා
- නගර වාසීහූ </p>

<p>ගම්පහ
නගර සංවර්ධන සැලැස්ම කිසිදු හේතුවක් මත වෙනස් කරන්නේ නෑ 
- ෆවූසි ඇමැති</p>

<p>මහජන
අදහස් නොවිමසා ගම්පහ නගර සංවර්ධනය කිරීම නිසා සිදුවන රැපියල් දෙකෝටියක පමණ
අලාභය පිළිබඳ කරැණූ පැහැදිලි කිරිම සඳහා ගම්පහ නගරවාසීන් මහාමාර්ග ඇමැති ඒ. එච්.
එම්. පවූසි මහතා පෙරේදා (19) හමූවූ අවස්ථාවේදී නගරවාසීන්ගේ ඉල්ලීම් ලබාදීමට
ඇමැතිවරයා එකඟත්වය පළ කර නොමැත.</p>

<p>ගම්පහ
වෙළෙඳ මිතුරෝ සංගමයේ මෙහෙයවීමෙන් සාකච්ඡාව සංවිධානය කර තිබිණ.</p>

<p>සාකච්ඡාව
සඳහා ගම්පහ නගරවාසීන් සමඟ ගිය නීතිඥ මහත්වරැන් මහජන අදහස් නොවිමසා ගම්පහ නගර
සංවර්ධනය කිරීම නිසා සිදුවන රැපියල් දෙකෝටියක පමණ අලාභය පිළිබඳ කරැණූ පැහැදිලි
කිරිම සඳහා ගම්පහ නගරවාසීන් මහාමාර්ග ඇමැති ඒ. එච්. එම්. පවූසි මහතා (19) හමූවූ
අවස්ථාවේදී නගරවාසීන්ගේ ඉල්ලීම් ලබාදීමට ඇමැතිවරයා එකඟත්වය පළ කර නොමැත.</p>

<p>ගම්පහ
වෙළෙඳ මිතුරෝ සංගමයේ මෙහෙයවීමෙන් සාකච්ඡාව සංවිධානය කර තිබිණ.</p>

<p>සාකච්ඡාව
සඳහා ගම්පහ නගරවාසීන් සමඟ ගිය නීතිඥ මහත්වරැන් දෙදෙනකු දුටූ ෆවූසි ඇමැතිවරයා
මූලින්ම ඕවූන් සාකච්ඡාවට අවශ්ය නැතැයි පවසමින් තම ආසනයෙන් ඉවත්ව ගොස් ඇත.</p>

<p>පසූව
නීතිඥ මහත්වරැන් දෙදෙනා සාකච්ඡාවෙන් පිටව ගිය පසූ ගම්පහ වෙළෙඳ මිතුරෝ සංගමයේ
සභාපති සිවිල් ඉංජිනේරැ අජිත් මාන්නම්පෙරැම මහතාගේ ඉල්ලීම මත ඇමැතිවරයා යළි
සාකච්ඡාවට පැමිණ තිබේ.</p>

<p>ගම්පහ
තානායම අසලින් ඉදිකිරිමට නියමිතව තිබූ ගුවන් පාලම වෙනත් ස්ථානයක ඉදිකිරමට සැලසූම්
කිරම නිසා ගම්පහ නිවාස සහ ව්යාපාරවලට කෝටි දෙකක පමණ අලාභයක් සිදුවන බව
පෙන්වාදුන් නගරවැසියෝ කළින් සැලැස්ම වෙනස් කළේ කාගේ බලපෑමක් මතදැයි
ඇමැතිවරයාගෙන් විමසා සිටියහ.</p>

<p>පොලිසිය
අසල ඉදි කිරමට යෝජිත වටරවූම සහ දුම්රිය හරස් පාර හරහා ඉදිවීමට යෝජිත ගුවන්
පාලම නිසා නගර වාසීන්ට සිදුවන විශාල දේපළ අලාභය ගැන ඇමැතිවරයාට කරැණූ පැහැදිලි
කරනූ ලැබූවද කිසිදු හේවතුවක් මත නගර සැලැස්ම වෙනස් කළ නොහැකි යැයි ඇමැතිවරයා
පැමිණි පිරිසට දන්වා සිටියේය.</p>

<p>මේ
අවස්ථාවේදී සාකච්ඡාවට ගිය පිරිස එක් හඬින් සිය ඉල්ලීම් ඉදිරිපත් කිරමට වූ අතර ඊට
ද අකමැත්තක් දැක්වූ ෆවූසි මහතා සංගමයක් ලෙස තනි තනිවම පැමිණ අලාභ සිදුවන අය ප්රශ්නය
නිරාකරණය කරගත යූතුව ඇතැයි දන්වා සිටියේය.</p>

<p>කෙසේ
වූවද ගම්පහ සංවර්ධනය නිසා නගරවාසීන්ට සිදුවන අලාභය ගැන ප්රායෝගිකව සොයා බැලීමට
තමා ගම්පහට පැමිණෙන බව වෙළෙඳ මිතුරෝ සංගමයේ සභාපතිවරයාට දැනූම් දුන් ෆවූසි
ඇමැතිවරයා අලාභ සිදුවන අයගේ නම් ලැයිස්තුවක් ලබාදෙන ලෙසද දැන්වීය.</p>

<p>ගම්පහ
නගර සංවර්ධනයේදී යෝජිත ගුවන් පාලමේ ඉදිකිරීම වෙනස්කර වෙනත් සැළසූමක් යටතේ
ගුවන් පාලම සෑදීමට තීරණය කිරීමට හේතු මොනවාදැයි නගරවාසීන් රජයේ අදාළ කාර්මික
නිලධාරීන්ගෙන් විමසූ අවස්ථාවේදී කාර්මික නිලධාරීහූ සහ සැලසූම් ශිල්පීහූ නිරැත්තර
වූහ.</p>

<p>මා එල්.ටී.ටී.ඊ. හිතවාදියකු යැයි ජනපතිනිය යළිත් චෝදනා කරලා
දොස්තර
ජයලත් ජයවර්ධන </p>

<p>ජනාධිපතිනිය
විසින් තමා එල්.ටී.ටී.ඊ. හිතවාදියකු ලෙස කරන චෝදනා නිසා අන්තවාදීන් අතින් තමා
මරැමූවට පත්වූවහොත් එහි සම්පූර්ණ වගකීම මාවෙත චෝදනා එල්ල කරන ඇය වෙත
පැවරෙන්නේ යැයි ගම්පහ දිස්ති්රක් එජාප පාර්ලිමේන්තු මන්තී්ර දොස්තර ජයලත් ජයවර්ධන
මහතා නිවේදනය කර සිටී. චන්ද්රිකා බණ්ඩාරනායක කුමාරතුංග මහත්මිය වෙත ලිපියක් යවමින්
ජයවර්ධන මහතා ඒ බව දන්වා තිබේ.</p>

<p>ජනාධිපති
වරප්රසාදවලට මූවාවෙමින් නැවත වරක් පසූගිය පෙබරවාරි 15 වැනි දින ස්වාධීන රෑපවාහිනි
සවිය සාකච්ඡාවට සහභාගි වෙමින් එල්.ටී.ටී.ඊ. හිතවාදියකු බවට ජනාධිපතිනිය චෝදනා
නගා ඇතැයි එම ලිපිය මගින් ජයවර්ධන මහතා අවධාරණය කර තිබේ.</p>

<p>මෙම
චෝදනා අප රටෙි ඔනෑම ස්වාධීන අධිකරණයක් ඉදිරියට ඉදිරිපත් කරන ලෙසද ජනාධිපතිනියට
දන්වා සිටින මන්තී්රවරයා එවැනි අභියෝගයකට තමා සූදානම් යැයිද සඳහන් කරයි.</p>

<p>තමා වෙත
මෙවැනි කොපමණ චෝදනා නැගුවත් ජනතාව වෙනූවෙන් කරන සේවාවන් නතර නොකරන බවද හෙතෙම
මෙම ලිපිය මගින් වැඩිදුරටත් අවධාරණය කර තිබේ.</p>

<p>වාරිමාර්ග ඉතිහාසයේ වැදගත් දැ මහවංශයේ ලියැවී නැත්තේ ඇයි?
-වාරිමාර්ග
ඇමැති ආචාර්ය සරත් අමූණූගම </p>

<p>මහාවංශය
නමැති ඓතිහාසික සීමාසහිත සටහන් ග්රන්ථයට අනූකූලව අප ඉතිහාසය යැයි දන්නා කරැණූවලට
තවත් වැදගත් කරැණූ රැසක් එක්විය යූතුය. නිවැරදිව කියනවා නම් අපේ ලංකා ඉතිහාසය
මෙතෙක් නිවැරදිව ලියවී නැති බවත්, අදින් වසර එකසිය එකකට ඉහත වාරිමාර්ග සංස්කෘතිය
ආශ්රිතව ඇරඹී නිදහස් සටන පිළිබඳව සත්ය කරැණූ අනාගතයේදී ඉතිහාසයට අන්තර්ගත කිරම
අනීවාර්ය බවත්, වාරිමාර්ග සහ ජලසම්පත් කළමණාකරණ සහ චිත්රපට කර්මාන්ත අමාත්ය
ආචාර්ය සරත් අමූණූගම මහතා පසූගියදා වාරිමාර්ග දෙපාර්තමේන්තු සභා ශාලාවේදී පැවති
රැස්වීමක මූලසූන දරමින් පැවසීය.</p>

<p>පනස්
තුන්වෙනි නිදහස් සමරැව සහ රණවිරැ ප්රණාමය නිමිත්තෙන් මේ උත්සවය පැවැත්විණි.</p>

<p>චිරාගත
සම්ප්රදායානූකූලව පහන් දැල්වීමෙන් අනතුරැව ආගමික වතාවත්ද, වියෝවූ රණවිරැවන්
සිහිකිරමද, සිදුවූණූ අතර, අනතුරැව ඇමතිවරයා තවදුරටත් මෙසේද පැවසීය.</p>

<p>අනූරාධපූර,
පොළොන්නරැව අතහැර ගොවි ජනතාව වෙනත් ප්රදේශවලට සංක්රමණය වූයේ මැලේරියාව වසංගතයම
නිසා නොවේ. රජරට ඉපැරණි වැව්, අමූණූ, වාරිමාර්ග කාලයාගේ ඇවෑමෙන් කැඩී, බිඳී
නටබූන් වූ නිසයි. එහෙත් එකල වාරිමාර්ග නිලධාරන් විසින් මේ සියල්ල ප්රතිසංස්කරණය
කිරමෙන් පසූ ඒ සැහැල්ලු ජීවිත පරිසරය තුළ නිදහස් සටනකට වාතාවරණය මතුවිණි. නිදහස්
සටන ඇරඹූණේ වාරිමාර්ග සම්පත සමග ඒකාබද්ධවයි.</p>

<p>එදවස ජල
කලමනා කරණයක් පැවතියේ එවක වාරිමාර්ග නිලධාරන්ගේ දායකත්වයෙනි. ඒ ජලකළමනාකරණ
තුළින් සිදුවූණූ මෙහෙවර ඉතා වැදගත්ය. අද පවා වාරිමාර්ග නිලධාරන් අතිශයින් ඇපකැපවී
කරන්නා වූ මේ ජාතික මෙහෙවර මම අගය කරමි.</p>

<p>එදා
අතීතයේ වාරි නිලධාරන් කටයූතු කළේ, ක්රියා කළේ, මිනිසූන්ගේ ගණනට ඉඩම්ද, ඉඩම්
ප්රමාණයට ජලයද සැපයීමේ ක්රමය අනූවයි. අද අපට එය කිරම අසීරැ වූවත් අනාගතයේදී
උද්ගතවන ඒ තත්ත්වයට මූහූණ දීමට ජල කළමනාකරණයක් අවශ්ය වේ. ඒ කියන්නේ ජල
බද්දක් හෝ ජලය විකිණීම හෝ සිදු නොවන බව පැහැදිලිව තේරැම් ගත යූතුය.</p>

<p>නිදහස්
සටනේ ආරම්භය වාරිමාර්ග ප්රතිසංස්කරණය නම්, එය තුළින් උපන් ශිෂ්ටාචාරයට ආශ්රිතව
එකල මෙන්ම, මෙකලද, වාරිමාර්ග නිලධාරින් කරනූයේ ඉමහත් ජාතික මෙහෙවරකි. දේශයක්,
ජාතියක්, ස්වාධීන වීමට ස්වයං පෝෂණය අත්යවශ්යයි. ඒ සඳහා අනූබලය ලැබෙනූයේ
වාරිමාර්ග නිලධාරන් වෙතිනි. එදා සිට අද දක්වා වාරිමාර්ග නිලධාරින් විසින් කරනූ ලබන
මේ උදාර මෙහෙවර නිදහස් සටනේ ප්රබල සංකේතයකි. මෙතරම් කැපවීමකින් ජාතික සේවයක්
කරන තවත් නිලධාරන් පිරිසක් මට හමූවී නැතැයිද, තව දුරටත් ඇමැතිවරයා කීවේය.</p>

<p>ලාඔසය,
කාම්බෝජය, තායිලන්තය, ආදී රටවල් රාශියක ජනතාව ගම්මාන වෙතින් කුමක් හෝ සූඦ
රැකියාවක් සොයා නගරවලට සංක්රමණය වන නමූත් අපගේ ගැමි ජනතාව දැනූදු ගමත්, හේනත්,
කුඹූරත්, ගැමි සංස්කෘතියත්, රැකගෙන බොහෝ දුරට ගම්මානවලම වාසය කිරම වැදගත් බවද,
එහෙත් කාලීන ලෙස ඕවූන් කවදා හෝ නගරයට සංක්රමණය වේයැයි, යන අපේකෂාවෙන් ඕවූන්ට
අවශ්ය ජල පහසූකම් ලබාදීමට ජල කළමනාකරණය නිසි පරිදි ක්රියාත්මක විය යූතු බවද
අමූණූගම මහතා පැවසීය.</p>

<p>බෙදුම්වාදී සන්නද්ධ සංවිධානයක් වන
එල් ටී ටී ඊ බි්රතාන්යය තුළ පමණක් නොව මූඦ මහත් යූරෝපාකරයේම තහනම්
කළ යූතුයි </p>

<p>ජනතා
විමූක්ති පෙරමූණ යූරෝපා පාර්ලිමේන්තු කණ්ඩායමට කියයි</p>

<p>එල්. ටී.
ටී. ඊ. සංවිධානය වැනි බෙදුම්වාදී සන්නද්ධ සංවිධානයක්, බි්රතාන්යය තුළ පමණක් නොව
මූඦ මහත් යූරෝපාකරය තුළම තහනම් කළ යූතු යැයි ජනතා විමූක්ති පෙරමූණ විසින් මේ
දිනවල ශ්රී ලංකාවේ සංචාරයක නියූතු යූරෝපා පාර්ලිමේන්තු කණ්ඩායමට ප්රකාශ කර තිබේ.</p>

<p>ජනතා
විමූක්ති පෙරමූණේ ප්රධාන කාර්යාලයේදී (20 දා) දහවල් දෙපාර්ශ්වය අතර පැවැති
සාකච්ඡාවකදී මේ බව ප්රකාශ කර ඇත.</p>

<p>යූරෝපා
පාර්ලිමේන්තු කණ්ඩායම නියෝජනය කරමින් යූරෝපා ජාතීන්ගේ කණ්ඩායම් සංගමයේ
සභාපති ජෙරාඩ් කොලින්ස් මහතාගේ නායකත්වයෙන් යූත් නියෝජිතයන් නවදෙනකු මෙම
සාකච්ඡාවට සහභාගි වූ අතර ජනතා විමූක්ති පෙරමූණ නියෝජනය කරමින් එහි ප්රධාන ලේකම්
ටිල්වින් සිල්වා හා ගම්පහ දිස්ත්රික් පාර්ලිමේන්තු මන්ත්රී විජිත හේරත් යන මහත්වරැ
මෙම සාකච්ඡාවට සහභාගි වූහ.</p>

<p>මෙහිදී
යූරෝපා නියෝජිත පිරිස ශ්රී ලංකාවේ ජනවාර්ගික ගැටලුව පිළිබඳව ජ.වි.පෙ. ස්ථාවරය
කුමක්දැයි විමසූ අතර එයට පිළිතුරැ දුන් ජ.වි.පෙ. නායකත්වය පවසා සිටියේ ශ්රී
ලංකාවේ සිංහල, දෙමළ, මූස්ලිම් යන ජාතිකත්වයන් එක්ව රට පූරා ජීවත් වන බැවින් මෙරට
ඇත්තේ ජනවාර්ගික ගැටලුවක් නොවන බවත්, මෙරට ජනවාර්ගික යූද්ධයක් නොමැති බවත්ය.
තවද මෙරට පවතින්නේ ජාතික ගැටලුවක් බවත්, යූරෝපා නියෝජිත පිරිසට ජ.වි.පෙ.
නායකත්වය විසින් පැහැදිලි කර දෙන ලදී.</p>

<p>එසේම
මෙකී ජාතික ගැටලුව නිර්මාණය වීමට හේතුව රටෙි සියලු ජන කොටස්වලට සමානාත්මතා
පදනමින් අයිතීන් ලබා දී නොමැති වීම බවත් ජ.වි.පෙ. නායකත්වය විසින් තවදුරටත්
පෙන්වා දුන්නේය.</p>

<p>ඉන් පසූව
යූරෝපා නියෝජිත පිරිස, ශ්රී ලංකාවේ මානව හිමිකම් ආරකෂා වීම හා ප්රජාතන්ත්රවාදය
ක්රියාත්මක වීම පිළිබඳව ජ.වි.පෙ. නායකත්වයෙන් කරැණූ විමසූ අතර, ඊට පිළිතුරැ දුන්
ජ.වි.පෙ. නායකත්වය පවසා සිටියේ වත්මන් ආණ්ඩූව සහ එල්. ටී. ටී. ඊ. සංවිධානය
විසින් රට පූරා මානව හිමිකම් උල්ලංඝණය වන අයූරින් කටයූතු කරන බවයි. මෙහිදී මානව
හිමිකම් ආරකෂා කිරමේදී ආණ්ඩූව සතුව වැඩි වගකීමක් පවතින බව ද ඕවූහූ අවධාරණය කළහ.
එසේම මැතිවරණ අදහස් ප්රකාශ කිරම, ජීවත්වීම ආදී සෑම කටයූත්තකදීම මෙරට ජනතාවගේ
ප්රජාතන්ත්රවාදී අයිතීන් උල්ලංඝණය කිරමට වත්මන් ආණ්ඩූව හා එල්. ටී. ටී. ඊ. සංවිධානය
කටයූතු කරන බවත්, ජ.වි.පෙ. නායකත්වය විසින් පෙන්වා දුන් අතර මෙහිදී ප්රධාන චූදිතයා
වන්නේ ආණ්ඩූව බවත් වැඩිදුරටත් පෙන්වා දුන්නේය.</p>

<p>නෝර්වේ
මැදිහත්වීම් සම්බන්ධයෙන් ජ.වි.පෙ. ස්ථාවරය කුමක්දැයි යූරෝපා නියෝජිතයන් විසින්
මෙහිදී විමසූ අතර, ඊට පිළිතුරැ දුන් ජ.වි.පෙ. නායකත්වය පවසා සිටියේ රටෙි
අභ්යන්තර ප්රශ්නයක් දඩමීමා කරගනිමින් එක් පාර්ශ්වයකට පමණක් වැඩි සැලකිල්ලක්
දක්වමින් පකෂග්රාහීව කරන මැදිහත්වීමකට තමන් විරැද්ධ බවත්, එය නෝර්වේ පමණක් නොව
කවර රටක් වූවද සිදු කරන්නේ නම් තමන් එයට දැඩි සේ විරැද්ධ වන බවත්ය.</p>

<p>මෙම
සාකච්ඡාවේදී ජ.වි.පෙ. නායකත්වය විසින් යූරෝපා පාර්ලිමේන්තු කණ්ඩායමට ඉල්ලීම්
කිහිපයක් ඉදිරිපත් කරන ලදී. රටෙි ප්රජාතන්ත්රවාදය ස්ථාපිත කිරම පිණිස වත්මන්
ආණ්ඩූවට බලපෑම් කළ යූතු බවත්, එම බලපෑම රටෙි අභ්යන්තර ප්රශ්නයක් දඩමීමා කරගෙන
කරන එකක් නොවිය යූතු බවත්, එය රාජ්යතාන්ත්රික බලපෑමක් විය යූතු බවත් ජ.වි.පෙ.
නායකත්වය ඉදිරිපත් කරන ලද පළමූ ඉල්ලීම විය.</p>

<p>දරැණූම පාතාල කල්ලි පිළිබඳ සංගණනයක් </p>

<p>ශ්රී
ලංකාවේ මේ වනවිට බිහිවී ඇති දරැණූම පාතාල සන්නද්ධ කණ්ඩායම් පිළිබඳව සියලු
තොරතුරැ එක්රැස් කර ඒවා ලේඛනගත කරන ලෙස පොලිස්පති ලකී කොඩිතුවක්කු මහතා
සියලුම ජ්යෙෂ්ඨ පොලිස් නිලධාරන්ට උපදෙස් දී තිබේ.</p>

<p>පාතාල
සන්නද්ධ කණ්ඩායම් විසින් සිදුකරන දරැණූ අපරාධ දිනෙන් දිනම ඉහළ යාම නිසා මෙම අපරාධ
රැල්ල මැඬපැවැත්වීමට පොලිස්පති වරයා විශේෂ අවධානය යොමූකර ඇත.</p>

<p>පාතාල
කණ්ඩායම් සහ එහි සාමාජිකයන් පිළිබඳව තොරතුරැ එක්රැස් කිරම නිසා ඕවූන් අත්අඩංගුවට
ගැනීම පහසූ වන බවත්, පාතාල කණ්ඩායම් මැඬපැවැත්වීමට දිවයින පූරා පොලිස් කණ්ඩායම්
යොදවා ඇති බවත් ලකී කොඩිතුවක්කු මහතා "දිවයින"ට පැවසීය.</p>

<p>පාතාල
සන්නද්ධ කණ්ඩායම් පිළිබඳව තොරතුරැ වෙතොත් පොලිස් මූලස්ථානය වෙත හෝ ළඟම ඇති
පොලිසිය වෙත දැනූම් දෙන ලෙස ඉල්ලීමක් කරන පොලිසිය එම තොරතුරැ ලබා දෙන අයගේ
රහස්යභාවය ආරක්ෂා කරන බව ද පවසයි.</p>

<p>ගුවන් යානයක් පණ ගැන්වූ වේගයට ආරක්ෂක මූර කුටියක් ඉවතට විසිවෙයි </p>

<p>කටූනායක
ගුවන් තොටූපළ ගුවන් අංගනයේදී ඉන්දුනීසියානූ ගුවන් යානයක් වැඩිපූර පණගැන්වීම
හේතුවෙන් ඒ අසල තිබූ ආරක්ෂක මූර කුටියක් ඉවතට විසිවී ගොස් එහි සිටි එක් අයකු
(20) තුවාල ලබා තිබේ.</p>

<p>ගුවන්
යානා අංගනයේදී භූමි පාලක නිලධාරියකු මෙසේ ඈතට විසිවී ගොස් කණූවක වැදී තුවාල
ලබා ඇති අතර ඕහූ පසූව ශ්රී ලංකා එයාර්ලයින් වෛද්ය මධ්යස්ථානයට ඇතුළත්කොට
ඇත.</p>

<p>මෙම
ගුවන් යානය පසූගියදා හජ්ජි වන්දනා පිරිසක් රැගෙන මක්කම බලා යද්දී ආපදාවට ලක්ව
කටූනායක ගුවන් තොටූපලට ගොඩ බැස්සූ ඉන්දුනීසියානූ ගුවන් යානය අලුත්වැඩියා කිරීම
සඳහා අවශ්ය අමතර කොටස් රැගෙන පැමිණි වෙනත් යානයකි.</p>

<p>දුප්පත්කම නිසා ළමා නිවාසයේ රැඳී සිටි දැරියගේ මරණය අභිරහසක් </p>

<p>බණ්ඩාරගම,
වැල්මිල්ලේ පදිංචි දහතුන් හැවිරිදි දෑරියක් කහතුඩූව පොලිසිය විසින් අත්අඩංගුවට
ගෙන කැස්බෑව මහෙස්ත්රාත් අධිකරණයට ඉදිරිපත් කරනූ ලැබිණි. ඇය සමග ඇගේ ඥාති සොයූරියක්ද
අධිකරණය හමූවට පමූණූවනූ ලැබ සිටියාය.</p>

<p>සළෙඦන්
සමග අනාචාරයේ හැසිරීම එම දැරියනට එරෙහිව අධිකරණයට ඉදිරිපත් කෙරැණූ චෝදනා විය.</p>

<p>දෑරියන්
පෝෂණය කිරීමට නොහැකි අන්දමේ ආර්ථික දරිද්රතාව නිසා ඕවූන්ගේ මව විසින්ම එසේ
දැරිය සළෙඦනට බිලිදුන් බවද පැවසිණි.</p>

<p>දෑරියන්
දෙදෙනාගෙන් එක් දැරියක් කඩවත රම්මූතුගල සහතික කළ බාලිකා පාසලටද, අනෙක් දැරිය
පරිවාස දෙපාර්තමේන්තුවේ මොරවින්න රැඳවූම් නිවාසයටද අධිකරණ නියෝග මත භාර කරන
ලදී.</p>

<p>රම්මූතුගල
බාලිකා නිවාසයේ රඳවන ලද දැරියගේ රැඳවූම් කාලය 1998 වසරේදී පමණ අවසන් වූ විට ඇය
ආපසූ රැගෙන යන ලෙසට ඇගේ භාරකාරයනට රැඳවූම් නිවසේ පාලක පකෂය විසින් දන්වා
තිබිණි.</p>

<p>එහෙත්
ඉන්නට හිටින්නට පවා තැනක් නැති දැරියගේ භාරකරැවනට (දෑරියගේ පියා නැති බවද, මව
සිහිමද ගතියෙන් පෙළෙන බවද දෑනගන්නට ලැබේ.) ඇය භාර ගැනීමට නොහැකි වූයෙන් දෑරිය
රැඳවූම් නිවාසයේම ජීවත් වූවාය.</p>

<p>එහෙත්
පසූගිය 15 දා උදෑසන රම්මූතුගල රැඳවූම් භූමියේ පොල් ගසක් යට සිහිසූන්ව වැටී සිටි
දැරිය පසූව මිය ගියාය.</p>

<p>රැඳවූම්
නිවාසයේ පාලිකාවන් නිතර තමාට පහර දෙන බව ඇය නිතර පැවසූ බවද මරණය ගැන සැලවූ වහාම
රැඳවූම් නිවාසයට ගොස් සෙසූ රැඳවියන් සමග කරැණූ විමසන විට තමන්ට ඕවූන් දොස් පැවරෑ
බවද මියගිය දෑරියගේ ඥාතීහූ පැවසූහ.</p>

<p>මරණය
සැකසහිත බවට දෑරියගේ ඥාතීන් විසින් බණ්ඩාරගම පොලිසියට ද බස්නාහිර පළාත් සභා
මන්ත්රී සහ එ.ජා.ප. සංවිධායක නීතිඥ හේමන්ත වික්රමාරච්චි මහතාගේ ප්රධානත්වයෙන්
බස්නාහිර පළාතේ පරිවාස සහ ළමාරකෂක කටයූතු පිළිබඳ ඇමැති මහේෂ් අල්මේදා යන
මහත්වරැනටද කරැණූ ඉදිරිපත් කිරමෙන් පසූව රාගම රෝහලේදී මරණ පරකෂණය පැවැත්විණි.</p>

<p>ගම්පහ
මහෙස්ත්රාත් විජිත සේනාරත්න මහත්මියගේ නියෝගය මත පැවැත්වූණ මරණ පරකෂණයේදී මරණය
ඉල ඇට බිඳීමෙන් හා බෙල්ලට හානිවීමෙන් සිදුවූ බවට රාගම රෝහලේ අධිකරණ වෛද්ය
නිලධාරි ආනන්ද සමරකෝන් මහතා වාර්තා කළේය.</p>

<p>උදේ හයට
අවදි වූ අපි සෙල්ලම් කරමින් සිටින විට උදේ තේ බීමට බෙල් එක ගැහූවා. ඉනෝකා තේ
බීමට යෑමට අකමැති බව කී නිසා අපි ගියා. පසූව ආපසූ අවූත් ඇය සොයන විට ඇය
පොල්ගසක් යට වැටී සිටියා යැයි මරණ පරකෂණයේදී සාකෂි දෙමින් රැඳවූම් බාලිකා පාසලේ
රැඳවියන් වන දාහත් හැවිරිදි නිලන්ති සහ සෙල්වි යන දෙදෙනා පැවසූහ.</p>

<p>සියක් වසක් වයසැති 11 දරැ මාතාවට ආයූබෝ වේවා! </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>වයස
අවූරැදු 100 යි. දරැවන් 11 යි. මූහූපූරැ මිණිපිරියන් 30 යි. දෙමූණූපූරන් 32 යි.
තුන්මූණූපූරන් 2 යි.</p>

<p>මේ
වාසනාවන්ත මෑණියන්ගේ ඇස් පෙනීම ශරර ශක්තිය සාමාන්ය තත්්ත්්වයේ තවම පවතියි.
ඇය කුඩා මිතව හේරත්හාමිගේ උක්කුහාමිය.</p>

<p>නිකවැරටිය
තලා කොළහේනේ ශී්ර සූදර්ශනාරාමස්ථානාධිපති කුඩාමිතව ඤාණරතන හිමියන්ගේ මෑණියන් වන
උක්කුහාමි මහත්මිය තුන් මූණූබූරන් දැක සතුටූ වන්නීය.</p>

<p>පොළොන්නරැවේ ඓතිහාසික ගල් විහාරය අභියසදී විශේෂ පිංකම්
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p>උපාලි
සමූහ ව්යාපාරයේ නිර්මාතෘ උපාලි විජයවර්ධන මහතා ඇතුඦ පිරිස අතුරැදන්වී වසර 18
ක් පිරම නිමිත්තෙන් පසූගිය 13 දා පොළොන්නරැවේ ඓතිහාසික ගල් විහාරය අභියසදී
විශේෂ පිංකම් මාලාවක් පැවැත්විණි.</p>

<p>දකෂිණ සංවර්ධන අධිකාරිය රැපියල් කෝටි ගණනක් මෙතෙක් වැයකර තිබෙන්නේ
හම්බන්තොට ජනයාගේ දැවෙන ප්රශ්න විසඳීමට නොවෙයි </p>

<p>ලංකා
සමසමාජ පකෂයේ හම්බන්තොට දිස්ත්රික් සංවිධායක</p>

<p>දකෂිණ
සංවර්ධන අධිකාරිය රැපියල්් කෝටි ගණනක මූදලක් මෙතෙක් වැයකර තිබෙන්නේ හම්බන්තොට
දිස්ත්රික්කයේ ජනතාවගේ දැවෙන මූලික ප්රශ්න විසඳීම සඳහා නොව කුකුල්කොටූ, එඦකොටූ
සැකසීම සඳහා යැයි ලංකා සම සමාජ පකෂයේ හම්බන්තොට දිස්ත්රික් සංවිධායක අමරජීව
එදිරිසූරිය මහතා පැවැසීය.</p>

<p>අමරජීව
එදිරිසූරිය මහතා එසේ පැවැසූවේ ලුණූගම්වෙහෙර ව්යාපාරයෙහි නව සංවර්ධන ප්රදේශයේ
ගොවින්ට මූහූණ පෑමට සිදුවන ගොවි ප්රශ්නවලට විසඳුම් ලබා ගැනීමට හා මූලික
අයිතිවාසිකම් දිනා ගැනීමේ අරමූණින් ලුණූගම්වෙහෙර විහාරස්ථානයේදී පිහිටූවා ගනූ
ලැබූ "කිරිඳිඕය නව සංවර්ධන ප්රදේශයෙහි ජනතා අයිතිවාසිකම් සූරකීමේ සංවිධානයේ
ගොවීන් අමතමිනි.</p>

<p>අමරජීව
මහතා තවදුරටත් අදහස් දක්වමින් හම්බන්තොට දිස්ත්රික්කයේ ජීවත්වන පිරිසෙන් වැඩි
දෙනකු ජීවත්වන්නේ ගොවිතැනින්. නමූත් අද ඕවූන්ට වගා කිරමට ජලය නැතිව අසරණවෙලා.</p>

<p>හම්බන්තොට
දිස්ත්රික්කයේ පදිංචි ජනතාවගෙන් හැට දෙදහසකට අධික පවූල් සංඛ්යාවක් කෘෂිකාර්මික
කටයූතුවල යෙදී සිටියත් එයින් 33% කට වගා කිරමට තමන්ට කියා ඉඩම් නැහැ. ඉඩම් ඇති
අයට වගා කිරමට ජලය නැහැ. මේ නිසා දුගී භාවයේ අනූපාතය 7% බවට පත්වෙලා.</p>

<p>එනිසා
හම්බන්තොට දිස්ත්රික්කයේ ජනතාවගේ එකම ඉල්ලීම ජලය ලබා දෙන ලෙසයි.</p>

<p>ලුණූගම්වෙහෙර
විකල්ප ජලයෝජනා ක්රමයක්් ඇති කළ යූතුයි. මේ සඳහා දේශපාලන වශයෙන් පිල් බෙදී
සිටින සියලු දෙනා එක්විය යූතුයි. දේශපාලන මත මැතිවරණ වේදිකාවට පමණක් සීමා කර
සමගිය සංකේතවත් කරන කහ කොඩිය යටතේ රජයට බලකරමූ. ලුණූගම්වෙහෙර කලු ගංගා නිල්වලා
යන ගංගා මූල්කරගෙන සැලසූම් කළ ජල යෝජනා ක්රමය ක්රියාත්්්මක කරන ලෙසය.</p>

<p>කිරිඳිඕය
ඒකාබද්ධ සංවිධානයේ ලේකම් රංජිත් කුමාරසිරි ගොවි මහතා - අද රජයට අවශ්ය වී
ඇත්තේ ලුණූගම්වෙහෙර ව්යාපාරයට ජලයෝජනා ක්රමයක් ක්රියාත්මක කිරමට නොව
හම්බන්තොට කර්මාන්ත පූරයට ජලය ලබා දීමේ ව්යාපෘතියක් ක්රියාත්මක කිරමටයි. මේ
සඳහා රජය ක්රියා කරන්නේ මැණික්ගඟ හම්බන්තොට කර්මාන්තපූරයට හැර වීමටයි.</p>

<p>මෙවැනි
තත්ත්වයක් උදා වූවොත් තිස්සමහාරාම හා ලුණූගම්වෙහෙර ප්රදේශයේ ඉඩම් මූඩූ බිම් බවට
පත්වීමේ පාරිසරික හානියකට මූහූණ දීමට සිදුවේ. එවැනි තත්ත්්වයක් උදා වූවහොත්
හම්බන්්තොටට ජලය ගෙන යන්න සිදුවෙන්නේ අපේ මළකඳන් උඩින් තමයි.</p>

<p>ගොවි
නියෝජිත උපාලි පොන්සේකා මහතා - ලුණූගම්වෙහෙර තවත් සෝමාරියාවක් නොකොට මැතිි
ඇමැතිවරැන් සඳහා වැයවන මූදල් එක් මාසයක් නතරකර ලුණූගම්වෙහෙරට විකල්ප ජලයෝජනා
ක්රමයක් ලබා දෙන්න කියල අපි රජයෙන් ඉල්ලා සිටිය යූතුයි.</p>

<p>ලුණූගම්වෙහෙර
වීරවිල ගොවි නියෝජිත ඔ. ඇම්. සිරිසේන මහතා අද ලුණූගම්වෙහෙර ගොවි ජනතාවට
ලැබිලා තිබෙන්නේ කුඩම්මාගේ සැළකිළි. නිසි ජල ප්රමාණයක් නොලැබීමෙන් කන්න කිහිපයක
සිට කුඹූරැ වගාව අසරැ වූණා. අද වී වගාවට ජල කඳුළක් ලැබිය නැහැ. මේ තත්ත්වය යටතේ
ලුණූගම්වෙහෙර ජනතාව අන්ත අසරණ වෙලා අද ජීවත් වෙන්න විදියක් නැතිව බටර් විස්කෝතු
කාල ජීවත් වෙන්නේ.</p>

<p>මේ නිසා
ලුණූගම්වෙහෙර විකල්ප ජල යෝජනා ක්රමයක් ඇති කරන තෙක් ගොවීන්ට සහනාධාර ක්රමයක් ලබා
දීමට පියවර ගන්නා ලෙස වගකිවයූතු නිලධාරන්ගෙන් ඉල්ලා සිටිනවා.</p>

<p>ඩූබායි වෙළෙඳ සල්පිලට මෙරට සමාගම් 18 ක්
අනූග්රහය
හැටන් නැෂනල් වෙතින් </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>ඩූබායි
වෙළෙඳ සල්පිලට සහභාගි වන ශ්රී ලාංකික සමාගම් වෙනූවෙන් හැටන් නැෂනල් සමාගම අනූග්රහය
දැක්වීම පිළිබඳව පසූගියදා පවත්වන ලද පූවත්පත් සාකච්ඡාවට සහභාගි වූ ශ්රී ලංකාවේ
එක්සත් අරාබි එමීර් රාජ්යයේ තානාපති සයීඩ් අලී අල් නොවයිස් මහතා සහ හැටන්
නැෂනල් බැංකුවේ කළමනාකාර අධ්යකෂ රියන්සි ටී. විජේතිලක යන මහත්වරැ ඇතුඦ පිරිස
ඡායාරෑපයේ වෙති.</p>

<p>ඩූබායි
වෙළෙඳ සල්පිල මෙවරත් මාර්තු 1 සිට 31 දක්වා රටවල් 30 ක සහභාගිත්වයෙන් පැවැත්වේ.</p>

<p>ශ්රී
ලංකාවේ ප්රධාන පෙළේ සමාගම් 18 ක් සඳහා තම නිෂ්පාදන ඉදිරිපත් කරන අතර හැටන් නැෂනල්
බැංකුව ඒ සඳහා අනූග්රහය සපයයි.</p>

<p>මෙම
වෙළෙඳ සල්පිලේ ශ්රී ලංකා දිනය යෙදී ඇත්තේ මාර්තු 24 දිනටය.</p>

<p>සල්පිලට
සහභාගි වීමෙන් ශ්රී ලාංකික සමාගම්වලට විශාල වෙළෙඳ පොළක් විවෘත වන බවත් එක්සත්
අරාබි එමීර් රාජ්යය අද ලෝක වෙළෙඳාමෙහි ප්රධාන මධ්යස්ථානයක් බවත් මේ
නිමිත්තෙන් පසූගියදා කොළඹ ටාජ් සමූද්ර හෝටලයේ පැවති ප්රවෘත්ති සාකච්ඡාවකදී හැටන්
නැෂනල් බැංකුවේ කළමනාකාර අධ්යකෂ රියන්සි ටී. විජේතිලක මහතා පැවසීය.</p>

<p>අනෙකුත්
රටවල් සහභාගි වන සමාගම් සඳහා ඒ ඒ රටවල රාජ්ය අනූග්රහය ලැබෙන මූත් ශ්රී ලංකාවේ
පමණක් පූද්ගලික ආයතනයක් ඊට අනූග්රහය දක්වන බව කී විජේතිලක මහතා මෙම වෙළෙඳ
සල්පිලෙහි ඇති වැදගත්කම මෙරට රජය වටහාගෙන නැතැයි තමා සිතන බවද පැවසීය.</p>

<p>කුලසිරි
බූදවත්ත ප්රමූඛ සංස්කෘතික කණ්ඩායමක් මෙම සල්පිලේදී සංස්කෘතික අංග ඉදිරිපත් කිරමටද
නියමිතය.</p>

<p>ශ්රී
ලාංකික කණ්ඩායම් වෙනූවෙන් ශ්රී ලංකන් එයාර් ගුවන් සමාගමද අනූග්රහය සපයයි. වෙළෙඳ
සල්පිලෙහි නිල ගුවන් සේවය එමිරේටිස් ගුවන් සේවයයි. මේ වෙළෙඳ සල්පිල සඳහා
සහභාගි වන ආයතන හා සමාගම් මෙසේය.</p>

<p>ස.තො.ස,
ශ්රී ලංකා තේ මණ්ඩලය, ශ්රී ලංකා ආයෝජන මණ්ඩලය, සහ ඩී. එස්. අයි. මැලිබන්, ඕඩෙල්
සිලෝන් කෝල්ඩ් ස්ටෝ්ස්, යූනික් මල්ටි, ඉයූරෝ ස්කෑන්, බ්ලු ඩයමන්ඩ්, එඩ්නා,
දිල්මා, ස්ටෑන්ඩර්ඩ් ටෙි්රඩින්, ඕරියන්ටි ඩිස්ටි්රබියූෂන්, ට්රයිකෝ, ටෝරස්
සමාගම්.</p>

<p>ජයසේකර කළමනාකරණ ආයතන පරිගණක පාඨමාලාව </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>ජයසේකර
කළමනාකරණ ආයතන පරිගණක පාඨමාලාව අවසන් කළ ශිෂ්ය ශිෂ්යාවන්ට එම ආයතනයේ
කළමනාකාර අධ්යක්ෂ ජේ. එම්. යූ. බී. ජයසේකර මහතාගේ ප්රධානත්වයෙන් සහතික ප්රදානය
කිරම පසූගියදා පවත්වන ලදී.</p>

<p>ලංකා වෙබ් ඩිරෙක්ටරි ඩොටි කොම් </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>්අලුත්ම
වෙබ් අඩිවිය දොරට වැඩීම නිමිත්තෙන් පැවති ප්රවෘත්ති සාකච්ඡාවට සහභාගි වූ අයි. එස්.
පී. පූද්ගලික සමාගමේ කළමනාකාර අධ්යක්ෂ ජගත් සූමතිපාල, වෙබ් අඩවි කි්රයාකාර
කළමනාකරැ අසංක හේවාවසම් රෙජී විජේවර්ධන. රංජිත් කුමාර යන නිලධාරන් ඡායාරෑපයේ
වෙති.</p>

<p>ලංකා බෙල් සබඳතාව ගම්බදට </p>

<p>ලංකා
බෙල් සිය දුරකතන සම්බන්ධතා ප්රමාණය ලක්ෂයේ සීමාව පසූකර දැන් ශී්ර ලංකාවේ ගම්බද
පළාත් උදෙසා සිය සේවාව ව්යාප්ත කරන බව එම ආයතනය නිවේදනයක් නිකුත් කරමින්
සඳහන් කරයි.</p>

<p>සමාගමේ
ප්රකාශක යකුට අනූව මෙම ඉලක්කය පෙබරවාරි මස අවසානයට පෙර ළඟා කර ගැනීමට ඕවූන්
බලාපොරොත්තු වේ. ඒ සඳහා නෙක්  වෙතින්  උපකරණ සපයන අතර ජපානයේ නකී්ඕ
සහ ඇමරිකා එක්සත් ජනපදයේ ටයිම් වෝර්නර් වෙතින් ජාත්යන්තර සැටලයිටි තාක්ෂණය
සපයනූ ඇතැයි එම නිවේදනයේ සඳහන්ය.</p>

<p>එමෙන්ම
රාජ්ය ආයතනය හා සහයෝගීත්වය ඇතිව කි්රයාකිරීම තුළින් මහජනතාව වෙත දුරකතනය හා
අන්තර්ජාලා පහසූකම් තවදුරටත් ළඟාකරවීමද ලංකා බෙල්හි තවත් එක් වැඩ පිළිවෙළකි.</p>

<p>මහජන බැංකුවේ නව ප්රධාන විධායක වැඩ බාර ගනියි </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>මහජන
බැංකුවේ නව ප්රධාන විධායක නිලධාරි සාමාන්යාධිකාර වශයෙන් බි්රතාන්ය ජාතික
ඩෙරික් කෙලී මහතා පසූගියදා වැඩ බාර ගත්තේය.</p>

<p>විවිධ
ක්ෂේත්ර රැසක ජ්යෙෂ්ඨ කළමනාකාර මටිටමින් අත්දැකීම් ඇති ජාත්යන්තර විධායක
නිලධාරියකු වන ඩෙරික් කෙලී මහතා සංගත සැලැස්ම ක්රියාත්මක කිරමේ සහ මහජන බැංකුව
ඉදිරිය කරා මෙහෙය වීමේ වගකීමට උර දී ඇත. හොංකොං සහ ෂැන්හයි බැංකු සමාගමේ 31
වසරක සේවා නිපූනත්වයක් ලබා ඇති ඒ මහතා ලෙබනනය, ජෝර්දානය සහ වියටිනාමය යන රටවල
එම බැංකුවේ ප්රධාන විධායක නිලධාරියා ලෙස කටයූතු කර ඇත.</p>

<p>ඡායාරෑපයේ
දැක්වෙන්නේ කෙලී මහතා වැඩ බාර ගැනීමෙන් පසූ බැංකු මූලස්ථානයේ පහළොස්වැනි මහලේ
බූදු මැදුරේ දී සෙත් පිරිත් සජ්ජායනා පිංකමට සහභාගි වීමෙන් අනතුරැව බෙල්ලන්විල
රජමහා විහාරාධිකාරි ආචාර්ය බෙල්ලන්විල විමලරතන හිමියන් විසින් පිරිත් නූල් ඒ
මහතාගේ අතෙහි ගැට ගැසූ අවස්ථාවයි.</p>

<p>අබාන්ස් සේවා නියෝජිතයන්ගේ 2 වන වාර්ෂික සමූඦව </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>අබාන්ස්
ඉලෙක්ටි්රකල්ස් සමාගමේ දිවයින පූරා විසිර සිටින තම සේවා නියෝජිතයන්ගේ 2 වන
වාර්ෂික සමූඦව පසූගියදා කොළඹ ගලධාරි හෝටලයේදී පැවැත්වීය. ලංකාව පූරා සිටින
අබාන්ස් පාරිභෝගිකයන්ට තවදුරටත් කඩිනම් උසස් සේවාවක් සැපයීම සඳහා අවශ්ය
තාක්ෂණික උපදෙස් හා දැනූම මේ සමූඦවේදී දිවයිනේ සියලුම දිස්ති්රක්ක නියෝජනය
කරමින් පැමිණ සිටි ආබාන්ස් සේවා නියෝජිතයන්ට ලබාදෙන ලද අතර පසූගිය වසර පූරා
ඕවූන් පාරිභෝගිකයන්ට ලබාදුන් සේවයට උපහාර වශයෙන් ත්යාග සහ සහතික ප්රදානය
කිරමද සමූඦවේදී සිදුවිය.</p>

<p>අබාන්ස්
සමාගමේ කළමනාකාර අධ්යක්ෂිකා අබාන් පෙස්ටොන්ජි මහත්මිය මේ සමූඦවේ මූලාසනය
ගත් අතර අබාන්ස් සමූහයේ අධ්යක්ෂකවරැන් වන ටිටෝ පෙස්ටොන්ජි, ඇලන් රාෆල්
සේවා නියෝජිත කළමනාකරැ නාලක සිරිවර්ධන මහත්වරැන් ප්රධාන අමූත්තන් වශයෙන් මෙයට
සහභාගි වූහ.</p>

<p>මූරලිගේ සහභාගිත්වය පිළිබඳ අවසන් තීරණය අද </p>

<p>ශ්රී
ලංකාවට දකුණූ අප්රිකා පරාජයේ පැල්ලම මකා ගැනීමට
එංගලන්තයට පකිස්තාන ජයග්රහණයේ ගෞරවය රැක ගැනීමට </p>

<p>ශ්රී
ලංකාව සහ එංගලන්තය අතර තරග 3 කින් සමන්විත ටෙස්ටි තරගාවලියේ ප්රථම තරගය අද (22 දා)
සිට ලබන 26 දා දක්වා ගාල්ල ජාත්යන්තර ක්රිකටි ක්රීඩාංගණයේදී පැවැත්වේ.</p>

<p>ශ්රී ලංකා
පිල සනත් ජයසූරිය විසින් මෙහෙයවනූ ලබන අතර එංගලන්ත පිල නසාර් හූසේන් විසින්
මෙහෙයවනූ ලබයි.</p>

<p>ශ්රී ලංකා
පිල පසූගියදා පැවති දකුණූ අප්රිකානූ ටෙස්ටි තරගාවලියෙන් පරාජය වී ගිලිහී ගිය ගෞරවය
නැවත ලබා ගැනීමේ අරමූණින් තරග වදින අතර එංගලන්ත පිල පකිස්තානයේදී පකිස්තාන පිල
පරදවා ලැබූ ගෞරවය රැක ගැනීමේ අරමූණින් තරග වදියි.</p>

<p>පසූගියදා
දකුණූ අප්රිකාවේදී ශ්රී ලංකාව පරාජය වූ ආකාරය එංගලන්ත පූවත්පත් දැඩි ලෙස විවේචනය
කර තිබූ අතර ශ්රී ලංකාව එහිදී තරග කළ ආකාරය දෙස බලන විට එංගලන්තය අනිවාර්යයෙන්ම
මෙවර තරගාවලිය ජයගත යූතු බව එම පූවත්පත් පවසා තිබූණි.</p>

<p>මේ
හේතුවෙන් එංගලන්ත නායක නසාර් හූසේන් ඇතුඦ කණ්ඩායමට දැඩි පීඩනයක් එල්ල වී ඇත.
එංගලන්ත පිතිකරැවන් පකිස්තානූ සංචාරයෙන් පසූ දඟ පන්දු යවන්නන්ට සාර්ථකව මූහූණ දෙන
බව සිව්දින තරගවලදී දැකගත හැකිවීමද ශ්රී ලංකාවේ අවධානයට ලක්විය යූතුය.</p>

<p>ශ්රී
ලංකාවේ අංක 1 දඟ පන්දු යවන්නා වන මූත්තයියා මූරලිදරන් ආබාධයකට ගොදුරැ වී සිටීමද
එංගලන්ත පිලට සහනයකි. එහෙත් මූරලිදරන් බොහෝදුරට ක්රීඩා කරනූ ඇතැයි ශ්රී ලංකා
කණ්ඩායම් ආරංචි මාර්ග සඳහන් කරයි.</p>

<p>මූරලිදරන්
ක්රීඩා කළහොත් ඕහූ සමග එක් දඟ පන්දු යවන්නකු (කෞෂල් ලොකුආරච්චි හෝ දිනූක්
හෙටිටිආරච්චි) ක්රීඩා කරනූ ඇත. මූරලි ක්රීඩා නොකළහොත් දඟ පන්දු යවන්නන් තිදෙනකු
ක්රීඩා කිරීමට තීරණය කර ඇති බවද වාර්තාවේ.</p>

<p>පිතිකරැ
ලැයිස්තුව තේරීමේදී ලකුණූ අතර සිටින ටී. එම්. ඩිල්ෂාන්ට මැද පෙල ස්ථානයක් දීම
ප්රශ්නයක් වී ඇත. පිතිකරැවන් ලෙස සනත්, මාවන්, කුමාර්, මහේල, අරවින්ද, රසල් ක්රීඩා
කරන බැවින් ඩිල්ෂාන්ට ස්ථානයක් ලබාදීම සඳහා පන්දු යවන්නන් 5 දෙනාගෙන් එක් අයෙකුගේ
ස්ථානයක් අහිමි කිරීමට සිදුවීම මෙම ප්රශ්නයයි. මේ අනූව ඩිල්ෂාන් මැද පෙලට එක්
වූවහොත් වේග පන්දු යවන්නෝ දෙදෙනෙකු සහ දඟ පන්දු යවන්නන් දෙදෙනකු ක්රීඩා කරනූ
ඇත.</p>

<p>එංගලන්ත
පිලද ගෆ් හා කැඩික් සමග දඟ පන්දු යවන්නන් ලෙස ගයිල්ස් සහ ක්රොෆ්ටි යෙදවීමට නියමිතව
ඇති අතර පිතිකරැවන් 5 දෙනකු සහ තුන් ඉරියව්වෙන්ම කැපී පෙනෙන මයිකල් වෝන් හා
ක්රේග් වයිිටි කණ්ඩායමට ඇතුළත් කරනූ ඇතැයි පැවසේ. ලකුණූ අතර නොසිටින ග්රැහැම්
හික්ට ස්ථානය අහිමිවීමට බොහෝදුරට ඉඩ ඇත.</p>

<p>මෙතෙක්
ශ්රී ලංකාව සහ එංගලන්තය අතර ටෙස්ටි තරග 6 ක් පවත්වා ඇති අතර ඉන් 3 ක් එංගලන්තය
ජයගෙන ඇත. තරග 2 ක ජය ශ්රී ලංකාවට හිමි වී ඇති අතර තරගයක් ජය පරාජයෙන් තොරව
අවසන් වී ඇත. ශ්රී ලංකාව සහ එංගලන්තය අතර පළමූ ටෙස්ටි තරගය 1982 දී කොළඹදී
පවත්වනූ ලදුව එම තරගයෙන් ශ්රී ලංකා කණ්ඩායම පරාජය විය.</p>

<p>මෙම
තරගයේ විනිසූරැවන් ලෙස පීටර් මැනූවෙල් (ශ්රී ලංකාව) සහ ඒ. වී. ජයප්රකාශ්
(ඉන්දියාව) කටයූතු කරන අතර තෙවැනි විනිසූරැ ලෙස අසෝක ද සිල්වා (ශ්රී ලංකාව)
කටයූතු කරනූ ඇත.</p>

<p>ප්රථම
ටෙස්ටි තරගයට සහභාගි වෙතැයි අපේකෂිත ශ්රී ලංකා පිල</p>

<p>සනත්
ජයසූරිය (නායක), මාවන් අතපත්තු, කුමාර් සංගක්කාර, මහේල ජයවර්ධන, රසල් ආනල්ඩ්,
අරවින්ද ද සිල්වා, ටී. එම්. ඩිල්ෂාන්, චමින්ද වාස්, නූවන් සොයිසා, මූත්තයියා,
මූරලිදරන්, කෞෂල් ලොකුආරච්චි හෝ දිනූක් හෙටිටිආරච්චි.</p>

<p>ප්රථම
ටෙස්ටි තරගයට සහභාගි වෙතැයි අපේකෂිත එංගලන්ත පිල</p>

<p>නසාර්
හූසේන් (නායක), මයිකල් ඇතර්ටන්, ග්රැහැම් තෝප්, මාක් ට්රෙස්කොතික්, ඇලෙක්
ස්ටූවර්ටි, මයිකල් වෝ්න්, ක්රේග් වයිටි, රොබටි ක්රොෆ්ටි, ඈෂ්ලි ගයිල්ස්, ඩැරන්
ගෆ්, ඇන්ඩි කැඩික්.</p>

<p>අනූරාධපූර දිල්රැකෂිට ආසියාවේ දෙවැනි තැන </p>

<p> </p>

<p>නවවන
ආසියානූ කියෝකුෂින් ශූරතා තරගාවලිය නියෝජනය කළ ශ්රී ලාංකික තරගකාරියක වන
අනූරාධපූර ස්වර්ණපාලි බාලිකා විද්යාලයයේ 11 වසරේ ඉගෙනූම ලබන වයි. එම්. එස්.
පී. දිල්රැකෂි සිසූවිය අධිබර පන්තිය (කාන්තා) දෙවන ස්ථානය ලබා ගැනීමෙන් අනතුරැව,
ඊට අදාළ කුසලාන ආසියානූ කියෝකුෂින් සංගමයේ සභාපති සිවාජි ගන්ගුලි මහතාගේ
බිරිඳ සෞම්යා බා ගන්ගුලි මහත්මිය අතින් ලබාගත් අයූරැ. මෙම ඉසව්වේ පළමූ ස්ථානය
කසකස්ථානයට හිමි වී ඇත. තරගාවලිය ඉන්දියාවේ කල්කටා නූවර පැවැත්වූණි. </p>

<p> </p>






</body></text></cesDoc>