<cesDoc id="sin-w-humanities-nuwana-176" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-humanities-nuwana-176.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>තොටගමුවේ
විජයබා පිරිවෙන සහ</p>

<p>කෑරගල
පද්මාවතී පිරිවෙන</p>

<p> </p>

<p>කපුරුබණ්ඩාර
ජයතිලක</p>

<p>කෝට්ටේ
යුගය සිංහල සාහිත්යයේ ස්වර්ණමය යුගය ලෙස පිළිගැනේ. අන් යුගවලට වඩා
ප්රමාණාත්මකවත්, ගුණාත්මකවත් පිරිපුන් සාහිත්යයක් මෙකල බිහිවීම මෙයට
හේතු වේ. ඒ අතර ම සාහිත්යයට විවිධ ලක්ෂණ ඇතුන්ථවීමත්, බොහෝ සාහිත්ය කෘති
නෂ්ටප්රාය නොවී පැවැතීමත් හේතු විය. පැවිදි යතිවරුන් මෙන් ම ගිහි පඬිවරුන් ද මෙකල
සාහිත්ය කරණයෙහි නියැලී සිටි බව පෙනේ. විදාගම මෛත්රෙය, තොටගමුවේ ශ්රී
රාහුල, වෑත්තෑවේ යන පැවිදි කවියෝ මෙන් ම ශ්රී රාමචන්ද්ර භාරතී, නන්නූර්තුනය
වැනි ගිහි කවියෝ ද මේ අතර වූහ.</p>

<p> </p>

<p>කෝට්ටේ
යුගයේ උසස් සාහිත්යයක් බිහිවීමට හේතුවූ ප්රධාන කරුණ නම් අධ්යාපනයේ සංවර්ධනයයි. ලොව
කුමන රටක හෝ සාහිත්ය දියුණුව දැනුමෙන් හා කුසලතාවෙන් පිරි නිර්මාණ කරුවන්ගේ
උත්සාහයේ ප්රතිඵල වේ. එවන් නිර්මාණකරුවන් බිහිවීමට පාදක වන්නේ අධ්යාපන ක්ෂෙත්රය තුළ
ඇතිවන පිබිදීම අනුවයි. මෙරට අධ්යාපන සංවර්ධනය පිළිබඳ ව සලකා බලන විට කෝට්ටේ යුගයට
හිමි වන්නේ වැදගත් තැනකි.</p>

<p> </p>

<p>මෙකල
කෝට්ටේ රාජ්යය තුළ ඉතා වැදගත් පිරිවෙන් අධ්යාපන ආයතන කිහිපයක් ම පැවැති බව
පෙනේ. තොටගමුවේ විජයබා පිරිවෙන, කෑරගල පද්මාවතී පිරිවෙන, වීදාගම
ඝනානන්ද පිරිවෙන හා පැපිළියානේ සුනේත්රා මහා දේවී පිරිවෙන මෙම පිරිවෙන් අතර
කැපී පෙනිණි. භික්ෂූන් වහන්සේලාගේ දහම් දැනුම හා විනය පැවැත්ම පිළිබඳ ඉගැන්වීම්
කටයුතුවලට ප්රධාන තැනක් දුන් මෙම ආයතනවල ඉන් බැහැර වූ ඇතැම් විෂයයන් ද ඉගැන්වූ බව
පෙනේ. මෙම පිරිවෙන් ආයතන අතර විශේෂයෙන් කැපී පෙනුණු ආයතන දෙකකි. තොටගමුවේ ශ්රී
රාහුල මාහිමිපාණන්ගේ ප්රධානත්වයෙන් පැවැති තොටගමුවේ විජයබා පිරිවෙන හා වනරතන
මාහිමිපාණන්ගේ ප්රධානත්වයෙන් පැවැති කෑරගල පද්මාවතී පිරිවෙන. මෙකල මෙරට අධ්යාපන රටාව
පිළිබඳ ඉතා වැදගත් තොරතුරු මේ පිරිවෙන් දෙකේ කටයුතු සංවිධානය දෙස බැලීමෙන්
අවබෝධ කර ගත හැකි ය. මේ පිළිබඳ ව අපට මග පෙන්වන්නේ ගිරා සන්දේශය හා හංස
සන්දේශයයි.</p>

<p> </p>

<p>ශ්රී
රාහුල මාහිමිපාණන් ප්රධාන වූ ග්රාමවාසී නිකායික තෙර නමක විසින් ගිරා සන්දේශය ලියා
ඇති අතර විජයබා පිරිවෙන් අධ්යාපන කටයුතු සංවිධානය පිළිබඳ ව එහි ඉතා අගනා තොරතුරු
අන්තර්ගත වෙයි. ආරණ්ය වාසී නිකායික වනරතන මාහිමිපාණන්ගේ ශිෂ්යයකු විසින් ලියා ඇති
හංස සන්දේශය පද්මාවතී පිරිවෙනේ අධ්යාපන කටයුතු පිළිබඳ ව වැදගත් තොරතුරු එළි
දක්වයි.</p>

<p> </p>

<p>අධ්යාපන
ආයතනයක ගුණාත්මක සංවර්ධනයට බලපාන සුවිශේෂ සාධක කිහිපයක් වේ.</p>

<p> </p>

<p>   1'  ukd l</p>

<p>   2'  fN!;sl jd;djrKh'</p>

<p>   3'  YsIH úkh</p>

<p>   4'  úIh ud,dfõ úúO;ajh</p>

<p>   5'  úêu;a bf.kqï b.ekaùï l%shdj,sh</p>

<p>   6'  wLKav we.ehSï l%uh</p>

<p>   7'  iudc iy iïnkaO;d</p>

<p> </p>

<p>මෙම
සාධක ම ඉහතකී පිරිවෙන් ආයතනවල අධ්යාපන සංවර්ධනයට බලපා ඇති බව පෙනේ. අපේ පැරණි
අධ්යාපන ක්රම පිළිබඳ ව වංශ කථාවලින්වත් සොයා ගත නො හැකි වැදගත් ඓතිහාසික
තොරතුරු ගිරාවෙන් හා හංසයෙන් හෙළි වන බව මහාචාර්ය පුංචිබණ්ඩාර සන්නස්ගල මහතා
කියයි.</p>

<p> </p>

<p>අධ්යාපන
ආයතනයක සංවර්ධනයට එහි පවත්වාගෙන යන කළමනාකරණ ක්රියාවලිය බෙහෙවින් බලපායි. රේඛීය
කළමනාකරුවා වශයෙන් ප්රධාන භූමිකාව මෙහෙය වන්නේ අධිපතියා ය. මෙම පිරිවෙන්වල
පරිවේණාධිපතිවරුන් ලෙස ශ්රී රාහුල හිමියන්ගෙනුත්, වනරතන හිමියන්ගෙනුත්
උක්ත කාර්යය මැනවින් සිදුවී ඇත. ආයතනාධිපතිගේ විවිධ භාෂා දැනුමත්, විෂය
දැනුමත්, පරිපාලන දක්ෂතාවත් මෙහි දී වැදගත් වේ. රාහුල මාහිමියෝත් වනරතන
මාහිමියෝත් සිංහල, පාළි, සංස්කෘත, ප්රාකෘත ආදි ප්රාචීන භාෂා
විශාරදයෝ ය. සිරි රහල් මා හිමිපාණන් භාෂා හයක්
දැන සිටි නිසා ම උන්වහන්සේ IâNdIdmrfïYajr kï jQy' Wkajykafia NdId yhlska
ldjH rpkd l</p>

<p> </p>

<p>අධ්යාපන  ආයතනයක 
.=Kd;aul ixj¾Okhg tys fN!;sl iïm;aj, is;a .kakdiq,q ieleiSu o fnfyúka
n,mdhs' úchnd msßfjk fuka u moaudj;S msßfjk o is;a .kakd iq,q mßirhl msysá w;r
wdh;k ìï o uekeúka ieleiqfKa úh'</p>

<p> </p>

<p>සිහිල්
දිය දහරා ඇද හැළෙන ගඟුලැලි ද, විකසිත මලින් ගහන පොකුණු ද,
පලතුරුවලින් බරවූ රුක් ගොමු ද, සරු ලෙස රෝපණය වූ රුක් ලතා ද, සුදු
වැලි අතුළ මං පෙත් හා මන්ථ ද මේ ආයතන දෙකේ ම පරිසරය අලංකාර කෙළේ ය. දාගබ්
ධර්මශාලා බුදු ගෙවල් හා සක්මන් මන්ථවලින් අලංකාරවත් වූ පරිසරය විහඟ බමර ගීයෙන් ද පෝෂිත
විය.</p>

<p>රළින් රළ බිඟුන් කෙළැ වතළ         දිමුතු ය</p>

<p>කෙළින් කෙළ පොකුණු වන තඹර     නුමුතු ය</p>

<p>පෙළින් පෙළ සුරත් නිල් මල්        මිණිමුතු ය</p>

<p>පළින් පළ විහිදි සුවඳැති මල්           වතු ය</p>

<p> </p>

<p>මේ
ගිරා සන්දේශ කවියා තොටගමු විජයබා පිරිවෙන් බිම වැනූ ආකාරයයි. හංස කවියා ද මීට නො
දෙවෙනි වන ආකාරයෙන් පද්මාවතී පිරිවෙන් බිම වනයි.</p>

<p> </p>

<p>දිමුත් වෙහෙර වට විසිතුරු මල්         වතු ය</p>

<p>එහිත් සුවඳ මල් නිති වෙයි මිණි        මුතු ය</p>

<p>මහත් තුරු මියුරු පල ගෙන නැමි       අතු ය</p>

<p>රැගත් සෙවන නොහැර ම සිටි පොල්    වතු ය</p>

<p> </p>

<p>මේ
තොරතුරුවලින් අධ්යයන කටයුතුවලට අතිශයින් උචිත මානසික සතුට වඩවන ශාන්ත පරිසරයක්
නිර්මාණය වී තිබූ බව පෙනේ.</p>

<p> </p>

<p>ශිෂ්ය
විනය අධ්යාපන ආයතනයක මනා සංවර්ධනයට බලපාන ඉතා වැදගත් සාධකයෙකි. විජයබා පිරිවෙනේත්
පද්මාවතී පිරිවෙනේත් සාමණේර, තෙර හා මහතෙර විවිධ මට්ටම්වල භික්ෂූන් මෙන් ම දෙස්
විදෙස් ශිෂ්යයන් ද වශයෙන් මිශ්රවූ ශිෂ්ය පිරිසක් අධ්යයන කටයුතුවල යෙදී සිටි බව පෙනේ. ගිහි
ශිෂ්යයන් අතර මූලික අධ්යාපනය ලද බාලකයන් මෙන් ම පශ්චාත් අධ්යාපනය ලද වැඩි හිටි ශිෂ්යයන්
ද සිටි බව පෙනේ. එහෙත් මේ හැම කෙනකු ම මහානායක හිමියන්ගේ උපදෙස් පිළිපදිමින්
නිසංසලේ තම අධ්යාපන කටයුතුවල නිරත වූ බව පෙනේ.</p>

<p> </p>

<p>විජයබා
පිරිවෙනේ ඉගෙන ගත් කුඩා සාමණේර භික්ෂූහු ශික්ෂාකමී වූහ. සත්ව කරුණාවෙන් ද පිරි සිටියහ.
ඒ බව සන්දේශයේ එන මේ පැදියෙන් පෙනේ.</p>

<p> </p>

<p>තුරුයටියෙන් හුණු පැටි ලෙහෙනුන්      රැගෙන</p>

<p>සිතු අටියෙන් සිඹ ඇඟ පිරිමැද         සෙමින</p>

<p>මල ගෙටියෙන් පැන් පොවමින්       අතිනතින</p>

<p>සිටි පැටියෙන් හෙරණුන්ගෙන් වෙයි     සොබන</p>

<p> </p>

<p>පද්මාවතී
පිරිවෙනේ ශිෂ්ය භික්ෂූන් ද බෙහෙවින් ශික්ෂාකාමී ව කටයුතු කළ බව පෙනේ. මෙන් සක කිරඹ
පිරිසිදු කර සිල් ගුණය උන්වහන්සේලා ශීල ප්රතිපදාව අනුව ජීවත් වූ බව මෙයින් පෙනේ.
එසේ ම දැඩි ගුරු භක්තියෙන් පිරී ඉතිරී ගියෝ ය. </p>

<p>''සරණ වැඳ ගුරුන් කියවා දිනෙන්        දින''</p>

<p>''දෙසරණ සිරස කර වැඳ ගුරුවරුන්      නිති''</p>

<p> </p>

<p>මේ
අයුරින් සලකා බලන විට මෙම ආයතන දෙකේ ම ශිෂ්ය විනය ඉතා යහපත් මට්ටමක පැවැති බව පෙනේ.</p>

<p> </p>

<p>අධ්යාපන
ආයතනයක ප්රධාන විෂය මාලාවක් මෙන් ම වෛකල්පික විෂය මාලාවක් ද ඇති විට විවිධ දක්ෂතා හා රුචි
අරුචිකම් ඇති ශිෂ්යයන්ට තම රිසි පරිදි විෂය තෝරා ගෙන ඉගෙනීමට හැකි ය. වෛකල්පික විෂය
අතර අනාගත ජීවිතයේ දී ප්රායෝගික ව වැදගත් වන විෂයන් ද ඇති විට ශිෂ්යයාගේ ඉගෙනීමේ
ඇති ආශාව ද වැඩි වේ. එවැනි අධ්යාපන ආයතන බෙහෙවින් ජනප්රිය ද වේ.</p>

<p> </p>

<p>විජයබා
හා පද්මාවතී පිරිවෙන්වල විෂය මාලාව පිළිබඳ ගිරා හා හංස සන්දේශවල ඉදිරිපත් වන
තොරතුරු සලකා බලන විට එම ආයතනවල විධිමත් හා සමබර විෂය මාලාවක් ක්රියාත්මක වූ බව
පෙනේ.</p>

<p> </p>

<p>අභිධර්ම
විනය හා සූත්ර මෙම ආයතනවල ශිෂ්ය භික්ෂූන්ට අනිවාර්ය වූ බව පෙනේ. එසේ ම ව්යාකරණය ද
අනිවාර්ය විය. ඒ සමග ම සිංහල පාළි හා සංස්කෘත භාෂාවන් ද අනිවාර්ය විෂය විය. මේ හැර
වෛද්ය ශාස්ත්රය, නක්ෂස්ත්රය, අර්ථ ශාස්ත්රය, කාව්ය නාටක හා ඉතිහාස පුරාණ
ආදිය ද තම තමන්ගේ රුචි කම් මත තෝරා ගෙන අධ්යයනයේ යෙදුණු බව පෙනේ. සාමණේර
හිමිවරුන් සන්, සඳ අත නො හැර ව්යාකරණ විෂය හැදෑරූ බව පෙනේ. පෙළ පොතක් වශයෙන්
සිදත් සඟරාව හදාරන්නට ඇතැයි සිතිය හැකි ය. සිදත් සඟරා කතුතුමා පොත ආරම්භ කරන්නේ
එබැවින් සන් සඳ ලිඟු විබත් සමස් පියවී යනුවෙනි. විජයබා පිරිවෙනේ භික්ෂූන් වහන්සේලා
සූත්ර අභිධර්ම හා විනය ග්රන්ථ හැදෑරූ අතර බමුණෝ වේද ග්රන්ථ හදාළහ. විජයබා පිරිවෙනේ
ඇතැම් භික්ෂූන් වහන්සේලා සූර්ය සිද්ධාන්ත නම් නැකැත් ශාස්ත්රය හැදෑරූ බව ද පෙනේ.
උන් වහන්සේලා නක්ෂස්ත්ර පෙළ පොතක් වශයෙන් සූර්ය සිද්ධාන්තය හදාරන්නට ඇති බව
සිතිය හැකි නමුත් ඒ එක් ග්රන්ථයක් නොව පෞලිය, රෝමක වාශිෂ්ට සෞර හා
පෛතාමන යන නැකත් න්යායන් හදාරා ඇති බව රැ. තෙන්නකෝන් මහතා කියයි.</p>

<p>විජයබා
පිරිවෙනේ සිසුන් වෛද්ය ශාස්ත්රය හැදෑරූ බව සඳහන් වේ. සමහර විට මූලික වෛද්ය කටයුතුවල
නිරත වූ ග්රාමීය වැඩිහිටියන් මෙහි දී වැඩිදුර අධ්යයනයේ යෙදුණහ.යි සිතිය හැකි ය. විජයබා
පිරිවෙන විවෘත උසස් අධ්යාපන ආයතනයක් වශයෙන් ද කටයුතු කොට ඇති බවට මෙය ද සාධකයකි.</p>

<p> </p>

<p>තවත්
සමහරු අර්ථ ශාස්ත්රය උගත් බව සඳහන් වේ. මේ කාර්යයේ යෙදුණේ භික්ෂූන් ද නැතහොත්
ගිහියන්දැ.යි නිශ්චිත ව කිව නො හැකි ය. ඔවුහු කෞටිල්ය විසින් ලියන ලද අර්ථ ශාස්ත්රය
පෙළ පොතක් වශයෙන් භාවිත කරන්නට ඇත. තවත් සමහරු ජන්දස් අලංකාර ඉගෙන ගත් අතර
ඔවුන් පෙර කවීන් ප්රකාශ කළ න්යායයන් නො පිරිහෙළා උගත් බව ද කියැවේ. භාමහ දණ්ඩි
වැනි සංස්කෘත ආලංකාරිකයන්ගේ පොත පත සේ ම සියබස්ලකර, එන්ථ සදැස් ලකුණු වැනි
සිංහල පොත් ද ඔවුන් පරිශීලනය කළා විය යුතු ය.</p>

<p> </p>

<p>මොවුහු
සංස්කෘත සිංහල මාගධ හා දෙමළ බසින් නාටක ලියූ බව ද සඳහන් වී ඇත.</p>

<p> </p>

<p>මේ
අයුරින් විජයබා පිරිවෙනේ විෂය මාලාව හා පෙළ පොත් භාවිත කැරුණු අතර පද්මාවතී
පිරිවෙනේ ද මීට සමගාමී ව විවිධ විෂයන් හා පෙළ පොත් භාවිත වූ බව පෙනේ. පද්මාවතී
පිරිවෙනේ විෂය මාලාව හා පෙළ පොත් මූලික වශයෙන් බුදු දහමට නැඹුරු විය. හෙරණ සික
කියති හෙරණෝ තැනින් තැන. යන සඳහනෙන් සාමණේර භික්ෂූන් සාමණේර ශික්ෂාව ප්රගුණ කළ
බව පෙනේ. මෙය උපසම්පදාව අපේක්ෂා කළ සාමණේර භික්ෂූන් පරිශීලනය කළ පෙළ
පොතක් බව ශාස්ත්රාචාර්ය ඩී. සී. ගම්මන්පිල මහතා කියයි. තවත් භික්ෂූන් පිරිසක් මුන්සික
සිකවළද කියැ වූහ. මෙය ක්රි. ව. 9-10 සියවස්වල ලියන ලද සිඛවළද හා සිඛවළද
විනිස විය යුතු ය.</p>

<p> </p>

<p>''රුවන්
තියා කාවිසි මුනි තුන් දොරින් සකසා වැඳ උපසපුව ලද පැවිජ්ජා පටන් හික්මිය යුතු සරිත්
වරිත් සිඛ සැකෙවින් කියනමේ'' යනුවෙන් දැක්වෙන එහි ආරම්භක පාඨයෙන් පෙනෙන්නේ එය
උපසම්පදා වූ භික්ෂූන්ගේ විනය ආරක්ෂාව සඳහා ලියැවුණක් බවයි. මහා වග්ග හා චූල වග්ග
යන විනය ග්රන්ථවල එන විනය නීති සංග්රහ කිරීමක් ලෙස මෙය දැකිය හැකි ය.</p>

<p>උපසම්පදා
වූ සමහර භික්ෂූහු නිසබණ හැදෑරූහ. නිසබණ උපසම්පදාවෙන් පස් වසරක් යන තුරු ආචාර්ය
උපදෙස් ඇති ව හැදෑරිය යුත්තකි. උපසම්පදාවෙන් පසු ව ධර්ම විනය යහපත් ලෙස හැදරූ
භික්ෂූනට තෙර පදවිය ලැබිණ. පද්මාවතී පිරිවෙනේ ඉගෙන ගත් සමහර භික්ෂූහු මේ සඳහා
සූදානමක් වශයෙන් තෙර බණ උගත්තෝ ය.</p>

<p> </p>

<p>මෙ
වැනි ධර්ම විෂයන් හැරුණු විට විජයබා පිරිවෙනේ මෙන් මෙහි ද විකල්ප විෂයයන් කිහිපයක් ද විය.
ඒ අතර ජන්දස්, තර්ක හා ව්යාකරණ ප්රධාන විය. වනරතන හිමියන්ගේ ආකල්ප හා
පද්මාවතී පිරිවෙනේ වැඩ පිළිවෙළ දෙස බලන විට ජන්දස් අලංකාර ආදී විෂයන් පැවිදි
ශිෂ්යයන් උගත්තේ ද නැතහොත් ගිහි ශිෂ්යයන් උගත්තේ දැ.යි නිශ්චිත ව කිව නො හැකි ය.</p>

<p> </p>

<p>මේ
තොරතුරුවලින් මෙම අධ්යාපන ආයතනවල ආගමික විෂයන් මෙන් ම ප්රායෝගික ලෞකික විකල්ප
විෂයන් ද ඉගැන්වූ බවත් එසේ ම ප්රායෝගික විෂය මාලාව අතින් විජයබා පිරිවෙන පද්මාවතී
පිරිවෙනට වඩා පෙරටුව සිටි බවත් පෙනේ.</p>

<p> </p>

<p>භික්ෂූන්
වහන්සේලාත් ගිහියනුත් මෙම ආයතනවල ඉගෙනගත් ආකාරයත් විෂය මාලාව හා පෙළ පොත්
පිළිබඳ ව පැවැති සංවිධානාත්මක ස්වභාවයත් පිළිබඳ ව සලකන විට ඉතා විධිමත් වූ
ඉගෙනුම් ඉගැන්වීම් ක්රියාවලියක් ක්රියාත්මක වූ බවත් කිව යුතු ය.</p>

<p> </p>

<p>සාමණේර,
උපසම්පදා තෙර යන මට්ටම් පිළිබඳ ව කියැවෙන තොරතුරු අනුව මෙම ආයතනවල විධිමත් ඇගැයීම්
ක්රමයක් ද පැවැතිණි. ශ්රී රාහුල හා වනරතන වැනි ධර්මධර, විනය ධර බහුශ්රැත පඬි
හිමිවරුන්ගේ දැඩි අධීක්ෂණය යටතේ මෙම ඇගැයීම් සිදු වී ඇත.</p>

<p> </p>

<p>මෙම
ආයතනවල සංවර්ධනය උදෙසා නො මද ජනතා සහභාගිත්වයක් ද විය. අධ්යාපනයේ යෙදී සිටි
මෙතරම් විශාල භික්ෂූ පිරිසකට සිව් පස දානය පිරිනමන ලද්දේ බැතිමත් ජනතාව විසිනි. විසිතුරු
ගම් පියෙස් මැද මේ අයතන පිහිටා තිබූ බව ගිරා සන්දේශයේ හා හංස සංදේශයේ එන
තොරතුරුවලින් ද පෙනේ. මේ ගම් බෙහෙවින් සරුසාර ව පැවැති බව ද ජනතාව මෙම ආගමික
ස්ථානවල පුද පූජා සඳහා නිරන්තරයෙන් සහභාගී වූ බව ද පෙනේ.</p>

<p>විජයබා
පිරිවෙන පිළිබඳ ව ගිරා සන්දේශය පවසන තොරතුරු හා පද්මාවතී පිරිවෙන
පිළිබඳ ව හංස සන්දේශය පවසන තොරතුරු අනුව බලන විට එම පිරිවෙන් දෙක කෝට්ටේ
යුගයේ පැවැති සෙසු පිරිවෙන් හා අධ්යාපන ආයතනවලට මග පෙන් වූ ශ්රේෂ්ඨ විද්යාස්ථාන බව
කිව යුතු ය. එසේ ම එකල රටේ අධ්යාපන ප්රගතිය, විෂය මාලාව පෙළ පොත් ඇගැයීම්
ක්රම ආදිය පිළිබඳ වටිනා තොරතුරු සපයන අතර ම එකල විශිෂ්ට සාහිත්යයක් බිහිවීමට ද හේතු
විය.</p>

<p> </p>






</body></text></cesDoc>