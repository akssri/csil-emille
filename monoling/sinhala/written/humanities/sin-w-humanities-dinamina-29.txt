<cesDoc id="sin-w-humanities-dinamina-29" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-humanities-dinamina-29.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>මහලුවීම ජයගත හැකිද?</p>

<p> </p>

<p>       ලෝකය මහලු
වේයයි විශ්වාසයක් ඇත. එහි අදහස නම්, ලෝකයේ සෑම රටකම පාහේ සිය ජනගහන
පිරමීඩයට අයත් තරුණ ජනගහනයට වඩා වැඩියෙන් වැඩිහිටි ජනගහනය ඉහළ යමින් පවතින බවය. ඒ
නව ඕෂධ හා තාක්ෂණය, සංක්රාමීය හා සංක්රාමීය නොවන රෝග වැලැක්වීමේ හැකියාව,
ආහාර, පෝෂණය, හා ස්වස්ථතාවය යන ක්ෂේත්රවල දියුණු බව, එන්නත් ක්රම
ආදී බොහෝ සාධක රැසක සංකලනයක් නිසා උදාවූ තත්ත්වයකි. </p>

<p> </p>

<p>       එහෙත් මහලු
වයසට පත්වූවන්ගේ සංඛ්යාව ඉහළ දීමීමට හේතුවන තවත් තත්ත්වයක් ඇත. මෙය ඇත්තටම මහලු
වයසට පත්වූ සංඛ්යාව ඉහළ දීමීමේ උත්සාහයක්ද නොයෙසේ නම් තාරුණ්ය වැඩි කාලයක් රැකගැනීමේ
උත්සාහයක්ද යන්න එකඑල්ලේ පැවසිය නොහැකිය. කෙසේ වෙතත් මේ පිළිබඳව පර්යේෂණ
සිදුකරන විද්යාඥයන් කියන්නේ මිනිසා සිය ආයුෂ භුක්ති විදීමේ අන්ත මායිමට තවමත් ආසන්නව
නොමැති බවය. එසේ නැතහොත් පරමායුෂ නොවිද ඇති බවකි. </p>

<p> </p>

<p>       වයස්ගත වීම
පිළිබඳ මෙම නව පර්යේෂණ ආරම්භ වූයේ වර්ෂ 1961 දී පමණය. එහි මූලිකයා වූයේ සෛල
ජෛව විද්යාඥයකු වූ ලෙනාඩ් හේලික්ය. ඔහු විසදීමට තැත්කලේ සෛල තමන්ට පොට වරද්දා ගෙන
මුලු ශරීරයම අගාදයකට ඇද ගනිද්ද? එසේ නැත්නම් වයස - සම්බන්ධ පරිහානිය නොතිබෙන්නට
සෛලයක සදාකාලික ජීවිතයක් පැවතිය හැකිද? යන ගැටලු යුගලය ය. </p>

<p> </p>

<p>       Tyq ish
ridhkd.drfha mÍCIK ;=, l  20-30
jdrhla fn§ hdfuka miqj ffi, úhm;a jQ njla fmKsk' fya,slaf.a fï fidhd .ekSu
Tiafia ;rl l </p>

<p>       කාලය එලැඹි විට
සෛල කි්රයාකාරීත්වය නවතා දීමීම එමගින් සිදුවෙයි. මෙම ඔරලෝසුව සොයා ගොස් කාලය
ගෙවීයාමේ සීනුව නාදවීම පමා කළ හැකිද? බොහෝ ජීවීන් මෙන්ම සෛලද ශක්තිය පරිවෘත්තිය
කරද්දී අපද්රව්ය නිපදවයි. </p>

<p> </p>

<p>       මෙම
කි්රයාවලියේදී නිපදවෙන එක් කරදරකාරී අපද්රව්යයක් නම්, අමතර ඉලෙක්ට්රෝනයක් සහිත
සාමාන්ය ඔක්සිජන් අනුය. තමන් සතු වූ මෙම විද්යුත් අසමතුලිත තාව යටපත් කර ගැනීම උදෙසා
මෙම අනුව වෙනත් අනුහා සම්බන්ධ වීමට දඟලන ලෙසක් පෙනේ. මුලු ජීවිත කාලය තුලදී සිදුවෙමින්
පවතින මෙම කි්රයාවලිය නිසා සෛලවලට හානි සිදුවීමට ඉඩ තිබේ. </p>

<p> </p>

<p>       මෙම විනාශයේ
ප්රතිඵල පරාසය තුළ පිළිකා වැනි දරුණු තත්ත්වයන් ගේ සිට සම රැලිවැටීම සහ
ආතරයිට්ස් හන්දිවේදනා, හා ප්රදාහය දක්වා වන සාමාන්ය වයස්ගත වීමේ රෝගද ඇතිවෙයි.
</p>

<p> </p>

<p>       මැතක සිට
පොෂක විද්යාඥයෝ බොහෝමයක් පලතුරු හා එලවලු බහුල ආහාර වේලක් ගැනීමට </p>

<p>ජනතාව පොළඹවති. ඔවුන්ගේ තර්කය නම් මෙම ආහාර තුළ
අඩංගු ප්රති ඔක්සිකාරක පී්රරැඩ්කාල්ස් නැතිනම් නිදහස් අංශූ කොටස් ගිලදමා ඒවා සිරුරෙන්
බැහැරට ගෙන යාමට දායක වන බවය. </p>

<p> </p>

<p>       කෙසේ වෙතත්
පර්යේෂණ වාර්තා විමර්ශනය කරන විට පෙනෙනුයේ මෙම ප්රති ඔක්සිකාරක සතුව සුභවාදී ප්රතිඵල
පමණක් සතුනොවන බවය. ඒවා බහුලව ගත් විටෙක පිළිකාවක් සුවපත් කිරීමට හේතුවන අතර
තවත් විටෙක පිළිකා තත්ත්වය උග්රකිරීමට ද එය දායකවෙයි. කෙසේ වෙතත් වර්ථමාන
පිළිගැනීම වී ඇත්තේ සලාද-පලතුරු හා එලවලු ආහාරයට ගැනීම දිගු දිවියකට මඟ සලසන බවය.
</p>

<p> </p>

<p>       එහෙත්
බොහෝ විද්යාඥයන් දීනට වඩාත් උනන්දුවක් දක්වනුයේ සෛල පරිවෘතියේ දී සිදුවන වෙනත්
අතුරු කි්රයාකාරී තත්ත්වයන් වෙතය. ග්ලයි කොසයිලේෂන් ලෙස විද්යාඥයන් හදුන්වන මෙය ආහාර
පිසින කෝ්කීන් විසින් හදුන්වන්නේ රෝස්වීම හෙවත් දුඹුරු පැහැගැන්වීම ලෙසය. කුකුලු
මස්,පාන්, කරමල් පුඩිං වැනි ආහාර වර්ග රත්කළ විට ප්රෝටීන් හා සීනි
බැදීමක් සිදුවන අතර ඒ හේතුවෙන් පෘෂ්ටය දුඹුරු පැහැ ගැන්වී ආහාරයට මෘදු හා ඇලෙන සුන්ථ
බවක් ලබා දෙයි. ජෛව රසායන විද්යාඥයන් 1970 දශකයේදී පළ කළ මතයක් වූයේ මෙම
ප්රතික්රයාව දියවැඩියාවෙන් පෙලෙන පුද්ගලයන් තුලද සිදුවනවා විය හැකිබවය. සීනි සහ ප්රෝටීන
බන්ධනය වූ විට ඒවා වෙනත් ප්රෝටීන්ද ආකර්ශනය කර එතැන ඇලෙන සුන්ථ මකුලු දීලක් බදු ජාලයක්
සාදයි. එහි ප්රතිඵලයක් ලෙස හන්දි තද ගතියක් ගැනීම, ධමනි අවහිර වීම, ඇහේ
ලෙන්සය වැනි පටක වල පැහැදිලි භාවය අඩුකිරීම මගින් සුදු ඇතිවීම වැනි තත්ත්වයක් උදා
කරයි. දියවැඩියා රෝගීහු මේ සියලු තත්ත්වයන්ගෙන් පෙලෙති. </p>

<p> </p>

<p>       එසේම වයස්ගත
වූවන්ද, ඒ පරිදිමය. දියවැඩියාව නොවන වයස්ගත පුද්ගලයන් තුළ ද සීනි පරිවෘතියේදී
ග්ලයිකොසයිලේෂන් කි්රයාවලිය සිදුවේද? එසේ සිදුවන්නේ නම්, ඒ ඉතා සෙමෙන් සිදුවන
කි්රයාවලියක් බව අධ්යයනය පෙන්වා දෙයි. </p>

<p> </p>

<p> </p>

<p> </p>

<p>       මෙම ඇලෙන
සුන්ථ ග්ලයි කෝසයිලේෂන් මන්ඩිවලට විද්යාඥයන් උචිත නමක්ද තබා ඇත. ඒ නම්, ඒජ්
යන්නය. එම නම සාදා ඇත්තේ ඇඩ්වාන්ස්ඩ් ග්ලයිකෝසයිලේෂන් ඒන්ඩ් ප්රොඩක්ට්ස් යන්න
කෙටි කිරීම මගිනි. දීනටමත් නිව්යෝර්ක්, මැන්හැසටේ හි වෛද්ය පර්යේෂණ පිළිබඳ
පිකවර් ආයතනයේ පර්යේෂකයන් පීමාගඩින් නම් ඕෂධයක් ඔස්සේ පර්යේෂණ මෙහෙයවයි. ඒජ් හා
ඒ වටා ඇති ප්රෝටීන අතර පවත්නා සම්බන්ධතාවය බිඳලීම සඳහා ද්රාවකයක් ලස පීමාගඩින් කි්රයාකරන බව විශ්වාසයයි.</p>

<p> </p>

<p>       ආයුෂ දිර්ඝ
කිරීම හේතු කාරක වන තවත් විකල්ප කි්රයා මාර්ගයක් ලෙස සෛල තුළ පෝෂන සැලසුම් කරණය අඩුකිරීමය
පෙන්වා දෙයි. අධ්යනයන්ට අනුව 30-40 ට අඩු කැලරි ප්රමාණයක් ලබාදුන් මීයන්ගේ ආයුෂ කාලය
40 න් වැඩි වූ බව පෙනී යයි. මෙය මිනිසා හා සම්බන්ධව ගත්කළ දිනකට කැලරි 1500 ක් පමණක්
ගැනීම ආයුෂ වසර 30 න් පමණ දීර්ඝ කර ගැනීමකි. මෙම සන්සිද්ධිය සිදුවන්නේ කෙසේද
යන්න පිළිබඳව පූර්ණ අවබෝධයක් තවම විද්යාඥයන් තුළ නොමැත. එහෙත් මෙරිලන්ඩ් හි
වයස්ගත වීම පිළිබඳ ජාතික ආයතනයේ අනුක කායික විද්යාඥයකු වන ජෝ්ර්ජ් රොත් මේ
සංසිද්ධිය යම් විග්රහයක යෙදීමට උත්සාහ දරා ඇත. කැලරි සීමා කළ විට ශරීර උෂ්ණත්වය
සෙල්සියස් අංශක එකකින් පමණ පහළ වැටේ. උෂ්ණත්වය පහළ වැටීම යනු පරිවෘත්තීය
කි්රයාවන් සිදුවන වේගයද අඩුවීමකි. එමගින් ආහාර සැකසුම් කරණයද පහල බසියි. සත්වයා වර්ධන
මාතයකට මාරු වෙයි. ඔවුනට ලැබෙන්නේ අඩු කැලරි ප්රමාණයකි. එනිසාම ඔවුන් දවන්නේද අඩු
කැලරි ප්රමාණයකි. මෙලෙස කැලරි පාලනයක් ඇති කිරීම අපගේ ආයු කාලය අවුරුදු 80 ඉක්මවා
යවයි. ජෝර්ජ් රොත් කියන්නේ අවු:100 ඉක්ම වූ මහත පුද්ගලයකු දීකීම පහසු කටයුත්තක්
නොවන බවය. </p>

<p> </p>

<p>       මේ අතර
හේලික් පිළිතුරු නොදුන් ප්රශ්නයක් ද වෙයි. ජෛව විද්යාඥයන් කලක සිටම සෛල මරණයට
හේතුවන ජානය සොයා පර්යේෂන කළද කිසිවක් සොයාගත නොහැකිවිය. එසේ වර්ණදේහ තුඩක
පවතින කිසියම් කාර්ය භාර්යක් නොමැති ප්රදේශයක් විද්යාඥයාට දක්නට ලැබී ඇත. </p>

<p> </p>

<p>       ටොලමිරේ ලෙස
හැදින්වෙන මේ ප්රදේශය සපත්තු ලේසයක කෙලවර ඇති ප්ලාස්ටික් ආවරණයක් වැනි වූවකි.
සෛල බෙදෙන සෑම වාරයකදීම නිපදවෙන නව සෛල තුළ ඇති ටෙලොම්රේ ප්රමාණය ක්රම ක්රමයෙන්
අඩුවෙයි. හේලික් දීක්වූ සීමාව එනම් වසර 50 පමණ බෙදී යාමෙන් පසුව ටෙලාම්රේ ප්රමාණය
තිතකට සීවාවෙයි. මෙසේ ටෙලමේරේ ප්රමාණය අඩුවීම සිදු නොවන සෛල වනුයේ ශුකාණු සෛල හා
පිළිකා සෛල පමණය. මෙම සෛල සතු ගුණාංගයක් වනුයේ පනස්වාරයක් නොව දහස්වාරයක්
වුව බෙදී යාමට පවත්නා හැකියාවය. </p>

<p> </p>

<p>       බර්ක්ලි හි
කැලිෆෝර්නියා සරසවියේ කැරොල් ග්රොයිඩර් සහ එලිසබෙත් බ්ලැක්බර්ග් යන අණුක ජෛව
විද්යාඥයන් දෙපළ 1984 දී මේ මෙලොම්රේ ආරක්ෂිත එන්සයිමයක් සොයා ගනු ලැබීය. එය
ටෙලොමෙරේෂ් ලෙස නම්කරනු ලැබීය. එයට වසර පහකට පසුව යේල් සරසවියේ ගේ්රග් මොරින්ද මෙම
ද්රව්ය පිළිකා සෛලවල තිබී සොයාගනු ලැබීය. ඇත්තටම සදාකාලිකත්වයේ රසායනිකය ලෙස මෙය
නම් කරනු ලැබීම වරදක් නොවේ. ජෙලෝන් ටෙලොමෙරස් නිෂ්පාදනය ඇති කරන ජානය සොයා
යාමයි. එසේ කළ හැකි වුවහොත් පෙත්තක් හෝ සෛල ප්රතිකර්මයක් මගින් විශේෂිත
ප්රදේශවල වයස්ගත වීම් පාලනය කළ හැකි වේයයි ඔහු කියයි. </p>

<p> </p>

<p>       ජාන තාක්ෂණය
තුලින් වයස්ගත වීම පවා කිරීමට ගන්නා උත්සාහයන් රසායනාගාර මට්ටමේ දී බොහෝ සේ ඵලදරන
බව පෙනේ. මොන්ටි්රයල් හි සරසවියේ සිග්ප්රයිඩ් හෙකිම් ගේ ජාන පර්යේෂණයකදී රසායනාගාර
තුළ පනුවන්ට දින 75 ක ජීවන කාලයක් ලබාදීමට හැකිව තිබේ. රසායනාගාරයෙන් බැහැරව
ස්වාභාවිකව උන්ට විසිය හැක්කේ දින 9 ක් පමණය. හෙකිම් මේ සාර්ථක තත්ත්වය ලබා ඇත්තේ
දීර්ඝායුෂ විඳ පනුවන් දෙමුහුන්කරණය තුලින් නව පරපුරේ ආයුෂ වැඩිකිරීමේ පියවර මගිනි.
ඒ අතරට මේ සතුන්ගේ වර්ණදේහ පිරික්සා වයස්ගත වීම හා සම්බන්දව එක් ජානයක්ද සොයා
ගැනීමට ඔහු සමත්විය. එහෙත් ඔහු ඔවුන්ගේ ජීවි ඔරලෝසුව සොයාගෙන ඇත.</p>

<p> </p>

<p>       මෙවැනි ජාන
මිනිස් සිරුර තුළද ඇත. එහෙත් ප්රශ්නය නම්, මේ සියලු ජාන සොයාගැනීමය. ජාන
7000 ට වඩා වයස්ගත වීම හා සම්බන්ධ බව විද්යාඥයාගේ විශ්වාදයයි. මේ 7000 ම ජාන තාක්ෂණිකව
පාලනය කිරීම කළ නොහැකි තරම් අසීරු කටයුත්තකි. ඔරලෝසු 7000 ක් ආපසු කරකැවිය
යුතුව ඇත. </p>

<p> </p>

<p>       වයස්ගතවීමේ
කි්රයාවලියට මැදිහත්වීමට තවමත් අසීරුවුවත්, මෙහිදී දත් දීනුමින් රෝග පාලනයට
පර්යේෂකයෝ කටයුතු කරති. </p>

<p> </p>

<p>       WodyrKhla f,i
fcfrdka ys m¾fhaIlhka fgf,dfufria 
wd¾'tia'à' tkaihsuh fhdodf.k ms </p>

<p>       මහලුවීම
වලක්වාලීම සඳහා වන පර්යේෂණ වල නියැලී විද්යාඥයන් ගේ අදහස සදාකාලික ජීවනය අපේක්ෂා
කළ හැකි පසුබිමක අප තවමත් නොසිටියත්, පුද්ගලයකු හට, අවුරුදු 120 ක ආයු
කාලයක් උරුම කිරීම අපහසු නොවනු ඇති බවයි. සැතුවාට වඩා දශක 4 ක් 5 ක් වැඩිපුර විසීමට
ලැබෙන අවස්ථාව වුව සුන්ථ පටු ජයග්රහණයක් නොවේ.</p>

<p> </p>

<p>නවයුගය 1998 ජූනි</p>






</body></text></cesDoc>