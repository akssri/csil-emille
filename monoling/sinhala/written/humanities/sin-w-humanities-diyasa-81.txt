<cesDoc id="sin-w-humanities-diyasa-81" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-humanities-diyasa-81.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>රොහාන්
පෙරේරා
</p>

<p> </p>

<p>ඒ
සම්ප්රදාය ඇතුළෙ ම මාක්ස් කියන මනුස්සයා (හෝ කව්රු හරි?) මුන්ථ මිනිස් සංහතිය ම
තමන්ගේ පවුල වශයෙන් සංජානනය කර ගත්තා ම අනිවාර්ය අහඹුව තමයි ප්රමාණාත්මක ව තිබුණ
දයලෙක්තික සංජානනය ගුණාත්මක තත්ත්වයකට පත් වෙන එක. මිනිස් සංහතියේ ම පේ්රමයෙන්
ඉපදෙන මේ වාගේ බිලින්දකුගේ ප්රසූතිය ගොඩක් වේදනාකාරී ඇත.ි  ඒ වේදනාවේ අඳෝනාවල් තමයි ''ෆොයබාහ් තීසීස"
කියන්නේ. හේගල්ට ආභරණයක් වුණ දයලෙක්තික සංකල්පය මිනිස් භාවිතය ඇතුළෙ
සංජානනයක් වුණා ම ඒකෙ අසීරුව (බෝම්බ ගතිය) මාක්ස්ට දැනෙන්න ඇති. මොකද මේ
ප්රසූතියෙන් පස්සෙ මූලික ලක්ෂණය තමයි එයා (මාක්ස්) මේ වචනය පාවිච්චි කරන්නත් බය වෙන
එක. ඊට පස්සෙ ඉඳලා හිටලා එයා ඒක විද්යාත්මක දයලෙක්තිකය කියලා හර්න්වන අවස්ථා විතරයි
තියෙන්නේ. එහෙත් මාක්ස් මුහුණ දුන්න මේ විපර්යාසයට එංගල්ස් මුහුණ දුන්නා කෙසේ වෙතත්
දැන  හිටියා ද කියලා වත් සැක හිතෙන මට්ටමට
එයා දයලෙක්තිකය ගැන විකාර ලිව්වා. දයලෙක්තික භෞතිකවාදය කියන ආගමික සංකල්පය ගොනු
කෙළේ ප්ලෙහානොව්. ලෙනින් මැරිලා හරියට අවුරුද්දකින් එංගල්ස්ගේ ලිපි එකතුව තනි
කෘතියක් හැටියට රුසියානු රාජ්යය මැදිහත් වීමෙන් වැපිරුවා. දයලෙක්තිකය ගැන ඉතිහාසයේ දීදයලෙක්තික
සංජානනය" ගොනු වෙන තාක් කල් දේවල්වල පිළිවෙළ සාරාංශ විදියට මෙහෙම ගොනු
කරන්න පුන්ථවන්. </p>

<p>01. 1845 දී මාක්ස් ෆොයර්බාහ්  තීසීස ලියන කොට එයාට මැදිහත් වුණු දයලෙක්තික
සංජානනය ගැන එයාට සවිඥානික වෙන්න පුන්ථවන් වෙන්නේ දී ඥාන විභාගාත්මක බාධාවන්" (අල්තුසර්
පාවිච්චි කරපු දීඥාන විභාගාත්මක ඛණ්ඩනය" සංකල්පයයි මේකයි දෙක ම මුලින් ම මුද්රිත ව පාවිච්චි
කරලා තියෙන්නේ එක ම ප්රංශ චින්තකයා, ගස්තොං බශ්ෂෙලාර්) ගණනාවක් මැදින්
විතරයි. දයලෙක්තිකයක් ගැන සංකල්පයක හාංකවිසියක් නැති  එතැන ඒ දැඟලිල්ල මතු වෙලා තියෙන්නේ වෙන ම සංකල්ප ගණනාවකින්.</p>

<p>02. 1858. 01. 06 මාක්ස් එංගල්ස්ට යවන ලිපියක මෙහෙම
සඳහන් වෙනවා. දීතව දුරටත් මට මාර ගොනු කිරීම් ටිකක් මුණ ගැහෙමින් පවතිනවා. නිදසුනකට
ලාභය ගැන න්යාය මේ තාක් තේරුම් කළ විදිය මම සම්පූර්ණයෙන් ම විනාශ කරලා ඉවරයි. </p>

<p>   uu ta
fjkqfjka mdúÉÑ l jqfKa" ;kslr wyUqjlska jdf.a uu kej;
n,dmq fya.,af.a ;¾lfhkqhs' tfyu jqfKa fya.,af.a fj¿ï .Kkdjla f.%hs,s.%dkag yïn
fj,d ug ;E.s l  fidhd .;a;d ú;rla fkdfjhs"  ñ:HdlrKhka lrmq ;d¾lsl .;slh fj; fmdÿ
mdGlhdg   lrk fldg yßhg u yß' ^f*da
udlaia  -  msgq  j 33&amp; fï isÿ ùu
;sfhkafka  ixhqla; m%d.aOkh ^lD;sh&amp;
Ndú;hg udlaia msg;a fjk fldg u hs' </p>

<p>03. ප්රාග්ධන
භාවිතයේ ඊළඟ කඩ ඉම මාක්ස් එංගල්ස්ට ලියන්නේ මෙහෙමයි. (1858. 04. 02 ) දී
ප්රාග්ධනය විසින් වැඩවසම්  යනාදිය හා .....
, දීඉඩම් දේපළ " මත කි්රයා කිරීමේ ප්රතිඵලයක් වශයෙන් දීඉඩම් දේපළ"වල  නවීන ආකෘතිය ඇති වුණ නිසා ප්රාග්ධනය ඉඳන්  වැටුප් ශ්රමය දක්වා පරිවර්තනය  දයලෙක්තික විතරක් නෙවී, ඓතිහාසිකත්
වෙනවා." (එකතු කළ කෘති - වෙන්ථම 40 - පිටුව 298) (අවධාරණය අපේ) මේ අනුව
පවුලේ අනාගතය සමාජ භෞතිකයක් එක්ක සංස්ලේෂණය කිරීමෙන් මාක්ස්ගේ සංජානනයට පවුලේ
අනාගත විද්යාවක ප්රවේශයක් ලැබෙනවා.</p>

<p>04. ඊට පස්සේ
මාක්ස්ගේ මේ සටහන් ටික කියවන එංගල්ස්ගේ ප්රතිචාරය වෙන්නේ මේ වාගේ එකක්. (1858.
04. 09)</p>

<p>  දීපළමු කොටසේ අර්ධය ගැන ඔයාගේ වියුක්තයන්
හදාරන්න ගිහිං  මාව මාර විදියට අභ්යාසයට
භාජනය වුණා.  ඒක අනිවාර්යයෙන් ම අතිශය
වියුක්තයි - ඒකෙ සාරාංශකරණයෙන් බැලූවත් එහෙමමයි, - අනික දයලෙක්තික පරිවර්තනයන්
හඳුනා ගන්න මට තදින් හොයන්න සිද්ධ වුණා. මොකද දැන් සියලූ ම වියුක්ත තර්කයන් ගැන මට
සම්පූර්ණ පිටස්තර කමක් දැනෙන නිසා." </p>

<p>  (එකතු කළ කෘති වෙන්ථම 40 - පිටුව 304)
(අවධාරණය මුල් කෘතියේ මයි.) </p>

<p> 05.   idudchSh mjq,hs kHIaál mjq,hs w;f¾ wd;;sh
ikao¾N lr .;a;= udlaiaf.a YÍrhhs fckSf.a YÍrhhs ñksia ixy;sfha wÆ;a .;slhla u;=
lrkjd' ta ;uhs mjqf,a wkd.;jdoh we;=f  .=Kd;aul ;,fha § fckSf.a Y%uh wd.ñl fj,d" udlaiaf.a Y%uh
úoHdjla fjkjd' laIKsl m%;sM,h fjkafka fckS udkisl j wikSm fj,d udlaia ldhsl j
wikSm ùu'</p>

<p>  1861. 12. 13 මාක්ස් එංගල්ස්ට එවන ලිපියක මෙහෙම
සඳහන් වෙනවා. </p>

<p>  දී මගේ භාර්යාව දරුණු මානසික තත්ත්වයක පසු වුණා.
ඒ නිසා කිහිප දවසක් යනකම් දොස්තර ඇලන් හොඳට ම කලබල වෙලායි හිටියේ. ඔහු
දන්නවා, නැති නම් සැක කරනවා හේතුව තියෙන්නේ කොහේ ද කියලා......." </p>

<p>  දීමගේ ලිවීම දිගට ම යනවා එහෙත් ඉතිං හෙමින් ......"</p>

<p>  (එකතු කළ කෘති - වෙන්ථම 41 - පිටුව 333) </p>

<p>06. 1863.11.24 ජෙනී එංගල්ස්ට ලියන ලියුමක මෙහෙම
තියෙනවා. දීපසු ගිය සතිය පුරා ම මාක්ස් අසනීප වෙලා කවිච්චියට බැඳලා වගෙයි ඉන්නේ.
එයාගේ  පිටෙයි කම්මුලෙයි විස ගෙඩි දෙකක්
ඇවිල්ලා....................................... මේක හරියට මේ දුෂ්ට පොත කවදාවත්
ඉවර වෙන්නේ නැතුව වගේ. ඒක හරියට භයංකර හීනයක් වාගේ අපේ උඩ පැතිරෙනවා. මේ
තල්මසාව එළියට දා ගන්න පුන්ථවන් වුණොත් ඒ ඇති." (එකතු කළ කෘති - වෙන්ථම 41 -
පිටුව 585)</p>

<p>07. කලින් සටහන් පොත් කිහිපයක් හැටියට ගොනු කළ
ප්රාග්ධනය මාක්ස් 1864 දී එකට ගොනු කරන්න අරගෙන 1867 දී මුද්රණය කළා. 1867.10.05
ජෙනී  පිලිප් බේකර් කියලා එක් කෙනකුට එවන
ලිපියක මෙහෙම තියෙනවා. දීඔයාට  දැනට ම කාල්
මාක්ස්ගේ පොත ලැබිලා නං, ඒ එක්කම මං වාගේ ඔයාට මුල් පරිච්ඡෙදවල දයලෙක්තික උප
මාතෘකා තේරුම් ගන්න බැරි නම්, මම කියන්නේ ඔයාට ප්රාග්ධනයේ මූලික
සමුච්චයයි, යටත් විජිතකරණයේ නවීන න්යායන් කොටසයි මුලින් කියවන්න කියලා. මට
ස්ථිරයි මං වාගේ  ඔයාට මේ කොටසින්
සෑහෙන්න දෙයක් ලැබේවි කියලා." (එකතු කළ කෘති - වෙන්ථම 20 - පිටුව 439)</p>

<p>08. ප්රාග්ධනය කෘතියට එංගල්ස් ලියන විචාරයක් 1867.10.12
මුද්රිත ව පළ වෙනවා. දීමේ තුළින් තහවුරු වන අලූත් ම කාරණය තමයි ප්රාග්ධනයේ
සමුච්චයයි, සංකේන්ද්රණයයි එක්ක එකට එක අතිරික්ත කම්කරු ජනතාවක් ඇති  වීම. මේ තුළින් සමාජ විප්ලවයක් එක් පසෙකින්
අවශ්ය කෙරෙන අතර, අනෙක් පසින් හැකි කෙරෙනවා."</p>

<p>  'අමතර ව කියන්න තියෙන්නේ පළමු පිටු 40 ටිකක්
තද දයලෙක්තික විලාසයෙයි තද විද්යාත්මක ගතියෙයි තියෙද්දි, කෘතිය කියවා ගෙන යන්න
පහසුයි....."</p>

<p>  (එකතු කළ කෘති, වෙන්ථම-20-පිටුව 213)
(අවධාරණය අපේ)</p>

<p>09. 1872 දී ප්රාග්ධනය වෙන්ථම 1 මොරිස් ලා ෂාත්ර ප්රංශ
භාෂාවට පරිවර්තනය කරද්දී මාක්ස් ඒ වෙන්ථම නැවත ලියනවා. ඒ ගැන ෂාත්රට මාක්ස් ලියන
ලිපියක මෙහෙම තියෙනවා. (1872-03-18)</p>

<p>  දීමීට කලින් ආර්ථික විෂයන් එක්ක, මා භාවිත කරන
විශ්ලේෂණ විධිය කි්රයාවේ යොදවලා තිබුණේ නැති නිසා, පළමු පරිච්ඡෙද ටික කියවන එක
තරමක් අසීරු කරනවා." </p>

<p>10. 1871 දී
ප්රංශ විප්ලවය සිද්ධ වුණා ම මේ අපූරු පවුලට (ජෙනී, මාක්ස්, එංගල්ස්,
දරුවෝ, මහ සමාජය) එල්ල වෙන ප්රතිවිරෝධය සංකීර්ණ වෙනවා. එතකොට පවුලේ
ශ්රමයේ මූලික ස්වරූපය තමයි බෙදා ගැනීම පිළිබඳ ගැටලූව. </p>

<p>  1859 දෙසැම්බර් ජෙනී එංගල්ස්ට මෙහෙම ලියනවා.</p>

<p>  දීමං වෙනුවට අද පුංචි දුව ජෙනී ලිපිය පිටපත් කරනවා.
මගේ කාර්යයෙන් මාව ඉක්මනින් ම දොලා විසින් 
neyer flf¾ú lsh,d ud úYajdi lrkjd" t;fldg ~ld¾h iydhlhkaf.a¶ ,ehsia;=jg
uf.a ku jefÜú' §¾> ld,Sk f,alïjr fiajhg miafia ug úY%du jegqmla .kak neß fjk
tl lk.dgqodhlhs'¶</p>

<p> (එකතු කළ කෘති - වෙන්ථම 40 පිටු 575 - 76) </p>

<p> ජෙනීගේ ශ්රමයේ කි්රයාකාරීත්වය මේ වාගේ ලිපි දහස්
ගණනක සටහන් වෙලා තියෙනවා. ඊට පසු කාලෙක දීනිවාස පිළිබඳ ගැටලූවේ" දෙවැනි
සංස්කරණයේ හැඳින්වීමට එංගල්ස් මෙහෙම ලියනවා.</p>

<p>  'මායි මාක්සුයි අතර තිබුණ ශ්රම විභජනයේ ඵලයක්
වශයෙන් සඟරා ප්රකාශනවලට අපේ අදහස් යවන එක මගේ උඩට වැටුණා. ඒ නිසා, මාක්ස්ට
මහා කාර්යයට (ප්රාග්ධනයට) ගොනු කරන්න කාලය තියෙන්න ඕන නිසා විශේෂයෙන් විරුද්ධ
මතවලට විරුද්ධ ව අරගල කරන එක මට පැවැරුණා. මේ නිසා, බහුතරයක් තැන්වල අපේ
අදහස් මට ඉදිරිපත් කරන්න වුණේ, අනෙක් දෘෂ්ටීන්වලට ප්රතිවිරුද්ධ ව වාදශීලී
ආකාරයකට" (එකතු කළ කෘති වෙන්ථම - 26)</p>

<p>11. 1876 සැප්තැම්බරයේ දී එංගල්ස් ඩූරිං විරෝධය
ලියනවා. ඒ කෘතියේ තොරොම්බල් වන දයලෙක්තික සංකල්පය මාක්ස් දයලෙක්තික සංජානනයට
වඩා වෙනස්. අපට ඒකට කියන්න පුන්ථවන් දයලෙක්තික ස්ථාවරය. ලෝකය පුරා වාමාංශික
ස්තරයන් හා චින්තකයන් බහුතරයක් (අල්තුසර් ඇතුන්ථ ව) ඔවුගේ න්යායික භාවිතයේ දී
කෙසේ වෙතත් ප්රායෝගික භාවිතය ගැන න්යායන් ඇතුළෙ තියෙන්නේ එංගල්ස්ගේ මේ
දයලෙක්තික ස්ථාවරය විතරයි. මේ අනුව පරම දයලෙක්තික සංකල්පය අදහන අය ඉබේ මාක්ස්වාදී
වෙනවා, ඉබේ විද්යාත්මක වෙනවා, ඉබේ වාමාංශික වෙනවා, ඉබේ මානුෂික
වෙනවා, ඉබේ රැඩිකල් වෙනවා, ඉබේ ඉබේ යනාදී වශයෙන් ..........</p>

<p>  ඩූරිං විරෝධයේ 8 වැනි පිටුවේ        එංගල්ස් මෙහෙම කියනවා.</p>

<p>  ' ඒ වුණාට එතැන තව කාරණයක් තිබ්බා. කෘතිය
ඇතුළෙ විවේචනය කරලා තියෙන ඩූරිං මහතාගේ දීක්රම පද්ධතිය" ගොඩක් පුන්ථල් ක්ෂේත්රයක
පැතිරිලා පවතිනවා, ඒ නිසා එයා යන යන තැන එයාව ලූහු බැඳලා, මගේ වටහා
ගැනීම එයාගේ එකට ප්රතිපක්ෂ කරන්න මට බල කෙරුණා. ඒකේ ප්රතිඵලයක් වශයෙන් මගේ
සෘණාත්මක විවේචනය ධනාත්මකයට හැරුණා. වාදශීලී ගතිකය අඩු වැඩි වශයෙන් දයලෙක්තික ක්රමයට
සම්බන්ධිත විවරණයකටයි, මාක්ස් හා මා විසින් පුරෝගාමී කරපු කොමියුනිස්ට් ලෝක
දැක්මකටයි හැරුණා. ඒ විවරණය යම් තරමකට පෘථුල ක්ෂේත්රයක් පුරා පැතිර පවතිනවා.
මාක්ස්ගේ දීදර්ශනයේ දුගී භාවය"යි දීකොමියුනිස්ට් ප්රකාශනය"යි තුළින් මුලින් ම ඒක
ලෝකයට ඉදිරිපත් කළාට පස්සේ, අපේ මේ දැක්මේ විධිය (ඵදාැ ද
දෙමඑකදදන), ප්රාග්ධනය පළ කරන තාක් මුන්ථමනින් ම අවුරුදු විස්සක බිජු රකින
කාලයක් ඇතුළෙන් ගමන් කරලා තියෙනවා. (පිටුව 8,9) (අවධාරණය අපේ)</p>

<p>   wjia:d  fofla§ u kùk fN!;sljdoh w;HjYHfhka  ohf,la;slhs' ta ksid wfkl=;a úoHdjkag
by</p>

<p>  'මේ මහා සොයා ගැනීම් දෙක, ඒ කියන්නේ
ඉතිහාසය ගැන භෞතික වැටහීම  හා අතිරික්ත -
වටිනාකම තුළින් ධනවාදී නිෂ්පාදනයේ රහස් හෙළි කිරීම වෙනුවෙන් අපි මාක්ස්ට ණය
ගැතියි. මේ සොයා ගැනීම් එක්ක සමාජවාදය විද්යාවක් වුණා" (පිටුව 27) (අවධාරණය
අපේ) (එකතු කළ කෘති - වෙන්ථම 25)</p>

<p>12. එංගල්ස් 
ohf,la;slh ixl,amh Ndú; lrk fldg fmkakqï lrk ~wúksYaÑ;h¶ ^yßhg u
yhsikan¾f.a ixl,amh u hs&amp; 1883' 03' 17' udlaiaf.a fidfydfka l</p>

<p>  ~~ud¾;= 14 od miajre folhs úkdä y;  ~jvd;a fN!;sl u ukqiailu - wdorh fl</p>

<p>  ^B&amp;    ~úoHd;aul
ñksidf.a ieá tfia úh' tfy;a Tyqf.a m%Odk ,CIKh fuh fkdù h' udlaiag úoHdj"
ft;sydisl" .dul úma,jjd§ n,fõ.hla úh' ;ju;a jHjydrfhys fhoúh fkdyels hehs
n,fmdfrd;a;= úh fkdyels" hï lsis kHdhsl úoHdjla wÆ;ska fidhd .ekSu ksid
Tyq fld;rï mS%;shlska ms  iïmQ¾Kfhka u fjk;a
wldrfha tlla úh' ksoiqkla jYfhka" úoHq;a fCIa;%fhys iy udfi,a fomaf¾f.a
kùk;u fidhd .ekSï .ek o Tyq buy;a ie,ls,s oelaùh'¶  ^f;dard .;a lD;s - fj¿u 3' msgq 113" 114&amp; ^wjOdrKh
wfma&amp; </p>

<p>13. 1891 දී හේගල්ගේ මරණයෙන් හැට වැනි සමරුව
වෙනුවෙන් ප්ලෙහානොව් මෙහෙ  ම ලියනවා. දී
මේක දර්ශනවාදී ක්ෂේත්රයේ වාගේ ම සමාජ විද්යා ක්ෂේත්රයේත් අයත් කර ගත්තු දැඩි වාසියක්
වෙන්නේ නවීන දයලෙක්තික භෞතිකවාදය සම්පූර්ණයෙන් අයත්  කර ගත්තු නිසයි. </p>

<p>  §fya.,a ;=</p>

<p>  ^f;dard .;a lD;s - maf,ydfkdõ - msgq 421"422&amp;</p>

<p>14. 1916 දී ලෙනින් මුද්රිත ව මෙහෙ ම කියනවා.</p>

<p>  ~ udlaia wmg ;¾lhla b;sß lr,d ;sh,d .sfha ke;;a thd m%d.aOkfha ;¾lh
b;=re lr,d .shd'¶</p>

<p>  ^od¾Yksl igyka fmd;a' msgqj 319&amp;</p>

<p>  1922'03'12 f,kska uqøs; j fufyu lshkjd'</p>

<p>  ~ udlaia ksfhdackh lrk øjHjdofha ú{dkl mdlaIslhl= tkï"
ohf,la;sl fN!;sljdÈhl= úh hq;= h' fuu wruqK imqrd,Su i|yd" udlaia úiska jHjydßl
f,i m%d.aOkfha yd Tyqf.a ft;sydisl yd foaYmd,kuh lD;skaj,g fhojQ
ohf,la;slh"''''¶ ^msgqj 181&amp;</p>

<p>  ~i;a;lska u fya.,shdkq ohf,la;slh wOHhkh lsÍu" thg w¾:
ksrEmKhla §u" th m%pdrh lsÍu b;d wudre 
h' fï wxYfha lrk uQ,sl m¾fhaIKj, § ksiel f,i u  w;a je/È isÿ jkq we;' tfy;a lsisjla fkdlrk tflla muKla je/È
fkdlrhs' fya.,shdkq ohf,la;slh øjHjd§ f,i wjfndaO lr f.k udlaia úiska fhdok ,o
jHjydßl l%uh wfma moku yeáhg f.k" wm ishÆ wxYhkays fï ohf,la;slh úia;drKh
l</p>

<p>15. 1963 අගෝස්තුවේ දි
අල්තූසර් මුද්රිත ව මෙහෙ ම කියනවා.</p>

<p>  ~udlaia  Èyd n,kak' thd
fmd;a oyhla tlal W;al¾Ij;a m%d.aOkh ,sh,;a thd ohf,la;slhla ,Sfõ keye' thd tal
,shk tl .ek l:d l</p>

<p>  ^f*da udlaia - msgqj 174&amp; </p>

<p>ඊ 1.  එංගල්ස්ගේ, ලෙනින්ගේ, අල්තූසර්ගේ
න්යායන් එක පැත්තකත්, මාක්ස්ගේ ලෙනින්ගේ භාවිතය එක පැත්තකත්, ජෙනීගේ
හෙලන්ගේ, කෲප්ස්කයාගේ ශ්රමය එක පැත්තකත් තියා ගෙන කරන භාවිතයක් ඇතුළේ
ප්රාග්ධනය කෘතියේ දේශපාලනය ලියන බැරෑරුම් වෑයමක මම මේ මොහොතේ නියැළෙනවා. ඒ
තුළින් පුහුණු වුණ ප්රවේශයකින් තමයි මම දයලෙක්තික සංජානන ඉතිහාසයක් මේ ලිපියේ
ලීවේ. මෙතැන දි  භාවිතය නෙවෙයි න්යායික
ව  මට ගොඩක් උපකාරී වුණ අල්තූසර්ගේ කෘති
සිංහලෙන් නැති වීම මේ ලිපිය කියවන සිංහල අයගේ කරුමයේ ගැටලූවකට වඩා දේශපාලනික
ගැටලූවක් වශයෙන් ගන්න සිද්ධ වෙනවා. මේක නිකං සිංහල විතරක් දන්න නිසා ඉංගී්රසි පොත්
කියවන්න බැරි වීම තරම් සරල නැහැ. දාර්ශනික කෘතීන් එක්ක අරගල කරන්න අවශ්ය ඥාන භාවිතයන්
නිර්ධන පංතියට නැහැ. ඥාන ඇබ්බැහියන් නැහැ, ඥාන රාගයන් නැහැ, ඥාන පුරුදු
නැහැ, ඥාන වීරකම් නැහැ, ඥාන හීනමානය නැහැ, ඥාන සංස්කෘතිය නැහැ,
ඥාන කුතුහලය නැහැ, ඥාන ප්රවේශයක් නැහැ, නැහැ, නැහැ,
................ දාර්ශනික ප්රවේශයකින් දීප්ති මේ කාරණය මෙහෙ ම කියනවා. දීපොදු ජනයාගේ
ආත්මයට දර්ශනය තුළ ආරම්භයක් මේ නිසා නොමැත." (මාතොට - කාණ්ඩය 2. කලාපය 6.
පිටුව 20.) ඉතිං නිර්ධන පාංතික සන්දර්භයක් ඇතුළේ තමන්ගේ ශරීරයේ දේශපාලනය
හඳුනා ගන්න අයට මේක අත්යවශ්ය දේශපාලන ගැටලූවක් වශයෙන් වටහා ගන්න වෙනවා. මේ ගැටලූව ම
ලාංකීය තලයෙන් ටිකක් ඈතට තල්ලූ කරලා ප්රාග්ධනය කියන කෘතිය වර්තමාන සන්දර්භයේ දී
දාර්ශනික කෘතියක් කියලා හඳුනා ගෙන (අල්තූසර් ඇතුන්ථ කිහිප දෙනකු ඒක කළා.)
මාක්ස් මුහුණ දුන්න දුෂ්කර දයලෙක්තික සංජානනය සමාජගත කරන්න උපායක්  නිර්මාණය කර ගන්න සිද්ධ වෙනවා. ප්රාග්ධනය කියන
සිවිල් කෘතිය බිහි වුණ භාවිතය අනුව ම ඒකෙ ඊළඟ අවධිය වෙන්න ඕනේ ඒක දාර්ශනික ව
තේරුම් ගැනීමක් නෙවෙයි. ඒක සමාජගත කිරීමක්. හේගල්ගේ දයලෙක්තිකය කකුලෙන්
හිටවේවා, ෆොයර්බාහ්ගේ භෞතිකවාදය පොළොවට ගෙනාවා වාගේ කයිවාරු අතර දැන්
අවශ්ය කයිවාරුව තමයි දාර්ශනික වස්තුවක් වෙලා තියෙන ප්රාග්ධනය පොළොවට ගේන එක.</p>

<p>  2. 
udlaiaf.a ÿIalr ohf,la;sl ixcdkkfha m%d.aOkh Ndú;h fjkqfjka mjq,hs
iudchhs ueo thd ;ukaj brd f.k ysg.;af;a úoHdfõ w¾nqohlska fm  úoHdj ;sfhkak mq¿jka fmd;aj, ú;rhs lsh,d' wvq .dfka ksÍCIlhl=
uÜgfuka yß ukqiaihd iïnkaO jqKq ;ek b|ka úoHdjhs u;jdohhs w;r wksjd¾h iïnkaOhla
mj;skjd' fldfydu yß u;jd§ fya;=j,g ;uhs ñksiaiqkag úoHdj ´k fjkafka' w,a;+i¾f.a
YÍrh .fõIKh fldf   lsh,d fyõfjd;a §úoHd;aul j¶ tl
ms  ,efnkafka keye'  ngysr fnda fj,d ;sfhk tla;rd isú,a ksh;hla fu;ek wfma {dk úNd.hg
ndOd lrkjd'  isú,a ixúOdkh fyd|g u
Yla;su;a rgj, .eiÜj,ska jdf.a rdcH m%ldY ksl=;a l       </p>

<p>             wyj,a m%ldYh  lrkak
l,ska $ miq j hkd§ jYfhka" kS;shg ;sfhkafk;a fï ls%hdldÍ;ajh u hs' b;sx ta
uecsla f¾Ldj msámiafia ;sfhk jHjia:dj" m%ldYh yefrkak ta f¾Ldj icSú lrjk
wdh;ksl Ndú;hka rdYshl=;a ta iudchkaj,g ;sfhkjd' tfy;a ,xldfõ udkj ysñlï m%ldY
ksl=;a lr,d ta ksl=;a lrmq  ckdêm;sg u
;ukaf.a yuqodj ,õjd ñksiaiq mqÉp,d" ;ukaf.a f.da,hka ,õjd weue;shka urjkak
mq¿jka' tfy;a b;sx ,xld $rg fNaohla ke;s j iudchSh úoHdjkaj, § fidhd .ekSï
yß" f.dkq lsÍï yß lrmq m</p>

<p>  පවුලයි ආගමයි කියන්නේ ඉබ්බයි ඉබි කට්ටයි තරමට
ඓන්ද්රීය ව බැඳුණු ප්රපංච දෙකක්. ඉතින් පවුලෙන් 
fjka lr,d wd.u jgyd .kak m%fõYhlg fN!;sl fjk tl w;sYh ÿIalr jevla'
f*dh¾ndya ;SiSi tfla fN!;sl ixcdkkhla .ek lshkak .sys,a,d mjq,hs wd.uhs
wú{dkslj yß udlaia tlal fjf</p>

<p>     'පරාරෝපණය පිළිබඳ අදහස මාක්ස්ගේ මුල්
යුගයේ  රචනා තුළත් කැපිටාල් තුළත්
දක්නට ලැබෙන්නකි. එබැවින් පරාරෝපණය මාක්ස් චින්තනය තුළ ප්රොබ්ලමැතිකයකි." (ප්රවාද
- 10 කලාපය - පිටුව 38) (අවධාරණය මගේ)</p>

<p>oeka wmsg yß u ir, j u jgyd .kak mq¿jka     OfkaYajr Ndú;hla ;=  ixcdkkhlg     ks¾Ok
mdx;sl iú{dkh flfia fj;;a ohf,la;sl ixcdkkhla 
ysñ fjk tl fjkak neß jevla' t;ek § mrdfrdamKh u ;uhs fjk;a ixcdkkhl h;=r
njg yefrkafka' tfy;a m%Odk;u ldrKh  ;j u
;sfhkafka fkdi,lmq m%foaYhlhs' lïlre wr., wd¾:ßljdofha b|ka foaYmd,khg mßj¾;kh
ùu yqÿ Ñka;khl ls%hdjla fkú' ta Ñka;kj,ska flfrkafka rdcH n,h fmr  cSú;hg idfmaCI mj;sk foaYmd,k
w¾nqohla jYfhka ixcdkkh fjkak uQ,sl .;slh ;sfhkafka YÍrh yd mjq, w;r mrdfrdamKh
fCIa;%fha' fudlo OfkaYajr iudc ikao¾Nhlg ;uka $ wfkld fnok iudc ikao¾Nh fj,d
;sfhkafka kHIaál mjq,' §mj;sk foaYmd,kh¶ w¾nqohla jYfhka ;reK YÍr úiska y÷kd
.ekSfï m%fõYh ;uhs ;reK YÍr ^úfYaIfhka mqreI&amp; ;j u mjq,g ^udkqIsl j $
fN!;sl j&amp; we;=¿ fj,d ke;s ksid 
mj;sk mrdfrdamKh' t;ek § ta YÍr mjq,g we;=¿ fjkak bkafka  úma,jhg miafi'a .egÆj ;sfhkafka  fï we;=¿ ùu kej; j;djla wr OfkaYajr we;=¿
ùuhs' mqreI YÍrhlg ixlS¾K;u ;Dma;sh fok i.hdj ^.eyeKq YÍrh&amp; WmlrKhla jYfhka
ixcdkkh lr .;a;d u mqreI YÍrfha iudc Ndú;h wksjd¾hfhka foìähs' fudlo tl
me;a;lska thd ;ukaf.a iudchSh wfkld jqKq .eyeKsh ;Dma;sfha jia;=jla lrkak
yokjd' wfkla me;af;ka ;ukaf.a iudchSh wfkld jqKq mqreIhdj iudchSh j y÷kd .kak
W;aidy lrkjd' B</p>

<p> </p>

<p>     'දැන් අපි පුරුෂ දේශපාලනයේ පැලෝ-මධ්යය
වටහා ගනිමු. වෙනත් විධියකට කියනවා නම් ලාංකීය මාක්ස්වාදී මතවාදී විද්යාවන්වල  අලංකාරිකය ගොඩ නැඟෙන්නේ සහ ක්ෂේත්රගත වෙන්නේ
මෙම පැලෝ මධ්යය වටා බව හඳුනා ගැනීමට උත්සාහ කරමු. මේ සඳහා අපි මෙහෙම එක්තරා
කල්පිතයක් ගොඩ නඟා ගමු. 'ලංකාවේ පිරිමි දේශපාලනය කරන්නේ සිය මහේක්ෂ ජීවිතය
තුළ. ඒත් ඔවුන්ගේ ශුක්රාණු පහ වෙන්නේ සිය ක්ෂුද්ර ජීවිතය තුළ දී ය." එහෙම නම්
ලංකාවේ පිරිමින් කරන දේශපාලනය විසංයෝජනය කළ හැකි භෞතික පැලෝ-මධ්යය නම් ලාංකීය
ස්ති්රයගේ යෝනි මාර්ග නිමිත්තයි. මේ හින්දා තමයි ලාංකීය බොහෝ පිරිමින්  විවාහය හෝ පේ්රම කිරීම හෝ ආරම්භ කළ විට
සිය දේශපාලන ව්යාපෘතීන් අත හැරලා දාන්නේ. හොඳයි කොහොම ද මෙම කි්රයාවලිය
දේශපාලන සමාජ-විද්යාවක් තුළ දී පහදන්නේ. </p>

<p>     විෂම - ලිංගික පුරුෂ දේශපාලන ව්යාපාරවල ප්රධාන
ලක්ෂණයක් තමයි එම ව්යාපාරවල කි්රයාකාරීත්වය තුළ 'ලිංගිකත්වයට" තහංචි දාන එක. මේ
හින්දා මෙවැනි ව්යාපාරවල  සාමාජිකයන්ගේ
ලිංගික ජීවිතවලට  භාෂාවක් ගොඩ නැඟෙන්නේ
නැහැ. ඒ හින්දා මෙම ව්යාපාරවල බොහෝ විෂම ලිංගික පිරිමින් නියුරෝසියාවට ලක් වන
අතර එවැනි ව්යාපාර තුළ වැඩ කරන ස්තී්රහු සයිකොසිස් හෝ හිස්ටීරියාවෙන් හෝ
පෙළෙති. මෙවැනි ව්යාපාර තුළ සම ලිංගික පිරිමි සහ සම ලිංගික ස්තී්රන් මානසික
රෝගීන් හැටියට හඳුනා ගැනති.</p>

<p>     මෙම විෂම-ලිංගික පුරුෂ දේශපාලන ව්යාපාරවල වැඩ
කරන පිරිමින්ට ලිංගික භාෂාවක් ගොඩ නඟන්න උදව් කරන්නේ යෝනි-මාර්ග නිමිත්ත. ලිංගික
භාෂාවකින් කියනවා නම්  විෂම ලිංගික පුරුෂ
දේශපාලන ආකෘතීන් ගොඩ නැංවෙන මාධ්යය තමයි යෝනි-මාර්ගය. හැබැයි මෙම යෝනි-මාර්ග
සංකේතය මෙම විෂම ලිංගික පුරුෂ දේශපාලන සංදර්භය තුළ තහංචියක්. ඒ මොකද මේ
සංකේතයෙන් තොර ව  සමාජය ගැන හිතන්න මෙම
ව්යාපාර තුළ වැඩ කරන පිරිමින්ට  භාෂාමය
වශයෙන් නොහැකි හින්දා. (ඕනෑ ම භාෂාවක් ගොඩ නැඟෙන ගැඹුරු ම මාධ්යය සපයන්නේ ස්තී්ර
පුරුෂ නිමිති වෙනස තුළ දී නිසයි එහෙම වෙන්නේ.) මිනිස් සංසර්ගය තුළ දී
පුරුෂයා ඔහුගේ දේශපාලන විශ්වය නිර්මාණය කරන තහංචි සංකේතය වූ යෝනි-මාර්ගය
විනිවිදිනවා. වෙනත් ආකාරයකට කියනවා නම් පුරුෂයා තමන්ගේ ලිඟුව ස්තී්ර
යෝනි-මාර්ගයට  (හෝ ගුද මාර්ගයට හෝ
වෙනත් මුඛ මාර්ගයකට හෝ) ඇතුන්ථ කරන විට තමන්ගේ ජීවිතයේ 'දේශපාලන අර්ථය"
නිර්මාණය කරන සංදර්භය අභිභවනය කරනවා. නැත්නම් විසංයෝජනය කරනවා. ලිංගික භාෂාවකින්
කියනවා නම් මෙහෙමයි. විෂම -ලිංගික පුරුෂ දේශපාලන අර්ථය නිෂ්පාදනය කරන්නේ යෝනි
මාර්ගය නම් තහංචියේ සන්දර්භයට සාපේක්ෂ ව ය. මෙම තහංචිය විනිවිදින සෑම නිමේෂයක දී ම විෂම
-ලිංගික දේශපාලනය අර්ථ විරහිත දෙයක් බවට විසංයෝජනය වෙයි. මෙන්න මේ අර්ථය විනාශ කරන
වස්තුව යෝනි-මාර්ගය බව පුරුෂ බල රටාව අවිඥානික ව දන්න හින්දා තමයි විෂම-ලිංගික
පුරුෂ දේශපාලනය තුළ දී ලිංගික-භාෂාව 
u¾okh lrkafk - ke;akï ksyvZ lrkafk'¶ ^m%jdo - 8 l,dmh - msgqj 69&amp;
^uq,a ,smsfha ke;s wjOdrKh ;sfhkafka w¾cqkf.a msgrg úpdrh we;=f</p>

<p>     මාක්ස්ගේ කෘති ඇතුළෙ දී ස්ථිර ව  පිළිගන්නා ස්ථාවර දෙකක් තියෙනවා. එකක් පවුල
වනාහි සමාජ ආර්ථිකඒකකය. අනික පවුල ඇතුළේ ගැහැණිය නිර්ධනයා වන අතර පුරුෂයා
ධනපතියා වෙනවා කියලා. ඉතිං ලෙහෙසියෙන් අහලා දාන්න පුන්ථවන්, ප්රතිගාමී ජීවිතයක්
ගෙවන කම්කරුවා (පුරුෂයා) කොහොම ද ප්රගතිශීලී භාවිතයක යෙදෙන්නේ කියලා. තවත්
විධියකට අහන්න පුන්ථවන් ජෙනීගේ ලිංගික ශ්රම ශක්තියේ අතිරික්තය වුණ පුරුෂ ලිංගික
තෘප්තියේ සූරා කෑමයි, ජෙනීගේ (මාක්ස්වාදී) ශ්රම ශක්ති අතිරික්තය වුණ දීපවුල්
සංවිධානය" සූරා කෑමයි නිෂේධනය නොකරන මාක්ස්ගේ සමාජයීය භාවිතය එහෙම නැති නම්
මානුෂික අනාගතවාදය කොහොම ද අංග සම්පූර්ණ වෙන්නේ. මේ විධියට මාක්ස් දේශපාලනයයි
ස්තී්ර දේශපාලනයයි තමන්ගේ නියම ශරීරය තුළ එකතු කරන අලූත් දේශපාලනයකින් විතරක්
දයලෙක්තික සංජානනයේ සාමාජයීය ගුණාත්මකය</p>

<p>   1980 cQ,s j¾ckfhka
riaid ke;s jqKq ish .Kklska uu wyf.k weúoao m%Yakhla ;sfhkjd' ta ;uhs riaidj
ke;s jqKd u f.a we;=f  ú.%y lr,d" igk jegqKq
tl .ek kHdh lkaordjla ;sfhkjd' tfy;a lïlrejkaf.a jev j¾ck úfrdaë Nd¾hdj fudk
;rï j¾ckh jÜgkak odhl jqKd o lshk tlg iudc úoHdjla keye' b;ska fu;ek § u;=
fjkjd  úoHd;aul úêhg iudc wkd.;h yokak
m%fõY jqKd u ta mqfrda.dñhdg fudk ;rï úoHd;aul 
fjkak fjkjd o lsh,d' wks;a w;g cSú;h úoHd;aul fjkjd lshk ldrKfha È ;j u
ñksia ixy;sh bkafka uq,a  wjêhl' wfma
f.da;%fha uq,a u i.hd jqK ld,a udlaiaf.a ~ojfia fõ.fhka¶ ú;rhs thdf.a wdo¾Yh
wmg y÷kd .kak mq¿jka fjkafka' §fudlo úis jeks Y; j¾Ifha i.fhda ú|ú,af, È udlaiag
jvd f.dvla bÈß.dñhs' foaYmd,khla yod .kak ;sfhk wiSrej we;=f  ^lïlrejl=f.a&amp; foìälu ;rï .=Kd;aul keye' m%udKd;aulhs' f.dryeä
idrdxYhla jYfhka fufyu fnokak mq¿jka' reishdkq úma,jjd§ka  b|ka fÉ yryd  1989 i.hka olajd  Tlafldau
lf|ka udlaiauh wkd.;jdÈhs' .%dïiaÑg *eisiaÜ Widúfhka ÿkak ;Skaÿj fï T¿j ys;k tl
kj;ajkak isr.; l  jqKq *qflda taâia yeÈ,d
uereKd' w,a;+i¾ udkisl wdndOhlska     
fy,kaf.a fn,a, ñßl,d thdj ur,d" Bg miafia udkisl wdndOfhka uereKd'
ä,shqia ish Èú ydks lr .;a;d' b;sx fï Tlafldu T¿fjka udlaiauh wkd.;jdÈhs' ^fckS
udkisl wikSmhlg f.dÿre jqKq tlhs udlaia YdÍßl wikSmhlg f.dÿre jqKq tlhs wdfhu;a
u;la lr .uq'&amp; b;sx fï y÷kd .ekSfï msámiafia ;sfhkafka fudlla o@ ta ;uhs
ÿIalr cSú;hhs ÿ¾,N Y%uhhs .ek l:dj' fï ;;a;ajhka hgf;a ;ukaf.a we;a; YÍrhg
m%;súfrdaOhkaj,g f.dÿre fkdlrjk cSú;hlg ;j ÿrg;a udlaiajd§ foaYmd,kh bÈßhg f.k
hkak flfia fj;;a kshef  thd ,shk foaj,a ,shkak ta
ukqiaihdg ta m%foaYj, ienEjg úksúÈkak jk ksihs' fudlo §ma;s ,shk nyq;rhla
foaj,a Th w¾cqk fyda fjk hï whl= fyda lshj,d ;sfhk úfoaY fmd;aj, keye' ^uu;a
tajd f.dvla lshj,d ;sfhkjd'&amp; §ma;sf.a ta kHdhka ckkh jk ;ek ljqre yß fydhd
f.k .sfhd;a wjidkfha uqK .efykafka ukqiaihdf.a ÿIalr Y%u ksfïIhla' yßhg
m%d.aOkh keue;s isú,a jia;=fõ Èh fj,d ;sfhk Y%u iuia;h msámiafia udlaiaf.a ÿ¾,N
Y%ufha Y%u Yla;sfha ld¾h fldgi jdf.a'</p>

<p>(ෘ)   මාක්ස්ගේ දයලෙක්තික සංජානනය හිර වුණ තැන ම
වාමාංශයත් හිර වුණා. වෙනත් විධියකට හඳුනා ගන්නවා නම් මාක්ස් විද්යාත්මක වුණු තැන මානුෂික
නොවුණා. නැති නම් මානුෂික වුණු තැන විද්යාත්මක නොවුණා යනාදී වශයෙන්. එක්කෝ එයාගේ
ක්ෂණික පවුල ගැන විද්යාත්මක වන තැන සමාජ පවුලට විද්යාත්මක වෙලා නැහැ. එහෙම නැති නම්
සමාජ පවුලට විද්යාත්මක වන තැන ක්ෂණික පවුලට විද්යාත්මක නැහැ. අපි මේ ගැටලූව ටිකක්
විද්යාත්මක ව හඳුනා ගමු. ගැටලූවේ එක් වැදගත් තැනක් දාර්ශනික ව හඳුනා ගන්නා අල්තූසර්
කියනවා දාර්ශනිකයන් පවා ඉපදිලා ඉගෙන ගෙන සංකීර්ණ වෙන්න වෙනවා කියලා. මේ කාරණය ක්ෂණික
ආලෝකයකින් ෆොයර්බාහ් තීසීසවල දී මාක්ස් කියනවා, ශික්ෂණයත් ශික්ෂණය ලබා ගත
යුතුයි කියලා. අල්තූසර්ගෙයි මාක්ස්ගෙයි මේ වාක්යවල තියෙන සීමාව තමයි, අතිශය
වැදගත්, තීරණාත්මක දේශපාලන ගැටලූවක් දාර්ශනික ගැටලූවක් වශයෙන් හඳුනා ගැනීම. ඒ
තමයි මාක්ස් ඉපදිලා හැදිලා විප්ලවවාදියකු (?) වුණා වාගේ, මාක්ස්ගේ ජීවිතය
එහෙම වෙන්න අහඹුවල් ඔස්සේ ලැබුණු වාස්තවික කොන්දේසි ක්රමානුකූල ව සමාජයට
වැපිරුවොත් එක්තරා කාලයක දී මුන්ථ නිර්ධන පංතිය ම (මොකද ධනේශ්වර භාවිතයට දයලෙක්ති
සංජානනය උරුම නැති නිසා) විප්ලවවාදීන් බවට පත් වෙනවා කියලා. ඒ කියන්නේ
අල්තූසර්ව අපි එක්තරා කාරණයකට  හැරවේවොත්
ඒකේ ප්රධාන ප්රකාශිතය තමයි එක්තරා තීරණාත්මක දේශපාලන ගැටලූවක් දාර්ශනික ගැටලූවක්
විධියට වේෂ නිරූපණය වන එක. ඒ කියන්නේ ප්රධාන ගැටලූව මාක්ස්ගේ  චින්තනයවත් ප්රාග්ධනය කෘතියවත් දාර්ශනික ව  විද්යාත්මක කරන එක නෙවෙයි. ඒක අතිරේක
ව්යාපෘතියක් විතරයි. ගැටලූව තමයි මාක්ස්ගේ චින්තනයයි ප්රාග්ධන කෘතියයි නිර්ධන සමාජයට
සමාජගත කිරීම. අර අතිරේක ව්යාපෘතියේ සීමාවන් පවා තීරණය කරන්න වෙන්නේ මේ ප්රධාන
කාර්යයට සාපේක්ෂ ව විතරයි. අතිශය සමාජවාදී වෙහෙසකින් බිහිවුණ දයලෙක්තික සංජානනය
මාක්ස්ගේ කාලයේ ඉඳන් ධනේශ්වර අවකාශයකට ගමන් කරලා තියෙනවා. ඕන නම් කියන්න පුන්ථවන්
සමාජයීය අවකාශයක් පුද්ගලිකකරණය වෙන්න අරගෙන කියලා. ඕන නම් කියන්න පුන්ථවන් චේයි,
අල්තූසරුයි කියන්නේ ඒ දිශාවේ කූට ප්රාප්තියන් කියලා. මාක්ස්ගේ විද්යාව එහෙම නැති නම්
දයලෙක්තික සංජානනයේ ස්වරූපය අනුව ම වුවමනා කෙරිලා තියෙන්නේ පුද්ගලයන් නෙමෙයි මහා
පවුලක්, පංතියක්.  ප්රාග්ධනය කෘතිය
ඇතුළේ ඉතිහාසයයි /පුද්ගලයායි අතර සම්බන්ධයත් මේ වාගේ දුෂ්කර එකක්, ඒ
කියන්නේ පෞද්ගලික මිනිස් ශරීරයට දුෂ්කර එකක්. උපමාරූපික ව වටහා ගන්නවා නම් දයලෙක්තික
සංජානනය සහිත  මාක්ස්ට පැවැත්ම තියෙන්නේ
සමාජයක් ඇතුලේ විතරයි. එහෙත් සමාජයට මාක්ස් කියලා පුද්ගලයකු වැඩැක් නැහැ,
අවශ්ය සමාජයට දිය වුණු මාක්ස් කාරකයක් විතරයි. ලාංකීය වාමාංශිකයන් ගාව තියෙන අහිංසක  ප්රාර්ථනාවන් එක්ක බැලූව ම මේක  අතිශය සංකීර්ණ කටයුත්තක් බවට හැරෙනවා. එහෙත්
කම්කරු /ගොවි පැවැත්ම අනුව ම මේකෙ කිසි ම සංකීර්ණතාවක් නැහැ. ඉතිහාසය දිගට ම පැවැතිලා
තියෙන්නේ නිශ්චිත පංති අවශ්යතාව මත වුණත්, දයලෙක්තික සංජානනයට සාපේක්ෂ ව
බැලූවොත් මෙතෙක් පැවැති ඉතිහාසය අතිශය ඉරණම්කාරියි. මාක්ස්මය භාවිතයේ ඉන්න අයගේ
දීදාර්ශනික"  ගැටලූ පැන නැගින්න ඕනෙ මෙතැනයි.
ලෝකයේ ද්රව්ය ප්රාථමික ද නැත් ද කියන එක 
od¾Yksl .egÆjla fkfuhs Ndú; .egÆjla' ta lshkafka Ndú;fha § øjH m%d:ñl o
ke;a o hkd§ jYfhka' udlaia foaYmd,kfha mYapd;a reishdkq ia:djrhla lsh,d  wms fïl m%j¾. fldf  wmg fjk;a od¾Yksl m%Yak rdYshla ks¾udKh fõú'
</p>

<p>නිදසුනකට:-</p>

<p>.   udlaiauh m%fõYhl § ñksiaiq
b;sydih ks¾udKh lrkafka iú{dksl j o wú{dksl j o@</p>

<p>.  udlaiauh m%fõYhl § úma,jjd§ka 
ñkskaiqf.a Y%uhghs cSú;hghs .re lrk ia:dkh fudlla o@ </p>

<p>.   whl= b;sydih we;=f 
udkqIsl fjk tlhs w;r  úoHdj
fudlla o@ </p>

<p>.   udlaiaf.a yq  tfy;a fïl we;a;g u ufkda
úYaf,aIK .egÆjlg jvd udlaiauh .egÆjla' ohf,la;sl ixcdkkh ixlS¾K fjkak neß
thdf.a ld,fha mjd udlaia fïl lSjd'</p>

<p>  ~mSg¾ úiska ;ukaf.a wkkH;dj ukqiaihl= jYfhka uq,ska u yokafka
fmda,aj ;u j¾.fha lsh,d ikaikaokh lrk tflka u hs'¶ ^m%d.aOkh - fj¿u1-msgqj
59&amp; ^ug u;l ke;s udlaiaf.a uq,a hq.fha lD;s lsysmhl fï ikaikaokh udlaia
lr,d ;sfhkjd'&amp; tfy;a Bg jvd ixlS¾K wjldYhl bkak wmg fï .egÆj fjk úêhlg
ia:dms; lrkak mq¿jka" uu ke;=j wfkld bkak mq¿jka o@</p>

<p> </p>

<p>     මාක්ස්ගේ දයලෙක්තික සංජානනය ආගමික සම්ප්රදායක්
වෙන්න අවිඥානික ව දායක වුණ මුල් දෙන්නා එංගල්ස් හා ප්ලෙහානොව් කියලා මට හිතෙනවා.
එංගල්ස්ගේ මේ දායකත්වය අවිධිමත් නම් ප්ලෙහානොව්ගේ එක විධිමත් එකක්. ප්ලෙහානොව් මෙහෙම
කියනවා. </p>

<p>     'ආගමික විශ්වාසයන්වලින් ආසාදනය වෙලා ඉන්න
මනුස්සයකුට අපේ සංවිධානයේ දොරවල් වහන්න අපට අයිතියක් නැහැ."</p>

<p>     (තෝරා ගත් කෘති - ප්ලෙහානොව් - පිටුව
446)</p>

<p>අපි
මේ ප්රකාශය ආචාර විද්යාවේ ඉඳන් දේශපාලන විද්යාව දක්වා පරිවර්තනය කරමු.</p>

<p>මේ
න්යායික ලිපියේ විද්යාව ගැන අපි මෙහෙම නිශ්චයකට එමු. මේ ලිපිය පුරා තියෙන න්යායන්
විද්යාත්මක නම් ඒ තුළ අනිවාර්ය නිශ්චයක් ගැබ් 
fjkjd' ta ;uhs fuf;la meje;s Èhei iÛrdj tfyu meje;=fKd;a úkdY fjkak
hkjd' ke;s kï .=Kd;aul mßj¾;khlska wdfhu;a mj;skak hkjd' fï ,smsh iuia;hla
jYfhka .;af;d;a m%udKh .=Khg mßj¾;kh jk Ndú;d;aul ksoiqkla fï ;=</p>

<p>        </p>






</body></text></cesDoc>