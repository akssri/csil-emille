<cesDoc id="sin-w-humanities-7" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-humanities-7.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>YS% ,xldfõ
jsh,s l,dmh wdYs%; m%d.a fM;sydisl m¾fhaIK ( </p>

<p>iS.srsh
)oUq,a, m%foaYfha m%d.a fM;sydisl f;dr;=re</p>

<p> </p>

<p> </p>

<p>.dñKs wosldß</p>

<p>l:sldpd¾h </p>

<p>mqrdúoHd mYapd;a Wmdê wdh;kh</p>

<p>le,Ksh úYajúoHd,h</p>

<p> </p>

<p> </p>

<p>ye|skaùu</p>

<p> </p>

<p>msysàu wkqj YS% ,xldj úúO l,dmSh foaY.=Ksl ,laIK j,ska hqla; jqjls' m%Odk
jYfhka n,k l, ;eks jYfhka .;fyd;a ;eks;=kls' uqyqoq uÜgfï isg wä 0)500  olajd ;sfnk my,g ;ekak wä 500 isg 3000  olajd ;sfnk uOH ;ekak wä 3000 isg 8000 olajd
;sfnk by, ;ekak jYfhks' tfy;a N+úoHd;aul yd foaY.=Ksl jYfhka ;sfnk úp,H;djhka
wkqj l,dm 6 lg wh;a fõ' fulS l,dmhka tu m%foaYhg wdfõksl YdL i;aj m%cdjkaf.ka
hqla; fõ' l,dmSh jYfhka ,efnk jeis m%Odk jYfhka fudaiï yd wka;¾ fudaiï iq</p>

<p> </p>

<p> </p>

<p>m%d.a flaïns%h hq.fha isg isÿjq úúO N+ úoHd;aul úp,H;djhka u.ska
fYaI  we;s hq. lsysmhl wjfYaI osjhsfka
we;eï ia:dk j,ska yuqjS we;'  fï whqrska
jq ld</p>

<p> </p>

<p> </p>

<p>YS% ,xldfõ m%d.a fM;sydisl udkjhkaf.a csjs;h ms 
tfy;a fulS úsIh fCI;%h ;=, ;ju;a fkdjsisoqkq .egtZ rdYshls'  flfia jqjo Ys,d fuj,ï udOHla lr.ksñka werÑ
fuu úsIh tajd yoqkd.ekSu Ndjs;h wdoS lrKq wkqj úsjsO udos,sfhka wOHkhg noqka
úsh' fuysoS m  uq,sl jYfhka Ys,d
m;=re ;=,ska flreK m%d.a fM;sydisl wOHkhka Bg jvd mqtZ,aj l,hq;= njg jrska jr
jsoaj;=ka u.ska lreKq bosrsm;a lr ;sfnz' ta wkqj Tjqka N+jsoHd;aul idlaIs,.=yd,
jsjD; jdiia:dk .ek;a udkjhskaf.a wdydr ls%hdj,shg fhdod.;a ;dlaIKh .ek;a lreKq
bosrsm;a lr ;sfnz' (De¾niyagala, S., 1992) 
tfy;a 1940 j¾IfhaoS mS'oerKsh., jsiska fmdis, yd l%udKql+, leKsï j,ska
,nd.;a ksheoSka wOHhkhg noqka lrñka YS% ,xldfõ Ys,d ixialD;sh, n,xf.dv
ixialD;sh f,i yoqkajd fok ,oS' tfukau Tyq by; wOHhk lafIa;%hka lsishï m%udKhlg
mqtZ,a lrñka m%d.a udkj m%cdj yd iuld,Sk i;aj, YdL m%cdjka .ek wOHkh lrkakg o
oevs W;aiyhl fhoqkq nj fmkajd osh yelsh' 
</p>




<p> </p>

<p>bka miqj isrdka oerKsh., jsiska Ys,d m;=re ,laI 2 la muK wOHkh lr YS%
,xldjg Ys,d fuj,ï udos,shla ilia lr we;' Tyq m%d.a udkjhd yd ine|s fN!;sl
iïnkaO;djka .ek oevs Wkkaoqjlska wOHkh fCI;%h mqtZ,a lrk ,oS' tfukau m%d.a
fM;sydisl wOHkh i|yd w;HjYH jslsrKYs,s osk kS¾khka ishhla muK Wmfhda.S
lr.kssñka YS% ,xldjg m%d.a fM;sydisl ld, rduqjla f.dv k.k ,oS' .=yd jdiia:dk,
jssjD; jdiia:dk yd merKs mia;gzgq mrSlaIdfjka Tyq fuu lreKq ikd: lrkq ,en we;'
(De¾niyagala, S., 1992) jsfcamd, uy;d jsiska lrk ,o leKsï fï ;;ajhka ;j oqrg;a
;yjqre lrk ,o w;r kj Ys,d hq.fha wjfYaI ,xldj ;=, we;s njg fhdackjdka bosrsm;a
lrk ,oS' Tyq f;;a l,dmfha lrk ,o leKsï ms</p>

<p> </p>

<p>Ys,d hq. ñksidf.a ixialD;sh .ek wOHhk fCI;%h fï whqrska mqtZ,ajqjo
YS%,xldfõ ish,q l,dmhkaf.a mq¾K jYfhka Y=oaO jsoHd;aul wOHkhka isoqjS we;af;a
b;d w,am jYfhks' ta fya;=fjka hï hï lreKq bosrsm;a lsrsfuys ,d we;sjk .eg,q
ksrdlrKh lr.; hq;=j ;sfnz'  l,dmSh fnoSu
;=, olakg ,efnk ixl%dka;s .ek ieioSï wOHkhla isoqjS fkdue;'  fuu mqrdjsoHd jsIh fCI;%fha mqtZ,a jsIh
Odrdjka iu. m%d.a fM;sydisl wOHhkh isoqjsh hq;=hs' Ys,d m;=re u.ska m%d.a
b;sydih ms 
tfy;a ta yd ine|s wfkl=;a jsoHd;aul fCI;%h ms</p>

<p> </p>

<p> </p>

<p>mqqrdWoaNso jsoHdj, mqrd i;aj jsoHdj, mqrd N+jsoHdj, ixialD;s fyda
WmixialD;s f.da;% fyda lKavdhï, cSjk rgdj rg wNHka;rfha ixjrK ls%hdj,sh .ek hï
;rulg fyda wOHkhka ish,a, isoqj we;af;a by,, my, f;;a l,dm yd uqyqoq nv ;Srh
;=,hs'  my; jsh</p>

<p> </p>

<p> </p>

<p>iS.srsh m%foaYh</p>

<p> </p>

<p>mqrdjsoHd{hska jsiska wjqreoq 5 muK ld,hla ;=, ^1988)92&amp; jrska jr lrk
,o wOHhk ;=,ska m%d.a b;sydih ms    m%d.a fM;sydisl idOl j,ska ,o wia:s fYaIhka wOHhk lsrsfuka .,a
hq. ld,fha yd j¾;udk i;aj fldgia w;r jsYd, mrdYhla oelsh fkdyels nj jsoaj;=ka
fmkajd oS ;sfnz' (Karuna¾thna, P.B. 1989) ^Chand¾¾thna, 1991) tfy;a fuu leKsï
j,ska yuqjq fn,a,ka .ek lrkq ,enq wOHkh u.ska m%d.a udkjhka cSj;a jq hq.h wog
jvd Ys;, hq.hla jsh yels nj jsoHd¾:Ska yoqkajd oS ;sfnz'(Premathilake,
1994)  iS.srsh oUq,a, m%foaYfha .fõIK iy
leKsï j,ska ,enqKq i;aj YdL o%jH j,g wu;rj b;d jsYd, jYfhka ,efnk m%Odk udOH
jkafka Ys,d m;=rehs' </p>

<p> </p>

<p>is.srsh ) oUq,a, m%foaYfha lrk ,o jsYd,u .=yd leKsu ;=,ska j¾. ñg¾ ishhla
^100 m2)  ;=, Ys,d m;=re
125'000 la muK mrSlaId lrk ,oS (Adikari 1994). 
fuhska Ys,d fuj,ï yd w¾O jYfhka Ndjs; fuj,ï f,i m;=re 4000la muK yoqkd
.ekSug yelsjsh'  fïjd cHdñ;sl yd cHdñ;sl
fkdjk yev ;, j,ska hqla; jk w;r osjhsfka wfkl=;a m%d.a fM;sydisl ia:dk j,ska
,efnk idOl yd fnfyjska iudkh' tys ;dlaIKh wuqo%jH yev .eiajSfï udOHhka ;=,o
lsjhq;= ;rï jsYd, fjkila fkdue;' </p>

<p> </p>

<p> </p>

<p>.,a wdhqO</p>

<p> </p>

<p>Ys,d fuj,ï j¾.slrK lghq;= ms  Tjqka
iajlSh ld¾h moaO;sh jsjsO fldgia j,g fnod ;snSu yd ta i\yd tla tla wjs j¾.hka
ilid ;snSu tysoS b;d jeo.;a fõ' tu wdhqO udOH ksIamdokfha wuq o%jH
f;dard.ekSfïoS o Tjqka olajd we;af;a iyc l=i,;djhls' Ys,d m;=re fukau i;aj
fYaIhka Wmfhda.s lr.;a njg b;du meyeos,s idlaIs ;snqko Tjqka fï i|yd oej udOH
Ndjs;dl, njg idOl fYaIjS fkdue;'  thska
oej Ndjs;hla fkd;snqfka hehs woyia l, fkdyelsh' fudjqka laIqo% Ys,d mdjspzpshg
.eksfïoS oej udOHkaf.a wdOdr we;sj Ndjs;hg .;a njg idlaIs f,dj fjk;a rgj,ska
yuqjS ;sfnz'  fï i|yd fmr wmr foos.skau
idlaIs ,efnz' (Larsson 1983).  foaY.=Ksl
;;ajhka hgf;a YS% ,xldj ;=, oej udOHh laIKslj cS¾Kh jS jskdYhg m;afõ' flfia
jqjo fYaIs; o%jH ;=,ska yd wdhqO f.dkqj wkqj m%Odk jYfhka yoqkd.; yels ld¾hhka
lSmhls' tkï lemSu , isoqrelsrSu , iSrSu , ;e,Su , fmdvslsrSu yd weUrSu ta
w;rska m%Odkh' tu ld¾hhka ;=, wkq j¾. jYfhka wfkl=;a wjs j¾. Ndjs;hg .kakg
we;ehs is;Su ifya;=l fõ' Ys,d udOHkag wu;rj fhdod.;a wia:s udOHhka u.ska
i;ajhskaf.a wx tu whqfrkau wdhqO f,i Ndjs;hg f.k we;s w;r Yla;su;a wia:s fldgia
j,ska iQrk yd jsosk wjs ksIamdokh lrf.k we;' W,alr ilid.;a i;aj wjs i;=ka
ovhfïoS fhdod.;a njg idOl yuqjS we;' 
(Kencht 1994) .YS% ,xldfõ wkqrdOmqr f.vsf.a leKsï j,os if;l=f.a ysi yryd
.sh wia:s wjshla iys;j yuqjS we;' b;du jsr, jYfhka yuqjk fulS ksheos j,ska
;rula oqrg fyda Tjqkaf.a hï hï ld¾hhka yoqkd .ekSug yelsjS we;'</p>

<p> </p>

<p> </p>

<p>mqrd WoaNso jsoHd;aul wOHhk ;=,ska w;S;fha isg jojS .sh yd j¾;udkfha
;sfnk hk fohdldrjq idOl yuqjS we;' CIqo% fmdis, wdOdrfhka lrk ,o fï wOHkh
;=,ska Ydl j¾. 43la muK yoqkdf.k we;' fuhska j¾. 18 muK j|jS f.dia we;' fïjd
fndfyda oqrg tu foaY.=K jsm¾hdih u; isoqjq nj is;sh yelafla tajd f;;a foaY.=Khg
Tfrd;a;=fok YdL nejsks (Premathilaka 1994 (a)).  fï ms</p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p>YS% ,xldfõ ñksia weg iels,s wOHkh lrñka th  ol=Kq os. wdishdfõ    
wjqreoq </p>

<p>28 000 ;rï merKs b;sydihla we;s njg uydpd¾h flkvs woyia bosrsm;a lr
;sfnz'  (Kennady 1993)  ta wkqj ,xldfõ m%d.a fM;sydisl ia:dk 10 lska
muK udkj idOl yuqj ;sfnz' fï ish,a, ;=,g w,q;ska tl;=jsh hq;= jkafka iS.srsh )
oUq,a, m%foaYfhka ,enS we;s udkj wia:s ksheoSkah'  fïjd ms</p>

<p> </p>

<p> </p>

<p>udkj wjfYaI</p>

<p> </p>

<p> </p>

<p>iS.srsh)
oUq,a, m%foaYfha .=yd leKsï ia:dk follska udkj wjfYaI ,enS we;'  1994 tla ia:dkhlska udkjhl=f.a o;la yd b  fulS wOHkhka ;ju;a m¾fhaIK ugzgfï mj;s'
tfy;a fïjd j  ,xldfõ fn,a,kane|sme,eiafika
yuqj we;s wegiels,s ms 
(De¾niyagala P.1940) . fulS wegiels,s ms  (5000 BC) muK ld,fhaos f,dj mqrd N+ñodk lghq;= j,os fuu brshõj
nyq,j Ndjs;dlr ;sfnz'  ns%;dkH ,
Tiafgz%,shdj , YS% ,xldj, iajsvkh, pSkh, BY%dh,h jeks rgj,a fuhg fyd| ksoiqka
yuqfõ' tfy;a fï whqrska ñkS je,,Su isoq lf</p>

<p> </p>

<p>fï lreKq ish,a, idlÉPdjg Ndckh lsrSfuka iS.srsh oUq,a, m%foaYh  m%d.a fM;sydisl m%dfoaYsh wkkH;djhla fuu
m%d.afM;sydisl udkjhdg ;snqkdo hkak ia:srj lsj fkdyelsh'  tfukau Tyqf.a cSjk rgdjka jsYd, fjkila jqjo
hkak fidhd .ekSu wmyiq fõ' ;dlaIKh ixialD;sh w;skao wfkl=;a m%d.a fM;sydisl
wx.hka YS% ,xldfõ wksl=;a m%d.a fM;sydisl ia:dkhkaf.ka ,enqkq idOlhka yd
ieif|a'  oskjljdKq jYfhkao fuh ta
whqrskau mj;S' osjhsfkys flrS we;s m%d.a fM;sydisl ia:dk wOHhk j,skao ,o
oskjljdkq wkqj fufid,s;sl ñksid ls%(mq( 30"000 oS muK werÑ lS%(mq( 1000
jeks ld,h olajd ish .,ahq.h ;=, ,nd.;a udkj ls%hdldrlï iy wkHfkdkH wjfndaOh
iudc mrs{dKh ;=,ska ,o w;aoelSï ish,a, ;u cSjs;hg tl;=lr .ksñka bka miq
;dlaIKsl ils%h;djhka ;=,ska fmdaIKh jq cSjk rgdjlg t</p>

<p> </p>

<p> </p>






</body></text></cesDoc>