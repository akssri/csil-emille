<cesDoc id="sin-w-humanities-58" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-humanities-58.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>කෘතිය: කලාත්මක ඥානය සහ සැබෑ ලෝකය</p>

<p> </p>

<p>කර්තෘ: එරික් ඉලයප්ආරච්චි</p>

<p> </p>

<p>ප්රකාශනය: විජේසූරිය ග්රන්ථ කේන්ද්රය</p>

<p> </p>

<p>සුප්රකට ඉංගී්රසි විචාරවේදියකු වූ ෆ්රෑන්ක්
කර්මෝඩ් ඒ සම්බන්ධයෙන් ඉතා වැදගත් පිළිතුරක් සපයා ඇත.</p>

<p>      ~~;u
fräfïâj, § l 
idys;H fidrlula  (plagiarism) isÿ
ùu hs'  tfia Wÿrd .kakd ,o fþoh u.Zf;dfÜ
§ yuq jk" uyd mßudKsl j ksmojQ fohla jqj;a m%;sM,h tf,i u h'  túg" we;a; jYfhka isÿ jkafka idys;H
fld,a,h jvd;a meyeÈ,s ùu hs'  vqIdïf.a
fräfïâ l,d lD;sh yd iïnkaO kS;s ;¾lh rij;a úh yels h'  tkuq;a idys;H fräfïâ ms  lúhg hd
yels jeä u ÿr ks¾mqoa.,;ajh (impersonality) yd ffjIhsl;ajh (objectivity) Èkd
.ekSu hs'  ú,shïiaf.a ù,a nefrdaj yd
frdfí .%Sf,af.a fldams fldamamh óg kso¾Ykhls' 
tajd lú ksmehqï úkd ienE ù,anefrda yd fldams fldamam fkdfõ' tkuq;a tl yd
iudk kHdhsl Wml,amk u; tajd mokï ù ;sfí'~~</p>

<p>      fï  f,iska 
u   kQ;k  jd.a  
úµd;aul  ixl,am   o </p>

<p>රැඩිකල්වාදී විචාරයක නාම පුවරු වශයෙන් අද මෙරට භාවිතා
වේ.  ඊට පදනම් වන කිසියම් හෝ සාහිත්ය
දර්ශනයක් පවතී නම් එය ද අපගේ විචාරයට ලක් විය යුතු ය.  එබැවින් නුතන සාහිත්ය කලාවට අදාල වන නූතන වාග් විද්යාත්මක සංකල්ප හා
ආකල්ප සංක්ෂේපයෙන් හෝ විමසා බැලීම ප්රයෝජනවත් වේ.</p>

<p>      ixksfõokh
mokï lr .;a iudc ixúOdkhla ;= 
tu woyi wkqj ndysr f,dalh ms  tneúka tu NdId jHqyfhka iajdëk jQ ±kqula
ñksidg ,nd .; fkdyels h'  h:d¾:h
ms  ta wkqj NdIdj ;=  úYajh u; ish
wd{dpl%h m;=rejk NdIdj úfYaI ixl,am rgdjla yd m%j¾. iuQyhla ia:dms; lrhs'  tu NdId fldgq±,ska ksoyia ùug ñksidg
kqmqMZjk'  jgq jeoaodf.a l:dfõ mejfikakd
fia b.s  fukhska f.dúhd" lïlrejd" úoHd{hd"
foaYmd,k{hd" ieñhd" ìß|" mQclhd wd§ka fuka f,aLlhd o NdId
jHqyfha wdêm;Hhg hg;a j lghq;= lrhs'</p>

<p>      ;;= fufia
kï" NdIdj ;udf.a iqjp lSlre w;jeishl= f,i is;kakg mqreÿ ù isá idys;Hlrejdg
isÿ jkafka l=ula o@  ;u idys;HlrKfha
Wmdx.hla f,i NdIdj iu.Z l  w¨;a w¨;a jpk ksmoùug yd iïm%odhsl
NdIdfõ jeg kshr ì| w¨;a ,shÈ ;ekSug Tyq W;aidy orkq we;'  tys § Tyqf.a wjOdkh fhduq jkq we;af;a kQ;k
jd.aúµdfõ fiuekaál wxYh fj;g hs'  NdIdj
u.ska ñksidf.a w;a±lSï lafIa;%h u; fhdokq ,nk wdêm;Hh ch .ekSfï ,d NdIdj ;=</p>

<p>      f,dalh
ms  ta wkqj ñksidf.a w;a±lSï NdId ixfla; kue;s
mqkS,h ;= 
wjOdrKh lrkq ,nkafka NdIdj orovZqq wÉpqjla fkdj ks¾udKd;aul f,i j¾Okh
l 
iqm%lg bx.%Sis jd.a úoHd{hl= jQ fc*a ,SÉ tu ldrKh meyeÈ,s lrkafka
fuf,isks'</p>

<p>      NdIdj
;udf.a u wÉpqjla (grid- අපගේ අත්දීකීම් මත යොදන්නේ ය.  එසේ නැති නම් අපගේ විශ්වය සංවිධානාත්මක ක්රමයක්
බවට කුඩා කරන්නා වූ ලාච්චු රාශියක් තනන්නේ ය. 
fï woyi meyeÈ,s lrk foaj,a fldf;l=;a m%ldY ù ;sfí'  fuu ksÍlaIKh fya;= fldg f.k merKs NdIdfõ§ka
l,amkd lf  tu woyi wkqj bx.%Sish uõ ni lr.;a flfkl=f.a
f,dal oDIaáh yd weußldkq bkaÈhdkq iajfoaYslfhl=f.a f,dal oDIaáh w;r mr;rh
fnfyúka úYd, h'  iajdNdúl m%mxp
ms  NdIdj,
{dk jHqyh ms  th tf,i
y÷kajd we;af;a tlaoyia kjish úis.Kka yd ;sia .Kkaj, § tu woyi bÈßm;a l 
fimsh¾-fjda*a   ia:djrhg úreoaO j
fkdfhl=;a ;¾l ú;¾l bÈßm;a l 
iEu NdIdjla u.Zska u th Ndú;d lrkakka udkisl cïmrhla ;=  l=uk NdIdjl
jqj;a tl u m%mxph fjkqfjka úl,am ixl,amSlrK Ndú;d flf¾'  ksoiqka jYfhka bx.S%is NdIdfõ § orejka"
fh!jkhka iy jeäysáhka jYfhka fyda ksis jhialdr iy nd, jhialdr jYfhka fyda
ñksiqka j¾. l  tmuKla
fkdj  w¾:h yd wNsfèh w;r fjkila ols;
fyd;a wmg fufia lsj yels h'  tkï"
tla NdIdjl lsishï ixl,amhla fjkqfjka wkqrEmS ixl,amhla wfkla NdIdfõ ke;s jqj;a
th wmg úia;r lr lshd §ug mq,qjk'ZZ</p>

<p>      </p>

<p>අද
විලාසිතාව වී ඇත්තේ උපතේ සිට මරණය දක්වා මිනිසා ක්රමගත කරවන :චරදටර්පපැා- අන්දමේ
උපන් ගෙයි හැකියාවක් භාෂාව සතු ය යන මතය යි. 
fuhska fimsh¾ - fjda*a WmkHdih m%;slafIam fõ'  bka ms  úYajjHdmS fiuekaál
m%j¾. iuQyhla ^im%dKsl$wm%dKsl" ñksia$fkdñksia" ffjIhsl$wffjIhsl&amp;
we;s nj o" iEu NdIdjla u ;udf.a u Wm - m%j¾. iuqodhla bka Wmqgd .kakd nj o
mejfia'  NdId tlsfklska fjkia fjkafka tu
Wm - m%j¾. f;dard .ekSu yd tajd ixl,kh lsÍu i|yd we;s wjirh wkqjh'ZZ)</p>

<p>      NdIdj
orovZq wÉpqjla fkdjk neúka th ks¾udKd;aul f,i j¾Okh lsÍug fuka u ;j ;j;a mgq
lsÍug o yels h hk woyi fiuekaál ùuxidfõ § u;= ù fmfka'  tys § NdIdj mdúÉÑ lrñka w¾: wvq jeä fldg lSu
ms 
tkuq;a furg w¨;a idys;H úpdr fudaia;rh njg m;a ù we;s kj wdLHdk
läl=,mamqjg fudk u wdldrhl fyda fiuekaál úYaf,aIKhla ke;s nj mejiSu ksjerÈ h'  isxy, NdIdj ;=  kj wdLHdkhla ks¾udKh lsÍu i|yd ks¾foaYs;
NdIduh l%Svd" tu m%;s-l,d;aul wNsu;d¾:hka uqÿka muqKqjk tajd ñi isxy,
NdIdjg wdfõKsl fiuekaál O¾u;d  ks¾udKd;aul  f,i 
j¾Okh  lrk  </p>

<p> tajd fkdfõ'  tu Bkshd
wdLHdkh Ndú;d u.Zska isÿ ù we;af;a NdIdfõ iïm%odhsl bv  lv ;j;a mgq lsÍu hs'  w¾: ckk yelshdj ;j;a fudg lsÍu hs'</p>

<p>      Tjqka
fkdokakd ldrKh kï merKs isxy, lùka mjd Yíod¾: ks¾udKh w;ska Tjqkag jvd .õ .Kkla
bÈßfhka isá nj hs'  Tõyq rEmlhZ Tiafia
NdIdfõ iSud ch .;ay'  ks¾udKd;aul
l%shdj,shla u.Zska idudkH NdIdfõ yelshdj w;sl%uKh l  ksoiqka jYfhka" ia;%shlf.a Wl=f  idudkH NdIdj u.Zska ia:dms;
lr we;s w¾:h wkqj ia;%sh im%dKsl h' 
lr;a; frdaoh wm%dKsl h'  lr;a;
frdaofha .uk jD;a;dldr h' 
mqMZ,ql=f  ia;%shlf.a mqMZ¨l=  lr;a; frdaoh ixialD;sl
fuj,uls'  ;jo" kdß Wl=  tkuq;a lr;a; frdaoh m%fhdacH
jákdlñka hq;= NdKavhls'  by; ;ekl úia;r
lrk ,o jd.aúoHd;aul úYaf,aIKhg wkqj NdIdj u.Zska ixl,am ,dÉpq iuQyhla ksmojd
we;s w;r wmf.a w;a±lSï jdla ixfla; jYfhka yev .iajd ta ta ixl,am ,dÉpq fj;
fhduq lrkq ,efí'  NdIdfõ uQ,sl ld¾Hh
ms 
tkuq;a rEmlh udOHh lr .ksñka b;d ks¾udKd;aul f,i Wl=</p>

<p>      lùka
úiska Ndú;d lrk ,o tu Yíod¾: Wml%uh ±ka ±ka kjl:dlrejka o ;u wdLHdkh i|yd Ndú;d
lrk nj fmfka'  i,auka rEIaäf.a yd
wrekao;S frdahsf.a kjl:d NdIdj mqrd tjeks rEml Wml%u úµdudk fõ'  mdr yryd udreùu i|yd ly bß we¢ ia:dkh ^iSn%d
udrej&amp; f,i;a" rd.sl f,i Wl="elvis the pelvis, ලෙසත්,
අරුන්දතී රෝයිගේ නවකථාවේ සඳහන් වේ. 
l=vd tia; ,sx.sl w;jrhlska ,o w;a±lSu Yíod¾:j;a f,i úYd, fldg ±laùu
fojkqj lS fhÿfï  wruqK hs' tu w;a±lSug
miqìïjQ Ñ;%má Yd,dfõ îu fj  hk fhÿu Ndú;d fõ'  tu pß;h
ckm%sh ixialD;sl ;,hla Wv /|jqKq lgjqÜ iajrEmhlska hq;= j ksremKh lsÍu tu
mdßfNda.sljd§ rEmlfha wruqKhs'</p>

<p>      tkuq;a
tjeks Wml%uhlska kjl:dlrejl=g ,nd .; yels Yíod¾: m%fhdackh wvq h' Bg fya;=j lúh
NdId iQlaIu  udOHhla ùu yd kjl:dj
tjekakla fkdùu hs'  kjl:dfõ § tu
fiuekaál msïu we;s jk wdldrh meyeÈ,s lsÍu i|yd ud¾áka úl%uisxyf.a l,shq.h
kjl:dj ksoiqka lr .ksuq' iurùr kue;s fidr ieñfhl= úiskA kkaod fj; tjk ,o
,shqula wkq,dg yiq fõ'  weh ta ms  tu m%Yak
lsÍfuka miq wkq,df.a is;=ú,s oduh úia;r lrk úl%uisxyfhda iqj| ú,jqka iys; fïih"
levm;" kkaodf.a ksok ldurh muKla fkdj uq¿ uy;a ksji;a" mrf.dv
úydrh;a" .e,a .ukl § meoafoñka ±,a jqKq ,ka;Ereu;a" fld  ta ;rï úYd, øjHuh
ixialD;shla Ñ;%Kh jQ ta jdla Ñ;%h kkaod yd wkq,df.a we;=¿ is;a iuÕ wkqnoaO
ùfuka ta ish,a, ;=  fkdmej;=Kq w¨;a
w¾:hla u;= lrhs'</p>

<p>      tkuq;a
isxy, h:d¾:jd§ kjl:dfõ wdLHdkh wog fkd.e,fmk nj;a" kj ffY,Ska f.dvkÕd .;
hq;= nj;a mjiñka NdIduh úuqla;slduhla m%o¾Ykh lrk wm rg ú,dis;djd§ka wkq.ukh
lrkafka NdIdfõ l%shdldß;ajh ;j ;j;a mgq lrk ms</p>

<p>      kj wdLHdk jHdmD;shg wkqnoaO úpdrh ;=
±lsh yelafla tjeks ;;ajhls'  th mdßNdIsl
jpk" cd.ka yd fudaia;ßl fhÿïj,ska msÍ ;sfí'  NdIdfõ fiuekaál úYaf,aIKh uÕska fmkajd fokafka mdßNdIslrKh  (jargonization) uÕska NdIdfõ jHqyd;aul"
wd{dodhl iajrEmh ;j;a Yla;su;a ù ñksia w;a±lSï mgq iSudjl r|jk nj hs'  f,aLlhdf.a yd úpdrlhdf.a ks¾udKd;aul NdId
Ndú;h fudg jk nj hs' idys;H rplhd jeoaodf.a f.da;% NdIdj mú;% fldg Ndú;d l  Tyq
bka woyia lrkakg we;af;a NdIdfõ Yíod¾: m:h mq¿,a lr tys fiuekaál jákdlu ÈhqKq lsÍu
nj ksiel h'  kQ;k bx.%Sis NdIdfõ mdßNdIsl;ajh"
jHdc;ajh yd fudaia;ÍlrKh ms 
jHdc NdId ffY,Ska ksid Ñka;kh È  Tyqg fufia ,sùug isÿùu
mqÿuhla fkdfõ'</p>

<p>      </p>

<p>භාෂාව
අශෝභන වීමත්, නිශ්චිතාර්ථවත් නොවීමත් සිදුවීමට හේතුව අපගේ මුග්ධ සිතිවිලි
ය.  එනමුත් භාෂාව තුළ ඇති වන එම
බංකොලොත්කම නිසා අපට රකුස් සිතුවිලි ඇති කර ගැනීම වඩාත් පහසු න්න්වේ.</p>

<p>      kQ;k
idys;H l;sldj ;=  ld,amksl idys;Hhla fjkqfjka úYaj idys;Hh
ms  tu
iïm%mam%,dm úuid n,k úg fmkS hkafka Tjqka úYaj idys;Hh u; megùug W;aidy .kakd
u;jdo lsysmhla we;s nj hs'  j¾;udk úYaj
idys;Hh h:d¾: úfrdaë idys;Hhls' 
;d;aúl;ajh" úYajikSh;ajh" i¾jld,Sk;ajh" h:d¾:jdoh"
pß; ksrEmKh" iudc miq;,h hkdÈh merKs .;dkq.;sl úpdr ixl,amfhda fj;s'  ;jo leuQ" l*ald" fcdhsia wd§yq
iudc h:d¾:h fkdi,ld mqoa.,sl h:d¾:h u t</p>

<p>      </p>

<p>මෙවැනි දුර්මත පෙරදීරි ව කටයුතු කරන
විචාරකයින්ට සිංහල යථාර්ථවාදී සාහිත්යය ද නොපෙනීයාම පුදුමයක් නොවේ.  ගම්පෙරළිය මොඩලයක් පමණක් නොව සිංහල
නවකථාවේ සිරුර සිරගත කළ ජම්පරයක් මෙන් ද, සාහිත්යය සමාජභිමුඛ වීම ගතානුගතික
වීමක් මෙන් ද, නවකථාවේ අතීතය හා වර්තමානය එකට ඈඳිය නොහැකි zශානන්ර දෙකක් මෙන් ද
ඔවුන්ට පෙනී යාම කවර නම් පුදුමයක් ද? 
tkuq;a fuu úpdruh wmirKfha uq¿ jro hf:dala; úpdrlhka u; u megùu o idOdrK
nj fkdfmfka'  Bg fya;=j iudchSh ldrKdj,
ÈhqKqjg fuka u mßydkshg o wLKav;djla ;sîu hs' 
b;d l=vd l,d;aul .eg¨ mjd iuia; iudc ixúOdkfha yd YsIagdpdrfha .eg¨ f,i
f;areï .; hq;= j we;af;a tneúks'</p>

<p>      </p>

<p>ඒ අනුව ජනතාවාදය හා සෝවියට් වර්ගයේ
ජනතා සාහිත්යය වර්තමාන නව ආඛ්යාන ශෛලිවාදයේ පූර්වගාමියා ලෙස හඳුනා ගැනීම යුක්තියුක්ත
ය.  මුන්ථමහත් ශතවර්ෂයක කලාත්මක අස්වැන්න ලෙස
ගිනිය හැකි නූතනවාදි සාහිත්යය මතු නොව නූුතනවාදී චිත්ර කලාව හා මූර්ති කලාව ද සෝවියට්
කලා පටලයෙන් මුවා කළ ජනතාවාදියෝ එහි වූ වටිනා කලාත්මක සම්පත් මුන්ථල්ල වෙතින් රසික
සිත්සතන් දුරස්ථ කළහ. ඊට පෙර බිහි වූ යථාර්ථවාදී සාහිත්යය පිළිබඳ කිසියම්
සැකයකුත්, නූතනවාදය පිළිබඳ ගෞරව සම්ප්රයුක්ත බියකුත් සිංහල පාඨක සිතෙහි මතු
වී ඇත්තේ මේ නිසා විය හැකි ය. 
mqIalska" f.df.d,a" f;d,aiaf;dahs" fodiaf;dfhõials"
f.da¾ls" fpfld*a wd§ reishdkq f,aLlhskaf.a o" fjda,ag¾ iafldÜ"
pd¾,aia älkaia" fn%dkafÜ ifydaoßfhda wd§ bx.%Sis f,aLlhkaf.a o" úlag¾
yshqf.da" .S o fudamidkaÜ" we,afnhd leuQ wd§ m%xY f,aLlhkaf.a o hdjÔj
mdGlhka jYfhka oYl .Kkdjla buy;a Nla;shlska hq;= j iïNdjH idys;Hh mßYS,kh l  ú,dis;djd§
úpdrlhska tu mdGl iudchg ksfhda.d;aul f,i lshd isákafka kj lshùulaZ werôh hq;=
nj hs'  tys ngysr w¾:h fiuekaál m%fõYhla
Tiafia idys;H lD;shl u;=msg NdId rEmh w;sl%uKh fldg jHqyhZ lrd .uka  lsÍu jqj;a" mQ¾fjdala; Y%S ,dxlsl mdGl
ikao¾Nh ;=  m%;s - l,d jHdmdrhla úiska m%;s - lshùula fhdackd lsÍu o
;¾ldkql+, h'  tu jHdmdrfha úpdrl
uKavmh" h:d¾:jd§ kjl:d iïm%odfha .eg¨ ms</p>

<p> </p>

<p>      nhsika;shdkq
l,dj" mqkreo l,dj" m%nqoaO;d l,dj" m%xY úma,ùh l,dj fuka u
kQ;kjdoh o úpdrhlska f;dr wNspdruh w¾:hlska f;areï .ekSu iïnkaOfhka l,dlrejl=g
,efnk ksoyi yd iudj úpdrlhl=g ,nd .; fkdyels h'  l,dj iïnkaOfhka mjd wNspdruh jákdlu OfkaYajr m%;sksIamdokh uÕska
wfydais lr ±uQ neõ fjda,ag¾ fnkacñka mäZjrhd úiska fmkajd ÿka nj m%isoaO
ldrKhls'  tkuq;a ú,dis;djdoh ;= 
ú,dis;djdofha o mokï ixl,amh ù we;af;a OfkaYajr m%;sksIamdokh u.Zska
l,dfjka bj;a lr ouk ,o mQcduh m%Nd uKav, (aura) ස්වභාවය ආයතනික ලෙස නැවත පණ ගැන්වීමට උත්සාහ කිරීම යි.  කලාව වෙත නැවත ආගමික ප්රභා මණ්ඩල තත්වය ප්රදානය
කිරීම පැසිස්ට්වාදයට අඩු දෙයක් නොවන බව වෝල්ටර් බෙන්ජමින් විසින් පෙන්වා දුන් අයුරු
පැහැදිලි කරමින් ෆ්රෑන්ක් කර්මෝඩ් මෙසේ පවසයි.</p>

<p> </p>

<p>      wo
Wmß-jHqyh ;=  fjda,ag¾ fnkacñka úiska ,shk ,o  ld¾ñl m%;sksIamdok hq.fha l,d lD;shZ kue;s
b;d uelaÆykarEmS  ,smsh'  fuu fjkialï ksid l,dfõ iuia; iajrEmh;a"
foaYmd,k meje;au;a wo fjkia ù we;' mQckSh;ajh fyj;a m%Nd uKav, ,laIKh wfydais ù
hdu iuÕ w;sZk; .;a ;dlaIKsl m%;sksIamdok yelshdj (reproducibility) ta w;ßka
m%Odk;u fjki hs'  l,dj ms  Bg fya;=j
ks¾udKl;ajh" W;alDIaG fm!reI m%Ndj" iodld,sl w.h" .+V;ajh jeks
ixl,am meisiaÜjdoh ÈYdjg lreKq ilia lsÍu fj; fhduq ùu hs'ZZ)</p>

<p> </p>

<p>      b;d,s
mqkreoh ;= 
;;= tfia kï kQ;kjdoh foi o ta yd iudk ft;sydisl oDIaáhlska ne,sh hq;=
h'  tys § OfkaYajr l%uh ;=  kQ;kjd§ hq.fha uq,a jljdkqj
;=</p>

<p> </p>

<p>      oykjjk
Y;j¾Ih wjidk Nd.fha § iudcfha ienE ;;ajh ms  mqkH fÉ;kd fmr±ß j l%shd lsÍu
fjkqjg iudc jro fjkqfjka jkaÈ f.ùfï jqjukdj Tjqka ;=  jeka f.da ;rï m%n, f,i wka ljfrl= fyda th
m%ldY lf  Ñ;%" we£ï"
,shqï yd Ôú;h uÕska Tyq th m%ldY lf 
ñksia wd;auh ms 
we;s fyd| u jd¾;d w;r uq,a ;ekla Tyqf.a ,shqï lshqïj,g ,efí'  tajdhska fmkS hkafka Tyq ±ä wd.ï Nla;sfhka
wkQk mqoa.,hl= jQ nj hs'  ;u jev lrk
Ôú;fha uq,a Nd.fha § Tyq jD;a;Ska folla ueo w;rux ù isáfha h'  tkï" is;a;r jD;a;Sh yd wd.ñl foaYl
jD;a;sh hs'  we;eï úg fmruqfKa isáfha
is;a;rd fkdj wd.ñl foaYlhdKka h'  tkuq;a
foaYkd lsÍfuka muKla ;Dma;su;a ùug Tyqg mq¿jkalula ;snqfKa ke;'  idka; m%ekaisia fuka ;u ifydaor ckhdf.a
b;du;a È  we;af;ka u ld¾ñl fn,aðhfï
;;ajh oy;=kafjks Y;j¾Ifha Wïì%hdfõ ;;ajhg;a jvd ÿlaÅ; úh'  Tyq foaYkdjd§ Ôú;hg wdhqfndajka lSfõ ÿla.eyeg
ksid fkdfõ'  is;=jï we£u i|yd jQ fkd;s;a
wdYdj ksihs'  fudk ;rï ffO¾Hhlska yd
wNsudkhlska hq;= j ukqIHhka ;u Ôjk wr.,hg uqyqK fokafka o hk nj ksrEmKh lrkq
msKsi is;=jï ks¾udKh lsÍfuka hg lS Ñ;a;dNHka;r .egqu ch .ekSug Tyq uq, § W;aidy
±Í h'  È  ñf,a Tyqf.a foúhd úh'  tla
i;shla ;=  uqMZ Ôú; ld,h mqrd Tyq
ñf,af.a l,d rEm m%;sks¾udKh lsÍfuys yd msgm;a lsÍfuys ksu.ak j isáfha h'  ol=KqÈ. §ma;su;a iQ¾hdf,dalh úiska Tyqf.a
j¾Kdj,sh w÷re fld</p>

<p>      jeka
f.daf.a ùrhd jQfha oykjjk ishjfia wjidk Nd.fha </p>

<p>ජීවත් වූ ත්යාගශීලී මිනිසුන් අතරින් පළමුවැන්නා වූ ලියෝ
තොල්ස්තෝයි ය.  මිනිසුන් වෙත ළං වීමේ
ආශාව නිසා තොල්ස්තෝයි සිනමාවේ පැමිණීම ද සාදරයෙන් පිළිගත්තේ ය.  චිත්රපටි තිර නාටකයක් රචනා කරන්නට
තොල්ස්තෝයිට  අවශ්ය විය. ප්රමාදය ඊට හරස්
විය. එනමුත් තමා චිත්රපට ගත කරගන්නට ඔහු සමත් විය. 
tu ksid f;da,aiaf;dahs kue;s fY%aIaG ñksid Ôj;a jQ ieá" tyd fuyd
.sh ieá" ±l n,d .ekSug wo wmg mq¨jk' Tyq ,S brk yeá" jev lrk
ñksiqkaf.a Ôú;h fnod .; hq;= h hk ye.ZSu m%ldY lrk ieá wms olsuq'  mSvlhl= f,i .; l  Tyq f.dùka iu. l:d lrk
ieá yd ;u wYajhd msgg k.Zsk ieá muKla fkdj ;u isgqjr ìßh iu.Z fkdi;=áka md k.Zk ieá o wmg fmfka'  vdkafÜ" uhsl,exðf,da yd
fíf;dajka fuka Tyq o ;u hq.hg by  fkdkefik mßl,amkfha m%d;syd¾Hh Tyqf.a kjl:d ;=  tkuq;a Tyqf.a Ôú;h wiaÓr;dj,ska msÍ  ;sìK' 
f.dùkaf.ka tfll= ùug Wjukd jQ kuq;a Tyq È.g u ro,hl= f,i Ôj;a úh'  úYaj fm%auh foaYkd l 
jhi wjqreÿ wiQfofla § Tyq weh yer od .sfha h'  ìhckl iajmakhla jeks jQ rd;%S .ukska miq Tyq .ïno ÿïßh iafÜIul
weo jegqfKa h'  kùk udOH m%pdrl
l,ne.Eksh msg; isÿ fjoa§ Tyq iafÜIka udiag¾f.a f.or ihkh u; wjika yqiau fy  Tyqf.a wjika jpk fïjd h'  f.dùka wjika yqiau fy,kafka flfia o@ZZ
Tyqf.a urKfhka miq f.dùka wd.ñl je,ems,s hdÉ[d .dhkd lroa§ th je 
wjux., fmryr je  th o ms  ksïk N+ñhla Èf.a fmryr we§ we§ hk ieá
o" ì÷Kq y±;s wkq.dñlhskaf.a bßhõ o wms olsuq'  fuh w¨;a wdldrhl ft;sydisl jd¾;djls'  is;a l,Ujk iqMZ jd¾;djls'ZZ</p>

<p> </p>

<p>එම දර්ශනය රන්ග දීක්වුණේ 1910 දී ය. ඉන්
අවුරුදු දෙකක් ඇතුළත දී රදර්ෆර්ඩ් හා අයින්ස්ටයින් තම තමන්ගේ ප්රථම සොයා ගැනීම් සිදු
කළහ.  මෙලෙස 1914 යුද්ධයට පෙර නව
යුගයක් උදා විය.  අප අද ජීවත් වන්නේ එම
යුගය තුළ න්න්ය.- </p>

<p>      </p>

<p>නූතනවාදයේ පෙරටුගාමී බලඇණිය චිත්ර
ශිල්පීන් විසින් නියෝජනය කරන ලද බවත්, ඔවුන් චිත්ර කලාව තුළ ඇති කළ
අභිවර්ධනය හේතු කොට ගෙන නූතනවාදී චිත්ර කලාව නමැති නව ප්ලාස්ටීය කලා ප්රභේදය බිහි වූ
බවත් අමතක කළ නොහැකි ය.  එනමුත්
සාහිත්යය තුළ සිදු වූ නූතනවාදයේ ව්යාප්තිය ඒ අයුරින් ම තේරුම් ගැනීම වැරදි ය.  සාහිත්යය දෙස ද එබඳු දෘෂ්ටියකින් බලා ලේඛකයන්
වර්ග කළ විචාරකයෙකු වූ සිරිල් කොනෝලිගේ සාහිත්ය ව්යාපෘතියක් පිළිබඳ ව
ෆ්රූන්ක් කර්මෝඩ් විසින් ඉදිරිපත් කර ඇති විචාරයක් මෙසේ ය.</p>

<p>      </p>

<p>ප්රංශයේ සිට එංගලන්තය හා ඇමරිකාව
දක්වා පැතිරිණු නූතනවාදය, 1910-25 අතර වකවානුවේ දී කූට ප්රාප්ත වී යැයි කොනෝලි
පවසයි.  මෙය සත්යයක් වූවත් වර්ජිනියා
වුල්ෆ්ගේ හා යේට්ස් කවියාගේ මරණයෙන් පසු ව එය පරිහානිගත ව්යාපාරයක් වී යැයි නිගමනය
කිරීමෙන් ප්රශ්න කිහිපයක් මතු වේ.  සැමුවෙල්
බෙකට්, රොබේ, - ග්රීලේ හා බියුටර් ඔහුගේ කාල රාමු සීමාවෙන් පිටත සිටින්නන්
බව පවසා එම ප්රශ්නවලින් යම් පමණකට බේරී සිටීමට කොනෝලි උත්සාහ දරයි.  ඔහුට අනුව ඉච්චාභංගත්වය රජ කළ හතළිස්
ගණන්වල න්දී එම ව්යාපාරය කෙළවර දුටුවේ ය. බරෝස් සහ බීට්ස් වැන්නවුන් හුදෙක් මත්ද්රව්ය
භාවිතය හා සමලිංගිකත්වය පිළිබඳ ව ඇමරිකාවේ නොවෙනස් ආකල්ප සමග ගැටෙමින් නූතනවාදී
කැරලිකාරිත්වය නොකඩවා පවත්වා ගෙන ගියෝ ය. 
wkd.;h ms  fumuKls'  tkï kQ;kjdofha
ksjqka ore ,laIKh jQ nqoaêh flfrys jQ Nla;shZ yd mßl,amkfha j,x.=;dj ms</p>

<p>      </p>

<p>මේවා සියල්ල අතිසරල කියමන් ය.  ඓතිහාසික වීමංසාව ලෙස වෙස් ගන්වා ඇත්තේ
ලේබල් ඇලවීමන්න්කි.-</p>

<p>      </p>

<p>නූතනවාදයේ ලාංකික මුහුණුවර පිළිබඳ
ව මෙරට පැතිර ඇත්තේ ද මුන්ඵමනින් ම වංචනික මතයකි.  මෙරට නූතනවාදය මහගම සේකරගෙන් ආරම්භ වූ බව කල්පනා කරන පිරිසක් ද
සිටිති.  තවත් සමහරු නව ආඛ්යානයක මාර්ගයෙන්
නූතනවාදයට මුල පුරන්නට බලා සිටිති.  මහගම
සේකර දෙස පමණක් නොව මුන්ඵ මහත් නිසඳැස් ව්යාපාරය දෙස මෙන් ම ගීත සාහිත්යය දෙස ද
බැලිය යුත්තේ මෙරට නූතනවාදය ද සමග මතු වූ ගැටලු ලෙස විනා කූටප්රාප්ති අවස්ථා ලෙස
නොවේ.  මෙරට නූතනවාදය සම්බන්ධයෙන් මෙතෙක්
ලියවුණු විශිෂ්ටතම කෘතිය එදිරිවීර සරච්චන්ද්රගේ සිංහල නවකථා - ඉතිහාසය හා විචාරන්ය බව
සඳහන් කළ යුතු ය.  එය ලේබල් ඇලවීමේ
කලාවට අනුගත නොවු සැබෑ ඓතිහාසික වීමංසාවකි. මෙරට නූතනවාදය ආගමික වාද විවාද,
පරිවර්තන සාහිත්යය හා පියදාස සිරිසේනගේ නවකථා ඔස්සේ බිහි වී මාර්ටින් වික්රමසිංහගේ
නවකථා, ලෙස්ටර්ගේ චිත්රපටි හා සරච්චන්ද්රගේ නාටක ඔස්සේ කූට ප්රාප්තිය කරා ළන්ගා
විය.  ගුණදාස අමරසේකර යළි උපන්නෙමි රචනා
කිරීම මෙන් ම එය ප්රතික්ෂේප කිරීමත්, මාර්ටින් වික්රමසිංහ බවතරණය රචනා කිරීමත්,
ධර්මසේන පතිරාජගේ සිනමාවත්, මෙරට නූතනවාදයේ විවාද සම්පන්න අවස්ථා ලෙස සැලකිය
හැකි ය.  බටහිර නූතනවාදයේ විශිෂ්ට අවස්ථා
අනුකරණය කිරීමෙන් හෝ නූතනවාදයේ ජනප්රිය සංකේත ප්රතිනිර්මාණය කිරීමෙන් හෝ මෙරට
නූතනවාදය බිහි කළ හැකි බව කල්පනා කිරීම විහින්ඵවකි.  මැක්සිකෝවේ නූතනවාදය කූට ප්රාප්ත වූයේ දියාගෝ රිවේරා විසිින්
ගොඩනැගිලි බිත්ති මත නිර්මාණය කරන ලද අතිවිශාල බිතු සිතුවම් නිසා බව සිහිපත් කිරීම වටී.  මානවවාදය නොව එහි  ප්රතිපාර්ශවය වූ අමානවීයත්වය නූතනවාදයේ නිල ලකුණ බවට පත් වූ සමයක පවා
ආන්ද්ර ජීද් වැනි නවකථාකරුවන් තම නිර්මාණ මන්ගින් මානවවාදය ප්රකට කළ බව විචාරකයෝ
පෙන්වා දෙති.  සාහිත්යයේ නූතනත්වය
පිළිබඳ ව මෝස්තරවාදී විචාරකයන් විසින් පසුගිය අවුරුදු දෙක තුන පුරා ලියු කියූ දේවල්
මන්ගින් මෙරට ලේඛක - පාඨක දීනුමට වචනයකුදු එකතු නොවූයේ නූතනවාදය දෙස පුන්ඵල්
දෘෂ්ටියකින් බැලීමට අපොහොසත් වීම නිසා ය. 
tmuKla fkdj furg idys;H l,d ;=  ksi|eia lúfha wid¾:l;ajh bÈßfha yqjd olajkq
,nk fudksld rejkam;srK lsú¢hf.a fndfyda lú ;=  Tjqkaf.a W;aidyh ù we;af;a
isxy, idys;H jHdmdrh mßmdájdohla njg mßj¾;kh lsÍu h' Tõyq furg kQ;kjdoh yd
mYapd;a kQ;kjdoh tu mßmdáh iuÕ wkkH lsÍug ;e;a or;s'  ta wkqj kï j¾;udk idys;Hldókaf.a m%Odk fufyjr úh hq;af;a ish¨
w;S; ixialD;sl ch.%yK bj; ,d tu mßmdáhg .e,fmk mßÈ ixialD;sh j¾Okh lr .ekSu
h'  fuh ;ukaf.a ck;djd§ idys;H mßmdáhg
wkql+, j idys;H j¾Okh lr .kakd f,i f,aLlhkag ksfhda. l</p>

<p>            kjl:d
l,dfõ iajrEmh wdLHdkjd§ f,i fjkia lsÍug;a" tu.Zska ;u wNsu;fha uqødj
;eîug;a W;aiql jkakka ;=  wdLHdk ud¾.h
fjkiaùu yd h:d¾:jdoh msgq±lSu hk ldrKd fol kjl:d flál:dj, fuka u Ñ;%má yd fg,s
kdglj, o w.h úksYaph lrk ñKqïovZq f,i Ndú;d lsÍfï m%;sM,h ù we;af;a l,ams;
Í;shlska ks¾udKh jQ fyd,sjqâ yd yskaÈ m,ayE,s jeks fnd</p>

<p>      tjeks ñKqïovq furg f,aLlhka yd mdGlhka u;
megùug fhduq ùfuka m%lg jkafka idys;H l,dfõ ld¾hNdrh ms  w;r tu.Zska j¾;udk ixialD;sl Ôú;h wrnhd W.;
yels mdvï /ila ;sfí'  kQ;kjd§ hq.h
;=</p>

<p>      oykjjk
Y;j¾Ifha wjidk Nd.fha § à'tÉ'yiala,s ixialD;sh iy wOHdmkhZ hk ud;Dldfjka hq;= j
mj;ajk ,o foaYkhl § mejiqfõ hqfrdamSh wOHdmk l%uh ;= 
;d¾lsl i;Hh yd øjHuh m%dfhda.sl;ajh fjkqfjka wemlem ù we;s kùk hq.hg
wjYH jQ ±kqu imhkafka ixialD;sh fkdj úoHdj nj Tyqf.a woyi úh'  kQ;k f,dal ikao¾Nh ;=  hqfrdamfha fuka u
weußldfõ o mqMZ,a wjOdkhlg ,la jQ tu woyig úreoaOj újdohg ng ue;sõ wd¾fkda,aâ
mejiqfõ wOHdmk l%uh ;=</p>

<p> </p>

<p> </p>

<p>      1950
.Kkaj, § ta yd iudk ;j;a újdohla ngysr f,dalh ;=  iS'mS'iafkda úiska 1959 § m%ldYhg m;a lrk ,o
ixialD;s follaZ kue;s ,smsh flakaø fldg f.k tu újdoh mek ke.Z=fKa h'  m%lg idys;HOrhl= jQ we*a'wd¾ ,Sõia ;rula
m%udo ù" tkï 1962 § tu ,smsh úfõpkh lsÍu;a iu.Z u újdoh w¨;a úh'  yiala,s - wd¾fkda,aâ újdohg f;aud jQfha
ixialD;sh yd úµdjhs'  iafkda - ,Sõia
újdofha § ixialD;sh ms  ù ;snqfKa h'</p>

<p> </p>

<p>      ixialD;s
follaZZ kue;s tu ksnkaOh werfUkafka úµd{hska iy idys;Hlrejka w;f¾ in|;d ì| jeàu
ms  tu fomd¾Yjh
w;f¾ wfkHdkH wjfndaOhla ke;s nj;a" tlsfkld ;u ;ukaf.a nqoaêuh l=àr ;=  fuu ÿria:ùfï
j.lSu fomd¾Yjh úiska u ±ßh hq;= kuq;a jeä j.lSu yd jro megfjkafka idys;Hlrejka
u; nj iafkda m%ldY lf  Tyqf.a tu
fpdaokdj b;d nrm;, tlls'  wm fjfik hq.fha
idys;Hlrejka oyfokl=f.ka kjfofkl= u foaYmd,k jYfhka   o jk nj fyf;u fmkajd ÿkafka
h'  ví,sõ' î' fhaÜia" tiard mjqkaâ
jeks fYa%IaG lúfhda o Tyqf.a pQÈ; kï ,ehsia;=jg we;=  ysÜ,¾ úiska oyia ixLHd; hqfoõjka >d;khg
,la lrk ,o TIaúÜia isrl|jqr udkj YsIagdpdrh fj;g /f.k tafuys ,d
idys;Hlrejkaf.ka o ie,lsh hq;= ;,a¨jla ,enqKq nj iafkdaf.a woyi úh'  Tyqf.a ks.ukh jQfha úµd{hskaf.a ;;ajh t;rï
krl ke;s nj hs'  ñksia ixy;sfha wkd.;h
ms 
iïm%odhg wkqj ixialD;sh jQ l,S idys;Hh hs' úµdfõ j¾Okh iuÕ tu iïm%odh
fjkia úh hq;= jQ kuq;a ngysr f,dalh ;ju;a md,kh fjkafka tu merKs wdl,am u.Zska nj
Tyq mejiS h'  ñksidf.a ÈhqKqjg fya;=
idOl jQ ld¾ñl úma,jh ms  ld¾ñl  úma,jfha w÷re me;a;  ±lSug úkd tu.ska ñksia ixy;sh fjkqfjka újD;
lr fok ,o úNj;djka f;areï .ekSfuys ,d tl, úiQ f,aLlfhda wiu;a jQy hs Tyq i|yka
lf</p>

<p> </p>

<p>      tu fpdaokd ud,dfjka
ikd: jk uQ,sl ldrKh kï úµdj iy l,dj tlsfkl iu.Z wkkH l  h:d¾:jdofhka neß kï woaN+; Í;shlska fyda
,shd iïm%odhsl idys;H ixialD;sfhka ksoyia ù ,snr,a ixialD;shla lrd fmr .uka
wrUk f,i h" Tjqka furg f,aLlhskag mjikafka'  Tjqka fkdokakd m%Odk ldrKh kï tod fuka u wo;a ,snr,ajdoh idys;Hlrejkaf.a
is;a f.k ke;s nj hs'  iqm%lg idys;H
úpdrlhl= jQ á%,sx tu ldrKh fuf,i meyeÈ,s lrhs'</p>

<p>      </p>

<p>විසිවෙනි
ශතවර්ෂයේ මුල් දශක තුන තුළ දක්නා ලැබෙන කුතූහලය දනවන සුන්ඵ සංස්කෘතික කාරණයක්
තිබේ. එනම් පොත් කියවන උගත් ජනතාව වඩ වඩාත් ලිබරල්වාදී හා රැඩිකල්වාදී වෙද්දී
පළමුවෙනි පන්තියේ එක ම සාහිත්යකරුවෙක්වත් ලිබරල් හා රැඩිකල් අදහස් වලට ගරු
නොකිරීමයි. එසේ ලිබරල් හා රැඩිකල් මතවලට ගරු කරන ලද දෙවෙනි පංතියේ සාහිත්යකරුවෝ
නම් බොහෝ දෙනෙක් සිටියහ.</p>

<p> </p>

<p>      මාර්සෙල් ප්රූස්ට්, ජේම්ස් ජොයිස්,
ඩි.එච්.ලෝරන්ස්,    ඩබ්ලිව් බී.
යේට්ස්, තෝමස් මාන්, ෆ්රාන්ස් කෆ්කා, ආන්ද්ර ජීද් වැනි මහා
සාහිත්යකරුවන් සතු ව ඔවුන්ගේ ම වූ මානව භක්ත්යාදරයක් පැවතුණේ වී නමුත් එය උගත්
පංතියේ නම්බුකාර ලක්ෂණයක් ලෙස ප්රකාශිත ලිබරල් ප්රජාතන්ත්රවාදය සමඟ බැඳුණු අදහස් හා
හැඟීම් කෙරෙහි වූ රුචියක ස්වරූපය ගත්තේ නැති බව ලයනල් ටි්රලිං තවදුරටත් පවසයි. කථාව
විනා එය පවසන කථාකාරයා විශ්වාස නොකරන්නැයි ඩී.එච්.ලෝරන්ස් පැවසූ සැටි ද ඔහු සිහිපත්
කරයි. ඔරමිඑ එයැ ඒකැ ්බා බදඑ එයැ එැකකැර ද එයෙැ ඒකැලෆ</p>

<p> </p>

<p>                        2
</p>

<p>      idys;H
l,dfjka laIKsl fjkialï b,a,d isàu l=uk rgl idys;Hhla iïnkaOfhka jqj b;d wufkda{
l%shdjls'  j¾;udk kjl:d idys;Hfha
fmruqfKa isákafkl= jQ fcdaka nd;a idys;H l,dj iïnkaOfhka m</p>

<p>      b;sydih hï
muKlg ;:H fohls'  tu ksid úisfjks
Y;j¾Ifha l,dj oykjfjks Y;j¾Ifha l,djg o" oykjfjks Y;j¾Ifha l,dj oywgfjks
Y;j¾Ihg o hkdÈ jYfhka hï muKlg fjkia ùug fya;= ;sfí'  wfkla w;g oYl lsysmhla m%nkaO l:d rpkd l  tkï" NdIdj kue;s
iq,n wuqøjHfhka ilia jQjla jYfhka idudkHfhka wfkla l,djka iuÕ ixikaokh lrk úg
idys;Hh tajd ;rug fjkiaùug wleu;s wlSlre l,djla jk nj hs'  fuh úfYaIfhka u .µ m%nkaO iïnkaOfhka
i;Hhls'  iuyr úg Bg fya;=j ckm%sh
ixialD;sh ;=</p>

<p>      fcdaka
nd;af.a ;j;a jeo.;a ks.ukhla fj; wmf.a ie,ls,a, fhduq lsßu jà' tkï" fï
Y;j¾Ifha weußldkq idys;Hfha ikaêia:dk f,aLlhska (landmark writers) fndfyda úg
wdlD;s w;ska fyda l,d ;dlaIKh w;ska fyda 
w¨;a w¨;a oE fidhd .shjqka fkdjk nj hs' 
iafldÜ  *sÜiafcr,aâ" w¾kiaÜ</p>

<p>හෙමින්වේ, ජෝ කෙලර්, රැල්ෆ් එලිසන්,
සෝල් බොලෝ, ෆිල් රොත් හා ජෝන් අෂ්ඩයික් යන ගත්කරුවන් ඔහු ඒ සඳහා
නිදර්ශන කොට ගනියි.  අප එම ලේඛකයින් අගය
කරන්නේ ඔවුන් ආකෘති අතින් හා තාක්ෂණය අතින් විස්මය දීන වූ නිසා නොවේ යැයි ඔහු
පවසයි.</p>

<p>      fcdaka nd;a
tu ksjerÈ ks.ukh lrd m%úIag jkafka hglS .;alrejka Tyqf.a m%sh;uhka fyda Tyqg
.=rejQjka ksid fkdfõ'  fuh wmf.a
fudaia;rjd§kag fuka u ish m%sh;u f,aLlhka fo;=ka fofkl=g wkqj uq¿ uy;a idys;Hh
úkh.; lsÍug ;e;a lrk fjk;a w,amY%e;hskag o lÈu mdvuls'  ;udf.a m%sh;uhka jkafka hglS ikaêia:dk
f,aLlhska fkdj wdlD;sh yd l,d ;dlaIKh me;af;ka w;ayod ne,Sï lrk ,o ¨hS
fndacia" ieuqfj,a fnlÜ" je,aÈó¾ fkdfndafldõ" whs;df,da  le,aùfkda" frdfí .%Sf,a" fcdaka
fydlaia" ú,shï .dia" fvdk,aâ ndf;,añ jeks .;alrejka nj Tyq fkdiÕjd
mjihs'  kj w;ayod ne,Sï ms  idys;Hfha l,d;aul w§k;ajh yd Ydkßh iaùh;ajh
iqrlaIs; lr .ekSug;a" w¨;a w;ayod ne,Sï ms  idys;Hh i;= wkkH;d ,laIK ms</p>

<p>      b;sydifha
tla;rd wjêhl § Ñ;% l,dj ksi, PdhdrEm l,dfjka fjfiid fjka j ish wkkH;dj ;yjqre
lr .;a mßÈ wm o wjOdkh fhduq l</p>

<p> </p>

<p>      úisfjks
Y;j¾Ifha we;eï f,aLlhska fia wdLHdk ud¾.fha § ld,h ms  fuh ñ,dka l=kafoard" i,auka
rEIaä" wrekao;S frdahs jeks f,aLl f,aÅldjkaf.a idys;Hh ;=  fcdaka nd;a
mjikafka ,sùu fuka u lshùu o f¾Çh lghq;a;la jk njhs'  ta wkqj f,aLlhdf.a Yla;sh fhduq úh hq;af;a l¾;D - l%shdj - l¾uh -
úrdu ,l=Kq hkd§  jYfhka msgqfõ jfï isg
ol=Kg úysfok jpk uÕska .%yKh lr .; yels ñksia wkqN+;sfha idys;H md¾Yjh fj;g
h'  idys;H udOHhg wkql+, fkdjQ foaj,a
n,y;aldrfhka ta ;=</p>

<p> </p>

<p>      wo furg
idys;H f,dalh ;=  ngysr Wiia
idys;H Ñka;lhka ;=</p>

<p>      kj mrmqf¾
;j;a m%lg f,aÅldjl jk fcdhsia lefrd,a ´Üia yd fn,dñ kue;s weußldkq úpdrlhd w;r
we;s jQ ixjdohl fldgila my; olajuq'</p>

<p> </p>

<p> h:d¾:jd§ iïm%odfha iïu;hka ì| ±óug W;aidy lrk f,aLlhska bÈßfha
we;af;a ljr wdldrhl bv m%ia:d o@</p>

<p> </p>

<p>අවුරුදු ගණනකට පෙර, දශක ගණනකට පෙර
ප්රබන්ධ කතාකරුවන් යථාර්ථවාදී සම්ප්රදායේ සම්මත බිඳ දීම්මා.  සාහිත්ය විචාරයේ සුලබ අදහසක් වී ඇත්තේ සම්මත බිඳ දීමීම, ටි්රස්ට්රෑම්
සෑන්ඩි (ලෝරන්ස් ස්ටර්නි 1760 දී ලියූ නවකථාවක්) කෘතිය තරම් ඈතට විහිදෙන බව
යි.  එනමුත් ටොම් ජෝන්ස් කෘතිය (හෙන්රි
ෆීල්ඩිං 1749 දි ලියූ නවකථාවක්) පවා චිරන්තන පවුරු පදනම් සසල කළ නිර්මාණයක්.
ඍැපැපඉර්බජැ ද ඔයෙසබටි ඡ්ිඑ  (මාර්සෙල්
ප්රූස්ට් 1909 - 12 දී ලියූ නවකථාවක්) වැනි පුදුමාකාර නවකථාවක් තිබුණා.  එය සර්වසම්පූර්ණ ලෝකයක්.  විශාල කළ සිතුවිල්ලක්.  යථාර්ථය සකස් කිරීමක් හා යළි සකස්
කිරීමක්.  අඩු තරමින් මා ජීවත් වන ලෝකය
සම්බන්ධයෙන්, බොහෝ මිනිසුන්ගේ දෛනික ජීවන යථාර්ථයට වඩා එය විශ්වසනීය යි.  1970 ජුලි 10 මාර්සල් ප්රූස්ට්ගේ 101 වෙනි උපන්
දිනය යි.  ඔහු නොමැරුණු ලේඛකයෙක්.  ෘරග ත්්මිඑමි (තෝමස් මාන් 1947 දි ලියූ
නවකථාවක්) සම්මත බිඳලූ තවත් නවකථාවක්. 
th fudk;rï úYsIag ks¾udKhla o@ 
uu mqÿudldr we,aulska" wdorhlska hq;=j kej; kej;;a f;dauia udkaf.a
ks¾udK lshjkjd'  ta jf.a ,shkakg"
wvq ;rñka f;dauia udkaf.a iq¿ m%Yxidjlg Ndckh úh yels wkaofï kjl:djla ug ,shkakg
yels kï!''''''''''ud l:djla fyda kjl:djla fyda ,shk úg úfYaI ú{dkhla iys; fjk u
mqoa.,hl= f,i yefÕkafka kE' fudk ;rï wmeyeÈ,s f,i jqj;a ziïm%odhlZ" ud
m%sh lrk ks¾udK iys; idys;H iïm%odhl fldgialdßhl njhs" ug yefÕkafka'  iEu l:dkaorhlg u Bg jvd;a u .e,fmk wdlD;shla
yd ffY,shla ;sfhkjd'  ta ksid ud lrkafka
fukak fï foaj,a - tkï" úêu;a lD;shl zpß;Z f,i y÷kajkq ,nk uf.a mßl,amkh
;=  isàuhs'  tneúka uu iïu;j,ska ksoyiaa ùug W;aidy lrk
f,aÅldjl nj fkdis;ñ'  we;a;g u  iïu;hka lshd fohla kE' Middlemarch ^fcda¾Ê
t,shÜ 1871-22 § ,shQ kjl:djla&amp; .;dkq.;sl kjl:djla o@  Bg iudk l  ;sfnkafka fm!reIhka h')</p>

<p> </p>

<p>      wo
ú,dis;djdofha m%Odk YdkrhZ njg m;a j we;s ydi m%nkaO wo furg iuyr f,aLlhkaf.a o
mrudo¾YS idys;Hh njg m;a ù we;'  iudc
Wmydih kjl:d l,djg wd.ka;=l fohla fkdfõ' 
kjl:dfõ mQ¾j.dó idys;H m%fNaohla f,i ie,flk msldfrlaia fyj;a fp!rdLdk
kjl;dfõ § ydiHhg uq,a ;ekla ,eî ;sìK' 
fcdk;ka ia*súÜ" pd¾,aia älkaia" ksfld,dhs f.df.d,a n÷
kjl:dlrefjda o m%yik wdLdHdkhg uq,a ;ek ÿkay' 
tkuq;a ta iïNdjH ydi kjl:dj yd j¾;udk kjl:dj w;r úYd, mr;rhla ;sfí'  od¾Yksl moku" wka;¾.;h yd iudc wdia:dkh
hk ldrKd w;ska tu wdLHdk ud¾. fol fjka fldg y÷kd .; hq;= h'</p>

<p>      </p>

<p>අද
බටහිර සාහිත්ය ලෝකයේ විලාසිතා ශානරන්ය බවට පත් ව ඇති හාසානුකරණ ප්රබන්ධ පිළිබඳ
ව සූසන් සොන්ටැග් නමැති ප්රකට ඇමරිකානු ලේඛිකාව විසින් දක්වා ඇති අදහස මෙරට
විලාසිතාවාදීන්ට කදිම අවවාදයකි.</p>

<p> </p>

<p>      ydidkqlrKhg
miq j meñfKkafka l=ula o@  m%.;shl
f;areu we;s fmr fkdjQ úrE w¨;a fohla ìys fõú o@  fï w¾:fhka kï l,dj ;=  l,dfõ fuu iajhxkdYl jev ms,sfj  wvq ;rñka flá l,lg
fyda tjekakla isÿ fõú'  ;udj u
n,y;aldrfhka ÿIKh (rape) lr .ksñka o" ffY,s .s, ouñka o" iaj-iú{dkl
fjñka o l,djg È.ska È.g .uka l  thska ñksiqka fjfyig m;a
fõú'  fõ.fhka msh keÕSfuka miq j ìu
je;sÍ fjfyi ksjk isß;la ñksiqka w;r we;' 
ta whqßka mKaä; W;am%dihg jvd lsishï wdldrhl ir,;djla WÑ; nj;a"
iqkaor nj;a yeÕS hkq we;'ZZ</p>

<p>      </p>

<p>ගොගොල්ගේ
සාහිත්යය හා ඒ ආකාරයේ හාස සාහිත්යය අතර පැවතිය හැකි සබඳතාවක් තිබේ ද?  මළ මිනිස්සු, පීටර්ස්බර්ග් කථා වැනි
ශේ්රෂ්ඨ නිර්මාණ ඔස්සේ නව විලාසිතා සාහිත්යය සඳහා කලාත්මක අඛණ්ඩතාවක් ඇති බව
පෙන්වීමේ අරමුණින් දියත් වන විචාරයට පදනම් වී ඇත්තේ නිකොලායි ගොගොල්  පිළිබඳ ඉතා වැරදි විනිශ්චයකි.  එම ශ්රේෂ්ඨ ලේඛකයා යථාර්ථවාදී සාහිත්ය
සම්ප්රදායෙන් ඉවත් කර ඊනියා පැරඩි හා ෆැන්ටසි සම්ප්රදායකට ඇතුළත් කිරීම එම විචාරයේ
අරමුණ යි.</p>

<p>      </p>

<p>සාහිත්ය ලෝකය තුළ කවදත් මවිතයක්
ඇති කරවන මට්ටමේ කෘතියක් වූ මළ මිනිස්සු ඊට පෙර බිහි වූ සාහිත්යය සමඟ මෙන් ම ඊට
පසු ව බිහි වූ සාහිත්යය සමඟ ද සැසඳීම මවිතයට කරුණක් විය නොහැකි ය.  එය ග්රීක වීර කාව්ය, පුනරුද යථාර්ථවාදය,
චෞරාඛ්යාන සාහිත්යය, සර්වැන්ටේගේ දොන් කුවිෂොට් හා බයිරන්ගේ දොන් ජුවාන්
කාව්යය සමඟ මෙන් ම කෆ්කා, චාලට් බ්රොන්ටේ ආදී පසුකාලීනයන්ගේ කෘති සමඟ ද සසඳා
ඇත.  එපමණක් නොව චිචිකොෆ් චරිතය
සාතන්, ඩොක්ටර් ෆෝස්ටස්, හිත්ක්ලිෆ් (වදරිං හයිට්ස්) කලිගියුලා වැනි
චරිත සමඟ සසඳනු ලැබේ.  ඒසා විපුල
ප්රොබ්ලමැතික චරිතයක් වූ චිචිකොෆ් රුසියානු යථාර්ථවාදී සාහිත්යයේ පෘථුලතාව පෙන්නුම් කරන
විශිෂ්ට චරිතයක් විනා ඊනියා ෆැන්ටසි චරිතයක් නොවේ.</p>

<p>      </p>

<p>චිචිකොෆ් අපූර්ව අරමුණක් පෙරදීරි කොට ගෙන
රුසියාව පුරා සැරිසරයි.  ඔහු ඉඩම් හිමියන්
හමු වී ඔවුන්ගේ මියගිය ප්රවේණිදාසයන් මිල දී ගනියි. 
ck ix.Kkfha § ,shdmÈxÑ ù we;s ish¨ m%fõKsodihka fjkqfjka bvï ysñhka
úiska rchg nÿ f.úh hq;= h'  ck ix.Kkfha
§ f,aLk.; ùfuka miq ñh.sh m%fõKsodihka fjkqfjka o nÿ f.úh hq;= neúka tu uD;
ñksia foam, kslïu jdf.a ÑÑfld*ag mejÍug Tõyq leu;s fj;s'  ÑÑfld*af.a wruqK ta wj;dr foam, Wlig ;nd Kh
.ekSu hs'  tkuq;a jy¨ka ñ, § .ekSfï
jHdmdrh id¾:l fj;a u ÑÑfld*af.a wruqK fjkZ;l fhduq fõ'  ;u kS;Hdkql+, foam, ika;lh iuÕ .relghq;=
jy,aysñfhl= f,i ia:djr ùug Tyq l,amkd lrhs' 
Tyq w;awvx.=jg m;aùu" kvqj yd ksoyiaùu hk isoaê ud,dj Tiafia uq¿
uy;a reishdkq ks,Odß ;ka;%h yd kS;s hka;%Kh ydiHhg ,la lsÍug f.df.d,a iu;a
fõ'  u  ÑÑfld*a yd bvïysñhka w;r we;s jk .kqfokqj, §
u  ÑÑfld*a
foam, ysñhl= njg m;a jkafka o" iudc .re;ajh .ek is;kafkl= njg m;ajkafka o
ta whqßks'  tu iudc jd;djrKh ;=  Tyq OfkaYajr ñksid ms  ñksia ixl,am rEmh jQ l,S
mrudo¾Yhlaj;a" yqÿ úia;rd;aul m%ia;+;hlaj;a fkdfõ'   th m%;smlaIhka folla w;f¾ mj;akd wd;;shls'
l,d;aul ñksia rEmh tlúg ùrhd o" il, ñksid o jkafka hZZ hs fudßia *aÍâudka
i|yka lrhs')</p>

<p> </p>

<p>      tu w.%.KH
idys;H lD;sh ks¾udKh lsÍu ;j;a úYd, iudc jHdmdrhla" uyd l;sldjla iuÕ
iïnkaO ù ;snqKq nj o wmg wu;l l 
rEfida m%uqL m%xY m%nqoaO;d jHdmdrfha Ñka;lhka ;= 
m%xY m%nqoaO;d jHdmdrh l        tla/ia lsÍug yd úYajfldaI rpkhg uq,a ;ekla ,enqKq w;r
f.df.d,a ;u kjl:dj reishdj ms  f.df.d,af.a ks¾udK yd oy wgfjks
Y;j¾Ifha ngysr m%nqoaO;djd§ .µ w;r iu;djla we;ehs reishdkq idys;H úpdßldjl jQ
wekd fh,Siag%dfgdajd i|yka lr we;') 
reishdj ;=  úma,ùh jljdkqj ;=  Ækdpd¾ials fuka fÄodka;h (tragedy) úma,jfhka
miq fl!;=l l,d m%fNaohla  (archaic
genre) ùh hk ks.ukhg o wkq.; fkdjQ fg%dÜials ta ldrKd fofla § hqfrdamSh idys;H
iïm%odfha msysgd isá nj;a" Ôú;h W;am%did;aul .egqfuka yd ;s;a; rifhka wkQk
h hk mqoa.,sl yeÕSug keUqre j isá nj;a fcda¾Ê iaghsk¾ i|yka lrhs')</p>

<p> </p>

<p>3</p>

<p>      f.df.d,af.a
kjl:dj ;=  u;=msáka fudk u iudk;djla fyda ke;ehs isf;k
bx.%Sis úlafgdaßhdkq kjl:dj iuÕ mjd Bg lsishï {d;s;ajhla ;sfí'  fuu {d;s;ajhg mokï ù we;af;a lsishï wdlD;sl
iu;djla fkdj kjl:d l,dfõ j¾Okhg fya;= idOl jQ ffjIhsl uQ,hka h'  fjk;a jpkj,ska lshf;d;a ro  hq;=j isÿ jQ njg kjl:dj o m%n, idOlhls'  tneúka kjl:dfõ iajhxj¾Okh yd wdlD;sl m%n,;d ms</p>

<p>      kjl:dj l,d
m%fNaohla f,i u;= ùu wdrïN jQ oywg fjks Y;j¾Ifha § OfkaYajr mx;sh f,dalfha
iudchSh mßmd,kh ish;g f.k fkd;snqK;a ro%fhka ÿ¾j, ù
hñka ;sìKs'  Okfhka" m%dfhda.sl
±kqfuka" wkd.;h ms  OfkaYajr ñksil= iqúfYaI
jYfhka Ñ;%Kh fkdjQ wjia:dj, mjd iodpdrfha w¨;a .eg¨" tal;ajfha fkdj
úúO;ajfha úÑ;%;dj yd ñksia Ôú;fha wjq,acd,h jYfhka Tyqf.a N+ñld n,mEu ksrEmKh
úh'  ful, Okh yd iudc mx;sh" udkj
in|;d ms  fÊka" Tiaáka" pd¾,aia
älkaia" pd¾,Ü fn%dkafÜ" fcda¾Ê t,shÜ" f;dauia ydä wd§kaf.a kjl:d
;=</p>

<p>      zzfÊka
Tiaákaf.a .¾jh iy w.;sh (Pride and Prejudice) kjl:dfõ fuka ;reK ñksfil= fyda
.eyeKshl fyda ;u ys;j;l= iuÕ b;d újD; iajrhlska hq;= j" újdyh yd ne÷Kq ,dN
m%fhdack .ek l:d lsÍu" oykjfjks Y;j¾Ifha fndfyda kjl:dj,g fmdÿ
cjksldjls'  hglS kjl:dfõ tk ñksid l¾k,a
*sÜiaú,shï h'  ;ud uqo,a fjkqfjka újdy
úh hq;af;a uka±hs Tyq tu kjl:dfõ m%Odk ia;S% pß;h yuqfõ meyeÈ,s lrhs'  ro 
;ud mqreÿ ù isák úhoï wêl úfõlS Ôú;h fkdlvjd f.k hdu i|yd Tyqg uqo,a
wjYH h'  ÿmam;a ia;%shl iuÕ újdyùfï iem;
Tyqg ±ßh fkdyels úhouls'ZZ</p>

<p>      </p>

<p> </p>

<p>කර්නල් ෆිට්ස්විලියම් යනු ඉතා ප්රියජනක
චරිතයකි.  යටකී පරිදි ගනුදෙනු විවාහය
පිළිබඳ මතු බලාපොරොත්තු හෙළි කිරීමෙන් පසු ද ඔහුගේ ප්රියජනකභාවයට කැළලක්
සිදු නොවේ.  එම නවකථාවේ ප්රධාන ස්ත්රී චරිතය
ඔහු පිළිබඳ ව එවැනි සාදාචාරමය විනිශ්චයකට නොඑළඹෙයි.  කතුවරිය ද එවැන්නක් නොකරයි.  ඔහුගේ තත්වය තුළ ඇතැයි පෙනෙන ගොරහැඩිකම
වෙනුවෙන් සමාව භජනය කරන අන්දමේ ආඛ්යාන විවරණයක් කිරීමට ඇය නොවෙහෙසෙයි.  හුදෙක් ඔහුගේ යථාර්ථය පිළිගැනීමට විනා අප
ලවා ඔහුට පමේ බැන්දවීමට ඇයට අවශ්ය නැත. 
m%shukdmNdjh jqj ks¾oh wkqlïmdúrys;Ndjh iuÕ w;sZk; f.k .uka lrkafka h hk
i;Hh mdGlhd úiska ms  ;jo" l;=jßh wkd.;h ±lSug wfmdfydi;a h'  tneúka wmf.a hq.h ;=  
jqjukdfjka   u   T,fudÜg,  
jQ    mdGl </p>

<p>පරපුරක්  wkd.;fha 
ìys  úh  yels  nj  fmr±lSug 
weh </p>

<p>අපොහොසත් වී ඇත.  හරි හෝ වැරදි හෝ වේවා
ඇගේ චරිත සම්බන්ධයෙන් කරුණු යෙදී ඇත්තේ එලෙසිනි.  ඇගේ චරිතවල ජීවිතය හැඩගැසෙන්නේ පංතිය හා මුදල් යන මාධ්ය දෙක
මඟිනි.  සාමාන්යයෙන් පිටතට පැන ගොස් තම
චරිත හැඩගස්වා ගන්නා මිනිසුන් පි්ළිබඳ උනන්දුවක් ඇය තුළ නැත.  ඇයගේ නවකථාවල ඇතුළත් කිසිම චරිතයක් සුන්ථ
වශයෙන් වුව එවැනි මාර්ගයක ගමන් නොකරයි. 
;elf¾" älkaia" fcda¾Ê t,shÜ" pd¾,Ü fn%dkafÜ yd ydä hk f,aLl
f,aÅldjka iïnkaOfhka o ;;ajh óg iudk h'ZZ</p>

<p> </p>

<p>      bx.S%is
h:d¾:jd§ kjl:d iïm%odh ;=  1770 isg u bx.%Sis lmq l¾udka;Yd,d" hlv l¾udka;Yd,d yd
.,awÕ=re wdlrj, ld¾ñl úma,jh Èh;a úh' 
tys m  lDIsld¾ñl wd¾Ól mrdh;a;Ndjh uÕska ;sria f,i tlg .eg .eiqKq iudc
ia:rhkag wNsfhda. lrk wkaofï .egqï isria jHqyh ;=  b;sydifha uq,a jrg b;d mq¿,a mßudKhlska
wd¾Ól wruqKq w;ska tlsfklg fjkia jQ iudc lKavdhï fyj;a mx;s" i;=re
m%;súfrdaO;djlska hq;= j keÕS isáhy' 
OfkaYajr ld¾ñl wruqKq" bvï ms  fuu isria f¾Çh m%;súfrdaOh mx;s keÕSu
kñ'  fÊka Tiaákaf.a isg f;dauia ydä
olajd jQ bx.S%is kjl:dj, n,j;a jQfha fï mx;s yeÕSuhs'ZZ)</p>

<p>      </p>

<p>ඉංග්රීසි
රදළ ක්රමයේ සිට රෙදි මෝල්, යකඩ කර්මාන්තය හා ගල් අඟුරු ආකර පෙරටු කර ගත්
කාර්මික විප්ලවය ඔස්සේ දියුණු ධනේශ්වර රාජ්යයක් බවට පත් වූ එංගලන්තය තුළ ධනය සහ
පංතිය විසින් අත්පත් කර ගන්නා ලද වික්ටෝරියානු ස්වරූපය ගොගොල්ගේ රුසියාව තුළ
අවතාර දේපලක් ලෙස නිරූපණය වීම පුදුමයක් නොවේ. 
fY%aIag fidhd .ekSï yd ;dlaIKsl m%.ukhka iys; ld¾ñl úma,jhl j¾Ok
wdYs¾jdoh fkd,enqKq miq.dó jevjiï iudc l%uhla iys; id¾jd§ reishdj ;=  hqfrdamSh idys;H iïm%odh .=re fldg
.;a ud¾áka úl%uisxyfhda o .ïfmr</p>

<p> </p>

<p>      m%:u
OfkaYajr úma,jh jQ 1786 m%xY uyd úma,jhg uÕ fy,nd fok ,o ;,a¨j o fuys § isyslg .; hq;= h'  ta m%nqoaO;djd§ka úiska bÈßm;a lrk ,o
m%.;sYS,s woyia yd mqreId¾:" úµd;aul Ñka;kh i|yd Tjqka úiska ,nd fok ,o
;,a¨j" Tjqkaf.a iqm%lg úYajfldaI rpkh" iudc wdh;k ms  fnd  m%nqoaO;djd§ jHdmdrh iuÕ kjl:dj ;=</p>

<p> </p>

<p>m%nqoaO;djd§
hq.fha § fp!rdLHdk kjl:dj ;=  iudcuh yd mqoa.,sl lreKq ms  fp!rdLHdk kjl:dfõ úldrjd§ iSud
blauùï yd jHdl+, jHqyh ke;s ù .sh w;r ta ;=  lsishï úêu;aNdjhls'  ñksid iy f,dalh ms  wdfõKsl j ;snQ wiïmQ¾K Nskak
oDIaàkag wdl¾IKh ù isá 17fjks Y;j¾Ifha fp!rdLHdkh bka neyer j m%nqoaO;djd§
udkjjdofha fya;=hqla;sjd§ ;¾lKh fj; yerefKa h' 
fp!rdLHdk kjl:dj m%nqoaO;djd§ kjl:dj njg m;a ùfï iudrïNl wjia:dj ,SfiaÊ
úiska ,shk ,o ð,aí,dia È ieáka,a,dkS ^1715 - 1735&amp; kjl:dfjka ms</p>

<p> </p>

<p>      m%nqoaO;djd§ka
;=  fuu kjl:dj ms</p>

<p> </p>

<p>      cdl=ia
kï jQ ffojjdÈhd jQ l,S fjk;a kjl:dj,g" l:dkaorj,g yd m%nkaO l,djg b;d ±ä
f,i wduka;%Kh lrkakd jQ kjl:djls' l;=jrhd tla;rd kjl:d j¾.hlg ±ä f,i m%ydr t,a,
lrk nj mdGlhdg blaukska wjfndaO  fõ'  fpdaokdjg ,la jkafka l=uk wkaoul m%nkaO
úfYaIhla±hs y÷kd .ekSu wmyiq ke;' 
l;=jrhd tf,i weÕs,a, È.= lrkafka wNjH l:d jia;= yd j¾. pß; ksrEmKh iys;
±ä;r wd, keUqrejlska hq;= m,dhkjd§  idys;Hh
fj;g h'  .;dkq.;sl m%nkaO l,dfõ
frduEkaál w;sfYdala;sh m%;slafIam lsÍu uÕska cdl=ia kjl:dj hqfrdamSh kjl:dfõ
b;d jeo.;a jQ;a" §¾>jQ;a iïm%odhlg we;=  tu iïm%odh ìys ù we;af;a kqjukd w,xlrK iys;
kslïu kslx m%nkaOh m%;slafIam lrñka i;Hh iuÕ ne÷Kq Wiia wfmalaId ms 
cdl=ia pß;h ia:sr fldg u mjikafka fuh f.d;k ,o l:djla fkdj i;H f,dalh
iuÕ iïnkaO jQ i;H l:djla nj hs'ZZ)</p>

<p> </p>

<p>      miq
j tx.,ka;h ;=  wo mjd h:d¾:jd§ kjl:dfõ wNHka;rh
újrKh lsÍfï § Ndú;d jk úfYaI w¾: iys; jpk rdYshla Ndú;hg meñKsfha o ful,
h'  OfkaYajr l¾udka; tx.,ka;h ;= ful, ìys jQ úlafgdaßhdkq kjl:dj o iïmQ¾K ft;sydisl hq.hlg .e,fmk
j,x.=;djlska iukaú; ù we;af;a o fï ldrKh fya;= fldg f.k h'  úlafgdaßhdkq kjl:dfõ pß;j, yeÈhdj yd yevks,
;SrKh lrk ,oafoa ld¾ñl úma,jfha wdYS¾jdoh ,nñka ;u foam, yd wd¾Ól wjYH;d j¾Okh
jk wkaofï foaYmd,k yd iudc jHqyhla ilia lr .ekSu i|yd bx.S%is OfkaYajrh W;aidy
l ft;sydisl ika;;shla uÕsks' 
h:d¾:jd§ kjl:d ms</p>

<p> </p>

<p>      tu
ft;sydisl jljdkqfõ § ixialD;sl Ñka;kh ;=</p>

<p> </p>

<p>      wo buy;a
jeo.;alula orK jpk rdYshla idudkH bx.S%is jHjydrh ;=  Bg fmr NdIdj ;=</p>

<p>      tjeks
is;shula ks¾udKh l  l¾udka;h" m%cd;ka;%jdoh (democracy),
mx;sh (l,dj yd ixialD;sh tu flaka§%h jpkfhda fj;s' w¾:h ms</p>






</body></text></cesDoc>