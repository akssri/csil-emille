<cesDoc id="sin-w-humanities-nuwana-198" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-humanities-nuwana-198.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>තිරසාර
සංවර්ධනය යනු කුමක් ද?</p>

<p> </p>

<p>රුහුණු
විශ්ව විද්යාලයේ </p>

<p>සහකාර
කථිකාචාර්ය (සමාජ විද්යා)</p>

<p> </p>

<p>තිලක්
විජයසුන්දර</p>

<p>මෙහි
දී  මුලින් ම  තිරසාර  සංවර්ධනය (Sustainable Development)
යන්නෙන් කුමක් අදහස් වන්නේදැයි විමසා බැලිය යුතු ය.</p>

<p> </p>

<p>"This
would be seen as economic at social development which avoided Pollution,
conserved non  renewable energy forms, Such as oil, and in general, did not
create future problems for which there were no easily avoidable solutions"1</p>

<p> </p>

<p>ඉහත
නිර්වචනයට අනුව, පරිසර දොෂණය වළකන, තෙල් වැනි නැවත ඇති නො වන ශක්ති
ප්රභවයන් ආරක්ෂා කරන, සාමාන්යයෙන් පහසුවෙන් විසඳිය නො හැකි අනාගත ප්රශ්න ඇති
නො කරන ආර්ථික හා සමාජ සංවර්ධනය තිරසාර සංවර්ධනය යනුවෙන් අර්ථ දක්වා ඇත.</p>

<p> </p>

<p>සාමාන්ය
අරුතින් ගත් විට තිරසාර සංවර්ධනය යන්න පරිසරය සමග ඉතා කිට්ටුවෙන් ගමන් කරන්නක් බව
සැලකිය හැකි ය. මේ නිසා මේ ආකාර සංවර්ධන කි්රයාවලියේ දී සිදුවන පාරිසරික හානිය අවම වේ.</p>

<p> </p>

<p>තිරසාර
සංවර්ධනය යන වචනය කරලියට පැමිණුණේ 1970 දශකයේ දී විශේෂයෙන් තුන්වන ලෝකයේ රටවල
ඇති කළ සංවර්ධන ව්යාපෘති නිසා, පරිසරය විනාශවීම හා පාරිසරික ප්රශ්න උද්ගතවීම සමග
ය. මේ ආකාර සංවර්ධනයකින් අපේක්ෂික අරමුණු ඒ අයුරින් ම ඉටු නො වන බව පෙනී ගියේ
ය.</p>

<p> </p>

<p>තිරසාර
සංවර්ධනය පිළිබඳ අදහස් ඉදිරිපත් වීමට පෙර සංවර්ධනය යන්න ''ඨරදඅඑය දබ ක්ය්බටැ''
යන්නෙන් අදහස් කරන ලදී. මෙහි අර්ථය වන්නේ වර්ධනයක් සමග සිදුවන වෙනස්වීම් යන්නයි. මේ නිසා
මෙම අදහස ා්්ප ීපසඑය නම් සම්භාව්ය ආර්ටික විද්යාඥයාගේ අදහස් මත බිහිවූ බව සැලැකිය හැකි
ය. මෙහි දී ආර්ථික වර්ධනයට සුවිශේෂ වැදගත්කමක් හිමි විය. නිදහසේ කරන වෙළෙඳාම මත
පුන්ථල් ආර්ථික සංවර්ධනයක් ඇතිකර ලීම මෙහි දී අපේක්ෂා කරන ලදී. මේ අදහස් උපයෝගිතා
අදහස් ලෙස ද හැඳින් වේ. මේ අදහස් සාරාංශ කර මේ අයුරින් දැක්විය හැකි ය :-</p>

<p> </p>

<p>   1' iïm;a wiSñ; h'</p>

<p>   2' ixj¾Okh wkka; h'</p>

<p>   3' ixj¾Okh j¾;udkhg keUqre j ilia úh hq;= h'</p>

<p>   4' ixj¾Okh jdia;dúl h' tfia u th uelsh yels h'</p>

<p>   5' ixj¾Okh u.ska iudkd;au;dj iajdNdúl j we;s fõ'
tfy;a tA i|yd fndfyda l,a .; fõ'</p>

<p>   6' ixj¾Okfha § mßirh wls%h fõ'</p>

<p> </p>

<p>උපයෝගිතාවාදීන්,
සංවර්ධනය පිළිබඳ ව දක්වන මෙම අදහස් අනුව එය කෙසේ හෝ අයත් කරගත යුත්තක් විනා
පරිසරය පිළිබඳ ව කිසිදු තැකීමක් නො කරන බව පෙනී යයි. සම්පත් නිසි පරිදි
කළමනාකරණයට එහි දී ලක් නො කරයි. මේ නිසා මේ ආකාර සංවර්ධනයකින් අහිතකර ප්රතිවිපාක
රැසක් ඇති කරනු ඇත. මේ පදනම මත සංවර්ධනය පිළිබඳ මේ මතය, තිරසාර
සංවර්ධනවේදීන්ගේ දැඩි දෝෂ දර්ශනයට ලක්විය. විශේෂයෙන්, සම්පත් අසීමිත වෙතැයි යන
මතය දැඩි ලෙස විවේචනය කරමින් මොවුන් කියා සිටින්නේ, නූතන මිනිසා විසින් භාවිත කරනු
ලබන සම්පත් අතීතයෙන් ලැබුණු අනාගතයේ ණයට ගත්තක් වන බවයි. මේ නිසා, සම්පත්
කළමනාකරණයකට ලක් කිරීමේ වැදගත්කම මෙමගින් අවධාරණය කරයි. කෙසේ වෙතත් තිරසාර
සංවර්ධනවේදීන්ගේ අදහස් සාරාංශ කර මෙසේ දැක්විය හැකි ය :-</p>

<p> </p>

<p>   1' iïm;a iSñ; h' </p>

<p>   2' ixj¾Okfha Wmßu iSud we;' </p>

<p>   3' mßirh l%shdldÍ fõ' </p>

<p>   4' ixj¾Okh yqfola wd¾Ólhg muKla iSud jQjla fkd j
th iuia; iajrEmhla .kS' tneúka th ueksh fkd yels h' </p>

<p>   5' ixj¾Okfha § idudh jYfhka iudk wjia:d
 i,id Èh hq;= h'  fya;=j" iudkd;au;dj iajdNdúl j we;s fkd ùu h' </p>

<p>   6' ixj¾Okh wkd.; keUqrejlska hqla; h'</p>

<p> </p>

<p>තිරසාර
 සංවර්ධනය  හා  උපයෝගිතා සංවර්ධනය යන සංකල්ප දෙක ම ලිබරල් වාදී
මතයෙන් පෝෂණය වී විවෘත ආර්ථිකය මත රඳා පවතින්නකි. එහෙත් තිරසාර සංවර්ධනයේ දී ලාභ
උපරිම කැරැ ගැනීමට බලාපොරොත්තු වන්නේ ආදායම වැඩි කැරැ ගැනීමෙන් නොව වියදම අඩුකැරැ
ගැනීමෙනි. මෙහි දී දැකිය හැකි කැපී පෙනෙන ලක්ෂණය වන්නේ, තිරසාර සංවර්ධනයේ දී
සම්පත් කළමනාකරණය හා පරිසරය සුරැකීම සම්බන්ධ ව සුවිශේෂ අවධානයක් යොමු කැරැලීමයි.</p>

<p> </p>

<p>''වත්මන්
පරපුර විසින්, ඔවුන්ගේ ඕනෑ එපාකම් සපුරා ගැනීම පිණිස භුක්ති විඳින කිසියම් සම්පත්
ප්රමාණයක් වේ ද එය කිසිදු අඩුපාඩුවකින් තොර ව අනාගත පරපුරට ද භුක්තිවිඳීමේ හැකියාව
තහවුරු කැරෙන සංවර්ධනය තිරසාර සංවර්ධනය වේ.''</p>

<p> </p>

<p>(සංවර්ධනය
හා පරිසරය සම්බන්ධ ජගත් කොමිසමේ 1988 වාර්තාව)</p>

<p> </p>

<p>මේ
අනුව, වර්තමානයේ මිනිසා භාවිත කරන සම්පත් ඒ අයුරින් ම අනාගත පරපුරට ද හිමිකැරැ දීම
තිරසාර සංවර්ධනය පිළිබඳ සංකල්පයේ දී අවධාරණය කරන බව පෙන්වා දිය හැකි ය. මේ නිසා
තිරසාර සංවර්ධනයේ දී අපේක්ෂා කරන්නේ, එක් පරම්පරාවක් පරිහරණය කරන සම්පත්
සම්භාරය, කිසිදු අඩු පාඩුවකින් තොර ව ඊළඟ පරපුරට ද ලැබෙන බව සහතික කිරීමයි.
එය වර්තමාන පරපුර විසින් අනාගත පරපුර වෙනුවෙන් ඉටුකළ යුතු යුතුකමක් වන බව මේ
මගින් තවදුරටත් කියා සිටී. දුප්පත් පොහොසත් භේදයෙන් තොර ව සෑම රටක් ම ඉහත පදනම මත
කි්රයා කිරීමෙන් තිරසාර සංවර්ධනය ළඟා කැරැ ගත හැකි බව මොවුන්ගේ මතය වේ.</p>

<p> </p>

<p>flfia fj;;a fujeks wdldr ;sridr ixj¾Okhla we;s le/ .ekSfï
§ jdia;úl .eg,q /ilg iEu rgla u mdfya j¾;udkfha uqyqK fok nj fmkS hhs'
úfYaIfhka ;=kajk f,dalfha rgj,a f,i y|qkajk rgj,a È </p>

<p>කෙසේ
වෙතත් වර්තමානයේ ද විශේෂයෙන් තුන්වන ලෝකයේ රටවලට තිරසාර සංවර්ධනයක් ඇති කැරැ
ගැනීම දුෂ්කර වී ඇති බව පිළිගත හැකි වුවත් මේ දුෂ්කරතා ජය ගැනීම තුළින් එය
කළ හැකි වන බව පෙනී යයි. ප්රධාන වශයෙන් දිගු කාලීන සැලසුම් :කදබට එැරප චක්බි- මත මේ
රටවල සංවර්ධන ව්යාපෘති සැලසුම් කිරීම ඉතා වැදගත් ය. මෙහි දී ආණ්ඩු වෙනස් වන විට
වෙනස්         නො වන ආකාරයේ ප්රතිපත්ති
අනුව කි්රයා කිරීම වැදගත් ය. </p>

<p> </p>

<p>''තුන්වන  ලෝකයේ 
rgj,  iïm;a l</p>

<p> </p>

<p>මේ
අනුව බලන විට දියුණු රටවලට පමණක් නො ව, තුන්වන ලෝකයේ රටවලට ද තිරසාර
සංවර්ධනයක් ඇතිකර ගැනීම දුෂ්කර නො වන බව පෙනේ. අවශ්ය වන්නේ නිවැරැදි ප්රතිපත්ති මත
පිහිටා කටයුතු කිරීමයි. ඒ තුළින්, තිරසාර සංවර්ධනයක් ඇති කර ගැනීම වැඩි
ඈතක නො වනු ඇත.</p>

<p> </p>






</body></text></cesDoc>