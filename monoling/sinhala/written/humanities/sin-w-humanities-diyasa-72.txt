<cesDoc id="sin-w-humanities-diyasa-72" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-humanities-diyasa-72.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>මහාචාර්ය ලෙස්ලි ගුණවර්ධන</p>

<p>      </p>

<p>ආගමික
මානව විද්යාව පිළිබඳ ලියැවී ඇති සියල්ල අතුරෙන් මිථ්යා වෘත්තාන්තයන් පිළිබඳ
අධ්යයනයන්ට තරම් මිඩංගු වූ අන් අධ්යයනයක් නැතැයි කිව හැකි යැයි, ක්ලෝඩ් ලෙවී
ස්ට්රවුස් සිය Structural Anthropology නම් කෘතියෙහි සඳහන් කරයි.
'සිද්ධාන්තමය වශයෙන් බැලුවහොත් මේ සම්බන්ධයෙන් මීට පනස් වසරකට පෙර තිබූ තත්ත්වය
තවමත් නොවෙනස්ව ඇතැයි සිතිය හැක. එනම් ව්යාකුල මිථ්යා මත වනාහී සාමූහික සිහින හෝ
කලාත්මක රංගනයෙන් බිහිවූවක් හෝ වත් පිළිවෙත් වලට පදනම් වූවක් හෝ ආදී වශයෙන්
එකිිනෙකට පරස්පර වූ අර්ථ දැක්වීම් තවමත් බොහෝ විට සිදුවේ.1
ලෙවී ස්ට්රවුස් සිය කෘතීන්හි මේ සම්බන්ධයෙන් සැලකිය යුතු බලපෑමක් ඇති කළ ද ඉහතින්
දැක් වූ ඔහුගේ ප්රකාශනයෙන් විසි වසක් පමණ ගතවී ඇති දැනුදු එම ව්යාකූලත්වය අඩුවී ඇතැයි
කිව නොහේ. මිථ්යා මත පිළිබඳ වැදගත් අධ්යයනයන් කර ඇති මැලිනොව්ස්කි,
බෝයස්, ක්ලක්හොන් හා ලෙවී ස්ට්රවුස් ආදීන් ද මූලික වශයෙන් සිය අවධානය යොමු
කර ඇත්තේ ම්ලේච්ඡ සමාජවල මිථ්යා විශ්වාස කෙරෙහි ය.</p>

<p> </p>

<p>      පැරණි ශිෂ්ටාචාරයන් හි මිථ්යා මත අධ්යයනය
කිරීමේ අවශ්යතාවය හොඳින් අවබෝධ කර ගෙන සිිටි මැලිනොව්ස්කි ප්රාථමික සමාජයන්හි දී
මිථ්යා මත කි්රයාකාරී වන්නා වූ ආකාරය අධ්යනය කිරීමේදී ඉහළ ශිෂ්ටාචාරවල මිථ්යා මත
වැදගත් වන්නේ කෙසේද යන්න ගැන අවබෝධයක් තිබිය යුතු යැයි කල්පනා කළේය. වඩා
මෑත කාලයේදී මානව විද්යාඥයන් මෙන් ම සම්භාව්ය පඬිවරු ( විශේෂයෙන්ම ගී්රක හා
මෙසපොතේමියානු සම්භාව්ය පඬිවරු) ලිඛිත මිථ්යා මත කෙරෙහි දැඩි අවධානයක් යොමු කිරීමට
නැඹුරු වී ඇත.2 කෙසේ නමුදු මිථ්යා මත අධ්යයනය කෙරෙහි විශ්වාසයක් නොමැති
ඉතිහාසඥයෝ, මිථ්යා මත සිය විෂයෙහි සැබෑ අංශයක් ලෙස පිළිගැනුමට මැලි වෙත්.
මේ හෙයින්, මිථ්යා මත අධ්යනය පිළිබඳව මානව විද්යා අංශයෙන් මෙතෙක් කිසියම්
ප්රගතියක් සිදුවී ඇතැයි කිව හැකි නම්, ඉතිහාස පර්යේෂණ අංශයෙන් බලන විට එවැනි
අධ්යයනයක් සඳහා තීරණාත්මක පටන් ගැනීමක් වත් සිදු වී ඇතැයි නිගමනය කළ නොහේ. මෙම
ලිපිය (මානව විද්යාත්මක සිද්ධාන්ත උපයෝගි කර ගනිමින් ) ශී්ර ලංකාවේ ප්රචලිත
එක්තරා මිථ්යා කථා සමූහයක් පිළිබඳව විග්රහ කිරීමට ගත් ගවේෂණාත්මක උත්සාහයකි. එසේ
හෙයින් දකුණු දිග ආසියාවේ ඉතිහාසඥයන් විසින් නොවැදගත් සේ නොසලකා හැර ඇති විශාල
මිථ්යා කථා සම්භාරයෙන් යමක් උකහා ගැනීමට උපකාර විය හැකි සිද්ධාන්තමය උපකරණ සංවර්ධනයෙහි
ලා මෙම පරිශ්රමය කෙතරම් තාවකාලික හා සීමාසහිත වුවද, ප්රයෝජනවත් වේයැයි අපේක්ෂා
කෙරේ. ලංකාවේ ප්රධාන වංශ කථාද්වය වන දීපවංශයෙහි හා මහා වංශයෙහි බුදුන් ලංකාවට වඩින
ලදැයි සැලකෙන අවස්ථා තුන පිළිබඳව විස්තරාත්මකව සටහන් කර ඇත. මෙම විස්තරය ලියා
ඇත්තේ පාලියෙනි. වඩාත් දීර්ඝ විස්තර ඇත්තේ දීපවංශයෙහි ය. එනම් මේ සඳහා කැප කර
ඇති පරිච්ඡෙද දෙක ගාථා 150කින් යුක්තය. මහා වංශයෙහි මේ සඳහා කැප කර ඇත්තේ එක්
පරිච්ඡෙදයක් පමණි.  බුදුන්ගේ ලංකා ගමන ගාථා
84 කින් වඩාත් සැකවින් මෙහි ඉදිරිපත් කර ඇත. තවද මේ පිළිබඳ තෙවැනි විස්තරයක්
වංසත්ථප්පාකාසිනියෙහි ඇත. මෙම කෘතිය මහාවංශ ටිකාවක් වූවද එහි දැක්වෙන විස්තරය
මහාවංශ විස්තරයට වඩා යම් යම් අයුරින් වෙනස් ය. </p>

<p> </p>

<p>      මෙම මුලාශ්රයන් තුනෙහිම විස්තර වන බුදුන්ගේ
ලංකා ගමන්, ඉන්දීය මුලාශ්ර මඟින් හෝ ලංකාවේ නොනැසී පැවති වෙනත් පාලි
ග්රන්ථවලින් හෝ තහවුරු නොවේ. මෙම විස්තර, බොහෝ ප්රාතිහාර්යයන් අඩංගු
කි්රයාවන්ගෙන් යුක්තය. බුදුන් ලංකාවට වැඩි මෙම තෙවතාවෙහි දීම අමනුෂ්යය,යක්ෂ,
නාග  හා දේව ගණයා පමණක්ම මුණ ගැසුණාහ.
එසේ වුවද 1845 දී ( The History of Ceylon ) නම් ග්රන්ථයක් පළ කළ
විලියම් නයිටන්  මෙම පුරාණ කථා මඟින්  සත්ය ඓතිහාසික සිදුවීම් විස්තර වේ යැයි සිතීය.
නයිටන් මෙන්ම 1900 දී (The History of Ceylon of Schools) දී නම් ග්රන්ථය රචනා කළ එල්.
ඊ. බ්ලාසේද යක්ෂ හා නාග යනු පුර්ව - ආර්ය ලංකාද්වීප වාසීන් ලෙස සැලකීය. අනුභවික ඉතිහාස
ලේඛන සම්ප්රදායකට හුරු පුරුදු වූ උගතුන් විසින් මෙම ආදී යුරෝපීය ලේඛකයන්ගේ
අතිශෝක්තියෙන් යුතු පිවිසුම විවේචනයට ලක් කරන ලදී. එසේ වුවද, බැතිමතුන්,
බෞද්ධයන් විසින් මෙම කථා පුවත් සැබෑ ලෙස විශ්වාස කරන බවට සැකයක් නැත. බුදුන් වහන්සේ
ලංකාවට වැඩියේ යැයි ඇදහීමට හේතු සාධක නොවේයැයි, (සෙනරත් පරණවිතාන
මහතා)  වරක් 1972 දී ප්රසිද්ධ රැස්වීමකදී
සඳහන් කළේය. මීට එරෙහිව උද්ඝෝෂණ රැසක් ගලා ආයේ ය. </p>

<p> </p>

<p>      nqÿka jykafiaf.a ,xld .uka ;=k ms  pß;hg b÷rd fjkia h' </p>

<p> </p>

<p>      fuu l:djkays oelafjk nqoaO pß;h ffu;s%fhka
w. ;ekam;a uyd ldreKsl .=Kfhka iukaú; hehs lsj fkdfya' fuys i|yka jkqfha  fn!oaO idys;Hfhys wfkla ;ekays yuqjk nqoaO
pß;hg b÷rd fjkia jQjls' </p>

<p>       uydjxYh yd jïi;a:mamdldiskSh iei|Sfï § nqÿkaf.a m%:u ,xld .uk yd
úchdj;rKh ms  úch yd Tyqf.a
msßig Èjhsfkys hCI rdcOdkshla yuq úh' úch ish ;ïnmKaKS rdcOdksh msysgqùug fmr
hl=ka yd igka lr l=fõKshf.a iyh o ,nd Tjqka úkdY lr,Sh' fï wkqj fuu l:doajh
tlsfklg iajdëk jQ iïNjhka we;s l:dka;r f,i o jxY l:dlrejka úiska ys;du;d u tlg
wuqKd we;ehs o is;Sug mq¿jk' </p>

<p> </p>

<p>      දීපවංශයට අනුව මෙම දිවයිනේ මුල් පදිංචි කාරයෝ
යක්ෂයෝය. බුදුන් පළමු වරට ලංකාවට වැඩි අවස්ථාවේ දී ඔවුහු මහියංගනයට එක් රැස් ව
සිටිය හ. මෙම යක්ෂ සමාගමයට පැමිණි බුදුන් ඊට ඉහළ අහස් කුස වැඩ සිට වැසි හා සීත
සුළං ආදිය මවා ඔවුන්ට පීඩා පමුණුවාලූහ. ඉක්බිති එම පිඩාවන්  ගෙන් ඔවුන් මුදවාලන්නට නම් තමන් වහන්සේට
හිඳිනා තැනක් දෙන ලෙස ඔවුන් ඇමතූහ. පැකිලීමකින් තොරව ඊට එකඟ වූ යක්ෂ සමූහයා
තමන්ගේ බිය ද, සීතද දුරු කර දෙන ලෙස බුදුන්ගෙන් අයැද සිටිති. තම පත්කඩ අතුරා ඒ
මැද වැඩ සිටිනා බුදුන්  ඒ පත්කඩින් ගිනි
විහිදුවා යක්ෂයන් ගී්රස්මයෙන් පිඩීත කර ඔවුන් පළවා හැරීමට සලස්වති. මීළඟට බුදුහු
ගිරි දීපය නම් වෙනත් ද්වීපයක් ලංකා ද්වීපයට ළඟා කරවති. බියපත්ව පීඩිතව පලා යන යක්ෂයන්
ගිරි දීපයට පැන ගත් පසු  බුදුන් ගිරි දීපය තිබු
තැන්හිම තබති.6 මහාවංශයෙහි දැක්වෙන මීට වෙනස් වූ කථා පුවතෙහි යක්ෂයෝ
තමන් බියෙන් මුදවාවූ බුදුන්ට මුන්ථ දිවයින ම දෙමිහයි ප්රතිඥාා දෙති. භූමියෙහි තම පත්කඩ එලා
හිඳිනා බුදුන්  එය හාත්පසින් විශාල කර ඉන්
ගිනි විහිදුවාලූ කල්හි යක්ෂයෝ දිවයින අන්තයටම පලා යති. ඉන් පසුව දීපවංශය කථා පුවතෙහි
දැක්වෙන ලෙසින් අන් තැනකට ගෙන ගොස් දැවෙති.7</p>

<p> </p>

<p>      මේ සිද්ධිය ඉතාම නාට්යා කාරයෙන් දක්වා ඇත්තේ
වංසත්ථප්පාකාසිනියෙහි ය.8 බුදුන් සිය අත්භූත බලයෙන් යක්ෂයින් හට
එකොළොස් වැදෑරුම් බියක් පමුණු වන්නා හ. මහා වර්ෂා හා සැඩ සුළං ඔවුන් වෙත ඇඳ
හැලේ. ගල්, අවි, ගිනි පුපුරු, දැවෙන අලු හා මඩ ද පතිත වේ. වර්ෂාව
තාන්ධකාරාදියෙන් ඔවුහු පීඩාවට පත්වෙති. බිය වෙති. යක්ෂයෝ තමන්ට අභය දෙන ලෙස
බුදුන්ගෙන් ඉල්වා සිටින කල්හි බුදුන් එසේ නම් තමන්ට හිඳිනා තැනක් දෙනුවයි යක්ෂයින්ගෙන්
ඉල්වති. බුදුන්ට මුලු ලක්දිව ම දෙන බව යක්ෂයෝ පවසත්. එහෙත් පසුව යක්ෂයින් මෙම
ප්රතිඥාාව ඉෂ්ඨ නොකරතැයි සැකයක් බුදුන් පළ කළ කල්හි බුදුනට මුලු දිවයිනම
කිසිවෙකුගේ විතර්කයෙන් තොරව සතුවන බව යක්ෂයෝ පවසති. සිය පත්කඩ මත හිඳිනා බුදුන්
යථා තත්වයට ඇති කරන නමුදු යක්ෂයෝ තවමත් ශීතලෙන් පීඩා විඳිති. ශීතය තුරන් කිරීමට
හිරු රැස් මුදාලන ලෙස ඔව්හු ගෙන් ඉල්වති. මේ ඉල්ලීම ඉටු කරන බුදුන් සිය පත්කඩින් ගිනි
දැල් විහිදුවා රශ්මිය ඇති කරන්නා හ. ඒ සමඟ ම පත්කඩ මුලු දිවයිනම වසා සිටින සේ
හාත්පසින් ම විශාල කළහ. විශාල වන පත්කඩ සමඟම බුදුන්ගේ සිරුරද විශාල වේ. අන්තිමේදී
දිවයිනෙහි දිග පළලට සමව පත්කඩ හා බුද්ධ ශරීරය ද විශාල වූ බව කියා ඇත. පැතිරෙන්නා
වූ පත්කඩ හාත්පසින් පලා යන යක්ෂයෝ වෙරළ දක්වා ම දුව යත්, 'දිවයින දේවාති
දේවයෝ විසින් අත්පත් කර ගත්හ. තමන්ට අහිමි යැයි " 9 විශ්වාස කෙරෙත්.
පීඩිත වූ යක්ෂයින් කෙරෙහි මෛති්රය උපදවන බුදුන් වංශ කථා ද්වයෙහි දැක්වෙන පරිදි ගිරි
ද්වීපය ඔවුන්ට සමීප කරවා එමඟින් ඔවුන් දිවයිනෙන් බැහැර කරලති. </p>

<p> </p>

<p>      ඉහත සඳහන් පරිදි තුන් අයුරකින් දක්වා ඇති
සිද්ධිය ති්රපිටකයෙහි දී අමනුෂ්ය දමනය"ට වඩා බෙහෙවින් වෙනස් ය. ආලවක යක්ෂ දමනය
පිළිබඳ කථාවෙහි බුදුන් ආලවක දමනය කෙරෙනුයේ සිය කරුණා, මෛතී්ර ආදි යහපත්
ගුණ සමුදායෙනි.10 එහිදී බුදුන් ආලවක තමන් අභිමුඛයෙහි දැක් වූ විරුද්ධවාදී
කි්රයාවන්ගෙන් කිසිසේත් නො කැලඹේ. එනමුත් මෙහිදී බුදුහු යක්ෂයන් හට නොයෙකුත් පීඩා
උපද්රවයන් පමුණුවති. හිඳුනා පිණිස යක්ෂයින්ගෙන් ඉඩක් ඉල්ලන බුදුහු අන්තිමේදී ඔවුන් සිය
මව් බිමෙන් පළවා හරිති. කථා පුවතේදී කිහිප පළකදීම බුදුන් හඳුන්වනුයේ 'ජින' හෙවත්
දිනූ තැනැත්තේ යන වචනයෙනි. ඇත්ත වශයෙන්ම මෙම කථා පුවතේදී ජින යන්න එහි ලිඛිත
අර්ථයෙන් ම ගැලපෙන සේය. මෙහි දි පැහැදිළිව ම දිනුම් ලද්දේ ය. මෛති්රය දක්වනුයේ
රාජධානියක් අල්ලා ගත් පසුව පමණි. ඇත්ත වශයෙන්ම මෙහිදී බුදුන්ගේ චරිත නිරූපනය කර
ඇත්තේ සාමාන්ය බුද්ධ චරිතයට වෙනස් අයුරකිනි.</p>

<p> </p>

<p>      ශී්ර ලංකාව බෞද්ධ සංඝයා හා රජය අතර
දීර්ඝ කාලයක් නොනැසුනා වූ සමීපත්වයක් ගොඩ නැගුණු පළමු වන රටවල් අතර එකකි. මේ
අයුරින් සංඝයා හා රජය අතර වර්ධනය වූ මෙම සම්බන්ධයට කල් නොයවාම දැඩි ප්රශ්නවලට
මුහුණදීමට සිදු වන්නට ඇත. සූත්ර පිටකයේ මහා සුදස්සන සූත්ර ආදියෙහි දක්වා ඇති
අන්දමට, මුල් කාලීන බෞද්ධ චින්තනයට අනුව යහපත් රාජ්ය භාවය අවිහිංසාව පදනම් කර
ගත්තකි. මෙය චක්රවර්ති සංකල්පය ඉදිරිපත් කර ඇති අයුරින් ම පැහැදිළිව පෙනේ. සිය
හමුදාවත් සමඟ දේශයන්හි සැරි සරන චක්රවර්ති තෙමේ, හිංසාවෙන් තොරව ධර්මය
පිළිබඳව චතුර දේශනයන් හා මුසු කර ගත් 
n,mEï Wmdh ud¾.fhka l=vd md,lhka wjk; lr .kS'  fuu ms11 uyd fndaê cd;lfhys 
i|yka jkqfha foaYmd,kh jkdyS p¾hd O¾uhkaf.ka  wjysr fkdù n,h fiùfï talSh wruqK ksfhdackh lrkakla f,isks'
foaYmd,k wdpd¾Hfhda wd;au j¾Okh Wfoid ish fouõmshka >d;kh lsrSug mjd wkqn,
ÿkay'12 YS% ,xldfõ ix>hd yd rdcH w;r iïnkaO;dj jeã f.k tk
wjia:dfõ§ igkaldó krjrfhl= " tkï iudcfha m%n,;u ixúOdkd;aul ysxidldrhka
ls%hd;aul lrjkafkl=" flfrys ix>hd flfia meje;sh hq;=o hkak fndfyda
ix>hd jykafia,dg is; le</p>

<p> </p>

<p>      මෙි නයින් බලන කළ (බුදුන්ගේ) ප්රථම
ලංකා ගමන පිළිබඳ ප්රබන්ධය මෙම උභතෝකෝටික ප්රශ්නයට මධස්ථ විසඳුමක් සෙදවීමේ
ප්රයත්නයක් යැයි සිතිය හැකිය. මෙම මිථ්යා ප්රබන්ධයෙහි බුදුන් ද ජයග්රහණය ලබන්නෙකි.
යක්ෂයින්ට පීඩා කිරීමට බුදුන් අත්භූත බලය උපයෝගි කර ගැන්ම නරවරයෙකු සතුරන් මැඩීම
සඳහා සාහසික කම් කිරීමට සමකළ හැකිය. 
nqÿka fujeks úIu mshjrla .ekSug fya;=j hCIhskag i;H wjfndaO lr .ekSug
wfmdfydi;a ùu" Ydikhg úreoaO ùu hk idOl ksid Tjqka Èjhsfkka ;=rka lr ,Sug wjYH
ùu hehs myod we;'14 tfyhska ñ:Hd m%nkaOfhys oelafjkqfha iQ;%
msglfhys hqla;s O¾u;djhg fjfiiskau fjkia jQjls' Ydikh /l .ekau Wfoid jQ idyisl
ls%hdjka  lï ke;' §i;H O¾uh¶ wjfndaO
fkdjkakd jQ fyda Bg úreoaO jkakd jQ fyda wh iïnkaOfhka o tfiau h' idyisl
ysxidjka flfrys wNsjD;a;sh fï whqßka kej; myod ,Su wdpdr O¾udkql+, mrudo¾YS rc
flfkl= yd wjia:dkql=,j lghq;= lsrSug isÿjk rfcl=f.a meje;au w;r we;s úh yels
mriamrh w;r idudkH myiq lsrSuhs' uyd jxYfhys ÿÜG .dñKS pß;h óg ukd ksoiqkls'
rcqf.a ls%hd mámdáh fya;= idOl fia oelaùug kj ksrela;s Wmfhda.s lr .efka'
wkqrdOmqrfhys m%Odk fn!oaO fjfyr úydr lsysmhla u bÈ lrk ,oafoa fu;=uka úiska
hehs lshfõ' fikam;sfhl= jQ ÿgq .euqKq rcq ls%( mQ( fojeks ish jfia§ ,xld oaùmh
tla fiai;a lsrSu i|yd ld;khla isÿ úh' id¾:l
hqoaO m%ydr fufyh jQ fujka fikam;s rfcl= fn!oaO ùrjrfhl= f,i yqjd oelaùu
meyeÈ  msKsi hehs o" Ydikh Wfoid hehs
o 15 uydjxY  l:dfõ ys
wjOdrKfhka u i|yka fõ' uydjxY l:dfõ lshfjk mßÈ ish igka ksu l  uyd fiakd >d;kh
jQ kuqÿ ta l¾ufhka iaj¾. ud¾.dka;rdfhla ke;af;a hehs ry;ka jykafia,d rcQ
wiajeiQ y' ~urK ,o ñksiqka tflla yd wvla jkafka h'¶ ta ulaksido h;a urK  ,oaojqkaf.ka mkais,a ys ms  fï whqßka ÿÜG .dñKS rcQ ksfodia
ry;ka jykafia,d fufia o jodr;s' § f;ms nqoaO idikh fkdfhla mßoafoka nnq¿
jkafkys h' tfyhska kf¾Yajrh" Ñ;a; fÄoh ÿr,j¶ 16 fn!oaO ixl,amh
wkqj ~;sßikqka jeks jQjka >d;kh lsrSu ¶ ÿYaYS, l¾uhla fõo'''@ fkdfõo'@ hk
wmyiq m%Yakh u;= lr ke;' jxY l:d lrejd fuu mßÉfPoh ksu lsrSfï§ f,daNh"
lduh fya;= fldg f.k ñksiqka >d;kh lsrSfï wdÈkj i|yka lrk ieá jeo.;a h'  tuÛska Tyq ldufhka ñksiqka uerSfï;a "
Ydik Wkak;sh17  Wfoid ñksiqka
uerSfï fjki biau;= lr olajhs' miqj ÿÜG .dñKS rcqf.a wNdjh úia;r fjk ;sia fojk
mßÉfPoh fl18 fï whqßka ÿgq .euqKq l:d mqj;
o nqÿkaf.a m  yd ldufhka Woa.; jkakd jQ
ieyeislï w;r fjkila we;s lr .;a; hq;= nj yÛjhs' fuh ieyeis ls%hd wjYHfhkau ÿIag
ÿYaYS, fÉ;kd ne|S ke;af;a hehs bÛshls' </p>

<p> </p>






</body></text></cesDoc>