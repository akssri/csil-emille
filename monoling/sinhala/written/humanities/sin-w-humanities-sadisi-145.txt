<cesDoc id="sin-w-humanities-sadisi-145" lang="sin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>sin-w-humanities-sadisi-145.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Vincent Halahakone</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-26</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>File from written Sinhala corpus collected by Vincent Halahakone, dated xx-xx-xx</h.title>
<h.author>unknown</h.author>
<imprint>
<pubPlace>Sri Lanka</pubPlace>
<publisher>unknown</publisher>
<pubDate>xx-xx-xx</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-26</date>
</creation>
<langUsage>Sinhala</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>             විකට චිත්රපට කුණුහරුප සිනමාවේම දිගුවක්ද?</p>

<p> </p>

<p>    iskud /,af,ka fyïn;a ù isá
fma%laIlhd kej;;a mjq,a msáka f.dia Ñ;%mg n,kag fhduq lrk ,oafoa ±ka olakg
,efnk úlg Ñ;%mg m%jK;djh úisks' fldf  ta w;r m%Odk ;ekla .kshs'tc "&amp;#9;u /,af,ka
fyïn;a ù isá fma%laIlhd kej;;a mjq,a msáka f.dia Ñ;%mg n,kag fhduq lrk ,oafoa
±ka olakg ,efnk úlg Ñ;%mg m%jK;djh úisks' fldf  ta w;r m%Odk ;ekla
.kshs'"</p>

<p>tc ""</p>

<p>    isxy, iskudfõ ish¨u wdodhï
jd¾;d ì| fyÆ Ñ;%mgh njg m;a ù ;snqfKa ~~ ? oksh,a oj,a ñf.,a -1 Ñ;%mghhs'  kuq;a ? oksh,a oj,a ñf.,a - 2 fï jk úg ish¨u
wdodhï jd¾;d ì| fy~~ljqo fndf,a we,sia~~ Ñ;%mghgo
ietc "&amp;#9;isxy, iskudfõ
ish¨u wdodhï jd¾;d ì| fyÆ Ñ;%mgh njg m;a ù ;snqfKa ~~ ? oksh,a oj,a ñf.,a -1
Ñ;%mghhs'  kuq;a ? oksh,a oj,a ñf.,a - 2
fï jk úg ish¨u wdodhï jd¾;d ì| fy~~ljqo fndf,a we,sia~~
Ñ;%mghgo ie</p>

<p>tc ""</p>

<p>    wfma iskudfõ úlg Ñ;%mg ìys ù
we;af;a w;eÕs,sj ,g;a jvd wvq ixLHdjls' úlg hk mdGh hgf;a fndfyda Ñ;%mg ks¾udKh
ù ;snqK;a ienE úlg rih tajdfhka Woa§mkh jkafkao hkak iel iys;h'tc "&amp;#9;wfma iskudfõ
úlg Ñ;%mg ìys ù we;af;a w;eÕs,sj ,g;a jvd wvq ixLHdjls' úlg hk mdGh hgf;a
fndfyda Ñ;%mg ks¾udKh ù ;snqK;a ienE úlg rih tajdfhka Woa§mkh jkafkao hkak iel
iys;h'"</p>

<p>tc ""</p>

<p>tc ""</p>

<p>    wfma Ñ;%mgj, úlg rih u;= lrk
wdldrh iy Ñ;%mgfha f;audfjka fm%alaIlhdf.a uki {dkkh lrk wdldrh mstc "&amp;#9;wfma Ñ;%mgj, úlg rih u;= lrk wdldrh iy
Ñ;%mgfha f;audfjka fm%alaIlhdf.a uki {dkkh lrk wdldrh ms</p>

<p>tc ""</p>

<p>    pd,s pema,ska uyd l,dlrejl=
njg b;sydi .; jQfha úlg Ñ;%mg yrydh' Tyqf.a ksyX iskudmgj, .eíù ;snQ fndfyda
lreKq j¾;udkfha fonia iys;  úlg Ñ;%mgj,
ke;' ydiHh hkq l=ulao@ ydiHh ;=tc "&amp;#9;pd,s pema,ska
uyd l,dlrejl= njg b;sydi .; jQfha úlg Ñ;%mg yrydh' Tyqf.a ksyX iskudmgj, .eíù
;snQ fndfyda lreKq j¾;udkfha fonia iys; 
úlg Ñ;%mgj, ke;' ydiHh hkq l=ulao@ ydiHh ;=</p>

<p>tc ""</p>

<p>    මෑතකදී අතිශයෙන් ජනපි්රයත්වයට පත් වූ '' මිස්ටර් බීන්
'' ටෙලි නාට්ය මාලාවේදී උසස් ගණයේ විකට කතාංග දීකගත හැකි අතර ඒවා විනෝදාස්වාදය
ඉක්මවා ගොස් සමාජමය දෘෂ්ටීන්ගෙන්ද යුක්ත විය. පුංචි තිරයේ චැප්ලින් බවට අද මිස්ටර්
බීන් පත්වෙමින් සිටී. නමුත් මෙතරම් තාක්ෂණික අතින් දියුණු පරිගණක, ඩිජිටල් රූප රාමු
සමග ස්ටීරියෝ නාදය ගෙන දෙන බොහෝ චිත්රපටවලට තවමත් චාලි චැප්ලින් පසුකිරීමට
සමත්කමක් දක්වා නැත.tc "&amp;#9;uE;l§ w;sYfhka ckms%h;ajhg
m;a jQ ~~ ñiag¾ îka ~~ fg,s kdgH ud,dfõ§ Wiia .Kfha úlg l;dx. ±l.; yels w;r
tajd úfkdaodiajdoh blaujd f.dia iudcuh oDIaàkaf.kao hqla; úh' mqxÑ ;srfha
pema,ska njg wo ñiag¾ îka m;afjñka isà' kuq;a fu;rï ;dlaIKsl w;ska ÈhqKq
mß.Kl\" äðg,a rEm rduq iu. iaàßfhda kdoh f.k fok fndfyda Ñ;%mgj,g ;ju;a
pd,s pema,ska miqlsÍug iu;alula olajd ke;'"</p>

<p>    tc "&amp;#9;"</p>

<p>     ~~ ? oksfh,a oj,a ñf.,a - 2 ~~ Ñ;%mgh tys uq,a Ñ;%mgh ;rï id¾:l
ù fkdue;' kuq;a m%n, m%pdrl wkq.%yh u; iy fgksika l=f¾" nkaÿ iurisxy f.a
ckm%sh;ajh ksid fuu Ñ;%mgh ckm%sh ùug n,mE idOl fõ' ~~ ? oksfh,a oj,a ñf.,a - 2
~~ Ñ;%mgfha pß;j, Ôj;a ùu ;=tc "&amp;#9; ~~ ?
oksfh,a oj,a ñf.,a - 2 ~~ Ñ;%mgh tys uq,a Ñ;%mgh ;rï id¾\:l ù fkdue;' kuq;a
m%n, m%pdrl wkq.%yh u; iy fgksika l=f¾\" nkaÿ iurisxy f.a ckm%sh;ajh ksid
fuu Ñ;%mgh ckm%sh ùug n,mE idOl fõ' ~~ ? oksfh,a oj,a ñf.,a - 2 ~~ Ñ;%mgfha
pß;j, Ôj;a ùu ;=</p>

<p>tc ""</p>

<p>    tfiau fuu Ñ;%mgfha l;dj
fmtc "&amp;#9;tfiau fuu Ñ;%mgfha l;dj
fm</p>

<p>tc ""</p>

<p>    ~~ ljqo fndf,a we,sia ~~
Ñ;%mgho ietc "&amp;#9;~~ ljqo fndf,a we,sia ~~
Ñ;%mgho ie</p>

<p>tc ""</p>

<p>    ÈfkaIa m%shidoa wOHlaIKh
l  ~~fldf Ñ;%mgho
;sr.; jQfha úlg Ñ;%mg hk mdGh hgf;ah' ;sr rpkh hï lsis ietc "&amp;#9;ÈfkaIa
m%shidoa wOHlaIKh l  ~~fldf Ñ;%mgho ;sr.; jQfha úlg Ñ;%mg hk mdGh hgf;ah' ;sr rpkh hï lsis ie</p>

<p>tc ""</p>

<p>    kqÿre wkd.;fha§ ;j;a úlg Ñ;%mg
lSmhlau ;sr.; ùug kshñ;h' iskudfõ .uka uÕg th fyd| w;aje,ls' Ñ;%mgh hkq l=ulao
hkak jgyd f.k fïjd ks¾udKh jkafka kï th b;d jeo.;ah' fï olajd wfma rfÜ ìysjQ
úlg Ñ;%mgj,ska lgmqrd úlg Ñ;%mg hhs lsj yelafla Ñ;%mg fol ;=klg muKs' tc "&amp;#9;kqÿre
wkd.;fha§ ;j;a úlg Ñ;%mg lSmhlau ;sr.; ùug kshñ;h' iskudfõ .uka uÕg th fyd|
w;aje,ls' Ñ;%mgh hkq l=ulao hkak jgyd f.k fïjd ks¾udKh jkafka kï th b;d
jeo.;ah' fï olajd wfma rfÜ ìysjQ úlg Ñ;%mgj,ska lgmqrd úlg Ñ;%mg hhs lsj yelafla
Ñ;%mg fol ;=klg muKs' "</p>

<p>tc ""</p>

<p>    f,dal m%lg úlg k¿jd pd,s
pema,ska jqj;a wfma iskudfõ pema,ska fcda wfíúl%u hhs lSfjd;a jrola fkdfõ'
olaI;u k¿fjl=j úlg k¿fjl =ùu wiSre jqj;a úlg k¿fjl=g pß;dx. k¿fjl= ùug mq¿jka
nj fcda wfíúl%u ~~ w,a,mq f.of¾~~ isg ~~ idrúg~~ yryd ~~ je,s l;ßka~~ Tmamq
lf  we;=¿
úlg iskudj we.hSug ,la fkdùu wjdikdjls'tc "&amp;#9;f,dal m%lg
úlg k¿jd pd,s pema,ska jqj;a wfma iskudfõ pema,ska fcda wfíúl%u hhs lSfjd;a
jrola fkdfõ' olaI;u k¿fjl=j úlg k¿fjl =ùu wiSre jqj;a úlg k¿fjl=g pß;dx. k¿fjl=
ùug mq¿jka nj fcda wfíúl%u ~~ w,a,mq f.of¾~~ isg ~~ idrúg~~ yryd ~~ je,s
l;ßka~~ Tmamq lf  we;=¿ úlg iskudj we.hSug ,la
fkdùu wjdikdjls'"</p>

<p>tc ""</p>

<p>    iEu f.orlu rEmjdyskS hka;%
;sfnk rd;%S" ld,fha nia fkdue;s" ldu /,af,ka iskudj jid.;a ld,hl
Ñ;%mg Yd,d fj;g nqre;= msáka fik. f.kajd .ekSug iu;a jQfha úlg Ñ;%mgj,g mskaisÿ
fjkakg nj ienEh' tfy;a wo mj;sk ;;ajfhka ñ§ úlg iskudj ;j;a bÈß mshjrla ;eîu
w;HdjYH idOlhls'tc "&amp;#9;iEu f.orlu rEmjdyskS hka;%
;sfnk rd;%S\" ld,fha nia fkdue;s\" ldu /,af,ka iskudj jid.;a ld,hl
Ñ;%mg Yd,d fj;g nqre;= msáka fik. f.kajd .ekSug iu;a jQfha úlg Ñ;%mgj,g mskaisÿ
fjkakg nj ienEh' tfy;a wo mj;sk ;;ajfhka ñ§ úlg iskudj ;j;a bÈß mshjrla ;eîu
w;HdjYH idOlhls'"</p>

<p> </p>






</body></text></cesDoc>