<cesDoc id="tel-w-science-agriculture-pogaku" lang="tel">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tel-w-science-agriculture-pogaku.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-13</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>పొగాకు సాగు</h.title>
<h.author>శ్రీనివాస్్</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1990</pubDate>
</imprint>
<idno type="CIIL code">pogaku</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 38.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-13</date></creation>
<langUsage>Telugu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>3-
 పొగాకు పంటలో భారభదేశము ప్రపంచములోనే ఒక
విశిష్ట స్థానాన్ని ఆక్రమించింది, వర్జీనియా, నాటు, బర్లీ, లంక,
నశ్యము, హుక్కా, బీడి, సిగార్్ వంటి రకాల పొగాకు మన
దేశములో పండించబడుచున్నాయి. అన్ని రకముల పొగాకు
విస్తీర్ణములో మనదేశము ప్రపంచములో రెండవస్థానములోనూ,
వర్జీనియా పొగాకు విస్తీర్ణములో మనదేశము మూడవ
స్థానములోనూ వున్నది. మన దేశములో అన్నిరకముల పొగాకు
కలసి 4.5 లక్షల హెక్టార్లలో పండించుచున్నారు. అందులో
వర్జీనియా పొగాకు విస్తీర్ణము 1.56 లక్షల హెక్టార్లు. ఆంధ్ర
ప్రదేశ్్లో అన్ని రకముల పొగాకు కలసి 22 లక్షల
హెక్టార్లలో పండించుచున్నారు. అందులో 1.52 లక్షల హెక్టార్లు
వర్జీనియా పొగాకు ఆక్రమించి, దేశంమొత్తముమీద వర్జీనియా
పొగాకు విస్తీర్ణములో 95 శాతంతో ఆంధ్రప్రదేశ్్ మొదటి
స్థానము ఆక్రమించియున్నది. పొగాకు పంటనుండి సాలీనా
ఎక్సైజు పన్నువల్ల 420 కోట్ల రూపాయలు ఆదాయము,
10 కోట్ల రూపాయలు విదేశమారకద్రవ్యము లభిస్తున్నాయి.
వర్జీనియా పొగాకు ఉభయ గోదావరి, ఖమ్మం, కృష్ణా,
గుంటూరు, ప్రకాశం, నెల్లూరు జిల్లాలలో పండిస్తున్నారు.
4-
ఉభయ గోదావరి జిల్లాలలో అన్ని రకముల పొగాకు కలసి
దాదాపు 38 వేల హెక్టార్లలోనూ, ప్రత్యేకించి వర్జీనియా
పొగాకు 28 వేల హెక్టార్లలోనూ పండించుచున్నారు. ఖమ్మం
జిల్లాలో 10 వేల హెక్టార్లలో వర్జీనియా పొగాకు
పండించుచున్నారు.
 ఆంధ్రప్రదేశ్్లో వర్జీనియా పొగాకు విస్తీర్ణము గత
కొద్ది సంవత్సరములనుండి పెరుగుతూ వచ్చింది. కాని పొగాకు
ఎగుమతులు మాత్రము ఆశించినంతగా పెరుగుటలేదు.
వర్జీనియా పొగాకు నాణ్యతకూడా వాతావరణ వైపరీత్యమువల్ల
యితర కారణాలవల్ల తగ్గుచున్నది. దీనికి ముఖ్యకారణము
రైతులు పంటమార్పిడి పద్ధతి నవలంభించుటలేదు. ప్రతి
సంవత్సరము ఒకే పొలములో వర్జీనియా పొగాకు
పండించుచున్నారు. ఇందువల్ల పొగాకుకు తెగుళ్ళు, చీడపురుగులు
ఎక్కువవుతున్నాయి. కలుపు మొక్కల సంఖ్య
పెరుగుతున్నది. పరాన్నభుక్కుల, పొగమల్లె పీడకూడ఼
ఎక్కువవుతూంది. కాని రైతులు లాభదాయకమైన పంట మార్పిడి
పద్ధతి నవలంభించాలి. చవిటి నేలలందు, పల్లపు, భూములందు
పొగాకు వేయకూడదు. పొగాకు రైతులు తమ పొగాకుకు
మంచి ధరపొందాలంటే, అనువైన భూములు, నీటి
వనరులున్నచోట పంటవేసి సరియైన సాగుపద్ధతులు
అవలంభించవలయును. వర్జీనియా పొగాకు పంటలో మెళుకువలు యీ క్రింద
పొందుపరచబడ్డాయి.
5-
 పొగాకు నారుమళ్ల పెంపకము
1. నారుమళ్ళకు, స్థలము ఎన్నుకొనుట:
 నారుమళ్ళు కట్టు స్థలము ప్రతి సంవత్సరము
మార్చుచుండవలయును. వేరుకాయ తెగులు సోకిన నేలలందు నారు
మళ్ళు కట్టరాదు. నారుమళ్ళు కట్టు స్థలం ఎత్తుగను, కొంచం
ఏటవాలుగను, నీరు నిలువ వుండునటువంటిదిగను వుండ
వలెను.
2. దున్నుట:
 నారుమళ్ళు వేసవిలో బాగా లోతుగా దుక్కి
దున్నవలెను. తరువాత మెత్తని దుక్కివచ్చే వరకు మరియు కలుపు
మొక్కలు నివారణ అయ్యేవరకు తరచుగా దున్నుచుండ`
వలెను.
3. కాల్చుట:
 నారుమళ్ళుకట్టు స్థలము విత్తుటకు ముందు ఎకరము
నారుమడికి సుమారు 900 బస్తాల ధాన్యపు వూకతో కాల్చిన
యెడల తెగుళ్ళు, చీడలబారి నుండి మరియు కలుపు మొక్కల
బారినుండి చాలావరకు నివారించుకోవచ్చును. ముందుగా
దీనివల్ల ఎక్కువ ఖర్చు అయినప్పటికి పురుగు, తెగుళ్లు
నరికట్టే మందుల ఖర్చు నుంచి, కలుపు తీతకగు కర్చు నుండి
తప్పించుకొనవచ్చును.
6-
4. నారుమళ్ళు తయారు చేయుట:
 నారుమళ్ళు 1.22 మీటర్లు వెడల్పుగాను, 10 మీటర్ల
పొడవుగాను, 15 సెం.మీ ఎత్తుగాను తయారు చేయవలెను.
మడికి మడికి మధ్య అర మీటరు వెడల్పు 10 సెం.మీ.
లోతుదారులు వుండుట మంచిది.
5. ఎరువు వేత:
 ఎకరానికి 40 బళ్ళ పశువుల పెంటగాని, ఫిల్టరుప్రెస్్
కేకునుగాని వేయవలెను. విత్తుటకు ముందు ఎకరమునకు 20
కిలోల అమ్మోనియం సల్ఫేటు, 120 కిలోల సూపరుఫాల్సేట్్
మరియు 40 కిలోల పొటాషియం సల్ఫేటు వేయవలెను. తేలిక
నేలలలో 40 కిలోల డోలమైట్్ కూడా వేయవలెను. తర్వాత
విత్తనము మొలకెత్తిన ఒక వారం నుండి 40 రోజుల
వ్యవధిలో 5 సార్లు ప్రతిసారి చ.మీ. నారుమడికి 35 నుండి
70 గ్రాముల చొప్పున అమ్మోనియం సల్ఫేటు ఎరువు వేయ
వలెను. ఎరువువేసిన తరువాత వారం రోజులవరకు మొక్కలు
పీకగూడదు.
6. విత్తుట:
 గుర్తించబడిన విత్తనములనే, నమ్మకమైనచోట నుండి
తెచ్చుకొనవలెను. ఎకరమునకు ఒక కేజి విత్తనముకంటే
ఎక్కువ విత్తనము వాడరాదు. విత్తనము ఎక్కువగా చల్లిన
నారుమడి ఒత్తుగా, బలహీనంగా వుండుటయేగాక మాగుడు
తెగులుకూడా వ్యాపించును. విత్తనములు బాగా మొలకెత్తుటకు
చిన్న మొక్కలను ఎండ, గాలి, పెద్ద వర్షములకు తట్టుకొని
7-
మొక్క బాగా మొలుచుటకు నారుమడిపై సర్వీ ఆకులుగాని,
గడ్డిగాని కప్పుట అవసరము.
7. పురుగు, తెగుళ్ళు నివారణ:
 విత్తుటకు ముందుగా నారుమళ్ళను 5:5:50 బోర్డో
విశ్రమముతో తడపవలెను. మరియు 4 శాతం ఎండో సల్ఫాన్్
పొడి ఎకరమునకు 12 కేజీల చొప్పున పై మట్టిలో బాగా
కలియునట్లుగా చల్లాలి. దీనివల్ల వానపాము, చీడల బెడద
పోగొట్టుకొనవచ్చును.
8. సస్యరక్షణ:
 పొగాకు నారుమళ్ళును మాగుడు తెగులు సులభముగా
ఆశించును. విత్తిన తర్వాత వారమునకొకసారి 2:2:50
పాళ్ళుగల బోర్డో విశ్రమముతోగాని, సులభముగా లభ్యమగు
శిలీంద్ర సంహారక మందు 150 గ్రాములు 100 లీటర్ల
నీటిలో కలిపిగాని చల్లుచుండవలెను. వర్షములు కురియునపుడు
యీ తెగులు ఎక్కువగా ఆశించి సరైన సస్యరక్షణ చర్యలు
తీసుకోనట్లయితే నారుమళ్ళూ సమూలముగా నాశనమవుతాయి
కాండము కుళ్ళు తెగులునకు, మాగుడు తెగులు నివారణకు
వాడు శిలీంద్ర సంహారక మందులను వాడవలెను. ఆకుమచ్చ
తెగుళ్ళకు డై.థేన్్. జడ్్-78 గాని, పైటలాన్్గాని 200
గ్రాముల పొడి 100 లీటర్ల నీటిలో కలిపి పిచికారి చేయ
వలెను. లేక ఎకరమునకు 80 నుండి 200 గ్రాముల బావి
స్టిన్్గాని చల్లవలెను. లద్దెపురుగు మరియు ఆకుపచ్చని
గొంగళి పురుగులు అధికముగా ఆశించి నారుమళ్ళను నాశనం
8-
చేయును. వీటి నివారణకు ఎండోసల్ఫాన్్ 35 శాతం ఇ.సి.
30 మిల్లీలీటర్లు 22 లీటర్ల నీటిలో కలిపిగాని, కార్బరిల్్ 50
శాతం డబ్ల్యు. పి. 50 గ్రాములు 22 లీటర్ల నీటిలో కలిపిగాని,
మొనోక్రోటోఫాస్్ 40 శాతం ఇ.సి. 22 మిల్లీలీటర్లు 22 లీటర్ల
నీటిలో కలిపిగాని, క్లోరోపైరిఫాస్్ 20 శాతం ఇ.సి. 30 మిల్లీ
లీటర్ల 22 నీటిలో కలిపిగాని, ట్రైక్లోరోఫాస్్ (డిప్టిరిక్సు) 40
మిల్లీలీటర్లు 22 లీటర్ల నీటిలో కలిపిగాని చల్లినివారించవచ్చును.
ఎకరాకు 200-300 లీటర్ల మందునీరు అవసరం.
 చవుకగా, సులభముగా లభించు వేప పప్పు ద్రవమును
కూడా వుపయోగించి లద్దెపురుగు ఆకులనుతిను ఇతర
పురుగులను నివారించుకోవచ్చునని కేంద్ర పొగాకు పరిశోధనాలయం
రాజమండ్రివారి పరిశోధనలో తేలినది.
9. వేప పప్పు ద్రవము తయారుచేయు విధానం:
 వేపగింజలను బాగా ఎండబెట్టి, దంచి,
పిండిచేయవలెను. 100 గ్రాముల వేప పప్పు పొడిని పలుచటి గుడ్డ
సంచిలో చుట్టి 10 లీటర్ల నీటిలో నానబెట్టి కొంతసేపయిన
తరువాత 15 నిముషముల సేపు వేప పప్పు పొడిసంచి బాగా
పిండవలెను. తర్వాత ఆ సంచిని తీసివేయవలెను. ఇప్పుడు
1 శాతం వేపు పప్పు ద్రవము లభించును. ఈ విధముగనే 10
లీటర్ల నీటిలో 200 గ్రాముల పొడి నుపయోగించిన 2 శాతం
ద్రవము తయారగును.
9-
 లద్దెపురుగు నివారణకు మొదటి దఫా 1 శాతం ద్రవం
విత్తనము మొలకెత్తిన 20 రోజులకు వాడవలెను. తర్వాత పది
రోజుల కొకసారి చొప్పున 3 సార్లు 2 శాతం ద్రవం పిచికారి
చేయవలెను. ప్రతి నారు మొక్క బాగా తడిసేటట్లుగా
చల్లవలెను. 100 చదరపు మీటర్ల నారుమడికి 50 నుంచి 70 లీటర్ల
ద్రవము కావలసియుండును.
తిరగ నాటుట:
 నారుమళ్ళలో 20-30 రోజులుగల నారును తిరిగి వేరే
ఖాళీమడిలో నాటుట ద్వారా నారుమడిలో వత్తు తగ్గించ
వచ్చును. ఆ విధముగా తిరుగ నాటుటవల్ల నారుమడి వత్తు
10-
తగ్గి నారు ఏపుగా, ధృఢముగా పెరుగుతుంది.
తద్వారాపొలములో నాటినపుడు త్వరగా కోలుకుంటాయి.
 విత్తిన 6 నుండి 8 వారములలో మొక్క నాటుకు
తయారగును. నారు తీసే 9,4 రోజులు ముందుగా నారు
మడికి నీరు కట్టడము తగ్గించాలి. నారు తీసే 10 రోజుల
ముందునుండి నారుమడికి నత్రజని ఎరువులు వేయటము
మానివేయాలి. దీనివలన నారు ధృఢముగా ఉంటుంది.
నాటుటకు తయారయిన నారు 15 సెం.మీ. ఎత్తువుండి, పెన్సిలు
మందము ఉండవలెను. వేర్లు బాగా పోసుకొని వుండవలెను.
వేరుకాయ, ఆకుముడత, సీతాఫలపు తెగులు సోకిన మొక్కలు
నాటరాదు. నాట్లు అయిపోయిన తర్వాత నారు అవసరము
లేదనుకున్నప్పుడు మళ్ళను దున్నివేయవలెను. రెండవపంట
వేసుకోదలచినప్పుడు వేరుశనగగాని, నువ్వుగాని, మిరపగాని
వేయవచ్చును. పొగాకు నారుమళ్లు పెంచటము ఖర్చుతో
కూడిన పని, ఎకరము నారుమడి పెంచాలంటే దాదాపు
4000 నుండి 5000 రూపాయలు ఖర్చవుతుంది. ఎకరమునారు
మడినుండి వచ్చే నారు సుమారు 60 నుండి 100 ఎకరములలో
నాటటానికి సరిపోతుంది.
 వర్జీనియా పొగాకుసాగు
 ప్రకాశం, నెల్లూరు, గుంటూరు, కృష్ణా, ఉభయ
గోదావరి, ఖమ్మం జిల్లాలలో విస్తారముగా పండిస్తారు. కర్ణాటక
11-
రాష్ట్రములో హున్సూరు, దావణగిరి వర్షాకాలము తేలిక
నేలలో వర్జీనియా పొగాకు పంట బాగా అభివృద్ధి
చెందుచున్నది.
1. నేలలు:
 వర్జీనియా పొగాకు నల్లరేగడిలోనూ మరియు తేలిక
నేలలయినటువంటి ఎర్రనేలలలోనూ, ఎర్రగరప పొలాలలోనూ
ఇసుక గరప నేలలలోనూ పండించవచ్చును. ఉత్తర ప్రాంతపు
తేలిక నేలలో నీటి ఆధారము క్రింద పొగాకు పండిస్తారు.
మన రాష్ట్రములో ఎక్కువగా నల్లరేగడి నేలలో
పండిస్తున్నారు. ఈ మధ్య తేలిక నేలలో వర్జీనియా పొగాకు
అభివృద్ధి క్రింద ఎర్రనేలలు, ఇసుక గరపనేలలో (ఉభయ
గోదావరి, ఖమ్మం జిల్లాల ఉత్తర తేలిక నేలలోను, ప్రకాశము.
నెల్లూరు జిల్లాల దక్షిణ తేలిక నేలలలోను) వర్జీనియా పొగాకు
సాగు బాగా అభివృద్ధి చెందినది. ఒక్క పశ్చిమ గోదావరి
జిల్లాలోనే తేలిక నేలలో వర్జీనియా పొగాకు 14000
హెక్టార్లలో పండించుచున్నారు. తేలిక నేలలో వర్జీనియా
పొగాకునకు విదేశాలలో మంచి గిరాకియున్నది. వర్జీనియా పొగాకు
ఉభయగోదావరి, ఖమ్మం జిల్లాలో యిసుక గరప నేలలో రబీ
పంట కాలములో నీటితడులు పెడుతూ పండిస్తారు. ప్రకాశం,
నెల్లూరు జిల్లాలలో ఎర్రగరప నేలలో వర్షాధారములోనే
సెప్టెంబరులో నాటి, జనవరి-ఫిబ్రవరి నెలల్లో పూర్తిజేస్తారు. కాని
1978-79వ సంవత్సరము వర్షము సరైన సమయములో
పడక పొగాకు నాణ్యత బాగా దెబ్బతిని యితర దేశముల
12-
వారికి నచ్చకపోవటము వలన మన రైతులు బాగా నష్ట
పోయారు. నల్ల రేగడి నేలలోని రైతులుకూడా అవసరమైన
దానికన్న ఎక్కువగా ఉత్పత్తి చేయుటవలన గిరాకీలేక దెబ్బ
తిన్నారు. కాన రైతులు తమ పొగాకు విస్తీర్ణము అనువుగా
లేని నేలల్లో తమంత తామే తగ్గించుకోవాలి. పల్లపు
భూములలోనూ, చవిటి భూములలోనూ పొగాకు వేయరాదు.
సేద్యములో శాస్త్రీయ పద్ధతులు అవలంబించి నాణ్యమైన పొగాకు
ఉత్పత్తి చేయాలి.
2. రకములు:
 నాణ్యతగల వర్జీనియా పొగాకు పండించాలంటే మన
పొలానికి, ప్రాంతానికి సరిపడు రకములనే నమ్మకమైన
చోటనుండి తెచ్చుకోవాలి. నాణ్యమైన వర్జీనియా పొగాకు
విత్తనములు రాష్ట్ర వ్యవసాయశాఖ ద్వారాగాని, సి.టి.ఆర్్.ఐ
రాజమండ్రి లేక వారి ఉపకేంద్రముల నుండిగాని పొగాకు బోర్డు
వారి ద్వారాగాని, రాష్ట్ర విత్తనాభివృద్ధి సంస్థవారివద్ద నుండి
గాని పొందవచ్చును.
మన రాష్ట్రములో సాగుచేయుటకు సరిపడు రకములు:
1. తక్కువ తేమగల నల్ల రేగడి నేలకు....కనకప్రభ,
 ధనదాయి
 (జయశ్రీ)
13-
2. ఎక్కువ తేమగల నల్లరేగడి నేలలకు .... కనకప్రభ,
3. ఉత్తర ప్రాంతపు తేలిక నేలలకు
(ఉభయ గోదావరి, ఖమ్మం జిల్లాలకు)
4. దక్షిణ ప్రాంతపు తేలిక నేలలకు
(ప్రకాశము, నెల్లూరు జిల్లాలకు)
5. నదిగట్టు లంకలకు....
 కేంద్ర పొగాకు పరిశోధనాలయము, రాజమండ్రివారు
అను సీతాఫలపు తెగులు నిరోధక రకములను
రూపొందించారు.
 ఇంకా పరిశోధనలలోనే యున్నవి. ఈ నిరోధక
రకములు కనకప్రభలవలనే వుండి తెగులు నిరోధక శక్తి కలిగి
వుంటాయి. కేంద్ర పొగాకు పరిశోధనాలయమువారి సలహాపై
రాష్ట్ర వ్యవసాయశాఖ పొగాకు వారు ఉభయ గోదావరి
జిల్లాలలో 1978-79 సం//రములో సాగుచేసిన వర్జీనియా పొగాకు
రకం దిగుబడి నాణ్యత మిగిలిన రకాలకంటే ఎక్కువగా
యున్నది. ఈ 2640 క్రొత్తరకము యీ సంవత్సరము
1979-80 లో కూడా వారి సలహాపై నాలుగు గ్రామాల్లో
ఒక్కొక్క ఎకరము పరిమితిలో పశ్చిమ గోదావరి జిల్లాలో
వేయించటము జరుగుతోంది.
14-
 ఉత్తర ప్రాంతపు తేలిక నేలలు
1. నేలలు:
 వర్జీనియా పొగాకు సాగుకు నిర్ణయించిన భూమి
పోయిన సంవత్సరము పొగాకుకు వేయనిదై వుండాలి. నేలలు
తేలికగాను, నీరు పీల్చుకొనే గుణం కలదిగాను వుండాలి.
వాలు ఎక్కువగానున్ననూ, వేరుకాయ తెగులు సోకిన
పొలములలోనూ వర్జీనియా పొగాకు వేయరాదు. నేలయొక్క
ఉదజని సూచిక (పి.హెచ్్.) 7 కన్నా తక్కువ వుండవలెను.
పొలములలోని మట్టిని, సాగునీటిని ముందుగా పరీక్షచేయించు
కోవాలి. మట్టిలో క్లోరైడ్సు పదిలక్షల పాళ్ళకు 100 కన్నా
ఎక్కువగాను, నీటిలో 50 పి.పి.యమ్్. కన్నా ఎక్కువగాను
వున్నచో పొగాకు సాగుకు పనికిరాదు.
 భూములను ఎండాకాలములో బాగా లోతుగా దుక్కి
దున్నాలి. పశువుల ఎరువు వేసేటపుడు మొక్క నాటేముందు
బాగా దున్నాలి.
2. ఎరువులు:
 ఎకరమునకు 4 టన్నులు పశువుల ఎరువుకాని, చక్కెర
ఫ్యాక్టరీలదగ్గరవున్నవారు ఫిల్టరు ప్రెస్్కేక్్నుగాని నాటుటకు
నెలరోజులు ముందు వాడవచ్చును. లేని పక్షములో రోజుకు
రోజుకు 100 గొర్రెల చొప్పున 8 రోజులు మంద కట్టవచ్చును.
 ఎరువులు వేయుటలో చాలా జాగ్రత్త, మెళుకువలు
పాటించాలి. తేలిక నేలలో వర్షములకు వేసిన రసాయన
ఎరు
15-
వులు కొట్టుకుపోతాయి. కాబట్టి నత్రజని ఎరువులు దఫ
దఫాలుగా వేయాలి. ఈ నేలలో మెగ్నీషియం లోపముంటుంది.
కాబట్టి మెగ్నీషియం సల్ఫేటు కూడా ఎరువులలో కలపాలి.
మామూలుగా భూసారము ననుసరించి ఎకరమునకు 30 నుండి
40 పాళ్ళ నత్రజని, 50 నుండి 80 పాళ్ళ భాస్వరము,
50 నుంచి 80 పాళ్ళ పొటాష్్ మరియు 12-15 పాళ్ళ
మెగ్నీషియం వాడాలి. నత్రజని, అమ్మోనియం సల్ఫేటు, కాల్షియం
అమ్మోనియం నైట్రైటు రూపములోగాని, భాస్వరము,
సూపర్్ ఫాస్ఫేటు రూపములోనూ పొటాష్్, సల్ఫేట్్ ఆఫ్్
పొటాష్్ రూపములోనూ మెగ్నీషియం, డోలమైట్్ సున్నపు
రాయి రూపములోనూ వాడాలి.
 సగము నత్రజని, మొత్తము భాస్వరము, సగము
పొటాష్్ మొత్తము మెగ్నీషియం నాటుటకు వారము రోజుల
ముందు నాగలిచాలులోగాని లేక నాటిన వారము రోజుల
లోపుగా మొక్క కిరువైపులా 12 సెం.మీ. దూరములో,
15 సెం.మీ. లోతుగా వేయాలి. మిగిలిన సగము నత్రజని,
సగము పొటాష్్మొక్క నాటిన 3 వారములలో వేయాలి.
 తేలికనేలలో నత్రజని ఎరువు వేసిన తరువాత, అధిక
వర్షాలు వచ్చినట్లయితే ఎరువు కొట్టుకుపోతుంది.
అటువంటపుడు అదనపు నత్రజని వేయవలెను.
3. నాటుట:
 మొక్కకు - మొక్కకు మధ్య 60 సెం.మీ. దూరంలో
చాలుకు - చాలుకు మధ్య 100 సెం.మీ. దూరంలో నాటాలి.
16-
సాధారణంగా సెప్టెంబరు ఆఖరు వారములోగాని అక్టోబరు
మొదటివారంలోగాని నాటుట మంచిది. వాతావరణ
పరిస్థితులను బట్టి నాటు సమయములను మార్చుకొనవచ్చును.
నవంబరు 15 నాటికి నాట్లు అయిపోవటం మంచిది. మొక్క
నాటిన తర్వాత మొక్కమీద తాటియాకులతో గొడుగులాగా
అమర్చుట వలన తీవ్రమైన ఎండనుంచి, అధిక వర్షం నుంచి
మొక్కకు మారాకు వేయువరకు కాపాడవచ్చును. మణేదలు
నాట్లు అయిన వారంరోజుల లోపుగానే పూర్తిచేయాలి.
4. ఎడసేద్యం:
 మొక్కలు బాగా నాటుకున్న తర్వాత గొర్రు,
గుంటకలతో తరచుగా చాళ్ళమధ్య ఎడసేద్యము చేయవలెను. దీని
వల్ల పై నేల గుల్లఅవటమే గాకుండా కలుపు మొక్కలు కూడా
నశించును.
5. నీరుకట్టుట:
 మొక్కనాటిన 2,3 రోజుల తర్వాత నేల పొడిగా
వుండిన కుండలతో నీరు పోయవలెను. నీరుకట్టుటలో చాలా
జాగ్రత్త మెళకువ అవసరము. సాగునీటిని ముందుగా
భూసార పరీక్షా కేంద్రములలో పరీక్ష చేయించుకొని నీటిలో
పదిలక్షల పాళ్ళలో క్లోరైడ్సు శాతము 50 పాళ్ళకన్నా
మించివుండిన ఆ నీరు సాగుకు పనికిరాదు. సరైన
సమయములో తగినంత నీరు మాత్రమే పెట్టవలెను. సాధ్యమైనంత
వరకు నీరుకట్టుట తగ్గించుట మంచిది. సెప్టెంబరు చివరి
వార
17-
ములో నాట్లు వేసినట్లయితే, ఒకటి, రెండుసార్లు నీరుకట్టుట
తగ్గించవచ్చును. అత్యధికముగా నీరుకట్టుటవలన తేలికనేలలు
కాబట్టి పోషక పదార్ధములు కొట్టుకొని పోవును. ఆకులో
క్లోరైడ్సు శాతము పెరిగి నాణ్యత తగ్గును. కాబట్టి సరిపడు
నీరు మాత్రమే కట్టవలెను. నాటిన 2 లేక 3 వారములకు
మొదటిసారి, తర్వాత ప్రతి 15 రోజుల కొకసారి చొప్పున
6 లేక 7 సార్లు పొగాకుకు నీరుకట్టిన సరిపోవును, మొదటిసారి
మరియు రెండదఫా ముప్పాతిక అంగుళం నీరుకట్టిన సరి
పోవును. తర్వాత ఒక అంగుళం నీరు కట్టవలయును. తల
కొట్టినపుడు ఒకసారి మాత్రము కొంచెము భారీగా అంటే
1.5 అంగుళం నీరు కట్టవలెను. ఏమైనప్పటికీ నీరుకట్టుట
వర్షాభావ పరిస్థితిపై ఆధారపడి యుంటుంది. సామాన్యముగా
ఉదయపు సమయములో ఆకు వాడినట్లు కనబడితే నీరు కట్ట
వలసిన సమయము ఆసన్నమైనదని గ్రహించవలయును.
6. తలకొట్టుట:
 సుమారుగా మొక్క 18,20 ఆకులు వేసిన తర్వాత
పూబంతిని సమూలముగా తీసి వేయవలయును. దీనినే తల
కొట్టుట లేక "టాపింగ్్" అందురు. తర్వాత మొక్క కణుపుల
వద్దనుండే అంకురాలు కూడా ఎప్పటికప్పుడు
తీసివేయవలయును. దీనినే జిడ్డుతీయుట అందురు. తోట బాగా ఏపుగా
చురుకుగా పెరుగుచున్న ఎడల తలకొట్టుట ఆలస్యము
చేయవలయును. ఆకులు సమయానికి పక్వానికి రానియెడల
18-
ఆలస్యముగా చేయవలయును. దీనివలన నాణ్యతగల ఆకు
దిగుబడి అగును.
 జిడ్డు లేక పిలకలు తీయుటకు ఒక తేలిక మార్గము
కలదు. పై ఆరు కణుపులవద్ద మొగ్గలమీద కొద్దిగా కొబ్బరి
నూనె రాసినట్లయిన పిలకలు రాకుండా ఆగిపోవును.
7. ఆకు రెలుచుట:
 సుమారు నాటిన 70-75 రోజులకు దిగువ ఆకులు
పక్వానికి వచ్చును. తర్వాత ప్రతి 6,7 రోజులకు దిగువనుండి
2 ఆకులు పక్వానికి వస్తాయి. పక్వానికి వచ్చిన వెంటనే
ఆకులను కొట్టాలి. దిగువ ఆకులు కొంచెము పచ్చిగా వున్నపుడే
రెలచాలి. మధ్య యిరుపు ఆకులు పక్వానికి వచ్చిన తర్వాత
రెలచాలి. చివర ఆకులు బాగా పక్వానికి వచ్చిన తర్వాత
రెలచాలి ప్రతిసారి 2 ఆకుల కంటే ఎక్కువ రెలచరాదు.
మొత్తము 9 లేక 10 సార్లు రెలచవలసియుంటుంది.
 1829 words
8. కాల్చుట:
 వర్జీనియా పొగాకు క్యూరు చేయుటకు పొగాకు
సేద్యమునకు ఖర్చులో సుమారు 4వ వంతు ఖర్చు అగును. 5 లేక
5 1/2 ఎకరములలోని ఆకు సుమారుగా 700 కర్ర అవుతుంది.
ఒక 16 16 16 అ. బ్యారనుకు సరిపోవును. పొట్టి బ్యారను
అంటే 162410 1/2 అ. బ్యారను అయితే 8:10 కర్ర
పడుతుంది. అంటే 6 ఎకరములలోని ఆకు సరిపోతుంది.
బ్యారనులో కర్రలు వత్తుగా పెట్టకూడదు. ఒకసారి కాల్చుటకు 5
లేక 6 రోజులు పడుతుంది.
19-
9. పొట్టి బ్యారను:
 CTRI - రాజమండ్రి వారు 16 అడుగుల పొడవు, 24
అడుగుల వెడల్పు, 10 1/2 అడుగుల ఎత్తుతో పొట్టి బ్యారనును
తయారు చేసారు. పశ్చిమ గోదావరి జిల్లా, తూర్పుగోదావరి
జిల్లాలలో చాలామంది రైతులు పొట్టి బ్యారన్లు కట్టారు. ఈ
బ్యారన్లవల్ల కాల్పుకగు బొగ్గు, కలపఖర్చు తగ్గటమేకాకుండా
కాల్పుకగు సమయములో కూడా 20 నుండి 36 గంటలు
తగ్గుతుంది. దీని ఎత్తు తక్కువుండి రెండు తలుపులుండుటవల్ల
కర్ర లోడింగుచేయుట, అన్్లోడింగ్్చేయుయట సులభముగాను,
త్వరగాను జరిగికూడా ఛార్జీలు తగ్గుతాయి. పొట్టి బ్యారను
కట్టుటకయ్యే ఖర్చు మామూలు బ్యారను కట్టుటకయ్యే ఖర్చు
సమానముగా నున్నది.
20-
10. గ్రేడింగు:
 తేలిక నేలలలోని వర్జీనియా పొగాకుకు మంచి ధర
రావాలంటే మొక్క స్థితి (ప్లాంట్్ పొజిషన్్) (వర్గ విభజన)
(గ్రేడింగు) చేయవలెను. అంటే మొదట రెండు వైపుల ఆకు
కలిపివేసి విడిగా గ్రేడింగు చేయవలెను. అలాగుననే తర్వాత
రెండుయిరుపుల ఆకు విడిగానూ, ఆతర్వాత రెండుయిరుపుల
ఆకు విడిగానూ మరియు తర్వాత మిగిలిన అన్ని యిరుపుల
ఆకు కలిసి విడిగానూ గ్రేడు చేయవలెను. ఆ విధముగా
మొక్క స్థితినిబట్టి ఆకు నాలుగు భాగాలుగా విభజించి గ్రేడు
చేయాలి. మధ్య యిరుపుల ఆకు ఎక్కువ ధర లభిస్తుంది.
దిగువ ఆకులకు, చివరి ఆకులకు తక్కువ ధర ఉంటుంది.
11. పంట మార్పిడి:
 వర్జీనియా పొగాకు సాగులో పంట మార్పిడి చాలా
ముఖ్యము. నాణ్యత కల పొగాకు పొందాలంటే, వేరుకాయ
అరికట్టబడాలంటే పంట మార్పిడి తప్పనిసరి. ఈ దిగువ
సూచించిన విధముగా పంటమార్పిడి చేయవచ్చును.
1. మొదటి సంవత్సరము - పప్పుజాతి పైరులయినటువంటి
 కంది లేక వేరుశనగ లేక బీర,
 దోస మొదలగు కూరగాయలు
రెండవ సంవత్సరము - నువ్వు లేక సజ్జ
మూడవ సంవత్సరము - వర్జీనియా పొగాకు
2. మొదటి సంవత్సరము - నువ్వు లేక సజ్జ లేక ఖాళీగా
 వుంచుట
రెండవ సంవత్సరము - వర్జీనియా పొగాకు.
21-
 మిరపగాని, నాటు పొగాకుకాని వేసిన పొలమువో
నత్రజని ఎక్కువగా వుంటుంది. కాబట్టి మరుసటి సంవత్సరం
వర్జీనియా పొగాకు వేయరాదు. అలాగే బొబ్బర్లు, ఉలవవేసిన
పొలాల్లో వేరుక఼ాయవుండే అవకాశముంటుంది. కాబట్టి ఆ
పొలాల్లో పొగాకు వేయకూడదు.
 నల్లరేగడి నేలలో వర్జీనియా పొగాకు సాగు
1. నేలలు:
 నీరు నిలువవుండే భూములు, పల్లపు భూములు, చవుడు,
వర్జీనియా పొగాకు సాగుకు పనికిరావు. మెరక పొలములు,
కొద్దివాలువున్న పంట పొలములు వర్జీనియా పొగాకు సాగుకు
శ్రేష్టము. ఎక్కువ నత్రజని వున్న పొలములలో వర్జీనియా
పొగాకు వేసినచో ఎక్కువ పెరిగి ఆకుకు సరైన రంగురాదు.
కాని మట్టి నమూనా పరీక్షచేయాలి.
2. దున్నుట:
 వేసవిలో బాగా లోతుగా ట్రాక్టరుతో దుక్కిదున్నాలి.
తర్వాత జూన్్, ఆగష్టు మాసములలో గొర్రుతోగాని, నాగలితో
గాని, కలుపు మొక్కలు రాకుండా పైనేల గుల్లబారుటకు
పశువుల ఎరువులు భూమిలో కలుపుటకు దున్నుతుండాలి.
3. ఎరువులు:
 ఎకరమునకు 3 మెట్రిక్్ టన్నుల పశువుల ఎరువు
నాటుటకు 6 వారముల ముందు తోలుట శ్రేష్టము. తర్వాత
భూసార పరీక్ష ననుసరించి ఎకరమునకు 20 నుంచి 30
22-
పౌన్ల నత్రజని, 30 నుంచి 50 పౌన్ల భాస్వరము, 15
నుంచి 30 పౌన్ల పొటాష్్ వేయవలెను. నత్రజని
అమోనియం నైట్రేట్్ రూపములోగాని వేయవచ్చును. భాస్వరము
సూపర్్ ఫాస్ఫేట్్ రూపములో, పొటాష్్ సల్ఫేట్్ ఆఫ్్ పొటాష్్
రూపములో వేయాలి, మ్యూరియేట్్ ఆఫ్్ పొటాష్్ ఎట్టి
పరిస్థితులలోనూ వాడకూడదు. ఎరువులు చివరి దుక్కిలో నాగలి
చాలులో మొక్కదగ్గర పడేటట్లు వేయాలి.
4. నాటుట:
 మొక్కలు 8080 సెం.మీ. దూరంలోగాని, 80
60 సెం.మీ. దూరములోగాని నాటాలి. మామూలుగా
అక్టోబరు నెలలో నాటుట మంచిది. మణేధలు వున్నట్లయితే
వెంటనే వేయించాలి.
5. అంతర కృషి:
 నల్ల నేలలు పగుళ్ళు వస్తాయి. కాబట్టి పగుళ్ళలు
అరికట్టుటకు, పదును పోకుండా కాపాడుటకు, కలుపు మొక్కల
నివారణకు తరచుగా అంతరకృషి చేయవలయును.
6. నీరు కట్టుట:
 మొక్క నాటిన తర్వాత వర్షములు అసలు కురవక
వాతావరణము పొడిగా వున్నట్లయితే 40-50 రోజులకు
ఒక్కసారి మాత్రము నీరు కట్టిన మొక్క బాగా పెరిగి
నాణ్యత చెడకుండా దిగుబడి వచ్చును. కాని నీరు కట్టేముందు
ఆ నీటిని పరీక్ష చేయించుకొని, నీటిలో క్లోరైడ్సు 30
23-
పి.పి.యమ్్. కన్నా ఎక్కువ వుంటే ఆ నీటిని పొగాకు
సాగుకుపయోగించరాదు.
7. తలకొట్టుట:
 తలకొట్టుటలో మెళుకువ, విచక్షణ అవసరము, తోట
బాగా ఏపుగా ముదురు ఆకుపచ్చ రంగుగా పెరుగుచుండిన
తల కొట్టకూడదు. ఆకు లేత ఆకుపచ్చగా వుండి పెరుగుదల
లేనట్లయితే తల కొట్టవచ్చును. తల కొట్టిన తర్వాత కణుపుల
వద్దవచ్చు పిలకలు తీసివేయవలెను.
8. ఆకు రెలుచుట:
 అడుగు ఆకులు నాటిన 65-75 రోజుల తర్వాత
రెలుచుటకు తయారగును. తర్వాత వారము పదిరోజుల
కొకసారి క్రిందనుండి తఫాకు 2 ఆకులు చొప్పున ఆకుల పక్వాన్ని
బట్టి రెలచాలి. మొత్తము 9,10 రెలుపులు అవుతుంది.
తర్వాత ఆకు కర్రలకు కట్టి సుమారు మామూలు బ్యారనుకు
700 కర్ర చొ//న ఎక్కించాలి. ఒక బ్యారను ఆకు క్యూరు
చేయుటకు 6,7 రోజులు పడుతుంది.
9. గ్రేడింగ్్:
 టొబాకో బోర్డువారు నిర్ణయించిన ప్రకారము 8
ఫారము గ్రేడులో చేయవలయును. గ్రేడింగు చాలా
జాగ్రత్తగాను, మెళుకువగాను చేసినట్లయితే మంచి ధర
వస్తుంది.
10. పంట మార్పిడి:
 నాణ్యతగల పొగాకు దిగుబడికి పంటమార్పిడి చాలా
24-
ముఖ్యము. ఈ దిగువ జెప్పిన పంట మార్పిడి పద్ధతులను
అనుకూలతను బట్టి అవలంబించవచ్చును.
 సస్యరక్షణ
 కీటకములు
1. పొగాకు లద్దెపురుగు:
 ఇవి ఆకులను తినివేసి విపరీత నష్టం కలుగజేయును.
ఈ పురుగులను చేతితో తీసివేసి నివారించుకోవచ్చును. ఆ
విధంగా నివారణ కాని పక్షములో చేతితో పురుగులను తీసి
వేసిన తర్వాత యీ క్రింద చెప్పిన మందులలో ఏవైనా వాడ
వచ్చును. పురుగు ఆశించిన ప్రదేశములో మాత్రమే మందు
కొట్టవలయును.
25-
 1. ఎండోసల్ఫాన్్ 35 శాతం ఇ.సి. 30 మి.లి.
22లీ. నీటిలో కలపి చల్లాలి.
 2. కార్బరిల్్ 50 శాతం డబ్ల్యు.పి. 50 గ్రా. నీటిలో
కలపి చల్లాలి.
 3. మోనోక్రోటోఫాస్్ 40 శాతం ఇ.సి. 22 మి.లీ.
నీటిలో కలపి చల్లాలి.
 4. క్లోరోవైరిఫాస్్ 20 శాతం ఇ.సి. 54 మి.లీ. నీటిలో
కలపి చల్లాలి.
2. నేలపెంకి పురుగు:
 క్రొత్తగా నాటిన పొగాకు తోటలలోని మొక్కల
కాండమును మొదలు కొరికి తినివేయుటవలన మొక్క చనిపోవును.
దీని నివారణకు ఎండోసల్ఫాన్్ 35 శాతం ఇ.సి. ఎకరమునకు
600 మి.లీ. గాని, క్లోరోపైరిఫాస్్ 20 ఇ.సి. ఎకరమునకు 700
మి.లీ. గాని 450 లీటర్ల నీటిలో కలిపి మొక్క మొదట్లో
పోయవలెను. లేక గానుగపిండిగాని, వేపపిండిగాని బాగా
నలగగొట్టి 1:5 నిష్పత్తి ప్రకారం ఇసుకలో కలిపి నాటిన
వెంటనే మొక్క మొదట్లో వేయవలెను. ఈ విధంగా
వేయుటకు ఎకరమునకు 30 కేజీల పిండి అవసరమగును.
3. తెల్ల ఈగలు:
 ఈ యీగలవలన ముడతతెగులు రోగము మొక్కలకు
వ్యాప్తిచేయబడును. ముడత వచ్చిన ఆకులు తీసివేసి కాల్చి
వేయవలెను.
26-
 ఈ యీగల నివారణకు డెమిటాన్్-ఎన్్-మిథైల్్ 25
శాతం ఇ.సి. అను మందు 30 మి.లీ. 22 లీ. నీటిలో కలిపి
8-10 రోజుల కొకసారి చొ//న 4 సార్లు పిచికారి చేయవలెను.
4. తేనెమందు (పెనుబంక):
 ఆకు అడుగుభాగమున కుప్ప తెప్పలుగా జేరి రసమును
పీల్చివేయును. దానినరికట్టుటకు యీ క్రింది మందులలో
ఏదైనా వాడవచ్చును.
 1) థయోమిటాన్్ 25 శాతం ఇ.సి. ఎకరమునకు
200 మి//లీ// చొప్పున
 2) డెమిటాన్్, ఎస్్. మిథైల్్ 25 శాతం ఇ.సి.
ఎకరమునకు 200 మి// లీ// చొప్పున
 3) డెమిథోయేట్్ 30 శాతం ఇ.సి. ఎకరమునకు 125
మి// లీ// చొప్పున
 4) ఫాస్ఫోమిడాన్్ 100 ఎకరమునకు 45 మి//లీ//
చొప్పున
5. పొగాకు పైరుపై క్రిమిసంహారక మందులు వాడుటలో
జాగ్రత్తలు:
 సాధ్యమైనంతవరకు పొగాకు పైరుకి పురుగు
నివారణకు మందులు వాడకుండా, గొంగళి పురుగు, లద్దె పురుగు
వగైరాలని ఏరివేయటముమంచిది. తప్పనిసరైనచో వర్జీనియా
పొగాకు పైరుకు మందులు వాడాలి. పొడిరూపములోనున్న
మందులు వాడకూడదు. మందుచల్లుట, సిఫార్సు చేయబడిన
27-
మందు సరియైన మోతాదులో పురుగు, ఆశించిన ప్రదేశములో
కొట్టవలెను, సిస్టమిక్్ మందులను మొవ్వులోని 4-5 ఆకుల
మీద మాత్రమే చల్లవలయును. మందు పిచికారి చేసిన
వెంటనే ఆకులు రాల్చరాదు.
 ఈ దిగువ పొగాకుపై వాడదగిన, వాడకూడని
మందులు ఇవ్వబడియున్నవి.
వాడదగిన మందులు వాడకూడని మందులు
1) ఎండోసల్ఫాన్్ ఎండ్రిన్్
2) కార్బరిల్్ బి.హెచ్్.సి. లేక లిండేన్్
3) మొనోక్రోటోఫాస్్ టాక్సాఫీన్్
4) డెమిటాన్్ యస్్.మిథైల్్ ఆల్ట్రిన్్
5) థయోమిటాన్్ డ్రైల్డిన్్
6) డై మిథోయేట్్ డి.డి.టి
7) ఫాస్ఫామిడాన్్ హెప్టాక్లోర్్
8) గానుగపిండి క్లోరిడేన్్
9) క్లోరోపైరిఫాస్్ పెరాఫి యాన్్పొడి
10) ట్రైక్లోరోఫాస్్ పెరామార్్ పొడి
11) ఎసిపేట్్ ఎక్్టాక్స్్ పొడి
12) వేపగింజల పొడి
 తెగుళ్లు
1. నల్ల మచ్చ తెగులు
 వాతావరణము తడిగా వున్నప్పుడు ఫంగస్్ క్రిముల
28-
వల్ల ఈ తెగులు వచ్చును. దీనివల్ల వేరుకాండము నల్లబడి
పోవును. ఆకుమీద నీటిబుగ్గలవలె పెద్దమచ్చ లేర్పడును.
 ఈ తెగులుసోకిన మొక్కలను తీసి కాల్చి వేయవలెను.
మొక్క మొదట, కాండము వెంబడి బోర్డో మిశ్రమము
(5:5:50) ఎక్కువగాపోసి బాగుగా తడిపినచో ఈ తెగులు
వృద్ధిపొందకుండా అరికట్టవచ్చును.
2. ఆకుమచ్చ తెగుళ్లు:
 గోధమరంగు మచ్చలు ఆకుమీద, కాండము మీద
ఏర్పడును. వీని నివారణకు బోర్డో మిశ్రమము (2:2:50)
చల్లవలెను.
3. సీతాఫలపు తెగులు:
 దీనివల్ల ఆకులమీద కారుపచ్చని లేక ఆకుపచ్చని
భాగము లేర్పడును. ఇదియొక అంటువ్యాధి. వ్యాధి తగిలిన
మొక్కలను ఆరోగ్యవంతమైన మొక్కలు తాకకూడదు.
 సీతాఫలపు తెగులును తట్టుకొను
రకములను వాడవలయును.
4. ఆకు ముడత తెగులు:
 ఇదికూడా విషరోగమే. యీ తెగులు సోకిన మొక్క
ఆకులు ముడతలుపడి యీనెలు బైటపడును. తెగులు ఆశించిన
మొక్కలు పీకివేసి నాశనము చేయవలెను. తర్వాత డెమి
టాన్్-ఎస్్-మిథైల్్ 30 మి.లీ. 22 లీ. నీటిలో కలిపి
పిచికారి చేసిన యీ తెగులు వ్యాప్తిచేయు తెల్ల యీగలను
నివారించుకోవచ్చును.
29-
5. మల్లెబోడు:
 పరాన్నభుక్కు అయినటువంటి మల్లెబోడు పొగ
మొక్క వ్రేళ్ళపై గుంపులు గుంపులుగా బయలుదేరి పొగ
మొక్క యొక్క సారమును పీల్చివేయును. మల్లెబోడు
మొక్క మొదట్లో భూమిపైన కనబడినంతనే గింజ కట్టక
ముందే చేతితో తీసివేసి తగులబెట్టవలెను. లేదా భూమిలో
పాతిపెట్టవలయును.
6. వేరుకాయ:
 ఈ తెగులు సరికట్టుటకు పంటమార్పిడి చేయవలెను.
పంటమార్పిడి వేరుశనగగాని, నువ్వులుగాని వేయవలెను.
7. రంపపుపొట్టు, ధాన్యపువూక, బ్రికెట్ యింధనములుగా
వర్జీనియాపొగాకు క్యూరింగ్్:
 వర్జీనియాపొగాకు క్యూరింగ్్నకు బొగ్గుగానీ, పుల్లగానీ
వుపయోగిస్తున్నారు. పుల్ల వాడకమువల్ల మన అటవీ సంపద
క్షీణించి పోతున్నది. బొగ్గువాడకంలో కూడా అనేకఇబ్బందులు
ఎదురవుతున్నాయి. ఈ కారణము వలన చవుకగా లభ్యమగు
యితర యింధనముల గురించి కేంద్ర పొగాకు పరిశోధనాల
యమువారు రాజమండ్రిలోని వారి క్షేత్రములో పరిశోధించగా
ధాన్యం ఊకనుగాని, రంపపు పొట్టునుగాని బ్రికెట్లుగా నొక్కి
ఉపయోగించినట్లయితే లాభం పొందవచ్చునని తెలిసినది.
ధాన్యపు ఊకకు హై-ప్రెషర్్ బ్రికెటింగ్్ ప్రేస్సులో 10
నుంచి 11 శాతము తేమ వుండునట్లు ఊకను సరిజేసి
నొక్కగా 9.6 మి.మీ దళసరి 6.6 సెం.మీ. వ్యాసం 35.5
30-
సి.సి. ఘనపరిమాణంగల బ్రికెట్లుతయారవుతాయి. ఒక్కొక్క
ఊక బ్రికెట్్ బరువు 464గ్రా. ఊకఘనపరిమాణము 93.3
శాతం మాత్రమే వున్నవి. అనగాఊక ఆక్రమించు స్థలములో
పదకొండో వంతు స్థలములోనే ఆ వూక నుండి తయారుచేసిన
బ్రికెట్లు యిమడగలవు. ఇదేవిధంగా రంపపుపొట్టుతో గూడా
బ్రికెట్లు తయారు చేసుకోవచ్చును.
 ఉత్సాహవంతులైన యువకులు చిన్నతరహా
పరిశ్రమలను నెలకొల్పి ధాన్యపు ఊకతోను, రంపపు పొట్టుతోనూ,
ఇతర వ్యవసాయ సంబంధమైన వ్యర్థపదార్థములతోను
బ్రికెట్లు తయారు చేసినచో, యిటు తమ పరిశ్రమపై లాభము
పొందుటయేగాక అటు రైతులకు ఇతర వినియోగదార్లకు
చాలా లాభము కలుగును.
31-
 మిగతా వివరములకు కేంద్ర పొగాకు
పరిశోధనాలయము, రాజమండ్రి వారిని సంప్రదించవచ్చు.
 వర్జీనియా పొగాకు నాణ్యత ఉత్పత్తి పెరగాలంటే
రైతులు ముందుగా వారి భూములు, సాగునీటిని భూసార
పరీక్షా కేంద్రాల్లో పరీక్ష చేయించుకొని పొగాకు సాగుకు
అనువైనవని తెలిసిన భూములలోనే వేయాలి. చౌడునేలలు,
క్లోరైడ్ల్్ శాతము పరిమితికన్నా ఎక్కువగావున్న నేలల్లోనూ
సాగునీటితోనూ, వర్జీనియా పొగాకు సేధ్యముజేస్తే ఉత్పత్తి
తగ్గటమేకాక నాణ్యత తగ్గిపోయి, ధరరాక
నష్టపోవలసియుంటుంది. పై నుదహగించిన ప్యాకేజీ పద్ధతులు అచరించి
వర్జీనియా ఉత్పత్తి నాణ్యతను పెంచుకోవచ్చును. భూసార సాగు
నీటి పరీక్ష చేయించుకోవడానికి యితర శాస్త్రీయ సాగు
వివరములకు రాష్ట్ర వ్యవసాయశాఖ (పొగాకు) సిబ్బందిని,
రాజమండ్రి కేంద్ర పొగాకు పరిశోధనాలయము వారిని, పొగాకు
బోర్డువారి సహాయ సహకారములు పొందవచ్చును.
 నాటు పొగాకు సేద్యము
 ఉభయగోదావరి జిల్లాలలో తేలిక నేలలలోనూ,
గోదావరిలంకలోను నాటు, లంక పొగాకు రకాలు విస్తారంగా
పండిస్తున్నారు. ఈ జిల్లాలలో సుమారుగా 10 వేల హెక్టార్లనాటు,
లంక పొగాకు రకాలు పండిస్తున్నారు. నాటు, లంక పొగాకు
చుట్టలకు వుపయోగిస్తారు. కాడలతో కీళ్ళీలలో వేసుకొనే జరదా
కూడా తయారు చేస్తారు. ఈ జిల్లాలలో పండించు లంక, నాటు
32-
పొగాకు రకాలకు యితర జిల్లాలలో, ఇతర రాష్ట్రములలో
మంచి గిరాకీ కలదు. నాటు పొగాకు నీటి ఆధారము క్రింద
పండించాలి.
1. నేలలు:
 ఇసుక గరపనేలలు, గరపనేలలు గోదావరి లంకలు,
నాటు పొగాకు సాగునకు శ్రేష్ఠం. నేలలు వర్జీనియా పొగాకు
వేసే నేలల కంటె బలమైనవిగా ఉండాలి. నీటి ఆధారం
వుండాలి. భూములు నీరు త్వరగా యింకే స్వభావముకలవై
వుండాలి. నీరు నిలవ వుండకూడదు. పల్లపు భూములు చవిటి
నేలలో నాటు, లంక పొగాకులు పండించకూడదు.
2. దున్నుట:
 కలుపుమొక్కలు, వేరుకాయ నాశించే క్రిములను
నరికట్టుటకు ఎండా కాలములో బాగా లోతుగా దుక్కిదున్న
వలెను. తర్వాత కలుపు మొక్కల నివారణకు అపుడపుడూ
దున్నుచుండవలెను. పశువుల ఎరువులు వేసిన తరువాత
ఒకసారి దున్నవలెను. తర్వాత మొక్క నాటుటకు ముందు
మెత్తని దుక్కివచ్చేవరకూ బాగా దున్నవలెను.
3. రకములు:
 గోదావరిలంకల్లో వేయుటకు సి.టి.ఆర్్.ఐ. రాజమండ్రి
వారు డి. ఆర్్ -1 లంక విత్తనములను ఉత్పత్తిచేసి సరఫరా
చేస్తున్నారు. మిగిలిన తేలిక నేలలో ప్రాంతీయ విత్తనములే
ప్రచారములో నున్నవి.
33-
4. నాటుట:
 వర్జీనియా పొగాకువేసే సమయములోనే దీనిని
వేయాలి. నాటు పొగాకు నారుమళ్ళు వర్జీనియా పొగాకు
నారుమళ్ళువలె తయారుచేసుకొని, అక్టోబరు-నవంబరు నెలలో
3232 అంగుళముల దూరంలో నాటుకొనవచ్చును. మణేదలు
ఏమన్నా వున్నట్లయితే వెంటనే వేయవలెను.
5. ఎరువులు:
 నాటు పొగాకునకు నత్రజని ఎరువులు వర్జీనియా
పొగాకుకంటే ఎక్కువవేయవలెను. ఎకరమునకు 10 టన్నుల
పశువుల ఎరువు నాటుటకు, నెలరోజుల ముందు వేయాలి.
లేదా మే-జూన్్ మాసములలో గొర్రెపెంట కట్టించవచ్చును.
తర్వాత నాటు పొగాకునకు ఎరసమునకు 200 పౌన్ల
నత్రజని, 84 పౌన్ల భాస్వరము 84 పౌన్ల పొటాష్్
వేయవలెను. నత్రజని ఎరువునకు 200 పౌన్ల నత్రజని, 84 పౌన్ల
భాస్వరము, 84 పౌన్ల పొటాష్్ వేయవలెను. నత్రజని ఎరువు,
వేరుశనగపిండి, అమ్మోనియం సల్ఫేట్్ లేక కాల్షియం
అమ్మోనియం నైట్రేటు లేక యూరియా రూపములో వేయవచ్చును.
భాస్వరము-సూపర్్ ఫాస్ఫేటు రూపములోనూ, పొటాష్్-
ఆఫ్్ పొటాష్్ రూపములోనూ వేయవలెను. మొత్తము
భాస్వరము సగము నత్రజని, (కొంత వేరుశనగపిండి రూపంలో),
సగం పొటాష్్ ఆఖరు దుక్కీలో వేయాలి. మిగిలిన ఎరువులు
నాటిన 20 రోజులలో బోదెలప్రక్క వేయవలెను.
34-
6. అంతర కృషి:
 మొక్క బాగా నాటుకున్న తర్వాత గొర్రు
గుంటకలతో కలుపు మొక్కల నివారణకు భూమిని గుల్లపరచుటకు
దున్నవలెను.
7. నీరు కట్టుట:
 ఉదయపు వేళలలో మొక్క వాడినట్లు కనబడితే నీరు
కట్టవలెను. మొదటిసారి సుమారు 20 రోజులకు, తర్వాత
ప్రతి 15 రోజుల కొకసారి నీరు కట్టవలెను.
8. తలకొట్టుట:
 పూబంతి వేసిన వెంటనే తలకొట్టవలెను. తర్వాత
వచ్చు పిలకలను వెంట వెంటనే తీసివేస్తూ వుండవలెను.
9. తెగుళ్ళు, పురుగుల నివారణ:
 వర్జీనియా పొగాకు నాశించు పురుగులు, తెగుళ్ళు,
నాటు పొగాకునకు ఆశించును. వర్జీనియా పొగాకునకు
వాడు మందులు దీనికికూడా వాడవచ్చును.
10. పంటకోత:
 నాటిన తర్వాత సుమారు 3 నెలలకు పంట కోతకు
వచ్చును. పంట మొత్తము ఒకసారి కోతకు వచ్చును. పంట
పక్వానికి వచ్చిన తర్వాత ఆకులన్నీకోసి ఒక త్రాడుకు కట్టి
లంక పొగాకు అయితే తాటాకు షెడ్డులలోనూ, గరువు
పొగాకు అయితే ఆరుబయట వెదురుకర్రలు కట్టి తాళ్ళు
ఒకదానిక్రింద ఒకటికట్టి సుమారు 20 రోజులు ఆరగట్టవలెను.
35-
లేనిపక్షమున ఆకులు పక్వానికి వచ్చిన తర్వాత చెట్టుమొదలు
కంటా కోసి అరగట్టవచ్చును. 20 రోజుల తర్వాత ఆకులను
కోయవలెను. ఆకు బాగా క్యూరు అయిన తీసి మండెలు
కట్టవలెను. మండెలును మూడురోజుల కొకసారి చొప్పున
నాలుగుసార్లు తిరగగట్టి తర్వాత బుట్టలలోపెట్టి అమ్మకానికి
పెట్టవచ్చును.
 ఒక క్వింటాలు నాటు పొగాకు సుమారు 700 రూ//లు
ఖరీదు వుంటుంది. ఎకరమునకు సగటున 5 లేక 6 క్వింటాళ్ళు
దిగుబడి వస్తుంది. ప్రత్యేక శ్రద్ధ తీసుకొంటే ఇంకా ఎక్కువ
కూడా పండించవచ్చును.
 (కేంద్ర పొగాకు పరిశోధనాలయము, రాజమండ్రివారి
సహకారముతో)
 పొగనారుమళ్లు - మెళకువలు
 1) మెరక ప్రదేశం, మురుగుపోవు గరువు నేలలు
ఎంచుకోవాలి.
 2) ప్రతి సంవత్సరం నారుమళ్ళు - ఒక ప్రదేశంలో
పోయరాదు.
 3) నారుమడి తడుపుటకు మంచినీటి వసతి ఉండాలి.
 4) నారుమడికి ఎంచుకొన్న నేలపై ఎండుచెత్త లేక
వరి ఊక పరచి కాల్చాలి.
36-
5. తొలకరి నుండి నారువేయు భూమి బాగా దున్నాలి.
6. 1.22 మీ. వెడల్పు, 10 మీ. పొడవు నారుమళ్ళు
కట్టాలి. మడికి-మడికి మధ్య 50 సెం.మీ. వెడల్పు
కాలువ ఉండాలి.
7. సెంటు నారుమడికి 1/2 బండి పశువుల ఎరుపు/ చెఱకు
మడ్డి వేయాలి.
8. సెంటు నారుమడికి ఒక కిలో సూపర్్ ఫాస్ఫేట్్ 200
గ్రాముల అమ్మోనియం సల్ఫేట్్, 4 కి.గ్రా.
పొటాషియం సల్ఫేటు వేసి కలపాలి.
9. మెగ్నీషియం లోపం సరిచేయుటకు సెంటుకు అరకిలో
డోలమైట్్ సున్నపు రాతిపొడికూడా వేయాలి.
10. చీమలు, వానపాముల నివారణకు సెంటుకు 120
గ్రాముల ఎండోసల్ఫాన్్ 4 శాతం పొడి లేక 350 గ్రా.
గానుగపిండి వేయాలి.
11. సిఫార్సు చేయబడిన మంచిరకం విత్తనం సెంటుకు 12
గ్రా. రెండు కిలోల ఇసుకతో కలిపి పల్చగా
సమానంగా చల్లాలి.
12. పొగాకునారు ఆగస్టు 15 వరకు పోసుకొని అక్టోబరు
15 నుండి నవంబరు 15 వరకు నాటుకోవాలి. 6 నుండి 8
వారముల వయస్సు నారు నాటాలి.
13. విత్తనం నేలలో 1/4 సెం.మీ. లోతులో పడునట్లు చదును
చేసిన మళ్ళపై వరుసలో చల్లి మెత్తని పశువుల
ఎరువుతో పల్చగా కప్పాలి.
37-
14. విత్తిన తరువాత సిమెంటు తూమును నారుమడిమీద
2-3 పర్యాయములు దొర్లించాలి.
15. సెంటు నారుమడిమీద 20 కి.గ్రా. వర్జీనియా పొగాకు
కాడతో కప్పాలి. ఈ కప్పును విత్తనములు మొలచిన
3-4 వారములకు తీసివేయాలి.
16. మళ్ళు రోజ్్కేస్్తో ఉదయం-సాయంత్రం చల్లబాటు
వేళలో తడపాలి. నారు పెరిగినకొద్ది మడి తడుపుట
క్రమంగా తగ్గించాలి.
17. పొగాకు విత్తనం 5-12 రోజులలో మొలకెత్తుతుంది.
నారుమడిలో ప్రతి 5 చ.సెం.మీ. నకు ఒక మొక్క
ఉండాలి.
18. 3-4 వారముల తర్వాత నారు మడిలో వత్తును తగ్గించి
పల్చగా ఉన్నచోట నాటాలి.
19. నారుమళ్ళకు విత్తనం మొలకెత్తిన వారం నుండి
నాలుగు రోజు కొకసారి చ.మీ. నారుమడికి 18 గ్రా.
చొప్పున అమోనియం సల్ఫేటును రెండు
పర్యాయములు 56 గ్రా. చొ//న మూడు పర్యాయములు నీటిలో
కలిపి రోజ్్కేస్్తో చల్లాలి. తొందరగా ఎదుగుతున్న
మొక్క఼ల ఆకులు త్రుంచి పెరుగుదలను అరికట్టాలి.
20. నారు తీసినప్పుడల్లా తగినంత నత్రజని ఎరువువేయాలి
నారు తీయుటకు ముందు 2-3 రోజులు తడులు తగ్గిస్తే
నారు మొక్కలు చక్కగా చిగురుస్తాయి.
21. పై విధంగా పెంచిన పొగ నారుమడి నుండి
ఆరోగ్య
38-
మైన, ధృఢమైన నారును 3 నుండి 5 పర్యాయములు
7-10 రోజుల కొకసారి తీయవచ్చు.
22. నారుమడిలో మాగుడు తెగులు నివారణకు 0.5 శాతం
బోర్డో మిశ్రమం లేదా బ్లయిటాక్సు లేక డైరెన్్
యం - 45 వాడాలి.
23. సెంటు విస్తీర్ణంలో పోసిన నారు, ఒక యకరంలో
నాటుటకు సరిపోతుంది.
 వ్యవసాయ జాయింట్్ డైరెక్టర్్, ఏలూరు
 ఆంధ్రప్రదేశ్్ వర్జీనియా పొగాకు పండించటానికి
 ఉత్తమ యాజమాన్య పద్ధతులు
 వర్జీనియా పొగాకు ప్రస్తుతం ఆకు పరిమాణంతో బాటు
కమ్మదనం, నికొటీను పరిమాణం ఎక్కువ (2.5 నుంచి 3
39-
శాతం దాకా) ఉన్న ఆకు ప్రపంచ మార్కెట్టులో
కోరబడుతున్నది. మేలిసాగు పద్ధతులను ఆచరించుటద్వారా అట్టి
పొగాకు ఉత్పత్తి చేయవచ్చును. 1981-82లో రైతులు
ఆచరించదగు మేలిసాగు పద్ధతులను రూపొందించుటకు పొగాకు
కార్యకర్తల సమావేశం 1981 సంవత్సరం ఆగస్టున
గుంటూరులో జరిగింది. వ్యవసాయ మంత్రిత్వశాఖకు చెందిన
పొగాకు అభివృద్ధి డెరొక్టారేట్, పొగాకు బోర్డు, రాష్ట్ర
వ్యవసాయశాఖ అధికారులు, ఐ.సి.ఎ.ఆర్్. క్రింద పనిచేస్తున్న
కేంద్ర పొగాకు పరిశోధనాలయ శాస్త్రవేత్తలు, ఐ.ఎల్్.టి.డి.
పరిశోధన విభాగపు నిపుణలు ఈసమావేశంలో పాల్గ఼ొన్నారు.
 వర్జీనియా పొగాకు పండించుటకు నల్లనేలలు, తేలిక
నేలలతో రైతులు ఆచరించదగు యాజమాన్య పద్ధతుల
వివరాలు దిగువ యివ్వబడ్డాయి.
 నల్లరేగడి నేలలో ఆచరించదగు పనులు
 పల్లపు భూములు, క్షారనేలలు, వరి పండించిన
భూములలో పొగాకు వేయరాదు. నేలలోని క్లోరైడు 0.01 శాతం
(100 సి.సి. ఎమ్్) లోపు ఉండాలి.
 వేసవిలో ట్రాక్టరు సాయంతో యేటేట లోతు దుక్కి
చేయుట లేక రెండేండ్ల కొక పర్యాయం గడ్డపారతో బెల్లు
విరుచుటద్వారా నేల గుల్లబారి మొక్క వేరు పోసుకొనును.
వాడదగు రకాలు:
 సి.టి.ఆర్్.ఐ. స్పెషల్్, జయశ్రీ, కనకప్రభ, ధనదాయి,
ఎఫ్్.సి.వి. స్పెషల్్, సి.టి.ఆర్్.ఐ స్పెషల్్ (ఎమ్్.ఆర్్)
40-
గోదావరి స్పెషల్్వంటి సిఫారసు చేసిన వంగడాలను
వేసుకోడం మంచిది.
మొక్కల మధ్య ఎడ఼ము:
 హెక్టారుకు 16000 మొక్కలకు బదులు 2000
మొక్కలుండునట్లు 70 సెం.మీ. 70 సెం.మీ. చొప్పున ఎడమనుంచి
నాటుటవల్ల అధిక దిగుబడి సాధించవచ్చును.
ఎరువుల వాడకం:
 హెక్టారుకు 300 నుంచి 35 కిలోల నత్రజని (150
నుంచి 175 కిలోల అమ్మోనియం సల్ఫేటు), భాస్వరం,
పొటాష్్ ఎరువులు ఒక్కొక్కటి 30 నుంచి 50 కిలోలు
ప్రారంభం ఎరువుగా వాడాలి.
తలత్రుంచుట:
 తలత్రుంచుటవల్ల ఆకు పరిమాణంతోబాటు నికొటీను
అను వస్తువు కూడా అధికమగుచున్నది. రాజమండ్రి కేంద్ర
పొగాకు పరిశోధనాలయ కాతేరు ఫారంలో 1980-81 సీజనులో
తలత్రుంచుటపై జరిగిన పరిశోధనవల్ల మొక్కకు పూబంతి
వేయగనె తలత్రుంచి పిలకలు తీయుటవల్ల మంచి ఫలిత మగు
పడినది. కావున సి.టి.ఆర్.ఐ. స్పెషల్్, జయశ్రీ, ఎప్్.సి.వి.
స్పెషల్్ వంటి వంగడాల విషయంలో ఇలా చేయుటవలన
మంచిది. కనకప్రభ విషయంలో ఎక్కువభాగము (75శాతం)
పువ్వులు వికసించినవె తలత్రుంచవలెను. కారెక్కియున్న
మొక్కలకు తలత్రుంచరాదు.
 -*-*-*-*-*-*-*-
      0 
</p></body></text></cesDoc>