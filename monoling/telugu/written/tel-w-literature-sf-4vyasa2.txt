<cesDoc id="tel-w-literature-sf-4vyasa2" lang="tel">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tel-w-literature-sf-4vyasa2.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-13</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>సైన్సు వ్యాసాలు</h.title>
<h.author>****</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">4vyasa2</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 21.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-13</date></creation>
<langUsage>Telugu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p> నిప్పుతోపాటు నీటినీ, గాలినీ మానవుడు ఉపయోగపరుచుకున్నాడు. నీరు
ప్రాణావసరాలలో ఒకటి. ఒక దశలో మానవసమాజం పెంపొందటానికి నదులు
చాలా తోడ్పడ్డాయి. వేదకాలపు ఆర్యులు నదులను స్తవంచేసి, వాటిపై తమ
జీవితం, తమ యజ్ఞం- అంటే జీవిత శ్రమ- ఎంతగా ఆధారపడి వున్నదీ తెలియ
జెప్పుకున్నారు. ఆదికాలపు నాగరికత నదీ తీరాల వెంబడే అభివృద్ధి అయింది.
కోస్తా వ్యాపారమూ, సముద్ర వర్తకమూ సాగినాకనే సముద్రతీరాల వెంబడి
నగరాలు ఏర్పడ్డాయి.
 నదులు జలసమృద్ధి కలిగించటమేగాక, రాను రాను ప్రయాణాలకూ,
వ్యవసాయానికీ కూడా తోడ్పడ్డాయి. కొన్నిచోట్ల నదీ ప్రవహాలు మిల్లులను
#తిప్పటానికి
వుపయోగించారు. గాలిని, శక్తిని కూడా ఇదేవిధంగా మిల్లులు తిప్పటానికీ,
#పడవలను
నడపటానికీ వినియోగించారు. స్టీమర్లు వచ్చినదాకా పడవలన్నిటికీ తెరచాపలుండేవి.
ఈనాడు కూడా చిన్నకాలువలలో పడవలు గాలి వీలుగా వున్నప్పుడు, తెరచాపల
సహాయంతో నడుస్తాయి.
 భూమినుంచి మానవుడు చాలా శక్తులను సాధించాడు. భూమి మానవుడికి
జీవితావసరమైన ఆహారాన్ని పంటలరూపంగా యిచ్చింది, ఓషధుల నిచ్చింది. ముడి
లోహాలనూ, వివిధ ఖనిజాలనూ ఇచ్చింది, అనేక రసాయనాలిచ్చింది. మట్టిని
ఉపయోగించినపుడు ఇళ్ళుకట్టే యిటుకలూ, కుండలూ మొదలైనవి తయారు
చేసుకున్నాడు.
 ఆధునిక మానవుడు రాక్షసిబొగ్గునూ, పెట్రోలియమునూ, విద్యుచ్ఛక్తినీ
అహర్నిశలు కాపాడే స్థితికి తిరిగిపోలేం.
 అదేవిధంగా పెట్రోలియం ఈనాటి సమాజానికి అత్యవసరమైన వస్తువు.
సుమారు ఒక శతాబ్దం క్రితం దాని పూర్తి ఉపయోగం ఎవరూ ఎరగరు. కొందరు
నీటికోసం బావులు తవ్వి అందులో పెట్రోలియం పడగా తమ శ్రమ
విఫలమయినందుకు విచారించరు. అటువంటి పెట్రోలియం యీనాడు లేకుండా పోయినట్లయితే
నాగరికత చాలా దెబ్బతింటుంది.
 అనేక పరిశ్రమలకు కీలకాలయిన రసాయనాలు ఇటువంటి సామాజిక
ధర్మాన్ని సంపాదించుకున్నాయి. అయితే పెట్టుబడిదారీ (ధనస్వామ్య) వ్యవస్థలో
గుత్తదార్లు ఇటువంటి శక్తులను తమ స్వతంగా అమలుజరపగలుగుతున్నారు.
ఎన్నో దేశాలలో, ఎంతో కాలంగా ఎన్నో వందలమందీ, వేలమందీ జరిపిన
పరిశోధనల ఫలితంగా సిద్ధించిన ఈ సామాజిక శక్తులను డబ్బుతో కొని ఈ
#గుత్తుదార్లు
42-
సంఘశ్రేయస్సుకు అపకారంకూడా చేయగల స్థితిలో ఉంటున్నారు. వీరిమధ్య జరిగిన
గొప్ప సంఘర్షణలే రెండు ప్రపంచ యుద్ధాలూ, అసంఖ్యాక చిల్లర యుద్ధాలూను.
 7. నల్ల బంగారం
 ఈ కాలపు నాగరికతకు పునాదిగా ఉంటూ, మనిషికి అపారమైన శక్తిని
అందజేసేవి బొగ్గూ, నూనె. ఈ రెంటిలోనూ నూనెకన్నా కూడా బొగ్గు
ప్రధానమైనది. ఎందుకంటే, నూనెను బొగ్గునుంచి కృత్రిమంగా తయారుచేయవచ్చు.
ప్రపంచంలో ఉన్న బొగ్గు అయిపోయిందంటే, దాని స్థానం ఆక్రమించ గలది
మరేదీ లేదు.
 సుమారు రెండు శతాబ్దాలుగా మానవుడు బొగ్గును శక్తి సాధనంగా
ఉపయోగిస్తున్నాడు. రెండు శతాబ్దాలకు పూర్వంఉండిన ప్రపంచానికీ, యీనాటి
#ప్రపంచానికీ
గల తేడా చూసినట్లయితే, మానవనాగరికత అభివృద్ధికి బొగ్గు ఎంతగా
తోడ్పడిందీ మనకు తెలుస్తుంది. బొగ్గు బాయిలర్లలో మండి నీటిని ఆవిరిగా
#మార్చి, దాని
వల్ల యంత్రాలను తిప్పుతోంది. ఈ యంత్రాల ద్వారా ఫ్యాక్టరీలూ, రైళ్ళూ,
#స్టీమర్లూ
నడుస్తున్నాయి. విద్యుచ్ఛక్తి పుడుతోంది. బొగ్గు మూలంగా ఇనుము, ఉక్కు,
#యంత్ర
నిర్మాణ పరిశ్రమలు అపారంగా అభివృద్ధి అయాయి. బొగ్గువల్ల ఇతర పారిశ్రామిక
ప్రయోజనాలు కూడా లేకపోలేదు. దానిలోనుంచి రంగులూ, సెంట్లూ, మందులూ,
పేలుడు పదార్థాలూ, ఎరువులూ, అనేక రసాయనాలూ తీస్తారు.
 బొగ్గుయొక్క ప్రాముఖ్యాన్ని మనం ఇంకొక విధంగా కూడా
గుర్తించవచ్చు. బొగ్గు ఈనాటి దేశీయ సంపదలో అత్యంత ప్రధానమయింది. బొగ్గు విలువ
తెలియని కాలంలో ప్రపంచంలో గొప్ప దేశాలుగా ఉండిన ఇటలీ, స్పెయిన్ దేశాలు
ఈనాడు వెనుకబడి ఉన్నాయి. చరిత్ర వాటిని వెనక్కు నెట్టేసింది. బొగ్గు
#గనులు
విరివిగా ఉన్న బ్రిటన్, అమెరికా, జర్మనీ, ఫ్రాన్స్్ మొదలైన దేశాలు
#అగ్రదేశాలయాయి. చైనాలో కూడా బొగ్గు అపారంగా ఉన్నప్పటికీ, రాజకీయంగా,
#పారిశ్రామికంగా
43-
వెనుకబడి ఉండడంవల్ల చైనా ఆ బొగ్గును ఉపయోగించుకోలేక
పోయింది. ఇప్పుడు చైనాకు అటువంటి అవాంతరాలు లేవు.
 బొగ్గుతాలూకు శక్తిని మానవుడు లాభసాటిగా వినియోగపరుచుకోవటం
ఇటీవలనే జరిగినప్పటికీ, బొగ్గు భూమిలో కోట్ల కొద్దీ సంవత్సరాలుగా ఉంది.
#అది
భూగర్భంలో తయారయే నాటికి భూమిమీద మానవజాతే లేదు. అనేక కోట్ల ఏళ్ళ క్రితం
భూమిమీద పెరిగిన ఒకరకం చెట్లు శిధిలమై మట్టికింద అణచబడి, కాలక్రమాన
బొగ్గుగా మారాయి. ఇట్లా ఒకసారి కాదు, అనేకమార్లు జరిగింది. అందుచేతనే
#బొగ్గు
గనులలో మట్టిపొరలూ, బొగ్గుపొరలూ ఒకదానిమీద ఒకటి కనిపిస్తాయి.
#మట్టి
పొరలలో పురాతన వృక్షాల వేళ్ళ ఫాసిల్స్్ కనిపిస్తాయి. బొగ్గు తయారుకావటం
#శతాబ్దాల
తరబడి జరిగింది. మూడడుగుల బొగ్గుపొర తయారుకావటానికి వెయ్యి
#సంవత్సరాలు
పట్టిఉంటుందని శాస్త్రజ్ఞులు అంచనా వేస్తున్నారు. వేల్స్్ దక్షిణ
#ప్రాంతంలోని బొగ్గు
గనులలో పొరలు ఏర్పడటానికి సుమారు ఆరున్నర లక్షల ఏళ్ళు పట్టిందట!
 బొగ్గులో ముఖ్యమైన పదార్థం కార్బన్ లేక కర్బనం. కర్బనం
ప్రాణవాయువుతో చేరి బొగ్గుపులుసు గాలి తయారవుతుంది. చెట్లు గాలిలోని బొగ్గుపులుసు
#గాలిని
పీల్చుకుంటాయి. చెట్లతాలూకు ఆకుపచ్చ పదార్థం (క్లోరోఫిల్) సూర్యరశ్మి
సహాయంతో ఈ బొగ్గు పదార్థాన్నిచెట్టుకు కావలసిన చక్కెర పదార్థంగా
#మారుస్తుంది.
అంటే ఎన్నో కోట్ల ఏళ్ళ క్రితం భూమిమీద ఉండిన వింత రకం చెట్లు
#గాలినుంచి
పోగుచేసిన కర్బనాన్఩ీ, సూర్యరశ్మినీ, మనం ఈనాడు శక్తి సాధనాలుగా
వినియోగించి మన నాగరికతను పెంపొందించుకుంటున్నా మన్న మాట.
 బొగ్గును పరిశ్రమలను ప్రధాన సాధనంగా ఉపయోగించటం, దానికోసం
అన్వేషించటం, లోతైన గనులనుంచి దానిని త్రవ్వటం, రాజకీయ చదరంగంలో
దానిని ఒక ముఖ్యమైన పావుగా వినియోగించటం, ఆధునిక చరిత్ర అయినప్పటికీ
మన పూర్వీకులు బొగ్గును ఎరక్కపోలేదు. రెండువేల ఏళ్ళ క్రితం గ్రీస్
#దేశస్తులు
నేలబొగ్గును వంటచెరుకుగా వాడారు. తరవాత రోమన్్లు కూడా దానిని వాడారు.
1271 లో చైనా వెళ్ళిన మార్కోపోలో అనే వెవ్నిస్్యాత్రీకుడు, అక్కడివాళ్ళు
ఒక నల్లని రాయిని కాలుస్తారని రాశాడు.
 బొగ్గుయొక్క పూర్తి విలువ తెలిసినాక కూడా మిగిలిన దేశాలకన్న
#బ్రిటన్
ముందు పైకి రావటానికి కారణ మేమంటే, బ్రిటన్్లో కొద్ది ప్రదేశంలో బొగ్గు,
ముడిఇనుము, ముడిసున్నమూ లభ్యమవుతుంది. ఇతర జాతీయ సంపదలకూ,
బొగ్గుకూ ఉండే తేడా ఏమంటే బొగ్గు చాలా భారమైనది. దాన్ని సులువుగా
#ఒకచోటు
44-
నుంచి మరొక చోటికి చేరవేయటం సాధ్యంకాదు. అందుకనే బొగ్గులేని దేశాలు ఆ
లోటును సులువుగా భర్తీ చేసుకోలేవు.
 బొగ్గు రవాణా సుకరంకాదు గనకనే, చాలదేశాలలో ఇతర పరిశ్రమలు
బొగ్గుగనులకు దగ్గిరలో ఏర్పాటుచేస్తారు. బ్రిటన్్లో బొగ్గుగనులు సముద్రతీరాన
ఉండటం, బ్రిటిష్ సామ్రాజ్య విస్తరణకు ప్రోత్సాహాన్నిచ్చింది. వలస
#దేశాలనుండి
స్టీమర్లలో చేరిన ముడిసరుకు రేవులకు సమీపానే ఏర్పాటయిన ఫ్యాక్టరీలలో తయారు
సరుకుగా చేయబడి తిరిగి ఓడ ఎక్కేది.
 నేలబొగ్గులో చాలా రకాలున్నాయి. పూర్తిగా తయారుకాని బొగ్గు గోధుమ
రంగులో ఉంటుంది. దీన్ని లిగ్నైట్ అంటారు. దక్షిణదేశంలో ఇటీవల లిగ్నైట్
గనులు పని ప్రారంభించాయి. ఇది బొగ్గులలోకల్లా హీనమైనరకం. ఇందులో
కర్బనంపాలు మరీ తక్కువగా ఉన్నప్పుడు చూడటానికి కొయ్యలాగే ఉంటుంది.
కొయ్యను మనం వంటచెరుకుగా ఉపయోగించుకుంటాం. కాని కొయ్య బొగ్గుస్థానం
ఎన్నటికీ ఆక్రమించలేదు. వంటచెరుకుమీదనే నడవాలంటే ఈనాటి పరిశ్రమలు
నడవనే నడవవు. మాటవరసకి బ్రిటన్్లో పరిశ్రమలకు బొగ్గు బదులు, కర్ర
వాడనిస్తే బ్రిటన్్నిండా చెట్లున్నాయనుకున్నా, ఒక్క సంవత్సరం లోపల అవన్నీ
ఖర్చయిపోతాయి.
 మేలురకం బొగ్గును `ఆంత్రసైట్' అంటారు. అందులోకి నూటికి 90
మొదలు 95 పాళ్ళదాకా కర్బనం ఉంటుంది. ఈ బొగ్గు చాలా గట్టిగా నిగనిగలాడుతూ
ఉంటుంది. దీన్ని, పట్టుకుంటే చెయ్యి మసికాదు. అయితే ఇది ఒకంతట
అంటుకోదు. దీన్ని బట్టీలలో గాలిధాటితో కలిపి మండిస్తారు. అప్పుడది చాలా
#వేడియిస్తూ
పొగరాకుండా మండుతుంది.
 మామూలు బొగ్గు-రాక్షసిబొగ్గులో - నూటికి 85-90 పాళ్ళు కర్బనం
#ఉంటుంది.
ఇది అన్నివిధాలా ఉపయుక్తమైనది. ఇతర మండేపదార్థాలతో పోలిస్తే చొక.
#దీన్ని
ప్రకృతి పరిమితంగానే ప్రసాదించింది. ఇప్పటికి రెండు శతాబ్దాలుగా, మనిషి
#దీన్ని
ఏటా అనేక లక్షల టన్నుల లెక్కన తవ్వి తీసి వాడుతున్నారు. ఈ లోపల ఇంకే
పదార్థమూ దీనిస్థానం ఆక్రమించకపోతే, మరో ఐదు శతాబ్దాల అనంతరం
ప్రపంచంలో ఉన్న బొగ్గుగనులన్నీ పట్టిపోవచ్చు. (ఈ విధంగా పట్టిపోయిన గనులు
బ్రిటన్్లో ఈనాడున్నాయి) ఏ పరిస్థితులలో ఈ బొగ్గుపొరలు నేలలో
#ఏర్పడ్డాయో,
ఆ పరిస్థితులు మళ్ళీ తిరిగి వచ్చేదుల్దప. వాటిని కృత్రిమంగా కల్పించటం
#కూడా
సాధ్యంకాదు.
45-
 ఈనాడు బొగ్గు ప్రత్యామ్నాయాల అన్వేషణ జరుగుతున్నది. స్టీమర్లకూ,
రైళ్ళకూ, కార్లకూ తదితర రవాణా వాహకాలకూ తైలాలు చాలాచోట్ల
ఉపయోగిస్తున్నారు. అయినా బొగ్గే మనిషికి ప్రధానబలంగా ఉంటున్నది. హిట్లరు, జర్మనీలోని
బొగ్గుగనులేకాక, తాను ఆక్రమించిన ఫ్రాన్స్్, పోలెండ్్, జెకోస్పవేకియాలలోని
బొగ్గుగనుల సహాయంతో తన ప్రతాపం వెలిగించాడు. చివరకు అతన్ని
పడగొట్టటానికి అమెరికా బ్రిటన్్ల బొగ్గుబలం కొంతవరకు పనికి వచ్చింది. ఈనాడు
అమెరికా, బ్రిటన్్ను తలదన్నటానికి బాహ్యచిహ్నంగా అమెరికా బ్రిటన్్కన్న
మూడింతలు బొగ్గు వాడుతున్నది.
 బొగ్గు ఆధునిక పరిశ్రమలకు సహాయపడటమేగాక, అనేక కొత్త నిర్మాణాలకు
కూడా దారితీసింది. ఆవిరియంత్రం మొదట తయారయినది బొగ్గుగనులు
తవ్వేటప్పుడు పడే నీటిని తోడివెయ్యటానికి, రైలుపట్టాలు మొదట వేసినది బొగ్గును
తొట్టెలలో తవ్విన చోటినుంచి చేరవెయ్యటానికి, ఆవిరి యంత్రమూ, పట్టాలు కలిసి
మనకు రైళ్ళు వచ్చాయి. ముడి ఇనుమును బొగ్గుతో కలిపి కాల్చటం ఆరంభమయే
దాకా ఆధునిక ఇనుము. ఉక్కు పరిశ్రమ లేనేలేదు. రెండు శతాబ్దాలకు పూర్వం
యీనాటి యంత్రాలన్నిటి పంపిణీ యొవరైనా కనిపెట్టి ఉండినప్పటికీ, వాటిని
తయారుచేయటానికి కావలసిన ఇనుము, ఉక్కు ప్రపంచంలోలేదు ఈ విధంగా
యంత్రనిర్మామ పరిశ్రమకు బొగ్గు దోహదంచేసింది.
 మానవజాతికి బొగ్గు చేసిన మేలుగురించి చెప్పుకునేటప్పుడు యిది ఊరికే
రాలేదనీ, దీనికోసం మానవ మారణహోమం చాలా జరిగిందనీ, యింకా
జరుగుతున్నదనీ తెలుసుకోవటం అవసరం. ఎందుకంటే గనులలో బొగ్గు తవ్వటం అత్యంత
ప్రమాదకరం. బ్రిటన్్లో సగటున రోజుకు ముగ్గురు బొగ్గుగనులలో ప్రమాదాల
మూలంగా చస్తూంటారు. ఇంకా యెన్నోరెట్లమంది తీవ్రంగా గాయపడుతుంటారు.
బొగ్గు గనులలోనుంచి ప్రాణహాని కలిగించే రెండురకాల వాయువులు వస్తాయి.
అందులో ఒకటి ఊపిరాడనీయక చంపేస్తుంది. రెండోది గాలితో కలిసి, మంట
సోకితే పేలుతుంది. లోతైన గనులలో వాయు ప్రసారమూ, విద్యుద్దీపాలూ యేర్పాటు
చేసి యీ ప్రమాదాలను తగ్గించవచ్చు.
 బొగ్గుగనిలో పనిచేసేవాళ్ళు బొగ్గు పొరకింద మట్టిపొరను తీసేసి బొగ్గు
పెళ్ళలు విరుస్తారు. ఒక్కోసారి పైనఉండే బొగ్గు విరిగి మీదపడుతుంది.
#పనివాళ్ళు
నలిగిపోతారు. ఇప్పటికింకా చాలాచోట్ల యంత్రపరికరాల సహాయం లేకుండానే బొగ్గు
తవ్వబడుతున్నది.
46-
 బొగ్గుపనివాడు వీరుడైన యోధుడితో సమానం. అతన్ని న్యాయంగా అట్లాగే
చూడాలి. ఒక్కొక్కసారి నాగరికతకు కృతజ్ఞత వుండక, తనను
పెంపొందించేటందుకు ఆత్మార్పణం చేసుకునేవాణ్ణి అంటరానివాణ్ణిగా చూస్తుంది.
 రష్యాలో బొగ్గుగనులలో పని చాలావరకు యాంత్రికం చేయబడినట్టూ,
బొగ్గు కార్మికుల క్షేమంకోసరం అనేకరకాల చర్యలు తీసుకోబడినట్టూ తెలుస్తుంది.
కార్మికులబలమే రష్యాను 40 ఏళ్ళకాలంలో వెనకబడిఉన్న జమీందారీ వ్యవస్థనుంచి
అమెరికాతో పోటీచేయగల బలవత్తరదేశంగా మార్చింది.
 మనదేశంలో బొగ్గు పరిమితంగా ఉంది. అందుచేత మనం జలవిద్యుత్తుల
వంటి ఇతర శక్తులపై హెచ్చుగా ఆధారపడాలి.
 8. రాతి చమురు
 పూర్వం బకూ నగరం పెద్ద యాత్రాస్థలం. ఎక్కడెక్కడి నుంచో
యాత్రికులు బకూనగరానికి వచ్చి, అక్కడ కనిపించే వింతైన మంటలకు దణ్ణాలు పెట్టుకుని
వెళ్ళేవారు. దేవతా మహిమచేత ఆ మంటలు కలిగేవని అప్పటివారి నమ్మకం. ఆ
మంటలకు నిజమైన కారణం బకూదగ్గరవున్న పెట్రోలియం నుంచి వెలువడే ఒక
వాయువు. ఇప్పుడు బకూ పుణ్యక్షేత్రం కాదుగాని, అక్కడ ప్రశస్తమైన పెట్రోలు
బావులున్నాయి.
 పెట్రోలియం అంటే రాతిచమురు అని అర్థం. బకూనగరాన్ని సందర్శించిన
మార్కోపోలో ఈ చమురును చూచాడు. "ఇది తినటానికి పనికిరాదుగాని చక్కగా
మండుతుంది" అని ఆయన రాసుకున్నాడు. ఒకప్పుడు ప్రజలు పూజించిన
పెట్రోలియం గ్యాసును బకూపౌరులు కాలక్రమాన వంటకు, దీపాలకు
ఉపయోగించుకున్నారు; వారు పెట్రోలియమును ఔషధంగాకూడా వాడేవారు.
 ఇంకా పూర్వం అలెగ్జాండర్ పర్షియా వెళ్ళినపుడు అక్కడ పౌరులు ఆయనకు
స్వాగతంగా పెట్రోలియంను వీధులలో చల్లి అంటించారు. అలెగ్జాండర్ ఇదిచూచి
చాలా ముగ్ధుడై ఆ నూనెను తన నౌకరుమీదచల్లి వాడికి నిప్పు అంటించాడు. వాడు
తగులబడి పోయినంతపని జరిగింది.
47-
 చైనావారు అనేకవేల సం// క్రితం పెట్రోలియం గాసును ఉపయోగించేవారు.
వారు దానిసహాయంతో సముద్రపు నీటినికాచి ఉప్పు తయారు చేసేవారు.
 నేలబొగ్గులాగే పెట్రోలియంకూడా, భూగర్భంలో మట్టిపొరల మధ్య చిక్కుకు
పోయిన పురాతన జీవుల కళేబరాలనుండి తయారయింది. అయితే బొగ్గును తయారు
జేసినది వృక్షజాతులు, పెట్రోలియమును తయారుజేసినది జంతుజాతులు. బకూలో
తీయబడే నూనె కేవలం "పిల్ల". అది సుమారు 2 1/2 లక్షల ఏళ్ళక్రితం మటుకే
తయారయింది. ఓల్గానదికి ఉరల్ పర్వతాలకు మధ్య ఇటీవల రష్యనులు కొత్త
నూనెగనులు కనిపెట్టి అభివృద్ధి జేశారు. దీనికి రెండవ బకూ అని పేరు
#పెట్టారు.
కాని వాస్తవానికి ఈ నూనె రమారమీ 40 కోట్ల ఏళ్ళ క్రితం భూమిలో
#తయారయింది.
"తాతకు మనవడి పేరుపెట్టారు:" అని ఒక సోవియట్ రచయిత చమత్కరించాడు.
 నూనెకు ఆధునిక నాగరికతలో ఎంత ప్రముఖస్థానమున్నదంటే సామ్రాజ్య
విస్తరణలో కొన్ని దేశాలు ఇతరదేశాల నూనెను గుత్తకు తీసుకోవటం, వాటికోసం
ఆయాదేశాల రాజకీయాలను హస్తగతం చేసుకోవటం ఆధునిక చరిత్రలో విరివిగా
కనిపిస్తుంది. మధ్య ప్రాచ్య రాజకీయాలు పెట్రోలియంతో రచించినవే. అంతర్జాతీయ
రంగంలో బ్రిటన్, అమెరికాల మధ్యగల పొత్తులో పెట్రోలియంపై సమిష్టి పెత్తనం
కూడా వుంది. రెండవ ప్రపంచయుద్ధం ముగియగానే ఆ రెండుదేశాలు నూనె
విషయంలో కలసి వ్యవహరించటానికి నిర్ణయించుకున్నాయి.
 పెట్రోలియంను ప్రముఖ పారిశ్రామికశక్తిగా వుపయోగించటం ప్రారంభించి
అర్ధ శతాబ్దానికి పైగా మాత్రమే అయింది. అంతకుపూర్వం ముడి పెట్రోలియం
నుంచి కిరసనాయిల్, లూబ్రికేటింగ్ తైలాలు తయారుచేసేవారు. పెట్రోలియం
వేలకొద్దీ గాలన్లు వచ్చేది. దానిని వ్యర్ధపదార్థంగా పరిగణించి ధ్వంసం
#చేసేవారు.
ఈ "వ్యర్థపదార్థం" కోసమే సామ్రాజ్యవాదులు ఇవాళ యుద్ధాలు చేయటానికిగూడా
సిద్ధంగా ఉన్నారు: 1848 లో ఒక అమెరికన్ పెన్సిల్ వేనియాలో ఉప్పునీటి
#బావుల
కోసం తవ్వాడు. ఆ రోజుల్లో ఉప్పుదొరకటం చాలా కష్టంగా ఉండేది. బావులనుంచి
ఉప్పునీరు తీసి దానినుంచి ఉప్పువేయాలని ఈ పెద్దమనిషి ఉద్దేశం. కాని
#దురదృష్టవశాత్తు ఇతను తవ్వినచోట పెట్రోలియం దొరికింది. వ్యాపార దక్షతగల ఆ పెద్ద
మనిషి విచారించక, పెట్రోలియంను సీసాలలో పెట్టి, "మందు" కింద అమ్మి
సొమ్ము చేసుకున్నాడు!....
 బొగ్గులా కాక పెట్రోలియం సులువుగా ఒకచోటినుంచి ఇంకొక చోటికి
చేరుతుంది. బొగ్గును సాధ్యమైనంతవరకు దొరికేచోటనే వినియోగించుతారు. కాని,
48-
నూనెకోసం కబంధహస్తాలు చాలా చాచుకొంటాయి! నూనెను గొట్టాల ద్వారా అనేక
వేలమైళ్ళు పంపుతారు.
 భూమిలో గూడా పెట్రోలియంకు చలన మున్నది. భూగర్భంలో
పెట్రోలియం చేరివుండే రాళ్ళపొరల మధ్యను ఒత్తిడి వచ్చినప్పుడు అది సంచారం
#ప్రారంభిస్తుంది. సాధారణంగా అది అతి కఠినమైన రాళ్ళ పొరలకింద చేరుతుంది. కాని
వీటిలో పగళ్ళు, సందులు, ఉన్నట్టయితే పెట్రోలియం వూట నుంచి మట్టి కూడా
నేలమీదకు రావటం జరుగుతుంది. ఈ విధంగా పైకి వచ్చిన పెట్రోలియం గాలి
వల్ల, సూర్యరశ్మివల్ల చిక్కబడి "బిట్యుమెన్" అవుతుంది. దీనినే రోడ్లకు
#వేస్తారు.
ట్రినిడాడ్ నగరంలో "బిట్యుమెన్" మేట లున్నాయి. దీనినే కీలు లేక తారు
అంటారు. కీలు చాలా ప్రాచీన గ్రంధాలలో కూడా చెప్పబడి వుంది. బేబిలాన్్లో
పూర్వం ఇళ్ళు కట్టడానికి కీలును సున్నం కింద ఉపయోగించేవారట. భారతంలో
చెప్పబడిన లెక్కయిల్లు యీవిధంగా కట్టినదేనేమో!
 ముడి పెట్రోలియంలో అనేక రసాయనిక ద్రవాలుచేరి వుంటాయి.
వీటన్నిటిలోనూ కర్బనము, హైడ్రొజ఼న్ వేరు వేరు పాళ్ళలో చేరివుంటాయి. దీనిని శుద్ధి
చేసి పెట్రోలియం మాత్రమేకాక యింకా అనేక యితర ద్రవాలు తీస్తారు. ముడి
పెట్రోలియంను కొద్దిగా వెచ్చబెట్టేసరికి దానినుంచి ఆక్టేన్, పెట్రోల్
#ద్రవాలు విడి
వడతాయి. పెట్రోలియంలో చేరివున్న ద్రవాలలో యివి చాలా తేలికయినవి. మరి
కొంత వేడెక్కిస్తే కిరసనాయిల్, మరింతకాస్తే లూబ్రికేటింగు నూనెలు
విడివడతాయి. ఇవన్నీ పోగా భారమైన చమురులు దిగబడిపోతాయి.
 ఈ భారమైన చమురు తాలూకు అణువులను పగులకొడతారు. ఇందుకు
ప్రధానంగా వేడీ, ఒత్తిడీ ఉపయోగిస్తారు. వీటివెంబడి నికెల్, కోబాల్ట్్
#లోహాలను
సహాయక సాధనాలుగా ఉపయోగిస్తారు. ఇటువంటి సహాయక సాధనాలు స్వయంగా
యేమీ కావు. కాని దగ్గర వుండి పనిజేస్తాయి. ఇటువంటి సాధనాలను రసాయన
శాస్త్రంలో ఉత్ప్రేరకా లంటారు. వీటి సహాయంతో భారీ చమురునుంచి కూడా
పెట్రోలు తయారవుతుంది. పెట్రోలుతోబాటు యింకా అనేక రకాల అమూల్యమైన
వస్తువులు, ప్లాస్టిక్ తాలూకు ముడిపదార్థాలూ, కృత్రిమ రబ్బరు తయారుచేయటానికి
పనికివచ్చేవీ, మందులూ, ఆపరేషన్లలో ఉపయోగించే మత్తుమందులూ, రంగులూ,
పేలుడు మందులూ, వెయ్యికిపైగా తయారవుతాయి.
 పెట్రోలును కృత్రిమంగా తయారు చేయవచ్చునని బొగ్గును గురించి
తెలుసుకొన్నప్పుడనుకొన్నాం. అతి సూక్ష్మమైన బొగ్గు నలుసులతో హైడ్రొజన్
49-
వాయువును చేర్చి బ్రహ్మాండమైన ఒత్తిడి కలిగించినట్లయితే పెట్రోలు
తయారవుతుంది. కాని ఇది సులువైన పద్ధతి కాదు. ఇంతకన్నా సులువైన
పద్ధతేమంటే.
 నేలబొగ్గునుంచి బొగ్గుగ్యాసు తయారు చేయటానికి దానిని ఇనుపతొట్లలో
వేసి కాస్తారు. ఆ విధంగా కాచిన మీదట అందులో "కోక్" అనే ఒకరకమైన
బొగ్గు మిగులుతుంది. దానిని బట్టీలోవేసి కాలిస్తే పొగలేకుండా మండుతుంది.
#ఇటువంటి "కోక్్" ను సాధ్యమైనంత హెచ్చువేడికి మండించి దాని గుండా
#నీటిఆవిరిని
పంపినట్లయితే హైడ్రొజన్ వాయువు, కార్బన్ మొనాక్సైడ్ అనే
ప్రాణహానికరమైన వాయువులు తయారవుతాయి. ఈ వాయువులకు మరింత హైడ్రొజన్ వాయువు
చేర్చి కోబాల్ట్, నికెల్ లోహాల మీదుగా పోనిస్తారె. అప్పుడు కృత్రిమ
#పెట్రోల్
తయారవుతుంది. ఈ విధంగా ఒక టన్ను బొగ్గు నుంచి అరవై గాలన్ల పెట్రోల్
తయారవుతుంది.
 పెట్రోలియం కోసం తవ్వడం చాలాశ్రమతో కూడుకున్నపని.
సాధారణంగా అది చాలా బలమైన రాతిప.రల కింద చేరి ఉంటుంది. దీనికి బెజ్జాలు
#తొలవటం అతి కఠినమైన పని. ఉదాహరణకు పైన చెప్పిన రెండవ బకూలో నూనె
ఉన్న వార్త తెలియగానే అమెరికన్, బెల్జియం, బ్రిటిష్, జర్మన్ నూనె
#కంపెనీల
వారు రష్యాకు తమ మనుష్యులను పంపారు. కజక్ వద్ద బ్రిటిష్్వారు "కజక్
ఆయిల్ ఫీల్డ్స్్ లిమిటెడ్" అనే సంస్థను నెలకొల్పారు. షాంఇర్ అనే
#అమెరికన్
తుయిమాజా జిల్లాలో ఒక నూనెబావి తవ్వటం కూడా సాగించాడు. కాని
#ఒక్కరికీ
అక్కడ నూనె దొరకలేదు. చివరకు 1929 లో సోవియట్ ప్రభుత్వం కింద ఇక్కడ
నూనె లభించింది. షాంఇర్ తవ్వివదిలిన చోటనే 1940 లో సోవియట్ శాస్త్ర
వేత్తలు లోతైన నూనెబావి తవ్వారు. కాని యుద్ధం మూలంగా నూనె అన్వేషణ
నిలిచిపోయింది. తుయిమాజా జిల్లాలో మొదటి నూనెబావి తవ్వడానికి తొమ్మిది
మాసాలు పట్టింది. ఆ రోజుల్లో రోజుకు 30 అడుగులు తవ్వితే అదే ఘనం. కాని
1947 లో ఒక్క నెలలో 2624 అడుగులు రాయి తొలిచారు.
 చెప్పవచ్చిందేమంటే గడచిన పదేళ్ళకాలంలో రష్యాలో నూనెబావులు
తవ్వటానికి కొత్త సాధనాలు కనిపెట్టి పని చాలా సులువు చేసుకొన్నారు.
#ఇందుకుగాను
50-
"టర్బోడ్రిల్" అనేది సృష్టించారు. ఈ విధంగా శక్తి సాధనాల వెంబడి అనేక
ఉపపరిశ్రమలు కొత్త యంత్రాల సృష్టి సాగుతోంది.
 9. జడపదార్థంలో ఘనశక్తి
 మానవుడు జడపదార్థాలలోని వివిధ శక్తులను తన అవసరాలకు
వినియోగించుకొన్నాడు. రాతి బరువూ గట్టితనమూ, కొన్ని రకాల శిలలపదనూ ఒకప్పుడు
మనిషికి బతుకు తెరువులుగా ఉపయోగపడ్డాయి. వెదురుబద్దలలోగల స్థితిస్థాపన
#శక్తి
బాణాలు వదలటానికి ఉపయోగపడింది. మండే వస్తువులలో అగ్నిశక్తి మానవుడికి
వంటసాధనంగా ఉపయోగపడటమేగాక లోహపరిశ్రమలకు తోడ్పడింది. చెకుముకి
రాయి నిప్పు పెట్టెగా ఉపయోగపడింది.
 తరువాత మానవుడు నదీ ప్రవాహాల విసురును శక్తి సాధనంగా
ఉపయోగించుకొన్నాడు. ఇప్పటికీ జలపాతాలు జలవిద్యుత్తును సృష్టిస్తున్నాయి. నీరు ఆవిరి
కావటంలో దాదాపు రెండువేలరెట్లు ప్రమాణానికి పెరుగుతుంది. అందుచేత నీటిని
బోయిలర్లలోపోసి కాస్తే ఆవిరి అపారమైన ఒత్తిడి కలిగిస్తుంది. ఇటువంటి
#ఒత్తిడితో
రైలు ఇంజన్లూ, ఫ్యాక్టరీలలో మరలూ, స్టీమర్లూ నడుస్తున్నాయి. (స్టీమ్ అంటే
ఇంగ్లీషులో నీటిఆవిరి).
 జడపదార్థంలో వాస్తవానికి అపారమైన శక్తి ఉన్నది. వివిధ పదార్థాలను
వివిధ స్థితులలో ఉంచితే శక్తి లభ్యమౌతుంది. రబ్బరును సాగదీసి
#విడిచినప్పుడది
కొంత శక్తిని విడుదలచేస్తుంది. ఈ శక్తిని కుర్రాళ్ళు పిట్టలను
#కొట్టడానికి
ఉపయోగించడం మనం చూస్తాం. స్ప్రింగును నొక్కి వదిలిపెడితే అందులోనుంచి శక్తి
విడుదల అవుతుంది.
 రసాయనికమైన మార్పులు కలిగినప్పుడుకూడా కొంతశక్తి విడుదలౌతుంది.
కట్టెలూ, బొగ్గులూ మొదలైనవి కాలినప్పుడు వాటిలో రసాయనికమైన మార్పులు
జరుగుతాయి. "బొగ్గుకాల్చి వేడి పుట్టించినప్పుడు బొగ్గులోని శక్తిలో
#శతసహస్రాంశం
51-
కూడా మనకు దక్కడంలేదు" అంటారు శాస్త్రజ్ఞులు. వారి ఉద్దేశం
యేమిటి? కాలినాకకూడా బొగ్గునుసీలో దానిశక్తి అంతా యింకా అట్టే ఉందన్నమాట!
ఏమిటీశక్తి? దీనిని గురించి తెలుసుకోటానికి మనం అణువులను గురించీ,
పరమాణువులను గురించీ కొంత తెలుసుకోవాలి.
 మనం ప్రపంచంలో రకరకాల పదార్థాలను చూస్తున్నాం. మనం చూసే
పదార్థాలలో చాలాభాగం మిశ్రమపదార్థాలు. ఉదాహరణకు మనం నీరు అనుకునే
దానిలో అచ్చంగా నీరే కాక అందులో అనేక లవణాలూ, వాయువులూ కరిగి
ఉంటాయి. ప్రపంచంలో ఉన్న "శుద్ధ" పదార్థాలను విడగొట్టి పరిశీలించ గలిగితే
అప్పటికీ అవి లక్షలసంఖ్యలో ఉంటాయి. ప్రతి శుద్ధ పదార్థంలోనూ దాని తాలూకు
అణువులు మాత్రమే ఉంటాయి. అంటే ప్రపంచంలో అనేక లక్షల రకాల
అణువులున్నట్టు స్పష్టమౌతున్నది.
 ఈ అణువులను పగులగొట్టినట్లయితే అవి పరమాణువుల కలయికలన్నది
స్పష్టమవుతుంది. ఐతే పరమాణువుల దగ్గరికి వెళ్ళేసరికి మనకు గోచరించే
చిత్రమేమిటంటే, ఎన్ని వివిధ పదార్థాలను పరీక్షించినాసరే మనకు సుమారు నూరు
#రకాల
పరమాణువులే కనిపిస్తాయి తప్ప అంతకంటె హెచ్చుగా కనబడవు. ఈ పరిమిత
సంఖ్యగల పరమాణువులు వేరువేరు విధాలుగా వేరువేరు పాళ్ళలో సంయోగంపొంది
అనేకరకాల అణువులూ, పదార్థాలూ ఏర్పడుతున్నాయన్న మాట.
 పిల్లలకు అణువులను గురించి చెప్పేటప్పుడూ, పరమాణువులను గురించి
చెప్పేటప్పుడూ వాటిని చిన్న గోలీగుండ్లలాగా చిత్రించి చెబుతారు. కాని అవి
వాస్తవానికి అట్లా వుండనేవుండవు. అణువులూ, పరమాణువులూ వాటి పరిమాణంలో
అవి చిన్న బ్రహ్మాండాల్లాంటివి. పరమాణువులో ఉండే విభాగాల ప్రమాణంతో
పోలిస్తే పరమాణువు ప్రమాణం చాలా పెద్దది. ఒక అణువులోగల పరమాణువులు
ఒకదానికొకటి చాలా దూరంగా వుంటాయి. పదార్థంలోగల అణువుల మధ్యగల
దూరం అణువుల ప్రమాణంతో పోలిస్తే బ్రహ్మాండమైనది. అందుచేతనే మనిషిలో
గల పదార్థంలో అణువులమధ్య, పరమాణువులమధ్యగల ఎడం ఏవిధంగానైనా తీసి
పారేయగలిగితే. అంతాకలిసి మైక్రోస్కోప్్లో కనపడీ కనపడనంతటి
నలుసవుతాడని శాస్త్రజ్ఞులు అంచనా వేశారు! కాని అంత నలుసూ మనిషి బరువూ ఉంటుంది!
 అయితే ఇట్లా అణువులమధ్యా, పరమాణువులమధ్యా గల ఎడం తీసెయ్యడం
మాటలుగాదు. దీన్ని మనం సులువుగా తెలుసుకోవచ్చు. ఉదాహరణకు, ఒకే పదార్థం
ఒకప్పుడు ఘనపదార్థంగా, మరొకప్పుడు ద్రవపదార్థంగా! ఇంకొకప్పుడు వాయు
52-
పదార్థంగా ఉంటుంది. (ఉదాహరణ: మంచు, నీరు, ఆవిరి): కాని మూడు
స్థితులలోనూ కలిగే మార్పు ఆ పదార్థం తాలూకు అణువులమధ్యగల దూరంలోనే. నీటిని
ఆవిరిచేయటానికి ఎంతో శక్తికావాలి. మంచుచెయ్యటానికి కూడా అదే విధంగా
ఎంతో శ్రమపడాలి. అతి ప్రయాసమీద గాలిని చల్లబెట్టి ద్రవంగా చెయ్యవచ్చు. 93
కోట్ల మైళ్ళదూరాన ఉంటేనే, మనం భరించలేకుండాఉన్న సూర్యుడిలో సమస్తమైన
లోహాలూ ఆవిరిరూపంలో ఉన్నాయి. కనుక, అణువుల మధ్యగల దూరాన్ని
తగ్గించటానికీ హెచ్చించటానికీ అపారమైనశక్తి కావాలి. ఆ దూరాన్ని అసలే
తీసెయ్యాలంటే ఎంత అసాధ్యమో మనం ఊహించుకోవచ్చు.
 ఇక పరమాణువులలో కలిగే మార్పుగురించి ఆలోచిద్దాం. అణువుకు ఒక
కేంద్రమూ, దానిచుట్టూ తిరుగుతూ కొన్ని ఎలెక్ట్రాన్్లూ ఉంటాయి.
#సూర్యుడిచుట్టూ
వేరు వేరు దూరాలలో గ్రహాలు పరిభ్రమిస్తున్నట్టే, ఎలెక్ట్రాన్్లు పరమాణు
#కేంద్రం
చుట్టూ, అతివేగంగా పరిభ్రమిస్తూ వుంటాయి. ఎలెక్ట్రాన్్లు నెగెటివ్ (ఋణ)
#శక్తి
కలిగివుంటాయి. కేంద్రం పోజిటివ్ (ధన) శక్తి కలిగివుంటుంది. పరమాణువు
తాలూకు బరువంతా దాదాపు యీ కేంద్రంలోనే వుంటుంది.
 ప్రపంచంలోని పరమాణువు లన్నిటిలోకీ చిన్నది హైడ్రోజెన్ పరమాణువు.
ఈ పరమాణువు కేంద్రంలో ఒక ప్రోటాన్, దీనిచుట్టూ తిరుగుతూ ఒక
#ఎలెక్ట్రానూ
మాత్రమే వుంటాయి. అయితే, అన్నిరకాల పరమాణువుల కేంద్రంలోనూ అచ్చగా
ప్రోటాన్లే ఉంటాయనుకోరాదు. కొన్నింటిలో ప్రోటాన్లతోబాటు, న్యూట్రాన్లనేవి
కూడా ఉంటాయి. న్యూట్రాన్లు గూడా ప్రోటాన్లలాటివేగాని, వాటికి ధనశక్తిలేదు.
ప్రోటాన్లకు మాత్రమే ధనశక్తి వుంది. హీలియం అనే వాయువు తాలూకు
పరమాణువు కేంద్రంలో రెండు ప్రోటాన్లతోబాటు రెండు న్యూట్రాన్లుకూడా ఉంటాయి.
అయితే, పరమాణువుతాలూకు ధన, ఋణశక్తులు సమంగా ఉంటాయిగనక, హీలియం
పరమాణువుతో పరిభ్రమించే ఎలెక్ట్రాన్్లు రెండే ఉంటాయి. దాని కేంద్రంలో
రెండు ప్రోటాన్లూ, రెండు న్యూట్రాన్లూ ఉండటంచేత, హైడ్రోజ఼ెన్ పరమాణువు
కంటే హీలియం పరమాణువు నాలుగు రెట్లు బరువుంటుంది. ఎలెక్ట్రాన్ బరువుతో
పోలిస్తే ప్రోటాన్, 1837 రెట్లు బరువుంటుంది. అందుచేత, ఎలెక్ట్రాన్్ల బరువు
ఆటే గణనలోకిరాదు.
 ప్రోటాన్్కూ-ఎలెక్ట్రాన్్కూ మధ్య ఉండేదూరం అపారమైనది. ప్రోటాన్్
#ఒక
బటానీగింజంత ఉంటుందని ఊహించుకుంటే, ఎలెక్ట్రాన్ దానికి 350 గజాలదూరాన
తిరుగుతూ ఉంటుందన్నమాట. ప్రోటాన్్చుట్టూ, ఎలెక్ట్రాన్ ఎంతవేగంగా
#తిరుగుతుందంటే,
53-
ఒక సెకండుకాలంలో అది లక్షకోట్లసార్లు తిరుగుతుంది. అందుచేత,
౟ీ ఎలెక్ట్రాన్, ప్రోటాన్్ను అనుక్షణం కవచంలాగా పొదిగివుంటూ, అంతటా
ఒకేసారి ఉండేదానినిగా ఊహించుకోవచ్చు.
 వివిధ పరమాణువులలో ప్రోటాన్్లూ, న్యూట్రాన్్లూ, ఎలెక్ట్రాన్్లూ
#వివిధ
సంఖ్యల్లో ఉంటాయి. కర్బనంలో 6 ప్రోటానులూ, 6 ఎలెక్ట్రాన్్లూ, 6
#న్యూట్రాన్్లూ
ఉంటాయి. అన్నిటికన్న బరువైన యురేనియం పరమాణువులో 92
#ఎలెక్ట్రాన్్లూ,
92 ప్రోటాన్్లూ, 146 న్యూట్రాన్్లూ వుంటాయి. ఎలెక్ట్రానులసంఖ్య ఎప్పుడూ
ప్రోటాన్్లసంఖ్యకు సమంగా ఉంటుంది.
 ఎలెక్ట్రాన్్ల సంఖ్య హెచ్చినకొద్దీ, పరమాణువు ప్రమాణం కూడా
పెరుగుతుంది. ఎందుకంటే, ఎలెక్ట్రాన్్లన్నీ కేంద్రానికి ఒకే దూరంలో ఉండక,
#వేరువేరు
దూరాలలో ఉంటాయి. కేంద్రానికి సమీపంలో ఉండే ఎలెక్ట్రాన్లకంటే,
#కేంద్రానికి
దూరంగా ఉండే ఎలెక్ట్రాన్లను సులువుగా లాగి వేయవచ్చు కొన్ని వస్తువులను
రుద్దితేచాలు. వెలుపలి ఎలెక్ట్రాన్లు విడబడతాయి. కొన్ని లవణాలను నీటిలో
#కరిగించినా
ఇదే జరుగుతుంది. ఎలెక్ట్రాన్్లను ఒక ప్రవాహంలో బయటికి లాగినట్లయితే,
విద్యుచ్ఛక్తి పుడుతుంది. కాని, కేంద్రానికి దగ్గిరిగావుండే
#ఎలెక్ట్రాన్్లను
విడగొట్టడమూ, పరమాణుకేంద్రాన్ని భేదించడమూ బహు దుస్తరం. ఈ పని చేయగలిగితే,
పరమాణుశక్తి ఉత్పత్తి అవుతుంది.
 పరమాణువు కేంద్రంలో ఈ శక్తి ఎక్కడుంది? అది ప్రోటాన్లనూ,
#ఎలెక్ట్రాన్లనూ కలిపిపెట్టే శక్తి. ఇది మానవుడికి తెలిసిన అన్నిశక్తులలోకీ
#ఉత్కృష్టమైనది.
మిగిలిన శక్తులు రసాయనికశక్తీ, విద్యుచ్ఛక్తి కూడా-దీని ముందు ఎక్కడా
#ఆగవు.
పరమాణుకేంద్రం భగ్నం కాగానే, అందులోని ప్రోటాన్లనూ, న్యూట్రాన్లనూ
కూడగట్టి ఉంచిన అపారశక్తి రేడియోషన్ రూపంలో బయటపడి
ప్రసారమవుతుంది.
 కొన్ని పదార్థాలలో పరమాణుకేంద్రం దానంతట అదే కొంతవరకు విచ్ఛిత్తి
పొంది, రేడియేషన్ కలుగుతుంది. దీనికి మంచి ఉదాహరణ రేడియం, అయితే,
రేడియంలో పరమాణు కేంద్రాలలో పరిణామం అతి నింపాదిగా జరుగుతుంది. ఒక
గింజ ఎత్తు రేడియం తీసుకున్నట్టయితే, అందులో సగభాగం విచ్ఛిత్తి గావటానికి
1620 ఏళ్ళు పడుతుంది. రేడియం పరమాణువులు తమకు తామేమారి కాలక్రమాన
సీసం తాలూకు పరమాణువులై, ఇక మారేశక్తిలేక, అట్లాగే నిలిచిపోతాయి.
54-
 ఈ విధంగా, సహజంగా రేడియేషన్ యిచ్చే పదార్థాలను
"రేడియోయాక్టివ్" పదార్థాలంటారు. రేడియం పూర్తిగా సీసం అయిపోయేలోపల అనేక
"రేడియో యాక్టివ్" పదార్థాలుగా మారుతుంది. కాని, ఈ మార్పులను
వేగిరించడానికిగాని, నింపాది చెయ్యడానికిగాని మార్గం ఏదీలేదు. రేడియంను కనిపెట్టి
#పరిశోధనలు చేసినది మదాం క్యూరీ అనే ఫ్రెంచి శాస్త్రజ్ఞురాలూ, ఆమె భర్త
#పియర్్క్యూరీ.
 సీసం కాకపూర్వం, రేడియం అన్ని దశలలోనూ రేడియేషన్ ఇస్తుంది. కాని,
ఒక దశలో యిచ్చినట్టు ఒక దశలో ఇవ్వదు. రేడియం మొదటి దశలో రాడాన్
అవుతుంది. ఈ దశలో దీని పరమాణువులు నాలుగు రోజులకు సగం చొప్పున
"శిథిలం" ఔతాయి. (నాలుగు రోజులలో సగం శిథిలమౌతాయంటే, మిగిలిన
సగమూ వారి నాలుగు రోజులలో అవుతాయనుకోరాదు. మిగిలిన సగంలో సగం
శిథిలం కావటానికి మరి నాలుగు రోజులు పడుతుంది.)
 రేడియం నుంచి రేడియేషన్ నింపాదిగా వస్తుందన్నంత మాత్రంచేత అది
తక్కువైన దనుకోరాదు. ఒక్కగ్రాము బరువు రేడియం నుంచి ప్రతి గంటకూ
నిప్పుపుల్ల వెలిగితే వచ్చే వేడిలో సగం చొప్పున అనేక ఏళ్ళతరబడి
#వస్తుంది.
వెయ్యేళ్ళ అనంతరం కూడా యీ శక్తిలో మూడోవంతు మాత్రమే తగ్గుతుంది.
ఒక్క పౌను బరువుగల రేడియం రేడియేషన్ ద్వారా యిచ్చే శక్తిని, ఒక్కవారం
రోజులలో బయటికి తీయగలిగితే ఆ శక్తితో 15 వేల టన్నుల బరువుగల స్టీమరును
గంటకు సుమారు 15 మైళ్ళవేగంతో వారంపాటు నడపవచ్చునని శాస్త్రజ్ఞులు అంచనా
వేశారు.
 కాని, ఇది కేవలం వూహ. మనం ఏం చేసినా రేడియం తనమానాన
రేడియేషన్్ యిస్తుంది. కాని, మనకు కావలసినట్లు యివ్వదు. అందుకే రేడియం
పరిశోధనలు జరిగిన ఎంతో కాలానికిగాని, మనిషికి పరమాణుశక్తి అందుబాటులోకి
రాలేదు.
55-
 10. శక్తీ, దాని ప్రసారమూ
 స్విచ్ వెయ్యగానే విద్యుద్దీపం వెలగటం మనకందరికీ తెలుసు. బల్బు
వెలగగానే, దానిలోని ఫిలమెంట్్ గుండా విద్యుచ్ఛక్తి ప్రవహిస్తున్నట్లు మన
#కన్ను
తెలుసుకుంటూంది. విద్యుచ్ఛక్తి కాంతి ప్రసారానికి కారణభూత మవుతున్నది.
 ఇనుమును కాలిస్తే, ఎర్రగా ఔతుంది; లేక ఎర్రని కాంతిని
#ప్రసరిస్తుంది.
అది కొంత చల్లారినాక కాంతి ప్రసరించటం మానేస్తుంది. కాని అప్పటికి కూడా
యినుముకు దగ్గరగా చెయ్యి పెడితే, చేతికి వేడి తగులుతుంది.
 వేడి కూడా కాంతిలాగే కిరణప్రసారం. ఈ ప్రసారం ఒక విధమైన అలల
రూపంలో జరుగుతుంది- నిశ్చలంగా ఉన్న చెరువులో రాయివేస్తే, అలలు బయలు
దేరి చుట్టూ ప్రసరించినట్టుగా కంటికి కనబడే కాంతి తాలూకు అలల ప్రమాణం
రంగు రంగుకూ మారుతుంది.
 అప్పుడప్పుడూ, ఆకాశంలో ఇంద్రధనుస్సు కనిపిస్తుంది. అది ఒక చివర
ఎర్రగా, రెండో చివర ఊదారంగుగా ఉంటుంది. మధ్యలో నారింజ, పసుపు, ఆకు
పచ్చ, నీలం మొదలైన రంగు లుంటాయి. కనబడే కాంతులన్నిటిలోకీ ఎరుపుకాంతి
తాలూకు అలలు ఎక్కువ పెద్దవి. ఊదారంగు కాంతివి చాలా చిన్నవి.
 ఎర్రనికాంతి అలలకన్న కూడా పెద్దవి వేడికిరణాల తాలూకు అలలు.
#వీటిని
"ఇన్్ఫ్రారెడ్" కిరణా లంటారు. ఈ కిరణాల సహాయంతో పోటోలు తియ్యటానికి
ప్రత్యేకంగా ప్లేట్లూ, ఫిలములూ ఉన్నాయి. వీటిమీద చీకటిలో కూడా ఫోటోలు
తీయవచ్చు.
 ఎర్రని కాంతి కన్న పెద్ద అలలున్నట్లే, ఊదారంగు కాంతికన్న చిన్న
#అలలు
కూడా ఉన్నాయి. వీటిని "అల్్ట్రా వయొలెట్" కిరణా లంటారు. అవికూడా
కంటికి కనిపించవు. కాని, ఫోటోలుతీసే ప్లేట్్ను పాడు చెయ్యగలవు.
 "ఇన్్ఫ్రారెడ్" కిరణాలను వాపులు నయంచెయ్యటానికి ఉపయోగిస్తారు.
"అల్్ట్రా వయొలెట్" కిరణాలను నూనెమీద కొంతసేపు ప్రసరించినట్టయితే, ఆ
నూనెలో "డి" విటమిన్ ఉత్పత్తి అవుతుంది. సూర్యకాంతిలో "అల్్ట్రా
వయొలెట్" ఉన్నది. దాని సహాయంతోటే, మన శరీరాలు తమకు కావలసిన "డి"
విటమిన్్ను తయారుచేసుకుంటాయి. చలిదేశాలలో మనుషులు ఎండ వచ్చినప్పుడు
శరీరాలకు నూనె పట్టించి, ఆతప స్నానాలు చేస్తారు.
 ఐతే, అన్నిరకాల ప్రసారమూ మంచి చేసేదే ననుకోరాదు. ఉదాహరణకు,
"అల్్ట్రా వయొలెట్" అమితంగా కళ్ళలోకి పోతే, కళ్ళకు జబ్బు చేస్తుంది.
#అందుకనే
56-
సూర్యుడికేసి ఉత్తకళ్ళతో చూడరాదు. సినిమా ప్రొజెక్టర్లు మొదలైన వాటిల్లో
ఉపయోగించే ఆర్క్్లైట్లకు "అల్్ట్రా వయొలెట్"ను హరించే అడ్డాలేవైనా ఉంటే
తప్ప, వాటికేసి ఉత్తకళ్ళతో చూడరాదు.
 "అల్్ట్రా వయొలెట్" అలలకన్న చాలా రెట్లు చిన్నవి "ఎక్స్్రే" లు
#ఇవి
కూడా కంటికి కనబడవు. ఫోటోలు తీసే ప్లేట్్లను పాడుచేస్తాయి. వీటికిగల
అపారమైన శక్తిఏమంటే, అద్దంలోనుంచి కాంతి కిరణాలు ఎంత సులువుగా
ప్రసరిస్తాయో, ఈ "ఎక్స్్" కిరణాలు దళమైన లోహపు పలకలను కూడా అంత
సులువుగా తోసుకుపోతాయి. మీలో చాలామంది "ఎక్స్్రే" ఫోటోలను చూసి
ఉండవచ్చు. కొన్ని కొన్ని జబ్బులలో ఊపిరితిత్తు లెలా ఉన్నదీ
#పరిశీలించటానికిగాను ఈ,
ఫోటోలు తీస్తారు. ఈ ఫోటోలలో వెన్నెముకా, పక్క ఎముకలూ, ఊపిరితిత్తులూ
స్పష్టంగా కనిపిస్తాయి. ఊపిరితిత్తులు ఏ స్థితిలో ఉన్నదీ వైద్యులు ఈ
#ఫోటోల
మూలంగా తెలుసుకోగలుగుతారు. ఎవరికన్నా ఎముక విరిగి ఒక భాగం
తొలగిపోయినప్పుడు "ఎక్స్్రే" ల సహాయంతో విరిగిన ఎముకను మళ్ళీ కలిపిపెట్టి, ఆ
#తరువాత
కట్టు కడతారు. సరిగా చేర్చిపెట్టని ఎముక తిరిగి అతుక్కున్నా బలహీనంగానే
ఉంటుంది. కాని, చక్కగా కలిపిపెట్టిన ఎముక అతుక్కున్న తరువాత మరింత
బలిష్టంగా ఉంటుంది.
 "ఎక్స్్" కిరణాలను పరిశ్రమలలో కూడా వాడతారు. యంత్రాలలో కొన్ని
భాగాలు బలిష్ఠంగా ఉండాలి. ఈ భాగాలను పోతపోసినప్పుడు వాటిలోపల పూడని
డొల్ల లున్నాయో, లేదో చూడటానికి ఈ కిరణాలు తోడ్పడతాయి. లోపల ఏ పాటి
డొల్లవున్నా ఈ కిరణాలు బయట పెట్టేస్తాయి.
 "ఎక్స్్" కిరణాలు కూడా అతిగాసోకితే ప్రమాదం ఉన్నది. కీటకాలు
మొదలైనవాటిమీద ఈ కిరణాలను ప్రసరించగా వాటికి పుట్టిన పిల్లలలో జాతిలక్షణాలు
మారిపోవటం జరిగింది.
 ఆకాశాన ఉన్న నక్షత్రాలలో సూర్యుడు ఒకటి. సూర్యుడికి అన్నింటికన్న
దగ్గరగా ఉన్న నక్షత్రంనుంచి సూర్యుడు దగ్గరికి కాంతి చేరాలంటే దాదాపు
#నాలుగున్నర ఏళ్ళు పడుతుంది. (కాంతి ఒక సెకండు కాలంలో 1,86,000 మైళ్ళు
ప్రసరిస్తుందని జ్ఞాపకం వుంచుకోండి!) దీనినిబట్టి సూర్యుడి సామీప్యంలో
#ఎక్కడా
శక్తివంతమైన ప్రసారవస్తువు లేదు. ఈ సూర్యుడినుంచి మనకు అంతులేని వెలుగూ,
వేడి వస్తున్నది. కాని, సూర్యుడు అనుక్షణమూ వెదజెల్లే ప్రసారాన్ని 200
#కోట్ల
భాగాలుగా విభజించినట్లయితే, అందులో ఒకే ఒక భాగం భూమిని చేరుతున్నది. ఈ
57-
కొద్ది ప్రసారంతాలూకు ఒత్తిడి 75 వేల టన్నుల బరువుకు సమంగా ఉంటుంది.
ప్రసారం రూపంలో సూర్యపదార్థం నిమిషానికి 25 కోట్ల టన్నుల చొప్పున తరిగి
పోతుందని శాస్త్రజ్ఞులు అంచనా వేశారు.
 సూర్యగర్భంలో 2కోట్ల డిగ్రీ లుంటుందని (నీరును ఆవిరిచేసే వేడి
100 డిగ్రీలు). అయితే సూర్యుడి గర్భంతో పోలిస్తే పైభాగం చాలా చల్లగా
ఉంటుంది. దాని వేడి సుమారు 6,000 డిగ్రీలు మాత్రమే. సూర్యుడి పై పొరలు
పోయి సూర్యగర్భంలోని వేడి భూమి మీద ప్రసరించినట్టయితే, కొద్దిసేపట్లో భూమి
మీద ఉన్న ప్రతి శిలా ద్రవించి, మరికొంతసేపటిలో భూమి యావత్తు వాయు
రూపంలోకి మారిపోతుంది.
 ఇంతవేడి సూర్యుడిలో ఏ విధంగా పుడుతున్నది? ఏ విధంగా ఈ
#అంతులేని
ప్రసారం సాగుతున్నది? కుంపటిలో బొగ్గులన్నీ పండునిప్పు లయాక చాలాసేపు
ఒకటే తీరుగా వేడి ఇస్తుంది. చాలాసేపటికిగాని వేడిలో తగ్గుదల తెలియదు.
సూర్యుడు అటువంటి కుంపటా? కాదు. ఎందుచేతనంటే, మానవులు 1945 లో
మొదటిసారి సాధించిన పరమాణుశక్తి అనాదిగా, అవిచ్ఛిన్నంగా, సూర్యమండలంలో
అనుక్షణమూ పుడుతున్నది.
 పరమాణువులు విచ్ఛిన్నమైనప్పుడు ఏయే ప్రసారాలు జరిగేదీ ఈనాడు
శాస్త్రజ్ఞులకు స్పష్టంగా తెలుసు. "అణువు" బాంబులు పేలినప్పుడు కాంతి
#ప్రసరిస్తుంది. ఇది 50 మైళ్ళ దూరానికి కనిపించిందనీ, 75 మైళ్ళ దూరానికి
#కనిపించిందనీ పత్రికలలో మనం చదివాం. కాంతి అంటే కేవలం కంటికి కనిపించే ప్రసారం.
తరంగాలుగా ప్రసరించే వాటిలో "ఇన్్ఫ్రారెడ్", "అల్్ట్రా వయొలెట్" కూడా
ఉన్నట్టు మనం ఇంతకు ముందు అనుకున్నాం. "అణువు" బాంబులు పేలినప్పుడ఼ు
పేలుడు సమీపంలో ఉన్న మనుషులు వాయురూపంలో అదృశ్యులైపోవడం
జరిగింది. అందుచేత, కంటికి కనిపించని తరంగాల ప్రసారం కూడా పరమాణువులు
విచ్ఛిత్తి పొందినప్పుడు జరుగుతుంది.
 ఇటువంటి తరంగా లన్నిటిలోకీ అత్యంత సూక్ష్మమైన వాటిని "గామా"
#కిరణాలంటారు. ఇవి `ఎక్స్్' కిరణ తరంగాల కన్న ఎన్నోరెట్లు చిన్నవి.
#ఘనపదార్థాలలోకి చాలాదూరం చొచ్చుకుపోతాయి. ఇవి ప్రాణోపద్రవం కలిగించ గలిగినవి.
హిరోషిమా నగరంమీద మనిషిచేసిన "అణువు" బాంబు పేలినప్పుడు చచ్చినవారిలో
నూటికి 70 మంది మాత్రమే అదేరోజున - మామూలుబాంబు దెబ్బతిని చచ్చిపోయిన
వాళ్ళ లాగా - చచ్చిపోయారు. మిగిలిన 30 మందీ ప్రసారం (రేడియేషన్) ఫలితంగా
58-
చచ్చినవారే. పరమాణుశక్తి వెంబడి ప్రసారమయే "గామా" కిరణాలూ,
న్యూట్రాన్్లూ కూడా మనిషి శరీరంలోని ప్రతి జీవకణాన్నీ భేదించి, శరీరమంతా
విషపూరితం చేస్తాయి. రక్తంలోని తెల్లజీవకణాలు-రోగక్రిములను సంహరించేవి.
నశించిపోతాయి. శరీరం మామూలుగా తట్టుకునే అంటువ్యాధులను నిరోధించలేక,
చచ్చిపోయిన వాడి శరీరంలాగా కుళ్ళనారంభిస్తుంది. ఈ ప్రసారం ఫలితంగా రెండు
వారాలలో చనిపోయినవారున్నారు. పది సంవత్సరాల అనంతరం - ఇటీవలనే
చనిపోయినవారు కూడా ఉన్నారు.
 హిరోషిమా, నాగసాకీ నగరవాసుల చావుకు కారణభూతమైన "గామా" కిరణ
ప్రసారం ఆకాశంలో నుంచికూడా వస్తున్నది. అయితే, అదృష్టవశాత్తూ మానవుడిపట్ల
మానవుడున్నంత క్రూరంగా ప్రకృతి ఉండటంలేదు. భూమికి వచ్చే "గామా"
కిరణాలలో చాలాభాగాన్ని భూమిచుట్టూ ఉండే గాలిపొర పీల్చేస్తున్నది. గాలిలో పైకి
వెళ్ళిన కొద్దీ గామాకిరణ ప్రసారం బలంగా కనిపిస్తుంది.
 కాంతి లాగా, "ఇన్్ఫ్రారెడ్" , "అల్్ట్రా వయొలెట్" ల లాగా,
#"ఎక్స్్"
కిరణాలలాగా, "గామా" కిరణాలలాగా న్యూట్రాన్్లు అలలుకావు. అవి పదార్థాల
తాలూకు పరమాణువుల కేంద్రాలలోచేరి ఉండేవి.
 ఒక పౌను యురేనియం భస్మీపటలం ఐనట్టయితే, అందులో ఎన్ని
పరమాణువు లుంటాయో, దానికి రెట్టింపు సంఖ్యలో న్యూట్రాన్్లు విడుదల ఔతాయి.
ఈ సంగతి మనం ఎరిగినదే. అవి పరమాణువుల కేంద్రాలను సైతం సుళువుగా
చేరగలవి. అందుచేత, "అణువు" బాంబు పేలినపుడే, న్యూట్రాన్్లు అపారమైన
సంఖ్యలో అంతులేని వేగంతో ప్రసారమై, వాటి బారిపడినవారిని బతికుండగానే
కళేబరాలకింద మార్చడంలో వింత ఏమీలేదు.
 "అలలు" గా కాకుండా ప్రసరించే వాటిల్లో ఒకటి "ఆల్ఫా" కణం. ఇందలో
2 న్యూట్రాన్లు, 2 ప్రోటాన్లు కలిసి ఉంటవి. హీలియం పరమాణువు కేంద్రంలో
ఇవే వుంటాయి. వీటిచుట్టూ 2 ఎలెక్ట్రాన్లు తిరుగుతూ వుంటాయి. ఆ
#ఎలెక్ట్రాన్లను
తీసివేసినట్లయితే, మిగిలినదాన్ని "ఆల్ఫా" కణం అంటారు. సూర్యుడినుండి
"ఆల్ఫా" కణాలు ప్రసారమవుతున్నాయి.
 రేడియం పరమాణువులు తమకుతామే విచ్ఛిత్తి పొందేప్పుడు కూడా "ఆల్ఫా"
కణాల ప్రసారం జరుగుతుంది. వాటితోబాటు, ఎలెక్ట్రాన్్లుకూడా ప్రసారమవుతాయి.
ఎలెక్ట్రాన్లను "బీటా" కిరణాలని కూడా అంటారు.
59-
 కనుక, మనం ప్రసారం గురించి స్థూలంగా తెలుసుకోదగిన
విషయాలేమంటే:
 1. ఇందులో అలలుగా జరిగే ప్రసారం కొంత, పరమాణువుల లోపలఉండే
భాగాలు, విడివిడిగా ప్రసరించటం కొంత.
 2. ప్రతి ప్రసారమూ ఒక శక్తిని సూచిస్తుంది.
 3. శక్తిసాధనాలలాగే ప్రసారాన్ని కూడా అదుపులో ఉంచకపోతే, చాలా
హాని కలిగించగలదు.
 4. అదుపులోకి వచ్చిన ప్రసారం అనేకవిధాల మనకు అదివరకే
తోడ్పడుతున్నది. పరమాణుశక్తి వెంట వెలువడే ప్రసారం కూడా ఇదేవిధంగా మానవ
క్షేమానికీ, అభ్యుదయానికీ ఉపయోగించే అవకాశం ఉంది.
 11. రేడియో పదార్థాలు- వాటి ఉపయోగాలు
 "ఘనీభవించిన శక్తే పదార్థం" అన్నారు శాస్త్రజ్ఞులు. యురేనియం,
#ప్లుటో
నియంలవంటి బరువైన పదార్థాల పరమాణువులను ఛేదించినప్పుడుకూడా, మనకు ఆ
పరమాణువుల పదార్థంలో ఉండే శక్తిలో వెయ్యోవంతు మాత్రమే లభిస్తున్నది.
హైడ్రొజెన్్వంటి తేలికపదార్థం తాలూకు పరమాణువులను ఛేదించినట్లయితే, ఇంకా
చాలా ఎక్కువ శక్తి వెలువడుతుంది. హైడ్రొజెన్ బాంబులోని శక్తి ఇటువంటిదే.
కాని, ఇలాంటి శక్తిని అదుపులోకి తీసుకురావటానికి ఇప్పటికింకా సాధనాలు
#లేవు.
అందుచేత, ఈ శక్తి మానవాభివృద్ధికి ప్రస్తుతంలో ఉపయోగపడదు.
 "ఎటామిక్ ఫైల్" నుంచి వెలువడే వేడిద్వారా, నీటిని బోయిలర్లలో
#ఆవిరి
చేసి దానిద్వారా జెనరేటర్లను నడిపి విద్యుచ్ఛక్తి సృష్టించవచ్చునని మనం
#అదివరకే
తెలుసుకున్నాం. "ఫైల్" తాలూకు వేడిని చలిదేశాలలో ఇళ్ళను వెచ్చగా
వుంచటానికి కూడా ఉపయోగించవచ్చు. పరమాణు శక్తిని అమెరికా, బ్రిటన్, ఫ్రాన్స్్
దేశాలలో యిదివరకే యీ విధంగా వుపయోగపరుస్తున్నారు. కాని, ప్రపంచంలో
60-
మొదటిసారిగా పారిశ్రామిక విద్యుత్తుకోసం పరమాణుశక్తిని వినియోగపరిచిన దేశం
సోవియట్ యూనియన్. 1954 జూన్ 27 న అక్కడ 50,000 కిలోవాట్ల శక్తిగల
పరమాణు విద్యుత్కేంద్రం పని ప్రారంభించి, చుట్టుప్రక్కల వ్యవసాయానికీ,
పరిశ్రమలకూ విద్యుచ్ఛక్తి అందిస్తోంది. లక్ష కిలోవాట్ల కేంద్రం మరొకటి
#అక్కడ
నిర్మాణంలో ఉన్నది.
 అమెరికాలో మొట్టమొదటగా పరమాణుశక్తిని "శాంతియుతంగా" వినియోగ
పరచినది ఒక సబ్్మెరీన్ (జలాంతర్గామి) నడపటానికే! ఇటీవలననే తయారైన
ఈ సబ్్మెరీన్ ప్రపంచంలోకల్లా పెద్దది. దీని నిడివి దాదాపు నూరుగజాలు
మామూలు సబ్్మెరీన్్లకంటే వేగంగా పోతుంది. దీని పేరు "నాటిలస్". దీని
"ఇంజను" ఒక చివర వున్నది; మనుషులు రెండో చివర ఉంటారు. మామూలు
ఇంజన్లలో బొగ్గుగాని, నూనెగాని మండటానికి ప్రాణవాయువ కావాలి. కాని,
పరమాణుశక్తి ఉత్పత్తిచేసే ఇంజన్్కు ప్రాణవాయువు అవసరంలేదు. సబ్్మెరీన్్లో
ఉండేవారికి కావలసిన ప్రాణవాయువును కృత్రిమంగా సరఫరా చేయవచ్చు. అందు
చేత, "నాటిలస్" లాటి సబ్్మెరీన్ నిరాఘాటంగా, ఎంత కాలమైనా నీటి అడుగున
సంచరించగలదు.
 "ఎటామిక్ ఇంజన్" తో నడిచే విమానాన్ని తయారుచేయటానికి సోవియట్
శాస్త్రవేత్తలు పరిశోధనలు జరుపుతున్నారు. పరమాణుశక్తితో సబ్్మెరీన్ నడిచిన
విధంగానే మామూలు నౌకలుకూడా నడవవచ్చు. కాలక్రమాన మోటారుకార్లకు కూడా
పరమాణుశక్తి ఉపయోగిస్తారేమో! ఇవన్నీ భవిష్యత్తులో జరిగే పరిశోధనలపై ఆధార
పడి వుంటాయి.
 ప్రస్తుతానికి జనసామాన్యానికి అందుబాటులో ఉండి, జీవితాన్ని
#అభివృద్ధి
పరచటానికి సిద్ధంగా వున్నది రేడియో పదార్థాలు. వీటిని గురించి రష్యాలోనూ,
#యితర
చోట్లా యిప్పటికే యెన్నో గొప్ప పరిశోధనలు జరిగాయి. ఈ పదార్థాల ఉపయోగాలు
కొంత వివరంగా తెలుసుకుందాం.
 అనేక పదార్థాలకు రేడియేషన్ శక్తిగల "ఐసోటోప్" లున్న సంగతి మనం
అదివరకే తెలుసుకున్నాం. వీటిని "ఎటామిక్" పైల్్లో కృత్రిమంగా తయారుచేయ
వచ్చు. రేడియం సహజంగా రేడియేషన్ ఇచ్చినట్టే, ఈ పదార్థాలు తాత్కాలికంగా
రేడియేషన్ ఇస్తాయి. ఇది కొద్దినిముషాలే ఉండవచ్చు. కొన్ని ఏళ్ళపాటు కూడా
ఉండవచ్చు. ఇనుము (53) రేడియేషన్ 9 నిమిషాలపాటుండి తరువాత పోతుంది.
ఇనుము (55) రేడియేషన్ 4 ఏళ్ళపాటుంటుంది. (ఇనుము 53 అంటే, దాని
61-
తాలూకు పరమాణువుల కేంద్రంలో ప్రోటాన్లూ, న్యూట్రాన్లూ కలిసి 53 అన్న
మాట! ఇనుము 55 లో ఈ సంఖ్య 55 ఉంటుంది.)
఼
            0 
</p></body></text></cesDoc>