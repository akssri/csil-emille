<cesDoc id="tel-w-socsci-ling-nerchu" lang="tel">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tel-w-socsci-ling-nerchu.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-13</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>అ ఆ లు నే఼ర్చుకొందాం!</h.title>
<h.author>పి. సరళ</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1982</pubDate>
</imprint>
<idno type="CIIL code">nerchu</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 24.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-13</date></creation>
<langUsage>Telugu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>పేజి 5 అచ్చులు
 ---------
 అ ఆ ఇ ఈ చదవాలి,
 అన్నీ చక్కగా దిద్దాలి.
 ఉ ఊ ఋ ఋ నేర్వాలి,
 ఊ౟యల హాయిగా ఊగాలి.
 ఎ ఏ ఐ అని వ్రాయాలి,
 ఎన్నో ఊసులాడాలి.
 ఒ ఓ ఔ అని నేర్వాలి,
 అం అః అంటూ ఆడాలి.
పేజి 6 హల్లులు
 -------
 క ఖ గ ఘ ఙ
 కమ్మని తెలుగు నేర్వాలి;
 చ ఛ జ ఝ ఞ
 చేతులు కలిపి పాడాలి;
 ట ఠ డ ఢ ణ
 టాటా చెపుతూ ఆడాలి;
 త థ ద ధ న
 తందానంటూ చెప్పాలి;
పేజి 7
 ప ఫ బ భ మ
 పాడుకొంటూ పలకాలి;
 య ర ల ళ వ
 ఎంతో ఇంపుగ వ్రాయాలి;
 శ ష స హ చెప్పాలి
 వర్ణమాలను కూర్చాలి
 అచ్చులు హల్లులు హాయిహాయిగా
 అందరు ఒక్కటై నేర్వాలి.
పేజి 8 అచ్చులు
 --------
 (పలుకులు - పాటలు )
 ---------------------
 `అ'తో వచ్చె అమ్మ, అన్నం
 అమ్మ ఒడిలో ఎదగాలి
 అన్నం అందరికి ఉండాలి
 `ఆ' తో వచ్చె ఆటలు
 ఆటలు ఆడితే ఆరోగ్యం
 ఆటలలోనే ఆనందం
పేజి 9
 `ఇ'పై వచ్చె ఇల్లు
 ఇల్లు ఎల్లరికుండాలి
 ఇంటిలో సుఖము పొందాలి
 `ఈ'పై వచ్చె ఈగలు
 ఈగలు తెచ్చు రోగాలెన్నో
 `ఉ'పై వచ్చె ఉప్పు, ఉన్ని
 ఉప్పు వంటికి ఉండాలి
 ఉన్ని ఒంటికి కావాలి
పేజి 10
 `ఊ'మీదెచ్చె ఊరు, ఊయల
 ఊరు మంచిగ ఉండాలి
 ఊయల ఊగుతు పాడాలి
 `ఋ'పై వచ్చును ఋతువు
 ఋతువులు మొత్తము ఆరు
 ఋతువులు తెలుపును కాలం
 `ఎ'తో వచ్చె ఎద్దులు, ఎరువులు
 ఎద్దులు పొలము దున్నాలి
 ఎరువులు చేనుకు కావాలి
పేజి 11
 `ఏ'తో వచ్చు ఏరు
 ఏరులు బాగా పారాలి
 పంటలు బాగా పండాలి
 `ఐ'తో వచ్చు ఐక్యము
 ఐక్యమె మనకు బలమిచ్చు
 `ఒ'తో వచ్చె ఒకటి
 ఒకటితో మొదలు అంకెల సంఖ్యలు
 ఒకటై మనము నేర్చేద్దాం
పేజి 12
 `ఓ'పై వచ్చె ఓడ
 ఓడల సాగు వ్యాపారం
 `ఔ'తో వచ్చు ఔషధం
 ఔషధమిచ్చె వైద్యులు
 `అం'పై వచ్చు అందరము
 అందరము అచ్చులు నేర్వాలి
పేజి 13 హల్లులు
 ---------
 (పలుకులు-పాటలు)
 ------------------
 క,ఖ,గ,ఘ,ఙ
 కమ్మని తెలుగు నేర్వండీ!
 `క'తో వచ్చెను కలము, కత్తి
 కత్తికన్నా కలమే మిన్న
 కలముతో వ్రాత వ్రాయాలి
 కలమే గొప్పని చాటాలి
 `ఖ'తో వచ్చెను ఖర్చు
 ఖర్చులు మితము చేయండీ
 పొదుపుగ ఉండుట నేర్వండీ
పేజి 14
 `గ'తో వచ్చెను గనులు
 గనులలో ఉన్నవి నిధులు
 నిధులే జాతికి సిరులు
 `ఘ'తో వచ్చెను ఘనులు
 ఘనమగు పనులు చేయండీ
 జాతి ఘనతను చాటండీ
 `ఙ'తో వచ్చెను జ్ఞానము
 జ్ఞానము మనిషికి కావాలి
 విజ్ఞానముతో వెలగాలి!
పేజి 15
 చ,ఛ,జ,ఝ,ఞ
 చేతులు కలిపీ నేర్వండీ!
 `చ'పై వచ్చే చదువులు
 చదువులు చక్కగ చదవాలి
 చక్కని పనులు చేయాలి
 `ఛ'పై వచ్చే ఛత్రిము
 ఛత్రము నీడను ఇచ్చేను
 ఎండా వానల ఆపేను!
పేజి 16
 `జ'పై వచ్చె జనుము
 జనుముతో నార వచ్చేను
 నారతో తాళ్లను పేనేరు
 `ఝ'పై వచ్చే ఝండా
 ఝండా దేశముకుండాలి
 దేశమును ప్రేమించాలి
 ట,ఠ,డ,ఢ,ణ
 టాటా చెపుతూ నేర్చేము
పేజి 17
 `ట'తో వచ్చెను టపా
 టపా వార్తలు తెచ్చేను
 `ఠ'తో వచ్చెను ఠంగు
 ఠంగున మ్రోగును బడిగంట
 ఠంగున మ్రోగును గుడిగంట
 `డ'తో వచ్చెను డబ్బు
 డబ్బులు పొదుపుగ వాడాలి
 హోయిగ కాలం గడపాలి
పేజి 18
 `ఢ'తో వచ్చెను ఢంకా
 ఢంకా మ్రోగెను ఢంఢం
 త,థ,ద,ధ,న
 తందానంటూ నేర్చేము
 తతో వచ్చెను తప్పులు
 తప్పులు చేయుట తగదమ్మా
 ఒప్పులు చేయుట నేర్వమ్మా
పేజి 19
 `ద'తో దర్జీ వచ్చేను
 దర్జీ బట్టలు కుట్టేను
 `ధ'తో వచ్చెను ధరణి
 ధరణిలో పంటలు పండాలి
 తిండి దండిగ వుండాలి
 `న'తో నదులు వచ్చేను
 నదులే నీళ్లు ఇచ్చేను
పేజి 20
 ప,ఫ,బ,భ,మ
 పాడుకుంటూ పలికేము
 `ప'పై వచ్చు పనులు, పరువు
 పనులు తప్పక చేయండీ!
 పరువుగ బ్రతక నేర్వండీ!
 `ఫ'పై వచ్చును ఫలము
 ఫలములు ఇచ్చును బలము
పేజి 21
 `బ'పై వచ్చును బడి, బండి
 బడికి పిల్లలు వెళ్లాలి
 బండికి చక్రం కావాలి
 `భ'పై వచ్చును భరతుడు
 భరతుడు పుట్టిన దేశం
 భారతదేశం మన దేశం
 `మ఼పై' వచ్చును మనిషి
 మనిషి మనిషిగా ఎదగాలి
 మానవతను నిలపాలి
పేజి 22
 య,ర,ళ,ల,వ
 ఎంతో ఇంపుగ వ్రాసేము
 `య'తో వచ్చును యంత్రము
 యంత్రము చేయును పనులెన్నో!
 `ర'తో వచ్చును రథము
 రథమున దేవుడు ఊరేగు
పేజి 23
 `ల'తో లతలు వచ్చేను
 లతలే అందం తెచ్చేను
 `వ'పై వలలు వచ్చేను
 వలతో చేపలు పట్టేరు
పేజి 24
 `శ'తో వచ్చును శక్తి
 శరీరమిచ్చును శక్తి
 `ష'పై వచ్చే షడ్రుచులు
 షడ్రుచులంటే తీపి, ఉప్పు,
 పులుపు, కారం, వగరు, చేదు
 `స'పై వచ్చే సరుకులు
 సరుకులు చౌకగా దొరకాలి
 అందరు సుఖముగ బ్రతకాలి
 `హ'తో వచ్చును హలము
 హలముతో పొలమును దున్నేము.
 శ,ష,స,హ చెప్పాము
 వర్ణమాలను అల్లాము
-------------------------------------------------------------------------
X
            0 
</p></body></text></cesDoc>