<cesDoc id="tel-w-science-astronomy-andina2" lang="tel">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tel-w-science-astronomy-andina2.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-13</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>అందిన ఆకాశం</h.title>
<h.author>రెడ్డి రాఘవయ్య</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">andina2</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 19.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-13</date></creation>
<langUsage>Telugu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>38-
 దూరసీమలకు దారి చూపిన రాకెట్లు
 కథ చెబుతున్న రఘురాంగారికీ, వింటున్న కోటికీ కాలం సంగతి పట్టలేదు.
అదితన పని తాను చేసుకు పోతున్నది.
 అప్పటికి రాత్రి పది గంటలు దాటింది. ఆకాశంలో చంద్రుడు
కనుపిస్తున్నాడు. పౌర్ణమి వెళ్ళి ఆనాటికి నాలుగు రోజులు అయ్యింది. అందుకని
చంద్రుడు మూడు వంతులేక కనుపిస్తున్నాడు.
 "ఇకను మన కథ దేన్ని గురించి నాన్నా?" కోటి అడిగాడు.
 "దేన్ని గురించా?" అని ఆయన ఆలోచించి. "ఆం! అదిగో చూడు!"
అంటూ ఆకాశంలోని చంద్రుణ్ణి చూపారు.
 "ఆం! అయితే చంద్రమండలాన్ని చేరుకున్న మనుష్యుల కథా?" కోటి
చంద్రుణ్ణి చూచి అడిగాడు.
 "అబ్బో! ఆ కథ చాలా ఉంది. చెప్పుకోవాలంటే ఇది సమయం కాదు.
కాని మనుష్యుల కంటే ముందు అక్కడకు బాటలు వేసిన అంతరిక్ష నౌకలను
గురించీ, వాటిని అంతరిక్షంలోకి మోసుకు వెళ్ళిన రాకెట్ల గురించీ
#చెప్పుకుందాం!
అన్నారు రఘురాంగారు.
 "సరే! చెప్పునాన్నా?" అని కోటి తరువాతి కథకు ఉత్సాహం
చూపాడు.
 "రాకెట్లు వాతావరణాన్ని పరిశీలిస్తున్నాయి. ఉపగ్రహాలను భూమి
#కక్ష్యలో
వదులు తున్నాయి. చంద్రమండలానికీ, దూరంలో ఇతర గ్రహాలకూ మనుషులు
వెళ్ళక పోయినా, అంతరిక్ష నౌకలను పంపిస్తున్నాయి. అందుకని ఇవి ఒక
విధంగా దూర సీమలకు దారి చూపిస్తున్నాయి." అని వివరించాడు ఆయన.
 "దీని కోసం రష్యా, అమెరికాలు కృషి చేసి ఉండవచ్చు. ఆ కృషి
#గురించి
చెబుతారా?" మధ్యలో కోటి అడిగాడు.
 "చంద్ర మండల యాత్ర అంటే ఆ రెండు దేశాలదేపై చేయి. రెండూ
పోటీపడి ఒక దాని కంటే ఒకటి ఎన్నో ఘన కార్యాలు సాధించాయి.
39-
 చంద్ర మండలానికి మనుష్యులు లేకుండా నౌకలను పంపటానికి అమెరికా
ప్రయత్నం చేసింది. ఒక విధంగా ఇవి ఉపగ్రహాలలాంటివే! కాక పోతే వీటికి
చంద్రుని చుట్టి వచ్చే ఏర్పాటు ఉంటుంది. అమెరికా దగ్గర "థార్్ -ఏబుల్్" అనే
రాకెట్్ ఉన్నది. థార్్ మొదటి అంచెఅయితే - ఏబుల్్ రెండవ అంచె, పొడవు 25
నుండి 28 మీటర్లు ఉంటుంది. ద్రవ ఇంధనాన్ని ఉపయోగిస్తారు. రాకెట్టుకు
ఇంకా డెల్టా, ఎజీనా-డి, మొదలైన రాకెట్లను కూడా కలిపారు. దీనితోనే
అమెరికావారు, ఎక్స్్ప్లోరర్్, టిరోస్్, టెలిస్టార్్, ఎర్లీ బర్డ్్ మొదలైన
#ఉపగ్రహాలను
భూమి కక్ష్యలోకి ప్రవేశపెట్టారు." రఘురాం గారు చెప్పారు.
 "అయితే దానికే "ఏబుల్్ రాకెట్్ను కలిపి" చంద్ర నౌకలను మోసుకు
వెళ్ళేందుకు ఉపయోగించారన్న మాట." కోటి మధ్యలో అన్నాడు.
 "అవును, ఆం! ఇకను `పయోనీర్్' అనే చంద్ర నౌకలను తయారుచేశారు.
వాటిని అంతరిక్షంలోకి వదలే కార్యక్రమాన్ని చేపట్టారు.
 1958 అక్టోబరు 11న మొదటి "పయోనీర్్" ను రాకెట్టు అంతరిక్షంలోకి
మోసుకు వెళ్ళింది. దానిని అంతరిక్షంలో వదలింది. పయొనీర్్-1 అలా వెళ్ళి
#వెళ్ళి
70,700 మైళ్ళ ఎత్తుకు వెళ్ళింది. తిరిగి భూమి మీదకు పడిపోయింది." రఘురాం
గారు వివరించారు.
 "అంటే, అంతవరకూ ఏ వాహనమూ వెళ్ళ లేనంత ఎత్తు అన్నమాట
అది" కోటి అడిగాడు.
 "అవును. అంత ఎత్తే! మొదటి పయొనీర్్ యిచ్చిన వుత్సాహంతో
2,3 నౌకలను కూడా పంపారు. మూడవ దానిని మోసుకు వెళ్ళిన రాకెట్్ను
"జూనో" అన్నారు. ఇంతకుముందు మనం "జుపిటర్్" రాకెట్లను గురించి
చెప్పుకున్నాం! ఆ జుపిటర్్కు యింకొక రాకెట్టును కలపగా ఏర్పడిందే " జూనో"
#రాకెట్్
 1958 డిసెంబరు 6వ తారీఖున 3 వ నౌకను పంపారు. పై అంచెల్లో
పని సవ్యంగా జరుగలేదు. అందుకని అది 66, 654 మైళ్ళు వెళ్ళ గలిగింది.
 అంతటితో ఆ ప్రయత్నాలకు స్వస్తి పలుకకుండా "నాలుగవ
పయొనీర్్" ను ప్రయోగించారు. 1959 మార్చి 3న మళ్ళీ జూనో రాకేట్్తోనే ఆ పని
40-
చేశారు. అది దాదాపు చంద్రమండలాన్ని చేరుకొంటుందనే ఆశించారు. కాని
37,300 మైళ్ళు యింకా వుందనగానే దాని పని అయిపోయింది"
 "దానితో ప్రయత్నాలు మానుకున్నారా?"
 "అబ్బే! ఎలా మానుకుంటారు. చెట్టుకొమ్మకు పండొకటి వున్నదనుకో!
దానిని కొట్టాలని రాయి విసురుతాం! ఆ రాయి దాని దగ్గరకు చేరుకోబోతూ
క్రిందకు పడిపోతుంది. మళ్ళీ మళ్ళీ ఆ పండుకు తగిలిందాకా రాళ్ళు విసురుతాం
కదా? అలా చంద్రుడు అందిందాకా ప్రయత్నాలు సాగాయి" రఘురాంగారు
కోటి ప్రశ్నకు వివరణ యిచ్చారు.
 రష్యాకు అందిన చందమామ
 "అయితే! ఆ కార్యంలో ముందంజ వేసింది ఎవరు నాన్నా?" కోటి
ఉబలాటంగా అడిగాడు.
 "రష్యా పంపిన చంద్రనౌకలు కొన్ని చంద్రుణ్ని అందుకున్నాయి"
అని రఘురాంగారు అన్నారో లేదో.
 "ఆ చంద్రనౌకల కధ వివరించు నాన్నా?" అని కోటి తొందర చేశాడు.
 "అమెరికావారు పయోనీర్్ నౌకలతో సతమతమౌతున్నప్పుడే! రష్యా
వారు కూడా చంద్రనౌకలతో ప్రయత్నాలు సాగించారు" అని రఘురాంగారు
మొదలు పెట్టారు.
 "రష్యన్్లు మొదట "కాస్మిక్్-రాకెట్్" అనే పేరుతో చంద్రనౌకా
ప్రయోగాలు ప్రారంభించారు. చంద్రుణ్ని చేరుకోవటం "పగటికల" అనే
వుద్దేశ్యంతో కాబోలు ఆ మొదటి దానికి "పగటికల" అనే అర్థం వచ్చే "మెచ్ టా"
అని పేరు పెట్టారు.
 అది 1959 జనవరి 2న అంతరిక్షంలోకి వెళ్ళింది. కాని చంద్రుడు
4600 మైళ్ళు వున్నాడనగానే ఆగిపోయింది. అక్కడికంటే ముందే భూమి
ఆకర్షణ పరిధి ముగిసింది. కాబట్టి అది సూర్యుడికి ఉపగ్రహంగా మారింది. దానిలో
రేడియో పరికరాలు. టెలీమీటరింగ్్ సాధనాలు. అంతరిక్షాన్ని పరిశోధించెందుకు
అవుసరమైన యితర పరికరాలు వుంచారు.
 మళ్ళీ "రెండవ లూనా" ను 1959 సెప్టెంబరు 12న రాకెట్లు అంతరిక్షం
41-
లోకి మోసుకు వెళ్ళాయి. 14వ తేదీన అది చంద్రుణ్ని తాకింది. చివరి అంచె
దానితో వున్న నౌకతో సహా 390 కిలోల బరువు ఉన్నది.
 మనం చంద్రుణ్ని ఎప్పుడూ ఒక వైపునే చూస్తున్నాం! అవతల ప్రక్క
ఏమున్నదో మన కస్సలు తెలియదు. ఆ తెలియని వైపుకు వెళ్ళి అక్కడ చూచిన
దానిని ఫోటోలతో సహా భూమికి పంపించింది. "లూనా-3 చంద్రనౌక" రఘురాం
గారు అనగానే కోటి అందుకున్నాడు.
 "అంటే మూడవ లూనానౌక అంతరిక్షంలో "కొలంబస్్" అన్నమాట.
 "మరి అంతే కదా! అంత వరకూ మానవులకు ఏమీ తెలియని దాన్ని
గురించి తెలియ జెప్పిందంటే ఎంతో గొప్పే కదా!" అని జవాబిచ్చారు రఘురాం
గారు.
 "సరే! దాని విజయం గురించి చెప్పు నాన్నా?" కోటి మళ్ళీ అడిగాడు.
 "చెప్పక తప్పుతుందా? మరి విను." అంటూ రఘురాంగారు "లూనా-3"
గురించి చెప్పటం సాగించారు.
 "చంద్ర మండలం వైపు ప్రయాణిస్తున్న నౌకల పరంపరల్లో `లూనా-3'
ఒకటి. దీన్ని కూడా అంచెల రాకెట్లే మోసుకు వెళ్ళాయి. చివరి అంచె
#రాకెట్్
బరువు 1553 కిలోలు ఉన్నది. దానిలోని నౌక బరువు 278 కిలోలు. నౌకలో
రేడియో వార్తా ప్రసార సాధనాలు, టెలీ మీటరింగ్్, టెలివిజన్్, శాస్త్ర
పరికరాలు, సోలార్్ బ్యాటరీలు ఎన్నో అమర్చారు. భూమిమీద నుండే రేడియో
సంకేతాల ద్వారా దానిని అదుపులో ఉంచ గలిగారు.
 1959 అక్టోబరు 4న దానిని అంతరిక్షంలోకి వదిలారు. అది అలా
చంద్రుడి అవతలి వైపుకు పోయింది. ఆ రోజు 7వ తారీఖు. ఆ రోజే అక్కడి
ఫోటోలు తీసింది. భూమికి ప్రసారం చేసింది."
 "తరువాత లూనా-3 ఏమయ్యింది?"
 "ఏమవుతుంది. అలా చంద్రుడి అవతలివైపు చూచి, చూచింది భూమికి
తెలిపింది, అలా 11 సార్లు చంద్రుడి చుట్టూ తిరిగింది. చివరికి భూవాతావరణంలో
ప్రవేశించి అంతు తెలియకుండా పోయింది." రఘురాంగారు జవాబిచ్చారు.
42-
 "అయ్యొయ్యో! చూడలేని చోటు గురించి తెలియజేసి పాడై పోయిందన్న
మాట." కోటి విచారం తెలియబరిచాడు.
 "ఏమైనాగాని....అది తన కార్యాన్ని తాను అనుకున్నట్లు
#నిర్వర్తించింది.
అందుకనే చరిత్రలో తన పేరు నిలుపుకున్నది." రఘురాంగారు వివరించారు.
 "మరి తరువాత కథ ఏమిటి నాన్నా?" కోటి అడిగాడు.
 "ఇంకొక పనికి లూనాలనే వాడుకున్నారు."
 "ఏం పని అది?"
 "మనం రాయి విసరివేస్తాం! అది ఎక్కడో ఒకచోట పడిపోతుంది. పడ్డచోటు
గట్టిగా ఉండి దెబ్బ బలంగా తగిలితే ఆ రాయి పగిలి ముక్కలౌతుంది. అదే
రాయిని మెల్లగా క్రింది వదిలామనుకో ఏమీ కాకుండా క్రింద పడుతుంది. అంటే
ఏమీ కాకుండా ఉండటానికి జాగ్రత్త పడుతున్నాయన్న మాట. ఇంత వరకూ
చంద్ర నౌకలను విసిరి వేసినట్లు పంపారు. అలా కాకుండా చంద్రుడిమీద
`నెమ్మదిగా' దిగేటట్లు నౌకలను పంపాలనుకున్నారు. దాని కోసం, 4,5,6,7,8
లూనా నౌకలను ప్రయోగించారు. వాటితో చంద్రతలంమీద నౌకలను నెమ్మదిగా
దింపటంలో ఎదురయ్యే చిక్కులను జాగ్రత్తగా గమనించారు." అని రఘురాం
గారు తరువాతి లూనా చంద్ర నౌకలను గురించి చెప్పారు.
 "అయితే! వాటిల్లో ఒక్కటీ తమ కార్యాన్ని నెరవేర్చలేదా?"
 "5వది చంద్రమండలం చేరింది. నౌకలు ఎలా దిగుతాయో తెలియబరచింది.
మిగతావి కూడా ఇంచు మించు అలానే చేసినవనుకోవాలి. 7మాత్రం చంద్ర
మండలాన్ని త్రాకి పాడైపోయింది. 8 వది కూడా అంతే!"
 "అన్నిటి గతి ఒకటే అయితే నెమ్మదిగా దిగిన నౌక ఏది మరి?" కోటి
ఉండబట్టలేక అడిగాడు.
 "ఉండు ఆ విషయానికే వస్తున్నా! తరువాతిది `లూనా-9' ఇది మాత్రం
అనుకున్న కార్యాన్ని చక్కగా నిర్వహించింది. 1966 జనవరి 11న దానిని
ప్రయోగించగా ఫిబ్రవరి 3వ తేదీన చంద్ర తలంమీద నెమ్మదిగా దిగింది. దానిలో
ఉన్న టి.వి. కెమేరాలు చుట్టూ పరికించి చూశాయి. మూడు రకాల కోణాల్లో
చంద్రుణ్ణి ఫోటోలు తీశాయి. ఆ చిత్రాలను భూమికి పంపించాయి. అది ఏడు
43-
సార్లు రేడియో సంకేతాల ద్వారా ఎన్నో శాస్త్ర విషయాలను తెలియజేసింది.
ముందు ముందు చంద్రమండలం చేరుకోవాలనేవారికి ఎన్నో విశేషాలు తెలియ
జేసింది."
 లూనా-9 గురించి రఘురాంగారు వివరించారు.
 "ఇంతటితో లూనాల ప్రయోగాలు అయిపోయాయా?"
 "ఎలా అయిపోతాయి, ముందుకు కొనసాగించారు. కాని ఈసారి దాన్ని
చంద్రుడికీ ఉపగ్రహంగా పంపించారు." రఘురాంగారు చెబుతూండగానే....
 "ఓహో! ఇది చంద్రుడికి మనుషులు నిర్మించి ఇచ్చిన మొదటి ఉపగ్రహం
అన్నమాట...." అని కోటి ఆశ్చర్యంగా అన్నాడు.
 "అన్నమాటేమిటి! సరిగ్గా అంతే! 1966 మార్చి 31న పంపించింది. మే
30వ తేదీవరకూ చంద్రుడి చుట్టూ తిరిగింది. ఎన్నో సార్లు ఎన్నో విశేషాలను
భూమి కేంద్రాలకు ప్రసారం చేసింది."
 అలాగే 11,12 నౌకలు కూడా చంద్రుడికి ఉపగ్రహాలుగా ఎన్నోసార్లు
దానిచుట్టు తిరిగాయి. లూనా-13 ను మాత్రం చంద్రుడిమీద నెమ్మదిగా దింపారు.
ఇది 1966 డిసెంబరు 21న ప్రయోగించబడి 24న చంద్రమండలం పైన దిగింది.
అక్కడి మట్టిని కూడా పరిశీలించి సంకేతాల ద్వారా విశేషాలను భూమికి ప్రసారం
చేసింది. ఇలా రష్యా శాస్త్రజ్ఞులు చంద్ర నౌకలను పంపించారు. అనిచెప్పి
#రఘురాంగారు ఆగారు.
 అమెరికా రేంజర్లు, సర్వేయర్లు
 కాస్సేపు అయిన తరువాత రఘురాంగారు ఏమీ చెప్పకపోయేటప్పటికి
కోటి అడిగాడు.
 "నాన్నా! అమెరికా వారు `పయొనీర్్' నౌకలతో ప్రయత్నాలు చేశారు
కదా! మళ్ళీ వాళ్ళు ప్రయోగాలు కొనసాగించలేదా?"
 "కొనసాగించారు. కాని ఈసారి కొత్త నౌకలతో, కొత్త రాకెట్లతో...."
అన్నారు రఘురాంగారు.
 "అవి ఏమేమిటి?" కోటి ముందు కధకు దారి చూపించాడు.
44-
 "అవే రేంజర్్ నౌకలు. వీటిని `అట్లాస్్-ఎజీనా' అనే రాకెట్లు
#మోసుకు
వెళ్ళాయి. అట్లాస్్ రాకెట్టును ఇంకొక అంచెగా `ఎజీనా' రాకెట్టును ఉంచారు.
అందుకనే దానిని `అట్లాస్్-ఎజీనా' అని అన్నారు.
 అవి చంద్రుణ్ని చేరుకొనేలోపలనే టి.వి కెమేరాలతో ఫోటోలు తీసి
భూమికి ప్రసారంచేసేటట్లు నిర్మించారు. నెమ్మదిగా దిగే విధానం వీటికి
సమకూర్చలేదు. ఒక్కొక్క దాని బరువు 365 కిలోలు. అవి 6 రకాల టి.వి.
కెమేరాలను మోసుకువెళ్ళాయి." అలా 6 నౌకలను పంపారు. కాని అనుకున్న విధంగా
అవి పని చేయ లేక పోయాయి.
 రేంజర్్-7 ను 1964 జులై 28న ప్రయోగించారు.
 అది చంద్రుడి మీద పడిపోయింది. అలా పడిపోయే ముందు ఎన్నో టి.వి.
చిత్రాలను తీసి భూమికి ప్రసారం చేసింది. అట్లాగే "రేంజర్్- 8,9 నౌకలు
కూడా వివిధ ప్రదేశాల చిత్రాలు తీసి భూమి కేంద్రాలకు అందజేశాయి."
 అని రేంజర్లను గురించి చెప్పారు రఘురాంగారు.
 "అయితే! నాన్నా! నెమ్మదిగాదిగే నౌకలను నిర్మించలేదా? వారు." కోటి
అర్థంకానట్లు అడిగాడు.
 "ఎందుకు కాదు. దాని కోసమే "సర్వేయర్్" అనే నౌకలను నిర్మించారు.
వీటికి చంద్రుడి మీద దిగటానికి మూడు కాళ్ళు కూడా అమర్చారు. అన్నారు
రఘురాంగారు.
 "సర్వేయర్లు తమ పనిని చక్కగా చేశాయా?" కోటి సర్వేయర్్ నౌకలను
గురించి అడిగాడు.
 "ఒక్కొక్క సర్వేయర్్ నౌక బరువు 280 కిలోల వరకూ ఉంటుంది. దాని
మార్గాన్ని సరిదిద్దటానికి, చంద్రుడి మీద చక్కగా దిగటానికి "రెట్రో
#రాకెట్లు
అమర్చారు. మొదటి సర్వేయర్్ను 1966 మే 30 న పంపించారు. అది
చంద్రుడ఼ిని చేరుకోబోయే ముందు! రెట్రో రాకెట్లు" పేల్చారు. నెమ్మదిగా చంద్ర తలం
మీద దిగబోయేముందు మళ్ళీ వాటిని ఆపుచేశారు.
 తరువాత అది చంద్రుడి మీద చక్కగా పడిపోయింది. తన చుట్టూ ఉన్న
చంద్రతలాన్ని పరిశీలించి ఎన్నో ఫోటోలు తీసింది.
45-
 ఇలాగే సర్వేయర్్ 2,4 కూడా ప్రయోగించారు. కాని అవి తమ పనిని
కొనసాగించలేక పోయాయి." అని రఘురాంగారు సర్వేయర్ల గురించి
చెప్పారు.
 "మరి తరువాతి వన్నా అనుకున్న పని చేశాయా?"
 "ఇదిగో 2,4 కు మధ్యలో మూడు ఉన్నది చూడు, అది మాత్రం కాస్త
మెరుగు అనిపించింది?"
 "ఎలా మెరుగుగని పించింది?"
 "సర్వేయర్్-3 తో పాటు చంద్రుణ్ని త్రవ్వే పరికరాన్ని పంపారు.
దానిని 1967 ఏప్రియల్్ 17న ప్రయోగించారు. త్రవ్వే పరికరం 20 సెంటీ
మీటర్ల వరకూ త్రవ్వింది. అలా వచ్చిన చంద్రమట్టిని పరిశీలించింది.
 1967 సెప్టెంబరు 8న సర్వేయర్్-6ను పంపించారు. అది నెమ్మదిగా
చంద్రుడి మీద దిగింది. అక్కడ ఉన్న మట్టిని తీసి `రసాయనిక పరీక్షలు'
#చేసింది
దానితో చంద్రుడిలో ఇనుము పాలు ఉన్నదని తెలిసింది.
 సర్వేయర్్-6 కూడా అక్కడి మట్టిని రసాయనికంగా పరీక్షించింది. దీనిలో
ఉన్న వెర్నియర్్ రాకెట్్ ఇంజన్ల ద్వారా దిగిన చోటికంటే 4 మీటర్లు
#అవతలికి
జరిపారు. అక్కడి నుండీ మళ్ళీ టి.వి. కెమేరాలు పని చేసేలాచూశారు.
 1968 జనవరి 7న "సర్వేయర్్-7 ను" పంపించారు. చంద్రతలం
ఫోటోలను అది పంపించింది. అంతేకాకుండా మట్టిని పరిశీలించి దానిలో ఉన్న
గుణాలను తెలియజెప్పింది.
 ఇలా ఇంకా ఎన్నో నౌకలు మనుషులు చంద్రమండలంమీద దిగకముందు
చంద్రుణ్ణి పరిశీలించాయి. దూర సీమలకు దారిని చూపాయి. ఇదంతా రాకెట్ల
ద్వారానే సాధ్యపడింది. అని అంతటితో ఆ కథ ముగిసింది అన్నట్లు చెప్పారు.
 "అదిసరేగాని నాన్నా! ఇందాక అక్కడక్కడా "రెట్రో రాకెట్ల" నీ,
#వెర్నియర్్ రాకెట్లనీ చెప్పారు. అవి ఎటువంటివి ఏమిటి?" అని కోటి అర్థంకాని
విషయం గురించి అడిగాడు.
 "ఆం! ఇవి చంద్ర నౌకలకుగాని, పెద్ద పెద్ద రాకెట్లకుగాని అమర్చిన
#చిన్న
రాకెట్లన్న మాట....చంద్ర తలంమీద చంద్రనౌక నెమ్మది" గా దిగాలనుకో నౌక
46-
పరుగెత్తుతూ ఉంటుందిగదా! ఆ పరుగుకు అడ్డుకట్టు వేయాలి. అందుకని "రెట్రో
రాకెట్ల" ను దానికి అమర్చుతారు. నౌక చంద్రుణ్ణి సమీపించగానే సంకేతాలద్వారా
వాటిని ప్రేల్చుతారు. అవి నౌక పరుగును ఆపి చక్కగా దిగేటట్లు చేస్తాయి.
 అలాగే అంతరిక్షంలో రాకెట్లుగాని, అంతరిక్ష నౌకలుగాని పరిగెత్తుతూ
ఉంటాయి. ఒక్కోసారి వాటి ప్రయాణ మార్గం తప్పిపోయి వేరే దిక్కుకు మార
వచ్చు. అటువంటి సమయాలలో "వెర్నియర్్ రాకెట్లు" ఉపయోగపడతాయి.
మారిపోబోతున్న దానిని గమనాన్ని సరిదిద్దుతాయి. రఘురాంగారు వివరించారు.
 "ఓహో! భలే బాగాఉంది. సాంకేతిక పరిజ్ఞానం ఎన్ని విచిత్రాలను
#సాధించింది. ఒకటి నౌకను సరైన దారినే నడిపిస్తుంది ఇంకొకటి నౌక వేగాన్ని
#తగ్గిస్తుంది." అని సంతోషంగా అన్నాడు కోటి.
 7
 రాబోయే రాకెట్లు
 "అబ్బా! రబ్బరు బుడగ వెనుక ఎంతకధ నడిచింది నాన్నా! ఇకను అయి
పోయినట్లేనా?" కోటి అలసిపోయి విసుగుపడినట్లు అడిగాడు.
 రఘురాంగారు అందుకొని "మరి మనం ఇప్పటివరకూ చెప్పుకున్న రాకెట్ల
వేగం గంటకు 25,000 మైళ్ళ పైన అనుకో! మరి శాస్త్రజ్ఞులు ఇంత వేగంతోటే
తృప్తిపడ్డారనుకొంటున్నావా?" అని కోటిని ఎదురు ప్రశ్నించారు.
 "కాక మరి ఇంకా వేగంగాపోయే రాకెట్ల గురించి ఆలోచిస్తున్నారన్న
మాట?" కోటి ఆశ్చర్యంగా అన్నాడు.
 "ఇప్పటి రాకెట్లతో చంద్రుణ్ణి చేరుకోవాలంటే 3.4 రోజులు పడుతున్నది.
మనకు దగ్గరలో ఉన్న గ్రహాల దగ్గరకు వెళ్ళాలంటే నెలల పర్యంతం ప్రయాణం
చేయవలసి ఉంటుంది. దీనికంటే ముందుగా చేరుకొనేటట్లు వేగంతో పోయే
రాకెట్లు నిర్మించలేమా? అని శాస్త్రజ్ఞులు ఆలోచించారు" అని చెబుతూండగానే
కోటి మధ్యలో కలిగించుకొని.
47-
 "ఆలోచించి కొన్నిటిని కనిపెట్టి ఉండవచ్చు. అవి ఏమిటో
#చెప్పునాన్నా?"
అని తొందర చేశాడు.
 కోటి అడిగినదానికి ఆయనిలా చెప్పటం సాగించారు.
 ఇప్పటిదాకా మనం చెప్పుకొన్న రాకెట్లు రసాయనిక ఇంధనాలతో
నడిచేవి. ఇప్పుడు చెప్పుకోబోయేవి వాటికంటే భిన్నమైనవి.
 ప్రస్తుతం "న్యూక్లియర్్ రాకెట్, అయోన్్ రాకెట్్, ఆర్క్్జెట్్
#రాకెట్్,
ఎలక్ట్రో మేగ్నటిక్్ రాకెట్్, ఫోటాన్్ రాకెట్్ అని పలురకాల రాకెట్్
#ఇంజన్ల
గురించి ఆలోచించి వేగంగా పోవడానికి శాస్త్రజ్ఞులు సూచించారు.
 "ఆం! హో! ఇవి కొత్త పేర్లుగా ఉన్నాయే! వివరంగా వాటి గురించి
#చెప్పు
నాన్నా? కోటి అడిగాడు.
 ఆ తరువాత రఘురాంగారు కొంచెంసేపు ఆలోచించి కథనం
ప్రారంభించారు.
 న్యూక్లియర్్ (నెర్వా) రాకెట్్
 "ఈ శతాబ్దంలో శాస్త్రజ్ఞులు కనిపెట్టిన వాటిల్లో మహాశక్తివంతమైనది
ఏది?" కోటిని అడిగారు రఘురాంగారు.
 "విద్యుచ్ఛక్తి" అని వెంటనే జవాబు ఇచ్చాడు కోటి.
 "కాదు. అంతకంటే శక్తివంతమైనది. `పరమాణు శక్తి' దీనిని
రకరకాలుగా ఉపయోగించటం శాస్త్రజ్ఞులు నేర్చుకున్నారు. నేర్చుకొని వినియోగంలోకి
తెస్తున్నారు కూడా!" ఇటువంటి శక్తిని రాకెట్లకు ఉపయోగించు కోవచ్చునా?
అని కొందరు శాస్త్రవేత్తలు ఆలోచించారు. ఆ ఆలోచనమీద 1960 లో
పరిశోధనలు ప్రారంభించారు. ఈ విధమైన రాకెట్్ యంత్రాలను "నెర్వా" అన్నారు
అంటే "రాకెట్్ వాహనాన్నికి న్యూక్లియర్్ యంత్రం! (న్యూక్లియర్్ ఇంజన్్
పర్్ రాకెట్్ వెహికల్్ అప్లికేషన్్) అనే అర్థం వస్తుంది.
48-
 1966 మార్చి 3వ తేదీన అమెరికాలో తయారుచేసి ప్రయోగించిన
న్యూక్లియర్్ యంత్రం మంచి ఫలితాన్ని ఇచ్చింది." అని చెప్పారు రఘురాంగారు..
 "ఈ యంత్ర నిర్మాణ విధానం. అవి పనిజేసే తీరు వివరించి చెప్పు
నాన్నా?" కోటి మధ్యలో అడిగాడు.
 కోటి ప్రశ్నకు రఘురాంగారు ఇలా జవాబు చెప్పటం సాగించారు.
 "హైడ్రోజన్్ ద్రవాన్ని ఒక అరలో నింపుతారు. దాని క్రింది భాగంలో
"అణురియాక్టరు" వుంటుంది. ఆ రియాక్టరు చుట్టూ ఒక వలయం ఏర్పాటు
చేస్తారు.
 ఈ వలియంలోకి హైడ్రోజన్్ ద్రవం రాగానే అది వేడి చేయబడి
వాయువుగా మారుతుంది. ఆ హైడ్రోజన్్ వాయువును అణురియాక్టరులోని రంధ్రాల
గుండా ప్రవహింపజేసి అక్కడ మళ్ళీ దానిని వేడి చేస్తారు. అక్కడ అది అమిత
వేడి వాయువుగా రూపొందుతుంది. తరువాత అడుగు భాగంలో ఉన్న "నాజిలు"
ద్వారా వేగంగా బయటికిపోతుంది. కాని దీనిని క్రింది వాతావరణంలో
వుపయోగించటానికి వీలుండదు. గాలిలోని ప్రాణ వాయువు వలన హైడ్రోజను మండి
పోతుంది. అందుకని గాలిలేని అంతరిక్షంలో మాత్రమే ప్రయోగిస్తారు.
 "అంటే ఏ శాటరన్్ రాకెట్టులోనో, లేక వోస్టాక్్ రాకెట్టులోనో చివరి
#అంచెగా ఈ న్యూక్లియర్్ యంత్రాన్ని ఉపయోగిస్తారన్న మాట." కోటి అన్నాడు.
 "సరిగ్గా! అంచే చేస్తారు. ఈ న్యూక్లియర్్ రాకెట్టే గాకుండా,
#ఇప్పుడు
మనం చెప్పుకోబోయే ఇతర రాకెట్లను గూడా ఇదే విధంగా మూడో అంచెగానో
నాలుగో అంచెగానో....లేక అంతరిక్షంలోకి ప్రయోగశాల
నుండో....ఉపయోగిస్తారు." అని రఘురాంగారు వివరించారు.
49-
 "మరి నాన్నా! న్యూక్లియర్్ యంత్రంతో అంతరిక్ష నౌకలను ఏలా
నడుపుతారు?" అని కోటి మళ్ళీ అడిగాడు.
 "మంచి ప్రశ్నే! న్యూక్లియర్్ యంత్రం ఉంటుంది గదా! దానితో
#రాకెట్్ను
కూరుస్తారు. ఆ రాకెట్్కు వెనుక పెద్ద పొడవాటి గట్టి కడ్డీలను అమరుస్తారు.#ఆ
కడ్డీలకు మనుష్యులు ఉండే నౌకలాంటి దానిని కడతారు.
 ఇదంతా ఒక గొప్ప ప్రయోగం. అని జవాబుచెప్పారు.
 "తరువాతి రాకెట్్ గురించి చెప్పునాన్నా!" అని అడిగిన కోటికి ఆయన
"ఆర్క్్జెట్్ రాకెట్్" గురించి చెప్పటం సాగించారు.
 ఆర్క్్జెట్్ రాకెట్్
 "మనం పైన చెప్పుకున్న వాటిల్లో ఇంకోకటి "ఆర్క్్ జెట్్ రాకెట్్.
#దీనికి
ఉండే యంత్రాన్నే "ఎలక్ట్రో థెర్మల్్ రాకెట్్ యంత్రం" అంటారు. హైడ్రోజన్్
ద్రవాన్ని గాని. మరి ఏ ద్రవాన్నయినా గాని పంపుల ద్వారా అంతర్దహాన గది
లోనికి పంపుతారు. అక్కడ దానిని విద్యుచ్ఛక్తితో వేడి చేస్తారు. అట్లా వేడి
చేసి నప్పుడు 50,000 డిగ్రీల వరకూ ఉష్ణం ఉత్పన్న మవుతుంది. మామూలుగా
రాకెట్టు యంత్రాల్లో కలిగే వేడికంటే ఇది పది రెట్లు ఎక్కువ.
 ఇట్లా అధిక ఉష్ణోగ్రతలో వేడి చేసినప్పుడు
ఏమవుతుందో తెలుసా?" అని కోటిని అడిగారు రఘురాంగారు,
కోటి "తెలియద"న్నట్లు మొఖం పెట్టాడు.
 "అయితే! నేనే చెబుతా విను." అంటూ రఘురాంగారు
మిగతా కథ చెప్పటం సాగించారు.
 "అధిక ఉష్ణోగ్రతలో వేడి చేసినప్పుడు వాయువులు
"ప్లాస్మా" గారూపొందుతాయి. ఈ ప్లాస్మా స్థితిలో వాయువుల
50-
అణువులు చాలా వేగంగా ప్రయాణిస్తాయి. అందు వల్ల ఎక్కువ వేగం
రాకెటుకు లభిస్తుంది. ఇంధనాన్ని ప్లాస్మాస్థితి కీ మార్చే రాకెటు
#యంత్రాన్నే
"ఆర్క్్జెటు యంత్రం" అంటారు. దీనితోనడిచేది ఆర్క్్జెటు రాకెట్టు."
 "అబ్బా! ఈ యంత్రం వాడిన రాకెట్్వేగం ఊహింపరాని కనుకుంటాను."
కోటి ఆశ్చర్యంగా అన్నాడు.
 "ఈ యంత్రం వల్ల వచ్చే వేగం చాలా ఎక్కువే! అయినా దీనిలో ఇంకో
చిక్కు ఉన్నది."
 "ఏమిటంట ఆ చిక్కు?" పూర్తిగా చెప్పక ముందే కోటి అడిగాడు.
 "దీనితో పాటు విద్యుచ్ఛక్తిని ఉత్పత్తి చేసే యంత్రాన్ని తీసుకు
పోవాల్సి ఉంటుంది. ఈ పరికరం బరువు ఎక్కువగా ఉంటుంది. ఈ పరికరం
బరువు ఎక్కువగా ఉంటుంది కదా! అందుకని ఈ రాకెట్లనిర్మాణం కాస్త సాధ్యం
కాకుండా పోతున్నది." అని రఘురాంగారు కోటి ప్రశ్నకు జవాబిచ్చారు.
 "ఎప్పటి కయినా పరిశోధనాల ద్వారా విధానం సులభం చేయ లేక
పోతారా? శాస్త్ర వేత్తలు." అని కోటి అన్నాడు.
 "ఒక విధంగా "విద్యుచ్ఛక్తి ఉత్పాదన" కేంద్రంబదులు "అణుశక్తి
రియాక్టరు" ను ఉపయోగించి దాని బరువు తగ్గుతుందేమోనని ప్రయత్నాలు
చేస్తున్నారు శాస్త్రవేత్తలు" అని భవిష్యత్తులో రాబోయే వాటిని గురించి చూశాయగా
చేప్పారు.
 అయోన్్ రాకెట్్ యంత్రం
 "సరే! ఇంకొక రాకెట్్ యంత్రం గురించి చెప్పు నాన్నా?" కోటి ముందు
కథకు దారి చూపించాడు.
51-
 "ఇకను తరువాత చెప్పుకోవాల్సింది "అమోన్్ రాకెట్్ యంత్రం"
అయోనులంటే సూక్ష్మమైన అణువులు సీజియం, సోడియం మొదలైన
పదార్థాలు వున్నాయి కదా! వాటిని వేడిచేసి విడగొట్టినప్పుడు ఏర్పడతాయి.
యిక్కడ వేడిచేయటం అంటే ఊహింపరానంత ఉష్ణానికి గురిచేయటమే!
 ప్రతి అణువుకూ ఒక కేంద్రం వుంటుంది. ఆ కేంద్రంలో "ధన"
విద్యుత్కణాలు వుంటాయి. వీటి చుట్టూ "ఋణ" విద్యుత్కణాలయిన "ఎలక్ట్రానులు"
తిరుగుతూ వుంటాయి. ఇవి ఎప్పుడూ అయానులను అంటి పెట్టికొని వుంటాయి.
కాబట్టి వీటిని అయానుల నుండి విడదీస్తే ధన విద్యుత఼్్ ఆవేశమే వుంటుంది.
గనుక వాటిని అమితవేగంగా ఇష్టం వచ్చిన దిక్కుకు నడిపించవచ్చు. అ఩ి
#శాస్త్రజ్ఞులు కనుక్కొన్నారు." అని రఘురాంగారు చెబుతూ వుండగానే.
 "రాకెట్్ యంత్రంలో ఈ చర్య అంతా ఎలా జరుగుతుందో చెప్పు నాన్నా?"
అని కోటి వివరం అడిగాడు.
 కోటి ప్రశ్న విన్న రఘురాంగారు మళ్ళీ చెప్పటం సాగించారు.
 "మూలకాలలో తేలికైనది. త్వరగా కాలి "అయోను" లుగా మారేదీ
"సీజియం", ఇది 81.5 ఫారన్్హీట్్ డిగ్రీలలో కరుగుతుంది. 1238 ఫారన్్హీట్్
డిగ్రీల్లో ద్రవరూపంలోకి మారుతుంది. ఈ సీజియం ద్రవాన్ని "బాయిలర్్" లోకి
ప్రవహింపజేస్తారు. అక్కడ అది వాయు రూపాన్ని పొందుతుంది. ఆ వాయువును
"టంగ్ట్య" నుతో చేసిన చట్రం గుండా ప్రవహింపజేసి దానిని మళ్ళీ అధిక
#ఉష్ణోగ్రతకు గురిచేస్తారు.
 అప్పుడు అది అణువులుగా రూపొందుతుంది. అక్కడ మళ్లీ
ఎలక్ట్రాను
52-
లను కోల్పోయిన అయాను అణువులు సెకండుకు 10,00,000 అడుగుల (షుమారు
560 మైళ్ళు) వేగంతో ప్రయాణిస్తాయి.
 "దీనిలో విద్యుత్్ ఆవేశాన్ని ఎలా సృష్టించాలి? దానికి అవుసరమైన
విద్యుచ్ఛక్తిని ఎలా సృష్టించాలి? అనే ప్రధానమైన సమస్యలను శాస్త్రవేత్తలు
ప్రయోగాలద్వారా తెలుసుకుంటున్నారు. దీనినే "ఎలక్ట్రొస్టాటిక్్ రాకెట్్
#యంత్రం
అని గూడా అంటారు."
 వివరణతో తృప్తిపడ్డ కోటి "అబ్బా! సెకండుకు 560 మైళ్ళ వేగమంటే
ఊహింపలేనంత వేగం అన్నమాట. యిది యెక్కడున్నా వుపయోగింపబడిందా?
నాన్నా:" అని అడిగాడు.
 "1964 లో రష్యన్్ శాస్త్రజ్ఞులు `వోస్ఖోడ్్' అనే అంతరిక్షనౌకలను
#ప్రయోగించారు. వాటిల్లో ఒక దానిలో ఈ యంత్రాన్ని అమర్చి అది బూమి కక్ష్యలో
తిరిగేటప్పుడ఼ు ప్రయోగించి చూశారు' అని చెప్పారు కోటి ప్రశ్నకు జవాబుగా
రఘురాంగారు.
 "నిజంగా దీనివేగాన్ని చూస్తుంటే ముందుపోయేటట్లు ఉన్నది"
అన్నాడు కోటి.
 "ఊహకంటే ముందు పోదుగాని దానితోపాటే అనుకో!" అన్నారు
రఘురాంగారు.
 ఎలక్ట్రో మేగ్నటిక్్ రాకెట్్ యంత్రం
 "తరువాతి యంత్రం ఏమిటీ?" కోటి దీర్ఘం తీశాడు.
53-
 ఇంకొకటి "ఎలక్ట్రో మేగ్నటిక్్ రాకెట్్ యంత్రం" అని తరువాతి
#దానిని
గురించి ప్రస్తావించారు.
 "ఇంతకుముందు మనం వాయువును "ప్లాస్మా" గా మార్చే "ఆర్్కు జెట్్
రాకెట్్ యంత్రం" గురించి తెలుసుకున్నాం! అట్లా ప్లాస్మాగా మారిన దానిని
విద్యుదావేశం ఉన్న అయస్కాంత క్షేత్రం గుండా పంపించి అణువుల క్రింద
విడగొడతారు. దీనితో సెకండుకు వందలకొద్దీ మైళ్ళ వేగం లభిస్తుంది.
 ఈ విధానాన్ని మొదట ప్రయోగాత్మకంగా రష్యన్్లు "జోండ్్" అనే
స్వయం చలిత అంతరిక్ష నౌకలో వాడి చూశారు.
 "ఆ విషయం సరిగ్గా చెప్పునాన్నా?" కోటి
ఉత్సాహంతో అడిగాడు.
 "అంగారక గ్రహం దూరంగా ఉన్నది. దానిని
చేరుకొనటానికి "జోండ్్" అనే స్వయం చలిత అంతరిక్ష
నౌకలను రష్యావారు తయారుచేశారు. వాటిల్లో రెండవ దానిని
1964 నవంబరు 30న అంగారక గ్రహానికి పంపారు. దానిని
అదుపులో ఉంచే విధానంలో దాదాపు ఆరు ఎలక్ట్రో
మేగ్నటిక్్ రాకెట్్ యంత్రాలను ఉపయోగించారు" అని కోటి
ప్రశ్నకు జవాబు ఇచ్చారు.
 ఫోటాన్్ రాకెట్్
 రఘురాంగారు చెప్పిన జవాబుకు కోటి సంతృప్తి పడ్డాడు.
 "నాన్నా! చివరగా ఇంకొకటి మిగిలినట్లున్నది అనుకుంటాను. ఏమిటది
ఫోటాన్్ రాకెట్టా? అవునుకదా?" అని మళ్ళీ అడిగాడు కోటి.
54-
 "అవును ఫోటాను రాకెట్టే!" అని చెప్పారు.
 "అయితే దాని సంగతి ఏమిటి?"
 "చెబుతా విను" అంటూ ఫోటాన్్ రాకెట్్ గురించి మొదలుపెట్టారు
రఘురాంగారు.
 "మనకు దగ్గరలో గ్రహాలు ఉన్నాయి. గ్రహాలు దాటి వెళితే నక్షత్ర
లోకాలు ఉన్నాయి. మనకు దగ్గరలో ఉన్న నక్షత్రం `ప్రాక్టిమా సెంటారి' అనేది
అది 4.3 కాంతి సంవత్సరాల దూరంలో ఉన్నది. కాంతి సెకండుకు 1,86,000
మైళ్ళ వేగంతో ప్రయాణిస్తుంది. అలా దానిని సంవత్సరంలో ఎంతదూరం
ప్రయాణిస్తుందో లెక్కగట్టాలి. దానిని 4 సంవత్సరాల మూడు నెలలతో
వెచ్చించాలి. అది ఊహించలేనంత దూరం. అంత దూరాన్ని కాంతి వేగంతో
ప్రయాణించే రాకెట్లో ప్రయాణించారనుకో! అప్పుడ఼ు అక్కడికి వెళ్ళి రావటానికి
8 1/2 కాంతి సంవత్సరాలు పడుతుంది."
 "అంటే అలా కాంతి వేగంతో ప్రయాణించ గల రాకెట్టే `ఫోటాన్్ రాకెట్టు
అన్నమాట." రఘురాంగారి వివరణ విని కోటి అడిగాడు.
 "అవును, దానినే "ఫోటాను రాకెట్టు" అంటారు. ఫోటానులంటే కాంతి
లాంటి శక్తి పూరిత రేణువులు. వీటినే "లేసర్్ కిరణా" లని గూడా అంటారు.
వీటిని పరిశోధనల ద్వారా శాస్త్రజ్ఞులు కనిపెట్టారు. వీటికి
#"త్రోపుడు శక్తి"
కూడా వుంటుంది. వేగం కాంతి వేగంతో సమానంగా ఉంటుంది. అందువల్ల
వీటిని రాకెట్లకు శక్తిని ఇచ్చే విధంగా మలచుకో వచ్చునా? అని
#శాస్త్రజ్ఞులు
తీవ్రంగా ఆలోచిస్తున్నారు."
55-
 "ఈ రాకెట్లే గనుక తయారై అంతరిక్ష యాత్రకు ఉపయోగిస్తే మనం
హైద్రాబాదు నుండి విజయవాడ వెళ్ళి వచ్చినట్లు చంద్రమండలానికి పోయి
రావచ్చు అన్నమాట." కోటి ఆశ్చర్యంగా అన్నాడు.
 "అంత వేగంగా సాధించవచ్చు. కాని దానితో ఎన్నో చిక్కులు కూడా
ఉన్నాయి." అని రఘురాంగారు అన్నారో లేదో.... కోటి అందుకున్నాడు.
 "అబ్బా! చిక్కులు లేకుండా దూర ప్రయాణం త్వరలో ఎలా అవుతుంది
కాని. ఆ చిక్కులు ఏమిటో చెప్పునాన్నా? అని అడిగాడు.
 "కాంతి వేగంతో ప్రయాణం చేయటానికి ఫోటాన్్ రాకెట్టును తయారు
చేస్తారనుకో! అంత వేగానికి మానవ శరీరాలు అలవాటు పడాలి. అదీగాక అది
పూర్తి వేగం పొందేందుకు "ఏక్సిలరేట్్" కావటానికే మూడు సంవత్సరాల
వరకూ పట్టవచ్చు. ప్రయాణం అయిపోయి మళ్ళీ వేగం తగ్గటానికి అంతే కాలం
పడుతుంది. అటువంటి ప్రయాణం చేయటానికి 10,12 సంవత్సరాల కాలం
పడుతుంది. మరి అంత కాలం అంతరిక్షంలో మనిషి ఉండాలంటే సాధ్యమయ్యే
పని కాదు." అని కోటికి చిక్కుల గురించి వివరించారు.
 "ఆ రోజు వస్తుందంటావా? నాన్నా!" కోటి ఆశగా అడిగాడు.
 "ఏమో భవిష్యత్తులో రావచ్చునేమో? ఎవరు చేశారు. మనం
1940-50 ల మధ్య రాకెట్లు ఆకాశం అంచులకు ఎగరటం
చెప్పుకున్నాం! 1950-60 మధ్య అంతర్గ్రహ క్షేత్రంలో ప్రయాణించటం
విన్నాం! 1960-70 మధ్య చంద్రమండలానికీ, ఇతర గ్రహాల
దగ్గరకూ వేగంగా ప్రయాణించటం చూశాము. 1970 నుండి ఇప్పటి
వరకూ ఎన్నో అద్భుతాలు సాధించటం తెలుసుకున్నాము.
 రాబోయే కాలం వాళ్ళు గ్రహాల దగ్గరకూ, నక్షత్రాల
దగ్గXBdO XvUAL XBKFlb hOIWqKBrZu? మరి మానవ మేధస్సుకు అంతం
అనేది ఎక్కడ? అని చెప్పగానే కోటి.
 "మరి అందుకే గద నాన్నా ఈ శతాబ్దం ప్రారంభంలో
56-
 "భూమి ఉయ్యాల లాంటిది. ఈ ఉయ్యాలతో పసిపిల్ల వాడిలా మానవులు
ఎంత కాలం ఉండగలడు" అని జియాల్్ కోవస్కి అన్నది" అని గుర్తుచేశాడు.
 "ప్రతి సంపన్న దేశమూ శాంతి యుతంగా పరిశీలనలు ప్రయత్నాలు చేసే
ఈ విజ్ఞానాన్ని ప్రతి వారికీ అందిస్తాయనే శుభముహూర్తం కోసం మనం ఎదురు
చూద్దా! ప్రస్తుతం మనం చెప్పుకున్న రాకెట్లు భవిష్యత్ మానవులు తమ మేధా
శక్తితో సృష్టించి నక్షత్ర మండల యాత్రలు సాగిస్తారని ఆశిద్దాం!"
 అని రఘురాంగారు కథనం ముగించారు.
 ----------
               0 
</p></body></text></cesDoc>