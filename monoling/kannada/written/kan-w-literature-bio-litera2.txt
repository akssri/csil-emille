<cesDoc id="kan-w-literature-bio-litera2" lang="kan">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kan-w-literature-bio-litera2.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-05-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ಡಾ. ಅಂಬೇಡ್ಕರ್</h.title>
<h.author>ಡಾ।। ಮ.ನ. ಜವರಯ್ಯ</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">litera2</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 5.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-05-21</date></creation>
<langUsage>Kannada</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>
-84-
ಪ್ರಬಂಧದಲ್ಲಿ ಶ್ರೀಯುತ ಭಂಡಾರ್್ಕರ್ ಹೇಳಿರುವಂತೆ `ವಿದೇಶಿಯರ ರಕ್ತ ಬೆರೆಯದೇ
ಇರುವ ಒಂದೇ ಒಂದು ವರ್ಗವಾಗಲೀ ಜಾತಿಯಾಗಲೀ ಭಾರತದಲ್ಲಿ ಇಲ್ಲವೇ ಇಲ್ಲ.
ವಿದೇಶಿಯರ ರಕ್ತ ಮಿಶ್ರವಾಗಿರುವುದು ಕೇವಲ ಕ್ಷತ್ರಿಯರಾದ ರಜಪೂತರು ಮತ್ತು
ಮರಾಠರಲ್ಲಿ ಮಾತ್ರವಲ್ಲ, `ನಮಗೆ ಯಾವ ವಿದೇಶಿಯರ ರಕ಼್ತವೂ ಬೆರೆತಿಲ್ಲ'ವೆಂದು
ಜಂಬ ಕೊಚ್ಚುವ `ಬ್ರಾಹ್ಮಣರಲ್ಲೇ ವಿದೇಶಿಯರ ರಕ್ತ ವಿಶೇಷವಾಗಿ ಬೆರೆತಿರುವುದು.
ಅಂದ ಮೇಲೆ ರಕ್ತ ಮಿಶ್ರಣವನ್ನು ತಡೆಗಟ್ಟುವುದಾಗಲೀ, ಅಥವಾ ಶುದ್ಧ ರಕ್ತವನ್ನು
ಕಾಪಾಡುವುದಾಗಲೀ ಜಾತಿಪದ್ಧತಿಯಿಂದ ಸಾಧ್ಯವೇ ಇಲ್ಲವೆಂಬುವರಲ್ಲಿ ವಿವಾದಕ್ಕೆ
ದಾರಿಯೇ ಇಲ್ಲ. ರಕ್ತ ಸಂಬಂಧ ಹಾಗೂ ಸಂಸ್ಕೃತಿಯಲ್ಲಿ ಪ್ರಪಂಚದ ಅನೇಕ ಪಂಗಡಗಳು
ಬೆರೆತುಹೋದ ಎಷ್ಟೊ ಶತಮಾನಗಳು ಉರುಳಿಹೋದ ನಂತರವೇ ಈ ಜಾತಿ ಪದ್ಧತಿ
ಅಸ್ತಿತ್ತವಕ್ಕೆ ಬಂದದ್ದು. ವಿಚಿತ್ರವಾದ ಅನೇಕ ಪಂಗಡ ಒಳಪಂಗಡಗಳನ್ನು ಹೊಂದಿದ್ದು
ಇದರಿಂದ ಇಡೀ ಸಮಾಜ ಹರಿದು ಹಂಚಿ ಹೋಗಿದ್ದರೂ ಅದನ್ನು ಉಳಿಸಿಕೊಂಡು ಅದರ
ಉಪಯೋಗವನ್ನು ಇನ್ನಷ್ಟು ಇನ್ನಷ್ಟು ಹಿಂಡಿಕೊಳ್ಳುವ ಸಲುವಾಗಿ ಅದಕ್ಕೆ ವಱ್ಣವಿಭಜನೆ~
ಯೆಂದು ಹೆಸರಿಟ್ಟು ಮೈಸವರುತ್ತ ಕರೆದುಕೊಂಡು ಬಂದಿದ್ದಾರೆ. ಒಬ್ಬ ಪಂಜಾಬಿ
ಬ್ರಾಹ್ಮಣನಿಗೂ ಒಬ್ಬ ಬಂಗಾಳದ ಅಸ್ಪಶ್ಯನಿಗೂ ಅಥಾವಾ ಒಬ್ಬ ಮದರಾಸಿ ಅಸ್ಪೃಶ್ಯ~
ನಿಗೂ ಬುಡಕಟ್ಟಿನ ಸಾಮ್ಯವೇನು? ಅಥವಾ ಒಬ್ಬ ಪಂಜಾಬಿ ಬ್ರಾಹ್ಮಣನಿಗೂ ಅಲ್ಲಿನ
ಒಬ್ಬ ಚಮ್ಮಾರನಿಗೂ ಬುಡಕಟ್ಟಿನ ವ್ಯತ್ಯಾಸವೇನು? ಅಥವಾ ಅದೇ ಓರ್ವ ಮದರಾಸಿ
ಬ್ರಾಹ್ಮಣನಿಗೂ ಅಲ್ಲಿನ ಓರ್ವ ಪರೆಯಾನಿಗೂ ಬುಡಕಟ್ಟಿನ ವ್ಯತ್ಯಾಸವೇನು? ಅಂದರೆ
ಇಂದಿನ ಜಾತಿಪದ್ಧತಿಯ ವ್ಯತ್ಯಾಸವನ್ನು ಬುಡಕಟ್ಟುಗಳಲ್ಲಿ ಹುಡುಕಲು ಸಾಧ್ಯವೇ ಇಲ್ಲ.
ಪಂಜಾಬಿನ ಬ್ರಾಹ್ಮಣ-ಚಮ್ಮಾರರ ಬುಡಕಟ್ಟಿನಲ್ಲೂ, ಹಾಗೆಯೇ ಮದರಾಸಿನ
ಬ್ರಾಹ್ಮಣ-ಪರೆಯಾರ ಬುಡಕಟ್ಟಿನಲ್ಲೂ ವ್ಯತ್ಯಾಸವೇನೂ ಇಲ್ಲ. ಈ ಎಲ್ಲ ಪಂಗಡಗಳೂ
ಒಂದೇ ಸ್ಟಾಕಿನ ಪ್ರಕಾರಗಳು, ಅಷ್ಟೇ. ಅಂತೆಯೇ ಜಾತಿಪದ್ಧತಿ ಬುಡಕಟ್ಟಿನ ಆಧಾರದ
ಮೇಲೆ ನಿಂತಿಲ್ಲವೆಂಬುದು ಸ್ಪಷ್ಟವಾಯಿತು. ಇಂದಿನ ಜಾತಿಪದ್ಧತಿ ಒಂದೇ ಬುಡಕಟ್ಟಿನ
ಜನಾಂಗವನ್ನು ಸಾಮಾಜಿಕವಾಗಿ ವಿವಿಧ ಪಂಗಡಗಳಾಗಿ ವಿಂಗಡಿಸಿರುವ ಒಂದು ಕುತಂತ್ರ
ಅಷ್ಟೆ. ಈ ಕುತಂತ್ರ ಸಾಧನೆಗಾಗಿಯೇ ಬಹು ಮಂದಿ ಇಂದೀಗೂ ಟೊಂಕ ಕಟ್ಟಿ ನಿಂತಿರು~
ವುದು.</p>

<p>ಬುದ್ಧಿಗೆ ಹಿಡಿದ ಭೂತ</p>

<p>ಇದನ್ನು ಒಪ್ಪಿಕೊಂಡಾಗ ಭಾರತದಲ್ಲಿ ರಕ್ತ ಮಿಶ್ರವಾಗಿರುವ ಬುಡಕಟ್ಟುಗಳ ಪರಸ್ಪರ
ಅಂತರಜಾತಿ ಮದುವೆಯಾಗುವುದರಿಂದ ತೊಂದರೆಯೇನು? ಅನೇಕ ವಿಶಿಷ್ಟ ಲಕ್ಷಣ~
ಗಳಿಂದಾಗಿ ಮನುಷ್ಯ ಪ್ರಾಣಿಗಳಿಗಿಂತ ಭಿನ್ನವಾಗಿರುವನೆಂದು ವಿಜ್ಞಾನ ಹೇಳಿದಾಗ
ಅದನ್ನು ಯಾರೂ ಒಪ್ಪದೇ ಇರುವುದಿಲ್ಲ. ಆದರೂ ಬುಡಕಟ್ಟುಗಳ ಶುದ್ಧಿತ್ವವನ್ನು
ಇಂದಿಗೂ ನಂಬುವ ವಿಜ್ಞಾನಿಗಳೂ ಕೂಡ ವಿವಿಧ ಬುಕಟ್ಟುಗಳ ವಿವಿಧ ರೀತಿಯ
-85-
ಮನುಷ್ಯರಿಂದ ತಂತಾನೇ ಆಗಿವೆಯೆಂಬುದು ನಿಜವಾದರೂ ಖಂಡಿತವಾಗಿ ಅದನ್ನವರು
ಪ್ರತಿಪಾದಿಸಲು ಇಚ್ಛಿಸುವುದಿಲ್ಲ. ನ್ಯಾಯವಾಗಿ ಆ ವಿವಿಧ ಬುಡುಕಟ್ಟುಗಳು ಒಂದೇ
ಬುಡದ ಅನೇಕಾರು ಅಂಗಗಳೆಂಬುದು ತಿಳಿದೇ ಇದೆ. ಹೀಗಿರುವಾಗ ಅಂತರಜಾತಿಯ
ರಕ್ತ ಸಂಬಂಧದಿಂದ ಬಂಜ಼ೆತನವಿಲ್ಲದೆ, ಮುಂದೆಯೂ ಉತ್ತಮ ತಳಿಯನ್ನು ಹುಟ್ಟಿಸಲು,
ಸಮರ್ಥವಾದ ಪೀಳಿಗೆಯನ್ನು ಇದರಿಂದ ಪಡೆಯಲು ಸಾಧ್ಯವಿದೆ. ಆದರೆ ದುರದೃಷ್ಟ~
ವಶಾತ್, ಈ ಜಾತಿಪದ್ಧತಿಯನ್ನು ರಕ್ಷಿಸುವ ಸಲುವಾಗಿ, ದಷ್ಟಪುಷ್ಟವಾದ
ಸಂತಾನೋತ್ಪತ್ತಿಯನ್ನು ತಡೆಗಟ್ಟುವಂಥ ಮಹಾ ಮೂರ್ಖತನವೇ ಈ ಸಮಾಜದಲ್ಲಿ
ಮೂರ್ತಿವೆತ್ತಂತೆ ಎದ್ದು ನಿಂತಿದೆ. ಆಯಾ ಪೀಳಿಗೆಯ ಬುದ್ಧಿ ಬೆಳವಣಿಗೆಯ ದೃಷ್ಟಿ~
ಯಿಂದ ಆಯಾ ಬುಡಕಟ್ಟುಗಳನ್ನು ಪ್ರತ್ಯೇಕವಾಗಿಯೇ ಪೋಷಿಸುವುದು ಸೂಕ್ತವೆಂತಲೂ
ಜಾತಿಪದ್ಧತಿಯನ್ನು ಸಮರ್ಥಿಸುತ್ತಾರೆ. ಆದರೆ ಈ ಜಾತಿಪದ್ಧತಿಯಿಂದ ಬುದ್ಧಿ
ಬೆಳವಣಿಗೆ ಹೇಗೆ ಸಾಧ್ಯವೆಂಬುದೇ ನನಗೆ ಅರ್ಥವಾಗುತ್ತಿಲ್ಲ. ಸ್ವಾಭಾವಿಕವಾಗಿ ಜಾತಿ~
ಪದ್ಧತಿಯೇ ಮೊದಲಾಗಿ ಬಂಜೆಯಂಥ ವಸ್ತು. ಅದು ವಿವಿಧ ಪಂಗಡಗಳ ಅಂತರ
ಜಾತಿಯ ಮದುವೆಯ ಉತ್ಸಾಹವನ್ನು ನುಂಗಿಹಾಕುವಂಥ ದುಷ್ಟ ಪದ್ಧತಿ. ಮದುವೆ~
ಯಾದಂಥ ದಂಪತಿಗಳಿಬ್ಬರೂ ಒಂದೇ ಜಾತಿಯವರಾಗಿದ್ದರೆ ಅದೊಂದು (ಈ ವಿಚಾರದಲ್ಲಿ)
ಅರ್ಥಪೂರ್ಣವಾದ ಮದುವೆಯೇ ಅಲ್ಲ. ಒಂದು ಜಾತಿಯ ಹುಟ್ಟು ಉತ್ತಮ ಸಂತತಿ~
ಯಿಂದ ಕೂಡಿದ್ದರೆ ಅದರ ಒಳಜಾತಿಯೂ ಅಷ್ಟೇ ಉತ್ತಮ ಸಂತತಿಯಿಂದ ಕೂಡಿರ~
ಬೇಕಾದುದು ಸ್ವಾಭಾವಿಕ. ಆದರೆ ಈ ಒಳಜಾತಿಯ ಉತ್ತಮ ಸಂತಾನಸಾರವನ್ನು
ಕಾಪಾಡಿಕೊಂಡು ಬರುವವರು ಎಷ್ಟು ಮಂದಿ ಇರುತ್ತಾರೆ? ಸರ್ವೇಸಾಮಾನ್ಯವಾದ
ಇದಕ್ಕಾಗಿ ಜೀವನ ತೆಯ್ಯುವುದು ಬುದ್ಧಿಗೆ ಹಿಡಿದ ಭೂತವೇ ಸರಿ. ಒಂದು ಜಾತಿ~
ಯೆಂದರೆ ಅದರ ಬುಡಕಟ್ಟು ಎಂದು ಅರ್ಥೈಸಿದರೆ ಅದರ ಒಳಜಾತಿಗಳನ್ನೂ ಬುಡಕಟ್ಟು~
ಗಳೆಂದು ಹೇಳಲು ಹೇಗೆ ಸಾಧ್ಯ? ಏಕೆಂದರೆ ಒಳಜಾತಿಗಳು ಒಂದೇ ಬುಡಕಟ್ಟಿನ~
ವಾಗಿದ್ದರೂ ಅವು ಕಾಲ಼ಕ್ರಮದಲ್ಲಿ ಸಾರಹೀನವಾಗಿರುತ್ತವೆ. ಆದ್ದರಿಂದ ಒಳಜಾತಿ~
ಗಳಲ್ಲಿ ಅಂತರಜಾತಿಯ ಮದುವೆಗಳು, ಅಂತರಜಾತಿಯ ಊಟ ಉಪಚಾರದ ಕೂಟಗಳು
ಮುಂತಾದ ಸಾರ್ವಜನಿಕ ಒಟ್ಟುಗೂಡುವಿಕೆಗೆ ಇರುವ ಪ್ರತಿಬಂಧದ ಉದ್ದೇಶ ರಕ್ತದ
ಅಥವಾ ಬುಡಕಟ್ಟಿನ ಶುದ್ಧಿತ್ವವನ್ನು ಕಾಪಾಡುವುದಿಲ್ಲ. ಕಾರಣ, ಆ ಒಳಜಾತಿ ಹುಟ್ಟಿ~
ನಿಂದಲೇ ತಾನು ಉತ್ತಮ ಸಂತತಿಯಾಗಿದ್ದರೆ, ಆ ಜಾತಿಯನ್ನು ಹುಟ್ಟಿನಿಂದಲೇ
ಉತ್ತಮ ಸಂತತಿಯನ್ನಾಗಿ ಮಾಡಲು ಅದಕ್ಕೆ ಯಾವ ಸಾರವತ್ತಾದ ಪದಾರ್ಥವನ್ನೂ
ಸೇರಿಸಬೇಕಾಗಿಲ್ಲ. ಒಂದು ಜಾತಿ ಹುಟ್ಟಿನಿಂದಲೇ ಉತ್ತಮ ಸಂತತಿಯಾಗಿದ್ದು ಪಕ್ಷದಲ್ಲಿ
ಆಗ ಅಂತರಜಾತಿಯ ಮದುವೆಗಳಿಗೆ ಇರುವ ಪ್ರತಿಬಂಧ ಏನು ಮತ್ತು ಏಕೆ ಅನ್ನುವುದು
ಅರ್ಥವಾಗುತ್ತದೆ. ಇದಕ್ಕೆ ಯಾವ ತರ್ಕದ ನೆರವೂ ಬೇಕಿಲ್ಲ. ಆದರೆ ಜಾತಿಗಳು
ಹುಟ್ಟಿನಿಂದ ಉತ್ತಮತೆಯನ್ನು ಹೊಂದಿಯೇ ಇಲ್ಲವೆಂಬುದು ಆಧಾರಭೂತವಾಗಿರುವಾಗ
ಅಂತರಜಾತಿಯ ಮದುವೆಗಳಿಗೂ ಅಂತರಜಾತಿಯ ಊಟದ ಕೂಟಗಳಿಗೂ ಇರುವ
-86-
ನಿಷೇಧದ ಉದ್ದೇಶವೇನು? ಇದನ್ನು ಅರ್ಥಮಾಡಿಕೊಳ್ಳಲು ಕೂಡ ಯಾವ ತರ್ಕವೂ
ಬೇಕಿಲ್ಲ. ವಿವಿಧ ಜಾತಿಗಳು ಒಟ್ಟಿಗೆ ಕುಳಿತು ಊಟ ಮಾಡುವುದರಿಂದ ಒಂದು ಜಾತಿ~
ಯವನ ರಕ್ತ ಕಣಗಳು ಇನ್ನೊಂದು ಜಾತಿಯವನ ರಕ್ತದಲ್ಲಿ ಸೇರಿಬಿಡವುದಿಲ್ಲ~
ವೆಂಬುದನ್ನು ಯಾರೂ ಹೇಳಿಕೊಡಬೇಕಿಲ್ಲ. ಅಂದರೆ, ಹಾಗೆ ಒಟ್ಟಿಗೆ ಕುಳಿತು ಊಟ~
ಮಾಡುವುದರಿಂದ ಬುಡಕಟ್ಟಿನ ರಕ್ತದ ಉನ್ನತಿಯಾಗಲೀ ಅವನತಿಯಾಗಲೀ ಆಗುವು~
ದಿಲ್ಲವೆಂಬುದು ಮಾತ್ರವಲ್ಲ, ಜಾತಿ ಅಥವಾ ವರ್ಣ ಪದ್ಧತಿಗೆ ಯಾವುದೇ ವೈಜ್ಞಾನಿಕ
ಆಧಾರವಿಲ್ಲವೆಂಬುದನ್ನು ಇದರಿಂದ ತಿಳಿದಂತಾಯಿತು. ಇಷ್ಟಾದರೂ ಜಾತಿಯತೆಗೆ
ಉತ್ತಮ ಸಂತತಿಯ ಆಧಾರವಿದೆಯೆಂದು ಹೇಳುವ ಅನೇಕ ಮಂದಿ ಇನ್ನೂ ಇದ್ದಾರೆ.
ಆದರೆ ಅವೈಜ್ಞಾನಿಕ ಹಾಗೂ ಕೊಳಕು ರಾಡಿಯಾದ ಈ ಜಾತಿಪದ್ಧತಿಗೆ ವಿಜ್ಞಾನದ
ರಂಗು ಬಳಿಯುತ್ತಿದ್ದೇವೆ ಎಂಬುದು ಅವರಿಗೆ ತಿಳಿದಿದ್ದರೂ ಅದನ್ನು ಅದುಮಿ ಹಿಡಿದು~
ಕೊಂಡು `ಇಲ್ಲ ಇಲ್ಲ ಜಾತಿಪದ್ಧತಿಗೆ ವೈಜ್ಞಾನಿಕ ಆಧಾರವಿದೆ'ಯೆಂದು ನಿರ್ಲಜ್ಜೆ~
ಯಿಂದ
ಹೇಳುತ್ತಾರೆ. ಅನುವಂಶೀಯತೆಯಿಂದ ಉತ್ತಮ ಸಂತಾನ ಪಡೆಯಬಹುದೆಂಬುದರ
ಬಗೆಗೆ ಪ್ರಾಯೋಗಿಕ ಸಾಧ್ಯತೆಗಳು ಇಂದಿಗೂ ಕಂಡುಬಂದಿಲ್ಲ. ಪ್ರೊ ಬೆಟ್್ಸನ್್ ತನ್ನ
`Mendle`s Principles of Heridity' ಎಂಬ ಗ್ರಂಥದಲ್ಲಿ ಹೇಳಿರುವಂತೆ ಒಂದು
ವರ್ಗದ ಬೌದ್ಧಿಕ ಗುಣಗಳು ಪತನವಾಗಲು ಕಾರಣವೇನೆಂದು ಹುಡುಕುವುದರಲ್ಲಿ ಏನೂ
ಅರ್ಥವಿಲ್ಲ. ಯಾರೊಬ್ಬರ ಗುಣಗಳೂ ಇನ್ನೊಬ್ಬರ ರಕ್ತಕ್ಕೆ ಸೇರಿಕೊಂಡು ಆ ಬೌದ್ಧಿಕ
ಗುಣಗಳು ಪತನವಾಗುವುದಿಲ್ಲ. ಹಾಗೆಯೇ ದಷ್ಟಪುಷ್ಟವಾದ ಶರೀರವುಳ್ಳಂಥ ದಂಪತಿಗಳು
ಕೂಡಿದಾಗ ಹುಟ್ಟಬಹುದಾದಂಥ ಆ ಮಗು ಬಹಳ ಮಟ್ಟಿಗೆ ಅವರಂತೆಯೇ ಹುಟ್ಟು~
ವುದು ಅಸಂಖ್ಯಾತವಾದ ಆ ವಸ್ತುಗಳಲ್ಲಿರುವ ಫಲದಿಂದಲೇ ಹೊರತು ನನಗೆ ಬೇಕಾದಂಥ
ಮಗುವನ್ನೇ ನಾನು ಹುಟ್ಟಿಸಬಲ್ಲೆ ಅನ್ನುವ ಛಲದಿಂದಲ್ಲ. ಜಾತಿಪದ್ಧತಿಯ ಮೂಲ~
ಕಲ್ಪನೆಗೆ ಕೈ ಹಾಕಿದರೆ, ಇಂದಿನ ಹಿಂದೂಗಳು ತಮ್ಮ ತಮ್ಮ ಜಾತಿಯ ಉತ್ತಮ
ಸಂತತಿಯ ಬೇರು ತಮ್ಮ ಪೂರ್ವಜರಲ್ಲಿಯೇ ಇತ್ತೆಂದು ಹೇಳುವ ತಿಳಿಗೇಡಿತನಕ್ಕೂ
ಅವರು ಹೇಸರು. ಇಂಥ ಅನುವಂಶೀಯತೆಯನ್ನು ಸಂಶೋಧಿಸಿ ಕಂಡುಹಿಡಿದಿರುವ
ಅವರಿಗೆ ಗೊತ್ತಿಲ್ಲವಾದ್ದರಿಂದ ಅವರು ಹಾಗೆ ಹೇಳುತ್ತಿದಾದರೆ ಅಷ್ಟೇ. ಒಂದು ಮರದ
ಫಲದ ಗುಣವನ್ನು ಅದರ ಬೀಜ ಮತ್ತು ನೆಲದ ಆಧಾರದ ಮೇಲೆ ನಿರ್ಧರಿಸಬಹುದು.
ಜಾತಿಯೆಂಬುದು ಒಂದು ಸಂತಿತಿಯ ಬೇರು ಎಂದು ಒಪ್ಪಿಕೊಂಡರೆ ಆ ಬೇರು ಎಂಥ
ತಳಿಯನ್ನು ಹುಟ್ಟಿಸಬಹುದು? ಶಾರೀರಕವಾಗಿ ಹೇಳುವುದಾದರೆ `ಹಿಂದೂಗಳು C3ನೇ
ವರ್ಗದ ಜನರು. (C3ನೇ ವರ್ಗದ ಜನ ಎಂದರೆ ದೈಹಿಕವಾಗಿ ಮತ್ತು ಬೌದ್ಧಿಕವಾಗಿ
ದುರ್ಬಲರಾದ ಜನ ಎಂಬ ಅರ್ಥ ವೈದ್ಯಕೀಯ ನಿಘಂಟುವಿನಿನಲ್ಲಿದೆ - ಅನುವಾದಕ)
ಕುಳ್ಳರು, ಕುಗ್ಗಿದ ಎದೆಯುಳ್ಳವರು, ಮತ್ತು ಅಂಥ ಬಲಶಾಲಿಗಳೂ ಅಲ್ಲದವರು.
ಸುಮಾರು ಹತ್ತರಲ್ಲಿ ಒಂಭತ್ತು ಜನ ಹಿಂದೂಗಳು ಮಿಲಿಟರಿಗೆ ನಾಲಾಯಕ್ಕಾದ ಜನರಿಂದ
-87-
ಕೂಡಿದ ದೇಶ-ಇಂಡಿಯಾ' ಅಂದರೆ ಜಾತಿಪದ್ಧತಿ ಉತ್ತಮ ಸಂತಾನ ಸಾರವನ್ನು
ಒಳಗೊಂಡಿದೆಯೆಂಬ ಇವರ ವಾದ ಮಹಾ ವಿಜ್ಞಾನಿಗಳಿಗೂ ತಿಳಿದಿಲ್ಲವೆಂಬುದು ಸ್ಪಷ್ಟ~
ವಾದಾಗ ಇವರ ಈ ವಾದದ ಹಿಂದಿನ ಮಸಲತ್ತು ಏನೆಂಬುದನ್ನು ಸುಲಭವಾಗಿ ಊಹಿಸಿ~
ಕೊಳ್ಳಬಹುದು. ಸಾಮಾಜಿಕ ಅಂತಸ್ತಿಗಾಗಿ ಸ್ವಾರ್ಥಿಗಳೂ ಅಹಂಭಾವದ ಜನರೂ
ಆದಂಥ ಹಿಂದೂಗಳು ತಮ್ಮ ಉನ್ನತ ಸ್ಥಾನಮಾನಗಳ ಪಟ್ಟಭದ್ರಗಳನ್ನು ಕಾಪಾಡಿ~
ಕೊಳ್ಳುವ ಸಲುವಾಗಿ ಈ ನೀಚ ಪದ್ಧತಿಯ ಮೇಲೆ ವೈಜ್ಞಾನಿಕ ಕಿರೀಟ ಇಟ್ಟು, ಅದೇ
ಪದ್ಧತಿಯನ್ನು ಕೆಳಪಂಗಡದವರ ಮೇಲೆ ಬಲಾತ್ಕಾರವಾಗಿ ಹೇರುತ್ತಿದ್ದಾರೆ ಎಂಬುದನ್ನು
ಮರೆಯಬೇಡಿ!</p>

<p>ಕಂಡಿದ್ದೂ ಹೇಸಿಗೆ ತುಳಿದರು.</p>

<p>ಜಾತಿಪದ್ಧತಿ ಆರ್ಥಿಕ ನೈಪುಣ್ಯತೆಯ ಸಂಕೇತವೇನೂ ಅಲ್ಲ. ಜಾತಿಪದ್ಧತಿಗೆ ವೈಜ್ಞಾನಿಕ
ರಂಗು ಬಳಿದು ವಾದಿಸುವ ಜನರ ಅಭಿಪ್ರಾಯದಂತೆ ಜಾತಿಪದ್ಧತಿ ಬುಡಕಟ್ಟುಗಳ ಶ್ರೇಷ್ಠತೆ~
ಯನ್ನು ಉದ್ಧಾರಮಾಡಿಲ್ಲ. ಆದರೆ ಜಾತಿಪದ್ಧತಿ ಸಾಧಿಸಿರುವ ಒಂದು ಮಹಾ ಸಾಧನೆ~
ಯೆಂದರೆ, ಹಿಂದೂಗಳ ಸಂಘಟನೆಯನ್ನು ವಿಚ್ಛಿದ್ರಗೊಳಿಸಿರುವ ಈ ನೀಚಪದ್ಧತಿ
ಅವರಲ್ಲಿರಬೇಕಾಗಿದ್ದ ನೈತಿಕ ಸೌಜನ್ಯವನ್ನೂ ಕುಲಗೆಡಿಸಿಬಿಟ್ಟಿದೆ.</p>

<p>ಮೊಟ್ಟಮೊದಲಿಗೆ ನಾವು ಗಮನಿಸಬೇಕಾದಂಥ ಅಂಶವೆಂದರೆ `ಹಿಂದೂ ಸಮಾಜ'
ಅನ್ನುವುದು ಒಂದು ನಿರಂತರ ಭ್ರಮೆ ಅಷ್ಟೆ. `ಹಿಂದೂ' ಅನ್ನುವ ಹೆಸರೇ
ವಿದೇಶಿಯರು ಸೃಷ್ಟಿಸಿ ತೆಗೆದ ಹೆಸರು. ಇಲ್ಲಿನ ಮೂಲ ನಿವಾಸಿಗಳಿಂದ ತಾವು ಬೇರೆ~
ಯೆಂಬುದನ್ನು ಗುರುತಿಸಿಕೊಳ್ಳುವ ಸಲುವಾಗಿ ಈ ಹೆಸರನ್ನು ಮೊದಲು ರೂಢಿಗೆ
ತಂದವರು ಮುಸಲ್ಮಾನರು. ಈ ದೇಶದ ಮೇಲೆ ಆದಂಥ ಮುಸಲ್ಮಾನರ ಆಕ್ರಮಣಕ್ಕೆ
ಮೊದಲು ಹಿಂದೂ ಅನ್ನುವ ಶಬ್ದ ಸಮಗ್ರ ಸಂಸ್ಕೃತ ಸಾಹಿತ್ಯದ ಯಾವ ಮೂಲೆಯಲ್ಲೂ
ಇರಲಿಲ್ಲ. ಆ ಪಂಗಡಕ್ಕೆ ಒಂದು ಪ್ರತ್ಯೇಕ ಹೆಸರಿಡಬೇಕಾದ ಅಗತ್ಯವೂ ಇರಲಿಲ್ಲ.
ಏಕೆಂದರೆ ಹಿಂದೂ ಅನ್ನುವ ಒಂದು ಪಂಗಡವಿತ್ತು ಎಂಬ ಪರಿಕಲ್ಪನೆಯೇ ಅವರಿಗಿರಲಿಲ್ಲ.
ಅಂದ ಮೇಲೆ ಹಿಂದೂ ಸಮಾಜ ಅನ್ನುವ ಒಂದು ವರ್ಗ ಅಸ್ತಿತ್ವದಲ್ಲೇ ಇರಲಿಲ್ಲವೆಂಬುದು
ನಿಸ್ಸಂದೇಹ. ಇಂದಿಗೂ ಇದೊಂದು ನೂರೆಂಟು ಜಾತಿಮತಗಳ ಕಂತೆ ಅಷ್ಟೆ. ಅದರೊಳಗಿನ
ಪ್ರತಿಯೊಂದು ಜಾತಿಯೂ ತನ್ನದೇ ಆದ ಸ್ವಭೇದದ ಹೇಸಿಗೆ ಹಾದಿಯಲ್ಲಿ ಸದಾ ಮೂಗು
ಮುಚ್ಚಿಕೊಂಡು ಅಸಹ್ಯವಾಗಿ ಸಾಗುತ್ತಿದೆ. ತನ್ನ ನರಸತ್ತ ಸ್ಥಿತಿಯನ್ನು ಮುಚ್ಚಿಕೊಳ್ಳಲು
ಎಲ್ಲರ ಎದುರು ನಿರ್ಲಜ್ಜವಾದ ಧೀರತನವನ್ನು ಮೆರೆಯಲೆತ್ನಿಸುತ್ತಿದೆ.</p>

<p>ತನ್ನ ಬಿಲದಲಿ ತಾನಿತ್ತು ಆ ಇಲಿ</p>

<p>ನಮ್ಮ ಹಿಂದೂ ಪಂಡಿತರು ಇಂದು ಹೇಳುತ್ತಿರುವ ಯಾವೊಂದು ಸಂಘಟನಾ ವಿಧಾನ~
ದಿಂದಲೂ ಜಾತಿಪದ್ಧತಿ ಹುಟ್ಟಿ ಬಂದಿಲ್ಲ. ಹಿಂದೂ - ಮುಸ್ಲಿಂ ಎನ್ನುವ ಒಂದು ದಂಗೆ~
-88-
ಯನ್ನು
ಬಿಟ್ಟರೆ ಹಿಂದೂ ಅನ್ನುವುದೊಂದು ಜಾತಿಯಾಗಿತ್ತು ಎಂದು ಯಾರೂ ಹೆಮ್ಮೆ~
ಪಡಬೇಕಾಗಿಲ್ಲ. ಎಲ್ಲ ಕಾಲದಲ್ಲೂ ಇದರ ಪ್ರತಿಯೊಂದು ತುಕಡಿಯೂ ಅನ್ಯರಿಂದ
ಪ್ರತ್ಯೇಕವಾಗಿರುವಂಥ ಪಶುಸಮಾನ ಪದ್ಧತಿಯಲ್ಲೇ ಇದರ ಜೀವನ ಸಾಗಿದೆ. ಪ್ರತಿಯೊಂದು
ಜಾತಿಯೂ ಊಟ ಉಪಚಾರಗಳಲ್ಲಿ ಇತರರಿಗಿಂತ ಭಿನ್ನ-ಈ ಜಾತಿಗಿಂತ ಆ ಜಾತಿ ಭಿನ್ನ,
ಮದುವೆ ವಿಚಾರದಲ್ಲಂತೂ ಒಂದು ಮರಿಜಾತಿಗೂ ಇನ್ನೊಂದು ಕಿರಿಜಾತಿಗೂ ಯಾವ
ಸಂಬಂಧವೂ ಇಲ್ಲ. ಆ ಮರಿಜಾತಿ ಕಿರಿಜಾತಿಗಳು ತಮ್ಮ ತಮ್ಮಲ್ಲೇ ಸುತ್ತಿ ಗಂಟುಹಾಕಿ~
ಕೊಳ್ಳುವುದರಲ್ಲಿ ಅವು ತಮ್ಮ ಜೀವ-ಜೀವನದ ಸಾರ್ಥಕತೆಯನ್ನು ಸಾಧಿಸಿಕೊಂಡೆವೆಂದು
ಹಿಗ್ಗುತ್ತವೆ. ಪ್ರವಾಸಿಗರನ್ನು ವಿನೋದಗೊಳಿಸುವ ಸಾವಿರಾರು ತರಹದ ಭಾರತೀಯರ
ವೇಷಭೂಷಗಳ ಬಗೆಗೆ ಎಷ್ಟು ಉದಾಹರಣೆಗಳು ಬೇಕು? ಯಾರ ಸಂಪರ್ಕವನ್ನೂ
ಒಲ್ಲದೆ ತನ್ನ ಬಿಲದಲ್ಲಿ ತಾನೇ ಸಾಯುವ ಇಲಿಗೂ ಹಿಂದೂಗಳಿಗೂ ಏನು ವ್ಯತ್ಯಾಸವಿದೆ?
ಎಲ್ಲಾ ಸಮಾಜ ಶಾಸ್ತ್ರಜ್ಞರೂ ಹೇಳುವಂತೆ `ಜಾಗೃತಿಯ ವಿಧಾನದ ಕೊರತೆ' ಹಿಂದೂ~
ಗಳಲ್ಲಲ್ಲದೆ ಮತ್ಯಾವ ಜನಾಂಗದಲ್ಲೂ ಇಷ್ಟು ಪ್ರಮಾಣದಲ್ಲಿಲ್ಲ. ಜಾಗೃತಿಯ ವಿಧಾನ~
ವೆಂದರೆ ಏನೆಂಬುದೇ ಹಿಂದೂಗಳಿಗೆ ತಿಳಿಯದು. ಪ್ರತಿಯೊಬ್ಬ ಹಿಂದೂವಿನ ಮನಸ್ಸಿನಲ್ಲಿ
ತನ್ನ ಜಾತಿ ಮತ್ತು ಉಪಜಾತಿಯ ಬಗೆಗೆ ಇರುವಷ್ಟು ಎಚ್ಚರ ಮತ್ಯಾವುದರ ಬಗೆಗೂ
ಇರುವುದಿಲ್ಲ. ಜೀವ ಹೋದರೂ ಜಾತಿ ಬಿಡರು. ಈ ಕಾರಣದಿಂದಲೆ ಇದುವರೆಗೂ
ಹಿಂದೂಗಳು ಒಂದು ರಾಷ್ಟ್ರೀಯರು' ಎಂಬುದನ್ನು ಅವರ ಆತ್ಮವೇ ಒಪ್ಪುತ್ತಿಲ್ಲ. ಈ
ಕಾರಣದಿಂದಾಗಿಯೇ ಹಿಂದೂಗಳು ಇವತ್ತಿಗೂ ಒಂದು ನಿರಾಕಾರ ಜನತೆಯಾಗಿ
ಉಳಿದಿದ್ದಾರೆ. ಭಾರತದ ಯಾವ ಮೂಲೆಯಲ್ಲಿರುವ ಹಿಂದೂಗಳನ್ನು ಪರಿಶೀಲಿಸಿದರೂ
ರೂಢಿ-ಪದ್ಧತಿಗಳು, ಹವ್ಯಾಸ-ನಂಬಿಕೆಗಳು ಮುಂತಾದವುಗಳ ವಿಚಾರದಲ್ಲಿ `ಅವರಿಗಿಂತ
ನಾವು ಮೇಲು' ಎಂದು ಒಂದು ಗುಂಪು ಘೋಷಿಸಿದರೆ, `ಇಲ್ಲ ಇಲ್ಲ, ಅವರು ಹೇಳುವು~
ದೆಲ್ಲಾ ಬರೀ ಸುಳ್ಳು, ಎಲ್ಲಾ ರೀತಿಯಲ್ಲೂ ಅವರಿಗಿಂತ ನಾವೇ ಮೇಲು' ಎಂದು
ಇನ್ನೊಂದು ಜಾತಿ ಬೊಬ್ಬೆ ಹಾಕುತ್ತದೆ. ಇಂಥ ವೈಚಿತ್ರ್ಯದ ನಡುವೆ ನಿಂತಂಥ
಩ೀವು ಹಿಂದೂಗಳದು ಒಂದು ಸಮಾಜವಾಗಿ ನಿಂತಿದೆಯೆಂದು ನಿಷ್ಪಕ್ಷಪಾತವಾಗಿ ಹೇಳ~
ಬಲ್ಲಿರಾ? ಕೆಲವು ರೂಢಿ ಸಂಪ್ರದಾಯಗಳಲ್ಲಿ ಅಕಸ್ಮಾತ್ ಕಾಣಬಹುದಾದಂಥ ಹಲ
ಕೆಲವು ಸಾಮ್ಯತೆಗಳಿಂದಲೇ ಹಿಂದೂಗಳದು ಒಂದು ಸಮಾಜವೆನ್ನಲು ಖಂಡಿತಾ ಸಾಧ್ಯವಿಲ್ಲ.
ಅವರ ಮನೋನಡತೆಯ ಮೂಲ ಬೇರನ್ನು ತಿಳಿಯದೆ ಬಂದು, ಹಿಂದೂಗಳ ಸಂಘಟನೆ
ಮಾಡುವೆವೆಂಬುದು ಮೂರ್ಖತನವಲ್ಲದೆ ಮತ್ತೇನು? ತನ್ನ ಸಮಾಜದಿಂದ ಸಾವಿರಾರು
ಮೈಲಿಗಳಾಚೆ ಇರುವ ಒಬ್ಬ ವ್ಯಕ್ತಿ ಹೇಗೆ ಆ ಸಮಾಜದ ಸದಸ್ಯನೆನಿಸಿಕೊಳ್ಳಲು ಸಾಧ್ಯ~
ವಿಲ್ಲವೋ ಹಾಗೆಯೇ, ಒಂದು ಜನತೆ ಒಂದು ಗುಂಪಿನಲ್ಲಿದ್ದಾಕ್ಷಣಕ್ಕೆ ಅದೊಂದು ಸಮಾಜ~
ವೆನಿಸಿಕೊಳ್ಳಲು ಸಾಧ್ಯವಿಲ್ಲ. ಎರಡನೆಯದಾಗಿ, ಕೆಲವು ರೂಢಿ, ಸಂಪ್ರದಾಯ, ನಡತೆ,
-89-
ಆಲೋಚನಾ ರೀತಿಯಲ್ಲಿನ ಅಲ್ಪ ಸ್ವಲ್ಪ ಸಾಮ್ಯತೆಯೂ ಒಂದು ಸಮಾಜವಾಗಲು
ಸಾಧ್ಯವಿಲ್ಲ.</p>

<p>ಕೆಲವು ಪದ್ಧತಿಗಳ ಸಾಮ್ಯವೇ ಒಂದು ಸಮಾಜವಲ್ಲ</p>

<p>ಕೆಲವು ಪದ್ಧತಿಗಳನ್ನು ಒಂದು ಗುಂಪಿನವರು ಇನ್ನೊಂದು ಗುಂಪಿನಿಂದ ಎರವಲು ಪಡೆದು~
ಕೊಂಡಾಗ ಆ ಎರಡು ಗುಂಪಿನ ಜನಜೀವನದಲ್ಲಿ ಸಾಮ್ಯತೆ ಕಂಡುಬರುವುದು ಸ್ವಾಭಾವಿಕ.
ಒಂದು ಕಾಲದಲ್ಲಿ ಒಂದು ಸಂಸ್ಕೃತಿ ಬಹಳ ಶೀಘ್ರವಾಗಿ ಬೆಳೆದು ಅದು ಇನ್ನೊಂದು
ಪಂಗಡವನ್ನು ಆಕರ್ಷಿಸಬಹುದು. ಆಗ ಈ ಎರಡು ಜನಾಂಗಗಳ ಜೀವನರೀತಿ, ನಂಬಿಕೆ
ರೂಢಿ ಮುಂತಾದವುಗಳಲ್ಲಿ ಸಾಮ್ಯತೆ ಬರಲೇಬೇಕು. ಇಷ್ಟು ಮಾತ್ರದಿಂದಲೇ ಆ ಎರಡು
ಜನಾಂಗಗಳೂ ಸೇರಿ ಒಂದು ಸಮಾಜವಾಗುತ್ತದೆ ಎಂದು ಹೇಳಲು ಸಾಧ್ಯವೇ ಇಲ್ಲ.
ಒಂದು ಜನಾಂಗದ ವಾಸ್ತವಿಕ ಜೀವನ ಪದ್ದತಿಯಲ್ಲಿ ವ್ಯತ್ಯಾಸವೇ ಇಲ್ಲದಂಥ ಸಾಮ್ಯ~
ವನ್ನು ಕಾಣಬಹುದು. ಒಂದು ಜನಾಂಗದಲ್ಲಿ ವ್ಯತ್ಯಾಸವೇ ಇಲ್ಲದ ಈ ಸ್ಥಿತಿಗೂ
ಸಾರ್ವಜನಿಕವಾದ ಇನ್ನೊಂದು ಸ್ಥಿತಿಗೂ ಅಪಾರ ಅಂತರವಿದೆ. ಇಲ್ಲಿ ವಾಸ್ತವಿಕ ಸಾಮ್ಯತೆ~
ಯನ್ನು ಪಡೆಯಲು ಇರುವ ಒಂದೇ ಮಾರ್ಗವೆಂದರೆ ಒಬ್ಬನಿಗೂ ಇನ್ನೊಬ್ಬ~
ನಿಗೂ ಇರಬೇಕಾದ ನಿಷ್ಕಳಂಕ ಹಾಗೂ ನಿರ್ಭಯವಾದ ಎಲ್ಲ ರೀತಿಯಲ್ಲೂ ವಾಸ್ತವ~
ವಾದ ಸಂಪರ್ಕದಿಂದ ಮಾತ್ರ. ಇದನ್ನು ಇನ್ನೊಂದು ರೀತಿಯಲ್ಲಿ ಹೇಳಬೇಕೆಂದರೆ, ಅಂಥ
ಸಂಪರ್ಕದಲ್ಲೇ ಸಮಾಜ ತನ್ನ ಇರುವಿಕೆಯನ್ನು ನೋಡಿಕೊಳ್ಳುತ್ತದೆ. ಹಾಗಲ್ಲದೆ,
ಯಾವುದೋ ಕಾರಣದಿಂದಾಗಿ ಒಂದು ಜನಾಂಗದ ನಡವಳಿಕೆಯನ್ನು ಇನ್ನೊಂದು
ಅನುಕರಿಸಿದರೆಂದ ಮಾತ್ರಕ್ಕೆ ಆ ಎರಡು ಜನಾಂಗಗಳನ್ನು ಒಟ್ಟಿಗೆ ಸೇರಿಸಿ ಒಂದು
ಸಮಾಜವಾಯಿತೆಂದು ಹೇಳಿ ಕೈತೊಳೆದುಕೊಂಡರೆ ಅದರಂಥ ತಿಳಿಗೇಡಿತನ ಇನ್ನೊಂದಿಲ್ಲ.
ಹಿಂದೂಗಳಲ್ಲಿರುವ ವಿವಿಧ ಜಾತಿ ಪಂಗಡಗಳು ಆಚರಿಸುವ ಹಬ್ಬ ಆಚರಣೆಯ
ಪದ್ದತಿಯಲ್ಲೂ ಒಂದು ಸಾಮ್ಯವನ್ನು ಕಾಣಲು ಸಾಧ್ಯವಿಲ್ಲ. ಇವರೆಲ್ಲರನ್ನೂ ಒಂದು
ಅವಿಭಾಜ್ಯ ಅಂಗವನ್ನಾಗಿ ಹಿಡಿದು ನಿಲ್ಲಿಸಬೇಕಾದರೆ, ಒಬ್ಬನ ನೋವು ನಲಿವನ್ನು ಕಂಡು
ಇನ್ನೊಬ್ಬ ಅದು ತನ್ನದೇ ಅನ್ನುವಷ್ಟರ ಮಟ್ಟಿಗೆ ಅದರಲ್ಲಿ ಅವನು ಭಾಗಿಯಾದಾಗ
ಮಾತ್ರ ಆ ಅವಿಭಾಜ್ಯ ಭಾವನೆ ಅವನಲ್ಲಿ ಬೇರು ಬಿಡಲು ಸಾಧ್ಯ. ಆದ್ದರಿಂದ ಒಬ್ಬ
ವ್ಯಕ್ತಿ ಒಂದು ಸಂಘಟಿತ ಚಟುವಟಿಕೆಯಲ್ಲಿ ಒಂದು ಅವಿಭಾಜ್ಯ ಅಂಗವಾದರೆ ಆ ಭಾವನೆ
ಆ ಸಂಘಟಿತ ಬದುಕಿನ ಸೋಲು ಗೆಲುವೆಲ್ಲ ತನ್ನದೇ ಎನ್ನುವಂಥ ಭಾವನೆಗೆ ದಾರಿ
ಬಿಡುತ್ತದೆ. ಆಗ ಆ ಭಾವನೆ ಆ ಸಮುದಾಯ ಒಂದು ಸಮಾಜವಾಗಿ ನಿಲ್ಲಲು ಸಹಾಯಕ~
ವಾಗುತ್ತದೆ. ಆದರೆ ಹಿಂದೂಗಳಲ್ಲಿನ ಜಾತಿ ಪದ್ಧತಿಯಿಂದಾಗಿ ಅಂಥ ಸಮುದಾಯ
ಚಟುವಟಿಕೆಗಳು ಕುರುಡಾಗಿ ಬಿದ್ದುಕೊಂಡಿವೆ. ಈ ದುರಭಿಮಾನ ಹಿಂದೂಗಳು
-90-
ಇನ್ನೆಂದೆಂದೂ ಒಂದು ಸಮಾಜವಾಗಲು ಸಾಧ್ಯವೇ ಇಲ್ಲವೇನೋ ಎಂಬಷ್ಟರ ಮಟ್ಟಿಗೆ
ಹಿಂದೂಗಳಲ್ಲಿ ಬೆಳೆದಿದೆ.</p>

<p>
",0
</p></body></text></cesDoc>