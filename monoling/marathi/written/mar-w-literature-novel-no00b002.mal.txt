<cesDoc id="mar-w-literature-novel-no00b002.mal" lang="mar">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>mar-w-literature-novel-no00b002.mal.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-25</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>करूणाष्टक</h.title>
<h.author>व्यंकटेश माडगूळकर</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown -  मॅजेस्टिक प्रकाशन</publisher>
<pubDate>1982</pubDate>
</imprint>
<idno type="CIIL code">no00b002.mal</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page  पुणे.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-25</date></creation>
<langUsage>Marathi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>एकूण पाने :- 25
FROM 26 TO 50</p>

<p>पान नं. - 26</p>

<p>तोडल्यावर कोणत्या देवध्यानी म्हातारीला संताप येणार नाही ?</p>

<p>
        -आणि  एक गोळे कम्पाऊण्डर होते. गोळे आणि गोळीण. हे मोठं
गमतीदार जोडपं होतं. गोळे कधीकाळी कंपाऊण्डर होते. आता ते
सेवानिवृत्त होऊन स्वतंत्रपणे वैद्यकीय व्यवसाय करीत होते; पण तरीही
लोक त्यांना गोळे कंपाऊण्डर म्हणत. त्याबद्दल गोळ्यांची काही
तक्रार नसे. ते  डोक्याला रूमाल, अंगात बाराबंदी आणि सोग्याचं
धोतर अशा वेषात असत. त्यांना फार कमी ऐकू येई. मोठयांदा
बोलावं लागे.</p>

<p>        गोळीणबाई, पोथीत स्त्रियांची चित्रं असतात, तशा दिसायच्या. कपा    ~
ळाला भलंमोठं कुंकू लावायच्या.</p>

<p>        ह्या जोडप्याचं पोट पिकलं नव्हतं. वात्सल्याचा वर्षाव ते एकमेकांवरच
करत. गोळीणबाई कंपाऊण्डरांचं लहान बाळासारखं कोडकौतुक करीत
आणि गोळे गोळीणबाईंशी बोलताना लहान मुलांशी बोलावं, इतक्या
मृदु आणि समजूतदार आवाजात बोलत.</p>

<p>        नडीअडचणीला उपयोग म्हणून आईनं गोळीणबाईंशी मैत्री केली
होती. त्या अर्धवट आहेत, असा गावात बभ्रा होता; पण आई त्यांच्याशी
छान गप्पा मारी. घरात काही एवढं तेवढं केलं-पाटवडया, मोदक, लिंबाचं
लोणचं, की आठवणीनं ती आमच्या हातून गोळीणबाईंच्या घरी पाठवी.
आणि घरात कुणाला आजारपाजार आला. म्हणजे त्याला गोळ्यांकडे
घेऊन जाई.</p>

<p>        गोळे आणि आई यांच्यांतला संवाद हा फारच एकतर्फी होई. मग
आईला सांगायचं आहे, ते ऐकून घेऊन गोळीणबाई आपल्या पद्धतीनं
खाणाखुणा करून गोळ्यांपर्यंत पोचवत. इतका वेळ अज्ञान माणसा ~
सारखा असलेला गोळ्यांचा चेहरा एकदम खुले. 'मला सर्व काही माहीत
आहे, आता औषध देतो,' असे आत्मविश्वासपूर्ण उद््गार त्यांच्या तोंडून
निघत.</p>

<p>पान नं. - 27</p>

<p>        आई अशी चार माणसं मिळवण्याच्या कामी दंग असताना बिचारी
आजी मात्र स्वत:चाच हात रगडत कोपऱ्यात चूप बसलेली असे, तिचं
तोंड दीनवाणं दिसे, जेव्हा तेव्हा ती पेटत्या चुलीपुढं बसून हात शेकीत
असे आणि  'भगवंता, भगवंता.....' अशा हाका देवाला मारीत असे.
'सोडव, रे बाबा.....ने, रे, तुझ्याकडं,' असा धावाही ती करी,</p>

<p>        तिच्या तोंडून अशा करुण हाका ऐकल्या, की आईला कळवळा
येई, ती आजीच्या जवळ जाऊन विचारी,
        ""अहो, तुम्हांला काय होतंय् ? काही दुखतंय् का ?""
        ह्यावर आजी काही बोलत नसे.
        खरं तर तिला ह्या गावी राहायचंच नव्हतं.'माझी हाडं आता माझ्या
गावात पडू द्या.' असं तिला म्हणायचं असे; पण ही गोष्ट तिला फक्त
माझ्या वडिलांपाशी बोलायची असे. दुसऱ्या कुणापाशी नाही.
        मग आई विचारी,
        ""तुम्हांला काय देऊ करून ? काय खावंसं वाटतं ?""
        आजीला कसलीही वासना राहिलेली नव्हती. आईला वाटायचं, तिनं
रोज काही सांगावं. मग आपण तत्परतेनं ते करून घालावं.
        मग आई विचारी,
        ""सांजा करू का ? खाल ?""
        आजी हळू आवाजात म्हणायची,
        ""हो.....""
        तिचा हा होकार पाटीवर रेघ मारावी, तसा लांब असे.</p>

<p>
        एरवी ती एकटी बसलेली असली, म्हणजे ही इथं नाहीच, असं वाटे.
ती मनानं कुठं कुठं हिंडत असेल, कोण जाणे.</p>

<p>        दुपारी वडील झोपले असले, म्हणजे हळूच त्यांच्या अंगावर चादर
टाक, आमच्यापैकी कोणी तिच्याजवळ जाऊन पसरलं, की पाठीवरून
हात फिरव, असं ती करायची, तेव्हा तिच्या डोळ्यांत ओळख दिसे.
एरवी दिसत नसे. ती परक्या डोळ्यांनी आणि परक्या चेहऱ्यानं घरात
वावरत असे.</p>

<p>पान नं. - 28</p>

<p>        मग पावसाळा आला. इथला पावसाळा वेगळा होता. आमच्या गावी
पाऊस वाजत गाजत , लेझीम खेळत यायचा. धडाधडा कोसळायचा.
काळ्या जमिनी फुगून सुरेख वास सुटायचा. धुळीनं मळकटलेली झाडंझुडं
स्वच्छ धुतली जायची. केरकचरा गढूळ पाण्याच्या लोंढयाबरोबर वाहून
गावातले रस्ते स्वच्छ व्हायचे.</p>

<p>        इथं पाऊस किरकिरत आला. एखादं रडवं पोर मांडी घालून बसतं
आणि तोंड वर करून रडतं, तसा हा पाऊस कुढा आणि हट्टी
होता.</p>

<p>        रस्त्यावर पातळ चिखल सतत राहू लागला आणि चालणाऱ्या लोकांचे
कपडे मागच्या बाजूनं भरवू लागला. हवेत ओलसरपणा राहिल्या ~
मुळं उकिरडे, गटारं घाणू लागली. घरातली पांघरूण घाणू लागली. गूळ
पातळ होऊन पाझरू लागला. भिंतींना ओल चढली आणि वस्तू बुरसू
लागल्या.</p>

<p>        कौलावर हिरवी बुरशी वाढली. केसाळ काळी सुरवंटं कौलांतून
शेकडयांनी जन्मली आणि टपाटपा अंगावर पडू लागली. सुरवंटं अंगा ~
खाली चुरडला, की खाजकुयली लागल्याप्रमाणं आगडोंब उसळू लागला.</p>

<p>        ओढयांना नवं पाणी आलं. नाना जागची माती वाहून आली. जाग ~
जागचे साथीचे आजार वाहून आले.</p>

<p>        खरोखर, हा पावसाळ्याचा काळ म्हणजे दुर्दिन होते. सारखा बुरूबुरू
पाऊस पडत असायचा, झोंबरी, ओली थंडी वाजायची. ओले कपडे
वाळायचे नाहीत. ओली लाकडं पेटता पेटायची नाहीत. घरात धूर
व्हायचा आणि कौलांतून बाहेर निघून धपरावर जमायचा. बापडया
आईला पावसातून ओढयावर जाऊन पाणी आणावं लागे. सगळ्यांची
नाकं गळत आणि घसे वाजत. थंडीताप एकाचा गेला, की दुसऱ्याला
येई.</p>

<p>        असा पाऊस बसला असतानाच बाईआजीला नवं पाणी बाधलं आणि
अतिसार झाला. वारंवार अंथरुणातचं तिची विटंबना होऊ लागली. जड</p>

<p>पान नं. - 29</p>

<p>पासोडया, सतरंजी, तिच्या अंगावरची आलवणं आईला वरचेवर धुवावी
लागू लागली.
        घराचा माश्या फार झाल्या.
        गोळे कम्पाउण्डर येऊन बघून गेले. त्यांनी नाना औषधं दिली. त्या
कडू-तुरट औषधाचा कप पुढं करताच आजी तोंड फिरवू लागली.
        माझ्या वडिलांना म्हणू लागली,
        ""नको रे, मला हे प्यायला सांगूस ! मला उलटी होते. सगळा जीव
गोळा होऊन तोंडात येतो.""</p>

<p>        तिचं उठणं-फिरणं बंद झालं. कोपऱ्यात,चुलीपुढं बसणं बंद झालं
अंथरूणाच्या गबाळ्यात कृश अशी बाईआजी डोळे मिटून कण्हत पडून
राहू लागली.
        वडीलांनी हाका मारल्यावर ती फक्त डोळे उघडून त्यांच्याकडं टका ~
टका बघत राही.
        तिला बोलण्याचेही श्रम होऊ लागले.
        आम्ही शाळेत गेलो. वडील कचेरीत गेले, म्हणजे आई घराला
कुलूप लावून ओढयावर जाऊ लागली. कारण दार उघडं राहिलं, तर
आत भटकी कुत्री शिरतील आणि बाईआजीला हात उचलून त्यांना
हाकलणंही होणार नाही, हे तिला माहीत होतं. नुसती कडी लावून
जाणंही तिला प्रशस्त वाटायचं नाही. घरात कोणी शिरलं आणि घर धुऊन
नेलं, तर ?</p>

<p>        अशीच एकदा दुपारी आई कुलूप लावून गेली आणि तासाभरानं
आली. तिनं कुलूप उघडलं. घागर ठेवली, धुणं वाळत घातलं आणि
बाईआजी काही हालत नाही,  कण्हत नाही ,हे तिच्या लक्षात आलं.
काळजाला चरका बसला.</p>

<p>        जवळ जाऊन तिनं अंगाला हात लावून 'सासूबाई, सासूबाई ' अशा
हाका मारल्या आणि तिच्या लक्षात आलं :
        बापडी बाईआजी मरून गेली !</p>

<p>पान नं. - 30</p>

<p>तीन दिवस आमचं कौलारू घर गडद उदासीनतेत बुडून गेलं होतं.</p>

<p>        आजी ज्या जागी गेली, त्या जागी आईनं पणती लावली होती.
पणतीच्या बुडाशी जोंधळ्याच्या पांढऱ्या शुभ्र पिठाचं खळं केलं होतं
आणि त्यावर नवी दुरडी झाकली होती.</p>

<p>        रात्री  झोपायच्या वेळी दिवा मालवला आणि पावसाळ्यातला ओला
अंधार घरात पसरला, की बांबूच्या चोयटयांनी विणलेली नवी दुरडी
आकाशकंदिलासारखी प्रकाशे. अंथरूणावर अंग टाकलेल्या आईला पुन्हा
पुन्हा भडभडून येई. ती म्हणे,</p>

<p>        ""अरे, तुम्हां चार कच्च्याबच्च्यांच्या व्यापातून त्या कष्टी जिवाकडं
बघायला, मला सवड अशी झाली नाही . अरे, शेवटच्या आजारातसुद्धा
घडावी, तशी त्या वडीलमाणसाची सेवा माझ्या हातून घडली नाही. कशी
मला बुद्धी झाली आणि घराला कुलूप लावून पाण्याला गेले !""</p>

<p>        माझ्या वडिलांनी डोळ्यांतून पाणी काढतानासुद्धा मी कधी पाहिलं
नव्हतं. आजीला ओढयाकडे न्यायला लोकांनी जेव्हा उचललं, तेव्हा
दोन्ही हातांनी तोंड झाकून ते ओल्या अंगणात मटकन् खाली बसले.</p>

<p>        पुढे अनेक दिवस आजीची आठवण येऊन त्यांचे डोळे भरून येत.
दोन्ही तळव्यांनी ते डोळे पुसत. त्यांचे पातळ ओठ काही वेळ थरथरत
राहत.</p>

<p>        ते म्हणयाचे,
        ""इतकं गोत असून शेवटच्या क्षणी तिच्यापाशी कोणी नव्हतं. मुलगा
नव्हता, सुन नव्हती, नातवंडं नव्हती . बेवारशासारखी माझी आई एकाकी
गेली..... आपल्या गावात ती आनंदात राहत होती, मी तिला उचलून
फरफठत इकडं आणली. तिथं आपल्या गावात, आपल्या घरात राहिली
असती, तर आणखी दहा वर्षे जगली असती.""</p>

<p>
        तिसऱ्या दिवशी आईनं दुरडी उचलून पीठ न्याहाळलं आणि ती
आम्हांला म्हणाली,</p>

<p>पान नं. - 31</p>

<p>""अरे, चिमणीचे  पाय उठले आहेत, बघा, ह्या पिठावर. अस्त्राप
म्हातारी ! देवानं तिला चिमणीच्या जन्माला घातलं !""</p>

<p>        पुढं कित्येक वर्ष उघडया दारातून चिमण्या भरारत घरात आल्या
आणि फरशीवर नाचत कणदाणे टिपू लागल्या, की मला आईचा स्वर
आठवे :
        'अस््राप म्हातारी ! देवानं तिला चिमणीच्या जन्माला घातलं !'
----------------------------------------------------------------
पान नं. - 32
                                                3</p>

<p>बाईआजी गेली. माझ्या वडिलांचं आणि आईचं शांतवन करण्यासाठी
बरेच नातेवाईक आले. ज्यांच्या घोडीचा टाप माझ्यापेक्षा मोठया भावानं
गालावर झेलली होती, ते गावचे कुळकर्णी विनुतात्या. आणि सर्वांगाला
चिलमीच्या धुराचा वास येणारे त्यांचे बंधू बलदाडीचे दादा आले, हेड ~
कॉन्स्टेबल काका आले, पहिल्या महायुद्धात सरकारी नोकरीत भरती
झालेले माझे दुसरे चुलते-तात्या सोलापूराहून आले. बाईआजीचे भाऊ
आणि आक्काचे सासरे आले. आम्ही कुठं तरी परदेशी आलो आहोत,
अशी माझी समजूत झाली होती. त्यामुळं ही माणसं पाहून मी चकित
झालो. कारण हे चेहरे मी ह्या कौलारू छपराखाली कधी पाहीन, अशी
कल्पनाही मी केली नव्हती.</p>

<p>        आलेली ही माणसं निघून गेली आणि घर खायला उठलं.</p>

<p>        आईला ह्या ओसाडीतल्या घरात रात्री आपण राहू नये, असं वाटायला
लागलं. तिला कसले भास होऊ लागले. आम्ही भास म्हणतो, तिला
मात्र ते सारे सत्यच होते. हे भास तिला अवसेला किंवा पौर्णिमेलाच नेमके
होत.</p>

<p>        एकदा भर पौर्णिमेच्या रात्री, पाण्याची घागर घेऊन ती बाहेर पडली.
दार ओढून घेतलं. वळली आणि पडकापलीकडे उभी राहिलेली कोणी
अनोळखी बाई तिला सामोरी आली. तिनं केस पाठीवर मोकळे सोडले.</p>

<p>पान नं. 33</p>

<p>होते. कपाळावर मळवट भरला होका आणि हिरव्या रंगाचे कोरं वस्त्र ती
नेसली होती. नुकतंच न्हाणंधुण झाल्यासारखी ती दिसत होती. न्हाताना
अंगाला लावलेल्या उटण्याचा सूक्ष्म वास येत होता.</p>

<p>        आईच्या कानांवर आवाज आला :
        ""माघारी फीर. अजून मध्यानरात्र आहे.""
        आई निमूट मागे फिरली. दार ढकलून घरात आली. मोकळी घागर
आणि गडू खाली टाकून अंथरूणावर बसली आणि मोठयानं रामाचा धावा
करायला लागली.
        हुडहुडी भरल्याप्रमाणं तिचा आवाज कापत होता.
        माझे वडील जागे झाले आणि त्यांनी विचारलं,
        ""काय झालं ? इतकी घाबरी का झाली आहेस ?""
        तर म्हणाली,
        ""कंदील लावा.""
        वडलांनी कंदील लावला.
        ""बाहेर व्हा आणि पडकात बघा....""
        ""काय दिसलं ?""
        ""उंबऱ्याबाहेर उभं राहून तुम्हीच बघा.""
        वडलांनी बाहेर होऊन पाहिलं. स्वच्छ चांदणं पडलं होतं. कंदील धरून
बघण्याची काही जरुरीच नव्हती. चार पावलं पुढं होऊन त्यांनी सगळी ~
कडं नीट पाहिलं. स्वच्छ चांदणं आणि पडक्या भितांडाच्या,झुपाटाच्या
काळ्या सावल्या ह्याशिवाय त्यांना काही दिसलं नाही.
        ""मला तर काही दिसत नाही...""
        ""नाही ?""
        ""काही नाही....""
        ""मग आत या आणि दार लावून टाका.""
        दार लावून वडलांनी कंदील समोर धरला आणि आईला ते म्हणाले,
        ""तुला काय दिसलं, नाग ?""
        आई गंभीरपणानं , एक एक शब्द काळजीपूर्वक उच्चारत बोलली,
        ""ह्या वास्तूत कुणी तरी ओली बांळतीण अपघातानं गेली आहे.</p>

<p>पान नं. - 34</p>

<p>ती मला दिसली. म्हणाली, 'मागं फीर, अजून मध्यानरात्र आहे.'""
        ""मध्यानरात्र आहेच.""
        एवढं बोलून वडील गप्प पडून राहिले.
        रातकिडे आणि बेडकं तेवढी आवाज करत राहिली.</p>

<p>        ही हकीकत आईनं विठूकाकांना तपशिलवार सांगितली आणि ती
आम्हीही धडधडत्या काळजांनी ऐकली. ज्या दिवशी ऐकली, त्या
संध्याकाळपासून माझ्या सर्वांत मोठया तेरा वर्षांच्या भावानं घरी झोपणं
अजिबात सोडून दिलं. रात्री कितीही उशीर झाला, तरी जेवणं झाली,
हातावर पाणी पडलं, की तो पांघरायला एक चादर आणि अभ्यासाची
पुस्तकं घेऊन बाहेर पडे. कधी शाळेत, कधी आपल्या कुंभारआळीतल्या
मित्राच्या घरी, तर कधी जैन मित्राच्या माडीवर झोपायला होता.</p>

<p>        घरात झोपायचा त्यानं धसकाच होता.
        काळोख पडला, की मी वडलांच्या पुढयात झोपत असे. माझ्यापेक्षा मोठा
भाऊ आईला बिलगत असे. अगदी धाकटा भाऊ दोन वर्षांचा होता.
तो आईच्या मांडीवर झोपून जाई.</p>

<p>        माझ्या हा धाकटा भाऊ गोरापान होता, पण ह्या भिकार गावात
आल्यापासून त्याचे दोन्ही पाय कसल्या तरी अनामिक त्वचारोगानं भरून
गेले होते. 'आगपैण' असं ह्या आजाराला आईच्या कोशातलं नाव होतं.
रात्री ह्या पायांना फार खाज सुटे. ते ओचकारून ओचकारून  हा रक्त ~
बंबाळ करून टाकी. जखमा अधिकच चिघळत. आई नाना उपचार करत
होती. अमुक वस्तू उगाळून लेप दे, तमूक झाडपाला वाटून लाव, हे
जाळून त्याची पूड कडू तेलात खलून लाव, असे तिचे नाना उपाय चालू
होते; पण कशाचा गुण येत नव्हता.</p>

<p>        मग आईनं जुन्या धोतराच्या दोन लहान पिशव्या शिवल्या. त्यांना
बंद लावले. दोन्ही हातांनी ह्या पिशव्या घालून , बंद बांधून ती पोराला
माझ्या पुढयात बसवायची आणि म्हणायची,</p>

<p>        ""ह्याला सांभाळ. रडू देऊ नकोस.""</p>

<p>पान नं. - 35</p>

<p>        -आणि ती आपल्या कामाधामासाठी बाहेर जायची.</p>

<p>        बापुडवाणं बसून ते पोर बारीक रडायचं. वरचेवर दोन्ही हात माझ्या
पुढं करून 'हे काढून टाक' म्हणून मला विनवायचं आणि रडायचं.
        मला फार वाईट वाटायचं.
        मी कुठंही बाहेर गेलो, तरी त्याला करमावं, म्हणून पाठकुळी घेऊन
जाई.</p>

<p>        वंताच्या वडलांनी एकदा माझ्या भावाचे पाय निरखून बघितले आणि
ते गंभीरपणानं म्हणाले,</p>

<p>        ""अरे, तुझ्या आईला म्हणावं, कोमट पाण्यानं ह्या पायावरच्या जखमा
रोज स्वच्छ धूत जा. त्यांवर धूळ, माश्या, बसू देऊ नका. असेच चिघळत
राहिले, तर दोन्ही पाय निकामी होतील ह्या बाळाचे.""</p>

<p>        मग माझ्या लहानग्या भावाला आई धुणं-भांडी करायच्या वेळी
आपल्याबरोबर ओढयाला नेऊ लागली.</p>

<p>        चांगली उन्हं पडल्यावर ओढयाची खळखळ वाहणारी धार कोमटच
असते. तिच्यात ती उघडा करून त्याला बसवी आणि शेजारी स्वत:
धुणंपाणी करी. पाण्यात खेळायला मिळाल्यामुळं माझा भाऊ आनंदात बसे,
पाखरासारखं पाणी अंगावर उडवून घेत खिदळे.</p>

<p>        बराच वेळ धारेत बसल्यामुळं त्याचे दोन्ही पाय आपोआप स्वच्छ
धुऊन निघत.
        माझी आंघोळ रोज ओढयावर असे. आई म्हणे,
        ""त्यालाही ने आणि पाण्यात बसव.""</p>

<p>        कधीकधी घरून निघताना मी साबणाची वडी घेऊन जाई. भरपूर फेस
काढून माझी शर्ट-चड्डी, भावाची शर्ट-चड्डी खडकावर धूत असे आणि
तापलेल्या वाळूवर सुकायला टाकत असे. मग आम्ही दोघेही भाऊ , जसे
आईच्या पोटातून बाहेर आलो, तसे नागडे पाण्यात डुंबत राहत असू.
आमचा वेळ फार मजेत जाई.</p>

<p>        तेव्हा  काही ध्यानात आलं नाही. पण पाण्याच्या धारेत बारीक
माशांचे थवे होते. ते भावाच्या पायांना डसत आणि दोन्ही पाय स्वच्छ
करत. खेळण्याच्या नादात ते त्याला मुळीच कळत नसे.</p>

<p>पान नं. - 36</p>

<p>आठ-पंधरा दिवसांतच पायावर खपल्या धरल्या. पोराच्या तोंडावर
टकळाई आली. ते मजेत चालू लागलं. बदाबदा पडू लागलं.
आई म्हणाली,
        ""हा पांगळा झाला असता, तर जन्मभर तुम्हांला आपले पाय त्याला
द्यावे लागले असते. पाटकुळी वागवावा लागला असता.""</p>

<p>
        वडलांना बढती मिळाली, म्हणजे काही विशेष फरक पडला नाही.
        मी एकवार जाऊनच बघितलं.
        कधी मला पुस्ती काढण्यासाठी कागद लागत. कधी 'राणी' निब हवं
असे, कधी शाईची पुडी हवी असे . ह्या वस्तू आणण्यासाठी मी
दुकानाऐवजी कचेरीत जात असे.</p>

<p>        पांढऱ्या रंगानं रंगवलेल्या सरकारी वाडयाचा प्रचंड लाकडी दरवाजा
आधी लागे . तिथं दोन शिपाई दोन्ही बाजूंना बंदुका घेऊन उभे असत.
ते मला अडवत नसत. थोडं समोर चालायचं. पुन्हा उजवीकडं वळायचं.
पांढऱ्या बोळातून बरंच चालायचं. डाव्या-उजव्या बाजूला फुलझाडं.
फुललेली असत. मग पुन्हा दरवाजा. पुन्हा पहारेकरी. आत प्रशस्त चौक.
देवडया, चार सोपे, तेलपाणी, प्यालेले काळ्या लाकडाचे प्रचंड खांब
तुळ्या, कडेपाट, तक्तपोशी. प्रत्येक दालनातून तांबडया रंगाची मोठी
जाजमं अंथरलेली. ओळीनं पांढऱ्या स्वच्छ गाद्या , तक्के. प्रत्येकाच्या
गादीपुढं लहान डेस्क. डावी-उजवीकडे फायलींचे ढीग. लाकडी रूळ
लाकडी ठोकळ्यांतून ठेवलेल्या चिनी मातीच्या दौती. एक काळी, एक
तांबडी. टाक, शिसपेनी, बारीक वाळू.</p>

<p>        माझे वडील वळणदार अक्षरात मोडी लिहीत आणि शाई वाळण्यासाठी
त्यावर बारीक वाळू टाकत.</p>

<p>        डोक्याला रुमाल, बंद गळ्याच्या शर्टावर पारशी फॅशनचा लांब कोट,
धोतर, असाच कचेरीत काम करणाऱ्या सगळ्यांचा पोशाख असे. काही ~
जण काळी टोपी आणि कॉलरवाला कोट घालणारेही होती. सगळे
खाली माना घालून काहीबाही लिहीत असत. कुणाकुणापुढं लोक</p>

<p>पान नं. - 37</p>

<p>बसलेले असत.
        वाडयाच्या पिवळ्या रंगाच्या भिंतीवर सुंदरसुंदर निसर्ग-चित्रं लाव ~
लेली होती.</p>

<p>        माझ्या वडलांच्या डोक्यावर खेडयातल्या गल्लीचा देखावा होता.</p>

<p>        पिवळं ऊन पडलं आहे. घराच्या निळ्या-जांभळ्या छाया रस्त्यावर
पडल्या आहेत. वडाच्या झाडाचा मोठा बुंधा, फांद्या, पारंब्या , त्याच्याखाली
जांभळ्या सावलीत पिवळ्या उन्हाचे ठिपके. एक सोडलेली बैलगाडी
आणि शांत बसलेले दोन बैल . एक पांढरा, दुसरा बाळा. चरणारा
कोंबडा आणि कोंबडया. कोंबडयाचा लाल तुरा, गळ्याखाली लोळ्या.</p>

<p>        हे सगळं पूर्वीसारखंच होतं. ज्या गादीवर पूर्वी माझे वडील चाळशी
लावून बसायचे, तिथंच, तसेच बसलेले होते.
        मी हळूच जाऊन जवळ बसलो,
        त्यांनी डोळे मोठ्ठे करून विचारलं,
        ""काय ?""
        मी मान हलवली.
        ""उगीच.""
        ते पुन्हा लिहायला लागले.
        रुळानं आखायला लागले.
        मी अगदी हळू आवाजात विचारलं,
        ""तुम्ही आता जेलर ना ?""
        ""हो. का , रे ?""
        ""उगीच .""
        म्हणजे कारकून आणि जेलर ह्यांत तसा काहीच फरक नव्हता.
        माझी निराशाच झाली.
        वडलांचं डेस्क बदललं नव्हतं. जागा बदलली नव्हती.
        चाळशीतनं माझ्याकडं बघून त्यांनी पुन्हा विचारलं,
        ""तुला काही पाहिजे का ?""
        ""नाही.""
        ""मग ?""</p>

<p>पान नं. - 38</p>

<p>        ""नुसताच आलोय्.""
        पण एवढयात कमरेला मोठ्ठा पट्टा बांधलेला शिपाई आला. मोजकी
पावलं टाकीत वडलांच्यासमोर उभा राहत खाड्कन् बुटावर बूट आपटून
त्यानं सलाम केला.</p>

<p>        वडलांनी खाली बघतच विचारलं,
        ""गिन्ती केली ?""
        ""जी, हां.""
        ""आलबेल ?""
        ""जी, हां. ""
        ""ठीक. जा.""</p>

<p>        पुन्हा एकवार खाड्कन् सलाम ठोकून शिपाई गेला.
        मला, माझे वडील फारच मोठे अंमलदार झाले, याची खात्री पटली.
        मधल्या सुट्टीत आलो होतो, पुन्हा उडया मारत शाळेकडे गेलो.</p>

<p>
        यानंतर आठवडा गेला असेल, नसेल. संध्याकाळी वडील कचेरीतून
आणि आईला म्हणाले,
        ""मला दिलेलं जेलरचं काम काढून घेण्याची मेहरबानी व्हावी आणि
मला पहिलीच कारकुनाची जागा परत मिळावी, असा अर्ज मी केला
आहे. शिरस्तेदार म्हणाले, 'अर्ज मंजूर होऊन येईल.'""
        आईचा चेहरा फटफटीत झाला.
        ""का, हो ? लोक बढती बढती म्हणून ऊर बडवतात आणि आपण
मिळालेली चांगली बढती सोडून दिली. तुम्हांला नाही काही लोभ
कशाचा, मला माहीत आहे ; पण आम्हांला आहे.""
        वडील म्हणाले,
        ""माझ्यासारख्या माणसाला ते खातं सांभाळणं अवघड आहे. रोज उठून
गुन्हेगारांची तोंडं बघावी लागतात. कुणीचोर, कुणी दरोडेखोर, कुणी
खुनी. परवाच्या दिवशी, आपल्याकडं हजामत करायला यायचा, त्या
न्हाव्यानं रात्री आपल्या बायकोचा गळा वस्तऱ्यानं कापला.""
        ""अगाई !""</p>

<p>पान नं. - 39</p>

<p>        ""हे काहीच नाही. आपल्या भाऊबंदापैकी एकाला बेडया घालून
माझ्यपुढं उभं केल्यावर मला काय वाटलं असेल ?""
        ""कोण, हो ?""
        ""रामूआप्पा.""
        ""खालच्या आळीचे, ते ? घोडयावरनं येतात, ते ?""
        ""हो, ते तलाठी होते बोरगावला. एक वर्षाचा गावचा सगळा वसूल
त्यांनी खाल्ला.""
        ""अरे, देवा ! काय बुद्धी झाली त्यांना ! पोरं लहानधाकटी आहेत
की, हो !""
        सगळं खरं. विनाशकाळी विपरीत बुद्धी. माझ्या डोळ्यादेखत त्यांना
जेलचे कपडे दिले. हातापायांत बेडया अडकवल्या. मलाच त्यांचा रोजचा
शिधा मंजूर करायला लागला.समोरासमोर गाठ पडताच, ओरडून मला म्हणाले,
        ""'डिगांबर ! अरे, एका कुळातले आपण ! तुझं लग्न मी ठरवलं, ह्या
हातांनी मी तुझ्या डोक्यावर तांदूळ टाकले, ह्या पायांनी तुझ्या सासुर ~
वाडीला हेलपाटे घातले. माझ्या हातापायांत बेडया अडकवताना तुला
बघवतं कसं ?'
        ""काय बोलणार मी ? त्याक्षणी मनात निश्चय केला, की ही बढती
नको.
        ""हो. कुणाला ठाऊक, उद्या माझ्यापुढं सख्खा भाऊसुद्धा उभा
राहील ; आणि त्याच्याही हातापायांत बेडया घालण्याची पाळी माझ्यावरच
येईल.""</p>

<p>        माझ्या वडलांनी हा निर्णय घेताना मानसिक क्लेश झाले असणारच.
कधी काळी त्यांना ही अपूर्वाईची जागा मिळाली होती. थोडा अधिकार
मिळाला होता. तो आपणहून त्यांनी सोडून दिला होता.</p>

<p>        आई फार कष्टी झाली. मूकपणानं ती घरात वावरत राहिली. तिची
मन:स्थिती लक्षात येऊन वडलांनी अखेर म्हटलं,
        ""माझी काही चूक झाली का ? अर्ज देण्याआधी तुला सांगायला
पाहिजे होतं का ?""</p>

<p>पान नं. - 40</p>

<p>आई म्हणाली,
        ""माझ्या नशिबातच जर सुख नसलं, तर ते मला कसं मिळणार ?
तुमची काही चूक नाही. माझेच भोग अजून भोगून संपले नाहीत !""
        ही गोष्ट काही खोटी नव्हती. आईला अजून मोठ्ठा भोग भोगून
संपवायचा होता.</p>

<p>पान नं. - 41</p>

<p>        आईचे बाळपण फार सुखात गेलं होतं. तिचे वडील गावचे प्रतिष्ठित
असे कुलकर्णी होते. शेतीवाडी भरपूर होती. अठरा एकर तर विहीर ~
बागाईतच होते. आणि मुलं ही फक्त दोनच. एक माझी आई आणि
दुसरी माझी मावशी. पोटी पुत्रसंतान नाही, म्हणून तिचे आईवडील कष्टी
झाल्याचं निदान माझ्या आईच्या तरी ध्यानी आलं नव्हतं. उलट , ते
ह्या मोठया मुलीलाच मुलगा समजत. त्यामुळंच बाळपण लाडात, वांडपणा
करण्यात गेलं होतं.</p>

<p>        कधीकधी, तिला आपल्या बाराबंदी घालणाऱ्या, भव्य छातीच्या
मायाळू वडलांची आणि नाजूक बाहुलीप्रमाणे दिसणाऱ्या , गोऱ्यापान
आईची आठवण झाली, म्हणजे समईच्या मंद उजेडात, अंथरुणावर बसून
ती आम्हांला आपलं बाळपण सांगायची.</p>

<p>        हुमणदांडग्या अशा गायकवाडांनी भरलेल्या त्या लहानशा गावात
कुलकर्ण्याची केवळ दोनच घरं होती. पाच-चार वर्षांतून गावात एखादा
तरी खून होई. हे खून पिढीजात वैरातून होत आणि एकदा झाले, की
पुढे प्रत्येक खून म्हणजे एक वेगळी चित्तथरारक सूडकथा असे. हा खून
कधीही लपून छपून किंवा दगा-फटक्यानं होत नसे. होळीची दंगल ,
दसऱ्याची सोनं लुटण्यासाठी गावाबाहेरच्या माळावर जमलेली गर्दी ,
असा सांस्कृतिक समारंभ गाठून झडलेली ही एक लहानशी चकमकच
असे. भाले आणि कुऱ्हाडी ही ह्या चकमकीतील हत्यारं असत आणि
ज्याचा मुडदा पडे, त्या माणसाला फरशी कुऱ्हाडीनं तोडला जाई , रक्ताची
रंगपंचमी होई. ह्यापैकी काही खून दडपले जात, तर काहींसाठी वर्षा ~
नुवर्ष खालची कोर्टं, वरची कोर्टं होत. पुराव्याअभावी गुन्हेगार मोकळे
सुटत. गावात परत येत आणि वैराच्या जुन्या जमिनीत सूडाचं नवं पीक
 वाढू लागे.</p>

<p>        शहरवस्तीपासून पार दूर आत, फोंडया माळरानाच्या गराडयात अस ~
लेल्या ह्या एवढयाश्या गावातल्या रहिवाशांना अंगातली रग भागविण्याची
ही एकच पद्धत माहीत होती.</p>

<p>        गावात दगडांनी रचलेले आणि पांढऱ्या मातीने लिंपलेले मोठमोठे</p>

<p>पान नं. - 42</p>

<p>वाडे होते. एक-दोन बरे रस्ते, नाहीतर सगळे बोळचोळच होते. दुभत्या
गाई आणि खिलारी बैलजोडया, मेंढरांची खाण्ड आणि कडक स्वभावाची
झिप्री कुत्री , काळ्याकरंद जमिनी आणि निळ्या निळ्या पाण्यानं तोंडापर्यंत
भरलेल्या बांधीव विहिरी, आंब्याची, जांभळीची , कवठाची आणि चिंचेची
झाडी आणि मोठमोठी बाभुळवनं, बारा महिने तेरा काळ खळखळा वाह ~
णारा गाव-ओढा , असं ह्या गावाचं रूप होतं.</p>

<p>
        अशा ह्या गावात गायकवाडांच्या पोरींबरोबर विटीदांडू आणि
चिंध्यांच्या चेंडूनं धबाधबी खेळण्यात , हिरव्या पिकातून बागडण्यात
आणि झिम्मा-फुगडी, नाच-फेर करण्यात , झाडांना बांधलेल्या झोक्यावर
धरतीकडून आभाळाकडे झोके चढवण्यात आईचं बाळपण संपतं, न
संपतं, तोच तिचं लग्न होऊन वयाच्या तेराव्या वर्षी ती आमच्या गावी
आली होती.</p>

<p>        ती सांगे,
        ""माझा सासरा फार कर्तृत्ववान पुरुष होता. फार ढाणक होता; पण
त्यानं मला पोटच्या मुलीसारख्या.</p>

<p>        सासरे होते, तोवर घरात सुख होतं, समृद्धी होती. शेतमळे अमाप
पिकत होते. दूधदुभत्याची चंगळ होती. गडीमाणसांचा राबता होता.
 वाडयातल्या चौकात घोडी होती. दुभत्या गाई गोठयात होत्या . सासऱ्यांचा
मोठा दरारा होता. पंचक्रोशीत त्यांचं नाव कर्तबगार कुलकर्णी म्हणून
गाजत होतं.</p>

<p>        आई सागांयची,
         ""त्यांना दगा झाला. वैऱ्यानं कुणी मूठ मारली. करणी केली, रे
माझ्या पहिल्या मुलाचं तोंड त्यांनी पाहिलं आणि अगदी उठाउठी गेले.
संध्याकाळी दिवेलागणीच्या वेळी घोडयावरनं आले शेताकडनं आणि
एकाएकी रक्ताची गुळणी आली. तासाभरात तर गेले. संपलं सगळं. ते
गेले आणि मागोमाग लक्ष्मीही गेली घरातली. गुरंढोरं गेली., शेतीजमिनी
पिकेनाशा झाल्या दुष्काळ आले. विहिरी आटल्या. सगळं होत्याचं</p>

<p></p>

<p>पान नं. - 43</p>

<p>नव्हतं झालं !""</p>

<p>पुढं लवकरच तिचे वडील आणि आईही गेली.</p>

<p>
        माहेरच्या जमिनीपैकी अर्धी लेकवासरानं आईला मिळाली होती; पण
ती करणार कोण ? वाटेकऱ्याला लावली होती. तो दुसऱ्याची जमीन
घाम गाळून कशाला करेल ? हळूहळू त्या जमिनीही पडीक पडू
लागल्या. जेमतेम धान्य येऊ लागलं. तेही खंडीभर पिकलं, तरी मणभर
पिकलं, असं सांगून त्यातलं सहा पायल्या मालकिणीच्या दारात येऊ
लागलं.</p>

<p>        सासरे आणि वडील ह्या दोघांचही अमाप कर्तृत्व आईनं पाहिलं होतं.
त्यामुळं शांत, धीम्या स्वभावाच्या आमच्या वडलांच्यावर तिचा मनातून
फार राग होता. त्यांचा उल्लेख ती नेहमी 'तुकारामबोवा' असा
करायची. त्यांची, त्यांची ही महिना तीस-बत्त्तीस रुपये देणारी नोकरी, ही
ताबेदारी, आपलं गाव सोडून बेलदाराप्रमाणे पोटामागं हिंडणं हे तिला
मुळीच आवडायचं नाही; पण तिचा आत्मविश्वासही मोठा होता
म्हणायची,
        ""आज माझी पोरं लहानधाकटी आहेत, तोवर मला सगळं सोसलं
पाहिजे. उद्या मोठी झाली, म्हणजे ती मला सोन्यारुपानं न्हाऊ
घालतील ! माझीच पोरं आहेत ती, ती चांगलीच होतील पुढं !""</p>

<p>
        'ओढयाकाठचं हे अपेशी घर नको, नको !' असं आई सारखं घोकत
होती. इथं आल्या आल्या बाईआजी मरून गेली होती. इथं आल्या
आल्या मोठया थोरल्या 'लांबडयानं' आईला दर्शन दिलं होतं. इथं
आल्या आल्या भर मध्यानरात्री तिला पडकात उभी राहिलेली, केस
मोकळे सोडलेली आणि कपाळभर कुंकू लावलेली बाई दिसली होती. ती
आईशी बोलली, 'ह्या निपुत्रिकाच्या वास्तूत राहून माझं काही</p>

<p>पान नं. - 44</p>

<p>भलं कधी व्हायचं नाही,' अशी आईची पक्की खात्रीच झाली होती. पण
जी गोष्ट आईनं इतकी जिवाला लावून घेतली होती, ती माझ्या वडलांना
फार तातडीची मुळीच वाटत नव्हती.
        ते म्हणायचे,
        ""घरावर काय असतं ? सगळं राहणाऱ्यावरच आहे. आपण धीरानं
राहावं. गडबडून जाऊ नये. मन चंगा, तो काटवटमे गंगा !""</p>

<p>        काटवट म्हणजे लाकडी परात. पावित्र्य जर मनातच असलं, तर
गंगेचा प्रवाह शोधत मैलोगणती जायला नकोच. ती, घरातल्या , स्त्रीनं
खरकटी परात विसळण्यासाठी तिच्यात ओतलेल्या आणि पिठानं पांढरट
झालेल्या पाण्यातसुद्धा दिसेल.</p>

<p>
        कोणत्या कारणानं, कोण जाणे, पण ह्या घऱातच आईला अर्धशिशीचा
भयंकर आजार सुरू झाला. नाना औषधं झाली, पण तो काही बरा
झाला नाही. जसा जसा सूर्य चढे, तशी तशी तिची अर्धशिशी चढे
आणि त्या वेदनेनं माझी आई वेडीपिशी होई. असह्य झालं, म्हणजे
शेणानं सारवलेल्या गार जमिनीवर ती तास न् तास पडून राही.</p>

<p>        तिसऱ्या प्रहरी हळूहळू तिच्या वेदना कमी होत आणि ती उठून
कामाला लागे.</p>

<p>        पदरानं डोकं आवळून कण्हत पडलेली आई, हे घरातलं द्दश्य अनेक
वर्षं बदललं नाही.</p>

<p>        आई त्याच्यासाठी नाना उपाय करी. खुरपं लाल तापवून जनावरांना
डागतात, तसे तिनं भुवयांवर डाग सुद्धा घेतले होते; पण काही उपयोग
झाला नाही . अति झालं, म्हणजे घरातल्या भिंतीशेजारी उभी राहून ती
आपलं दुखरं कपाळ भिंतीवर दाणदाण आपटून घेई. म्हणे,
        ""ह्यापेक्षा मला मरण का येत नाही ? किती काळ मी हे सहन
करू ?""</p>

<p>        ह्या वेदनांमुळं तिच्या डोळ्यांतून पाण्याच्या धारा लागत.
डोके आकसून बारीक होत. अशा वेळी आम्हां मुलांचे चेहरे चिमणी ~
एवढे होते. भेदरल्या मनानं आम्ही आईच्या उशाशी चूप बसून राहत</p>

<p>पान नं. - 45</p>

<p>असू.</p>

<p>        असं डोकं चढलं असताना एकदा ऐन जेवणवेळेला कोणी गोसावी
भिक्षा मागायला आला. दारात उभा राहून मोठयांदा म्हणाला,
        ""जयशंकर भोलेनाथ !""
        माझी आई मनानं फार धार्मिक बाई होती. दारात आलेला भिक्षेकरी
काही न घेता परत जाणं तिला मुळीच चालत नसे.
        गोसाव्याची हाक ऐकून आई उठळी. बुचकुलीभर पीठ घेऊन
उंबऱ्याशी आली. भगवा गोसावी काखेची झोळी फाकीत दोन पावलं
पुढं झाला.
        पीठ झोळीत टाकून आई म्हणाली,
        ""बाबा, काही विद्या शिकला आहेस, का नुसत्याच दाढीजटा
वाढवल्या आहेस ?""
        ""कसली विद्या सांगू , माई ?""
        ""अरे, माझी अर्धशिशी जाईल, असा झाडपाला मला सांग.""
        गोसावी म्हणाला,
        ""मी तोडगा करतो. करून घेशील का ?""
        ""हो, घेईन. कर.""
        मग गोसाव्यानं जाऊन पिंपरणीची चार पानं तोडून आणली. त्यांतलं
एक पाण्यानं स्वच्छ धुतलं. ताठ अशी त्याची सुरळी केली आणि
आईच्या उजव्या नाकपुडीत ती घालून, आपल्या दणकट बोटांनी खालून
एकच टिचकी मारली.</p>

<p>        त्यासरशी नाकातून काळसर रक्ताची धार जमिनीवर लागली.
गोसाव्यानं मग तिच्या माथ्यावर थंड पाणी थापलं.
        रक्त बंद झालं आणि अर्धशिशी थांबली.</p>

<p>
        आई हा प्रकार सांगून म्हणे,
        ""पुढं बघ, कित्येक महिने माझं कपाळं दुखलं नाही. त्यावर पुन्हा
तो जटावालाही कधी दिसला नाही.""</p>

<p>
पान नं. - 46</p>

<p>        पण आईला वाटलेलं हे बरं तात्पुरतंच ठरलं. अर्धशिशीनं तिची सोबत
उभा जन्म केली.</p>

<p>        ओढयाच्या पलीकडं झाडाच्या गर्दीत असलेल्या मठात एक साधुपुरुष
होता किंवा साधू होण्याची खटपट मनापासून करणारा असा माणूस
होता. त्याच्या दाढीचे बरेच केस सफेत झाले होते; पण अंगानं तो
काही फार थकलेला दिसत नसे.</p>

<p>        दर आठवडयाच्या गुरुवारी पीठ मागण्यासाठी तो ओढा ओलांडून
गावात येई. जी काही दहा घरं मागे , त्यांत आमचंही एक असे. चार
सामान्य गोसाव्यांच्या अंगात असतो, तसलाच भगवा पोशाख त्याचाही
असे; पण एका बाबतीत मात्र वेगळा होता. तो आपला हातात भोप ~
ळ्याचा कटोरा धरून नुसता उभा राही. आतबाहेर करता करता कुणाचं
जेव्हा लक्ष जाईल, तेव्हा त्याच्या कटोऱ्यात पिठाची बुचकली पडे. ते
पडलं, की आशीर्वादरूप असं काही मिशांतल्या मिशांत पुटपुटत,
पायांतल्या खडावा वाजवत तो चालू लागे.</p>

<p>        भिक्षा मागण्याच्या त्याच्या ह्या पद्धतीबद्दल आई मतभेद व्यक्त
करी.</p>

<p>        पीठ घालता घालता एकदा ती त्यालाच बोलली,
        ""तुझं तोंड बंद का ठेवतोस ? मुकाबिका आहेस का ? अरे, न
बोलता तिन्ही त्रिकाळ जरी असा अंगणात उभा राहिलास, तरी कुणी
तुला वाढणार नाही.""</p>

<p>        गोसावी हसला आणि निघून गेला. त्यानंतर आला , म्हणजे थोडा
वेळ वाट पाही आणि मग 'माई,माई.....' अशा हाकी मारी.</p>

<p>
        असा एकदा आळ्यावर आईनं त्याला जोत्यावर बसायला सांगितलं
माझे वडील कामासाठी राजधानीच्या गावी जाऊन दीड महिना उलटला
होता. दरम्यान आईनं फार हलाखी सोसली होती. उधारपाधार, उसनवार</p>

<p>पान नं. - 47</p>

<p>असं किती काळ चालणार ?
        गोसावी बसला, तेव्हा चौकटीला हात देऊन आई उंबऱ्यात उभी
राहिली. म्हणाली,
        ""बाबा, तुला बायका-पोरं आहेत का ?""
        ""नाही, माई. मी एकटा आहे.""
        ""एवढं पीठ मागतोस, तेवढं सगळं खातोस का ?""
        ""थोडं खातो. बाकी साठवतो. वाण्याला विकून टाकतो !""
        या खुलाशावर आई थोडी गप्प राहिली. तिनं आपली सगळी शक्ती
एकवटली. म्हणाली,
        ""माझ्या घरात आज पीठ नाही. पोरं भुकेलीत. वाण्याला घालतोस,
ते पीठ मला घाल.""</p>

<p>        गोसाव्याला वाटलं, ही बाई थट्टा करतेय्. कपाळावर ठळक कुंकू अस ~
लेला तिचा काळासावळा चेहरा गंभीर होता. गंभीरच नव्हे, तर त्यावर
वेदनाही होती. ह्या गोसाव्यानं दहा घरचं मागून आणलेलं पीठ भुकेल्या
मुलांना खाऊ घालणं ही गोष्ट तिला फार लाजिरवाणी वाटत असली
पाहिजे.</p>

<p>        संसाराचं असारपण जाणलेला ते भगवा माणूसही चार लेकरांच्या
आईची ही मागणी बघून हलला. त्याचा निबर चेहरा झोकाळून गेला.</p>

<p>        ""हां, माई !""
        एवढंच बोलून तो उठला. चालायला लागला.</p>

<p>        आई कुठं तरी पाहत उंबऱ्यातच उभी राहिली आणि मग कुठं तरी
बघत बसून राहिली.</p>

<p>        तिची ही कुठं तरी लागलेली द्दष्टी मला अगदी स्पष्ट आठवते, अनेक
वेळा तिला अशी एकटक बघत बसलेली मी पाहिली आहे,
        आपल्या सुखी बाळपणाकडं, आईबापांच्या सावलीत होती, त्या दिवसां ~
कडं ती पाहत असेल का ?
        नाही. मला वाटतं, ती त्या भूतकाळात जात नव्हती. तो तिनं केव्हाच
गंगार्पण केला होता.</p>

<p>पान नं. - 48</p>

<p>        ती वर्तमानकाळात आहे, असंही त्या बघण्यावरून वाटत नसे.
        खात्रीनं ती पुढंच पाहत होती. आणखी पंचवीस-तीस वर्षांपुढं
जाऊन मोठया झालेल्या मुलांचं कर्तृत्व ती बघत असावी. मुलांचे भरले
संसार तिला दिसत असावेत. सर्वत्र सुख आहे, समृद्धी आहे, अशा
काळात ती मनानं शिरत असावी. तिच्या त्या बघण्यावरून असाच
तर्क होई.</p>

<p>        तासाभरानं मठबुवा परत आला. पांढऱ्या कापडाची भलीमोठी थैली
त्यानं पीठ भरून आणली होती. ती उंबऱ्यावर ठेवून तो म्हणाला,
        ""घे, माई. जेवू देत तुझी मुलं !""
        भानावर येऊन आई जागची उठली. तिनं ते पीठ घेण्याअगोदर
म्हटलं,
        ""बाबा, रे , याचे पैसे द्यायला हवेत; पण ते आज माझ्यापाशी नाहीत.
पुढं देईन. चालेल का ?""
        आईच्या तोंडाकडं बघून गोसावी म्हणाला,
        ""सावकाश दे !""
        गोसाव्याच्या भलेपणानं वितळून जाऊन आई बोलली,
        ""काळ काही राहत नाही,. उद्या माझी मुलं लहानाची मोठी होतील.
चार मुलांचे आठ हात मिळवू लागतील. माझं घर भरेल. दुसऱ्याला
उचलून काही देण्याइतकी शक्ती माझ्यापाशी येईल....जगला-वाचलास,
तर माझी मुलं तुझ्या उपकाराची फेड करतील !""
        गोसाव्याला काय वाटलं, कोण जाणे, पण तो गंभीरपणे म्हणाला,
        ""अशी बळकट आशा मनात ठेवू नकोस. काही वेगळंही घडेल.""
        ""नाही घडणार. बाबा रे, माझे कष्ट कसे वाया जातील ? मुळीच
जाणार नाहीत.""</p>

<p>        हे तिचं निश्चयाचं बोलणं ऐकल्यावर गोसावी आणखीन काही भविष्य ~
वाणी बोलला नाही. गप्प बसून राहिला आणि मग निघून गेला.</p>

<p>        सगळा दिवस आई कुणाशी धड बोलली नाही. दुपारचा स्वयंपाक</p>

<p>
पान नं. - 49</p>

<p>करून तिनं आम्हांला जेवू घातलं. स्वत: जेवली नाही. आठवडयातले
तीन वार ती करायची; पण आज कसलाही उपवासाचा वार नसतानाही
ती जेवली नाही.</p>

<p>
        रात्री, होती ती दशमी आम्हांला वाढून तिनं भांडं रिकामं केलं
लक्ष्मीसाठी म्हणून नेहमी ती एखादी चौत शिल्लक ठेवीत असे. कायमची
पाठ फिरवून असलेली लक्ष्मी कधी चोरपावलांनी येईल आणि घरात
अन्नाचा कण नाही, हे बघताच निघून जाईल, म्हणून आमच्या तोंडातून
काढून ती कोर-अर्धी दशमी शिंक्यातील भांडयात ठेवीत असे; पण
आज तिनं तीही ठेवली नाही.
        मी विचारलं,
        ""आई तू जेवणार नाहीस ?""
        तिनं मानेनंच नकार दिला.
        ""लक्ष्मीला काही ठेवलं नाहील ?""
        ""नाही ठेवलं.""
        ""पण तू दुपारीही नाही जेवलीस.""
        खरकटयावरून शेणगोळा फिरवताच आई हळू आवाजात पुटपुटली,
        ""कशाला, रे, हा देह जगवायचा मी ?""</p>

<p>
        ह्याच खिंडारातल्या घऱात आम्ही राहत असताना गावात उंदीर पडू
लागले.</p>

<p>        राहत्या बिळातून झिंगल्यासारखं चालत उंदीर बाहेर उघडयावर येत.
ही, ती दिशा घेत. स्वत:भोवती प्रदक्षिणा घालत. त्यांच्या लांबोडक्या
मुस्काटावर लालभडक रक्ताचा थेंब दिसे आणि तोंड वासून ते मरून
पडत. गटारात, उकीरडयावर, रस्त्यावर, घरात, माळवदावर असे उंदीर पडू
लागले. ते कुजून घाण वास सुटला.</p>

<p>        -आणि 'आली, आली' म्हणेपर्यंत आगीप्रमाणे प्लेगची साथ
पसरली.</p>

<p>पान नं. - 50</p>

<p>        जांघेत काखेत गाठ आणि सणसणून ताप येऊन, मघा हिंडता
फिरता माणूस अंथरुणावर पडू लागला. बरळू लागला  . अठ्ठेचाळीस
तासांत यम त्याला नेऊ लागला.</p>

<p>        रोज एक-दोन, एक-दोन चिता ओढयात धडधडू लागल्या.
माणसं हवालदिल झाली, गावाला , स्मशानकळा आली. गाव फुटू
लागलं. बुडत्या जहाजावरचे लोक जसे धडाधड पाण्यात पडून किनाऱ्या ~
कडे जातात. तसे गाव सोडून लोक बाहेर पडू लागले. रानामाळात वस्त्या
घालून राहू लागले. शाळा बंद झाल्या. सरकारी कचेऱ्या बंद झाल्या
बाजारपेठ, आठवडा-बाजार बंद झाला. बाहेरचे लोक हया गावी येईनात
आणि ह्या गावातले लोक बाहेरगावी जाईनात. प्लेगच्या भीतीनं लोकांचे
चेहरे पांढरेफटक पडले. सरपण-गोवऱ्या यांचा तुटवडा पडला.</p>

<p>        पायऱ्यांजवळच्या खोलीत, आमचं आणि विठूकाकांच सरपण साठवलेलं
असे. वखारीतून आणलेली फोडीव लोकडं, शेणी, कोळशाची पोती इथं
रचून ठेवलेली होती.</p>

<p>        एके दिवशी सकाळी विठूकाका सरपणाची खोली उघडून आत गेले,
तर दारातच त्यांना, तोंडं वासलेले आणि पाय आखडलेले दोन उंदीर
उताणे मरून पडलेले दिसले.</p>

<p>        दार तसंच बंद करून उघडेवाघडे विठूकाका आमच्या घरी आले
आणि वडिलांना म्हणाले,
        ""पंत, प्लेग उंबऱ्याशी आला हो ! आता पळालेलं उत्तम !""
        वडील अंथरुणावर बसून भूपाळी म्हणत होते, ते काही वेळ गप्प
झाले. सावकाश म्हणाले,
        ""तुम्ही जाल आपल्या गावी. एकटे आहात. मी बिऱ्हाड उचलून
कुणीकडं जाऊ ?""
        ""मग काय राहणार असल्या साथीच्या खाईत ? अहो, पोरं आहेत
चार पदरात तुमच्या, पंत. देवावर हवाला, म्हणून भागायचं
नाही.""
        यावर वडील काही बोलले नाहीत. त्यांनी केवळ दीर्घ सुस्कारा</p>

<p>  *
  cont'd</p>

<p>----------------------------------------------------------------
</p></body></text></cesDoc>