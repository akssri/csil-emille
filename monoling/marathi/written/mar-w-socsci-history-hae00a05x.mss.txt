<cesDoc id="mar-w-socsci-history-hae00a05x.mss" lang="mar">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>mar-w-socsci-history-hae00a05x.mss.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-25</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>पेशवे घराण्यांचा इतिहास</h.title>
<h.author>प्र. "". ओ'</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - unknown</publisher>
<pubDate>****</pubDate>
</imprint>
<idno type="CIIL code">hae00a05x.mss</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page पुणे.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-25</date></creation>
<langUsage>Marathi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>एकूण पाने :- 11
FROM 22 TO  183
+
 पानन नं. 22</p>

<p> 'देशमुखीचा हक्कवाजिमा मालिकंबरने ठरवून दिला होता असे या दस्तऐवज जात (या विवेचनानंतरचा
संक्षिप्त लेखांक पहा. संपादक ) म्हटले आहे.  म्हणजे शक 1500 पासून शक 1530 पर्यंन्ताच्या अवधीत
दंडाराजपुरीच्या भट देशमुखांचा भटाकंडे देशमुखी होती असा अर्थ होता.  ह्याहीपूर्वी शक 1400 पासून
भटांकडे देशमुखी चालत होती. हे इतस्तत: दाखवून दिले आहे. '
शक 1632 मध्ये बाळाजीचे वजन राजकारणात त्या काली इतके वाढले होते. की, नुसती इच्छा
प्रदर्शित केली असता (चालले असते) तथापि हबशांच्या प्रांतातील देशमुख ह्या नात्याने हबशांकजे कदिम
देशमुखी चालविण्याकरीता अर्ज करणे केवळ शिरस्ता व संपद्राय म्हणून रीतसरच होते. इलतिमासात
बाळाजी म्हणतो, 'प्रगणे मजकूरची देशमुखी आपली कदिम असे. आपले वतन चालत नाही तर
मेहेरबान होऊन वतन दिल्हेयाने वतनावर बेटा एऊन चाकरी करील'
       7लेखांक 1
       8लेखांक 1
   बज्यानेब आमीलान हाल व इस्तकबाल प्रगणा प्रगणे मस्ड-नादगांव तालुके कोट जंजिरे दंडाराजपुरी
बीदानद सुरूसन्न इहिदे मयातेन व आलफ आं की बालाजी विश्वनाथ देसाई याणी इलतिमास लिहून
पाठविले की प्रगणे मजकुराची देशमुखी गेले याकरिता आपलेस वतन चालत नाही तर मलिक साहेबाचे जागा
साहेब आहेत. मेहरबान होऊन वतन दिल्याचे वतनावर बेटा येऊन चाकरी करील म्हणोन इलतिमास
पाठविली त्यावरून मेहेरबान होऊन प्रगणे मजकूरचे वतन देशमुखी मशारूलची कदिम सबब देऊन
कदिम मलिकंबरी हकलवाजिमा खातरेस आणून पेशजी देविला होता लारी 62./ रू.9 सवाबासष्ट
लाहारी नऊ रूके तो दूर करून हाली जदीद हकलवाजिमा रयतनिसबती करार केला आसे बी तपसील</p>

<p>
 वतनाचे खिजमत बदल पेशजी शंकराजी केशव नाईक पाठविला होता.  तो घाटावर गेला हली त्याचे
ऐवजी विश्वनाथ भट बहिरंभट मेदले पाठविले आहेत याचे हाताने वतनाची जमत घेऊन सदरहू
वरहुकूम साल बे साल आबाब करून मशरूलचे नावे खर्च लिहीत जाणे ताजे सनदेचा उजरू न करणे
नक्कल घेऊन  आसल फिराऊन देणे परवानगी हुजूर रसवानगी खुसे मा जबानी रामाजी महादु पाकर
अफराद रा छ माहे सवाल. '
                                        - वि.का. राजवाडे
(मूळ विवेचनाचा जरूरीपुरता संक्षिप्त भाग फक्त वर दिला आहे - संपादक)</p>

<p>सर रिचर्ड टेंपल यांचा अभिप्राय
 मुंबई इलाक्याचे एक गव्हर्नर सर रिचर्ड टेंपल (सन 1877-80 )आपल्या Orintal Experiemces
या ग्रंथात पेशवे कुलाच्या कर्तबगारीबद्दल पुढील अभिप्राय व्यक्त करतात. 'वंशशुध्दीसंबंधाने सूक्ष्म बंधने
पाळणारी जात पृथ्वीवर ब्राह्मणा इतकी दुसरी नाही.  अर्थात त्यांच्या ठिकाणी पुष्कळ चांगले गुण
आनुंवशिक रीतिने उतरलेले आढळतात. पेशव्यांना राज्यप्राप्ती झाल्यामुळे या गुणात काही एक कमतरता
उद््भवली नाही.  हे पहिल्या चार पेशव्यांच्या उदाहरणावरून स्पष्ट दिसून येते.  एका मागून एक
पराक्रमी चार पेशवे निर्माण झाले. त्यांच्या तोडीचे दुसरे उदाहरण समग्र हिंदू राजावलीत सापडत नाही,
तरी इतर राष्ट्रात असे चार पुरूष एकाच कुटुंबात झालेले दाखविता येतील काय असे कोणी विचारतील
तर मुसलमानातील मोगल बादशहांचा निर्देश करता येईल. राज्याची मुख्य अंगे चार-परराष्ट्रीय व्वहार,
लष्कर, अंतर्गत दिवाणी कारभार आणि दरबारी इतमाम. या चारही बाबतीत पेशव्यांचे नैपुण्य
  अप्रतिम दिसून आले. पसारा आवरण्याचे सामर्थ्य, हुकुमाची तामिली, कष्ट व संकटे सोसण्याची
ताकद अशा सर्व गुणांनी हे चार पेशवे विशेष स्मरणीय झाले.  चवथा पेशवा अल्पवयी असूनही गुणांत
कमी नव्हता.  त्याला अकाली मृत्यू आला नसता तर आणखी पुष्कळ अघटित पराक्रम त्याने करून
दाखविले असते. इंग्रजी लोक हिंदी लोकांविषयी बोलताना त्यांच्या दोषावर जसा भर देतात तितकाच
त्यांनी गुणावरही देणे उचित होय. '
-----------------------------------------------------------
प्रकरण पाचवे
 श्रीवर्धनचा वाडा</p>

<p>पान नं. 28</p>

<p> भट घराण्याकडे श्रीवर्धनची देशमुखी असल्यामुळे निदान चार पिढया जुना वाडा श्रीवर्धनला होता.  हा
मूळचा देशमुखी वाडा थोरले नानासाहेबाच्या कारकीर्दीत मोडकळीला आला होता.  या वाडयाबद्दल
अतिशय माहितीपूर्वक लेख जुन्या पिढीतील संशोधक श्री.शं. वत्स जोशी यांनी 'सह्याद्री' मासिकाच्या
बाजीराव विशेषांकात लिहिला (मे, सन 1940) होता तो येथे घेतला आहे.
तिसरे पेशवे बाळाजी बाजीराव ह्यांनी आपल्या 'वतनी जुन्या घरट्याच्या पहिल्याच जागी पुनर्रचना
करून टुमदार वाडा बांधला. बाळाजी विश्वनाथानंतर थोरले बाजीरावांची राजवट सर्व हिंदुस्थान हे
महाराष्ट्राच्या पराक्रमाची शाळा आहे, हे सिध्द करण्यातं व्यतीत झाली. त्यातच पेशव्यांचा वतनी प्रांत
जो कोकण तो सिद्दकडून हस्तगत झाला.  अर्थात त्यानंतर म्हणजे नानासाहेब पेशव्यांच्या कारकिर्दीत
त्यांना आपल्या मूळ घराकडे लक्ष देण शक्य झाले. त्यावेळच्या बांधणीतील चौथरा अविशिष्ट व
अनाच्छादित असा आपल्या वैभावाचे स्मरण देत   स्फुरण देत आज दर्शित होत आहे.</p>

<p>बाळाजी बाजीराव यांच्या रोजनिशीच्या पहिल्या भागात पृ. 109 वर ते सांगतात ' श्रीवर्धनास देशमुखीचा
जुना घरटा आहे, तेथे नवे घर बांधावे लागते त्यांस सामान 43 लहान मोठ्या तुळया 86 खांब 500
कडया व तितकेच सागवानी वासे इत्यादी 1329 लाकडे आणून देणे. ' ही आज्ञा वसई प्रांतांचे
सउभेदार शंकरजी केशव हयांस केली आहे आणि हे सामान आणण्यास आपल्या देशमुखी वतनातील
उत्पन्नातून खर्च करावा असेही तेथे सुचविले आहे व पुढील पृष्ठावर जिवाजी गणेश खासगीलवाले हयांस
या 'वतनी वाडया' वरील बांधकामासाठी गुजराथी कसवी सुतार इत्यादी कारागीर पुरवण्याबद्दल हुकूम
दिला आहे. हया हुकुमाप्रमाणे त्यावेळी  (सन 1750 ऑगस्ट) वाडा बांधला गेला.  वाडयाचा चौथरा
60 फूट लांब 40 फूट रूंद असून कमरेइतका उंच आहे.  चौथऱ्याचे चौफेर बांधकाम उत्तम चिरेबंदी
व सामान्यत: शाबूत आहे. चौथरा पूर्वाभिमुख आहे.  त्यास चार पायऱ्या सवा पाच फूट रूदींच्या अशा
प्रशस्त आहेत.  चौथऱ्यावर भिंती वगैरे बांधकाम आज उभे नाही.</p>

<p>    पान नं. 29</p>

<p>  फक्त एक दगडी हौद, दाराची सोय असलेला - दार बसवण्याचे नर शिल्लक आहेत असा- आहे.
हौदाजवळ एक कुंड आहे, जोत्यास उंचीमध्ये पूर्व अंगाला आठ आठ त्रिदली पदकशिलाखाली व वर
असा असून पायऱ्याच्या दोन्ही बाजूस षोडशदली दोन कमले उठावदार काढली आहेत.  चौथऱ्यावर
खांबाचे चार तळखडे 14 फुटांवर समांतर रेषेत पूर्व - पश्चिम आहेत.  जोत्याची पुरणी म्हणजे.
जमिनीतील रूंदी चांगली दोन हाताइतकी जाड आहे. चौथऱ्यात एकदोन ठिकाणी पिपंळाचे मूळ शिरून
चौथऱ्याचा भेद चालला आहे व पायऱ्यांचे धोंडे सरकले आहेत.</p>

<p>श्री लक्ष्मीनारायण हे देवस्थान आज श्रीवर्धन येथे 'श्रीलक्ष्मीनारायण' ह्याच नावाने ओळखले जाते.
श्रींमत पंतप्रधान पेशवे ह्याच्या कुलदेवतांची आणि ग्रमदेवतांची जी यादी भा. इ.स. मंडळाच्या
इतिवृतात ( शके 1835) पेशव्यंचे उपाध्ये कर्वे  व आप्त ओंकार ह्याच्या संग्रहात उपलब्ध झालेली
प्रसिध्द आहे.  त्या यादीतही ' लक्ष्मीनारायण' हेच नाव दिले आहे.  तथापि देवाच्या हातातील
आयुधक्रम पहाता ही मूर्ती 'श्रीधराची' आहे असे म्हणणे प्राप्त आहे.  मूर्तीशेजारी डावी कडे 'श्री'
 म्हणजे लक्ष्मी उभी आहे व त्यावरून 'लक्ष्मीनारायण' हे नाव रूढ झाले.  पण मूळ श्रीधराचीच मूर्ती
आहे हे अवश्य लक्षात घेतले पाहिजे व श्रीवर्धन हया ग्रामनामाशी त्याचा असलेला संबंध ध्यानांत येण्यासही
ते सोपे जाईल. मूर्तीच्या उजव्या अंगास पायाशी गरूड असून लक्ष्मी व गरूड यांच्या शेजारी
जयविजय दाखविले आहेत.  एकंदरीत मूर्तीशिल्प फार सुरेख  आहे, श्रीधराच्या अंगावरील दागिने
पाहिले म्हणजे नखशिखांत सालंकार मूर्ती कशी असते हे सहज लक्षात येईल.  मूर्तीच्या प्रभावळीत
दशावतार दाखविले आहे.  ही मूर्ती अकराव्या शतकातील आहे.  होयसळ शिल्पाची छाया या
मूर्तीशिल्पात दिसून येते. पायाच्या घोटयाजवळ नऊ पदवलये असून तेथेच प्रलंब हार दिसतो. तो
जानव्याप्रमाणे धारण केलेला आहे.''
ह्या लक्ष्मीनारायणाच्या मंदिराचा जीर्णोद्धार केल्याचा काष्ठलेख मंदिरातच आहे, मंदिर एकंदर सहा
खणी लाकडी असून त्यातील प्रवेशद्वाराकडून म्हणजे पूर्वेकडून दुसऱ्या भालेवर तुळईवर चार ओळीचा
एक लेख पुढीलप्रमाणे आहे :
 श्रीनारायणचरणितत्पर
धोंडु जोसि श्रीवर्धनपुर निरं
तर स (श) क 1696 जयानामा संवत्स (र) फा
ल्गुन वा 13 मेसतरी लाल
 ( टीप : 'श्रीवर्धनपूर' हा शब्द 'श्रीवर्धनकर' असा असावा. मोडी वाचनाचे चुकीमुळे 'पुर' लिहले
असण्याची शक्यता वाटते - संपादक).
हया धोंड जोशांचा उल्लेख इ. संग्रहातील 'पे. द. सनदादिकांची माहिती' ह्या विभागात पृ. 150 वर
दिसतो. पेशव्यांच्या ग्रामदेवता श्रीवर्धन येथेच कुसुमादेबी, सोमजाई, जिवनेश्वर व भैरव हया चार
आजही प्रसिध्द व नित्य पूजित अशा आहेत. सवाई माधवराव ह्यांच्या जन्मानिमित्त सर्वभर जे उत्सव
सभारंभ आणि देकार इत्यादी झाले.  त्यात श्रीवर्धन येथील कुसुमादेवी व स्थानपुरूष हे सोडून बाकी
देवांपुढे दक्षिणा ठेवल्याची नोंद आहे. असाच नोंदी आणखीही उचित प्रसंगी सापडतात.</p>

<p>पान नं. 30</p>

<p> ह्या नारायण देवस्थानाचा प्रकार चांगला प्रशस्त असून त्यात एक मोठी पुष्करणी आहे, प्रतिवर्षी येथे
तीन मोठे व एक छोटा असे चार उत्सव परिस्थितीनुसार सांप्रत साजरे होतात. त्यात एक थोरल्या
बाजीरावांचा स्मृत्युत्सव वैशाख शु. 13 स साजरा होतो. ह्या देवस्थानास 4 खंडी भात आणि जोशी
वाडीतील आगरांचे 75 रू. हे उत्पन्न सनदी आहे व वहिवाट पंचाकरवी होते.  पंचमंडळीत स्थानिक
ब्राह्मण प्रभु गृहस्थ आहेत.
श्री.य.न. केळकर यांचे संग्रहातून उपलब्ध झालेल्या नोंदीमध्ये श्रीवर्धनचा वाडा शाकारण्याबद्दलच्या
तजवीजीची एक नोंद मिळते-
 छ रजी समान सीतैन मया व अलफ
  (28 नोंव्हे. सन 1667 शनिवार)
 का श्रीवर्धन येथे सरकारचा वाडा  बांधला आहे त्यांस शाकारणीस सालगुबाजी गंगाधर बेलापूर
याजकडून कवले सत्तर हजार देविली होती त्यास त्याजकडील कवले सबब नेली नाही याजकरिता
हाली तुम्हाकडून नळे कवले फर्मासी सुमार 70,000 सत्तर हजार देविली असेत तरी प्रांतासाष्टी खर्च
लागेल तो देऊन कवले तयार करून श्रीवर्धनचे बंदरी पोहोचऊन देणे म्हणोन रामाजी महादेव यांस
रवानगी यादी सनद    1</p>

<p> का श्रीवर्धन येथे सरकारचा वाडा बांधला आहे त्यांस शाकारणीस सालगु कडील कवले  वाईट
सबब नेली नाही याजकरिता हाली तुम्हाकडून नळे कवले फर्मासी सुमार 70,000 सत्तर हजार देविली
असेत तरी प्रां साष्टी प्रा खर्च लागेल तो देऊन कवले तयार करून श्रीवर्धनचे बंदरी पोहोचऊ देणे
म्हणोन रामाजी महादेव यांस रवानगी यादी सनद   1
 का श्रीवर्धन येथे श्रीजीवनेश्वराचे देवालय सरकारातून बांधले आहे. त्याचे शाकारणी बा विटा सुमार
75000 देविल्या तरी वसई प्रांते खर्च लागेल तो देऊन विटा तयार करून गलबतावर  घालून
श्रीवर्धनास पोचोऊन देणे म्हणून विसाजी केशव यांस सनद   1
-----------------------------------------------------------------
प्रकरण 6.
 कुलदेवता -कुलाचार
 भट घराणे हे ऋग्वेदी कोकणस्थ (चित्पावन) ब्राम्हण घराणे. गोत्र गार्ग्य, सूत्र आश्वलायन,  शाखा
शाकल. गार्ग्य गोत्र  त्रिप्रवरी असून त्याचे प्रवर आंगिरस, ब्राहस्पत्य, भारद्वाज गोत्राचे प्रवरही हेच
आहेत.  चित्पावनांच्या 60 प्राचीन आडनांवाच्या यादीत 'भट' आडनाव नाही.  त्या यादीत गार्ग्य
गोत्रजांपैकी 1) कर्वे 2) गाडगीळ 3) लोंढे 4) माटे, 5) दाबके, हीच फक्त आडनावे आढळतात.
( भुस्कुटे = गाडगीळ), 'चित्पावन' पुस्तकात श्री. ना. गो. चापेकरांनी दिलेल्या गार्ग्य गोत्राच्या श्लोकात
22 आडनावे असून त्यात मात्र 'भट' हे आडनाव आहे, शाकल शाखीय गार्ग्य गोत्री घराण्यापैकी
कुठल्यातरी घराण्याच्या कुलवृत्तांतात हे गोत्र, शाखा, सूत्र यासंबंधी कदाचित सविस्तर माहिती आढळेल
म्हणून श्रीक्षेत्र हरिहरेश्वराचे माहात्म्य फार असे. ह्या स्थानाबद्दल पेशवे मंडळीची श्रध्दादेखील गाढ असे.
हरिहरेश्वरचा प्राचीन इतिहात पूर्णपणे कोठेही उपलब्ध नाही, असे वाटते. सन 1715 पासूनचा थोडातरी
इतिहास उपलब्ध आहे.  मूळ देवालये व तीर्थास जाण्याच्या पायऱ्या जावळीचे सुभेदार राजे
चंद्रराव मोरे यांनी बांधल्या छत्रपती शिवाजी महाराज हे येथे अनेकदा दर्शनाकरिता येत असत, शेवटले
दर्शन सन 1674 मध्ये झाले.  यानंतरचा दाखला मिळत नाही रामदास स्वामीही महाबळेश्वरचे दर्शन
करून हरिहरेश्वरचे दर्शन घेऊन गेल्याचा उल्लेख त्यांचे तीर्थाचे यादीत मिळतो.  धर्मवीर संभाजी
महाराज आपले वडीलांप्रमाणेच येथे दर्शनास येत इतकेच नव्हे तर यात्रेकरूस त्रास न होईल अशी
दक्षता ठेवीत.
 श्रीकाळभैरवाचे देवालयावरील कोरीव लेखावरून या देवालयाचा जीर्णोध्दार श्रींमत पहिले बाजीरावसाहेब
पेशवे यांनी  सन 1723 मध्ये केला हे कळते. श्रीमत कान्होजी आंग्रे यांनी या कामाकरिता एक हजार
रूपये श्रींमत पहिले बाजीरावसाहेब पेशवे यांचे हवाली केले.  श्रीकाळभैरवाचे देवालय 1793 साली
आगीने जळाले ते पुन्हा श्रीमंत पेशवे सरकारने बांधले. जीर्णोध्दार मंदिरातील लेख पुढीलप्रमाणे आहे :</p>

<p>पान नं. 32</p>

<p> कोरीव लेख  ( शक 1645 वैशाक शु.)
 1 इष्वाद्धि शोडशे  शाके !   शोभकृत वत्सरे प्रभु :!
 माधवे के शुक्ले !  प्रांरभे देवतालयम्् !!
 2 श्रीवर्धन देशाधिकृत ! बाळाजी विश्वनाथामिध !
   सुत विश्वनाथानत ! जीर्णोद्दार: कृता !!
 देवळाचे उजवे बाजूस समुद्रकाठी मोडून पडलेली ही धर्मशाळा सन 1749 मध्ये पुरी झाली.
चौघडयाची  इमारत श्रीमंती थोरले माधवरावसाहेब पेशवे यांचे कारकीर्दीत पुरी  झाली व त्यांनीच येथे
1769 मध्ये चौघडा सुरू केला तो निदान सन 1946 पर्यंत चालू असे.
 पेशवे सरकारातून या देवस्थानास महावस्त्रे (तिवेटी 3, दुपेटे 5, चिरा बादली 1. पटका 1, जरी खण
1,  भरजरी वस्त्रे देवीस 1, जरी पातळ 1, शेला 1, मंदील जरी 1, धोतरजोडी, देव पुसणेस
सुती शेला- किंमत 1030 ) 1030 रू.ची नित्यवस्त्रे  (लुगडी, तिवटे, फेटे, चिरा बदली 1, जरी
कापड 1, पटका 1, दुपेटा 1 धोतरजोडी 1 - किंमत रू. 260 आ. 8) मिळत असत.
नगारखान्यास रू. 2400 व नंदादीपास व नैवेद्यास रू. 2090 रू. 10 प्रतिवर्षी मिळत. शिवाय
3 खंडी भात, 17 मण मूग, 3 पायली उडीद, 6 शेर तूप, 6मण तेल, 20 नारळ, नळे, चंद्रज्योति व
नक्त रू. 31 श्रावण व कार्तिक मासाकरिता मिळत असत.  सर्व नोकरवर्गास घरे दिली होती.  ती
शाकारणेस पेंढा 2000 मिळे. देवावर पाणी घालणारे गुरवास वर्षात 3 खंडी भात व ब्राह्मण पुजाऱ्यास
दरमहा 5 मण भात शिवाय कापडचोपड, तसेच देव पुसण्यास खादी व शेला मिळत असे. येथे कोणास
मोठया देणग्या देणे त्या पेशवे सरकारामार्फतच द्याव्या लागत. अशा देणग्यांची नोंद पेशवे पहिले
बाजीरावसाहेब यांच्या रोजनिशीत नमूद आहे. चौघडयाचे व्यवस्थेकरीता एक स्वतंत्र कारकून असे.
बाकीची व्यवस्था श्रीवर्धनचे मामलेदार पहात. सेवेकरी व यात्रेकरी यास सर्वतोपरी पेशवे
सरकारातून मदत मिळत असे.  जरी जंजिऱ्यांचे नबाबसाहेब पेशव्यांबरोबर मित्रत्वाने असत तरी अनेक
बेजबाबदार सरदार मराठी मुलुखास त्रास देत  अशा वेळी हिंदू लोकास बाणकोट येथील इंग्रज रेसिडेंट
आश्रय देत असे.  सन. 1756 मध्ये इंग्रज व पेशव्याच्यांत तह झाला. त्या तहान्वये
कोकणीकिनाऱ्यावरील नऊ गावे बाणकोट, वेळास वगैरे पेशव्यांनी इंग्रजास दिली तेव्हा इंग्रजांनी
आग्रहपूर्वक श्रीक्षेत्र हरिहरेश्वरची मागणी केली होती पण रामजीपंत किल्लेदारांनी हिंदू जनता आपली
मुंडकी अर्पण करतील परंतु हरिहरेश्वर केव्हाही देणार नाहीत असे स्पष्ट सांगितले. एवढयाकरिता तह
मोडतो असे पाहून बऱ्याच वाटाघाटी नंतर इंग्रजांनी पडते घेतले. हे स्थान सर्व हिंदूस पूज्य
असून त्यातले त्यात पेशवे यास तर ते प्राणापलिकडे पूज्य वाटे. तेव्हा इंग्रजांनी हरेश्वरचा हटट
सोडला. पण हरेश्ररापेक्षा कितीतरी पटीने  जास्त उत्पन्नाचे गाव दासगाव पत्करले. नंतर हा तह बरेच
महिन्यानंतर तडीस गेला. ह्याप्रमाणे हे अंत्यत पूज्य स्थान प्राणापलिकडे अशिल हिंदूंनी अगदी
शेवटपर्यंन्त आपलेकडे राकले ते पुढे राज्यव्यवस्थेचे सोयीसाठी मुलुखाचे अदलाबदलीत सन 1828 मध्ये
हबशी सरकारकडे गेले.
 पेशवे दप्तरी या क्षेत्री अनेक लहानथोर व्यक्ती येऊन त्यांनी आपली पिंडा निवारण केल्याची बरीच
उदाहरणे आहेत.  पिडीतास व सेवेकऱ्यास सरकारी मदत असे  आजारीपणात श्रीमंत माधवराव
पेशवे ओरडत. त्यांची ही ओरड व क्लेश श्रीचा तीर्थप्रसाद मिळताच थांबली असा उल्लेख आहे.
 पेशवे कुलातील सर्व स्त्री-पुरूष व इतर सरदार येथे नेहमी दर्शनास येत व अतानात पैसा खर्च करीत
शेवटले बाजीराव साहेबांनी 5000 रू. शक 1733 ! 34 मध्ये परशुरामशास्त्री व गंगाधरपंत मराठे
यांचे मार्फत पाठवले. 1759 साली अतिरूद्र,महारूद्र, लघुरूद्र व सर्व ग्रहांचा जप इत्यादी करण्यासाठी
1029 रू. खर्च केला आहे. असे सहस्त्रावधी रूपये येथे खर्च केले जात. (हरिहरेश्वर संक्षिप्त वर्णन)
(भा. इ.स. मंडळ त्रै व. 19 अं. 4 ) हे क्षेत्र जरी देवघर नावाच्या खेडयात असले (कदाचित त्याचे
जवळ असावे) तरी त्या खेडयाचे नाव लुप्तप्राय असून ते खेडे 'हरेश्वर 'नावानेच पूर्वीपासून ओळखले
जाते. हरेश्वर हे आगबोटीचे बंदर आहे. हे स्थान अगदी समुद्रतीरावर आहे.  श्रीदेवी हरिहरेश्वर
काभैरवाचे स्थान 1/2 मैल डोंगराचे पायथ्याशी अगदी समुद्र काठी आहे.</p>

<p> प्रकरण आठवे
 व्यक्तिपरिचयात्मक वृत्तांत</p>

<p> पान नं. 50</p>

<p> महादाजी विसाजी :3) सन 1576 मध्ये हे दंडाराजपुरीचे देशमुख होते. यांच्यापूर्वीची माहिती मिळत
नाही.  यांचे वडिलांचे व आजोबांचे अनुक्रमे विसाजी व शिवाजी ही नावे, नातवाला आजोबांचे अनुक्रमे
विसाजी लक्षात घेऊन अंदाजाने वंशावळीत तूर्त योजिली आहेत. त्याला प्रमाण मात्र इतिहासात मिळालेले
नाही. किंबहुना महादाजीपंताच्या वडिलांचे व आजोबांचे नाव काय होते याची स्पष्ट माहिती मिळत
नाही.
  शिवाजी महादाजी : 4) हे छत्रपती शिवाजी महाराज्यांच्या चाकरीत असल्याची माहिती उपलब्ध आहे.
 यांचेकडे श्रीवर्धनचे देशमुखीचे व कुळकरणाचे वडिलोपार्जित काम असे. ""पेशवे यांचा मूळपुरूष
परशरामपंत भट हे प्रपंचाच्या तापत्रयामुळे शिवाजी राजे पुंडावा करीत होते, त्याजजवळ येऊन
करवीरचे मुक्कामी येऊन भेटले. सर्व हकीकत विदीत करोन खास कोठीवर राहिले. पुढे चिरंजीव
विश्वासराव हाही आले. त्यांचे कर्तृत्व पाहून राजे आणि सरदारी सांगितली. (म.द.रू.भा. 2)""
इतिहास संशोधक, यांचे नाव ""शिवाजी ऊर्फ परशुराम"" असे वंशावळीत देतात. यांचे एक नातू विठठल
विश्वनाथ ( थोरल्या बाजीरांवाचे सख्खे चुलते) हे आपल्या तीर्थोपाध्यांकडील लेखात, यांचे नाव
""विठ्ठल विश्वनाथ आजे अंताजीपंत "" असेही देतात. थोरल्या बाजीरावांच्या मातोश्री श्रीमंत
राधाबाईसाहेब यांच्या लेखातही यांचे नाव "" राधाबाई भ्रतार बाळाजीपंत सासरे विसाजीपंत अजे सासरे
आंताजीपंत  ""  असे लिहिले आहे. ""बाळाजी विश्वनाथ पेशवे चरित्र ले.चि.ग. गोगटे आवृत्ती सन
1907"" या पुस्तकात यांचे नाव 'जनार्दनपंत ""असे दिले आहे ते चूक असावे.
 विश्वनाथ शिवाजी : (5) हेही छत्रपती शिवाजी महाराजांचे चाकरीत असल्याची माहिती उपलब्ध आहे.
 यांचकडे दोन हजार स्वारपथकाची सरदारी होती. (म.द.रू.भा. 2) तसेच श्रीवर्धनाच्या देसमुखीचे व
कुळकरणाचेही काम होते (बाजीराव पेशवे चरित्र - ना.वि. बापट ) काही कारणाने भट मंडळीवर
हबशांची अवकृपा झाल्यावर सन 1678 च्या सुमाराल हबंशाकडून यांची श्रीवर्धनची देशमुखी जप्त
झाली.</p>

<p>
पान नं. 51</p>

<p> ""विश्वासराव"" या नावानेही यांचे उल्लेख सापडतात (म.द.रू. भा. 2) मुलगी : ही श्रीवर्धनच्या
कारभारात लक्ष घालत असे. (पे. द. 9 ) हिचे पति बहिरवभट मेहेंदळे. (नाना फडणीसांचे
आजोळाकडील पूर्वज).
 जनार्दन विश्वनाथ : (6) हे बाळाजी विश्नाथ यांचे एक सख्खे बंधू. ऐतिहासिक कागदपत्रात यांचे नाव
 ""जानो विश्वनाथ"" असे आढळते.  बाळाजीपंतांकडे गोवले, बोरलाई, मांडले, म्हसले या चार तंपाचे
(मूळ शब्द, तप= परगणा-महाराष्ट्र शब्दकोष) कुळकरणाचे वतन होते. हया वतनाचे काम जानो
विश्वनाथ सांभाळत ( ब्र स्वा चरित्र ) तसेच बाळाजी विश्वनाथ चिपळूणकडे गेल्यानंतर, हे
श्रीवर्धनलाच राहून आपले वडिलोपार्जित काम पहात असत. त्यांना ""जंजिऱ्यांच्या नबाबाने गोणीत
घालून बुडविले"" ही समजूत चुकीची आहे, असे इतिहाससंशोधनातून कालांतराने उपलब्ध झालेल्या
कागदपत्रातील माहितीवरून दिसते. (ज्यांना समुद्रात बुडवले त्यांचे नाव ""संभाजीपंत मोकाशी""
 शाहू चरित्र-हेडवाडकर यात दिले आहे.) यांच्याच हातची सन 1706 मधील एक नोंद त्रिबंकेश्वर
-नाशिकच्या क्षेत्रोपाध्यांच्या चोपडीत सापडली आहे. बंधु बाळाजीपंत, पेशवे झाल्यानंतर हे ""सुभेदार""
होते. भावाप्रमाणे हेही कतर्बगार पुरूष होते. शके 1623 मधील एका पत्रात यांना ""सरसुभेदार प्रांत
पुणे""असे म्हटले आहे. (म.द.रू.भाग 2) 1708 साली हे शाहू महाराजांच्या पदरी मोठया हुद्दयावर
होते.
नारायण जनार्दन :(7) पत्नी गोदावरी ही निदान सन 1790 पर्यंन्त ह्यात असावी.  कारण आंनदीबाई
स्वत: कोपरगावला कैदेत असताना, गोपिकाबाईचे पश्चात, गोदावरीबाईकडे (पे. द. 4) कुलाचारसंबंधी
विचारणा केलेली दिसते. आंनदीबाईच्या चरित्रात ""बाळाजी विश्वनाथ पेशव्यांच्या पुतण्यांची बायको""
 असा गोदावरीबाईचा उल्लेख आहे. (गोदुबाई ही बालाजी विश्वनाथ यांची थोरली भावजय असा उल्लेख
श्री.ग.ह.खरे "" शनवारवाडा"" या पुस्तिकेत करतात, तो कसा ? हे समजले नाही. )
 विठठल विश्वनाथ:(6) हे बाळाजी विश्वनाथ यांचे धाकटे बंधू (सोयीसाठी यांची माहिती बालाजीच्या
माहितीच्या पूर्वी दिली आहे)हे महाडला कारखानीस होते. (पे.द. 44) नाशिकला तीर्थोपाध्यायांकडे यांचे
लेक आहेत.  शके 1601 (सन 1679) मध्ये यांनी उल्लेख ""श्रीवर्धन देसाई"" असा केला आहे.
 बालाजी विश्वनाथ :(6) हे भट घराण्यातील पहिले पेशवे. सर्व उपलब्ध माहितीवरून यांचा जन्म सन
1660 मध्ये झाला असावा. श्रीवर्धन हेच यांचे जन्मस्थळ (कुलाबा गॅझेटिअर आ. 1964) यांचे मूळ
नाव बाळकृष्ण (बा. वि. पेशवे चरित्र-बापट ) सन 1678 च्या सुमाराला या घराण्याची देशमुखी
हबशांकडून जप्त झाली असावी असे उल्लेख कागदपत्रात मिळतात. चिपळूण दाभोळकडे वास्तव्य
असतानाच बाळाजीचे लग्न झाले. चिपळूण  बंदर त्यावेळी जंजिऱ्याच्या हबशांच्या ताब्यात होते.</p>

<p>पान नं. 52</p>

<p> त्यांच्या तर्फेने चिपळूणचा सुभा आवजी बल्लाळ यांच्याकडे होता व मीठबंदराचा हवालाबाळाजी
विश्वनाथ भट श्रीवर्धनकर यांचेकडे होता. (ब्र. स्वा. चरित्र) मीठबंदराचा हवाला व चिपळूणचे
जकातीचे नाके घेतल्यामुळे बाळाजीपंत चिपळूणला रहात असत. (बा. वि. पेशवे च.- गोगटे)पुढे
बाळाजीपंत दाभोळचे सभासद होते. (देशमुख दंडाराजपुरी, अधिकारी श्रीवर्धन व सभासद दाभोळ शके
1618, आशा नोंदी मिळतात. - भा. इ.स. मं. शके 1835) बाळाजी पंतानी देशावर तसेच
चिपळूणकडे स्थलांतर केले त्याबद्दल अनेक उपपत्ती दिल्या जातात. बा. वि. पेशवे चरित्र, ले. चिं. गं.
गोगटे या पुस्तकात पुढीलप्रमाणे माहिती दिली आहे. -""बाळाजीपंतानी श्रीवर्धन सोडले त्यावेळी वेळास या
गावी हरि महादेव, बाळाजी महादेव, रामाजी महादेव असे तीन भानू कुलोत्पन्न बंधू रहात असत.
त्यांचा व बाळाजीपंतांचा लहानपणापासून स्नेह असे. वेळासला जाऊन बाळाजींनी, श्रीवर्धन सोडण्याचा
बेत भानू बंधूंना सांगितला. भानू बंधूही बाळाजीबरोबर यायला निघाले. बाळाजीपंत तिवरा घाटाने देशावर
यायला निघाले  बाळाजीपंत चिपळूणला येताच तोच श्रीवर्धनमधून ते पळून गेल्याची गोष्ट हबशांस
समजली. हबशांने संतापाने बाळाजीस पकडून आणण्याबद्दल अंजणवेलच्या किल्लेदारास  हुकूम दिला.
किल्लेदाराने बाळाजीचा पाठलाग करून त्यास कैद केले व अंजणवेलीस आणले. हबशाने, ह्यास
अंजनवेलीच्या किल्ल्यात कैदेत ठेवण्याचा हुकूम किल्लेदाराला दिला. त्याठिकाणी बाळाजी सुमारे 25
दिवस कैदेत होते.  या काळात त्रिवर्ग भानूनी मोठी खटपट करून किल्लेदारास लाच देऊन नानांची
(बाळाजीपंत) मुक्तता केली. नंतर भानूंसह बाळाजी, अंबाजी त्रिबंक पुरंदरे यांचेकडे सासवडला येऊन
पोहोचले. बाळाजींनी अंबाजीस आपला बेत कळवला. तो बेत अंबाजीस पसंत पडून अंबाजी, बालाजी,
भानू बंधू साताऱ्याला येऊन पोहोचले. काही दिवस अंबाजी व बाळाजी हे मक्तयाच्या मामलती करून
मोगलाईत 1000 - 500 स्वार बाळगून होते. (याचा सविस्तर तपशील भानू फडणीस घराण्यातील एका
गृहस्थाने सन 1792 मध्ये लिहिलेल्या वृत्तांतात आहे. मराठी ज्ञानप्रसारक पुस्तक 16, अंक 8,
ऑगस्ट सन 1865 पृ. 236-242) ""साताऱ्याला, (तळटीप : बाळाजीपंत देशावर येण्याचे एक कारण
असे की, ते मीठबंदराच्या कामावर असताना एकेकाळी जरूर तेवढे मीठ जमा झाले नाही म्हणजे
पिकले नाही. त्यामुळे सिद्दीच्या ऐवजाचा फडशा झाला नाही या जबाबदारीला जामीन असलेल्या
संभाजीपंत मोकाशी यांना सिद्दीने संतापून समुद्रात बुडवून ठार मारले. यावेळी अनेकांचा सिद्दीकडून
अनन्वित छळ झाला होता. - पे.द. 7-1, 2 व 40 शाहू चरित्र-हेरवाडकर),
 बाळाजीपंत कृष्ण जोशांमार्फत शंकराजीपंत सचीवांकडे चाकरी केली. चिपळूणला असताना
बाळाजीपंतांचा ब्रह्मेंद्रस्वामी धावडशीकरांशी संपर्क आला होता.
 त्याबद्दल स्वामीचे चरित्रात पुढीलप्रमाणे माहिती दिली आहे.</p>

<p>पान नं. 53</p>

<p>प्रकरण दहावे</p>

<p> `सत्तावनी' नंतरची हकीकत
 `1857'नंतर  पेशवे घराण्याचा ऐतिहासिक संदर्भ संपतो. रावबाजींची शाखा 1875 नंतर अस्तंगत
झाली.  धाकटया चिमीजीआप्पांचा मुलीकडून (गं. भा. द्वारकाबाई थत्ते) जो विस्तार होता त्यापैकी
द्वारकाबाईचे नातू कै श्री. लक्ष्मणराव चिमाजी थत्ते यांना 1881 साली गं.भा. सईबाईसाहेब पेशव्यांनी
नेपाळात दत्तक घेतले होते. परंतु त्या मंडळीनी 'थत्ते' आडनावच कायम ठेवले. 'सन 1857 च्या
बंडाचे भूत पेशवे मंडळीच्याच मागे हात धुवून लागले.' (कै) श्री ल्क्ष्मणराव थत्ते म्हणतात ते अक्षरश:
खरे आहे,हे आतापर्यंतचा इतिहास वाचून ध्यानात येईल. श्रीमंत अमृतरावांचे दत्तक नातू माधवराव,
ज्यांनी 1857 मधील वनवासाचा भरपूर अनुभव घेतला होता, तेथून 1913 पर्यंत हयात होते.
त्यांच्या आजमितीपर्यंतच्या वंशजांची माहिती येथे दिली आहे. भट-पेशवे घराण्यापैकी ही एकच शाखा
आज विद्यमान आहे.  माधवरावांना दोन मुलगे होते. ज्येष्ठ कृष्णराव व कनिष्ठ सदाशिवराव.
 कृष्णराव माधवराव : (12) यांचा जन्म अंदाजे 1865 सालचा असावा. यांनी संस्कृत अध्ययन पूर्ण
केले होते.  इंदूरच्या प्रिन्स कॉलेजमधून बी.ए. झाले. यांचे नातू कृष्णराव यांचा मृत्यू बरेली येथे
1911 साली कळवतात, तर (कै.) लक्ष्मणराव थत्ते यांनी आपल्या लेखांमध्ये कृष्णराव ऊर्फ बाळासाहेब
यांचा मृत्यू 1907 साली झाला असे लिहिले आहे. (पेशवे कुटुंबीयांचे निकटवर्ती श्री. नानासाहेब कर्वे
यांचे म्हणण्याप्रमाणे 1907 साल हेच बरोबर आहे- संपादक) मृत्यूचे वेळी यांचे वय 41-42 वर्षांचे
होते असे समजते. बाळासाहेब या नावानीच ते ओळखले जात.  बरेली येथेच त्यांचे वास्तव्य होते.
पत्नी : रमाबाई. जन्म अंदाजे सन 1870 सुखवस्तु गृहिणी वास्तव्या बरेली येथे. मृत्यू सुमारे सन
1933 साली पिता 1963 वारल्या. पती विनायकराव पटवर्धन, वाडीकर रा. कुरूंदवाड, व्य जमीनदार (2)
 ठकूताई, सासरचे नाव लक्ष्मीबाई. सुखवस्तु गृहिणी. विवाहपूर्वी वास्तव्य बरेली नंतर बुधगाव. या
1977 साली वारल्या.</p>

<p>पान नं. 181</p>

<p> पती माधवराव हरिहरराव पटवर्धन, संस्थानाधिपती बुधगाव (या भगिनींचा ज्येष्ठताक्रम पेशवे कुटुंबियांचे
निकटवर्ती श्री. नानासाहेब कर्वे यांचेकडून समजला संपादक. )
विनायकराव कृष्णराव : (यांचा जन्म सन 1901 चे सुमारास असावा असे वाटते बापूसाहेब कर्वे
 या नावाने हे विशेष परिचित होते.  वा. सुरूवातीपासून बरेली. द्वितीयविवाहानंतर प्रथम बरेलीलाच
रहात.  नंतर काही कारणाने हे बरेली साडून कुरूंदवाडला सासरी राहू लागले.  कुरूंदवाडला
गेल्यानंतर काही काळांनी हे आजारी पडले व 1920 पूर्वी कुरंदवाड येते निधन पावले. पत्नी: (2)
मालतीबाई. बापूसाहेबांचे निधनानंतर या 8-10 वर्षे हयात होत्या.  यांचे कुरूदंवाडलाच निधन झाले.
पिता विनायकराव हरिहर ऊर्फ नानासाहेब पटवर्धन, कुरूदंवाडकर संस्थानिक. मृत्यू सन 1931
( पटवर्धन घराण्याचा इतिहास.)  मुलगी : मालतीबाईना एक मुलगी झाली होती पण ती फार लवकर
वारली.
 विश्वनाथ कृष्णराव : (13) यांचा जन्म 1905 साली झाला. हे आग्रा विद्यापीठाचे पदवीधर (बी. ए.
इंग्लिश) होते. संगीताची आवड व अभ्यास केला होता.  सुखवस्तु गृहस्थ प्रथम वास्तव्य मुख्यत:
बरेलीला असे.  कारणपरत्वे पुण्यातही येत असत.  1936 सालापासून पुण्यात कायमचे स्थायिक झाले
 नंतर बरेलीला जाऊन येऊन वास्तव्य करीत. यांचे आजोबा माधवराव हे 1857 मधील राजबंदी
असल्यामुळे माधवरांवाना पुण्यात उन्हाळ्यात येऊन रहाण्यासाठी सुध्दा इंग्रजांची पूर्वपरवानगी
लागे ''
   तसेपूर्व परवानगीचे बंधन  पुढच्या पिढीला राहिले नाही. यामुळे त्यांच्या मातोश्री गं. भा.
रमाबाईसाहेब (साधारणत: 1920 सालपासून माहेरी खेप करण्यच्या निमित्ताने पुमे-बुधगाव इकडे
येऊ लागल्या.  त्यानंतर सर्व मंडळीचे पुण्याशी दळणवळण जास्त वाढले. पेशवे कुटुंबियांच्या इतर
जनतेशी संपर्क येऊ द्यायचाच नाही असे इंग्रजांचे पूर्वीपासून धोरण होते.  असो, विश्वनाथराव,
राजासाहेब या नावानीच जास्त परिचित होते.  ते 1951 साली पुण्यात निधन पावले. रियासतकार गो.
स. सरदेसाईना यांनीच पेशवे घराण्याची 1857 नंतरची माहिती दिली होती.  पत्नी : अन्नपूर्णाबाई.
यांचा जन्म 1911 साली झाला.  सुखवुस्तु गृहिणी. यानां संगीताची आवड होती.
    माहेरचे नाव कुसुम. (दुसरे नाव गौरी-भावे कुलवृत्तांत आ. सन 1940 ) यांचे वास्तव्य अर्थातच
प्रथम बरेली नंतर पुण्यात असे. या 1950 साली पुण्यात वारल्या. पिता विश्वासराव गोविंद भावे, व्य
समाजकार्य रा. सागर. मुली : (1)  सरोजीनी. यांचा जन्म 1925 साली झाला.  शिक्षण
मॅट्रिक्युलेशनपर्यंत. त्यांचे सासरचे नाव सरोजीनीच आहे. पती श्री भय्यासाहेब ऊर्फ मंगलमूर्ती विनायक
पटवर्धन रा. तासगाव पुणे, तासगावचे वतनदार, (2) रत्नमाला. जन्म 1935 साली झाला.  सुमारे 6
महिने इतकेच वय असताना वाराणसी येथे वारल्या.
 कृष्णराव विश्वनाथराव  (14) यांचा जन्म सन 1927 साली झाला.  रावसाहेब या नावानेच विशेष
परिचित आहेत.  पुणे विद्यापिठांची एम. एस्सी (भूशास्त्र) ही पदवी 1957 साली मिळवली.
जेमोलॉजिकल ऑसोसिएशन ऑफ इंडियाचा रत्नशास्त्राचा डिप्लोमा घेतला आहे.  याखेरीज लंडनची
रत्नशास्त्राची परीक्षा मुंबईतून दिली आहे.  निवृत्तीनंतर पुण्याला वास्तव्य. 1983 सालापासून
अभियांत्रिकी विद्यापीठात (ज्ञानेश्वर अंभियांत्रिकी ) प्राध्यापक. पत्नी : रमा यांचा जन्म 1937 साली
झाला.  सुखवस्तु गृहिणी. एस. एन. डी.टी विद्यापीठाची बी. ए (मराठी) पदवी 1983 साली घेतली.
माहेरचे नाव उषा. पिता दत्तात्रय नारायण आपटे,  रा. पुणे, मुली (1)जन्म 1958 साली झाला.
लगेच काही दिवसातच त्यांचे निधन झाले.  (2) मोहिनी. जन्म 1960 साली झाला.  सुखवस्तु गृहिणी
विवाहापूर्वी वास्तव्या रांची, नागपूर विवाहानेतर वास्तव्य पुणे. सासरचे नावही मोहिनी. पती प्रकाश
नारायण करकरे. व्य. भारतीय नौदलात अधिकारी, रा. पुणे.
विश्वनाथ कृष्णराव : (15) यांचे प्रचलित नाव महेंद्र. जन्म सन 1963.
विनायकराव विश्वनाथराव : (14) उदयसिंग या नावानेच सर्वत्र परिचित आहेत.  यांचा जन्म 1939
साली झाला.  1962 साली डेहराडूनहून डी.पी.आय ही पदवी मिळवली.  1977 साली पुणे
विद्यापीठाची पी.एच््डी. (डॉक्टरेट) मिळवली. पुणे विद्यापीठात भूशास्त्राचे प्राध्यापक. पी. एचडी. साठी
विद्यापीठ मान्यातप्राप्त गाईड. पुम्याच्या पर्वती देवस्थानाचे 1967 सालापासून एक विश्वस्त.
व्याख्यानानिमित्त अनेकदा परदेश प्रवास केला.  वेगवेगळ्या देशी -परदेशी मासिकांमधून 1983 साल
असेर 51 लेख प्रसिध्द. वास्तव्य पुणे.  शिक्षणानिमित्त वास्तव्य डेहराडून, हॉंलड. पत्नी : सरस्वती
माहेरचे नाव जयमंगला. माहेरचे नाव विशेष प्रचलित. यांचा जन्म 1942 साली झाला.  1978 साली
पुणे विद्यापीठाची एम.ए.पदवी मिळवली. पिता आप्पासाहेब पंतप्रतिनिधी. विशालगड.  रा. कोल्हापूर, पुणे,
गोवा, इंग्लंड. संस्थानिक, नंतर मनीलेंडर्स खात्यात 25 वर्षे अधिकारी. मुली : (1) शुभदा . 1964
साली जन्म. वा. पुणे. (2) सुपर्णा ज. 1968 वा. पुणे.
रघुनाथ विनायकराव : (15)  पुष्कर हे दुसरे नाव प्रचलितय यांचा जन्म 1975 साली झाला.
सदाशिवराव माधवराव : 12) बाबासाहेब या नावाने विशेष ओळखले जात.  यांचे शिक्षण
मॅट्रिक्युलेशनपर्यंत झाले होते.  वास्तव्य बरेलीला असे.  हे 1916 साली वारले.  पत्नी : पार्वतीबाई.
या  बरीच वर्षे बरेलीलाच रहात. 1950 साली यांचे निधन झाले.  पिता बापूशास्त्री ऊर्फ नरहरी
सीताराम देव.  हे जुन्या काळातील प्रकांड पंडित व वाराणसीतील प्रख्यात ज्योतिषी होते.
मुलगी : हया बालवयातच वारल्या.  वयाने बंधू रघुनाथरावांपेक्षा लहान.</p>

<p>पान नं. 183</p>

<p> रघुनाथराव सदाशिवराव (13) यांचा जन्म 1900 साली झाला. प्रचलित नाव तात्यासाहेब. बरेचसे
वास्तव्य बरेली येथेच झाले.  यांचे शिक्षण मॅट्रिक्युलेशनपर्यंत झाले होते.  1925-26 सालापासून हे
पुण्यात येऊ लागले.  त्यावेळी विंचूरकरांचे वाडयात रहात. हे 1932 साली पुण्यात वारले. हे काही
दुखण्यामुळे वारले असे. (कै) लक्ष्मणराव थत्ते आपल्या लेखात म्हणतात. पत्नी: कमलिनीबाई यांचा
जन्म 1909 साली झाला.  सुखवस्तू गृहिणी. माहेरचे नाव इंदिरा. यांचे 1973 साली निधन झाले.
पिता विनायकराव गणेश पटवर्धन, वतनदार तासगाव गं.भा. कमलिनीबाईसाहेब यांची तीन मुले बारसे
होण्यापूर्वीच वारली असे समजते. त्या खेरीज माहिती पुढीलप्रमाणे : मुलगी : प्रभावती. या 1927
साली सुमारे 1-1/2 वर्ष वयाच्या असताना वारल्या.
 विश्वासराव रघुनाथराव : (14) यांचा जन्म अंदाजे 1923-14 साली झाला. दोन वर्षांचे असताना
वारले.
 *
-----------------------------------------------------------------
THE END</p>

<p></p></body></text></cesDoc>