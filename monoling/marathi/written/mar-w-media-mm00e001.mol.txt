<cesDoc id="mar-w-media-mm00e001.mol" lang="mar">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>mar-w-media-mm00e001.mol.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-25</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>चौफेर</h.title>
<h.author>माधव गडकरी</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - दामोदर दिनकर कुलकर्णी</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">mm00e001.mol</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page पुणे.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-25</date></creation>
<langUsage>Marathi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p> एकूण पाने : - 13
 FROM 54 TO 66</p>

<p>
 पान नं . 54
 असे वाटत असताना  ही इतकी घाई कशासाठी ?</p>

<p> मुख्यंमंत्री श्री . भोसले  यांना  आमची विनंती इतकीच की  ,  त्यांनी पोहण्याचा तलाव जरूर
 बांधावा , परंतु  असा घाईने बांधू नये . सावकाश चांगला स्फटिकाच्या दगडांचा बांधावा .
 त्यात पोहता पोहता लोकांना भेटता येईल अशी सोय ठेवावी . , भेटीस आलेला पत्रकार
 जास्त प्रश्न विचारतो आहे असे वाटले तर   त्यास मान धरून खाली ढकलून द्यावे . बिहार
 प्रेस विधेयकाचीही मग गरज नाही .</p>

<p> आणखी एक मुख्य सोय हवी  ती डुंबताना वाचण्याची . किनाऱ्यावर सेक्रेटरीला
 फायली घेऊन बसवावे व  पोहता पोहता मुख्यमंतत्र्यांनी  ते ऐकावेत . मग साहजिकच  त्यांना
 ओल्या हाताने  सही करता येईल .  ज्याचे काम असेल त्यालाही काठावर बसवावा . सर्वासाठी
 चांगले रंगीत चौरंग बसण्यासाठी आणावेत . मध्ये तलावात मुख्यमंत्री पोहत आहेत , सेक्रेटरी
 फाइल वाचून दाखवीत आहेत ,  ते ऐकू यावे म्हणून  लाउडस्पीकर लावले आहेत आणि
 काठावर अनेक चौरंगावर लोक बसले आहेत असे दृश्य तेथे दिसेल . सगळी सोंग आणता
 येतात , परंतु  पैशाचं सोंग आणता येत नाही ,  हे सांगता सांगता मुखयमंत्र्यांचंही सोंग
 आणता येतं , सोंगातला मुख्यमंत्री  मी पाहिलेला आहे असे सांगून श्री . बाबासाहेब भोसलेच
 गुरूवारी म्हणाले , "" खरोकरच  मी  स्वतःलाच त्यात पाहतो आहे ! ""</p>

<p> पोहण्याच्या तलावात मुख्यमंत्र्यांचं सोंग अधिक चांगलं दिसेल .  ते पाहण्यासाठी मग
 तिकिट लावायलाही हरकत नाही . प्रकल्पग्रस्तांच्या पुनर्वसनासाठी  त्यांना  जो निधी हवा
 आहे  तोही मिळेल .</p>

<p> सध्या या तलावाचे दीड लाख रूपये  त्यांना का देत नाही ?</p>

<p>
  13 वे  कसले घालता ?</p>

<p>  दिनांक 12 - 10 - 82</p>

<p> दादरच्या स्मशानभूमीचे बाहेर कै . सदानंद रेगे  यांच्या मित्रांनी  त्यांचे जाहीर तेरावे
 घातले . त्याकरिता श्री . नामदेव ढसाळांनी मोठी पोस्टर्स काढली होती व   ती मुंबईभर
 लावली होती .  हे तेरावे घालणारे कवी सादनंद रेगे  यांचे जिवलग मित्र होते , त्याबद्दल
 मतभेद नाही .  त्यांच्या रेगेबद्दलच्या भावनाही तितक्याच प्रामाणिक याबद्दल  मला थोडीही
 शंका नाही . सदानंद रेगेसाठी भररस्त्यात काव्यश्रध्दांजली वाहण्याची  त्यांची कल्पनाही फार
 चांगली होती . परंतु   ती स्मशानभूमीजवळ व  ` तेरावे घालणार ' या ओबडधोबड
 भाषेत
  कशाला ?</p>

<p> काल कल्याणहून परतताना रेल्वेगाडीत दादरला कवी श्री . नारायण सुर्वे भेटले .  तेही</p>

<p> पान नं . 55</p>

<p> तेरावे घालून आले होते . रस्तायवर कविता म्हणण्याची कल्पना त्यांनाही आवडलेली होती ,
 परंतु   हे तेरावे घालणे त्यांनाही आवडलेले दिसले नाही . पुन्हा सध्या या देशात उक्ती व
 कृत्तीचे संबंध तर  साफ तुटत चाललेले आहेत . कै . सदानंद रेगे  हे तर  नास्तिक होते .
 देवाधर्माचे  त्यांना पूर्ण वावडे होते .  त्यांना मंत्राग्नी देण्यात आला .  त्यांच्या भाईबंदांना  जे
 योग्य वाटले  ते  त्यांनी मनापासून केले . कुंटुबाचा प्रश्न म्हणून  त्यात कुणी तोंड खुपसले
 नाही . श्री . नामदेव ढसाळ  हे या तेराव्याचे प्रमुख सूत्रधार .  ते तर  बौध्दधर्मीय .
 डॉ . आंबेडकरांना  जो बुधघ्द समजला त्यात  हे ` जाहीर तेरावे ' बसते असे तर  मुळीच
 वाटत नाही . पुन्हा सदानंद रेगे  हा  तुमचा प्रिय मित्र गेला तर  भितांडांवर पोस्टर्श लावून
  त्याचे ` तेरावे घालणार ' म्हणून  धिंडवडें  कशाला काढता ? मृत्यू  हा सुंदर असतो ,
 ` सुंदर  मी होणार '  ही नाशिकच्या कवी गोविंदांची ओळ निदान  लेखकांना मार्गदर्शक
 ठरावी . श्रध्दांजलीलाही सौदंर्याचे कोंदण असावे . सदानंद रेगे  यांचे  तुम्ही जिवलग मित्र
 असाल तर   ही कर्मकांडे  कशाला ? पुन्हा  त्यांचे दहन शीवला झाले . तेथे गर्दी जमणार
 नाही म्हणून  स्मशान का बदलावे ? रेगेसाठी जरूर रस्त्यावर कविता म्हणा .  तुमच्या
 कविता ऐकून सदानंद रेगेचा आत्मा खरोखरच तृप्त होईल . ऐकणारेही तृप्त होतील .
 परंतु  त्यात 13 वे  कशाला ?</p>

<p>  पैसा  हेच जीवनाचे केंद्र</p>

<p>  दिनांक 12 - 10 - 82</p>

<p> "" पैसा  हाच जीवनाचा केंद्रबिदूअसतो ""  याचा अनुभव असा सकाळी येईल अशी
  मला कल्पना नव्हती , कारण   तो केंद्रबिंदू असता कामा नये , पैसा  हे साधन आहे , साध्य
 नाही व   ते साधन  आपल्या आवक्यात ठेवलं नाही तर  साधनाचे  आपण गुलाम होतो
 असे व्याख्यान कल्याणगायन समाजाच्या सभागृहात रविवारीच  मी देऊन आलो होतो .</p>

<p>  त्याचे कारणही तसेच होते . कै . सुधाकर वावीकर या कल्याणमधील समाजवादी कार्य -
 कर्त्यांच्या स्मृतीप्रीत्यर्थ एक निबंधस्पर्धा 10 वीच्या विद्यार्थ्यांसाठी घेतली होती व  त्यात
 ` जोतिबा फुले  यांचे काम , ' ` परंपरा व  विज्ञाननिष्ठा ' आणि  ` पैसा  हे जीवनाचे केंद्र '
 असे तीन विषय दिले होते . 57 विद्यार्थी - विद्यार्थिनींना या निबंधस्पर्धेत भाग घेतला .
 त्यांपैकी 31 जणांनी "" पैसा  हे जीवनाचे केंद्र "" या विषयावरच निबंध लिहिले . सुधाकर
 वावीकरनेच स्थापन केलेल्या जयप्रकाश मंडळाने  ही स्पर्धी आयोजित केली होती . बक्षिसे
 मिळविणारे विद्यार्थी आले होते . सुधाकर वावीकर  हा अविवाहित राहिला . विचाराने पूर्ण
 समाजवादी . समाजवादी पक्षाच्या कामासाठी  त्याने  आपले सर्व आयुष्य वेचले . परंतु</p>

<p> पान नं . 56
 पुढाऱ्यांना भांडणाने व्यथित झालेल्या या माणसाने  आपल्या आजारावर उपचार करून
 घेण्याचे नाकारले व  इच्छामरण स्वीकारावे तसे 51 व्या वर्षी सरळ मरण पत्करले .  त्याने
 औषध घेतले असते तर   तो बरा झाला असत .  तो काही नटांसारखा व्यसनी नव्हता .
 "" पंरतु समाज  आपल्या रोगांवर औषध घ्यायला तयार नाही .  मी शरीरासाठी घेणार नाही ""
 असा विचार मांडून वावीकर गेला . श्री . अच्युतराव पटवर्धनांवर  मी थोडी टीका ` चौफेर '
 स्तंभात केली होती . समाजवादी नेत्यांना पुण्यात्मे मानणाऱ्या वावीतरांचे  माझी हजेरी
 घेणारे पत्र आले होते . त्यांच्यासाठी जयप्रकाशजी सांगितले , खांडकरांचा ध्येयवाद सांगितला ,
 साने गुरूजींच्या जीवनानिष्ठा सांगितल्या . काहींनी जोतिबा फुले यांच्यावर लिहिले होते .
 परंतु   ते लिहिताना "" जोतिबा फले  यांना वाळीत टाकले "" असे लिहिण्याऐवजी ` वाळ्यात '
 टाकले असे लिहिले होते . पालकांचे उत्तम मार्गदर्शन विद्यार्थ्यांना असावे असे निबंधावरून
 जाणवले असे परीक्षकप्रमुख कल्याण कॉलेजचे उत्साही प्राध्यापक श्री . कुलकर्णी म्हणाले .</p>

<p> खूप उकाडा होता . भाषण झाल्यावर काही लोक म्हणाले ""  तुम्ही फार चांगले
 बोललात "" एखादे काम व्हावे म्हणून   आपण पाच - दहा रूपये देतो तेव्हा   तो आपल्याकडे
 पाहतो तेव्हा   तो मनात बोलत असतो , ""  तुम्ही फार चांगले गृहस्थ आहात ! "" सुधाकर
 वावीकर मागे काहीही ठेवून गेला नाही कारण   त्याला मूल्ये जपायची होती व  बाकीच्यांना
 मूल्ये जमवायची होती .</p>

<p>
  सुखदुःखाची चोरी</p>

<p>  दिनांक 16 - 10 - 82</p>

<p> पुण्यात टपाल पेटीतील पाकिटे काढून त्यावरील तिकिटे चोरणाऱ्या प्रकाशकाला अटक
 झाली आहे .  ही बातमी वाचून पुणे शहरातील मोठे प्रकाशक मनमुराद हसले असतील .
 "" प्रकाशक टपाल तिकिटे चोरतो म</p>

<p> पान नं . 57
 देणारा वाटला  असावा  . रंतु मुद्देमालासह ते पकडले  गेले  . वेगवेगळ्या पाकिटांवरून काढून
 घेतलेली  372 तिकिटे त्यांच्याकडे मिळाली  . पहाटे चार वाजता  स्वारी आपल्या दुकाना -
 समोरची&lt;AVAJ&gt; टपाल  पेटी  उघडत  असे व त्यामधील सर्व टपाल  काढून घेत  असे . आंतर्देशीय
 पत्रांवर तिकिटे नसल्यामुळे त्यांना फक्त ते हात लावीत  नसत  . टपाल  व तार  खात्यातील
 वरिष्ठ अधिकारी पोलिसांकडे गेले  व त्यांनी लोकांच्या तक्रारीकडे लक्ष वेधले  . पुण्याचे
 पोलिसप्रमुख श्री . राममूर्ती यांनी खास अधिकारी दिले  . संशयाप्रमाणे या पेटीवर लक्ष
 ठेवले  व पुण्याचे एक प्रकाशक मुद्देमालासह मिळाले  .</p>

<p> प्रत्येक गुन्हेगाराचा एक जोडीदार असतो  . यांचा जोडीदार बारामतीत होता  . दिलीप
 दत्तात्रय शेटे हा मूळ पुणेकर बारामतीत हेच करीत  होता  . त्याच्याकडेही तिकिटे , पाकिटा -
 तून काढून घेतलेले  तरूंणीचे फोटो व टपालपेटयांची कुलपे उघडण्यासाठी वापरल्या  जाणाऱ्या
 किल्ल्या मिळाल्या  . बातमीत पुढे म्हटले  आहे  , "" या जोडगोळीच्या ताब्यातून जप्त केलेल्या
 पाकिटात अनेकांच्या सुखदुःखाच्या बातम्या , विमा कंपन्यांच्या नोटिसा , शास्त्रज्ञांचा संशोधन
 संस्थांशी झालेला  संशोधनविषयक पत्रव्यवहार असे महत्वाचे कागदपत्र आढळले  . ""</p>

<p> दिल्लीत एक पत्रे चोरणारा पोस्टमन सापडला  होता  . तो झाडाखाली बसून सर्व
 टपालाचा निकाल लावी  . रद्दीत सर्व विकून टाके  . यांनी तिकिटे काढून घेतली  व टपाल  नष्ट
 केले  . सुखदुःखाचा , आशाआकांक्षांचा निकाल लावला  . प्रकाशक , पुस्तक विक्री करणारी
 माणसेही या वयात असे करू  शकतात  . शिक्षणाचा व ग्रंथसाहचर्याचा संस्कृतीशी संबंध
 असतोच  असे नाही  .</p>

<p>
  बालकवींचे वेगळे दर्शन</p>

<p>  दिनांक 26 - 10 - 82</p>

<p> फुलामुलांचे कवी बालकवी यांच्या पत्नी पार्वतीबाई ठोंबरे  यांच्या वाटयाला आलेल्या
 निर्दय जीवनाची कहाणी सांगणारे त्यांचे छोटेसे आत्मचरित्र पुण्यात सोमवारी समारंभाने
 प्रसिध्द झाले  . श्रीमती पार्वतीबाई ठोंबरे  आता हयात नाहीत  . त्या काही वर्षे चोपडयाला
 राहात  होत्यास  नंतर मुंबईत होत्या  . पुस्तक फक्त 80 पानांचे असल्याने  झटकन् वाचूनही
 झाले  , आणि त्यामधील मजकूर वाचून अस्वस्थ झालो  . 1918 मध्ये बालकवींचे निधन
 झाले  . 1982 मध्ये ` मी आणि बालकवी ' हे पार्वतीबाई ठोंबरे  यांचे पुस्तक वाचत  होतो  .
 ` आनंदी आंनद गडे  08 इकडे तिकडे चोहिकडे ' लिहिणाऱ्या  ठोंबऱ्यांच्या जीवनात कुठे
 आनंद  होता  असे दिसतच  नाही  .</p>

<p> त्याचबरोबर त्यांच्या पत्नीचे जीवन तर अतिशय दुर्दैवी ठरले  . ` मी आणि बालकवी '</p>

<p> पान नं . 58
 हे पुस्तक वाचल्यावर तर ` हिरवे हिरवे गार गालिचे08 हरित  तृणांच्या मखमालीचे '
 लिहिणारा हाच कवी बोरीच्या काठीने आपल्या पत्नीला बदडून कसा काढतो  हे कळतच
 नाही  . पार्वतीबाई लिहितात  , ` अशा वेळेला जर का बालकवी घरात आले  तर मला भीतीने
 कापरे भरे  . ते आले  की मी पळून जाई . त्यांच्या चष्म्याचीसुध्दा मला भीती  वाटायची  . '
 आपल्याला बालकवींच्या डोळ्यांवरचा चष्मा आतापर्यंत कसा केविलवाणा वाटायचा08
 पार्वताने वाढले  नाही  म्हणून एक दिवस घरात वाद झाला  . बालकवी वरच्या&lt;AVAJ&gt; मजल्यावर
 गेले  व त्यांनी पार्वतीला वर पाठवायला  भावजयीला सांगितले  . वर गेल्यावर काय झाले
 याची हकीगत पार्वतीबाईंनी लिहिली  आहे  . कवीने काही हळुवार प्रेम वगैरे केलेले  नाही  .
 पार्वतीबाई लिहितात  , "" मी जाते  तोपर्यंत , वाडयात बोरीचे झाड  होते  . त्याच्या फांद्या वर -
 पर्यंत पसरल्या  होत्या  . त्यांची एक काठी बालकवींनी काढून ठेवली  होती  . मी वर जाताच  ते
 मला त्या काटेरी काठीने एकसारखे मारू लागले  . बोरीचे काटे  बोचून मला मार  अधिकच
 लागे  . मी मोठयाने रडू लागले  . तर मला रडूनही देईनात  . मला पोटभर मार  दिल्यावर ते
 खाली निघून गेले  . जाताना त्यांनी दाराला बाहेरून कडी घातली  . ""</p>

<p> बालकवींनी दुसऱ्या खेपेस पायातील चपलांनी पार्वतीबाईना मारले  . तो मार  सर्व त्यांनी
 हातावर घेतला  . हात सुजले . या बाईचे दुःख असे की तिची आईही  तिला छळत  असे .
 तीही तिला जबर मारहाण करी  . हे पुस्तक वाचल्यानंतर वाटले  की या बालकवींचे जीवन
 कुणीतरी आतून उद््ध्वस्त केले  होते  . आता हे पुस्तक प्रसिध्द झाल्यावर काही विरूध्द
 टीकाही होईल  . परंतु बालकवींच्या जीवन चरित्रात खलनायकासारखा त्यांचा भाऊ अमृत -
 राव वावरला  आहे  . या अमृतरावानेही एक दिवस पार्वतीबाईच्या कमरेत लाथ मारली  . काही
 चूक नसताना मारली  व बालकवी काही करू  शकले  नाहीत  . पूर्वीच्या&lt;AVAJ&gt; काळात स्त्रियांना
 मारहाण ही नैमित्तिक गोष्ट होती  . व त्यात विशेष काही नाही  असेही  कुणी म्हणेल  , 0
 ` निर्झरास ' ` श्रावणमास ' आणि ` पांखरास ' या कविता लिहिणारे बालकवी या पत्नीच्या
 आत्मचरित्रात कुठेच दिसत  नाहीत  .</p>

<p>
  आर्यन सिनेमा बंद</p>

<p>  दिनांक 27 - 10 - 82</p>

<p> पुण्यातील पहिले चित्रपटगृह ` आर्यन ' पुणे महानगरपालिकेने ताब्यात घेतल्याने ते
 बंद झाले  आहे  . पालिकेने ताब्यात घेतल्याने खेळ  होणार  नाहीत  अशी पाटी थिएटरवर
 झळकली  आहे  .</p>

<p> 67 वर्षांपूर्वी हे थिएटर गंगाधर ऊर्फ बापूसाहेब पाठक यांनी बांधले  . ते बाहेरून</p>

<p> पान नं . 59
 नाटकाच्या थिएटरसारखे दिसे  . नंतर तेथे स्त्रियांचे पुतळे बसवण्यात आले  . आज मराठी
 चित्रपटांचे एकमेव व हुकमी आश्रयस्थान म्हणून ते ओळखले  जाई . अर्थात चांगल्या
 चित्रपटांचे निर्माते आता आर्यनला आपला चित्रपट लावण्यास तयार नव्हते  . परंतु एके -
 काळी अनंतराव माने  यांच्या ` सांगत्येऐक ' व ` केला  इशारा ' ने  येथे शंभर आठवडे
 गाजवले  . ` सांगत्ये  ऐका  ' ने  शतकोत्तर रौप्यमहोत्सव साजरा केला  होती  . त्यानंतर
 ` एक गाव  बारा भानगडी ' , रंगपंचमी ' व दादा कोंडके यांच्या नव्या चित्रपटांनी रौप्य -
 महोत्सव साजरे केले  .</p>

<p> मूकपटाच्या जमान्यात लोक सिनेमा पाहण्यास येत  नसत  . त्या वेळी ` आर्यन ' चे मालक
 बाहेर विदूषकाला उभे करीत  व चित्रपटाची जाहिरात करीत  . घोडेस्वार मूक चित्रपटात
 आले  , परंतु आवाज नव्हता  . तेव्हा करवंटयांच्या साहाय्याने घोडयांच्या टापांचा आवाज
 काढला  जाई . वाघााची डरकाळी , कुत्र्यांचे भुंकणे  , पक्ष्यांचा किलबिलाट , लाढाईच्या वेळी
 आरडाओरड हे सर्व करण्यासाठी तज्ज्ञांचा एक संचय मालक श्री . पाठक यांनी ठेवला
 होता  . ते सर्व आवाज काढीत  व मूक चित्रपट चाले  .</p>

<p> पहिला बोलपट ` आलमआरा ' व पहिसा मराठी बोलपट ` अयोध्येचा राजा  ' येथेच
 प्रदर्शित  झाला  . ` राजा  हरिश्चंद्र ' येथेच प्रकटला  . नानासाहेब सरपोतदार यांनी ` आर्यन ' च्या
 नावाखाली काही मूकपट काढले  .</p>

<p> पुणे महानगरपालिका या थिएटरचा कशासाठी उपयोग करतो  ते आता पाहावयाचे आहे  .
 कदाचित हे थिएटर पूर्णपणे पाडले  जाईल  व तेथे वाहने ठेवण्याची सोय पालिका करील
 असे म्हटले  जाते  . इतिहास  घडविणे ही गोष्ट वेगळी असते  , तो जमीनदोस्त करणे  फार
 सोपे असते  . ` गोंधळात  गोंधल ' हा चित्रपट कालपर्यंत तेथे चालू  होता  . शेवटी ते मंडई -
 वाल्यांचे थिएटर होते  . थोरामोठयांचे नव्हते  , त्यामुळे ते अनावश्यक ठरले  . ` आर्यन '
 थिएटर बंद झाले  या घटनेचे दुःख जुन्या पिढीच्या लोकांना , नव्यांना वाहतूक सुधारल्याचे
 समाधान मिळाले  .</p>

<p>
  बादलीभर पाण्याची चोरी</p>

<p>  दिनांक 27 - 10 - 82</p>

<p> बिहारचे मुख्यमंत्रीश्री . जगन्नाथ मिश्रा यांचे नशीब काही चांगले दिसत  नाही  . आता
 या प्रकरणाला मुख्यमंत्री मिश्रा स्वतः काही जबाबदार नव्हते  . परंतु त्यांच्या निवास  -
 स्थानातील नळाचे बादलीभर पाणी "" चोरल्याच्या "" आरोपावरून सुरेंद्र ठाकूर नावाच्या
 एका हरिजन मुलाला सात दिवसांचा तुंरूगवास घडला  आहे  . दि . 1 ऑक्टोबर रोजी त्याला</p>

<p> पान नं . 60
 अटक करण्यात आली  , ताबडतोब त्यास तुरूंगात पाठविण्यात आले  , मॅजिस्ट्रेटसमोर उभे
 करण्याल पोलिसांना नेहमीप्रमाणे उशीर झाला  व त्या तरूण पोराला सात दिवसांचा तुरूंगवास
 भोगावा  लागला  . मंत्र्यांना आपल्यावर होणाऱ्या टीकेसाठी वृत्तपत्रांचे स्वातंत्र्य कमी करावे
 असे वाटते  , आता येथे एका बादलीभर पाणी घेणाऱ्याचे पूर्ण स्वातत्र्यच आठवडाभर
 हिरावून घेतले  गेले  , त्याची भरपाई कोण व कशी करणार  असा सवाल पाटण्याच्या
 पत्रकाराने ही बातमी देऊन केला  आहे  .</p>

<p> घडले  तेही मोठे विचित्र आहे  . पाटण्यात बेलो  रोडवर मुख्यमंत्री श्री . जगन्नाथ मिश्रांचा
 बंगला आहे  . आत अंगणात महापालिकेचा नळ आहे  . बंगल्यासमोर 02धाबा03 पध्दतीची
 हॉटेल्स आहेत  . अर्थात स्वस्तातील ती हॉटेल्स आहेत  . मुखयमंत्र्यांना अगर पोलिसांना ती
 दुकाने तेथे नको  असतील  तर ती त्यांनी उठवावयास हवी  होती  . परंतु तसे घडण्यासाठी
 त्यांनी एका मुलाला वेठीला धरले  . मुख्यमंत्र्यांच्या बंगल्याच्या अंगणातील नळाला एक
 प्लॅस्टिकचा पाईप लावलेला  होता  व तो भिंतीबाहेर आणलेला  होता  . त्यामुळे प्रत्यक्षात
 मुख्यमंत्र्यांच्या वाडयात न शिरता  पाणी घेता  येते  . रखवालदारांचीही त्यास अनुमती होती
 व असा सुखाचा व्यवहार चालू  होता  .</p>

<p> दि . 1 ऑक्टोबर रोजी आठ वाजता  पाटणा महापालिकेचे आय  . ए . एस . अँडमिनिस्ट्रेटर राजदेव प्रसाद सिंग चालले  होते  . त्ल्ल्या लेल्या  चो  , त्यांनी</p>

<p> त्यास अटककेली व पुराव्यासाठी बादली व ती प्लॅस्टिक ट्यूबही बरोबर घेतली  . प्रथम ते
 जवळच्या&lt;AVAJ&gt; पोलिस ठाण्यावर गेले  . तेव्हा इतरांना येतो  तो अनुभव  त्यांनाही आला  . "" या
 पोलिस ठाण्याच्या हद्दीत हा गुन्हा येत  नाही  , "" असे त्यांनाबी सांगण्यात आले  . मग या
 आय  . ए . एस . अधिकाऱ्याने त्याच्या हातात बेडया घालण्यास सांगण्यात आले  . व
 सेक्रेटेरिएट ठाण्यावर नेले  . "" अगदी पाणी चोरताना पकडल्याचा "" आरोप  त्याच्यावर
 ठेवण्यात आला  . दि . 1 ऑक्टोबरच्या रात्री हे घडले  , दि . 2 ऑक्टोबर रोजी त्याचा
 खटला उभा राहीपर्यंत कोर्ट स्थगित झाले  , तीन ऑक्टोबरला मॅजिस्ट्रेटने बादलीभर
 पाण्याच्या पुढील चौकशीसाठी न सोडता  त्याला तुरूंगात पाठविले . दि . 9 ऑक्टोबरला
 त्याची जामिनावर सुटका झाली  . अजून खटला चालयचा आहे  ! महानगरपालिकेने ते हॉटेल
 तेथून दुसऱ्या दिवशी उठविले . रखवालदारांना स्वस्तात तेथे जेवण मिळत  होते  ते गेले  .</p>

<p> आता या सुरेंद्र ठाकूरचा कदाचित चालणारही  नाही  . वृत्तपत्रांनी बे प्रकरण
 बाहेर आणल्याने त्याला आत वेगळे स्वरूप येईल  . या विषयामुळे तरी स्वातंत्र्य कुणाचे हे
 या देशात महत्वाचे मानले  जाते  हे लोकांच्या लक्षात  यावे  . मंत्र्यांचे स्वातंत्र्य , राजकर्त्याचे
 स्वातंत्र्य , श्रीमंतांचे स्वातंत्र्य , गरिबांचे स्वातंत्र्य वृत्तपत्रांचे स्वातंत्र्य हे एकाच पातळीवरचे
 विषय नाहीत  .</p>

<p></p>

<p>  पान नं . 61</p>

<p>  प्यारेलाल
  दिनांक 29 - 10 - 82</p>

<p> गांधीजीचे सेक्रेटरी श्री . प्यारेलाल  याचे आकस्मिकपणे निधन झाले . तसे  त्यांचे वय
 झालेले होते . परंतु  प्रकृती उत्तम होती व   तो कार्यमग्न होते . डॉ . सुशील नय्यर  यांचे  ते
 मोठे बंधू पुण्यास आगाखान पॅलेमध्ये गांधीजी होते  त्या वेळी  त्यांचे सेक्रेटरी महादेवभाई
 देसाई  यांचे निधन झाले . त्यानंतर प्यारेलाल  यांनी  ते काम हाती घेतले व  निष्ठेने केले .
 भारतीय स्वातंत्र्याच्या शेवटच्या वाटाघाटीत फाळणी व  स्वातंत्र्यानंतरचे एक वर्ष  हा काल -
 खंड प्यारेलाल  यांनी जवळून पाहिला व   तो सर्व माहिती सांगणारा ग्रंथ ` दि लास्ट फेज '
 दोन खंडात लिहिला . 8 : पानांचा सादारण  हा एकेक खंड आहे व  गांधीजींची 1944
 मध्ये सुटका झाली , तेव्हापासून गांदीजीच्या मृत्यूपर्यंतचा सर्व अधिकृत इतिहास या ग्रंथात
 आहे . राष्ट्रपती डॉ . राजेंद्र प्रसाद  यांची प्रस्तावना  त्या ग्रंथाला आहे .</p>

<p> श्री . प्यारेलाल  यांचे  हे दोन्ही खंड गाजले . परंतु   ते अधिक गाजले  ते गांधीजीच्या
 नौखालातील ` एकाल चलो रे  ' च्या यात्रेने . ` वन् मॅन बाऊंडरी फोर्स ' चे  ते गाजलेले माऊंट -
 बॅटनचे पत्र या पुस्तकानेच लोकांसमोर आले . गांदीजींच्या  ते अगदी जवळ असल्याने  त्या
 महात्म्याचे प्रत्येकाबरोबर होणारे संवाद  त्यांना कळत होते . पहिल्या खंडात  त्यांनी गांधी -
 राजाजी संवाद दिला आहे . सोमवार असल्यामुळे गांधीजीचे मौन आहे . राजाजी व   ते
 यांच्यात मतभेदाचे वातावरण आहे . राजाजींचेंही इंग्रजी उत्तम . दोघे एकमोकांशी चिठया
 लिहून बोलत आहेत . राजाजींनी सेवाग्रमाला येऊन राहावे असे गांधीजी सुचवितात .</p>

<p> राजाजी : 30 च्या सुमारास  मी सेवाग्रामला येऊ शकेन .
 गांधीजी : या तारखेस  मी  तुमचा शोध घेईन .
 राजाजी : जशी तमची इच्छा .
 गांधीजी :  तुमचा शोध घेईन  याचा अर्थ  काय !
 राजाजी : माणूस धोक्यांचाही शोध घेत असतो कधी कधी
 गांधीजी :  तुम्ही हवा तर  तसा अर्थ कर .  मला  तो धोकाही हवा आहे .  मला तुमच्याशी
 खूप बोलायचे आहे .
 राजाजी : तोपर्यंत  आपण खूप विसरून गेलेले असू . म्हणजे  नोंदी उरलेल्या नसतील .
 गांधीजी : मग  आपण हसू आणि  लठठ होऊ .</p>

<p> बादशाहाखान व  गांधींजी यांच्यामध्ये फाळणीच्या वेळेस  जे बोलणे झाले  त्याचे प्रायरेलाल
  हे साक्षादार . कै . हरिभाऊ जोशींबरोबर  मी जलालाबादला बादशाहाखानांकडे गेलो तेव्हा   ते
 म्हणाले "" या प्यारेलाला  काय झाले !  त्याने  माझ्या पत्राचे उत्तर पाठविले नाही ! ""</p>

<p> सरहद्द गांधीच्या सूचनेवरून हरिभाऊ प्यारेलालला परतल्यावर भेटले .  मी बरोबर होतोच .
 प्रायरेलाल यांच्याबरोबरचा सर्व संवाद नंतर प्रसिद्द केला होता .  त्यांनी नुकतेच मालिश</p>

<p> पान नं . 62
 केलेल असल्याने  त्यांची अंगकांती चकाकत होती .  ते नंतर  आपल्या कुत्र्याला डॉक्टरकडे
 घेऊन जाणार होते . बादशाहाखानांवर  ते नाराज दिसले . शेवटी गांधी - राजाजी 42 च्या
 ` चले जाव ' ठरावावर भांडतात . पुढे पाकिस्तान होणार असे दिसताच एकत्र येतात .
 नेहरू - बादशाहाखान  यांचे बिनसते . गांधींजी संभाळतात .  ते सर्व उच्च वातावरण या ग्रंथात
 आहे . प्यारेलाल  यांचे मोठपणही  त्या सुवर्णाच्या संगीतीचे . आपल्यासाठी  तो सारा इताहास
 ठेवून गेले  हे  त्यांचे चांगले कार्य . गांधीजींची  ती नौशाली यात्रा या ग्रंथाने अमर केली !</p>

<p>  पंतप्रधानांचा शिपाई</p>

<p>  दिनांक 30 - 10 - 82</p>

<p> शंकराच्या नंदीला अगर  समर्थाघरच्या श्वानाला जसे महत्व असते तसे महत्व सध्या
 पंतप्रधान श्रीमती इंदिरा गांधीच्या शिपायाला आले आहे . दर गुरूवारी श्रीमती गांधीच्या
 खात्यावर राज्यसभेत प्रश्नaत्तरे होत असल्यामुळे  त्या हजर होऊ लागल्या .  त्या हजर होणार
 आहेत ,  याची वर्दी सर्वप्रथम  त्यांचा शिपाई देतो . 11 ला काही मिनिटे कमी असताना
 पंतप्रधानांचा शिपाई राज्यसभेत शिरतो .  त्याच्या हातात एक काळी बॅग असते व  एक
 ` फोल्डर ' असतो .  तो बॅग खाली ठेवतो व  ` फोल्डर ' टेबलावर ठेवतो . शिपाई आला म्हणजे
 पंतप्रधान येणार  याची निश्चिती होते व  सभागृह , विशेषतः सरकार पक्षाचे खासदार  आपल्या
 टोप्या वगैरे सारख्या करू लागतात . संसदीय खात्याचे मंत्री श्री . भीष्म नारायण सिंग
 त्यांच्याबरोबर असतात . पंतप्रधान सभागृहात येतात तेव्हा  इंदिरा कॉग्रेस पक्षाचे खासदार
 उभे राहतात , नमस्कार - चमत्कार होतात व  पंतप्रधान सभापतींना नमस्कार करू बसतात .</p>

<p> गेल्या गुरूवारी राज्यसभेत  काय घडले  याचा एक वृत्तान्त दिल्लीच्या एक दैनिकाने
 साप्ताहिक समालोचनात दिला आहे . विज्ञान व  तंत्रज्ञान खात्याचे राज्यमंत्री श्री . सी . पी .
 एन् . सिंग प्रश्नांना उत्तरे देत होते , परंतु  पंतप्रधान उपस्थित व  अनुभव गाठीशी नसल्या -
 मुळे  त्यांना खासदार कोंडीत पकडत होते . सर्व प्रश्नोत्तरे चालू असताना श्री . सिंग  यांना
 पंतप्रधान उत्तरे पुरवत राहिल्या .  त्या वाक्ये बोलत होत्या , जरा अंतरावर असलेले सिंग
 वाकून वाक्ये ऐकत होते व   आपले उत्तर पुढे चालवीत होते . प्रश्न पुन्हा  आपल्या मुंबई -
 जवळच्या तारापूर आण्विक वीज केंद्राचा होता . या क्रेंद्राची वीज - उत्पादनक्षमता किती आहे
  हे शेवटपर्यंत कुणी सांगितले नाही . पंतप्रधानांनी एक कागद राज्यमंत्र्यांना दाखविला व
 त्यात आकडेवारी आहे असे सांगितले . परंतु  कसचे  काय ? आडात नसेल तर  पोहऱ्यात
 कुठून येणार ? या गडबडीत श्री . के . सी पंत  यांचा उल्लेख सन्माननीय मंत्री असा खुद्द
 पंतप्रधानांनीच केला .  आपल्या चुकीला  त्याही हसल्या . श्री . के . सी पंत  हे आता  कोणत्याच</p>

<p> पान नं . 63
 पक्षात नाहीत .  हे स्वतंत्र म्हणून  बसतात . श्रीमती गांधीचे  ते हुशार सहकारी होते . अशी
 माणसे श्रीमती गांधीनी गमाविली व   ज्यांना साध्या प्रश्नांना उत्तरे देता येत नाहीत  ती
 जमविली तर   काय उपयोग ?</p>

<p> हल्ली पंतप्रधान राज्यसभेत तशा कमीच येतात . दि . 4 ऑक्टोबर रोजी  त्या आल्या  ते
 काश्मीर विधेयकाबद्दल निवेदन करण्यासाठी , परंतु  सर्वांत महत्वाचे खाते त्यांच्याकडे आणि
 राज्यमंत्री उत्तरे देण्यास अपुरे पडतात .  ही प्रश्नोत्तरे संपल्यावर पंतप्रधनांना निरोप आला .
 पुन्हा शिपायाचा शोध झाला . शिपाई आला तेव्हा  खासदारांना कळले की  , आता पंतप्रधान
 जाणार . पुन्हा पक्षीय खासदार नमस्काराला उठले . यात मानसन्मान होत असेल , परंतु
 संसदीय संकेताच्या  हे विरूध्द आहे . कुणीही सभागृहात शिरत असताना अगर  बाहेर जात
 असताना सभागृहाच्या कामात व्यत्यय येण्याचे कारण  नसते . पुन्हा नेता व  सहकारी ,
 यामधील  हे अंतर  त्या पक्षाच्याही हिताचे नाही . नेहरू राज्यसभेत अधिक वेळा येत व
 ज्येष्ठ खासदारांशी संपर्क ठेवत . तेव्हा  मान होता , सध्या चालला आहे  तो नुसता
 सन्मान आहे .</p>

<p>  त्र्यं . गो आणि  विनोबा</p>

<p>  दिनांक 30 - 10 - 82</p>

<p> महाराष्ट्राचे नवे माहितीमंत्री श्री . त्र्यं . गो देशमुख  हे विनोबाजींना भेटले . नेहमीप्रमाणे
 नावावर कोटी वगैरे झालीच . "" पत्रकारांना शिस्त हवीच , "" असे उत्तर विनोबाजींनी
 दिल्याचे आता प्रसिध्द झाले आहे . "" वर्तमान बरे असो बुरे असो , जसेच्या तसे दिले
 पाहिजे "" असे विनोबाजी म्हणाले . भडक , अपप्रचार , चारित्र्यहनन व  बीभत्स लिखाण
 असेल तर  त्यावर अनुशासन हवे , असे तसा प्रश्न विचारला तेव्हा  विनोबाजी म्हणाले .
 आता  हीच प्रश्नोत्तरे  आपण राजकारणी व्यक्तींबद्दल करू या .</p>

<p> प्रश्न : मंत्र्यांना चारित्र्य असावयास हवे  काय ?
 विनोबाजी : अवश्य असावयास हवे .
 प्रश्न : मंत्र्यांनी दारू प्यावी  काय ?
 विनोबाजी : मुळीच पिऊ नये .
 प्रश्न : मंत्र्यांनी  आपल्या पत्नीखेरीज कुणाशीही शरीरसंबंध ठेवू नये असे  तुम्हाला
 वाटते  काय ?
 विनोबाजी : अर्थातच .
 प्रश्न : मंत्र्यांनी एकपत्नीव्रत राहावयास हवे असे  तुम्हाला वाटते  काय ? महाराष्ट्रात</p>

<p> पान नं . 64
 द्विभार्या प्रतिबंधक कायदा आहे .
 विनोबाजी : मंत्रिपदे गेल्यानंतर  आपले बंगले मंत्र्यांनी ताबडतोब सोडायवयास हवेत ना90
 विनोबाजी : एक दिवसही  त्यांनी राहता कामा नये .
 प्रश्न :  त्याचे भाडेही भरले नसेल तर   काय करावे ?
 विनोबाजी :  त्याचे सामान सरकारने जप्त करावे .
 प्रश्न : परंतु   आपले भाडे थकलेले आहे  हे  कुणीच  आपल्याला सांगितलेले नाही असे
 म्हटले तर   काय करावे ?
 विनोबाजी :  तो खोटे बोलतो असे म्हणावे .
 प्रश्न सामान्य माणसांचे चारित्र्य व  मंत्र्यांचे चारित्र्य  याला वेगळे नियम आहेत  काय ?
 विनोबाजी : सामान्य माणसाने काही पाप करू नये तर  मंत्र्यांनी करणे महापाप आहे .
 प्रश्न : मग अशी पापे वृत्तपत्रांनी छापणे योग्य आहे  काय ?
 विनोबाजी :  ते सर्वात मोठे पुण्यकर्म . कारण  जसा राजा तशी प्रजा होते . राजा
 सुधारला , नीट वागला , तर  प्रजा चांगली राहते . पैसे खाणे  हे सत्कर्म ठरले तर  सामान्य
 माणूस  तेच करील व  देश बुडेल .</p>

<p> श्री . त्र्यं . गो . देशमुख  आमच्या वतीने  हे प्रश्न पुढील खेपेस विनोबाजींना विचारतील
  काय ?</p>

<p>
  देशमुख आणि  मोरारजी</p>

<p>  दिनांक 3 - 11 - 82</p>

<p> कै . चिंतामणराव देशमुख  यांच्या दुःखद निधनानिमित्त मुंबई मराठी ग्रंथ संग्रहालयाने
 शोकसभा आयोजित करून औचित्य साधले . परंतु  तेथे  त्यांनी मोठे स्थान आहे , प्रतिष्ठा
 आहे  हे सर्व खरे . परंतु  चितांमणरावांच्या तेजस्वी कारकीर्दीची अखेर ज्यांच्यामुळे झाली ,
  त्या मोरारजीभाईंना देशमुखांच्या शोकसभेला बोलाविणे म्हणजे  भुटटोच्या शोकसभेला जनरल
 झियांना बोलाविण्यासारखे आहे . मरणान्तानि वैराणि वगैरे सर्व तत्वज्ञान खरे असले तरी
  ज्या बाणेदारपणाबद्दल देशमुखांचे नाव या देशात राहणार आहे  त्याचा अंशही  आम्हा
 लोकांत प्रगट होत नाही  याला  काय म्हणावे ?</p>

<p> मुंबई महाराष्ट्राला मिळू नये या कारस्थानाचे सूत्रधार श्री . मोरारजी देसाई होते . 1 ""
 हुतात्म्यांचे बलिदान  त्यांनी घेतले . या गोळीबारीची चौकशी करण्यास पंतप्रधानांनी नकार</p>

<p> पान नं . 65
 दिला  हे देशमुखांच्या राजीनाम्याचे एक प्रमुख कारण  होते . ""  तुम्ही एका साध्या लाठी -
 माराची चौकशी करता व  इतक्या मोठया गोळीबाराची करीत नाही ! ""  हाच आक्षेप कै .
 देशमुखांनी घेतला होते . इतकेही करून श्री . देसाई  त्या शोकसभेत  काय म्हणाले ?
 "" दुसऱ्यांच्या भावनांचाही विचार करावा लागतो  हे आय . सी . एस् परंपरेतून आल्यामुळे
 देशमुखांना अपरिचित होते "" अशा आशयाचे उद्गार श्री . देसाई  यांनी काढले . या
  त्यांच्या उद्गाराला आक्षेपच घ्यायला हवा होता . कारण  त्यांनीच मारठी माणसांच्या
 भावनांची कदर केली नाही व  त्यामुळेच कै . देशमुखांना राजीनामा द्यावा लागला . साऱ्या
 भाषांना  आपले प्रांत मिळाले व   ज्या प्रांताला सर्वात मोठा इतिहास  त्या महाराष्ट्राला मात्र
 भाषिक राज्य नाकारण्याचा उद्योग करणाऱ्यांच्या कटाचे श्री . मोरारजी देसाई सूत्रधार
 होते . इतकेच नव्हे तर   त्यांनी नजीकच्या काळात , सोलापूर येथे केलेल्या भाषणात या
 संयुक्त महाराष्ट्र चळवळीत गुजराती स्त्रियांवर अत्याचार झाले असे उद्गार काढले होते .
 "" दुसऱ्यांच्या भावनांची कदर करणे  आपण अलीकडे शिकलो "" अशी कबुलीही या
 मोरारजीभाईंनी दिल्याचे वृत्त आहे . परंतु  सोलापूच्या विधानावरून व  सर्वश्री एसेम व
 गोरे यांच्याबद्दल  त्यांनी मध्यंतरी  जे अनुदार उद्गार काढल व  नंतर  ते नाकारण्याचा प्रयत्न
 केला , त्यावरून दुसऱ्याच्या भावनांची काही बूज  त्यांना आहे असे मुळीच वाटत नाही .</p>

<p> श्री . देसाई हेसुद्दा सुरूवातील डेप्युटी कलेक्टर होते . देशमुख सनदी नोकर आणि   हे
 महसूल कर्मचार ! कशालाही होकारार्थी उत्तर देण्याची सवय नसलेल्या पंरपरेतील देसाईना
 या शोकसभेसाठी बोलाविण्याची मुळीच गरज नव्हती . देशमुख  हे जर  सत्याग्रही होते असे
  ते आज म्हणत असतील तर  तेव्हा  देसाई व  नेहरूंनी सत्याचा खून केला असा अर्थ होतो .
 कै . देशमुखांचे अर्थमंत्रिपद नंतर या मोरारजीभाईंनीच घेतले . देशमुख जेथे राहात होते ,  त्या
 विलिंग्डन क्रिसंटमध्ये देसाई राहायला गेले . महाराष्ट्र राज्य झाले , देशमुख संपले , आणि
 शोकसभेला पुन्हा मोरारजीभाईच ! महाराष्ट्राच्या बाणेदारपणाला  आमचे शतशः
 प्रणाम !</p>

<p>  `  जो ' नेहरू सामान्य विद्यार्थी</p>

<p>  दि . 8 - 11 - 82</p>

<p> कै . जवाहरलाल नेहरू  हे विद्यार्थी म्हणून  सामान्य होते , हुषार नव्हते ,  ते आय . सी
 एस . होऊ शकले नसते व  बॅरिस्टर झाल्यावर  त्यांनी वडिलांची वकिलीची गादी चालविण्याचा
 प्रयत्न केला असता तर  ` ब्रीफलेस् बॅरिस्टर ' - काम नसलेले वकील म्हणून   ते जन्मभर
 राहिले असते , असे सिध्द करणारे एक नवे पुस्तक प्रसिद्द झाले आहे . पुन्हा  ते काही</p>

<p> पान नं . 66
 वर्णनात्मक पुस्तक नाही . क . मोतीलाल नेहरू  यांनी  आपल्या मुलाला ,  तो शिकत असताना
 लिहिलेल्या पत्रांनी भरलेले  ते पुस्तक आहे "" सिलेक्टेड वर्क्स ऑफ मोतीलाल नेहरू ""
 असे या 406 पानी ग्रंथाचे नाव असून , विकास प्रकाशन संस्थेने  ते प्रसिद्द केले आहे .
 ` इंडिया टुडे ' या मासिकाने या पुस्तकाचा परिचय दिला आहे .</p>

<p> जवाहरलालला वयाच्या सोळाव्या वर्षी पित्याने इंग्रंडला शिकायला पाठविले . जवाहरलाल
  हे संबंध नाव इंग्रज पोरांना उच्चारणे शक्य नव्हते , म्हणून   ते  त्यास `  जो ' ( Joe )
 म्हणत . 1899  ते 1918 या काळातील 116 पत्रे या पुस्तकात आहेत व  त्यांपैकी 89
 पत्रे जवाहरलालना लिहिलेली आहेत . हॅरो व  केंब्रिज येथे जवाहरलाल शिकत आहेत ,
  त्या वेळी लिहिलेल्या पत्रांत मोतीलाल म्हणतात , "" तू  त्या वेळी गरम कापडे
 कमी घातले होतेस . तिथली थंडी  तुला बाधणार नाही , तू काळजी घेतली पाहिजेस ""</p>

<p> मोतीलाल नेहरूंनी एका पत्रात विचारले आहे . "" हॅरोला तुझ्याकडून ` फ्लॅगिंग डे ' च्या
 दिवशी  कोणते चाकराचे काम करून घेतले गेले  हे  मला कळव .  ज्याच्या घरी 50
 चाकर आहेत ,  त्याच्या लाडक्या मुलाकडून  कोणते घरमोकराचे काम करून घेतले जाते  हे
  मला कळले पाहिजे . ""  आपला मुलगा आय . सी . एस . होऊन कलेक्टर झाला आहे  याचे
 स्वप्न मोतीलाल कसे पाहतात ? "" जवाहरलाल नेहरू , एस्क्वायर , एम . ए  . ( कॅन्टब ) ,
 आय . सी . एस् . , डिस्ट्रिक्ट मॅजिस्ट्रेट , झाशी , "" असे वर्णन  त्यास लिहून पाठवतात .</p>

<p> परंतु  नेहरू आय . सी . एस् . ला आवश्यक असलेले कठीण विषय जमत नसल्यामुळे
 एकामागून एक सोडत गेले . येथे मोतीलाल खेत्री व  अमेठीच्या राजांबरोबर मोजवाण्या
 झोडतात . मुलगा इंग्लंडहून येणार म्हणून  घर पाश्चात्त्य करतात . शेवटी सात वर्षानंतर
 मुलगा जवाहर परत येतो . बॅरिस्टर होऊन येतो . परंतु  मोतीलाल  त्याला घेण्यास मुंबईला
 येत नाहीत .  त्या वेळचा सर्व प्रवास बोटीचा व  रेल्वेचा . अशा उन्हाळ्यात गाडीचा प्रवास
 सोसणार नाही , असे मोतीलाल लिहितात .  आपल्या एका नातेवाइकाला  त्याला घेण्यास
 सांगतात .  तो  त्यांचे मुंबईला स्वागत करतो . व  जवाहरलाल नेहरू , एस्क्वायर , एम् . ए  .
 ( कॅन्टब )  यांना गाडीत बसून देतो . ज्यांच्यासाठी पुढे हजारोंचा जनसंमर्द जमणार असतो .
  ते नेहरू एकटेच अलाहाबादला जातात . एक ` इंग्लिश ' घर  त्यांची वाट पाहात असते .
*
----------------------------------------------------------------
THE END
</p></body></text></cesDoc>