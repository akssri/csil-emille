<cesDoc id="mar-w-other-txt00a05x.mss" lang="mar">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>mar-w-other-txt00a05x.mss.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-25</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>नागरिकशास्त्र इ. 10 वी</h.title>
<h.author>श्री .  शं . भा चांदेकर</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - unknown</publisher>
<pubDate>****</pubDate>
</imprint>
<idno type="CIIL code">txt00a05x.mss</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page पुणे.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-25</date></creation>
<langUsage>Marathi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p> 
 एकूण पाने :- 16
 FROM 1 TO  32</p>

<p>  +
 पान नं .  1</p>

<p>   1 .  संघराज्य</p>

<p>
 भारतीय राज्यघटनेत आपल्या संघराज्याचे स्वरूप दिलेले आहे .  संघराज्याचे कायदेमंडळ ,  कार्यकरी
 मंडळ आणि न्यायमंडळ यांची रचना ,  अधिकारी आणि कार्ये यांचे वर्णन Iटनेत केलेले आहे .</p>

<p>                  संसद</p>

<p> संघराज्याच्या कायदेमंडळास संसद म्हणतात .  राष्ट्रपती तसेच राज्यसभा व लोकसभा असी दोन
 सभागृहे मिळून संसद बनते .  राज्यसभेस वरिष्ठ सभागृह व लोकसभेस कनिष्ठ सभागृह असे म्हणतात .
 दोन्ही सभागृहांच्या सभासदांना  ` खासदार '  म्हणतात .</p>

<p>                 लोकसभा</p>

<p>
 मतदारांनी प्रत्यक्ष निवडून दिलेले प्रतिनिधी लोकसभेत असतात .  राज्यसभेतील सभासद
 अप्रत्यक्षरीत्या निवडले जातात .  राज्यसभेपेक्षा लोकसभा अधिक शक्तिशाली आहे .</p>

<p> रचना  :  लोकसभेत एकूण 547 सभासद संख्या निर्धारित आहे .  घटक राज्यांतील प्रादेशिक
 मतदारसंघातून प्रत्यक्ष रीतीने जास्तीत जास्त 525 सदस्य निवडले जातात .  संघ राज्यक्षेत्रातून जास्तीत
 जास्त 20 सभासद असतात .  अँग्लो - इंडियन समाजास प्रतिनिधित्व मिळाले नसल्यास त्या समाजातून
 दोन सदस्यांची नियुक्ती राष्ट्रपती करतो .  अनुसूचित जाती व जमातींसाठी जागा राखून ठेवण्याची ~
 तरतूद घटनेने केली आहे</p>

<p> लोकसभा सदस्यत्वाची पात्रता  :   लोकसभेच्या सदस्यात्वासाठी पुढील पात्रता आवश्यक आहे .</p>

<p> 1 )  तो भारताचा नागरिक असावा .
 2 )  त्याचे  वय 25 वर्षापेक्षा कमी नसावे .
 3 )  संसदेने यासंबंधी वेळोवेळी विहित केलेल्या अटी त्याने पूर्ण केलेल्या असाव्यात .</p>

<p> प्रतिनिधित्व  :  प्रत्येक घटक राज्यासाठी लोकसंख्येच्या प्रमाणात लोकसभेच्या जागा निश्चित
 केलेल्या असतात .  जागांच्या संख्येनुसार प्रत्येक राज्यात प्रादेशिक मतदारसंघ आखले जातात .  प्रत्येक
 मतदारसंघातून एक प्रतिनिधी निवडला जातो .</p>

<p> लोकसभेचा कार्यकाल  :  सर्वसाधारणपणे लोकसभेचा कार्यकाल 5 वर्षाचा असतो .  राष्ट्रपती
 मुदतीपूर्वीही लोकसभेचे विसर्जन करू शकतो .  आणीबाणीमध्ये लोकसभेचा कार्यकाल एकावेळी एक
 वर्षाने वाढविता येतो .</p>

<p> अधिवेशने एका वर्षात लोकसभेची कमीत कमी दोन अधिवेशने होतात ,  दोन अधिवेशनांत सहा
 महिन्यापेक्षा जास्त अंतर असू नये .  कामकाज चालविण्यासाठी आवश्यक संख्येस गणसंख्या  ( कोरम )
 म्हणतात .  एकूण सभासदांच्या 1 / 10 एवढी गणसंख्या असते .  सदस्याला लोकसभेत हिंदी इंग्रजी
 अथवा त्याच्या मातृभाषेतून चर्चेत भाग घेता येते .  सदस्याला संसदेने ठरविलेले वेतन मिळते .  याशिवाय
 अधिवेशनास हजर राहिल्यास भत्ते मिळतात .</p>

<p> पान नं .  3 .</p>

<p> राज्यसभा</p>

<p> राज्यसभेत घटक राज्ये व संघ राज्यक्षेत्राना प्रतिनिधित्व असते .</p>

<p> रचना  :  राज्यसभेत जास्तीत जास्त सभासद संख्या 250 असते .  घटक राज्ये व संघ राज्यक्षेत्राच्या
 प्रतिनिधींची संख्या जास्तीत जास्त 238 असते ,  साहित्य विज्ञान , कला व समाजसेवा या क्षेत्रांतून 12
 सभासदांची नेमणूक राष्ट्रपती करतो .</p>

<p> सदस्यत्वाची पात्रता  :  राज्यसभेच्या सदस्यत्वासाठी पुढील पात्रता आवश्यक आहे .
 1 )  तो भारताचा नागरिक असावा .
 2 )  त्याच्या वयाची 30 वर्षे पूर्ण झालेली असावीत .
 3 )  संसदेने यासंबंधी वेळोवळी केलेल्या अटी त्याने पूर्ण केलेल्या असाव्यात .</p>

<p> प्रतिनिधित्व  :  प्रत्येक घटक राज्य व संघ राज्यक्षेत्रांसाठी जागांची संख्या राज्यघटनेने ठरवून
 दिलेली आहे .</p>

<p> निवडणूक पद््धती  :   राज्यविधान सभांच्या निर्वाचित सभासदांकडून राज्यसभेच्या सभासदांची
 निवड होते .  म्हणजेच ते अप्रत्यक्ष रीतीने निवडून येतात .
 कार्यकाल  :   राज्यसभेचे विर्सजन होत नाही .  ते संसदेचे कायम सभागृह आहे .  प्रत्येक सभासदाचा
 कार्यकाल हा सहा वर्षाचा असतो .  राज्यसभेच्या एकूण सभासदांच्या 1 / 3 सभासद दर दोन वर्षानी
 निवृत्त होतात .  तितकेच सभासद नव्याने येतात .  राज्यसभा सदस्यांना वेतन व अधिवेशन काळात
 भत्ते मिळतात .
 राज्य - सभापती  :   भारताचा उपराष्ट्रपती राज्यसभेचा पद््सिद्ध सभापती असतो .  राज्यसभेच्या
 कामकाजाचे नियमन सभापती करतो .  उपराष्ट्रपती राज्यसभेचा सभासद नसल्याने राज्यसभेत चर्चिल्या
 जाणाऱ्या प्रश्नांवर मत देता येत नाही .  मात्र एखाद्या प्रश्नावर समान मतदान झाल्यास तो निर्णायक मत
 देऊ शकतो .  राज्यसभेतून उपसभापतीची निवड होते .  सभापतीच्या गैरहजेरीत उपसभापती
 कामकाज चालवितो .
 लोकसभा - अध्यक्ष  :   लोकसभेचे सभासद आपल्यामधूनच अध्यक्ष व उपाध्यक्ष यांची निवड
 करतात .  सभासदांप्रमाणेच त्यांचा कार्यकाल पाच वर्षाचा असतो .  लोकसभा विसर्जित झाल्यावर नव्या
 सभागृहाच्या पहिल्या बैठकीपर्यंत पूर्वीचा अध्यक्ष अधिकारपदी राहतो .  लोकसभेच्या सदस्यत्वाचा त्याने
 राजीनामा दिल्यास किंवा त्याच्या विरूद्ध अविश्वासाचा ठराव मंजूर झाल्यास त्याला अधिकारपद सोडावे
 लागते .</p>

<p> लोकसभा अध्यक्षाची कार्ये व अधिकार  :</p>

<p> 1 )  सभागृहाचे अध्यक्षस्थान भूषविणे .
 2 )  लोकसभेच्या कामकाजाचे नियमन करणे .
 3 )  सभागृहात शिस्त राखणे .
 4 )  पुरवणी प्रश्न विचारण्यासाठी सभासदांना परवानगी देणे अथवा नाकारणे .
 5 )  हरकतीच्या मुद््दयावर निर्णय देणे .
 6 )  असंबद््ध प्रश्न उपस्थित करण्यास प्रतिबंध करणे .</p>

<p> पान नं .  5</p>

<p> 7 )  गणसंख्येअभावी बैठक स्थगित करणे .
 8 )  भाषणासाठी  किती वेळ द्यायचा ते ठरविणे .
 9 )  सभासदांच्या हक्कांचे व विशेषाधिकारांचे रक्षण करणे .
 10 )  संसदेच्या विविध समित्यांच्या सभापतींची नेमणूक करणे .
 11 )  एखादे विधेयक धन विधेयक आहे की नाही याबाबत निर्णय देणे .
 12 )  संसदेच्या दोन्ही सभागृहांच्या  , सं      युक्त बैठकीचे अध्यक्षस्थान भूषविणे .</p>

<p> 1 )  कायदेविषयक अधिकार  :  -  घटनेत दिलेल्या संघसूची व समवर्ती सूचीतील सर्व विषयांवर
 संसद कायदे करते .  धन विधेयकाखेरीज अन्य कोणत्याही विधेयकावर दोन्ही सभागृहांमध्ये मतभेद
 होतात
 तेव्हा संयुक्त बैठक होऊन बहुमताने निर्णय घेतला जातो .  अशा बैठकीचे अध्यक्षस्थान लोकसभेचे
 अध्यक्ष भूषवितात .
 2 )   कार्यकारी मंडळावर नियंत्रण ठेवण्याचे अधिकार  :   संसदीय लोकशाहीत मंत्री परिषद
 वास्तव सत्ताधारी असते .  ती संसदेला जबाबदार असते .  संसदेत मंत्र्यांना प्रश्न विचारले जातात .
 माहिती मिळविली जाते .  उणीवा दाखविल्या जातात .  चौकशीची मागणी केली जाते .  मंत्री परिषदेचे काम
 समाधानकारक नसेल तर लोकसभा अविश्वासाचा ठराव मंजूर करून मंत्री परिषदेस राजीनामा देण्यास
 भाग पाडते .  अशाप्रकारे संसद कार्यकारी मंडळावर नियंत्रण ठेवते .
 3 )  वित्तीय अधिकार  अर्थसंकल्पास संसद मंजुरी देते .  आकस्मिक खर्चास मान्यता देणे ,
 कर बसविणे ,  कर रद््द करणे किंवा कराचे दर बदलणे आदी बाबींमुळे लोकसभाच प्रत्यक्षात वित्तीय
 अधिकारांचा वापर करते .  धन विधेयक प्रथम लोकसभेतच मांडावे लागते .
 4 )  घटना दुरूस्तीचे अधिकार  :  संसदेला घटना दुरूस्तीची अधिकार आहे .  भारतीय घटनेत
 तीन प्रकारे दुरूस्ती करता येते .
 1 )  घटनेतील काही कलमे सामान्य बहुमताने दुरूस्त करता येतात .
 2 )  काही कलमे दुरूस्त करण्यासाठी संसदचे विशेष बहुमत आवश्यकअसते .
 3 )  काही कलमे गुरूस्त करण्यासाठी संसदेचे विसेष बहुमत तर लागतेच ,  पण त्याशिवाय त्याला
 किमान निम्म्या घटक राज्यांच्या विधान मंडळांची संमती आवश्यक असते .</p>

<p> पान नं .  7</p>

<p> 5 )  इतर अधिकार  :  संसदेत निवडून आलेले सभासद राष्ट्रपती व उपराष्ट्रपती यांच्या
 निवडणुकीत मतदान करतात .  राष्ट्रपतीविरूद्ध संसदेला महाभियोग चालविण्याचा अधिकार आहे .
 उपराष्ट्रपतीविरूद्ध राज्यसभेने ठराव केल्यास व लोकसभेने त्या ठरावास मान्यत दिल्यास त्याला
 पदावरून दूर करता येते .  गैरवर्तन , अकार्यक्षमता यांसाठी सर्वोच्च न्यायालयाच्या न्यायमूर्तीस बडतर्फ
 करण्याची शिफारस संसद राष्ट्रपतीला करू शकते .</p>

<p> संघराज्याचे कार्यकारी मंडळ :
 संघराज्याच्या कार्यकारी मंडळात राष्ट्रपती ,  पंतप्रधान व मंत्री परिषद यांचा समावेश होतो .  घटनेने
 सर्व कार्यकारी अधिकार राष्ट्रपतीला दिले असले तरीही संसदीय लोकशाहीनुसार पंतप्रधान व मंत्री
 परिषद प्रत्यक्षात अधिकारांचा वापर करतात .  मंत्री परिषद लोकसभेस जबाबदार असते .</p>

<p> राष्ट्रपती :</p>

<p> घटनेने राष्ट्रपतीस शासनातील सर्वाच्च स्थान दिले आहे .  तो प्रशासनाचा औपचारिक किंवा घटनात्मक
 प्रमुख असतो .  त्याच्याच नावाने सर्व आदेश निघतात व कृती होतात .  घटनेने त्याला दिलेले अधिकार
 पंतप्रधान व मंत्री परिषदेच्या सल्ल्याने वापरण्याचे त्याच्यावर बंधन असते .
 निवडणूक  :  संसदेची दोन्ही सभागृहे व विधानसभा यांतील निर्वाचित सभासद राष्ट्रपतीची निवड
 करतात .
 पात्रता  :  राष्ट्रपतीपदाच्या उमेदवारासाठी पुढील पात्रता आवश्यक आहे .
 1 )  तो भारताचा नागिरक असावा .
 2 )  त्याच्या वयाची 35 वर्षे पूर्ण झालेली असावीत .
 3 )  तो लोकसभेचा सभासद म्हणून निवडून येण्यास पात्र असावा .</p>

<p> कार्यकाल  :   राष्ट्रपतीचा कार्यकाल पाच वर्षाचा असतो .  तो स्वखुशीने राजीनामा देऊ शकतो .
 राष्ट्रपतीकडून घटनाभंग झाल्यास संसद महाभियोगाद्वारे त्याला पदभ्रष्ट करू शकते .  राष्ट्रपतीचे वेतन ,
 भत्ते व इतर सोयी - सवलती ठरिवण्याचा अधिकार संसदेला आहे .
 अधिकार  :  राष्ट्रपतीच्या नावे देशाचा कारभार चालतो .  तो संघराज्याचा प्रमुख आहे .  तिन्ही
 सेनादलांचा तो सर्वोच्च सेनापती आहे .  राष्ट्रपतीच्या अधिकारांचे वर्गीकरण पुढीलप्रमाणे करता येते .
 1 )  कार्यकारी अधिकार
 2 )  कायदेविषयक अधिकार
 3 )  न्यायविषयक अधिकार
 4 )  वित्तीय अधिकार
 5 )  आणीबाणीविषयक अधिकार</p>

<p> 1 )  कार्यकारी अधिकार  पंतप्रधान व मंत्री परिषदेची नेमणूक राष्ट्रपती करतो .  महान्यायवादी ,
 नियंत्रक व महालेखा परीक्षक ,  विविध आयोगांचे सदस्य व अध्यक्ष ,  राज्यपाल , राजदूत ,  लष्कराच्या
 तिन्ही दलांचे प्रमुख या सर्व नेमणुका राष्ट्रपती करतो .  प्रशासनाचे नियंत्रण करणे ,  मार्गदर्शन करणे ,
 परराष्ट्रविरूद््ध युद्ध घोषित करणे ,  त्यांच्याशी तह वा करार करणे इत्यादी बाबी राष्ट्रपतीच्या ~
 कार्यकारी अधिकारात येतात .</p>

<p> पान नं .  9</p>

<p> 2 )  कायदेविषयक अधिकार  :  -  संसदेने मंजूर केलेल्या विधेयकास राष्ट्रपतीच्या संमतीनेच
 कायद्याचा दर्जा प्राप्त होतो .  राष्ट्रपती संसदेची बैठक बोलावितो .  सत्र समाप्त करतो .  आवश्यकता
 वाटल्यास लोकसभा विसर्जित करतो .  संसदेच्या दोन्ही सभागृहांसमोर अभिभाषण करतो .  त्यांना संदेश
 पाठवितो .  संसदेच्या विरामकाळात अध्यादेश जारी करतो .
 3 )  न्यायविषयक अधिकार  :  सर्वोच्च व उच्च न्यायालयांतील सर्व न्यायाधीशांच्या नेमणुका
 करणे ,  उच्च न्यायलयांतील न्यामूर्तीची संख्या ठरविणे ,  संसदेने न्यायाधीशांना बडतर्फ करण्याचा ठराव
 विशेष मताधिक्याने मंजूर केल्यास त्यांना बडतर्फ करणे , अपराध्यास दया दाखविणे .  शिक्षा स्थगित
 करणे ,  शिक्षेत सूट देणे किंवा ती सौम्य करणे हे अधिकार राष्ट्रपतीला आहेत .
 4 )  वित्तीय अधिकार  :   राष्ट्रपतीच्या शिफारशीनेच धन विधेयक लोकसभेत मांडता येते .
 संघराज्याच्या काही करांच्या उत्पन्नाचे संघराज्य आणि घटकराज्ये यात वितरण करणे संघराज्याकडून
 ~ घटक राज्याला अनुदान देणे .  यासंबंधीच्या शिफारशी करण्यासाठी वित्त आयोगाची राष्ट्रपती नेमणूक
 ~ करतो .
 5 )   आणीबाणीविषयक अधिकार :  पुढीलपैकी कोणत्याही प्रकारची आपत्कालीन स्थिती
 निर्माण झाल्यास राष्ट्रपती आणीबाणी जाहीर करतो .
 1 )  युद््ध ,  बाह्य आक्रमण किंवा अंतर्गत सशस्त्र उठाव झाल्यास
 2 )  राज्यात घटनात्मक पेचप्रसंग निर्माण झाल्यास ,
 3 )  आर्थिक पेचप्रसंग निर्माण झाल्यास .</p>

<p> आणीबाणीचे घोषणापत्र संसदेला सादर करावे लागते आणि दोन महिन्यांच्या आत संसदेची त्याला
 मान्यता मिळणे आवश्यक असते .  बाह्य आक्रमण किंला अंतर्गत सशस्त्र उटावामुळे भारताच्या
 ~सुरक्षिततेस धोका निर्माण झाला आहे असे वाटल्यास राष्ट्रपतीना 352 व्या कलमाप्रमाणे आणीबाणी
 जाहीर करता येते .  1988 मध्ये राज्यघटनेत 59 वी घटना दुरूस्ती करण्यात आली .  या घटना
 ~दुरूस्तीने वरील तरतुदीत खालील भर घातली आहे .
 1 )  संपूर्ण पंजाबमध्ये किंवा पंजाबातील एखाद्या भागात अतंर्गत अशांततेमुळे भारताची एकात्मता
 धोक्यात आली आहे असे राष्ट्रपतींना वाटल्यास राष्ट्रपती तेथे आणीबाणी घोषित करू शकतात .
 2 )  कायद्याच्या आधाराशिवाय कोणत्याही व्यक्तीचे जीवित किंवा स्वातंत्र्य हिरावून घेतले जाणार
 नाही असा हक्क 21 व्या कलमाने दिला आहे .  परंतु राष्ट्रपतीने पंजाबमध्ये आणिबाणी जाहीर केली
 असेल तर या कलमाची कार्यवाही पंजाबपुरती स्थगित होते .</p>

<p> वरील दोन्हीही बदल 1988 चा 59 वा घटना दुरूस्ती कायदा अंमलात आल्यापासून 2 वर्षाच्या
 कालावधीनंतर रद्द होतील .  वरील सर्व अधिकार राष्ट्रपतीला मंत्री परिषदेच्या सल्ल्यानेच वापरावे
 ~लागतात .</p>

<p> उपराष्ट्रपती  :</p>

<p>
 संसदेच्या दोन्ही सभागृहांतील निर्वाचित सभासद उपराष्ट्रपतीची निवड करतात .  या पदाच्या
 उमेदवारासाठी खालील पात्रता आवश्यक आहेत .
 1 )  तो भारताचा नागरिक असावा .
 2 )  त्याच्या वयाची 35 वर्षे पूर्ण झालेली असावीत .
 3 )  तो राज्यसभेचा सभासद म्हणून निवडून येण्यास पात्र असावा .</p>

<p>
 पान नं .  10</p>

<p> उपराष्ट्रपतीची मुदत पाच वर्षाची असते .  संसद त्याचे वेतन व भत्ते निश्चित करते .  तो राज्यसभेचा
 पदसिद््ध सभापती असतो .  राष्ट्रपतीच्या गैरहजेरीत तो राष्ट्रपती म्हणून काम पाहतो .</p>

<p> मंत्री परिषद  :</p>

<p> लोकसभेतसज्या पक्षाला बहुमत असते ,  त्याच्या नेत्यास राष्ट्रपती पंतप्रधानपदी नेमतो .  पंतप्रधानाच्या
 सल्ल्यानच तो इतर मंत्र्यांची नेमणूक करतो .  एखादा मंत्री संसदेचा सभासद नसेल तर ,  त्याला सहा
 महिन्यांच्या आत संसदेचे सदस्य व्हावे लागते .  मंत्री परिषद लोकसभेस जबाबदार असते .  ही जबाबदारी
 सामूहिक स्वरूपाची असते .  लोकसभेने अविश्वासाचा ठराव मंजूर केल्यस मंत्री परिषदेस राजीनामा
 द्यावा लागतो .</p>

<p> मंत्री परिषदेचे अधिकार व कार्ये  :  मंत्री परिषदेचे अधिकार आणि कार्ये पुढीलप्रमाणे आहेत .
 1 )  कायदेविषयक अधिकार 2 )  वित्तीय अधिकार ,  3 )  प्रशासकीय अधिकार ,
 4 )  परराष्ट्रसंबंधविषयक अधिकार</p>

<p> 1 )  कायदेविषयक अधिकार  :   मंत्री परिषदेच्या पुढाकाराने संसदेत कायदे मंजूर होतात .  मंत्री
 परिषदेचा पाठिंबा असल्याशिवाय कोणतेही विधेयक मंजूर होत नाही .  अध्यादेशाचा मसुदा
 ~मंत्री परिषद तयार करते .
 2 )  वित्तीय अधिकार  :  मंत्री परिषद देशाच्या अर्थ व्यवस्थेवर नियंत्रण ठेवते .  नवे कर
 आकारणे ,  कर्जै उभारणे व निरनिराळ्या बाबींवर खर्च करणे यांसारख्या प्रश्नावर मंत्री परिषद निर्णय घेते
 तिने केलेल्या सूचना सर्वसाधारणपणे संसदेकडून स्वीकारल्या जातात .  मंत्री परिषदेची कोणताही सदस्य
 धन विधेयक संसदेत मांडू शकतो .
 3 )  प्रशासकीय अधिकार  :  मंत्री परिषद धोरण ठरविते व त्याची अंमलबजावणी करते .  या
 धोरणाची अंमलबजावणी करण्यात देशभर विखुरलले सनदी नोकर मंत्री परिषदेस मदत करतात .  मंत्री
 परिषदेच्या शिफारशीनुसार राष्ट्रपती सर्व महत्त्वाच्या नेमणुका करतो .  संसदेने संमत केलेल्या कायद्याची
 अंमलबजावणी करण्याची जबाबदारी मंत्री परिषदेची असते .
 4 )  परराष्ट्रविषयक अधिकार  :   देशाचे परराष्ट्र धोरण आखणे ,  तह करणे ,  करार करणे इत्यादी
 कार्ये राष्ट्रपतीच्या नावे मंत्री परिषद करते .
 पंतप्रधान</p>

<p> घटनेत पंतप्रधानास महत्त्वाचे स्थान आहे .  राष्ट्रपती पंतप्रधानाच्या शिफारशीनुसार मंत्र्यांची नेमणूक
 करतो .  आवश्यकतेनुसार पंतप्रधान मंत्री परिषदेची पुनर्रचना करतो .  मंत्र्याला राजीनामा देण्यास सांगू
 शकतो .  राजीनामा न दिल्यास त्याला बडतर्फ करण्याची शिफारस राष्ट्रपतीकडे करतो .  पंतप्रधानाचा
 राजीनामा म्हणजे संपूर्ण मंत्री परिषदेचा राजीनामा होय .  पंतप्रधान केंद्र शासनाच्या संपूर्ण प्रशासनाला
 मार्गदर्शन करतो व नियंत्रण ठेवतो .  तो मंत्र्यांमधील मतभेद दूर करतो .</p>

<p> पंतप्रधान -  दुवा  :  मंत्री परिषदेच्या बैठकी पंतप्रधानांच्या अध्यक्षतेखाली होतात .  त्याचा
 राष्ट्रपतीबरोबर नेहमी संपर्कअसतो ,  तो मंत्री परिषदेच्या निर्णयाची माहिती राष्ट्रपतीस देतो .  मंत्री परिषद
 व राष्ट्रपती यांच्यामधील हा अत्यंत महत्त्वाचा दुवा आहे -
 संसदेतीन स्थान  :  पंतप्रधान लोकसभेचा नेता असतो .  मंत्री परिषदेचा प्रमुख या नात्याने धोरण</p>

<p> पान नं .  12</p>

<p> महत्त्वाच्या नेमणुका राष्ट्रपती पंतप्रधानाच्या सल्ल्यानेच करतो .</p>

<p> न्यायमंडळ  :</p>

<p> न्यायव्यवस्थेच्या शिरोगामी सर्वाच्च न्यायलय आहे .  ते दिल्ली येथे आहे .  सर्वाच्च न्यायालयात एक
 सरन्यायाधीश व इतर जास्तीत जास्त पंचवीस न्यायाधीश असू शकतात .  कायदा करून न्यायाधीशांची
 संख्या निश्चित करण्याचा अधिकार संसदेला आहे .  सरन्यायाधीशाची नेमणूक राष्ट्रपती करतो आणि
 सरन्यायाधीशाशी सल्लामसलत करून तो इतर न्यायाधीशाची नेमणूक करतो .</p>

<p> सर्वोच्च न्यायालयाच्या न्यायाधीशपदासाठी पात्रता  :</p>

<p> 1 )  तो भारताचा नागरिक असावा .
 2 )  उच्च न्यायालयात त्याने किमान 5 वर्षे न्यायाधीश म्हणून काम केले असले पाहिजे .  किंवा
 निदान 10 वर्षे त्याने उच्च न्यायालयात वकिली केली असली पाहिजे किंवा राष्ट्रपतीच्या मते
 तो नामवंत कायदेपंडित असला पाहिजे .  एकदा न्यायाधीश म्हणून नेमणूक झाल्यावर तो
 वयाची 65 वर्षे होईपर्यंत त्या पदावर राहू शकतो .  न्यायनंडळाची निःपक्षपाती वृत्ती व
 स्वातंत्र्य यांचे रक्षण करण्यासाठी घटनेने त्यांना कार्यकालाच्या सुरक्षिततेची हमी दिली आहे .
 निवृत्तीनंतर न्यायाधीशाला भारतातील कोणत्याही न्यायलयात वकिली करता येत नाही .</p>

<p> बडतर्फी  दुर्वर्तन किंवा अकार्यक्षमता या कारणांवरून संसदेने रीतसर ठराव केल्यास राष्ट्रपती
 न्यायाधीशांस बडतर्फ करतो .</p>

<p> वेतन व भत्ते  :   सर्वोच्च न्यायालयाच्या न्यायाधीशांचे वेतन व भत्ते घटनेने निश्चित केले आहेत .
 अधिकार क्षेत्र  :   एखाद्या विशिष्ट खटल्यात न्याय देण्याच्या न्यायालयाच्या अधिकारास
  """""""""""""""" न्यायालयाचे अधिकार क्षेत्र """"""""""""""""  म्हणतात .  घटनेने ठरविलेले सर्वोच्च न्यायालयाचे अधिकार क्षेत्र
 पुढीलप्रमाणे आहे .</p>

<p> 1 )  प्रारंभिक अधिकार क्षेत्र  :   अ )  संघराज्य घटकराज्य अथवा घटकराज्य - घटकराज्य
 संबंधातील खटले फक्त सर्वोच्च न्यायालयातच चालतात .  ब ) नागरिकांच्या मूलभूत अधिकारांवरील
 आक्रमणासंबंधी खटले या न्यायालयात चालविले जातात .</p>

<p> 2 )  अपीलाबाबतचे अधिकार क्षेत्र  :  अपीलाबाबातच्या अधिकार क्षेत्रात खालील दोन
 प्रकारच्या दाव्यांचा समावेश होतो .
 अ )  घटनाविषयक दावे ब )  आपल्या निकालांचे पुनः परीक्षण करण्यासंबंधीचे दावे .</p>

<p> अ )  घटनाविषयक दावे  :   उच्च न्यायालयाने एखाद्या दिवाणी किंवा फौजदारी खटल्यात
 दिलेल्या निकालात घटनेच्या अर्थासंबंधी कायदेशीर प्रश्न असले तर सर्वाच्च न्यायालयात अपील करता
 येते .  घटनाविषयक दावे पुढील तीन प्रकारचे असतात .
 1 )  दिवाणी दावे ,  2 )  फौजदारी दावे ,  3 )  खास दावे .</p>

<p> 1 )  दिवाणी दावे  :  उच्च न्यायालयाने निकालात घटनेच्या अर्थासंबंधी कायदेशीर
 प्रश्न असेल तर सर्वाच्च न्यायालयात अपील करता येते .
 2 )  फौजदारी दावे  :  पुढील प्रकराच्या दाव्यांत सर्वाच्च न्यायलयात अपील
 करता येते .</p>

<p> पान नं .  13</p>

<p> अ )  खालच्या न्यायलयात निर्दोष सुटलेल्या आरोपीस उच्च न्यायालयाने फाशींची शिक्षा दिली
 असल्यास .
 ब )  उच्च न्यायालयाने खालच्या न्यायालयाकडील खटला काढून आपल्याकडे घेऊन आरोपीस
 फाशीची शिक्षा दिली असल्यास .
 क )  एकादा खटला सर्वोच्च न्यायालयात अपील करण्याजोगा आहे असे शिफारसपत्र उच्च
 न्यायालयाने दिले असल्यास .</p>

<p> 3 )  खास दावे  -  अपीलाची खास परवानगी  :   देशातील कोणत्याही न्यायलयाने किंवा
 न्यायाधिकरणाने दिलेल्या निकालाविरूद््ध न्यायलय अपीलाची खास परवानगी देऊ शकते .</p>

<p> ब )   न्यायिक पुनर्विलोकन  :  भारतातील कोणत्याही विधी मंडळाचा कायदा ,  राष्ट्रपती वा
 राज्यपालाने काढलेला अध्यादेश घटनेशी विसंगत असेल तर सर्वाच्च न्यायलयास तो रद्द करण्याचा
 अधिकार आहे .  यालाच न्यायिक पुनर्विलोकनाचा अधिकार म्हणतात .</p>

<p> 4 )  सल्लामसलतीचे अधिकार क्षेत्र  :   महत्त्वाच्या कायद्यांच्या प्रश्नांबाबत आवश्यक
 वाटल्यास राष्ट्रपती सर्वोच्च न्यायालयाकडे सल्ला मागू शकतो .</p>

<p> 5 )  अभिलेख न्यायलय  :   सर्वोच्च न्यायालय हे अभिलेख न्यायलय आहे .  त्याच्या
 निर्णयांची नोंद ठेवली जाते आणि त्यांना कायद्याचे स्वरूप प्राप्त होते .  कोणत्याही न्यायलयासमोर
 सर्वोच्च न्यायालयाच्या अभिलिखित निर्णयांना हरकत घेता येत नाही .  सर्वोच्च न्यायालयाचे निर्णय
 भारतातील सर्व न्यायालयांवर बंधनकारक असतात .  न्यायालयाचा अधिक्षेप केल्यास न्यायलय शिक्षा
 करू शकते .</p>

<p> संघराज्याचे कायदेमंडळ , कार्यकारी मंडळ आणि न्यायमंडळ यांचे परस्पर संबंध  :  घटनेने
 घालून दिलेल्या मर्यादेत संघराज्याचे कायदेमंडळ , कार्यकारी मंडळ , आणि न्यायमंडळ काम करते . तिन्ही
 अंगांमध्ये सुसंवाद राहील अशी योजना घटनेनेच केली आहे .  ती अंगे परस्परांवर नियंत्रण ठेवतील अशी
 व्यवस्था घटनेत केली आहे .  पुढील बाबींवरून हे स्पष्ट होईल .</p>

<p> 1 )  राष्ट्रपती वा राज्यपाल यांचा अध्यादेश आणि संसद किंवा राज्यविधानमंडळाचा कायदा जर
 घटनाविरोधी असेल तर सर्वोच्च न्यायालय तो रद्द ठरवू शकते .  घटनेचा अर्थ लावण्याचा
 अंतिम अधिकार सर्वोच्च न्यायालयाला आहे .
 2 )  संसद कायदा करून न्यायाधीशांची संख्या निश्चित करू शकते .  दुर्वर्तन व अकार्यक्षमता
 यांसाठी न्यायाधीशाला बडतर्फ करण्याची शिफारस संसद राष्ट्रपतीला करू शकते .
 न्यायाधीशांच्या नेमणुकीचे अधिकार राष्ट्रपतीला आहेत .
 3 )  लोकसभेने अविश्वास व्यक्त केल्यास पंतप्रधान व मंत्री परिषदेस राजीनामा द्यावा लागतो ,
 तर लोकसभेने मंत्री परिषदेच्या धोरणास विरोध केल्यास पंतप्रधान राष्ट्रपतीस लोकसभा
 मुदतीपूर्वी विसर्जित करण्याचा सल्ला देऊ शकतो .  प्रश्न , ठराव यांद््वारे शासकीय धोरणावर
 चर्चा करून ,  चौकशीची मागणी करून संसद कार्यकारी मंडळावर नियंत्रण ठेवते .</p>

<p>
 पान नं .  14</p>

<p> 4 )  राष्ट्रपतीला लोकसभा विसर्जित करण्याचा अधिकार आहे ,  तर संसद राष्ट्रपतीवर  ` महाभियोग '
 चालवून त्याला पदावरून दूर करू शकते .</p>

<p>
 पान नं .  25</p>

<p> 3 .  संयुक्त राष्ट्रे आमि आंतरराष्ट्रीय सामंजस्य</p>

<p>
 युद््ध हा मानव वंशाला लाभलेला एक शाप आहे .  याच शतकात दोन वेळा जागतिक युद््धे झाली
 आणि कोट्यवधी माणसे मारली गेली .  मानवी प्रगतीसाठी शांततेची गरज आहे .  शांतता हवी असेल तर
 राष्ट्रांराष्ट्रांतील तंटे शांततामय मार्गाने सोडविले गेले पाहिजेत ,  म्हणून पहिल्या महायुद््धाच्या
  ( 1914 - 18 )  शेवटी अमेरिकेचे अध्यक्ष वुड्रो विस्लन यांच्या पुढाकाराने जागतिक शांतता
 राखण्यासाठी राष्ट्रसंघाची  ( League of Nations )  स्थापना झाली .  राष्ट्रसंघाने आपले कार्य 1920
 मध्ये सुरू केले .  परंतु दुर्दैवाने बड्या राष्ट्रांनी प्रामाणिक सहकार्य न दिल्यामुळे राष्ट्रसंघ आंतरराष्ट्रीय
 शांतता राखण्याच्या कार्यात अपयशी ठरला .  दुसरे महायुद्द्ध 1939 - 45 या काळात झाले .  या युद््धात
 पहिल्या महायुद््धापेक्षा कितीतरी पटीने अधिक मनुष्यहानी आणि वित्तहानी झाली .  अण्वस्त्रांचा वापर
 प्रथम केला गेला .  त्यामुळे एक गोष्ट स्पष्ट झाली ती ही की यापुढे महायुद््ध झालेच तर पूर्ण मानव
 जातच नष्ट होईल .  युद््धाच्या ज्वालांत होरपळून निघालेलेय् जगातील राष्ट्रांनी आंतरराष्ट्रीय शांतता
 कायम
 ठेवण्यासाठी नवी संस्था निर्माण केली .  त्या संस्थेचे नाव संयुक्त राष्ट्रे  ( United Nations )  आहे .
 सॅन फ्रॉन्सिस्को परिषदेत पन्नास राष्ट्रांच्या प्रतिनिधींनी संयुक्त राष्ट्रांच्या सनदेवर सह्या केल्या .
 अशाप्रकारे संयुक्त राष्ट्र संघटना 24 ऑक्टोबर 1945 रोजी अस्तित्वात आली .  24 ऑक्टोबर हा
 दिवस सर्व जगभर  ` सयुंक्त राष्ट्र दिन '  म्हणून साजरा केला जातो .</p>

<p> उद््दिष्टे  :  संयुक्त राष्ट्रांच्या सनदेच्या पहिल्या कलमात संघटनेचे हेतू आणि उद््दिष्टे सांगितली
 आहेत .  ती थोडक्यात खालीलप्रमाणे :</p>

<p> 1 )  आंतरराष्ट्रीय शांतता आणि सुरक्षा टिकविणे आणि शांतताभंग , आक्रमण याविरूद््ध
 सामुदायिक उपाय योजना करणे .
 2 )  जागतिक शांततेसाठी राष्ट्राराष्ट्रांत मित्रत्वाचे व सलोख्याचे संबंध निर्माण करणे .
 3 )  आंतरराष्ट्रीय क्षेत्रांतील  आर्थिक , सामाजिक ,  सांस्कृतिक आणि मानवतावादी समस्या
 सोडविण्यासाठी आणि मानवी हक्क व मानवी स्वातंत्र्य यांचा उपभोग घेता यावा म्हणून
 राष्ट्राराष्ट्रांत सहकार्य वाढविणे .
 4 )  वरील उद्दिष्टे साध्य करण्यासाठी निरनिराळ्या राष्ट्रांच्या प्रयत्नात सुसंवाद आणि सहकार्य
 निर्माण करणारे केंद्र बनणे .</p>

<p> संयुक्त राष्ट्रांच्या सनदेत सदस्य राष्ट्रांनी कोणत्या नियमांचा अवलंब करावा याचेही स्पष्टीकरण
 दिले आहे .  ते नियम पुढीलप्रमाणे आहेत .  सर्व सदस्यांनी आपसातील तंटे शांततेच्या मार्गाने मिटवावेत .
 परस्परांविरूद्ध त्यांनी सैन्यबळाचा वापर करू नये .  संयुक्त राष्ट्रांनी एखाद्या आक्रमक राष्ट्राविरूद््ध
 केलेल्या कार्यवाहीस मदत करावी .  सदस्य राष्ट्रांनी सनदेत नमूद केल्याप्रमाणे आपापली कर्तव्ये
 पाळावीत .</p>

<p> संयुक्त राष्ट्रांचे मुख्य घटक  :   संयुक्त राष्ट्रांचे 6 मुख्य घटक आहेत .
 1 )  आमसभा , 2 )  सुरक्षा समिती , 3 )  आर्थिक आणि सामाजिक परिषद ,  4 )  विश्वस्त
 परिषद 5 )  आंतरराष्ट्रीय न्यायालय ,  आणि 6 )  सचिवालय .</p>

<p> पान नं .  27</p>

<p> 1 )  आमसभा  :   संयुक्त राष्ट्रांचा सदस्य - देश हा आमसभेचा सभासद असतो .  प्रत्येक देशाला
 आमसभेत पाच प्रतिनिधी पाठवता येतात .   परंतु प्रत्येक देशाला एकच मत असते .  आमसभेतील सर्व
 सदस्य राष्ट्रे समान दर्जाची मानली जातात .  आमसभेचे अधिकार शिफारसवजा असतात .  सामान्यतः
 आमसभेचे वर्षातून एकदा अधिवेशन होते .  निम्म्याहून अधिक राष्ट्रांनी विनंती केल्यास खास अधिवेशन
 घेतले जाते .  आमसभेत निर्णय बहुमताने घेतला जातो .  परंतु आंतरराष्ट्रीय सुरक्षा ,  नवीन सभासद घेणे
 अथवा सभासदत्व रद्द करणे इत्यादी महत्त्वाच्या बाबींवर 2 / 3 मतांनी निर्णय घ्यावा लागतो .  सध्या
 आमसभेची सदस्य संख्या 159 आहे .</p>

<p> दरवर्षी आमसभा आपला अध्यक्ष निवडते .  आमसभा विविध परिषदांमार्फत कार्य करते .</p>

<p> कार्य :</p>

<p> 1 )  आर्थिक , सामाजिक ,  सांस्कृतिक ,  शैक्षणिक इत्यादी क्षेत्रांत राष्ट्राराष्ट्रांत सहकार्य वाढविणे .
 2 )  मानवी अधिकार आणि स्वातंत्र्याच संरक्षण करणे .
 3 )  आर्थिक आणि सामाजिक प्रगतीला पोषक वातावरण निर्माण करणे .
 4 )  आंतरराष्ट्रीय कायद्याच्या विकासाचे प्रयत्न करणे .
 5 ) आंतरराष्ट्रीय संघर्ष शांततेने सोडविण्यासाठी प्रयत्न करणे .
 6 )  सुरक्षा परिषदेचे अस्थायी सभासद तसेच विश्वस्त परिषद आणि आर्थिक आणि सामाजिक
 परिषदेच्या सभासादांची निवड करणे .
 7 )  नवीन राष्ट्रांना सभासदत्व देणे अथवा रद्द करणे याबाबतीत सुरक्षा परिषदेकडे शिफारस
 करणे .
 8 )  संयुक्त राष्ट्रांचे अंदाजपत्रक मंजूर करणे .
 9 )  संयुक्त राष्ट्रांच्या सनदेत दुरूस्ती करणे .</p>

<p> 2 )  सुरक्षा परिषद :   सुरक्षा परिषद , संयुक्त राष्ट्रांचे कार्यकारी मंडळ आहे .  जबाबदारीच्या
 दृष्टीने संयुक्त राष्ट्राचा हा सर्वात महत्त्वाचा घटक आहे .  सुरक्षा परिषदेचे 15 सदस्य असतात .  त्यापैकी
 पाच सदस्य कायम सभासद असतात .  अमेरिका , रशिया फ्रान्स , इंग्लंड आणि चीन ही पाच कायम
 सदस्य राष्ट्रे होत .  सभासद राष्ट्रापैकी 10 अस्थायी सभासद असतात .  त्यांची निवड आमसभा दोन
 वर्षाकरिता करते .  सुरक्षा परिषदेचे कार्य बड्या राष्ट्रांच्या सहकार्यावर अवलंबून असते .  साध्या कारभार
 विषयक प्रश्नांवर 9 मतांनी निर्णय घेता येतो .  परंतु महत्त्वाच्या प्रश्नांवर निर्णय घेण्यासाठी पाचही बड्या
 राष्ट्रांची संमती आवश्यक असते .  पाच बड्या राष्ट्रांपैकी एकाही राष्ट्राचा विरोध असला तरी सुरक्षा
 परिषदेला तो निर्णय अमलात आणता येत नाही .  यालाच नकारिधिकार किंवा  ` व्हेटो '  चा अधिकार
 असे म्हणतात .</p>

<p> कार्य  :  आंतरराष्ट्रीय शांतता व सुरक्षितता राखण्याची जबाबदारी प्रामुख्याने सुरक्षा परिषदेवर
 ~आहे .  ही परिषद राष्ट्रराष्ट्रांतील तंट्याची चौकशी करते तसेच हे तंटे मिटविण्याचे मार्ग सुचविते
 निःशस्त्रीकरणाच्या योजना तयार करून शस्त्रास्त्रांच्या वाढीवर मर्यादा घालण्याचा प्रयत्न करते .  एखादा
 तंटा शांततेच्या मार्गाने मिटत नसेल तर आक्रमक राष्ट्राविरूद्ध लष्करी कारवाई करू शकते .  वार्षिक
 अहवाल आणि खास अहवालही परिषद आमसभेकडे पाठविते .</p>

<p> पान नं .  28</p>

<p> 3 )   आर्थिक आणि सामाजिक परिषद  :  आर्थिक व सामाजिक परिषदेत एकूण 54 सदस्य
 असतात .  त्यांची निवड आमसभेकडून होते .  ही निवड 3 वर्षासाठी केलेली असते .  बड्या राष्ट्रांना या
 परिषदेचे कायम सभासदत्व नसले तरी सोयीसाठी त्यांना नेहमीच सभासदत्त्व दिले जाते .  भारताने
 ~पंधरा वर्षे आर्थिक व सामाजिक परिषदेवर प्रत्यक्ष काम केले आहे .  जगातील सर्व लोकांना अन्न ,
 ~वस्त्र ,  निवारा मिळावा या साठी ही परिषद झटते .  तसेच बेकारी ,  शिक्षण इत्यादी प्रश्नांसंबधी मार्गदर्शन
 करते .
 मानवी हक्कांच्या समस्यांचा अभ्यास करते .  संयुक्त राष्ट्रांच्या सर्व घटकांत या परिषदेचे कार्य मूलभूत
 स्वरूपाचे आहे .</p>

<p> 4 )  विश्वस्त परिषद  :  -  दुसऱ्या महायुद््धानंतर पारतंत्र्यात असलेले बरेच देश स्वतंत्र झाले
 प्रत्येक राष्ट्राला स्वतंत्र होण्याचा हक्क आहे हे तत्त्व आज सर्वमान्य झाले आहे .  संयुक्त राष्ट्रांनी
 पारतंत्र्यात असलेल्या या देशांना स्वातंत्र्य मिळवून देण्यात फार मोठे यश मिळविले आहे .  दोन्ही
 महायुद््धांत विजेत्या राष्ट्रांनी जे प्रदेश पराभूत राष्ट्रांकडून जिंकून घेतले होते त्या प्रदेशाची त्यांनी
 ~विश्वस्त म्हणून देखरेख करावी आणि योग्य वेळ येताच त्यांना स्वातंत्र्य द्यावे हे तत्त्व या शतकात
 आंतरराष्ट्रीय
 क्षेत्रात रूढ झाले आहे .  विश्वस्त प्रदेशावर देखरेख करण्यासाठी आणि तेथील कारभार योग्य प्रकारे
 चालविला जातो किंवा नाही हे पाहण्यासाठी संयुक्त राष्ट्रांच्या विश्वस्त परिषदेची स्थापना करण्यात
 ~आली .  सुरक्षा परिषदेची पाट बडी राष्ट्रे त्याची कायम सदस्य आहेत .  याखेरीज विश्वस्त प्रदेशाचा
 कारभार
 पहाणारी राष्ट्रे या परिषदेची सभासद असतात .  परिषदेच्या वर्षातून दोन सभा होतात व निर्णय
 ~बहुमताने घेतले जातात .</p>

<p> कार्य  :</p>

<p> 1 )  विश्वस्त प्रदेशावर देखरेख करणे .
 2 )  त्या प्रदेशावर देखरेख ठेवणाऱ्या राष्ट्रांकडून त्या प्रदेशाविषयीचा वार्षिक अहवाल स्वीकारणे ,
 3 )  विश्वस्त प्रदेशातील जनतेचे विनंती अर्ज स्वीकारणे तसेच गरज पडल्यास त्या प्रदेशाची
 प्रत्यक्ष पाहणी करणे .</p>

<p> एकूण 11 प्रदेश या परिषदेच्या देखरेखीखाली होते .  आज केवळ जवळजवळ सर्वच प्रदेश स्वंतत्र
 झाले आहेत .</p>

<p> 5 )  आंतरराष्ट्रीय न्यायालय  :   राष्ट्रसंघाने स्थापन केलेल्या  ` कायम आंतरराष्ट्रीय
 न्यायालयास '   ` आंतरराष्ट्रीय न्यायालय '  हे नामाभिधान संयुक्त राष्ट्रांच्या सनदेत देण्यात आले आणि त्या
 न्यायालयाच्यासनदेलाच संयुक्त राष्ट्रांच्या सनदेचा भाग बनविण्यात आले .  या न्यायालयाची मुख्य
 कचेरी नेदरलॅड येथील  ` हेग '  शहरात आहे .  या न्यायालयात पंधरा न्यायाधीश असून त्यांची निवड
 आमसभा आणि सुरक्षा परिषदेद््वारे केली जाते .  जगातील विविध भागातील न्यायपंडित या परिषदेवर
 नियुक्त केले जातात .  न्यायधीशांचा कार्यकाल 9 वर्षाचा असून दर तीन वर्षांनी 1 / 3 न्यायाधीश
 निवृत्त होतात .
 कार्य
 1 )  आंतरराष्ट्रीय कायद्याच्या आधारे राष्ट्रराष्टांतील तंटे सोडविणे .
 2 )  आमसभेच्या परवानगीने आंतरराष्ट्रीय संघटनेतील विविध परिषदांना कायदेशीर सल्ला देणे .</p>

<p> पान नं .  29</p>

<p> 6 )  सचिवालय  :  संयुक्त राष्ट्रांचे दैनंदिन कार्य सचिवालयामार्फत चालते .  सचिवालयाची
 मुख्य कचेरी न्यूयॉर्क येथे आहे .  सचिवालयाचा प्रमुख सरचिटणीस असतो .  त्याची नेमणूक सुरक्षा
 परिषदेच्या शिफारसीनुसार आमसभा करते .  त्याचा कार्यकाल पाच वर्षाचा असतो .  त्याच्या मदतीसाठी
 उपसचिव व अन्य अधिकारी वर्ग असतो .  सचिवालयाचा सरचिटणीस हा संयुक्त राष्ट्रांचा मुख्य
 प्रशासकीय अधिकारी असतो .</p>

<p> सरचिटणीसाची कार्ये  :
 1 )  दैनंदिन कारभारावर लक्ष ठेवणे .
 2 )  नोकरवर्गाची नियुक्ती करणे .
 3 )  विविध परिषदांच्या सभांना उपस्थित राहून चर्चेत भाग घेणे .
 4 )  बैठकीची तयारी करणे .
 5 )  सभासदांना निमंत्रण पाठविणे .
 6 )  सभेची कार्यक्रम पत्रिका तयार करणे ,
 7 )  संयुक्त राष्ट्रांचे तफ्तर सांभाळणे .
 8 )  आंतरराष्ट्रीय कराराच्या नोंदी करून त्या प्रसिद्ध करणे .
 9 )  संयुक्त राष्ट्रांचे वार्षिक अंदाजपत्रक तयार करणे इत्यादी कामे करावी लागतात .</p>

<p> आंतरराष्ट्रीय शांतता व सुरक्षितता एखाद्या घटनेमुळे धोक्यात आली आहे असे संयुक्त राष्ट्रांच्या
 सरचिटणीसास वाटते तर तो ती बाब सुरक्षा परिषदेच्या नजरेस आणू शकतो .  सरचिटणीसास सभासद
 राष्ट्राच्या बरोबरीने अधिकार आहेत .  संयुक्त राष्ट्रांचे प्रतिनिधित्व तो अधिकारवाणीने करू शकतो .</p>

<p> संयुक्त राष्ट्रांची शैक्षणिक , वैज्ञानिक व सांस्कृतिक संगटना  ( युनेस्को )   :   """""""""""""""" सर्वासाठी
 न्याय , विधीनियमांचे अधिराज्य , मानवी हक्क व मूलभूत स्वातंत्र्य त्यांच्याबद्दल चिरंतन आदरभाव
 वाढावा यासाठी शिक्षण ,  विज्ञान आणि संस्कृती यांच्या माध्यमांनी राष्ट्राराष्ट्रांत सहकार्याला प्रोत्साहन
 देऊन शांतता व सुक्षितता यांना हातभार लावणे ,  """"""""""""""""  या उद्देशपूर्तीसाठी 4 नोव्हेंबर ,  1946 मध्ये युनेस्को
 अस्तित्वात आली .</p>

<p> ही संघटना आर्थिक आणि सामाजिक परिषदेच्या माध्यमातून संयुक्त राष्ट्रांशी जोडली गेली आहे .
 तिचे मुक्य कार्यालय पॅरिस येथे आहे .  कार्यकारी मंडळाची निवड आमसभा दोन वर्षासाठी करते .  या
 कार्यकारी मंडळाच्या वर्षातून कमीत कमी दोन बैठका होतात .  युनेस्कोचे धोरण अमलात आणण्याची
 जबाबदारी कार्यकारी मंडळाची असते .  युनेस्कोचे स्वतंत्र सचिवालय असून त्याचा एक प्रमुख
 संचालक आहे .</p>

<p> युनेस्को राष्ट्राराष्ट्रांतील परिचय वाढवून जागतिक शांततेला पोषक वातावरण तयार करते .  तसेच
 ज्ञानाचा प्रसार करणे ,  मानवी स्वातंत्र्याबद््दल आदर वाढविणे ,  आंतरराष्ट्रीय न्यायावरील श्रद््धा वाढविणे
 ही या मंडळाची उद््दिष्टे आहेत .  त्याबरोबरच ही संघटना शास्त्रज्ञ ,  शिक्षणतज्ञ ,  कलावंत यांची
 राष्ट्रराष्ट्रांत देवाण घेवाण घडवून आणते व गरजू राष्ट्रांना तांत्रिक ज्ञानाची मदत करते .</p>

<p> भारत आणि संयुक्त राष्ट्रे :</p>

<p> भारत हे संयुक्त राष्ट्रांच्या संस्थापक राष्ट्रांपैकी एक आहे .  संयुक्त राष्ट्रांची सनद तयार करण्यासाठी</p>

<p> पान नं .  30</p>

<p> सॅनफ्रान्सिस्को येथे बोलविण्यात आलेल्या बैठकीत भारत सहभागी होता  ( 1945 )  .  स्वातंत्र्योत्तर
 काळात आंतरराष्ट्रीय शांतता टिकविण्यासाठी भारताने नेहमीच पुढाकार घेतलेला आहे .  संयुक्त राष्ट्रे ही
 खऱ्याखुऱ्या अर्थाने जागतिक संघटना व्हावी म्हणून भारताने प्रयत्न केले .  आफ्रिका आणि आशिया
 खंडांतील पारतंत्र्यात असणाऱ्या राष्ट्रांना स्वातंत्र्य मिळवून देऊन ,  त्यांना संयुक्त राष्ट्रांचे सदस्यत्व
 मिळवून देण्यासाठी भारत नेहमीच प्रयत्नशील राहिला .  जगातील सर्व स्वंतत्र राष्ट्रे सदस्य बनल्याशिवाय
 संयुक्त राष्ट्रांच्या निर्णयांना बळकटी येणार नाही असे भारताचे सुरूवातीपासून मत होते .  म्हणूनच
 साम्यवादी चीनशी सरहद्दीबबात तंटा असूनही चीन हा संयुक्त राष्ट्रांचा सदस्य बनला पाहिजे हीच
 भूमिका भारताने घेतली .  भारताने नेहमीच वसाहतवादी राष्ट्रांविरूद्ध भूमिका घेऊन वसाहतींच्या
 स्वातंत्र्याचा पुरस्कार केला आहे .  दक्षिण आफ्रिकेच्या वर्णद्वेषाच्या धोरणाचा सातत्याने विरोध करीत
 आहे .  अविकसित राष्ट्रांना आर्थिक मदत मिळवून देण्यास पुढाकार घेत आहे .  संयुक्त राष्ट्रांच्या
 ~आर्थिक आणि सामाजिक परिषदेचा सदस्य म्हणून भारताने पंधरा वर्षे काम केले .  संयुक्त राष्ट्रांच्या
 ~स्थापनेनंतर जगाची विभागणी सरळ सरळ दोन गटांत झाली .  एक अमेरिकन गट आणि दुसरारशियन
 ~गट
 दोन्ही गटांत शीतयुद्द्ध सुरू होते .  या शीतयुद््धाची परिणती प्रत्यक्ष युद््धात होण्याची चिन्हे
 दिसत होती .  या कालखंडात जागतिक शांतता राखण्याच्या कामी भारताने बजाविलेली भूमिका अत्यंत
 महत्त्वाची आहे .  दोन्ही गटांपासून वेगळा असा तिसरा छोट्या आणि अविकसित राष्ट्रांचा तटस्थ गट
 भारताने निर्माण केला आणि शांतिदूताची भूमिका पार पाडली आहे .  पंचशीलांचा पुरस्कार केला .  तो
 तत्त्तवे अशी .</p>

<p> 1 )  परस्परांच्या प्रादेशिक सीमांविषयी व सार्वभौमत्वाविषयी आदर बाळगणे .
 2 )  अनाक्रमण .
 3 )  परस्परांच्या अंतर्गत कारभारात ढवळाढवळ न करणे .
 4 )  समानता .
 5 )  शांततापूर्ण सहजीवन</p>

<p> संयुक्त राष्ट्रांची सभासद संख्या जसजशी वाढत गेली तसतसा हा तिसरा गटही गेला .  या
 गटाच्या प्रभावामुळे तिसऱ्या महायुद््धाची शक्यता कमी झाली आहे .  संयुक्त राष्ट्रांच्या प्रत्येक शांतता
 स्थापनेच्या प्रयत्नात भारत सहभागी असतो .  उदाहरणार्थ 1953 सालच्या कोरियातील युद््धात संयुक्त
 राष्ट्रांतर्फे भारताने युद््धकैद्यांच्या अदलाबदलीची जबाबदारी स्वाकीरली होती .  1954 साली
 व्हिएतनाममध्ये युद््ध थांबविण्याच्या बाबतीत भारताने महत्त्वाची कामगिरी बजावली .  1956 साली
 इजिप्त , 1960 साली कांगो ,  1965 साली सायप्रस या ठिकाणी संयुक्त राष्ट्रांनी निर्माण केलेल्या
 शांतिसेनेत भारत सहभागी झाला होता .  संयुक्त राष्ट्रांच्या शस्त्रकपातीच्या प्रयत्नास भारताने नेहमीच
 पाठिंबा दिला आहे .  अणुशक्तीचा वापर युद््ध व संहारासाठी न होता मानवी प्रगतीसाठी व्हावा ही भूमिका
 सुरूवातीपासून भारत ठामपणे मांडित आहे .  संयुक्त राष्ट्रांच्या कार्यात अनेक भारतीयांनी महत्त्वाची
 भूमिका पार पाडली आहे .  श्रीमती विजयालक्ष्मी पंडित या संयुक्त राष्ट्रांच्या आमसभेच्या अध्यक्षा म्हणून
 निवडून आल्या होत्या .  आमसभेच्या त्या पहिल्या स्त्री अध्यक्षा होत्या .  नगेंद्रसिंग हे आंतरराष्ट्रीय
 न्यायालयाचे न्यायाधीश होते .  याशिवाय सर्वश्री लेफ्टनंट जनरल थोरात ,  राजेश्वर दयाल ,  बी .  आर सेन ,</p>

<p> पान नं .  31</p>

<p> जनरल थिमय्या , ब्रिगेडियर रिखये ,  होमी भाभा ,  नरसिंहन ही नावे त्यांच्या संयुक्त कामगिरीमुळे
 चिरकाल स्मरणात राहणारी आहेत .</p>

<p> आंतरराष्ट्रीय सामंजस्य -  विकास</p>

<p>
 राष्ट्रांनी आपसातील मतभेद सामोपचाराच्या व तडजोडीच्या मार्गानी मिटवणे आणि परस्परांच्या गरजा
 ओळखून त्या पूर्ण करण्याची भावना व्यक्त करणे व त्यानुसार प्रयत्न करणे म्हणजे आंततराष्ट्रीय
 सामंजस्य होय .  संयुक्त राष्ट्रांच्या सनदेतून हीच भावना व्यक्त करण्यात आली आहे .  पुढील तीन
 मार्गानी आंतरराष्ट्रीय सांमजस्याची वाढ होत असते .</p>

<p> 1 )  परस्परावलंबन  :  नागरिक या नात्याने फक्त स्थानिक व राष्ट्रीय शासनाच्या कृतीचाच
 आपल्यावर प्रभाव पडतो असे नाही तर संबंध जगात जे घडत आहे त्याचाही आपल्यावर परिणाम होतो .
 झपाट्याने प्रगती करीत असलेल्या विज्ञानामुळे व तंत्रविद्येमुळे आणि वाहतूक दळणवळणाच्या साधनात
 क्रांती झाल्यामुळे जग पूर्वीपेक्षा आता फार लहान असल्यासारखे वाटते .  एका देशात तयार झालेल्या वस्तू
 जगातल्या सर्व देशांत उपलब्ध होऊ शकतात .  एखाद्या देशात दुष्काळ अगर महापूर यांना तोंड द्यावे
 लागले तर इतर सर्व देशांतील लोक दुष्काळग्रस्त किंवा पूरग्रस्त लोकांच्या मदतीला धावतात .  साथीचे
 रोग पसरत असल्यामुळे संबंध जगातच प्रतिबंधक उपाय योजावे लागतात .  प्रगत देशातील तंत्रज्ञ
 मागासलेल्या देशांच्या विकासाच्या प्रक्रियेत सहभागी होतात .  प्रदूषण किंवा अणुशक्तीचे किरणोत्सर्जन
 यांसारखे परिणाम या समस्या सोडविण्यासाठी राष्ट्रांना एकत्र यावे लागते .  अशा तऱ्हेने कोणास आवडो
 वा
 न आवडो ,  पूर्वीपेक्षा लोकांना एकमेकांस बरोबर घेऊन काम करावे लागते .  निरनिराळ्यादेशांतील लोक
 अशा रीतीने एकमेकांवर अवलंबून राहतात .</p>

<p> 2 )  राष्ट्राराष्ट्रांतील संबंध  :   संयुक्त राष्ट्रे व त्यांच्या साहाय्यक संस्था यांच्यामार्फत व
 उभयपक्षी करार करून अथवा युरोपीय सामायिक बाजारपेठ ,  राष्ट्रकुल यांसारख्या प्रादेशिक संघटना
 स्थापन करून सामंजस्य व सहकार्य वाढविण्याचा राष्ट्रे प्रयत्न करीत आहेत .  विकसित देशांचा एक
 मोठा गट व अविकसित किंवा विकसनशील देशांचा एक मोठा गट अशी जगाची दोन गटांत विभागणी
 झाली आहे .  विकसनशील देशांपैकी बरेचसे देश गेल्या तीस वर्षात स्वतंत्र झाले आहेत .  भारतासारख्या
 विकसनशील देशांना विकसित देशांत उपलब्ध असलेले भांडवल ,  ज्ञान व कौशल्य यांत वाटेकरी
 व्हावेसे वाटते .  प्रगत देशांकडून तांत्रिक व आर्थिक साहाय्य घेण्यास विकसनशील देश तयार आहेत .
 विज्ञान ,  शेती ,  व्यापार ,  आरोग्य व सांस्कृतिक क्षेत्रांमध्ये अशा प्रकारचे राष्ट्राराष्ट्रांतील संबंध वाढीला
 लागले आहेत .  जागतिक आरोग्य संघटना ,  अन्न आणि कृषि संघटना  ( एफ .  ए .  ओ )  ,  जागतिक मजूर
 संघटना  ( आय .  एल .  ओ .   )  ,  रेड क्रॉस सोसायटी ,  जागतिक बॅक ,  आंतरराष्ट्रीय नाणे निधी इत्यादी
 संस्थाद्वारे राष्ट्राराष्ट्रांतील संबंध विकसित होण्यास मदत होत आहे .</p>

<p>          ( 3 )  विश्वनागरिकत्व  :  विचारांत व विज्ञानात झालेली क्रांती राष्ट्राच्या सीमा ओळखीत नाही .
 आर्थिक व वाणिज्यदृष्ट्या सबंध जग हे एक घटक बनले आहे .  वाढते विशेषीकरण व औद्योगिकरण
 आणि वाढता व्यापार यांमुळे राष्ट्रे एकमेकांवर अधिकाअधिक अवलंबून राहत आहेत .  सबंध जागतिक
 समाजाचे ज्ञान असणे हे आता आवश्यक आहे .  आंतराराष्ट्रीय शांतता व सहकार्य यांवर आधारलेल्या</p>

<p> पान नं 32</p>

<p> जागतिक समाजाची प्रस्थापना हे मानव समाजाचे उद््दिष्ट झाले आहे .</p>

<p> आपण विशिष्ट राज्याचे नागरिक असलो तरी त्याबरोबर जगाचेही नागरिक आहोत याची जाणीव
 ठेवणे आवश्यक आहे .</p>

<p>*                                                                                                                                 *
  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
 THE END</p>

<p></p>

<p></p>

<p>
"""""""",0"""",0"",0",0
</p></body></text></cesDoc>