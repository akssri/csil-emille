<cesDoc id="asm-w-literature-essay-aes03" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-literature-essay-aes03.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>প্রকাশ..</h.title>
<h.author>মহেশ্঵র.</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1983</pubDate>
</imprint>
<idno type="CIIL code">aes03</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0409.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;প্রকাশ ( ড: মহেশ্঵র নেওগ )
অরুনোদইর
অরিহণা
অতুলচন্দ্র হাজরিকা ।
সংবাদ-পত্র সমন্঵িতে বহুত কাকত অসমর
সাহিত্য়-জগতলৈ আহিল আরু গ'ল । সেইবোরর
নামটো পর্য়ন্ত পাহরণির বুকুত মচ খাই গৈছে ।
তার মাজতেই 1946 চনত ওপজা ` অরুণোদই '
নামর কাকতখন মৃত্য়ুর পিছতো থাকি গ'ল আরু
আগলৈকো থাকিব , সাহিত্য়র ন য়ুগর পথ-প্রদর্শিকা
হৈ এটা সরু ভোটা তরার নিচিনাকৈ । আহোম
রাজত্঵র বেলিমারর আগে আগে সাত সাগর তের
নদী পার হৈ আমার দেশলৈ আহিছিল আমেরিকার
খ্রীষ্টীয য়াজকসকল । সেইসকলর এহাতে আছিল
ধর্মর জোঁর , আনহাতে আছিল জ্ঞানর আরিযা ।
তার লগতে তেওঁলোকে কঢ়িযাই আনিছিল নতুন
য়ুগর বতরা , নতুন দিনর কাহিলি কাহিলি পোহর ।
সেই সমযত আমার দেশত আছিল গার লোম
দেখা-নেদেখা কাল-সন্ধিযা । সেই পোহর আরু
আ঩্ধারর সন্ধিক্ষণতে সৃষ্ঠি হৈছিল বর্তমান অসমীযা
সাহিত্য়র অরুনোদই-য়ুগ , য়েন ` তিমির ফেরিযা
বাজ রবির কিরণ । '</p>

<p>রাচলতে অরুনোদই -য়ুগ নুবুলি , অরুনোদই
স্তর বুলিলেহে সঁচা কথা কো঵া হয । কিন্তু এই~
টোও সঁচা য়ে , একোখন নাম-য়শ থকা কাকতর
নামেরে সাহিত্য়র একোটা য়ুগর নামকরণ করা
হয , য়েনে জোনাকী-য়ুগ , বাঁহী-য়ুগ , আ঵াহন-য়ুগ ,
রামধেনু-য়ুগ । এই অর্থত অরুনোদই -য়ুগ বুলি
ক'লে নগরত জগর লাগিব নোপায । প্রকৃতার্থত
অরুনোদযে অসমীযা সাহিত্য়র বর্তমান য়ুগর
পাতনিহে মেলিছিল , ল'রা-ওমলো঵া ধাইনাম গাই ।
এযা আছিল এটা সম্ভা঵নাপূর্ণ নতুন য়ুগর প্রাথমিক
পর্য়ায । আরু এটা মন করিবলগীযা কথা , অরু~
নোদযে য়দিও নিজকে ` সম্বাদ-পত্র ' বুলি চিনাকি
দিছিল তথাপি এইখন কাকতচ একেলগে সমবাদ-পত্র
আছিল . সাহিত্য়-আলোচনীও আছিল । আরু এটা
স্মরণীয কথা এযে য়ে এই অরুনোদইর পোহরতে
বিকশিত হৈছিল অসমীযা সাহিত্য়র বর্তমান য়ুগর
প্রতিষ্ঠাতা ত্রিমূর্তি - স্঵নামধন্য় আনন্দরাম ঢেকিযাল
ফুকন , আভিধানিক হেমচন্দ্র বরু঵া আরু বুরঞ্জী~
বিদ গুণাভিরাম বরু঵া ।</p>

<p>সম্প্রতি অসম প্রকাশন পরিষদর উদ্য়মত
বৃহত্ অরুনোদই সমকলন পোহরলৈ আগ বাঢ়ি
অহার আগন্তক হো঵াত দূরৈর গঙ্গা ওচর চাপিল
বুলি ক'ব পারি । এই মূল্য়঵ান প্রকাশে উ঵লি
য়ো঵া দিনবোরর বহু সম্ভেদ দিব পারিব বুলি
আমি আশা করিছোঁ । এই পর্য়ন্ত ড: বিরিঞ্চি~
কুমার বরু঵া সংকলিত আরু ড: মহেশ্঵র নেওগ
সম্পাদিত , অসম সাহিত্য় সভার ` অরুনোদইর
ধলফাট ' প্রন্থখনেই আছিল আমার বাবে কণার
লাখুটি । তারে আলম লৈ মাছর তেলেরে মাছ
ভজার নিচিনাকৈ এই আলচ য়ুগুত করিছোঁ ।
আন আন গ঵েষকপণ্ডিতরো আ঵শ্য়কমতে সহায
লো঵া হৈছে । এঘরর পাটনাদ , এঘরর জরি ,
এঘরে পানী তোলে ঘটং ঘটং করি । কাণি-মূনি
বাই, মোর গাত দায নাই ।</p>

<p>1926 চনর যাণ্ডুবু সন্ধির সুবিধা লৈ দূরণি~
বটীযা ইংরাজসকলে ফাওতে আমার দেশর ভোগ~
দখল করিবলৈ অহার আগতে , অরুনোদইর পোহর
পরার আগছো঵া কালত এই দেশত চলিছিল এখন
বিযোগান্ত নাটক , আঁউসীর ঘোপমরা অন্ধকার ।
আত্মকলহ , আত্মবিপ্ল঵ আরু মান-মরাণর দেও~
পারণিত দেশখন লাং লাং ঠাং ঠাং হৈ পরিছিল ।
সেই সমযত চলিছিল রাজনৈতিক অস্থিরতা , মানু~
হরমনত অশান্তির বিভীষিকা । সাহিত্য়-সংস্কৃতি
আরু রাজ্য়র প্রগতি প্রায রসাতললৈ গৈছিল ।
এনে অশান্ত পরিস্থিতির মাজতে ইংরাজে ভগা
দেশ পাতিবলৈ আহি হাতত রাজদণ্ড তুলি লৈছিল ।
তেওঁলোকর লগত লেমূটৌ হৈ আহিছিল বঙালী
আমোলা-মহরী , ডাক্তর-শিক্ষক প্রভৃতি , শাসন~
কার্ষত ইংরাজক সহায করিবলৈ আরু বঙলা
সম্পসারণবাদর পোহর মেলিবলৈ । তেওঁলোক
অহার আগতে দূর্গম পথ , অটব্য় হাবি , বহু সংখ্য়ক
নদ-নদী আরু আলেখ বাধার হেঙার অতিক্রম
করি জনচেরেক আমেরিকার খ্রীষ্টীয য়াজকে
আমার দেশর পূব প্রান্তর শদিযা-জযপুরত থিতাপি
লৈ খ্রীষ্ট-বাণী প্রচার করার উপরিও , এশ এবুরি
বাধা0বিঘিনি নেওচি পঢ়াশালি পাতিছিল , চিকিত্সা~
লয পাতিছিল , ছপাশাল পাতিছিল । এই সকল
য়াজক য়ে কে঵ল ধর্মপ্রেমীযে আছিল তেনে
নহয , তেওঁলোক আছিল মান঵প্রেমী , অসমবন্ধু
আরু অসমীযা-হিতৈষী । তেওঁলোকে ক্রমাত্
ভটিযাই আহি পুরণি রংপুর বা বর্তমান শি঵সাগর
নগরত খোপনি লৈছিল । সেইসকলর অসম~
আগমনর কাহিনী অমৃত-সমান ।</p>

<p>য়াজকসকলে শি঵সাগরত নিজাকৈ ` শি঵সাগর
মিছন প্রেছ ' নাম দি এটা ছপাশাল পাতি লৈছিল
আরু এই প্রেছর পরাই তরহে তরহে পুথি উলিযাই~
ছিল আরু অরুনোদই কাকতখনো প্রকাশ করিছিল
1846 চনর পরা 1882-83 চনলৈ । অরুনোদইর
বিলুপ্তি ঘটার পিছতেহে 1883 চনত ছপায়ন্ত্রটো
বেচি দিযা হৈছিল . গরুও মরিল , পোকো
সরিল ।</p>

<p>অরুনোদই আছিল য়াজকসকলর ধর্ম-প্রচারর
মুখপত্র । ওপরতে কো঵া হৈছে য়ে ধর্ম-প্রচারর
লগতে মানুহক মানুহ করি তোলাটোও আছিল
মান঵-প্রেমী য়াজকসকলর অন্য়তম মুখ্য় উদ্দেশ্য় ।
তাতে তেওঁলোকে পূর্বাঞ্চলত পোহরর সন্ধান দিবলৈ
য়ত্ন করিছিল । য়'তে আন্ধার তাতে তেওঁলোকে
পোহর বিলাইছিল ।</p>

<p>সেই সমযত আছিল অসমীযা ভাষা-সাহিত্য়র
কলঢপ অ঵স্থা । লেম্টো হৈ অহা বঙালী~
বিলাকে শাসক ইংরাজবিলাকক লগনীযা কথা
কৈ কৈ বুজাই দিছিল য়ে জংঘসী অসম দেশর
মানুহবিলাক জংঘলী , মাত-কথাও জংঘলী । এই
শিযালবিলাকর কথাতে প্রত্য়য মানি ইংরাজ শাসক~ //
গোষ্ঠীযে সাতামপুরষীযা অসমীযা ভাষাক বনবাস
দি চ'পনীযাবিলাকক রাজপীরত বহু঵াই রাজভোগ
খাবলৈ দিছিল । বঙলা রাজভাষা হ'ল । স্কুল~
কাছারীত বঙলা ভাষা-সাহিত্য়ই নেগুর পারি
বহিল । আমেরিকার য়াজকসকলে এনে অন্য়ায
ব্য়঵স্থার পোষকতা নকরিলে । ঢেকিযাল ফুকন
প্রভৃতি অন্য়ান্য় অসমর দেশহিতৈষী লোকর লগতে
তেওঁলোকে অসমীযা ভাষা-সাহিত্য়ক ন্য়ায়্য় স্থানত
প্রতিষ্ঠা করিবলৈ নানা প্রকারে চেষ্টা চলাইছিল ।
বহু য়ুঁজ-বাগরর অন্তত তার ফলতেই এদিন বঙলা
ভাষার এই দেশত সান্দহ খো঵া বালি তল গৈছিল
আরু বঙলার ঠাইত অসমীযা রাজভাষা হৈছিল ,
পঢ়াশালির ভাষা হৈছিল । এই কথা কৃতজ্ঞতারে
স্঵ীকার করিব লাগিব য়ে আমেরিকার য়াজকসকলে
এদিন মৃতপ্রায অসমীযা ভাষাক তুলসী-তলর
পরা ঘরর ভিতরলৈ আনি মহা নীলকণ্ঠ খু঵াই়
প্রাণর সঞ্চার করিছিল । তেওঁলোকর লগত জন~
চেরেক দেশী লোকো আছিল । এই মহা নীলকণ্ঠ
ঔষধপালিযেই আছিল ` অরুনোদই সম্বাদ-পত্র ' ।
দুখর বিষয , আজিও অসমর অসমীযাত্঵ বিপূপ্তির
কারণে বাহিরে-ভিতরে চেষ্টা চলিবই লাগিছে ।
ই য়েন এক শেষ নোহো঵া সাধুহে । চোবো঵া
কথাকে চোবাই অপ্রিয প্রসঙ্গ বহুল নকরোঁ ।</p>

<p>নিজে কো঵া কথা঵েই এই খিনিতে অরুনোদইর
চমু চিনাকি দিছোঁ ।</p>

<p>অরুনোদই সম্বাদপত্র
প্রথম চো঵া । মোকাম সিবসাগর , জানোআরি
1846 খ্রিষ্ট অ: সঁক । 1 নম্বর ।
অরুনোদইর প্রথম প্রকাশক আছিল কটার চাহাব ।
তেওঁর পিছর সম্পাদকসকল আছিল য়থাক্রমে
ডক্তর নেথান ব্রাউন
এপলটন হাও ডেনফোর্ড
চেমুএল এম ডেনফোর্ড
ছেমুএল এম হো঵াইটিং
ডক্তর মাইলচ ব্রনছন
শ্রীমতী এছ-আর ওআর্ড
ই ডব্লিউ ক্লার্ক
এ-কে গার্নী প্রভৃতি ।</p>

<p>কাকতখন লাভভিত্তিক নাছিল , আছিল জ্ঞান~
ভিত্তিক । যাজকসকলে অর্থোপার্জন করিবলৈ এই
কাকত প্রকাশ করা নাছিল । তেওংলোকে গাঁথির
ধন ভাঙি লোকচান করিও 1846 চনর পরা
1883 চনলৈকে এরা-ধরাকৈ কাকতখন চলাই
আছিল । অরুনোদইর আর্থিক দিশটোর সম্ভেদ
পত্রর কারণে মাহে 50 টকা খপচ হই , অর্থাত্
বচরে 600 টকা য়াই ; কিন্তু আমি লোকর পরা
প্রাই 400 টকাহে পাও । """"</p>

<p>অরুনোদইর পরিচালকসকলে এই আর্থিক
ত্য়াগো স্঵ীকার করি লৈছিল - নিজর বাবে নহয ।
এখন দূরণিবটীযা বেলেগ দেশর , বেলেগ তেজর
মানুহর বাবে । প্রকৃত ধর্ম-মণ্ডলত আপোন-পরর
প্রশ্ন নুঠে , দেশ বিদেশ঱ প্রশ্ন নুঠে । আমার ধর্ম~
গুরু শ্রীমন্ত শঙ্করেও ইযাকে করা নাছিল জানো ?</p>

<p>বান্দরে নারিকলর মোল নুবুজার দরে সেই
সমযর সর্বসাধারম অসমীযা মানুহে জ্ঞানর মোল
বুজা নাছিল । কানির রাগীত তেওঁলোকে হিচাহিত
জ্ঞান হেরু঵াইছিল । স্঵দেশহিতৈষী ঢেকিযাল
ফুকণর ` অসমীযা লরার মিত্র'র হিত-বাণীও তেওঁ~
লোকর কাণত য়োমো঵া নাছিল । এই সম্পর্কত
অরুনোদইত প্রকাশিত এই চিঠিখনত সেই সমযত
আমার দেশর অনগ্রসরতার এখন ফটফটীযা ছবি
দেখা পো঵া য়ায ।</p>

<p>অচম দেসর লোক সকলর প্রতি নিবেদন পত্র
Letter from n Assamese in Calcutta to his
Countrymen</p>

<p>প্রিয বন্ধু সকল ,
সম্প্রতি আমার দেসর লোক সকলে বিদ্য়াক
জেনে অনাদর করে , ইযাক দেখি ভবিষ্য়ত কালত
তেওঁ বিলাকর জি হব , তাক চিন্তিব লাগিলে সরিলো
কম্পন হই । আমার মহা উপকারি ইংরাজি
দেসাধিকার সকলে ঠাযে ঠাযে পহা সালি পাতিলেও ,
তালৈ অতি তাকর লোকহে আপোনার লরা
চোআলিক পঠিযাই । জি কোনো এ লরাক পহিবলৈ
দিএ , সিও আখর চিনি সুন্দরকৈ লিখিব পরা
হলেই মাক বাপেকর সলনি ধন আর্জিবলৈ উপদেস
পাই , সুদিপ্তি মই বিদ্য়ার পর্ব্বতত অলপ উঠি এই
আকও নামি আহে । লরা চোআলি দুইকো সমানে
বিদ্য়া সিকোআ আরু প্রতি পালন করা , বাপেক
মাকর প্রধান করম ; কিন্তু জি দেসত কে঵ল লরা~
কেই সম্পূর্ণরূপে বিদ্য়া সিকিবলৈ নি দিএ সেই
দেসত জে চোআলিবোরে বিদ্য়া সিকি আরু কালত
মাক বাপেকক আরু গাভরু হলে আপোনার স্঵ামীএ
সৈতে সদায সজ কথার আলোচনা করিব ; আরু
সন্তান সন্ততি হলেও সিহঁতক সিকাব বুজাব এনে
সম্ভা঵েই নাই । লরা কি চোআলি দুযোরো বিদ্য়া
সিকা উচিত , আরু সাস্ত্রতো ইযার বিধান আচে ।
কিন্তু ভারতবর্ষর লোক সকলে এই মিচা কুজসর
পরা চোআলিবিলাকক পর্হিবলৈ নিদিএ । তথাপি
এতিযা সেই কুজস হোআর অনর্থক ভাবনা
জত জত সুচিচে , সেই সেই ঠাইর লরা
চোআলি দুযো সমানে বিদ্য়া সিকিচে । এতেকে
আমিও ভারসা করোঁ আমার দেসর পরা ও জেন
সেই কুসংস্কার দুর করি সকলোএ বিদ্য়া সিকিবলৈ
জতন করে ।</p>

<p>এই কথা মনত রাখিব লাগিব সেই দিনত
দেশত শিক্ষিত মানুহর সংখ্য়া নিচেই কম আছিল ।
কাকত-কিতাপর মোল বুজা মানুহ আছিল তাতোকৈ
কম । তথাপি এইটোও বুজিব নেলাগিব য়ে
অরুনোদই জনপ্রিয হো঵া নাছিল , বরং ওলোটা~
টোহে হৈছিল । অসমর গাঁ঵ে-ভুঞে সর্বত্র অরুনো~
দয মানুহর মন জয করিব পারিছিল । তেতিযার
মানুহে , আমার আজোককা-আজোআইতাহঁতে য়ি
কোনো বাতরি কাকতকে - আনকি বঙলা সঞ্জী঵নী ,
হিতবাদী আদি কাকতকো - আনকি বঙলা সঞ্জী঵নী ,
হিতবাদী আদি কাকতকো ` অরুন্দয ' আখ্য়া দিছিল ।
তেওঁলোকর মানত আছিল ` অরুন্দয ' মানে বাতরি
কাকত । এযা কাকতখনর সুখ্য়াতি আরু জন~
প্রিযতার নিদর্শন নহয জানো ? এই জনপ্রিযতার
এটা ভাষা আছিল নকৈ ওপজা শিশুর থুনুক-থানাক
আধাফুটা মাতর দরে-মিঠা মিঠা শুনি ভাল
লগা , সহজে বুজি পো঵া ধরণর । অরুনোদই
মরার বহু দিন পিছলৈকে মানুহর মুখত এইখন</p>

<p>কাকাতর বর নাম আছিল , এই শতিকার আরম্ভ~
ণির পরাহে সেই নাম তল পরিল ।</p>

<p>অরুনোদইর আগত পূর্ববর্তী কোনো আহি
নাছিল । খোজ পেলাবলৈ সুগম পথ নাছিল ।
কাকতখনে নিজেই নিজর বাটকটীযা হৈ আগ বাঢ়ি
য়াবলগীযা হৈছিল । প্রথমতে উদ্ভ঵ হৈছিল ভাষা~
সমস্য়া । দেশত তেতিযা অসমীযা ভাষাই মোট
সলাইছিল । এইখিনিতে উল্লেখ করিব পারি য়ে
বৃটিছ আমোলর পূর্বে অসমীযা ভাষা-সাহিত্য়র
দুটা কেন্দ্র আছিল । এটা উজনির রংপূরত আরু
আনটো ভাটীর কেচবেহারত । মহাপুরুষে প্র঵র্তন
করা ব্রজবলী মিহলি দেশী ভাষাক আরু বুরঞ্জীর
ভাষাকে তেতিযার অসমীযা ভাষা আছিল বুলি
ক'ব পারি । কীপ্তন-দশম-নামঘোষার ভাষা
শদিযার পরা বেহারলৈ বরঅসমর সকলো মানুহে
বুজি পাইছিল , আজিও পায । সেই ভাষাক অসমর
রাষ্ট্রভাষা আখ্য়া দিব পারি । সেই য়ুগ-সন্ধির
কালত পুরণি পূথি-পাজিবোর ধোঁ঵াচাঙত উঠি~
ছিল । বৈষ্ণ঵ী অসমীযা ভাষাও জনসমাজত অচল
হৈ পরিছিল । তদুপরি সমগ্র অসমরে কথ্য়ভাষা
বা উপভাষা একেটা নহয । গুরিগছ একে হ'লেও
আরু মুলতে এটা ভাষা হ'লেও এই কথ্য় ভাষা~
বোরর মাজত মিল য়িমান , অমিলো প্রায সিমান ।
ঘরে প্রতি ঘৈনী , খালে প্রতি য়খিনী থকার দরে
এনে কথ্য়ভাষা অলেখ । তথাপি এটা উমৈহতীযা
কেন্দ্রীয ভাষা নহ'লে নচলে , য়িটো ভাষাক অসমীযা
ভাষা বুলি ক'ব পারি । সেই কারণে মার্কিন
য়াজকসকলে তেওঁলোকে থিতাপি লো঵া শি঵~
সাগরর রংপুরীযা ভাষাকে মুল অসমীযা ভাষা
হিচাপে গ্রহণ করিছিল আরু সেযেই আছিল
অরুনোদইরো ভাষা । সাহিত্য়রথী বেজবরু঵া
প্রমুখ্য় জোনাকী য়ুগর লেখকসকলেও সেই ভাষা~
রেই পোষকতা আছিল । আজিকালি অ঵শ্য়ে সেই
জকাইচুকীযা ভাষার সম্প্রসারণ ঘটো঵া হৈছে
আরু ইযার পরিসরো বহু পরিমানে আহল-বহল
হৈছে । এটা জীযা ভাষার পরিবর্তন আরু পরি~
বর্দ্ধন অ঵শ্য়ম্ভাবী । ই ঠিহিরা মারি নেথাকে ।</p>

<p>ভাষা সমস্য়া সমাধান হো঵ার পিছত আরু
এটা জটিল সমস্য়া অরুনোদযে নিজেই সমাধান
করি লৈছিল । এই আউলীযা সমস্য়াটো আছিল
ব্য়াকরণ আরু আথর - জোঁটনির সমস্য়া । রবিন্সনর
ব্য়াকরণ আরু ব্র়ঞ্চনর আভিধান আছিল
অরুনোদইর পথ-প্রদর্শক । পোন প্রথমে অসমীযা
অভিধান সংকলক জাদুবান ডেকাবরু঵ার আরু
ব্রন্সনর অভিধানর আখর-জোঁটনির মাজত বিশেষ
অমিল নাছিল ।</p>

<p>মই এই সম্পর্কে বহলকৈ মুখ নেমেলোঁ ।
অরুনোদইত প্রকাশিত এটা টোকাত এই সরল
সহজ আখর-জোঁটনির বিষযে খরচি মারি কো঵া
হৈছে :
শ
Style and Mode of Spelling
9 বছর । নম্বর 4 । এপ্রিল , 1954 ।
আমি কে঵ল পণ্ডিতলৈ লিখা নহই , সকলো
মানুহর নিমিত্তে লিখিচো । সংস্কিত আরু ভাষা
মিহলি করিলে আমি ভাল নে দেখো ; সংস্ক্রিত
হলে সুধ সংস্ক্রিতেই হব লাগে ; ভাষা হলে সুদা
ভাসাই হব লাগে । সংস্ক্রিতত প্রাই সঞ্জোগি
আখর , ভাসাত গোট গোট আখর । আগর কালর
মানুহর মুখত সংস্ক্রিতর সকলো মাত লিখা দরে
ফুটিছিল ; এতিযা প্রাই পণ্ডিতর মুখতো নুফুটে ।</p>

<p>ভাষা লিখোতে আখরর জিমান জাতি , তিমানহে
লিখিব লাগে ; একে জাতিরে দুই তিনি প্রভেদর
সকাম নাই । জ , য জর , এই দুইরো একেটা
উচ্চারণ ; এই হেতুকে অন্তস্থ যর বাহিরে আন য়
নি লিখো । শ ষ স এই তিনিটারে একে মাত ;
শ্রীস্রীর একো ভেদ নে দেখোঁ , আমি কেবল সহে
লিখো । ন ণ আরু হ্রস্঵ দির্ঘতো এইদরে বুজিবা ।
ভাষাত রণ নো বোলে ; রনহে বোলে ; পানী হলে
পানী বোলে ; পাপীর ঠাইত পাপী ; কুলর ঠাইত
কুল ; সূর্য়্য়র ঠাইত সুর্জ বা ,সুরুজ । আরু রেফর
পাচত এটা আখর দুটাকৈ নু ফুটে ; পর্ব্বত নো
বোলে পর্বতহে বোলে ; সর্ব্ব , ধর্ম্ম , কর্ম্ম , মর্ম্ম ,
মুখত নোলাই ; সর্ব , ধর্ম , কর্ম , মর্ম নাইবা সরব ,
ধরম , করম , মরম , এই রূপে ওলাই । স্লোক লিখিলে
সংস্কৃতর ভাবেরে অবশ্য়ে সিখিব লাগে । ; ভাসাত
হলে আমার দাই ন ধরিবা । আরু আমার
অরুনোদইত জদি কোনো কোনো কথা ভাল নে
দেখা , তেন্তে তাক এরি ভালখিনি গ্রহণ করিবা ;
জেনেকৈ স্লোকত লিখা আছে ; প্রাজ্ঞস্ত জন্নতাং
পুংষাং শ্রুত্঵া বাচ্য় শুভা শুভা । শুনবদ্ধাচ মাদত্তে
হংসক্ষীবসিবাস্তষা । অর্থাত্ পণ্ডিত গিযানি মানুহে
কথা কোআ লোকর ভাল-বেযা বাক্য় সুনি গুণ
থকা বাক্য় গ্রহণ করে ; জেনেকৈ রাজহাহক
পানিযে গাখিরে মিহলাই দিলে , এঁআ গাখির মিহলাই
খাই ; পানি নে খাই ।</p>

<p>ওপরর কথাখিনি অরুনোদই ভাষা নীতির
এখনি প্রামাণ দলিল বুলি ক'ব পারি । ভাষা
মানে বুজিব লাগিব মাতৃভাষাকে । আজিও আমার
সংস্কৃত টোলর প্রশ্ন কাকতত ভাষালৈ ( মাতৃভাষালৈ )
তর্জমা করিবলৈ দিযা হয । মাতৃভাষাত অধিক
গুরুত্঵ আবেগ করিলেও অরুনোদইর দে঵ভাষা
সংস্কৃতর প্রতি কিন্তু অশ্রদ্ধা নাছিল ।</p>

<p>অরুনোদইর সরলীকৃত বর্ণবিন্য়াস পদ্ধতি
বহু বছর চলিছিল য়দিও বিশেষকৈ হেমচন্দ্র বরু~
঵ার বিরোধিতার বাবে খুঁটি লরিল । এই সম্পর্কে
হেমচন্দ্র আরু অরুনোদইর অন্য়তম মুখ্য় লেখক
নিধিরামর মাজত এখন চুর্চুরীযা কাকতর য়ুদ্ধ
হৈছিল । এই য়ুদ্ধত হেমচন্দ্র জিকিল , অরুনোদইর
নিধিরাম ঘটিল । তার ফলত সরল বর্ণবিন্য়াস
জটিল হ'ল । গুরি গছর লগত সম্পর্ক রাখি ,
হেমচন্দ্রর বর্ণবিন্য়াস মূল সংস্কৃতর শঠা-মিতির
হ'ল । পিছত শরত্ গোসাঁযে অরুনোদইর পথ
ল'বলৈ কুরুংকারাং করিছিল , কিন্তু নো঵ারিলে ।
আজিকালি অ঵শ্য়ে বিশ্঵বিদ্য়ালয দুখন , অসম
প্রকাশন পরিষদ আরু কোনো কোনো লেখকে
অরুনোদই-পথর কাষেদি খোজ পেলো঵া দেখা
গৈছে , আংশিকভা঵ে হ'লেও বর্ণবিন্য়াস পদ্ধতির
সংস্কার সাধন করি । এই কথা আমি দুখেরে
লক্ষ্য় করিছো , বর্তমান আমার ভাষা-সাহিত্য়ত
দৃঢ় নির্দেশ দিওঁতা নাই , নির্দেশ মানোতাও নাই ।
বহু ক্ষেত্রত ব্য়াকরণ-অভিধান অসার্থক আরু
অনা঵শ্য়ক হৈ পরা য়েন অনুভ঵ হৈছে ।</p>

<p>এতিযা অরুনোদইর বিষযে -বস্তুলৈ মন করা
য়াওক । এই বিষয-বস্তুও আছিল বাতরির উপরিও
বহু বিধ রামধেনু-বুলীযা সাহিত্য় সম্ভার । য়েন
পঢ়াশালির পরা বিশ্঵-দর্শন । ড: মহেশ্঵র নেওগে
অশেষ শ্রম করি ষুগুতাই ` অরুনোদইর ধলফাট'র
ভূমিকাত সন্নিবিষ্ট করা অরুনোদইর 192 ( 96-12 )
সংখ্য়ার ` ষোল চো঵া ' বিষয-সুচীলৈ চকু ফুরালেই
য়শোদাই শ্রীকৃষ্ণর মুখর ভিতরত বিশ্঵দর্শন করার
দরে অরুনোদই-পেটত থকা মেটমরা সাহিত্য়~
ভঁরালটোর আভাস পাব পারি । তাত নাছিল কি ?
আছিল দেশী-বিলাতী ছত্রিছ ব্য়ঞ্জন । নানা তরহর
বিষয - কিঞ্চিত্ হ'লেও কাকো বঞ্চিত করা হো঵া
নাছিল । তলত দুই-চারিটা বিভাগর উদাহরণ
দিছো , য়েনে :
 +&gt;*"",0",0
</p></body></text></cesDoc>