<cesDoc id="asm-w-arts-music-amu09" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-arts-music-amu09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>রাগসঙ্গী</h.title>
<h.author>বী.কু.ফু</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - book.</publisher>
<pubDate>1990</pubDate>
</imprint>
<idno type="CIIL code">amu09</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 1237.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;পণ্ডিত ব্য়ঙ্কটমখীযে তেওঁর চতুর্দণ্ডী প্রকাশিকা নামর গ্রন্থত এই
নিযম অনুকরণ করিযেই 72 টা ঠাট বা মেল উদ্ভা঵ন করি দেখু঵াইছে ।
প্রচলিত শুদ্ধ আরু বিকৃত মুঠ বারটা স্঵রর পরা ইযাতকৈ সরহ সংখ্য়ক
স্঵র সমষ্টি বা ঠাট উদ্ভা঵ন করাটো সম্ভ঵ নহয । এই কথা উপলব্ধি
করিযেই ব্য়ঙ্কটমখীযে দৃঢ়ভাবে এই বাসত্তরটা ঠাটর কথা উল্লেখ করি
কৈছে য়ে স্঵যং দেবাদিদে঵ মহাদে঵ে চেষ্টা করিলেও প্রচলিত স্঵রর
পরা ইযাতকৈ অধিক সংখ্য়ক ঠাট বা মেল উদ্ভা঵ন করিব নো঵ারে -</p>

<p>হিন্দুস্থানী বা উত্তর ভারতীয সঙ্গীতত প্রচলিত
স্঵রর ভিত্তিত বত্রিছটা ঠাট
পণ্ডিত ব্য়ঙ্কটমখীর সমযত দক্ষিণ ভারতত প্রচলিত স্঵রর ভিত্তিত
বাসত্তরটা মেল বা ঠাট কেনেদরে উদ্ভা঵ন করা হৈছিল সেই কথা ওপরত
বিস্তৃতভাবে আলোচনা করা হ'ল । এইখিনিতে স্঵াভা঵িকতে এটা
প্রশ্নর উদয হয । উত্তর ভারতীয বা হিন্দুস্থানী সঙ্গীতত বর্তমান
কালত প্রচলিত বারটা স্঵রর পরা এনেদরে বাসত্তরটা ঠাট উদ্ভা঵ন করাটো
সম্ভ঵ হযনে নহয । হিন্দুস্থানী সঙ্গীতত একেটা স্঵ররে দুটাকৈ
নাম নাই । গতিকে ঠাট সৃষ্টির সকলো নিযম পালন করি হিন্দুস্থানী
সঙ্গীতর প্রচলিত বারটা স্঵রর পরা মাত্র বত্রিছটা ঠাটহে উদ্ভা঵ন করিব
পরা য়ায ।</p>

<p>হিন্দুস্থানী বা উত্তর সঙ্গীতত প্রচলিত বারটা স্঵র এনে
ধরণর -
সা রে রে গা মা মা পা ধা ধা নি নি</p>

<p>প্রথমতে তীব্র মধ্য়ম বাদ দি তার ঠাইত তার সপ্তকর সা সংয়োগ
করিলে স্঵র সমূহর অ঵স্থান হ'ব তলত দিযা ধরণর -
সা রে রে গা মা পা ধা নি নি সা</p>

<p>এই বারটা স্঵রর সমষ্টিক দুটা ভাগত ভগালে প্রথমার্দ্ধ হ'ব
সা রে রে গা গা মা আরু দ্঵িতীযার্দ্ধ হ'ব পা ধা ধা নি নি সা । প্রতিটো
ভাগর পরাই আমি তলত দেখু঵ার দরে চারিটাকৈ স্঵র সমষ্টি সৃষ্টি
করিব পারিম -
প্রথম ভাগর পরা -
1 ) সা রে গা মা
2 ) সা রে গা মা
3 ) সা রে গা মা
4 ) সা রে গা মা</p>

<p>দ্঵িতীয ভাগর পরা -
1 ) পা ধা নি সা
2 ) পা ধা নি সা
3 ) পা ধা নি সা
4 ) পা ধা নি সা</p>

<p>এতিযা প্রথমার্দ্ধর প্রতিটো স্঵র সমষ্টির লগত দ্঵িতীর্যাদ্ধর প্রতিটো
স্঵র-সংয়োজনা লগ লগালে ( 4 পূরণ 4 ) মুঠ 16 টা স্঵র সমষ্টি বা ঠাট
পাব পরা য়াব । এই 16 টা ঠাট হ'ব শুদ্ধ মা য়ুক্ত ঠাট । ঠিক
একেদরেই তীব্র মা য়ুক্ত স্঵র সমষ্টিও ষোলটা স্঵র সমষ্টি
য়োগ দিলে মুঠতে বত্রিছটা ঠাট পো঵া য়াব । এইদরে উদ্ভ঵ হো঵া
ঠাট সমূহর এখনি তালিকা তলত দিযা হ'ল -</p>

<p>শুদ্ধ মধ্য়ম য়ুক্ত ঠাট -
1 ) সা রে গা মা পা ধা নি সা ( ভৈর঵ী ঠাট ) //
বর্তমান শতাব্দীর আরম্ভণীর পরা উত্তর ভারতীয বা হিন্দুস্থানী
সঙ্গীতত রাগ-রাগিনী পদ্ধতির ঠাইত ঠাট বা মেল পদ্ধতির ভেটিত
রাগ-বিভাজনর প্রথার প্রচলন হ'ল । সেযেহে আজিকালি রাগিনী শব্দর
ব্য়঵হার নাই - সকলো রাগ । অ঵শ্য়ে ঠাট পদ্ধতির প্রচলনর ফলত
রাগ সঙ্গীতর অন্তর্নিহিত প্রকৃতি আরু সৌন্দর্য়্য় বহুতোখিনি হ্রাস পো঵া
বুলি স্঵র্গীয পণ্ডিত ওঙ্কারনাথ ঠাকুরকে আদি করি দুই চারিজন
সঙ্গীতবিদে আপত্তি তোলা দেখা য়ায । কিন্তু এই অভিয়োগ দুর্বল ।
আকাশবাণী আরু দূরদর্শনকে আদি করি চরকারী-বেচরকারী সকলো
শিক্ষা আরু সংস্কৃতিমূলক অনুষ্ঠানেই রাগিনী শব্দ সম্পূর্ণ পরিহার করি
কে঵ল ` রাগ ' শব্দরহে প্রযোগ করাটোলৈ চাই , ঠাট বা মেল পদ্ধতিযে
য়ে ইতিমধ্য়ে য়থেষ্ট জনপ্রিযতা আরু ব্য়াপকতা লাভ করিছে , এই
কথা নিঃসন্দেহে ক'ব পরা য়ায ।</p>

<p>গণিতর সহাযেরে য়োগ-বিযোগ করি বারটা স্঵রর পরা সর্বমুঠ
বাসত্তরটা ঠাট বা মেলর সৃষ্টি করিব পরা য়ায সঁচা ; কিন্তু প্রচলিত
রাগ-সমূহর বিভাজন বা বর্গীকরণর উদ্দেশ্য় এই বাসত্তরটা ঠাটর মাত্র
দহোটা ঠাটরহে প্রযোজন । বর্তমান য়ুগর সর্ব্বশ্রেষ্ঠ ভারতীয সঙ্গীত~
বিদ্ সকলর অন্য়তম পণ্ডিত বিষ্ণুনারাযণ ভাটখাণ্ডেজীযেও হিন্দুস্থানী
সঙ্গীতর প্রচলিত রাগসমূহর শ্রেণী বিভাজন বা বর্গীকরণর ক্ষেত্রত
মাত্র দহোটা ঠাটরহে আশ্রয লৈছে । ভাটখাণ্ডেজীর মতে বর্তমান
আমার মাজত প্রচলিত সকলোবোর রাগেই স্঵রর ভিত্তিত মাত্র দহোটা
ঠাটর সহাযেরে শ্রেণীবদ্ধ করিব পরা য়ায । অ঵শ্য়ে আধুনিক বহুতো
সঙ্গীতজ্ঞর মতে প্রচলিত রাগসমূহর সঠিক বিভাজনর বাবে মাত্র দহোটা
ঠাটেই য়থেষ্ট নহয । এই বিষযে চেতার বাদক পণ্ডিত রবি~
শঙ্করর বক্তব্য় মন করিবলগীযা -</p>

<p>দহোটা ঠাটর ভিত্তিত রাগ বিভাজনর বিষযে দুই এজন সঙ্গী~
তজ্ঞই ক'রবাত আপত্তি দর্শালেও বর্তমান হিন্দুস্থানী সঙ্গীতর
ক্ষেত্রত পণ্ডিত ভাটখাণ্ডেজীর সিদ্ধান্তকে সকলো঵ে গ্রহণ করিছে আরু
বিভিন্ন বিস্঵বিদ্য়ালয আরু সঙ্গীত-শিক্ষানুষ্ঠান-সমূহর পাঠ্য়ক্রমতো এই
পদ্ধতিকেই সন্নিবিষ্ট করা দেখা গৈছে ।</p>

<p>হিন্দুস্থানী সঙ্গীতর প্রচলিত রাগ সমূহ শ্রেণীবদ্ধ বা বর্গীকরণর
উদ্দেশ্য়ে পুরণি বাসত্তরটা মেলর পরা ভাটখাণ্ডেজীযে গ্রহণ করা
দহোটা ঠাট বা মেল এনে ধরণর -</p>

<p>1 ) সা রে গা মা পা ধা নি - বিলা঵ল ঠাট । সকলো স্঵র শুদ্ধ ।
2 ) সা রে গা মা পা ধা নি - কল্য়াণ ঠাট । মধ্য়ম তীব্র । আন সকলো
স্঵র শুদ্ধ ।
3 ) সা রে গা মা পা ধা নি - খাম্বাজ ঠাট । নিষাদ কোমল । বাকী স্঵র
শুদ্ধ ।
4 ) সা রে গা মা পা ধা নি - ভৈর঵ ঠাট । ঋযভ আরু ধৈ঵ত
কোমল । বাকী স্঵র শুদ্ধ ।
5 ) সা রে গা মা পা ধা নি - কাফী ঠাট । গান্ধার আরু নিষাদ কোমল ।
বাকী স্঵র শুদ্ধ ।
6 ) সা রে গা মা পা ধা নি - আসা঵রী ঠাট । গান্ধার , ধৈ঵ত আরু
নিষাদ কোমল ।
7 ) সা রে গা মা পা ধা নি পূরবী - ঠাট । ঋষভ , ধৈ঵ত কোমল ,
মধ্য়ম তীব্র আরু অন্য়ান্য় স্঵র শুদ্ধ । //
8 ) সা রে গা মা পা ধা নি - মার঵া ঠাট । ঋষভ কোমল , মধ্য়ম তীব্র ,
বাকী স্঵র শুদ্ধ ।
9 ) সা রে গা মা পা ধা নি - টোডী ঠাট । ঋষভ , গান্ধার আরু ধৈ঵ত
কোমল , মধ্য়ম তীব্র আরু বাকী স্঵র শুদ্ধ ।
10 ) সা রে গা মা পা ধা নি - ভৈর঵ী ঠাট । ঋষভ , গান্ধার , ধৈ঵ত
আরু নিষাদ কোমল বাকী স্঵র শুদ্ধ ।</p>

<p>ঠাটসমূহ চিনাক্ত করণর বাবে প্রত্য়েক ঠাটর সৈতে স্঵রগত
সাদৃশ্য় থকা একোটা বিখ্য়াত আরু জনপ্রিয রাগর নামেরে ঠাট
বিলাকর নামাকরণ করা হৈছে । এই কেইটা রাগ ওপরর তালিকাত
উল্লেখ করা হ'ল । ঠাট কেইটার নাম সহজে মনত রাখিবলৈ তলত
দিযা রচনাটি প্রণিধানয়োগ্য় -</p>

<p>ঠাটর বিষযে মনত রাখিবলগীযা কেতবোর নিযম
ঠাট সৃষ্টির ক্ষেত্রত কেতবোর নিযম পালন করিব লগা হয ।
তদুপরি ঠাটর কেতবোর বৈশিষ্ট্য়ও আছে । সেইবোরর বিষযে জানি
থলে সঙ্গীতর ঔপপত্তিক দিশটো আলোচনা করাত সুবিধা হ'ব বুলি
আশা করোঁ ।</p>

<p>ক ) ঠাট বা মেল সাতোটা স্঵রর সমষ্টি । গতিকে , সাতোটাতকৈ
কম বা অধিক সংখ্য়ক স্঵র এটা ঠাটত থাকিব নো঵ারে । তদুপরি
স্঵র সমূহর অ঵স্থান ক্রমানুসারে অর্থাত্ সা - র পিছত রে , রে-র পিছত
গা ইত্য়াদি ধরণেরে হো঵াটো ঠাটর অন্য় এটা বিশেষত্঵ ।</p>

<p>খ ) কোনো কোনো ক্ষেত্রত একেটা স্঵ররে শুদ্ধ আরু বিকৃতরূপ
ক্রমানুসারে ঠাটত সন্নিবিষ্ট হ'ব পারে । হিন্দুস্থানী রাগ-পদ্ধতিত
ব্য়঵হৃত হো঵া ঠাটসমূহত এনেধরণর স্঵র সংয়োজনা দেখা নগলেও ,
কর্ণাটক পদ্ধতিত এনেধরণর ঠাট বা মেলর প্রযোজন আছে ।</p>

<p>গ ) ঠাটত কে঵ল স্঵র-সমূহর আরোহণহে থাকে । অ঵রোহণর
প্রযোজন নহয । ঠাটর সৃষ্টি রাগ বিভাজন বা বর্গীকরণর উদ্দেশ্য় ।
কণ্ঠ বা য়ন্ত্রসঙ্গীতর সাঙ্গীতিক স্঵রবিন্য়াস হিচাপে তাক গ্রহণ করা
নহয । সেইবাবে ঠাটর স্঵র-সমূহর অ঵রোহ , বা রঞ্জকতা আদি গুণর
প্রযোজন নাই ।</p>

<p>ঘ ) আগতেই কৈ অহা হৈছে য়ে কোনো এটা ঠাট চিনাক্ত~
করণর বাবে সেই ঠাটর লগত স্঵রগত সাদৃশ্য় থকা এটা বিখ্য়াত
আরু জনপ্রিয রাগর নামেরে ঠাটটোর নামাকরণ করা হয । উদাহরণ
স্঵রূপে ` সা রে গা মা পা ধা নি ' , কোমল রে আরু ধা য়ুক্ত
এই স্঵র সমষ্টি ভৈর঵ নামর এটা বিখ্য়াত রাগর স্঵রর সৈতে মিলে ।
গতিকে কোমল রে আরু ধা য়ুক্ত এই স্঵র-সমষ্টির নাম ভৈর঵ ঠাট
রখা হ'ল । সেইদরে কোমল রে , গা , ধা আরু নি য়ুক্ত "" সা
রে গা মা পা ধা নি "" - এই স্঵রসমষ্টিটোর ভৈরবী নামর রাগর
স্঵রর সৈতে মিল আছে । গতিকে এই স্঵রসমষ্টিটো ভৈরবী ঠাট নামে
জনাজাত হ'ল । //
রাগ
বিশ্঵ সংস্কৃতির অনুপম রত্নভাণ্ডারলৈ ভারতবর্ষর সর্বশ্রেষ্ঠ অ঵দান
হ'ল রাগ-সঙ্গীত , এই কথাত সন্দেহর অ঵কাশ নাই । রাগ সঙ্গীত
কোনো এক ব্য়ক্তি বিশেষর সৃষ্টি বা কোনো বিশেষ স্থান বা
কালরো অ঵দান নহয । শতাব্দী জোরা সাধনারে পুষ্ট ভারতীয
দর্শন , সাহিত্য় , চিত্রকলা আরু স্থপতিবিদ্য়ার দরেই রাগ সঙ্গীতো ভার~
তীয পরম্পরার এটি অবিচ্ছেদ্য় অঙ্গ । সেযে ভারতীয সভ্য়তাই য়'তে
বিকাশ বা প্রসার লাভ করিছে রাগ-সঙ্গীতেও সেই ঠাইর জন সাধারণর
আধ্য়াত্মিক আরু সাংস্কৃতিক জী঵নত প্রভূত প্রভা঵ বিস্তার করি
আহিছে । সিন্ধু-গঙ্গা উপত্য়কার বৈদিক ব্রাহ্মণর সাম গানর পরা
আরম্ভ করি দ্রাবিড় আরু অনার্য় জনজাতীয সুর সম্ভূত কর্ণাটী , আন্ধ্রী ,
গুর্জ্জরী , মাল঵ী আদি সুরের পরিপুষ্ট হৈ উঠিছে ভারতীয রাগ-~
সঙ্গীত । পশ্চিম ভারতর সোরট , মুলতানী আরু গান্ধারীর সতে , কামরূ~
পর চালনি , গুঞ্জরী , আকাশ মণ়্ডল , মেঘমণ্ডল , দে঵মোহন রাগে
একেলগে ওরা ধরি ইযার সৌষ্ঠ঵ বঢ়াই তুলিছে । রাগ-সঙ্গীত কে঵ল
ভারতীয সঙ্গীত কলার শ্রেষ্ঠতম নিদর্শনেই নহয , আমার কৃষ্টি আরু
চিন্তাধারার ঐক্য় আরু সমন্঵যর প্রতীক হিছাপেও তাক অবিসম্বাদে
গ্রহণ করিব পরা য়ায ।</p>

<p>উল্লেখয়োগ্য় য়ে ভারতীয শাস্ত্রীয সঙ্গীতক বেদর দরেই অপৌরু~
ষেয বুলি গণ্য় করিলেও বর্তমান অর্থত ` রাগ ' শব্দটোর প্রযোগ
বহুতো পিছর কালতহে আরম্ভ হয । ভরতর নাট্য়শাস্ত্রত জাতি
গানরহে উল্লেখ আছে । অ঵শ্য়ে জাতি গানর মুখ্য় লক্ষণ ` অংশ '
স্঵রর বিষযে ক'বলৈ য়াওঁতে -
বুলি গ্রন্থকারে রাগ শব্দটো প্রযোগ করিছে । নাট্য় শাস্ত্রর পাছর
গ্রন্থ "" নারদীয শিক্ষা "" আদিতো রাগ শব্দর স্পষ্ট উল্লেখ নাই ।</p>

<p>সঙ্গীতর প্রাচীন গ্রন্থসমূহর ভিতরত আনুমানিক ন঵ম শতাব্দীতে
রচিত মতজ্ঞদে঵র "" বৃহদ্দেশী "" নামর গ্রন্থত পোন প্রথমবারর বাবে
রাগ শব্দর স্পষ্ট উল্লেখ পো঵া য়ায । মতজ্ঞদে঵র মতে সর্বসাধারণ
শ্রোতার মন রঞ্জিত করিব পরা , স্঵র আরু বর্ণরে বিভূষিত বিশিষ্ট
স্঵র বা ধ্঵নির সমষ্টিযেই হ'ল রাগ -</p>

<p>বহুতো আধুনিক সঙ্গীতবিদে পাশ্চাত্য় সঙ্গীতর ` ম'দ ( Mode )
অথবা ` স্কেল ' ( scale )র লগত ভারতীয রাগর তুলনা করিবলৈ বিচারে ।
কিন্তু ম'দ বা স্কেলর সহাযত আমি এটা সুরত ব্য়঵হৃত হো঵া স্঵র~
বিলাকর আভাসহে পাব পারোঁ । তার য়োগেদি রাগ ফুটি নুঠে ।
কারণ রাগ মানে কেতবোর স্঵রর থুপ বা সমষ্টিযেই নহয । একোটা
রাগর স্঵রূপ আরু প্রকৃতি অনুয়াযী স্঵র-সমূহর প্রকাশ ভঙ্গী , সংয়োজনা ,
উপস্থাপন আরু তার লগতে শিল্পীর অনুভূতি , সৃজন প্রতিভা আদি
সকলো একত্রিত হলেহে এটা ` স্কেল ' বা ` ম'দ 'র পরা রাগর সৃষ্টি
হো঵াটো সম্ভ঵ । প্রতিমা সাজিবলৈ ব্য়঵হার করা খেরর জুমুঠিটোক
প্রতিমা আখ্য়া দিব নো঵ারি । মাটি , রং আরু অন্য়ান্য় কারু~
কার্য়্য়েরে খনিকরর কুশল হাতর পরশত মূর্তিমান হৈ উঠিলেহে তাক
প্রতিমা বোলা হয । ` ম'দ ' বা ` স্কেল 'ক রাগ আখ্য়া দিযাটো ,
জুমুঠিটোক প্রতিমা বোলার দরেই ।</p>

<p>
বর্তমান শতাব্দীর আরম্ভণীরে পরা ভারতীয রাগ সঙ্গীতে ভালেমান
পাশ্চাত্য় সঙ্গীতবিদরো দৃষ্টি আকর্ষণ করিবলৈ সক্ষম হৈছে । কেইবা~
গরাকীও পাশ্চাত্য় সঙ্গীতবিদে রাগ সঙ্গীতর সংজ্ঞা নিরূপণ করার
প্রযাস করাও দেখা গৈছে । পপ্লে ( Popley ) চাহাবর মতে রাগ
হ'ল একোটা সপ্তকর ভিতরত থকা স্঵র সমূহর বিশিষ্ট ক্রম বা
অভিয়োজনা য়াক সকলো ভারতীয গীতর আধার হিছাপে গ্রহণ করা
হয । //
বিশিষ্ট স্঵র সংয়োজনা আরু কোনো কোনো বিশেষ স্঵রর
ওপরত দিযা গুরুত্঵ই এটা রাগক আনটোর পরা পৃথক করে ।
রাগ সঙ্গীতর প্রতি বিশেষভাবে আকৃষ্ট হো঵া বর্তমান কালর প্রখ্য়াত
বেহেলা বাদক যহুদী মেনুহীনর মতে রাগ হ'ল -</p>

<p>রাগ সঙ্গীতর প্রচলনর কাল বা সময সম্পর্কে আমার দেশর
পণ্ডিত সকলর বিভিন্ন মত থাকিলেও , রাগ শব্দর অর্থ সম্পর্কে
প্রায সকলো প্রাচীন গ্রন্থকারেই একমত । রাগ শব্দর উত্পত্তি
সংস্কৃতর ` রঞ্জ ' শব্দর পরা । রঞ্জ মানে রঙেরে বোলো঵া । "" রঞ্জযতীতি
রাগঃ "" - অর্থাত্ য়ি শ্রোতার মন রঙেরে বোলাব পারে সেযেই
রাগ । চিত্রকরে তুলিকার সহাযত এখনি পটত বিভিন্ন রঙেরে
ছবি অঁকার দরেই রাগ সঙ্গীতে শ্রোতার মানসপটত স্঵রর সহাযেরে
বিভিন্ন ভাব আরু আবেগ অনুভূতির সৃষ্টি করি শুনোঁতাক আনন্দ
দান করে ।</p>

<p>প্রাচীন গ্রন্থকারসকলর মতে স্঵র আরু বর্ণর দ্঵ারা সমৃদ্ধ
ধ্঵নি বা স্঵রর সমষ্টি য়ি শ্রোতাক আন্নদ দিব পারে সেযেই হ'ল
রাগ । মতজ্ঞর বৃহদ্দেশী গ্রন্থত উল্লেখিত শ্লোকর অর্থকে পাছর
য়ুগর ভালেমান গ্রন্থকারে সামান্য় পরিবর্তন করি রাগর ব্য়াখ্য়া দিওঁতে
ব্য়঵হার করিছে -</p>

<p>রাগর লক্ষণ
রাগ সম্পর্কে বিশদভা঵ে আলোচনা করার আগতে কেইটিমান
প্রযোজনীয কথা উল্লেখ করি থো঵া ভাল হ'ব । রঞ্জকতা বা
শ্রোতার মনত আনন্দ দিব পরা গুণর উপরিও প্রত্য়েক রাগেই
কোনো এটা ঠাট বা মেলর অন্তর্ভুক্ত হো঵া উচিত । সাতোটাতকৈ
অধিক অথবা পাঁচোটাতকৈ কম সংখ্য়ক স্঵রর দ্঵ারাই রাগর সৃষ্টি
হ'ব নো঵ারে । অ঵শ্য়ে এই ক্ষেত্রত দুই এটা ব্য়তিক্রম নোহো঵া
নহয । উদাহরণ স্঵রূপে ভ঵ানী নামর রাগত মাত্র চারিটা স্঵র আরু
মালশ্রীত তিনিটা স্঵রহে ঘাইকৈ ব্য়঵হার করা হয ।</p>

<p>রাগত সাধারণতে একেটা স্঵ররে শুদ্ধ আরু বিকৃত দুযোটা
রূপ এটার পাছত আনটো ক্রমানুসারে ব্য়঵হার নহয । কিন্তু এই
নিযমরো ব্য়তিক্রম নোহো঵া নহয । কেদার রাগর আরোহণত কোনো
কোনো সমযত তীব্র আরু শুদ্ধ দুযোটা মধ্য়ম এটার পাছত আনটো
ব্য়঵হার করি রার সৌন্দর্য়্য় বঢ়ো঵া হয । ললিত নামর অন্য় এটা
রাগত শুদ্ধ আরু তীব্র মধ্য়ম একেলগে ব্য়঵হার করা হয । অ঵শ্য়ে
এনেধরণর রাগর সংখ্য়া অধিক নহয , আরু প্রচলিত সরহ-সংখ্য়ক
রাগতে এটা স্঵রর শুদ্ধ আরু বিকৃত রূপ ক্রমানুসারে ব্য়঵হার হো঵া
দেখা নায়ায ।</p>

<p>কোনো রাগতে মধ্য়ম আরু পঞ্চম একেলগে বর্জন করা নহয ।
মধ্য়ম বর্জিত হ'লে পঞ্চম স্঵রর ব্য়঵হার অনিবার্য়্য় আরু সেইদরেই
কোনো রাগত পঞ্চম স্঵র ব্য়঵হৃত নহ'লে , মধ্য়ম স্঵রর প্রাধান্য়
থাকিবই লাগিব ।</p>

<p>রাগত আরোহণ আরু অ঵রোহণ দুযোটারে প্রযোজন । রাগ ভেদে
বহুতো সমযত ব্য়঵হৃত স্঵র সমূহর রূপ বা ক্রমর সালসলনি ঘটে ।
বহুতো রাগ আছে য়িবোরর আরোহণত ব্য়঵হৃত স্঵রর সতে অ঵রো~
হণর স্঵রর বহুতোখিনি পার্থক্য় থাকে । সেইবাবে রাগর ক্ষেত্রত তার
সঠিক রূপ প্রকাশর বাবে স্঵রসমূহর আরোহণ আরু অ঵রোহণ দুযোটা
ক্রিযারে প্রযোজন হয । //
বর্ণ
প্রাচীন গ্রন্থকার৉সকলে রাগর বিষযে কওঁতে "" স্঵রবর্ণবিভূষিতঃ ""
বুলি উল্লেখ করা কথাষার তাত্পর্য়্য়পূর্ণ । কে঵ল মাত্র স্঵রর সমষ্টিযেই
রাগর সৃষ্টি হ'ব নো঵ারে । ব্য়঵হৃত স্঵রসমূহর একোটা বিশেষ অভি~
য়োজনার দ্঵ারাহে বিভিন্ন রাগ বা সুরর সৃষ্টি হো঵াটো সম্ভ঵ ।
স্঵রর এই বিশিষ্ট গতি বা গো঵ার ধরণকে বর্ণ বোলা হয ।</p>

<p>বর্ণ চারি প্রকারর । স্থাযী বর্ণ , আরোহী বর্ণ , অ঵রোহী বর্ণ
আরু সঞ্চারী বর্ণ ।</p>

<p>একেটা স্থানতে কোনো এটা স্঵র একাধিক বার একেলগে
উচ্চারিত হ'লে তাক স্থাযী বর্ণ বোলা হয । য়েনে - সা সা সা ,
গা গা গা , মা মা , পা পা পা পা , সা সা সা সা ।</p>

<p>সুর সৃষ্টির বাবে কেতিযাবা স্঵রসমূহক এটার পাছত আনটো
ক্রমানুসারে খাদর পরা চরালৈ আরু কেতিযাবা তার বিপরীতে চরার
পরা খাদলৈ প্রযোগ করিবলগীযা হয । খাদর ক্রমাত্ চরার
ফালে গতি করা ক্রিযাক আরোহী বর্ণ বোলা হয । য়েনে - সা রে
গা মা পা ধা নি অথবা গা পা ধা নি চা রে গা ইত্য়াদি । ঠিক একেদরেই
চরার পরা ক্রমাত্ খাদর ফালে গতি করার প্রক্রিযাক অ঵রোহী
বর্ণ বোলে । উদাহরণ স্঵রূপে - সা নি ধা পা মা গা রে অথবা ধা পা মা
গা রে সা ইত্য়াদি ।</p>

<p>স্থাযী , আরোহী আরু অ঵রোহী এই তিনিও প্রকারর বর্ণর
সংমিশ্রণর ফলত সৃষ্টি হয সঞ্চারী বর্ণ । উদাহরণ স্঵রূপে - সা সা সা রে
গা মা পা ধা নি রে রে সা নি ধা পা ইত্য়াদি । এই স্঵র সমূহর প্রথম
তিনিটা স্঵রত স্থাযী বর্ণ ব্য়঵হার করা হৈছে । তার পাছত রে গা মা
পা ধা নি বোলোঁতে আরোহী বর্ণ , রে রে বোলোঁতে পুনর স্থাযী বর্ণ
আরু শেষত সা নি ধা পা অংশটোত অ঵রোহী বর্ণ ব্য়঵হৃত হৈছে ।</p>

<p>রাগর জাতি বিভাজন - ঔড়঵ , ষাড়঵ , সম্পূর্ণ ইত্য়াদি
এটা রাগক কোন ঠাট বা মেলর অন্তর্ভুক্ত করা উচিত , সেই
কথা ঘাইকৈ নির্ভর করে রাগটোত ব্য়঵হৃত স্঵রসমূহর একোটা ঠাটর
সতে থকা সাদৃশ্য়র ওপরত । কিন্তু প্রত্য়েক ঠাটতে সাতোটা স্঵র
প্রয়ুক্ত হ'লেও রাগর ক্ষেত্রত এই নিযমর ব্য়তিক্রম দেখা য়ায ।
সুরর বৈচিত্র আরু ভাব রস সঞ্চারর বাবে বিভিন্ন ধরণর স্঵র-সংয়োজন
রাগর পক্ষে অপরিহার্য়্য় । ফলস্঵রূপে সুর , ভা঵ আরু রস অনুসরি
প্রত্য়েক রাগরে স্঵র-সংখ্য়া আরু ক্রমর সাল-সলনি হো঵াটো স্঵াভা঵িক ।</p>

<p>স্঵রর সংখ্য়া অনুসরি রাগসমূহক ঘাইকৈ তিনিটা ভাগত ভগাব
পরা য়ায । প্রথম ভাগত ধরা হয সাতোটা স্঵রয়ুক্ত রাগসমূহক ।
এই প্রকারর সাতোটা স্঵রয়ুক্ত রাগর সম্পূর্ণ জাতির বোলা হয ।
দ্঵িতীয ভাগত ধরা হয ছটা স্঵রয়ুক্ত রাগসমূহক য়াক ষাড়ব জাতির
রাগ বুলি কো঵া হয । পাঁচটা স্঵রর রাগসমূহক তৃতীয ভাগত সামরি
লো঵া হয আরু এনেধরণর রাগসমূহক বোলা হয ঔড়঵ জাতির রাগ ।
ভারতীয শাস্ত্রীয সঙ্গীতর প্রচলিত নিযম অনুসরি পাঁচোটাতকৈ কম সংখ্য়ক
স্঵রেরে রাগ সৃষ্টি হো঵াটো সম্ভ঵ নহয । অ঵শ্য়ে এই ক্ষেত্রত দুই
এটা ব্য়তিক্রমর কথা ইতিপূর্বে উল্লেখ করা হৈছে ।</p>

<p>রাগসমূহক বহলভা঵ে সম্পূর্ণ , ষাড়ব আরু ঔড়ব - এই তিনি
ভাগত ভগাব পারিলেও বহুসংখ্য়ক রাগত আরোহত ব্য়঵হৃত স্঵রর
সংখ্য়া অ঵রোহর সংখ্য়াতকৈ অদিক কিম্বা কম হো঵াও দেখিবলৈ
পো঵া য়ায । সেইবাবে সম্পূর্ণ ষাড়ব আরু ঔড়ব এই তিনি প্রকারর
জাতির রাগক ইটোর সতে আনটো সংয়োগ করি মুঠতে ন প্রকারর
জাতির সৃষ্টি করা হৈছে । কো঵া বাহুল্য় প্রচলিত সকলো রাগকে
এই ন প্রকারর জাতিযে সামরি ল'ব পারে ।
 +&gt;*
",0
</p></body></text></cesDoc>