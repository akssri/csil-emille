<cesDoc id="asm-w-literature-travel-atr16" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-literature-travel-atr16.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>প্রকাশ--</h.title>
<h.author>শইকীযা--</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - magaz</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">atr16</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0711.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;চীনত কি দেখিলো
[ প্রথমবার চীন দেশলৈ গৈছিলো 1984 চনর
26 অক্টোবর তারিখে । সেইবারর কথা লিখি~
ছিলো । দুটা অধ্য়ায লিখার পিছতে আরু লেখার
মন নোহো঵া হৈ গ'ল । মনর মহিমা পো঵া
টান । মনেই কেতিযাবা লেখকর শত্রু হৈ উঠে ।
অ঵শ্য়ে সেইবার বহুত কিবাকিবি দেখিলো আরু
চালো । কিন্তু জানিলো জানো ? সেই সমযর
কথাখিনি এইবারর ভ্রমণ কাহিনীতে সুমাবলৈ
য়ত্ন করিম ।</p>

<p>এইবার বেইজিঙলৈ গৈছিলো 1989 চনর
3 জুনত । সেইদিনা রাতিযেই তিযেনআনমান
প্রাঙ্গণত ধর্ণা দি ছাত্রসকলর ওপরত চীনা চর~
কারে সামরিক অভিয়ান চলাইছিল । ফলত বেইজিং
কেইদিনমানর বাবে মরা চহরত পরিণত হ'ল ।
আমার গৃহস্থ আছিল চীনা লেখক সংঘ ।
লেখক সংঘই এইবারর ভ্রমণ স্থগিত রাখিবলৈ
সিদ্ধান্ত ল'লে । ফলত আমি 8 জুনর দিনা
বেইজিঙর পরা গুচি আহিব লগাত পরিলো ।</p>

<p>জুন মাহর অভিজ্ঞতাকে এই ভ্রমণ কাহিনীর
আদিতে ` প্রকাশ 'র পাঠকসকলক জনাব খুজিছো ।
কারণ 3-4 জুনর সামরিক অভিয়ান আরু চীনা
ছাত্র আন্দোলন সম্পর্কে জানিবর বাবে বহুতেই
আগ্রহ প্রকাশ করিছে । তাত য়ি দেখিছো ,
য়ি পঢ়িছো , আরু য়ি শুনিছো তাকেই য়ুক্তি~
সঙ্গতভা঵ে ক'বলৈ য়ত্ন করিম । ]</p>

<p>অতি প্রত্য়ুষতে আমি দিল্লীর ইন্দিরা
গান্ধী আন্তর্জাতিক বিমান-বন্দর ওলালোগৈ ।
সেইদিনা আছিল 1989 চনর তিনি জুন ।
মোর লগত শ্রীবিজয তেণ্ডুলকার , ডঃ ইউ.আর
অনন্তমূর্তি , ডঃ গোপীচাঁদ নারেং আরু শ্রীমতী
মৃণাল পাণ্ডে । বিজয তেণ্ডুলকার প্রসিদ্ধ মারাঠী
নাট্য়কার । অনন্তমূর্তি প্রসিদ্ধ কন্নড় ঔপন্য়াসিক আরু
গল্প লেখক । নারেং প্রসিদ্ধ উর্দ্দু সমালোচক ।
মৃণাল পাণ্ডে হিন্দী সাপ্তাহিক ` হিন্দুস্থান 'র
সম্পাদিকা আরু কাহিনীলেখিকা । লেখক~
সকলক নির্বাচন করিছিল ভারতর সাংস্কৃতিক
সম্পর্করক্ষী পরিষদে । মোক দলটোর নেতা করি
দিছিল । এই দাযিত্঵ পালনর বাবে মোর কিবা
বিশেষ য়োগ্য়তা আছে বুলি মই নেভাবো । এটা
কথাই অ঵শ্য়ে মোক মুগ্ধ করিছিল । এই আটাই~
কেউজন প্রসিদ্ধ লেখকে এই য়াত্রাত সকলো প্রকারে
মোক সহায করিছিল । এই কারণে তেওঁলোকর
প্রতি মই কৃতজ্ঞতা প্রকাশ করিবলৈ বাধ্য় ।</p>

<p>আমি এযার ফ্রেন্সর এখন বিমানত গৈ
বেইজিং সময চারে পাঁচ বজাত চীন দেশত প্র঵েশ
করিলোগৈ । এযারপর্টটো নকৈ বহলাই শু঵নি
করিবলৈ য়ত্ন করার দৃশ্য় চকুত পরিল । অল~
পতে চীনত ` এছিযাড ' হ'ব । তার বাবেই এই
নবীকরণ ।</p>

<p>বেইজিং এযার পর্টটো আগরবার য়িমান
ভাল লাগিছিল , এইবার নেলাগিল । রাণ঵ের
একেবারে দাঁতিফালে পকার ভিতরতে বন গজিছে ।</p>

<p>আমাক আগ বঢ়াবলৈ বেইজিঙর ভারতীয
দূতাবাসর লোক আরু চীনা লেখক সংঘর প্রতি~
নিধি আহিছিল । তেওঁলোকর লগত চিনাকি
হ'লো । ভারতীয দূতাবাসর প্রতিনিধিজনক
সুধিলো , "" বেইজিঙর অ঵স্থা কেনে ? ""
"" উদ্঵েগজনক । ""
"" আমি চীন দেশ সুকলমে ভ্রমণ করিব
পারিমনে ? ""
"" পারিব বুলিযেই ভাবিছো । ""</p>

<p>অলপ পিছতে দুখন টেক্সিত আমাক উঠি~
বলৈ দিযা হ'ল । লগে লগে বেইজিং অভিমুখে
আমার য়াত্রা আরম্ভ হ'ল । আগরবার আহোতে
বাটটো বর নির্জন য়েন লাগিছিল । এইবার
কিয জানো তেনে নেলাগিল ।</p>

<p>বেইজিং এযারপর্টত লগ পো঵া লোকসকলর
নাম জানি লৈছিলো । ভারতীয দূতাবাসর প্রতি~
নিধি হিচাপে আহিছিল গৌতম বোম্বা঵ালা । চীনা
লেখক সংঘর পরা আহিছিল জাং কু঵ান জু঵ান
বাসর প্রথম সচিব । চেন্ চিরু চীনা লেখক
সংঘর সদস্য় আরু কু঵ান জু঵ান দোভাষী ।
আমি রাতি 8-10 বজাত আহি বেইজিঙর হোটেল
পালোগৈ । তেতিযা তিযেনআনমান স্কো঵ার
উত্তপ্ত । আমার গাড়ী সেইপিনেদি নগৈ ঘূরি-পকি
আহিহে হোটেল সোমালেহি । হোটেলটোর নাম
জিন যু঵ান । তার 1524 নং কোঠাত মোক থাকি~
বলৈ দিযা হ'ল । এই হোটেলটোত য়ো঵াবারো
মই আছিলোহি ।</p>

<p>মুখ-হাত ধুই তলত খাবলৈ গৈ দেখো রেষ্টোরাঁ
বন্ধ । ঘড়ী চাই দেখো রাতি দহ বাজি গ'ল ।
কফি-বিপনিত লঘু আহার খাই চীনা দেশ ভ্রম~
ণর সূচীখন জানি ললো । চেনে ক'লে , বেই~
জিঙত আমি 7 জুনলৈকে থাকিব লাগিব । তার
পিছত চাংহাইলৈ য়াব লাগিব । আমার ভ্রমণ~
সূচীত আরু দুখন চহর আছিল । এখনর নাম
হংজৌ । ইখনর নাম গু঵াংজৌ । গু঵াংজৌ হ'ল
প্রাচীন কেণ্টন চহর ।</p>

<p>হোটেলটো বেইজিং বিশ্঵বিদ্য়ালযর পরা 4
কিলোমিটার দূরত । বেইজিঙর হৃত্পিণ্ড তিযেন
আনন্মান স্কো঵ারর পরা হোটেলর দূরত্঵ ছয
কি দহ কিলোমিটারর । সেই সমযত সমগ্র
বিশ্঵বাসীর চকু এই স্কো঵ারর ওপরত । তাত
বেইজিঙর ছাত্রসকলে ধর্ণা দিছে আরু অনশন
করিছে । তেওঁলোকর দাবী হ'ল দুর্নীতি নিবা~
রণ আরু বাক্ স্঵াধীনতার প্রতিষ্ঠা ।</p>

<p>বেইজিং এযারপর্টর পরা বেইজিং মহা~
নগরলৈ আহিলে য়াত্রীযে সদায তিযেনআন্~
মান স্কো঵ার পার হৈহে আবাস-স্থানলৈ সততে
আহে । য়ো঵াবার আহোঁতে ( 1984 চনর
অক্টোবর-নবেম্বর মাহত ) আমি এই ঠাই পার
হৈহে হোটেল পাইছিলোগৈ । এইবার কিন্তু এই
স্কো঵ারলৈ য়ো঵া নহ'ল । য়ো঵া অসম্ভ঵ো ।</p>

<p>বেইজিং মহানগর সোমো঵ারে পরা সততে
এটা দৃশ্য় সঘনাই চকুত পরিছিল । এক কিলো~
মিটার আরু দুই কিলোমিটার দূরে দূরে নাগ~
রিকর জুম । কিছুমান ঠাইত নাগরিকসকলে
ট্রাক রখাই সেনাসকলর লগত কথা পাতিছে ।
অনুমান হ'ল মানুহে তেওঁলোকক তিযেনআন~
মানলৈ য়াবলৈ হাক দিছে । ঠাযে ঠাযে সেনার
একোটি গোট ভরি মিলাই খোজ কাঢ়ি গৈছে ।
অনুমান হ'ল এওঁলোক উভতি গৈছে । পিছত
গম পাইছিলো কেইবাদিনরে পরা সেনাসকলক
তিযেনআনমান প্রাঙ্গনলৈ অহাত নাগরিকসকলে
বাধা দি আছিল । আমি বাটত য়িবোর
সেনা দেখা পাইছিলো সেইবোরর হাতত
বন্দুক-বারুদ একো নাছিল । চীনা সেনাসকলে
বোলে সাধারণতে বন্দুক লৈ নুফুরে ।</p>

<p>চেনে ক'লে , "" আপোনালোকক আমি আও~
হতীযা বাটেদি হোটেললৈ লৈ য়াম । বেযা নেপাব । ""</p>

<p>আমার বেযা পাবলৈ একো নাছিল । গৃহস্থই
আলহীক পোনপটীযা বাটে নিনি আওহতীযা
বাটে নিলেও নিব পারে । তাত আপত্তি কিয
করিম ? আওহতীযা বাটে য়াওঁতে কিছুমান পিছ~
পরা অঞ্চল দেখা পালো । তাত আলিবাট কেঁচা ,
ঘর-দু঵াররো শোভা নাই ।</p>

<p>আমি কিন্তু তেতিযা জনা নাছিলো তলে
তলে সামরিক অভিয়ানর আযোজন চলিছে ।
সেইদিনা রাতি 27 নং সেনা বাহিনীটোর হতু঵াই
তিযেনআনমান স্কো঵ারত ধর্ণা দি থকা ছাত্র~
সকলক তারপরা বলেরে স্থানান্তরিত করার বাবে
চীনা চরকারে ব্য়঵স্থা লৈছিল । মই সপোনতো
এই কথা ভাবিব নো঵ারিলোহেঁতেন । মোর দৃঢ়
বিশ্঵াস আছিল দুযো পক্ষই শান্তিপূর্ণভা঵ে
ছাত্রর দাবীসমূহর কথা আলোচনা করি এটা
মীমাংসাত উপনীত হ'ব । মাও-চে-টুঙে চীনা
সমাজবাদী সমাজর অন্তঃবিরোধসমূহ মীমাংসা
করার বাবে শান্তিপূর্ণ পদ্ধতি অ঵লম্বন করার কথা
কৈছিল । তেওঁর মতে রাইজর ভিতরত ঘটা
মতভেদর মীমাংসা শান্তিপূর্ণ হ'ব লাগে । চীন
দেশর আদর্শই শান্তিপূর্ণ মীমাংসার পথ সহজ
করিব বুলি মোর এটা দৃঢ় বিশ্঵াস আছিল ।</p>

<p>রাতি ভাল টোপনি গৈছিল । পু঵া গা-পা
ধুই তললৈ গৈ পু঵ার আহার খাবর সমযতহে
গম পালো রাতি কিবা এটা অভাবনীয ঘটনা
ঘটিল । হোটেলটোত সরহভাগ মানুহেই বিদেশী ।
হোটেলর পরিচারিকাসকল অ঵শ্য়ে চীনা । কিন্তু
চীনা ভাষা নেজানিলে তেওঁলোকর লগত কথা
পতা মস্কিল । সেইদিনা কাগজো ওলো঵া নাই ।
চেনে মাথোন ক'লে , তিযেনআনমান স্কো঵ার
বর্তমান সেনাবাহিনীর দখলত । ইযার অর্থ ছাত্র
সকল বহিস্কৃত । কথা-বতরা হো঵ার পিছত বুজা
গ'ল য়ে য়থেষ্ট মানুহ মরিছে । কিন্তু কিমান
মরিছে তাক জনা টান হৈ পরিল ।</p>

<p>চীনা সেনাবোরর চেহেরা দেখিলে শান্ত-শিষ্ট
ল'রার দরে লাগে । তেওঁলোক হঠাতে নিষ্ঠুর
হৈ উঠিব , এই কথা ভাবিবলৈও টান । আমি
আমার ভিতরতে ঘটনাটো আলোচনা করিবলৈ
চেষ্টা করিলো । লাহে লাহে কিছু খবর পো঵া
গ'ল । বহুত সমযরে পরা সেনাবোরে তিযেন~
আনমানত সোমো঵ার আখরা চলাইছিল য়দিও
নিশা 1-30 বজাতহে নিরস্ত্র ছাত্রসকলর বিরুদ্ধে
নিযমিত সামরিক অভিয়ান আরম্ভ হ'ল । সেই
অভিয়ান দৃশ্য় আংশিকভা঵ে দেখা মানুহ
পিছত লগ পাইছিলো । তারে দুজন মানুহ
আমার দূতাবাসর চাকরিযাল । তেওঁলোক কিছু
সময তিযেনআনমান প্রাঙ্গণত আছিল । 		//
4 জুনর দিনা আমি ঘটনাবোর সম্য়ক স্঵রূপ
উপলব্ধি করিব পরা নাছিলো । কাজেই য়েতিযা
চেনে ক'লে , "" এই ভ্রমণ পূর্ণ করিব পরা অসম্ভ঵ "" ,
তেতিযা আমি চক্ খাই গৈছিলো । আমি
ক'লো , "" আমি ভ্রমণ কিয সামরিব লাগে , তাক
বুজি পো঵া নাই । ""</p>

<p>চেনে ক'লে , "" সকলো বন্ধ । চীনা প্লেনো
বন্ধ । বাছ-টেক্সি বন্ধ । বেইজিঙতে ফুরা নিরাপদ
নহয । গণ্ডগোল ক্রমাত্ আন চহরবোরলৈ বিয~
পিছে । য়ো঵া টান হ'ব । চরকারে রেডিঅ'
য়োগে কৈছে , সকলো মানুহ ঘরর ভিতরত
থাকিব লাগে । চহরর ঠাযে ঠাযে গুলির শব্দও
শুনা গৈছে । আপোনালোক ঘূরি য়ো঵াই ভাল
হ'ব । অ঵শ্য়ে চীনা কর্ত্তৃপক্ষই গৃহস্থ হৈ আল~
হীক উভতি য়াবলৈ কো঵াটো বর অশোভনীয
হ'ব । সেইবাবে আপোনালোকে নিজে এটা সিদ্ধান্ত
ল'লে ভাল হয । ""</p>

<p>কিন্তু আমি ক'লো , "" মিঃ চেন , আমিনো এই
বিপদর সমযত আপোনালোকক এরি কেনেকৈ
য়াওঁ ? আমার কষ্ট হয হওক । আপোনালোকর
বিপদ আমারো বিপদ । আমি ভ্রমণ বাতিল
নকরি আরম্ভ করিবলৈহে ভাল পাম । আপো~
নালোকর দুখত আমি দুখী । ""</p>

<p>চেনে উত্তর দিলে , "" আমার লেখক সংস্থাই
কৈছে আপোনালোক পাঁচজনক আকৌ মাতিব ।
লাগিলে ভারতর পরা ইযালৈ অহা আরু ইযার
পরা য়ো঵া খরচো আমিযেই ভরিম । কিন্তু
এতিযা ভ্রমণ য়ুগুতো঵া আরু চালু রখা সহজ
কাম নহ'ব । আপোনালোকে বোধহয বুজি পো঵া
নাই পরিস্থিতি কি ভযানক । ""</p>

<p>দোভাষিণী শ্রীমতী জাং কু঵ান জু঵ানে হঠাতে
চেন্ চিরু আরু তেওঁর ঘরু঵া পরিস্থিতির কথা
ক'লে । তেওঁলোকে কোনেও নিজর নিজর ঘরর
খবর পো঵া নাই । জু঵ানর এজনী সরু ছো঵ালী
আছে । গিরিযেকে মার্কিন য়ুক্তরাষ্ট্রত পঢ়ি আছে ।
তেওঁলোক দুযোরে বর কম বযসতে বিযা হৈছিল ।
সরু ছো঵ালীজনী ` বেবী-ছিটার 'র হাতত গতাই
থৈ আহিছে । জু঵ানর অন্তরত উদ্঵েগ আরু
দুশ্চিন্তা । তেওঁর য়ন্ত্রণা দেখি নিমিষতে বুজি
পালো য়ে সমগ্র মহানগরর য়োগায়োগ ব্য়঵স্থাত
ব্য়াঘাত জন্মিছে । য়াতাযত সহজ হৈ থকা নাই ।
জু঵ানর চকু঵ে-মুখে য়ন্ত্রণা । এই য়ন্ত্রণা কিমান
ভযানক তাক তেওঁর মুখ নেদেখিলে ধরিব নো঵ারি ।</p>

<p>চেনরো অ঵স্থা তথৈবচ । তেওঁর ঘরত ঘৈণী~
যেক আরু এটা ল'রা । ঘৈণীযেকে কাম করে ।
তেওঁ এখন কলেজর উপাধ্য়ক্ষ । তে঵োঁ ঘরর
খবর পো঵া নাই । তেওঁর মুখখন কিন্তু নির্বিকার ।
ভিতরত জুই জ্঵লিছে য়দিও বাহিরখন শান্ত ।</p>

<p>আমার সকলোকেইজন সদস্য়ই বাহিরলৈ
ওলাই য়াবলৈ আরু ভ্রমণ করিবলৈ ইচ্ছা প্রকাশ
করিলে । তেওঁলোকর মনোভা঵ বুজি পাই চেনক
ক'লো , "" মই ভারতর রাজদূতর লগত আলোচনা
করি তেওঁকে এটা সিদ্ধান্ত ল'বলৈ কম । কারণ
এই ভ্রমণর উদ্য়োক্তা দুই দেশর চরকার । দুই পক্ষই
আলোচনা করি য়দি আমাক য়াবলৈ কয , তেন্তে
আমিও উভতি য়াম । ""</p>

<p>আমার অনুরোধ পেলাব নো঵ারি চেনে
সেইদিনা দুখন টেক্সি য়োগার করিলে । মই
উঠা টেক্সিখনর ড্রাইভারজনর নাম মিঃ হান ।
কম বযসীযা । কিন্তু গা-গারি বিশাল , ওখহে
কম । টেক্সিখন চরকারর । হানে কুরি বছর
ধরি টেক্সিখন চলাইছে ।</p>

<p>আমি টেক্সিরে ` চামার পেলেচ ' চাবলৈ
গ'লো । 1984 চনত আহোতেও এই রাজপ্রাসাদ
চাইছিলো । সেইবার সোমাইছিলো মুখর দু঵ারেদি ।
এইবার সোমাব লগা হ'ল পিছ দু঵ারেদি ।
তিযেনআনমান স্কো঵ারত হো঵া সামরিক অভি~
য়ানর কারণেই এনে করিবলগা হ'ল । এই
প্রাসাদটো 	` নিষিদ্ধ মহানগর 'ক অংশ বিশেষ ।
নিষিদ্ধ মহানগরর ঘাই অংশ হ'ল চীন সম্রাটর
বিশাল প্রাসাদটো । প্রকৃত পক্ষে পুরণি সম্রাট~
সকলর দিনতহে ই নিষিদ্ধ মহানগর বুলি জনাজাত
হৈছিল । সম্রাটসকলর শাসন অ঵সান ঘটার পিছত
এই সুবৃহত্ প্রাসাদটো সংগ্রহালযলৈ রপান্তরিত
হয । তাত অগণন য়াত্রীর ভীর । য়ো঵াবার আহোঁতে
চীনর বহুত সুখী পরিযালক ভ্রমণরত অ঵স্থাত
লগ পাইছিলো । সেই দৃশ্য় বেইজিং মহানগরর
চাইকেল চালকসকলর দৃশ্য়র দরেই মনোরম ।
কিন্তু আজি সেই জনাকীর্ণ প্রাসাদ মরিশালীর
দরে জঁয পরি আছে । প্রাণহীন । প্রাসাদটো
য়েন বিধ঵া ! বরষুণ হৈ এই নিরানন্দক আরু
করুণ করি পেলাইছিল ।</p>

<p>আমি গোটেই প্রাসাদটো চাব নো঵ারিলো ।
জহকালির প্রাসাদর সরহভাগ কর্মচারী কামলৈ
অহা নাই । এই মহা শোকর দিনতো কিন্তু চাফাই
কর্মচারীসকল আহিছে । মহা শোকর বেজ্বেজনিত
অন্তর দেই-পুরি মরিলেও এই মানুহখিনির কর্তব্য়~
বোধ কিন্তু জাগ্রত হৈ আছে । এই দৃশ্য় দেখি
ভাল লাগিল ।</p>

<p>সেইদিনা চারিআলি কিছুমানত পোরা
সামরিক ট্রাক , পরিত্য়ক্ত বেরিকেড আরু মানুহর
জুম দেখিছিলো । কিন্তু সেইবিলাক দেখি
ঘটনার আঁতি-গুরি একো বিচারি পো঵া নাছিলো ।
4 জুনর দিনা ভারতীয রাজদূত রঙ্গনাথনে আমাক
হোটেলত লগ পাবলৈ আহিছিল । তেওঁর লগত
আলোচনা করোতে তে঵োঁ আমাক ভ্রমণ-সূচী~
মতে কাম করি য়াবলৈ উপদেশ দিলে । তেওঁ
নিজর ফালর পরা ভারতীযসকলর চীন পরিত্য়াগর
কথা তেতিযাও ভবা নাছিল । ঘটনার সবিশেষ
বোধহয তে঵োঁ ভালকৈ পো঵া নাছিল । দূতা~
বাসর মানুহে মাথোন আমাক কৈছিল তিযেন~
আনমান স্কো঵ারর আশে-পাশে গুলীযাগুলী চলি
আছে । এই গুলীযাগুলি কার কার ভিতরত
চলিছিল তাক তেওঁলোকে ঠা঵র করিব পরা
নাছিল । সেনাবাহিনীর লোকর ভিতরত
গুলিযাগুলি হো঵ার সম্ভা঵নাও তেওঁলোকে নুই
করা নাছিল । এই গুলীযাগুলির স্঵রূপ জনা
আমার পক্ষে সম্ভ঵ নাছিল ।</p>

<p>আমি শুনিছিলো তিযেনআনমানত ছাত্র~
সকলক আক্রমণ করা সেনাবাহিনীটো হ'ল
27 নং সেনাবাহিনী । বেইজিং সেনা~
বাহিনীযে ছাত্রসকলক গুলীযাবলৈ দ্঵িধা বোধ
করিলে । চীনর ` জীনমুক্তি সেনা 'র 27 নং টো঵ে
ছাত্রসকলক গুলীযাবলৈ সন্মত হ'ল , অথচ
38 নং বাহিনী গুলীযাবলৈ সন্মত নহ'ল -
এই কথা আমি কেইবাজনরো মুখত শুনিবলৈ
পাইছিলো । ফলত মোর মনত ভাব হ'ল য়ে
চীন দেশ গৃহ-বিপ্ল঵র পথেদি আগ বাঢ়িছে ।
হোটেলত এটা কথা বরকৈ শুনা গৈছিল । //
প্রকাশ । চতুর্দশ বছর । দ্঵াদশ সংখ্য়া ।
চীনত কি দেখিলো
চারি জুনর গধূলি এটা কথা মোর চকুত
স্পষ্ট হৈ পরিল । বেইজিঙর অ঵স্থা অশান্ত ।
বাছ , টেক্সি , দোকান , খবর কাগজ বন্ধ । বিদেশী
ভ্রমণকারীসকল ঘরর ভিতরত । হোটেলত খাদ্য়-~
বস্তুর নাটনির আশংকা । বেইজিঙর বাহিরর
নগরবোরতো গণ্ডগোল । কাজেই আমার ভ্রমণ~
সূচী য়িকোনো মুহূর্ততে বাতিল হ'ব পারে ।
য়িকোনো মুহূর্তত আমি প্লেনত উঠি ঘরর ল'রা
ঘরলৈ উভতি য়াব লগা হ'ব । কিন্তু আমার
রাজদূতে য়েতিযালৈকে আমাক য়াবলৈ নকয
তেতিযালৈকে আমি নেয়াওঁ বুলি দৃঢ়প্রতিজ্ঞ ।</p>

<p>সেইদিনা গধূলি ইউ.আর. অনন্তমূর্তি ,
তেণ্ডুলকর আরু মৃণাল পাণ্ডে ফুরিবলৈ ওলাই গৈ
বেইজিং বিশ্঵বিদ্য়ালযর কাষর এটা হোটেলত
নামিলগৈ । সেই হোটেলত এজন ভারতীয লোক
থাকে । তাতে গৈ তেওঁলোকে নতুন তথ্য়র
অন্঵েষণ করিলে ।</p>

<p>নতুন তথ্য়র ভিতরত পো঵া গ'ল বি.বি. চির
সংবাদ । য়ুক্তরাষ্ট্রই চীনা চরকারক অস্ত্র বিক্রী
করা বন্ধ করি দিছে । ছোভিযেত রুছিযাই সৈনিক
অভিয়ানটোক চীনর ঘরু঵া বিষয বুলি অনুভ঵
করিছে । ফ্রান্সে আরু বৃটেইনে দুখ প্রকাশ
করিছে । আরু দুই-এটা খবর তেওঁলোকে কঢ়িযাই
আনিছিল । 3-4 জুনর সৈনিক আক্রমণত 2600 র
পরা 3000 মানুহ মরিছে বুলি বহুতে অনুমান
করিছে ।</p>

<p>এইবিলাক সঁচাকৈযে ভযানক ঘটনা ।
রাতিলৈ তেণ্ডুলকারে টেলিভিশ্য়ন চাই এটি
অসাধারণ দৃশ্য় দেখা পালে । ক'লা পোছাক
পিন্ধা এজন খবর পঢ়োতাই এক মিনিটর
বাবে তিযেনআনমানর সৈনিক আক্রমণর ছবি
দেখু঵াইছিল । কে঵ল এক মিনিটর বাবে । এই
ক'লা পোছাক পরিহিত পঢ়োতাজনে হেনো
কৈছিল : "" বর্বর আক্রমণত হেজার-বিজার নির্দোষী
নাগরিকর মৃত্য়ু হ'ল । "" চীনা ভাষা তেণ্ডুলকরে
নেজানে । কিন্তু বেজিঙত থকা এজন ভারতীয
লোকে তেওঁক চীনা কথাবোরর অর্থ ভাঙি দিছিল ।
এই খবর শুনি আমি আচরিত হ'লো । এক
মিনিটর খবরটো নিশ্চয সাংবাদিকে দিছিল । ইমান
মানুহ মরা খবর সেই এবারেই দিলে । পিছত
য়িবোর খবর টি. ভিযে দিছিল , সেইবোর এক~
পক্ষীয চরকারী খবর ।</p>

<p>চরকারে ইমানবোর মানুহ মরিছে বুলি স্঵ীকার
করা নাই । তেওঁলোকর মতে মুঠেই তিনিশ
মানুহরহে জী঵ন নাশ হৈছে , আরু সাত হেজার
লোক ঘুণীযা হৈছে । কিন্তু চরকারে দূরদর্শনত
তিযেনআনমানর হত্য়াকাণ্ডর দৃশ্য় নেদেখুও঵াটো
ইযার ডাঙর দুর্বলতা । চরকারী তথ্য়ত আমি
সম্পূর্ণ আস্থা স্থাপন করিব পরা নাছিলো । অসম
আন্দোলনর সমযত আমার মানুহে চরকারী তথ্য়~
পাতিত য়িদরে আস্থা হেরু঵াইছিল , ইযাতো মানুহে
সেইদরে চরকারর তথ্য়ত আস্থা হেরু঵াইছিল ।
ইযার মানুহে বি.বি.চির খবর শুনে বুলি জানি
মই একরকম আচরিতেই হৈছিলো । সমাজবাদী
খবর ব্য়঵স্থাত মত প্রকাশ আরু জ্ঞাপনর ক্ষমতা
পুঁজিবাদী খবর-ব্য়঵স্থাতকৈ অধিক লোকহিতকর
রপত প্রকাশ পাব লাগে । ` বেইজিং রিভিউ 'র
মে মাহর সংখ্য়া ( 22-28 ) এটাত এনে এটা
সম্পাদকীযই পঢ়িছিলো । খবর ঢাকিবলৈ গ'লে
খবর কাগজ বা রেডিঅ'র বিশ্঵স্ততা কমে । উক্ত
সম্পাদকীযত এটা কথা কো঵া হৈছে । দুর্নীতি
রোধ করিবলৈ প্রকাশর স্঵াধীনতা আ঵শ্য়ক ।
এইটো ডাঙর য়ুক্তি । ছাত্র-আন্দোলনর নেতা~
সকলে আরু এটা দাবী করিছিল । তেওঁলোকর
লগত কর্তৃপক্ষই করা আলোচনা-বিলোচনাবোরর
কথা কাকতত প্রকাশ পাব লাগে , রেডিঅ'-~
দূরদর্শনত ওলাব লাগে । তেতিযা রাইজে দাবী~
বোরর শুদ্ধাশুদ্ধ বিবেচনা করিবলৈ সমর্থ হ'ব ।
মোর বোধেরে সরু হ'লে এই কামে চীনর বাতরি
ব্য়঵স্থাক অধিক আরু আ঵শ্য়কীয স্঵াধীনতা দিযার
পথ মুকলি করিলেহেঁতেন । তিযেনআনমানত ঘটা
3-4 জুনর সৈনিক আক্রমণর খবরো বাতরি-~
ব্য়঵স্থাত প্রকাশ পাব লাগিছিল । তেতিযা চীনা
রাইজে তার য়ুক্তিয়ুক্ততার কথা বিবেচনা করিব
পারিলেহেঁতেন । তেতিযা তেওঁলোকে বি.বি.চি
শুনিবলৈ নগ'লহেঁতেন ।</p>

<p>আচল কথা সত্য়ক জনা । চীনদেশর সত্য়
জানিবলৈ অসমত কোনো অনুষ্ঠান নাই । ভারততো
চীনতত্ত্঵বিদর সংখ্য়া বর কম । To seek truth from
facts । কথাষার মাও চে টুঙর । Fact মানে ঘটনাও
হয , প্রকৃত অ঵স্থাও হয , Fact মানে তথ্য়ও ।
আচল ঘটনা , অ঵স্থা , বা তথ্য় বুজি বিভিন্ন ঘটনা ,
অ঵স্থা আরু আভ্য়ন্তরীণ সম্পর্কর স্঵রূপ অধ্য়যন
করাই সত্য় অন্঵েষণ করা ।</p>

<p>আজি এই বস্তুটোরেই অসমর তথা ভারতর
বহুত বুদ্ধিজী঵ীর মাজত অভা঵ । অহরহ সত্য়
অন্঵েষণর অভ্য়াস নেথাকিলে তাক আ঵িষ্কার করা
টান । চীনা ভাষা , বুরঞ্জী , সমাজ , রাজনীতি ,
কলা , বিজ্ঞান আরু সাহিত্য়র জ্ঞান মোর নাই ।
সেইবাবে বেইজিঙর ছাত্র-আন্দোলনর স্঵রূপ সেই
জ্ঞানর আলোকত আ঵িষ্কার করা মোর পক্ষে
অসম্ভ঵ । এই কারণে চীনা বুদ্ধিজী঵ীসকলর মতা~
মতর ওপরত বর্তি সত্য় আ঵িষ্কার করিব লাগিব ।
সাংবাদিকসকলে দিযা তথ্য়বোর অমূল্য় । অকল
চীনা সাংবাদিকেই নহয , ভারত আরু অন্য়ান্য়
দেশর সাংবাদিকর তথ্য়ও অমূল্য় । কারণ তেওঁ~
লোকেই অতি ওচরর পরা তিযেনআনমান স্কো঵ারর
ঘটনা঵লীর তথ্য় আমাক দিছে । //
এই তথ্য়বোরর পরা অনুমান হয 15 এপ্রিল
তারিখর পরা জুনর 3 তারিখলৈকে ছাত্রসকলর
আন্দোলন শান্তিপূর্ণ আছিল । স্কো঵ারর আশে-~
পাশে সামরিক আইন জারি আছিল য়দিও
জনতাই তালৈ কাণষার করা নাছিল । প্রায
এপষেক কাল ধরি সেনাবাহিনী আরু জনতার
মাজত তর্কাতর্কি হৈ আছিল । সেনাসকল
স্কো঵ারলৈ আহিব খোজে , কিন্তু রাইজে তেওঁ~
লোকক বুজাই-বঢ়াই পঠিযাই দিযে । দেখা য়ায
সেনাসকলর বহুতেই নিরস্ত্র আরু আন্দোলনকারী
ছাত্রসকলক বলেরে তিযেনআনমেন স্কো঵ারর পরা
আঁতরো঵ার কথা চিন্তা করিবলৈ টান পাইছিল ।
জনমুক্তি সেনার সদস্য়সকলর রাজনৈতিক চেতনাও
প্রখর । হিংসার বিরুদ্ধে হিংসা প্রযোগ করিলে
শু঵ায । কমিউনিষ্ট দলর ভিতরত এটা দলে
ছাত্রসকলর দাবীবোর সমর্থন করিছিল । তার
ভিতরত চীন কমিউনিষ্ট দলর সম্পাদক জাও
জিযাঙর নাম উল্লেয়োগ্য় । জাও জিযাঙে অর্থ~
নৈতিক সংস্কারসমূহর দ্রুত অগ্রগতির অর্থে রাজ~
নৈতিক সংস্কার করাটো আ঵শ্য়ক বুলি বিবেচনা
করিছিল । এইকারণে তেওঁ রাষ্ট্রীয পরিষদক
আরু পলিট বুরোক ছাত্রসকলর লগত আলোচনা
করি প্রকাশনর স্঵াধীনতা আদি দাবীবোরর এটা
সুমীমাংসা করিবলৈ কৈছিল য়েন লাগে ।</p>

<p>কিন্তু 3 জুনর দিনাখন জাও জিযাঙর উপদল~
টোর স্থিতি কি আছিল তাক ক'ব নো঵ারিলে ।
তেওঁর রাজনৈতিক প্রভা঵ ইতিমধ্য়ে টুটি গৈছিল ,
বহুতরে অনুমান । 20 জুনত রাষ্ট্রীয জন কংগ্রেছর
সপ্তম অধিবেশন বহার কথা আছিল । ` বেইজিং
রিভিউ 'র মতে এই অধিবেশনত জনমত প্রদর্শন
আরু বাতরি কাকত সম্পর্কীয দুখন আইনর কথা
আলোচনা করিবলগীযা আছিল । জাও জিযাঙে
ইতিমধ্য়ে বেইজিঙলৈ অহা তুরস্কর সামাজিক~
গণতান্ত্রিক জনপ্রিয দলর প্রতিনিধিবর্গক কৈছিল
য়ে "" চীন দেশে অর্থনৈতিক সংস্কারর কাম আগ~
বঢ়াযেই ক্ষান্ত নেথাকে , ই রাজনৈতিক ব্য়঵স্থার
পুনরগঠনরো দিহা করিব । অর্থনৈতিক আরু রাজ~
নৈতিক সংস্কার দুযোটাই পারস্পরিক নির্ভরশীল
হৈ চলিব লাগিব । "" বিক্ষোভকারী ছাত্রসকলেও
রেডিঅ'ষ্টেশ্য়ন , দূরদর্শন , জিন্হু঵া বাতরি প্রতি~
ষ্ঠান , পিপল্ছ ডেইলি , গু঵াং মিং ডেইলি আরু
কমিউনিষ্ট দলর প্রচার শাখার দপ্তরত গৈ বাতরি
কাকতর স্঵াধীনতা দিবলৈ আরু দল তথা
চরকারর নেতাসকলর সৈতে কথা-বতরা পতার
সুয়োগ বিচারি আবেদন দিছিল । ছাত্রসকলে
এইবিলাক দাবী পূরণ হ'ব বুলি আশা করিছিল ।
 +&gt;*
",0
</p></body></text></cesDoc>