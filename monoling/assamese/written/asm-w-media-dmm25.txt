<cesDoc id="asm-w-media-dmm25" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-media-dmm25.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>প্রান্তি</h.title>
<h.author>ভবেন্দ্র</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1990</pubDate>
</imprint>
<idno type="CIIL code">dmm25</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0313.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>বিশ্঵নাথ প্রতাপ সিঙর প্রথম 50টা দিন
প্রধান মন্ত্রী বিশ্঵নাথ প্রতাপ সিঙর প্রথম
50টা দিন কিছু আশাপ্রদ হ'লেও
সামঞ্জস্য়হীন কাম-কাজেরে ভরপূর । তেওঁ
লো঵া কিছুমান ব্য়঵স্থা আরু ঘোষণা
উত্্সাহজনক হৈছে । সেই ব্য়঵স্থা আরু
ঘোষণাবোরে সেইবোরর উদ্দেশ্য়র
একনিষ্ঠাকো প্রতিফলিত করিছে । তথাপি
সেইবোর সুনির্দিষ্ট বা স্পষ্ট নহয । ভ঵িষ্য়ত
ভারতর কেনে এক প্রতিচিত্র তেওঁ মনত
পুহি রাখিছে সেইটো স্পষ্ট নহয ।</p>

<p>মন্ত্রীসভা বা ইযার উপসমিতিত হো঵া
আলোচনা-বিলোচনাবোর ফলপ্রসূ হৈছে ।
কিন্তু মন্ত্রীসকলর মাজত আলোচনা-
বিলোচনা খুব কম হয । ভি. পি. সিঙে কারো
কামত হস্তক্ষেপ নকরে ; প্রতিজন মন্ত্রীযেই
কম-বেছি পরিমাণে এক মুক্ত প্রতিনিধি ।
ইফালে শ্রীসিঙে নিজকে নিজর কামতেই
আবদ্ধ রাখে । এজন আগশারীর মন্ত্রীর
ভাষাত , """" কারোবার সৈতে আলোচনা
করার অভ্য়াস বিশ্঵নাথ প্রতাপ সিঙর
নাই । """"</p>

<p>সেযেহে য়িবোর সিদ্ধান্ত লো঵া হৈছে
সেইবোরত য়থেষ্ট প্রভা঵শালী য়েন লগা
বিষযাসকলর ছাপ স্পষ্ট । রাষ্ট্রীয বা আন
কামকাজর দিশত পরামর্শ দিবলৈ চরকারর
বাহিরত কোনো সংস্থা নাই । এনে পরামর্শ
আগেযে গোপী আরোরাই দিছিল ; বর্তমান
বিনোদ পাণ্ডেই দিযে । প্রশাসনীয সে঵ার
নিয়ুক্তিবোরর ক্ষেত্রতো কম-বেছি পরিমাণে
একে ঘটনাই ঘটিছে । রাজ্য়সমূহলৈ বিষযা
সকলক অদল-বদল করা কামটোর ফল
খুম কমহে পো঵া য়ায । প্রকৃত ব্য়াধিটো
হ'ল - য়ো঵া বহু বছর ধরি রাজনীতি~
বিদসকলে এই বিষযাসকলক আগতীযা
বেতন বৃদ্ধি আরু পদোন্নতির প্রলোভন দি
থকার কারণে জনসাধারণক আকর্ষিত
করিব পরা মূল্য়বোধ আরু তত্ত্঵গত
বিবেচনাবোর উ঵লি গৈছে ।</p>

<p>এই প্রক্রিযাটোর পরিবর্তন সধা বা
প্রতিটো ক্ষেত্রতে নীতি-নিযম আরু
মূল্য়বোধ পুনর ঘূরাই অনাটো঵েই হৈছে
সমস্য়া । এইক্ষেত্রত স্থিতা঵স্থা বাহাল থকা
কথাটো঵ে দেশখনর সমস্য়াবোরর সমাধান
করিব নো঵ারে । বিশ্঵নাথ প্রতাপ সিঙে
নিজেও বিভিন্ন উপাযেরে এই অ঵স্থাটো
আঙুলিযাই দিছে । তথাপি জনশক্তিক
সংগঠিত করাত সহায করিবলৈ তেওঁ একো
করা নাই । এনে জনশক্তিযে চরকারক
আওবাটে য়ো঵ার পরা বিরত করার ক্ষেত্রত
প্রভা঵ পেলার পরা শক্তি হিচাপে কাম
করিব পারে । আনকি শাসনাধিষ্ঠ জনতা
দলটোও মূলতঃ কাগজে-কলমেহে আছে ;
দেশর সত্্ আরু কর্মক্ষম লোকসকলক
একত্রিত করার প্রতি প্রধান মন্ত্রী গরাকী
কিমান আগ্রহী সেইকথা কর্মক্ষেত্রত
দলটোক সংগঠিত করার পদ্ধতিযে প্রমাণ
করিব ।</p>

<p>রাজী঵ গান্ধীর চরকারে উদ্দেশ্য়পূর্ণ~
ভা঵ে জটিল করি পেলো঵া কাশ্মীর আরু
পঞ্জাব সমস্য়ার ক্ষেত্রত তেওঁ চলো঵া
প্রচেষ্টাই এই সমস্য়া দুটার সন্মুখীন হো঵ার
অর্থে নীতিত বিশ্঵াসী লোকসকলক কামত
লগাবলৈ থকা তেওঁর আগ্রহক প্রতিফলিত
করিছে । উগ্রপন্থীসকলর সৈতে পোনপটীযা
আলোচনার তেওঁ পোষকতা করে । আরু
পঞ্জাবর ক্ষেত্রত তেওঁ ঘোষণা করা
রেহাইবোর য়ুক্তিয়ুক্ত হৈছে । কিন্তু
রাজনীতির ক্ষেত্রত য়েনেকৈ এক ভিত্তি
থাকে ঠিক তেনেকৈ প্রশাসনর ক্ষেত্রতো এক
ভিত্তি থাকে । তেওঁ সমন্঵য আরু সততার
বার্তা বহন করিছে , কিন্তু কোনো ক্ষেত্রত
সীমার বাহিরলৈ গ'লে য়ে বিপরীত ফল দিব
পারে এই কথাটো তেওঁ বুজাবলৈ সক্ষম
হো঵া নাই । জটিল সমস্য়াবোর য়ে তেওঁ
এরাই চলা নাই সেইটো উল্লেখয়োগ্য় ।
এইক্ষেত্রত তেওঁক মান঵তার দিশটোহে
স্পষ্টভা঵ে প্রতিফলিত হৈছে , কঠোরতার
দিশটো প্রতিফলিত হো঵া নাই ।</p>

<p>য়িযেই নহওক প্রধান মন্ত্রী হিচাপে এই
50টা দিনত বিশ্঵নাথ প্রতাপ সিঙে নিজর
শ্রেষ্ঠত্঵ প্রতিপন্ন করিছে । চরকারত অথবা
তেওঁ নেতৃত্঵ করা দলটোত তেওঁ কোনো
প্রত্য়াহ্঵ানর সন্মুখীন হো঵া নাই ।
আচলতে প্রধান মন্ত্রীর পদ আরু দলর
সভাপতি এই দুযোটা পদ একে সমযতে
অধিকার করিবলৈ তেওঁকেই প্রথম
আনুষ্ঠানিকভা঵ে অনুরোধ জনো঵া হৈছিল ।
এসমযত তেওঁক প্রত্য়াহ্঵ান জনো঵া উপ~
প্রধান মন্ত্রী দেবীলালে দ্঵িতীয স্থানত থাকি
সান্ত্঵ না লভিছে । আন এজন দাবীদার
চন্দ্রশেখরে এতিযাও ঠেহ পাতি আছে য়দিও
তে঵োঁ দলটোর আধা ডজনর বেছি সংসদ
সদস্য়র সমর্থনেই আদায করিব পরা নাই ।</p>

<p>পূর্বর লোকদলর সদস্য়সকলর
( এসমযত দেবীলাল , উদ্য়োগ মন্ত্রী অজিত
সিং আরু বস্ত্র মন্ত্রী শারদ য়াদব দলটোর
সদস্য় আছিল ) মনত তেওঁলোকর
ফৈদটোক উল্লেখয়োগ্য় বিভাগবোর পরা
বঞ্চিত করা হৈছে বুলি এক ধারণাই দেখা
দিছে । উদাহরণ স্঵রূপে প্রতিরক্ষা দপ্তরটো
বিশ্঵নাথ প্রতাপ সিঙে নিজে রাখিছে ।
রাজী঵ গান্ধীর কংগ্রেছ দলর পরা পৃথক হৈ
অহা জন মর্চাত তেওঁর সহয়োগীরূপে থকা
মুফটি মহম্মদ ছৈযদ হ'ল গৃহমন্ত্রী । আরু
বৈদেশিক পরিক্রমার দাযিত্঵ত আছে
বিশ্঵নাথ প্রতাপ সিঙর ঘনিষ্ঠ বন্ধু ইন্দর
কুমার গুজরাল । অ঵শ্য়ে পুরণি লোকদলর
সদস্য়সকলে অ঵হেলিত বুলি য়ি অনুভ঵
করিছে সেই ধারণা তেওঁলোকে ব্য়ক্তিগত~
ভা঵েহে প্রকাশ করিছে ; তাতকৈ বেছি
একো করা নাই । য়ুটীযাভা঵ে কার্য়্য়ব্য়঵স্থা
গ্রহথণ করাটো সম্ভ঵ নহযো , কারণ
দেবীলাল আরু অজিত সিঙর মাজত অহি
নকুল সম্পর্ক । ভি. পি. সিঙে এইকথা
জানে ।</p>

<p>কাগজে-কলমে চরকারখন চলো঵া
বহুদলর এক সমন্঵য রাষ্ট্রীয মর্চার
প্রাসংগিকতা নোহো঵া হৈছে । মর্চার প্রধান
অংশীদার তেলুগু দেশমে অন্ধ্র প্রদেশর
পরাজযর পাছত নিজর মত প্রকাশর
অধিকার হেরু঵াইছে । মর্চারো প্রধান হৈ
থকা তেলুগু দেশমর সভাপতিগরাকীযে
রাষ্ট্রীয মর্চার অধ্য়ক্ষপদটো বিশ্঵নাথ প্রতাপ
সিঙলৈ আগবঢ়াইছেই ।</p>

<p>বিশ্঵নাথ প্রতাপ সিঙর প্রতি
কেতিযাবা কিবা প্রত্য়াহ্঵ান আহিলে
বাহিরর পরাহে আহিব । বাহিরর পরা
তেওঁর চরকারখনক সমর্থন দি থকা
সোঁপন্থী ভারতীয জনতা পার্টি ( BJP )
আরু বাঁওপন্থী কমিউনিষ্টসকলে এদিন
তেওঁলোকর সমর্থন প্রত্য়াহার করিব
পারে । লোকসভাত ( ইযার বর্তমানর শক্তি
526 ) জনতা দলর সদস্য় হ'ল 143 জন ।
বিজেপির আছে 88 জন আরু কমিউনিষ্ট
সকলর 55 জন । এই দুটা পক্ষর কোনোবা
এটাই সমর্থন প্রত্য়াহার করিলেও জনতা
দলর চরকারখনর পতন ঘটিব ।</p>

<p>কিন্তু দুযোটা দলেই কংগ্রেছ দলর
বিরোধিতা করে । কংগ্রেছ দলক
ভোটদাতাসকলে প্রত্য়াখান করিছে বুলি
তেওঁলোকে আন্তরিকতারে বিশ্঵াস করে ।
অন্য় দিশর পরাও তেওঁলোকর বাবে আন
কোনো বিকল্প নাই । তেওঁলোকর স্঵ার্থর
সৈতে জনতা দলর স্঵ার্থর মিল আছে ।
জনতা দলে নিজর নীতির ক্ষেত্রত বাঁও বা
সোঁ কোনো এটা পন্থাই অ঵লম্বন নকরি এক
মধ্য়মপন্থী অ঵লম্বন করিছে । বিশ্঵নাথ
প্রতাপ সিঙর অধ্য়ক্ষতাত তেওঁলোকে
পঞ্জাবত দুখন য়ুটীযা সভা পাতিলে ।
সংসদর য়ো঵া 10 দিনীযা অধিবেশনত
বিজেপি আরু কমিউনিষ্ট সদস্য়সকল
বিশ্঵নাথ প্রতাপ সিং চরকারখন রক্ষা
করিবলৈ আগবাঢ়ি আহিছিল ।</p>

<p>বিজেপিযে চরকারত য়োগ দিব
খোজে । কিন্তু কমিউনিষ্টসকলর
বিরোধিতার বাবেহে তেওঁলোকে য়োগদান
করিব পরা নাই । বিজেপিক চরকারত
অন্তর্ভুক্ত করিলে তেওংলোকর সমর্থন
প্রত্য়াহার করিব বুলি কমিউনিষ্টসকলে ভি.
পি. সিঙর স্পষ্ট করি দিছ । আনহাতে
ভি. পি. সিঙর কিছুমান ঘোষণা লৈও
বিজেপি অসন্তুষ্ট । সেই ঘোষণাবোর
ইমানেই """" ধর্মনিরপেক্ষ """" য়ে তেওঁলোকে
হজম করিব পরা নাই । এটা সমযত
বিজেপিযে দেবীলালর ওপরত সম্পূর্ণ
ভারসা করিছিল । কিন্তু দেবীলাল য়েতিযা
ভি. পি. সিঙর নেতৃত্঵ মানি ললে তেতিযা
বিজেপির পক্ষে ঘটনা-প্রবাহে য়েনেদরে
গতি লৈছিল ঠিক তেনেদরে তাক মানি
লো঵ার বাহিরে গত্য়ন্তর নাছিল ।</p>

<p>বিহার , গুজরাট , হিমাচল প্রদেশ , মধ্য়
প্রদেশ , উরিষ্য়া , মহারাষ্ট্র , রাজস্থান আরু
দিল্লী - এই আঠখন রাজ্য়র আগন্তুক
বিধান সভার নির্বাচনর বাবে আসন
ভাগো঵ার কথাটো লৈ বিজেপি আরু জনতা
দলর মাজত কিছু মতপার্থক্য়র সৃষ্টি হো঵া
য়েন লাগে । জনতা দলর ঘাটি বিহার আরু
উরিষ্য়াত কোনো বিবাদ হো঵া নাই ।
বিজেপিযে নিযন্ত্রণ করা মধ্য় প্রদেশ আরু
হিমাচল প্রদেশতো কোনো মতপার্থক্য়র
সৃষ্টি হো঵া নাই ।</p>

<p>মহারাষ্ট্রতো কোনো সমস্য়ার সৃষ্টি
হো঵া নাই । কারণ তাত বিজেপিযে
অকলেই প্রতিদ্঵ন্দ্঵িতা করিব । তাত
তেওঁলোকে জনতা দলে বেযা পো঵া উগ্র
প্রাদেশিকতাবাদী দল শি঵সেনার সৈতেহে
মিত্রতাত আবদ্ধ হৈছে । কিন্তু রাজস্থান
আরু গুজরাটত কঠিন সমস্য়ার সৃষ্টি হ'ব
পারে । জনতা দল আরু বিজেপি - দুযোটা
দলেই তাত গরিষ্ঠসংখ্য়ক আসন দাবী
করিছে । বিজেপির সৈতে দেবীলালর
কেইখনমান বৈঠক অনুষ্ঠিত হৈছিল ।
আসনর ক্ষেত্রত বুজাবুজিত উপনীত হ'ব
পরা য়ায বুলি তেওঁ সম্পূর্ণ আশাবাদী ।</p>

<p>জনতা দল আরু বিজেপির প্রার্থী-
সকলর মাজত লোকসভার নির্বাচনর
সমযত হো঵ার দরে কিছু """" বন্ধুত্঵পূর্ণ
প্রতিদ্঵ন্দ্঵িতা """" হ'ব পারে । পিছে বিধান
সভার নির্বাচনর পাছতহে হঠাতে
মতপার্থক্য় উদ্ভ঵ হ'ব পারে । মধ্য় প্রদেশ ,
হিমাচল প্রদেশ আরু দিল্লীত নিজর চরকার
গঠন করি লব পারিলে বিজেপিযে দলর
সদস্য়সকলর মন্ত্রীসভাত লবলৈ বিশ্঵নাথ
প্রতাপ সিঙর ওপরত হেঁচা দিবই ।</p>

<p>এই কথা অনুভ঵ করিযেই ইতিমধ্য়ে
বিজেপি আরু কমিউনিষ্ট নেতাসকলক
নিজর ঘরলৈ আনন্ত্রণ জনাই ভি. পি. সিঙে
এক ভোজমেল কূটনীতিত লিপ্ত হ'বলৈ
আরম্ভ করিছে । দুযোটা পক্ষকে চরকারলৈ
আনিবলৈ তেওঁ চেষ্টা করি আছে । য়ো঵া
কেইদিনত তেওঁ য়েনে এক আস্থার
প্রতিচ্ছবি দাঙি ধরিছে তার পরা তেওঁ য়ে
এনে এটা জোটো সম্ভ঵ করি তুলিব পারে
তারেই ইংগিত পো঵া য়ায ।</p>

<p>পরিচ্ছন্নতাই হ'ল বিশ্঵নাথ প্রতাপ
সিঙর ঘাই শক্তি । নিজর কর্মপন্থার ক্ষেত্রত
রাজী঵ গান্ধী আরু তেওঁর মাক ইন্দিযা গান্ধী
য়েনেদরে বিপথগামী হৈছিল , ঠিক
তেনেদরে তেওঁ বিপথগামী হো঵া নাই ।
সেযেহে ভি. পি. সিং ভালকৈযে গ্রহণয়োগ্য়
হৈছে । তেওঁ এক আস্থার ভাব জগাই
তুলিছে । ই জনসাধারণকো সুখী করি
তুলিছে । রাজনৈতিক দলবোরে নিজা
বিবেচনারে কার্য়্য়নির্বাহ করে , কিন্তু
তেওঁলোকেও ভি. পি. সিঙে কোনো
অনৈতিক কাম নকরিব বুলি ভাবে য়েন
লাগে ।</p>

<p>পিছে এখন মুক্ত চরকার আগবঢ়ার
বুলি ভি. পি. সিঙে য়ি প্রতিশ্রুতি দিছিল সেই
প্রতিশ্রুতিহে পূরণ নহ'ব য়েন লাগে ।
বিষযাসকলে তথ্য়পাতি য়োগান ধরিবলৈ
আগতে য়েনেদরে অপারগতা প্রকাশ করি
আছিল এতিযাও তেনে ধরণর অপারগতাই
প্রকাশ করি আছে । নীতি-নীযমবোরো
আগর দরে আওপকীযা হৈযেই আছে ।
চরকারী বিষযাসকলে গোপনীযতা রক্ষা
করার ক্ষেত্রত এক ন্য়স্ত স্঵ার্থ গঢ়ি তুলিছে ,
কারণ এনে ধরণর গোপনীযতার বাবে
তেওঁলোকর বেযা কামবোর প্রকাশ
নোপো঵াকৈ থাকে । রাইজর স্঵ার্থর
অনুকূলে নোহো঵া বুলি স্পষ্ট হৈ থকা
একোটা নির্দিষ্ট সিদ্ধান্তত এই বিষযাসকল
কিয আরু কেনেকৈ উপনীত হয সেইটো
ব্য়াখ্য়া করা প্রযোজন ।</p>

<p>রাইজর হেঁচার ফলত উত্তর প্রদেশর
তেহরি বান্ধর নির্মাণ য়েতিযা বন্ধ হৈছিল
তেতিযা সকলো঵েই সুখী হৈছিল । কিন্তু
বান্ধটোর নির্মাণকার্য়্য় পুনর কিয আরম্ভ
হৈছিল তার কোনো ব্য়াখ্য়া দাঙি ধরা হো঵া
নাই । বিশিষ্ট সমাজকর্মী সুন্দরলাল
বহুগুনাই অনশন করার লগতে কেন্দ্রীয
পারিপার্শ্঵িকতা মন্ত্রালযে বান্ধটোর নির্মাণর
ক্ষেত্রত করা বিরোধিতা প্রত্য়াহার করা
নাই । সেযেহে কোনো ব্য়াখ্য়া দাঙি নধরাকৈ
তেওঁলোকর ইচ্ছাক কিয অসন্মান জনো঵া
হ'ল সেইটো জনার অধিকার রাইজর
আছে । নিজর চরকারে করা কামর
জরিযতে ভি. পি. সিঙে রাইজর আস্থা
য়িমানখিনি জগাই তুলিব পারিব তার
ভিত্তিতেই তেওঁ নিজর বা এই ক্ষেত্রত
ভারতর ভ঵িষ্য়ত গঢ়ি তুলিব পারিব । //
অসমত গেছ ক্রেকার স্থাপনর
অর্থনীতি আরু রাজনীতি
ভুবন বরু঵া ।
পশ্চিমবংগ চরকারে হালদিযাত
পাতির খোজা পেট্রো রাসাযনিক
চক্রটো সম্পর্কে দেশর প্রায সকলো
আগশারীর ব্য়ক্তিগত বিনিযোগকারী
আগ্রহী হৈ উঠার খবর সম্প্রতি কাকতে~
পত্রই প্রকাশ পাইছে আরু মুধাফুটা
ব্য়঵সাযী গৃহ আম্বানী , গো঵েংকা , পোদ্দার ,
ছাহ , মিট্টান আদির মালিকসকলে য়ো঵া
কিছুদিন ধরি করিকাতার চরকারী মহলত
ঘূরা-পকা করি আছে বুলি দনা গৈছে ।
পশ্চিমবংগ চরকারে প্রস্তা঵ করা এই
প্রকল্পত সংয়োজিত নাপথা ক্রেকার
গোটটো঵ে বছরি 1.90 লাখ টন ইথিলিন ,
বেনজিন , বুটাডিন , প্রপিলিন আদি সামগ্রী
উত্পাদন করিব আরু তারেই ভিত্তিত
হালদিযার পরা কলিকাতালৈ সমগ্র
পথটোত অসংখ্য় সরু-বর রাসাযনিক তথা
প্লাষ্টিক উদ্য়োগ গঢ় লৈ উঠার কথা ।
ব্য়ক্তিগত উদ্য়োগপতিসকল এনে এটা
প্রকল্প সম্পর্কে আগ্রহী হৈ উঠার কারণ
পশ্চিমবংগর প্রতি অনুরাগ নিশ্চয নহয ,
আচলতে প্রচুর লাভর সম্ভা঵নাপূর্ণ এই
উদ্য়োগহে তেওঁলোকক আকর্ষিত
করিছে । কেইবছরমানর আগেযে
মহারাষ্ট্রর সমীপবর্তী বোম্বে হাইত
প্রাকৃতিক গেছ উদ্ঘাটন হো঵াত তাক
1700 কিল'মিটার দৈর্ঘ্য়র হাজিরা বিজাপুর
পাইপ লাইনেগি বো঵াই নি বৃহত্্ আকারর
গেছ ক্রেকার প্রকল্প স্থাপন করা হৈছিল ।
গেছভিত্তিক উদ্য়োগর ক্ষেত্রত দেশর
ভিতরত সবাতোকৈ অগ্রণী রাজ্য়খন হ'ল
গুজরাট । তাত ইণ্ডিযান পেট্রো কেমিকেলছ
লিমিটেডর উদ্য়োগত বোম্বে-বরোদা পথ ,
বরোদা-আহমেদাবাদ অঞ্চল ভরি পরিছে
অসংখ্য় ভটিযনি উদ্য়োগেরে , আরু তারেই
ফলত রাজ্য়খনর ঔদ্য়োগিক মানচিত্রখনেই
সম্পূর্ণভা঵ে পরিবর্তিত হৈছে । এতিযা প্রশ্ন
হ'ল - অসমত গেছভিত্তিক উদ্য়োগ
স্থাপনর প্রচুর সম্ভা঵না থকা সত্ত্঵েও ,
রাজ্য়খনর কোকরাঝারর পরা ডিগবৈলৈকে
একে ধরণেরে ঔদ্য়োগিক পরিবেশ কিয
পরিলক্ষিত নহ'ল ? এতিযাও আমার মনর
পরা এটা বা দুটা নতুন তেল শোধনাগারর
জরিযতেই রাজ্য়খনর উদ্য়োগ বিকাশ ক্ষিপ্র
হো঵া বুলি ভবার মানসিকতা দূর নহ'ল ।
অথচ রাজ্য়খনত প্রচুরভা঵ে প্রাপ্ত আরু
য়ো঵া তিনিটা দশখত অপরাধমূলকভা঵ে
বিনষ্ট করা অপর্য়্য়াপ্ত পরিমাণর প্রাকৃতিক
গেছে রাজ্য়খনর উদ্য়োগ মানচিত্রকে সলনি
করি নিযোগ সম্ভা঵না রাজ্য়িক আয বৃদ্ধির
লগতে আঞ্চলিক বৈষম্য়কো হ্রাস করিব
পারিলেহেঁতেন । অসমত বার্ষিক 4.75 লাখ
টন ইথিলিন উত্্পাদনর সম্ভা঵না থকা
নাই ; অথচ তার আধাখিনি উত্্পাদনর
সম্ভা঵না থকা হালদিযাত রাজ্য় চরকারর
উদ্য়ম দেখি বিনিযোগ অভিলাষী
পুঁজিপতিযে খদমদম লগাইছে ; কোটি-
কোটি টকা বিনিযোগ করিবলৈ
প্রতিয়োগিতামূলকভা঵ে আগু঵াই
আহিছে ।</p>

<p>এইখিনিতে উল্লেখ করি থব খুজিছোঁ
য়ে অসম চরকারর উদ্য়োগ উন্নযন নিগমে
( AIDC ) 1984 চনতে কেন্দ্রীয চরকারর
পরা রাজ্য়খনর তৈলক্ষেত্রত পো঵া
প্রাকৃতিক গেছর সদব্য়঵হারর বাবে এটা
নাপছা-কাম গেছ ক্রেকার স্থাপনর বাবে
লেটার অ঵ ইনডেণ্টর আবেদন জনাইছিল
ৃয়দিও অসমর প্রতি নতুন দিল্লীর
অমনোয়োগিতা আরু দীর্ঘসূত্রিতামূলক
নীতির ফলত এই প্রস্তা঵র প্রতি য়ো঵া
পাঁচোটা বছরত ইতিবাচক সহারি দেখুউ঵া
নহ'ল । এই প্রকল্পত তুলনামূলকভা঵ে
য়থেষ্ট কম পরিবহম ব্য়যতে , স্থানীযভা঵ে
পো঵া প্রচুর প্রাকৃতিক গেছ ব্য়঵হার করি
পেট্রো-রাসাযনিক উদ্য়োগর বাবে
প্রযোজনীয উত্্পাদনত গুরুত্঵ আরোপ
করা হৈছিল ।
",0
</p></body></text></cesDoc>