<cesDoc id="asm-w-media-nmm128" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-media-nmm128.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>প্রান্তি</h.title>
<h.author>ভবেন্দ্র</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">nmm128</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0020.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>প্রান্তিক ( ভবেন্দ্র নাথ শইকীযা )
শেষ উপনিবেশর স্঵াধীনতা প্রসংগ :
নামিবিযা :-
ড. সুনীল পবন বরু঵া ।
নিউযর্কর পরা প্রকাশিত মার্কিন সুত্রর
বাতরি মতে এংগোলার পরা 24 মাহর
পরা 30 মাহর ভিতরত কিউবার সৈন্য়বাহিনী
অপসারিত করিবলৈ সন্মত হৈছে । য়ো঵া
অক্টোবর মাহর পরা প্রচারিত বহু প্রকাশিত
এই বাতরির উত্স আছিল মার্কিন মধ্য়স্থতাত
এংগোলা , কিউবা , দক্ষিণ আফ্রিকা আরু
মার্কিন য়ুক্তরাষ্ট্রর মাজত ইতিমধ্য়ে চলি
থকা আলোচনার শেহতীযা বৈঠক । কংগোর
রাজধানী ব্রাজাভিল চহরত এই প্রসংগত
প্রস্তা঵িত চূড়ান্ত আলোচনাই এনে এক গুরুত্঵পূর্ণ
বাতরি সদরি কারর বাট মুকলি করি দিযে ।
অ঵শ্য়ে প্রকাশিত বাতরি সম্পর্কে মন্তব্য় দি
দক্ষিণ আফ্রিকার প্রেছিডেণ্ট পি.বোথাই কয
য়ে এংগোলার পরা কিউবার সৈন্য় অপসারণর
সময সীমা নির্ধারণর ক্ষেত্রত এতিযাও কোনো
সিদ্ধান্ত হো঵া নাই । সৈন্য় অপসরণর নীতিটো
নীতিগতভাবে মানি লো঵া হৈছে য়দিও সময সীমার
কথাটো এতিযাও আলোচনার বিষয হৈ আছে ।
আফ্রিকা মহাদেশত বর্ণবাদী ঔপনিবেশিক শাসনর
শেষ নিদর্শন স্঵রূপ দক্ষিণ-পশ্চিম আফ্রিকা
ওরফে নামিবিযার স্঵াধীনতার বাটত এংগোলাত
প্রায 40 হাজার সৈন্য়র উপস্থিতির বিষযটো঵ে
ইমান দিনে হেঙাররূপে দেখা দি আহিছে ।
বিশ্঵ জনমতক উপেক্ষা করি নামিবিযার
স্঵াধীনতাত পলম ঘটাই অহা সংশ্লিষ্ট পক্ষক
বর্তমানর গুরুত্঵পূর্ণ সিদ্ধান্তই কেনেদরে প্রভাবিত
করে তার বাবে এতিযা সকলোরে দৃষ্টি
দক্ষিণ আফ্রিকার ওপরত নিবদ্ধ । নামিবিযার
স্঵াধীনতা আরু কিউবার সৈন্য়র উপস্থিতি -
এই দুযোটা বিষযে কেনেকৈ এটা আনটোর
পরুপূরক হৈ পরিল , তার ওর বিচারি
আফ্রিকার দক্ষিণ অঞ্চলটোর অতীত বুরঞ্জী
চালি-জারি য়ো঵ার প্রযোজন উপেক্ষা করিব
নো঵ারি ।</p>

<p>উনবিংশ শতিকার মাজভাগলৈকে তথাকথিক
সভ্য়তার সো঵াদ পো঵া বিশ্঵বাসীর বাবে আফ্রিকা
মহাদেশ আছিল সম্পূর্ণ অঞ্জাত , অপরিচিত
অঞ্চল । ডেভিদ লিভংষ্টন , ষ্টেনলি আদি
সুপ্রসিদ্ধ পর্য়্য়াটকসকলর অভিয়ানে পৃথিবীর
এই বিচিত্র ভুখণ্ডক বিশ্঵বাসীর সন্মুখত পরিচিত
করার বাবে আফ্রিকার বিস্তৃর্ণ অঞ্চল , ভুগর্ভত৉
লুকাই থকা অতুলনীয সম্পদ , শিল্পবিপ্ল঵র
বাবে কেঁচামাল আরু প্রায বিনামূল্য়ই লাভ করিব
পরা মান঵ শক্তির প্রচুর সম্ভা঵নাই ইউরোপর
রাজনীতিত এক উত্তপ্ত পরিবেশর সৃষ্টি করিছিল ।
এই সম্ভা঵নাক বাস্ত঵ত রূপাযিত করার বাবে
1876 চনত বেলজিযামর দ্঵িতীয লিযপণ্ডে প্রথম
বারর কারণে রাজধানী ব্রাছেলছত এক আন্ত:রাষ্ট্রীয
অধিবেশন আহ্঵ান করিলে । উক্ত ফলস্঵রূপে
আফ্রিকা মহাদেশত বসবাস করা প্রায ছয হেজার
উপজাতিক লৈ গঢ়ি উঠিল পঞ্চাছখনরো অধিক
পশ্চিমীযা উপনিবেশ । লগে-লগে আরম্ভ হ'ল
কৃষ্ণাংগ আফ্রিকার ওপরত শ্঵েতাংগ ইউরোপে
চলো঵া শ বছরীযা শাসন-শোষণ আরু অত্য়াচার ।
ইতিমধ্য়ে আফ্রিকাত চলি থকা পশ্চিমীযা সাম্রাজ্য়বাদ
তথা উপনিবেশবাদর প্রতিয়োগিতাত এক নতুন
প্রতিয়োগিতার আবির্ভা঵ ঘটিল । ঔপনিবেশর
প্রতিয়োগিতাত পলমকৈঅথচ প্রচুর সম্ভা঵না লৈ
য়োগদান করা সেই প্রতিয়োগীর নাম আছিল জার্মেনী ।
ঐতিহাসিক বার্লিন কংগ্রেছত ( 1982 ) আফ্রিকাক
নতুনকৈ ভাগ-বাটো঵ার করোতে আফ্রিকার আন
তিনিটা অঞ্চলর লগতে দক্ষিণ-পশ্চিম আফ্রিকা
( নামিবিযা ) জার্মেনীর ভাগত পরিল । প্রথম
বিশ্঵য়ুদ্ধত ( 1914-1918 ) পরাজযর সন্মুখীন
নোহো঵ালৈকে দক্ষিণ আফ্রিকা আছিল জার্মান
উপনিবেশ । 1919 চনর ভার্ছাই চুক্তিমতে
জার্মান উপনিবেশর শাসনর বাবে অস্থাযী
তত্঵াবধাযক শাসনর ব্য়঵স্থা করা হ'ল । ইযারে
আধারত ন঵ প্রতিষ্ঠিত জাতি-সংঘই পূবর জার্মান
উপনিবেশ দক্ষিণ-পশ্চিম আফ্রিকাক সামযিকভাবে
শাসন করার ভার দক্ষিণ আফ্রিকার শেতাংগ
চরকারর ওপরত ন্য়াস্ত করে । দ্঵িতীয মহায়ুদ্ধর
সামরণিত রাষ্ট্রসংঘই ( U.N.O ) দক্ষিণ-পশ্চিম
আফ্রিকার বাবে আন্ত:রাষ্ট্রীয ন্য়াত পদ্ধতি প্রবর্তন৉
করি দক্ষিণ আফ্রিকার চরকারক এই ন্য়াস চিক্তিত
চহী করিবলৈ নির্দেশ দিলে । দক্ষিণ আফ্রিকার
বর্ণবাদী শ্঵েতাংগ চরকারে এই নির্দেশ অগ্রাহ্য়
করি ওলোটাই দক্ষিণ-পশ্চিম আফ্রিকাক দক্ষি
আফ্রিকার লগত চামিল করার বাবে দাবী জনালে ।
এই ক্ষেত্রত দক্ষিণ আফ্রিকার য়ুক্তি হ'ল সংখ্য়াগরিষ্ঠ
বাসিন্দাই তেনে মত পোষণ করে । রাষ্ট্রসংঘর
সাধারণ সভাই অ঵শ্য়ে এই দাবী নাকচ করিলে ।
দক্ষিণ-পশ্চিম আফ্রিকার ওপরত শাসন চলাবলৈ
দক্ষিণ আফ্রিকার চরকারে লাভ করা কর্তৃত্঵র
ম্য়দ ওর পরাত 1966 চনত রাষ্ট্রসংঘই পূর্বর
অস্থাযী তত্঵াবধাযক শাসনর চুক্তি নাকচ করে ।
কিন্তু দক্ষিণ আফ্রিকার শ্঵েতাংগ প্রশাসনে বিশ্঵জনমতক
উলংঘা করি এক পক্ষীযভাবে পূর্বর জার্মান
উপনিবেশত নিজর কর্তৃত্঵ অব্য়াহত রখাত
আন্ত:রাষ্ট্রীয ন্য়াযলযে নামিবিযার ওপরত দক্ষিণ
আফ্রিকাই বলপূর্বকভাবে চলাই ছকা শাসনক
অবৈধ বুলি ঘোষোণা করে । উল্লেখয়োগ্য় য়ে
দক্ষিণ-পশ্চিম আফ্রিকাত অ঵স্থিত নামিব
মরুভুমির বাবে স্থানীয অধিবাসীসকলে বর্তমান
এই দেশক নামিবিযা বুলি পরিচয দিছে ।</p>

<p>আফ্রিকা মহাদেশত পশ্চিমীযা সাম্রাজ্য়াবাদ
তথা উপনিবেশবাদর স্রষ্টাসকলে চলাই অহা
শাসন আরু শোষণর শেষ চিহ্ন হিচাবে সম্প্রতি
নামিবিযা বিশ্঵র প্রগতিশীল জনতার বাবে এক
অতি পরিচিত নাম । ইতিমধ্য়ে ষাঠীর দশকর
পরাই আফ্রিকাত চলি অহা পশ্চিমীযা উপনিবেশ
শাসনত গরাখহনীযা আরম্ভ হৈছে । আফ্রিকার
কৃষ্ণাংগ জনগণে য়ুদ্ধোত্তর পৃথিবীত দুই প্রধান
পরিচালিকা শক্তি গণতন্ত্র আরু জাতীযবাদ
দ্঵ারা অনুপ্রাণিত হৈ দাসত্঵ তথা পরাধীনতা
আরু বর্ণবাদী শাসনর বিরুদ্ধে চলো঵া মুক্তি
সংগ্রামর ফলত প্রাযবোর উপনিবেশত পশ্চিমীযা
শাসনর ওর পরিল আরু স্঵াধীনতা ঘোষণা করা
হ'ল । 1974 চনত নামিবিযার কাষরীযা দুখন
গুরুত্঵পূর্ণ পর্টুগীজ উপনিবেশ এংগোলা আরু
মোজ্জাম্বিকে স্঵াধীনতা লাভ করিলে । 1980 চনত
আফ্রিকাত শেষ বৃটিছ উপনিবেশ রোডছিযার
পরা 90 বছরীযা অবৈধ শাসনর ওর পরাত
বর্তমান আফ্রিকাত গোষ্ঠী-নিরপেক্ষ আন্দোলনর
নেতা স্঵রূপ স্঵াধীন জিম্বাবু঵েই আত্মপ্রকাশ
করিলে ।</p>

<p>ভৌগলিক আযতনর দিশর পরা নামিবিযা এখন
বৃহত্ দেশ । পূর্বর জার্মান উপনিবেশ দক্ষিণ-পশ্চিম
এই অঞ্চলটোর আযতন ভারবর্ষর প্রায তিনি ভাগর
এভাগ । কিন্তু আযতনর তুলনাত ইযার জনসংখ্য়া
তেনেই তাকর । এই বৃহত্ দেশখনত মাত্র এক নিয়ুত
মানুহেহে বাস করে । এওঁলোকর ভিতরত প্রায
1 লাখ হৈছে ইউরো-আফ্রিকান সম্প্রদাযর অন্তর্ভুক্ত ।
শ্঵েতাংগসকলর শতকরা 40 ভাগ জার্মান বংশোদ্ভ঵ ।
কৃষ্ণাংগসকলর ভিতরত ভালেমান উপজাতি আছে ।
ইযার ভিতরত ওভাণ্ডো উপজাতিটো঵েই সকলোতকৈ
আগবঢ়া । দেশর উত্তারাঞ্চলর সারু঵া ঠাইত বসবাস
করা ওভাণ্ডোসকলে মুঠ জনসংখ্য়ার 48 ভাগত
প্রতিনিধিত্঵ করে । স্মরণয়োগ্য় য়ে দেশখনক
দক্ষিণ আফ্রিকার উপনিবেশর পরা মুক্ত করিবলৈ
ইতিমধ্য়ে নামিবিযাত গঢ়ি উঠা দক্ষিণ-পশ্চিম
আফ্রিকা গণসংস্থার নেতৃত্঵াধীন এই সংস্থার গণমুক্তি
বাহিনীযে কাষরীযা দেশ নামিবিযা আরু মোজাম্বিকত
ঘাঠটি স্থাপন করি গেরিলা সংগ্রামর য়োগেদি বর্ণবাদী
দখলকারী প্রশাসনক ব্য়তিব্য়স্ত করি তুলিছে ।
1973 চনর ডিচেম্বর মাহর রাষ্ট্রসংঘর সধারণ
অধিবেশনে শ্঵ো঵াপ'ক নামিবিযার জনসাধারণর
একমাত্র নির্ভরয়োগ্য় প্রতিষ্ঠান বুলি ঘোষণা করাত
নামিবিযা মুক্তি আন্দোলনে আন্ত:রাষ্ট্রীয স্঵ীকৃতি
লাভ করিলে । ইতিমধ্য়ে গোষ্ঠী-নিরপেক্ষ দেশসমুহে
শ্঵ো঵াপ'ক এই সংস্থার সদস্য়রূপে স্঵ীকৃতি প্রদান
করি নিজর অধিকার সাব্য়স্ত করিবলৈ নামিবিযার
জনসাধারণে আরম্ভ করা সশস্ত্র সংগামর প্রতি
সমর্থন আগবঢ়াইছে । কিন্তু রাষ্ট্রসংঘর সাধারণ
সভা আরু গোষ্ঠী-নিরপেক্ষ দেশসমুহর হেঁচা অব্য়াহত
থাকিল য়দিও পশ্চিমীযা একাংশ রাষ্ট্রর বিধি-পথালির
বাবে নামিবিযার স্঵াধীনতার প্রশ্নটো঵ে বিশেষ অগ্রগতি
লাভ করিব নো঵ারিলে । অ঵শেষত রাষ্ট্রসংঘর
নিরাপত্তা পরিষদে নামিবিযা সংম্পর্কত 1978 চনর
29 ছেপ্তেম্বর তারিখে এক ঐতিহাসিক প্রস্তা঵ গ্রহণ
করিলে । পশ্চিমীযা সমন্঵যরক্ষী নামে জনাজাত
পাঁছখন পশ্চিমীযা দেশে আগবঢ়ো঵া এই প্রস্তা঵য়োগে
নামিবিযার পরা আগবঢ়ো঵া দক্ষিণ আফ্রিকার সৈন্য়
বাহিনীর অপসরণ , নির্দিষ্ট সমযর ভিতরত য়ুদ্ধ
বিরতি , রাষ্ট্রসংঘর সহাযকবাহিনী নিযোগ ,
রাষ্ট্রসংঘর তত্ত্঵াবধান আরু নিযন্ত্রণত মুক্ত নির্ব্বাচনর
ব্য়঵স্থা নামিবিযাত রখা হ'ল । রাষ্ট্রসংঘর 435/1978
নম্বর প্রস্তা঵ত সাধারণভাবে সমগ্র বিশ্঵তে নামিবিযার
অনুকুলে সদিচ্ছা প্রতিফলিত হৈছিল । দক্ষিণ আফ্রিকার
বর্ণবাদী পোনপতীযাভাবে এই প্রস্তা঵ অগ্রাহ্য় করিব
নো঵ারিলেও এক অদৃষ্ট শক্তির বলত নানা চল-চাতুরিরে
নিরাপত্তা পরিষদর 435/78 নম্বর প্রস্তা঵ কার্য়্য়করী
করাত বাধা আরোপ করি বিশ্঵ জনমতক বিভ্রান্ত
করিবলৈ চেষ্টা করি থাকিল । বিশেষকৈ নিরাপত্তা
পরিষদর প্রস্তা঵ক আওকাণ করি নামিবিযাত এক
অস্থাযী চরকার গঠন করি আরু এক পক্ষীযভাবে
স্঵াধীনতা প্রদান করি দক্ষিণ আফ্রিকাই নিজর অনুকুলে
রাজনৈতিক পরিবেশ এটা গঢ়ি তুলুবলৈ চেষ্টা
করিলে । অন্তর্বর্তী চরকারত শ্঵ো঵াপ ডেমক্রেট ,
দক্ষিণ-পশ্চিম আফ্রিকা নেচনেল ইউনিযন , লেবার
পার্টি , বিহবথ লিবারেশ্য়ন ফ্রণ্ট , ডেমক্রেটিক
টার্঩হেলি এলাযেঞ্চ নেশ্য়নেল পার্টিক স্থান দিযা
হ'ল । শেহর দুযোটা দল শ্঵েতাংগ লোকর দ্঵ারা
গঠিত আরু বাস্তবিকতে দুযোটা দক্ষিণ আফ্রিকার
শাসনাধিষ্ট দলর সহয়োগী দল বুলি ক'ব পারি ।
রাষ্ট্রসংঘই শ্঵ো঵াপক নামিবিযার জনসাধারণর
একমাত্র প্রতিনিধি বুলি প্রদান করা স্঵ীকৃতিক
প্রত্য়াহ্঵ান জনাই দক্ষিণ আফ্রিকাই এই অন্তর্বর্তীকালিন
চরকার গঠন করিছিল । নামিবিযার রাজনৈতিক
পরিস্থিতিযে কিছু পরিমাণে হ'লেও বর্ণবাদী প্রশাসনক
সহায করিছিল । কিযনো দেশত ভিন্ন জনমতক
সহায করা প্রায একুরি রাজনৈতিক দল আছে ।
কিন্তু অন্তর্বর্তী চরকারর মাজত দেশর সংবিধান
রচনার বিষযটো লৈ মতভেদ সৃষ্টি হ'ল । চরকারত
য়োগদান করা চারিটা কৃষ্ণাংগ রাজনৈতিক বিতর্কিত
বর্ণবৈষম্য় আইন এখন বিনাচর্তে প্রত্য়াহার করিব
খোজে । শ্঵েতাংগ দুযোটা দলে ইযার বিরোধিতা
করে । ফলত দক্ষিণ আফ্রিকাই নামিবিযালৈ
আগবঢ়ো঵া জাতীয ঐক্য়র বিকল্প চরকারর প্রস্তা঵টো
বিশেষ কামত নহাত নিরাপত্তা পরিষদর প্রস্তা঵টো
কার্য়্য়করী করাত অয়থা পলম ঘটিছে ।</p>

<p>চলিত দশকর আরম্ভণীরে পরা দুটা ঘটনাই
নামিবিযার স্঵াধীনতার প্রশ্নটোত দুটা বিপরিতমুখী
চাপর সৃষ্টি করিছে । এহাতে জিম্বাবু঵ের স্঵াধীনতাই
আফ্রিকার শেষ উপনিবেশ হিচাবে নামিবিযার
স্঵াধীনতার বিষযটোত অধিক গুরুত্঵ আরোপ করে
আরু আনহাতে মার্কিন য়ুক্তরাষ্ট্রত রোনাল্ড রেগান
প্রেছিডেণ্ট রূপে নির্ব্বাচিত হো঵া ঘটনাই নামিবিযা
প্রসংগত রাষ্ট্রসংঘই গ্রহণ করা প্রস্তা঵টো কার্য়্য়করী
করাত বাধা প্রদান করিবলৈ দক্ষিণ আফ্রিকার
বর্ণবাদী চরকারক নতুনকৈ উত্সাহিত করে ।
সমগ্র বিশ্঵তে নামিবিযা দিবস উদয়াপন করি
আফ্রিকাত চলি থকা বর্ণবৈষম্য় তথা উপনিবেশবাদর
প্রতি দৃষ্টি আকর্ষণ করিবলৈ চেষ্টা চলিলেও
নিরাপত্তা পরিষদে গ্রহণ করা 435/78 নম্বর
প্রস্ত঵র উথ্থাপক পাংছখন পশ্চিমীযা রাষ্ট্রই নিজেই
প্রস্তা঵টো কার্য়্য়করীকরণত বিধি-পথালি দিযাত
পরিস্থিতিযে নাটকীয রূপ ধারণ করিলে । উল্লেখয়োগ্য়
য়ে নিরাপত্তা পরিষদর প্রস্তা঵টো কার্য়্য়করী করার
স্঵র্ত হিচাবে অতি কৌশলেরে নামিবিযার চুবুরীযা
দেশ এংগোলাত অ঵স্থান করা কিউবার সৈন্য়
বাহিনী অপসরণ করার প্রশ্নটো জড়িত করা হ'ল ।
কো঵া বাহুল্য় য়ে 1974 চনত পর্টুগিজ শাসনর
পরা মুক্ত হো঵া এংগোলা আরু মোজ্জাম্বিকত
মার্ক্সবাদী চররাকর গঠিত হৈছিল । কিন্ত ছোভিযেট
ইউনিযন আরু কিউবার পৃষ্ঠপোষকতাত এংগোলাত
প্রেছিডেণ্ট জ'ছ এড঵ার্ড ডন ছানটছর নেত৉ত্঵ত
গঠিত মার্ক্সবাদী প্রশাসন সোঁপন্থী নেতা জনাছ
ছাভিমবীর ইউনিটা গেরিলা বাহিনীর সন্মুখীন
হ'ল । আফ্রিকার দক্ষিণ ভাগত একে সমযতে
দুখনকৈ মার্ক্সবাদী চরকার বিশেষভাবে প্রতিষ্ঠা
হো঵াত বিশেষভাবে চিন্তিত আফ্রিকার শ্঵েতাংগ
চরকার আরু মার্কিন প্রাশসনে ইউনিটা গেরিলা
বাহিনীর পিছত থিয দিলে । ন঵ প্রতিষ্ঠিত
বাওঁপন্থী চরকারক রক্ষণাবেক্ষণ দিবলৈ ইতিমধ্য়ে
প্রায 40 হজার কিউবান সৈন্য় এংগোলাত উপস্থিত
হ'লহি । য়ো঵া 10 বছর ধরি এংগোলাত চলি
থকা গৃহয়ুদ্ধর এইটো঵ে হ'ল পটভূমি ।</p>

<p>ইতিমধ্য়ে নামিবিযাত শ্঵াতাংগ প্রশাসনর বিরুদ্ধে
গেরিলা সংগ্রাম চলো঵া শ্঵ো঵াপই এংগোলাত
ঘাটি স্থাপন করার উজুহাতত দক্ষিণ আফ্রিকার
সৈন্য়বাহিনীযে এংগোলার বিরুদ্ধে পোনপতীযা
অভিয়ান চলাই দক্ষিণ এংগোলার একাংশ দখল
করার পরিস্থিতি অধিক জটিল হৈ পরিল ।
এনেদরে আক্রমণাত্মত নাতি বাহাল রাখি
শ্঵ো঵াপর প্রতি সমর্থন আগবঢ়ো঵া আফ্রিকার
সকলো আগশারীর দেশর ওপরত দক্ষিণ
আফ্রিকাই সকলো প্রকার রাজনৈতিক , অর্থনৈতিক
হেঁচা দিবলৈ চেষ্টা চলাই অহা দেখা গৈছে ।</p>

<p>এংগোলার গৃহয়ুদ্ধ আরু কিউবার সৈন্য়র
উপস্থিতির লগত নামিবিযার স্঵াধীনতার বিষযটো
সাঙুরি মার্কিন য়ুক্তরাষ্ট্র , কানাডা , ফ্রান্স ,
পশ্চিম জার্মেনী আরু বৃটেইনর প্রতিনিধি লৈ
নামিবিযা প্রসংগত গঠিত পশ্চিমীযা সমন্঵যরক্ষী
কমিটি ওলোটাই প্রভাবান্ধিত করাত দক্ষিণ
আফ্রিকাই ভালেখিনি সফলতা লাভ করিছিল ।
এই ক্ষেত্রত প্রিযোরিযার সংখ্য়ালঘু শ্঵েতাংগ
প্রশাসনর শংকার মূল কারণ হ'ল চুবুরীযা
এংগোলার স্঵াধীন নামিবিযাযো কিউবান সৈন্য়ক
আমন্ত্রণ জনাব পারে । দক্ষিণ আফ্রিকার সীমান্তত
কমিউনিষ্ট সৈন্য়র উপস্থিতিক কোনেপধ্য়ে মানি
ল'ব নো঵ারি । এতেকে পশ্চিমীযা মহল সাবধান
হো঵া উচিত । তথাপিও দক্ষিণ-পশ্চিম আফ্রিকাক
কেন্দ্র করি আন্ত:রাষ্ট্রীয মহল অপ্রত্য়াশিতভাবে
দ্রুত পরিবর্তনর সূচনা হৈছে আরু ইযাত
ভালেমান আনুসংগিক ঘটনাই অরিহনা য়োগাইছে ।
এই প্রসংগত রাজনৈতিক পর্য়্য়বেশকসকলে
মিখাইল গর্বাচেভর নেতৃত্঵ত ছোভিযেট ইউনিযনে
বিশ্঵র চলিত সংকটসমুহর ক্ষেত্রত গ্রহণ করা
বাস্ত঵ধর্মী দৃষ্টিভংগী , আগন্তক মার্কিন প্রেছিডেণ্টর
রিপাবলিকান দলর প্রার্থীর প্রশ্ন , দক্ষিণ আফ্রিকাত
অ঵স্থান করা দক্ষিণ আফ্রিকার দখলকারীর ওপরত
কিউবান বাহিনীযে করা ব্য়পক অভিয়ান , আফ্রিকার
শ্঵েতাংগসকলর ভবিষ্য়তর কর্মপন্থা লৈ মতভেদ
হো঵া আরু নামিবিযা আরু এংগোলার য়ুদ্ধত
প্রিটোরিযার শ্঵েতাংগ প্রশাসনর ব্য়াপক ক্ষতির
( প্রতি দিনে খরচ প্রায 5 নিয়ুত ডলার )
বিষযটোত বিশেষভাবে গুরুত্঵ আরোপ করা হৈছে ।
ইযার পরিপেক্ষিতত চলিত বছরর মে মাহর পরা
মার্কিন য়ুক্তারাষ্ট্রর মধ্য়স্ততাত এংগোলা , কিউবা
আরু দক্ষিণ আফ্রিকার প্রতিনিধির মাজত চলা
আলোচনাই নামিবিযার স্঵াধীনাতর প্রশ্নত রাষ্ট্রসংঘর
435/78 নম্বর প্রস্তা঵ত এক নতুন অর্থ প্রদান
করিবলৈ সমর্থবান হৈ পরিছে । অ঵শেষত
নামিবিযার স্঵াধীনতা প্রদান আরু কিউবার পরা
সৈন্য় অপসরণ এই দুযোটা বিষযর মাজত
সোয়োগ ঘটাই আপোচ সুত্র এটা উদ্ভা঵ন করা
হ'ল । আগষ্ট মাহত স্঵াক্ষরিত এই চুক্তিত
য়ুদ্ধ বিরতি , দক্ষিণ আফ্রিকার পরা সৈন্য়
অপসরণ আরু নামিবিযাত নির্ব্বাচনর দিন
ঘোষোণার ক্ষেত্রত বুজাপরা এটাত সিদ্ধান্ত
উপনীত হ'ব পরা গৈছে । এতিযা কে঵ল
কিউবার সৈন্য় এংগোলার পরা অপসরণর
বিষযটো বাকী আছে । এই বিষযটো নিস্পত্তি
হ'লে পূর্ব্বতে ঘোষোণা করা মতে সংশ্লিষ্ট
সকলো পক্ষর সহয়োগিতা চলিত মাহর
পহিলা তারিখর পরা নামিবিযাত প্রস্তা঵র
আধারত নির্ব্বাচনী প্রক্রিযা আরম্ভ করিব পরা
য়াব । চারি রাষ্ট্রর মাজত সম্পাদিত চুক্তির
ফল হিচাবেই ই রাষ্ট্রসংঘর সচিব প্রধান
গরাকী নামিবিযা প্রসংগত গৃহিত 435	/78
প্রস্তা঵ কার্য়্য়করী করার ক্ষেত্রত আলোচনার
বাবে সুদীর্ঘ বিরতির পাছত দক্ষিণ আফ্রিকার
রাজধানী প্রিটোরিযাত উপস্থিত হো঵ার বাট
মুকলি হৈ পরছে ।
",0
</p></body></text></cesDoc>