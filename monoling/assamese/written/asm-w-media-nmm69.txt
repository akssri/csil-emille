<cesDoc id="asm-w-media-nmm69" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-media-nmm69.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>প্রান্তি</h.title>
<h.author>ভবেন্দ্র</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1990</pubDate>
</imprint>
<idno type="CIIL code">nmm69</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0043.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;প্রান্তিক ( ড: ভবেন্দ্রনাথ শইকীযা )</p>

<p>মার্টিনা , শিশু-প্রীতি আরু
হেরাই য়ো঵া মুখখন :
`` সমুখত কোনো প্রত্য়াহ্঵ান থিয
হ'লেই মোর ভোক দুগুণে চরে ।
শরীরর অভ্য়ন্তরত এক উদ্঵েলিত ভা঵
অনুভ঵ করোঁ । ""</p>

<p>য়ো঵াবারর উইম্বলড়ন টেনিছ
প্রতিয়োগিতার আগে-আগে মার্টিনা
নাভ্রাটিলোভাই এক সাক্ষাত্কারত এইবুলি
কৈছিল । য়ো঵াবার মার্টিনার সমুখত
প্রত্য়াহ্঵ান আছিল উইম্বলডনত নবম বারর
বাবে ছিংগলছ খিতাপ অর্জন করি ইতিহাস
গঢ়া । দুখর কথা , মার্টিনা সেইবার ব্য়র্থ
হৈছিল । ফাইনেলত ষ্টেফিপ্রাফে তেওঁক
পরাজিত করি তেওঁর অভিলাষ পুরণর
ঘটনাটো এটা বছরর বাবে পিছু঵াই দিলে ।
এইবার সেই সাফল্য় তেওঁ লাভ করিলে
জিনা গেরিছনক পরাভৃত করি ।
উইম্বলডনর সেউজীযা খেল-পথারত
মার্টিনার পদচিহ্ন পরিণত হ'ল ইতিহাসর
উপাদানত ।</p>

<p>গোটেই ক'র্ট জুরি দৌরা-দৌরি করি
বিভিন্ন ধরণর ষ্ট্র'কেরে পইণ্ট লাভ করাটো
নিশ্চয সম্ব঵ ; কিন্তু তার লগত য়দি শক্তি
আরু গতির সংমিশ্রণ ঘটে , তেনেহ'লে
খেলু঵ৈগরাকী অপ্রতিরোধ্য় হৈ উঠার
সম্ভা঵না থাকে । মার্টিনা নাভ্রাটিলোভার
ক্ষেত্রত সেযাই হৈছে । সত্তরর দশকর
আরম্ভণিতে অষ্ট্রেলিযার মার্গারেট স্মিথে
য়িটো ধারা আরম্ভ করিছিল , মার্টিনাই সেই
ধারার প্রভূত পরিমাণে উত্কর্ষ সাধন
করিবলৈ সক্ষম হয ।</p>

<p>অ঵শ্য়ে এই লেখাত ` কিম্বদন্তী '
মার্টিনার অন্য় এটা দিসতহে আলোকপাত
করিব খোজা হৈছে । সেযা য়েন আন এক
মার্টিনার কথা ।</p>

<p>মার্টিনার হৃদযর এক বিশাল অংশ
দখল করি আছে শিশুসকলে । সেই শিশু
য়দি দু:স্থ অথবা শারীরিকভা঵ে অক্ষম হয ,
তেনেহ'লেতো কথাই নাই ; মার্টিনা হৈ পরে
অত্য়ন্ত ম্রিযমান । সেইসকল শিশুর মতে
মার্টিনাই তেওঁলোকর মাদার টেরেছা ।
অথবা ছাণ্টা ক্লজ ।</p>

<p>বিশ্঵র বিভিন্ন প্রান্তর শিশুসকলর বাবে
মার্টিনার মানসিক অথবা আর্থিক সাহায়্য়র
দু঵ার সদায কোলা থাকে । এবার লিপটন
আন্ত:রাষ্ট্রীয প্লেযার্ছ টেনিছ প্রতিয়োগিতাত
ছিংগলছ , ডাবলছ আরু মিক্সড ডাবলছর
কেউটা শাখাতেই মার্টিনা বিজযী হৈছিল আরু
তেতিযা তাত পো঵া অর্থর আধাখিনি দান করিছিল
ইথিঅ'পিযার খরাংপীড়িতসকলর বাবে আযোজিত
`` ছেইভ দা চিলড্রেন "" নামর আঁচনিলৈ । তদুপরি
শিশুসকলর বাবে পুঁজি সংগ্রহর বাবে মার্টিনাই
য়ো঵া প্রায 6-7 বছর ধরি বিভিন্ন ক্রিছ এভার্ট ,
পাম শ্রাইবার আদি খেলু঵ৈসকলর সৈতে প্রদর্শনী
খেলত অংশ লৈ আহিছে ।</p>

<p>মার্টিনাই প্রতি বছরে তেওঁর টেক্সাছর
বিশাল বাসগৃহত শিশুসকলর বাবে এক
পার্টির আযোজন করে । বিভিন্ন ঠাইর শিশুর
তাত সমাবেশ ঘটে । মার্টিনাই সেইদিনা সকলো
কাম বাদ দি তেওঁলোকর সৈতেই ব্য়স্ত হৈ পরে ।
প্রতিজনর খা-খবর লয , তেওঁলোকর সুখ-দুখর
সমভাগী হ'ব খোজে , নানা ধরণর সাধু কয ,
সাধু শুনে , নিখুঁত গৃহস্থর দরে তেওঁলোকর
আলপৌচান ধরে ইত্য়াদি । মুঠতে সেইখন
নাভ্রাটিলোভার এখন সুকীযা পৃথি঵ী ।</p>

<p>কিয এই শিশু-প্রীতি ?
বহুতেই কৌতুহলী হৈ প্রশ্ন করে ।
নি:সন্তান বাবেই তেওঁর তেনে হৈছে
বুলি ক'বলৈতো তেওঁ বিযাতো সোমো঵া
নাই !</p>

<p>কিন্তু কিছুমানে ক'ব খোজে য়ে
য়ৌবনর দু঵ারডলি পার হো঵ার পাছত
মার্টিনাই করি অহা এক অক্ষমনীয ভুলর
বাবেই তেওঁ শিশুসকলক তেনেকৈ ভাল
পাবলৈ ললে । হযতো হয , হযতো নহয ।
কিন্তু এই কৌতুহল নিবৃত্ত করিবর বাবে
আমি হযতো বিচার করি চাব পারোঁ
মার্টিনাই তেওঁর আত্মজী঵নী `` বিইং
মাইছেলফ ""- ত অকপটে স্঵ীকার করি য়ো঵া
কিছু কথা ।</p>

<p>`` মোর জন্ম 1956 চনর 18
অক্টোবরত ; প্রাগত । মোর বযস তিনি বছর
হওঁতেই দেউতা মিরোশ্লাভ চুবার্টার সৈতে
মার বিচ্ছেদ ঘটে । তার পাছত তিনি
বছরীযা সন্তানটি লৈ মা উলটি আহে
রেভনিচর মোমাইদেউহঁতর ঘরলৈ ।</p>

<p>মোর মার টেনিছর লগত স্কিতো
মোটামুটিভা঵ে এটা নাম আছিল ।
মোমাইদেউর ঘরলৈ অহার কিছুদিনর
পাছতে মা মিউনিছিপাল টেনিছ ক্লাবর
চাকরিত সোমায । রঙা ক্লে' ক'র্ট । চহরর
বহু লোক তালৈ আহে , - ঘাইকৈ অ঵সরর
সময পার করিবলৈ ।</p>

<p>মার সৈতে মযো নিযমিতভা঵ে টেনিছ
ক'টলৈ য়াবলৈ লৈছিলো । বযস তেতিযা
ছারে তিনি বছরমান হৈছিল । এদিন
দেখিলোঁ , - ক'র্টত ঢালিবর বাবে
কেইজনমান মানুহে রঙা গুড়ি লৈ ভিতরলৈ
সোমাই গৈছে । মযো তেওঁলোকর সৈতে
ভিতর সোমালো । সেই সকলরে এজনে
মোর সৈতে নানা রসিকতা করিবলৈ
ধরিলে । এদিন তেওঁ মোক চেকোশ্লোভাকিযার
শিশুসকলর মাজত অত্য়ন্ত প্রিয ` কোফিলা '
চকলেট খাবলৈ দিলে । তার পাছত কেইবাদিনো ।
মানুহজনর নামটো এদিন জানিব পারিলোঁ ।
আমার দেউতার সৈতে একেই । তেওঁর উপাধিটো
আছিল নাভ্রাটিল ।</p>

<p>এদিন মই দৌরাদৌরি করি থাকোঁতে
হঠাত্ আবিষ্কার করিলোঁ- ক'র্ণ্টর একোণর
বেঞ্চ এখনত সেই ভদ্রলোক আরু মাই বেছ
ঘনিষ্ঠভা঵ে বহি কথা পাতি আছে ।
এসমযত মানুহজনে আমার ঘরলৈকো
ঘনাই আহিবলৈ ললে ।</p>

<p>দেউতার সৈতে বিচ্ছেদর পাছত মার
মন স্঵াভা঵িকতে ভাগি পরিছিল । কিন্তু
ক্রমাত্ লক্ষ্য় করিলোঁ , - মার সেই হেরো঵া
উদ্য়ম য়েন ঘুরি আহিছে । ক'ব পারি ,
মিরেকেই ( মানুহজনক বহুতে সেই
নামেরেই মাতিছিল ) মাক নতুন জী঵ন দিলে ।</p>

<p>1961 চনর এক জুলাইর দিনা মার
সৈতে মিরেকর বিযা হয । পাছতহে
জানিছিলো য়ে মার সৈতে মিরেক আরু
মোর দেউতার পরিচয তেতিযার নহয ।
আনকি তেওঁলোক দুযোজনেই হেনো মাক
বিযা করাব খুজিছিল আরু মুরকত মার
বিচ্ছেদর কারণো আছিল সেইটো঵েই । য়ি
নহওঁক , বিযার পাছত আমি তিনিজনে
ককাহঁতর ঘরর ওপর তলাত থাকিবলৈ
লৈছিলোঁ । 1963 চনর জুনত মোর ভনী
জেনার জন্ম হয ।</p>

<p>মোর জী঵নত মাই হ'ল প্রথম
শিক্ষযিত্রী আরু আদর্শ নারী । নিজর জী঵ন
সম্পর্কে মা বর সচেতন আছিল । তেওঁ
য়েতিযাই য়ি করিছিল , - খেলাতেই হওক
বা ঘরু঵া কামতেই হওক ,- বর একান্তমনে
করিছিল । মার পরাই মই প্রথম খেলার
প্রেরণা পাওঁ আরু তেওঁর দরেই বিভিন্ন
খেল খেলিবলৈ লৈছিলো । ল'রাবোরর
লগতে ফুটবল খেলিছিলোঁ । খেলিছিলোঁ
আইছ হকি । মোর শারীরিক গঠনো ল'রার
দরেই আছিল । বাটে-ঘাটে বহুতে মোক
ল'রা বুলি ভূলো করিছিল ।</p>

<p>এদিন এখন ডাঙর আযনাত মোর
প্রতিবিম্ব দেখি মই চিন্তিত হ'লো । বযস
তেতিযা 12-13 বছর হ'ব । কিন্তু ছো঵ালী
হো঵ার কোনো লক্ষণেই নাই । গোটেই
জী঵ন এনেকৈযে থাকিব লাগিব নে কি !
মোর চকুলো ওলাইছিল । মোক সান্ত্঵না
দিবলৈ আগবাঢ়ি আহিছিল মোর দ্঵িতীয
দেউতা : `` ভয করিবলগীযা একো নাই ।
তুমি হযতো কিছু পলমকৈ প্রস্ফুটিত হ'বা ।
আরু কি জানা ? য়িসকলর বিলম্ব ঘটে ,
তেওঁলোক দেখিবলৈ বর সুশ্রী হয ! ""</p>

<p>এইজন দেউতার সৈতে মোর সম্পর্ক
বন্ধুর দরেই আছিল । জী঵ন বা য়ৌবন
সম্পর্কে কৈশোরর সেইখিনি সমযত মোর
মনর বহুত প্রশ্নর উত্তর তেওঁ খরচি মারি
বুজাই দিছিল । এদিন মই মাক
সুধিছিলো, - কেঁচু঵া কেনেকৈ জন্ম হয ?
স্঵ভা঵গত সস্মার্টনেছ আরু গাম্ভীর্য়্য় বজাই
রাখি মাই আঙুলিযাই দিলে দেউতালৈ
আরু দেউতাই মোক বহু কথা ব্য়াখ্য়া করি
মানুহর জন্মবৃত্তান্ত শুনালে ।</p>

<p>অ঵শেষত আহিল `` ছুইট
ছেভেণ্টিন্থ । "" তেতিযামানে মোর শরীরর
প্রখর পরিবর্তন ঘটিছিল । টেনিছ খেলর
জরিযতে বিভিন্ন ঠাই ভ্রমণর কামো আরম্ভ
হৈছে । বাটে-ঘাটে দেখোঁ - মোর বযসর
ল'রা-ছো঵ালীবোরে কেনেকৈ নির্বিবাদে
প্রেম চলাই গৈছে । এজন বযফ্রেণ্ড সম্পর্কে
মোরো কৌতুহল আছিল । কিন্তু কাষত
ছোন কাকো নাপাওঁ ! কোনো এখন খেল
শেষ করি উঠি লকার-রুমর ওচরে-পাজরে
দেখা পাওঁ , -হয কোনোবা কর্মকর্তা ,
সাংবাদিক , নহ'লে এজেণ্টক । নিজকে প্রশ্ন
করোঁ , - এই স্মার্ট ডেকাবোর গ'ল ক'লৈ ?</p>

<p>হাইস্কুলত থাকোঁতেই মোর এজন
বন্ধু আছিল । বযসত অ঵শ্য়ে মোতকৈ চারি
বছরমানে ডাঙর হ'ব । আমি একেলগে
অনুশীলন করিছিলোঁ । বিভিন্ন ঠাইর `` টেনিছ
টুর ""-লৈ তে঵োঁ গৈছিল । তেওঁক মোর
ক্রমাত্ ভাল লগা হৈছিল । তেওঁ আছিল
খেলু঵ৈ , স্মার্ট , অথচ শান্ত প্রকৃতির ; আরু
বযসতো মোতকৈ ডাঙর ।</p>

<p>সেই সমযত ` ছেক্স ' সম্পর্কে মোর
আগত দুটা ধারমা উপস্থিত হৈছিল ।</p>

<p>মাই কৈছিল বিযার আগলৈকে
কুমারীত্঵ বজাই রখা উচিত । আনহাতে
দেউতার মত সম্পূর্ণ বিপরীত । তেওঁর মতে
ভ঵িষ্য়ত সংগীজন কেনেকু঵া হ'ব , সেই
বিষযে আগতেই নিশ্চিত হৈ লব লাগে ।
অ঵শ্য়ে তেওঁ লগতে এইবুলিও সকীযাই
দিছিল য়ে য়িসকলর সৈতে বিযা করার
কোনো পরিকল্পনা নাথাকে , তেওঁলোকর
সৈতে কিন্তু তেনে ওচর সম্পর্ক রাখিব
নালাগে । তদুপরি 21 বছরর আগতে
সেইবোর চিন্তা মনলৈ ননাটো঵েই ভাল ।</p>

<p>সেই সমযত মোর কোনো বিযার
পরিকল্পনা নাছিল । কিন্তু বিচারিছিলোঁ
জী঵নটো উপভোগ করিবলৈ । মোর সেই
বন্ধুজনে তেতিযা প্রাগর স্থাপত্য়-঳িল্পর
স্কুলত চাকরি করিবলৈ লৈছিল । এদিন
তেওঁ মোক তেওঁর বাসস্থানলৈ আমন্ত্রণ
জনালে । বুজিব পারিছিলো তেওঁর
উদ্দেশ্য় । অস্঵স্তিও লাগিছিল । কিন্তু এদিন
মন বান্ধি সঁচাকৈযে তেওঁর ঘর ওলালোগৈ ।
সেইদিনা ঘরত তেওঁর বাহিরে আরু কোনো
নাছিল ।</p>

<p>ভাবিছিলোঁ , জন্ম নিযন্ত্রণর বিষযত
তেওঁর কিছু জ্ঞান আছে । কিন্তু সাতদিনর
পাছত বুজিলো , সেই বিশ্঵াস ভুল ।
উপাযহীন হৈ দেউতার কাষ চাপিলো ।
দেউতাই ক'লে : সেইবোর প্রশ্ন 21 বছরর
পাছত । এইবার মার কাষ চাপিলো । মার
চিন্তা হ'ল ; চিন্তা বেছি হ'ল মোর টেনিছ -
জী঵নক লৈহে । অ঵শেষত মার
উদ্য়োগতেই সমস্য়ার সমাধান হ'ল । ""</p>

<p>এযা আছিল `` বিইং মাইছেলফ ""-ত
টেনিছ সম্রাজ্ঞী মার্টিনার আত্ম-কথা ;
কোনো লুক-ঢাক নরখাকৈ কো঵া কথা ।</p>

<p>তেনেহ'লে ? মার্টিনার এই শিশু-প্রীতির
কারণ সেযাই নে কি ? হযতো সেই অলেখ
শিশুর মাজতে মার্টিনাই বিচারি ফুরে তেওঁর
হেরো঵া `` প্রথম সন্তান ""-র মুখখন !
নিষ্পাপ সেইখন মুখ য়ে ক'ত হেরাল !
-জিতেন গগৈ ।</p>

<p>বিশ্঵কাপ
1990
ভাগ্য়র ভুমিকা :
এইবারর বিশ্঵কাপত এটা কথা
বারুকৈযে প্রমাণিত হৈছিল য়ে , স্কিল ,
টেকনিক , টেকটিকছতকৈও ডাঙর বস্তুটো
হৈছে ভাগ্য় । আরম্ভণির খেলখনর পরা লক্ষ্য়
করিলে দেখা য়ায য়ে , আজে ণ্টিনা দল কেমেরুনত
হাতত পরাজিত হ'ল । রুমানিযার লগত তেওঁলোকে
ড্র করিলে আরু গ্রপর তৃতীয দল হিচাপে দ্঵িতীয
রাউণ্ডত প্রবেস করিলে । প্রি-কো঵ার্টার ফাইনেলত
ব্রাজিলে খেল আরম্ভ হো঵ার লগে লগে গ'ল দিযার
সুযোগ পালে , অথচ কেরেকার দরে খেলু঵ৈযে গ'ল
দিব নো঵ারিলে । মাজতে আজে ণ্টিনা দলে খেলর
গতির বিরুদ্ধে গ'ল দিলে আরু খেল শেষ হো঵ার
ঠিক আগমুহুর্তত ব্রাজিলর মুলারে সন্মুখর আজে
ণ্টিনা দলর অসহায গ'ল রক্ষকক পরাজিত করিব
নো঵ারিলে । ব্রাজিলর পরাজযে য়েনে দির্ভাগ্য়জনক
ঠিক তেনেকৈ আজেণ্টিনার জযলাভ সম্পূর্ণ ভাগ্য়-~
নির্ভর । কো঵ার্টার ফাইনেলত য়ুগোশ্লোভিযার বিরুদ্ধে
আজেণ্টিনা দলে টাইব্রেকারত 3-2 গ'লত জযলাভ
করিলে । মারাডোনাই পেনাল্টি শ্঵টেরে গ'ল দিব
নো঵ারিলে , কিন্তু আজেণ্টিনার গ'লরক্ষক ছেরগিঅ'
গযকোচি঵াই অভা঵নীযভা঵ে শেষ দুটা পেনাল্টি শ্঵ট
রক্ষা করি আজেণ্টিনা দলক বিজযী করিলে । আজেণ্টিনা
দলর এক নম্বর গ'লরক্ষক নেরি পম্পিডু঵ে রাছিযার
রাছিযার লগত খেলোঁতে ঘাত পায । তেওঁ দেশলৈ
ঘুরি য়ায আরু দ্঵িতীয গ'ল রক্ষক গযক'চিযাই খেলর
সুয়োগ লাভ করি অসাধারণ দক্ষতা প্রদর্শন করে ।
আরু এখন কো঵ার্টার ফাইনেলত কেমেরুন দল 3-2
গ'লত ইংলেণ্ডর হাতত পরাজিত হয । অতিরিক্ত
সমযত লিনেকারে পেনাল্টির গ'ল দিযাত ইংলেণ্ড
দলে ছেমিফাইনেলত প্রবেশ করে । সেই খেলখনত
70 ভাগ আধিপত্য় বিস্তার করিছিল কেমেরুণ দলে ।
প্রবীণ গ'লরক্ষক শ্঵িলটনর লগতে ভাগ্য় ইংলেণ্ডর
সহায হৈছিল । ছেমিফাইনেলত ইটালি আরু আজে~
ণ্টিনার খেলখনলৈ লক্ষ্য় করক । খেলর ফলাফল
1-1 হো঵াত দুটা দলে এনেভা঵ে খেলিবলৈ ধরিলে
য়ে টাইব্রেকারে হ'লেহে য়েন তেওঁলোক রক্ষা পরে ।
অর্থাত্ তেওঁলোকে ` ভাগ্য় '-ক এটা সুয়োগ হিচাপে
লব । কার্য়্য়ত দেখা গ'ল ভাগ্য় কিন্তু আজেণ্টিনার
প্রতিহে সুপ্রসন্ন ।</p>

<p>প্রথমখন ছেমিফাইনেলর দরে দ্঵িতীযখন
ছেমিফাইনেলোঁ নিষ্পত্তি হৈছে ` টাইব্রেকারত । '
খেলু঵ৈসকলর টেকনিক , টেকটিকছ নাইবা
প্রসিক্ষকসকলর চিন্তা-ভাবনার প্রতিফলন কিন্তু গ'ল
স্ক'র করার ক্ষেত্রত দেখা নগ'ল । দলসমুহে সমানে-~
সমানে য়ুঁজিছে আরু য়েন বিচারিছে পেনাল্টির সহায
লবলৈ । জার্মেনির মেথুউছ নাইবা স্পেইনর মিছেলর
বাহিরে আন য়িগরাকী খেলু঵ৈযে দুরর পরা গ'ল লক্ষ্য়
রি শ্঵ট লৈছে , তেওঁর নাম গেরি লিনেকার । আনকি
মারাডোনার দরে খেলু঵ৈযেও ( য়ার বাঁও ভরিত আছে
বজ্রর দরে শ্঵ট ) মাথোঁ এবারহে ইটালির বিরুদ্ধে
দুরর পরা গ'ল লক্ষ্য় করি শ্঵ট লৈছিল । বেছিভাগ
সমযত খেলু঵ৈসকলে ড্রিবল করি পেনাল্টি ভিতরত
প্রবেশ করি নিশ্চিত হৈহে গ'ল দিবলৈ বিচারিছিল
নাইবা পেনাল্টির `` সুবিধা "" বিচারিছিল । ফলত
এইবার পেনাল্টির প্রাচুর্য়্য় দেখা গৈছিল আরু য়িদলর
ভাগ্য় ভাল সেই দলেই বিজযী হৈছিল ।
- পুলক লাহিড়ী</p>

<p>আমানুল্লা আরু নাই :
সম্প্রতি প্রাক্তন রাজ্য়িক ফুটবল খেলু঵ৈ
আমানুল্লাই পরলোক গমন করিছে ।
শি঵সাগরর দিখৌ নৈর পারত জন্ম গ্রহণ
করা আমানুল্লাই তেওঁর দৃপ্ত আরু সাহসী
খেলেরে দর্শকক মুগ্ধ করিছিল । এসমযত
দেরগাঁও পুলিছক অসমর অন্য়তম শক্তিশালী
দলরূপে পরিগণিত করাত আমানুল্লাই মুখ্য়
ভুমিকা লৈছিল । মুলত : গু঵াহাটী টাউন ক্লানর
সভাপতি রাধা গোবিন্দ বরু঵ার প্রচেষ্টাত আমানুল্লাহ
1956 চনত গু঵াহাটী টাউন ক্লাবত য়োগদান করিছিল ।
ইযার পাছত তেওঁর খেলর মানর প্রভূত পরিমাণে
উন্নতি হৈছিল । জলন্ধর লিডার্ছর বিরুদ্ধে নাইবা
1958 চনত বার্নপুতর পরম শক্তিশালী বংগ দলর
বিরুদ্ধে তেওঁর খেল স্মরণীয হৈ আছে ।</p>

<p>শরত্ দাস আরু পুতু চৌধুরীর পরবর্তী কালত
কেইগরাকীমান প্রতিভাবান `` বেক ""-র আবির্ভাব
ঘটিছিল অসমর ফুটবল ক্ষেত্রত ; য়েনে-জামাল ,
সুনীল দাস , উমেশ দাস , মণ্টু চৌধুরী , মণি
ঘোশ , ঵হিযা বিশ্঵াস , তোষেশ্঵র দত্ত আরু প঵ন
গোহাঁই ষ কিন্তু এইসকল খেলু঵ৈর তুলনাত
আমানুল্লার খেলর এক বৈশিষ্ট্য় আছিল । টাফ ,
অসাধারণ টেকলিং , সোঁভরিত প্রচণ্ড শ্঵ট আরু
দৃষ্টিনন্দন হে'ড঵'র্ক আছিল আমানুল্লার খেলর
প্রধান বৈশিষ্ট্য় । আরু এটা গুণ , - সেইটো হৈছে
নিজর দল বা রাজ্য়ক অসাধারণভা঵ে ভালপো঵া ।
তেওঁর টিম স্পিরিটে গোটেই দলটোক উজ্জী঵িত
করিছিল । লদতে আছিল তেওঁর তীব্র সংগ্রামী
মনোবৃত্তি । তেওঁ বিশ্঵াস করিছিল য়ে খেলর শেষ
হুইছেল নবজালৈকে খেল শেষ নহয । বিপক্ষে
দল য়িমানেই নামী নহওক লাগিলে , নিজর
স্঵াভা঵িক খেল য়াবলৈ তেওঁ সদায সতীর্থ
খেলু঵ৈসকলক উপদেশ দিছিল । সাহস আরু
শৌর্য়্য়র আন এটি নাম আমানিল্লা বুলি ক'ব
পারি । তেওঁর মৃত্য়ুত আমি হেরু঵ালো এগরাকী
আদর্শ ফুটবল খেলু঵ৈক ।
",0
</p></body></text></cesDoc>