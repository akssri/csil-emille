<cesDoc id="asm-w-commerce-industry-sih04" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-commerce-industry-sih04.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>বুনিযাদি</h.title>
<h.author>ডঃবরা,দাস</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - book-</publisher>
<pubDate>1984</pubDate>
</imprint>
<idno type="CIIL code">sih04</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 08284.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;এই শিল্পত বছরর ছযমাহমানহে কাম চলে । প্রতি বছরে
কারিকরবিলাক প্রায সলনি করি নতুন কারিকর নিযোগ করা হয । এই শিল্পত
কিছুমান পূর্ণভা঵ে কারিকর নিযোগ করা হয আরু কিছুমান হাজিরা ভিত্তিত
নিযোগ করা হয । কিছুমানত আকৌ পরিযালর মানুহে সহায করা দেখা য়ায । 
কাঁহর শিল্প এটি পারিবারিক শিল্প , গতিকে , কারিকরর পরিযালতে ইযার কর্মবোর
কেন্দ্রীভূত হৈ থাকে । এই কারিকরবিলাক অর্থনৈতিকভা঵ে জুরুলা সেযে সামাজিক~
ভা঵ে আগবাঢ়ি আহিব পরা নাই । সেযে এনেকু঵া কঁহারর মৃত্য়ু঵ে তলর কর্মী~
সকলক নিবনু঵া হো঵াত ইন্ধন য়োগায । </p>

<p>কাঁহী , পাণবটা , শরাই , বটা , ভোরতাল , লোটা , ঘটি , চরিযা , চামুচ আদিযে
প্রধান । ইযার উপরিও কিছুমানে বিশেষকৈ আসন , বরকলু ( Borkalu ) হাতী~
খুজীযা বাটি , মাইহাং বাটি , সফুরা আদিও গঢ়ায । </p>

<p>কাঁহ , তাম ( 77 শতাংশ ) আরু টিন ( 23 শতাংশ )র মিশ্র ধাতু । কাঁহর~
শালত কেঁচা কাঁহর উপরিও ভঙা কাঁহ , তুহ মিঠাতেল , বোকা , লা , এঙার , মরাপাট 
আদিও দরকার হয । এই শিল্পজাত সামগ্রীর চাহিদা অসমত 26 শতাংশ , 
অসমর বাহিরর কিন্তু ভারতর ভিতরত 24 শতাংশ , ভারতর বাহিরত 14 শতাংশ
আরু বাকী 36 শতাংশর বিষযে জনা নায়ায । </p>

<p>সর্থেবারীর কঁহার শিল্পই ইযার নিখুঁততা তথা চানেকীর বাবে সমাদর লাভ
করি আহিছে । ইযার বিভিন্ন আকারর তথা বিভিন্ন ধরণর পাত্র আরু সামগ্রীবোরত
শিল্পীযে অংকিত করা চিত্রবোরর বাবে ই সমাদৃত । </p>

<p>অন্য়ান্য় হস্তশিল্পবোর লোপ পো঵ার লগে লগে এই শিল্পতো ইযার প্রভা঵
পরিছে । ইযার কারণ কেবাটাও । কেঁচা কাঁহর দাম আগতকৈ বহুগুণে বাঢ়ি
য়ো঵ার ফলত মূলধন আগতকৈ অধিক লগা হৈছে । সেযে মৃতপ্রায শিল্প কিছুমান
বাদ দিবলগীযা হৈছে । </p>

<p>এই কঁহার শালবোরর কার্য়করী মূলধনর য়থেষ্ট আ঵শ্য়ক হয । শালবোর কেঁচা
কাঁহর দাম বৃদ্ধির লগে লগে মূলধনর পরিমাণ বৃদ্ধি করিব লগা হৈছে । বিভিন্ন ধরণে
সংগ্রহ করা মূলধনর তালিকারপার বুজা য়ায য়ে এতিযাও এই শিল্পবোর সুবিধাবাদীর 
কবলরপরা মুক্ত হ'ব পরা নাই । //
1933 চনত গঠিত আরু 1939 চনত পঞ্জীভূত সমবায সংঘত বর্তমান এই
শিল্পর 64 শতাংশ সদস্য়রে অংশ আছে । ইযার 64 টা পঞ্জীভূত শাখা আছে । 
ইযাত মুঠতে 21 জন কর্মচারী নিযোগ করা হৈছে । কাঁহর শিল্পর উন্নযন আরু
বিকাশক আগত রাখিযে এই সমবায সংঘর জন্ম হৈছিল । য়থেষ্ট পরিমাণে কার্য়করী
মূলধন নথকাত এই সংঘই ইযার সদস্য় শালবোরক 20 শতাংশ কেঁচামালহে
য়োগান ধরিব পারে । </p>

<p>এই সংঘই সদস্য় সকলরপরা 38,000.00 টকা অংশ মূলধন আরু রাজ্য়িক
চরকারর পরা 1,00,000 টকার মূলধন সংগ্রহ করিবলৈ সমর্থ হয । 1956 চনত
উদ্য়োগবিভাগর পরা আধুনিক য়ন্ত্র-পাতি কিনার বাবে 1,95,000.00 টকার
ঋণ গ্রহণ করে । </p>

<p>এই সংঘক ইযার সদস্য় শালবোরর বাবে 1,50,000.00 টকা মূল্য়র 200
কিলোগ্রাম কেঁচামাল দরকার হয । এই শালবোরে কেঁচামালর অভা঵ত 33 শতাংশ
হে উত্পাদন করিবৈল সমর্থ হয । </p>

<p>সর্থেবারীর এই পরম্পরাগত কাঁহর শিল্পই য়েনেকৈ ইযার নিপুণতা তথা নানা
চানেকীর বাবে ভারতর বাহিরতো সমাদর লাভ করিব পারিছিল , বর্তমানেও 
সকলো সুবিধা পালে এই সু-নাম অক্ষুণ্ণ রাখিবলৈ সমর্থ হ'ব । চরকারর উদ্য়োগ
বিভাগরপরা আর্থিক সহায তথা আধুনিক য়ন্ত্র-পাতির লগতে উপয়ুক্ত কেঁচামালর
য়োগান পালে ই বিশ্঵র বজারত এক বিশেষ স্থান অধিকার করিবলৈ সমর্থ হ'ব আরু
ই এক হস্তশিল্পর পরা উদ্য়োগলৈ উন্নীত হৈ ভারতীয অর্থনীতিত এক বুজন
পরিমাণর সহায আগ বঢ়াব পারিব । </p>

<p>হস্তশিল্পজাত পিতলর সামগ্রী আরু হাজো :
ধাতু ব্য়঵হার হো঵ার দিনরেপরা ইযার ব্য়঵হারর বিষযে অনুমান করিব পরা
য়ায । 5000 বছরর আগরে পরা পিতলর ব্য়঵হার হো঵া প্রমাণ পো঵া য়ায । 
ভারতীয সভ্য়তাত ধাতুর ব্য়঵হারর মূল কারণ হৈছে - এই ধাতু শিল্পজাত
সামগ্রীবোর মানুহর সামাজিক জী঵নত ব্য়঵হারর উপয়োগী আরু এই শিল্পবোর
সামাজিক জী঵নর লগত ওতঃপ্রোতভা঵ে জড়িত । </p>

<p>উপয়ুক্ত ব্য়঵হারয়োগ্য় আরু মনোমোহা পিতলর হস্তশিল্পজাত সামগ্রী উত্পাদক
শালবোর কামরূপ জিলার হাজোত কেন্দ্রীভূত হৈ থকা দেখা য়ায । অতীজরেপরা
পরম্পরাগতভা঵ে চলি অহা এই হস্তশিল্পই ভারতর বাহিরর বজারো দখল করার
লগতে এক বুজন পরিমাণর মানুহক নিযোগর সুবিধা দিছে । এই শিল্পত তৈযারী
বস্তুবোরে সামাজিক জী঵নর লগত ওতঃপ্রোতভাবে জড়িত হৈ আছে । </p>

<p>পিতল হৈছে এটি মিশ্র ধাতু । তাম আরু দস্তার ( zinc ) এই শংকর ধাতুর
ফলি ( fali ) আরু চাক ( chak ) এই শিল্পর একমাত্র কেঁচামাল । </p>

<p>হাজোর স্থানীয কারিকরর দ্঵ারা নির্মিত বিভিন্ন ধরণর পিতলর সামগ্রীবোরর
নিপুণতা আরু সৌন্দর্য়ই ইযার য়শস্য়া কঢ়িযাই অনার লগতে চাহিদা বৃদ্ধি করাত
সহায করিছে । হাজোর কারিকরসকল নির্মিত সামগ্রীবোরর ভিতরত উল্লেখয়োগ্য়
সামগ্রীবোর হ'ল 1 ) পিতলর শরাই , 2 ) পিতলর কলহ , 3 ) লোটা , 4 ) ঘটি , 
5 ) ডমুরি ( Dumari ) 6 ) থালী , 7 ) হেতা , 8 ) খরাহী , 9 ) টৌ ,
10 ) চরিযা । </p>

<p>হাজোর এই পিতল শিল্পত নিযোজিত মানুহবিলাক সাধারণতে পিছপরা শ্রেণীর । 
ইযার বাসিন্দা প্রায বিলাকর পিতলর সামগ্রীর ব্য়঵সায । এই ব্য়঵সাযবোরর 17
74 শতাংশই সমবায সংঘর পরা আর্থিক সহায লাভ করিবলৈ সক্ষম । বাকীবিলাকে
কোনো ব্য়ক্তিগত মালিকানা বা ব্য়঵সাযীর তলত কাম করে । এই ব্য়঵সাযীবিলাকর
শতাংশর আয 2000.00 টকার তলত , 4 শতাংশর 2000.00 টকার পরা 3000.00
টকার ভিতরত । এই ব্য়঵সাযর বাবে তামর ফলি আরু ` চাক ( chuk ) কেঁচামাল
হিচাপে ব্য়঵হার করা হয । </p>

<p>ব্য়঵সাযীসকলে সন্মুখীন হো঵া দুটা মুখ্য় সমস্য়া হৈছে কার্য়করী মূলধনর অভা঵
আরু কেঁচামালর অপর্য়াপ্ত য়োগান ( inadequete ) । তারোপরি উপয়ুক্ত বজারর
অবিহনে বিক্রীয়োগ্য় ব্য়ক্তিগত ক্রেতা বা দালালর ওচরত বিক্রী করিবলগীযা হয । 
কারিকরে কেঁচামাল সংগ্রহ করার সমযত প্রাযবোরে ব্য়ক্তিগত ব্য়঵সাযীর ওচরত
চুক্তিবদ্ধ হো঵া দেখা য়ায । এই শিল্পজাত সামগ্রীর একমাত্র প্রচার হৈছে কোনো
প্রদর্শনীত য়োগদান করা । ইযার উপরিও বজারর বিষযে উপয়ুক্ত খবর নোপো঵া
আধুনিক কারিকরী জ্ঞান থকা কারিকরর অভা঵ । //
বিভিন্ন ধরণর হস্তশিল্পজাত সামগ্রীবোরর ভিতরত টৌ আরু চরিযাবোরর চাহিদা
আটাইতকৈ বেছি দেখা য়ায । </p>

<p>চরকারী অনুষ্ঠান তথা বেংকরপরা সহায লেখত ল'বলগীযা ধরণর নহয । 1972
চনতো হাজোত খোলা এলাহাবাদ বেংকর শাখাই প্রতিজন ব্য়঵সাযীক মাত্র
1500.00 টকালৈহে শতকরা 4.00 টকা সুত হারত 36 টা কিস্তিত পরিশোধয়োগ্য়
ঋণ দিযে । 1977-78 চনত উক্ত বেংকে 10 জন কারিকরক মুঠ 2500.00 টকার
ঋণ দিযে । কিন্তু ঋণ দিযার ক্ষেত্রত উক্ত বেংকে আকৌ পিছ হুঁহকি আহিব
লগাত পরে । 1981 চনত এই বেংকর অনাদায ঋণর পরিমাণ হযগৈ 21300.00
টকা । বেংকর ঋণ পরিশোধ করার ক্ষেত্রত সিমান আগ্রহী নহয , কিন্তু ঋণ
লো঵ার ক্ষেত্রত য়থেষ্ট আগ্রহ দেখা য়ায । তারোপরি গাঁ঵র কিছুমান মুখ্য় ব্য়ক্তিযে
ঋণ পরিশোধর বিপক্ষে মত পোষণ করা দেখা য়ায । পিতলর বাচন-বর্তন গঢ়ো঵া
হস্তশিল্প য়দিও বহুদিনীযা পুরণি , তথাপি সিমান আগবাঢ়ি আহিব পরা নাই । </p>

<p>হাতীদাঁতর শিল্প :
অসমর বনজ সম্পদর ভিতরত হাতী এটা অমূল্য় সম্পদ । হাতীর দাঁতর পরাই
এসমযত অসমর বরপেটাত গঢ়ি উঠিছিল বিশ্঵র বজারত সমাদর লাভ করা 
হাতীদাঁতর সামগ্রী নির্মাণ করা হস্তশিল্প । অসমর হাবিত থকা হাতীরপরা
পো঵া দাঁতর নানা ধরণর অলংকার পাতির লগত চালে চকুরো঵া সামগ্রী নির্মাণ
করা শিল্প কেন্দ্রীভূত হৈ উঠিছিল বরপেটাত । বিভিন্ন ধরণর চানেকী
খোদিত হাতীদাঁতর বিভিন্ন ধরণর সামগ্রীবোরর হস্তশিল্প বর্তমান মৃতপ্রায । এই
শিল্পর একমাত্র কেঁচামাল হৈছে হাতী দাঁত । চোরাং চিকারী আরু চোরাং বেপারীর
দ্঵ারা এই হাতীদাঁতবোর চোরাংভা঵ে দেশর বাহিরলৈ য়ো঵ার ফলত এই শিল্পই
কেঁচা মালর অভা঵তে মৃত্য়ুমুখত পরিব লগা হৈছে । </p>

<p>হাতীদাঁত শিল্প য়ে অতি পুরণি , বুরঞ্জীযে তার প্রমাণ দিযে । প্রাগজোতিষপুরর
রজা ভগদত্তই য়ুধিষ্ঠিরর রাজসূয য়জ্ঞ উপলক্ষে দিযা উপহারর সামগ্রীর ভিতরত
` অশ্঵সার নির্মিত পাত্র ' আরু ` হাতীদাঁতর তরো঵ালো ' আছিল । কামরূপর রজা
কুমার ভাস্কর বর্মাই উত্তর ভারতর রজা হর্ষবর্দ্ধনলৈ ` হাতীদাঁতর কুঞ্জল ' আরু
` গজমুক্তা ' পঠাইছিল । </p>

<p>1923 চনত শিল্পী ভগ঵ান দাসে অসম হাতীদাঁতর Assam Ivory works 
কারখানা স্থাপন করে । 1926 চনত পাণ্ডুত বহা কংগ্রেছ অধিবেশনর প্রদর্শনীত
হাতীদাঁতর কৃষ্ণার্জুনর রথ , হর-পার্বতীর মূর্তি , নটরাজর মূর্তিযে অসমর বাহিরতো
সন্মান অক্ষুণ্ণ রাখিছে । বর্তমানর ( Assam Ivory works )র নামফলির বাহিরে শিল্প
বুলিবলৈ নোহো঵া হৈছে একমাত্র কেঁচামালর অভা঵র বাবে । চরকারে বিভাগীয
কর্ত্তৃপক্ষর দ্঵ারা উপয়ুক্ত কেঁচামালর য়োগান ধরিব পারিলে ই পূর্বর গৌর঵ ঘূরাই 
আনিব পারিব বুলি আশা করা য়ায । </p>

<p>পরম্পরাগতভা঵ে চলি অহা হস্তশিল্পবোর বহুল ব্য়঵সাযিক ভিত্তিক করিবলৈ
ললে আরু ইযাত নিযোজিত মূলধনর পরিমাণ বৃদ্ধি করিব পারিলে অর্থনৈতিক
উন্নযনর লগতে নিযোগর পরিমাণো বৃদ্ধি হ'ব । </p>

<p>অসমত ক্ষুদ্র শিল্পর প্রসার য়থেষ্ট পরিমাণে হৈছে । 1961 চনত 197 টা
পঞ্জীযন করী ক্ষুদ্র শিল্পর ঠাইত 1978 চনত 3302 টা হযগৈ । </p>

<p>1983 চনর ক্ষুদ্র শিল্পর সংজ্ঞা অনুয়াযী য়িবিলাক শিল্পত কলকব্জা আদি স্থাযী
সম্পত্তির বিনিযোগর পরিমাণ 20 লাখ টকাতকৈ কম সেই বিলাকক ক্ষুদ্র শিল্প
বোলে । আকৌ য়ি বিলাক সহাযকারী শিল্পত ( Ancillary ) বিনিযোগর পরি~
মান 25 লাখতকৈ কম সেই বিলাকক সহাযকারী শিল্প বোলে । এই ধরণর শিল্প
বিলাক ডাঙর ডাঙর শিল্পর কোনো অংশ উত্পাদন করিবর বাবে প্রতিষ্ঠা করা
হয । 1977 চনত ক্ষুদ্র শিল্পর লগতে আন এটা একেবারে সরু শিল্পর গঠনর কথা
চরকারে আগবঢ়াইছে । এইবিলাক শিল্প গোটত বিনিযোগ করা  টকার
পরিমাণ 2 লাখতকৈ বেছি হ'ব নো঵ারিব । </p>

<p>1977 চনর শিল্পনীতি অনুয়াযী প্রতিখন জিলাত উদ্য়োগ কেন্দ্র স্থাপন করিবলৈ
সিদ্ধান্ত লো঵া হৈছিল । জিলাখনর শিল্প বিষযর সকলো বিলাক সমস্য়া এই জিলা
উদ্য়োগ কেন্দ্রই সমাধান করিব । এই উদ্য়োগ কেন্দ্রই জিলার সকলো বিলাক কেঁচা
সামগ্রীর তথ্য়পাতি , ঋণর ব্য়঵স্থা উত্পাদিত দ্রব্য়র বজার , মান নিযন্ত্রণ ( Quality
Control ) গ঵েষণা , আরু সম্প্রসারণ আদি সকলো বিলাক সুবিধা আগবঢ়াব । 
এই কেন্দ্রই বিভিন্ন ক্ষুদ্র শিল্প উন্নযন সংস্থার লগত সমন্঵য রাখি কাম করিব । ক্ষুদ্র
শিল্প উন্নযনর বাবে বিভিন্ন সংস্থা য়েনে সর্বভারতীয তাঁতশাল পরিষদ , ক্ষুদ্র শিল্প
উন্নযন নিগম , ক্ষুদ্র , শিল্প সে঵া কেন্দ্র , ক্ষুদ্র সম্প্রসারণ সে঵া কেন্দ্র , ঔদ্য়োগিক
বিকাশ বেংক আদিযে বিভিন্ন সহায আগবঢ়ায । //
অসমর উদ্য়োগ বিকাশর বাবে পরিকল্পনার কালছো঵াত চরকারে বহুতো
ব্য়঵স্থা গ্রহণ করিছিল । সেই বিলাকর ভিতরত উদ্য়োগ পামর কথা উল্লেখ
করিব পারি ( Industrial Estate ) প্রথম পরিকল্পনাত উদ্য়োগ পামর
মূল লক্ষ্য় আছিল দেশর উদ্য়োগী সকলক শিল্প প্রতিষ্ঠা করিবর বাবে একে~
ঠাইতে সকলো সুবিধার য়োগান ধরা । সাধারণতে উদ্য়োগ পাম বিলাকত
কারখানার বাবে ঘর , পানী য়োগান , বিদ্য়ুত রাস্তা , বেংক , ডাকঘর , হোটেল , 
ডাক্তরখানা আদি সকলো বিলাক শিল্প প্রতিষ্ঠানর সুবিধা দিযা হয । এই
বিলাকর উপরিও বহুতো উদ্য়োগী একেলগ করি সমূহীযা উন্নযন , আধুনিক কৌশল , 
সমূহীযাকৈ কেঁচা সামগ্রী ক্রয আরু সমূহীযাকৈ উত্পাদিত সামগ্রী বিক্রী আদির
ব্য়঵স্থা করা হয । বর্তমান অসমত 10 খন উদ্য়োগ পামে 1500 শতকৈও অধিক
লোকক নিযোগর সুবিধা দিযার লগতে বছরি 229 লাখ টকার সামগ্রী উত্পাদন
করে । </p>

<p>কাঠ , বাঁহ-বেতর শিল্প :
খরাহী জাতীয শিল্প ( Basket ware industry ) অসমর প্রাযকেইখন ঠাইতে
থকা দেখা য়ায য়দিও গু঵াহাটী , নলবারী , তেজপুর , উত্তর লক্ষীমপুর , ডিব্রুগড় , 
য়োরহাট , নগাঁও , কার্বি আংলং শিলচর , করিমগঞ্জ আরু বদরপুরত বৃহদাকারত
ব্য়঵সাযিক হস্তশিল্পর গঢ় লো঵া দেখা য়ায । অসমত এই শিল্পর 600 টাতকৈ
অধিক শাখা ( unit ) আছে । আরু প্রতিটো শাখাতে 20 জনর পরা 50 জনলৈ
কর্মসংস্থান পাইছে । ইযার উপরিও দিন হাজিরা করা কর্মীর সংখ্য়াও কম নহয । 
ইযার উপরিও পরিযাল ভিত্তিত কিছুমান পরম্পরাগত ভাবে চলি , এই শিল্প দেশর
সকলো ঠাইতে বিযপি পরিছে । </p>

<p>এই শিল্পর বিকাশ আরু উন্নযনর বাবে অসম চরকারর বেচা-কিনা নিগম আরু
উত্তর-পূর্ব হস্ত আরু তাতশিল্প উন্নযন নিগমে চেষ্টা করি আহিছে । এই শিল্পর
বাবে লাগতিযাল কেঁচামাল অসমর হাবিত প্রচুর পরিমাণে পো঵া য়ায । বাঁহজাতীয
গছ , বেত আরু ঘাঁহজাতীয বন ইযার মূল কেঁচামাল । কাছার জিলাত পো঵া
লাজাই , জাতি আরু গোলা বেত ইযার বাবে উত্তম কেঁচামাল । </p>

<p>এই শিল্পর সামগ্রীবোর ইযার স্থানীয মানুহে ব্য়঵হার করার উপরিও দেশ
বিদেশলৈ প্রচুর পরিমাণে রপ্তানি করা হয । এই সামগ্রীবোরর ভিতরত বাঁহর
কুলা , চালনি , ঢারি , জাপি , জাকৈ আদি , বিভিন্ন আকার আরু ধরণর খরাহী । 
নলবারীত বিস্তৃত ব্য়঵সাযিক ভিত্তিত জাপি সজা হয । এই বিভিন্ন ধরণর জাপি
অসমর বাহিরলৈ রপ্তানি করা হয । </p>

<p>বেতর আচবাব বনো঵া শিল্প অসমর প্রাযবোর ঠাইতে লোপ পাই আহিছে । 
অসমত বর্তমান প্রায এনেকু঵া 250 টা মান ব্য়঵সাযিক প্রতিষ্ঠান আছে । এই
শিল্পত প্রায 3000 মানুহ নিযোজিত হৈ আছে । অসম চরকারর বেচা-কিনা
নিগমে ইযার উত্পাদিত সামগ্রীবোর বিক্রী আরু রপ্তানির দাযিত্঵ লয । নলবারী , 
মংগলদৈ , শিলচর , য়োরহাট , ডিব্রুগড় আরু তিনিচুকীযাত থকা ইযার শাখার
দ্঵ারা এই উত্পাদিত সামগ্রীবোর সংগ্রহ করা হয । এই নিগমর অসমর বাহিরত
দিল্লী আরু কলিকতা আরু শ্঵িলঙত বিক্রী ভাণ্ডার আরু ( Show room ) 
আছে । নলবারীত থকা ইযার উত্পাদন শাখাটোত টেবুল-চকী , মুড়া , চোফা , 
খরাহী জাতীয তথা ঘরর সৌন্দর্য় বর্দ্ধক সামগ্রী উত্পাদন করা হয । </p>

<p>এই শিল্পর বাবে কেঁচামাল ব্য়ক্তিগত ব্য়঵সাযীর পরা সংহ্রহ করা হয । বর্ত~
মান অসমর হাবি-বন কমি অহার লগে লগে এই শিল্পই কেঁচামালর অভা঵ অনুভ঵
করিব লগা হৈছে । ঢারি কঠ জাতীয সামগ্রীবোরর কেঁচামাল অসমর হাবি বন~
নিত প্রচুর পরিমাণে পো঵া য়ায । ঢারি আরু কঠ জাতীয সামগ্রীবোরে ভারতর
বাহিরত সমাদর লাভ করিবলৈ সক্ষম হৈছে । এই ঢারি কঠর ভিতরত শীতল
পাটীর চাহিদা আটাইতকৈ বেছি দেখা য়ায । এই শীতল পাটী তৈযার করা
প্রতিষ্ঠান প্রথমতে কাছার জিলাতে সীমাবদ্ধ আছিল । বর্তমানে অসমর শীতল
পাটী উত্পাদন কেন্দ্র 190 টা মান আছে আরু ই বছরি প্রায 25 লাখ টকার সামগ্রী
উত্পাদন করে । সমবায ভিত্তিত গঢ়ি উঠা Katakhal Patikar Co-opera~
tion Ltd. এ ভালেমান বছর বছরি লাখ টকার সামগ্রী উত্পন্ন করি আহিছে ।
এই শীতল পাটী মজিযা , বের , টেবুল আদির উপরিও আবরণ হিচাপে ব্য়঵হার করা
হয । এই সমবায সমিতির উত্পাদন কেন্দ্রত আ঵শ্য়ক হো঵া কেঁচামালর বাবে
বর্তমান 40 বিঘাতকৈ অধিক মাটিত গছর খেতি করিবলৈ লৈছে । বাঁহ তথা
ঘাঁহজাতীয সামগ্রীবোরর পরা উত্পন্ন করা ঢারি , কঠ আদিও নানা কামত
ব্য়঵হার হয । </p>

<p>অসমর হাবিত বনজ সম্পদেরে ভরপুর । এই হাবিত প্রচুর পরিমাণে পো঵া
মূল্য়বান কাঠরপরা নানা ধরণর সামগ্রী হস্তশিল্পত উত্পন্ন করা হয । এই কাঠর
পরা সামাজিক জী঵নত লাগতিযাল প্রাযবোর সামগ্রী তৈযার করি লো঵া হয হস্ত
আরু তাঁত শিল্পর য়োগেদি । বর্তমানে ব্য়঵সাযিক ভিত্তিত 25 টা মান হস্তশিল্প
কাঠর সামগ্রী উত্পাদনার্থে অসমত বর্তমান । শিলচর , গু঵াহাটী , তেজপুর আরু
ডিব্রুগড়ত এই শিল্পবোর কেন্দ্রীভূত হৈ থকা দেখা য়ায ।</p>

<p>মৃত্-শিল্প :
মাটির নানা পুতলা তথা নিত্য় ব্য়঵হার্য় সামগ্রী গঢ়ো঵া মৃত্ শিল্প অসমর সকলো
ঠাইতে সিচঁরিত হৈ আছে । এই শিল্পত হীরা আরু কুমার নামেরে পিছ পরা
শ্রেণীর লোক সাধারণতে নিযোজিত হৈ আছে । কেঁচামালর প্রচুরতার বাবে এই
শিল্পও পরম্পরাগত ভা঵ে চলি অহাত সুবিধা হৈছে । তারোপরি কেঁচামাল ক্রযর
কারণে বা কার্য়করী মূলধনর কারণে বৃহদাকারর মূলধনর আ঵শ্য়ক নহয । </p>

<p>আটাইবোর হস্তশিল্পর লুপ্ত হৈ অহার কারণবোর বিচারি চালে দেখা য়ায য়ে 
প্রাযবোরতে উপয়ুক্ত পরিমাণে কেঁচামাল য়োগানর অভা঵ , কার্য়করী মূলধনর অভা঵
উপয়ুক্ত বজারর অভা঵েই আরু আধুনিক কারিকরর অভা঵ে মূল কারণ । চরকার
তথা অন্য়ান্য় বিত্তীয অনুষ্ঠানর পরা আর্থিক সাহায়্য়ই আশানুরূপ সুফল লাভ নকরাকৈ 
প্রাযবোর শিল্প মহাজনর কবলর পরা মুক্ত করি বিভাগীয চরকারে এই শিল্পবোরলৈ
কম সুদ হারত বিত্তীয সাহায়্য় আগবঢ়াব পারিলে , এই মৃতপ্রায শিল্পত কেঁচামালর
য়োগান নিযমীযা আরু আ঵শ্য়ক অনুসরি করিব পারিলে ই লাভ দেখু঵াবলৈ সমর্থ
হ'ব । 
 +&gt;*</p>

<p></p>

<p></p>

<p></p>

<p>",0
</p></body></text></cesDoc>