<cesDoc id="asm-w-science-biology-abl09" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-science-biology-abl09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>প্রাণীজগ</h.title>
<h.author>ডিঃচলিহা</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - book-</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">abl09</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 1274.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;চরাই আরু আমি
চরাই সৃষ্টি মানুহ সৃষ্টির বহু আগত । সেইপিনরপরা চালে মানুহ
সৃষ্টি নোহো঵া হ'লেও চরাইর জী঵নত একো বাধা নোপো঵াটো঵েই স্঵াভা~
঵িক । বরং মানুহর অত্য়াচারতে লুপ্ত হ'ব লগা বহুতো চরাই হযতো
আজিও জীযাই থাকিলেহেঁতেন । কিন্তু মানুহর জী঵নত , জীযাই থকার
ব্য়঵স্থাত চরাইর অ঵দান অনেক । মান঵ সভাত্য়ার আগরেপরাই চরাইর
লাগত মানুহর এটা নিবিড় সম্পর্ক গঢ়ি উঠিছে । এই সম্পর্ক রখারপরা
চরাইর একো লাভ হো঵া নাই , বরং লাভবান হৈছো আমি মানুহবিলাক ।
তথাপি য়ে চরাযে প্রকৃতির নির্দেশ মানিযেই হওক বা নিজর সহিষ্ণুতা গুণর
কারণেই হওক - একো ওজর-আপত্তি করা নাই তাতেই আমি সুখী
হো঵া উচিত ।</p>

<p>প্রথম কুকুরাই ডাক দিওঁতেই গাঁ঵র জীযরী-বো঵ারীহঁতে ঘরর বাহী-~
বনত হাত দিযে , হালো঵ার কান্ধত নাঙল উঠে , গরালত বন্দী হৈ থকা হাঁহ ,
পার , কুকুরা ছাগলীকেইটাই দিনটোর স্঵াধীনতাকণর আনন্দতে নিজ নিজ
গরালরপরা গীত গাই গাই ওলাই আহে । ইহঁতর গীতর সুরর লগতে
সুর মিলাই কাউরীযেও রাগ টানে । রাতিপু঵াতেই এই চরাইসমূহর গীত-~
মাত শুনিলে এনেহে লাগিব য়েন সিহঁতে কিবা উচ্চাঙ্গ সঙ্গীতর আখরাহে
করিছে । পাছে প্রকৃততে সিহঁতে এলেহু঵া মানহজাককহে কামত লাগিবলৈ
কাকূতি-মিনতি করিছে । ঠিক তেনেকৈ জাক জাক ভাটৌ , বগলী , কাউরীর
ঘরমু঵া য়াত্রাই আমাক সন্ধিযার আগজাননী দিযে । পথারত পরা বগলী ,
কণামুছরির জাকে পথারত মাছ থকার উমান দিযে । ডা঵রীযা বতরত
ম'রা চরাইর কর্কশ শব্দ আরু পানীপিযা চরাইর ` পিউ ' ` পিউ ' চিঞরে দীঘল~
ঠেঙীযাক আমন্ত্রণ জনায । ফেঁচার কুরুলিযে য়েনেকৈ কারোবার মঙ্গলজনক
বাতরির আগজনানী দিযে , তেনেকৈ ` নিউ ' ` নিউ ' মাতে কারোবার মৃত্য়ুর
সংকেত দিযে । হুদু আরু শগুণ ঘরত পরিলে অমঙ্গল হয বুলি মানুহর
বিশ্঵াস । কুলি-কেতেকীর মাতে বসন্তর আগজাননী দিযে আরু ডেকা-~
গাভরুর জী঵নত য়ৌ঵নর জোঁ঵ার তোলে । পার চরাই শান্তির প্রতীক ।</p>

<p>বান্দর জাতীয প্রাণীরপরা য়দি মানুহর উত্পত্তি হো঵া কথাটো মানি
লো঵া হয , তেন্তে এইটোও মানি ল'ব লাগিব য়ে মানুহ অতীজতে ফলা~
হারীযেই আছিল । কিন্তু কিহর প্রভা঵ত পরি
মানুহ মাংসাহারী হ'ল - সি আজিও এক রহস্য়
হৈযেই আছে । মানুহর সভ্য়তাই য়ে কাক কি করা নাই । কুকুর , মেকুরী
আদি মাংসাহারী প্রাণীক ভাত খু঵াই ভতু঵া করিলো আরু নিজে ফলাহারী
প্রাণী হৈ আমি মাছ-মাংসর জুতি লো঵াত লাগিলো । তদুপরি আমার
মাংসলোভো ইমান বাঢ়ি গ'ল য়ে অন্য় প্রাণীর কথা বাদ দিও আমার এনে
লোভর ফলত বহুতো চরাইর জাতেই পৃথি঵ীরপরা লুপ্ত হ'ব লগা হ'ল ।
নিউজিলেণ্ডর নো঵াচরাই , মাদাগাস্কারর হস্তীপক্ষী , উত্তর আমেরিকার
পেচেজার পার ইত্য়াদি মাংসলোভী মানুহ কারণেই পৃথি঵ীরপরা মেলানি মাগিব
লগা হ'ল ।</p>

<p>চরাইর মাংস আরু কণী এবিধ অতি উত্তম প্রটিনয়ুক্ত খাদ্য় । য়ুক্ত~
রাষ্ট্রত চরাই পোহাটো খেতিরপরা গৈ এতিযা ডাঙর ডাঙর উদ্য়োগত
পরিণত হৈছেগৈ । পৃথি঵ীর লাখ লাখ চিকারী হাবিযে বননিযে চরাই নিরী~
ক্ষণ করাত ব্য়স্ত । অন্য়দেশত এই উদ্য়োগসমূহ আরু চিকারে রাজকোষো
য়থেষ্ট পরিমাণে টনকিযাল করে । আমার দেশতো হাঁহ-কুকুরার খেতি
করিবলৈ লো঵া হৈছে । পাছে তারপরা হো঵া উপার্জনেরে বিষযাসকলর
খরচরে জোরা নমরে । আনহাতে ঘরু঵াভা঵ে হাঁহ-কুকুরা পুহি অ঵স্থা টন~
কিযাল করার ব্য়঵স্থা আমার বহুতে করিছে । চিকার এতিযাও এচাম
ধনীক সম্প্রদাযর লাহবিলাহর বস্তু হৈযেই আছে । সুখর বিষয য়ে ইতিমধ্য়ে
বিজ্ঞানীসকলর নেরানেপেরা চেষ্টার ফলত কৃত্রিম প্রজননর দ্঵ারা উন্নত জাতর
হাঁহ , কুকুরা আদিরো সৃষ্টি করা হৈছে । চীন দেশত দুই-এজাত চরাইর
বাহরপরা তৈযার করা চুরুহাও মানুহে খায । এইবাহবিলাক চরাযে মুখর
লালটিরে তৈযার করার কারণে হেনো বর তৃপ্তিদাযক হয । //
চরাইর পাখির দ্঵ারা গারু , লেপ আদি নানান তরহর বস্তু তৈযার করিব
পারি । আগর দিনত রাজহাঁহর পাখি এখনর গুরিটো জোঙা করি পাখি
কলম তৈযার করা হৈছিল । পাছে নিজরা-কলম ওলো঵ার পাছত পাখি-~
কলম অচল হৈ পরিল । আমেরিকার - রেড্
ইণ্ডিযানবিলাকে উত্স঵ আদিত মূরত বিভিন্ন
পাখি গুজি লয । চরাইর পাখিরে গা-মূর সজো঵াত আমার পাহারী
লোকসকলরো এটা চখ্ আছে । 15 শতিকার শেষ ভাগলৈকে পৃথি঵ীর
প্রায সকলো দেশতে চরাইর পাখিরে গা সজো঵াটো এক বিশেষ ধুনেই
আছিল । ধনু-কাঁড় ব্য়঵হার করাসকলে কাঁড়র গুরিরফালে দুখনমান পাখি
লগাই লয , য়াতে কাঁড়পাত - নির্দিষ্ট পথেদি বহুত দূরলৈ য়াব পারে । চরাইর
পাখিরে কাণ খজু঵াইযো আমার বহুতে কাণর খজু঵তি মারে । ব্রাজিল
আরু আর্জেণ্টিনাত রিযা-চরাইর নেজর পাখিরপরা দাস্তার ( Duster )
তৈযার করা হয । আমার ভারতবর্ষতো বিভিন্ন চরাইর রং বিরঙর চুটি
টুটি পাখিরপরা ধূলি-মাকতি চাফা করা ঝারু তৈযার করা হৈছে । ম'রা
চরাইর পাখিরপরা বিচনী আরু পুতলা আদি তৈযার করা হয । বেডমিণ্টন
খেলর কর্কবিলাকো চরাইর পাখিরেই তৈযারী ।</p>

<p>ছাণ্টাক্রুজ দ্঵ীপর অধিবাসীসকলে সরু মৌপিযা নামর চরাই এজাতর
পাখিরে ফিটা ( belt ) তৈযার করে এই ফিটাকে তেওঁলোকে মুদ্রা হিচাপে
ব্য়঵হার করে । এনে দহডালমান ফিটার মূল্য়রে হেনো এগরাকী কন্য়াকে
পাব পারি । কোনো কোনো ঠাইর মানুহে ভাটৌর পাখিরে নেক্লেচ্
পিন্ধে । জাতীয শিক্ষার্থী বাহিনী আরু আন কিছুমান মিলিটেরী সংগঠনত
মূরর টুপীত চরাইর পাখি লগাই লো঵া হয । আফ্রিকার পাহারী লোক~
সকলেও মূরর টুপীত পাখি লগাই লয । এস্কিমোবিলাকে হাঁহ আরু অ'ক
চরাইর ছালেরে চোলা আদি বনাই লয ।</p>

<p>আপোনার য়দি ঘরর আশে-পাশে , রাস্তাই-ঘাটে কিবা জন্তু কিবা
প্রকারে মরে , আপোনার প্রথম চিন্তা হ'ব - মিউনিচিপেলিটি অফিচত খবর
দিযা হয কেনেকৈ ? দ্঵িতীয চিন্তা হ'ব য়ে খবর পঠালে
বা মানুহ পঠিযায কেতিযা ? তদুপরি বক্চিছ্
আদিতো আছেই । পাছ তাকে নকরি চকু-কাণ মুদি এটামান দিন কটাই
দিব পারিলেই আপুনি রক্ষা পরিব । দর্খাস্তর প্রযোজন নাই , বক্চিছো
ইহঁতক দিব নলাগে । ইহঁত প্রকৃতির জামাদার । প্রকৃতির নির্দেশ মতে
আহিবই লাগিব । ইহঁত হ'ল কাউরী , শগুণ আরু শেন ইত্য়াদি । এই চরাই~
সমূহ নথকা হ'লে গোটেই পৃথি঵ীখনেই দুর্গন্ধময হ'লহেঁতেন । দুর্গন্ধর উপরিও
গেলা মরাশরবোররপরা বিভিন্ন রোগর বীজাণুও বিস্তারিত হয । কিন্তু এই
চরাই কেইজাতে আমাক তারপরাও রক্ষা করে । গরু , ম'হ আদি ঘরচীযা
প্রাণীর নোমর মাজত চিকরা আদি অজস্র তেজহোপা প্রাণী থাকে । বগলী
আদি চরাযে এই চিকরাসমূহ খাযো গরু , ম'হক পরজী঵ীর আক্রমণরপরা
রক্ষা করে ।</p>

<p>মাংসভোজী চরাইবিলাকরপরা আমার উপকার অপকার দুযোটাই হয ।
কাউরীযে পার-পো঵ালি আরু চিলনী , শেন আদিযে হাঁহ , কুকুরার পো঵ালি
থপিযাব বিচারে । পাছে ইহঁতে আমার উপকারো কম নকরে । পাছে এই
কেইজাত চরাযেই কের্কেটু঵া , নিগনি এন্দুর আরু দুই-এজাত সাপ আহার
হিচাপে খো঵াত আমি বহুতো উপদ্র঵রপরা হাত সারিব পারো । ফেঁচাযো
নিতৌ য়থেষ্ট এন্দুর বধ করি খায । ও঵াটচন্ চাহাবে হিচাপ করি পাইছিল
য়ে পো঵ালি তোলার সমযত এয়োর ফেঁচাই দিনে 40 টা মানকৈ এন্দুর বধ
করে । এন্দুরর প্রজনন ক্ষমতাও অদ্ভুত । এয়োর এন্দুর দম্পতীযে বছরেকত
8/10 বার পো঵ালি দিযে , আরু প্রতিবারতে 18 টা পর্য়ন্ত পো঵ালির জন্ম
হয । অকল সেযে নহয , এয়োর এন্দুরে দুমাহমান বযসতে পো঵ালি দিব
পরা হয । গতিকে এয়োর এন্দুররপরা বছরেকত কমেও এহেজার পো঵ালির
জন্ম হ'ব । ফেঁচাই এন্দুর মারিবলৈ নোলো঵া হ'লে এন্দুরর উত্পাততে মানুহ
জীযাই থকা অসম্ভ঵ হ'লহেঁতেন । আনহাতে এন্দুর সংক্রামক বীজাণুর বাহক
আরু ই য়থেষ্ট শস্য় খায আরু নষ্ট করে । কের্কেটু঵াই আমার ফল মূল খায ।
সাপে চরাই পো঵ালি আরু কণী বিচারি ফুরে । য়থেষ্ট সংখ্য়ক কের্কেটু঵া আরু
সাপো মাংসাহারী চরাইসমূহেও নাশ করি থাকে ।</p>

<p>বহুতো চরাযে আমার অপকারী পোক-পতঙ্গ খাই আমাক সিহঁতর
উপদ্র঵রপরা রক্ষা করে । বর্তমানে শস্য় খো঵া পোক-পতঙ্গ মারিবলৈ বিভিন্ন
প্রকারর ঔষধ আদি ওলাইছে আরু সেইবিলাক বহুপরিমাণে ব্য়঵হারো করা
হৈছে । পাছে তার বেযা ফাল এটাও আছে । //
পোকমরা দর঵ খাদ্য় বা
পানীর লগত মিহলি হৈ দুই-এজনক মৃত্য়ুর মুখত পেলো঵ার দুই-এটা খবর
বাতরি কাকতত ইতিমধ্য়ে প্রকাশ পাইছে । এই
দর঵র ব্য়঵হারত অপকারী পতঙ্গর লগতে দুই-এটা
উপকারী পতঙ্গও মরা পরে । আনকি এনে দর঵ ব্য়঵হার করার ফলতে শস্য়
আরু মাছ দুযোটা থকা দ' পথারবোরত মাছ নাইকিযা হ'ল । তদুপরি দর঵
দিযা শস্য়র গোন্ধ এটাও থাকে । এই কুফলবিলাকরপরা হাত সারিবলৈ পতঙ্গ~
ভোজী চরাই পুহি পতঙ্গ নিধনর ব্য়঵স্থা করা হ'লে দুযোকুল রক্ষা পরিলে~
হেঁতেন । অন্য় প্রাণীর তুলনাত পোক-পতঙ্গর সংখ্য়াও বেছি । আমার
ভারতবর্ষতে এতিযালৈকে 30,000 হাজাররো অধিক জাতর পোক-পরু঵ার
চিনাক্তকরণ করা হৈছে । তাতে প্রত্য়েকটো঵ে কণীও পারে বহুত । শালিকীযে
গোবর খুছরি পোক-পতঙ্গ বিচারি ফুরে । কাঠর মাজত , আনকি বাকলির
ভিতরলৈ গৈযো সারিব খোজা পোক-পতঙ্গই বাঢ়ৈটোকার চকুত ধূলি দিযা
টান হৈ পরে । পোক-পতঙ্গ বিচারি কুলি , কেতেকী , টুনি আদি চরাযে
পাতর মাজে মাজে অভিয়ান চলায । মুঠতে বিভিন্ন পতঙ্গভোজী চরাযে
বিভিন্ন ধরণেরে অভিয়ান চলাই য়থেষ্ট সংখ্য়ক পোক-পতঙ্গ মারি আমাক
সদায সহায করি আহিছে । এই কথা উপলব্ধি করিযেই মার্কিন পক্ষীবিদ
মাইকেলেট চাহাবে এষার বঢ়িযা কথা কৈ গৈছে য়ে ` চরাই নথকা হ'লে
এই পৃথি঵ীত মানুহ বসবাস করা অসম্ভ঵ হ'লহেঁতেন ' । জলচর চরাই
সমূহেও পানীত থকা পোক , শামুক আদি খাই পানী নির্মল করি রাখে ।</p>

<p>চরাযে আমার অনিষ্টও কিছু নকরাকৈ নাথাকে । বগলীযে কঠিযা~
তলিত মাছ বা আন পোক পতঙ্গ বিচারে । কিন্তু লগতে আমার কঠিযাও
কিছু পরিমাণে নষ্ট করে । পকা শস্য়র সমযত চরাই হৈ পরে আমার শত্রু ।
বিভিন্ন ধরণর পকা ফলো চরাইর মুখরপরা
রক্ষা করা টান । কিন্তু একেখিনি চরাযে উপকারো
কিছু পরিমাণে করে । শস্য় নষ্ট করা ঘাঁহর গুটিও ইহঁতেই খায আরু
আমার শস্য় রক্ষা পরে । এবার এটা কপৌর পাকস্থলীত 7500 টা
এনে ঘাঁহর গুটি পো঵া গৈছিল । ফল-মূল খাই বিভিন্ন ঠাইত
গুটিবিলাক পেলাই দিযার ফলত সেই ফলর গছ বিভিন্ন ঠাইত গজি
উঠে । দুই-এজাত চরাযে তদুপরি দুপাহ ফুলর মাজত পরাগ সংয়োগো ঘটায ।</p>

<p>আযুর্বেদিক আরু অন্য়ান্য় দুই-এক চিকিত্সাত চরাইর মাংস আরু
পাখির ব্য়঵হার হয । টনিক আদি তৈযার করোতে কুকুরার কণী মিহলো঵া
হয । পক্ষাঘাত রোগত ভোগা রোগীক পারর মাংস
খাবলৈ দিযা হয । ইনফ্লুযেঞ্জা আদি রোগতো পারর
মাংসর চুরুহা ভাল বুলি কয । বিভিন্ন বেক্টেরিযা , ভাইরাছ আদি রাখি
অধ্য়যন করিবলৈ কুকুরার কণীক মাধ্য়ম হিচাপে ব্য়঵হার করা হয ।</p>

<p>আগর দিনত চরাইর হতু঵াই ডাক-অনা-নিযারো ব্য়঵স্থা করা হৈছিল ।
এই কামত বিশেষকৈ পার চরাইকে নিযোগ করা হৈছিল । প্রথম মহায়ুদ্ধ ,
দ্঵িতীয মহায়ুদ্ধ আরু কোরিযা য়ুদ্ধত এই পারর ব্য়঵হার বহুল পরিমাণে
হৈছিল । ভারতবর্ষতো মোগল য়ুগত ডাক অনা-নিযা কামত পারক লগো঵া
হৈছিল । আগর দিনত গুপ্ত-প্রেমর চিঠি আদান-প্রদানতো হেনো পারই
বর বিশ্঵াসয়োগ্য়ভা঵ে কাম করিছিল ।</p>

<p>চরাইর মলো এক অতি উত্তম সার । গু঵ানো ( Guano ) নামর
এবিধ চরাইর মল শস্য়র সার হিচাপে বহুদিন ধরি ব্য়঵হৃত হৈ আহিছে ।
কারণ এই চরাইজাতর মলত নাইট্রো'জেন , ফচফেট , কেলচিযাম আরু লো
ইত্য়াদি থাকে । কুকুরার মলো শস্য়র গুরিত সার হিচাপে দিযা হয ।</p>

<p>আগর দিনত ভারতবর্ষ আরু আরবত চরাইক লৈ নানান খেল-~
ধেমালিরো আযোজন করা হৈছিল । শেন-চরাই পোহা হৈছিল আরু
চিকার দেখিলে ধরিবলৈ এরি দিযা হৈছিল । আমার
অসমতো আহোম স্঵র্গদেউসকলে শেন পুহিছিল
আরু কনু঵া ধরিবলৈ শেন মেলি রং চাইছিল । এতিযা আরু সেই
স্঵র্গদে঵ো নাই আরু শেন পোহার ব্য়঵স্থাও নাই । অ঵শ্য়ে শেন~
চরাইক চাওঁতা-চিতোতা শেনচো঵াসকল এতিযাও আছে । কুকুরা-য়ুজোঁ
এক অতি আকর্ষণীয খেল । ভাটৌ , মইনা , শালিকা আদি সঁজাত ভরাই
সিহঁতক মাত-কথা শিকাযো বহুলোকে আনন্দ পায । দুই-একে এনে মাত-~
কথা শিকা আরু দুই-একে কাম করিব জনা ভাটৌ , মইনা আদি সঁজাত
ভরাই রাস্তাই-ঘাটে মানুহক দেখু঵ায আরু নিজেও দুপইছার মুখ দেখে । //
খাদ্য়চক্র
এটার ধ্঵ংসত আনটোর উন্নতি এটার মৃত্য়ুত আনটোর জনম , এটার
কান্দোনত আনটোর হাঁহি খিকিন্দালি , এটার পতনত আ নটোর গঠন -
পৃথি঵ীত নতুন কথা নহয । সকলো঵েই জীযাই থাকিলে মরিব কোন আরু
সকলো মরিলেই বা জীযাই থাকিব কোন ? উরণ , বুরণ , গজন আরু গমন
এই চারিমুঠি জী঵ , ইটো঵ে সিটোর ওপরত নির্ভর করিযেই বর্তি আছে ।
এই জী঵ আন এটা জী঵র লগত এনেভা঵ে সাঙোর খাই আছে য়ে এটার
অবিহনে আনটোর স্থিতি অসম্ভ঵ । জী঵ই জী঵র লগত সাঙোর খাই থকা
অ঵স্থাটো এধার মালার লগত তুলনা করিব পারি ।</p>

<p>এই জী঵-জগতর খাদ্য়র কারণে এই জী঵-জগতরে দৈনিক কিমান
জী঵ক হত্য়া করা হৈছে তাক সংখ্য়ারে প্রকাশ করা অসম্ভ঵ । তথাপি তার
সরুসুরা উদাহরণ আমার চকুর আগতে হাজার-বিজার
আছে । মাছর বজারত পাচিযে পাচিযে ভরি থকা
মাছবিলাক , কচাইখানার হাজার হাজার ছাগলীর মঙহ , টোত বেচিবলৈ
অহা হাঁহ পার , কুকুরাবিলাক আমার পেট-পূজার কারণেই নহয জানো ?
সরু সরু মাছবিলাকক ডাঙর মাছে খায , ডাঙর মাছবিলাকক আকৌ আমি
খাওঁ , এটা কুরি ফুট দীঘল অজগরক ফুটে প্রতি সপ্তাহত আধাসেরকৈ মঙহ
লাগে । এটা সাধারণ পোকে দৈনিক তার ওজনর দুজোখ আহার খায ,
একোটা শূককীটে ( larva ) কীটলৈ পরি঵র্তন হো঵ার আগলৈকে তার নিজর
ওজনর প্রায 86000 গুণ আহার খায । আকৌ একোটা মাংসাহারী চরাযে
নিমিষতে শ শ পোক খাই শেষ করে ।</p>

<p>তৃণভোজী প্রাণীবিলাকতকৈ মাংসভোজী প্রাণীবিলাকর আহারর পরিমাণ
য়থেষ্ট কম , পাঁচ কিলোমান ওজনর সাগরীয শামুক খালে এটা কডমাছর আধা
কিলোমান ওজন বাঢ়ে , আধা কিলোমান শামুকর
মঙহ তৈযার করিবলৈ পাঁচ কিলোমান পোক-পরু঵ার
প্রযোজন , আকৌ সেই একে আধা কিলো পোক-পরু঵ার মঙহ তৈযার
করিবলৈ পাঁচ কিলোমান ক্ষুদ্রতর প্রাণীর প্রযোজন । মুঠতে এটা কডমাছ আমি
খাব পরা হো঵ালৈকে সি প্রায আধাটন ওজনর মঙহ খাই শেষ করে । এই
সাধারণ ঘটনাটিযেই আমাক দেখু঵াই য়ে জীযাই থাকিবর কারণে এজাত
প্রাণীর ওপরত আন এজাত প্রাণীর কি ভীষণ অত্য়াচার । মানুহ জী঵ জগতর
এক বিশেষ সৃষ্টি । গতিকে মানুহর কথা বাদ দি এই খাদ্য়চক্রর কথা পৃথি঵ীর
প্রত্য়েক প্রাণীতেই খাটে । প্রত্য়েক প্রাণীযেই দুমুখীযা বাটত থিয দিব লগা
হৈছে । এহাতে য়েনেকৈ খাদ্য়র কারণে অন্য় প্রাণীক মারিছে তেনেকৈ একে
সমযতেই নিজেও কারোবার চিকারর সামগ্রী হ'ব লগাত পরিছে । গতিকে
এই দুযোকুল রক্ষা করি থকাজনেহে দুদিন তিস্থি থাকিব পারিছে ।</p>

<p>বর ডাঙর প্রাণী এটাই তেনেই সরু প্রাণী এটাক খাবলৈ নাহে । সিংহ
একোটাই চিকা মারি ভোক পলু঵াব পারিব জানো । কেইটামান বিশেষ
প্রাণীর কথা বাদ দি বাকীবিলাকর ক্ষেত্রত দেখা য়ায য়ে খাবলৈ লো঵া
প্রাণীটোতকৈ তাক খো঵া প্রাণীটো আকারত য়থেষ্ট ডাঙর । তৃণভোজী
প্রাণীবিলাকর ক্ষেত্রত অ঵শ্য়ে এটা ক্ষুদ্রমান পোকেও এজোপা ডাহর গছর
ফলপাত আদি খাই শেষ করি পেলাব পারে কিযনো গছর চলন আরু প্রতি~
রোধ করা শক্তি প্রাণীতকৈ কম । সেই কারণেই জে,এইচ,ব্রেদলিযে উল্লেখ
করিছে য়ে "" উদ্ভিদসমূহ প্রতিশোধ লো঵াত বর দুর্বল আরু আনহাতে প্রাণী~
সমূহ পাপ করাত বর সবল । ""</p>

<p>দুই-এক ক্ষেত্রত সরু সরু প্রাণীযেও ডাঙর ডাঙর প্রাণীক আক্রমণ করা
দেখা য়ায । লতামাকরী বাঘ হাতীতকৈ আকারত সরু হ'লেও গছর ওপরর~
পরা জাঁপ মারি হাতীর পোনতেই রাজহাড়ডাল ভাঙি আহারর সামগ্রী করি
লো঵া দেখা য়ায । বিষাক্ত সাপবিলাকে জিভাত থকা বিহর সহাযেরে বহুতো
ডাঙর জন্তুকো বধ করিব পারে । মজারলি পরু঵া কোদো , বরল , মৌ আদি
আকারত আমাতকৈ বহুতো সরু হ'লেও কামুরি সিহঁতে আমার অ঵স্থা শোচ~
নীয করে । নিদ্রারোগর ( sleeping sickness ) কীটানুবোরে আফ্রিকার
হাবি জঙ্ঘলবিলাকত মানুহক মৃত্য়ুর মুখত পেলায ।</p>

<p>প্রাণী-জগতর খাদ্য়চক্র তৈযার করোঁতে কিছুমান নির্দিষ্ট নীতি-নিযমর
প্রযোজন হৈ পরে আরু প্রাকৃতিক জগতত এই নীতি-নিযম আ঵হমান কালর
পরি চলি আহিছে আরু চলিও থাকিব । //
আকারর পিনরপরা প্রাণীসমূহক
ডাঙর , মজলীযা , সরু , ক্ষুদ্র আদি কিছুমান ভাগত
ভগাব পারো । আগতেই উল্লেখ করি অহা হৈছে
য়ে খুব ডাঙর প্রাণী এটাই তেনেই ক্ষুদ্রমান প্রাণী
এটাক আহার হিচাপে নাখায , কারণ তারে তার ভোক নুগুচে । দ্঵িতীযতে
তেনে করিলে মধ্য়ম আকারর প্রাণীবোরে খাব কি ? সেই কারণেই হাবি-~
জঙ্ঘলত বাঘে হরিণ , গাহরি আদি প্রাণীকহে সাধারণতে আহার হিচাপে
খায ; এন্দুর বা নিগনি বিচারি নুফুরে । অ঵শ্য়ে আহারর নাটনিত সরু
প্রাণী খাবলৈ লো঵াটো বেলেক কথা ।</p>

<p>একোটা মাংসাহারী প্রাণী জীযাই থাকিবলৈ জী঵ন কালত তাতকৈ
নিম্নস্তরর বহুতো প্রাণীর প্রযোজন হৈ পরে । গতিকে ডাঙর সরু সকলো
প্রাণীরে সংখ্য়া সমান হ'লে অর্থাত্ খাদক আরু খাদ্য় সমান হ'লে খাদ্য়চক্র
টিকি থকা অসম্ভ঵ । সেই কারণেই সরু স্তররপরা ডাঙর স্তরলৈকে প্রাণীর
সংখ্য়া এক নির্দিষ্ট হারত কমি আহে । জে.এইচ.ব্রেদ্লির মতে প্রাণীর
আকৃতি বাঢ়ি অহার লগে লগে সংখ্য়া কমিবই লাগিব । নহ'লে খাদ্য়চক্র
ছিঙি-ভাঙি পরিব ।</p>

<p>য়দিঘরখনত নিগনি মারিবলৈ অনা মেকুরী আরু নিগনির সংখ্য়া সমানেই
হয - তেন্তে মেকুরী কেইজনীর জাতো য়াব , ভোকো নুগুচিব , বেযা অ঵স্থাহে
হ'ব । সেইদরে একে ঠাইতে থকা এন্দুরতকৈ শিযালর সংখ্য়া আরু পহুতকৈ
বাঘর সংখ্য়া কম হ'ব লাগিব । য়দি তাকে নহৈ খাদ্য়-খাদকর সংখ্য়া
সমান হয তেন্তে প্রথম কথা - এটি এন্দুর আরু পহুও বাচি নাথাকিব আরু
দ্঵িতীয কথা - শেষলৈ খাদ্য়র অভা঵ত শিযাল আরু বাঘেও মৃত্য়ুর আশ্রয
ল'ব লাগিব ।</p>

<p>প্রকৃতির তর্জুখন বর অদ্ভূত ধরণর । তাত হাতর টিপ্টাপ দগার
হেরফের হো঵ার আশঙ্কা নাই । বিশ্঵র সকলো সৃষ্টি তাত সোমাই আছে ।
একোটা পাহারী পিউমাই ( puma ) একোটা
শীতকালত প্রায পঞ্চাশটা হরিণ খায , সিংহ একো~
টাকো বহুতো মাংস লাগে - বাঘরটো কথাই নাই । অথচ কোনো হাবিতেই
হরিণ মরি শেষ হো঵া নাই ; আরু বাঘরো সংখ্য়া সেই অনুপাতে বঢ়া
নাই । সেইদরে একোটা ফেঁচাই বহুতো সরু সরু চরাই খায , সেই
চরাইবোরে আকৌ বহুতো কীট-পতঙ্গ খাই জী঵ন ধারণ করে । অথচ
একোটা অঞ্চলত কীট-পতঙ্গ সরু জাতর চরাই বা ফেঁচারে সংখ্য়া বর বেছি
বঢ়া বা টুটা নাই । একোটা পুখুরী , হ্রদ বা সাগরত তেনেই ক্ষুদ্রমান পোক~
রপরা আরম্ভ করি বহুতো ডাঙর ডাঙর মাছ কাছ আদিলৈকে বিভিন্ন ধরণর
প্রাণী থাকে । ইহঁতর তৃণভোজীখিনির বাহিরে বাকীবিলাকে ইটো঵ে
সিটোর ওপরত নির্ভর করি চলিব লগাত পরে । অথচ কোনোবিধ প্রাণীরে
সংখ্য়া খুব বেছি ধরণে বাঢ়ি নায়ায বা নকমেও । প্রাকৃতিক জগতত এই সহ~
অ঵স্থানর ব্য়঵স্থা সঁচাকৈযে মন করিবলগীযা । প্রাণী য়িমানেই সরু হয , সিহঁত
নিরাপত্তা কমে সঁচা - কিন্তু সেইযা পূরণ করে সিহঁতর ব্য়াপক প্রজননে ।</p>

<p>এই প্রসঙ্গতে ও঵াছিংটনর কৈবাব নামর হাবি এখ঩র ঘটনা এটি উল্লেখ
করা ভাল হ'ব । তাত পিউমা আরু হরিণবোরে বহুকাল ধরি এক বুজা-~
বুজির ভাবেরে বাস করি আহিছিল । এই হরিণবোরেই পিউমার খাদ্য়
আছিল য়দিও দুযোরে সংখ্য়া নকমিছিল । এবার কেইজনমান বিশেষজ্ঞই
ঠিক করিলে য়ে পিউমাবিলাক মারি পেলালে হরিণর সংখ্য়া বঢ়াব পরা য়াব ।
পিউমাবিলাক মারি পেলো঵া হ'ল । ফলত হরিণর সংখ্য়া অস্঵াভা঵িক
রূপে বাঢ়িবলৈ ল'লে । এই বর্ধিত সংখ্য়ক হরিণর কারণে হাবিখনত ঘাঁহ
বন নাছিল । ফলত কিছুদিনর ভিতরতে হরিণাবোরে হাবির সকলো ঘাঁহ-বন
খাই হাবিখন টকলা করি পেলালে । ইতিমধ্য়ে শীতকাল আহিল , হাবিখ঩ত
ঠাযে ঠাযে বরফ পরিল । হরিণর খাদ্য় সংস্থানর টনাটনি হ'ল । এইবার
হরিণবোরে জাক পাতি হাবিত থকা গরখীযা ল'রাবিলাকর তম্বু঵ে তম্বু঵ে
ভিক্ষা বিচারি ওলাল । চারিওফালে কে঵ল হরিণ আরু হরিণ । চারিও~
পিনে হরিণর উপদ্র঵ বাঢ়িবলৈ ধরিলে । শেষত উপায নাপাই একে কেই~
জন বিশেষজ্ঞই ঠিক করিলে য়ে কিছুমান হরিণ মারি পেলাব লাগে । হরিণর
সংখ্য়া কমাই দিযাত বাকী থকাখিনিযে জীযাই থকার খাদ্য়-সামগ্রী পালে ।
পিউমাবিলাক আগতে নমরা হ'লে হযতো আপোনা-আপুনি দুযোবিধ প্রাণী~
যেই এটা নির্দিষ্ট সমতা রক্ষা করি জীযাই থাকিলেহেঁতেন । //
য়েতিযা খাদ্য়র উত্কট অনাটনে দেখা দিযে - তেতিযা বহুতো প্রাণীযে
নিজর জাতর প্রাণীকো আহার হিচাপে গ্রহণ করা দেখা য়ায । য়দি হরিণ~
বিলাক তৃণভোজী নহৈ মাংসভোজী হ'লহেঁতেন , তেন্তে ওপরর ঘটনাটোত
হরিণবিলাকে গরখীযার ওচরত ভিক্ষা নিবিচারি লগর হরিণাকে হযতো
ভিক্ষার দ্রব্য় করি ল'লেহেঁতেন । কুকুরে প্রস঵র পাছত ভোকত আতুর হৈ
নিজর পো঵ালি একোটাকে কেতিযাবা খাই পেলায । মাংসভোজী মাছ~
বিলাকে নিজর জাতর মাছকে খায । ল঵ছটার ( lobster ) নামর নিম্ন~
শ্রেণীর প্রাণী এজাতে লগর ল঵ছটারবোরকে খাই পেলায । জী঵বিদসকলে
প্রমাণসহ দেখু঵াইছে য়ে ডাইন'ছরবিলাকর মূরত থকা আঘাতবোর আন
কারোবার নহয - লগর ডাইন'ছররহে ।</p>

<p>প্রকৃতির অন্য় প্রাণীর তুলনাত মানুহর জী঵ন বর বেছি আত্মকেন্দ্রিক ।
মানুহে নিজর সুখ-স্঵াচ্ছন্দ্য়র বাবে প্রাকৃতিক জগতর নীতি-নিযমরপরা ফালরি
কাটি আহি এক কৃত্রিম জগতর মাজত আশ্রয লৈছে ।
আমি পাহরি গৈছো য়ে আমিও প্রাণী-জগতর আন
আন শ্রেণীর দরে এটা শ্রেণীহে ।</p>

<p>প্রাণীর ক্রমবিকাশত চারিঠেঙীযারপরা দুঠেঙীযা হো঵াটো঵েই মানুহর
জী঵নত বহুতো পরি঵র্তন আনিলে ; হাত দুখনর বলতে শিল ঘঁহাই জুই
উলিযাবলৈ শিকিলে , শত্রুলৈ দলিযাব পরা হ'ল আরু পরা হ'ল বন্দুকর
ঘোঁরাত আঙুলি বুলাব । বন্দুকধারী মানুহর আগত অন্য় প্রাণীসমূহে ফের
পাতিব নো঵রা হ'ল । পাছে ইমানখিনি হো঵া সত্঵েও আমার লগত প্রাণী~
জগতর কাজিযাখন চলিযেই থাকিল । ` ইণ্ডিযান ইযের বুক - 1928 'ত
উল্লেখ করা মতে 1925 চনত আমার এই ভারতবর্ষতে প্রাণীর পরা মৃত্য়ু
হো঵া মানুহর সংখ্য়া 1962 জন । ঢেকীযাপতীযাই 974 জন , লতামাকরীযে
181 জন । কুকুরনেচীযাই 265 জন , ভালুকে 82 জন , হাতীযে 78 জন
আরু ঘোঙে 6 জন মানুহ মৃত্য়ুর মুখত পেলালে । এই একে বছরতে সাপর~
পরা হো঵া মৃত্য়ুর সংখ্য়া 19,258 জন ।</p>

<p>দেখা গ'ল য়ে আমার প্রকৃতির লগত য়ুঁজখন এতিযাও শেষ হো঵া
নাই আরু নহযো । অর্থাত্ প্রকৃতির খাদ্য়চক্রক লগত আমি পোটপটীযাকৈ
হ'লেও আওপকীযাভা঵ে এতিযাও সাঙোর খাই আছো ।
 +&gt;*",0
</p></body></text></cesDoc>