<cesDoc id="asm-w-socsci-sociology-aso10" lang="asm">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>asm-w-socsci-sociology-aso10.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>সমাজ.বি.</h.title>
<h.author>অসম.প্রক</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book.</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">aso10</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0666.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-21</date></creation>
<langUsage>Assamese</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;অসম
অসমর জনজী঵ন : ভারতর উত্তর-পূব অঞ্চলত অ঵স্থিত অসম আমার রাজ্য় । পর্বত
পাহারে বেষ্টিত অসমর এটি সুকীযা ভৌগোলিক আরু ঐতিহাসিক বৈশিষ্ট্য় আছে । অধিক
বৃষ্টিপাত , অসংখ্য় , নদ-নদী আরু জান-জুরি , শ্য়ামল প্রান্তর , মহাবাহু ব্রহ্মপুত্র আরু
ধীরস্রোতা বরাকর শুকুলা বলুকা , শরতর সোণো঵ালী মেঘ আরু বসন্তর কুঁহিপাকর পযোভরে
রাজ্য়খনর ভৌগোলিক বৈশিষ্ট্য়ত অরিহণা য়োগাইছে ।</p>

<p>অসম উত্তর-পূব ভারতর সাতখন রাজ্য়র রাজহাড় স্঵রূপ । ইযার চারিওকাষে থকা রাজ্য়বোরর
লগত আমার দেশর বাকী বৃহত্্ ভূ-ভাগর য়োগায়োগ অসমর মাজেদিযেই স্থাপিত হয । 24.1
ডিগ্রী উঃ অক্ষরেখা আরু 27.9 ডিগ্রী উঃ অক্ষরেখা আরু 89.8 ডিগ্রী পূঃ দ্রাঘিমার মাজত অসম
অ঵স্থিত । ইযার আযতন 78.523 বর্গ কিমি. । ই ভারতর সাতখন রাজ্য় আরু দুখন অন্য়
দেশ স্পর্শ করি আছে ।</p>

<p>ভূ-প্রকৃতি : অসম পাহার আরু সমতলেরে মিশ্রিত এখন রাজ্য় । ইযার উত্তর অংশ জুরি
আছে বৃহত্্ ব্রহ্মপুত্র সমতল , মাজভাগত আছে কার্বি মালভূমি আরু উত্তর কাছারর পাহারীযা
অঞ্চল আরু দক্ষিণত আছে বরাক উপত্য়কা ।</p>

<p>ব্রহ্মপুত্র সমতল ভূটান-অরুণাচল হিমালযর গাতে লাগি পূবা-পশ্চিমাকৈ প্রায 600 কিমি.
দীঘল আরু উত্তরা-দক্ষিণাকৈ গড়ে প্রায 60 কিমি. বহল । এই সমতলখনর দক্ষিণত আছে
নগা পাহার , কার্বি মালভূমি আরু মেঘালয মালভূমি । সমতলখনর মাজেদি বৈ গৈছে বিশাল
ব্রহ্মপুত্র । এই ব্রহ্মপুত্রত উত্তরে হিমালয আরু দক্ষিণর নগা পাহার , মেঘালয আদিরপরা আহি
বহুতো সরু-ডাঙর উপনৈ পরিছে পূবরপরা পশ্চিমলৈ সমতলখনর ঢাল য়ত্্সামান্য় হো঵া বাবে
চারিওফালর পর্বত-পাহারবোরত দুদিনমান ধারাষার বরষুণ হ'লেই ইযার ঠাযে ঠাযে বা঵পানী
উঠে । তদুপরি সমতলখনত বহুতো জলাহ , পিতনি , বিল আরু চর-চাপরি আছে ।
বারিষাকালত এইবোর পানীরে উপচি পরে ।</p>

<p>অসমর মাজভাগত কার্বি মালভূমি দরাচলতে মেঘালয মালভূমি আরু দাক্ষিণাত্য় মালভূমির
অংশ বিশেষ । এই অঞ্চলটো অতীব পুরণা রূপান্তরিত আরু আগ্নেয শিলারে গঠিত ।
কালক্রমত ক্ষয গৈ ই পাহার আরু টিলার সমষ্টিরূপে থিয হৈ আছে । কার্বি মালভূমির উচ্চতা
300 মিটাররপরা 1000 মিটারর ভিতরত । ইযার উচ্চতম ঠাই ছিমহাহিছেন , 1359 মিটার
ওখ । কার্বি মালভূমির গাতে লাগি দক্ষিণফালে আছে । উত্তর কাছার পাহারীযা অঞ্চল । এই
পাহারবোর স্তরীভূত শিলর ভংগিল পাহার । ইহঁত নগা পাহারবোরেরে সংলগ্ন আরু উত্তর-পূব
দক্ষিণ-পশ্চিমমু঵া এই পাহারবোরর আটাইতকৈ ওখ শারীটোক বরাইল পাহারশ্রেণী বোলা হয ।
ইযাত 1600 মিটারতকৈযো ওখ কেইবাটাও শৃংগ আছে । উত্তর কাছার জিলার সদর ,
বিতোপন হাফলং চহর , এই পাহারমালার ওপরতে অ঵স্থিত ।</p>

<p>উত্তর কাছার পাহারীযা অঞ্চলর দক্ষিণত বরাক উপত্য়কা । ই 40 কিমি. দীঘল আরু প্রায
60 কিমি. বহল । বরাক নৈ ইযার মাজেদি পূবরপরা পশ্চিমলৈ সর্পিল গতিরে বৈ গৈছে ।
উত্তরর উত্তর কাছার আরু দক্ষিণর মিজোরাম পাহাররপরা বহুতো উপনৈ আহি ইযাত পরি-
ছেহি । ইযাতো বিল , জলাহ বহুত আছে আরু বারিষা এইবোর পানীরে উপচি পরে ।
উপত্য়কটোত বহুতো সরু সরু টিলাও সিঁচরতি হৈ আছে ।</p>

<p>অসম এখন নদী মাতৃত রাজ্য় । ব্রহ্মপুত্র আরু বরাক - এই ঘাই নৈ দুখনর বাহিরেও
এইবোরত আহি পরা বহুত সরু-ডাঙর উপনৈ আছে । এই সকলোবোরে মিলি রাজ্য়খন কৃষির
উপয়ুক্ত আরু উর্বরা করি তুলিছে । এইবোরর ভিতরত ব্রহ্মপুত্রর উত্তরপারর সো঵ণশিরি ,
বুরৈ , জীযা ভরলী , জীযা ধনশিরি , পাগলাদিযা , মানস আরু সোণকোষ আরু দক্ষিণপারর
বুঢ়ীদিহিং , দিছাং , দিখৌ , ভাঁজী , ধনশিরি , কপিলী , কুলসী , দুধনৈ , কৃষ্ণাই আরু
জিঞ্জিরাম প্রধান । বরাকর ঘাই উপনৈ হ'ল জাটিঙা , লংগাই , ধলেশ্঵রী আরু সোণাই ।</p>

<p>জলবাযু : অসম ক্রান্তীয মৌছুমীর জলবাযুর রাজ্য় । ন঵েম্বররপরা ফেব্রু঵াকীলৈকে ইযাত উত্তাপ
কম ( 20 ডিগ্রী ছেরে কম ) আরু বতর শুকান । মার্চরপরা উত্তাপ বাঢ়িবলৈ লয আরু দুজাক
এজাক ধুমুহা গাজনিরে বরষুণ হয । মে আরু জুন মাহত উত্তাপ বাঢ়ি 30ডিগ্রী-38ডিগ্রী ছে.
হযগৈ । কিন্তু জুনর তৃতীয সপ্তাহত দক্ষিণ-পশ্চিম মৌছুমী বতাহর ফলত বৃষ্টিপাত আরম্ভ হয
আরু ই ছেপ্তেম্বর পর্য়্য়ন্ত থাকে । রাজ্য়খনত বছরটোত এইদরে গড়ে 200 ছেমি,রপরা 300
ছেমি. বৃষ্টিপাত হয । এনেদরে হো঵া প্রচুর বৃষ্টিপাতে অসমত ধান , মরাপাট , চাহ আদি
শস্য় হো঵াত সহায করে ।</p>

<p>প্রাকৃতিক সম্পদ : অসমর সমতলবোরর মৃত্তিকা পরসু঵া আরু উর্বর । পাহারর মাটি রঙা
চানেকীযা । এনে মাটি স্঵াভা঵িকতে উর্বর নহ'লেও অলপ সার দিলেই খেতির উপয়ুক্ত হৈ
উঠে । রাজ্য়খনর সকলোবোর মাটিযে অম্লজ । এনে মাটিযে চাহ , মরাপাট , ধান ,
টেঙাজাতীয ফলমূল আদি উত্্পাদন সহায করে ।</p>

<p>রাজ্য়খনর 35 শতাংশ ঠাই জুরি হাবি-বন আছে । এইবোরর বহুতো অপরিণামদর্শী মানুহে
কাটি-ছিঙি ধ্঵ংস করিছে য়দিও এতিযাও রাজ্য়খনত কিছু বনজ সম্পদ আছে । এনে বনজ
সম্পদর ভিতরত বাঁহ , বেত আদি আরু হলং , বনচোম , গর্জন , তিতাচপা , শাল , চেগুণ
সোণারু , শিঙরী আদি মূল্য়঵ান গছ আছে । অসমত খের , ইকরা আদি ঘর সজাত ব্য়঵হার
করা ঘাঁহজাতীয তরু-তৃণক গজে । অসমর হাবি-বনত গঁড় , হাতী , বনরীযা ম'হ , হরিণা
বাঘ , ভালুক , প্রায 12 বিধ বান্দর আরু নানান আপুরুগীযা চরাই-চিরিকতি পো঵া য়ায ।</p>

<p>অসম খনিজ সম্পদতো চহকী । ইযাত পেট্র'লিযান , কযলা , প্রাকৃতিক গেছ , চূণশিল আদি
অপর্য়্য়াপ্ত পরিমাণে পো঵া য়ায । ডিগবৈ , নামরূপ , নাহরকটীযা , হুগ্রীজান , মরাণ , লাকু঵া
গেলেকি , রুদ্রসাগর , বরহোলা আদিত পেট্র'লিযাম আরু প্রাকৃতিক গেছ , লেখাপানী ,
বরগোলাই , টিপং , লিডু , জযপুর , শিলভেটা আদিত কযলা আরু কার্বি মালভূমিত
চূণশিল পো঵া য়ায ।</p>

<p>রাজ্য়খনর অর্থনৈতিক ভিত্তি ঘাইকৈ কৃষির ওপরত প্রতিষ্ঠিত । শতকরা 70 ভাগ লোকে
জী঵িকার বাবে কৃষির ওপরত নির্ভর করে । ধান রাজ্য়খনর ঘাই খাদ্য়-শস্য় । অসমে বছরি
20 লাখ টনমান ধান উত্্পাদন করে । আন আন খাদ্য়শস্য়র ভিতরত গোমধান , ঘেঁহু , মাহ
আরু সরিযহ প্রধান । কৃষিভিত্তিক চাহ উদ্য়োগত অসম পৃথি঵ীবিখ্য়াত । পৃথি঵ীর প্রায
এক-চতুর্থাংযশ চাহ অসমে উত্্পাদন করে । ইযাত 750 খন মান চাহ বাগিচা আছে আরু এই
রাজ্য়ই বছরি প্রায 2.53 নিয়ুত কিল'গ্রাম চাহপাত উত্্পাদন করে ।</p>

<p>অসমর ডিগবৈ , নুনমাটি ( গু঵াহাটী ) আরু বঙাইগাঁ঵ত তিনিটা তেল শোধনাগার আছে ।
ইযার উপরিও গোলাঘাটর ওচরর নুমলীগড়ত প্রস্তা঵িত চতুর্থ তেল শোধানাগারটো স্থাপন করা
হ'ল । রঙাইগাঁ঵ত ঘাইকৈ পেট্র'রাসাযনিক পণ্য় উত্্পাদন করা হয । তদুপরি নামরূপত এই
রাসাযনিক সার কারখানা , বোকাজানত এটা চিমেণ্ট কারখানা , ডেরগাঁও আরু বদরপুরত দুটী
চেনি কারখানা , শিলঘাটত এটা মরাপাটর কল , জাগীর'ড আরু য়োগীঘোপত দুটা কাগজ
কারখানা আরু চারিদু঵ারত এটা কপাহ কটা কল আছে । ইযার বাহিরেও রাজ্য়খনর বহুত
ঠাইত প্লাইউড ফেক্টরী আছে । তারে ভিতরত মার্ঘেরিটা আরু মরিযনির প্লাইউড ফেক্টরী
প্রধান । রাজ্য়খনর গু঵াহাটী আরু তিনিচুকীযাত অন্য়ান্য় কারখানার ভিতরত লোহার র'ড আরু
অন্য়ান্য় য়া঵তীয সাজ-সরঞ্জাম প্রস্তুত করা কারখানা আছে । ইযার বাহিরেও রাজ্য়খনর বহুত
ঠাইত ধানবনা কল , তেল পেরা কল , আটা মযদার কল আরু কাঠফলা কল আছে ।
অসমর কুটীর শিল্পর ভিতরত উল্লেখয়োগ্য় বো঵া কটা । ইযার কপাহী কাপোরর বাহিরেও
এড়ি , মুগা আরু পাটর সূতা উত্্পাদ঵ আরু এইবোররপরা বস্ত্র প্রস্তুত করা হয । শু঵ালকুছি
এনে বো঵া-কটার বাবে প্রখ্য়াত । রাজ্য়খনত পরম্পরাগতভা঵ে কাঁহ আরু পিতলর
বাচন-বর্তনো প্রস্তুত করা হয ।</p>

<p>মান঵ সম্পদ আরু জনজী঵ন : 1991 চনর লোকপিযলমতে অসমর জনসংখ্য়া 2,22,94,562
জন । 1971-81 র দশকটোত অসমর জনসংখ্য়া 34 শতাংশ বাঢ়িছিল । আমার গোটেই
ভারতত কিন্তু এনে বর্ধন 23 শতাংশহে আছিল । ইযারপরাই অনুমান করিব পারি য়ে
রাজ্য়খনত জনসংখ্য়ার স্঵াভা঵িক বর্ধনর বাহিরেও , অন্য়ান্য় ঠাইরপরা ইযালৈ বহুতো লোক
আহি আছে ।</p>

<p>আদিম কালরেপরা অসম বিভিন্ন জনগোষ্ঠীর সংগমস্থল হৈ আহিছে । বুরঞ্জীযে ঢুকি নোপো঵া
সুদূর অতীতর প্রথমে ইযাত আহি বসবাস করিবলৈ লৈছিল , অষ্ট্র 'এছিযাটিক গোষ্ঠীর
আজিকালির খাছী জযন্তীযাসকলর পূর্বপুরুষসকলে । তেওঁলোকর বহুতো আজিকালি আমার
কার্বি আংলঙর হামরেণ মহকুমাত আছে । তার পাছত অসমলৈ পূব আরু পশ্চিমরপরা দুটা
প্রব্রজনকারীর সোঁত বরলৈ ধরে । উত্তর-পূবরপরা আহে তিব্বেত ' বার্মান ভাষা-ভাষীর
মংগোলীযসনকল আরু পশ্চিমর গংগা উপত্য়কারপরা আহে ইণ্ড'-ইউরোপীযান ভাষা-ভাষী
আর্য়্য়সকল । সময য়ো঵ার লগে লগে এই তিনিওটা জনগোষ্ঠীর সমন্঵যত অসমত গঢ় লবলৈ
ধরিলে এটা সুকীযা ঐতিহ্য় আরু সংস্কৃতি । ত্রযোদশ শতিকাত ( 1228 চনত ) আকৌ
পূবরপরা শ্য়ামীয-চৈনিক ভাষা-ভাষী মংগোলীয আহোমসকল ইযালৈ রাজ্য় স্থাপন করিবলৈ
আহে । তার ঠিক 23 বছর মানর আগরপরা ( 1205 চনত ) পশ্চিমরপরা ইছলাম ধর্মা঵লম্বী
ককেছীয বংশোদ্ভূত মুছলমানসকল দেশ জয করিবলৈ আহিবলৈ ধরে । আহোমসকলে ছশ
বছরীযা রাজত্঵র পাতনি মেলি ব্রহ্মপুত্র উপত্য়কাত এটি অসমীযা জাতির রাজনৈতিক ,
সামাজিক আরু সাংস্কৃতিক অস্তিত্঵ দৃঢ়ভা঵ে গঢ়ি উঠাত সহায করে । তার পাছতো মধ্য়য়ুগত
অসমলৈ পূব আরু পশ্চিমর প্রব্রজনকারীর সোঁত ক্ষীণভা঵ে হ'লেও চলিযে থাকে । বৃটিছে
1826 চনত অসমর শাসন হস্তগত করার পাছত শাসনয়ন্ত্র চলাবর বাবে তেতিযার
বংগদেশরপরা আমোলা-মহরি আরু নেপালরপরা চিপাহী চন্তরী আনিবলৈ লয । তদুপরি চাহ-
বাগিচাত কাম করিবলৈ ছোটনাগপুর অঞ্চলরপরা চাহ শ্রমিক আনিবলৈ ধরে । বেহা-বেপারর
বাবে আগবাঢ়ি আহিল রাজস্থানর বণিক সম্প্রদাযর লোক । য়ো঵া শতিকার শেষভাগরপরা
আহিবলৈ ললে তেতিযার পূর্ববংগর মাটিহীন মুছলমান কৃষকসকল । তদুপরি দেশ বিভাজন
আরু স্঵াধীনতার লগে লগে সোঁত ববলৈ ধরে পূব পাকিস্তানর হিন্দু শরণার্থীসকলর ।
এইদরেই প্রাগঐতিহাসিক য়ুগরোপরা বিভিন্ন কালত , বিভিন্ন দিশরপরা , বিভিন্ন জনগোষ্ঠীর
মানুহে আহি বর্তমান অসমর জনসংখ্য়া গঢ়ি তুলিছে । তেওঁলোকর বহুতরে সুকীযা ভাষা ,
সামাজিক রীতি-নীতি আরু ধর্ম আছে , কিন্তু সকলো঵েই অসমর সামূহিক ঐতিহ্য়লৈ অরিহণা
য়োগাইছে ।</p>

<p>রাজ্য়খনর জনসংখ্য়ার দ্রুত বৃদ্ধি হো঵ার লগে লগে জনবসতির ঘনত্঵ও বাঢ়ি আহিছে । ইযার
নলবারী , করিমগঞ্জ আরু নগাঁও জিলাত এতিযা প্রতি বর্গ কিমি.ত গড়ে 350 জনতকৈ
অধিক লোক বাস করে । পার্বত্য় অঞ্চলত অ঵শ্য়ে জনবসতির ঘনত্঵ কম । অসমর শতকরা
90 জন লোক গ্রামবাসী । ইযার 21995 খন গাঁও আরু মাথোন 78 খনহে নগর আছে । এই
নগরবোরর অধিকাংশই সরু ।</p>

<p>অসমর শতকরা 72 জন লোক হিন্দু , 24.5 জন মুছলমান , 2.6 জন খ্রীষ্টিযান আরু কিছু
লোক বৌদ্ধ আরু জনজাতীয ধর্মা঵লম্বী । রাজ্য়খনত অলপ সংখ্য়ক জৈন আরু শিখ ধর্মর
লোকো আছে ।</p>

<p>1971 চনর জনপিযলমতে ইযার 61 শতাংশ লোকর মাতৃভাষা অসমীযা , 19 শতাংশর
মাতৃভাষা বাংলা , 8 শতাংশ বিভিন্ন জনজাতীয ( তিব্বেত-বার্মান ) ভাষা আরু 5 শতাংশর
হিন্দী । বাকীসকলর নেপালী আরু অন্য়ান্য় ভাষা ।</p>

<p>সাক্ষরতার ফালরপরা রাজ্য়খন পিছপরা । 1971 চনত অসমত মাথোন 28.1 শতাংশ লোকহে
সাক্ষর আছিল ।</p>

<p>আগতে উল্লেখ করার দরে অসমর অধিকাংশ ( 66 শতাংশ ) লোকেই কৃষিজী঵ী । রাজ্য়খনর
ঘাই খেতি ধান । পরস্পরাগতভা঵ে করা শালি আরু আহু ধানর বাহিরেও অসমর মানুহে
এতিযা অলপ-অচরপ অধিক উত্্পাদনক্ষম টাইচুং আদি বিভিন্ন ঋতুত করা ধান করিবলৈ
লৈছে । ব্রহ্মপুত্রর চর অঞ্চলত আহি বসবাস করা পমু঵া ন-অসমীযাসকলে প্রচুর মরাপাট ,
আহুধান , বাওধান , সরিযহ আরু শাক-পাচলি উত্্পাদন করে । ইযার বাহিরে শতকরা প্রায
11 শতাংশ কর্মরত ব্য়ক্তি চাহখেতির সৈতে জড়িত । অসমত শিল্প-উদ্য়োগত জড়িত লোকর
সংখ্য়া কম , মাথোন 4.14 শতাংশহে । বেহা-বেপারত নিযোজিত লোক 56.6 শতাংশ । প্রায
9.69 শতাংশ লোক বিভিন্ন সে঵া আরু লোকসে঵াত নিযোজিত । য়াতাযাত-পরিবহণত কাম
করা লোক 2.5 শতাংশ ।</p>

<p>য়ো঵া 1826 খ্রীষ্টাব্দত অসমক বৃটিছ শাসনলৈ অনার পাছরপরা অসম রাজনৈতিকভা঵ে ভারতর
এখন প্রদেশ হয । তার আগতে অসম রাজনৈতিকভা঵ে স্঵াধীন আছিল য়দিও ই ভারতীয
সমাজ , সভ্য়তা আরু ধর্মব্য়঵স্থার অন্তর্গত আছিল । বৃটিছে ইযাত ভারতর এটা রাজনৈতিক
অংগ করি শাসনর সুবিধার বাবে রাজ্য়খনক কেইবাখনো জিলাত ভাগ করে । সমযত ব্রহ্মপুত্র
উপত্য়কা আরু বরাক উপত্য়কার জিলাবোরর বাদেও বর্তমানর মেঘালয , নগালেণ্ড ,
মিজোরাম আরু অরুণাচলরো একাংশ অসম প্রদেশর ভিতরত অন্তর্ভুক্ত করা হয । আমার
দেশ স্঵াধীন হো঵ার পাছত লাহে লাহে এই পার্বত্য় অঞ্চলবোর একোখনকৈ সুকীযা রাজ্য় হৈ
পরে । অসমর বাকী থকা অংশত এতিযা (1) ধুবুরী (2) কোকরাঝার (3) গো঵ালপারা , (4)
বঙাইগাঁও , (5) বরপেটা , (6) নরবারী , (7) কামরূপ , (8) দরং (9) শোণিতপুর , (10)
মরিগাওঁ , (11) নগাঁও , (12) গোলাঘাট , (13) য়োরহাট , (14) শি঵সাগর , (15)
লক্ষীমপুর , (16) ধেমাজী , (17) ডিব্রুগড় , (18) কার্বি আংলং , (19) উত্তর কাছার ,
(20) কাছার (21) করিমগঞ্জ , (22) তিকিচুকীযা আরু (23) হাইলাকান্দি , এই 23 খন জিলা
আছে ।</p>

<p>রাজ্য়খনর প্রধান নগর গু঵াহাটী । জনসংখ্য়া আনুমানিক 7 লাখ । ইযার কাষরে দিছপুরত
রাজ্য়খনর অস্থাযী রাজধানী পতা হৈছে । গু঵াহাটী সমস্ত উত্তর-পূব ভারতর দু঵ার স্঵রূপ ।
ইযাত তেল শোধনাগারকে ধরি বহুতো ক্ষুদ্র আরু মজলীযা উদ্য়োগ আছে । রাজ্য়খনর দ্঵িতীয
প্রধান চহর ডিব্রুগড় । ই চাহশিল্পর কেন্দ্র । ইযার ওচররে তিনিচুকীযা এখন শিল্প-প্রধান
চহর । আন আন প্রধান চহরর ভিতরত য়োরহাট , তেজপুর , নগাঁও , আরু শিলচর
প্রধান । এই আটাইবোর জিলার সদর ঠাই হিচাপে , প্রশাসনিক কেন্দ্র , বাণিজ্য় কেন্দ্র আরু
য়াতাযাত পরিবহণর কেন্দ্র ।</p>

<p>রাজ্য়খনর ভৌগোলিক পরি঵েশ , বিশেষকৈ ভূ-অ঵যব আরু জলবাযু঵ে ইযার জনজী঵নত
প্রভূত প্রভা঵ পেলাইছে । বানপানী কমকৈ উঠা অঞ্চলর মানুহে শালিধানর খেতি করে ।
বানপানী সঘনাই হো঵া অঞ্চলত আহু আরু বাওধান আরু মরাপাট করা হয । তদুপরি এইবোর
অঞ্চলত খরালি শাক-পাচলি , আলু , সরিযহ আদি করা হয আরু মধ্য় , পূব আরু দক্ষিণ
অসমর ঢালু ঠাইত চাহ খেতি করা হয । এইদরে বিভিন্ন অ঵স্থানর কৃষকে বিভিন্ন শস্য়
উত্পাদন করি কৃষি কর্মবোরর লগত জী঵ন য়াত্রার ধারা খাপ খু঵াই লৈছে । অসমত বরষুণ
অধিক হয বাবে ইযার পরস্পরাগত ঘরবোর আওগঢ়ীযা চালেরে সজা হয য়াতে বরষুণ
পরিলেই পানী বাগরি আহিব পারে । সেইদরে পাহারর ঢালত বা নৈর পারত বাস করা
জনজাতীয লোকসকলে চাংঘর সাজি লয , য়াতে বরষুণত বাগরি অহা বা বাঢ়ি অহা পানীযে
ঘরটোর হানি করিব নো঵ারে বা তাত থকাত অসুবিধা ঘটাব নো঵ারে । রাজ্য়খনত বিভিন্ন
ঋতুত কৃষির বিভিন্ন প্রক্রিযা হাতত লো঵া হয । এই প্রক্রিযাবোরর মাজে মাজে কৃষকসকলে
পালন করে উত্্স঵-পার্বণ , বিহু-হলি , পূজা-পাতল আরু এইদরেই প্রকৃতি আরু মানুহর
ক্রিযা-প্রক্রিযার সমন্঵যত গঢ়ি উঠিছে অসমর কৃষিভিত্তিক জনজী঵ন । এইখিনিতে মন
করিবলগীযা য়ে খেতির বাবে কৃষকসকলর য়থেষ্ট মাটি নথকা আরু কৃষিত আধুনিকীকরণ
নোহা঵াত অসমর কৃষকসকল সাধারণতে দুখীযা । তদুপরি রাজ্য়খনত শিল্প-উদ্য়োগো কম ।
গতিকে ইযার অর্থনৈতিক অ঵স্থা আশানুরূপ হৈ উঠা নাই । //
আমার ঐতিহ্য়
ভারতবর্ষর জনগণর প্রজাতীয গঠন : প্রথমেই আমি জনা উচিত প্রজাতি মানে কি ? সকলো
মানুহেই হ'ম' ছেপিযেন্স নামর একোটা জৈ঵িক গোষ্ঠীর অন্তর্ভুক্ত । একে গোষ্ঠীর হ'লেও
সকলো মানুহর শারীরিক লক্ষণ একে নহয । সাধারণভা঵ে চকুরে চালেই আমি ধরিব পারোঁ
য়ে কেতবোর লক্ষণ , য়েনে - গার বরণ , নাক-মুখর গঢ় , দৈহিক আরু চকুর রং , এচাম
আকৌ চকুরে চালেই ধরিব নো঵ারি । জোখ-মাখ আরু সূক্ষ্ম বৈজ্ঞানিক নিরীক্ষণর য়োগেদিহে
সেইবোর নির্ণয করিব পারি । কিছুমান উমৈহতীযা শারীরিক লক্ষণ বংশগতভা঵ে একো
একোটা মানর গোটর মাজত অতীজরে পরা বিরাজ করি আহিছে । এনে গোটবোরক প্রজাতি (
ইংরাজীত RACE ) বোলা হয । একে প্রজাতির লোকর মাজত বংশগতভা঵ে নামি অহা
কিছুমান শারীরিক সাদৃশ্য় আছে । আনহাতে বিভিন্ন প্রজাতির লোকর মাজত শারীরিক দিশত
কিছুমান স্পষ্ট প্রভেদ আছে । মান঵ গোষ্ঠীর মুখ্য় প্রজাতিবোর হ'ল , (1) অষ্ট্রেলীযা , (2)
নিগ্র ' (3) মংগোলীয আরু (4) ক'কেছীয । এইবোররো আকৌ নানা ভাগ , উপভাগ আছে ।</p>

<p>পৃথি঵ীর এনে বহুতো দেশ আছে য়িবিলাকর দেশবাসী ঘাইকৈ একেটা প্রজাতির লোক ।
উদাহরণ স্঵রূপে ইউরোপর সরহসংখ্য়ক দেশর মূল বাসিন্দাসকল ক'কছীয প্রজাতির লোক ।
আনহাতে চীন আরু জাপানর লোকসকল মংগোলীয প্রজাতির । আফ্রিকার দেশসমূহত আকৌ
নিগ্র ' প্রজাতির লোকর প্রাধান্য় দেখা য়ায । ভারতবর্ষর ক্ষেত্রত হ'লে আমি এনেধরণর এক
প্রজাতীয জনগঠন দেখা নাপাওঁ । ভারতীয ' বুলিলে আমি একেটা বৃহত্্ সাংস্কৃতিক আরু রাষ্ট্রীয
গোটকে বুজো , কিন্তু ভারতীয বুলি কোনো প্রজাতি নাই । আমার মাজত বিবিধ প্রজাতির
লোক আছে । বহুক্ষেত্রত আকৌ একেটা জাতি , সম্প্রদায বা জনগোটর মাজত বিবিধ প্রজাতীয
উপাদানর সংমিশ্রণো ঘটিছে । এককথাত কবলৈ হ'লে আমি এক বিশাল সংমিশ্রণর
জনগোষ্ঠী । প্রাচীন কালত বাটপথ সুচল নাছিল । তথাপিও দুর্গম পর্বতমালা আরু সাগরে
বেঢ়ি থকা এই দেশলৈ প্রাগঐতিহাসিক কালরেপরা চামে চামে মানুহ আহি বসতি স্থাপন
করিছে । এই প্রব্রজনকারীসকলেই নানা প্রজাতীয উপাদান ভারতলৈ আনিছে আরু ক্ষেত্রবিশেষে
বিভিন্ন প্রজাতির লোকর মাজত সংমিশ্রণো ঘটিছে । ফলত একোটা সম্প্রদায বা জনগোষ্ঠীর
লোকর মাজত আমি কেইবারকমর প্রজাতীয উপাদান দেখা পাওঁ । ভারতত ঘাইকৈ নিম্নলিখিত
প্রজাতীয জনগোষ্ঠীবোর দেখা পাওঁ ।</p>

<p>(1) প্রাক্্ অষ্ট্রেলীযা : নৃতাত্঵িসকলর মতে ভারতর আদিম বাসিন্দাসকল অষ্ট্রেলীযা বা প্রাক্্-
অষ্ট্রেলীয প্রজাতির লোক আছিল । খ্রীষ্ট-পূর্ব 2500 রপরা 1500 বছর আগতে ভারতলৈ অহা
ক'কেছীয প্রজাতির বৈদিক আর্য়্য়সকলে এওঁলোককে হযতো ` দাস ' , ` দস্য়ু ' বা ` নিষাদ '
আখ্য়া দিছিল । আজিও পূব , মধ্য়ভারত আরু দাক্ষিণাত্য়র কিছুমান জনজাতির মাজত
প্রাক্্-অষ্ট্রেলীয প্রজাতির শারীরিক লক্ষণ পরিষ্কারভা঵ে দেখা য়ায । প্রাক্্-অষ্ট্রেলীয প্রজাতির
লোক চাপর গঢ়র । এওঁলোকর গার বরণ ক'লা , চুলি ঢৌ খেলো঵া , মূর দীঘল আরু নাক
চেপেটা ।</p>

<p>(2) নিগ্র ' : ভারতর জনগণর মাজত নিগ্র ' প্রজাতির লক্ষণ প্রকট নহয । অ঵শ্য়ে আন্দামান
দ্঵ীপপুঞ্জবাসী আন্দামানী , ওংগে , চেণ্টিনেনিজ , আরু জারো঵া নামর জনজাতিবোর নিগ্র '
প্রজাতির ` নেগ্রিট ' ঠালর লোক । এওঁলোক তেনেই চুটি-চাপর অর্থাত্্ ` পিগমি '
(PIGMY ) গঢ়র । গার বরণ ক'লা , চুলি ঘন আরু কুণ্ডিত মূর সরু আরু নাক চেপেটা ।</p>

<p>(3) ক'কেছীয : প্রাক্্ অষ্ট্রেলীযসকলর পাছতে লেখত লবলগীযা প্রজাতীয উপাদান হ'ল
ক'কেছীয । ক'কেছীয প্রজাতীয বিভিন্ন ঠাল ঘাইকৈ উত্তর-পশ্চিম দিশরপরাই ভারতবর্ষত
প্রবেশ করিছিল । ইযারে কিছুমান প্রাগৈতিহাসিক য়ুগতে ভারতবর্ষলৈ আহিছিল আরু
প্রাক্্-বৈদিক য়ুগর চমকলগা হরাপ্পা সভ্য়তা সৃষ্টি করিছিল । এই ক'কেছীয ঠালটোর প্রজাতীয
উপাদান দক্ষিণ ভারতত দ্রাবিড় ভাষা-ভাষী লোকসকলর মাজত পো঵া য়ায । এওঁলোকর
দৈহিক উচ্চতা মধ্য়মীযা , দেহর গঢ় লাহী , গার বরণ ক'লা , মূর দীঘল , মুখমণ্ডল
ঠেক , নাক সরু কিন্তু বহল । //
অলপ আগতেই কৈ অহা হৈছে য়ে অতীজতে ভারতলৈ অহা আর্য়্য়সকল ক'কেছীয প্রজাতির
লোক । এওঁলোকর ভাষা আছিল ইণ্ডো-ইউরোপীয মূলর আরু এওঁলোকেই ভারতত এক নতুন
সভ্য়তার পাতনি মেলিছিল । আর্য়্য়সকলর মাজত বিভিন্ন ক'কছীয ঠালর উপাদান আছিল ।
য়েনে-দীঘল-মূরীযা , ভূমধ্য়-সাগরীয , আরু বহল মূরীযা , আলপাইন , ডিনারিক আরু
আর্মেনীয । এওঁলোকর আটাইরে শারীরিক লক্ষণ সমকালীন ইউপোরীয সকলর লগত বিজাব
পারি । গার বরণ পাতল , দৈহিক উচ্চতা ওখরপরা মধ্য়মীযা , নাক জোঙা আরু চুলি মিহি
আরু ঢৌ খোলো঵া । এই ক'কেছীয ঠালরবোরর বিবিধ শারীরিক লক্ষণ উত্তর , পূব আরু
পশ্চিম ভারতর নানা জাতি আরু সম্প্রদাযর মাজত দেখা য়ায ।</p>

<p>(4) মংগোলীয : মংগোলীয প্রজাতীয লক্ষণসমূহ হিমালয অঞ্চল আরু উত্তর-পূব ভারততে
বিশেষকৈ দেখা য়ায । এই প্রজাতির বিভিন্ন ঠালর লোক অতীজরেপরা হিমালয আরু
উত্তর-পূব গিরিপথ অতিক্রম করি বেলেগ বেলেগ সমযত ভারতলৈ আহিছে । সাধারণভা঵ে
মংগোলীয শারীরিক লক্ষণসমূহ এনে ধরণর : চুলি ঋজু অর্থাত্্ ঠরঙা , ডাঢ়ি গোঁফ আরু
গার নোম পাতল , চকু ঠেক আরু বক্র , মুখমণ্ডল বহল আরু চেপেটা , গার বরণ পাতল
হালধীযা আভাস থকা । উত্তর-পূব ভারতর জনজাতীয লোকসকল প্রায সকলো঵ে মংগোলীয ।
অসম আরু চুবুরীযা রাজ্য়সমূহর অন্য়ান্য় সম্প্রদাযর মাজতো মংগোলীয প্রভাব দেখা য়ায ।
বিবাহ আরু সংমিশ্রণর য়োগেদি প্রজাতীয উপাদানবোর বিস্তারিত হৈ পরে ।</p>

<p>এতিযালৈ উল্লেখ করা প্রজাতীয উপাদানর উপরিও আন আন প্রজাতীয সম্ভার বিভিন্ন সমযত
ভারতত সোমাইছে আরু ইযার জনগণর মাজত বিলীন হৈ গৈছে । মধ্য় এছিযার বহুতো
প্রজাতির লোক ভারতত সোমাইছিল । আকৌ , মুছলমান সাম্রাজ্য়ই ভারতত পাতনি মেলার
আদিতে তুর্কীসকলে ইযাত রাজত্঵ করিবলৈ আহিছিল । আবর আরু পারস্য়র ( বর্তমানর
ইরাণর ) বাসিন্দাসকলে প্রাক্্খ্রীষ্টিযান কালতে আহি ভারতর পশ্চিম অঞ্চলত বসতি মেলিছিল
আরু স্থানীয বাসিন্দার লগত বৈবাহিক সম্পর্ক পাতিছিল । আনহাতে ইংরাজ আরু ভারতীযর
সংমিশ্রণত ` এংলো ইণ্ডিযান ' ( ইংগ ভারতীয ) নামর এটা বিশেষ সম্প্রদাযরে উত্্পত্তি
হ'ল । এনেকৈ বিভিন্ন প্রজাতীযসকলর সমম্বযত সুবৃহত্্ ভারতীয জনগোষ্ঠী গঢ়ি উঠিছে ।
ভারতীয জনগণর প্রজাতীয গঠনর বিষযে কওঁতে এটা কথা আমি মনত রখা উচিত য়ে
আজির দিনত কোনো ভারতীয জনগোটর মাজত বা সম্প্রদাযর মাজতে বিভিন্ন প্রজাতির
লক্ষণবোর নিখুঁতভা঵ে বর্তি থকা নাই । সংমিশ্রণ আরু বৈ঵াহিক সম্পর্কর য়োগেদি এই
লক্ষণবোর বিযপি পরিছে আরু নানাধরণে পরি঵র্তিত হৈছে । আমি মনত রখা উচিত য়ে
ভারতীয বুলিলে য়েনেকৈ কোনো এক প্রজাতিক নুবুজায তেনেকৈ অসমীযা , বঙালী বা বিহারী
বুলি কোনো প্রজাতি হ'ব নো঵ারে । আমার প্রায সকলো ভাষিক আরু রাজ্য়িক গোটর মাজত
বিবিধ প্রজাতীয লক্ষণর সমাবেশ দেখা য়ায । একেধরণে ` দ্রা঵িড় ' বা ` আর্য়্য় ' বুলিও
কোনো প্রজাতি হ'ব নো঵ারে । এইবোর প্রাচীন কালত ভারতত বসতি মেলা একো একোটা
বৃহত্্ ভাষিক জনসমষ্টিহে । প্রজাতীয দিশত আমার মাজত থকা বৈচিত্র ভারতীয ঐতিহ্য়র এটা
বৈশিষ্ট্য় । বহু প্রজাতির বহু গঢ়র মানুহলৈ ভারতীয জনগণর সৃষ্টি হৈছে । ফলত ভারতীয
জনগণক ` মিশ্রিত জনগণ ' বুলিও কব পারি ।
 +&gt;*
",0
</p></body></text></cesDoc>