<cesDoc id="kas-w-literature-essay-es91.kal" lang="kas">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kas-w-literature-essay-es91.kal.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>FIKR</h.title>
<h.author>M.ZAMAN</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - BOOK</publisher>
<pubDate>1981</pubDate>
</imprint>
<idno type="CIIL code">es91.kal</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 17-29.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-24</date></creation>
<langUsage>Kashmiri</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;FIKRI HEINZ TAKAR / MOHMAD ZAMAN AZURDA/ ESSAYS&gt;</p>

<p>								0( 17 ) 1</p>

<p>								03شرافت</p>

<p>	شرافتَس متلِق کێنْہ وننٔ برۆنْہ کرٔ بٔ اَکھ ضۆرُوری کتھ عرٔض ۔ شۄ گےِ ےِ 
زِ ہرگاہ مێ کأنْسِ متلِق اکھ رژھ تِ شکھ گژھان چُھ زِ ےِ چُھ شریف مێ
چس اسن ضۆرُور ےِوان ۔ دراصل چِھ اَتھ قدرِ پیٹھ تیتےْا پردٔ کٔھتی.مٔتی
زِ امێُک اصلی بُتھُے چُھنٔ وۄنی بوزنٔ ےِوان ۔ ےَنٔ پیٹھٔ سأنِس مُلکَس منْز
شریف انسان جنٹلمےن سپُد تَنٔ بدلیو اَتھ بێےِ ہن تِ مطلب ۔ ہرگاہ حضرت.عیسٰی(ع)
سُنْد شریف انسان ےَتھ وقتَس منْز تۄہِ برْونْہ کُن ےِےِ ، برْونْہ
کُن کےْا ہرگاہ خےالَس منْز تِ وۄتلِ ، تُہی أسِو ضۆرُور ۔ تس
أکِس بتھی لَبِ تھپٔر کٔڑتھ گژھ تۄہِ دِلٔ.ہَن خۄش ۔ ےێلِ
وۄنی سُ دێےِم بُتھی لب پےش کرِ تٔ اَکھ فتحانٔ اسن کھنْگالٔ
ترْآٔوِتھ لأَےوس تۆہی بےاکھ تھپر ۔ تِ تَوپَتٔ چُھس ےُ تُہنْڈُے
خأر نتٔ جکُس چُھ فیسٔ ورأے ےتھ وقتس منْز نٔر تُلان ۔ وےسے
گژِھ ےِتھِ موقعٔ کم.ازکم سُ پَتٔ نَرِ مۄٹھ دےاوُن نتٔ گنزریوسٔ
تۄہِ تِ شریف ۔</p>

<p>	کێنْہ لکھ چِھ پَنُن سورُے کێنْہ دِتھی پانس خلاف</p>

<p> 
									0( 18 ) 2</p>

<p>کێنْہ وۆہو ، لیکٔ زٔ ژور ، مضمُون ےا پوزار ہیوان ۔  وۆہوَن
تٔ لےکن کےْا چُھ اما مضمُونو سٔتی چھێ شۄہرَتھ گژھان تٔ پوزارَو
سٔتی چھێ ہےر نٔمِتھ روزان ۔ لُکھ چِھ دَپان زِ ےِ چُھ زےْادَے
شرےف بنےْومُت ۔</p>

<p>	شرافتَس یُس لُکھ اَکُے لفظ گٔنْذران چِھ ےِ چُھ اکھ بۆڈ مجرِم ۔
اصلی چِھ ےِم زٔ لفٔظ  شر  تٔ  آفت  ڈۄشوے اکھ أکی سٔنْدِ خۄتٔ
خوفناک ۔ ممکن چُھ ےِم آسن اکھ أکِس نےُوٹرلاےز
کران ےا ےِم آسَن اَکھ أکِس  انٹی.ڈوٹ تٔ اَوے
چھێ ےِ لفظ بُوزی.تھٔے اکھ حٔسین شکٔل خےالَس منْز وۄتلان 
ےِ چَێ بدل کتھ زِ سُ باشرافت چھا پَننِ خأطٔر شر تٔ آفت
کِنٔ بێےَن پیٹھ چُھ اَنِ اَندازٔ نأزِل سَپدان ۔</p>

<p>	منشی.پرےم.چندَس کٔرِن خۄدا سۄرگواس ےێمی زن کھرس کُن
اِشارٔ کٔرِتھ شرافتُک اکھ خاص پہلو لُکن ہوو ۔ کےنْژن لُکن ہُنْد خےال چُھ
زِ شرےفس پیٹھ چُھ خۄداےِ سُنْس فضٔل آسان ، تےلِ کےا عجیب چُھ ےِ 
زِ لکن ہنْذ چھێ أمِس تھۄکٔ لانتھ ۔ دَےِ سنْدین ٹاٹھین پیٹھ کر چھێ لکو مہربأنی 
کٔرمٔژ ۔ تِمَن چُھ سُے ٹوٹھ آسان ےس پیٹھ خۄداےِ سٔنْز پھٹکار
آسِ ۔ مثلن شےطانَس چُھ لُکھ دِٔ جانٔ عاشق حتاکِ خۄداےن چُھ
سُ پانَس نِشٔ دور کۆرمُت ، باےزتھ عہدٔ نِشِ
 </p>

<p>									0( 19 ) 3</p>

<p>ہٹوومُت ۔ تمِ دربارُک سزاےافتٔ چُ لُکَن ۂنْدین دِلَن پیٹھ حۆکُومَتھ
کران ۔ ےۆدوَے ےِ پۆز چُھ زِ خۄدا چُھ دِلَس منْز آسان 
ےِتِ چُھنٔ اَپُز کێنْہ زِ شےطان چُھ انسانَن ہنْزن رگن منْز
خَونس سٔتی دورٔ کران روزان ۔ سأنی ڈاکٹر بچأری کےْآ زانَن
تِمَن چِھ خۄردبےنِ منْز صرف ریڑبلڑکارپسلز ےا
واےٹبلڈکارپسلز بوزنٔ ےِوان ۔ تےوت مےگفانےگ پاور کَتِ آو
وُنِ شیسَن زِ ےمٔ شےطأنی سیلٔ ےہن بوزنٔ ۔ توپتٔ چھا ےمن ژھانْڑُن
کێنْہ ۔ ۔ ےِم چھ دوان اَتھ سأنِس شیٔشس تِ برْم ۔ گاہے سورخ لباس
لأگِتھ روزان تٔ گاہے سفےد ۔ وُچھی.تو کێُتھ ناکارہ چُھ ےِ شےطان ، لوتِ
پأٹھی کورُن حملٔ تٔ بٔتِ  ہےْۆت نَس شرافتَس نِش دور گژھُن ۔ جانٔے
گو جلدٔے پیو ےاد ۔</p>

<p>کےنہ لکھ چھ شریف تٔمی سٔے گنزران ےَس تِمَن خوامخواہ تأریف کران
روزِ ط تہندس خۄشٔک بخش منْذ ولنٔ ےےتِ تٔ پَتٔ مانِ بٔکی بار ۔</p>

<p>سأنو اکثڑ پروفےسر چھ تٔمے.سٔے طألِب.علمس شرےف
گٔنْذران ےس نٔ کلاسس منْذ تمس سوالے
کَرِ ےودوے کریس تِ تٔ تمی جوابٔ سٔتی گژھ مطمےن
ےس سُ پروفےسَر دتھ ہێکِ
 </p>

<p>									0( 20 ) 4</p>

<p>اَفسرس نِش چُھ سُے ماتحت شرٔیف یُس افسرسٔنْذِ کُنِ کَتھِ مانِ ۔ چُھٹی ہێےِ کم 
تٔ بحث کرِنٔ زانْہ صبحَس سُلِ تٔ شامَس نےرِ ژیری ، دۄہَس روزِ کأم کران تٔ افسر 
سٔنْز شرےفانٔ افسری ۂنْذِ درْےِ کران ازکل چِھ جمہوری
زمانٔ ےێمِس سٔتی ےێتی لُکھ آسَن سُ چُھ تێُوت شرےف
ڈلی اگر سأنی جوان ، طالب.علم وزےرن ہنز پأڑوی کران وزارت حاصِل کرنٔچ فِکر ہیوان 
چھ وزیرانٔ ٹھاٹ ژھانڑان 
کانْہ غأر شرےفانٔ حرکتھ ۔</p>

<p>شأےرَن نزدیک چُھ شرےف سُے یُس تِہنْڈس پرْٹھ مصرٔ دےِ ۔ امێُک
چُھنٔ تِمَن پرواےی زِ سُ کےْازِ چُھ پَتٔ تمی شار
پرژھان ۔</p>

<p>	لٔڑکین نِش چُھ شرےفٔ سُنْد تصّور ہنا بدلٔے ۔ ےَس محفل منْز
کَن تھاوِ ۔ خَتھ کم کرِ تٔ اداکأری زےادٔ اما مٔنْزی.مٔنْزی تلی ٹارِ
نظٔر بچأوِتھ کأنْسِ أکِس کُن وُچھِ تٔ پَتٔ موقٔ ےتھ الگ کُن  تٔمی سٔنْدِس
حُسنَس تأریف کرِ سُ چُھ شریف ۔ سُ کےْآزِ آسِ نٔ بێین 
الگ پنٔ.نیو اشارو سٔتی پَنٔ.نِس شرافتس داد ہیوان تِمن شرمندٔ
کران ۔ اَما ےێتِ چھێ شڑافتٔچ کہؤٹ ےِ زِ
گژھن اَکھ أکِس نِش بے.خبر آسٔنی 
 </p>

<p>								0( 21 ) 5</p>

<p>کێنْہ تێلِ چُھ شرافتُک برْم تٔ وقار قأےِم ۔ ؤنی
ےِ راز فاش گو تٔ تێلِ چھ ےِ َرےف شر تٔ آفت بٔنِتھ
شہرُک رۄخ کران ۔</p>

<p>بےگماتَن ہنْزِ واکبلری منْز چُھ ےِ
بێی اندازٔ سٔتِھ ۔ ےێلِ تۆہی وچھُون وتِ کانْہ 
بچس کۄچھِ منْز تٔ بێےِس اۆنگجِ تھپھ کٔرِتھ پکان تٔ
کانْہ برْۆنْہ أمی سٔنْز خۄشپوش بیگم قلو پطرۂ ہِش
گرْآےَن ۂنْدی پأٹھی پنٔنی قدم تُلان تٔ مٔنْزی.مٔنْزی پَتھ
کُن نظراہ کٔرتھ وُٹھ کملاوان زانُن گژھِ زِ ےِ چُھ أمی سنْدس
شراتھ ؤنْدی.ؤنْدی لگان ۔</p>

<p>	نوجوان لٔڑکٔ چھ سیٹھا سمجدار ۔ ےِمَن چھێ پرتھ نوجوان
لٔڑکی شریف لبنٔ ےِوان ۔ اما ےمس أکِس سٔتی ےمو منْزٔ
خانْدر چُھ سَپدان ۔ سۄ چھێ زرِ ہنا غأر شریف باسان
حالانْکِ دال تِ چھێ مزٔدار تٔ مرغُوب سانِ خأطٔر زےادے
اتھ چُھ گلنَس رژھ ہنا ژےر لگان ادٔ کِنی
گمانٔ شریف ؤنِتھ ۔
مولِوےَن تٔ برہمَن ہنْد متٔ پُرژھی.تۆ کێنْہ ۔</p>

<p> </p>

<p>								0( 22 ) 6
ےِہنْدی کِنی چھێ شرافت تٔ بوقُوفی ےِکٔ.وَٹے زامٔژ ۔ یُسُے رژھا
گاٹُول آسِ سُ چُھ ےِہنْدی کِنی غارشرےف ۔ دوے چھ گاٹٔلی تِ ےِمَن برْونْہ کَنِ 
بےقٔل لاگان ۔</p>

<p>	أمیرَن تٔ ؤزیرَن نِش چُھ شٔریف تِمَے ےِم نٔ پَننِ مطلبٔچ کتھ زانْہ
کرن اما ووت دی.دی تھواَن بٔ ےنْدی مطلب پُورٔ کرنس کانْہ کسر ۔
پونسٔ والین چھێ اۆلے انسانَن ہنْدِ خۄتٔ مشینِ پسنْد ۔ ےِمٔ بٹنس 
پیٹھ پکان چھێ زانٔ نٔ کانْہ اعتراض کران ۔ ہرگاہ کُنِ وِزِ کانْہ
پُرزٔ پھٹیکھ تٔ سۄ تِ چھێ کأنْسِ انسان سٔنْزے کانْہ غأر
شرےفانٔ حرکت آسان ۔ انسان چھ ہرتال کران ۔ ےِمَن چھێ
بۄچھِ لگان ، بازرُک درْوجر چُھ ےِمَن متأثِت کران ۔ ےِم چِھ آبأدی
ہُرراوان ۔ مشےنَن ہِنْدِ خأطٔر چُھنٔ ےتھ کانْہ مسلٔ ےێمِ دۄہ
کأم کرن وألی مۆزُور تِ مشینِ بنن تمِ دۄۂ بنن سأڑی
شرےف تٔ مألِک کرن ےمن تِ پٔسنْد ۔</p>

<p>	مےأنی کِنی یُتھ اےکسپلاےٹ شرات چھێ آمٔژ کرنٔ تێُتھ آسِ نٔ بێےِ کانْہ 
قدٔر آمٔژ کرنٔ ۔ بہرحال ےِتھٔ پأٹھی چھُ گاٹلین ۂنْدِ اسنُک سامانٔ بنان ۔ تِمَن
چُھ پرتھ شرےف انسانس منْز اکھ
COmic character لَبنٔ ےِوان ۔
 </p>

<p>									0( 23 ) 7</p>

<p>								03شہرس منْز بَسُن</p>

<p>	پرْتھ کانْہ چُھ پَنِ.پَننِ جاےِ پَننِ لسنٔ.بَسنٔ کِس
طریقس پننِ نظرِ وچھان ۔ گۄڈٔ چُھنٔ ےێتِ کانْہ پنٔ.نِس حالَس
خۄش آسان ۔ انسان سٔنْذ چھێ ےِ فطرتٔے کےْاہتام عجیب
بس چھێ اکھ کتھ ےتِ ارداہ آنٔ ۔ سۄ گٔےِ اڈین ارمان تٔ 
أڑی پشےمان ۔ نٔ گام خۄش تٔ نٔ سرےن منْز روزن وألی
ہرگاہ ےمن پرْٔژھو تٔ ےِہنْدس کن تھاوو تےلِ چِھ سأری
گامٔکی شہر والٔنی تٔ شہرٔ کی گام کھالٔنی ۔ اتھ چُھ وجِ زِ گامکین
باسان شہرس منْز بسُن رحمت تٔ شہرٔ کین باسان شہرس منْز بَسُن زحمت ۔</p>

<p>	مثلاََ گامکین آسان برونْہ کنِ زِ اَسِ چُھ پرْتھ کامِ
موجُوب شہر پیَوان واتُن ۔ حآٔکِمَن ہندی دفتر چھ أتی اہم کارٔبأری
ایجنسی چھێ أتی ۔ ےِ کےنْژھا باگرن چُھ آسان تتھ چِھ
گۄڈٔ شہرٔ کی شرٔیک ۔ کانْہ کأم آسیکھ ےِم چِھ پَتَے
ہیکان تتھ روزِتھ ۔</p>

<p>	ےِتِ چھێ اَکھ رحمت ےوان ماننٔ زِ ےێتِ
 </p>

<p>									0( 24 ) 8
بٔڑی.بٔڑی ہسپتال ۔ انسانَس چُھ اَکھ نتٔ اکھ دود کروُنُے شہرس منْز
چِھس سأری ےلاج حأضِر ۔ خُون کڑنٔ برنس تام ۔ ژٹنٔ پیٹھٔ
واٹنس تام سأری موجود ۔ پرْتھ قٔسمٔ ڈاکٹر ۔ بێ.قضا تِ مَرِ نٔ کانْہ ۔</p>

<p>	اَچھا شہرس منْذ چُھ ٹرانسپورٹ سیٹھ مۄےسَر ۔ گرِ 
درْاو انسان تٔ سوأری چَھس حأضِر ۔ اَمِ سٔتی چھێ زندٔگی
باسان سہل تٔ آرام،دہ ۔</p>

<p>	شہرَس منْذ چِھ بٔڑی.بٔڑی تألیمی ادارٔ ۔ سوکُول تِ گٔے أشی
تھٔے شہرٔکی چھ شُرین آسأنی سان ہیکان اَمِ پَتٔ کالجَن
تٔ تھدین تألیمی ادارَن منْز سُوزِتھ ۔</p>

<p>	رےڑےو ہن ہرگاہ جاےِ نکھٔ انِ ؤنی  اکھ أکِس اما توتِ چھێ
شہرس منْز حالاتن ہنْز زےادہ خنراتر روزان وارےہ قسمٔ اخبار
دُوردُورِ پیٹھٔ وقتس پیتھ واتان ۔ جاےِ.جاےِ چھ پرنٔکی موقٔ تِ
انسانٔ سٔنْز تألمی وأنْس چھێ بڑان ےا کم.ازکم درجس پیٹھ روزان ۔ ریڑنگ
رُوم تٔ کتاب.خانٔ چِھ شہر دراون سٔنْدِ خأطٔر ؤتھی آسان ، نٔو کانْہ کتاب
ےِےِ بازرَس منْز تٔ ےِمَن گوڈَے پَے لگان ۔</p>

<p>	نوجوانَن چھێ نظر آسان زِ نٔوی.نٔوی فےشن چھ شہر برونْٹھٔے واتان
تٔ شہرٔ کی چِھ ےِم وقتس پیٹھ اپناوان ۔ ےێلِ دُوردُور ےژٔکألی واتان چِھ ۔
 
								0( 25 ) 9</p>

<p>ےتھٔ کَنِ فےشنَن منْز تِہنْز وأنْس پَتھ گژھان ۔</p>

<p>	توپتٔ چھێ شہرس منْز رونق پرْتھ ساتٔ باسان ۔ اَدٔ چَھنٔ مۄحرمس 
تٔ عیذ ہِنْز کتھٔے ۔
شہرس متلق چھێ اکھ کتھ سیٹھٔے اہم زِ شہرٔچ زبان چھێ ےوان مستند
تٔ سٔہی ماننٔ ۔ سأرٔ اَتھ تقلےد کران ۔ ہرگاہ کُنِ ساتٔ اکِ لفظٔ کین
دۄن تلفظن پیٹھ اختلاف آسِ تٔ شہر کِس تلفظس چھێ ےِوان ترجےح دِنٔ ۔</p>

<p>	شہرس منْز چُھ انسانس اکُے چیز ژھانڑُن سُ گو پونْسٔ ۔ ےِ گژھِ 
أمِس کُنِ طرےقٔ یُن زےننٔ پتٔ چھ سورُے أمی سُنْدُے ۔ بازر درْآو
یُس چیز خۄش کریس سُ ہێکِ مۆلی ہیتھ ۔ اَدٔ اکٔے فکرا چَھس . رٔنی.تھٔے
ےێژھ انُن سُتِ چُھ مۄےسر ۔ بازر سٔے منْز ێژھِ کھےْۆن تٔ گرێُک
دۄپس تِ کریس نٔ کێنْہ ۔</p>

<p>	شہرٔچ آبأدی چھێ بہرحال زےادٔہ آسان ۔ انسان سٔندِس
حالَس چھ اَتھ بِرِ منْز پردٔ روزان ۔ بێےِ چُھنٔ مجبُورٔے یُتھ بےعت
آسیس تتھێ لکن سٔتی رلِ ۔ نٔ لاریس پتَے کێنْہ نٔ لارِ پتے کأنْسِ ۔</p>

<p>	اما بازرے چُھ باسان زُ شہرس منْز بسُن چُھ زحمتھٔے گۄڈنی چُھ
اَتِ شور آسان زِ زَن چُھ ےِ پاگل خانٔ کتابٔ 
أسی.تن کأژا ۔ اما ےێلِ نٔ پرنٔ خأطٔر پرسکون جاےی أس
 </p>

<p>								0( 26 ) 10</p>

<p>کےْا کَرِ ۔</p>

<p>	بازے چُھ مێ باسان زِ شہرس منْز بسن وول پرْٹھ کانْہ شخص چُھ
نصف زۆر آسان ۔ ہرگاہ أمِس بوزنُک ہوش سلامت آسِ تٔ بس
اڈس نکھٔ ہیکِ نٔ سُ کِہیں بُوزِتھ رےڑٔ والی  ۂنْذِ کرْکٔ بوزِےْا کنٔ ےارن
ۂنز کتھٔ ۔ ےِ گژھِ سیٹھا پرےشان ہرگاہ ےو پٔزی پأٹھی سورُے
بوزِ ۔ مێ کۆر اکِ دۄۂ اکھ تجربٔ شہرس منْز بَس اڈس پیٹھ رُوزِتھ 
أکِس دوستس سٔتی کتھٔ کران تھوو مێ ٹےپ.رکارڈ لأگِتھ پتٔ بوزنووم
سُ دوس شامس أکِس بند کمرس منْز ےِ ۔ ادٔ بیٹھ تٔمِس پژھ زِ
شہرس منْز بسن والین منْز چِھ سأری زٔری ٹےپ بُوزِتھ تٔر نٔ اَسِ
اکھ کَتھ تِ فکرِ ۔ گرا چھ اَتھ گژھان  ملائی والی کلفی   گرا
نوێُٹ ، حول ، لال.بازار  أتھی سٔتی  رٔنی.وور ، خانےار ، سأدٔکٔدٔل 
اَتھ منْز کرکھا زِ رٔٹێُونو  ژور ہا ژورہا ، ہتا زہر ہاکھےونس  ےِ گَو
أھی.تھٔے زِ  کرَکھ نا نابینہس ےأری.ےاہ  ۔ وۄلٔ سأ پےر دستگیرٔ سُنْد
نےازا ۔ اوہ کےْا دِےو پننِ کلک خأراتھا  ےمٔ آوازٔ آسٔ ےیژ رٔلِتھ
زِ وارِےَۂ لٹِ ےِ ٹےپ بُوزِتھ کٔری اَسِ اکثر اندازے
اَسِ تِ آسِ پننِ کتھ ےاد ۔ تِم تِ وَنِ أمی ٹےپَن اما فمرِ کس ترٔہَن
ؤنی کٔری تو تۆہی پانے فأصلٔ زِ أسی
 </p>

<p>									0( 27 ) 11</p>

<p>أسےْا زٔری کنٔ نٔ ۔ اَسِ اوسنٔ پنٔ.نیو کتھو ورأے بُوز مُتے کێنْہ ۔
ےِتِ ما وَنِ کانْہ زِ اَتھ ٹےپس اوس ےِ سورُے اورے پھٹان ۔
	ےۆہَے اثر چُھ أمِس شہرس منْز بسن وألِس أچھن تِ آسان
باضے کانْسِ شہرٔ کِس پنُن کانْہ وَتِ وُچھان ۔ أمی نٔ آسان
وُچھمُتے اما تس ملالٔ گژھان ۔ کُنِ ساتٔ نٔ ےادٔے پیوان ۔
وۄنی ےِیَس دُورِ پیٹھٔ کانْہ ےِ کتِ انِ فرصت تٔ تٔمِس نِش بیہِ.ےِ ۔ 
ےپأری چُھ أمِس آب تانی مۆلی ۔ أمِس چَھ ہچو ہۆل گٔنْڑِتھ
نےرُن ، اَدٔ وانَس پیٹَ آسیس بہُن ےا سڑکِ پیٹھ بأزی.گار
تماشِ ہاوُن ۔ ادٔ سُے ؤنی ملأزِم تێلِ چَھنٔ کتھٔے ۔ امی سُنْد پٔژھِس
سٔتی فرصٔژ سان کتھ کرنی چھێ أمِس مُلزِم بناونٔ خأطٔر کافی
اوسنٔ کێنْہ ہرج ۔ چھٹی ہێےِ.ہا اما دۄہ کےْاہ کَرِےِ چُھٹی 
چھێ مےلان ؤری.ےَس منْز پنْداہ دۄہ ۔ تِمن ےِ تۆمُل کھالےْا ،
نێژتیل انےْا کِنٔ زێُن ، کنٔ باقٔے چےز ژھانڑِ ۔ پیٹھٔ چُھ أمِس
شہرس منْز بسنٔ موجوب یُن گژھُن تِ زےادٔہ آسان حتاکِ
أمِس چھنٔ عیز دۄہ تِ کانسِ نِش گژھنس فرصت آسان ۔
بازے پرْتھ گرس پننِ ہاےِ خےال آسان زِ ےاتٔ
چھنٔ بێس ہمساےَس عےدٔے ےا چَھنٔ اَسِ ۔ گژھان
 </p>

<p>								0( 28 ) 12</p>

<p>ےِ اَمِ دۄۂ تِ أکِس بێےِس نِش اما سۄ تِ بُوزی.تَو نوکری ہشی ۔ اَتھ
چَھنٔ گێنی کڑنٔچ ضۆرُورتھ ۔ تِکےْازِ ےِ زےٹھِ کتھ تٔ تیژ کتِ انٔ بٔ فٔرصتھ
أخٔر شہرُک ہے چُھس ۔</p>

<p>	شہرٔ منْذ زن چَھک ماےی پیروپار کٔرمٔژ ۔ نالٔ.مٔتی باضے شہر
دار تِ کران اما ےِم آسان لولٔ رٔژھی ۔ اَتھٔ،واس کران اما بُتھی
ژٔکرِتھ ۔ ہَے.ہَے تھپھ ما گژھِ ۔</p>

<p>	ڈاکٹرَن ہُنْد متٔ پرْچھی،تَو کێنْہ ۔ ےوت نٔ آدمَس دگ
تلان دود چُھ تێُوت چھس ےِ ڈاکٹر وأنٔج تچھان ۔ ڈاکٹرَن ۂنْز مہربأنی
سٔتی چُھنٔ وۄنی شہرس منْذ اۆرُے کانْہ ۔ کھینٔ برْونْہ اکھ پِل  بۄچھِ
بۄچھِ لگنٔ باپَتھ تٔ کھیتھ بےاکھ شرْپنٔ باپَتھ ۔ ؤرنِس تَل اژنٔ برْونْہے
اَکھ پِل نێنْڈرِ باپتھ تٔ سٔتی بےْآکھ زِ یُتھ نٔ پِلِ سٔتی شۆنْگی.تھٔے روزِ ۔
صُبحَس أکِس ڈاکٹرس نِش تٔ شامَس بێےِس نِش ۔ أکِس پیٹھ کرِ
ہا آدڈ تٔ تیژ کَتِ انِ فرصتھ بێےِس ڈاکٹر سنْد اشتہار تِ کتِ چُھ
تیژ ژیڑ کرنَس وار تھاوان ۔ 
	برْونْہ أسی دپان زِ ضۆرُورتٔ سٔتی چِھ نوی چےز اےجاد کرنٔچ فکِر لگان ۔ اما 
وۄنی
ایجادٔ سٔتی ضۆرُور ہران ۔ ےِ ہریر گَو زِ ےِمَن چیزَن ۂنْز برونْہ خبرٔے أسنٔ 
آسان تمے چھ وۄنی
ؤنی اشد ضۆرُوری ۔ دہن قدمن آسِ گژھن تٔ گۄڈٔ گژھِ</p>

<p>
								0( 29 ) 
بس ےا ٹےکسی آسٔنی ۔ ہرگاہ شُرین ہنْزَن ضۆرورتَن واٹھ دِمَو
ےِمٔ ستٔ.ٹاسٔ پیٹھ شێرُوع گژھِتھ ختمٔے چَھنٔ گژھان تٔ
پان چُھ پر پیوان ۔ فلمٔ تٔ باقٔے تفرےح چِ ژھۄنْبٔ
ژھأنْڑی.ژھأنْڑی فکرَے ہُران ۔
	نیتَو وۄنی شرم ، حےا ، زانُن مانُ گرد ، دُہ بےترِ
تٔ تێلِ  چُھ شہرَس منْز بَسُن پُورٔ زحمت ۔ خأر تۆہی چِھو
پانٔ دانا ونٔ.نٔچ کےْا ضۆرُورتھ چھێ ۔</p>

<p>
================================================================</p>

<p>EOF()</p>

<p>               0 
</p></body></text></cesDoc>