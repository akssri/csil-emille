<cesDoc id="kas-w-literature-critic-cr49.kal" lang="kas">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kas-w-literature-critic-cr49.kal.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>MEEZAN</h.title>
<h.author>RASOOL</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - BOOK</publisher>
<pubDate>1994</pubDate>
</imprint>
<idno type="CIIL code">cr49.kal</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 295-299ْ.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-24</date></creation>
<langUsage>Kashmiri</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>									ْ0ُ 295 ٔ 1</p>

<p>قطرس ہگنگنِ یمن کٔگِنھ ھُرھگنٔ نَنُن نصےن ۄُھ وُنِ نِنھے کٔنی ۄَھنٔ
نگھمٔنی سۆمنٔرمیژ سگٔرٔے شگعری شگہکگر َ ھۄن ہنن سننن صفن
ہنھِس  شِہی گُل  شعری موممعس منْز شگٔمِل گَکھ ہَنھ گَکھ نظمٔ ی ژۄھگہ
غزل نٔ وَژن ی نرێے سگنین ی نٔ منھگہ ہگیٔریںگنھ ی مٔرِنھ ۄھێ ےِ کَنھ
مگنٔنی میَوگن زِ نگٔھِم ۄُھ گَکِ منفرھ گسلُونُک شگعر ی یُس گۃنصگر شگعری ہُنْھ
مُولٔ ہُسن مگٔنِنھ نِ سُ مرْنھ نظمِ منْز قگَٔےِم ۄُھنٔ ہےۆکمُن نٔھوِنھ ی نَۆِ نگووُوھ
نِ ۄُھ نگٔھِمَن لفٔظ گرُن نٔ نگوژنَل نیرل نٔ وورٔ رۆس َ گگر نٔ نگٔھِم نِ
گیسِہے نێلِ روزہے نٔ نگٔھِم ی نگٔھِمٔے َ نٔمی سُنْھ ۃگیصٔ لفظ رگش نٔ نمێُک
وَرنگو  نٔمی سٔنز شنےہ کگٔری نٔ مےکر سگزی ۄھێ سٔننی لگےق َ نگٔھِمنی شگعری
مٔرِنھ ۄُھ کگٔشُر کلۄر نٔ نمھُن گٔۄھرٔ گٔنَو منْز گَسِ نَھلِنَھلِ وُۄھگن نٔ کٔسیرِ ہُنْھ
فطری ہُسن ۄُھ سگنین نظرَن نروننھٔ کنِ نژنِ لگگن َ گورٔ نٔ ےور گرْگیےِ لٔگِنھ ی 
وُنلِ منْز ہینٔ ےِنھ نٔ فرےن کھینھ نُ رُوھ سُ سوننٔ ہوگوَس ھگمگنَس
نَھمھ کٔرِنھ زنھگی ۂنْز سَنھ نێۄھِنگوَن </p>

<p>	وُنی گوس ےێنی گۆو نگلٔ نٔرِنھ ی وگوُک ہُوہُو
 </p>

<p>									ْ0ُ 296 ٔ 2</p>

<p>نَھم نرْگٔوِنھ ژۆل
ننْنٔلگٔوِنھ ژۆل
مَنٔنگٔوِنھ گٔنی نۆن سگل کٔرِنھ ی وگوُک ہُوہُو
نٔھوی نےمگنےم مێ مےْگلٔ نٔرِنھ ی ےۆن گیو نٔ کگنْہ
ھَھرگے لٔوِم
وۆنھ ژُور     ژٔوِم
کَنِ گیو سُ زگنہ نَرٔ ژگو نٔ کگنہ
مُھن مۄکھنٔ سُ گوسُم مگلٔ کٔرِنھ 
ھِز گژھٔنَے نٔھرِ مێے موش ۂرِنھ ی نٔرنیو گُوگُو
وَنٔ مَھنٔے نَھوِمَس ۄشمٔ ؤرِنھ ی ۃوشنُو ی ۃوشنُو
مُھن مۄکھنٔ سُ گوسُم مگلٔ کٔرِنھ ی مَنِ مۄکھنٔ لَوگہ
ھیو رُوشِنھ گو
وَنِ سگمُر میو
لۄنِلۄنِ ژۆل گٔنھی کِنی مگشِ مٔرِنھ ی زَن سوننٔ ہوگ َ
 </p>

<p>								ْ0ُ 297 ٔ 3</p>

<p>کُس نگم مۆک کٔنی نٔلی نگو مٔرِنھ ۃوشنُوی ۃوشنُو</p>

<p>									ُ نظم  وگو  شہِلی کُل ٔ
کُس نگم مۆک کٔنی نٔلی نگو مٔرِنھ ۃوشنُوی ۃوشنُو</p>

<p>		نگٔھِمَن لُوکٔ لَےِ نٔ نژن نرْگےِ سٔنی رَلٔوُن </p>

<p>	ۃطگنےِ ی نٔ رَوزےِ گیہنگ ےێمِ سٔنی نٔمی مننِ شگعری ہُنْھ گیغگز
کۆڑ رُوھ نَس گٔۃرس نگم نِنھے کٔنی سٔنیسٔنی ےِنھٔ کٔنی مگز نٔ نَم ۄِھ نِکےگزِ
سُنِ رُوھ وگٔنْسِ گگہے زمگنَس نٔ وگنگنس نٔ گگہے مگنس سٔنی زنھگی ۂنھِس
مُرگسرگر ھگسنگنَس منْز گھَکِ نَنٔ نێےِ گنھگزٔ نرسر مےکگر َ</p>

<p>سگٔری ھۄہ مےْگٔنی زَن مہگنھگرن     سگرێے رگٔژ ۄھێ رگمگےن
نے ۄُھ ہمگں وِگرس مێ کرےشُن     نٔ ۄُھ ھِل ۄۆن زگنہ رگم گژھگن
گَسِ کِس سٔھرَس ژھگننِ نٔ نرٔہگ   کرٔہگ نٔنھینٔنھی ھرشن ۄون
نێلِ گوس گَؤ نگمَنھ لۄکۄگرَس      زُو مےۆن لۆن مگٔنھی گگم گژھگن
											ُ نگٔھِم ٔ</p>

<p>=================================================================</p>

<p></p>

<p>
               0 
</p></body></text></cesDoc>