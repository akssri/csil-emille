<cesDoc id="kas-w-literature-critic-cr13.kal" lang="kas">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kas-w-literature-critic-cr13.kal.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>JAD.SHAERI</h.title>
<h.author>HAMIDI</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - BOOK</publisher>
<pubDate>1982</pubDate>
</imprint>
<idno type="CIIL code">cr13.kal</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 63/.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-24</date></creation>
<langUsage>Kashmiri</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>ْ0ْ3                              1ُ63ٔ
ْ0ْ3                                 زگن</p>

<p>              شمع زگ وےۆم ہَنِ کے رَنَے ظلمگنَس گَہِ مےۆم
              گَنھ ظلمگنَس لگل کێگ ۄھنَے سُ کَس منَے گوم</p>

<p>	1947ئ ےَس گو ہنھوسنگن گیزگھ نٔ ےِ گو ھۄن ہِصَن ہنھوسنگن
نٔ مگٔکِسنگنَس منز نقسیم ی ےِ گسو گَکھ نگٔرےۃی نٔ نۆہرگٔنی ھَور َ نقسیمَس سٔنی سمھٔھی
مُلکَس منز ۃُونرےز فسگھگنھ نٔ سورُے مُلُکھ گیو ننگٔہی ی گننشگر ی ۃوفٔ روگنٔ وگٔرگٔنی
ۂنھِس گیولٔنِس منز ہینٔ َ ےِمَن ہگلگنَن ہُنْھ سۆن گںر میو گٔھینَن نٔ شگٔےرَن ۂنْھِس 
گێہسگسَس نٔ ذێہنَس مینھ َ ۄنگنۄِ نقسیمکِ گَکلےہُک نٔ فسگھگنَن ہنْھ وگقگنَن
ۂنْھِس نٔنیوَس منز گنسگٔنی قھرَن ۂنْز نۆر نگٔھی رُوز گَنھ وقنَس منز گھنُک نۆڑ ہےُوہ
موضُوع َ گیزگھٔھی سٔنی ےِمٔ گیشِ نٔ وۄمےزٔ لُوکَو وگنسنٔ گیسٔ کَرِ مَژٔ ی نمن لۆگ مھےش
نٔ گمِ  موےُوسی ہنْھ گظہگر گیو وگےِ کرنٔ نٔ فٔنی زگٔوی وگرس رگٔۄھ کرَن وگلین
شگٔےرشَن مںلَن فےض گہمھ فےضس نِ نٔو نٔ گٔمی موضُوعَن نھَمھ َ نٔمی نِ لێۄھ  صنہ گیزگھی 
نٔ زِنھگں کی گےک شگم  ہِشِ نظمٔ ےِمَن منز گیزگٔھی سٔنی وگنسٔ کَرنٔ گیمژَن گیشَن
فگش گژھنُک گێہسگس مووُوھ ۄھُ َ ےِ کَنھ ۄھێ وَننَس لگےق زِ 1947ئ مَنٔ گَو ہنھوسنگٔنی
 </p>

<p>ْ0ْ3                                 2ُ64ٔ 
گَھنَس نٔ شگٔےری منز گَکھ نۆو ےور شَورُو ی گگرۄِ ےِ ۄھێ گَکھ ہقیقَنھ زِ 1947ئ 
#مَنٔ گیو گٔنھَن
ھَہَن ؤری ےَن مینَ مشنمل گَکھ عۆنُوری ھوری ےَنھ منز وُنِ نقسیمٔکی نٔ فسگھگنَن ۂنْھی زَۃم
نگزٔ گٔسی نٔ سمگٔوی نٔ سےگٔسی زِنھگگی ہنز نو نشکیلٔۄ ھرۄس نٔ گفرگنفری نگرسَس
گٔس ی گَنھ ھورَس منز رُوھنٔ گَھنی ذێہَن نِ کُنِ ینوِ َ </p>

<p>	1955ئ مَنٔ ہێنِن سےگٔسی نٔی سمگٔوی ہگلگنھ ینوِ نٔ قرگرٔ نِہنی 0َ گٔھینَو نٔ شگٔےرَو
ہےۄ گٔنْھی مکھِس نٔ مننِس مگنَس وگرٔ مگٔنھی سگم ہێنی نٔ گَکِ نَوِ نۃلیقی ھورُک گیغگز ہےۆنُن
سمھُن َ کشیرِ منز نِ گَو 1947ئ مَنٔ زِ صرف گَکھ نۆو ی مگنِ ھگر نٔ فٔنی نلوغُک
زمگنٔ َ گَنھ ھورَس منز زگے نٔوی سےگٔسی نٔ سمگٔوی مَسلٔ نٔ ۃۄھِ کٔشیر نَنے ےِ منُن
مُسنقنل مگنٔ طَے کرنَس مُنلِق گَک نےن گلگقوگٔمی مَسلٔ ی ےِ مَسلٔ رُوھ نٔ مَہض گَکھ سےگٔسی
مَسلٔ نٔلی کِ 1947 ےَس منز قنگٔلی ہملٔ مَنی کٔشیر ہُنْھ گۆی ہِصٔ مگٔکِسنگنَس منز ےِنٔ مَنٔ
ننےوۆ ےِ کگٔشرین ۃگیص کٔرِنھ ہوش منْھَن نٔ فن کگرن ہنْھ ذێہنی نٔ نفسےگٔنی مَسلٔ نِ 0َ 
مُقگٔمی سےگسنَس منز نضئھ نٔ نھۄلٔ نھۄلٔ نگوۆوُوھ رُوز کٔشیر گقنصگٔھی رٔنگی
کَم زێگھٔ نرۆنہ مکگن َ نگٔلیمی لیہگیظٔ نِ ہێنن لُوکھ نرۄنْہ مَکٔنی َ شہر گگمٔ سۆکُولَن
نٔ کگلوَن ہنْھ نےھگھ ہرنَس سٔنی سٔنی گیے مےشِ وَرگنٔ گِھگرٔ گنوےنگنگ ی مےےکل
نٔ زِ رگٔعنی کگلیو نِ کھولنٔ َ گَمِ علگؤ گیے وۆمِس نِ سرےنگرنس منز نھز نگٔییمِ ۃگٔطرٔ
ےُونورسنی ےِ نِ قگٔےِم کَرنٔ م گنگریزی نگٔلیمِ نٔ گھنَس سٔنی زگن گژھنٔ سٔنی گٔےِ
نوین گٔھینَن نٔ شگٔےرنۂنْھِ ذێہنِۄِ ھگرِ ےَلٔ َ نِمَن نڑے ےِ نظر ی نِم گئے
ےُورمی زنگٔنین منز نَوین نہزینی نٔ فکری نہریکَن نٔ گھنی سرگرمےَن نٔ روہگنَن نِشِ 
وگٔقفٔ 0َ ؤھیھِ ہنھوسنگٔنی زنگنَن مَںلَن گرُھُو نٔ ہنھی ےُو ہگیل گوس نِمن نرونْنھٔے
 </p>

<p>ْ0ْ3                                3ُ65ٔ
ھێۆھ ی نگٔلیمِ وُسَنھ لگنٔ سٔنی گَو نِہنز زگنکگٔری منز وگرَے ہُریر َ گُرھُو نٔ ہنھی
علگؤ گیو رُوسی ی فرگنسیسی نٔ گنگنریزی زنگٔنی ہنْھین کےنْژَن شَہ مگرَن کگٔشر رُوم
ھنٔ 0َ گَمِ علگؤ ۄھێ ےِ نٔنی کَنھ ز ِ کٔشیر رُوز نٔ وۄنی ہنھسنگنٔ نِش گَکھ ھُور ےمٔژ
رےگسَنھ نٔلی کِ نرگنسموِن نٔ رَسلٔ رَسگٔےلٔ کیو وَھیھ ذٔری ےَو گٔنی ےِ نگقٔے مُلکَس نکھٔ 0
نِ صرف مننِس مُلکَس نَکھٔ نٔلی کِ رےیےو ی گۃنگر نٔ کنگنَو کۆر سمنھر مگر مُلکھن سٔنی ھُوریر کَم 0
ھُنی ےہکِس کُنِ نِ مُلکَ منز سمھٔی نَن کێنہ نِ گمێُک گںر ہےۆنُن کگٔشرس مینھ نِ لٔلی
ؤلی مےۆن َ کگٔشری گٔھین نٔ شگیٔےر رُوھی نٔ وۄنی کریری ننِ مۄنھُک ی نِم گئے ذێہنی
نٔ سےگٔسی طور نےھگر ی ہُیگر نٔ وگٔقٔف کگر 0 نِمَو ہےۆن عگلمی سطہَس مییھ وِزِ وِزِ
ےِنٔ وگومنین ننھیلےَن ی نرڈقےَن نٔ کشکمشن ہنْھ شۆعُور مگٔھٔ کرن َ قۄھرٔنی کَنھ ۄھێ
زِ گَنھ نٔوِس مگہَولَس منز نھھنٔ مےمنِس گَھنَس نٔ شگٔےری ۄھُ نۃَو مِزگزٔ ی نٔو 
گٔیرَن نٔ نَووُے ۃَولٔ ۃَط َ </p>

<p>	1947ئ مَنٔ رُوھ ےێنِ ھہَن نہَن ؤری ےَن گَھنَس منز مگرکسی رُوہگن نگرسَس َ 
ےِ ۄھُ سُ وَق ےێلِ گُرھُو ہَس منز ےِ روہگن 1935ئ مینھٔ نرقی مسنْھ نہریکِ ۂنْھِس
صُورَس منز نہَن ژۄھٔ ہَن  وری ےَن گَھنٔۄ وَگ گَنھَشس منز رننٔ مَنٔ ژھۄکُن گوس
ہےۆنمُن َ 1947ئ نرۆنْہ نِ گوس ےۆن مگرکسی لنرےۄر وگنگن نٔ ےێنِ کین کےنْژَن
شگٔےرَن مںلَن گزگھَس گٔس گمِۄ زگن مگٔھٔ گگٔمٔژ مگر نَمِ وَقنٔی ہےۆکِ نٔ مگرکسزم  عگم لگٔعلمی 
مُووُون کگنْہ نہریک نٔنِنھ َ گنوکن نرقی مسنْھ مُصنفےَن ہنز کگٔشر شگۃِ ہنْز گۄینِۄ
مننگ سمٔز نفول ھےنگ نگنھ نگھم  1941ئ ہَس منز ہھوسنگنٔ کِس مشہُور گفسگنٔ نٔویس
رگمگننھ سگگرنِس گِشگرَس مینھ سگٔنِس مشہُور گفسگنی نٔویس مرھےسی
 
ْ0ْ3                                4ُ66ٔ
صگٔننِ گرِ  0َ گَنھ مێُول مُنگٔسِن نٔ موگٔفِق مگہول قنگٔلی ہملٔ مَنٔ َ ےێلِ 
#کگٔشریو مری ینیو
شگیٔےرو یرگمگ نِگگرَو ی سےگسَنھ ھگنَو نٔ ھگٔنِشورو کلۄرل فرنن قگٔےِم کۆر َ ےۄہَ
کلۄرل فرنن گو مَنٔ غلگم مہمھ صگھق ی ھےنگ نگنھ نگھم ی مہوور ی سوم نگنھ زنشی ی 
#مرزگ عگرف نٔی
مروفےسر مشم سٔنْنز رہنمگٔےی منز 1949ئ ےَس منز کلۄرل کگنگرسیس منصز ننھیل 0َ وُلَے
1949ئ ےَس منز سمُھ کلۄرل کگنگرسیُک گۄینێُک گِولگس نٔ گَینھ منز گیو گعلگن
نگمٔ منظئُور کَرنٔ َ گَنھ گعلگن نگمَس منز گیو وَننٔ زِ  کعشیرِ ۂنْھِس مرْنھ کگٔنْسِ نگشۆعُور
نظر ی نێےِ طرفٔ نھوِ سُ رُوعن مسنھن ۂنْزن کُوشِشَن نِ مَھِ نظر 0َ</p>

<p>	گَنِ مینھٔ گیےِ نگضگٔٔ مگرکسی گۆصُولَو مُووُون گَھن مگٔھٔ کرنٔۄ نہریک
 ۄلگونٔ 0 گَمِ ھورٔکی ےێمی گێہسگسَن زِ مۆزُور نٔ مێہنَنھ کَش گۆژھ منُن ۃُون کینٔ
مَنٔ مےزنٔ سگن لسنَس نسنَس لگےق گژھُن ی ننگٔو کلۄرل کگنریسٔۄ گَھنی نہریک نوین
گٔھینَن نٔ شگٔےرَن منز مقنُول نٔ گَمِ وقنٔکی وگرِےگہ شگٔرگے گَنھ سٔنی وگنسنٔ ی ےَِن 
منْز ۄھِ عگصی ی روشن ی نگھم ی رگہی ی عگرف ی کگمل ی سننوش نٔ فرگق شگٔمِل 0 گھنٔ کین
ھۆےمین صِنفَن ےگنے نںٔر نگگٔری ی گفسگنٔ نٔویسی نٔ یرگمگ نگگٔری سٔنی نولُق نھَوَن وگلین
منْز گٔسی سوم نگنھ زنشی ی گۃنر مہی گلیےن ی مرگن کشور نےنرِ مگرکسذمٔکی سرگٔم مگٔرَوکگر 0َ </p>

<p>		مُقگٔمی سطہشس مینھ ےُس گقنرگٔکی گَھن ۃگص کٔرِنھ شگٔےری 1947ئ مینھ
لگ نگ 1960ئ ےس نگم لیکھنٔ گیو ی سُ مۄلَو نٔ وِزِ ۄھێ نقگھَس سۄے
موےُوسی سمھگن ےۄسٔ گرھُو کین ؤسی مگےِ مگرکسی شگٔےرن مںلَن نےگز ہھرٔنی شگٔےری
 </p>

<p>ْ0ْ3                                 5ُ67ٔ
مرنھ ۄھێ گژھگن َ ےژھٔ وگرےگہ مومُولی ھَروۄِ نظمٔ ۄھێ غلگم ننی ع
#گرض ی نگھم ی عگرف
زگری نور مہمھ روشن ی رگہی م کگمل نٔ فرگقَن لێۄھ مَژٔ َ ےِمَن منْز ۄھێ وگرِےگہ نظمٔ 
#نێےَن ھےکھگ ھےکھی
لیکھنٔ گیمٔژ ےگ ۄھِ ۄھرگ مولُومگنھ نظمگونٔ گیمٔنی َ ےِمٔ ۄھێ سگرَس مرومےگنیگ ننگونۄِ
وێۄھِ مِںگلٔ 0 گلننٔ کێنہ شگرےگ کێنہ نظمٔی ۄھێ نِژھٔ نِ لیکھنٔ گیمیٔژ ےِمٔ مرکسی گۆصُولَن 
ۂنھ سَنِ مُطگلُک نٔ ۃۆلُوص نےنُک نگس ۄھێ ھِوگن ی ےِن منز ۄھێ نگھِمنِ کێنِ
 نظمٔ مَںلھن  سونْنھ نٔ ہرھ وزنگٔنی ووشۄِ نٔ مرزرٔۄ مِںگل مےش کرگن َ</p>

<p>	گٔنْھێُم ھَزٔھَزٔ ہینھ گزنگٔھِم گیمُن مزرَس وُزنگونے
مگر وزنگٔنی ووش نورنُک ۃۆلُوص نٔ مزرٔے ےون ہێکِ نٔ شگٔرگنٔ نورنٔۄ نُنی ےگھ نٔنِنھ
نٔلُی کِ ۄھُ گَوِ ۃگٔطرٔ ضۆرُوری زِ شگٔےرگنٔ نورنٔ گژھِ گٔکِس ہھس نگم وزنگٔنی 
#ےنٔ نِشِ نھُون
گٔژھنھ نقول گےلےن شۃصےنٔ نِشِ لۆن ہےۆن نٔ یوروضی منلگزمِ 
منُن 0 نھقسمنی سٔنی ۄھَنٔ ےِم نظمٔ نَنھ فٔنی ھروَس نگم وگنگن ی لہگذگ
ۄھَنٔ کگنْ ہ نِژھ مِںگل نوۆنْنہ کُن ےِوگن ےۄسٔ مگرکسی نظرےس نَل لیکھنٔ ےِنٔ نگوۆوُوھ
فےض سٔنْزھن کےنْژَن نظمَن مںلَن  ملگقگن ی ننہگئی ی شگم  ےگ سرھگر وعفری سٔنْزَن کےنْژَن
نظمَن مںلَن  منھر کی ھگر ی مےرے ۃوگن ی وےل کی رگن ی ھگ سنھ  ےگ مۃھوم سٔنْزٔ نظمٔ مںَلَن
ۄگرہ گرس  فٔنی مۄۃنگی رٔنْگی نِکھٔ ہێکھِ ےِنھ ےگ ےِمَن ہنْھ ووگن ہێکِ مےش کٔرِنھ َ ےِمٔ
ۄھێ لہوِ کِ نژرٔ نٔ ۃےگلٔ کِ شورٔ نٔ رنْگٔ یَنْگٔی نگوۆوُوھ مومُولی نظمٔ َ ےِمن منز ۄھێ
وَضگہَنھ نٔ لیکۄر نگٔزی ی ےِمٔ ۄھێ نگکگرٔ مقصھس نہنھ شۆعُوری طور گرنٔ گیمٔژ ی ےِمن ہنز
نٔی ۃگٔمی ۄھێ ےِ زِ ےمٔ ۄھێ یفظٔ ژگٔرِ نِشوِ مہرُوم نٔ نگقی فٔنی لوگٔزِ مگنَن مںلَن گۃنصگر مسنھی
علگمن نگگٔری ی مُرژٔ گٔری ےگ گنہگمُک ۄھُنٔ کگنْہ ۃےگل گیمُن نھوگنٔ َ کێنہ مںگلِ ۄھێ
 </p>

<p>ْ0ْ3                             6ُ70ٔ
نٔ ھگٔنِشوَرَن گَو مننِ کگٔشر کلۄرُک نٔ گھنُک نۆو گێہسگس مگٔھٔ نٔ مرون نہزینی نٔ گَھنٔ
میرگں ھُنگرٔ زِنھٔ کرنُک نٔ مۄلٔ نگوُک شۆعُور گَو نےھگر َ گٔنھی سٔنی سٔنی گےوو شگٔےرن
نٔ گٔھینَن مننِ مگھری زنگٔنی کگٔشر ۄِ گٔہمِےژ ہُنْھ گێہسگس 0 ھۆےِم کَنھ ۄھێ ےِ زِ کگٔشریو
شگٔےرَو نٔ گٔھینَو ےے4ۆن قومی نٔ مُلکی سطہس مینھ فکری نٔ گَھنی نہریکَن سٔنی 
#سٔنی عگلمی نہریکَن 
نٔ نرگےَن ۂنْز مُویرٔ وگٔقفِےَنھ مگٔھٔ کرٔنی ی گَمِ سٔنی مێول کگٔشر شگٔےری فٔنی نٔ 
#فکری لیہگظٔ نٔوی
وہگن سرکرنٔکی موفٔ َ</p>

<p>	نقسیمٔ منٔ رُوھی ہقیقَنھ نگگٔری ہِنھِس روہگنَس سٔنی کےنْژَن شگٔےرَن
مںلَن فگضل کشمےری ی عگرض نٔ گہھ زرگرنِ شگٔےری منز عشقٔکی نٔ نصوفٔکی کگٔفی ہھَس
نگم رُومگٔنی ےگھگٔۃلےنٔ کِس رُوہگنَس سگ ھِوگن 0َ فگٔضلٔنی عگشقگنٔ شگٔےری ۄھێ کےنْژَن
نوزُک ی موصُوم نٔ ژہَن منز نھلٔ ؤنین وزنَن ۂنْز زگن کرگن 0 ےِ ۄھُ مۆز زِ نٔمی 
#سٔنْز شگٔےری
ہُنْھ زێگھٔ ہِصٔ ۄھُ ریوگٔےنی کَنھَن ہُنْھ گَمنگر نگسگن نِمَے مگشوقٔ سٔنْھِ زِ لف ۃگلٔکی
گفسگنٔ نِ نسٔنْزنے وفگٔےی نِ عگشقٔ سٔنْز ووگٔنی ہُنْھ صھَم َ ےِم ۃےگل ۄھِ غزلَس منز نوری
رٔھیف ی قگٔفےِ نٔ نشنیہَن ۂنْھِ نَورٔ نگوۆوُوھ نگسٔری نٔ گَمزٔی نگسگن ی ےِم ۄھِنٔ شگےرَس
ژمِ نۄن ؤنھی مٔنی نٔ ۃُونَس منز شولے مٔنی ی لہگذگ ۄھِنٔ مرَن وگٔلِس نِ نگرکھسگن َ</p>

<p>	نگہم ۄھێ ےِ گٔمنگنَس لگےق کَینھ زِ فگضِل کشمیری سٔنْزَن غزلَن منز ۄھِ ننھی
شگیرِ نِ مووُوھ ےِمَن منز یفظَس نٔ مگنِ ےھس مێُک گٔژھنھ نورنٔۄ نێہ ۄھێ منُن نگس ھِگون َ 
ےِنھین شگرَن منز ۄھُ نورنٔ وگش کیگن ی عققُک مژرر نٔ ھۄکھ ۄھُ زِنْھگی ۂنھین وِہَن
ۂنز زگن کَرگن نٔ مرن وگٔلی ۄھِ شگٔےرٔ سٔنھِس نورنَس منز شریکھ گژھگن مگر 
#ےِنھی ژِہی ۄھِ
ۃگل ۃگلٔے ےِوگن نٔ موےوسی ی مرگرنٔکی ی نے ہگٔصلی نِ ھِل لگٔ نٔکُوی نرگٔنْنی ۄھِ گژھگن ِ 
 </p>

<p>ْ0ْ3                              7ُ75ٔ
ۃگٔطرٔ نھرمُن َ  مگر ےِ کَنھ ۄھێ ؤنٔنی ضَورُوری زِ عگرضُن وگرےگہ کلگم ۄھُنٔ مومُولی
وزنگٔنی سطہِ مینھٔ نھۆھ کھسگن َ گلننٔ زنگٔنی ۂنز سگھٔگی ی مےۄھر نٔ قۄھرٔنی ہسنٔ نٔ
گۆسلُونۄِ نگزٔگی نٔ نٔرژِ کٔفۓژ کِنی ۄھِ نٔمی سٔنْھی نورنٔ مگٔری مٔنھی نگسگن ی سُ ۄھُ
فگٔضلٔنی مگٔنھی عگم لُوکَن شگٔےر گیسنٔ نگوۆوُوھ مٔنْزی مٔنْزی ذێہنی نھزر مرگوگن ی فگٔضلنٔ
کھۄنٔ ۄھُ عگرض نِس رُوہَس گٔنْھر کگٔشر زنگٔنی ی شگٔےری ی ریوگۓژ نٔی معگشر نٔکی سٔنی
نٔ مگےھگر مُول نٔ سُ ۄھِ گمێُک زێگھٔ لگنھ نُلگن ی نٔمی سٔنْھِس کلگمَس منز ۄھێ وگےِ  وگےِ
نمٔ نرکینیٔ نٔرگ سنےگرٔ مُووُوھ ےمٔ کگٔشرین ۂنْز نہزینی نٔ ذێہنی زنھگی ہنھین ؤری ےِ
وگھَن ۂنْھین نورنن ہنْگ رَس نٔ ووہر ۄھِ مےش کرگن ی نٔ کگٔشر مےژ ہُنْھ مُشِکھ ژَنگن
ۄھِ َ مںلَن ِ موشِ نسنر ی مۄۃنٔ ییکٔ ی سۄنٔ مگلِ ی رۄنِ ھگر ننْگر نَرِ ی نگرٔ وُزمل
لولٔ سینَس ی موشٔ وُن لۄکۄگری گیرٔ نٔرنی غزل ی رنْگٔ ھگر ی شگٔلی نگرَکھ ی شن رنْگ 
شِہێل کُل ی رۄمٔ نَن ی سۄنٔ سگٔوس ی وَژھِ وگٔنی وِ ی لکَۄھِ نگٔوِس ی گَۄھِ کُنھِ ی نُورٔ وُزمَلٔ 
عگٔشِ مَنیو مۄۃنٔ وگلَر ی ھرٔھ گگہِۄ ن مہنگن روےَس ی لولٔ گرھگنَس  </p>

<p>	عگٔرضٔ نِس کلگمَس منز ۄھێ عشقۄِ ھگٔۃلی نٔ رومگٔنی کٔف2ےِژ ےنٔ نین ہمعصرن
مںلھن مہوور ی گیزگھ نٔ فگٔضلٔ سنْھِ کھۄنٔ زێگھٔ رنْگگنٔرنْگێ مگےِ ۂژرنٔ نرھگر نگسگن ی
گگر سُ مرگوےی ہُنْھ شِکگر گژھِ ہے نٔ نٔ ننقیھی شۆعُورَس رَژھ مہن زێگھٔ کگٔم ہێےِ ہگی 
 سُ گژھِ ہگ ےِمَن سگرٔنٔے نوۆنْہ ی نگہم ۄھِ نٔمی سٔنْھی وگرِےگہ شگیرمٔسنی ی سرْێہِ 
نٔ مےۄھرٔ سٔنی نٔرِنھ ِ
                           کون شگہ گٔنْھری رۆنُم نگرنگلَو کِنی ژھۆنُم 
                           نگؤکَس وگٔنِو مھنٔم ہَے ہگے مێگنِ ھلنرو
 </p>

<p>ْ0ْ3                             8ُ80ٔ
             نۄمٔ نَنِ گیےَم مگرٔوَلَے ی نرمرم سرنگمگ
             ژھۄمٔ ہینھ روزھَس گۄمھٔ نَلے ی وننٔ نٔ لےنِ کێگ</p>

<p>	ےێنی نَس ۄھێ گکھ کَنھ وضگہنھ طلن َ سۄ ۄھێ ےِ زِ گہھ زرگر کم فٔنی ۂنھی ےگر ۄھُ
منُن رُومگٔنی نٔ گسرگری ھُنی ےگہ مگٔھٔ کرنٔ ۃگٔطرٔ وَرنگوگن ێ</p>

<p>	گٔزی کین شگٔےرن ۂنْھی مگٔنھی ۄھُنٔ سُ مۆرمُن لےۆکھمُن ی نٔمِس ۄھَنٔ غگٔلنَن
شگٔےرگنٔ نزگکنَن نٔ ضۆرُورنَن ۂنْز نیژ زگن ےیژ زگن نوین شگٔےرن مںلھن رہمگن رگٔہےَس
ۄھێ َ نگہم ۄھێ ےِ ہگیٔرنَس نٔ ۃۆشی لگےق کھنھ زِ نٔمِس ۄھُ شگرُن علمٔ غگٔنٔکی 
ٔگٔنھی ھرھَس
گیمُن ی نقو غگلن ِ
              گینے ہےں غےن سے ےِ مضگمےن ۃےگل مےں
              غگلن صرےر ۃگمِ نوگے سروش ہے </p>

<p>	نصوفُٔکی موضُوع وێھی گینٔ نٔ نِمن مینھ نگضگٔنطٔ مگٔنھی سونۄنٔ نگوۆوُوھ ۄھُ
گێگَہھ زرگر نمن نۃلےقی عملِ سٔنی شکٔل نھلگونَس منز کگمےگن گژھگن 0 ےِ ۄھھنٔ مومُولی کَنھ
مرنھ کگنہ شگٔےر ۄھُنٔ نصوُف ےگمزہن شگرہیکگن ننگٔوِنھ 0َ مُووُوھٔ صٔھی منز ۄھِ مگسنر
زِنْھٔ کول ےگ غلگم رسول کگمگگر ہوی شگٔےر نِ لیکسگن رُوھی مٔنی َ مگسنر زنھہ کول ۄھُ
وۄسنگھَن منز گٔنْزرَنٔ ےِوگن َ نٔمی ۄھُ نصوف ی مزہن ی گێگن ی گیۃلگق نٔ گنسگٔنی ےَنھ
منُن رُوہگٔنی غٔزگ ننوومُن طَ ےِم ۄھِ سینھگ نھھی ۃےگل نٔ زِنْھٔ کول ۄھُ ےِم ۃےگل نزرگگنٔ
لہوَس منز نےگن کَرگن م مگر مُشکل ۄھُنِ ےِ زِ نٔمی سُنْھ وگرےگہ کلگمٔ ۄھُ ہنھُوھےو مگلگنٔ
سنسکرٔن لفظو سٔنی مُشکِل ننومُن نٔلی کِ ےِ زِ نٔمِس ۄھُ نٔصیہَنھ کرنُک غگٔر شگٔےرگنٔ
گَنْھگز قگٔےِم روزگن َ شگر ۄھُ زنْھگی ہنْھ گنکشگف نَ کِ نٔصیہَنھ ی نٔ شگرَس منز
 </p>

<p>ْ0ْ3                            9ُ88ٔ 
گَنھگزَس منز نگونٔۄ کُوشِش کَرگن ےُس مرن وگلین ۂنْھ ۃگٔطرٔ وۄمرےگ مُشکل گیسِ نٔ
سُ ۄھُ مننین وگٔری ھگنَن منز نێےَن نِ شٔریکھ کرُن ےژھگن طَ لہگذگ ۄھُنٔ وگننگوِرٔ
نِشِ گٔۄھ وَننُک سَوگلٔے مگٔھٔ گژھگن ی مگر ےِ سوۄُن زِ شگٔےر ۄھُ صگف ی وگضیہ نٔ
قطعی گنھگزَس منز منُن ۃےگل ےگ نورنٔ نێےَن نگم وگننگوگن ی ۄھُنٔی صہی ی نکێگزِ
شگٔےری ےۄسٔ نُنی ےگٔھی کنھ نٔژ نِشِ نےۆن ۄھێ کیگن سۄ ۄھێ ےِہَے زِ شگٔےر
ۄھُنٔ وگضیہ ی نگکگرٔ نٔ قطعی نورنٔ مِش کرگن ی گکێُک وَوہ ۄھُ ےِ زِ شگٔےر ۄھُ
مَنٔنی نورنٔ نٔںرلیِکھَن وگلِس نِشِ گلگ گٔژھنھ ی مھگٔلگٔوِنھ نٔ وَضگہنٔ نوگےِ
گۃنصگرَس نٔ شِھنَس کگٔم ہیوگن َ گۃنصگر نٔ یِھَنھ
ۄھێ لگٔزی می طور شگیرَس گسنےمگٔری نٔ علگمٔنی گنہگر ھِوگن نٔ نٔنیوِ ۄھُ زِ سگٔےرگنٔ
نورنٔ ۄھِ ؤلِنھ ی وَرھگر نٔ غگٔر قطعی گۆسلونَس منز وۄنلگونٔ ےِوگن َ قۄھرٔنی کَنھ ۄھێ
زِ شگٔےری منز ۃگص کٔرنھ نِژھِ شگٔےری منز ےَنھ منز سٔنی نٔ میۄیھٔ نورنٔ گیسَن
گژھ گنہگم مگٔھٔ نٔ ےُون کُنِ شگرَس منز گنہگمٔکی گمکگنگنھ گیسَن نےُون ۄھُ نَنھ فٔنی
مرننٔ ہرگن ی گَمِۄ نگکگرٔ مِںگل ۄھێ غگلن نٔ گےلےن سٔنْز شگٔےری مےش کرگن طَ
ےَنھ ویژضھنگونٔ ۃگٔطرٔ شرہَن نٔ ننقیھَن ۂنْھی گَمنگر ۄھِ لٔگی مٔنی نٔ ےِ کگٔم ۄھێ نرگنر
وگٔری َ</p>

<p>	 گہھ زرگرن ۄھُ مننین ہمعصرن منز گنہگمٔ کِنی شگٔےری نھزر نٔ نور عطگ
کۆرمُن َ ےِ ۄھھنٔ کُنِ صُورنَس منز نٔمی سٔنْھِ فنٔۄ کگنْہ ۃگٔمی ی گمےن کگمل ۄھُ 
لیکگن ِ
گنہگم ےنھ گٔسی صُضر نٔ ۃطرنگکھ ۄھِ وَنگن ی گوےگ نٔ نے مگنے
شگروَنٔنی ےگ ھکھ موضُوع نٔ ۃےگل نِنھ مگٔنھی نےگن کرن ی ےێمُک
 </p>

<p>ْ0ْ3                             10ُ89ٔ
وگننٔ نێگز نگکگم گیسِ رُوھمُن نٔ ےَس گِنھگن گننہگٔےی کُوششِ
نگوۆوُوھ کُنِ صُورنَس لٔنِنھ ہێکِ نٔ َ گہھزر گرُن وگرےگہ کلگمٔ
ۄھُ گَمی مُنہم گوےی ہُنھ شِکگر َ</p>

<p>	کگٔمِل صگٔننِ گَمِ نےگنٔ نَلکٔ ۄھُ نَنگن زِ نَمو ۄھُ گنہگم ھرگَصٔل گِشکگلٔ کِس مگنِ ےَس
منز گسنےمگل کۆرمُنَ گِشکگل ۄھُ مٔزی مگٔنھی شگرَس معمِ ننگوگن نٔ گَمِ کِ
موضُوعُک وگنٔ نێگر ۄھُ نٔ مُشکل نٔلی کِ نگممکِن ننگن َ ےِ ۄھُ مٔزی مگٔنھی شگٔری نۄقص
مگر ےِ کَنھ ۄھێ وننَس لگےق زِ گہھ زرگرُن کلگم ۄھُنٔ گَمِ گِشکگلُک ُےَنھ کگمِل صگٔن
گنہگم ۄھُ وَنگنٔ شِکگر َ مزر ۄھُ ےِ زِ نہنھِس کلگمَس منز ۄھُ مٔنْزی مٔنْزی گنہگمٔۄ
کٔفےنھ مگٔھٔ گژھگن ٔ ۓمی سٔنْھین کےنںۄَن شگیرَن منز ۄھُ موضُوع نِنھ کٔنی ورنگونٔ گیمُن
زِ سُ ۄھُنٔ عگم مرن وگٔلی سٔنھِ ۃگٔطرٔ لنُن گیسگن َ سَوگل ۄھُ مگٔھٔ گژھگن زِ شگیر گژھێگ
 مرنھ مرن وگٔلِس فکرِ نرُن ی سٔنی ۄھُ ھۆےُم سَوگل ےِ وۄنھگن زِ شگیرَس منز گژھێگ
لگنُھی فکرِ نروُن موضُو ےگ ۃےگل مےش کرنٔ ےُن ی گمێُک مگنِ گژھێگ مُورٔ مگٔینھی فکرِ نرُن 
 ےۆنگ منھ گۄینکِ سوگلُک نولُق ۄھُ ےِ کَنھ ۄھێ مگننٔ گیمٔژ زِ شگیٔےری ۃگص کٔرنھ سۄ
شگٔےری ےنھ مَنْز سٔنی نٔ کھُری ھگر نورنٔ گیسَن ی ۄھَنٔ مرنھ کگٔنْسِ مرن وگٔلی سٔنھی
مُطگلنٔ مُورٔ کَرگن َ گَمِ کین مرن وگلین ہُنْھ ہلقٔ ۄھُ مہھُوھ گیسگن َ0 گیزرگ مگونی نٔ
گےلےن سٔنْز شگیٔےری کگٔنہن ۄھێ فکرِ نرگن ێ غگۆنٔ سٔنھی مرن وگٔلی ۄھِ گَزنِ
ۃگصُل ۃگص َ وھیھ کگٔشر سگٔےر رہمگن رگہی ۄھُنٔ عگم لُوکَن ہُنْھ شگٔےر رُوھمُن
لہگذگ گگر گہھ زرگرٔنی کےنْژَن شوقٔ وگلِ نٔے فکرِ نرن ی ۄھێ نٔمی سٔنْھِ شگٔےرگنٔ وۆوُوھٔۄ
 </p>

<p>ْ3 ْ0                         11ُ92ٔ
گےلےن سٔنْز مشہُور نظم ویسن لےنی ُ1922ئٔ نٔ گٔنھی وقنَس منز مگونی سٔنْز گمےوسن
نہرےکِ نھلوو شگٔےری ہنھ مزگزٔ وگرِےگہَس ہھَس نگم َ مُصوری منز ھێُن 1905ئ َ 1910ئ 
ےَس منز مکگسون کےو نرہکس روہگنَس ریوگو َ فزسکِ نَوِ نہقیقگنٔ سٔنی ویسرے زمگنِ
مکگنٔکی مرْگٔنی نصورگنھ ےنھین نَوین ہگلگنَن منز لۆگ گِنسگٔنی سوۄَس لَرِ مھێُر نٔ مُووُوھ
صٔھی ۂنھِس نیصفَس نگم وگنگن وگنگن نھلے ےِ ھُنی ےِہکی ہگلگنھ نَمِ نیزی سگن
زِ گٔۄھھن لگرےَوو گےُور نٔ شَۆعُورَس گَو نلُکھ مینھ َ گَمِ سٔنی نیلےۆو سگرێ گھنُک
رنگ نٔ گَنہگر َ گٔھینَن نٔ شگٔےرن ۂنھِس سوۄَس نٔ ذێہنَس ہیژَن نگرٔ ننھیلےِ
ےنَِ مرْگٔنی نظرِےِ ی فلسفٔ نٔ قھرٔ ہیژَن ۄھَلِ ۄھَلِ گژھنِ نٔ فنکگرَس مُیو ہیسٔ
ویسرگون وگوِ نین ہقٔیقنَن ہنْھ مُقگنلٔ کرُن َ </p>

<p>	1955ئ مننٔ ےێمِ ۃۆلُوصٔ نٔ گعنمگھٔ سگن سگنیو کگٔشریو شگٔےرَو مننین
کےنْژَن نظمَن نٔ غزلَن منز نَوِ گیگہی ہنْھ گظہگر کۆر ی نَمِ سٔنی ۄھُ نَنگن زِ کگٔشر گَھنی
ذێہَن ۄھُنٔ مُلکی نٔ غگغٔر ملکی سوۄَوِ نٔ سوۄَن نرگےَن نِشِ مَنھ ےگ لۆن ہینھ َ ےِ ۄھُ نرگنر
مھگنمھلگن ی نٔوی گںرگنھ وینرگوگن نٔ وێمرگوگن نٔ نرۆنْہ کُن مکگن َ نٔوی شگٔےِر
ۄھِ کےنْژھگ لگشۆعُوری زگٔنی سٔنی نٔ کےنْژھگ علمٔ نٔ ۃنر ذٔری ےِ کگٔےنگٔنی نورنَن نٔ
ھُنی ےگٔوھ مسلَن ہُنھ ننیر کرگن نٔ کےنْCھگ ۄھُ مُلکَس منز نِ نِنھی ہگلگنھ نٔ وگقگنھ سمھگن
رُوھی مٔنی ےِم نَوِ گیگہی گۄی نرگن نٔ مھگنْملگوگن رُوھی َ گۄینِۄ کنھ ۄھێ ےِ زِ 1955ئ
مَنٔ ہےۆن ےێنِ کیو  سےگٔسی نٔ سمگٔوی ہگلگنَو کُلہُم مگٔنَی گَکِ ینوِ روزُن ی سےگٔسی نی
سمگٔوێ مسلٔ ےِم ذێہنی نٔ وزنگٔنی وُنِش گٔسی ھِوگن ی ننے ےِ لگشۆعُورٔ کی ہصٔ نٔ
لگشۆعُوری کھرین نٔ وۄکھَن منز گو گضگفٔ نٔ نو گھنی شۃصےنھ ننے ےِ سینھگہ مےۄیھٔ نٔ
 </p>

<p>ْ0ْ3                            12ُ93ٔ
کھری ھگر َ گمێُک نفسےگٔنی مہلُو ننۆو زێگیھٔ گَہَم نٔ نووہَس لگےق طَ گنھ میو ملکی نٔ
غگٔر ملکی مےۄیھٔ ہگلگنَن ہُنْھ نٔ سێھِ سےۆھ گَںر طَ </p>

<p>	ھۆےِم کَنھ ۄھێ ےِ زِ رےگسنَس منز نگٔلیمی سۆہُولےگنھ م4ےسر سمھ نٔ منٔ
رُوھی نٔ سگٔنی شگٔےرگن مر ی نمَو ہێںر گعلگ نگٔلیم ہگٔصِل کَرٔنی نٔ ۃگص کٔرنھ کۆر نِمو گُرھُو
ہنھی ی نٔ گنگریزی زنگٔنین ہنْھ گھنُک نٔ گَھنی نہریکَن نٔ نرْگےَن ہُنْھوگرٔ مگٔنھی
مُطگلٔ ی گَمِ مُطگلٔلی ھرْگے وگن نٔ مُفیھ نٔنیوِ طَ شگٔےرَن ۂنْھِس شۆعُورَس
منز گَو نٔرژَر نٔ وُسَنھ ی گَھنی نہریکَن نٔ ننھیلےَن ۂنز زگن مگٔھٔ کرنَس سٔنی سٔنی
کۆر نمَو نێےَن زنگنین ۂنھین شہ مگرَن ہُنْھ مُطگلٔ ی نمَو وُۄھ نٔوی گَھنی فگرم
ُہےژٔ نٔ گظہگر ی نٔ ہےنِکھ نظم لُعرگی گیزگھ نظم ی سگینن ی سوفےھ شگر نےنر کگٔشرس
منز گَنٔی ی ےِمَو نورنَو نھلگو شگٔےری ہنْز شکلٔ صُورَنھ َ</p>

<p>	نرْێےِم کَنھ ےێمِ نۆو شگرٔ مِزگزٔ وۄمھگونَس نٔ مُروَوِ کرنَس منز مھھ
ھێُن ی ۄصھێ ےِ زگِ گَنھ وَقنَس منز گیے نێےن زنگٔنُین ہنھین وگرِےگہَن شگٔےرگنٔ
نۆمُونَن کگٔشرِس منْز نرومٔ کرنٔ َ ےِمھو نروکَو ھێُن شگٔےری نٔرژر نٔ رنگگرٔنْگی
ہُنْھ گێہسگس َ نرومٔ گٔسی نرۆنہ نِ سمھگن َ ۄنگنۄِ مرگنیو شگٔےرَو مںلَن
مقنول شگہ کرگلٔ وگٔری ی مہموھ گگٔمی نٔ ولی گللّہ منو نٔ نگقٔی شگٔےرَو ۄھێ وگرےگہ
نُمنگم نٔ مَشہُور فگرسی مَںنٔوی ےِ کگٔشرگوِ مَژٔ نٔ زَنگٔنی نرژر کۆرمُن َ وہگن مرےَن
ۄھێ  شگہ نگمِ فرھوسی  ہِش نرٔ کنگن کگٔشرس منصز مھر مٔژ نٔ گیزگھن لێۄھ گقنگل
سُنھ گںر رٔننھ  شکوہ گنلیس َ گیزگھس مَنٔ نِ رُوھ نروکَن ہنْھ
سِلسلٔ وگٔری ی مہی گلھےن ہگٔونےَن کۆر  مسھس ہگلی ےُک نرومٔ َ نَےگورٔنی کێنہ 
 </p>

<p>ْ0ْ3                            13ُ94ٔ
نںری نٔ منظُوم گَھن مگرٔ نِ گیے نرومٔ کرنٔ َ نرومَن ۂنز گَہم نٔ موشِ ؤنی
کگٔم کٔر غکگم ننی ۃےگلن ی عمر ۃےگم سٔنْز رُنگٔعےِ کگٔشرس منز مھرنھ ُ ےِ کگٔم ۄھێ
مرزگ عگرفن نِ گوێوگم ھِژمٔژٔ گَمِ علگؤ وگنٔ نوو نٔمی گرسطو سُنْھ  مولنیکسُک 
نرومٔ نِ گَنْھ َ گَمِ علگؤ ۄھُ فرگقھن کےنس سٔنْز نظمِ
نرومٔ نلنُلَس کن نٔ رگہی سُن ھ یگکنر فگسنس کِ گٔۃری منظرُک نرومٔ گَہمےنھ 
نھوگن َ</p>

<p>	ےِمَو کُوشِشَو مَنٔ نِ رُوھ نروکَن ہنْھ عمل وگغری ط ۄنگنْۄِ شےکسمےرنین
یرگمگہَن ۂنھیو کےنْژن ہِصَن ۂنھِ نرومٔ مینھٔ ُ ےِمن منز کێہن نگھِمن نِ ۄھِ
کٔری مٔنی ٔ نوین گرھُو شگٔےرن مںلَن مہمھ علؤ سٔنْزَن کےنْژَن نظمھن ۂنھی نرومٔ
نِ گیے کَرنٔ طَ ےێنی نَس ۄھُنٔ مےۆن مُھٔگزِ نٔ کرٔ نروکَن ہُنْھ فہرسن مُرنن َ
نٔلی کِ ۄھُ مێ ےِ ونُن زِ نرومٔ گیے مَے ھر مَے کرنٔ َ کێنہ کمزورنٔ کێنِ وگن نٔ
زنگٔنی وُسَنھ مرٔھٔی کرنس سٔنی سٔنی ھںُن ےِمَو سگنِ ؤھیھ شگٔےری منز شگٔےرگنٔ
نورنٔ زگٔنی رگونَس ی مھگنمھلگونَس نٔ مگٔرگونَس منز مھنٔ مێ ۄھُ نگسگن زِ مُووُوھٔ
ھورَس منز گیےِ کێنہ نظمٔ نٔ غزلٔ وۆوُوھَس منز ےِمَو مزی مگٔنھی کگٔشر شگٔےری ہُنْھ
ھَروِ نھٔز روو َ ےِژھن نظمَن منز ۄھُ لفظٔ ںرگٔرِ ہنْھ گێہسگس گنگن نٔ نورنُک
سۄن ۄھُ گٔمنھرِ مِ نگرٔ نیرنھ ی غگٔر شگٔےرگنٔ ہِصَو ےگنے کھۄۄرٔ نِشِ مگک وصگف
سمھِنھ مگسٔ سۄن ننےۆمُن َ شگٔرگنٔ ہِصَو ےگنے کھۄۄرٔ نِشِ مگک وصگف
سمھِنھ مگسٔ سۄن ننےۆمُن َ  شگٔےر ۄھُ منُن نورنٔ مگنٔ نِ مۄلنگوگن نٔ مرنھ
سگنٔ ۄھُ مننِ صلگٔہےژ کگٔم ہیوگن َ گَمِ لہگظٔ ۄھُنٔ ےِ ونُن غلقط زِ نَوِ شگٔےری
ۂنز گۄینِۄ ۃَوصُوصےنھ ۄھێ گَمِۄ ۃۄھ گیہگگٔہی َ ےِ ۃۄھگیگگٔہی ۄھێ سگٔےری
منز 1960ئ نرۆنْہ کُن وۄنٔ کےمٔژ نٔ مرگنِ شگٔےری نِشِ گلگ نٔ نھُون گژھگن َ
 </p>

<p>ْ0ْ3                           14ُ95ٔ
گیزگھ نٔ مہوُور ۄھِ کٔشیرِ ۂنھِس گَھنی مۆن منظرس منز ۃےگل نٔ گظہگرَس منز
نویر وَرنگونٔ نگوۆوُوھ نَمِ ننقیھی نظرےگ نۃلیقی ۃۄھ گیگگٔہی نِشِ وگرِےگہَس ہھس
نگم ھُور نگسگن ی ےۄسٔ گٔزی کِس ھورس منز رگٔہےس 4گ کگٔملس مھھ ۄھَ کرگن َ </p>

<p>	شگٔےری ۄھێ نُنی ےگٔھی ٔطور گَکھ نۃلیقی فن َ شگٔےر ۄھُ مُووُوھ کگٔےنگنَس
منز مننِ نظر ی نۃےلٔ نٔ ووھگنٔ سٔنی گَکھ نٔو ی مگٔری مٔنْز نٔ شُونِ ھگر کگٔےنگنھ مگٔھٔ کرگن َ
گرسطوہَن ۄھێ سینھگ نرۆنْنھ نگوَنھ کٔرمٔژ شگٔےر ۄھُنٔ مُوُوھ کگٔنےگنٔۄ ےگ فطرنٔۄ
نقگٔلی کرگن ی نٔلی کِ ۄھُ سُ شۃصےنٔ ۄو گنھرِ میو قۄوَنَو سٔنی گَکھ نَو کگےنگنھ ھٔری ےگف
کرگن طَ گَنھ کگٔےنگنَس ۄھُ نێنُھ ووھ گیسگن زِ مرن وگلین ۄھُ منُن گۆنْھ مۆکھ
مٔشِنھ گژھگن َ شگٔےرگنٔ نورنٔ ۄھُ مُشگٔہھٔ ی نۃےُل ی ہےگن نٔ گِھگک کیو مٔنزی لَو
منْز گُزرنھ منگٔسِن نٔ موزُون لفظَن منز شرمگن نِنھٔ کٔنی زِ لفظن نٔ نورنَس ۄھُنٔ
مگنٔ وگٔنی کگٔنہ ھُوریر روزگن َ ےِم ۄھِ ھگٔۃلی گۆصُولَن نہنھ نقول ہریرن رےی
عضوی ہےنَس منز ننھیل گژھگن نٔ فَن مگرٔ ۄھُ گَکھ ۃۄھ مۃنگر گِکگٔےی نَنگن َ
گُنھُو شگٔےری منز فَن مگرٔۄ ےِ ۃۄھ مۃنگر گِکگٔےی مرز نگونٔ نَمٔ نگٔو 1955ئ ےَس
نکھ نکھ نگصر کگظمی ی مےر نےگزی ی گنن گنشگئ نٔ ۃلےل گلرہمگن گعظمی ےَن 0َ ےَمَو شگٔےرو
کۆر شگرُن ریوگےنٔ کیو زولگ نَو نِشِ گیزگھ نٔ ھگٔۃلی لہوُک ہُسُن کۆر ہَس عطگ َ
مرزر ۄھُ ےِ زِ نقسیمٔ نرونْنھٔے کۆر گقنگلس نٔ عظمن گللّہ ۃگنَس مَنٔ ہلقِ گرنگن
ذوقٔ کیو شگٔےرَو علگؤ رگشھ ی مےرگوی نٔ گۃنر گلےمگنَن ہَےنٔ کین مرگنین طرےقَن نکگر
نٔ شگٔےری منز ھێنُک نورنٔ مسنْھی مینھ زور طَ ےِ کُوشِش گٔےِ وگرٔ وگرٔ گٔکِس
نٔوِس روہگنَس منز ننھیل نٔ ےِ رُوہگن ہےۆنُن 1955ئ ےورکُن نۆن نٔےرن ےێسی
 </p>

<p>ْ0ْ3                          15ُ96ٔ
نوین شگٔےرن ۂنز گَکھ نٔی ومگنھ مںلھن مہمھ علوی ی شہر ےگر ی عمےق ہنفی ی کمگر مگشی ی نلرگو
کومل ی فگروقی ی قگضی سلےم ی نگقر مہھی ی نِھگ فگضلی ی نٔ وزےر گیغگ نَنٔ مَنٔ گَمٔکی طٔرفھگر
نٔ نرومگن نَنے ےِ ی نَوِ شۆعُور کِ فنکگرگنٔ گٔہگرُک مٔطلن ۄھُنٔ زِ گُرھُو شگٔےری
ہُنْھ مرون سرمگےِ ۄھُنٔ مۄلُل ی وزنھگر نٔ نھیکُن لگےق َ مےر ی غگلن نٔ گقنگل
ۄھِ گَسِ نرۆنْہ کَنِ سگٔےرگنٔ نھزرٔکی نٔ نورٔکی نگکگرٔ ںۆنُونھ َ کنھ ۄھێ صرف ےِہَے
زِ مَنھ کگلِ کۆر شگٔےری مُۃلنف شۃصےنَو نٔرژر نٔ ہریر َ گَکھ میرگ ےگ غگلنگ گَو مگٔھٔ نٔ
کلہُم مگٔنھی گیو نٔ نۃلیقی رُوہگن نگرسَس ےنھ گکِ کھۄنٔ زێگھٔ شگٔےرَو گۄینۆر نٔ ےێمێُک
ظۆہُور گٔسی گٔزی کِس ھورَس منز وۄھگن ۄھِ م گَمِ نۄقطَے نظرےِ کِنی ہَے وُۄھو
نێلِ ۄھَنٔ مرگٔنی کگٔشر شگٔےری نِ سگٔرٔے ریوگٔےنی ی ژ ھرنٔ ۄھێنی َ گَنھ منز نِ وُۄھَو 
نێلِ ۄھَنٔ مرگٔنی کگٔشر شگٔےری نِ سگٔریے ریورگٔےنیی ژھرنٔ ۄھێنی َ گَنھ منز نِ ۄھێ
ننھٔ رێش ی لل ھیھ ی ہنٔ ۃونُون ی رسل مےر ی شمس فقےری  رہمگن یگر ہِشِ نھذٔ شۃصےژ
مگٔھٔ گگٔمٔژ 0َ ےِہنھِس کلگمَس مینھ گَکھ نظر نرگٔوِنھ ۄھُ نَنگن زِ ؤسی مرھِ نٔ مومُولی
کلگمٔ کِس گَمنگرس منز ۄھِ مۄلٔلی ی نَہ ھگر نٔ شُونِ ھگر فَن مگرٔ نِ گگہ نرگوگن َ 
کێنہ مِںگلٔ وُۄھوِ ےِمَن منز فنُک ۃۄھ مۃنگر وۆوُوھ منُن نگس ۄھُ ھِوگن ِ
                     
               گیےس وَنے گٔےَس نٔ وَنے
               سمَن سۄنھ منز لکُوسُم ھۄہ
               ۄنْھَس وُۄھُم ہگرنٔ گَنے
               نؤ نگرس ھِمٔ کێگنو
 </p>

<p>ْ0ْ3                              16ُ101ٔ
مگر ےِ کَنھ ۄھێ وننَس لگےق زِ 1960ئ مَنٔ ےێلِ نگٔھِ مٔنِ نَوِ نظمٔ مںلَن
نگنھ نٔ نینھٔ وین ی لکھۄِ ۄھُ لکھۄُن  ےگ  کگٔنھی ھروگزٔ مینھٔ گرٔ نگم  نرۆنْہ کُن گیےِ
نگٔھمٔنِ نۃلیقی وۆوُوھُک نۆو گَنہگر ہےۆنُن مرزلُن َ ےِمَن نٔمن منز ۄھُنٔ گنقلگنُک
گرْزےگسێسِ سےۆھ مگنِ مطلنُک ےگ موضُعُک گظہگر سمھُمُن ی نٔلی کِ ۄھُ ےِمن منز نگٔںرگٔنی
نٔ علگمنی گَنْھگز ورنگونٔ گیمُن َ نگٔںرگٔنی نٔ علگمنی گنھگزٔۄ ےِ ملٔ وَن ۄھێ نشنےہن
نِ گسنےگرَن منز نٔنی نےرگن َ ےِمٔ نظمٔ ۄھێ نگٔھمٔنِ زنھگی نٔ معگشرنٔ کین کےنْژَن
گٔوی لین نٔ ورھگر مسلَن سَنٔ نٔکِ رٔوی ےِۄ زگن مگٔھٔ کَرگن َ سُ ۄھُ مگنٔ ذگٔری سطہس
مینَ ےِمن مسلَن سگم ہیوگن طَ نگسگن ۄھُ زِ سُ ۄھُ ئێےَن ہمعصرن شگٔےرن مںَلَن
رگٔہی ےگ فرگق سٔنھی مگٔنھی نظرےگٔنی سگٔےری نِشِ ننگ گیمُن نٔ گَمِ نِشِ لۆن
ہیوگن ی گَمِ ذێہنی ننھیلی ہنْھ نَنِ وگنٔ گظہگر ےگ گعنرگف کرنٔ  ورگٔے ۄھُ سُ
گٔزی کِس مگٔھی نٔ ۃۄھ غرض ھُنی ےِہس منز نظرےَن نٔ قھرن ۂنز ننگٔہی ہُنھ گَنھ کُن
رُوزِنھ نظگرٔ کَرگن ی ۄنگنۄِ نظےےہَن ہُنْھ نھٔےنھنیگر ی وۄمےزَن ۂنْز مگمگٔلی نٔ ۃگنَن
ۂنز ۄھَلِ ۄھگنگر علگؤ ہےۆن نگٔھِ مَن مننین نظمَن منز زِنھگی نٔ مگہولٔ کین نێےَن نووُہ
طلن مسلَن مینھ غور و فکر کَرٔنی ےِمن منز ےگون ی نور ی وننھ ہرھ ی کگٔشر ہسن ی</p>

<p></p>

<p>
               0 
</p></body></text></cesDoc>