<cesDoc id="kas-w-literature-essay-es40.kal" lang="kas">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kas-w-literature-essay-es40.kal.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>anhaar</h.title>
<h.author>morgub</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - journal</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">es40.kal</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page p.no.101.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-24</date></creation>
<langUsage>Kashmiri</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;anhaar&gt;&lt;vol.1/ no. 15 / dr. morgub banhalli/ essays&gt;  </p>

<p>
 									 (101)1
                             کأشرِ مَرثیِ
         
                  اَتھ کَتھِ چھُنٔ شید کأنْسِ انِکار زِ مرثِیِ چھے ظُلمَس خلاف
 احتجاجُک ناو بامے سُ ظلم کانہ تِ کٔری ین ذوالقدر خان تا تاری  (ذوالچِ )
 ێیلِ کشیرِ منْز لُوکن بلا امتیاز مزہب مِلت کترِ اشکھاش کران چُھ
 تَمِ وِزِ چِھ کأشرِی لُکھ تِہنْدِس طلُمَس خلاف ێیمِ آیِ احتجاج تٔ
 ِظہار افسوس کران۔ 
 بھگوانٔ وۄنی کرژٔ پَنٔ دیا       ذواوچو کریڑ سُنْد بیا کاس
    شرُاہمِ صٔدی منْز ییلِ یوسُف شاہ چک اکبر سٔنْز سأزشنَ 
وے آرام
                   لگے تَتھ  ماہ  جبینس  لول  ہو آم
   غرض واقعِ کربلا کِ موضوع علاؤہ چِھ مختلف وَقتنَ منْز وَننٔ آمٔتی
 بیے شماُر مرثیِ نمُونٔ اَتھِ آمٔتی یِم لۄکٹیو پیٹھٔ بڑ یویام اَلٔ پَلٔ سارِؤے </p>

<p> 								 (102)2
   شأعِرو چِھ تخلیق کٔری مٔتی بٔ تھاؤ پننِ مقالٔ ہُک سل تھر واقعِ کربلاہَس
 مُتعلِق لیکھنٔ آمژَن مرثِیَن متھٔے محدُود۔ 
     کشیرِ چھے پَتٔ وَتھ مختلف تہزیبَن ہِنْدِس اپرس تَل رُوز مٔژ
ناگ ، پشاچ ،دراوڑ تٔ آریا ہوَ علاؤہ چُھ ییتیُک تہزیب تٔ تمدُن
  کأتِہو تہزیبَو  پَتٔ سوو بنوومُت تٔ اَتھ مِلٔ مِشِ تہزیبَس چُھ اسِلامُک
 اَثر زیادَے پَہھَن لَبنٔ یوان۔ کشیرِ کرتٔ کِتھٔ پأٹھی ووت اسلام اَتھ
 مُتعلق چِھ مختلف نظریات پیش یِوان کرنٔ۔ عام پأٹھی چھے اَتھ مُتعلق
 سٔتِم یا أٹِم صدی ہجری میلٔ کٔنی ماننٔ یِوان۔ مگر سون ادب  1974ٔ
 کِس أکِس مضمُونَس منْذ چھے مختلف حوالَو سٔتی ثأبِت کرَنٔچ
 کُوشِش آمٔژ کرَنٔ زِ ہجِرتِ برْۄنہ چُھ اسلام کشیرِ ووتمُت تٔ
 لوپر ناگ چُھ جناب ساقی کوثر سٔنْزِ معجز نمایی ہِنْز نٔنی کرامات تٔ غانِم
 ہنْدٔ نأوی اَکھ وزیر دربار چُھ دیِن حقِ کِس تلاشَس منْز بلع گژھتھ
 اسلام قبُول کران۔ یِ چُھ 300 ہجری ہُنْد واقعِ۔ 
      بہر حال اَکھ کَتھ چھے پیقٔ زِ رنیچن سٔندِ بلبل صأبنِ دٔسی
 صدر الدین بننٔ پَتے مٔج اسلامَس کشیرِ منْز سرکأری سر پرستی
 تٔ للگ تیزی سان لُوکن منْز پھألنِ۔ یِ کَتھ تِ چھے متفِق زِ یِم تِ
 اسلأمی مُبلغ دشیرِ آے تِموَ کۆر پنِ نِس تبلیغس سٔتی سٔتی واقعِ ے کربلاہُک
 تِتزکرِہ تٔ أسِلامَس منْز جزبٔ ایپار تٔ خدایِ سٔنْزِ وَتِ منْز مستانٔ وار  </p>

<p> 								 (103)3
  پرْیتھ کینہ چھَپِ دِنُک فلسَفِ ہێکِ ہے نٔ واقعِ کربلاۂکِ تزکرٔہ وَرأے
 فِکرِ تارنٔ یِتھ ۔ میرسید علی ہمدأنی چِھ کشیرِ سَتَن گَتَن سیدن
 ہِنْدِس صُورتَس منْز اکھ پُوریہ تہزیب ہیتھ تشریف اَنان ۔ یِم</p>

<p> گامٔ گوم پھیرتھ تبلیغ دین  چِھ کران۔ تِم أسینِ تَمِ وِزِ ترو سَتتَو فِرقوَ
 منْز مُنِ مخصوص فِرقُک تبلیغ کران بلکِ اصِل اسِلامُک تٔ
 قرآن پاکُک پاضیح فرمان چُھ انمایرید الا لیزہب
 عمکم المرجس اہل البیت ویظہراکم تطہرأ تٔ
 یِہنْدِ پاک تٔ پاکیزگی ہِنْز ضمانت قرآن مجید دِیِ ،تِموَ وارٔے
 کُس ہیکِ صیح رہبر تٔ رہنما أسِتھ تی تہِنْز وَتِ  علأِہ کِسٔ
 وَتھ ہلکِ سێزٔ صاف تٔ شُوژ أسِتھ جناب امیر کبیر چِھ اَتھ مُتعلق
 اَکھ جامع کتاب مودتہ فی القربی تحریر کران یۄسٔ وُنِ تِ
 سِر سوبُت موجود چھے۔ غرض شأعِروَ کٔر یِمَن واقعٔ ہَن پنِ نین لفظنَ
 ہنْدی جامٔ لاگنٔچ کُوشِش۔ اَتھ سلِسلس منْز تِ ژھیپٔ ژھارِ ژینٔ ؤنی
 لٔبِتھ۔  
       حضرت شاہَس شیرِ یزدانَس      ێیمی ژۆٹ کھێیِ مہمانَس سٔتی
       کُور دُو رسول کدایس زاے        سۄے کُور  زینٔ آے عالمَس شُوب
     حضرت شہس سۄے باگٔنی آے         تس چِشِ زاے زٔے آبرُوب 
     ناوتس حضرت فاطمِ آے              ونْدہس زوُ جان تی چھم لُوب </p>

<p> 							 (104)4
  وقت قیامت نبٔ نٹِ کرواے     سِے کُورنَے آسِہے گژِھہے نبِ دُوب
 عشق چھُے کرتلِ سَردارُن       سُ دأرِتھ یِیِ  تٔ پَکھ 
 عشق چھُے غنیمس بُتھِ نیُرن   سُ پھیرتھ یِیِ تٔ پَکھ
 عشق چھُے رَتٔ جأبدن پأرن   سُ رخت گنْڑِتھ یِ تٔ پَکھ
         اَمی وَقتٔچ چھے اَکھ سوبُوت مَرپیِ یَتھ منْز حمد نعت
 مدح تٔ درد بیترِ سأری چیز چِھ لَبنٔ آمٔتی۔ اَمیُک تخلیف کار چُھ
 میر حسن یِم814ھ مطأبق 1393ٔ وفات کران چِھ۔ اگر أسی
 للِ ہُنْد زِ اسنٔ 1335ٔ صحیح مانَو تٔ شیخ العالمَن 1377ٔ تَمِ
 حِسابٔ چُھ شیخ العالم 1440ٔ منْز وأصِل سَپدان یعنے میر حسن
 چُھ یِمَن دۄن پایِ بڑین درمیانی وَقتَس منْز اوسمُت یا وَنوَ
 دِنؤنی ہُنْد ہمکال اوسمُت ۔ ونیُک مکِ تحقیقِ مُوجُب چُھ میر
 حسن کشیرِ منْز مرثیِ نگأری ہُنْد بأنی کار یِ چُھ کشیرِ منْز
 سُلطان سکنْدر بُت شکن سُنْد وق آسان۔</p>

<p>     دِؤ دین ہار چھُکھ میون ژٔ گیہ بار چھکھ
              کیولی شُوبان ژٔے چھکھ بس تے
     اۄژھ پون کیا کرِ تعریف   چانین پاتھن کروا تِ لطٔف
              سارِنی  پاپن نوارتس تے
    شُنی کار گژھِ ێیلِ یَتھ سَمسارس   کند لگِ منزرِ تِ بند ہاکارس
    ژیتھ رَچھ کونہ پێیِ سورُے ژیتَس تے </p>

<p> 								 (105)5
 کرر حٔمژ صٔدرس دامن گیری         دِیِ پُژی ێیمی کٔر زگتس پھیتی
               ہوا دارسُے کَۆر تھَن عالمس تے
 پانْژنَ وَقتَن ێیمی کٔرأہی           دِومیانِ اَتھ ماسِؤ زاہ آیی
               یؤَ بَنِ ہیم کینہ تَتھ بازرَس تے
 تِہنْزٔے ماے چھم بٔڑ              نَزِاَنْدر ێیمی لۆب بۆڈ پاے
               بۆرُن نٔ مأل زاہ سۄکھ سادس تے
 تَس رۆس عالم ہُنی واس اوسُے       امرت شۄ دِمَنٔ یۄسِ سند اس اوسُے
               اَڈٔہ ڈێیمی کۆر نَبٔ ژٔنْدرس تے
 آرتھ گژھِ ہے جن جاتَس          شاہ نے بینز رہے زوپا کزاتَس
              تی تھێوک ذاتن ملکو تَس تے
 کأر نێ مِرتھ رُود پیگِس ڈیشتِھ     ژأر ێیلِ بۄند ون کٔر شُمی ڈیشتِھ
              لألر گأمٔژ زیو تالَس تے
                       ێیٔژے    درنو مارس یِ آسیم لێژٔے
              . . . . . . .  . . . . . . . . .
 شامٔ کِس سَفرَس یِ سا پاتھیم     أژھی بالک ہیتھ کِتھ پأٹھی وایُوم
                  وۄپر دیشن پردُشس تے
 میندس دارو آسٔن گۆژُھم         غمٔ سٔتی ژٔنْدرٔکی پأٹھیھ یِتِ مۄژم
                 ڈیۆل گنڑ کرتَس رۄپٔ دٔستس تے </p>

<p> 								 (106)6
        ھَمِ نظمِ ہِنْد لِسأنی تجزِیِ تَلٔ چُھ صاف طأہرِ زِ اَمِچ زبان چھے
 للِ تٔ شیخ العالمنِس طرزَس لَرِ لۆر پَکان۔ واریاہ تِتھی لفظ چِھ 
 اَتھ نظمِ منْز یِم از متروک چِھ تٔ یِمَن ہُنْد معنِ تٔ مطلب تِ کمٔنے
 لُوکن پَے پَتٔ آسِ۔ وۄں گو تشبیہ تٔ اسِتعارَن علاؤ علامتی 
 ورتاوُک ژٔنْدٔر چُھ سنسکرت آمیز زبأنی ہِنْدِ کریہنِ اۆبرٔ مٔنْزی کلٔ
 کٔڑِتھ میر حسن سٔنزِ وۄستأدُی لِ ہِنْدِ عیز ہُنْد نیب دِوان۔ 
        ژۄ دأہمِ صٔدی ہنْدِس میر حسنَس پیٹھٔ اردأمہِ صٔدی
 ہِنْدِس خۄجِ حسٔن میرستام چھے کأشرِ مرثیِ واریاہَو مَرحَلوَ تٔ مُصیبتوَ
 مٔنْزی گزُران یِم اَکھ أکِس نِش بێون بێون تٔ بدُون أسِتھ تِ الگ
 ہێکو کھنٔ کٔرِتھ تِ کیازِ پَزرَ چُھ تأریخِ چِ وُنلِ تَل تِتھٔ پأٹھی ہینِ آمُت
 زِ وَتھ کڑٔنی چھے مشُکل بٔلکِ ناممکن تِ بَنان ۔ پنْدأہمِ صٔدی منْز
 کأشرِ ادبُک مطالعٔ کرنٔ تَلٔ چُھ نَنان زِ صرف چِھ اۆنگجِ ٹۆنْٹین
 پیٹھ گنْزرٔنی سُنْب شأعِر اَتھ آمٔتی مگر مرثیِ نگارَن ہُنْد تیداد چُھ
 یِتھین حالاتَن منْز تِ درجَن وادتٔ یُس کلام وُنُیکتام اَمِ وقتُک
 اَتھ آمُت چُھ سُ چُھ أتھی وَقتَس دوران بافی سارِنٔے شعر ذأژَن
 منْز اَتھ آمتِ کلامٔ کھۄتٔ واریاہ ژۆر۔ وۄں کواز تام یِموَ أدیبوَ
 کأشرِاَدبٔچِ تأریخٔ لیچھِ تِمَن کیا زِ آونٔ یِ اَتھِ یِ آسِ سانِ کھۄتٔ
 شاید زیادٔہ پَہَن تِمنٔے پَے یاتٔ چھَنٔ تِموَ مرثیِ کانہ شعر زاتھ
 مأنی مٔژٔے ۔ نَتٔ کیاہ وجِ چُھ زِ میعیار تٔ گۆتٔ جارکِ لێحاظ تھیکُن لایق</p>

<p> 								 (107)7
   مَواد آسنٔ باوجُود چھُنٔ تِموَ اَمیُک تزکِرٔہ کۆرمُت۔ حسن کھۄیِ ہأڈلیَس
 پیٹھٔ أزی کِس شفیح شوقَس تام چُھ سارِؤے تأریخ لیکھن والیو
 کأشرِ مرٔیِ سٔتی بین ورتوومُت اعظم دیدٔ مری چُھ پنٔ نِس
 وۄستاد عبدالحکیم ساطع سُنْدسِن وفات تِ لیکھان تٔ یِہنْدِ
 تَھدِ پایِ فارسی شأعِر آسنُک تِ تزکرِٔہ کران مگر بحیثت کأشرُ
 سأعِر یا مرثیِ نِگار چھُنٔ سُ تَس مُتعلق اَکھ حرف تِ لیکھان۔ مرحوم
 آزاد پھیُور یِ دٔپی زِ تِ گھَرٔلالٹین ہیتھ کأشرِ ادبُک
 پےَ کڑنِ مگر کأشرِ مرثیِ ہُنْد گوس نٔ رژھ تِ احساس۔ یِ وَنُن
 چھُنٔ صیحزِ تَس کیا آسِنٔ کأنْسِ اَمیُک پَے پَتٔ دێتمُت تِ کیازِ
 رانگر چُھ ژوڈرِس پیٹھٔ کنِ تٔ آزاد مرحُومَس أس ژوڈٔری منْزی
 بَہَن پَہرَن وَتھ تٔ سِکھ دورٔ پیٹھٔ اَز تام چھے برابر کم سے کم ؤری یَس
 منْز اَکِ لَٹِ ژوڈرِ مجلِس آسان، ێیتِ کأشرِ مرثیِ پرَنٔ چھے یوان۔ 
      رہبر صأبَن چھے کأشرین مرثِیَن ہِنْز کم کاسٔ ذِکر کٔرمٔژ
 مگر نمُونٔ رٔنگی چھَکھ سۄے نیے ناو مَرثی دِژمٔژ، یُۄسٔ حدٔحنی
 صأبَن کأشرِ شأعری منْز اۆش تٔ آب  کِتابِ منْز دَرٔج چھے کٔرمٔژ
 آسِ ہے تِموَ آسِ ہے ضرُور تِمَن طظیم مرثیِ نِگارَن ہُنْد پَے پَتِ دیتمُت
 یِموَ پننِ رَتٔ ژأنْگی زألِتھ کأشرُ ادب گأشروومُت چُھ 
          حأرأنی ہِنْز کتھ چھے یِ زِ کشمیر یونیورسٹیہِنْدی    </p>

<p>
 								 (108)8
 کأشرِی شعبَن یُس کأشرِ ادبُک توأرِیخ 1978ٔ منْز چھپوُمُت
 چُھ سُ چُھ اَتھ معاملَس منْز بِلکُل خاموش ۔ اَتھ مُتعلق چُھ دعوی
 کرنٔ یِوان زِ مختصر آسنٔ باوجود چُھ یِ کأشرِ ادبُک مُمکل توأریخ۔
 لل واکھَو پیٹھٔ أزی چِ معری نظمِ تام چُھ اَتھ منْز سارِنے شعٔر
 ذأژن مُتعلق کَتھ آمٔژ کرنٔ مگر کأشرِمرثیِ سٔتی چُھ آمُت اَتِ تِ سُے
 وورٔ ماجِ سلُوک ( میأنی کِنی زأنِتھ مأنِتھ ) کرنٔ یێلِ زن اَمِ برۄنہ
 کأشرِ مرثیِ مُتعلق واریاہ کِتابٔ بازرَس تٔ یونیورسٹی ہِنْزِ لایبریری 
 منْز موجُود آسنٔ یِمَن منْز حکیم صفدر ہمدأنی شُنْد  اۆش تٔ آب  
 مُنشی صادق سٔنْز کأشرِ مرثیِ کأشرُ ادب تٔ مرثیِ نگأری ( 1974) تٔ
 سون ادب تٔ شیرازٔ کین شماُرَن منْز واریاہ مفامین۔ 
         اَمِ کھۄتٔ گأرأنی ہِنْز کَتھ چھے یِ زِ اَتھ تأریخس منْز
 چھے صفعِ 144 ہَس پیٹھ یِ بے بنُیاد کَتھ تِ لیکھِتھ زِ یِمَن ( تنہا
 انصأری صأبَس ) برۄنْہ أس کأشرِ مرثی کُنِ فارمِ رۆستُے
 لیکھنٔ یِوان تٔ اَتھ منْز أسی شہدأ کربلاہَن ہِنْدی صرف ویداکھ
 آسان اَمِ تَلٔ ہێکِ صاف پأٹھی پَے لٔگِتھ زِ یِ کِتاب کأژاہ
 دیانت دأری سان چھے تیار آمٔژ کرنٔ یێلِ زنَ مرثیِ باضأبِط پَنُن
 اَکھ مخصُوص فارم پَتٔ وَتھ چُھ اوسمُت تٔ تنہا صأبن چھے مَرثی
 کأشرِ مرثی ہُنْد پنُن فارم تْرأوِتھ فارسی قألِبَن منْز لیچھِ مٔژ تٔ</p>

<p> 						 (109)9
    کأشرِ اَدبٔ کِ گۄڈٔہ کالٔ پیٹھ اَز تام چُھ برابر اَتھ منْز شہدأ کربلاہَن
 ہِنْد تزکرٔ علاؤہ حمدِ خدا، نعت رسول مدح امِ ،سصُول دین تٔ
 فروغ دین علاؤ دُنی یِہ کِس لۄہ لنگرس سٔتی وابستٔ پرْیتھ چیز
 چِھ میلان ۔ اَمِ علاؤہ یُس اَمیُک غألِب پہلو چُھ سُ چُھ بہودُری
 صبٔر، ایثار، ظألمن سٔتی نفرت تٔ مظلُومَن سٔتی ہمدردی ہُنْد
 جَزبٔ نظرِ گژھان۔ 
      پندأہِ صٔدی عیسوی منْز اوس کشیرِ منْز اوراد فتحیِ بالجہر
 تٔ یِکوَٹِ پَرُن شرُوع گومُت تٔ مرثیِ نگاری رَوتِ چُھ ارپیِ بَڑِ ہَٹِ تٔ
 یِکوَٹٔ پرنُک طرز پانٔ نوومُت۔ اَمِ دورٔکی یِم کم کاسٔ مَرثی نمُونِ اَسِ
 میلان چِھ تِم چِھ فارم تٔ لِسأنی اعتبارٔ میر حسن سٔنْز مرثی نِش
 بێون۔ میر حسن سٔنْز مرثی چھے تُرْو مصرعی یَتھ منْز گۄڈنِکین دۄن
 مِصرعَن بحر تِ کُنی تٔ قأفیِ تِ کُنی چِھ، یێلِ زَن ترْیمیِس مِصرعو پَتٔ یِم ترْیِم
 مِصرٔ چِھ تِمَن چھے ہێر بۄن بحرتِ کُنی تِ چھے مگر قأفیِ سِکیم تِ۔ گۄڈنِکین
 دۄن دۄن مِصرعَن اَلگ۔ اَتھ منْز چِھ سنسکرت لفٔظ
 تٔ فارسی لفظ چِھ نِ آسنَس برابر ، یێلِ زَن پندأہمِ صدی ہِنْزن
 پرثِیَن منْز سنسکرت لفٔظ نِ آسنَس برابر چِھ تٔ فارسی کَفظنَ ہُنْد
 گۆتٔ جار چُھ جایِ جایِ نظرِ گژھان۔ مرثیِ چھے چھنْد منْزٔلی بأتھ، </p>

<p> 							 (110)10
 وان وابنْد تٔ روانٔ فارمَن منز وننٔ علاؤہ فارسی پیٹھٔ آمتین قألِبَن
 منْز وَنٔنپِ ابتدأیی کُوششِ تِ کرنٔ آمٔژہ منشی محمد صادق چِھ
 کأشرِ مرثیِ کِتابِ ہِنْدِس صَفہ2 ہَس پیٹھ لیکھان زِ  اَمِ دورٔچِ
 مرثیِ چھے نہایت سلیس تٔ مختصر زبأنی منْز وَننٔ آمٔژ۔
      اَتھ دوران یِمَو شأعِرو اَتھ شعٔر زأژ آبیأری کٔرمٔژ چھے تِمَن
 منْز چِھ را، حاجی حیدر (2) محمد مشتاق ژوڈر (3)سید حسین اسکندر
 پورہ (4) سید حسن رہنازڈی بل (5) میر سید علی زڈی بل (6)
 محمد ملک ژوڈر (7) مُلا نامی (8) بابا طالب اصفہانی شوپیان 
 (9)مُلا نامی (10) میر جزأ الا (11)تقی میر رسول سوپور (12) سید 
 حسین ( 15) میر سید محمد میر گنڈ (16) مُلا طالب (17) علی محمد 
 ماہر (18) سید حسن چاک بَٹِ پورہ (19) مُلا محمد امین متغنی (20) میرزا
 علی طان (21)مُلا غالب۔
            مُلا غالِب أسی مشہُور عالِم دین مولانا سعید الدین سٔنْدی
 زرُی۔ مولانا سعید الدین أسی کشیرِ دین حَقُک تبلیغ کرنٔ باپَتھ سوزنٔ
 آمٔتی۔ تِم آے 1393ٔ منْز میر سید محمد ہمدانی فرزبد امیر کبیر سید
 علی ہمدانیَن سٔتی کشیرِ میر صوف أسی تِمَن بَڑٔ قدر کران تٔ تِموَ
 کٔری تِم خانقاہ معلی ۂکی گۄڈنِکی مُتولی مُقرر تٔ میر سید محمد ہمدانی
 صأبَن چُھ تِمَن پَننِس وقف  نامَس منْز  برادر ارشدو امجد  ہِی
 
 							 (111)11
 لقب لیکھی مٔتی (تاریخ حسن حِصِ اول  ٔ333، ٔ334 وتاریخ
 شیعاں ٔ 130 ) یِ کَتھ چھے پِرٔنی لایق زِ کأشرِ زبأنی منْز چھے مرثی مولانا
 سعید الدین سٔنْدی خاندانَن معراج کمالَس واتنأومٔژ۔ 
     قاضی احمد علی مرحوم یِم گۄنڈِ خواجِ قأسِم أسی روزان چُھ
 مرثیِ نگأری اَکھ نۆو طرح دِوان تِم چِھ پرْأنِس تٔ مٔنْزی مِس
 دورَس درمیان اَکِ کدٔلٔچ کأم کران۔ ( کأشرِ مرثیِ صَفٔ 8 مُنشی صادق)
 مُومنوز بُدۂ قدرت چِھ حذرت تٔ شاہ ولایت۔
                             دۄنوے چِھ بادشاہ عالِم نور
              تِمے چِھ ساکِن طریق وحدت ہادی خلفت
                             اُورِ  تجلی دنیاہَس  کۆرُکھ ظہُور
              حضرت چُھ نبین سرُور       شاہ چُھ اِمامٔژ مصدر
                         تس ِنش درْاے بێیِ  نور کاہ
             . . . . . . . . . . . . . . . . . </p>

<p> اَکھ زبان قدرت بیاکھ نیان معرفت
                 تِموَ اَکِ  وَتِ آسُن زونکُھ ضۆرُور
        تِم دریاے رحمتٔکُی اچھر    زأنی توکھ زٔ اچھر برابر
                   یَؤہ کِن دۄن چشمَن چُھ ہِوُے نِگاہ
 تَنِ چِھ ہیتھ تِم مُصیبت یۆ تام یِیِ ہو قیامت
                        حضرت اسرافیل وایِ صُور </p>

<p> 							 (112)12
 پیوانتِم وُچھتو یِ خلقت
             مرتفی علی ہیوہ مارُن تھووک منظور
         ژلٹُکھ عٔلی مُک جِگر    دین کۆرُکھ اَبتر
                یُتھ ظُلم پَتٔ وَتھ سٔری زینِ زاہ
    اَمِ پَتٔ چُھ کأشرِ مرثیِ ہُنْد سُ سِر سأوی دور شروع
 گژھان یُس اَلٔ پَلٔ سِکھ دورس تام چلان چُھ۔ اَتھ دِران چھے
 کأسرِ نرثیِ واریاہ مٔنْزِل ژھنْڑِتھ معراج کمالَس واتان ۔ شہمیری 
 تٔ ژک دورس دوران چھے یِ شٔٔر ذاتھ پنِ نین پکھَن جان
 واشکڑان۔ واریاہ طویل مرثیِ چھے ٹخلیق یِوان کرَنٔ یِمَن منْز حمد،
 معت، مدح، معجٔز تٔ درد بیترِ سأری لوأزمات نظرِ چِھ گژھان۔
 وُچھوَے تٔ کأسرِ مَرثیِ بحیثیت الگ صِنف سخن چھے أتھی دورس 
 منْز پنُن انفرأدی وجود قأیِم کران۔ یدوَے وۄپر حُکمرانوَ کأشرین
 ہُنْد پنُن ادبی میراث صأع کرنٔ باپَتھ بے شُمار ژۄیِ تِ کِرَ تٔ باقی
 چیزو علاؤہ آو خاص پأٹھی کأشرین ہُنْد مرثیِ ذخیرہ تٔ ژکن ہِنْدی
 سامان جنگ بیترِ تِ نابُود کرنٔ تَمِ باوجود تِ چِھ اَسِ اَمِ دوریکی بے شمار
 مرثیِ نمُوننٔ میلان ۔ یِ کَتھ وَننَس منْز چھُنٔ کانہ حرَج زِ پَتھ کالِ 
 اوسنٔ کشیرِ منْز فرقِ وارانِ انِتِشار تٔ نِ أسی مسلمان مسلک کِس
 ناوَس پیٹھ اَکھ أکِس نِش الگ تٔ نِ أسٔکھ رَتَس تٔ تریشِ ہَر۔
 فرقِ وارانِ انتشار آو کأشرین منْز پننٔی مقصد پُورٔ کرنٔ باپَتھ </p>

<p> 							 (113)13
  کینْژَن ایجنِسین ہِنْدُ دٔسی زِأنِتھ مأنِتھ شروع کرناونِٔ ێیمِکی شِکار
 نِ صرف ییتِکی عام لُکھ گٔے بیلی کِ کینہ تھَےِ پایِکی گۄنٔ مات تِ۔
 اَمِ تھزَرٔ گو کھا اکبرس گھرنِیاے ہیتھ فرٔی یاد۔
         ہۄنٔ ماتٔ غنٔی یِنی پأٹھی پانَس رأچھی کٔری زِ ہے
 اَمِیُک دراویِ یِ نتیجِ زِ بقول پروفیسر حاجنی ( کأشر شأعری 
  کأشرین رأونٔ صرف حکومتھٔے یأژ بٔلکِ مۄغلو کۆر تِجِنْز زبأنی
 تٔ ادبَس سٔتی سُے سلُوک یُس اَکھ وۄپَر فاتح پننین رعیتَن
 سٔتی کران چُھ ...... اکثر شأعِرَن ہُنْد کلامٔ گو ییٔر۔ نار مار تٔ لُوٹ
 کرنٔکی آے کٔتی حرَبِ وَرتاونٔ تِ سکھ دورس تام آو شیَٔن کم از کم
 دَہِ لَٹِ لُوٹ کرنٔ بٔلکِ أسی لُوٹرَن حأج خطاب یِوان دِنِ۔
    ہلٔ کر حاجو پُلسٔ چھُے دُور       گۄڈنیتھ زاِون کمانگر پور 
 ساسٔ وادلُکھ آے مارنٔ تٔ زٔڈی بَلَس آو اَکِ وِزِ محض اَمِ مُوجُب
 حَملٔ کرنٔ زِ اَتِ آسٔ مرثیِ پرَنٔ یِوان۔ اَمِیُک تزکرِہ چُھ مورِخ حسَن
 پننِ تأریخ کِس پیٹھٔ 496 تام   تاراج شیعان  نأوی
 بابَس منْز کران۔ اَمِ ظلم وتشدُد تٔ کترِ بَتٔ پَتٔ تِ چُھ اَسِ واریاہَن
 مرثی نِگارَن ہُنْد نیب میلان۔ یِمَن مرثی نِگارَن منْز چِھ  (1) میر محمد 
 جعفر شوپیاں (2) مُلا احمد مہری (3) حسن شاہ ژک (4) علی شاہ
 ژک (5) یوسف شاہ (6) میر سید حسن چھژبل (7) محمد حیات (8) بابا محمد 
 جواد سرینگر (9)خواجِ اکبر (10) سید عبدالا شاہ (11) خواجِ عباس</p>

<p> 				                 (114)14 
 (12) میر احمد علی (13) سید صالح رضوی (14) سید شرف الدین احمد پورہ  
 ماگام (15) قاضی احمد دین حنفی ورُمل (16) خواجِ حسن بٹ (17)
 خواجِ محمد (18) خواجِ محمد فاذل (19) میر محمد عبدس ( راجپور) (20) مرزا
 جزأ الھ (21) نُنْدٔ ڈار (22) مُلا عبد الراحمان والسیو ویوسر کولگام
 (23) عبد الحکیم ساطح۔ 
            اَمِ دورٔ کین قد آور مرثیِ نگارَن منْز چُھ مُلا احمد میری سُنْد
 ناو خاص پأٹھی قأبل ذِکر۔ یِم چِھ کشیرِ ہُنْدِس أخری خۄد مختار
 بادشاہ یُوسف شاہ ژک سٔنْدی وۄستاد أسی مٔتی۔ علی شاہ ژکن اوس
 أمِس ملک الشعرأ خطاب دُتمیُت۔ یِموَ چھے فارسی یَس منْز تِ 
 شأعری کٔرمٔژ تٔ کأشِرس منْز تِ۔ یِموَ چھے واریاہ پرثیِ لیچھِ مَٔژ یِمَن
 منْز ظُلم کربلا علاؤ تِ واریاہَن چیزن ہُنْد تزکِرٔہ چُھ۔ یِہنْز زبان چھے 
 عألمانٔ تٔ یِموَ چھے تشبیۂ ، استعارٔہ تٔ علامٔژ بے وایِ ورتاو مَژٔ۔
 یہنْدِس کلامَس منْز چِھ فصاحت تٔ بلا غتٔکی ناگراد وُزان۔
 سُے مُفتی قدرت منشی تقدیر قأسِم روزی کۆر خألقن
                  سُے مُرشد جبریل ملکن ہُنْد پیر مُہمن ہُنْد رفیق پیغمبرن
 بوہادی معرفت تمی ہاؤنی دیِ وَتھ عصاے موسی سُے یدبِیضا
                  سُے سیف الا وجیہ الا شاہ مردانے
 وحی رسول الا ولی الا تر موضنِن ہُنْد پادشاہ
                  اَمی دورُک بیاکھ نامور مرثیِ نگار چُھ مرزِ جزأ الا مرحوم ۔ یِ </p>

<p>
 								 (115)15
 چُھ مشہُور شأعِر اوسمُت تعمی چُھ اوراد فتحیِ ہشِ کِتابِ کأشرُ ترجمِ کۆرمُت
 اَمِ کِ ترجمُک تمہید چُھ ێیمِ آیِ شرُوع گژھان۔ 
 الہی وچھُکھ ارحم الراحمن       کۆرُتھ پأدٔ ژیے آسمان وزمین
 تویی وحدہُ لا شریک لہُ        ستارا لیعوب غفوراُُ غفور
 چُھ چونے عرش لوح قلم دَۄہ تٔ راتھ    چھے با مصلحت پرْیتھ گراہ ژیُوہ تٔ ساتھ
 محمد چُھ پیٹھ بر حق رسول خدا    سُ ہادی تٔ رہبرِ نور ھِدُا
 تِمَن پیٹھ ہزاراں درود وسلام        ہمِ آل واصحاب ویاراں تمام
         یِہنْز اَکِ مرثیِ ہُنْد اَکھ بنْد چُھ۔ 
 مۄکھ کیا دِکھ صاحِبَس سۄکھ گأری گأری ہو
                   چھُکھ وَتٔ لأج گومُت مفسٔنِ  دَگِ
 رہ تَمِ کَس کیاہ چارٔ گزأری ہو
                   ێیلِ ساہ حسین نِس قأتلَس ژھانْڑو لگِ
 ژٔ ودو امام حسینَس 
              یِ ہُنْدُے وَدُن چھُے آبرو سٔتی
    میرزا; جزأ الا سٔنْز اَکھ پھیرٔہ ہأوخصُوصیت چھے یِ زِ یِموَ چِھ
 نٔوی قأفیِ ژأری مٔتی۔ مرثیِ چھَکھ دو مصرعی لیچھٔ مٔژ تٔ پرْیتھ شعرُک
 گۄڈنِیُک نِصرٔ چُھ اَنْدٔ وَنْد أکی سٔے قأفیَس منْز تٔ یتھَے پأٹھی
 چُھ پرْیتھ شعرُک دۆیم مِصٔرٔ تِ اَنْد وَنْد أکی سٔے قأفیَس منْز
 یِم ادیب یِ اعتِراض چِھ کران زِ کأشرِ مرثیِ أسی پَتھ کالِ کُنِ فارمِ </p>

<p> 							 (116)16
 ورأیی آسان تٔ اَتھ منْز چھُنٔ ویدا کھَو ورأے کِہینی ۔تِمَن پَزِ جزأ الا
 سنْد کلامٔ پَرُن تٔ تھمِیُک تجزِیِ کرُن۔
 آولٔنی دُنیاہکٔی وۆلمُت چھُکھ گۄنہگأری ہو۔
            کونہ زانِ واولَرِ ناو چأنی بۆٹھ کِتھ لَگِ
 اَمِ دورٔکین قأبِل ذِکر مرثیِ نگارَن منْز چُھ یُوسُف شاہ ژک سُنْد
 ناو ہینٔ یِوان ۔ افسُوس چُھ زِ یِہُنْد فارسی تٔ کأشُر کلام چھُنٔ
 وَقتٔ کیو گأیِلو نِش بٔچِتھ ہێوکمُت۔ دَپان اَکِ پھرِ اوس یِہُنْد
 وۄستاد مہری اکھ مرثی لیکھان۔ حمد، نعت تٔ مدح لتکھتھ گوپٔتی مین
 دۄن مِصرعَن ہِنْدی ردیف تٔ قأفیِ باپَتھ سخ بنْد یِم تِموَ شے مِصرٔ
 لیکھی مٔتی أسی چِھ یِتھٔ پأٹھی۔
 پیٹھ فلکن ولتھ پاپیلا      ملک بُتھی کِنی پٔسی پیے سأری
 بۄے وَنٔ کِتھ نعپذ بالہ      زبان   دزَیم     نأری
 شاہ مردان سجدس پیٹھ یام رُود   ترأو مُلجٔمی کرنل زہر آلود
    یِم أسی سوچِ سٔدرَس منْز ہینٔ آمٔتی مگر تارٔچ أسٔکھنٔ
 کانہ تِ صُورت نطرِ گژھان۔ تام آو یُوسُف شاہ ژک تٔ پرْژُھکھ
 پریشأنی ہُنْد وجِ تٔ کٔری ہَکھ پٔتم زٔ مصِرٔ یِتھٔ کٔنی پُورٔ۔۔ 
 کرْہنیا یِ حجرالا سود لَرزٔ تَس ژاو
                      للگ بُتھِ بُتھِ دِواں دأر کڑنِ ابراہیم فلیلو</p>

<p>  							 (117)17
اَمِ دورٔچ مشہور ترین شأعِر عرحبِ خوتونِ مُتعلق چُھ اوتار
 کرشن رہبر کأشرِ ادبٔچ تأریخ کِتابِ منْز لیکھان ۔  حقیقت چھے
 انِسانٔ سٔنز زنْدگی أس پأنی پانٔ أکِس چھۄ کِ لَدنٔ شجید گأمِتس
 انِسانٔ سٔنْززنِدگی ۔ تٔمی یِم وَژن لێکھی تِم چھِنٔ کِہینی بٔلکِ تِم مَرثی
 یِم زنَ تٔمی پأنی پانَس پیٹھ ؤنی تٔمی سٔنْزن مرثیَن منْز چِھ اکِس
 زخمی انِسان سٔنْدی آمٔ تاو تٔ ویداکھ موجُود یِمَن ویداکھَن
 ہُنْد مول چُھ سُ غم یُس تٔمی سٔنْدِس دِل جِگرس پرْیتھ وِزِ ژٹان
 تٔ کُوران اوس</p>

<p>  اَمِ دورکس مَرثی ہس پیٹھ چُھ عبدالکیم ساطح سُنْد
 ناو پھل ژنْدرٔکی پأٹھی تابان ۔ یِم أسی مشہور مرثی نِگار مُلا غالِب 
 سٔنْدی فرزند ۔ ساطح سُنْد فارسی کلام اوس نِ صرف کشیرِ
 منْز بلکِ مغل بارَس تٔ ایرانَس منْز تِ سیٹھا مشہُور۔ یِہنْزٔ
 اَکھ مشہُور فارسی ربأعی چھے۔
 مُفتم زِ جام عشق متی دادند      کِ ایں نیستم بِ نقدہستی دادند
 سرمایِ بر آں چِ بود دادم از دست    ارزاں نِ متاع تنگدستی دادند
         یِم أسی کشیرِ ہُنْدِس مشہُور مورِخ خِاجِ اعظم دیدٔ مری
 سٔنْدی وۄستاد۔ یِہنْدِس مَرنَس پیٹھ چُھ دیدٔ مری  صأب قطعِ وفات
 یِتھ کٔنی لیکھان۔
 گفت اعظم بِ سال تاریخش       نور ایمان بمر قدش ساطح </p>

<p> 							 (118)18
 نمونِ کلام۔
   تێلِ رَتھ وۆتھ دَگِ منْمبرس منْز خدایِ سٔنْدِس گرس بایو
   ێیلِ چھۄکھ لۆگ شاہ حیدرس دیِ سٔنْدِس سِرس بایو</p>

<p> خالقٔ سر سبز کرتٔ میانِ دِلٔ کُے چمن
          یُتھ پھۄلٔ تِم رنگٔ رنگٔدۄکھٔکی یا سمن
 گژھِ معرکِ معطر یِیِ عالمس خۄشبو
               کڑکھ جِگر کِ کوزٔ نِش دستٔ کَرِ تِمَن
     سُے معرکس منْز پشَیش ہو تِمَن
                 کرے قبول عابدین شاہ یِیم آبرو
 نُنْدٔ ڈار چُھ اَمُ دورُک اَکھ اہم مرثی نِگار گزُر یومُت۔
 أمی سُنْد تزکِرٔہ چُھ پروفیسر حأجنی صأبَن تِ کۆرمُت۔ رہبر صأبَن 
 آزاد صأبَن تٔ ناجی منور تٔ شفیح شوقَن تِ ، نمُونِ چھِکھ أمی سٔنْزِ
 مرثی ہِنْدِی دِتی مٔتی ۔أمی چھے واریاہ مرثیِ لیچھِ مَژٔ تٔ بقول انیس 
 کأظمی چُھ أمی سُنْد مرٔی دیوان ساسٔ واد شعرن پیٹھ مشتمل ۔ أمی
 سٔنْز اَکھ مَرثی چھے سَتَن بنْدَن پیٹھ مشتمل ..............  
.................. . . . . . . . یَتھ منْز حمِد خدا، معت رسُول
 مدِح أمِ، معجزات تٔ واقعے کربلا چُھ۔ پریتھ بنْدَس چِھ پنْداہ
 پنْداہ مصِرعِ۔ طاق مِصرعن چُھ الگ قأفیِ تٔ جُفت مصرعنالگ </p>

<p> 								 (119)19
 قأفیِ۔ پنْداہیْم مِصرعِ تِ چُھ جُفت مِصرعن ہِنْدی سٔے قأفیَس
 منْز تٔ پرْیتھ ژۄہوَ مصرعَو پَتٔ چُھ repeatگژھان تٔ 
 نفیس مفظٔ ژأر تٔ علامتی وَرتار چُھ اَمِ مرثیِ ہِنْز خصوصیت
 کینہ مِصرعِ چِھ۔ 
 حضرت فاطمٔنی تِم ٹأٹھی فرزنْد حضڑت اِمام تے
                   نٔکھی أسی رٔچھی مٔتی حضرت علی یَن أچھن ہِنْدِی گاشدار
 یَنےَ تِموَلٔب شہادَت تَنَے رُودُکھ نام تے
                    شفاعتَس تِہنْدِس سورُے دُنیا چُھ اُمیدوار
 یَنَے تِم گٔے عالِم بقاتنے رُود ماتم تے 
        یِم تٔ زلٔ ڈوۂکی چھی دپان برْما چُھ سمسار
 اردأہمِ صٔدی ہِنْدین مرثیِ نِگارَن منْز چُھ سید محمد ہادی
 فاخر اَکھ اہم ترینمرثی نِگار گزُریومُت تِموَ چُھ 1194ھ وفات  
 کۆرمُت۔ یِموَ چھے مرثِیوَ علاؤج باقی صِنفَن منْز تِ طبع آزمأیی
 کٔرمٔژ۔ کینْژَن ہُنْد خیال چُھ مشہُور بأتھ  یارٔہ لوگُتھ سنگ دِل
 چانِ جانُک چھُم قسم  چھُنٔ ژَکِ رسُل میُرن بٔلکِ فاخرسُنْد۔ 
                       ( کأشرِ ادبُک توأریخ صفٔحِ 49)
    فاخرن چھے مرثیِ باقی فارمَو علاؤہ زبأنی ہِنْزِ أکِس
 پرانِ شعرٔ ذأژ یَتھ چِھ وَنان أسی مٔتی ، منْز تِ مرثیِ لیچھِ مَٔژ
 یِنٔز اَکھ مشہُور مرثیِ یۄسٔ  پد  فارمَس منْز چھے ، چھے ہَتٔ واد </p>

<p> 							 (120)20
 ؤری گزرنٔ باوجُود وُنِ تِ بَڑٔ مشہُور بٔلکِ چُھ اَمِیُک نعتیِ حصِ
 ازَکل تِ واریاہَن جایَن صُبحأے نیمازِ پَتٔ تبُرکَن پرَنٔ یِوان۔ 
      اے مہ سروِقدِ مَن         قد مَن پأری لگے یو
     نگہے سوۓ من فگن         وُچھنَس پأری لگے یو
 منِ منْز یارٔ دَما بیہ        رَنِ مَژٔ خاصٔ نعمٔژ کھیِ
 مور ہَکھ تَشنِ دَہَن          مَرنَس پأری لگے یو 
     أمی سُنْد فارسی دیوان چُھ ژۄن ساسَن شعرََن پیٹھ مشتمل۔
 اَمِ دورٔکین مرٔی نِگارَن منْز چُھ مُلا طاہر غنی سُنْد ناو تِ ہینٔ یِوان۔            
 یِمٔ بے شمُار مرثیِ بے ناو چھے تِموَ چُھ واریاہ
 کلام غنی کشمیری یَس کُن تِ منسوب یِوان کرنٔ۔ اَتھ منْز کأژاہ
 حقیقت چھے تَتھ مُتعلق ہیکوَ نٔ وثُوقِ سان کِہینی ؤنِتھ۔ 
  1977ٔ منْز چھپیو وتعمیرس منْز ڈاکٹر غلام رسول جان سُنْد اَکھ
 مضمون غنی کشمیری یَس مُتعلق تٔ یِمٔ نٔے دۄہَن کرے یِ
 دُور دتشن سرینگرن تِ کأمِل صأبٔنی لیچھمٔژ اکھ فلم پیش ێمِ
 مُطأبِق غنی سُنْد تعلُق مولانا سعید الدین سید نِس خاندانَس </p>

<p> 								 (121)21
 سٔتی ہاونٔ اوس آمُت تٔ أمی خاندانَن چِھ کٔتی بہلِ پایِ مرثیِ نِگار
 پأدٔ کٔری مٔتی تٔ یِتھین حالاتَن منْز غنی نِ مرثیِ نِ لیکھنِ چھُنٔ
 عقلِ ویژان۔ 
 اَرداعہمِ صٔدی ہِنْدین مرثی نِگارَن منْز چھے خواجِ حسی
 بٹنِ مرثیِ میلٔ کنِ ہننزِ حیثیت تھاوان۔ خواجِ حسن بٹ چِھ اکھ
 شأعِر تٔ ذأکِر آسنٔ علاویہ اَکھ شمأجی کارکُن تِ أسی مٔتی ۔ تِموَ چُھ
 اَکھ گام بسومُت یُس سرینگرٔ پیٹھٔ کٔژتام کلو میٹرٔے دُور
 وَرمُلی سٹکِ پیٹھ وُنِ تِ گنڑ حسن بٹ ناؤہ آباد چُھ۔ یِموَچِھ کینہ 
 دُعایِیِ مَرثیِ لیچھِ مَژٔ یِمٔ وُنِ تِ معرکن منْز پرَنٔ چھے یِوان ۔ یِم              
أسی نادان تٔ  قلیل تخلُص استعمال کران۔ یِم چِھ أسی مٔتی معرکَن
 منْز پننِ مرثیِ پانے پرَان۔ 
       زیر بحث وَقتَس دوران وَننٔ آمژن مرثِیَن ہِنْدِ مُطالعٔ
 تلِ چھے تِمَن ہِنْدٔ کینہ خصُوصِیٔژ ٹاکارہ لَبنٔ یِوان۔ یِ کَتھ چھے
 پرْیتھ بَنْدٔ تٔ مِصرٔعِ منْز نٔنی زِ یِمٔ مَرثیِ چھے محض دِلٔ کِ شوقٔ
 تٔ دَیِ لولٔ لیکھنٔ آمٔژ۔ ێیمِ وَننٔ وِزِ چھُنٔ یِمَن شأعِرن کُنِ 
 دُنیأوی جاہ وحشمَتُک طمَح تٔ نِ کأنْسِ دُنیأوی معشپقٔ سٔنْدِ
 دٔے گژھاُک شوق بٔلکِ چُھ مزہب اَکھ ژھیدُن تصور کٔرِتھ
 دِلٔ کین جَزباتَن اظہارٔچ زیُو آمٔژ دِنٔ ۔ یِمَن مرثیَن منْز چُھ کمال
 خلُوص تٔ ریا کأری تٔ مَکأری بیترِ جزباتَن ہِنْز چھَنٔ ژھاے تِ </p>

<p> 								 (122)22
 گژھان۔
 دیُت خریدارو نقد وجان بیعانو ہێوتُک دُنیا تٔ أخرت مقیمانو
 تمچی قدر زأنِکھ قدر دانوَ گۄڈٔنی دٔسلابٔ ہێوتُک ایمانو
 دۆپُکھ دۄن عالمَن أسی کرَو کیْاہ اَسِ گژِھ آسُن رضاے الاہ
 تِموَ گوہرِ محبَتٔ دِلِکی خزانِ بھرٔی معرفتکِ بازرٔ أسی جوہری 
            بێیِ زأنِکھ قدر تٔ قٔمت أش مختو اَمِ کے سودہُک بۆرُکھ ہاوسو
 ہمیشِ غمٔ کُے تجارت کۆر کھو واتنوُکھ فأیدس خلق الاہ
                                ( محمد جعفر)
    تَصَوُفٔکی تِ سلسلِ چِھ تِم سأری چِھ صرف نقشبندی
 سلسلِ وَرأے پنُن آگرُ حضرت علی یَس مانان۔ یِ تصور چُھ
 شانِ صوفی شأعری ہُنْد ڈیکٔ ٹِکٔ تٔ اَمِیُک اِظہار چُھ کأشرِ 
 مرثیِ ہِنْدِس مَدَح حِضَس منْز جایِ جایِ لبَنٔ یِوان۔ شُراہمِ
 صٔدی ہِنْدِ ابتدأیی دورُک اَکھ نمُونٔ چُھ۔
 فخر زون فخری درویش کأمِلو          دِتُک دم شہِ سُنْد دۆپُکھ چُھ سون مولا
 رۆٹُک پَتِ تَس مُرشد سُنْد سلسلو        طریقتَس گۓ آگاہ 
 اغسی جلال لأگِتھ پھیتانو              مدح شہِ سٔنْدی پرَانو
 تھیکان کیْاہ لوسٔ وُن شیر خدایو        پران لافتی الاعلی لاذوالفقار
   مگر اکھ چیز چُھ مرثیِ نگارَن باقی صوفی شأعِرن نِش بێون
 کڑان سُ گو یِ زِ چھِنٔ تِ جایِ شریعت ترْأوتھ طریقت </p>

<p> 								 (123)23
 پانٔ ناونٔچ ترغیب دِوان یِہُنْد خیال چُھ زِ طریقتُک آگر جناب علی
 چھِنٔ کُنِ تِ وَقتٔ لوأزمات دین ترک کران تٔ ێیلِنٔ خداوند کریمَن 
 تِہنْدِ باپَتھ یِم چیز معاف کٔری تٔ أسی کِتھ پأٹھی ہیکوَ یِموَنِش
 مُچھِ أسِتھ جناب علی سٔنْدِ عبادَتُک تٔ حضُور قلبُک اظہار چُھ 
 تِموَ ێیمِ آیِ کۆرمُت۔ 
 تمی شأہی رضاے حق کۆر فأصلو      قضاوقدرُک بوسگاہ دستگاہ 
 سُ ێیلِ گژِھ ہے نمازِ اندر دأخلو      زانٔ ہَن قضا کٔرِتھ گو علی مرتضی
 کُس اوس تژِھ عبادت منْز تیارہَس کرانو   سُتِ اوس دۄہَس تٔ راتَس پانْژ پِرِ مرانو 
 اوسُس کانٔ پھل بند گومُت أکِس جأے۔ 
 سُ اوس آمُت پھُٹِتھ کۆڈہَس بَدَن ژٔٹِتھ توتِ گو نٔ نماز گزار خبردار 
   یِمَن مرثیِن منْز چُھ خداوند کریم سٔنْدی حمد تٔ تعریف باضأبطِ 
 اَکھ حصِ اگر یِم بَند الگ کڑِتھ جمع کرَو۔ شأعری ہُنْد ہاوُن 
 باوُن لایق مواد ہیکِ جمع گژھِتھ۔ دۄنی گویِم فِکرِ ٹھیکھ پأٹھی 
 ترَنٔ باپَتھ گژھِ اِسلامُک مترون مطایٔعِ آسُن تِکیازُ یِمَن منْز چِھ 
 منعت تلیمح علاؤ کینہ تژِھ ترکیبٔ تٔ علامٔژ ورتاونٔ آمٔژ یِم نٔ 
 پێٹھی پێٹھی فِکرِ تَرَن تِمَن منْز چُھ ابہام جایِ جایِ لَبنٔ یِوان۔ 
 کُنِ کُنِ جایِ چُھ گُمان گژھان زِ شأعری ہِنْز مأری منْز مَہرین چھے 
 ابہام تٔ وۄپر زبأنین ہنِْدین حرفَن ہِنْدین گہنُنَ ہِنْدِس پرَدسَ
 تَل ژھایِ گأمٔژ۔ </p>

<p> 									(124)24
 مومنو حمدوثنا شُوبِ تَس پادشاہَس
                          ێیمی نبی تٔ مرسل کٔری ایجاد 
 سُ چُھ خألق تٔ پأدٔ کٔرِن زٔ عالم
                          زبان قدر کُن  پان کۆرُن ارِشاد 
 سپُن معنی فیکن   روشن درینٹھ آیِ قدرت صاحب ہوشن
 کۆرُکھ وحدٔژ آ         خدا چُھ اَکھ تٔ 
 سَپنِ کلُی نٔے قَلم تٔ دریاو دوار       سأری خلقٔژ لوسن لیکھان
 برَنٔ یِن سَت زمین تٔ نوسماوات        خألی روزِ بێیِ کانہ مکان 
       أنْتھ چھُنٔ صفژن کُنے       بخشِ فضلٔ پَنِ نے
                     آیِ لاتقنطو چُھ اَمِ کے گواہ 
        لل دێد پیٹھٔ محمود گأمی یِس تام یِمٔ تبدیلیِ کأشرِ
 زبأنی منْز آیِ تَمِیُک پے ہیکوَ أسی کأشرِ مرثیِ پٔرِیھ ٹھیکھ پأٹھی 
 لٔبِتھ۔ ابتدایی دوَرس منْز اوس زبأنی پیٹھ سنسکرتُک غلبِ تٔ
 دۄں أس سنسکرتٔچ جاے فارسی مَفظوَ رٔٹمٔز یِ چُھ پزَرَزِ باقی 
 شٔر ذأژوَ مقابلی چھے کاشرِ مَرثیِ خاصکر حمُد تٔ نعت حصَس منْز فارسی یُک زیادَے 
پَہَن اَپر بلکِ عربِیُک تِ مگر یِ چُھ موضوع تِ
 وَقتُک تقاضِٔ شأعر چھُنٔ ہیکان اَمِ نِش بچٔتِھ </p>

<p>							 (125)25
   خالقٔ ژٔے چُھکھ قدیم وقأیم باقی ہمیشِ حی وقیوم
        بێیِ وأقف ضمیر حکیم وعألِم ذات چأنی سپٔنی نٔ کأنْسِ معلُوم
 کۆرُتھ ظأہِر کُنت کنزاََ خفی ژٔ بأ۔ِن جایِ موجود چھکھنٔ مریی
 دِتُت لن ترانی پیٹھ طُورِ سینا      سپن کل موسی کلیم اتھ سِرسٔے
 کھێیوو اولو العرجو ہولو            رودُ کھنٔ قوت پۆرُکھ لاحولو 
 مگر مرثی نِگارَس چُھ احساس زِ کتِ نَس کۄسٔ زبان شُوبِ اَمی
 مرَثی ہُنْد درد وُچھی توَ یُس اَز برۄنْہ اَلٔ پَلٔ ژور ہَتھ ؤری  
 چُھ لیکھنٔ آمُت۔ مگر گمُاں چُھ گژھاں زِ یِ ما چھے أزی چی زبان
 ہَے ہَے کیاہ َِنٔ ظلم ہَے نَٹھ ژایَم     وۆدُس شمع زَن بند سپنُم موم 
 بۄکِتھٔ وَنٔ تَٹاں چھِم رُم رُم         تِہنْزَن کورین ژورِ وَتِ عاموَ کۆر ہجوم
 آکھ انْدٔ انْدٔ ژلٔنِچِ وَتٔ رَٹنے      تَمی کانْپِ سٔتی معصوخم لٔگی نٹنے 
 گۆنْڑ ہَکھ نار خٔمَن یاں ژپُاسے      کورین ژاو لرزۂ تٔ بیم
 آیَکھ ێیلِ نارٔچ برْیہا              آسٔ ژھانْڑان چاہ
 ژاکھ نارٔ سٔتی اَلٔ اَلو            آسٔ ژھایِ روزان سێکِ تَلو 
 کأشرِ شأعری منْز چُھ نعتیِ حصِ بِلکُل الگ شعٔر ذاتھ
 گٔنزراونٔ یِوان۔ واریاہَو شأعِرو چِھ صرف نعت لیککھِت پننِ
 باپَتھ مُقام متعین کۆرمُت۔ کأشرین مرثِین منْز چُھ نعتِیِ حصِ 
 بِلکُل اَکھ الگ حِصِ تٔ ساسٔ واد شعٔر چِھ صرف اردأ ہمِ صٔدی
 تام لیکھنٔ آمٔتی یِمَن منْز فن معراج کمالَس پیٹھ چُھ ۔ سارِؤے</p>

<p>								 (126)26
  مرثیِ نگارو چھے نعتیِ شأعری کٔرمٔژ
 عطا کۆرُن حدیِث لولاک تِہنْدِس شانَس
                  پرْیتھ انِسانَس گژھِ اعتِقساد
 پأدٔ کۆرُن اول تٔ سوزن أخٔر زمانَس 
                 تھونکھ قدرٔژ سَرِنٔے ہُنْد اعتماد
         بادشاہ کۆرُن عالم غیبَن گنہگارَن پوشندہ عیبن
         اُود سارِنٔے عالمَن ہُنْدُے حق تٔ
                         ( جعفر) 
      اَلٔ پَلٔ چُھ سارِنٔے مرثی نِگارَن ہُنْد یِ اعتقِاد ز نبوت
 تٔ امامت چِھ لأزم وملزوم۔ جناب رسالماب چِھ خاتم لملرسلین
 تٔ علی چُھ تِہنْدُے جانشین یِم ہیکَن نٔ اَکھ أکِس نِش ہر گز تِ
 الگ گٔژھِتھ۔ حضرت علی جناب محمد مصُطفے پتٔ سارِؤے کھۄتٔ 
 بزرگ قابِل اێحترام تٔ معجز نما مانٔنی چِھ مرثی نِگارَن ہُنْد ایمان
         شہر علم محمد مصطفے    بێیِ مرتضی علی سُ باب العلوم
    تَس حضرتن دۆپ لحَمک کحمی     یس اوش چشمن خلل تَس درینْٹھ آے زٔے
    أسٔکھ اَکٔے زبان أسی یک دِلو    دۄنوَے صراط المستقیم رسول الا
 کأشرِس منْز یدوَے مثنوی نِگارو منظر نگأرو منظر نگأری ہِنْدی
 تھدِ تٔھدی شاہکار پیش چِھ کٔری مٔتی۔ مگر مرثیِ نِگارَوچھ مثنوی </p>

<p> 								 (127)27
 نگأری برْۄنْہ ہَتٔ واد ؤری یِم پرمانٔ تِ چُکأوتھ تھأوی مٔتی
 تٔ اتھ معاملَس منْز چھکھ واریاہ میلٔ کنِ پَتھ ترْاوِمَژٔ۔ واقعات
 چھکِھ یِتھ پأٹھی پیش کٔری مٔتی زَنتٔ أچھَن برۄنْہ کنِ چِھ سپدان
 دِژ تام شمُرَن کرْیکھا گو حأصل مدعا لٔدِو وُونْٹن رسولٔ سُنْد خلخان
 ژھنکھ برْۄنْہ برْۄنہ عابدین شاہ پَتِ فوجا خُولی کافر انْدٔ انْدٔ پھیران
 تیُوت اوس کھوژٔوُن سُتِ زان بیب سیکنِ یۄسٔ بیِ سٔتی أس وُلٹٔ پأٹھی
 وسی پیوان۔
 ێیلِ کوفٔ شہرکِ برَژٔاو درْاو آنتھٔ رۆس خلقا مدنٔ کین بازرن چُھ پَکُن
 یِہُنْد چدَ چُھ محمد مصطفے توے کۆرکھ کیا عأشرُک دۄہ کربلا میدان 
      أنِکھ سُوال کٔرتھ ترْأوِکھ مأرِتھ مأرِتھ تِ وُنِ چھکھنِ مرثی تُلان
                                           (میر محمد عباس) 
 کأشرِ مرثیِ چھَنِ محض منظُوم تأریخ بٔلکِ اَتھ منْز رٔژِ
 شأعری ہِنْدی سأری گۄن موجُود۔ جزبات نگأری چُھ اَمِ 
 شأعری ہُنْد گہنٔ۔ اَتھ منْز چھے جایِ انِسأنی جزباتن ہنْز
 ترجمأنی۔ 
 کٔرِنی حمد تیتی ییتی بھأے بند مأری ہس
                     وۆدُن پرْیتھ شہیدس کۄچِھ ہیتھ نرَدتھ شانْد
 قربان تس حضرتَس تٔ تۂنْدِس صبرس
                     دۆوُن ہر شہیدس مَرَن وِزِ خانِ آباد </p>

<p> 							 (128) 28
 پھِران لَرِ لَرِ بایَن تٔ گۆبرَن وُچھان عباس عٔلی یِنین زَخمَن
                     یَس دۄنوَے نَرِ ژٔ ٹِتھ  ترْاوہَکھ تٔ
 حسین سٔنْد شہادتُک حال وُچھِتھ
                     بٔڑی بابٔ رسالتماب سٔنْدی جزبات
 دۆپنَس کوریِ ڈیکِ لوحسَ دسخط کانِ قلمَن
                     نورٔ دیدٔ گاہ مے تھۆکم
 اتھ مصحف رویس خوانِ سۄرخی پھیش کمٔی دیْتُے
 ماجِ جناب فاطمِ زہرا سٔنْدی جزبات
                    فاطمِن وۆد زار زارہو
                    جبریلِ سٔنْد رٔچھمٔتِ
 ہیتھ کۄچِھ کأر دۆپنَس لگیو پأری ہو
                    کأنْسِ پأری زان کٔرٔے نو ترْیشِ دامٔ اَکِ
                                     (جزأالا) 
     جناب زینب ێیلِ بایَن تٔ گبرَن اکِ اکِ چھے مران وچھان
 تٔ پَتٔ ێیلِ جناب حسین عِ چِھ مأدان سکھران پرْیْتھ کأنْسِ چُھ پَے
 زِ یِ چُھ أخری ملاقات تٔ اَمِ ساتٔ کیْاہ چِھ جناب زینب سٔنْدی
 جزبات۔        بوزم زینب ذوالفقار ہیتھ درایِ پانو
               ژٔہ روز حیسٔ گۄڈٔ سَپنَے بٔے فِدا </p>

<p> 									 (129)29
      اَسِ دۄن بێنین رُودُکھ بأے جوکُن ہو
      بایو کھۄرمُر ساعتاہ  گۄڈٔ مَرٔ بو
      مارَن       چون وُچھِتھ ہیکٔ نو 
            اَکھ موج چھے پَنٔ اِس مہرازٔہ پۄترس وَدان۔
 گٔیَم چانِ رتٔ یِتٔ مے تے   پننِ   والچ ورٔ ژھیتے 
        اَتھ جزبار نگأری منْز چھے اَکھ اہم کَتھ یِ زِ کأنْسِ تِ بێیِ
 ماجِ یا کورِ چُھنٔ یِ افسوس زِ اَسِ کیازِ دِژ یِ قربأنی بٔلکِ چِھ یَژھاں
 بَڑِ کھۄتٔ بٔڑ قرُبأنی دِنی تٔ اَتھ سأری سٔے پیٹھ چِھ تَن برَضا۔ 
 بیب مہربان چھے پَنٔ نِس ترْواہ وُہرِس مہرازَس مأداں سَکھراونٔ وِزِ
 ونان۔
 وچھتو بیب مہربانٔ کش بوحاتم      دۆپنَس پترو پۆت پھیرِتھ ؤچھزِنا
 تَمِ دۆپ شاہ قأسمَس پٔرتھ مرنَس نیرتم 
                            لگے پۆت ژھایِ
                  امام حسین مشیم کتھٔ چون مَرُن ہو
                 قبُول پۄترٔہ دۄکھ  چھُم یا اِلہی      (محمد جَواد) 
 یِمٔ مرثیِ چھے پننِ وَقتُک أنٔ ۔ ێیلِ ژکوَ پَتٔ کشیرِ منْز فرقِ وارانِ
 افرا تفری تُلنٔ آیِ ژۄپأری اوس نار مار تٔ لُوٹ سپَدان۔ خواجِ حسی
 بٹ چُھ اَمِیُک تزکِرٔہ کران۔
 باکٔ کِتھ پأٹھی دِمو ێیتِ ہوز اجیکھ امام حسیننِ ماتم سراے </p>

<p> 							 (130)30
                               ہَے ہَے وَدٔ ؤنین جاے
 تِہنْدی لارِ لَر ودٔ ؤنی ژأری گأری گأری مارنٔ آے ہے ہے
                               ہَے ہَے زأجہَکھ کیاب خانن جاے
 رُوز کھنٔ چھپنس جاے کینْژَو کۄلِ وۄٹھ ژھٔنی کینْہ قَبرَن ژاے
                              ہَے ہَے أسی روزوَ ژھاے 
    زیرِ بحث وَقتَس دوراں وَننٔ آمژَن مرثِیَن ہُنْد مطالعٔ کرَنٔ
 تَلٔ چُھ نَنان زِ یِمٔ چھے پابند وان تٔ فارسی قألِبوَ علاؤَہ تتھین سأپخن
 منْز پیش کرَنٔ آمٔژ ێیمِ پَتٔ اَمِیُک مستقل فارم بنیو تٔ أتھی فارمَس
 چِھ اَز کل کأشر ریوایتی مَرثی وَنان۔ خۄجِ حسن میر چُھ اَمِ قٔسمِ
 مرثی لیکھنُک بأنیکار۔ تِہنْدی دٔسی ووت یِ فنَ بابِ پور حبِ کدل
 کین رٔیسن ہُنْدتٔ پَتِ گو مرثی نگارَن ہُنْد زیٹھِ زیُوٹھ کارواں  
 پأدٔ یِمنَ منْز حکیم عظیم منشی یُوسُف تٔ خۄجِ بأقِرہی وۄستاد
 تِ شأمِل چِھ۔ اَمِ قسمٔ چھے ساسٔ واد مرثیِ لیکھنٔ آمٔژ۔ یِمَن منْز
 لحَچِھ بٔدی شعر چِھ۔ خۄجِ حسن میر چُھ اردأہمِ تٔ کُنوہُمِ صٔدی ہُنْد 
 سُ عظیم گۄنمات ێیمی مرثیَن اَکھ نۆو زوُ زیتٔ تٔ پوشن پاے 
 دیُت۔ تِم أسی حبکِ بانِگل رِزان تٔ شہر أسی لۄبَر کنٔنِ وَسان۔
 اَکِ دۄۂ لٔگی تِم حبِ کدل حکیم عظیم تٔ پرْژُ ھہَس یِ کیْاہ چھے
 تٔمی دۆں نکھ وَتھرُن تٔ وُرُنط حکیم عظیمَن ێیلِ یِ جواب بُوز تمٔی</p>

<p> 								 (131)31
  ژیُون أمی سُنْد بَبرَتٔ رۆٹُن واریاہَن گری تٔ تس نِس تِنْہزٔ
 مرثیِ بُوزِتھ لاجِ حکیم عظیم تٔ مُنشی یُوسفَن تِ اَمی آیِ مرثی لیکھنِ
 خۄجِ حسن میر چُھ گۄڈنِیُک شخص ێیمی ناؤ سان مرثیِ لیحچھ۔ اَمِ پَتِ
 چُھ سلسِلِ برابر اَز تام جأری ۔ ناو ہێکِ مرثیِ کینہ تِ أسِتھ مگر
 ناو ژأرِتھ چُھ مرٔی نِگارَس خیال پیوان تھاوُن زِ اَتھ فارمَس سٔتی
 وابستٔ سأری چیز گژھَن مرثی منْز یِنی۔ بێیِ گژھِ مرثی منْز 
 ناوُک خیال انْدٔہ ونْد روزُن ۔ مشہُور تأریخی واقعِ چُھ زِ خۄجِ باقر
 تٔ محمُود گأمی أسی ہمکال شأعری تٔ اَکھ أکِس یار تٔ ێیلِ تِمَن کانْہ
 نؤ تخلیق تھَنٔ پێیِ ہے تِم أسی اَکھ أکِس بوزناوان۔ خۄجِ باقرن
 ؤنی مرثیِ  بہار  تٔ بوزنووُن محمود گأمی ۔ اَنْدٔ ونْد نُوزتھ وۆنُس
 محمود گأمی یَن زُ مرثیِ چھے اصٔل۔ وۄں گو بہارَس منْز چُھ کٔنکٔ
 ڈارَن منْز  نیاے  پوش پھۄلان ێیمیُک ژے تزکرٔہ کۆرمُتے 
 چھُے نٔ۔ خۄجِ بأقِرَن وۆننَس زِ دردُک یِ مصاعِ ۔ 
   یُس خُدأے پھۄلِ نود  سُے یزید  ژٔٹِتھ  نیْوُ
 محمُود گأمی ێیلِ یِ بُوز تس گٔے ؤدی ؤدی گٔحچھِ تٔ دیُتنکھ داد۔
 خۄجِ حسن میرَن چھے نۄمٔ مرثیِ لیحچِھ مَژٔ (1) أیینٔ (2) اسم                
 الا (3) رمل (4) حق نوا (5) خۄش نوا (6) غلات ونعمات 
 (7) سرِ وحدت (8) جوأہرِ (9) عابد (10) ہما (11) کمان وغیرہ 
 فی مَرٔیِ منْز چِھ ہَتٔ واد شعر۔ خۄجِ حسین میرنی چھے کأشرِ </p>

<p> 							 (132)32
 مرٔیِ مقام بٔندی تٔ چھیر تِ چھے أمی سُنْدے ایجاد۔ ژک دورَس
 منْز اوس کشیرِ منْز صوفیانِ موسیقی کُن شأعِرَن گومُت تٔ یِتھ 
 پأٹھی چُھ صوفیانٔ موسیقی منْز آغاز ، اوأنی، تٔ سپرد بیترِ آساچُھ
 یِتَھے پأٹھی چُھ مرثیِ منْز تِ برخواست، گت تٔ نشست آسان صِوفیانِ
 موسیقی تٔ مرثیِ چِھ دۄنوَے ظلم وجبرکی پأداوار۔ دۄنؤنی ہِنْدِ
 پرنُک انداز چُھ اَلٔ پَلٔ ہوُے۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔
  یِ کَتھ چھے اِنتِہأیی اہم زِ کأشرِ مرثیِ چھے آڈیو یعنے بوزنُک چیز
 نِ کِ پرنُک ، تِکیازِ پرْیتھ مرثی چُھ پنُن الگ وَزَن آسان تٔ یِ ہێکنٔ
 اَمِ ورأے بێیِس وَزنَس منْز پرَنھٔ یِتھ۔ یِم وزن چِھ پانِ آسان
 شأعِرَن مُقرر کٔری مٔتی۔ پرْیتھ مرثی چِھ کم سے کم پانْژھ چھیر (بند)
 آسان۔ یِ تیداد ہێکِ زیادٔ تِ أسِتھ۔ مرزا ابوالقاسم نین مرَثین
 منْز چُھ یِ تیعدادوُہِ پنٔژٔہ تِ پێٹھی۔ پرْیتھ چھیر چھے کینْہ
 مِصٔرعِ آسان۔ ێیمِ چاٹٔ گژھِ وضاحت۔</p>

<p> 3مرثی ہِنْدی پانژھ حِصِ
   (1) حمد۔ خدا وند عربی سٔنْدی تعریف ، گۄنِ ہَن عفو تٔ
 خدایِ سُنْد شُکرانٔ اَکھ چھیر۔
 (2) معت۔ محمد عربی سٔنْدی تعریف تٔ تِہنْدِ وسیلٔ خدایَس
 گۄنِ ہَن مغفرت منگُن۔ اکھ چھیر۔</p>

<p> 							 (133)33
    (3) مدح۔ تعریف حضرت علی تٔ ژۄداہ معصُوم ۔ اکھ چھیر۔
    (4) معجٔزہ۔ پیغمبر آخرالزمان سُنْد کانْہ مُعجٔزہ یا کأنْسِ پیغمبر یا
 حضرت علی یا امام سٔنْز کانْہ کرامات یا کُنِ اسلأمی جنگُک
 حال احوال۔ چھیرَن ہنْد تعداد حسب ضرُورت کم یا زیادٔ سَپدان۔
   (5) درد۔ تزکرہ وفات جناب رسالتماب ، حصرت علی
 جناب فاطمِ باواقعِ کربلا یا یِتھی ہیُْوہ کانہ واقعِ۔ چھیرن
 ہُنْد تیعداد حسب ضرورت۔</p>

<p>نْہ مِصٔرعِ آسان۔ ێیمِ چاٹٔ گژھِ وضاحت۔ 3مرثی ہِنْدی پانژھ حِصِ   (1) حمد۔ خدا وند عربی سٔنْدی تعریف ، گۄنِ ہَن عفو تٔ
 خد
               0 
</p></body></text></cesDoc>