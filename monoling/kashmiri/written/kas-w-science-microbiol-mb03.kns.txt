<cesDoc id="kas-w-science-microbiol-mb03.kns" lang="kas">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kas-w-science-microbiol-mb03.kns.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ARSHAS TAM</h.title>
<h.author>SHOWK</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - BOOK</publisher>
<pubDate>1974</pubDate>
</imprint>
<idno type="CIIL code">mb03.kns</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 105/.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-24</date></creation>
<langUsage>Kashmiri</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>
ْ0ْ3                          1ُ105ٔ
زٔ نسلی گیگسن وےسمےن 
سُنھ وَنُن ۄھُ زِ ےِمن ھۄن ہِصن ۄھُنِ مگنٔ ؤنی کگنْہ وگنھ َ مگہول ی ورنگو
ےگلیے وگنگو ۄھُ وسمگٔنی ہصس مینھ سینھگہ گںر نرگوگن نٔ نھلےِ گنگن ی 
مگر نسلی ہصس مینھ ۄھُنِ نمےُک کگنْہ گںر میوگن َ مںلن ےِ ۄھُنِ ضۆرُوری
زِ مہلو گنس گژھ طگقن وَر نۄِ زےۆن َ نٔمی صُنھ نۄِ ہێکِ لگغر نِ گٔسِنھ ی
سُ ہێکِ مننِ زنھگی منز منُن مگن کمگونٔ سٔنی مہلوگن نٔنِنھ َ گنھ نرعکس
ۄھُ کُنِ سگنٔ گٔکِس لگغر گنسگنس نِ طگقن وَر نۄِ زیوگن َ </p>

<p>	کگٔشرین زنگنن وون وۄنی سینھگہ کگل منٔنی کَن ژۄمگن َ گگر
ہگٔصِل کٔمٔژ ۃۆصُوصی ےَنھ ورگںنی گیسِ ہے ی کُنِ سگنٔ گٔژھ کگنْہ کُور کَن
ژۆمِنھ نِ زےۆنٔی </p>

<p>	ےِنھی کےنْہ مزر نظر نَل نھگٔوِنھ سمھ لیمگرک سُنھ فلسفٔ رۆ َ مگر گز کل
ۄھُ گنھ مینھ نَوِ سرٔ سونْۄنٔ ےوگن َ کےنْہ نِنھی نورنِ ۄھِ ےوگن کرنٔ ی ےِم ےِ 
ںگٔنِنھ
ۄھِ کرگن زِ مگہول ہێکِ نسی ہصس مینھ نِ گںر نرگٔوِنھ َ نِ کٔرنھ ۄھِ سگےنس
ھگن ونگن زِ زِنھگی منز ہگٔصِل کرٔ مٔژ نھلےِ ۄھَنِ ورگٔںنھ نرگن َ مگر نێلِ
کےگہ ۄھُ ےِمن نورنن ہنھ وَوِ َ نِ ۄھَنٔ وُنِ کگٔنسِ ۃنر َ </p>

<p>ْ02َ یگروِن سنھ نظرےِ َِْ0 ۄگرلس یگروِن ُ1809َ1884ئٔ</p>

<p> </p>

<p>ْ0ْ3                                2ُ106ٔ
گوس گکھ گنگرےز ط ہےگٔنی گرنقگۄِ مرێژھِ ووگن ھِنس منز ۄھُ یگرونس
سگرِنٔے کھۄنٔ نھۆھ رۄننٔ ہگٔصِل َ نٔمی سُنھ نظرےِ ۄھِ گکںر لُکھ مگنگن 
نٔمی ھےُن نٔ ےِ منُن نظرےِ گۄھ مٔۄھ ؤنِنھ ھۄہن ےگ رینَن منز ط نٔلی کِ لگگنگر
گکوُہن ؤری ےن نم ژَم گگٔلِنھ ط یگروِن فےُور مشہُور وہگز نیگلس مینھ
سینھگہ ھُنی ےِہس َ ےِ سفر کۆر نٔمی مگنژن ؤری ےن طَے َ وگرےہن علگقن
منز گژھنھ کۆر نٔمی ہےوگنن نٔ کلین ہنھ مشگٔہھٔ َ نٔمی وُۄھ مرینھ وگےِ
زُو زگٔژن منز مگہولس مطگٔنِق نھلےِ گنٔ نٔۄ گکھ ہگٔرگن کرٔؤنی نہو َ </p>

<p>	یگروِن سٔنھِس نظرےہس ۄھِ عگم مگٔنھی قۄھرنٔ زگٔر
نظرےِ ونگن َ </p>

<p>1837ئ ےس منز مۆر یگرونن گکھ مضمُون گینگٔھی مینھ
گمےُک لےکھن وول گوس مگلنھس
گنھ مصمُونس منز ۄھُ وَننٔ گمُن زِ گینگٔھی ۄھَ وےَومےنری 
ہنھِس نسننس مینھہرگن نٔ کھین ۄین نٔ منگہ وگے ۄھِ گٔکی سٔے ینوِ
روزگن َ ےِ نۆی نگرٔ نضگھ ۄھُ ونگ ی وَنگ ی ھرگگ ی نےنرِ مگٔھٔ کرگن ط ُمگلنھس
سُنھ نظرےِ ۄھُ رَھ سمھ مُن ٔ یگرونن ننگٔو مگلنھس سٔنزِ رگے مننِ
فلسفٔۄ نُنی ےگھ َ یروِن ۄھُ منُن ملسفٔ ژھۆنرگٔوِنھ ےنھٔ مگٔنھی نگوگن َِ
مرینھ کگنہ زگنھ ۄھَ نےھگھس منز نگرٔ وُژھ ہرگن َ ےێمِ کِنی زری ےگٔژن</p>

<p> </p>

<p>ْ0ْ3                               3ُ107ٔ
منز گکھ نٔی نگرٔ وھ و وہھ وگٔری ۄھَ َ ےنھ گٔسی زِنھٔ روزنٔۄ وھ و وہھ وَنَو َ 
ےس فرھس فگٔےھٔ منھ ۃَوصُوصی ۓژ ہگٔصِل گیسن ی نس گیسن زِنھٔ روزٔ نس
منز زےگھٔ سہُولٔژ نِ ہگٔصِل َ نٔ نٔمِس ۄھَ قۄھرَن زِنھٔ روزنگٔ نگمنھ ژگرگن ی
سُ فرھٔ ۄھُ مننِ فگٔےھٔ منھ ۃۆصُوصی ےژ منٔ نین زرین ورگںنس منز ھِوگن َ 
گَو یگروِن سٔنھِس فلسفس ۄھِ مگنژھ ہِصٔ َِ</p>

<p>ْ01َنھلےِ ۄھَ گیفگٔقی َِْ0 گٔسی ۄھِ زگنگن زِ زٔ فرھ ۄھِنٔ زگنْہ نِ ہُونہُو
نقل گیسگن َ مرینھ کگٔنْصِ ۄھَ مننِ ۃۆصُوصی ۓژ ےگنھلےِ 
گیسگن َ ےِم نٔ زگنْہ نِ نێےِ کگٔنْسِ سگرێے گیسن َ نھلےِ گیسنٔ ۄھُ قۄھرنُک
گکھ سۃن قونُون َ یگرونن نےِ منُن فلسفٔ وگضہ کرنٔ ۃگٔطرٔ لۄکۄ لۄکۄِ
نھلےِ َ مگر نٔمی کٔر نٔ ےِمن نھلےن ہنز نُنی ےگھ زگنٔ نٔۄ کُوشش َ نٔمی سنھ نروسٔ
گوس زِ فگٔےھٔ منھ نھلےِ نھگوگن وول فرھ ہێکِ نِمن فرھن ہنھِ کھۄنٔ زےگھٔ
سہل مگٔنھی زِنھٔ رُوزِنھ ی ےِمن نٔ ےِم فگٔےھٔ منھ نھلےِ گیسن َ گو نھلےِ ۄھَ
قۄھرنٔ ژگرِ ہنز نُنی ےگھ َ </p>

<p>ْ02َھرٔ ؤنی گینگٔھی َِْ0 سگرێے زُو زگٔژ ۄھَ نگرٔ وُژھ منٔنی گینگٔھی ہرٔ رگوگن
ہۆس ےۆھوَے وُژھ ۄھُنٔ نِوگن نٔ ۓژ کگٔلی ۄھُ مین ھِوگن َ لےکن گگر صرف
گکھ ۂسی وُوری نرٔ ہِ وہری نسٔل کٔشی شلرُو کرِ ی نمنِ وہری کرن ےِم شێِ
نۄِ مگٔھٔ َ یگروِنن کۆر شُمگر زِ 750ہن ؤری ےن منز کرہے ےِ وُوری 900</p>

<p> </p>

<p>ْ0ْ3                            4ُ108ٔ
لۄھ زُری مگٔھٔ َ </p>

<p>	مرگمےںےم ۄھُ گکھ سینھگہ نۄ زُو زگنھ 
ُنےُون نِۄ ز گٔۄھَو سٔنی ۄھُنِ نوزنٔ ےِوگن ٔ گَمی زگٔژ ہھ ھکھ فرھِ ۄھُ 47ون
گگٔننن منز نرێِ فرنقسیم گٔژھنھ گٔنھ زری مگٔھٔ کرگنَ گگر گٔمِس گٔکِس
فرھٔ سٔنھی سگری زُری مگنژن ؤری ےن زِنھٔ روزن َ ےِم ننگؤ ہن زمینِ ےینی
ےینی ھگہ سگس گنٔ نۆی زِنھٔ مگھُک گکھ نُن َ ہسگن ۄھُ گیمُن کرنٔ زِ نَو
سگس میر گٔژھنھ رنِ ہے ےِ زِنھٔ مگھُک نُن سگری کگٔے نگنٔۄ وگے َ
نٔ گمےُک ہلوُم ہرہے گگشِ کِس رفنگ4س مینھ ط ےگنے گٔکِس سێکنیس
منز 184000میل َ </p>

<p>ْ03َ زِنھٔ روزنٔ نگمنھ وھو وہھ ِْ0 سرێےِ زُو زگٔژ ۄھھ سینھگہ
وُژٔ ہِ نےھگھس مز ہُرگن َ مگر غزگنٔ منگہ ۄھِ مہھُوھ َ گؤکِنی ۄھِ سگرِنٔے
زُو زگٔژن ہنھی فرھِ مگنےۆ مگنےۆ کرگن نٔ زےگھ کھۄنٔ زےگھٔ کھین ۄین نٔ منگہ
وگے ہگٔصِل کرنٔ نگمنھ سَۃ نرِ زنگ کرگن َ ےِ وھ و وہھ ۄھِ زو ھگر منٔ نِ
زگٔژن ہنھِ نٔے فرھن سٔنی زےگھٔ مہن کرگن َ نِ کےگزِ ےِہنزٔ ضۆرُورٔژ
ۄھَ ہِشے گیسگن َ مگر کُنِ سگنٔ ۄھَ ےِ وھ و وہھ مۃنلف زگٔژن نِ مگنٔ ؤنی
میوگن کرٔنی ط نرێِ مِ قسمٔۄ وھ و وہھ ۄھَ زُو زگٔژ قۄھرن کین
طگقنن ۃلگف نِ کرگن َ ےِم قۄھرنٔکی طگقن ۄھِ ےمی ی ونگ ی نُنلی طُوفگن ی </p>

<p> </p>

<p>ْ0ْ3                       5ُ109ٔ
رۄھ ی شین نےنرِ َ گنسگن نِ ۄھُ ےِ وھ و وہھ نگقٔے زگٔژن ۂنھی
مگٔنھی کرگن َ </p>

<p>ْ04َ گٔری سُنھ زےنُن َِْ0 گسِ وُۄھِ زِ سگرِنٔے زُو ھگرن ۄھَ مننِ
مننِ نھلےِ َ نٔ ےِمن ۄھَ زِنھٔ روزنٔ نگمنھ گژھین وھ و وہھ میوگن کرٔنی ی
ےِ ۄھ نٔنیکنھگ زِ ےس گٔکس ےِم نھلےِ فگٔےھٔ منھ گیسن ی سُ زےنِ گنھ
وھ و وہھ منز ضۆرُور َ گنھ ونو گٔسی گٔری سنھ زےنُن
ےگ قۄھرنٔ ژگٔر َ 
نِ کےگزِ قۄھرن نِ ۄھَ نٔمی سٔے زِنھٔ روزنٔ نگمنھ ژگرگن
ےس نھلےِ فگٔےھٔ منھ گیسن َ ےگ ےِم مگہولس زےگھٔ وگن مگٔنھی ژگٔلِنھ ہێکےن ی
ےِمن فرھن نٔ ےِم فگٔےھٔ منھ نھلےِ گیسن ی نِمن ۄھَ قۄھرن مِنگوگن َ 
گَوَے کِنی ۄھِ سگٔری زُو ھگر گٔنھی َطکھس مُطگٔنِق منُن مگن نھلگونٔۄ
کُوشش کرگن َ </p>

<p>ْ05َ فگٔےھٔ منھ نھلےِ ۄھَ ورگںنی َِْ0 زِنھٔ روزنٔۄِ وھ و وہھ منز زےننِ
نگمنھ ۄھِ سگٔری زُو ھگر فگٔےھٔ منھ نھلےِ مگٔھٔ کرنٔۄ کُوشس کرگن َ مگٔھٔ
کٔرمٔژ نھلےِ ۄھِ زُو زگٔژ منٔ نین نسلن نٔ زرین ورگںنس منز نۃشگن ی
ےِم نَوِ نھلےِ نٔ ۃۆصُوصی ۓژ ۄھَ میرمنھ میر زےگھٔ رےگھٔ نَنِ نےرگن ی
ےۆنگم کےنہ میرِ گٔژھنھ نِل کگنہ نو زو زگنھ نُوھ ۄھَ گژَھگن ی</p>

<p> </p>

<p>ْ0ْ3                           6ُ110ٔ</p>

<p>ےنھ مگٔنھی ۄھ زو زگٔژ ننگن َ سگٔنی وگٔنِس ۄھَ نیژگہ
ژھۆن زِ گٔسی ۄھِنٔ ہیکگن ےِ زینھ عمل وُۄھِنھ َ ےۆہے ۄھُ وَوِ زِ
کگنہ نے قٔل ۄھُ مرژھگن َ نێلِ کونٔ ۄُھ گزنِ کگنْسِ وگنْھس
گنسگن ننگن ێ</p>

<p>ْ0یگرون سٔنھِس نظرےہس ۃٔلگف گےنرگض َِْ0 1َ یگروِن ۄھُ
ونگن زِ قۄھرن ۄھَ زُو زگٔژن ۂنز لۄکۄِ لۄکۄِ ۃۆصُوصی ۓژ نظر نل
نھگٔوِنھ ژگٔر کرگن َ مگر ےِ گنھگزٔ کرُن ۄھُ سینھگہ مشکل زِ ےِم لۄکۄِ
لۄکۄِ نھلےِ کنھٔ مگٔنھی ۄھَ کگنٔسِ زُو ھگرس زِنھٔ روزنٔ نگمنھ وھ و وہھ
منز مھھ کرگن َ کےنہ ۄیز ۄھِ نٔنی مگٔنھی گَنھ وھ و وہک منز مھھ ہیکگن
کٔرنھ َ مںلن سرٔو علگقن منز ہَےوگنن ہنز وَنھ َ گگسٔ کھگو ہےوگنن نشِ
نۄنٔ ۃگٔطرٔ کےنCن کلین ہنھی کٔنیی ی ےگ کےنژن گگین ۄھِ ھشمنن نِسِ
نۄنٔ ۃگٔطرٔ لۄِ مینھ نولی ھگر کٔنیی َ ےگ کےنژن گگین ۄھِ ھشمنن نِسِ
نۄنٔ ۃگٔطرٔ کےنژن مینھ نولی ھگر کٔنیی َ فگٔےھِ ہنھ ۃۆضُوصی ۓژن ۂنزٔ ۄھَ ےِژھ
گٔنِنھ گۆس مںگلٔ َ مگر ےِ کنھ ۄھَنٔ ہَنِ نۄن وَسگن زِ مومُولی نھلےِی
مںلن منٔ مونمرین ۂنھی مۃنلِف رنگ ی کلین مینھ نِکی ےگ نٔیی مَنٔ ؤنھٔر گیسٔنی
نےنرِ کنھٔ مگٔنھی ھِن زُو زگٔژن فگٔےھٔ نٔ ننن وَرگںنی </p>

<p>2َ کےنژن زگٔژن ۄھِ کےنْہ نگن نٔلی زےگھٔ مۃصُوص ننگن َ مںلن
ہگنگلن ہنھی ہێنگ َ ہگنگلن ۄھِنٔ منٔنی لنوِ ھگر ہێنگ کگنْہ ۃگص</p>

<p> </p>

<p>ْ0ْ3                            7ُ111ٔ
فگٔےھِ ھِوگن َ نٔلی کِ ۄھِس گنین ونگلن منز ژلنٔ وِزِ سینھگہ نھۄس گَنگن ی
مگر نونِ ۄھِ ہگنگلن ہنھی ےِم ہێنگ ورگںنی َ وۄ گَو عگم مگٔنھی ۄھُ
ۃےگل ےِوگن کرنٔ زِ منھ کگلٔکی ھےو قگمن ہےوگن گئے گَوِ کِنی نگےُوھ زِ
نِم گٔسی نٔلی زےگھٔ وێنھی َ نِمن گوس نٔ منُن وێۄھر زِنھٔ روز نِۄِ وھ و وہھ منز
کگنْہ فگٔےھٔ ھِوگن َ </p>

<p>3َکےنژن زگٔۄن ۄھَ ےِ ۃۆصُوصی ےنھ زِ نمن گگر کگنْہ نگن ژھێنگن
ۄھُ ی نِم ۄھُ سُ نگن نوِ سرٔ مگٔھٔ کرگن َ مگر یگروِن سُنھ فلسٔ ۄھُنٔ
گنھ کنھٔ فُنھ کیگن َ مںلن کےنژھن سرفٔ زگٔژن گگر لٔن ژھێنگن ۄھێَی
نِم ۄھھِ کےنْہ کگل گٔژھنھ نولُن مگٔھٔ کرگنَ مگر لصِ ہنھ کُس فگٔےھِ ۄھُ
ےِمن ہےوگننێ کہنی نٔ  گگر مزی مگٔنھی لُن ےِمن زِنھٔ روزنٔی نگمنھ ضۆرُوری
گیسِ ہے ی نێلِ گۆژھ سُ ہےوگن لُن نَوِ سرٔ مگٔھٔ کرنٔ نرۄنٔہے مرن 
ےِم مزر نظر نل نھگٔونھ یگروِن سٔنھِس نظرےہس مینھ سیگیھگہ
شک ےِوگن کرنٔ َ یرونن ۄھُ مگنٔ نِ مونمُن زِ قۄھرنٔ ژگٔر ہنھ
گۆصُول ۄھُ ہےگٔنی گنقگہُک ننی ےگٔھی وَوِ 0َ مگر ےِہے ےونٔ ۄھُنٔ 
ورگںنس مینھ مےنیل سٔنھی گۆصُول 1900ئ ےس منز ھٔری ےگف
سمھنٔ سٔنی لۆہ یگروِن سٔنھِس فلسفس سینھگہ ھَکٔ َ نِ کےگزِ ےِ گیو
وُۄھنٔ زِ ورگںنُک طریقِ صھُ نسلٔ سیلن منز َ یگرونس گوس نٔ</p>

<p> </p>

<p>ْ0ْ3                            8ُ112ٔ
سیلِ ہنھ کُنِہ نِ علم َ وۄنی ےێلِ زن سیلِ ۂنھی علی مَن سینھگہ نرقی
ۄھَ کٔمٔژ ی گرنقگہُک کھری ھگر مسلٔ سموُن نِ ننےوو سہل طَ گٔزی کین سگےنس
ھگنن ہنھ ۃےگک ۄھُ زِ ورگںنی نھلےِ ۄھَ نسلٔ سیلن منز گۄگنُک ننھیلےو
سٔنی مگٔھٔ گژھگن َ گمِ کِنی ۄھُ یگرون سُنھ لۄکۄن لۄکۄن نٔ مسلسل نھلےن
ہنھ ۃےگل ری ےوگن کرنٔ َ یگروِن سٔنِھس  نظرےہس منز گیےِ ےِ نرمیم کرنٔ
نٔ یگروِن سُنھ نظرےِ گَو نَوِ سرٔ زِنھٔ َ</p>

<p>ْ03َ یی وےریز سُنھ نظرےِ َِْ0 ہگو یی وےریز گوس گَکھ
ولنھ ےزی ط نٔمی کٔری منٔنی نورنِ کلین مینھ َ نٔمی وۆن زِ نَوِ زگٔژ ۄھَ مرگنیو
زگٔژھو مینھٔ گۄگنک نٔ ہنگ نٔ منگٔ مگٔھٔ گژھگن َ ےِم گۄگنک نھلےِ
ۄھَ گیسگن وَرگںنی َ مرگنِ زگٔژ نٔ نَوِ زگٔۄ منز نگگ ۄھُنٔ مٔنزی مین  زگٔژن
ہنھ گکھ سِلسلٔ گیسگن َ ےِم گۄگنک نھےِ ہێکن فگٔےھٔ منھ ی فۆضُول نٔ
نۄقصگن ھیہِ نِ گٔسِنھ ط قۄھرن ۄھِ ےِمے گۄگنک نٔ نوِ نھلےےو نظر نل
نھگٔوِنھ زگٔۄ ژگرگن َ گکھ نسھ0 زگنھ ہێکِ گٔکی سٔے میر منز نِ مگٔھٔ گٔژھنھ 
یی وےریز سٔنھی منٔنی لفظ ۄھِ  نَو زگٔژ ۄھَنٔ وگرٔ وگرٔ ی سھلن
سۆن ہَنٔ نھین نٔ سگسٔ نھین ؤری ےن منز فۄورنٔ ژگرِ سٔنی مگٔھٔ گژھگن ی
نٔلی کِ گۄگنک ی مگر لۄکۄَو ی نھلےَو سٔنی  گنِ ۄھُ نگسگن زِ یی
وےریز سُنھ نظرےِ ۄھُ ژَنھ وگنھ کٔرنھ یگروِن سنھُے نظرےِ َ مگر ےِ نظرےِ </p>

<p> </p>

<p>ْ0ْ3                              9ُ113ٔ
ۄھُنٔ ےِ ہگوگن زِ کُلی ن ہےوگن کنھٔ مگٔنھی ۄھِ گٔنھی مٔکھس مُطگںنِق مگن
نھلگوگن َ </p>

<p>ْ0ننیوِ َِْ0 گسِ وُۄھ نرێشی ؤنی نظرےہن ۂنزٔ ۃُونےِ نِ نِ ۃگٔمےِ نِ 
گسِ نۆر فکرِ زِ ےۆھَوے ےِمو منزٔ کگنہ نِ مکمل ۄھَنِ مگر نونِ ۄھُ یرروِن
سُنھ نطرےِ سینھگہ نسلی نۃش طَ </p>

<p>مز ر ۄھُ ےِ زِ ےِم نرێشی وَے نظرےِ ےکٔ وَنٔ کٔرنھ مےلِ گسِ گرنقگۄِ کھری
ھگر مریژھ ہنھ زےگھٔ وگن ووگن 
 
 </p>

<p>ْ0ْ3                             10ُ114ٔ</p>

<p>ْ0ْ3                   زِنھگی وێنُن نٔ نھلُن 
سگٔری زُو ھگر ۄیز ۄھِ نێ زُو ۄیز ونِش کےنژو طریقو مۃنلِف َ
ےۆھوَے گز زُو ھگر ۄیزن نِ مگنٔ ؤنی سینھگہ فرق ۄھَ ی مگر سگری زُو
ھگر ی نِم منھ کگلٔکی نَن ےگ گز کگلٔکی ی ۄھِ وێنھگن ی کھیوگن نٔ کھێنٔ نٔ
وێنھنٔ نگمنھ ہرکن کرگن َ ُےِ ہرکن گٔسی نن صرف زمینِ منز مُول
کیٔنی ےگہوہس منز لنوِ ؤہرگونِ ٔ گمِ علگؤ ۄھِ زُو ھگر نسل کٔشی کرگن ی
ےِم ۄھِ مُورٔ مگٔنھی وێنِھِنھ ی مُونھِنھ نقسیم گژھنٔ سٔنی ی نےۆل نٔ لۄھِ
ننگونٔ سٔنری ی نھُول نرگونٔ سٔنی ی نَنٔ نێےِ کُنِ طریقٔ مگنس ہوی
نۄِ مگٔھٔ کرگن َ </p>

<p>	زُو ھگرن ۄھُ وێنھنس گکھ ۃگص ہھ گیسگن َ سینھگہ سگھٔ نٔ نِکی
زُو ھگر ی مںلن زِ نھٔ مگھُک گکھ ۃۄرھنینی قطرٔ گمێنگ
ۄھُ گٔکِس ۃگص ہھس نگم وێنھِنھ ھۄن ہصن منز نقسیم گژھگن َ نِم زٔ
نَ9ی گمێنگ نِ ۄھِ مننِ وگرِ ےِنھَے مگٔنھی نقسیم گژَھگن َ </p>

<p> </p>

<p>
ْ0ْ3                               11ُ115ٔ
مینھ ٔ وگرےگہ ۃۄرھنینی زُو زگٔژٔ ۄھَ کےنژس کگلس علمی طور زِنھٔ روزگن
نٔ وێنھگن نٔ نمِ منٔ ژھۄکگن نٔ نێے کگر گٔژھنھ مگنس گٔکِس گۄژ منز ننھ
کرگن َ گۄژِ منز ۄھِ نِم مننِ کھۄنٔ لۄکنین گٔننھ رۆس لۄھَن منز نقسیم
گژھگن َ ےِم لۄھٔ ۄھَ موقٔ ےنھ نێنر نےرگن ی ۄھُکرگونٔ ےوگن نٔ منٔ نین وھن
ہنھی مگٔنھی وێنھگن َ مےیۄھٔ زُو زگٔژن منز ۄھُنِ نوُن ےُون سگھٔ َ 
وۄں گَو ےِ سگھٔ نقسیم گژھن ۄھُ کےنژن نیین زو ھگرن منز نِ سمھگن َ 
مگر ھٔۄھِنی لگےق ۄھَ ےِ نھ زِ سگرِنٔے زُو زگٔژن مینھ ۄھُ گکھ قونُون
لگگو ی ےگنے گٔکِس ۃگص ہھس نگم وێنھُن َ مرنٔ نرۄنہ ۄھُ گکِ وقنٔ
ےِہنھ  َوێنھُن ننھ سمھگن َ منُن گٔنیِنھ وێۄھر لٔنِنھ ۄھِ ےِم مَونھگن نی
نۄِ ہیوگن مگٔھٔ کرٔنی َ ےِم نۄِ ۄھِ ےگنٔ مکمل گٔژھنھ زیوگن نَنٔ نھُولن
فگہ ھِنھ َ مگر زُو ھگرن ہنھ سورُے وسم ۄھُنٔ نۄِ مگٔھٔ کرگن ی نٔلی کِ وسمُک
گکھ ۃگص ہِصٔ َ نۄِ مگٔھٔ کٔرنھ ۄھِ زو ھگر وگٔنسِ ےوگن نٔ مرگن ی ےِمن 
ۄھَ گمِۄ گکھ ۃگص ضۆرُورنھ گیسگن َ ےِہنز زِنھگی نٔ وێنھنس ۄھُ گیسگن 
گکھ ۃگص ہھ َ ےِم مزر ےِنھی ہےوگنن منز ۄھِ ی ننھی ۄھِ کلین منز نٔ َ مگر ےِم
مزر ۄھِنٔ نێے زُو ۄیزن منز َ نێ  زُو ۄیز ی مںلن ےلور نِ ۄھُ وێنھگن َ 
مگر ےِنھس وێنھنس  ۄھُنِ کگنہ ژھین َ نێے زُو ۄیز ۄھِنٔ مننِ نشِ ہرکن
کرگن نِ ۄھِ ےِمن مگنس منز کگنْہ منٔنی ہرکن گیسگن َ نلور ہێکن لۄھِ نھین</p>

<p> </p>

<p>
ْ0ْ3                            12ُ116ٔ
ؤری ےن کُنِ ننھیل نغگٔر رُوزِنھَ کگنْہ نِ نێے زُو ۄھُنٔ نسل کٔشی کرگن َ </p>

<p>	زُو ھگرن ہنھ مرن ی وێنھُن نٔ نوُن ۄھُ گسِ کےنژن عٔوین ننیون
کُن نوگن َ گکھنۄِ ےُس سےۆھُے ےگ کےنہ مٔنزم شکلٔ مرگٔوِنھ کگٔنسِ
زُو ھگرس زیوگن ۄھسُ ی سُ ۄھُ منٔ نِس مگٔلِس مگوِ ہوے گیسگن َ مگر
ےِم ۄھِنٔ زگنْہ نِ گیسگن نِل کُل ہوی َ ےِمن منز ۄھَ گیسگن ہمےشِ کگنْہ نَنٔ
کگنہ فرق ی ےنھ گٔسی  گنفرگٔھےن  ونو ط گگر ےِہِ سٔکی گکھ سگس مَنٔ مونمری
نێےِ نٔری ےِ نٔوی گکھ سگس منٔ مونمٔزی مگٔھٔ کرن ی گسِ نگسن نِم نُو نہُو
مرگنین منٔ مونمرین ہویَ مگر ےِمن گیسِ مگنٔ ؤنی مُمُولی فرق ضۆرُور َ
سگنِ نگمنھ  ۄھ ےِ فرق زگنٔنی سینَگہ مُشکل َ مگر گنسگنن منز ےِ فرق
وُۄھِنی ۄھُ سہل َ گٔزکی مہونوی نٔ زنگنٔ ۄھ 1800قنل مسیہ کین
مہونوین نٔ زنگنَن ۂنھی زُری َ مگر گسِ منز گیسِ نٔ گکھ نفرنِ منی مِ
میرِ ہنھِس کگٔنسِ نفرس ہےُو َ ےِ مزر ۄھُٔ گنسگنن نٔ منٔ مونمرین منزٔے
ےونٔی نٔلی کِ ۄھُ کلین کنین منز نِ َ مرینھ کگنْہ زگنھ ۄھَ منٔنی گنفرگٔھےن
مرینھ کُنِ میرِ منز نھلگوگنَ ےِ مزر ےنھ گنسگنن منز ۄھُ ی نِنھُے گوس
نِمن لۄکنین وگن ورن منز نِ ےِم زَن نگٔریۃٔ کین ھۄن گننھگٔیی زمگنن ی
ےگنے گۄیکگل نٔ منھ کگل 
منز نگرٔ نےھگھس منز سمێی ی نوۆێی نٔ مُوھی َ </p>

<p> </p>

<p>ْ0ْ3                          13ُ117ٔ
زُو ھگرن ۂنز مرینھ کگنْہزگنھ ۄھ مسلسل مرگن نٔ نێےِ نرٔ نےھگھس
منز زیوگن َ 
	نێلِ سُونۄو نَو کُنِ زگٔژ ہنز گٔکِس میرِ کےگہ سمھ ێ کےنْہ نفر گیسن
کےنژن ہنھ کھۄنٔ زےگھٔ گٔری نٔ ھٔری ی زےگھٔ مۆنی ورۆنی نٔ زنھگی منز
زےگھٔ کگم ےگن َ کےنْہ گیسن لگغر نٔ کم کگم ےگن َ کُنِ سگنٔ گگن کگنْہ ۃگص
وگقٔ سمھیو ی ہگلن گَے ۃرگن ی سگرِنٔ4 زُو ھگرن مینھ گیسِ گَکُے قونُن
لگگو ےگنے ےِم فرھ وگن مگٔنھی مگہولس منگٔسِن گیسن ی نِم روزن زِنھٔی
وێنھن نٔ نون َ ےِم نٔ مگہولس منگٔسِن گیسن ی نِم گژھن ۃنم َ کمزور
فرھ گیسن ۃۄرگک ہگغصِل کرنس کم قگٔنِل َ نِم ہێکےن نٔ منٔی نین ھشمنن
نرۄننھ کنِ ھٔرنھ ط نٔ نِم نێکننٔ گزرگنٔی کٔرنھ َ ےنھٔ کِنی ۄھِ مرننھ
نٔ نگمنگٔسن ۄھِ ےوگن ہَنگونٔ نٔ گٔری نٔ ھٔری نٔ منگٔسِن فرھ ۄھِ
زِنھٔ ھورزنٔ نگمنھ ژگر نٔ ےوگن َ </p>

<p>	گنھ عملِ ۄھِ  فۄھرنٔ ژگٔر  
ےگ  گٔری سُنھ ھریر 
ونگن َ ھۆےُم نگو روزِ نێہنر َ </p>

<p> </p>

<p>ْ0ْ3                                  14ُ118ٔ
زِ گگر مگہول نھلنٔ  مرینھ کگنْہ زُو زگنھ ۄھَ مےرِمنھ میرِ زےگھٔ کھۄنٔ
زےگھٔ مگہولس موگٔفِق ننگن َ </p>

<p>مگٔنی نَو زِ مگہول نھلیو َ گکھ فرھ ےُس کگم ےگن گوسی گژھ نگکگم 
نٔ سُ فرھ ےُس نگکگم گوس ی ننِ کگم ےگن َ زگٔژ نھلین میر0نھ میرِ نٔ
نِم فرھ ی ےِم مھنگن نٔ مھگنمھلگن گٔسی ی روزَن ھرہم نرہم گژھگن نٔ مرگن َ 
نٔوی وفرھ گژھن زور لنگنی ےۆنگم نَمِ زگٔژ ہنز عگم شکل نِ نھلِ
مںگلِ نگمنھ ہون گکھ گُٔ رنگ ؤۄل ہےوگن ی ےُس شینٔ
علگقن ہنز سۃن سرھی ژگلگن ۄھُ َ گٔمِس ۄھُ سفےھ نٔ گۆن ےےر
گؤ کوِنی زِ شکگرین ےےِ نٔ سہل مگٔنھی نوزنٔ نٔ نیر ہێکِ ژگٔلِنھ َ 
ےِ زگنھ ننگومیرمنھ میر منُن ےےر زےگھٔ کھۄنٔ زےگھٔ گۆنَ ےۆنگم نێےِ
گۆن ننگونُک کگنْہ کگنْہ فگٔےھٔ ےےِ نٔ </p>

<p>	مگنو نِم علگقٔ کِس گین و ہوہس منز ےےِ ننھیلی َ گرمی فےرنٔ شین
گژھ گٔلِنھَ سُ ؤژل ہےوگن زورِ ؤری ےِ کِس ؤری ےس ینٔ ہور کگو
ہےُو نۆن َ نٔمی سُنھ ےےر نَنِ نٔمِس کےُنھ مصینن َ مگر ےس ہےوگنس
ےےر نَنِ ی نٔمِس ےےِ فگٔےھِ َ ےنھٔ مگٔنھی گژھِ سفےھ شین ہےُو ےےرٔ وگوِنی
زگنھ وگرٔ وگرٔ ۃنم نٔ گُرٔ رنگ زگنھ مھھنمھلِ َ وۄں گَو گین و ہوہس
مگ مگر گگر ےِ ننھیلی ےک ھَم گیےِ ی ےِ زگنھ نِ گژھ نگنُوھ طَ گگر ےِ</p>

<p> </p>

<p>ْ0ْ3                            15ُ119ٔ
 ننھیلی گسنٔ گسنٔ ےےِ ی نێلِ گگر نٔمس زگٔژ مشکل نِ مین نُلٔنی ینونِ
ہێکِ سۄ میرِ منھ میر مگہلس سٔنی رٔلِنھ َ گنھ ننھیلی ۄھِ ھُو زگٔژن
منز نرمیم ونگن َ </p>

<p>	شگےھ ۄھَنٔ ےژھ ننھیلی ننھ سگٔری سٔے علگقس منز ےوگن ی ےێنِ
سۄ وژل زگنھ روزگن گیسِ َ نٔلی کِ ےِ ہێکِ ےنھ کُنِ نٔیس ی منھرس
منز گٔکِس گٔنھس َ ےِ ہێکِ ےنھٔ کُنِ نگلس مینھ ےگ ہێکِ نقسیم گٔژھنھی
نٔ کُنِ گٔکِس گَنھس ےےِ نٔ َ گلف سٔنریم ہِش گکھ گکھ نہری رَو
ہێکِ رۄۃ نھلگٔوِنھ نٔ کرِ سرہھُک گکھ طرف گرم نٔ نێےِس طرفس
نھگوِ سرھ َ سرھ طرفس کُن کرےِ زگنھ زےگھٔ زےگھٔ نٔ سفےھ وَنھ
مگٔھٔ کرنٔ کُوشس طَ مگر نێےِس طرفس کُن کرےہے زگنھ نٔنی نٔ 
گُرٔونھ مگٔھٔ کرنۄ کُوشش َ </p>

<p>	ؤژ نٔ رنگٔ ۄِ ننھیلی سٔنی سٔنی روزن نێےِ نِ کےنہ ننھیلےِ
ےوگن َ شگےھ ےےِ منون منز نِ فرق َ نِ ےگزِ گمِ زگٔی َ وےس ہِصٔ
شینس منز روزگن گوس ی سُ گیسِ ہے منزی مٔنزی ۃۄرگک ژێھگنی 
ۃگٔطرٔ شین ہُھگن َ ےێلِ زن گَمی زگٔژ ہنھ نێگکھ ہصٔ گُرٔ مےژ نینھ گور
ےور فےرگن گوس ی گین و ہوہۄِ گمِ نگرٔ ننھیلی سٔنی ےےِ شگےھ مےّسر ۃۄرگکس
منز نِ ننھیلی نٔ ۃۄرگکٔۄ ننھیلی گنِ وَنھن نٔ ہگٔضمٔ کِس نظگمس منز َ </p>

<p> </p>

<p>ْ0ْ3                           16ُ121ٔ
وگنھ سمھگن َ نھلےِ گیسٔ ےِوگن نٔ زُو زگٔژ گیسٔ نگرٔ نےھگھس منز 
نُوھ سمھگن َ شگےھ گیسِ ہے زِنھگی نِمن ھۄہن نژٔھے وُژھ نٔ ژھۆن 
ےنھی ھۄھٔ نٔ وری ۄھِ َ قۄھرنٔ ژگٔر نێلِ کُنِ میرِ مۄکلگوگن گیسِ ہے
نو میر گٔس نَمِۄ وگے ےک ھَم رنگن َ </p>

<p>	قۄھرنٔ زگٔژ ۄھَنٔ نیژ وُژھ گنسگنن گژھ ی ےیژ نێےِ کُنِ زگٔژ
گٔژھ ۄھَ ی مغرنی مُور ےکس گٔکِس ۓسی کینس ۄھِ لگگن وُہ ؤریی
گکھنُے زےگھٔی مُورٔ وێنھنس نٔ نسل کٔشی کرنس نگم َ مگر وگرِ ےِہن
زُو ھگرن ہنز نوِ مننےِ ۄھ لگگن نگلیغ سمھٔنس گَکُے ؤری ےگ نمِ کھِنٔ
کم َ گۄیٔ کگلٔ کین سمنھرن منز ےِم ھگھٔ نٔ نِکی زُو ھگر گٔسی ی نِمن کےُنھ 
گوس وێنھُن نٔ نسٔل کُشی کرٔنی گگٔننن ےگ کےنژن مِننن ہنز کگم َ 
زِھگی منز گیسِ ہے ژَنھ وگنھ وُژھ فرق سێنھگہ وُژی مِ ےوگن َ 
زِنھگی گٔس مُورٔ مگٔنھی قنے مٔژ نٔ سینَگہ زگٔژ گیسٔ ہن وۄمھ ےمٔژ
ےێلِ زِنھگی ہنھیمۆن نِشگنٔ ۂنھی مۆن نِشگنٔ ہێننو ۄنگنن منز مہفُوظ روزٔنی </p>

<p></p>

<p>                      ======================
?EOF</p>

<p></p>

<p></p>

<p></p>

<p></p>

<p></p>

<p>               0 
</p></body></text></cesDoc>