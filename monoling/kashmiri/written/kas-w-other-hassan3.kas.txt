<cesDoc id="kas-w-other-hassan3.kas" lang="kas">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kas-w-other-hassan3.kas.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Hakim aila</h.title>
<h.author>Nikolie V Gogol</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - ****</publisher>
<pubDate>****</pubDate>
</imprint>
<idno type="CIIL code">hassan3.kas</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page unknown.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-24</date></creation>
<langUsage>Kashmiri</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>                        ْ0نرْیٔیم گنگ
						گۄینیُک مگٔنھٔر
	ُمنظَر ۄُھ سُےَ ھۆیمِ ھۄۂ صُنہُک َوکھَ ووی گَفسَر ۃیرِ عگمی موسن مگسنری 
	سۆکُول گنسمکنر نٔ یون ۄِنسکی نٔ نون ۄِنسکی ۄِھ ہُشیگٔری سگن نٔ یِ ھٔمی زِ 
	نٔ کھۄرٔ نێنڑین مینھ مَکگن گَژگنَ سگٔری ۄھِ ورھی لگٔگِنھَ نِہنْزَو ہرکنَو 
	سٔنی ۄُھ نگسگن زَن ۄھَ نِمَن مۄشگکٔۄ نُمگٔیِش کَرٔنی یۆن نگم لینگ کوف گَژگن 
	ۄُھ نۆن نگم ۄُھ مگٔنھٔر لۄنِ سَمھگنٔ
وو ِ ُ سگرِ نٔے ۄُھ گٔیِس ژئ کرَس مَنْز وۄھنِ نَھوگنٔ ھوسنوی َوکھ ضگیِ کرنٔ رۆسنُےی 
	ۃوھگیِ سٔنھ ۃگٔطٔر رُوزی نَو گیرٔ کونڑلِس مَنْز نٔ ننظیمٔ سگن ۃۄھگے کری 
	نَس یگٔریَ ھَرنرٔ سے مَنْز گٔژ ۄھَس نٔ وگےی نٔلی کِ ۄُھ مریوی کونسلَس نٔ 
	ھٔنِ رگوگن  سیۆھ رُوزِو 1 ینھ مگٔنھی فووِکی سِمگہ روزگن ۄِھَ ۄھگٔنی کٔڑِنھ 
	نگرعن  مینر گیوگنووِۄ ی ژٔ یِ یێمِ طرفَٔ نٔ ژٔ گَژھ نمِ طرفٔ مٔنر 
	گیوگنووِۄی نھیک مگٔنھی روز گِسنگھٔ ُ یون ۄِنسکی نٔ نون ۄِنسکی ۄھِ ہُمور 
	یمور گژھگن ٔ
گفسرِ ۃیرِعگمِ فیوھورووِۄی ینھٔ مگٔنھی َونَکھ نِنھٔ مگٔنھی کَروی مگَر گَسِ مَزِ مٔزی 
	مگٔنھی کۄشِش کَرٔنی َ
وو ِ کێگ کُوشِش ێ
گفسر ۃیرعگم ِ نہرہگل نۄہِ ۄُھ منگہ کیْگ کُوشِشَ
وو ِ ۄون مَطلَن ۄھگ  کمرَس مٔنی کِنی گَنھٔ نِوگن نٔ گشگرٔ ہگوگنی یێمیُک 
	مطلَن رشؤ ھێُن  ۄُھَ
گفسر ۃیرعگم ِ نِلکُل نیَ کێگزِ نٔ ێ
وو ِ یِ ۄَھ ۃطرنگک کَنھَ یِ نُلِ ؤ شورِ مہشَری نۄہِ ۄُھنگ منگہ یِ ۄُھ نۆی 
	سَرکگٔری ہگٔکِمی ہگں وۄنی گَو گگر گٔسی سگرِ نٔے نڑین مۆہنِوین نِشِ ۄَنھٔ 
	سۆمنرگٔوِنھ زِ یِ ۄُھ سگنِ مۄہننُک نہفٔ نێلِ گو وگن َ
موسن مگسنر ِ یگ ونوسَ  یمِ ۄھَ یگک ۃگٔنٔ ذٔری یِ رۄمیِ وگژٔ مژٔی مگَر کَسٔنھِ 
	نگؤ ۄھََ نِ ۄُھنٔ منگہ  
گفسر ۃیرِعگم ِ ۃنرھگر ۄھُےَ یُنھ نٔ ژێُیی یگکٔ نھیلَس نٔرِنھ میرۄں مگر کَری نٔ گَنھِ 
	یکھ نَٔ مێون مطلَن ۄُھی گٔکِس نگضگٔنطِ منظم ریگسنَس مَنْز ۄِھنٔ ٔنھٔ 
	مگٔنھی کھَنڑٔ وگو طرٔیقَس مینَ کگٔم کرَگنَ گٔسی کێگزِ گٔیِ سگٔرٔ ومگنھ وۄھنِ 
	رُوزِنھ ہِش َ گَسِ مَزِ وگرِ وگرِ گژُن گومِس نِشِ نٔ رُونرُو سَمھِنھی یێمِس یِ ۃۄش 
	کَرِ نٔ کَرُنَ یُنھ زَن نٔ کگٔنْسِ نِ کَنس کَن ۃنَر لگِ َ یہۆے ۄُھ طرٔیقٔ یُس 
	گٔکِس مُنَظم سمگوَس مَنْز َورنگونٔ یوگن ۄُھَ لہگزگ فیوھورووِۄی نۆہی گٔژی نَوگۄیَٔ
وو ِ وگن گو گگَر نۆہی گٔژِو گۄیٔی نِکێگزِ نُہنھ سٔے ہسمَنگلَس مَنْز کھیو گۄیٔ یینی 
	نٔڑی مێمگنن کھگنَٔ
گفسر ۃیرعگم ِ مێ نگسگن زێگھٔ وگن گیُوو گگر سۆکول گنسکمنر گَژِ ہے َ نِکێگزِ یِ ۄُھ سُ 
	شۃٔص یُس نوووگنَن مَنْز نیھگٔری مگٔھٔ کرگن ۄُھَ
سۆکُول گنسمکنر ِ نٔ ہیکٔ نِ وِنگنَوی مێگنِ ھٔسی ییِ نٔ یَِ مۆز گَے نُوزِوَ نٔ ۄُھس 
	نمِ طرٔیقِ ملیومُن زِ گگَر کگنْہ شۃصَ سُ گٔسی نَن گکۆے موو عۄہھَس مَنْز مێ 
	نھۆھ مێ سٔنی کنھ کَرِی مێ ۄُھ ھِل ہنِ مگوِ ؤسِنھ گژھگن نٔ زَن ۄُھم گگرُ 
	زیو ہینھ ژلگنَ ھوسنو مێ ھِیوِ مگٔفیی مێ نۃَشوِ  
گفسر ۃیرعگم ِ وو صگٔنی نێلِ وگٔژ گنوگٔری نۄہی مینھی وۄں کُس ۄُھ نۄہِ نغگٔرێ نێیِ 
	ۄُھ نۄہِ ۃۄھگین نول ۄگلِ ہُنھ سُ گنھَگز نۃٔشمُن زَن ۄھُ مگنٔ سِسرو نہنْز زیۆِ 
	نَقریر کَرگنَ
وو ِ نھوِو نھوِو وۄنیَ گۆژھُس نگ سِسرو سُنھ ہیُوہ نقریر کَرُن نێیِ مگ ۄُھ کینْہ 
	ونُن ێ یِ ۄِھؤ نۆہی گمِ کِنی ونگن زِ مٔنْزی گگر مێ کُنِ وِزِ مگلنُو ہونین یگ 
	شکگٔری ہونین مینھ نہَں کَرنَس مَنْز ھِلۄسمی ہگٔو َ
سگٔری ِ نَس گٔنھی گٔنھی سمَگن ٔ نٔ َ ہُونی گٔنی ۄِھنٔ یمَن مُنلِق نۆہی نُولِنھ 
	ہێکِوَ نۆہی ہێکِو نگنل کِس نگَورس مُنلِق نِ نقریر کٔرِنھ وو صگٔن  
	ونگن فیوھورووِۄی گَسِ منٔ کٔری نَو مَنْز ونِ گیۄھِ گَسِ رٔۄھی نَوَ گَسِ نھٔوی 
	نَو مِھرگنٔ شفقنُک گَنھ مینَ وو صگٔن َ گَسِ کٔری نوَ رہنمگٔیی
وو ِ مێ نرْگٔوِو نگ یلٔ ھوسنوَ نلٔ سگٔ کٔرِو مێہرنگٔنی َ
	ُگمی وقنٔ ۄَھ لینگکوفٔ سٔنھِ کمرٔ مَنْز ژگس کَرنٔۄ نٔ مکنٔۄ گیوگز یوگنَ
	 سگٔری ۄِھ زَن ژێننھ کٔرِنھ ھَروگزَس کُن گژھگن نٔ ھکٔ ھَک کٔرِنھ نیرنٔۄ 
	کُوشِش کرَگنَ ژیلٔ ژرْیوِ مَنْز ۄِھ نِم گَکھ گٔکِس لنٔ مۄنوِ کَرگنی نٔ لۄنِ لۄنِ 
	ۄھَکھ کرْکٔ ژَلگن نیرِنھ ٔ
نون ۄِنسکی سٔنْز گیوگز ِ وگٔیی یون ۄِنسکی یگی کھۄرہگ مُھنروونھم 
	گفسر ۃیرعگم سٔنْز گیوگز ِ نٔ ہگ مُوھُس یگرٔی ھم ہگ گومی گَکھ شگہ ھی نَومُو
 کھگرنٔ مَرنٔ نرْونْہ 
			 ُیم سگٔری ۄِھ ژِ ہِس کھنڑھس  ؤہی نٔ وگے  کرگنَ ۄھیکرَس 
	ۄِھ سگٔری نیرگنی سنیو ۄُھ ۃگٔلی گژھگن ٔ
لینگ کوف ِ ُکُنُے زۆن َ گٔۄھَن ۄھَس نێنھرِ سینی مُھکڑٔ کَھنی مٔنیٔ مێ کُر شگیھ ھَن 
	کے نێنْھٔرَ یمَو کنِ سَنگ ینھی نرٔم گسیلٔ نٔ لیفٔ نٔ نیرِ شگنھٔ گئنھی گٔنی 
مٔنی َێ وُشنیر سٔنی ۄِھم وُسوگٔسی نھی مٔنیَ نگسگن ۄُھم نرْیشِ نَھلٔ گوس ہَم 
	کێگہ نگنی زێگھَے سَۃ مگہَن ھێنمُنَ کلٔ ۄُھم وُنِ نژگن  مێ ۄُھ نگسگنی 
	گِنسگن ہێکِ یێنِ وگن مگٔنھی رگہن وگہن کٔرِنھَ مێ ۄھِ فیگض نٔ مٔژھی لگزَے 
	کرَن وگٔلی لُکھ نڑٔ ۃۄش کرَگنَ نٔ ۄُھس زیگھے ۃۄش گژھگن یێلِ مێ کگنْہ 
	ۃَوھغرضی ورگٔی نٔ مَزِ ھِلٔ مگذرنھ کَرِ  نی نێیِ ۄَھنٔ مێیر سٔنْز کۄر 
	نِ شکلِ نگکگرٔ کینْہَ نٔ نسٔنْز مگٔو نِ ۄَھ نَڑٔ نورُنٔ کگر ۃُونصُورنھ زَنگنٔ 
	نگسگنَ سینگرٔ ۄَن نگرَن ۄُھ مِضرگن لگگٔ نُکُے گون ضۆرُورَنھ نٔ سُ َونِ
	مٔزی مگٔنھی یۆھَوے مێ وُنِ فِکرِ ۄُھنی نَرگنی مگَر مێ ۄَھ یێمِ قٔسمٔۄ زنھگی 
	سینھگہ مَسنھ َ
وو ِ ُ گَژگن نٔ رونٔ گژھگن َ مگنَس سٔنی وَنگن ٔ نَڑِ ۃۄھیِ مێگنِ  وۄنی گٔنی زیم 
	گونرۄے مێ ۄھَے زنگِ نَنگین َُ منُن مگن زورٔ گِسنگھٔ نھونھ نٔ کھۆہورِ طرفٔ 
	گلونْھ نَلوگرِ شیرگن شیرگن نَڑِ ہَنِٔ وِنگنی نسلیمگنِ نٔ ۄُھس لیگمکِنی نِیگمِکنی 
	یسڑکن وو نٔ کگلیوُک مُشیر 
لینگ کوف ِ نھٔوِو نشریف َ گَۄھگ نٔ نۆہی ۄِھؤ یێنِ وو ێ
وو ِ نٔ گوس ہَس ینِ کیو معزز لُوکَو نرێین وُری ین ۂنھ ۃگٔطرٔ وو مُننۃن کۆرمُنی 
	وگریگہ کگل گوی نٔ ننٔ وونُم نرگنَر گَنھ عۄہھَس مینھ ۃٔھمن کرَگن َ
لینگ کوف ِ یۄہِ ۄھگ وو گیسُن ی مێون مطلَن ۄُھی یِ عۄہھٔ ۄھگ نۄہِ فگٔیھٔ مَنھ 
	نگسگنێ 
وو ِ ونگنی نََون وُری یَن گنھ عۄہھَس مینھ کگٔم کَرنَس ھورگن گیو مێ سینن ولگھ یمیر 
سُنھ نمغٔ ھروِ ۄہگرُم ھِنھ یزَنھ نٔڑ رگونٔی ہۆکُومنَن گٔس مێگٔنی کگٔم وُۄِھنھ نمغٔ 
	ھِنٔۄ سِفگمرِش کٔر مٔژَ ُ مگنَس سٔنی ٔ ہگرٔ ۄھَے مێ مۄۄھِ مَنْزَ نٔ نگرٔ 
	رێہ زَن ۄھَم مۄنھ مھگنٔ وَنس نیگرَ ُ سُ ۄُھ منُن گنھٔ نرْونْہ مگہَن مَکنگوگنٔ
لینگکوف ِ مێ ۄُھ سینن ولگھیمیر سُنھ نَمغٔ نَڑٔ مَسنھَ گَنھ مُقگنلٔ ۄُھنٔ نمغٔ سینن 
	گینگ نیُون کینْہ نِ َ
وو ِ ُ منُن گَنھٔ وگرٔ وگرٔ نرْونْہ مَکنگٔوِنھ مگنَس سٔنی ٔ نَڑِ ۃۄھگیِ مٔگنِی مێ ۄُھے 
	نٔ منگہ زِ مێ ۄَھگ کھۄرَن نَل مێژ کِنٔ نَٔ نگسگن ئھُم زَن ۄِھم نلٔ مھین شٔرنِ 
	نُھکگنَ
لینگ کوف ِ نۄہِ کێگ ۄُھ یِ مَوۄھِ مَنْز ێ
وو ِ ُ مغٔز زَن ۄِھس نگۃنٔ گژھگن نٔ نون ۄُھ گَنھ مَنْز نرْگوگنٔ کِہنی نٔ ونگن 
	کِہنی نٔ 
لینگ کوف ِ نِ کێگ گو کِہنی نٔێ مٔھنرِس مینھ مێیِ نگ یم نون ێ
وو ِ ُ کھۄر نیۄنھٔ مینھٔ نگلِ نگم ۄِھس نٔن کٔنڑ مَھنگن ٔ نٔ ونگنی نِلکُل نٔ ُمگنَس 
	سٔنی ٔ مێگنِ ۃۄھگیِی زن ۄُھس نٔ ژورٔ سٔنھی مگٔنھی ووَس نرْونْہ کنِ  
زَن نرنَم وُنی گگیِ نی نێہ گَنٔ نَم سگیٔنیریگ کِس زِنھگنَس مَۆْز 
لینگ کوف ِ ُنون نُلگن ٔ ونگن کێگ ۄِھؤ نۆہی ٔمٔ گے ہگرٔ ۄھَی یم ۄِھنگ نونێ
وو ِ ُ مگنَس سٔنی ٔ مگملٔ وون گَنھ نٔ مُوھُس  نٔ گوس ۃنم
لینگ کوف ِ یۆھوَے نێہی یِ مێ وۄزُم ھِیوێ  کێگ ۄِھؤ َونگن ێ 
وو ُِ وَل وَلٔ نے شکی ونگن نے شک نۆہی نَھوویم  مێ سَمھِ گَمِ سٔنی گِننہگٔیی 
	ۃُوشی1 ُ مگنَس سٔنی ٔ مگنگ ی رژَھگ کر وِرنھ مگٔھٔ ۃۄھگ صگٔنگ  وو کر نَم 
	مۄکلَن مگے  
لینگ کوف ِ مێ گٔیِ سگرے ہگرٔ سفرَس مَنْز ۃرٔۄی گر گٔکِس ۄیزَس مینھ گرگ نێیِس 
	ۄیزَس مینھَ ۃگٔر ینھُے نٔ گَرٔ وگنٔ نِنُھے سوزَے وگمَس َ
وو ِ نۆہی کێگ ۄِھؤ یِ َونگنێ ٔژھ کنھٔے مِ سُونۄِو نۆہیَ نۄہِ منْوِ ؤ ہگرٔ نٔ 
	مێ نۃٔشُؤ یزنھ  ننٔ کنِ گَنٔ نٔ ھَسَ وۄں گو ونگنی مێ ۄُھ کگم کَرنُک 
	شوق نِ منین ہگٔکِمَن ہُنھ ۄُھس وفگھگرَ نٔ کَرٔ کُوشِش زِ یزَنھ مرْگونُک 
	گۆژُھس نٔ نِ ہقھگر نَنُن  ُ نھۆھ وۄنھگن نٔ سێوھ گِسنگھٔ روزگن سِمگہ 
	سٔنھی مگنھی ٔ نٔ ۄُھس نٔ یژھگن زیگھٔ کگلَس یێنِ نِہنھ نۆہِ ہَروِ کرُنَ 
	مێ مُنلِق مگ ۄُھ کێنْہ ہھگین ێ 
لینگ کوف ِ کێگہ ہھگین ێ
وو ِ مَطلَن ۄُھ نۆہی کئرِو نگ یسڑکن عھگلٔژ ۃگٔطٔری کہنْئے ہھگین وگٔری ێ
لینگ کوف ِ کمِ نگمنێ کێگزِێ مێ ۄُھنٔ وُنِکس گَنھ سٔنی نِلکُل کگنْہ نولُقَ کٔہنٔے 
	ہھگین ۄُھنٔی نِلکُل نَٔ شُکریِی سینھگہ شُکریِ 
وو ِ ُ کلٔ نۆمرگن نٔ نیرگن نیرگن مگنَس سٔنی َونگنٔ قصنٔ کۆر گَسِ فنہَ قصنٔ ۄُھ 
	وۄنی سون 
لینگ کوف ِ ُ مگنَس سٔنیٔ نَڑٔ وگن مۆہُونُو ۄُھ یِ وو َ
موسن مگسنر ِ ُ یُنھے گَژگن ۄُھ مگنَس کھوہورِ مگسٔ گلونھ نلوگرِ ۄُھ نَھمھ ژٔھنِنھ 
	سِمگہ ہیُو گِسنگھٔ روزگن َٔ ونگنی ننھہ ۄُھ نسلیمگن عرٔض کرَگنَ نٔ ۄُھس 
	شمکینَ موسن مگسنر نٔ کورن کونْسلرَ
لینگ کوف ِ گٔژِو گٔژِو نشریف نھٔوِو  مێ ۄَھ رٔنین مۆہنوِین سٔنی وۄنھ نینھ ۃۄش 
	کرَگن  نۆہی ۄِھؤ ہر ہمیشِ یێنھی قَصنَس مَنْز روزگن گیمٔنیَ کِنِ نٔ ێ
موسن مگسنر ِ وِنگن ی نِلکُل نہُنھ َونُن ۄُھ نھیک َ
لینگ کوف ِ مێ کۆر نہُنھ یِ لۄکُن قصنٔ نَڑٔ مَسنھَ گینگٔھی نِ ۄھَس نٔ نیژ زیگھٔ 
	کینْہَ مگَر نمێُک کێگ ۄُھێ نِ کٔرِنھ یِ مگ ۄَھ رگزھگٔنیَ کِنٔ نٔ ێ
موسن مگسنر ِ ونگن نِلکُل صہی ۄُھَ
لینگ کوف ِ نِکێگزِ نھھین ۃگنٔ وگھَن ہنھی لُکھ ۄھِ صرف رگزھگنِ مَنْزے گون گیسگنَ ننِ 
	مگ ۄھِ گگمٔ گرْیسی روزگنَ کِنٔ نٔ ێ نُہنْز کێگ رگے ۄھَ ێ 
موسن مگسنر ِ نُہنھ فرمگوُن ۄُھ صہی ُ مگنَس سٔنی ٔ نگسگن ۄُھمی نکنُر ۄھُس نٔ 
	گَکھ رَژھ نَِ مرْنھ کُنِ مگمَلس مُنلِق ۄُھ کنھٔ کرَگنَ
لینگ کوف ِ مگَر یِ کَنھ مێیِ مگنٔنی زِ گٔکِس لۄکِنس قَصنَس مَنْز نِ ۄُھ ۃۄش نٔ 
	ۃۄشہگل مگٔنھی روزُن مُمکِن َ
موسن مگسنر ِ نِلکُل ھُرْس ۄُھی وِنگن
لینگ کوف ِ گِنسگنَس کێگہ ۄُھ ضۆرورَنھ گیسگنَ مێگنِ ۃیگلٔ  یزَنھ نٔ مۄہنَنھ 
	کِنٔ نٔ ێ
موسن مگسنر ِ نِلکُل نھیک ونگن 
لینگ کوف ِ نٔ ۄھِس نَڑٔ ۃۄشَ مێگٔنی نٔ نُہنھ ۃیگلگن ۄھِ رلگنَ لُکھ ۄھِ مێ 
	َونگن نٔ ۄُھس عوینٔےی گٔکِس ینوِ مینھ ۄھُس نٔ روزگنَ مگرْ نَنھ کێگہ کرٔ زِی 
	سُ گو عگھنَ ُمگنَس سٔنیٔ ہگلگنکِ موسن مگسنر سٔنْزَن گٔۄھَن سٔنی ۄھَس 
	گٔۄھ رٔلِنھ ٔ ۄُھنگ صلگہ یێمِس موسن مگسنرَس نِ ہیمو کم قٔلیلگ وۄزُمَ ُنَڑٔ 
	ہنِ عوین نمگشگ سمُھ مێ َ یور وگنگن وگنگن مۄکلے یِ مێ سگرے ہگرٔ َ یِ ۄھگ 
	مُمکِن نُہنھِ نگمَن زِ مێ ھِیوِرُونلَس نرْێ ہَنھ وۄزٔمی ێ
موسن مگسنر ِ وِنگن کێگزِ نٔ ێ مێ سَمھِ نَڑِ ۃۄشی َ رٔنِو وِنگنی نٔ ۄُھس مرْنھ َوکھنٔ 
	ہگٔضِری مَزِ ھِلٔ ۃَھمَن کَرنٔ نگمَن َ
لینگ کوف ِ نٔ ۄھُس نہُنھ شُکر گُزگر َ مَزر گو یِی زِ سفرَس مَنْز منٔ نین ۃگٔہشَن 
	ہۆن ۄیرُن ۄُھ مێ سینھگہ کھَرگن َ نٔ نِ کٔرِنھ کێگزِ کَرٔ نٔ یژھ ہرکَن ێ 
	کِنٔ نٔ ێ
موسن مگسنر ِ َ مۆز ۄُھ ونگن َُ نھۆھ ۄُھ وۄنھگن َ سێوھ گِسنگھٔ روزگن نٔ نلوگرِ 
	مینھ گَنھ نھَوگن ٔ ونگنی نٔ ۄُھس نٔ زێگھٔ کگلَس نہُنھ مۄلل َوکھ ضگیِ کَرُن 
	یژھگن  نۄہِ ۄھگ یگک ۃگنٔ کِس گِننظگمَس مَنْز کگنْہ کٔمی نیشی نگسگن ێ کگنْہ 
	نگکگرٔگی مگ نگسے یؤہ ێ
لینگ کوف ِ نِلکُل نٔ َ ُ موسن مگسنر ۄُھ نِلکُل کلٔ نۆمرِنھ نیرگن نٔ لینگ کوف مگنَس 
	سٔنی َونگن ٔ موسن مگسنر صگٔنی نگسگن ۄُھم نۆہی نِ ۄِھؤ نَڑٔ نٔفٔس گِنسگنَ 
	کِہنی نے نٔ مێ نھووُنھ گێہسگن َ مێ ۄِھ ینھی گِنسگن نَڑٔ مَسنھ َ
گیوگز ِ ُ ھُورِ مینھٔی مَگر صگف کنَن گژھگن ٔ ژٔ کێگزِ ۄُھکھ یُون کھوژگن ێ 
سۆکۄل گنسکمنر ِ ُ مگنٔ گَژگن کینْہ ۄُھنٔی مَگر ھَروگزٔ مٔنی کِنی ۄِھس گنھَر کُن ھَکٔ 
	ھِوگن َ سُ ۄُھ سێوھ گِسنگھٔ روزگن َ مَگر زَنگَن ۄھَس نٔ نٔنِنی ژلگن َ کھورِ 
	طرفٔ گلونْھ نلوگرِ مینھ گَنھ نھوگن ٔ ونگنی نٔ ۄُھس لوموف َ سۆکُول گنسکمنر نٔ 
	گعزگٔزی کونْسلَر َ
لینگ کوف ِ ؤلِوی ؤلِوی نِہِوی نھُوِو نشریف  ۄێیوِ سِگگر  ُ سِگگر ۄُس میش کَرگن َٔ
سۆکُول گنسکمنر ِ ُ ہِرگسٔ کٔرِنھ مگنس کُن ٔ مێگنٔ ۃۄھگیِ وۄں کێگہ کَرٔ ێ رَنگہ کِنٔ نٔێ 
لینگ کوف ِ رٔنِو رٔنِو  یِ ۄُھ نڑٔ وگن سِگگر َ یِ ۄُھنٔ سُ میل یُس مٔنرز نرگٔ عگم 
	میلگن ۄُھ َ یێلِ نٔ مینرز نرگٔ گیسگن ۄُھس منْژہَن رُونلَن ہَنھ سِگگر مٔلی 
	ہیوگنی مزٔ ھگر سِگگر ۄھِی ۄینھ ۄَھ گۆنگوِ نینڑین نگم ۃۄشنۄ روزگن َ رٔنِو 
	زگٔلِو َ
 				ُ گنھک یٔنی ۄُھس مِلٔ نگوگن َ سۆکُول گنسمکنر ۄُھ سِگگر زگلنٔۄ 
	کُوشِش کرَگنی ہێرِ نۆنٔ ۄَھس نَنھ نَنھ ٔ گۆہو غلط طرفٔ مٔ زگٔلِو 
سۆکُول گنسکمنر ِ ُ ۃوفٔ سٔنی ۄِھس سِگگر ؤسی میوگنَ ژگس کٔرِنھ ۄُھ مگنَس 
	سٔنی ونگن ٔ نرْنھ مێینی ھگٔری سٔے َ گمِ نٔنی نِ کۆرنَس نُرنگھ 
لینگ کوف ِ نگسگن ۄُھم نۄہِ ۄِھنٔ سگگر زیگھٔ مَسنھ َ مگر مێ ۄِھ نڑٔ ۃۄش کَرگن َ 
	مێ ۄَھ گَمِۄ کمزُوری َ گیی نٔیِ زنگنَن ہنْز َ نَنھ ۄُھس نٔ ہیکگن  مَطلَن 
	ۄُھ نِہنز ۃَونصُورنی نرْونْہ کَنِ ۄُھس ۂنھی یگیر نرْگوگن َ نۆہِ نِ ۄھگ یِ 
	کمزُوری ێ نۄہِ کمِ گَنہگرٔ ۄِ زنگنٔ ۄھَ مَسنھ ێ سفیھ ژمِ ہنْز گی کِنٔ گێشگمِ ێ 
	ُسۆکُول گنسکمنر ۄُھ رگوگن ہیُوی نَس ۄُھنٔ کِہنِی َونُن نَگگن ٔ ژھۄمٔ کێگزِ 
	کٔرؤَ ؤنِو سفیھ کِنٔ گێشگمِ ێ
سۆکُول گنسمکنر ِ مێ ۄُھنٔ مَنٔنی رگے ونٔ نُک وُرنھٔے َ
لینگ کوف ِ ؤلِو وۄنی َ ہِنھ مٔ ہێیو َ نٔ ۄُھس نہُنھ مَسنھ یژھگن زگنُن َ
سۆکُول گنسمکنر ِ نێلِ ۄُھ مێگنِ ۃیگلٔ  ُ مگنَس سٔنی ٔ منگہ ئُھم نٔ کێگہ ہێونُم 
	وورُن 
لینگ کوف ِ گَۄھگ نٔ گو نۆہی ؤنِو نٔ َ مێ ۄُھ منگہ نۄہِ ۄُھ گکِ ۃُونصُورنھ گێشگمِ 
زنگنِ ھِل نیُومُن 0 مۆز ؤنِو َ ۄُھنگ ێ ُ سۆکُول گنسمکنرَس ۄَھ گلِ زیو گژھگن ٔ وُۄھو 
	وُۄِھوی نۄہِ کِنھٔ مگٔنھی کھۆن نُنھِس وۄشلُن  نۆہی کێگزِ ۄِھؤ نٔ کَنھ کَرگنێ 
سۆکُول گنسمکنر ِ مێ ۄَھ شرم یوگن َ یُر گیکسی نونل  گیکسی  لیننی 
	لینسی  ُ مگنَس سٔنی ٔ زێوِہ کۆر نُم گفسوسی کٔننھ ژھُن نَس 
لینگ کوف ِ نۄہِ ۄَھگ شرٔم گژھگن مۆز ونٔ نَس ێ مۆز ۄُھ مێگنین گٔۄھَن مَنْز کێگہنگم 
	گَںر یُس نێین مَنْز شرٔم مگٔھٔ کَرگن ۄُھ  یُس نییس ویسٔ رگوگن ۄُھ َ مێ 
	ۄُھ منگہی سۄ ۄھَنٔ کگنْہ زَنگنٔ یۄسٔ مێگنِ نَظرِ نِشِ مَنُن ھگمگنٔ نۄگٔوِنھ ہێکِی کنٔ 
	ہێکِ ێ
سۆکُول گنسمکنر ِ وِنگنی نِلکُل نٔ َ
لینگ کوف ِ نہَر ہگل یِ کَنھ نھؤی نو گٔنیی َونُن ۄُھمی مێ سٔنی سَمُھ عوین نمگشگی 
	یۆن وگنگن وگنگن مۄکلیوو مێ سورُے مونْسٔ َ نۆہی ہێکی ؤ مێ رُونلَس نرْے ہَنھ 
	وۄزُم ھِنھ ێ
سۆکُول گنسمکنر ِ ُ ۄنھَس گَنھٔ ژٔھنگن مگنَس سٔنی ٔ گگَر نٔ مێ ۄنھَس مونْسٔ گوسی 
	نێلِ کێگ نَنِ مێ ێ مگر ۄَھم  ۄُھم  نون ۄُھ ۄنھٔ مَنْز کڑگن نٔ لینگ کوفَس 
	کھُوژی کھُوژی نٔ نَنگن نَنگن میش کَرگن ٔ
لینگ کوف ِ نٔ ۄُھس سینھٔ ۂے شُکر گزگر َ
سۆکُول گنسمکنر ِ ُ نھۆھ وۄنھگن نٔ نلوگرِ مینھ گَنھ نھٔوِنھ ٔ نٔ ۄُھس نٔ زیگھٔ کگلَس 
	نۄہِ ہروِ کرنُک وُرنھ کَرگن َ
لینگ کوف ِ گیھگن 
سۆکُول گنسمکنر ِ ُ ول ول نیرگن نیرگن مگنَس سٔنی َونگن ٔ ۃۄھگیِ گَز وُۄھمَے ۄون 
	نوَر نگسگن ۄُھم شگیھ کَرِنٔ مھرَسن مگٔینٔ َ
گفسرِ ۃیرِ عگم ِ گَژگن گَژگنٔے سِمگہ سٔنھی مگنھی وۄھنِ روزگن نٔ نلوگرِ نَھمھ کَرگن ٔ 
	ونگنی نسلیمگنی نٔ ۄُھس گفسرِ ۃیرِعگمی نگو ۄُھم زیملیگ نِکگَ
لینگ کوف ِ مِزگو نۃیر َ نھٔوِو نَشریف َ
گفسرِ ۃیرِعگم ِ ونگن مێ ۄُھ فۃر مێ کێر ۃگٔرگنٔ گھَگرن ہِنھس مگٔٔنَس مینھ یۄہِ 
	گسنیقنگل نٔ ہگٔو مَؤہ یہنْز کگرکرھگی َ نۄہِ ۄُھ منگہ نٔ ۄُھس یمَن گھَگرن ہُنھ 
	سرنرگہ َ
لینگ کوف ِ گیی یگھ ۄُھم َ نۄہِ ھێُنؤہ مێ شگنھگر لنْۄ َ
گفسرِ ۃیرِعگم ِ ونگن مێ ۄَھ مَننِ وطنٔۄ ۃھٔمنھ کَرنٔ سٔنی سینھٔ ہے ۃۆشی سَمھگنَ	
لینگ کوف ِ مَزر گَو یِ زِ مێ ۄُھ رُن کھگنٔ کھێنٔۄ نَڑٔ کمزُوریی کێگ کَرٔی وگن کھین 
	ۄُھم ۃۄش کَرگن َ گَۄھگ مێ ؤنی نَوی مێ ۄُھ نگسگن نۆہی گٔسی ؤ نٔ رگنھ یینی 
	نھھٔی َ کِنٔ گٔسی ؤ ێ 
گفسر ۃیرِعگم ِ سینھگہ مُمکِن ۄُھ ی گیسٔ ہنِ ُ گٔکِس لہظَس ژھۄمٔ کٔرِنھ ٔ ونُن ۄُھمی 
	نٔ ۄُھس ھِلٔ وگنٔ مَننٔی یِونی مُورٔ کَرنٔۄ کُوشِس کرگن َ ُ کُرسی ۄُھ نرْونہ 
	مَکنگوگن نٔ لۄنِ َونگن ٔ ونگنی یُس یِ یێنِ موسن مگسنر ۄُھ َ یِ ۄُھنٔ نِلکُل 
	کِہنی کَرگن َ سگٔری سٔے ۄھَن وگٔرگِنی کٔر مٔژ َ ۄِنھ نٔ مگرسل ۄِھنٔ سوزنَے
	 یوگنی نۆہی ہێکِو مگنٔ سورُے کینْہ نفصیلٔ سگن وُۄِھنھ َ ووی یُس مێ نرْونْہ 
	وُنی گوس گیمُنی سُ نِ ۄُھنٔ کِہنی کَرگن َ صِرف ۄُھ ۃرگوشَن منٔ لۆگمُنی نێیِ 
	ونگن ۄُھ عھگٔلٔژ ہِنھین کمرَن مَنْز ہُونی رَۄھگن َ نٔ نَسٔنھی طور طرٔیقٔ ََ مێ 
	ۄُھ نگسگن مێ مَزِ صگف صگف َونُنی نِکێگزِ ریگسنۄِ نوگٔیی ۃگٔطرٔ ۄُھ مۆز َونُن 
	ضۆرُوریی ہگلگنکِ سُ ۄُھ مێون رِشنٔ ھگر نٔ ھوس نِ َ وِنگن نَسٔنھی گَطوگر 
	ۄھِ مَشکُوک نٔ نَھنگٔمی ہُنھ نگٔعں نَنگن َ یێنِ ۄُھ گَکھ زٔمینھگری نگو ۄُھس یون 
	ۄِنسکی ی نۄہِ نِ ۄُھوون وُۄھمُن َ نہرہگل ینُھے یِ یون ۄِنسکی گَرِ نیرگن ۄُھی 
	ونگن وو ۄُھ میوگن نور وگٔنِنھی نٔ ۄُھ یون ۄِنسکی سٔنْزِ زَنگنِ سٔنی مسگیِ 
	ژنگن  نٔ کَرٔ گَنھ مینَ قسم  یون ۄِنسکی سٔنھی شُری ۄھِ سگٔری 
	گٔن نٔ غگٔن ووِ سٔنْزِ شَکلِی گَکھ رَژھ نِ ۄَھکھ نٔ فرق َیون ۄِنسکی سٔنْزِ
	 شَکلٔ ۄھُنٔ گَکھ شُر نِی نٔ سگرِؤے لۄکٔن کُور نِ ۄَھ ووِ سٔے ہِش َ
لینگ کوف ِ ُ لُطف نُلگن ٔ کیْگ ۄِھؤ َونگن  مێ ییِ نٔ یِ کَنھ زگنْہ ۃیگلَس مَنْز نِ 
گفسر ۃیرِعگم ِ ونگنی نۆہی وُۄھنوکھ مگنٔ یون ۄِنسکی سینھی شُری َ مرٔ وُنی نَو نٔ 
	مگ ۄھُس گَمُز َونگن َ مَنھ گو سۆکُوۆ گنسمکنڑ َ مێ ۄُھنٔ نَرگن فِکرِ ہۆکُومنَن 
	کِنھٔ مگٔنھی لوگ سُ گَنھ عۄہھس مینھ َ سُ ۄُھ گٔکِس گِنقلگٔنی سٔنھِ کھۄنٔ 
	نَنر َ سُ ۄُھ نوووگنَن ھیمگغَس مَنْز گِنقلگنی ۃیگیلَن ہُنھ سُ زہر نَرگن زِ ونٔنۄ 
	کَنھ ۄھَنٔ َ ۄُھنگ صلگہ نٔ ھِمٔ نۆہِ یِ سورُے کینْہ لیکھِنھ َ
لینگ کوف ِ شوقٔ سگن ھِیو َ مێگنِ ۃگٔطر ۄھَ سۄ ۃۆشی ہنْز کَنھ َ یێلِ نٔ کُنُے زۆن 
	گژھگن ۄُھسی مێ ۄُھ کگنْہ ھلۄسم ۄیز مَرُن ۃۄش کَرگن َ گَۄھگ نٔ نۄہِ کێگ ونیۆ 
	مَنُن نگو ێ مێ ۄُھنٔ یگھے روزگن 
گفسر ۃیرِعگم ِ زیملیگ نِکگ َ
لینگ کوف ِ گیی گیی زیملیگنِکگی میۆم یگھ َ گَۄھگ نۆہی ؤنی نو مێ نۄہِ ۄَھگ شُرگ کۆنگہ کگنْہێ
گفسر ۃیرِعگم ِ ونگن کێگزِ نٔ  مگنْژھ ۄِھمی زٔ ۄِھ نِمَو مَنْزٔ نگلیغ َ
لینگ کوف ِ وُۄھ کێگ ؤنِو  نگلیغ ۄھگ گگٔمٔنی ێ نٔ نِمھن کیگ ۄِھ ێ
گفسر ۃیرعگم ِ نہُنھ مطلَن ۄھگ نِمَن کێگ ۄُھ نگو ێ
لینگ کوف ِ نی نُوزِوی نِمَن کێگ ۄِھؤ ونگن ێ
گفسر ۃیرعگم ِ نکولگٔیی گیوگنی گیلزگھِینگی میریگ نٔ میرێمی نُویگ َ
لینگ کوف ِ وگہی وگہ 
گفسر ۃیڑعگم ِ ونگن مێ ۄُھنٔ نۄہِ زیگھٔ کگلَس گوش ۃرگٔشی کَرنُک وُرنھی نٔ نٔ 
	ۄُھس نٔ یژھگن نہُنھ قٔمنی َوکھ ضگیِ کَرُن 
			ُ کلٔ نۆمرگن نٔ نیرُن ہیوگن ٔ
لینگ کوف ِ ُ ھروگزَس نگم ۄُھس سٔنی گژھگن ٔ نٔی نِلکُل نٔی نۄہِ کُس َوکھ کۆرؤ 
	مێون ضگیِ و یِ کێنژھگ نۄہِ وۆن ؤی سُ ۄُھ نَڑٔ ھِلۄسم َ گۄھگ نێیِ نِ ہگٔوی 
	زیو ھیھگر َ کنھِنگنھِ گیو لُطف ُ گفسر ۃیرعگم ۄُھ ھروگزٔ نینَر نیرگنی لینگ کوف ۄُھ 
	ھرمگھٔ ننھ کَرگنی مگر سٔنی ۄُھ ھروگزٔ مُژرنِھ گیلَو ھِوگن ٔ ہے یمگٔری کێگ وۆنؤ 
	نُۄہِ مَنُن نگو َ مێ ۄُھنٔ مورٔ نگو یگھٔے روزگن َ
گفسر ۃیرعگم ِ ُ ھَروگززَس مَنْزَس وۄھنِ ٔ گیرنِمے فلمووِۄ زیملیکگنِکگ َ
لینگ کوف ِ گیرنِمے فلمووِۄی مێ ۄُھ مسلٔ ھَرمیش َ سفرَس ھورگن گو مێ سورُے مونْسٔ 
	ۃرٔۄ گگر نۆہی مہرنگٔنی کٔرِوی مێ ھؤو رُونلَس ژور ہَنھ وۄزُم ی گگر ۄنھَس 
	ۄِھؤ َ
کمشنر ۃیرگن ِ گی ۄَھم ُ نون کٔڑِنھ لینگکوفَس ھِوگن ٔ
لینگ کوف ِ یِ ۄَھ مێگنی ۃۄش نصٔنٔ َ مێ مَزِ نہُنھ سینھگہ سُکرگنٔ کرُن ُ گفسر ۃیر 
	عگم ۄُھ نیرگن َ نون ۄِنسکی نٔ یون ۄِنسکی ۄھ گَژگن ٔ
نون ۄِنسکی ِ ونگن ی نسلیمگن  ننھَس ۄھ نگو مینر گیِوگنو وِۄ نون ۄِنسکیَ نٔ 
	ۄُھس گَکھ زمٔنھگر َ
لینگ کوف ِ گیی مێ ۄِھوَ نۆہی وُۄھمٔنی ُ یون ۄِنسکی یَس کُن ٔ شگیھ لوے یوؤ نۄہِ 
	ھن َ وۄنی کێگ ۄَھو نسنِ ۃَنر ێ
نون ۄِنسکی ِ ۃۄھگیَن کۆر گێہسگنٔےی نۄہِ کێگزِ نٔرؤ فِکِری وۄنی ۄھُم زۃٔم مُورونٔ 
	گیمُن َ
لینگ کوف ِ شکُر مگٔلی کَس کُنَ نٔ گوس نَڑٔ ۃۄش یِ نُوزنھ ُ یکھَم ۄُھ کنھِ ہُنھ 
	رۄۃ مِھرگن ٔ مونْسٔ مگ ۄُھؤ کگنْہ ۄنھَس ێ
نون ۄِنسکی ِ مونْسٔێ کێُنھێ کێگزِ ێ
لینگ کوف ِ مێ گۆژھ رُونَل سگس کَھنڑ وۄزُمَ
نون ۄِنسکی ِ ۃۄھگیَس مَنھ ۄُھم نٔ کِہنیی مینر گیوگنو وِۄ ۃنَر مگ ژێ مگٔ گیسی ێ
یون ۄِنسکی ِ نٔ مێ نِ ۄُھنٔ کێنہ ُ لینگکوفس کُن ٔ وِنگنی مێ ۄُھ سورُے مونْسٔ 
	منلک نرسنس مَنْز ومع َ
لینگ کوف ِ ۃگٔری گگَر نٔ سگس رُونَل گیسِؤی نونِ مُمکِن ۄُھ ہَنھ شَنھ گیسِ ؤےێ
نون ۄنسکی ِ ُ منٔ نین ۄنھَن ھُونژھ ھِوگن ٔ ژێ ۄُھیِ رُونلٔ ہنھی مینر گیوگنووِۄێ مێ 
	ۄھِ کُل نِل ژَنویی
یون ۄِنسکی ِ ُ مَننِس نیگس وُۄِھنھ ٔ یێنِ ۄھِ کُل مٔنژھ رُونَلَ
نون ۄِنسکی ِ وگرٔ وُۄھی ۄنھَن مگ ۄھُے کینْہَ مێ ۄھَ ۃنَری مینر گیوگنووِۄ ژے ۄھُے 
	ھٔۄھنِس ۄنھَس زۆھَ مێ ۄُھ یقین کونھٔ کِس گسنرَس سٔنی گیسی گٔنھری کِنی 
	کینْہ ننٔ کیۆْہ لگٔرنِھ گۆمُنَ
یون ۄِنسکی ِ نٔ ننھ مَنْز نِ ۄُھنٔ کینْہ ێ مٔ ۄُھ مُورٔ مَے َ
لینگ کوف ِ ۃگٔری وۄنی کێگ فِکِر ہێژؤ نۄہَِ کینْہ مروگے ۄُھنٔ 65 رُونَل نِ ۄِھ 
	کگٔفیَ کگٔم نیرِ َ
		ُنون ۄُھ رَنگن ٔ
یون ۄِنسکی ِ ونگنی نٔ گوسُس یژَھگن گٔکِس نوزُک مسلَس مینھ نۄہِ کینْہ 
	عرٔض کرُن َ
لینگ کوف ِ ؤنِو کێگ ھٔلیل ۄَھ ێ
یون ۄِنسکی ِ یِ ۄُھ گَکھ نَڑِ نوزُک مگملٔ َ مێون زِێُنھ نێۄُو ۄُھ کھگنْھرٔ نرْونْۂے 
	زگمُنَ  
لینگ کوف ِ مٔزی مگٔنھِ ێ
یون ۄِنسکی ِ مگملٔ ۄُھ ینھٔ کِنی یُنُےی مگر زَن نٔ سُ ۄُھ زگمُن گَسِ نگضگٔمطٔ 
	کھگنْھر کٔرِنھی نِکێگزِ منٔ کۆر مێ کھگنھر نگمَل گٔنِنھ قُونُونی مگٔنھی سورُے کینْہ 
	ھُرْس َ وِنگنَ نۆہی کٔری یو مہرنگٔنیَ نٔ ۄُھس یژھگنَ سُ گژھِ وگٔیز طور 
	مێون نێۄُِو نسلیم سمھُن نَس نِ گژھِ مێگنی مگٔنھی یون ۄِنسکی نگو کَرنٔ یُنَ 
	ونگن 
لینگ کوف ِ کگنْہ مروگے ۄُھنٔ َ یِ ہێکِ سَمھِنھ َ نَس نٔ گژھِ یون ۄِنسکی ونُن
یون ۄِنسکی ِ نٔ ھِمی ہگ نٔ نۄہِ نکلیف َ مگر کێگ کرٔی مێ ۄُھ سینھگ ھُکھ نے ۄگرٔ 
	ۄُھ نَڑٔ لگیق لڑکٔ َ مێ ۄَھ نَس مَنْز نٔڑ گیش َ سگٔری شگر ۄِھس زَنگٔنی یگھَ
	نٔ گگَر کُنِ وگیِ شرْگکٔ مُوگہ ننیسی گٔۄھ نِنرگیِ منْز ھِیِ ؤ نم نم نٔ گُر ننگٔوِنھ َ 
	زَن گیسِ کگٔنْسِ نٔڑی مُورژگرَن ننوومُن َ ونگنی نون ۄِنسکی کَرِؤ گَنھ کنھ 
	نصھیق َ
نون ۄِنسکی ِ گیی نَڑٔ قگٔنِل لڑکٔ ۄُھ َ
لینگ کوف ِ نَڑٔ وگن گُو َ گَنھ مگملَس مَنْز یُونگہ مێ مِلِ نیُونگہ کَرٔ نٔ کُوشِشَ نٔ نِ 
	رَنس کَنھ َ مێ ۄَھ وۄمیھ سورُے ییِ نگمل َ گیی نِلکُل َ ُنون ۄِنسکی یَس کُنٔ 
	شگیَھ ۄُھکھ ژٔ نِ کێگہ نگم َونُن یژھگن َ
نون ۄِنسکی ِ وِنگنی مێ ۄَھ گَکھ ضُورُوری کَنھ عرض کَرٔنی َ
لینگ کوف ِ گیی ؤنِوی ؤنِوَ کێگ کَنھ ۄَھ ێ
نون ۄِنسکی ِ ونگن عرٔض ۄُھمَ یێلِ نۆہی مینرزنرگ وگمَس نٔڑِوی ننِ ؤنی نَو سگرِنٔے 
	نَڑین مێہنوینی سمنھری فوؤ کین گفسرَن ی شگٔہی مُشیرَن  ؤنی زیۆکھ
	 یُور گیکسی لینسی یگ گعلگ ہضرنی نۆہِ ۄھگ منگہی فلگٔنی قَصنَس مَنْز ۄُھ 
	مینرگیوگنووِۄ نون ۄِنسکی روزگنَ  نس یُنُے ۄُھ ونُن َ  ہُوضُوری فلگٔنی قَسنَس 
	مَنْز ۄُھ مینرگیوگنو وِۄ روزگن َ 
لینگ کوف ِ گۄھگ 
نون ۄِنسکی ِ گگَر نۄہِ مگھشگہ سلگمنَس سٔنی کَنھ کرنُک موقٔ مێُولی نێلِ ؤنی زیو 
	نِمَن نَِ  ہۆضُور مگھشگہ سلگمن  فلگٔنی قصنَس مَنْز ۄُھ مینر گیوگنووِۄ روزگنَ
لینگ کوف ِ نێہنری نێہنر 
یون ۄِنسکی ِ گَسِ ھِیو مگٔفی گَسِ کۆر نُہُنھ َوکھ ضگٔیِ َ
نون ۄِنسکی ِ گی گَسِ ھِیو مگٔفیی گَسِ کٔر نۆہِ گوش ۃرگٔشی َ
لینگ کوف ِ نِلکُل نٔ ی نِلکُل نٔی مێ سَمٔز سینٔ ۂے شگھمگٔنی ُ یم ھۄشَوے نیرِنھ مگنَس 
	سٔنی ٔ یم کِنھی سَنگ گفسَر ۄھِ یێنِ ێ یِ کِژھ ذگنھگہ ێ مێگنی کِنی ۄُھ یمَن 
	شگیھ ۃیگل زِ نٔ ۄھُس وۆکُومنُک گَکھ نھۆنگ َ رگنھ ژھُن مێ یمَن گٔۄھَن 
	کَۄھ یۄ کۄسٔ گہمَق ومگنھگہ ۄھئے َ مێ ۄُھ نگسگن نٔ لیکھٔ گَنھ مُنلِق 
	سورُے کینْہ رگزھگنِ  نرْیگمۄکِنَس کیکھٔ  سُ ۄھ مضمُون نینرِ لیکھگن َ 
	سُ کھگرِ یَنھ نَڑٔ رنگ َ سگرِ نٔے کھگریکھ عرشَس ہیو گوسِمگ ُ گوسِم ۄُھ ھروگزٔ 
	کِنی نُنھ ہگوگن ٔ نلگ کگگز قلم گین  
گوسِم ِ نٔ یکھَم ُ وگمَس ۄُھ گژھگن ٔ
لینگ کوف ِ نٔ گگر نرْیگمِۄکن کگٔنْسِ منٔ لگِ گَنھ مێیِ مُور ہیس نھگوُنَ نَس نگسِ یِ 
	نَڑٔ وگن مزگقَ نٔ مزگقٔ ۃگٔطرٔ گَو سُ منٔ نِس مگٔلِس نِ کٔنین وولَ گگَر 
	مونْسٔ نُونک نِ نَنگوُن یژھِ ی نَنھ نِ روھِ نٔ مَنھ َ مگَر ۄھِ یم گفسر وگنَ 
وۄزُم ھێُنکھ مێ یمَو ێ نَڑٔ وگن وصٔفی گۄھگی نٔ وُۄھ نگ کُل کُونگہ رقم ھێُن مێ یمَو ێ 
	نرْیے ہَنھ َ یم ھِنی ووَن َ نێیِ نرْے ہَنھی یم گٔیِ موسن مگسنر سٔنھیی یێے 
	ہَنھی سَنھ ہَنھ گٔنھ شَنھی وُۄھ کێگ ۄُھ نێلِ کُن ہیُوہ نونَ گٔنھ شَنھی نو 
	شَنھ ییییی وگہ وگ ی یِ ۄُھ سگسٔ گێورُی وگنگن َ کمنگن صگٔن وُنی کِس گۆژھُکہ 
	ێنینھ گیسُنی گھٔ وُۄھَہو کہُننز موو سۄن نولِ ہےی کٔہنْز موو رۄمھ نولِ ہے 
		ُ گوسِم ۄُھ کگکز قلم ہینھ گژگن ٔ
	وَولگی وُۄھُو گہمقگہی مێ کێگ یزنھ ۄُھ سَمھگن یێنِ ێ 
		ُ لیکھُن ہیوگن ٔ
گوسِم ِ گیی وُۄھُمی شُکر نَس ذگنِ مگکَس کُن  مگر گَکھ کَنھ ۄھَ   نۆہِ ۄھگ 
	منگہ  ێ
لینگ کوف ِ کێگہ ێ 
گوسِم ِ گَسِ مَزِ وۄنی نیرُن یێمِ قَصنٔ مَنْز َ وُنکیس ۄُھ وَکھ ۃۄھگیَس مَنھ 
لینگ کوف ِ ُ لێکھگن لێکھگن ٔ کێگ نَکوگس ۄُھکھ وَنگن  نٔ کێگزِ نیرٔ ێ
گوسِم ِ نٔ ۄُھسَے وَنگنَ یمَن گۆروگن ی گَسِ گێیِ یێنِ زٔ ھۄہ وۄنی  وَوں گو 
	ہَھ وۄں کێگ فگٔیھٔ ۄُھ یمَن سٔنی روزنَس ێ لعنن ژٔھینُوکھ َ کێگ منگہ 
	ۄِھ نیێِ کگنْہ مێیِ نَن وگٔنِنھ َ ۃۄھگیَس مَنھ وۆن مے مێز گیوگن 
	گلیگزگنڑرووِۄ  َ یێنِ ۄھِ گُری نڑٔ لگیق َ نِمێ کینھ ھورَو ژُمَنوَل َ
لینگ کوف ِ ُ لیکھگن ٔ نٔی نٔ ۄُھس یَژھگن نێیِ کینْژَس کگلَس یێنی روزُن َ مُمکِن 
	ۄُھ مگگہ نیرَو َ
گوسِم ِ مگگہ ێ نرْ نزِۄ ۄھَنٔ ۃنر گیسگن ۃۄھگ رٔۄھِن َ نٔ ۄُھسے مۆز وَنگن ی گیوگن 
	گلیگزگنڑرو وِۄ ی گَسِ مَزِ یێنِ نیرُن وۄنیَ یَزنھ نِ گو مۆزُےَ مھگر گٔژھی نَن 
	کێنْہ ژھلگہی نوَے ۄُھسے ھمَگن گٔسی نیرَو یێنِ ؤلی ؤلیَ مێ ۄُھ یقین یمَن ۄُھ 
	کیْگہنگم ھۄکھٔ َ کُس نگم نرْونْنَ نٔ نِ کٔرِنھ گژھی مول سَۃ نگرگض یُون کگل 
	نینَر روزنَس مینََ ۄِھنگ وگنی گگَر یَزنٔ سگنٔے یێنِ نیرَو وُنی کیس ھِن یم گسِ 
	وگن گُری سوگرِ نگمَنَ
لینگ کوف ِ ُ لیکھگن ٔ گَۄھگ نٔ نی کروی مگَر گۄیٔ نرْگو یِ ۄِنھیَ نٔ نێیِ کرنگو یگک 
	گُرین ہُنھ گِننظگمَ مگر گُری گژھنَے گصٔل گیسٔنیَ نمیُک نھَوی زِ ۃیگلَ ہرکگرَن 
	ھٔمی زِی نٔ ھِمَکھ شرگنٔ نگمَن گَکھ گَکھ رُونَلی مگر شرٔط ۄُھ نِم گژھَن گُرین ہوگ 
	ھورٔ نگؤنیی زَن نٔ شگٔہی گیلۄی ۄھُس نٔ نێیِ گژَھن نَھھِ ہنِ سگرَس مَنْز 
	گیؤنی ُلیکھگن ٔ نرْیگمۄکن ینُھے یِ سورُے مَرِ نَس گژھ گَسگن گَسگن ۂلی کگنین 
	گُرُس َ
گوسِم ِ نٔ سوزوؤ یِ ۄِنھی یہنھِس نوکرَس گَنھَِ یینِس کگلَس وَنٔ نٔ سگمگنَٔ ننٔ 
	گژھِ وَکھ نیرِنھی ونگن 
لینگ کوف ِ ُ لیکھگن ٔ رُن ۃیگل ۄُھی مگر مێ گَن موم نٔنیَ
گوسِم ِ ُ نینر نیرِنھ نگھ لگیگن ٔ ہَے نگیگ وۄلٔ یُوریَ یِ ۄِنھی نِ یگکۃگنَس مینھَ ننِ 
	ؤنی زِ موسن مگسنرَس یۄ گژھِ نِکنِ ورگٔے سوزٔنیَ یِ ۄھێ سرکگٔری ۄِنھیَ 
	نێیِ ؤنی زیس زِ مێگنِس مگلکٔ سٔنھِ نگمَن سگرِؤے کھۄنٔ گصٔل گُر سوزُن یُس 
	گیلۄی سوزنَس لگگن ۄُھی وُنی ینھے مگٔنھی ھٔمی زیس میْون مگٔلِک ۄُھنٔ کِرگے 
	کینْہ ھِوگنَ گٔمی سُنھ سفَر ۃرٔۄ ۄَھ ہۄکُومن نرھگش کَرگنَ گُری گژھَن نگزٔ ھَم 
	گیسٔنیی ننٔ گژھِ میون گیغٔ وێہرَٔ ژٔ مرْگر یینیی ۄِنھی ۄَھنٔ وُنِ نیگرَ
لینگ کوف ِ ُ وُنِ لیکھگن ٔ گَمگی سُ کنِ سَنگ وُنی کیس روزگنێ موسن مگسنر سِنرین کِنٔ 
	گوروۃگفیگێ نَس نِ ۄُھنگ عگھن گَرِ گَرِ مکگنٔ نَھلگونُک کِرگے نۄگونٔ نگمن وۄلٔ وُۄھَو 
	موسن گیفِس سنرین کِس منگہَس مینھ سوزوسی ھیِٔ وگنیس ُ ۄِنھی ۄُھ نگہ 
	کٔرِنھ لفگفَس نھرگن نِ سَرنگمٔ ۄھُھ لیکھگنَ گوسِم ۄُھ موم ننی گَنگن نٔ لینگکوف
 	ۄُھ لفگفَس مۆہر نرْگوگن ٔ
ھرھِمورھگ سٔنْ گیوگز ِ ُ ھُورِ مینھ ٔ ہےی ریشلگی ژٔ کۆن مکگن نرْونْہ نرْونْہَ گور ۄُھنٔ 
	کگٔنْسِ نِ ہکُم گژنَس نۄہِ ۄُھنگ وَننٔ گیمُنَ
 مگسنر</p>

<p></p>

<p>
 
 </p>

<p></p>

<p>
               0 
</p></body></text></cesDoc>