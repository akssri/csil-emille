<cesDoc id="kas-w-literature-humour-es10.kal" lang="kas">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kas-w-literature-humour-es10.kal.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>NUNIPOSH</h.title>
<h.author>AZURDA</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - BOOK</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">es10.kal</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page NUNI POSH.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-24</date></creation>
<langUsage>Kashmiri</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>                      پَرنَ والین </p>

<p>بٔ چُھس تِمن پَرن والین ہُند شُکر گُزار ےِموَ مےانِ دوشوَے ایسے سۆمبرنٔ  فِکرِ 
ہنز ٹِکرِ (1980ئ) تِ (1983ئ)پَرِ تٔ تِمنَ پیٹھ وِزِ وِزِ اظہارِ خےال کُورکُھ ۔ ےِ 
چُھ پَرن والِنے ہُند دُےت زِ  ایسے ےَس مےُول 1984ئ ہُک ساہےتِ اکاومی اےوارڈ تٔ اَمےُک انگرےزی
امےُک انگرےزی ترجمِ چھپےو 1986 ئ سٔ منْز ۔ ڈاکٹر عابد رضا بےدار ڈاےرےکٹر 
خُدا بخش لاێبری پٹنِ سُند شُکریٔ کُرن چُھ مےْ فرض ےِمو کأشرِ ایسے ہُک اُردو
ترجمٔ چاپُن پننِس اشعاعتی پروگرامس منز شأمِل کۆر۔ مێ چھێ وۄمےد زِ برۄنہ
  کُن تِ پِھرناوَن تِم کاشٔرِ اہم کتابٔ اردۆہَس اَندر۔ 
         َےَتھ ایسے سۆمبر نِ یۄسٔ توہِ برۄنہ کَنِ چُھ ناو نُنِ پوش۔ بٔ چُس
غُلام نبی گُوہر صأبُن تٔ محمد ےوسُف ٹےنگ صأبُن شُکر گُزار ےِمَو اَمِ ناوُک انت۔
خاب کرنَس منْزمےْ مدد کوْر۔ نُنِ پوش أسی کأشِری بنفشِ پوشَس وَنان ےُس ےِم 
وَنَن منْز محنت کٔرتھ سوْمبران أسی تِ پتواوسکھ اَتھ قٔیمتیی پوشَس وزَنَس
وَذن نوْن مےلان۔ ےِ باسےوو مےْ کاشرین پٹیھ سپدَن والِ اسحصالُک اکھ سُ نمۆنٔ 
ےُس برونٹھٔ کَنِ رٔٹیتھِ کأشرین سوچُن پَزِ۔
       
    </p>

<p>     مےْ چُھ یاد ےێلِ مےِر غلام رسۆل نأزکی  صأبن ایسے پٔر تٔ تمِو  وۆن زِی    
یَتھ کتابِ گۆژھ ناو آسُن                     
۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔۔
   ذہنی اَرٔسَرٔ۔ مێ چُھ اَتھ ستٔی اتفاق تِ کےازِ دُنیہس منْز۔ چِھ ذہنی اَرٔ 
سَرے ۔ ےأتی باقۓ روازن ۔ اَمِ علاؤ ہیکِ تِ کےا لکھَن وول کٔرِتھ۔ 
         </p>

<p>
              ێمیِ کتابِ ہندِس أخرس پٹیھ کےنہ تبصرٔ  ایسے سۆمبر نِ پٹیھ
شامِل کرنِ آمتِی۔ اَتھ منز چُھنٔ کانہ تخصےص سوایِ کِ برۄنہِ کَنِ أسی تِم کٔری 
شأمل۔</p>

<p>      اکھ خاص کَتھ یَتھ کتابِ متعلق:۔ یِم ایسے چَھ آمٔتی مختلف وقتن پٹیھ
لکھنِ اَؤکنِی باسِ کُنِ کُنِ خیالاتَن ہُند ہِشر باسیوو مێ یُتھُے زِآفتابٔ چُھ دوہَے کھسان،
أسی کَرچہِن یِ کٔھستھ وُلوِتھ  اَنان زِ راتھێ ہَێ ژِ وُچھکُھ۔ بہارپرتھ ۆری یِ یِوان 
تِ أسی اَتھ ہمیشِ لول بَران أتھ منْز چُھ وندٔ تِ پرتیھ ۆری یِوان۔  </p>

<p>
           اَؤ أسی کیا اَتھ چھا بُتھ پِھران۔ یُتُھے ہِشر آسِ یِمنَ ایسے ہَن منْز تِ
لُنِ کُنِ یَتھ اَتھٔ لاگُن مێ باسیوو زَن أنَس دولٔ وُچُھن۔ اَؤ کنِی  تھۆو مێ یِتھَے 
کٔنی۔   
                                                                          
     آخرس چُہس بٔ تُہند شکر گزار یِمَن یِ سۆمبرن زِیرِ مطالعٔ چھێ ۔             </p>

<p>                                                     مُحمّد زَمان آزُردہ     
                                                          سری نگر
                                                       19 فروری 1988ئ               
  </p>

<p>                             نُنٔ پوش</p>

<p>     کأشُر محاورٔ رۄپیِ ہَتَھس کانگرٔ بَند چُھ تۄہِ سارِؤے بۆزمت۔ موسمچِ 
سختی کِنی یا بێِ کُنِ مجبۆری چُھ رۄپیِ ہَتَس اِنسان کانگرٔ بند رَٹان مگر مٔلی چُھنِ</p>

<p>ہیوان یتیِس تٔھدِس رقمَس کینہ۔ وۄنی  گۄۄ ێلیِ زِندگی ہنزن باقێ ضرۆرژَن سام ہمَو
تٔ کأشُرچُھ بێیَن ہِزِ زبردستی تٔ چلأکی پرتیھ ساتٔ اۄرزمان تھومُت ۔ أمی سٔندِس
اَتھ دأدِس سپدِنٔ زانہ تِ علاج۔ توأریخنَ پُھےر یِ لَرِ لَرِ۔ منز وتَن تھووُن سأوِت۔
سَتَن درِیاوَن پِر نووُن اَما تریش دِژنَس نٔ چَنٔ۔ یِ بۄچھ ہۆت تٔ تریشِ ہۆت رۆد دۄ     
دۆہَے زمینَس وایان۔ اَمِ منزٔ فصٔل کڈان، کیری کَھنان تِ تِمۆ منزٔ آب کَڈان مگر یِ
فصٔل تٔ آب اوس دۄہَے بێَےن نصیبٔ۔ یِ زاو پوشَن ہُند مۄل پر زٔ ناوان مگر کےا کرِ۔
نارس دِیا نَرِ۔ أمِس رۆد ازَلَے لون بُتھ پِرِتھ۔ أمی ہاوِپَننِ مرگٔ لۆکَن۔ تِہُند بورسو۔
رُن، ژۄچِ کَھنجَن پیٹھ۔ یمَن سألانین منز نَ أسی أمِس پننٔی زِٹھی یمن خدمت 
کٔرِتھ یِ توشِ ہے۔ نَ أسٔس پَننٔی شُری یَمَن وَوتھ ہأوِتھ یِ پَنُن آے ہُرراوہے ۔ 
یِم أسی وۄپرٔے وِۄپرَ۔ أمِس دِژوو قُدرتَن کنیو منزٔ مگر رَٹِ ہَے کِتہھ کٔنی أمِس 
اوس بانَے۔ گۄڈٔ اوسُس کےازِ نٔ اما سُ ژٔھنُن پانٔ تولُن بلکِ بێَےن کیُت دِتُن اَتھ 
لالَس وَٹٔ رنگ۔ یَمن وَٹن بدلٔ کےا میۆلُس کُنِ نۆن تٔ کُنِ کٔنکٔھ۔ سُ تِ نٔ تَمِ 
وَزِ نٔ یُس اَتھ نیلمَس اوس ۔ جنگلوَ تٔ بالَو دِژَس پوشِ، جَڑِ، زنڑ یِمَن طِبس منز 
بڈٔ مۄل اوس ، اَؤأمِس میول صرف گاسٔ مۄل۔     </p>

<p>         مێ ێلیِ واریہَن نِشِ یِ بۆز زِ نُنٔ پوش تِ چھِ کٔشیرِ منز آسان ۔ بٔ 
گوس حأرتن۔ نُنٔ گُری تٔ تلٔ تلاؤ یِ اوس بوزمُت تۆملس منز نُنِ بیول تِ اوس 
مشہۆر تٔ برْونہم ذٔالدار أسی مُشکٔ بدُجِ علأ اَمُےک بَتٔ تِ کھیوان۔ انس           </p>

<p>               0 
</p></body></text></cesDoc>