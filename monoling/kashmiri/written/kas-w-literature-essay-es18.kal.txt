<cesDoc id="kas-w-literature-essay-es18.kal" lang="kas">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>kas-w-literature-essay-es18.kal.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Anhar</h.title>
<h.author>Margoob</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Journal</publisher>
<pubDate>1986</pubDate>
</imprint>
<idno type="CIIL code">es18.kal</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page P.no/1.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-24</date></creation>
<langUsage>Kashmiri</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;Anhar/Vol.9 / no.12/ DR. Margoob Banhali/ Essays&gt;</p>

<p>								( 1 ) 1
												مشعل سُلطانپوری</p>

<p>						0کأشُر لِکٔ اَدب 2      2</p>

<p>							0		اکھ مۄخصر سام</p>

<p>
	ےێمِ مضمُونٔ باپَتھ چُھ مێِ موضُوع دِنٔ آمُت " Research in kashmiri folk 
literature -a study "  ےانے کأشرِس لِکٔ اَدبس منز تحقیق۔ اکھ مطالعٔ مےأنُو کِنی 
چُھ لُکٔ اَدب، زبأنی زبأنی چولٔنین تِمَن لُکٔ کَتھَن، لُکٔ بأتَن، پریژھَن، کہاوژن، 
دٔپی تین تٔ گنڑتین ہُند سُ سرماےِ ےُس لێکِھِتھ رٔژھراونٔ یےِ تٔ ےَتھ زمانَس منز 
چُھ ےِ سرماےِ رٔژھرراونُک مُنأسِبرےقٔ لێکھِتھ چھاپھ کَرُن، تِکےازِ مُسودن ۂنزِ شکلِ 
منز رُوزِتھ تِ چُھ اَتھ ضاےِ گژھنُک اندےشٔ۔ ےۆتاں ےِ لےکھنٔ ےِےِ نٔ اَتھ لُکٔ ادب 
وَنُن چُھنٔ ہَٹِ وَسان۔
	سوال چُھ پأدٔ سپدان زِ ےۆتاں ےِ لےکھنٔ ٔےِ نٔ تۆتاں کُس ناویےِ اَتھ دِنٔ۔ 
انگریزی زبأنی منز چُھ اَمِ خأطٔ Folk lore تٔ ےِ ترکیب چھێِ گۄڈنچِ لَٹِ ےَس منز 
ڈبلےو، جے، تھامس
 </p>

<p>								( 2  ) 2
نأوی أکی انگریز أدیبَن وَرتأومٔژ۔ انساۓکلو پےڑےا برْیٹنکا کِس نؤی مِس جلدس منز 
چُھ صفٔ نمبر 440
ہس پیٹھ اَمِچ ویژھنَ یتھٔ پأٹھی " The word was coined by the 
English Antiquari an W. J. Thomas in 1846 to denote the
traditions, customes and the superstitions of the
uncultured classes in civilised nations.|"
پَتٔ چھِ اَتھ فوک لورس حصٔ۔ فوک مےُوزِک، فوک ڈانس، فوک سانگ، فوک ٹےلز۔ 
پۆز فوک
لٹرےچر ناۄا گۄو نٔ مێِ کُنِ نظرِ۔ ظأہر چُھ وَجہ ےِ زِ لےکھنٔ یتھ ےا چھاپَس کٔھسِتھ 
ما ۂێکِ نٔ ےِ فوک رُوزِتھ
پۆز وق چُھ تیژ تیزی سٔتی بدلان زِ اگر ےِ سرماےِ لێکھِتھ رٔژھراونٔ یےِ نٔ، کےنہ 
کال گٔژوھتھ روزِ نٔ امیُک
بےب نِشانَے کُنِ۔ کأشرِس منز چُھنٔ مےانِ خبرِ تَل وُنستاں فوک لورٔ باپَتھ کانہ لفظ 
تیُتھ ورتاوس
منز ٔتھ ہےۆکھمُت ےُس اَمِ کِ مانِ باپَتھ پُورٔ پأٹھی نَکھٔ ؤسِتھ ہێکِ۔ پروفےسر 
محی الدین حاجنی صأبن 
چُھ لُکٔ رس ورتوومُت تٔ بێےَو أدےبَو  عام طور لُکٔ اَدب۔ پۆز یم دۄسوے لفظ چِھنٔ 
اَتھ نکھٔ وأتِتھ ہێکی مٔتی۔
	ےِ فوک لور رٔژھراونٔکی چھِ ترێِ مٔنزِل۔ گۄڈنیتھ گژھِ ےِ سورُے لےکھنٔ یُن۔ 
پَتٔ چھاپھ
کرنٔ ےُن تٔ پۆتُس ہێکِ اَتھ کہؤٹ دِنٔ یتھ تٔ زگ تٔ پرۆن بےۆن بےۆن ژارنٔ یتھ 
پۆز
مێحنتھ لأگِتھ بَچاونٔ خأطرٔ چُھ ضۆرُوری زِ سۆمبران سۆمبرأنی یےِ اَتھ زول دِنٔ۔ 
اصل رٔژھراونٔ تٔ نقل تراونٔ۔
	پۆز اے بوزو اَسِ چِھنٔ وُنِ گۄڈنکی زٔ مٔنزِل تِ پرأوی مٔتی۔ أسی چھِ فوک 
لورٔ کِنی تیتی
بختاوَر زِ ےُورپی مُحقِق نولزس چُھ اَمِ کَتھِ ہُند اعتراف کرُن پےومُت زِ فول لورس منز 
چُھنٔ
کأشرِس قومس کانہ جورٔ۔ کأشُر فوک لور رٔژھرراونچِ یم کُوشِشِ سَپٔز تِمَن ہُند دس 
چُھنٔ کأشریو،
بٔلی کِ غأر کأشریو تُلمُت۔ کانہ گَرٔ تِ اگر دۄلابَن منز یوان چُھ تَمِ کین بأژن 
چُھنٔ اَمِ کَتھِ</p>

<p> </p>

<p>									(3  ) 3
ہُند حیسٔے روزان زِ تِمَن کُس کُس چیز چُھ راوان کےا کےا چُھ ضاےِ گژھان تٔ کےا کےا 
چُھ مۄچھِ مُورِتھ 
نِنٔ یوان۔ وۄں چھنٔ کُنِ قومٔچ کتھٔے۔ ےۆہَے وَجہ چُھ زِ کأشرین گۄونٔ پَننِس اَتھ 
سرماےس کُن 
ظۄنٔے، تِمَن گٔےِ تَمِ ساتٔ اوکُن کُل ےێلِ نێبر میو کوکُن اِشارٔ کۆر۔
	ؤی اگر أسی کأشرِس لُکٔ ادبس منز تحقیقٔچ کَتھ کرو تٔ کِتھٔ کٔنی۔ سون 
فوک لورے سورُے
وُنِ اَدبٔ کِس دأےرس منز آمتُے چُھنٔ۔ ےێتِ چُھس بٔ ادب لفظُک مانے تحریری سرماےس 
تامٔے مۆحدُود تھاوان۔ فوک لورُک ےُوتاہ حصٔ چھاپھ سَپُدمُت چُھ سُ سۆمبرَن والین 
تٔ لےکھَن والین
نٔ أس تیژ فُرصتھٔے تٔ نٔ باسےیَکھ ضۆرُورتھٔے زِ تِم ےِ کےنژھاہ لےکھان چِھ تَتھ 
پیٹھ کےاہ چُھ تحقیق 
تِ کرُن۔ اَتھ سِلسلس منز ےێتِ ےِ کَتھ ےاد تھأطؤنی لاےق چھێِ زِ تِمَن اوس اکھ 
ٹھےکٔ پُورٔ کَرُن ےانے
أکِس خاص وقتس اَندر اَندر اکھ سۆمبرَن تَےار کَرٔنی تَتِ چھێِ ےِ کتھ تِ داد دِنی 
لاےق زِ تِمَن چَھنٔ 
کَم مێحنَتھ کَرٔنی پھےمٔژ ےِ زانِ سُے ےَس کأم کَرٔنی پێےِ۔ فوک لور سۆمبرنٔچ تٔ
 پھاننِ کٔڑِتھ
لےکھنٔچ کأم چَنٔ سَہل۔ اَتھ منز چِھ وارےاہ مُشکِل بُتھ یوان یمَو مَنزٔ کَرٔ بٔ 
کےنچَن کُن اِشارٔ۔
	1۔ أکی سٔے لکٔ کَتھِ تٔ لُکٔ بأتَس منز چُھ بےۆن بےۆن علاقَن منز گٔژِھتھ 
	پھےر باسان۔
	2۔ مقأمی اثرس تَحتھ تٔ زمانٔ پِھرِس سٔتی سٔتی چھێِ اَکے کَتھِ ۂنز تٔ 
	اَکی بأتٔچ شکل ڈلان
	روزان۔
	3۔ باضے چُھ کأنسِ گۄنماتٔ سُند کانہ مقبُول بأتھ مٔشِتھ گژھان تٔ لُکَن چُھ 
	امیُک
	صرف کانہ مِصرٔ ےا کانہ شارٔے ےوت ےاد روزان۔ ےَتھ پَتٔ تِم پَننِ ضۆرُورتٔ 
	مطأبِق وَننی مِصرٔ رَلأوِتھ بدلٔے بأتھا بَناوان چھِ۔
	4۔ کأشرین مثنؤی ےن منز لےکھنٔ آمٔتی بأتھ چھِ اکثر لُکَن منز مشہُور 
	سپٔدی مٔتی۔تِمن چُھنٔ
	شأےرن ہُند ناو لیکھُت ےادٔے رُودمُت تٔ کُنِ چَھکھ یمَن بأتن ہَناہ شکل
	ڈأجمٔژ ےێمِ کِنی یم لُکٔ اَدبس منز شأمل کٔری مٔتی چِھکھ۔
 
							(  4 ) 4 </p>

<p>
	5۔ باضے چُھ گۄنماتَو کُنِ لُکٔ بأتُک کانہ مصرٔ بێ شُونکی پأٹھی پانٔ نوومُت تٔ 
	تَتھ سٔتی 
	شار گٔنڑی مٔتی۔ ےێمِ کِنی لُکَو سُ لُکٔ بأتھٔے مونمُت چُھ ۔ شأیرو چھێِ 
	کُنِ کُنِ لُکٔ بأتن ۂنز
	مقبُولِےتھ وُچھِتھ زأنِتھ مأنِتھ یمنٔے     تٔ ردیق قأفِیَن منز بأتھ ؤنی مٔتی
	زِ تٔہندی بأتھ تِ گژَھن مقبُول۔
		وۄنی ےۆتاں نٔ کأشُر لُکٔ ادب سۆمبرَن وألِس کأشِر شأےری ہندِس تمام
سرمایَس پیٹھ نظر آسِ، سُ لُکَن ہُند مِزازٔ زانان آسِ تٔ فوک لورچِ سارێے نزاکٔژ نظرِ 
تَل آسنَس
سُ کِتھٔ پأٹھی ہێکِ لُکٔ بأتَس مُتلق فأصلٔ کٔرِتھ زِ ےِ چھا لُکٔ بأتھ کِنٔ نٔ۔
	فوک لور سۆمبرَن وِزِ چُھ ےِ مُشکل تِ پےش یوان زِ اَتھ دأےرس منز کےاہ ہێکِ 
یتھ۔لُکٔ
کَتھ تٔ لُکٔ بأتھٔے ےأتی ما گۄَو فوک لور، پریژھ، کہاؤژ، شُری بأتھ، تلمیحٔ، ضرب 
المثلٔ تِ پین أتھی 
کھاتس تراونِ۔ بڑان بڑان چُھ امیُک دأےرٔ محاورن تٔ پانٔ زبأنی تاں واتان۔ لَٔےی 
شہَس مُتلق
چُھ گہے وَننٔ یوان زِ ےِ چُھ لُکٔ اَدبُک حِصٔ تٔ گہے چُھ ےِ اَمِ بِدُون کڑنٔ یوان۔ 
موتی لال ساقی صأبن
چھِ یتھی بأتھ لُکٔ بأتَن سٔتی شأمِل کٔری مٔتی تٔ ناظر لۄلگامی صأبم نٔ کےنہ۔ 
دۄشؤنی نِش چھِ وۆجُوہاتھ ۔
	مےون خےال چُھ زِ نیندٔ بأتَو تٔ چَھکرِ بأتَو منزٔ تِ گژھن تِمَے بأتھ لُکٔ 
بأتَن منز
شُمدر سَپدٔنی یمَن لُکٔ مزازٔ آسِ۔ مسلسل آسن نٔ کأنسِ أکِس شأےرٔ سٔنز 
شۆعُوری تخلیقی کُوشِشِ
ہُند نتیجِ آسَن نٔ۔ بٔ دِمٔ اکھ مثال کٔملی چھِ مێِ اکثر نیندٔ ڈورین منز ےِ 
معراج نامٔ نیندٔ بأتھ
بنأوِتھ گیوان بُوزی مٔتی۔
	کرٔےو رنگٔ رُومالے واو      تاجدار سون معراجس دراو
تٔ ےِ ہێکےاہ محض اَمی کِنی لُکٔ بأتَن منز شأمِل سَپدِتھ؟ مےأنی کِنی گَژھِ نٔ 
سَپدُن۔ بےاکھ کَتھ 
چھێِ ےِ زِ اَکِ نَندُن چُھ سأنی مشہُور تٔ مقبُول لُکٔ کقتِ۔ ےِ کَتھ چھێِ نظمَومٔژ 
گۄنماتَو تِ۔ مثالِ پأٹھی
رمضان بٹن، صمد مےرن، احد زرگرن۔ یہندی یم منظُوم اکٔ نندُن ہێکِ نا لُکٔ ادبٔ کِس 
وانگس</p>

<p> 
							( 5 ) 5
منز یتھ؟ موضُوع کِنی گَژھن ینی پۆز شأےرن ہنز انفرأدی شۆعُوری تخلیقی کُوشِشَو 
کِنی نٔ کےنہ۔
	فوک لور سۆمبرن والین چُھنٔ وُنیُک تام دۄن اہم کَتھن کُن توجُہ گومُت۔ نادِ 
لمان
لمان، رےڑٔ پکناوان پکناوان ےا گےلٔ ڈلٔ وان ڈلٔ وان یم ناد مۆزُورن ۂنزِ زےۆِ نےران 
چھِ تِم تِ ما
چھِ فوک لورُک حِصٔ؟ مےانی کِنی گژَھن تِم اَتھ دأےرس منز ینی۔
	یمٔ دٔلیلٔ کأشریو گۄنماتَو مثنؤی ےَن منز وَنِ مَژٔ چھێِ تِمٔ چھێِ کأشِر اَدبُک 
حصٔ بَنے مٔژ۔
پۆز یژھَن لولٔ دلیلن ےا جنگی قٔصَن ہُند بےاکھ رٔپ چُھ لُکٔ کَتھن ۂنز صُورژ منز 
تِ مُوجُود، یژھٔ
لُکٔ کَتھٔ چھێِ بِلکل دٔلیلٔ ہِشِ تٔ منزٔ منزٔ چھکھ بأتھ یمَن تننن ونان چھِ۔ 
مِثالِ پأٹھی سامٔ
سٔنز دٔلیل ےا لألِ ۂنز دٔلیل ےُس اسمال میرنِ زێوِ تِ لُکَو بُوزمٔژ چھێِ۔ یتھی دٔلیلٔ 
گٔری چھِ وۄنی 
اکِ اکِ ےێمِ عالمٔ گژھان۔ یژھ دٔلیلٔ تِ گژھن لُکٔ اَدبٔ چَن سۆمبرنَن منز اَننٔ 
ینِ۔
	وۄنی بُوزی تَو فوک لور چَن سۆمبرنَن مُتلق تِ کےنہ کَتھٔ۔ ےِ زَن مێِ پیٹھ 
کَنِ وۆن،
سانِ لُکٔ دٔلیلٔ لےکھنٔچ کُوشِش کٔر گۄڈٔ ےُورپی مُحقِقو۔ جی ایچ نولزن کُر 64 کأشِر 
لُکٔ کَتھ 1887ئ
ےَس منز جمع Folk talks  of Kashmir کَرین شیرٔژأر کٔرِتھ لَندنٔ چھاپھ۔ سر آرل 
سٹاےنن
کرٔ  حاتمز ٹےلز ناوی بێےِ 11 کأشِر لُکٔ کَتھٔ 1896ئ ےَس منز جمع یم چھاپھ 
سَپٔزِ۔ اَمِ پَتٔ
کٔر نور محمد روشنن بێےِ ترْہ لُکٔ کَتھٔ جمع یم لالٔ رُخ پبلی کےشنن پوشِ تھٔر 
نأوی 1955، ےَس منز
چھاپھ کٔر شمبو ناتھ بٹ حلیمن کٔر 6 لُکٔ کَتھَن ۂنز اکھ سۆمبرَن  بالٔ ےار نأوی 
1960ئ ےَس منز چھاپھ۔
اَمِ پَتٔ سَپٔز بےاکھ سۆمبرن ایس ایل سادھو سٔندِ دٔسی 1961 ئ ےَس منز چھاپھ۔ 
اختر محی الدین تٔ
پُشکر بھان نِ دٔسی سپٔز بێےِ تِ کےنہ کتھٔ دٔلیلٔ نأوی چھاپھ سپدِتھ مۆحفُوظ۔ 
پَتٔ نی کلچرل اکےڑےمی
ےِ کأم دروس تٔ اَمِ اِدار کِ طرفٔ سپٔز کأشرین لُکٔ کَتھن ۂنزٔ ترێِ سۆمبرنٔ 
شاےع گۄڈنِچ چھێِ ٹاک
زےنٔ گےری سٔندِ دٔسی جمع سپٔز مٔژ ےَتھ منز 39 کَتھٔ چھێِ تٔ بێےِ زٔ محمد 
احسن احسنن ترتیب دِژمٔژ
یمَن منز بل ترتیب 52، 53 لُکٔ کَتھٔ رٔژھراونٔ آمٔژ چھێِ۔ یمٔ دٔلیلٔ چَھنٔ پےشِ 
وَر دٔلیلٔ گَریو،
 
								( 6 ) 6
بٔلی کِ کٔشیرِ ۂندیو لِکھاریو اکیڑےمی پےش کَرِمژٔ ےێمِ کِنی یہُند پَنُن لُکٔ لہجِ 
قأیم چھنٔ رُوزِتھ ہےۆکھمُت۔ کُوشِش
چھێِ ےِ کرنٔ آمٔژ زِ کانہ کَتھ گٔژھ نٔ دُبارٔ لےکھنٔ ینی۔ ےێدوَے اَتھ منز، ہَتھ 
فیصدی کامےابی ہُند دعوا کرُن غلط چُھ۔ ےۆتاں کأشرین لُکٔ بأتَن ہُند تالُق چُھ۔ 
یہندی چھِ اَز تاں نَو جلد شاےع
سَپدی مٔتی، أٹھ وادی ۂندین لُکٔ بأتَن ہندی تٔ اکھ ڈوڈہ ضِلٔ کین لُکٔ بأتَن 
ہُند۔ یمَن لُکٔ بأتَن
پیٹھ کَتھ کرنٔ برونٹھ چُھ ےَتھ کَتھِ واَ کڑُن مُنأسب باسان زِ لُکٔ بأتَن چُھ مُختلف 
قٔم آسان۔
وَنٔ وُن، رۆو، چَھکِر بأتھ، مَنزلی بأتھ بےتر۔ وَنٔ وُن ہێکِ مزید دۄن قَسمَن ہندین 
بأتن منز
تقسیم سَپدِتھ نَژن بأتھ تٔ ہُری۔ ہُری چھِ أکی سٔے جاےِ بِہِتھ ےا مہرازس پَتٔ 
پَتٔ دٔری گٔنڑِتھ 
وَننٔ یوان ، خاندرٔکِس پریتھ واقس مُتلق مأنز لاگَن وِزِ، آب شیرن وِزِ، مَس کاسَن وِزِ.
مہرازٔ پأرَن وِزِ، سَبِ بَتٔ دِنٔ وِزِ بےترِ تی نژَن ہُری چھِ الگ آسان۔ ےۆلِ مہرنِ ےا 
مہرازس
مأنز لاگنٔ لوان چھێِ، مہرنِ پَلَو لاگنٔ وَتٔ. مہرازٔ ہوُور گٔژھِتھ مہرێنی وأرُےو 
وأتِتھ، تَتِ بِہِتھ
تٔ تَس مۄہَر تُلنٔ پَتٔ چھێِ اکھ زنانٔ تھۆد ؤتِھتھ نَژان تٔ نَژن بأتھ وَنان تٔ باقٔے 
زنانٔ
ووج دِوان۔ مثالِ پأٹھی ےِ بأتھ ع
						تختَس بےُوٹُھکھ بختَس چُھےو مُبارکھ
وَنٔ وَنٔ کین یمَن دۄشۄنی لِکٔ بأتَن ہُند آہنگ چُھ بےۆن بےۆن تٔ لُکٔ باپتَن ہِندی 
مأہر چِھ یم
فورن پَرزٔنأوِتھ ہیکان۔ رۆو چُھ سُ بأتھ ےُس عید دۄہ. ماہ رمضانٔ چَن رأژن ےا 
کُنِ بٔڑِس
دۄہَس پیٹھ زنانٔ دٔری گٔنڑِتھ أکِس خاص آنہگس پیٹھ پَتھ تٔ برونٹھ پکان پکان 
وَنان
چھێِ۔ نیندَن گۄڈ رو کَران ےا مکاےِ ژُور کَران یم بأتھ وَننٔ یوان چھِ تِم گٔے 
بأتھٔے۔ یم چھِ 
عۆمُومَن کاعشِر شأےری ۂندی وَژَن ےا گیت ہِوی بأتھ آسان۔ چھکرٔ گٔےِ الگ، تِم 
بأتھ یم 
سازَس پیٹھ گیَونٔ یوان چھ۔ ےا وژَن أسی تَن ےا رٔوی۔ یمَن سارِنٔے بأتَن چُھ بےۆن 
بےۆن آہنگ
تٔ تُلٔ تراو۔ لِکٔ بأتَن ہِنزن سۆمبنَن منز چِھ یم اَکھ أکِس سٔتی رَلاونٔ آمٔتی تٔ 
أہندِ بےۆن
 </p>

<p>								( 7 )   7
آہنگ آسنُک چِھُنٔ کانہ خےال تھاونٔ آمُت۔ مِثالِ پأٹھی کأشِری لُکٔ بأتھ جلد اول 
چھاپھ 
1965ئ سۆمبرَن وألی موتی لال ساقی تٔ نا جی منور، ترتیب علی محمد لون صفٔ 64 
ہَس پیٹھ لےکھنٔ
آمُت ےِ بأتھ
				ژُورِ ےار ژۆلُم تے ڈےۄٹھُم ناے ڈےُوٹھُم ناے
ےِ چُھ رۆو پۆز نیندٔ بأتَن منز چُھ شأمِل کرنٔ آمُت۔
	یتھَے پأٹھی اَمی سۆمبرنِ ۂندِس 113 صفس پیٹھ لےکھنٔ آمُت رۆو :۔
		گُورٔ گۆرٔ کَرٔےو کَنٔ کے دُورو
ےُس منزٔلی بأتَن ۂندِس کھاتَس تراونٔ آمُت چُھ۔ ظأہر اَمِ کِنی زِ اَتھ منز چُھ 
گُورٔ گُور لفظ۔
حالانکِ کَنٔ دُورس تِ چُھ گُورٔ گُور کرُن گۄو ٹاٹھِنےار وَرتاونٔ علاؤ کأنسِ پیٹھ ناز 
کرُن تِ۔
	نأظر کۄلگأمی سٔنز لِکٔ بأتَن ۂنز سۆمبنِ منز تِ چھێِ یژھ مِثالٔ نظرِ 
گژھان۔ مثالِ
پأٹھی لُکٔ بأتھ جلد 6کِس 41مِس صَفس پیٹھ درج ےِ بأتھ :۔
گۆبرٔ لالو لٔج ےو پَنٔنی مأج ےے
چُھ منَزٔلی بأتَن ۂندِس حِصَس منز پۆز ےِ چُھ مےأنی کِنی نَژَن ہُر۔ یتھَے پأٹھی 
اَمی سۆمبرنِ 
ۂندِس 99 صفس پےٹھ:۔
					ہا شاہ ۓندرازٔ نی کُوری ےے
								ےا
					بۆمبُر ےێمبٔر زلے دراو
دۄشوَے چھِ نژَن ہُری پۆز چھکرِ بأتَن ہینز گێجِ گنڑنٔ آمٔتی۔ پیٹھٔ چھێِ عجیب 
کتھاہ ےِ زِ رۆو تٔ چھکرِ بأتھ چُھ لِکٔ بأتَن ہُند اَکُے قٔسم ماننٔ آمُت۔
 
								( 8 ) 8
لُکٔ بأتھ سۆمبرَن والیو چُھنٔ اَمِ کَتھِ ہُند پُورٔ خےال تھوومُت زِ کانہ بأتھ ما 
گژھِ Repeat ۔ 
کاغز، کتابت تٔ چھپاےِ بےترِ ۂندِس ےَتھ درۄجرس منز یژھِ کَتھِ ہُند خےال نٔ تھاوُن 
چُھنٔ کُنِ
تِ لێحاظٔ مُنأسب لُکٔ بأتھ جلد 2 ترتیب موتی لال ساقی۔ اَتھ منز چُھ در ملک 
ےێنِ وألی گٔے
اظہار۔ اکھ لٔڑی شاہ بأتھ ےُس لُکٔ بأتَن ۂندِس گۄڈنِکِس جلدس منز صفٔ نمبر 74 
تَس پیٹھ درج
چُھ۔ تأجُب چُھ ےِ زِ گۄڈنِکِ جلدٔ کی بأتھ تِ چھِ ساقی صأبنٔے سۆمبرأوی 
مٔتی۔ دۆےمِ جلدکِس صفٔ نمبر
187تَ پیٹھ چُھ وَنٔ ؤنیُک ےِ ہُر
				توہِ تَے اَسِ کٔر پانٔ وأنی دُوستی
				پوستینٔ نٔری آے ألراوان
	ےُس لُکٔ بأتَن ۂنز گۄڈنِچِ سۆمبرنِ ہندِس صفٔ نمبر 252 ہَس پیٹھ درج 
چُھ۔ ےتھَے کٔنی
چھِ وَنٔؤنکی وارےاہ ہُری پِھری پِھری لےکھنٔ امٔتی۔ لُکٔ بأتَن ۂندِس گۄڈنِکس 
جلدس منز تِ چُھ 
کُنِ کُنِ تکرار لَبنٔ یوان۔ صفٔ نمبر 41ہس پیٹھ چُھ ےِ بأتھ درج۔ ٔ گاہے مێِ 
گۄڈ بۆر باغ وَسٔ وُنے
تٔ ےۆہَے بأتھ چُھ پَتٔ صفٔ نمبر 153ہَس پیٹھ درج۔
لُکٔ بأتن ۂنز شێمِ سۆمبرنِ منز چھِ کےنہ بأتھ پِھرنٔ آمٔتی۔ ےانے تِتھی کےنہ 
چیز لےکھنٔ
آمٔتی یم برُونٹھی مین لُکٔ بأتَن ۂنزَن سۆمبرنَن منز درج چِھ۔ مثالِ پأٹھی ےِ 
بأتھ
					 بوزو بلبلو لول چون آمو 
ےِ چُھ اَمِ جلد کِس صفٔ نمبر 151ہس پیٹھ تٔ ےۆہے بأتھ چُھ گۄڈنِکِ جلدکِس صَفٔ 
نمبر 135ہس
پیٹھ درج۔ یتھَے کٔنی وَنؤنی تِ پِھرنٔ آمٔتی۔ اکھ زٔ مثالٔ چھێِ یم صفٔ 188تَس 
پیٹھ
ےِ ہُر :۔
				مہرازٔ لالو کۆت گژَھکھ سالَس
				آب زمزم چھُے پےالَس کیتھ
 </p>

<p>								( 9 ) 9  </p>

<p>چُھ گۄڈنِکِ جلدٔکِس 230 ہَس پیٹھ تِ 
								ےا
						بِسم اللّٰہ کٔرِتھ ہیمو وَنٔ وونُے
							صأحبَن انجام اونُے ےے
	ےُس گۄڈنِکِ جلدٔکِس 167صفس پیٹھ لێکِھتھ چُھ۔
   شُری بأتَن تٔ یژھَن منز تِ چھێِ نأظر صأبن کےنہ پِھری پِھری لێکھی مٔتی۔
بِشتٔ بشتٔ بےارےو کھۆت کھو وَن۔ انٹی منٹی 2   2 اۆکُس بۆکُس 2   2 تُلے لنگُن 
تُلان چُھس۔
یم چھِ شێمِ جلدٔکِس 60 ّ 61 صفس پیٹھ درج ےێلِ زَن یم برونتھَے گۄڈنکِک 
جلدکِس صفٔ نمبر 102ہس
 پیٹھ درج چھِ۔ یتھَے پأٹھی اَمِ جلدٔکِس 87 صفس  پیٹھ یژھٔ پریژٔ ۔ 
آسمأنیپکان جانوارا
میمٔ رنگی پَن۔ رَزِ لمان بأزی گارا تَمیُک مانے وَن یم گۄڈنِکِ جلدٔکِس صفٔ نمبر 
125ہَس پیٹھ
درج چھێِ۔
لُکٔ بأتھ ترتیب دِنٔ وِزِ چُھنٔ ےَتھ کَتھِ کُن تِ زےادٔ توجُہ رُودمُت زِ ےُس تِ بأتھ 
لےکھٔ ہون ےِ
کَرٔہَؤ وارٔ سَرٔ کَھرٔ زِ ےِ چھا لُکٔ بأتھٔے کِنٔ کأنسِ شأےرٔ سُند چُھ۔ کےنژَن 
بأتَن ہنز صُورتھ تٔ
رُہ رُکانٔ چُھ ٔأنی پانَے بارو دِوان زِ بٔ چُھسنٔ لُکٔ بأتھ بٔ چُھس کستاں شأےر سُند۔ 
اگر 
یتھین بأتَن ہُند شأےر اَتھ آےاونٔ۔ محض شَکٔ کِس بنٔ ہَس پیٹھ واتٔ ہَن یم 
ترأؤنی۔ تاہم اے کےنہ 
بأتھ تِتھی چھِ یم  لیکھُت تاں ہیتھ مشہُور چھ۔
	لُکٔ بأتھ جلد اول چھاپھ 1965چ سۆمبرَن وألی ساقی تٔ نأظر ترتیب عل، 
محمد لون صفٔ 
45ہَس پیٹھ چُھ ےِ بأتھ درج:۔
						کَتِ پھۆلٔ ہَم لو گۄلابو لو
ےِ چُھ مشہُور شأیر اسد میرُن بأتھ ےَتھ وۄنی بشیر اطہرن کلام اسد میر ترتیب دِتھ 
تصدیق 
کۆرمُت چھ۔ ؤچھو کلام اسد میر صفٔ 84۔ اَتھ چھێِ رَژھ کھنڑ صُورتھ ڈالنٔ آمٔژ۔ 
اَمی جلدٔکِس
 
								( 10 ) 10
115 صفس پیٹھ ےِ مَنزٔلی بأتھ
				دۄد چتو دامٔ دامٔ گلِ گلے
				ہو ہو کرٔےو اَڈٔ کلے
	ےِ چُھ محمُود گأمی سُند تٔ تسٔنز لأل مجنُون مَثنوی منز چُھ درج۔
لُکٔ بأتھ جلد 2 ترتیب موتی لال ساقی۔ صفٔ 80تَس پیٹھ چُھ ےِ وَنٔ وُن درج:۔
					شوقٔ سان وَنٔ ووے تازٔ بٔ تازٔہ
					حضرت سون مہرازے آو
ےو وَنٔ وُن چُھ  حضرت خدےجِ رضی اللّٰہ عنہا  مثنوی منز درج لُکٔ بأتھ جلد 
3کِس صفٔ 56ہَس پیٹھ درج
ےِ بأتھ :۔
					تا شوق چون چُھم ماشوقٔ مےانے
					مےلتو لعل میر جانَے سٔتی
ےِ چُھ محمُود گأمی سُند تٔ تَسٔنز محمُود غزنوی نأوی مثنوی ۂندِس نُوراللّٰہ خان 
تٔ لعل میر جان
قٔصز سٔتی چُھ تالُق تَھوان۔ لُکٔ بأتھ جلد 6کِس صفٔ نمبر 106ہَس پیٹھ درج ےِ 
بأتھ :۔
					ماجِ وۆدےو راجِ گۆبرو رود ہمو
چُھ محمُود گأمی سٔندِس  ناد لاےے شاہ ےُوسفو پۄلو  بأتس سٔتی تالُق تَھوان تٔ 
امی جلدٔ کِس
 177 صفس پیٹھ درج ےِ بأتھ چُھ محمُود گأمی ےَن تٔ تَسٔنزِ ےُوسُف زلےخا مثنوی 
منز موجُود ۔
				وۄلو مو ژَل وۄلو مو ژَل
صفٔ 273تَس پیٹھ درج بأتھ :۔
				لارس پَتٔ نَتٔ مارس پان
چُھ ولی اللّٰہ متو سٔنز  ہی مال  مثنوی منز مےلان۔
 
								( 11 ) 11
270 صفس پیٹھ درج ےِ وَنٔ وُن :۔
				نبی(ص) ےَس کُن ہے گنڑوے دٔری ےے
				ژرٔی ےے درُودٔ پٔری توسے
چُھ مشہُور نعت گو شأےر عبدالاحد نأدم سُند۔ ےتھے کٔنی صفٔ 169تَس پیٹھ درج 
بأتھ
				دۄن جُدأےی مو گٔژِھن جورے
	اَمِ علاؤ چھِ یمَن سۆمبرنَن منز کےنہ بأتھ تِتھی تِ یم نَنِ وانٔ باسان چھِ 
زِ لُکٔ بأتھ گژھَن
نٔ آسٔنی پۆز بٔ ہےۆکُس نٔ یہُند پُورٔ پَے پتاہ حأصل کٔرِتھ تِکےازِ مضمُون لےکھنٔ 
حأصل اوس
وَق سیٹھاہ کَم۔ بٔ دِمٔ کےنہ مثالٔ :۔
					گُل پھۆلی تے بہار چھاوو
					بوزو بُلبلو لول چون آوو
								کأشِری لُکٔ بأتھ جلد اول صفٔ 135
					سُلِ پھۆل ےاون مےون گلِ ٹُورا
					سَن دِتھ گوم سٔنی ژُوراہ اوس
								کأشِری لُکٔ بأتھ جلد2
					وَنتو کاوو بےُوٹھُھکھ کَتھ تھرے
					چُھما گرِ گرے جِگرس داغ
								کأشِری لُکٔ بأتھ جلد 4
 </p>

<p>								( 12 ) 12
					رٔنگی مَےو مأنزے نَم
					کٔمےُو  ےارَن دِتُے برْم
							کأشِری لُکٔ بأتھ جلد 6 صفٔ 115
					عید آےِ تَے روزِ وێسی 
					سوز دل ہے چُھم وزان
							 کأشِری لُکٔ بأتھ جلد 6  صفٔ 155
	اَندس پیٹھ ونٔ بٔ یہَے کَتھ زِ کأشر فوک لور چُھ وُنِ سۆمبراونے یوان، تحقیقُک
مٔنزِل ووت نٔ وُنِ۔ تاہم اگر أسی سۆمبران سۆمبران اَمِ کِس تحقیقس کُن تِ خےال 
تھاوو، أسی 
بَچاوَو پَنُن وَق تٔ کم کٔرِتھ کرأوِتھ زےنَو زےادٔ۔ لُکٔ بأتھ سۆمبرَن والےو چھێِ 
پنٔنی کِنی سیٹھاہ مێحنَتھ
کٔرمٔژ ےَتھ داد نٔ دےُن نانصأفی چھێِ۔ تاہم اگر تِم یمَن سۆمبرنَن ۂندین دێےمین 
ایڑےشنَن وارٔ
نظر گُزر دِتھ بازر تراوَن ناکارٔ گژھِ نٔ۔
    کأمِل صأب :۔ لُکٔ ادبُک تحقیقی مطالٔ؟ کِتھٔ پأٹھی ہیکو ؤنِتھ زِ ےِ گژھِ 
			منزٔلی بأتھ آسن تٔ ےِ
			گژھِ رُوو آسُن۔
			ےِ اوس نٔ مےون موضُوعٔے
	 بشیر اختر :۔ پریتھ لُکٔ بأتَس پَتٔ چھێِ اکھ ہسٹری تَتھ مُتلق گۆژھ 
			تحقیق سَپدُن۔ مثالِ پأٹھی
			پنٔ ؤتھرَن کےاہ چُھ لێکِھتھ۔ شےخ محمد عبداللّٰہ اَتھ پَتٔ چھێِ اکھ 
			ہسٹری
			:      تِ گژھِ فوک لورس منز شأمِل سَپدُن۔
			: ۔ وان ےانے ٔ وون وان  وان وہرأوِتھ مرنَس پیٹھ ےِ وَننٔ 
				یوان چُھ۔ ےِ 
				چُھ تراونٔ آمُت۔ کأشرین محاورن منز کِتھ کٔنی چھێِ بےۆن 
				بےۆن ذأژن ہُند ےا
				حرفن ہُند مزازٔ ہاونٔ آمُت۔</p>

<p> 
									پرۄفےسر پُشپ</p>

<p></p>

<p>
			0کأشِر لُکٔ اَدبٔچِ وَنتٔی ذأژ</p>

<p>
	کأشُر لُکٔ وَنُت چُھ دۄےِ آےِ اَس تام ووتمُت۔ اکھ کَتھٔ کَتھِ رٔنگی تٔ دۆیُم 
گیون
لَےِ منز۔ تأجُبا چُھ نٔ ہرگاہ کےنہ حصاہ اَمیُک دۄشؤے آےَو بارسَس آمُت چُھ۔
	وێٹھی پُوٹھی پأٹھی ہێکی زِ ؤنِتھ زِ کَتھٔ کَتھِ رٔنگی اَسِ تام ووتمُت لُکٔ 
وَنُت گۄو لُکٔ کَتھ،
گیون لَےِ منز رٔژھراونٔ آمُت لُکٔ ونُت گۄو لُکٔ بأتھ تٔ دۄشونی ہُند مِلٔ مِش گۄو 
داستان۔
اما ےێلِ أسی یمَن ترین مُولٔ وَنٔتی ذأژن سَنان چھِ یمن أندی پٔکھی تٔ اَندر تِ 
چھێِ اَسِ کےنہ
وَنیتی ذأژ ےا ذألی ذأژ نظرِ گژھان۔ یمن سارِنٔے سام ہینٔ خأطرٔ چُھ ضۆرُوری زِ 
لُکٔ وَنٔ کِس 
مُولٔ تصورس پیٹھ تِ تراوَو نظراہ۔
 
							( 14 ) 14
	انگریزی پأٹھی ےَتھ فوک لور وۆنُکھ تَتھ اندر چھِ وَنتٔ علاؤ ےَژھ پَژھ، شِگ 
شنِکھ
تٔ ریتھ ریواج ہِوی سمأجی پہلُو تِ وۄکھناونٔ یوان تٔ لُکٔ وَنُت بُوزی تون فوک لور 
ناوکِ
زأنی میراثُک انگا اکھ لُکٔ ادب تِ ہےکَو ؤنِتھ ہاگاہ ادب لفظُک ؤسی مانِ مطلب
نِمَو۔ ےانِ لفظن ۂندِ ذرٔی ےِ ےِ کےنژھاہ ظأہر کرنٔ یےِ تٔتھی دَوپُکھ وَنُت تٔ قلم 
بند
سپدنٔ برۄنہ زےێِ پیٹھ کٔھسِتھ تٔ سٔنٔ بٔ سینٔ رٔژھراونٔ یتھ لُکٔ انہار رَٹنٔ کِنی 
چُھ کانہ
زےۆِ نےران تٔ سینٔ بٔ سینٔ للٔ ناونٔ یتھ زےۆِ ژھوہ ماران۔ ےِ بُوزی تَو تِژھَے ہِش 
عملا یژھ دٔپتی، گنڑتِےِ تٔ پرژھٔ نٔنی راوان چھێِ۔ کانہ تُرکھاہ چُھ کُنِ خاص 
موقس پیٹھ 
خاص حالاتَن منز تِژھ شرپٔ ؤنی تٔ لُوبٔ ؤنی کَتھاہ وَنان زِ مُولٔ ونن وألی سُند 
چُھ نٔ اَتھ 
پیٹھ کُنِ قٔسمُک اجارَے روزان، ےێمِ کِنی سأری بوزن وألی چھِ اَتھ پنُن زأنی 
تھٔے پھرکاوان
تٔ برۄنہ برۄنہ پکناوان۔ لُکٔ بأتھ تِ چُھ یتھَے کٔنی کرتام کٔمی تام گۄنٔ ماتن پَننِ 
آےو وۆنمُت
تٔ بوھن والیو چُھ ےِ سرۆد ذأنِتھ لِکٔ آےِ گینگٔ روومُت ےا گےۆومُت تٔ اَتھ 
دورانَس منز 
 چُھ ےِ لُوکَو رَندٔ کٔڑِتھ بدلؤنِ انہارٔ تِ برۄنہ پکنوومُت۔ بٔلی کِ کےنہ تِتھی بأتھ 
تِ چھِ اَمِ 
آےِ اَسِ تام وأتی مٔتی یم زن لیکھتٔ مُوجُوب کاَنسِ خاص گۄنٔ ماتٔ سٔندی ؤنِتھ 
ہێکی زِ تٔ تکٔرِتھ
چُھ کأتِہَو شأیرَو لُکٔ آےِ تِ وارےاہ کےنہ تِ وۆنمُت ےَتھ لُکٔ ونُت وَنٔ نَس تھۄس 
ہِش
چھێِ یوان
	لێحازا چُھ بُنی ےأدی سوال وۄتھان زِ لُکٔ کَتھ کَتھ ؤنی زِ۔ تَتھ پےا
	( 1 ) ےُس کانسِ گۄنٔ ماتَن پَنُن لیکھُت دِنٔ ورأےی لُوکَن برۄنہ کَنِ تراویو تٔ پَتٔ 
کٔمی تام
بوزن وألی تَمی آےِ رٔژھرأوِتھ نۆن کجیو؟
	( 2 ) ےُس کأنسِ گۄنٔ ماتَن پَنُن لیکھُت دِتھ لُوکن برۄنہ کَنِ تراویو تٔ پَتٔ بوزن
 </p>

<p>									( 15 ) 15 
والیو وِزِ پننِ پَننِ آےِ کم کاس ہےر پھےر ےا بٔرژر کٔرِتھ اَسِ تام واتنوو۔
	( 3 ) ےُس کأنسِ گۄنٔ ماتٔ سٔندی نأوی زاننٔ یتھ تِ لُکٔ میراثُک اَژھےۆن 
سرماےِ بنےومُت
آسِ؟
	( 4 ) ےُس کأنسِ گۄنٔ ماتٔ سُند وُرکار کلام أسِتھ تِ چھکرٔ ےا رٔوِس منز پےش 
کرنٔ
یوان چُھ؟
	( 5 ) ےُس کأنسِ  گۄنٔ ماتَن حال حالٔے لُکٔ آےِ لےُوکھمُت چُھ، اما کٔمی تام 
سۆمرن کارن 
پرون لُکٔ بأتھ مأنِتھ ےا فرض کٔرِتھ چھاپ کرنوو؟
	اَتھ بُنی ےأدی سوالَس سٔنی تَو تِ باسیو زِ لُکٔ ونُت تٔ لُکٔ شأیر سُند وَنُت 
چھ پانٔ وأنی کُنِ حدس تام ہِوی أسِتھ تِ ذأژ کِنی اَکھ أکِس نِشِ بِدُون، تِ کےازِ 
لُکٔ شأےر سُند
وَنُت چُھ کأنسِ أکی سُندُے کلام زاننٔ یوان ےێلِ زَن لُکٔ وَنُت چُھ نٔ کأنسِ أکی سُندُے 
کلام زاننٔ یوان، بٔلی کِ چُھ پریتھ أکی سُند بٔنِتھ درُس سماجُک سَنژیتھ ماننٔ یوان۔ 
کأنسِ أکی سُندُے مُولٔ ونُت ثأبِتھ سپدنٔ پَتٔ ہرگاہ ےِ لُوکَو پَننِ آےِ نشنےومُت آسِ، 
اَتھ لُکٔ
وَنُت بنےومُت۔ حبٔ خوتنِ تٔ أری نِ مالِ ۂندی خبر کأتےاہ وَژن آسَن یتھَے کٔنی لُکٔ 
وَنُت
ماننٔ آمٔتی تٔ تأجباھ چُھنٔ اگر کینہ لُکٔ وَنتٔی تِ آسَن حبٔ خوتنِ تٔ أری نِ 
مالِ ہِندِس شار
کھاتَس تراونٔ آمٔتی
	خأر لُکٔ وَنُت ہیکو مُلٔ تَلٔ یمَن پانژَن ونٔتی ذأژن اندر ویژھنأوِتھ۔
   اکھ : لُکٔ کَتھ۔ دۆےُم: لُکٔ بأتھ۔ ترےیم : داستان۔ ژُورےم : دپُت تٔ 
پنژےم  پریژھ تٔ کأشُر لُکٔ ادب بُوزی تَون یمنٔے پانژن مُولٔ ذأژن ہُند جہار۔
سُ گۄو یتھٔ کٔنی:
 </p>

<p>								( 16 ) 16 
0لُکٔ کتھ :۔ سۄ کتھ ےۄس لُوکھ پَتٔ وَتھ اَکھ أکِس بوزناوان آمٔتی 
چھِ۔پےشٔ ور
		   کتھ گۆر تٔے پانٔ بوزناوِ سُ تِ چُھ وۄستاد سٔندی طرفٔ بوزناونُک 
			دَم بران۔ شۆرۄع چُھ ےِتھٔ کٔنی کران :۔
			 دپان وۄستاد۔ ےِ لُکٔ کَتھ چھێِ بسےار لنگ لَنجِ کڑان تٔ رنگٔ 
			رنگٔ ترکیبَن کأم ہیوان۔ کَتھِ سٔتی کَتھ، کَتھِ منز کَتھ، کَتھِ منز 
کَتھ،
			تٔ کَتھِ أندی أندی کَتھ بےترِ بےترِ۔ اَمِ چِ ضألی ذأژ چھێِ یمٔ:
	( 1۔0 کتھاےِ :۔0 دےو مالأےی، مزہبی تٔ اێخلأقی قٔصٔ یمَن منز تیرتھَن ہنز 
مہاتمٔ
			کَتھٔ تِ شاپمل چھێِ۔ مثلن :۔ ستی سرٔچ کتھ، وۄلرٔچ کتھ ےا
			کونسر ناگٔچ کَتھ۔ پَن دِنٔچ کَتھ ےا ستےِ نارأےن کتھا تٔ ہرژنٔد 
رازٔنی
			کَتھ
	2۔ دینےأتی قٔصٔ :۔ یمَن اَندر حدیث تٔ کشف کرامات تِ شأمل چھِ تٔ
			رُوحأنی بُزرگَن ۂندی قٔصٔ۔ مثلن :۔
			سیرت النبی صلی اللّٰہ علےہ وسلم چِ ریواۓژ، تخت سلےمانُک قٔصٔ، 
چاہ بابل، ہارُوت
			مارُوت صأب۔
	3۔ دٔلیلٔ :۔ عجیب و غریب کِردارن ےا ظۄنن أندی أندی وونن آمتین
			واقاتَن ۂنز کٔری ہانکل ہِش پےش کرٔؤنی دِل چسپ قصٔ،
			یم رنگٔ رنگٔ چھِ :
	(1 )    پادشاہ کَتھٔ: رازن تٔ رانین ۂنز کَتھ یمَن ؤزیرن،
			ؤزیر زادن تٔ ؤزیر زأدےِن ۂندی قٔصٔ تِ آمٔتی چھِ تٔ مٔنزی
			مٔنزی جِنَن تٔ پٔری ےَن سٔتی بُتھی لاگےِ ۂندی بےان تِ ووننٔ 
آمٔتی</p>

<p> 
									( 17 ) 17
			چھِ
	( 2 )     پٔری کتھٔ :۔ یمَن اَندر جِنٔ کتھٔ تٔ طِلسمأتی قٔصٔ تِ شأمِل 
چھِ۔
		3.۔ جانور کَتھٔ :۔ یمَن منز کُنِ کُنِ کُلی کٔٹی تِ کَتھٔ کرنِ لگان چھِ۔
		4۔ گاٹٔ جار کتھٔ :۔ یمَن منز پریژٔ تِ وَرتانٔ یوان چھێِ تٔ مُشکِلو منزٔ 
نےرٔنچِ
		ژھۄمبٔ وۄتلان چھێِ۔
		5۔ چأری کَتھٔ :۔ یمَن منز بێِ شۆعُورن ۂندی پھر تٔ پأتھٔر تٔ پھےار 
لُطف
		دِوان تٔ اَسناوان چھِ۔
		6۔ اَسن کَتھٔ :۔ یمَن منز چاٹھ، جٹٔ، مسخٔرل، طنز، ہَزٔل تٔ ہجو بےترِ
			اَسَن تراےِ تِ شأمِل چھێِ۔
			ظأہر چُھ زِ یمَو منزٔ کےنہ ذألی ذأژ چھێِ اکھ أکِس نکھٔ آکھ تِ 
			تٔ نألی نال تِ۔ مثلن :
			چأری کَتھِ منز چھێِ اَسَن تراے تِ روزانٔے تٔ جانور کَتھِ
			منز چھێِ چأری کَتھ ےا گاٹٔ جار کَتھ تِ ویپانٔے۔ یتھَے کٔنی 
		چھێِ یمَن ترێشؤنی ہنز گُنجأیش پادشاہ کتھِ ےا پٔری کَتھِ منز تِ 
		آسانٔے تٔ بانڑ پأتھرِس منز چھِ یم سأری وَنٔتی آے
		نقلس تابے روزان، بٔلی کِ لُکٔ بأتُک اَتھٔ واس تِ پراوان۔
0لُکٔ بأتھ :۔0 لُکٔ بأتھ تِ چُھ وارےاہِ رٔنگی پرێتھےوومُت :
	1۔ وَنٔ وُن : ( ےَتھ اندر ہێنزے تِ شأمل چُھ) ۔ وَنٔ وُن چُھ کھاندرن
				کُھندرن تٔ بڑین دۄہَن خاص شُوب اَنان۔ وَنٔ وَن چِ
				لَنجِ ۂندِس أکِس کٔرِس چھِ وَنان ہُر، تِ کےازِ مُلٔ تلٔ چُھ</p>

<p> 
								( 18 ) 18
		ےِ دِمصری آسان۔ وِگنِ ہٹین ہُند رَلٔ وُن سوز چُھ اَتھ سیٹھٔے
		دِلکش بناوان۔ ےۆہَے ہُر ےێلِ سیٹھٔے زےچھِ لَےِ منز درْسٔ دی دی
		پےش کرنٔ یوان چُھ، اَتھ چھِ وَنان ہێنزے۔
		ہێنزےأنی لفظس سٔتی چُھ نٔ اَتھ کانہ تِ واٹھ، بٔلی کِ چُھ ےِ 
پرانِ (پراکرت،
		اپِ بھرنُش) ہنْجے ( ہن جے ) لفظُک بدلےومُت رٔپھ۔تِ زَن گۄو
		ہےے کٔرِتھ آلَو دِنٔ پَتٔ وَنٔ ناوُن۔ چنانچِ بَٹنِ چھێِ اَز تِ ہێنزے
		ؤنِتھ ےِ وَنٔ وُن یتھٔ کٔنی شۆرُوع کران:
				شۆکلم کٔرِتھ ہیمو اَنٔ وونُے.....
			ےانِ شۆکلامبَر دھرَم دےوم شلُوک پٔرِتھ، ہیمو أسی وَنٔ وُن .....
		تٔ مسلمان پأٹھی چُھ وَنٔ وُنِس یتھٔ کٔنی بسم اللّٰہ یوان کرنٔ :
					بِسم اللّٰہ کٔرِتھ ہیمَو وَنٔ وونُے ....
		( ےانِ بِسم اللّٰہ ارّحْمٰن  الرَّحےْم .... پٔرِتھ ہیمَو أسی وَنٔ وُن .... )
	وَژُن : وَژُن چُھ مُلٔ تَلٔ ژُ مصری شارن ۂنز ہانکل ےَتھ ژُوریُم مِصرٔ ہِوے 
		آسنٔ کِنی ووج بَنان چُھ۔ ریواےتی اندازٔ بُوزی تون وژُن
		کأشُر گیت تٔ ےێلِ ےِ وَژُن کھاندر کُھندر کِ موقٔ تمیُک نارِ کھسان
		چُھ، أتھی چھِ وَنان تُمبک نارِ وَژُن۔ چھکرِ اندازٔ پےش کرنٔ یتھ گۄو
 		ےۆہَے چھکرِ وژُن۔ نَتٔ چَھنٔ چھکرٔ کانہ وَنٔتی ذاتھ بٔلی کِ چھێِ ےِ لُکٔ
		مُوسیقی ۂنز اکھ گےون تراے۔
		وژنٔکی خاص قٔسم چھِ یم :
	لولٔ وژُن : ےَتھ اندر لول ژھٹنٔ یوان چُھ نَتٔ لولٔ ویداکھ دِنٔ یوان
 </p>

<p>						( 19 ) 19 </p>

<p>		چِھ۔ صُوفےانٔ کلامَس اندر چھِ اَز تِ وارےاہ لُکٔ وژن اَمی قٔسمکی گیونٔ
		یوان۔
	(2) لیلاےِ وَژُن : ےَتھ اندر دےِ لول ےا بھکتِ باو ژھۄہ مارِ، نَتٔ گےان دےان دِلٔچ
			دُبٔ راے بَنِ۔
	(3 ) نعتےِ وژُن 	 ےَتھ اندر آنحضورن (ص) لول برنٔ یےِ۔
	( 4 ) منقبت وژُن : ےَتھ اندر دینٔ کین بزرگن ( ؤلی ےن، رێش ےا مُرشدن )
			عقیدتُک نزرانٔ پےش یےِ کرنٔ۔
3۔ نَژَن بأتھ  : یم مۄژان مۄژان ِۄنی دۄےی قٔسمٔکی رُودی مٔتی چھِ :
		( 1 ) ویگی وژُن: وژُن ناو أسِتھ تِ یُس عام وژنٔ نِشِ ہنا بےۆن باسان
			چُھ تِ کےازِ ۓژ پأٹھی چُھ ےِ دِنی دُمصری رُودمُت۔ ےِ ویگی وژُن
			آسِ بٹنِ تَتھ ویگِس پیٹھ نژان ےَتھ کٔھسِتھ مےکھلِ مہاررازٔ پَتٔ
			مندرس ےا دیوی بل گژھان اوس تٔ مہارازٔ برات ہیتھ نےران
			اوس ےا مہارِنی ہیتھ گرٔ واپس پھےران اوس۔
		( 2 ) رۆفی بأتھ :۔ ےُس زنانٔ بڑین دۄہن پیٹھ خاص کٔرِتھ عید پیٹھ 
دٔری
			کٔری کٔری گراےِ ماران ماران گےوان چھێِ۔ اَمِ بأتُک آہنگ
			چُھ نَژن گراین تٔ ڈالن ۂنز تُلٔ تراوِ مطأبق آسان تٔ بأتُک
			دۆےُم مِصرٔ چُھ ( ۓژ پأٹھی ) دۄےِ دۄےِ پِھرِ ادا کرنٔ یوان۔ مثلن:
							عید آےِ رسٔ رسٔ
							عید گاہ وَسٔ وَے
							عید گاہ وَسٔ وَے ......
 
							( 20 ) 20
	( ب) 		سٔمی وے وِگنیو رۆف ہے کرٔوَے
			سنگر مالن ژھاےے لو لو      سنگر مالن ژھاےے لو لو  ......
	( ج )    ےِ چُھ دُنی ےاہ نَوِ کھۄتٔ نۆؤےے لو
			 بألی ےاونَس کَرٔوَے رۆف ےے لو
	
لڈی شاہ :۔   ناسٔ تراس حالاتن ےا وێکھژارن پیٹھ ٹیپ ژٹٔ ؤنی شار
			چھِ مُلٔ تَلٔ اَسَن تراےِ ہندی منظُوم انہار۔ بُوزی تَو ےۆہَے
			شہر آشُوب ہےُو۔ دۆہرس پیٹھ پےش کَرن وول لڑی شاہ
			چُھ اَتھ وِزِ کےنہ نَتٔ کےنہ وۄکھنَے کرانٔے روزان۔ چنانچِ
			 گۄو کُنُے کَھنٔ بل کھادن ےار، تِ چُھ اَسِ تِتھُے ہےُو لُکٔ ونُت
			باسان ےُتھ ، ہوأےی جہاز آو مُلک کشمیر، چُھ ۔ تِ کٔرِتھ لالٔ لخمن
			سٔندی بِسےار بأتھ چھِ نا اَمی حسابٔ لُکٔ وَنٔتی بنےمٔتی۔
	( 5 ) شُری بأتھ :۔  یم بأتھ شُری اکھ أکِس رَنزٔ ناونٔ خأطرٔ بوزناوان 
چھِ،
			نَتٔ دِل بہلأےِ باپَتھ ےِکٔ وَٹٔ گیوان چھِ۔ یم چھِ دۄےِ قسمٔکی:۔
		( 1 ) وَنَن بأتھ : یم ۓژ پأٹھی گِندنٔ رۆستُے پےش یوان چھ کرنٔ
			حالانکِ یم ہیکن گِندنٔ تِ یتھ۔
			مثلن:۔
				 بِشتٔ بِشتٔ برارےو کھۆتکھو وَن 
						یا
				ہۆپٔ لێلِ ژھوپٔ کر .......</p>

<p></p>

<p> </p>

<p>
               0 
</p></body></text></cesDoc>