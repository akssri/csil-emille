<cesDoc id="pun-w-sanjh-news-02-04-01" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-02-04-01.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 02-04-01</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>02-04-01</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਰਵੀ
ਸਿੱਧੂ ਦੇ ਲੱਖਾਂ ਰੁਪਏ ਦਾ ਬੈਂਕ ਬੈਂਲਸ ਅਤੇ ਕਰੋੜਾਂ ਦੀ ਜਾਇਦਾਦ ਸੀਲ</p>

<p>ਖਰੜ : ਖਰੜ ਦੇ
ਜੁਡੀਸ਼ਿਅਲ ਮੈਜਿਸਟ੍ਰੇਟ ਸ਼੍ਰੀ ਰੋਸ਼ਨ ਲਾਲਾ ਚੌਹਾਨ ਨੇ ਅੱਜ ਪੰਜਾਬ ਪਬਲਿਕ ਸਰਵਿਸ ਕਮਿਸ਼ਨ ਦੇ
ਚੇਅਰਮੈਨ ਸ਼੍ਰੀ ਰਵੀ ਸਿੱਧੂ, ਜਿਸ ਨੂੰ ਪੰਜਾਬ ਵਿਜੀਲੈਂਸ ਵਿਭਾਗ ਵੱਲੋਂ 5 ਲੱਖ ਰੁਪਏ ਦੀ ਰਿਸ਼ਵਤ
ਲੈਣ ਦੇ ਦੋਸ਼ ਅਧੀਨ ਗ੍ਰਿਫਤਾਰ ਕੀਤਾ ਗਿਆ ਸੀ, ਨੂੰ ਅੱਜ 3 ਅਪ੍ਰੈਲ ਤਕ ਮੁੜ ਪੁਲਿਸ ਰਿਮਾਂਡ ਵਿਚ
ਭੇਜ ਦਿੱਤਾ ਸੀ ਅਤੇ ਹੁਕਮ ਦਿੱਤਾ ਕਿ ਉਸਨੂੰ ਉਸ ਦਿਨ 10 ਵਜੇ ਪੇਸ਼ ਕੀਤਾ ਜਾਵੇ। ਉਸ ਦੀ
ਕਸੌਲੀ ਵਿਚ ਇਕ ਕੋਠੀ ਹੈ ਤੇ ਉਸ ਦੀ ਅੰਦਾਜ਼ਨ ਕੀਮਤ 51 ਲੱਖ 28 ਹਜ਼ਾਰ 300 ਰੁਪਏ ਹੈ। ਵਿਭਾਗ
ਨੇ ਇਹ ਪਤਾ ਕਰਨਾ ਹੈ ਕਿ ਉਸ ਪਾਸ ਇਹ ਰਕਮ ਕਿੱਥੋਂ ਆਈ ਹੈ। </p>

<p>ਗਿਆਨੀ
ਪੂਰਨ ਸਿੰਘ ਦੀ ਗੈਰਹਾਜ਼ਰੀ ਦਾ ਮਾਮਲਾ ਵਿਚਾਰਾਂਗੇ : ਵੇਦਾਂਤੀ</p>

<p>ਅਮ੍ਰਿੰਤਸਰ :-
ਅਕਾਲ ਤਖਤ ਦੇ ਜਥੇਦਾਰ ਗਿਆਨੀ ਜੋਗਿੰਦਰ ਸਿੰਘ ਵੇਦਾਂਤੀ ਨੇ ਦਰਬਾਰ ਸਾਹਿਬ ਦੇ ਹੈੱਡੱ ਗ੍ਰੰਥੀ
ਗਿਆਨੀ ਪੂਰਨ ਸਿੰਘ ਵੱਲੋਂ ਅਕਾਲ ਤਖਤ ਦੀਆਂ ਮੀਟਿੰਗਾਂ ਵਿਚ ਗੈਰਹਾਜ਼ਰ ਰਹਿਣ ਦਾ ਮੁੱਦਾ ਅਗਲੀਆਂ
ਮੀਟਿੰਗਾਂ ਵਿਚ ਵਿਚਾਰ ਕਿ ਕਿਸੇ ਸਿਰੇ ਲਗਾਉਣ ਦਾ ਫੈਸਲਾ ਕੀਤਾ ਹੈ। ਇਨ੍ਹਾਂ ਮੀਟਿੰਗਾਂ ਵਿਚੋਂ
ਗੈਰਹਾਜ਼ਰ ਰਹਿਣ ਨੂੰ ਮਰਿਯਾਦਾ ਦੀ ਘੋਰ ਉਲੰਘਣਾ ਸਮਝਿਆ ਜਾ ਰਿਹਾ ਹੈ।</p>

<p>ਸ਼੍ਰੋਮਣੀ
ਕਮੇਟੀ ਦੇ ਬਹੁਤੇ ਮੈਂਬਰ ਸਿੱਖੀ ਦੇ ਪ੍ਰਚਾਰ ਬਾਰੇ ਸੰਤੁਸ਼ਟ ਨਹੀਂ</p>

<p>ਅਮ੍ਰਿੰਤਸਰ :-
ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ(ਬਾਦਲ) ਨਾਲ ਸਬੰਧ ਰੱਖਦੇ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਦੇ ਮੈਂਬਰਾਂ ਦੀ ਸਥਾਨਕ ਭਾਈ
ਗੁਰਦਾਸ ਹਾਲ ਵਿਚ ਕੱਲ੍ਹ ਦੀ ਹੋਈ ਮੀਟਿੰਗ ਵਿਚ ਮੈਂਬਰਾਂ ਨੇ ਪਿਛਲੇ ਸਮੇਂ ਦੌਰਾਨ ਧਰਮ ਪ੍ਰਚਾਰ ਲਈ
ਕੀਤੇ ਗਏ ਕੰਮਾਂ 'ਤੇ ਅਸੰਤੁਸ਼ਟੀ ਪ੍ਰਗਟ ਕੀਤੀ ਅਤੇ
ਕਿਹਾ ਕਿ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਨੂੰ ਸਿੱਖੀ ਦੇ ਪ੍ਰਚਾਰ ਲਈ ਜੋ ਕੁਝ ਕਰਨਾ ਚਾਹੀਦਾ ਸੀ, ਉਹ ਨਹੀਂ
ਕੀਤਾ ਗਿਆ।</p>

<p>ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ
ਸਾਬਕਾ ਪ੍ਰਧਾਨ ਬੀਬੀ ਜਾਗੀਰ ਕੌਰ ਨੇ ਮੈਂਬਰਾਂ ਨੂੰ ਸੰਬੋਧਨ ਕਰਦਿਆਂ ਕਿਹਾ ਕਿ ਧਰਮ ਪ੍ਰਚਾਰ ਵਿਚ
ਅਸੀਂ ਬਹੁਤ ਕਿਹਾ ਕਿ ਧਰਮ ਦੇ ਪ੍ਰਚਾਰ ਵਿਚ ਅਸੀਂ ਬਹੁਤ ਪਛੜ ਗਏ ਹਾਂ ਤੇ ਧਰਮ ਪ੍ਰਚਾਰ ਦੀ ਲਹਿਰ
ਨੂੰ ਬਲਵਾਨ ਕਰਨ ਦੀ ਲੋੜ ਹੈ। </p>

<p>ਸੁਰਜੀਤ
ਪਾਤਰ ਪੰਜਾਬੀ ਸਾਹਿਤ ਅਕਾਦਮੀ ਦੇ ਪ੍ਰਧਾਨ ਬਣੇ</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>ਲੁਧਿਆਣਾ :- ਪੰਜਾਬੀ ਸਾਹਿਤ ਅਕਾਦਮੀ ਲੁਧਿਆਣਾ
ਦੀ ਅੱਜ ਇਥੇ ਪੰਜਾਬੀ ਭਵਨ ਵਿਚ ਦੋ ਸਾਲਾ ਲਈ ਹੋਈ ਚੋਣ ਵਿਚ ਕਵੀ ਸੁਰਜੀਤ ਪਾਤਰ ਆਪਣੇ
ਵਿਰੋਧੀ ਡਾ. ਦੀਪਕ ਮਨਮੋਹਨ ਸਿੰਘ ਨੂੰ 73 ਵੋਟਾਂ ਦੇ ਫਰਕ ਨਾਲ ਹਰਾ ਕੇ ਪ੍ਰਧਾਨ ਚੁਣੇ ਗਏ ਹਨ।
ਪੋਲ ਹੋਈਆਂ 474 ਵੋਟਾਂ ਵਿਚੋਂ ਸ਼੍ਰੀ ਪਾਤਰ ਨੂੰ 271 ਅਤੇ ਡਾ. ਦੀਪਕ ਨੂੰ 198 ਵੋਟਾਂ ਪ੍ਰਾਪਤ
ਹੋਈਆਂ। </p>

<p>ਬਾਦਲ
ਲਈ ਘਾਤਕ ਹੁੰਦੇ ਜਾ ਰਹੇ ਨੇ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਦੇ ਭਲਵਾਨੀ ਦਾਅ</p>

<p>ਮਲੋਟ : ਨਿਰਪੱਖ
ਮੋਰਚੇ ਵੱਲੋਂ ਕੀਤੀ ਤੀਹਰੀ ਮੋਰਚਾਬੰਦੀ ਅਤੇ ਪ੍ਰਦੇਸ਼ ਕਾਂਗਰਸ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ ਕੈਪਟਨ ਅਮਰਿੰਦਰ
ਸਿੰਘ ਵੱਲੋਂ ਇਸਤੇਮਾਲ ਕੀਤੇ ਤਿੰਨ ਭਲਵਾਨੀ ਦਾਅ ਮਲੋਟ ਦੇ ਚੋਣ ਦੰਗਲ ਵਿਚ ਅਕਾਲੀ ਸੁਪਰੀਮੋ ਸ਼੍ਰੀ
ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਲਈ ਘਾਤਕ ਹੁੰਦੇ ਜਾ ਰਹੇ ਹਨ।</p>

<p>ਸ਼ਿਵ
ਸੈਨਾ ਅਤੇ ਬਜਰੰਗ ਦਲੀਏ ਅੱਤਵਾਦੀਆਂ ਨੇ ਪ੍ਰੈਸ ਕਾਨਫਰੰਸ ਕਰ ਰਹੇ ਗਨੀ ਲੋਨ
ਤੇ ਹਮਲਾ</p>

<p>ਜੰਮੂ : ਟੀ.ਵੀ.
ਚੈਨਲਾਂ ਦੇ ਹਵਾਲੇ ਨਾਲ ਪਤਾ ਲੱਗਾ ਹੈ ਕਿ ਹੁਰੀਅਤ ਨੇਤਾ ਗਨੀ ਲੋਨ ਇਕ ਪ੍ਰੈਸ ਕਾਨਫਰੰਸ ਕਰ ਰਹੇ
ਸਨ ਜਿਸ ਵਿਚ ਦੁਨੀਆਂ ਭਰ ਦੇ ਪੱਤਰਕਾਰ ਸ਼ਾਮਲ ਸਨ। ਅਚਾਨਕ ਸ਼ਿਵ ਸੈਨਾ ਅਤੇ ਬਜਰੰਗ ਦਲ ਦੇ
ਅੱਤਵਾਦੀਆਂ ਨੇ ਉਨ੍ਹਾਂ ਤੇ ਹਮਲਾ ਕਰ ਦਿੱਤਾ ਇਸ ਦੀ ਕਵਰੇਜ਼ ਸਾਰੇ ਟੀ.ਵੀ. ਚੈਨਲਾਂ ਅਤੇ ਪੱਤਰਕਾਰਾਂ
ਨੇ ਦੇਖਿਆ।</p>

<p>ਗੁਜਰਾਤ
'ਚ ਮੁੜ ਲਾਸ਼ਾਂ ਦੇ
ਢੇਰ ਲੱਗੇ</p>

<p>ਅਹਿਮਦਾਬਾਦ :
ਨਵੇਂ ਸਿਰਿਓਂ ਫਿਰਕੂ ਹਿੰਸਾ ਭੜਕ ਉਠਣ ਕਾਰਨ ਗੁਜਰਾਤ ਦੇ ਅਹਿਮਦਾਬਾਦ ਮੇਹਸਾਨਾ ਅਤੇ ਅਣਦ
ਜ਼ਿਲ੍ਹਿਆਂ ਵਿਚ 7 ਹੋਰ ਵਿਅਕਤੀ ਮਾਰੇ ਗਏ ਹਨ ਅਤੇ ਰਾਜ ਦੇ ਕਈ ਹੋਰ ਹਿੱਸਿਆਂ ਵਿਚ ਕਰਫਿਊ ਲਾ
ਦਿੱਤਾ ਗਿਆ ਹੈ। ਲੋਕਾਂ ਦੀ ਭੀੜ ਵੱਲੋਂ ਵੀ ਗੋਲੀਆਂ ਚਲਾਈਆਂ ਗਈਆਂ, ਦੇਸੀ ਬੰਬਾਂ ਦੀ ਵਰਤੋਂ
ਕੀਤੀ ਗਈ ਅਤੇ ਭਾਰੀ ਪਥਰਾਓ ਕੀਤਾ ਗਿਆ। </p>

<p> </p>






</body></text></cesDoc>