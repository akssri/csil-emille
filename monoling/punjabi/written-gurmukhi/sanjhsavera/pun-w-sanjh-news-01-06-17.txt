<cesDoc id="pun-w-sanjh-news-01-06-17" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-01-06-17.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 01-06-17</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>01-06-17</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਦੇ ਇਸ ਹਮਾਮ ਵਿਚ ਸਾਰੇ ਨੰਗੇ, ਭਾਵੇਂ ਉਹ ਕਿਸੇ ਵੀ ਪਾਰਟੀ ਦੇ ਹੋਣ</p>

<p>ਲੋਕਤੰਤਰ
ਦੀ ਦਿਨ ਰਾਤ ਦੁਹਾਈ ਦੇਣ ਵਾਲਿਆਂ ਦਾ ਇਸ ਖਬਰ ਨਾਲ ਮੋਹ ਭੰਗ ਹੋ ਜਾਣਾ ਚਾਹੀਦਾ ਹੈ ਕਿ ਨਾਗਪੁਰ
ਨਿਗਮ ਦੇ ਸਾਰੇ ਦੇ ਸਾਰੇ 129 ਕੌਂਸਲਰ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਵਿਚ ਸ਼ਾਮਲ ਪਾਏ ਗਏ। ਇਨ੍ਹਾਂ ਵਿਚੋਂ ਬਹੁਤੇ
ਸਾਡੇ ਦੁੱਧ ਦੇ ਧੋਤੇ ਭਾਜਪਾ ਨੇਤਾ ਹਨ। ਉਨ੍ਹਾਂ ਦਾ ਹੀ ਇਕ ਮੇਅਰ ਵੀ ਹੈ। ਉਂਝ ਤਾਂ ਇਸ ਟੋਲੇ
ਵਿਚ ਲਗਭਗ ਸਾਰੀਆਂ ਸਿਆਸੀ ਪਾਰਟੀਆਂ ਦੇ ਪ੍ਰਤੀਨਿਧੀ ਹਨ ਪਰ ਭਾਜਪਾ ਭਰਾਵਾਂ ਦੀ ਗਿਣਤੀ ਸਭ ਤੋ
ਜ਼ਿਆਦਾ ਇਸ ਦੇ ਲਗਭਗ 70 ਕੌਂਸਲਰ ਜੇਲ ਦੀ ਹਵਾ ਖਾ ਰਹੇ ਹਨ ਅਤੇ ਜਿਵੇਂ ਕਿ ਉਮੀਦ ਸੀ ਕਿ
ਭਾਜਪਾ ਇਹ ਹੰਗਾਮਾ ਕਰ ਰਹੀ ਹੈ ਕਿ ਮਹਾਰਾਸ਼ਟਰ ਦੀ ਖਿਚੜੀ ਸਰਕਾਰ ਦਾ ਇਹ ਕਦਮ ਸਿਆਸਤ ਤੋਂ
ਪ੍ਰੇਰਿਤ ਹੈ ਪਰ ਲੋਕਾਂ ਦੀ ਉਨ੍ਹਾਂ ਨਾਲ ਕੋਈ ਹਮਦਰਦੀ ਨਹੀਂ। ਬੰਦ ਦਾ ਸੱਦਾ ਵੀ ਠੁੱਸ ਹੋ ਕੇ ਰਹਿ
ਗਿਆ। ਭਾਜਪਾਈਆਂ ਦਾ ਧਿਆਨ ਉਨ੍ਹਾਂ ਮੈਂਬਰਾਂ ਦੀ ਗ੍ਰਿਫਤਾਰੀ ਵਲ ਦਿਵਾਇਆ ਗਿਆ ਤਾਂ ਉਹ ਬੁਖਲਾ
ਗਏ। </p>

<p>ਰਿਟਾਇਰਡ
ਅਧਿਕਾਰੀਆਂ ਨੂੰ ਵੀ ਨੌਕਰੀ ਦੇਣ ਸਮੇਂ ਜਾਂਚ-ਪੜਤਾਲ ਜਰੂਰੀ </p>

<p>ਸਭ
ਪਾਰਟੀਆਂ ਅਤੇ ਸਿਆਸਤਦਾਨ ਸੱਤਾ ਪ੍ਰਾਪਤੀ ਤੋਂ ਬਾਅਦ ਆਪਣੇ ਚਾਪਲੂਸਾਂ, ਪਿਛੱਲਗਾਂ ਅਤੇ ਕਦੇ ਕਦੇ
ਸੁਲ੍ਹਾਂ-ਸਫਾਈ ਦੀ ਖਾਤਰ ਆਪਣੇ ਵਿਰੋਧੀਆਂ ਨੂੰ ਕਈ ਤਰ੍ਹਾਂ ਦੀਆਂ ਨੌਕਰੀਆਂ ਨਾਲ ਨਿਵਾਜਦੇ ਹਨ।
ਇਸ ਪ੍ਰਕਿਰਿਆ ਵਿਚ ਕਿਸੇ ਵੀ ਵਿਅਕਤੀ ਦੇ ਅਤੀਤ ਦੇ ਕਾਰਨਾਮਿਆਂ ਦੀ ਚੰਗੀ ਤਰ੍ਹਾਂ ਛਾਣਬੀਣ ਕਰਨ ਦੀ
ਲੋੜ ਨਹੀਂ ਸਮਝੀ ਜਾਂਦੀ। ਮਹੱਤਵਪੂਰਨ ਅਹੁਦਿਆਂ 'ਤੇ ਉਨ੍ਹਾਂ ਨੂੰ ਲਗਾਉਣ ਵੇਲੇ ਲੋਕਹਿੱਤਾਂ ਨੂੰ ਪੂਰੀ ਤਰ੍ਹਾਂ ਨਜ਼ਰਅੰਦਾਜ ਕਰ ਦਿਤਾ ਜਾਂਦਾ
ਹੈ। ਕਈ ਵਾਰ ਤਾਂ ਇਕ ਹੀ ਕਸੌਟੀ ਹੁੰਦੀ ਹੈ ਕਿ ਫਲਾਣਾ ਵਿਅਕਤੀ ਕਿਸੇ ਵਿਧਾਇਕ ਜਾਂ ਮਹੱਤਵਪੂਰਨ
ਸਿਆਸਤਦਾਨ ਦਾ ਸਬੰਧੀ ਹੈ ਜਾਂ ਪੁਰਾਣਾ ਸੇਵਾਦਾਰ(ਇਸੇ ਰਵੱਈਏ ਦੇ ਚਲਦਿਆਂ 'ਦੇਸ਼ ਦੇ ਅਣਗਿਣਤ ਸਰਵਿਸ ਸਿਲੈਕਸ਼ਨ ਬੋਰਡ,
ਪਬਲਿਕ ਸਰਵਿਸ ਕਮਿਸ਼ਨ ਅਤੇ ਸਿੱਖਿਆ ਸੰਸਥਾਵਾਂ ਬੇਕਾਇਦਗੀਆਂ ਅਤੇ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਦੇ ਕੇਂਦਰ ਬਣ ਕੇ
ਸਾਰੇ ਵਾਤਾਵਰਣ ਨੂੰ ਦੂਸ਼ਿਤ ਕਰ ਰਹੇ ਹਨ। ਅੱਜ ਦੇ ਮਾਹੌਲ ਦੇ ਚਲਦਿਆਂ ਇਹ ਸੋਚਣਾ ਬੇਕਾਰ ਹੈ ਕਿ
ਸਾਡੇ ਨੇਤਾ ਆਪਣੇ ਆਲੇ ਦੁਆਲੇ ਖਾਲੀ ਅਹੁਦੇ ਭਰਨ ਲਈ ਕਿਸੇ ਵਿਅਕਤੀ ਦੀ ਯੋਗਤਾ, ਈਮਾਨਦਾਰੀ
ਵਗੈਰਾ'ਤੇ ਵੀ ਧਿਆਨ
ਦੇਣਗੇ। ਜ਼ਿਆਦਾਤਰ ਵੱਡੇ ਵੱਡੇ ਅਹੁਦੇ ਪੁਰਾਣੇ ਚੇਲਿਆਂ ਅਤੇ ਦਬਾਅ ਦੀ ਰਾਜਨੀਤੀ ਖੇਡਣ ਵਾਲ਼ਿਆਂ
ਨਾਲ ਹੀ ਭਰੇ ਜਾਂਦੇ ਹਨ।</p>

<p>ਅਕਾਲੀ ਦਲ ਇਸ ਵਾਰ ਨਵੇਂ ਚਿਹਰੇ ਸਾਹਮਣੇ ਲਿਆਏਗਾ</p>

<p>ਚੰਡੀਗੜ੍ਹ
: ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਵਲੋਂ ਆਉਂਦਿਆਂ ਵਿਧਾਨਸਭਾ ਦੀਆਂ ਚੋਣਾਂ ਵਿਚ ਕਈ ਨਵੇਂ ਚਿਹਰਿਆਂ ਨੂੰ
ਮੈਦਾਨ ਵਿਚ ਉਤਾਰੇ ਜਾਣ ਦੀ ਸੰਭਾਵਨਾ ਹੈ। ਪਾਰਟੀ ਵਿਚ ਇਹ ਚਰਚਾ ਹੈ ਕਿ ਇਸ ਵਾਰ ਵੋਟਰ ਸਰਕਾਰ
ਦੀ ਕਾਰਗੁਜ਼ਾਰੀ ਦੇ ਨਾਲ ਨਾਲ ਵਜ਼ੀਰਾਂ ਅਤੇ ਵਿਧਾਇਕਾਂ ਦੀ ਕਾਰਗੁਜ਼ਾਰੀ ਦਾ ਲੇਖਾ ਜੋਖਾ ਵੀ ਕਰਨਗੇ।
ਅਕਾਲੀ ਦਲ ਭਾਾਜਪਾ ਗਠਜੋੜ ਦੀ ਇਕ ਧਿਰ ਭਾਰਤੀ ਜਨਤਾ ਪਾਰਟੀ ਦੀ ਲੀਡਰਸ਼ਿਪ ਨੇ ਇਹ ਸੰਕੇਤ ਦਿੱਤਾ
ਹੈ ਕਿ ਵਿਧਾਨਸਭਾ ਚੋਣਾਂ ਵਿਚ ਇਕ ਅੱਧ ਸੀਟ ਨੂੰ ਛੱਡ ਕੇ ਵਰਤਮਾਨ ਵਿਧਾਇਕਾਂ ਨੂੰ ਹੀ ਅੱਗੇ
ਲਿਆਂਦਾ ਜਾਵੇਗਾ, ਪਰ ਗਠਜੋੜ ਦੀ ਮੁੱਖ ਧਿਰ ਅਕਾਲੀ ਦਲ ਦੀ ਲੀਡਰਸ਼ਿਪ 'ਚ ਵੱਖਰੀ ਚਰਚਾ ਚਲ ਰਹੀ ਹੈ। ਪਾਰਟੀ ਦੇ
ਅਹਿਮ ਮਾਮਲ਼ਿਆਂ ਵਿਚ ਫੈਸਲਾਕੁੰਨ ਭੂਮਿਕਾ ਨਿਭਾਉਣ ਵਾਲੇ ਕਈ ਆਗੂਆਂ ਦੀ ਰਾਇ ਹੈ ਕਿ
ਆਉਂਦੀਆਂ ਚੋਣਾਂ ਵਿਚ ਕਈ ਹਲਕਿਆਂ ਵਿਚ ਨਵੇਂ ਉਮੀਦਵਾਰ ਮੈਦਾਨ ਵਿਚ ਉਤਾਰਨ ਦੀ ਲੋੜ ਹੈ।
ਇਨ੍ਹਾਂ ਆਗੂਆਂ ਦਾ ਕਹਿਣਾ ਹੈ ਕਿ ਜਿਹੜੇ ਵਿਧਾਇਕਾਂ ਤੇ ਮੰਤਰੀਆਂ ਨੇ ਆਪਣੇ ਪੰਜ ਸਾਲ ਦੇ ਸਮੇਂ
ਵਿਚ ਹਲਕੇ 'ਚ ਕੋਈ ਕੰਮ ਨਹੀਂ
ਕੀਤਾ ਉਹ ਕਿਸੇ ਨਾ ਕਿਸੇ ਵਿਵਾਦ ਵਿਚ ਘਿਰੇ ਰਹੇ ਹਨ, ਉਨਾਂ੍ਹ ਨੂੰ ਮੁੜ ਕੇ ਮੈਦਾਨ ਵਿਚ
ਲ਼ਿਆਉਣਾ ਪਾਰਟੀ ਲਈ ਜੋਖਮ ਭਰਿਆ ਕੰਮ ਹੋਵੇਗਾ। </p>

<p>ਪਾਕਿਸਤਾਨ ਕਮੇਟੀ ਨੂੰ ਮਾਨਤਾ ਨਹੀਂ ਦਿਆਂਗੇ : ਤਲਵੰਡੀ</p>

<p>ਚਮਕੌਰ
ਸਾਹਿਬ : " ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ, ਪਾਕਿਸਤਾਨ ਸਿੱਖ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ
ਕਮੇਟੀ ਨੂੰ ਮਾਨਤਾ ਨਹੀਂ ਦਿੰਦੀ, ਇਸ ਲਈ ਉਹਦੇ ਪ੍ਰਧਾਨ ਵਲੋਂ ਦਿਤੇ ਬਿਆਨਾਂ ਦੀ ਵੀ ਕੋਈ
ਕੀਮਤ ਨਹੀਂ" ਹੈ। ਇਹ ਵਿਚਾਰ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ ਜਗਦੇਵ ਸਿੰਘ ਤਲਵੰਡੀ ਨੇ ਅੱਜ
ਪੱਤਰਕਾਰਾਂ ਨਾਲ ਗੱਲਬਾਤ ਕਰਦਿਆਂ ਪ੍ਰਗਟ ਕੀਤੇ। ਪਾਕਿਸਤਾਨ ਕਮੇਟੀ ਦੇ ਚੇਅਰਮੈਨ ਅਤੇ
ਆਈ.ਐਸ.ਆਈ. ਦੇ ਸਾਬਕਾ ਡਾਇਰੈਕਟਰ ਜਨਰਲ, ਜਨਰਲ ਜਾਵੇਦ ਨਾਸਿਰ ਨੇ ਬੀਤੇ ਦਿਨੀਂ ਕਿਹਾ ਸੀ
ਕਿ ਜੇ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਪਾਕਿਸਤਾਨ ਦੇ ਸਿੱਖ ਗੁਰਧਾਮਾਂ ਦੇ ਮਸਲਿਆਂ ਸਬੰਧੀ ਗੱਲ ਕਰਨੀ ਚਾਹੁੰਦੀ ਹੈ
ਤਾਂ ਉਹ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਦੇ ਕਿਸੇ ਪ੍ਰਤੀਨਿਧ ਨੂੰ ਪਾਕਿਸਤਾਨ ਕਮੇਟੀ ਦਾ ਮੈਂਬਰ ਬਣਾ ਸਕਦੇ ਹਨ, ਪਰ
ਸ਼ਰਤ ਇਹ ਹੈ ਕਿ ਪਾਕਿਸਤਾਨੀ ਸਿੱਖਾਂ ਦਾ ਕੋਈ ਨੁਮਾਇੰਦਾ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਵਿਚ ਹੋਵੇ। ਜਥੇਦਾਰ
ਤਲਵੰਡੀ ਨੇ ਕਿਹਾ ਕਿ ਉਹ ਅਤੇ ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਅਗਲੇ ਮਹੀਨੇ ਪਾਕਿਸਤਾਨ ਦੇ
ਫੌਜ਼ੀ ਹਾਕਮ ਪਰਵੇਜ਼ ਮੁਸ਼ਰੱਫ ਨੂੰ ਮਿਲ ਕੇ ਪਾਕਿਸਤਾਨ ਦੇ ਗੁਰਧਾਮਾਂ ਦੇ ਮਸਲੇ ਉਠਾਉਣਗੇ ਅਤੇ
ਕੋਈ ਸਰਬ ਪ੍ਰਵਾਨਿਤ ਰਾਹ ਕੱਢਣ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰਨਗੇ।</p>

<p>ਬਠਿੰਡਾ ਵਿਚ ਚਾਰ ਮੰਜ਼ਿਲੀ ਇਮਾਰਤ ਧੱਕੇ ਨਾਲ ਢਾਹੁਣ ਆਏ 18 ਵਿਅਕਤੀ ਕਾਬੂ</p>

<p>ਬਠਿੰਡਾ
:- ਇਥੇ ਪੁਲਿਸ ਨੇ ਨਵੀ ਬਸਤੀ ਵਿਖੇ ਸਥਿਤ ਮੋਤਾ ਸਿੰਘ ਇਮਾਰਤ ਨੂੰ ਜਬਰੀ ਢਾਹੁਣ ਦੇ ਦੋਸ਼ ਵਿਚ
18 ਵਿਅਕਤੀਆਂ ਨੂੰ ਗ੍ਰਿਫਤਾਰ ਕਰ ਲਿਆ। ਮੁਹੱਲੇ ਦੇ ਲੋਕਾਂ ਨੇ ਇਮਾਰਤ ਢਾਹੁਣ ਦੀ ਧੱਕੇਸ਼ਾਹੀ ਤੇ
ਗੁੰਡਾਗਰਦੀ ਵਿਰੁੱਧ ਵਿਖਾਵਾ ਕੀਤਾ। ਇਸ ਇਮਾਰਤ ਵਿਚ ਰਹਿੰਦੇ ਕਿਰਾਏਦਾਰ ਹਰੀ ਕਾਲੀਆ, ਜੋ
ਦਿੱਲੀ ਗਿਆ ਹੋਇਆ ਹੈ, ਦਾ ਸਮਾਨ ਅਤੇ ਕੁੱਝ ਮਲਬਾ ਸੜਕ ਉਤੇ ਖਿਲਰਿਆ ਪਿਆ ਸੀ। ਇਮਾਰਤ ਦੇ
ਕਮਰਿਆ ਦੀਆਂ ਕਈ ਛੱਤਾਂ ਵਿਚ ਮਘੋਰੇ ਕੀਤੇ ਹੋਏ ਸਨ। ਆਂਢ ਗੁਆਂਢ ਦੇ ਦਸੱਣ ਅਨੁਸਾਰ 30-35
ਵਿਅਕਤੀ ਤਿੰਨ ਟਰੈਕਟਰਾਂ ਉਤੇ ਆਏ ਤੇ ਉਨ੍ਹਾਂ ਨੇ ਅੰਦਰ ਜਾ ਕੇ ਹੇਠਲੀ ਮੰਜ਼ਿਲ ਦੇ ਕਮਰਿਆਂ
ਵਿਚੋਂ ਸਾਮਾਨ ਕੱਢਣਾ ਅਤੇ ਇਮਾਰਤ ਦੇ ਛੱਜਿਆਂ ਨੂੰ ਢਾਹੁਣਾ ਸ਼ੁਰੂ ਕਰ ਦਿੱਤਾ।</p>

<p>ਇਤਲਾਹ
ਮਿਲਣ 'ਤੇ ਪੁਲਿਸ ਫੋਰਸ
ਨੇ ਮੌਕੇ ਉਤੇ ਪਹੁੰਚ ਕੇ 17-18 ਵਿਅਕਤੀਆਂ ਨੂੰ ਕਾਬੂ ਕਰ ਲਿਆ ਜਦਕਿ ਬਾਕੀ ਦੇ ਟਰੈਕਟਰਾਂ 'ਤੇ ਚੜ੍ਹ ਕੇ ਫਰਾਰ ਹੋ ਗਏ। ਐਸ.ਐਸ.ਪੀ.
ਜਤਿੰਦਰ ਜੈਨ ਤੇ ਹੋਰ ਅਧਿਕਾਰੀਆਂ ਨੇ ਮੌਕਾ ਦੇਖਿਆ ਤੇ ਕੋਤਵਾਲੀ ਵਿਚ ਜਾ ਕੇ ਇਸ ਸਬੰਧ ਵਿਚ
ਸਬੰਧਤ ਲੋਕਾਂ ਦੀ ਗੱਲ ਸੁਣੀ। ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਭਾਵੇਂ ਉਹ ਕਿੰਨੇ ਵੀ ਬਾਰਸੂਖ ਹੋਣ, ਦੋਸ਼ੀਆਂ ਨੂੰ
ਬਖਸ਼ਿਆ ਨਹੀਂ ਜਾਵੇਗਾ। ਮੌਕੇ ਉਤੇ ਲੋਕਾਂ ਨੇ ਸ਼੍ਰੀ ਜੈਨ ਨੂੰ ਦੱਸਿਆ ਕਿ ਇਮਾਰਤ ਢਾਹੁਣ ਸਮੇਂ
ਵਰਦੀ ਪਾਈ ਇਕ ਏ.ਐਸ.ਆਈ. ਅਤੇ ਇਕ ਹੌਲਦਾਰ ਉਥੇ ਮੌਜੂਦ ਸਨ ਤੇ ਉਨ੍ਹਾਂ ਨੇ ਹੀ ਪੁੁਲਿਸ
ਆਉਣ 'ਤੇ ਦੋਸ਼ੀਆਂ ਨੂੰ
ਭੱਜ ਜਾਣ ਲਈ ਆਖਿਆ।</p>

<p>ਲਾਦੇਨ ਵੱਲੋਂ ਅਮਰੀਕੀ ਦੂਤਘਰ ਨੰੂ ਉਡਾਉਣ ਦੀ ਸਾਜ਼ਿਸ਼ ਫੜੀ-ਦਿੱਲੀ 'ਚ
3 ਗ੍ਰਿਫਤਾਰ</p>

<p>ਨਵੀਂ
ਦਿੱਲੀ: ਦਿੱਲੀ ਪੁਲਿਸ ਨੇ ਕੌਮਾਂਤਰੀ ਦਹਿਸ਼ਤਪਸੰਦ ਓਸਾਮਾ ਬਿਨ ਲਾਦੇਨ ਲਈ ਕੰਮ ਕਰਦੇ ਇਕ ਸੁਡਾਨੀ
ਖਾੜਕੂ ਨੰੂ ਗ੍ਰਿਫਤਾਰ ਕਰਕੇ ਇੱਥੇ ਸਥਿਤ ਅਮਰੀਕੀ ਦੂਤਘਰ ਨੰੂ ਉਡਾਉਣ ਦੀ ਸਾਜ਼ਿਸ਼ ਨੰੂ ਨਾਕਾਮ
ਬਣਾ ਦਿੱਤਾ ਹੈ । ਪੁਲਿਸ ਸੂਤਰਾਂ ਅਨੁਸਾਰ ਸੁਡਾਨੀ ਰੌਫ ਅਤੇ ਹਵਾਸ਼ ਅਤੇ ਉਸ ਦੇ ਭਾਰਤੀ ਸਾਥੀ
ਸ਼ਮੀਰ ਸਰਵਨ ਜਿਹੜਾ ਬਿਹਾਰ ਦਾ ਵਸਨੀਕ ਹੈ , ਨੰੂ ਕੁੱਲ੍ਹ 6 ਕਿਲੋਗ੍ਰਾਮ ਆਰ.ਡੀ.ਐਕਸ ਅਤੇ ਦੇਸੀ
ਵਿਸਫੋਟਕ ਯੰਤਰਾ ਨਾਲ ਗ੍ਰਿਫਤਾਰ ਕੀਤਾ ਗਿਆ । ਹੋਰ ਦੱਸਿਆ ਗਿਆ ਕਿ ਦੂਤਘਰ ਨੰੂ ਉਡਾਉਣ ਦੀ
ਯੋਜਨਾ ਯਮਨ ਦੇ ਇਕ ਨਾਗਰਿਕ , ਜਿਸ ਦੀ ਪਛਾਣ ਅਲ-ਸ਼ਫਾਈ ਵਜੋਂ ਹੋਈ ਹੈ , ਦੇ ਕਹਿਣ 'ਤੇ ਤਿਆਰ ਕੀਤੀ ਗਈ, ਜਿਸ ਨੇ ਕਿ ਹਵਾਸ਼
ਨੰੂ ਇਸ ਮੰਤਵ ਲਈ 5 ਲੱਖ ਰੁਪਏ ਦੀ ਰਕਮ ਦਿੱਤੀ ਸੀ । ਪੁਲਿਸ ਅਧਿਕਾਰੀ ਅਸ਼ੋਕ ਚੰਦ ਨੇ ਦੱਸਿਆ
ਕਿ ਪੁਲਿਸ, ਲਾਦੇਨ ਦੇ ਗਰੋਹ ਦੇ ਦੋ-ਤਿੰਨ ਹੋਰ ਸਾਥੀਆਂ ਦੀ ਭਾਲ ਕਰ ਰਹੀ ਹੈ । ਉਨ੍ਹਾਂ ਦੱਸਿਆ
ਕਿ ਉਸਮਾ ਬਿਨ ਲਾਦੇਨ ਨੇ ਲਗਪਗ 2 ਸਾਲ ਪਹਿਲਾਂ ਹਵਾਸ਼ ਨੰੂ ਦਿੱਲੀ ਵਿਚ ਆਪਣਾ ਅੱਡਾ ਕਾਇਲ ਕਰਨ
ਲਈ ਕਿਹਾ ਸੀ ।</p>

<p>ਤਖਤ ਸ੍ਰੀ ਕੇਸਗੜ੍ਹ ਸਾਹਿਬ ਵਿਖੇ ਪੈਸਿਆਂ ਦੀ ਦੁਰਵਰਤੋਂ ਦੀ ਜਾਂਚ ਲਈ ਉਡਣ ਦਸਤਾ
ਭੇਜੀਆ-ਤਲਵੰਡੀ</p>

<p>ਸ੍ਰੋਮਣੀ
ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਵੱਲੋਂ ਤਖਤ ਸ੍ਰੀ ਆਨੰਦਪੁਰ ਸਾਹਿਬ ਵਿਖੇ ਜੋ ਕਥਿਤ ਤੋਰ 'ਤੇ ਪੈਸਿਆਂ ਦੀ ਹੋਈ ਦੁਰਵਰਤੋ ਸਬੰਧੀ
ਪਰਦਾ ਫਾਸ਼ ਹੋਇਆਂ ਹੈ ਉਸ ਦੀ ਜਾਂਚ ਪੜਤਾਲ ਕਰਨ ਲਈ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਵੱਲੋਂ ਉਡਣ
ਦਸਤਾ ਭਾਈ ਮਹਿੰਦਰ ਸਿੰਘ ਮੀਤ ਸਕਤਰ ਦੀ ਅਗਵਾਈ ਹੇਠ ਸ੍ਰੀ ਅੰਨਦਪੁਰ ਸਾਹਿਬ ਵਿਖੇ ਭੇਜ ਦਿੱਤਾ
ਗਿਆ ਹੈ ਜਿਸ ਦੀ ਰਿਪੋਰਟ ਆਉਣ 'ਤੇ ਹੀ ਦੋਸ਼ੀਆਂ ਖਿਲਾਫ ਕਾਰਵਾਈ ਕੀਤੀ ਜਾਵੇਗੀ । ਇਸ ਗੱਲ ਦਾ ਪ੍ਰਗਟਾਵਾ ਸ੍ਰੋਮਣੀ
ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ ਜਥੇਦਾਰ ਗੁਰਦੇਵ ਸਿੰਘ ਤਲਵੰਡੀ ਨੇ ਗੁਰਦੁਆਰਾ ਸ੍ਰੀ
ਕਤਲਗੜ੍ਹ ਸਾਹਿਬ ਵਿਖੇ ਦੀਵਾਨ ਹਾਲ ਅਤੇ ਲੰਗਰ ਹਾਲ ਦੀ ਨੀਂਹ ਪਧਰ ਰੱਖਣ ਦੌਰਾਨ ਇਕ ਪ੍ਰੈਸ
ਕਾਨਫਰੰਸ ਦੌਰਾਨ ਕੀਤਾ । ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਸ੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਚੋਣ ਬਾਰੇ
ਸ੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਚੋਣ ਕਮਿਸ਼ਨ ਨੰੂ ਲਿਖ ਦਿੱਤਾ ਹੈ ਕਿ ਜਦੋਂ ਮਰਜ਼ੀ ਚੋਣ ਕਰਵਾਈ ਜਾਵੇ ।
ਜਥੇਦਾਰ ਤਲਵੰਡੀ ਤੋਂ ਪੁੱਛੇ ਪ੍ਰਸ਼ਨ ਕਿ ਤੁਸੀ ਸਾਰੇ ਅਕਾਲੀ ਦਲਾਂ ਵਿਚ ਏਕਾ ਕਰਵਾਉਣ ਦੇ ਚਾਹਵਾਨ
ਹੋ ਤਾਂ ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਮੈ ਤਾਂ ਏਕਾ ਕਰਵਾਉਣ ਲਈ ਤਿਆਰ ਹਾਂ ਪਰ ਜੇਕਰ ਇਹ ਸਾਰੇ ਅਕਾਲੀ ਦਲ
ਮੰਨਣਗੇ । </p>

<p>ਬਾਦਲ ਦੋ ਹਲਕਿਆਂ ਤੋਂ ਚੋਣ ਲੜਨ ਦੀ ਤਿਆਰੀ ਵਿਚ ! ਦੂਜਾ ਹਲਕਾ ਮਜੀਠਾ ਹੋਵੇਗਾ?</p>

<p>ਲੁਧਿਆਣਾ:ਕੀ
ਸਰਦਾਰ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਇਸ ਵਾਰ ਦੋ ਹਲਕਿਆਂ ਤੋਂ ਚੋਣ ਲੜਨਗੇ, ਜਿਵੇਂ ਉਨ੍ਹਾਂ ਨੇ ਫਰਵਰੀ
1997 ਵਿਚ ਦੋ ਹਲਕਿਆਂ ਤੋਂ ਲੜੀ ? ਜਾਣਕਾਰ ਉਤਰ ਇਸ ਦਾ ਜਵਾਬ ਹਾਂ ਵਿਚ ਦੇਣ ਤੋਂ ਝਿਜਕਦੇ ਨਹੀਂ
।</p>

<p>ਦੂਜਾ
ਸਵਾਲ ਇਹ ਹੈ ਕਿ ਹਲਕੇ ਕਿਹੜੇ ਕਿਹੜੇ ਹੋਣਗੇ ? ਇਸ ਦਾ ਜਿਹੜਾ ਜਵਾਬ ਉਹਨਾ ਦੀ ਜਾਣਕਾਰੀ
ਹਲਕਿਆ ਵੱਲੋਂ ਦਿੱਤਾ ਜਾਂਦਾ ਹੈ, ਉਹ ਹੈਰਾਨ ਕਰਨ ਵਾਲਾ ਹੈ ਉਹਨਾਂ ਮੁਤਾਬਕ ਇਸ ਵਾਰ ਸ੍ਰ:
ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਆਪਣੇ ਪਹਿਲੇ ਹਲਕੇ ਵਲੋਂ ਲੰਬੀ ਦੀ ਬਜਾਏ ਗਿੱਦੜਬਾਹਾ ਉੱਤੇ ਅੱਖਾ ਟਿਕਾਈ
ਬੈਠੇ ਹਨ। ਹਾਲਾਂਕਿ ਗਿੱਦੜਬਾਹਾ ਵੀ ਉਹਨਾ ਲਈ ਪਹਿਲੇ ਜਿਨਾਂ ਮਜਬੂਤ ਨਹੀਂ, ਇਹ ਗੱਲ ਪਿਛਲੀ ਲੋਕ
ਸਭਾ ਚੋਣ ਸਮੇਂ ਸਾਬਤ ਹੋ ਚੁੱਕੀ ਹੈ, ਪਰ ਲੰਬੀ ਨਾਲ ਕੁਝ ਕੁ ਸੁਖਾਵਾਂ ਹੋ ਸਕਦਾ ਹੈ । ਇਸ ਤਰਾਂ
ਕਰਨ ਨਾਲ ਭਤੀਜੇ ਮਨਪ੍ਰੀਤ ਸਿੰਘ ਮਨ ਵਿਚ ਕੁੜੱਤਣ ਹੋਰ ਵਧ ਜਾਣ ਦਾ ਡਰ ਵੀ ਹੈ , ਪਰ ਸਾਊ ਸੁਭਾਅ
ਵਾਲਾ ਭਤੀਜਾ ਇਸ ਕੁੜਤਣ ਨੰੂ ਜੱਗ ਜਾਹਿਰ ਕਰਨੋ ਪ੍ਰਹੇਜ ਕਰੇਗਾ। ਫਿਰ ਵੀ ਲੰਬੀ ਵਿਚ ਕਿਸਮਤ
ਅਜਮਾਉਣਾ ਸ਼ਾਇਦ ਉਸ ਨੰੂ ਵੀ ਪਸੰਦ ਨਾ ਹੋਵੇ ।</p>

<p>ਪਰ
ਦੂਜਾ ਹਲਕਾ ਕਿਹੜਾ ਹੋਵੇਗਾ , ਜਿੱਥੋ ਸ੍ਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਲੜਨਗੇ ? ਪਿਛਲੀ ਵਾਰੀ ਉਹਨਾ ਨੇ
ਕਿਲ੍ਹਾ ਰਾਏਪੁਰ ਚੁਣਿਆ ਸੀ ਰਾਏਪੁਰੀਆਂ ਨੇ ਉਨ੍ਹਾਂ ਨੰੂ ਚੁਣ ਲਿਆ ਸੀ । ਪਿੱਛੋਂ ਉਨ੍ਹਾਂ ਦੇ
ਅਸਤੀਫਾ ਦੇ ਜਾਣ ਕਾਰਨ ਬਾਦਲ ਅਕਾਲੀ ਵੱਲੋਂ ਸ੍ਰੀ ਜਗਦੀਸ਼ ਸਿੰਘ ਗਰਚਾ ਵੀ ਭਾਵੇਂ ਜਿੱਤ ਗਏ ਸਨ,
ਲੜਾਈ ਸੌਖੀ ਨਹੀਂ ਸੀ ਰਹੀ ਤੇ ਹੁਣ ਤਾਂ ਉਸ ਹਲਕੇ ਵਿਚੋਂ ਆਸ ਰੱਖੀ ਹੀ ਨਹੀਂ ਜਾ ਸਕਦੀ ।</p>

<p>ਦੂਜਾ
ਹਲਕਾ ਕਿਹੜਾ'ਬਾਰੇ ਪੁੱਛੇ ਗਏ
ਸਵਾਲ ਦੇ ਜਵਾਬ ਵਿਚ ਬੜੀ ਦੱਬਵੀ ਜਬਾਨ ਵਿਚ ਮਜੀਠਾ ਦਾ ਨਾਂਅ ਲਿਆ ਜਾ ਰਿਹਾ ਹੈ । ਕਿਸੇ ਯੋਗ
ਅਕਾਲੀ ਆਗੂ ਤੋਂ ਸੱਖਣੇ ਇਸ ਹਲਕੇ ਤੋਂ ਪਿੱਛੇ ਜਿਹੇ ਹੋਈ ਉਪ ਚੋਣ ਵਿਚ ਬਾਦਲ ਅਕਾਲੀ ਦਲ ਦਾ
ਉਮੀਦਵਾਰ ਰਾਜਮਹਿੰਦਰ ਸਿੰਘ ਜਿੱਤ ਗਿਆ ਸੀ । ਪਰ ਉਹ ਜਿੱਤ ਪਾਰਟੀ ਦੀ ਨਹੀਂ , ਖੁਦ ਸ੍ਰ: ਬਾਦਲ
ਦੀ ਸਿਰੜੀ ਸਰਗਰਮੀ ਸਾਮ-ਦਾਮ ਦੰਡ ਭੇਦ ਦੇ ਸਭ ਜਾਇਜ਼ ਨਜਾਇਜ਼ ਹਥਕੰਡਿਆਂ ਕਾਰਨ ਹੋ ਸਕੀ ਹੈ ।
ਉਸ ਉਪ ਚੋਣ ਦੌਰਾਨਕੀਤੀ ਗਈ ਮਿਹਨਤ ਅਤੇ ਦਿੱਤੇ ਗਏ ਗਰਾਂਟਾ ਦੇ ਗੱਫਿਆਂ ਕਾਰਨ ਸ: ਬਾਦਲ
ਜਿਹੜਾ ਪ੍ਰਭਾਵ ਉਥੇ ਬਣਾ ਚੁੱਕੇ ਹਨ, ਉਸ ਦਾ ਲਾਭ ਲੈਣਾ ਚਾਹੁੰਦੇ ਹਨ । ਕੁਝ ਪਿੰਡਾਂ ਦੇ ਸਰਪੰਚਾ
ਨੰੂ ਕਾਂਗਰਸ ਨਾਲ ਤੋੜ ਕੇ ਉੱਦੋ ਉਨ੍ਹਾਂ ਨੰੂ ਆਪਣੇ ਨਾਲ ਜੋੜ ਲਿਆ ਸੀ ਤੇ ਜਿਹੜੇ ਉਦੋ ਨਹੀਂ
ਸਨ ਜੋੜੇ ਜਾ ਸਕੇ,ਉਹਨਾ ਨੰੂ ਹੁਣ ਸੰਭਾਲਣ ਲਈ ਪਾਰਟੀ ਦੇ ਨਾਲ ਨਾਲ ਸਰਕਾਰੀ ਮਸ਼ੀਨਰੀ ਵੀ ਸਰਗਰਮ
ਦਿੱਤੀ ਜਾ ਰਹੀ ਹੈ ਚਰਚੇ ਤਾਂ ਇਥੋ ਤੱਕ ਹਨ ਕਿ ਇੱਕ ਉੱਘੇ ਕਾਂਗਰਸੀ ਨੰੂ ਅਕਾਲੀ ਦਲ ਵਿਚ ਸ਼ਾਮਲ
ਹੋਣ ਲਈ ਤਿਆਰ ਕਰ ਲਿਆ ਗਿਆ ਹੈ ।</p>

<p> </p>






</body></text></cesDoc>