<cesDoc id="pun-w-sanjh-news-01-05-06" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-01-05-06.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 01-05-06</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>01-05-06</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਜਫਰਵਾਲ
ਨੂੰ ਸਖਤ ਸੁਰੱਖਿਆ ਹੇਠ ਨਾਭਾ ਜ਼ੇਲ੍ਹ ਭੇਜਿਆ </p>



<p>ਅੰਮ੍ਰਿਤਸਰ,
5 ਮਈ - ਖਾਲਿਸਤਾਨ ਕਮਾਂਡੋ ਫੋਰਸ ਦੇ ਮੁਖੀ ਤੇ ਪੰਥਕ ਕਮੇਟੀ ਦੇ ਮੈਂਬਰ ਵੱਸਣ ਸਿੰਘ ਜ਼ਫਰਵਾਲ ਜਿਸ
ਨੂੰ ਜਿਸ ਨੂੰ 11 ਅਪ੍ਰੈਲ ਨੂੰ ਗ੍ਰਿਫਤਾਰ ਕੀਤਾ ਗਿਆ ਸੀ, ਨੂੰ ਸਖਤ ਸੁਰੱਖਿਆ ਹੇਠ ਅੱਜ ਸਾਮੀਂ
ਨਾਭੀ ਜ਼ੇਲ ਵਿਚ ਭੇਜ ਦਿੱਤਾ ਗਿਆ । ਅੰਮ੍ਰਿਤਸਰ ਦੀ ਕੇਂਦਰੀ ਜ਼ੇਲ ਦੇ ਬੁਲਾਰੇ ਨੇ ਦੱਸਿਆ ਕਿ ਹੋਰ
ਪੁਲਿਸ ਰਿਮਾਂਡ ਹਾਸਲ ਕਰਨ ਲਈ ਕਿਸੇ ਨੇ ਵੀ ਪਹੁੰਚ ਨਹੀਂ ਕੀਤੀ ਇਸ ਲਈ ਉਸ ਨੂੰ ਨਾਭਾ੍ਹ ਜ਼ੇਲ
ਭੇਜ ਦਿੱਤਾ ਗਿਆ । ਵਰਣਨਯੋਗ ਹੈ ਕਿ ਅਦਾਲਤ ਵਲੋਂ ਉਸ ਨੂੰ 16 ਮਈ ਤਕ ਨਿਆਂਇਕ ਹਿਰਾਸਤ ਵਿਚ
ਭੇਜਣ ਦੇ ਆਦੇਸ਼ ਪਿੱਛੋਂ ਗੁਰਦਾਸਪੁਰ ਜ਼ਿਲੇ ਦੇ ਜ਼ੇਲ ਅਧਿਕਾਰੀਆਂ ਨੇ ਉਸ ਨੂੰ ਰੱਖਣ ਤੋਂ ਇਨਕਾਰ
ਕਰ ਦਿੱਤਾ ਸੀ । ਉਸ ਉਤੇ ਧਾਰੀਵਾਲ ਥਾਣੇ ਵਿਚ ਕਤਲ ਕੇਸ ਅਤੇ ਹਥਿਆਰ ਰੱਖਣ ਸਬੰਧੀ ਕੇਸ ਦਰਜ
ਹਨ । ਅਧਿਕਾਰੀਆਂ ਦਾ ਕਹਿਣਾ ਸੀ ਕਿ ਸੁਰੱਖਿਆ ਪ੍ਰਬੰਧਾਂ ਦੀ ਘਾਟ ਕਾਰਨ ਉਹ ਸਾਬਕਾ ਅੱਤਵਾਦੀ ਨੂੰ
ਨਹੀਂ ਰੱਖ ਸਕਦੇ ।  </p>

<p>ਵਿਸ਼ਵ
ਸਿੱਖ ਕੌਸਲ ਦੋਵੇਂ ਧੜੇ ਅਕਾਲ ਤਖਤ ਤੇ ਤਲਬ
</p>



<p>ਅੰਮ੍ਰਿਤਸਰ,
5 ਮਈ - ਵਿਸ਼ਵ ਸਿੱਖ ਕੌਂਸਲ ਦੇ ਦੋਵਾਂ ਧੜਿਆਂ ਵਿਚ ਪੈਦਾ ਹੋਏ ਅੜਿੱਕੇ ਨੂੰ ਦੂਰ ਕਰਨ ਲਈ ਅਕਾਲ
ਤਖਤ ਵਲੋਂ ਉਨ੍ਹਾਂ ਦੋਵਾਂ ਧੜਿਆਂ ਨੰੂ 14 ਮਈ ਨੂੰ ਤਲਬ ਕੀਤਾ ਗਿਆ ਹੈ । ਵਰਣਨਯੋਗ ਹੈ ਕਿ 23
ਮਾਰਚ 2000 ਨੂੰ ਉਸ ਵੇਲੇ ਦੇ ਅਕਾਲ ਤਖਤ ਦੇ ਜਥੇਦਾਰ ਗਿ: ਪੂਰਨ ਸਿੰਘ ਨੇ ਕੌਂਸਲ ਦੇ ਦੋਵਾਂ
ਧੜਿਆਂ ਨੂੰ ਭੰਗ ਕਰਕੇ ਇਕ 12 ਮੈਂਬਰੀ ਕਮੇਟੀ ਦਾ ਗਠਨ ਕੀਤਾ ਸੀ ਪਰ ਇਸ ਦੇ ਬਾਵਜੂਦ ਦੋਵੇਂ
ਧੜੇ ਕੰਮ ਕਰਦੇ ਰਹੇ ਸਨ । ਇਸੇ ਕਾਰਨ ਬੈਂਕ ਵਿਚ ਪਏ ਲੱਖਾਂ ਰੁਪਏ ਕਿਸੇ ਸਾਰਥਕ ਕੰਮ ਲਈ ਨਹੀ
ਸੀ ਵਰਤੇ ਜਾ ਸਕੇ । ਜਥੇਦਾਰ ਵਲੋਂ ਉਦੋਂ ਸੱਦੀ ਗਈ ਮੀਟਿੰਗ ਵਿਚ ਵਿਸ਼ਵ ਸਿੱਖ ਕੌਂਸਲ ਦੇ ਜਨਰਲ
ਸਕੱਤਰ ਸ੍ਰੀ ਜਸਵੰਤ ਸਿੰਘ ਮਾਨ ਨੇ ਕੌਸ਼ਲ ਦਾ ਹਿਸਾਬ ਕਿਤਾਬ ਜਥੇਦਾਰ ਗਿ: ਪੂਰਨ ਸਿੰਘ ਨੂੰ ਸੌਂਪ
ਦਿੱਤਾ ਸੀ ਪਰ ਭਾਈ ਰਣਜੀਤ ਸਿੰਘ ਨਾਲ ਸਬੰਧਤ ਜਸਟਿਸ (ਰਿਟਾ) ਸ੍ਰੀ ਕੁਲਦੀਪ ਸਿੰਘ ਦੀ ਅਗਵਾਈ
ਵਾਲੇ ਧੜੇ ਨੇ ਕਿਸੇ ਕਿਸਮ ਦਾ ਵੇਰਵਾ ਨਹੀ ਸੀ ਦਿੱਤਾ । ਇਹ ਵੀ ਵਰਣਨਯੋਗ ਹੈ ਕਿ ਭਾਈ ਰਣਜੀਤ
ਸਿੰਘ ਨੇ ਜਥੇਦਾਰ ਹੁੰਦਿਆਂ ਤਖਤ ਸੀ੍ਰ ਕੇਸਗੜ੍ਹ ਸਾਹਿਬ ਦੇ ਜਥੇਦਾਰ ਪ੍ਰੋ: ਮਨਜੀਤ ਸਿੰਘ ਤੇ ਸਿੰਘ
ਸਾਹਿਬਾਨ ਦੀਆਂ ਮੀਟਿੰਗਾਂ ਉਤੇ ਇਹ ਪਾਬੰਦੀ ਲਾ ਦਿੱਤੀ ਸੀ ਕਿ ਜਦੋਂ ਤਕ ਉਹ ਕੌਂਸਲ ਦਾ ਹਿਸਾਬ
ਨਹੀ ਦਿੰਦੇ, ਉਨ੍ਹਾਂ ਨੂੰ ਮੀਟਿੰਗ ਵਿਚ ਸਾਮਿਲ ਨਹੀ ਹੋਣ ਦਿੱਤਾ ਜਾਵੇਗਾ । ਇਸੇ ਲਈ ਜਦੋਂ ਤੀਕ
ਉੇਹ ਜਥੇਦਾਰ ਰਹੇ, ਪ੍ਰੋ; ਮਨਜੀਤ ਸਿੰਘ ਸਿੰਘ ਸਾਹਿਬਾਨ ਦੀ ਮੀਟਿੰਗ ਵਿਚ ਸਾਮਿਲ ਨਹੀ ਸੀ ਹੋ ਸਕੇ
। ਅਕਾਲ ਤਖਤ ਦੇ ਜਥੇਦਾਰ ਗਿ: ਜੋਗਿੰਦਰ ਸਿੰਘ ਵੇਦਾਂਤੀ ਦੇ ਨਿਜੀ ਸਹਾਇਕ ਸ੍ਰੀ ਪ੍ਰਿਥੀਪਾਲ ਸਿੰਘ
ਨੇ ਇਹ ਗੱਲ ਮੰਨੀ ਕਿ ਵਿਸ਼ਵ ਸਿੱਖ ਕੌਂਸਲ ਦੇ ਅਹੁਦੇਦਾਰਾਂ ਨੂੰ ਅਕਾਲ ਤਖਤ ਤੇ ਸੱਦਿਆ ਹੈ ਤੇ
ਇਸ ਸਬੰਧ ਵਿਚ ਉਨ੍ਹਾਂ ਨੂੰ ਪੱਤਰ ਭੇਜ ਦਿੱਤੇ ਗਏ ਹਨ । ਪੰਜ ਸਿੰਘ ਸਾਹਿਬਾਨ ਦੀ ਪੰਥਕ ਮਾਮਲਿਆਂ
ਬਾਰੇ ਵਿਚਾਰ ਕਰਨ ਲਈ 14 ਮਈ ਨੂੰ ਮੀਟਿੰਗ ਹੋਣੀ ਹੈ । ਉਸੇ ਦਿਨ ਹੀ ਮੀਟਿੰਗ ਤੋਂ ਪਹਿਲਾਂ
ਕੌਂਸਲ ਦੇ ਅਹੁਦੇਦਾਰਾਂ ਨੂੰ ਸੱਦਿਆ ਗਿਆ ਹੈ । ਉਸ ਵੇਲੇ ਦੇ ਜਥੇਦਾਰ ਗਿ: ਪੂਰਨ ਸਿੰਘ ਵਲੋਂ
ਦੋਵਾਂ ਸਮਾਨਤਰ ਧੜਿਆਂ ਦੇ ਵਿਚੋਂ ਬਣਾਈ 12 ਮੈਂਬਰੀ ਕਮੇਟੀ ਵਿਚ ਜਸਟਿਸ ਕੁਲਦੀਪ ਸਿੰਘ ਨੂੰ
ਮੈਂਬਰ ਲਿਆ ਗਿਆ ਸੀ ਪਰ ਉਨ੍ਹਾਂ ਨੇ ਕੰਮ ਕਰਨ ਤੋਂ ਇਨਕਾਰ ਕਰ ਦਿੱਤਾ ਸੀ । ਗਿ: ਪੂਰਨ ਸਿੰਘ
ਨੂੰ ਲਿਖੇ ਪੱਤਰ ਵਿਚ ਜਸਟਿਸ ਕੁਲਦੀਪ ਸਿੰਘ ਨੇ ਕੰਮ ਕਰਨ ਤੇ ਆਪਣੀ ਅਸਮੱਰਥਾ ਪ੍ਰਗਟ ਕੀਤੀ ਸੀ ।
ਗਿ: ਪੂਰਨ ਸਿੰਘ ਵਲੋਂ ਅਕਾਲ ਤਖਤ ਤੇ ਸੱਦੀ ਮੀਟਿੰਗ ਵਿਚ ਦੋਵਾਂ ਧੜਿਆਂ ਨੇ ਹਿੱਸਾ ਲਿਆ ਸੀ ਪਰ
ਕਿਸੇ ਸਿੱਟੇ ਤੇ ਨਹੀਂ ਸੀ ਪੁੱਜੇ । ਭਾਈ ਰਣਜੀਤ ਸਿੰਘ ਨੇ ਆਪਣੇ ਧੜੇ ਨੂੰ ਸਿੰਘ ਸਾਹਿਬਾਨ ਕੋਲ
ਕੌਂਸਲ ਦਾ ਹਿਸਾਬ ਪੇਸ ਕਰਨ ਦੀ ਆਗਿਆ ਨਹੀ ਸੀ ਦਿੱਤੀ । ਵਰਣਨਯੋਗ ਹੈ ਕਿ ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ
ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਸਤੰਬਰ 1995 ਵਿਚ  ivSv is@K
sMmyln krv`ieE` igE` sI | pRo: mnjIq isMG aus vyly Ek`l qKq dy vI k`rjk`rnI
jQyd`r sn | ausy sMmyln ivc ivSv is@K kONsl k`iem krn d` PYsl` kIq` igE` sI,
ijs dy m@dy nzr 1996 ivc ies kONsl nUM rijstrf krv`ieE` igE` sI | 1997 ivc Ek`l
qKq d` jQyd`r inwukq hox mgroN B`eI rxjIq isMG ny ivSv is@K kONsl d` srpRsq hox
dy n`qy aus dy ihs`b ikq`b dI mMg kIqI sI | pRo: mnjIq isMG Eqy kONsl dy jnrl
sk@qr jsvMq isMG m`n ny koeI ihs`b nhI sI id@q` ijs dy is@ty vloN ivv`d SurU ho
igE` sI | </p>

<p>ਅਕਾਲੀ
ਆਗੂ ਅਕਾਲ ਤਖਤ ਨੂੰ ਦਿੱਤੇ ਲਿਖਤੀ ਵਾਅਦੇ ਤੋਂ ਪਿੱਛੇ ਹਟੇ </p>

<p>ਲੁਧਿਆਣਾ,
5 ਮਈ - ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਦੇ ਕਈ ਸੀਨੀਅਰ ਆਗੂਆਂ ਨੇ 10 ਜਨਵਰੀ 1995 ਨੂੰ ਅਕਾਲ ਤਖਤ ਨੂੰ
ਇਕ ਲਿਖਤੀ ਵਾਅਦਾ ਦਿੱਤਾ ਸੀ ਕਿ ਉਹ ਅਜਿਹਾ ਕੋਈ ਕੰਮ ਨਹੀ ਕਰਨਗੇ ਜਿਸ ਨਾਲ ਸ੍ਰੋਮਣੀ ਅਕਾਲੀ
ਦਲ ਏਕਤਾ ਭੰਗ ਹੁੰਦੀ ਹੇਵੇ । ਉਨ੍ਹਾਂ ਇਸ ਲਿਖਤੀ ਕਸਮ ਵਿਚ ਇਹ ਵੀ ਕਿਹਾ ਸੀ ਕਿ ਉਹ ਇਕ
ਦੁਜੇ ਖਿਲਾਫ ਕੋਈ ਬਿਆਨਬਾਜ਼ੀ ਨਹੀ ਕਰਨਗੇ ਤਾਂ ਇਸ ਨਾਲ ਏਕਤਾ ਦੀ ਮੁਹਿੰਮ ਨੂੰ ਕੋਈ ਨੁਕਸਾਨ
ਹੋਵੇ । ਇਹ ਉਨ੍ਹਾਂ ਨੇ ਪ੍ਰਤਿਗਿਆ ਕੀਤੀ ਸੀ । ਪਰ ਇਹ ਪ੍ਰਤਿਗਿਆ ਬਹੁਤਾ ਸਮਾਂ ਕਾਇਮ ਨਾ ਰਹਿ
ਸਕੀ ਅਤੇ ਕੁਝ ਜਿਆਦਾ ਅਸਰ ਰਸੂਖ ਵਾਲੇ ਅਕਾਲੀ ਆਗੂ ਜਿਨਾਂ੍ਹ ਨੇ ਇਸ ਉਤੇ ਸਹੀ ਪਾਈ ਸੀ, ਇਸ
ਤੋਂ ਹੁਣ ਪਿੱਛੇ ਹਟੇ ਹੋਏ ਹਨ । ਇਸ ਪ੍ਰਤਿਗਿਆ ਪੱਤਰ ਉਤੇ ਦਸਖਸਤ ਕਰਨ ਵਾਲਿਆਂ ਵਿਚ ਸ੍ਰੀ
ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ, ਜਗਦੇਵ ਸਿੰਘ ਤਲਵੰਡੀ, ਜਥੇਦਾਰ ਗੁਰਚਰਨ ਸਿੰਘ ਟੌਹੜਾ, ਸ੍ਰੀ ਕੁਲਦੀਪ ਸਿੰਘ
ਵਡਾਲਾ, ਮਰਹੂਮ ਸ੍ਰੀ ਪ੍ਰਕਾਸ ਸਿੰਘ ਮਜੀਠਿਆ, ਸ੍ਰੀ ਸੁਰਜੀਤ ਸਿੰਘ ਬਰਨਾਲਾ, ਸ੍ਰੀ ਗੁਰਦੇਵ ਸਿੰਘ
ਬਾਦਲ ਆਦਿ ਸਾਮਲ ਸਨ । ਉਨ੍ਹਾਂ ਨੇ ਰੱਲ ਕਿ ਇਹ ਸਹੁੰ ਖਾਧੀ ਸੀ ਕਿ ਸਾਰੇ ਮਸਲੇ ਅਸੀ ਸਿੰਘ
ਸਾਹਿਬਾਨ ਦੇ ਦਿਸ਼ਾ ਨਿਰਦੇਸ਼ ਅਨੁਸਾਰ ਆਪਸੀ ਗੱਲਬਾਤ ਤੇ ਵਿਚਾਰ ਵਟਾਂਦਰਾ ਕਰਕੇ ਹੱਲ ਕਰਾਂਗੇ ।
ਸ੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਦੇ ਸੀਨੀਅਰ ਆਗੂਆਂ  nUUM
ieh s~JI ksm K`x dI loV audoN peI sI jdoN qqk`lI mu@K mMqrI byEMq isMG pMj`b
ivcoN Eiqv`d nUM Kqm kr rhy sn Eqy Ek`lI lIfriSp iek pRk`r dI gu@Ty l@gI hoeI
sI | ies s`ry k@uJ nUM B~pidE~ Ek`lI dl dy sInIEr E`gUE~ ny E`pxI guE`cI hoeI
S`K nUM muV bh`l krn leI Eqy s@q` ivc E`aux leI Ek`l qKq dI At leI | auh 1997
dIE~ ivD`n sB` cox~ ivc bhumq E` gey Eqy B`jp` n`l imlky E`pxI srk`r bx` leI |
dUjy p`sy jdoN Ek`lI lIfriSp s@q` auqy k`bz ho geI q~ QoVy Ersy mgroN ies dy do
sInIEr E`gU sRI pRk`s isMG b`dl Eqy jQyd`r gurcrn isMG tOhV` Ek`l qKq nUM id@qI
ilKqI pRiqigE` qoN iPslH gey | ku@J sm~ mgroN hI ienH~ doh~ E`gUE~ ivc E`psI
m@qByd vDxy SurU ho gey jo pUrI isKr qy phuMcy Eqy iek dUjy dy iKl`P ic@kV
auC`lx qoN b`Ed 1999 ivc qoV ivCoV` ho igE` | ies qr~ dUjy E`gU sRI suKdyv isMG
FINfs` Eqy sRI surjIq isMG brn`l` ijn~ pRk`S isMG b`dl n`l iml ky rihx dI ksm
K`DI sI dovyN E`ps ivc E@f E@f ho bYTy Eqy Ek`l qKq auqy iekju@t rihx dIE~
K`DIE~ s`rIE~ ksm~ Bu@l-Bul` gey |  </p>

<p>ਸੰਤ
ਧਨਵੰਤ ਸਿੰਘ ਨੂੰ ਡੇਰੇ ਵਿਚੋਂ ਬਾਹਰ ਕੱਡਿਆ, ਅਕਾਲ ਤਖਤ ਦੇ ਫੈਸਲੇ ਉਪਰੰਤ ਪੱਲ਼ੀ ਝੱਕੀ ਮੁੱਖ
ਕੇਂਦਰ ਦੀ ਤਾਲਾਬੰਦੀ</p>

<p>ਬੰਗਾ, 5 ਮਈ - ਸਿੰਘ ਸਾਹਿਬ ਜਥੇਦਾਰ ਜੋਗਿੰਦਰ
ਸਿੰਘ ਵੇਦਾਂਤੀ ਦੇ ਇਕ ਅਹਿਮ ਫੈਸਲੇ ਵਿੱਚ ਕੋਟ ਪੱਲੀਆਂ ਦੇ ਮੁਖੀ (ਸੰਤ) ਧੰਨਵੰਤ ਸਿੰਘ ਨੰੂ ਦੇਸ਼
ਅਤੇ ਵਿਦੇਸ ਦੀਆਂ ਸੰਗਤਾਂ ਮੂੰਹ ਨਾ ਲਾਉਣ ਬਾਅਦ ਅੱਜ ਬੰਗਾ ਤੋ ਛੇ ਕਿਲੋਮੀਟਰ ਦੂਰੀ ਤੇ ਸਥਿਤ
ਲੂਰਵਿਸ਼ਵ ਰੂਹਾਨੀ ਚੈਰੀਟੇਬਲ ਸੰਸਥਾ ਦੇ ਮੁੱਖ ਦਫਤਰ ਵਿਖੇ ਸੰਸਥਾ ਦੇ ਟਰੱਸਟਿਆਂ ਨੇ ਸੀ੍ਰ ਅਕਾਲ
ਤਖਤ ਸਾਹਿਬ ਦੇ ਹੁਕਮਨਾਮੇ ਨੰੂ ਮੁੱਖ ਰੱਖਦਿਆਂ ਧਨਵੰਤ ਸਿੰਘ ਨੰੂ ਸੰਸਥਾ ਦੇ ਸੰਸਥਾਪਕ ਦੇ ਅਹੁਦੇ
ਤੋ ਬਰਖਾਸਤ ਕਰ ਦਿੱਤਾ । ਮੱੁਖ ਕੇਂਦਰ ਦੀ ਤਾਲਾਬੰਦੀ ਵੀ ਕਰ ਦਿੱਤੀ ਗਈ । ਸੰਸਥਾ ਦੇ ਜਨਰਲ ਸਕੱਤਰ
ਸਤਨਾਮ ਸਿੰਘ ਦੀਪ ਨੇ ਦੱਸਿਆ ਕਿ ਧਨਵੰਤ ਸਿੰਘ (ਗੁਰਦਾਸਪੁਰ)ਵਾਲੇ ਦਾ ਸ਼ਰੇਆਮ ਭਾਂਡਾ ਫੱੁਟ ਗਿਆ
ਹੈ । ਸੰਗਤ ਦੀ ਇਸ ਨਾਲ ਕੋਈ ਸਾਂਝ ਨਹੀਂ ਰਹੇਗੀ । ਉਸ ਨੇ ਦੋਸ਼ ਲਾਇਆ ਕਿ ਉਸ ਨੇ ਦੇਸ਼ ਅਤੇ
ਵਿਦੇਸ਼ ਤੋਂ ਮਾਇਆ ਇਕੱਤਰ ਕਰਕੇ ਆਪਣੇ ਨਿੱਜੀ ਹਿੱਤਾਂ ਲਈ ਵਰਤਿਆ, ਜਿਸ ਦੇ ਲਈ ਉਸ ਨੇ
ਵਿਲੇਜ ਆਫ ਚੈਰਿਟੀ ਦੇ ਨਾਂਅ ਦਾ ਇਕ ਪ੍ਰੋਜੈਕਟ ਬਣਾਇਆ ਅਤੇ ਉਸ ਦੀ ਪੂਤਰੀ ਲਈ ਪੋਜੇਵਾਲ
ਗੜ੍ਹਸੰਕਰ ਅਨੰਦਪੁਰ ਸਾਹਿਬ ਰੋਡ ਤੇ ਦੋ ਕਰੋੜ ਰੁਪਏ ਦੀ ਜ਼ਮੀਨ ਖਰੀਦਣ ਦੀ ਤਜਵੀਜ਼ ਬਣਾਈ । ਇਸ
ਤਜਵੀਜ਼ ਤਹਿਤ ਉਸ ਨੇ ਇਕ ਪਾ੍ਰਪਰਟੀ ਡੀਲਰ ਰਾਹੀ ਕਥਿਤ ਬਿਆਨਾ ਦੇਣ ਦੀ ਬਜਾਏ ਉਸ ਪੈਸੇ ਦੀਆਂ
ਕਥਿਤ ਤੌਰ ਤੇ ਉਸ ਨੇ ਨਵਾਂ ਸ਼ਹਿਰ ਦੇ ਗੁਰੂ ਅੰਗਦ ਨਗਰ ਵਿਚ ਇਕ ਕੋਠੀ ਜਿਹੀ ਜਿਸ ਦੀ ਔਸਤਨ
ਕੀਮਤ 18 ਲ਼ੱਖ ਰੁਪਏ, ਸਫਾਰੀ ਗੱਡੀ ਨੰਬਰ ਪੀ.ਬੀ. 32(2001), ਅਸਟੀਮ ਕਾਰ ਨੰਬਰ ਪੀ.ਬੀ.32ਏ.
7393 ਅਤੇ ਮਹਿੰਦਰਾ ਯੂ ਟੇਲਿਟੀ ਗੱਡੀ ਐਚ.ਆਰ.09-3154 ਖਰੀਦ ਲਈਆਂ । ਉਨ੍ਹਾ ਦੋਸ਼ ਲਾਇਆ
ਕਿ ਕਰੋੜਾਂ ਰੁਪਏ ਉਸ ਨੇ ਵਿਦੇਸ਼ਾ ਦੀ ਸੰਗਤ ਤੋਂ ਹਵਾਲਾ ਰਾਹੀ ਮੰਗਵਾਏ । ਉਨ੍ਹਾਂ ਸਾਲ 1999
ਵਿਚ ਸੰਸਥਾ ਵਲੋ ਉਕਤ ਕੇਂਦਰ ਵਿਚ ਮਨਾਏ ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦੇ ਗੱਦੀ ਨਸ਼ੀਨ ਪੁਰਬ ਸਮੇ
ਧਨਵੰਤ ਸਿੰਘ ਵਲੋ ਇਕੱਤਰ ਸੰਗਤਾਂ ਨੰੂ ਅਪੀਲ ਕਰਕੇ ਬੇਸਹਾਰਾ ਔਰਤਾਂ ਦੀ ਮਦਦ ਦੇ ਨਾਂਅ ਤੇ
ਕਿਲੋਗ੍ਰਾਮਾਂ ਨਾਲ ਇਕੱਠੇ ਕੀਤੇ ਸੋਨੇ ਦਾ ਜ਼ਿਕਰ ਕੀਤਾ ਅਤੇ ਕਿਹਾ ਕਿ ਇਸ ਮੌਕੇ ਬਾਰੇ ਕਿਸੇ ਨੰੂ
ਕੋਈ ਪਤਾ ਨਹੀਂ । ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਡੇਰੇ ਵਿੱਚ ਕਿਸੇ ਵੀ ਬੇਸਹਾਰਾ ਲੜਕੀ ਦੀ ਸ਼ਾਦੀ ਨਹੀਂ ਕੀਤੀ
ਗਈ । ਉਨ੍ਹ੍ਹਾਂ ਕਿਹਾ ਕਿ ਉਨ੍ਹਾਂ ਨੰੂ ਉਸ ਪ੍ਰਤੀ ਉਦੋਂ ਨਫਤਰ ਜ਼ਿਆਦਾ ਹੋਈ ਜਦੋਂ ਉਸ ਨੇ ਡੇਰੇ
ਅੰਦਰ ਮਨਮਾਨੀਆਂ ਕਰਨੀਆਂ ਸ਼ੁਰੂ ਕਰ ਦਿੱਤੀਆਂ । ਗੁਰਦੁਆਰੇ ਦੇ ਪ੍ਰਧਾਨ ਸ : ਗਿਆਨੀ ਸਿੰਘ ਨੇ
ਕਿਹਾ ਕਿ ਉਸ ਨੰੂ ਮੁੜ ਡੇਰੇ ਵਿਚ ਨਹੀ ਵੜਨ ਦਿੱਤਾ ਜਾਵੇਗਾ । ਇਸ ਮੌਕੇ ਹੋਰਨਾਂ ਤੋ ਇਲਾਵਾ
ਗਿ: ਹਰਬੰਸ ਸਿੰਘ ਤੇਗ , ਸੇਵਾ ਸਿੰਘ ਖਜ਼ਾਨਚੀ, ਹਰਜੀਤ ਸਿੰਘ ਨਾਗਪਾਲ, ਜਸਵੰਤ ਸਿੰਘ ਸਾਬਕਾ
ਪ੍ਰਧਾਨ ਤੇ ਤਰਲੋਕ ਸਿੰਘ ਖੰਡੂਰ ਸਾਹਿਬ ਹਾਜਰ ਸਨ । </p>

<p> </p>






</body></text></cesDoc>