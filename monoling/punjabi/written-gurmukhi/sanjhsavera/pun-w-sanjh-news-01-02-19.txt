<cesDoc id="pun-w-sanjh-news-01-02-19" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-01-02-19.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 01-02-19</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>01-02-19</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਮਜੀਠਾ ਚ ਵੋਟਾਂ ਲਈ ਸਖਤ ਸੁਰੱਖਿਆ ਪ੍ਰਬੰਧ, ਵੋਟਾਂ ਅੱਜ  </p>

<p>ਮਜੀਠਾ : ਮਜੀਠਾ
ਵਿਧਾਨ ਸਭਾ ਹਲਕੇ ਦੀ ਜ਼ਿਮਨੀ ਚੋਣ ਲਈ ਪ੍ਰਚਾਰ ਕੱਲ੍ਹ ਖਤਮ ਹੋ ਗਿਆ ਸੀ ਅਤੇ ਪ੍ਰਸ਼ਾਸ਼ਨ ਨੇ ਅੱਜ ਪੈਣ
ਵਾਲੀਆਂ ਵੋਟਾਂ ਲਈ ਪ੍ਰਬੰਧ ਮੁਕੰਮਲ ਕਰ ਲਏ ਹਨ। ਹਲਕੇ ਦੇ ਸਾਰੇ 122 ਪਿੰਡਾਂ ਵਿਚ ਸੁਰੱਖਿਆ
ਜਵਾਨ ਤਾਇਨਾਤ ਕਰ ਦਿੱਤੇ ਗਏ ਹਨ ਤਾਂ ਜੋ ਸੁਤੰਤਰ ਤੇ ਨਿਰਪੱਖ ਢੰਗ ਨਾਲ ਵੋਟਾਂ ਪੈ ਸਕਣ। ਮਜੀਠਾ
ਪੁਲਿਸ ਜ਼ਿਲੇ ਦੀ ਪੁਲਿਸ ਤੋਂ ਇਲਾਵਾ ਹੋਰ ਜਿਲਿਆਂ ਦੀ ਪੁਲਿਸ ਵੀ ਤਾਇਨਾਤ ਕੀਤੀ ਗਈ ਹੈ।
ਸੀ.ਆਰ.ਪੀ.ਐਫ ਦੀਆਂ ਤਿੰਨ ਕੰਪਨੀਆਂ ਵੀ ਹਲਕੇ ਵਿਚ ਪੁੱਜ ਗਈਆ ਹਨ। ਹਲਕੇ ਵਿਚ ਵੋਟਾਂ
ਇਲੈਕਟਰਾਨਿਕ ਮਸ਼ੀਨਾਂ ਰਾਹੀਂ ਪੈ ਰਹੀਆਂ ਹਨ।</p>

<p>          sUqr~ ny d@isE` ik mjIT` ivD`n sB`
hlky nUM j~dIE~ s`rIE` sVk~ qy bYrIkyt l` id@qy gey hn q~ jo srk`rI v`hn~ dy
d`KLy nUM roikE` j` sky| kyvl aunH~ EiDk`rIE~ dy hI srk`rI v`hn hlky ivc j`
skxgy ijMnH~ dI cox ifaUtI hovygI| bUQ~ qy kbzy dIE~ Gtn`v~ nUM rokx leI bUQ~
qy puils mul`zm q`ien`q kr id@qy gey hn| </p>

<p>          pUr` hlk` puils C`auxI ivc bdilE`
hoieE` hY| puils ny s`ry hlky nUM 12 sYktr~ ivc vMifE` hoieE` hY| hlky ivc ku@l
94 poilMg stySn Eqy 132 poilMg bUQ hn| ies s`ry ivD`nsB` hlky c 28 Eiq
sMvydnSIL Eqy 35 sMvydnSIl poilMg stySn hn| Eiq sMvydnSIl stySn~ ivc kotl`
guzr~, virE`m nMgl, jyTUv`l, qMg`lI, mjIT` inMbrivMf, civMf` dyvI, gop`lpur,
p`Krpur`, mrVI kl~, sYdpur`, c`cov`lI , Ebd`l, audokl~, sp`rIivMf, Bom`, sohIE`
kl`, n`g kl`, r`m dIv`lI E`id ipMf S`ml hn| </p>

<p>          sur@iKE` p@Ko krVI ingr`nI krn leI
hlky ivc 5 EYfISnl EYs.pI Eqy 15 fI.EYs.pIz dIE~ ifaUtIE~ lg`eIE~ geIE~ hn|
hlky ivc ku@l 700 puils krmc`rIE~ dIE~ ifaUtIE~ l`eIE~ geIE~ hn ijMnH~ ivc 233
k~stybl, 12 Q`xyd`r, 36 k~stybl, 12 ieMspYktr qy sb ieMspYktr qy 300 isp`hI
v@Kry qOr qy cox~ dOr`n poilMg bUQ~ dy b`hr q`ien`q hoxgy| </p>

<p>          iesy dOr`n puils ny n`j`iez Sr`b dy
dOr nUM rokx leI v@K v@K Q`eIN C`py m`r ky 46 ivEkqIE~ nUM igRPq`r kIq` hY|
ieMnH~ ivEkqIE~ qoN 3 l@K 7 hz`r 560 imlIlItr Sr`b br`md kIqI geI Eqy 291 iklogR`m
l`hx PVI geI hY| eynI ijE`d` Sr`b PVI j`x qy ieh is@D ho igE` hY ik hlky ivc
cox z`bqy dIE~ D@jIE` auf`eIE~ j` rhIE~ hn|  </p>

<p>ਬਾਦਲ ਮਜੀਠਾ ਜ਼ਿਮਨੀ ਚੋਣ ਲੁੱਟਣ ਦੀ ਪੂਰੀ
ਯੋਜਨਾਬੱਧ ਤਿਆਰੀ ਕਰ ਚੁੱਕੇ ਹਨ: ਬਰਾੜ  </p>

<p>ਅਮ੍ਰਿੰਤਸਰ
: ਪੰਜਾਬ ਲੋਕ ਸਭਾ ਦੇ ਕਾਂਗਰਸੀ ਮੈਂਬਰ ਸ. ਜਗਮੀਤ ਸਿੰਘ ਬਰਾੜ ਨੇ ਪੱਤਰਕਾਰਾਂ ਨੂੰ ਸੰਬੋਧਨ
ਕਰਦਿਆਂ ਕਿਹਾ ਕਿ ਪੰਜਾਬ ਦੇ ਮੁੱਖ ਮੰਤਰੀ ਸ੍ਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਮਜੀਠਾ ਜ਼ਿਮਨੀ ਚੋਣ ਲੁੱਟਣ ਦੀ
ਯੋਜਨਾਬੱਧ ਤਿਆਰੀ ਮੁਕੰਮਲ ਕਰ ਚੁੱਕੇ ਹਨ। </p>

<p>          SRI br`V ny ikh` ik jykr mjIT` ivc
izmnI cox p`rdrSI FMg n`l hoey q~ k~grs dI ij@q p@kI hY| b`dl srk`r dy EDIn hI
ieh cox Eis@Dy FMg n`l ho rhI hY| ikaNuik muK cox kimSn ny b`dl srk`r duE`r`
cox z`bqy dIE~ D@jIE~ ayuf`aux dy b`vjUd E@K~ bMd kIqIE~ hoeIE` hn| aun~H ikh`
ik jykr k~grs dy poilMg eyjMt 19 PrvrI nUM poilMg bUQ~ qy sUcyq rhy q~ b`dl dy
auumIdv`r dI Srmn`k h`r hovygI Eqy b`dl srk`r dy mUMh qy kr`rI cpyV hovygI|
</p>

<p>          SRI br`V ny ikh` ik mjIT` Kyqr dy lok
E`pxI soc nUM s@c mMn ky ibn~ iksy db`E 19 PrvrI nUM vot~ p`aux q~ b`dl dI koeI
D@kyS`hI c@lx v`lI nhIN | SRI br`V ny ikh` ik k~grs d` B@ivK pMj`b ivc pUrI
qrH~ aujl` hY ikauNik b`dl srk`r n`l hryk pMj`bI vrg pUrI qr~H inr`S ho cu@k`
hY Eqy k~grs nUM s@q` sOpx leI l`l`ieq hY| </p>

<p>          aun~ ny svIk`r kIq` hY ik k~grs d` cox
pRc`r Ek`lI B`jp` n`loN k`PI dyr n`l E`rMB hoieE` hY pr hux k~grs dI siQqI k`PI
mjbUq hY| mjIT` ivD`n sB` Kyqr dy n`girk k~grs nUM ijq`aux leI pUrI qrH~
auqS`ihq hn jykr iek hPq` pihl~ mYnUM cox pRc`r ivc bul` ilE` huMd` q~ mY
cox~vI dOr` pUrI qr~H Kqm kr lYNd`|   </p>



<p>ਸ੍ਰੀਨਗਰ 'ਚ ਤੀਜੇ ਦਿਨ ਵੀ
ਪੁਲਿਸ ਤੇ ਮੁਜਾਹਰਾਕਾਰੀਆਂ 'ਚ ਝੜਪਾਂ - 10 ਫੱਟੜ, </p>

<p>* ਫੌਜ ਨੇ ਗੋਲੀ ਚਲਾਉਣ ਦੀ ਗੱਲ ਮੰਨੀ * ਕਈ ਹੋਰ
ਥਾਵਾਂ ਤੇ ਕਰਫਿਊ * ਜੇ ਕੇ ਐਲ ਐਫ ਮੁਖੀ ਮਲਿਕ ਗ੍ਰਿਫਤਾਰ </p>

<p>ਸੀ੍ਰਨਗਰ,18 ਫਰਵਰੀ- ਅੱਜ ਤੀਸਰੇ ਦਿਨ ਵੀ ਕਾਫੀ
ਝੜਪਾਂ ਹੋਈਆਂ ਅਤੇ ਸ੍ਰੀਨਗਰ ਸ਼ਹਿਰ ਦੇ ਕੁਝ ਹੋਰ ਹਿੱਸਿਆਂ ਵਿਚ ਕਰਫਿਊ ਲਗਾਉਣ ਦੇ ਸਮਾਚਾਰ ਹਨ।
ਅੱਜ ਤੀਜੇ ਦਿਨ ਵੀ ਪੁਲੀਸ ਤੇ ਮੁਜਾਹਰਾਕਾਰੀਆਂ ਨਾਲ ਝੜਪਾਂ ਹੋਈਆਂ ਜਿਸ ਕਰਕੇ 10 ਵਿਅਕਤੀ ਸਖਤ
ਜਖ਼ਮੀ ਹੋ ਗਏ, ਜਿਹਨਾਂ ਵਿਚੋਂ 2 ਪੁਲੀਸ ਦੀ ਗੋਲੀ ਕਾਰਨ ਫੱਟੜ ਹੋਏ। ਜਦੋਂ ਕਿ ਖਾੜਕੂਆਂ ਨੇ
 iek pulIs suprfYNt dy Gr qy hml` kIq`
Eqy hornIN Q`eIN iqMn K`VkU Eqy iek sur@iKE` krmc`rI muk`bilE~ ivc m`ry gey|
</p>

<p>          v`dI
ivc jIvn burI qr~ pRB`vq irh` Eqy E`l ieMfIE` hurIEq k`nPrMs dy sInIEr E`gUE~
nUM GrIN hI njrbMd r@iKE` igE` Eqy jMmU-kSmIr PrIfm p`rtI dy muKI SbIr Eihmd
S`h nUM bIqI r`q qoN irh`sq ivc ilE` igE` hY jdoN ik jMmU-kSmIr ilbrySn PrMt dy
muKI muhMmd w`sIn nUM id@lI qoN v`ps prqidE~ hI E@j igRPq`r kr ilE` igE`| POj
ny mMinE` ik aus ny svY-r@iKE` leI muz`hr`k`rIE~ 'qy golI cl`eI sI |srk`rI
sUqr~ Enus`r nOjv`n~ dy iek toly ny Sihr ivc v@K-v@K Q`v~ ijnH~ ivc nOSihr`,
gojv`V` , nUrb`g, nYdk`dl , Kv`j` b`z`r, n`qIpor`, prIgpor`, bymIn`, hbkdl Eqy
b`b` d`mb vI S`iml hn, ivKy pQr`A kIq` Eqy t`ier~ nUM E@g lg` ky sVk~ aupr
su@ty| </p>

<p>          pulIs
ny iehn~ muj`hr`k`rIE~ nUM iq@qr ib@qr krn leI l`TI c`rj, hMJU gYs dy goly Eqy
b`Ed ivc is@DI golI cl`eI  ijs k`rn 10
lokIN burI qr~H P@tV ho gey qy pulIs golI n`l do ivEkqIE~ dI h`lq n`zuk d@sI
j~dI hY|  </p>

<p>ਲਾਵਾਰਿਸ ਲਾਸ਼ਾਂ ਦੇ ਸਸਕਾਰ ਦੀ
ਜਾਂਚ ਦਾ ਘੇਰਾ ਵਧਾਉਣ ਤੋਂ ਨਾਂਹ </p>

<p>ਨਵੀਂ ਦਿੱਲੀ,18 ਫਰਵਰੀ - ਕੌਮੀ ਮਨੁੱਖੀ ਅਧਿਕਾਰ
ਕਮਿਸ਼ਨ ਨੇ ਲਾਵਾਰਿਸ ਲਾਸ਼ਾਂ ਦਾ ਘੇਰਾ ਸਾਰੇ ਪੰਜਾਬ ਤਕ ਫੈਲਾਉਣ ਦੀ ਬੇਨਤੀ ਰੱਦ ਕਰ ਦਿੱਤੀ ਹੈ,
ਪਰ ਨਾਲ ਹੀ ਕਿਹਾ ਹੈ ਕਿ 2097 ਲਾਸ਼ਾਂ ਦੇ ਸਬੰਧ ਵਿਚ ਜਾਂਚ ਸੀ.ਬੀ.ਆਈ. ਵਲੋਂ ਕੀਤੀ ਜਾਵੇਗੀ।
ਇਹ ਸਾਰੇ ਕੇਸ ਅੰਮ੍ਰਿਤਸਰ, ਮਜੀਠਾ ਅਤੇ ਤਰਨਤਾਰਨ ਜ਼ਿਲਿਆਂ ਨਾਲ ਸਬੰਧਿਤ ਹਨ। </p>

<p>          kimSn
dy cyErmYn jsits jy.EYs.vrm` dI Egv`eI hyT kimSn dy pUrn bYNc ny ikh` ik hryk
kys ivc muE`vzy dI rkm d` PYsl` s`ry kys~ dI j~c  qoN b`Ed hI kIq` j`vyg`| kOmI mnu@KI EiDk`r kimSn ny spSt kIq` ik
ies ny auhn~ 18 kys~ ivc muE`vzy d` koeI PYsl` nhIN ilE` ijhn~ ivc pihl~ hI
pMj`b srk`r ny 1-1 l@@K rupey dI pySkS kIqI sI|iehn~ kys~ sMbMDI PYsl` vI EKIr
ivc  kIq` j`vyg`| </p>

<p>          "kmytI
P`r ienPrmySn EYf ienISIeyitv E`n pMj`b" ny kimSn nUM bynqI kIqI sI ik j~c
d` Gyr` s`ry pMj`b q@k kIq` j`vy ikauNik pulIs ny s`ry pMj`b ivc K`VkU lihr
dOr`n  Exmnu@KI qS@dd,qy byksUry lok~
nUM mUMh ivc D@kx dIE~ k`rv`eIE~ Eqy JUTy pulIs muk`bly kIqy sn | pulIs ny
iehn~ byksUry lok~ nUM qsIhy dy ky m`irE` Eqy iPr ExpC`qI l`S kih ky ssk`r kr
id@q` j~ E@D jlIE~ l`S~ smS`n G`t~ ivcoN cu@kv` ky nihr~ ndIE~ ivc su@tv`
id@qIE~ sn| ies bynqI dy jv`b ivc kOmI mnu@KI EiDk`r kimSn ny ikh` ik suprIm
kort ny 12 dsMbr 1996 nUM j~c d` kMm kimSn nMU sONpidE~ spSt qOr qy 2097 kys~
dI j~c dy hukm id@qy sn ijnH~ dI sI.bI.E`eI v@loN puStI kIqI geI sI| Ed`lq ny
jo b`Ed ivc kys~ dI suxv`eI kIqI sI aus ivc vI koeI EijhI g@l nhIN khI ik ijs
EDIn ieh Gyr` s`ry pMj`b q@k sIimq kIq` j`vy|  </p>

<p>ਫੌਜ ਵੱਲੋਂ ਵੀ ਵਾਦੀ
ਵਿਚ ਹੋਏ ਗੋਲੀਕਾਂਡਾਂ ਦੀ ਜਾਂਚ ਦੇ ਆਦੇਸ਼ </p>

<p>ਸ੍ਰੀਨਗਰ,18 ਫਰਵਰੀ - ਭਾਰਤੀ ਫੌਜ ਨੇ ਅੱਜ ਇਹ
ਪ੍ਰਵਾਨ ਕਰਦਿਆਂ ਕਿ ਉਸ ਦੇ ਦਸਤਿਆਂ ਨੇ ਆਪਣੀ ਸਵੈ-ਰੱਖਿਆ ਖਾਤਰ ਗੋਲੀ ਚਲਾਈ ਸੀ, ਕਸ਼ਮੀਰ
ਵਾਦੀ ਵਿੱਚ ਹੇਅਗਾਮ-ਸੋਪੋਰ ਅਤੇ ਮੇਅਸੁੰਮਾ ਗੋਲੀਕਾਂਡਾਂ ਦੀ ਜਾਂਚ ਦੇ ਆਦੇਸ਼ ਦਿੱਤੇ ਹਨ ਜਿਹਨਾਂ ਵਿਚ
3 ਸਿੱਖ ਵੀ ਮਾਰੇ ਗਏ ਸਨ ਅਤੇ 18 ਹੋਰ ਜਖਮੀ ਹੋ ਗਏ ਸਨ। </p>

<p>          15
kor dy km~fr lYPtInYNt jnrl jy.E`r.muKrzI ny E@j p@qrk`r~ n`l g@lb`q kridE~
ikh` ik B`vyN r`j srk`r ny inE~iek j~c dy E`dyS j`rI kIqy hoey hn pr POj E`pxy
qOr qy iehn~ dov~ Gtn`v~ dI ivsQ`rpUrvk j~c krv`eygI| ieh j~c sm~b@D hovygI| auhn~
ieh vI ikh` ik jy POj d` koeI vI jv`n ksUrv`r inkilE` q~ aus n`l k`nUMn Enus`r
inpitE` j`vyg`| lYPtInYNt jnrl muKrjI  jMmU kSmIr srk`r dy sur@iKE` sl`hk`r vI hn|  </p>

<p>ਹਰਿਆਣਾ, ਪੰਜਾਬ ਤੋਂ ਬਿਜਲੀ
ਲਵੇਗਾ ਅਤੇ ਪੰਜਾਬ ਦੇ ਪਿੰਡਾਂ ਵਿਚ ਸਿਰਫ 6 ਘੰਟੇ ਬਿਜਲੀ </p>

<p>ਫਤਿਆਬਾਦ,18 ਫਰਵਰੀ - ਹਰਿਆਣਾ ਦੇ ਮੁੱਖ ਮੰਤਰੀ
ਓਮ ਪ੍ਰਕਾਸ਼ ਚੌਟਾਲਾ ਨੇ ਨੇੜਲੇ ਪਿੰਡ ਅਹਿਰਵਾਂ ਵਿਚ ਜਨ ਸਭਾ ਨੂੰ ਸੰਬੋਧਨ ਕਰਦਿਆਂ ਦੱਸਿਆ ਕਿ
ਪੰਜਾਬ ਸਰਕਾਰ ਹਰਿਆਣਾ ਨੂੰ ਰੋਜਾਨਾ 100 ਮੈਗਾਵਾਟ ਬਿਜਲੀ ਦੇਣ ਲਈ ਸਹਿਮਤ ਹੋ ਗਈ ਹੈ। ਉਹ
ਇਥੇ 132 ਕੇ.ਵੀ.ਸਬ ਸਟੇਸ਼ਨ ਦਾ ਨੀਂਹ ਪੱਥਰ ਰੱਖਣ ਲਈ ਆਏ ਸਨ। </p>

<p>          auhn~
ikh` ik r`j nUM r`jsQ`n qoN jo 100 mYg`v`t ibjlI imldI sI auh hux bMd ho geI
hY|auhn~ d@isE` ik pMj`b dIE~ cox~ dOr`n PyrI ivc auhn~ dI pMj`b dy m@K mMqrI
pRk`S isMG b`dl n`l hoeI g@lb`q ivc qYE hoeI rxnIqI ivc PYsl` hoieE` Eqy jldI
hI l`gU vI ho j`vyg`|  </p>

<p>ਭਾਈ ਕੁਲਵੀਰ ਸਿੰਘ ਬੜਾ ਪਿੰਡ
ਨੂੰ ਭਾਰਤ ਹਵਾਲੇ ਨਾ ਕੀਤਾ ਜਾਵੇ :ਸਿਮਰਨਜੀਤ ਸਿੰਘ ਮਾਨ </p>

<p>ਬਸੀ ਪਠਾਣਾਂ,18 ਫਰਵਰੀ - ਅਕਾਲੀ ਆਗੂ ਅਤੇ ਸ਼੍ਰੋਮਣੀ
ਅਕਾਲੀ ਦਲ(ਅੰਮ੍ਰਿਤਸਰ) ਦੇ ਪ੍ਰਧਾਨ ਸ: ਸਿਮਰਨਜੀਤ ਸਿੰਘ ਮਾਨ ਵਲੋਂ ਅਮਰੀਕਾ ਸਰਕਾਰ ਨੂੰ ਜ਼ੋਰ ਦੇ
ਕੇ ਕਿਹਾ ਹੈ ਕਿ ਉਹ ਖਾੜਕੂ ਨੌਜਵਾਨ ਕੁਲਵੀਰ ਸਿੰਘ ਬੜਾ ਪਿੰਡ ਨੂੰ ਭਾਰਤ ਹਵਾਲੇ ਨਾ ਕਰੇ
ਕਿਉਂਕਿ ਇਸ ਨੌਜਵਾਨ ਨੇ ਸਿੱਖ ਕੌਮ ਦੀ ਆਜ਼ਾਦੀ ਦੇ ਸੰਘਰਸ਼ ਵਿਚ ਨਿੱਗਰ ਯੋਗਦਾਨ ਪਾਇਆ ਹੈ।
ਸ੍ਰੀ ਮਾਨ ਅਮਰੀਕਾ ਦੀ ਇੱਕ ਅਦਾਲਤ ਵਿਚ ਇਹ ਗੱਲ ਭਾਈ ਕੁਲਵੀਰ ਸਿੰਘ ਦੇ ਹੱਕ ਵਿਚ ਕਹੀ। ਇਹ
ਜਾਣਕਾਰੀ ਸ੍ਰੀ ਮਾਨ ਦੇ ਬੁਲਾਰੇ ਇਕਬਾਲ ਸਿੰਘ ਟਿਵਾਣਾ ਨੇ ਦਿੱਤੀ ਹੈ।  </p>

<p>ਕੇਂਦਰ 'ਤੇ ਘੱਟ ਗਿਣਤੀਆਂ ਦੀ
ਰਾਖੀ ਨਾ ਕਰ ਸਕਣ ਦਾ ਦੋਸ਼ </p>

<p>ਧਾਰੀਵਾਲ, 18 ਫਰਵਰੀ - ਸਾਬਕਾ ਮੰਤਰੀ ਅਤੇ
ਸਰਬਹਿੰਦ ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਦੇ ਜਨਰਲ ਸਕੱਤਰ ਸ੍ਰੀ ਸੁੱਚਾ ਸਿੰਘ ਛੋਟੇਪੁਰ ਨੇ ਕਿਹਾ ਕਿ ਕੇਂਦਰੀ
ਸਰਕਾਰ ਦੇਸ਼ ਦੀਆਂ ਘੱਟ ਗਿਣਤੀਆਂ ਅਤੇ ਖਾਸ ਕਰਕੇ ਜੰਮੂ-ਕਸ਼ਮੀਰ ਵਿਚ ਸਿੱਖਾਂ ਦੀ ਸੁਰੱਖਿਆ ਕਰਨ
ਵਿਚ ਅਸਫਲ ਰਹੀ ਹੈ। </p>

<p>          auhn~
ikh` ik n` isrP kyNdr srk`r hI sgoN jMmU-kSmIr dI srk`r vI jMmU-kSmIr ivc v@s
rhy is@K~ dI sur@iKE` ivc koq`hI krn dI doSI hY| auhn~ ikh` ik icTI isMG pur`
ivKy hoey is@K~ dy kqlyE`m smyN, sUb` srk`r Eqy kyNdr srk`r ny is@K~ nUM
sur@iKE` dyx dy ijhVy EYl`n kIqy sn, auhn~ nUM iblkul hI pUry nhIN kIq` igE`|
auhn~ sur@iKE` jon bx`aux dI g@l duhr`eI| </p>

<p>          auhn~
ieh vI doS lg`ieE` ik m`ry gey is@K~ dy pRIv`r~ nUM srk`r v@loN koeI ivSyS
sh`ieq` nhIN id@qI geI Eqy s`r` kMm l`irE` l@ipE~ n`l hI s`irE` j` irh` hY| 
</p>

<p>ਪੁਲੀਸ 'ਤੇ ਗੁਰਦੁਆਰਾ ਸਾਹਿਬ
ਦੀ ਉਸਾਰੀ ਢਾਹੁਣ ਦਾ ਦੋਸ਼ </p>

<p>ਗੁਰਦਾਸਪੁਰ, 18 ਫਰਵਰੀ - ਪੁਲੀਸ ਨੇ ਗੁਰਦਾਸਪੁਰ
ਸ਼ਹਿਰ ਦੇ ਬਿਲਕੁਲ ਨਾਲ ਲਗਦੇ ਪਿੰਡ ਬਾਬੋਵਾਲ ਵਿਖੇ ਬੀਤੀ ਰਾਤ 12.30 ਵਜੇ ਦੇ ਕਰੀਬ ਪਿੰਡ ਦੇ
ਦਲਿਤ ਲੋਕਾਂ ਵਲੋਂ ਆਰੰਭ ਕੀਤੀ ਗਈ ਭਗਤ ਰਵਿਦਾਸ ਦੇ ਗੁਰਦੁਆਰੇ ਦੀ ਉਸਾਰੀ ਢਾਹ ਦਿੱਤੀ ਤੇ
ਪੁਲ਼ੀਸ ਇਥੇ ਪਈਆਂ 20 ਹਜ਼ਾਰ ਦੇ ਕਰੀਬ ਇੱਟਾਂ, ਨਿਸ਼ਾਨ ਸਾਹਿਬ, ਸਪੀਕਰ, ਕਹੀਆਂ ਅਤੇ ਭਗਤ
ਰਵਿਦਾਸ ਦੀਆਂ ਤਸਵੀਰਾਂ ਵੀ ਚੁੱਕ ਕੇ ਲੈ ਗਈ ਅਤੇ ਪੁਲੀਸ ਨੇ 34 ਵਿਅਕਤੀਆਂ ਖਿਲਾਫ ਕੇਸ ਦਰਜ
ਕਰਕੇ ਉਹਨਾਂ ਨੂੰ ਗ੍ਰਿਫਤਾਰ ਕਰ ਲਿਆ ਹੈ । ਉਹਨਾਂ ਖਿਲਾਫ ਧਾਰਾ 447,506,148,432 ਅਧੀਨ ਕੇਸ
ਦਰਜ ਕੀਤਾ ਗਿਆ ਹੈ ਕਿ ਇਹ ਲੋਕ ਪੰਚਾਇਤ ਦੀ ਜ਼ਮੀਨ ਤੇ ਜਬਰੀ ਕਬਜ਼ਾ ਕਰ ਰਹੇ ਸਨ। </p>

<p>          irport
ivc ikh` igE` ik ipMf dy k@uJ lok~ ny irport ilKv`eI sI ik k@uJ lok ipMf dI
S`ml`t v`lI jgH` ij@Qy ipMf d` gMd` p`xI pYNd` hY aupr kbz` krn dI koiSS kr rhy
hn Eqy puLIs ny k`rv`eI krky iehn~ ivEkqIE~ nUM igRPq`r kIq` hY| puLIs r`qIN
12.30 vjy phuMcI Eqy siqsMg qy bYTy lok~ n`l h@Qo p`eI vI hoeI| ieh k`rv`eI krn
qoN pihl~ auhI pur`xy qrIky Epx`ey gey ik ipMf dy l`aUf spIkr dIE~ q`r~ qoV
id@qIE~, ipMf dy s`ry tYlIPon Kr`b kr id@qy, jo ik ipMf dI tYlIPon EYkscyNj qoN
Kr`b kIqy gey Eqy pulIs ipMf dy Gr~ dIE~ kMD~ t@p ky EMdr dKl huMdI rhI Eqy
doSIE~ nUM l@BdI rhI| </p>

<p> </p>






</body></text></cesDoc>