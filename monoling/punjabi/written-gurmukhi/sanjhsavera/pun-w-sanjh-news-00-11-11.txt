<cesDoc id="pun-w-sanjh-news-00-11-11" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-00-11-11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 00-11-11</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>00-11-11</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਨਿਊਜ਼ੀਲੈਂਡ ਵਿੱਚ ਫੈਡਰੇਸ਼ਨ ਦੀ ਇਤਹਾਸਿਕ ਜਿੱਤ </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>ਨਿਊਜ਼ੀਲੈਂਡ ਵਿੱਚ ਪੰਥਕ ਅਦਾਰਿਆਂ ਵਿੱਚ ਉਸ ਸਮੇਂ ਖੁਸ਼ੀ ਦੀ
ਲਹਿਰ ਦੌੜੀ ਜਦੋਂ ਇੰਮੀਗਰੇਸ਼ਨ ਮਨਿਸਟਰ ਵਲੋਂ ਪਿਛਲੇ ਸੱਤ ਸਾਲਾਂ ਤੋਂ ਸਕਿਊਰਟੀ ਤੋਂ ਖਤਰਾ ਸਮਝ
ਕੇ ਰੋਕੇ ਭਾਈ ਮਨਪ੍ਰੀਤ ਸਿੰਘ ਨੂੰ ਨਿਊਜ਼ੀਲੈਂਡ ਵਿੱਚ ਪੱਕੇ ਤੌਰ ਤੇ ਰਹਿਣ ਲਈ ਇਜਾਜ਼ਤ ਦੇਣ ਦਾ
ਐਲਾਨ  ਕੀਤਾ ਹੈ । </p>

<p>ਭਾਈ ਮਨਪ੍ਰੀਤ ਸਿੰਘ
ਜੋ ਕੇ ਫੈਡਰੇਸ਼ਨ ਦੇ ਪ੍ਰਧਾਨ ਦੇ ਤੌਰ ਤੇ ਸੇਵਾ ਕਰ ਰਹੇ ਹਨ ਨੂੰ ਅਸਾਈਲਮ ਦਾ ਕੇਸ ਪਾਸ ਹੋਣ ਦੇ
ਬਾਵਜੂਦ ਵੀ ਪਿਛਲੇ ਸੱਤ ਸਾਲ ਤੋਂ ਲਟਕਾਇਆ ਜਾ ਰਿਹਾ ਸੀ, ਹੁਣ ਜਦੋਂ ਕੇ ਫੈਡਰੇਸ਼ਨ ਦੇ ਆਗੂਆਂ
ਦੇ ਸਬੰਧ ਪ੍ਰਧਾਨ ਮੰਤਰੀ, ਡਿਪਟੀ ਪ੍ਰਧਾਨ ਮੰਤਰੀ, ਇੰਮੀਗਰੇਸ਼ਨ ਮਨਿਸਟਰ, ਮਨਿਸਟਰ ਆਫ ਜਸਟਸ ਅਤੇ
ਮਨਿਸਟਰ ਆਫ ਟਰਾਂਸਪੋਰਟ ਨਾਲ ਬਹੁਤ ਚੰਗੇ ਬਣ ਗਏ ਹਨ ਤਾਂ ਇਹ ਐਲਾਨ ਸੰਭਵ ਹੋਇਆ ਹੈ। </p>

<p>ਇੰਮੀਗਰੇਸ਼ਨ
ਮਨਿਸਟਰ ਨੇ ਸਿੱਖ ਆਗੂਆਂ ਨਾਲ ਮੀਟਿੰਗ ਕਰਕੇ ਸਿੱਖਾਂ ਦੀਆਂ ਬਾਕੀ ਮੁਸ਼ਕਲਾਂ ਨੂੰ ਵੀ ਹੱਲ ਕਰਨ ਦਾ
ਵਾਆਦਾ ਕੀਤਾ ਹੈ, ਜਦੋਂ ਕੇ ਮਨਿਸਟਰ ਮੈਟ ਰੌਬਸਨ ਨੇ ਪੂਰੀ ਤਾਕਤ ਲਾਉਦੇ ਇੱਥੋਂ ਤੱਕ ਕਿਹਾ ਕੇ
ਜੇ ਮਨਪ੍ਰੀਤ ਸਿੰਘ ਨੂੰ ਕਰਿਮੀਨਲ ਸਮਝਦੀ ਹੈ ਤਾਂ ਮੈਨੂੰ ਵੀ ਕਰਿਮੀਨਲ ਸਮਝਿਆ ਜਾਵੇ, ਉਹਨਾਂ ਕਿਹਾ
ਸਿੱਖ ਦੋਹੇ ਪਾਸੇ ਚੱਕੀ 'ਚ ਪਿਸ ਰਹੇ ਹਨ ।  ਕੀ ਆਪਣੇ ਹੱਕਾਂ ਲਈ ਲੜਨਾਂ ਗੁਨਾਹ ਹੈ ?
</p>

<p>ਡਿਪਟੀ ਪ੍ਰਾਈਮ
ਮਨਿਸਟਰ ਜੈਮ ਅੇਡੰਰਟਿੰਨ ਨੇ ਡਿਨਰ ਤੇ ਫੈਡਰੇਸ਼ਨ ਆਗੂਆਂ ਨੂੰ ਆਖਿਆ ਕੇ ਉਹ ਇਹ ਕੇਸ 24
ਘੰਟੇ 'ਚ ਸੁਲਝਾਉਣ ਲਈ ਮਨਿਸਟਰ ਨੂੰ ਸਲਾਹ ਦੇਣਗੇ ਤੇ ਅਗਲੇ ਹੀ ਦਿਨ ਮਾਨਙੋਗ ਮਨਿਸਟਰ ਨੇ
ਐਲਾਨ ਕਰ ਦਿੱਤਾ । </p>

<p>ਪਿਛਲੀ ਰਾਤ
ਫੈਡਰੇਸਨ ਵਲੋਂ ਧੰਨਵਾਦ ਵਜੋਂ ਕੀਤੇ ਡਿਨਰ ਵਿੱਚ ਬੋਲਦਿਆਂ ਮਾਨਯੋਗ ਮਨਿਸਟਰ ਮੈਟ ਰੌਬਸਨ ਤੇ
ਉਹਨਾਂ ਦੀ ਪਤਨੀ ਨੇ ਕਿਹਾਂ ਕੇ ਅਸੀਂ ਮਨਪ੍ਰੀਤ ਸਿੰਘ ਨੂੰ ਸਾਬਿਤ ਕੀਤਾ ਹੈ ਕੇ ਸਾਡੇ ਦੇਸ਼ ਵਿੱਚ
ਜਸਟਿਸ ਹੈ ਉਹਨਾਂ ਕਿਹਾ ਮੈਨੂੰ ਅਫਸੋਸ ਹੈ ਕੇ ਮੈਂ ਅੱਜ ਦਸਤਾਰ ਬੰਨ ਕੇ ਨਹੀਂ ਆ ਸਕਿਆ
ਕਿਉਂਕਿ ਮੈਂ ਤੁਹਾਡਾ ਭਰਾ ਤੇ ਦੋਸਤ ਹਾਂ । ਨੌਜੁਆਨ ਆਗੂ ਦਲਜੀਤ ਸਿੰਘ ਜੋ ਕੇ ਮੈਟ ਰੌਬਸਨ ਦਾ
ਗਹਿਰਾ ਦੋਸਤ ਹੈ ਨਾਲ ਮੈਟ ਰੌਬਸਨ ਨੇ ਲੱਗਭੱਗ 2 ਘੰਟੇ ਹੋਰ ਸਿਆਸੀ ਮਸਲਿਆਂ ਉਪਰ ਗੱਲਬਾਤ
ਕੀਤੀ। ਮਨਪ੍ਰੀਤ ਸਿੰਘ ਨੇ ਕਿਹਾ ਕੇ ਸਰਕਾਰ ਨੇ ਮੇਰੇ ਉਪਰ ਵਿਸ਼ਵਾਸ਼ ਕੀਤਾ ਹੈ ਮੈਂ ਇਸ ਨੂੰ
ਪੱਕਿਆ ਕਰਾਂਗਾ। </p>

<p>ਯਾਦ ਰਹੇ ਪੁਲੀਸ
ਮੁਖੀ ਬਰਾਈਨ ਰੋਵੇ ਨੇ ਪਿੱਛੇ ਜਿਹੇ ਪੇਪਰਾਂ ਵਿੱਚ ਬਿਆਨ ਦਿੱਤਾ ਸੀ ਕਿ ਫੈਡਰੇਸ਼ਨ ਇੱਕ
ਕਲਰਫੁੱਲ ਜਥੇਬੰਦੀ ਹੈ ਜਿਸ ਨੇ ਸਾਨੂੰ ਭਰੋਸੇ 'ਚ ਲੈ ਲਿਆ ਹੈ ਤੇ ਮਨਪ੍ਰੀਤ ਸਿੰਘ ਦੀ ਪ੍ਰਸੰਸ਼ਾ
ਕੀਤੀ ਸੀ। ਫੈਡਰੇਸ਼ਨ ਆਗੂਆਂ ਨੇ ਕਿਹਾ ਅਜਿਹਾ ਤਾਂ ਹੀ ਸੰਭਵ ਹੈ ਕਿੳਂੁਕਿ ਅਸੀ ਬਕਾਇਦਾ ਜਥੇਬੰਦੀ
ਰਜਿਸਟਰ ਕਰਵਾ ਕੇ ਹਰ ਕੰਮ ਕਨੂੰਨ ਦੇ ਦਾਇਰੇ 'ਚ ਕੀਤਾ ਹੈ ਸਾਨੂੰ ਅੱਜ ਖੁਸ਼ੀ ਹੈ ਕੇ ਸਰਕਾਰ
ਸਾਨੂੰ ਸਹਿਯੋਗ ਦੇ ਰਹੀ ਹੈ। ਡਯਕਟਰ ਅਮਰਜੀਤ ਸਿੰਘ ਵਾਸ਼ਿੰਗਟਨ ਅਤੇ ਭਾਈ ਸਤਿੰਦਰਪਾਲ ਸਿੰਘ
ਕੈਨੇਡਾ ਨੇ ਫੈਡਰੇਸ਼ਨ ਦੀ ਜਿੱਤ ਤੇ ਵਧਾਈ ਦਿੰਦੇ ਖੁਸ਼ੀ ਦਾ ਪ੍ਰਗਟਾਵਾ ਕੀਤਾ ਹੈ । ਟਰਾਂਟੋ ਤੋਂ
ਸੁਖਮਿੰਦਰ ਸਿੰਘ ਹੰਸਰਾ ਨੇ ਦਲਜੀਤ ਸਿੰਘ ਅਤੇ ਮਨਪ੍ਰੀਤ ਸਿੰਘ ਹੋਰਾਂ ਦੀ ਨਿਊਜੀਲੈਂਡ ਸਰਕਾਰ ਵਿੱਚ
ਆਪਣੀ ਸ਼ਾਖ ਕਾਇਮ ਕਰਨ ਤੇ ਪ੍ਰਸੰਸਾ ਕੀਤੀ। ਸਿੱਖ ਮੀਡੀਆ ਐਸੋ: ਦੇ ਮੁੱਖੀ ਸੁਖਮਿੰਦਰ ਸਿੰਘ
ਹੰਸਰਾ ਨੇ ਵੈਨਕੂਵਰ ਵਿੱਚ ਸਿੱਖ ਕੌਮ ਦੇ  ਵਕਾਰ ਨੰੂ ਲੱਗ ਰਹੀ ਸੱਟ ਤੇ ਗਹਿਰੇ ਦੁੱਖ ਦਾ
ਪ੍ਰਗਟਾਵਾ ਕਰਦਿਆਂ ਕਿਹਾ ਹੈ ਕਿ ਵੈਨਕੂਵਰ ਦੇ ਖਾਲਿਸਤਾਨੀ ਆਗੂਆਂ ਨੰੂ ਇਨ੍ਹਾਂ ਵੀਰਾਂ ਤੋਂ ਕੰਮ
ਕਰਨ ਦੀ ਜਾਂਚ ਸਿੱਖਣੀ ਚਾਹੀਦੀ ਹੈ। </p>

<p>ਗੁਰੂ
ਨਾਨਕ ਪਾਤਸ਼ਾਹ ਦੇ 532ਵੇਂ ਗੁਰਪੁਰਬ ਸੰਗਤਾਂ ਵਿੱਚ ਭਾਰੀ ਉਤਸ਼ਾਹ, 18 ਅਖੰਡ ਪਾਠਾਂ ਦੇ ਭੋਗ ਪਾਏ
ਗਏ,  ਸੰਗਤ ਨੇ ਫੁੱਲਾਂ ਦੀ ਵਰਖਾ ਕੀਤੀ</p>

<p>
 
 
<gap desc="illustration"></gap>ਮਿਸੀਸਾਗਾ:
ਗੁਰੂ ਨਾਨਕ ਪਾਤਸ਼ਾਹ ਦਾ 532ਵਾਂ ਪ੍ਰਕਾਸ਼ ਉਤਸਵ ਉਨਟਾਰੀਓ ਦੀ ਸਿੱਖ ਸੰਗਤ ਨੇ ਬੜੀ ਧੂਮ ਧਾਮ ਨਾਲ
ਮਨਾਇਆ ਹੈ। ਡਿਕਸੀ ਰੋਡ ਗੁਰੂਦੁਆਰਾ ਸਾਹਿਬ ਵਿਖੇ ਗੁਰੂ ਨਾਨਕ ਪਾਤਸ਼ਾਹ ਦੇ ਆਗਮਨ ਦੀ ਖੁਸ਼ੀ
ਵਿੱਚ 18 ਅਖੰਡ ਪਾਠ ਸਾਹਿਬਾਂ ਦੀ ਲੜੀ ਆਰੰਭ ਕੀਤੀ ਗਈ ਸੀ ਜਿਨ੍ਹਾਂ ਦਾ ਭੋਗ ਸ਼ਨੀਚਰਵਾਰ ਨੰੂ
ਸਵੇਰੇ 10 ਵਜ੍ਹੇ ਪਾਇਆ ਗਿਆ। ਹੈਰਾਨੀ ਵਾਲੀ ਗੱਲ ਇਹ ਹੈ ਇਸ ਸਮੇ੍ਹਂ 18 ਸ੍ਰੀ ਅਖੰਡਪਾਠਾਂ ਦਾ
ਭੋਗ ਪਾਇਆ ਗਿਆ ਅਤੇ 18 ਵਾਰ ਹੀ ਹੁਕਮਨਾਮਾ ਲਿਆ ਗਿਆ। ਇਹ ਅਖੰਡਪਾਠ ਵੱਖ ਵੱਖ ਸ਼ਰਧਾਲੂ
ਪ੍ਰੀਵਾਰਾਂ ਵਲੋਂ ਕਰਵਾਏ ਗਏ ਸਨ ਜਦੋਂ ਕਿ 18 ਪ੍ਰੀਵਾਰਾਂ ਵਲੋਂ ਇੱਕ ਸ੍ਰੀ ਅਖੰਡਪਾਠ ਸ਼ਰਧਾ ਨਾਲ
ਕਰਵਾਇਆ ਜਾ ਸਕਦਾ ਸੀ। ਗੁਰੂ ਨਾਨਕ ਸਾਹਿਬ ਨੇ ਧਾਰਮਿਕ ਦੁਨੀਆਂ ਵਿੱਚ ਆਈ ਗਿਰਾਵਟ ਨੰੂ ਰੋਕਣ
ਲਈ ਆਪਣੇ ਜੀਵਨ ਕਾਲ ਵਿੱਚ ਵਿਸ਼ੇਸ਼ ਉਪਰਾਲੇ ਕੀਤੇ ਸਨ ਤਾਂ ਕਿ ਸਮਾਜ ਵਿੱਚ ਧਰਮ ਦੇ ਨਾਂ ਤੇ
ਪਾਖੰਡਵਾਦ ਨੰੂ ਰੋਕਿਆ ਜਾ ਸਕੇ। ਗੁਰੂ ਸਾਹਿਬਾਂ ਨੇ ਆਪਣੇ ਉਪਦੇਸ਼ਾਂ ਵਿੱਚ ਸਿੱਖ ਨੰੂ ਬਾਬਰ ਨੰੂ
ਜਾਬਰ ਕਹਿਣ ਦੀ ਗੁੜਤੀ ਵੀ ਦਿੱਤੀ ਸੀ ਜਿਸ ਦਾ ਗੁਰਪੁਰਬ ਤੇ ਜ਼ਿਕਰ ਕਰਨਾ ਜਰੂਰੀ ਬਣਦਾ ਹੈ ।
ਡਿਕਸੀ ਰੋਡ ਗੁਰੂਦੁਆਰਾ ਸਾਹਿਬ ਵਿਖੇ ਸਾਰਾ ਦਿਨ ਨਿਰੰਤਰ ਕੀਰਤਨ ਪ੍ਰਵਾਹ ਚੱਲਦੇ ਰਹੇ ਅਤੇ ਸੰਗਤ
ਵਹੀਰਾਂ ਘੱਤ ਕੇ ਸਾਰਾ ਦਿਨ ਹੀ ਗੁਰੂਦੁਆਰਾ ਸਾਹਿਬ ਵਿਖੇ ਆਉਂਦੀ ਰਹੀ। ਸ਼ਾਮ ਨੰੂ ਵਿਸ਼ੇਸ਼ ਦੀਵਾਨ
ਸਜਾਏ ਗਏ ਜਿਸ ਵਿੱਚ ਕੀਰਤਨ ਤੋਂ ਇਲਾਵਾ ਕੁੱਝ ਵੋਟਾਂ ਵਿੱਚ ਹਿੱਸਾ ਲੈ ਰਹੇ ਉਮੀਦਵਾਰਾਂ ਨੇ
ਸੰਗਤ ਨੰੂ ਵਧਾਈਆਂ ਦਿੱਤੀਆਂ ਅਤੇ ਵੋਟਾਂ ਦੀ ਮੰਗ ਕੀਤੀ। ਰਾਤ ਦੇ 12:20 ਤੇ ਸੰਗਤ ਨੇ ਧੰਨ ਧੰਨ
ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਜੀ ਉਪਰ ਤਾਜ਼ੇ ਫੱੁਲਾਂ ਦੀ ਵਰਖਾ ਕੀਤੀ। ਸਮਾਪਤੀ ਉਪਰੰਤ ਵੱਡੀ ਤਾਦਾਦ
ਵਿੱਚ ਇਕੱਤਰ ਸੰਗਤ ਨੇ ਸੱੁਖ ਆਸਨ ਦੀ ਅਰਦਾਸ ਵਿੱਚ ਸ਼ਮੂਲੀਅਤ ਕੀਤੀ। ਇਸ ਤੋਂ ਇਲਾਵਾ ਸਮੂਹ ਗੁਰੂਦੁਆਰਾ
ਸਾਹਿਬਾਨਾਂ ਵਿੱਚ ਇਸ ਦਿਨ ਬੇਮਿਸਾਲ ਇਕੱਠ ਹੋਇਆ ਹੈ।</p>

<p>ਤਸਵੀਰਾਂ ਸਹਿਤ ਰਿਪੋਰਟ</p>

<p>ਪਾਕਿਸਤਾਨ
ਵਿਚਲੇ ਗੁਰਧਾਮਾਂ ਦੀ ਸਾਂਭ ਲਈ ਵਿਸ਼ੇਸ਼ ਸਹੂਲਤਾਂ </p>

<p>           
ਨਨਕਾਣਾ ਸਾਹਿਬ (ਪਾਕਿਸਤਾਨ), 11 ਨਵੰਬਰ - ਪਾਕਿਸਤਾਨ ਦੇ ਰਾਸ਼ਟਰਪਤੀ ਸ. ਰਫੀਕ ਤਾਰੜ ਨੇ ਅੱਜ
ਇਥੇ ਕਿਹਾ ਕਿ ਪਾਕਿਸਤਾਨ ਸਰਕਾਰ ਨੇ ਪਾਕਿਸਤਾਨ ਵਿਚਲੇ ਸਿੱਖ ਗੁਰਧਾਮਾ ਦੀ ਸਾਂਭ ਲਈ ਸਿੱਖ
ਭਾਈਚਾਰੇ ਨੂੰ ਵਿਸ਼ੇਸ਼ ਰਿਆਂਇਤਾਂ ਦਿੱਤੀਆਂ ਹੋਈਆਂ ਹਨ ਇਸ ਦਿਸ਼ਾ ਮੁਢਲਾਂ ਕਦਮ ਪਿਛਲੇ ਵਰੇ
ਸਰਕਾਰ ਵਲੋਂ ਪਾਕਿਸਤਾਨ ਸਿੱਖ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਸਥਾਪਨਾ ਕਰਨਾ ਹੈ । </p>

<p>          s. q`rV ieQy gurduE`r` sRI nnk`x`
s`ihb ivKy is@K Drm dy b`nI SRI gurU n`nk dyv jI dy 531vy pRk`S auqsv dy mOky
mn`ey j` rhy sm`gm iv@c dunIE~ Br qoN iek@Ty hoey is@K SrD`lUE~ nUM sMboDn kr
rhy sn | SrD`lU pMj`b id@lI, ieMdOr Eqy hor Q`v~ qoN ibn~ vrq`nIE~, EmrIk` ,
kYnyf`, jrmnI , h`lYNf , E`strylIE~ qoN E`ey hoey sn | ies mOky pMj`b dy gv@rnr
G@t igxqIE~ dy vjIr sRI EYs. ky. trYslr , pMj`b dy EOk`P mMqrI sRI K`ld r~J` ,
p`iksq`n is@K gurduE`r` pRbMDk kmytI dy dovy cyErmYn sI. j`vyd n`jr Eqy SRI S`m
isMG h`jr sn | b`hrly dyS` iv@c rihMdy K`ilsq`nI lihr dy kuJ sm@rQk ijnH~ iv@c
f`. gMg` isMG iF@loN Eqy gurmIq isMG EOl@K Eqy dl K`ls` dy SRI mnmohx isMG vI
S`iml sn | styj d` pRbMD b@br K`ls` ieMtrnYSl vloN sMB`ilE` igE` | pMj`b Eqy
kuJ hor Q`v` qoN ies gurpurb iv@c S`iml hox leI E`ey jiQE~ dy E`gU vI styj aqy
h`jr sn ienH~ iv@c sMq sm`j dy E`gU b`b` mMgl isMG , srb ihMd sRomxI Ek`lI dl
id@lI dy E`gU prmjIq isMG srn`, B`eI mrd`n` w`dg`rI sus`ietI dy hrp`l isMG
Bu@lr S`iml sn | styj qy suSoBq kuJ hor E`gUE~ iv@c is@K stUfYts PYfrySn dy
b`nI B`eI hrbMs l`l, pRo. drSx isMG (s`bk jQyd`r Ek`l qKq) h`lYNf qoN BuipMdr
isMG Eqy mlySIE~ qoN juigMdr isMG Eqy ivSv bYNk dy s. hrBjn isMG vI sn | </p>

<p>          sRI q`rV ny ikh` ik s`fI icr~ qoN ieC`
sI ik p`iksq`n ivcly is@K gurD`m~ dI syv` sMB`l Kud is@K hI krn Eqy ies leI
p`iksq`n srk`r vloN ipCly fyF vryN qoN ijhVy kdm cu@ky j` rhy hn aunH~ dy nqIjy
vVy vDIE~ inkl rhy hn, ijs dI ims`l E@j gurpurb iv@Ky hj`r` isMG~ dI h`jrI hY |
srk`r G@t igxqIE~ Eqy ivSyS kr ky is@K G@t igxqI dI ies DrqI qy qr@kI Eqy
KuSh`lI leI wqn pihl~ hI kr rhI hY ieh B`vy k`PI nhI iPr vI nyVly smyN iv@c ies
dI r`jI KuSI leI hor koiSS kr~gy | myr` quh`fy n`l ieh v`Ed`  hY ik EsI ieQoN dy gurduE`irE~ dI shI s~B
sMB`l vDIE` FMg n`l krdy rh`gy | </p>

<p>          ies mOky s. q`rV nUM sRI S`m isMG vloN
n`irE~ dI guMj`r iv@c isrop` id@q` igE` | auprMq pRo. drSn isMG Eqy id@lI dy
prmjIq isMG srn~ vloN s. q`rV n`l E`ey vjIr~ Eqy auc EPsr~ nUM isropy Eqy snm`n
id@qy gey gMg` isMG iF@loN vloN sRI S`m isMG nUM snm`nq kIq` igE` | ies auprMq
iek ivS`l ngr kIrqn SurU hoieE` ijs iv@c b`hroN Eqy lokl jiQE~ ny ih@s` ilE` |
ies mOky skUlI b@cy Sbd pVH rhy sn Eqy kuJ wuvk gqk`b`jI dy jOhr idK` rhy sn |
lgB@g 15 hj`r SrD`lUE~ ny ies jlUs iv@c ih@s` ilE` | </p>



<p>ਬਾਦਲ
ਸਰਕਾਰ ਦੀ ਨਾਕਾਮੀ ਕਾਰਨ ਜੁਰਮ ਨੂੰ ਰੋਕਣ ਲਈ 10 ਪੁਲੀਸ ਜ਼ਿਲਿਆਂ ਚ 15 ਹੋਰ ਕੰਪਨੀਆਂ ਭੇਜੀਆਂ </p>

<p>           
ਚੰਡੀਗੜ੍ਹ, 11 ਨਵੰਬਰ - ਪੰਜਾਬ ਵਿਚ ਹਿੰਸਕ ਵਾਰਦਾਤਾਂ ਦੀ ਵਾਧੇ ਨੂੰ ਦੇਖਦੇ ਹੋਏ 15 ਹੋਰ
ਕੰਪਨੀਆਂ 10 ਪੁਲਿਸ ਜ਼ਿਲਿਆਂ ਵਿਚ ਭੇਜੀਆਂ ਗਈਆਂ ਹਨ । ਮਹੱਤਪੂਰਨ ਸ਼ਹਿਰਾਂ ਵਿਚ ਗਸ਼ਤ ਦੇ ਨਾਲ
ਠੀਕਰੀ ਪਹਿਰੇ ਲਾਉਣ ਅਤੇ ਦੁਬਾਰਾ ਤੋਂ ਨਾਕੇ ਲਾਉਣ ਦੇ ਬਾਵਜੂਦ ਹਿੰਸਕ ਵਾਰਦਾਤਾਂ ਵਿਚ ਵਾਧਾ
ਹੋਇਆ ਹੈ । ਦੋ ਦਿਨ ਪਹਿਲਾਂ ਹੀ ਮੋਗਾ ਪੁਲਿਸ ਜ਼ਿਲੇ ਦੇ ਧਰਮਕੋਟ ਵਿਚ ਇਕ ਕਮਿਸ਼ਨ ਏਜੰਟ ਤੋਂ
4.95 ਲੱਖ ਰੁਪਏ ਲੁੱਟੇ ਗਏ ਸਨ । </p>

<p>          r`j ivc iGnOxy jurm dIE~ bhuq s`rIE~
v`rd`q~ hoeIE~ hn, ijnH~ ivc lu@t-Koh qoN pihl~ byrihmI n`l kql kIqy gey |
</p>

<p>          jlMDr, biTMf`, gurd`spur qoN ibn~ nvyN
puils ijilE~ ivc hor puils Pors ByjI geI hY jd ik jlMDr Eqy biTMf` nUM do-do
kMpnIE~ Eqy gurd`spur nUM iek kMpnI id@qI geI hY | PqihgVH s`ihb, mog` Eqy
mukqsr nvyN bxy izilE~ ivc do-do kMpnIE~ jd ik nv~Sihr, jgr`A, KMn` Eqy PrIdkot
ivc iek-iek kMpnI ByjI geI hY | gSq vD`aux Eqy ihMsk jurm dIE~ v`rd`q~ dI j~c
pVq`l krn K`iqr puils dI sh`ieq` leI ieQy puils Pors ByjI geI hY | </p>

<p>          ies s`l sqMbr dy EKIr qk pMj`b ivc
sMnH l`aux dIE~ 1349 Gtv`n~ pitE`l` ivc, 337 jlMDr Eqy 9 luiDE`x` ivc v`prIE~ |
Koh~ dy 46 kys luiDE`x`, 45 jlMDr, 34 pitE`l` Eqy 33 kys EMimRqsr 9 nvMbr qk
v`pr cu@ky sn | </p>

<p>          srk`rI sUqr~ Enus`r pR~q ivc mOjUd`
s`l dOr`n isrP 24 hiQE`rbMd fkYqI dy kys hoey jd ik lu@t~ Eqy corI dIE~ Gtn`v~
ivc v`D` hoieE` hY | 1900 corI Eqy lu@t~ dIE~ Gtn`v~ icMq` pYd` krdIE~ hn |
</p>

<p>          9-10
EkqUbr nUM kuJ ExpC`qy ivEkqI jgdIS isMG dy Gr d`Kl hoey Eqy kuJ nkdI, iek
vI.sI.E`r., iek mob`eIl Pon, iek sP`rI k`r Eqy sony dy gihxy ilj`x qoN pihl~
aunH~ dy Gr dy mYNbr~ qy hml` krky SRI jgdIS isMG, aus dI BYx ieMdrjIq kOr,
ipq` gurcrn isMG, Br` suKbIr isMG Eqy EMkl kulvMq isMG nUM gMBIr rUp ivc jKmI
kIq` | ies qoN iqMn idn pihl~ hI luiDE`x` nyVy FMf`rI kl~ ivc iqMn ivEkqIE~
ijnH~ ivc ibjnsmYn Eqy ausd` g`rf vI S`ml hY, qoN ipsqOl dI nok qy 9.05 l@K rupey
lu@ty gey | </p>

<p>          22
EkqUbr nUM do ivEkqIE~ nUM igRPq`r kIq` igE` ijnH~ ny iek EOrq koloN sony dI
muMdrI KohI sI Eqy mlyrkotl` ivc pYNdy noD`rnI ipMf ivc ifaUtI qy h`ijr do
puils v`ilE~ aupr hml` kIq` sI | sMgrUr ijly dy ipMf grQ`lI ivc iek hor Gtn`
dOr`n kuJ ExpC`qy hiQE`rbMd lutyry suirMdr isMG dy fyErI P`rm h`aUs qy E`ey Eqy
E`pxy n`l 13 bld lY gey | iesy idn hI iProjpur ivc SRI EjY bj`j dy Gr kuJ
hiQE`rbMd ivEkqI d`Kl hoey Eqy 2.7 l@K rupey nkd, 5-7 iklogR`m dy sony dy
gihxy, 4.1 iklogR`m dI c~dI dy is@ky, iek 32 bor dI irv`lvr Eqy 7 k`rqUs lu@t
ky lY gey | iesy idn hI PrIdkot ijly ivc jYqo dy vsnIk nUM zKmI krky aus qoN
1.36 l@K rupey c`r ivEkqIE~ duE`r` lu@tx dI Gtn` vI v`prI | </p>

<p>          1
nvMbr nUM nkodr ivc v`prI Gtn` dOr`n kuJ hiQE`rbMd lutyirE~ ny qyj D`r hiQE`r~
n`l suMdr isMG d` kql kIq` Eqy do hor ivEkqIE~ nUM gMBIr rUp ivc zKmI krn mgroN
sony dy gihxy lu@t ky lY gey | </p>

<p>          lu@t~-Koh~
Eqy corI dIE~ ihMsk v`rd`q~ ivc hoey v`Dy ny lok~ EMdr fr pYd` kr id@q` hY |
EiDk`rIE~ d` kihx` hY ik puils Pors ivc kIqy v`Dy ivc lok~ ivc kuJ ivSv`s
E`vyg` jd ik ihMsk v`rd`q~ nUM rokx leI kuJ hor kdm vI cu@ky j`xgy ijnH~ b`ry
vyrv` dyx qoN EiDk`rIE~ ny kuJ k`rn~ krky ienk`r kIq` | </p>

<p>ਬਾਦਲ ਦਾ ਬਦਲ ਲੱਭਣ ਦੀਆਂ
ਕੋਸ਼ਿਸ਼ਾਂ ਤੇਜ਼, 15 ਨਵੰਬਰ ਨੂੰ ਪੰਥਕ ਕਨਵੈਨਸ਼ਨ </p>

<p>          luiDE`x`,
11 nvMbr - is@K pMQ nUM drpyS Ejoky sMkt d` h@l ql`Sx leI 15 nvMbr, 2000 nUM
iek mh`n pMQk knvYnSn ho rhI hY | ieh knvYnSn s`rIE~ ivroDI p`rtIE~ vloN iml ky
kr`eI j` rhI hY | ies ivc pMQ dy E`gU b`b` srbjoq isMG bydI, pRD`n, gurmiq
pRc`rk sMq sm`j, ismrjIq isMG m`n, pRD`n, SRomxI Ek`lI dl (E), gurcrn isMG
tOhV`, pRD`n, srb ihMd SRomxI Ek`lI dl, jsbIr isMG rofy, pRD`n, SRomxI Ek`lI dl
(pMQk), kuldIp isMG vf`l`, pRD`n, SRomxI Ek`lI (fYmokryitk) ih@s` lY rhy hn |
</p>

<p>          auprokq
s`ry lIfr~ dy n`m qy EKb`r~ ivc v@fy-v@fy ieSiqh`r vI E` rhy hn ik auh 10 vjy
gurduE`r` sbjImMfI, luiDE`x` ivKy iel`ky dy pRqIinD~ Eqy pMQk s@jx~ smyq phuMc
ky h`jrI l`aux | ies knvYnSn ivc b`dl srk`r dy kMm~ d` lyK`-joK` kIq` j`vyg`
Eqy E`aux v`lIE~ ivD`n sB` dIE~ cox~ b`ry ivc`r hovygI | </p>

<p>          15
nvMbr nUM hox v`lI ies knvYnSn dI hor vI mh@qq` ies krky v@D geI hY ik ies
mhIny dy ivc hI SRomxI gurduE`r` pRbMDk kmytI dy pRD`n Eqy hor k`rjk`rI mYNbr~
dI cox hoxI hY |</p>

<p> </p>






</body></text></cesDoc>