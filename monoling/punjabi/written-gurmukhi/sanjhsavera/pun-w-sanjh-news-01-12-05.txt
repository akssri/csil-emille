<cesDoc id="pun-w-sanjh-news-01-12-05" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-01-12-05.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 01-12-05</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>01-12-05</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਗੁਰਪੁਰਬ ਦੀਆਂ ਲੱਖ
ਲੱਖ ਵਧਾਈਆਂ ਹੋਣ। </p>

<p>ਯੂਥ
ਵਿੰਗ ਦੇ ਪੁਨਰਗਠਨ ਦਾ ਮਾਮਲਾ ਲਟਕਿਆ, ਵਰਕਰ ਨਿਰਾਸ਼</p>

<p>ਮੋਗਾ : ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਵੱਲੋਂ
ਪਾਰਟੀ ਦੇ ਯੂਥ ਵਿੰਗ ਦੇ ਪੁਨਰਗਠਨ ਦਾ ਮਾਮਲਾ ਫਿਲਹਾਲ ਟਾਲ ਦਿੱਤੇ ਜਾਣ ਕਾਰਨ ਵਿੰਗ ਦੇ ਆਗੂਆਂ ਤੇ
ਵਰਕਰਾਂ ਵਿਚ ਨਿਰਾਸ਼ਾ ਹੈ। ਭਰੋਸੇਯੋਗ ਸੂਤਰਾਂ ਦੇ ਦੱਸਣ ਮੁਤਾਬਕ ਮੁੱਖ ਮੰਤਰੀ ਅਤੇ ਪਾਰਟੀ ਪ੍ਰਧਾਨ
ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਨੇ ਯੂਥ ਵਿੱਚ ਦੀ ਪ੍ਰਧਾਨਗੀ ਲਈ ਸਿੱਰਕੱਢ ਆਗੂਆਂ ਦੀ ਲੰਬੀ ਹੁੰਦੀ ਜਾ ਰਹੀ
ਸੂਚੀ ਦੇ ਮੱਦੇਨਜ਼ਰ ਅਤੇ ਆਉਂਦੀਆਂ ਵਿਧਾਨਸਭਾ ਚੋਣਾਂ ਵਿਚ ਪਾਰਟੀ ਲਈ ਕੋਈ ਮੁਸੀਬਤ ਮੁੱਲ ਨਾ
ਲੈਣ ਦੀ ਮਨਸ਼ਾ ਨਾਲ ਯੂਥ ਵਿੰਗ ਦਾ ਨਵਾਂ ਪ੍ਰਧਾਨ ਬਣਾਉਣ ਦੀ ਯੋਜਨਾ ਫ਼#145;ਤੇ ਹਾਲ ਦੀ ਘੜੀ ਅਮਲ
ਰੋਕ ਦਿੱਤਾ ਹੈ। </p>

<p>ਪੰਜਾਬ ਫ਼#145;ਚ
ਚੁੰਗੀ ਖਤਮ ਹੋਣ ਨਾਲ ਭਾਜਪਾ ਦੇ ਹੌਂਸਲੇ ਬੁਲੰਦ</p>

<p>ਚੰਡੀਗੜ੍ਹ : ਪਹਿਲੀ ਦਸੰਬਰ ਤੋਂ ਚੁੰਗੀ
ਖਤਮ ਹੋਣ ਨਾਲ ਪੰਜਾਬ ਭਾਜਪਾ ਦੇ ਹੌਂਸਲੇ ਬੁਲੰਦ ਹੋ ਗਏ ਹਨ। ਚੋਣਾਂ ਸਮੇਂ ਕੀਤੇ ਵਾਅਦੇ ਨੂੰ
ਪੰਜਾਬ ਵਿਧਾਨ ਸਭਾ ਦੀਆਂ ਚੋਣਾਂ ਸਮੇਂ ਕੀਤੇ ਵਾਅਦੇ ਨੂੰ ਪੰਜਾਬ ਵਿਧਾਨਸਭਾ ਦੀਆਂ ਚੋਣਾਂ ਦੇ ਨਜ਼ਦੀਕ
ਮੰਨ ਲੈਣਾ ਇਸ ਪਾਰਟੀ ਲਈ ਬਹੁਤ ਲਾਭਦਾਇਕ ਮੰਨਿਆ ਜਾ ਰਿਹਾ ਹੈ। ਸਵੈ-ਸ਼ਾਸ਼ਨ ਸਰਕਾਰ ਦੇ ਮੰਤਰੀ
ਬਲਰਾਮਜੀ ਦਾਸ ਟੰਡਨ, ਜਿਨ੍ਹਾਂ ਨੇ ਮੰਤਰੀ ਮੰਡਲ ਦੀ ਮੀਟਿੰਗ ਵਿਚ ਇਹ ਤਜ਼ਵੀਜ ਪੇਸ਼ ਕੀਤੀ ਸੀ,
ਇਤਫਾਕ ਵਸ ਉਨਾਂ੍ਹ ਦੀ ਪ੍ਰਧਾਨਗੀ ਹੇਠ ਹੋਈ ਮੀਟਿੰਗ ਵਿੱਚ ਚੁੰਗੀ ਖਤਮ ਕਰਨ ਦਾ ਫੈਸਲਾ ਕੀਤਾ
ਗਿਆ ਹੈ। </p>

<p>ਪੰਜਾਬ ਦਾ ਅਗਲਾ
ਮੁੱਖ ਮੰਤਰੀ ਕੌਣ ?</p>

<p>27ਨਵੰਬਰ ਦੀ ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ
ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ ਦੀ ਚੋਣ ਨੇ ਪੰਜਾਬ ਦੀ ਰਾਜਨੀਤੀ ਵਿੱਚ ਪੁੰਗਰਦੀਆਂ ਕੁਝ ਨਵੀਂਆਂ
ਸੰਭਾਵਨਾਵਾਂ ਫ਼#145;ਤੇ ਮਿੱਟੀ ਪਾ ਦਿੱਤੀ ਹੈ। ਨਵੀਂ ਦਿੱਲੀ ਦੇ ਗੰਗਾਰਾਮ ਹਸਪਤਾਲ ਵਿੱਚ ਦਾਖਲ
ਮੁੱਖ ਮੰਤਰੀ ਸ਼੍ਰ: ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਦਾ ਹਾਲ-ਚਾਲ ਪੁੱਛਣ ਬਹਾਨੇ ਉਨ੍ਹਾਂ ਨੂੰ ਮਿਲੇ ਪੰਥਕ ਮੋਰਚੇ
ਦੇ ਆਗੂ ਆਕੇ ਘਾਗ ਅਕਾਲੀ ਰਾਜਨੀਤੀ ਵਿੱਚ ਕਈ ਨਵੀਆਂ ਕਿਆਸ ਅਰਾਈਆਂ ਨੂੰ ਜਨਮ ਦਿੱਤਾ ਸੀ।
ਭਾਵੇਂ ਕਿ ਦੋਹਾਂ ਧਿਰਾਂ ਵੱਲੋਂ ਕਿਹਾ ਗਿਆ ਸੀ ਕਿ ਇਸ ਮਿਲਣੀ ਪਿੱਛੇ ਕੋਈ ਸਿਆਸਤ ਨਹੀਂ ਹੈ
ਅਤੇ ਨਾ ਹੀ ਇਸ ਭੇਟ ਵਾਰਤਾ ਵਿੱਚ ਕੋਈ ਸਿਆਸੀ ਵਿਚਾਰ ਹੀ ਹੋਈ ਹੈ, ਪ੍ਰੰਤੂ ਉਸ ਤੋਂ ਬਾਅਦ
ਦੇ ਅਕਾਲੀਆਂ ਬਾਦਲ ਅਤੇ ਪੰਥਕ ਮੋਰਚੇ ਦੇ ਨੇਤਾਵਾਂ ਦੇ ਅਖਬਾਰੀ ਬਿਆਨ ਅਕਾਲੀ ਦਲ ਵਿੱਚ ਪੰਜਾਬ
ਵਿਧਾਨਸਭਾ ਚੋਣਾਂ ਦੇ ਮਨੋਰਥ ਨਾਲ ਏਕਤਾ ਵੱਲ ਕਦਮ ਪੁੱਟਦੇ ਨਜ਼ਰ ਆਉਣ ਲੱਗ ਪਏ ਸਨ। </p>

<p>ਕਾਮਰੇਡ ਵੀ
ਆਪਸ ਵਿੱਚ ਪਾਟੇ !!!</p>

<p>ਜਲੰਧਰ: ਭਾਰਤੀ ਕਮਿਊਨਿਸਟ ਪਾਰਟੀ ਅੰਦਰਲੀ
ਫੁੱਟ ਅਜ ਉਸ ਸਮੇਂ ਨਵੇਂ ਦੌਰ ਵਿਚ ਦਾਖਲ ਹੋ ਗਈ, ਜਦੋਂ ਸਾਬਕਾ ਸੂਬਾ ਸਕੱਤਰ ਕਾਮਰੇਡ ਮੰਗਤ
ਰਾਮ ਪਾਸਲਾ ਨੇ ਕੇਂਦਰੀ ਕਮੇਟੀ ਦੇ ਜਨਰਲ ਸਕੱਤਰ ਅਤੇ ਪੋਲਿਟ ਬਿਊਰੋ ਮੈਂਬਰ ਕਾਮਰੇਡ ਹਰਕਿਸ਼ਨ
ਸਿੰਘ ਸੁਰਜੀਤ ਵੱਲੋਂ ਪਾਰਟੀ ਲਾਈਨ ਤੋਂ ਹਟ ਕੇ ਚੱਲਣ ਵਿਰੁਧ ਖੁੱਲ੍ਹਮ-ਖੁੱਲ੍ਹੀ ਬਗਾਵਤ ਕਰਦਿਆਂ
ਸੁਰਜੀਤ ਸਿੰਘ ਧੜੇ ਗੜ੍ਹਸ਼ੰਕਰ ਵਿਖੇ 14 ਅਤੇ 15 ਦਸੰਬਰ ਨੰੂ ਸੱਦੇ ਹੋਏ ਪਾਰਟੀ ਦੇ ਸੂਬਾ ਪੱਧਰੀ
ਅਜਲਾਸ ਦੇ ਮੁਕਾਬਲੇ 13 ਅਤੇ 14 ਦਸੰਬਰ ਨੰੂ ਜਲੰਧਰ ਵਿਖੇ ਬਾਗੀ ਗਰੁੱਪ ਦੇ ਵੱਖਰੇ ਅਜਲਾਸ
ਬੁਲਾਏ ਜਾਣ ਦਾ ਐਲਾਨ ਕਰ ਦਿੱਤਾ।</p>

<p>ਫ਼#145;ਇਕ ਹੱਥ
ਦੇ ਤੇ ਇੱਕ ਹੱਥ ਲੈਫ਼#146; ਵਾਲਾ ਬਣਦਾ ਜਾ ਰਿਹੈ ਚੁੰਗੀ ਮਾਫੀ ਦਾ ਫੈਸਲਾ</p>

<p>ਲੁਧਿਆਣਾ:- ਪੰਜਾਬ ਅੰਦਰ ਚੋਣਾਂ ਮੌਕੇ
ਵਪਾਰੀ ਤੇ ਸ਼ਹਿਰੀ ਵਰਗ ਨੂੰ ਖੁਸ਼ ਕਰਨ ਲਈ ਚੁੰਗੀ ਮਾਫੀ ਦੇ ਫੈਸਲੇ ਕਾਰਨ ਭਾਵੇਂ ਭਾਰਤੀ ਜਨਤਾ
ਪਾਰਟੀ ਖੁਸ਼ੀ ਨਾਲ ਖੀਵਾ ਹੋਈ ਫਿਰਦੀ ਹੈ, ਪਰ ਪਾਰਖੂ ਤੇ ਸਿਆਸੀ ਸੋਚ ਰੱਖਣ ਵਾਲੇ ਵਪਾਰੀ ਆਗੂ
ਇਸ ਫੈਸਲੇ ਨੂੰ ਸਰਕਾਰ ਦਾ ਫ਼#145;ਇੱਕ ਹੱਥ ਦੇ ਤੇ ਦੂਜੇ ਹੱਥ ਲੈਫ਼#146; ਵਾਲਾ ਚਾਣਕਿਆ
ਫਾਰਮੂਲਾ ਦੱਸ ਰਹੇ ਹਨ। ਸ਼ਹਿਰੀਆਂ
ਤੇ ਵਪਾਰੀਆਂ ਦੀ ਨੁਮਾਇੰਦਾ ਅਖਵਾਉਂਦੀ ਭਾਜਪਾ ਦੇ ਦਬਾਅ ਹੇਠ ਲਏ ਗਏ ਫੈਸਲੇ ਬਾਰੇ ਜਦੋਂ
ਵਪਾਰੀ ਭਾਈਚਾਰੇ ਦੀ ਨਬਜ਼ ਟੋਹੀ ਗਈ ਤਾਂ ਉਨਾਂ੍ਹ ਦੱਸਿਆ ਕਿ ਸਰਕਾਰ ਇੱਕ ਪਾਸੇ ਤਾਂ ਚੁੰਗੀ ਮਾਫ
ਕਰਕੇ ਵਪਾਰੀਆਂ ਨੂੰ ਖੁਸ਼ ਕਰ ਰਹੀ ਹੈ ਤੇ ਦੂਜੇ ਪਾਸੇ ਚੁੰਗੀ ਮਾਫੀ ਦੇ ਖੱਪੇ ਨੂੰ ਭਰਨ ਲਈ ਸੇਲ
ਟੈਕਸ ਫ਼#145;ਤੇ ਸੈਸੱ ਤੇ ਪ੍ਰਚੇਜ਼ ਰਿਟਰਨ, ਗੁੱਡਜ਼ ਅਂੈਟਰੀ ਟੈਕਸ ਰਾਹੀਂ ਵਪਾਰੀਆਂ ਫ਼#145;ਤੇ
ਦੂਣਾ ਚੂਣਾਂ ਬੋਝ ਲੱਦਣ ਦੇ ਚੱਕਰ ਵਿੱਚ ਹੈ।</p>

<p>ਬਡੂੰਗਰ ਨੇ
ਪਹਿਲੇ ਵਾਰਫ਼#146;ਚ ਤਲਵੰਡੀ ਦਾ ਰਿਸ਼ਤੇਦਾਰ ਨੁਕਰੇ ਲਾਇਆ</p>

<p>ਅੰਮ੍ਰਿਤਸਰ: ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਭੰਧਕ
ਕਮੇਟੀ ਦੇ ਨਵੇਂ ਬਣੇ ਪ੍ਰਧਾਨ ਸ੍ਰ: ਕ੍ਰਿਪਾਲ ਸਿੰਘ ਬਡੰੂਗਰ ਨੇ ਸਾਬਕਾ ਪ੍ਰਧਾਨ ਜਥੇਦਾਰ ਜਗਦੇਵ ਸਿੰਘ
ਤਲਵੰਡੀ ਨਾਲ ਰਾਜਸੀ ਕਿੜ ਕੱਢਦਿਆਂ ਉਨ੍ਹਾਂ ਦੇ ਰਿਸ਼ਤੇਦਾਰ ਤੇ ਸਮਰੱਥਕ ਮੁਲਾਜ਼ਮਾ ਨੰੂ ਖੁੱਡੇ
ਲਾਉਣ ਦਾ ਪਹਿਲਾਂ ਕੁਹਾੜਾ ਚਲਾਉਦਿਆਂ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਦੇ ਚੰਡੀਗੜ੍ਹ ਸਥਿਤ ਸਬ-ਆਫਿਸ ਦੇ
ਇੰਚਾਰਜ ਸ੍ਰ: ਜਵਾਹਰ ਸਿੰਘ ਨੰੂ ਤਬਦੀਲ ਕਰਕੇ ਸਿੱਖ ਰੈਫਰੈਂਸ ਲਾਏਬੇ੍ਰਰੀ ਅਤੇ ਰਿਸਰਚ ਸੈਂਟਰ
ਵਿਖੇ ਲਗਾ ਦਿੱਤਾ ਹੈ ਅਤੇ ਰੋਸ ਵਜੋਂ ਸ: ਜਵਾਹਰ ਸਿੰਘ ਦੇ ਅਸਤੀਫਾ ਦੇਣ ਦੇ ਵੀ ਚਰਚੇ ਹਨ।</p>

<p>ਪੰਥਕ ਮੋਰਚੇ
ਨੰੂ ਤਾਕਤ ਮੁਤਾਬਕ ਹੀ ਸੀਟਾਂ ਛਡਾਂਗੇ: ਕਾਸ਼ੀ ਰਾਮ</p>

<p>ਬੁਢਲਾਵਾ: ਬਹੁਜਨ ਸਮਾਜ ਪਾਰਟੀ ਜਾਤ-ਪਾਤ
ਮਿਟਾ ਕੇ ਇੱਕ ਮਜ਼ਬੂਤ ਭਾਈਚਾਰਾ ਕਾਇਮ ਕਰਕੇ ਅਗਲੀ ਸਰਕਾਰ ਬਣਾਏਗੀ। ਇਹ ਸ਼ਬਦ ਅੱਜ ਇਥੇ ਤਾਜ
ਪੈਲੇਸ ਵਿਖੇ ਅੱਗਰਵਾਲ ਸਮਾਜ ਦੇ ਵੱਡੇ ਇੱਕਠ ਨੰੂ ਸੰਬੋਧਨ ਕਰਦੇ ਹੋਏ ਬਸਪਾ ਸੁਪਰੀਮੋ ਸ੍ਰੀ
ਕਾਂਸ਼ੀ ਰਾਮ ਨੇ ਕਹੇ । ਉਨ੍ਹਾਂ ਸਪੱਸ਼ਟ ਕੀਤਾ ਕਿ ਮੇਰਾ ਨਿਸ਼ਾਨਾ ਚੰਡੀਗੜ੍ਹ ਨਹੀਂ ਦਿੱਲੀ ਤੇ ਤਖਤ
ਪਹੁੰਦਣਾ ਹੈ। ਇਸ ਤੋਂ ਬਾਅਦ ਸ੍ਰੀ ਕਾਂਸ਼ੀ ਰਾਮ ਨੇ ਪ੍ਰੈਸ ਕਾਨਫਰੰਸ ਦੌਰਾਨ ਕਿਹਾ ਕਿ ਉਹ ਪੰਥਕ
ਮੋਰਚੇ ਨਾਲ ਸਮਝੌਤੇ ਦੇ ਇਛੁੱਕ ਤਾਂ ਹਨ, ਪ੍ਰੰਤੂ ਹਾਲੇ ਤੱਕ ਮੋਰਚੇ ਨੇ ਆਪਣੀ ਤਾਕਤ ਦਾ ਪ੍ਰਦਰਸ਼ਨ
ਨਹੀਂ ਕੀਤਾ। ਸਿਰਫ ਮੇਲਿਆਂਫ਼#146;ਤੇ ਹੀ ਕਾਨਫਰੰਸਾਂ ਕੀਤੀਆਂ ਹਨ। ਬੀ.ਐਸ.ਪੀ.ਪੰਥਕ ਮੋਰਚੇ ਦੀ
ਤਾਕਤ ਮੁਤਾਬਕ ਹੀ ਸੀਟਾਂ ਛਡੇਗੀ।</p>

<p>ਕੈਪਟਨ ਅਮਰਿੰਦਰ
ਸਿੰਘ ਹੀ ਮੁੱਖ ਮੰਤਰੀ ਬਣਨਗੇ: ਜਗਮੀਤ ਬਰਾੜ</p>

<p>ਬਾਘਾ ਪੁਰਾਣਾ: ਪੰਜਾਬ ਪ੍ਰਦੇਸ਼ ਕਾਂਗਰਸ ਆਉਂਦੀਆਂ
ਵਿਧਾਨ ਸਭਾ ਚੋਣਾਂ ਵਿੱਚ ਖੱਬੇ ਪੱਖੀ ਪਾਰਟੀਆਂ ਨਾਲ ਗੱਠਜੋੜ ਕਰੇਗੀ ਅਤੇ ਇਸ ਸੰਬੰਧੀ ਉਨ੍ਹਾਂ ਨਾਲ
ਗੱਲ਼ਬਾਤ ਚੱਲ ਰਹੀ ਹੈ। ਆਪਣੇ ਵਾਲੇ ਕੁਝ ਦਿਨਾਂ ਤੱਕ ਸੀਟਾਂ ਦਾ ਤਾਲਮੇਲ ਕਰ ਲਿਆ ਜਾਵੇਗਾ।
ਇਹ ਗੱਲ ਅੱਜ ਕਸਬਾ ਸਮਾਲਸਰ ਵਿਖੇ ਜ਼ਿਲ੍ਹਾ
ਕਾਂਗਰਸ ਕਮੇਟੀ ਮੋਗਾ ਵੱਲੋਂ ਕੀਤੀ ਗਈ ਵਿਸ਼ਾਲ ਰੈਲੀ ਨੰੂ ਸੰਬੋਧਨ ਕਰਦਿਆਂ ਪੰਜਾਬ ਪ੍ਰਦੇਸ਼ ਕਾਂਗਰਸ
ਕਮੇਟੀ ਦੇ ਸਾਬਕਾ ਪ੍ਰਧਾਨ ਸ੍ਰੀ ਵਰਿੰਦਰ ਕਟਾਰੀਆਂ ਨੇ ਕਹੀ। ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਇਨ੍ਹਾਂ ਤੋਂ ਬਿਨਾਂ
ਹੋਰ ਕਿਸੇ ਵੀ ਪਾਰਟੀ ਨਾਲ ਚੋਣ ਸਮਝੌਤਾ ਹੋਣ ਦੀ ਕੋਈ ਸੰਭਾਵਨਾ ਨਹੀਂ । </p>

<p>ਹਰਭਜਨ ਸਿੰਘ ਨੇ
ਸਪਿਨ ਦਾ ਜਾਦੂ ਮੁੜ ਦਿਖਾਇਆ</p>

<p>ਮੁਹਾਲੀ: ਭਾਰਤ ਦੇ ਸਪਿਨਰ ਹਰਭਜਨ ਸਿੰਘ
ਨੇ ਤਿੰਨ ਟੈਸਟ ਮੈੱਚਾਂ ਦੇ ਪਹਿਲੇ ਮੈਚ ਦੀ ਪਹਿਲੀ ਪਾਰੀ ਫ਼#146;ਚ 5 ਵਿਕਟਾਂ ਲੈਂਦਿਆਂ ਇੰਗਲੈਂਡ
ਦਾ 238 ਫ਼#146;ਤੇ ਬਿਸਤਰਾ ਗੋਲ ਕਰ ਦਿੱਤਾ, ਜੋ ਕਿ ਵੱਡਾ ਸਕੋਰ ਖੜਾ ਕਰਨ ਦੀ ਧਾਰੀ ਬੈਠਾ ਸੀ ।
ਭਾਵੇਂ ਭਾਰਤ ਦੇ ਨਵੇਂ ਗੇਂਦਬਾਜ਼ ਟੀਨੰੂ
ਜੋਹਾਨਨ ਪਹਿਲੇ ਟੈੱਸਟ ਮੈਚ ਦੇ ਪਹਿਲੇ ਦਿਨ ਦੀ ਖੇਡ ਦੇ ਆਪਣੇ ਪਹਿਲੇ ਦਿਨ ਦੀ ਖੇਡ ਦੇ ਆਪਣੇ
ਪਹਿਲੇ ਓਵਰਫ਼#146;ਚ ਇੰਗਲੇਂਡ ਦੇ ਤਕੜੇ ਬੱਲ਼ੇਬਾਜ਼ ਮਾਰਕ ਬੌਚਰ ਦੀ ਵਿਕਟ ਲੈ ਲਈ ਸੀ, ਪਰ ਇਸ
ਤੋਂ ਬਾਅਦ ਇੰਗਲੈਂਡ ਦੇ ਬੱਲੇਬਾਜ਼ਾਂ ਨੇ ਸੁਰਤ ਸੰਬਾਲੀ ਅਤੇ ਭਾਰਤੀ ਬੱਲੇਬਾਜ਼ਾ ਦੀ ਜੰਮ ਕੇ
ਧੁਲਾਈ ਕੀਤੀ ।</p>

<p> </p>






</body></text></cesDoc>