<cesDoc id="pun-w-sanjh-news-02-09-11" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-02-09-11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 02-09-11</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>02-09-11</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਰਾਜਧਾਨੀ
ਐਕਸਪ੍ਰੈਸ ਨੰੂ ਹਾਦਸਾ, 120 ਤੋਂ ਵੱਧ ਮੌਤਾਂ ਦਾ ਖ਼ਦਸ਼ਾ</p>

<p>ਰਫੀਗੰਜ: ਹਾਵੜਾ
ਤੋਂ ਨਵੀਨ ਦਿੱਲ਼ੀ ਜਾਂ ਰਹੀ ਜਰਾਜਧਾਨੀ ਐਕਸਪੈ੍ਰਸ ਦੇ 18 ਵਿਚੋਂ 15 ਡੱਬੇ ਪਟੜੀ ਤੋਂ ਲਹਿਣ ਕਾਰਨ
ਵਾਪਰੇ ਹਾਦਸੇ ਵਿਚ ਘਟੋਂ ਘੱਟ 120 ਵਿਅਕਤੀਆਂ ਦੇ ਮਾਰੇ ਜਾਣ ਦਾ ਖ਼ਾਦਸਾ ਹੈ । ਹਾਦਸੇ ਵੇਲੇ ਰੇਲ
ਗੱਡੀ ਔਰੰਗਾਬਾਦ ਜ਼ਿਲੇ ਵਿਚ ਧਾਬੀ ਦਰਿਆਂ ਦਾ ਪੁਲ ਪਾਰ ਕਰ ਰਹੀ ਸੀ। </p>

<p>ਕਸ਼ਮੀਰ'ਚ ਪੀ.ਡੀ.ਪੀ. ਦੇ
ਦੋ ਆਗੂਆਂ ਸਮੇਤ 15 ਹਲਾਕ</p>

<p>ਸ੍ਰੀਨਗਰ:
ਬਾਰਾਮੂਲਾ ਵਿਧਾਨ ਸਭਾ ਹਲਕੇ ਤੋਂ ਭਾਰਤੀ ਜਨਤਾ ਪਾਰਟੀ ਦਾ ਉਮੀਦਵਾਰ ਕੱਲ਼੍ਹ ਰਾਤੀਂ ਸਿੰਘਾਪੁਰਾ
ਵਿਖੇ ਆਪਣੀ ਰਿਹਾਇਸ਼'ਤੇ ਅਤਿਵਾਦੀਆਂ ਵੱਲੋਂ
ਆਟੋਮੈਟਿਕ ਹਥਿਆਰਾਂ ਨਾਲ ਕੀਤੇ ਇਕ ਕਾਤਲਾਨਾ ਹਮਲੇ'ਚ ਸਾਫ ਬਚ ਗਿਆ ਅਤੇ ਵਾਦੀ ਵਿੱਚ ਹੋਰਨੀਂ ਥਾਈਂ ਲੰਘੀ ਸ਼ਾਮ ਤੋਂ ਵੱਖ ਵੱਖ
ਵਾਰਦਾਤਾ ਵਿਚ ਤਿੰਨ ਹਿਜ਼ਬੁਲ ਮੁਜਾਹਿਦੀਨ ਅਤਿਵਾਦੀਆਂ ਪੀਪਲਜ਼ ਡੈਮੋਕਰੈਟਿਵ ਪਾਰਟੀ ਦੇ ਦੋ ਆਗੂਆਂ
ਸਮੇਤ 15 ਵਿਅਕਤੀ ਮਾਰੇ ਗਏ।</p>

<p>ਕੈਪਟਨ
ਅਮਰਿੰਦਰ ਸਿੰਘ ਨੰੂ ਹਸਪਤਾਲ ਤੋਂ ਛੁੱਟੀ ਅੱਜ</p>

<p>ਨਵੀਨ ਦਿੱਲੀ :
ਪੰਜਾਬ ਦੇ ਮੁੱਖ ਮੰਤਰੀ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਜਿਨ੍ਹਾਂ ਦੀ ਕੱਲ਼੍ਹ ਬਤਰਾ ਹਸਪਤਾਲ ਵਿਚ
ਐਂਜਓਪਲਾਸਟੀ ਕੀਤੀ ਗਈ ਸੀ, ਦੀ ਹਾਲਤ ਸਥਿਰ ਹੈ। ਹਸਪਤਾਲ ਦੇ ਡਿਪਟੀ ਮੈਡੀਕਲ ਡਾਇਰੈਕਟਰ
ਡਾਕਟਰ ਕੇ.ਪੀ.ਦੱਤ ਨੇ ਕਿਹਾ ਕਿ ਮੁੱਖ ਮੰਤਰੀ ਪੂਰੀ ਤਰ੍ਹਾਂ ਤੂਦਰੁਸਤ ਹਨ। ਸਾਬਕਾ ਮੁੱਖ ਮੰਤਰੀ
ਪ੍ਰਕਾਸ਼ ਸਿਮਘ ਬਾਦਲ ਨੇ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਦਾ ਹਾਲ ਪੁੱਛਿਆਂ ਤੇ ਉਨ੍ਹਾਂ ਦੇ ਜਲਦੀ ਸ਼ਿਹਤਯਾਬ
ਹੋਣ ਦੀ ਕਾਮਨਾ ਕੀਤੀ।</p>

<p>ਸੋਧੀਆਂ
ਬਿਜਲੀ ਦਰਾਂ'ਤੇ ਅਮਲ ਆਰਜ਼ੀ
ਤੌਰ'ਤੇ ਰੁਕਿਆ</p>

<p>ਚੰਡੀਗੜ੍ਹ: ਮੁੱਖ
ਮੰਤਰੀ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਦੇ ਬੀਮਾਰ ਹੋਣ ਕਾਰਨ ਸੋਧੀਆਂ ਬਿਜਲੀ ਦਰਾਂ ਵਾਲੇ ਆਦੇਸ਼ ਦੇ ਲਾਗੂ
ਹੋਣ'ਤੇ ਅਰਜ਼ੀ ਤੌਰ'ਤੇ ਅਮਲ ਰੁਕ ਗਿਆ ਹੈ। ਉਧਰ ਪੰਜਾਬ ਰਾਜ ਬਿਜਲੀ
ਬੋਰਡ ਵੀ ਕੁਝ ਸਿਫਾਰਿਸ਼ 'ਤੇ ਮੁੜ ਜਾਇਜ਼ੇ ਲਈ ਰੀਵਿਊ
ਪਟੀਸ਼ਨ ਪਾਉਣ ਦੀ ਤਿਆਰੀ ਵਿਚ ਹੈ। </p>

<p>ਰਵੀ
ਕਾਂਤ ਸ਼ਰਮਾ ਨੰੂ ਭਗੌੜਾ ਕਰਾਰ ਦੇਣ ਸਬੰਧੀ ਅਦਾਲਤੀ ਨੋਟਿਸ ਚਿਪਕਾਏ</p>

<p>ਪੰਚਕੁਲਾ:
ਦਿੱਲੀ ਪੁਲੀਸ ਨੇ ਅੱਜ ਸ਼ਿਵਾਨੀ ਕਤਲ ਕੇਸ 'ਚ ਹਰਿਆਣਾ ਦੇ ਆਈ.ਪੀ.ਐਸ. ਅਧਿਕਾਰੀ ਰਵੀ ਕਾਂਤ ਸ਼ਰਮਾ ਨੰੂ ਰਸਮੀ ਤੌਰ
ਤੇ ਭਗੌੜਾ ਕਰਾਰ ਦੇਣ ਤਹਿਤ ਉਨ੍ਹਾਂ ਦੇ ਨਿਵਾਸ ਤੇ ਸ਼ਹਿਰ'ਚ ਕੁਝ ਹੋਰ ਥਾਵਾਂ'ਤੇ ਇਸ ਸਬੰਧੀ ਨੋਟਿਸ ਚਿਪਕਾ ਦਿੱਤੇ। </p>

<p>ਕਸ਼ਮੀਰ
'ਚ ਕੌਮਾਂਤਰੀ
ਨਿਗਰਾਨਾਂ 'ਤੇ ਪਾਬੰਦੀ ਦੀ
ਕੋਈ ਤੁਕ ਨਹੀਂ : ਗੁਜਰਾਲ</p>

<p>ਚੰਡੀਗੜ੍ਹ :-
ਸਾਬਕਾ ਪ੍ਰਧਾਨ ਮੰਤਰੀ ਇੰਦਰ ਕੁਮਾਰ ਗੁਜਰਾਲ ਨੇ ਜੰਮੂ ਕਸ਼ਮੀਰ ਵਿਧਾਨ ਸਭਾ ਦੀਆਂ ਚੋਣਾਂ ਲਈ
ਕੋਮਾਂਤਰੀ ਨਿਗਰਾਨਾਂ ਨੂੰ ਰਾਜ ਵਿਚ ਬੁਲਾਏ ਜਾਣ ਦੀ ਹਮਾਇਤ ਕਰਦਿਆਂ ਕਿਹਾ ਹੈ ਕਿ ਜਦੋਂ ਭਾਰਤ
ਵਿਚਲੇ ਵੱਖ ਵੱਖ ਦੇਸ਼ਾਂ ਦੇ ਦੂਤਘਰਾਂ ਦੇ ਅਧਿਕਾਰੀ, ਰਾਜਦੂਤ ਅਤੇ ਵਿਦੇਸ਼ੀ ਪੱਤਰਕਾਰ ਸੂਬੇ ਵਿਚ ਆ
ਜਾ ਸਕਦੇ ਹਨ ਤਾਂ ਕੌਮਾਂਤਰੀ ਨਿਗਰਾਨਾਂ ਉਤੇ ਪਾਬੰਦੀ ਲਾਉਣ ਦੀ ਕੋਈ ਤੁੱਕ ਹੀ ਨਹੀਂ ਹੈ।</p>

<p>ਗ੍ਰਿਫਤਾਰੀ
ਦੀ ਅਫਵਾਹ ਨੇ ਅਕਾਲੀ ਲੀਡਰਾਂ ਵਿਚ ਹਲਚਲ ਮਚਾਈ ਰੱਖੀ</p>

<p>ਲੁਧਿਆਣਾ :-
ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ (ਬਾਦਲ) ਵੱਲੋਂ 11 ਸਤੰਬਰ ਨੂੰ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਸਰਕਾਰ ਦੇ ਖਿਲਾਫ
ਦਿੱਤੇ ਜਾਣ ਵਾਲੇ ਧਰਨੇ ਦੇ ਮੱਦੇਨਜ਼ਰ ਜਿਲਾ ਦੇ ਸੀਨੀਅਰ ਪਾਰਟੀ ਨੇਤਾਵਾਂ ਦੀ ਗ੍ਰਿਫਤਾਰੀ ਦੀ ਅਫਵਾਹ
ਦੇ ਕਾਰਣ ਉਨ੍ਹਾਂ ਵਿਚ ਅੱਜ ਦਿਨ ਭਰ ਹਲਚਲ ਮੱਚੀ ਰਹੀ। </p>

<p>ਮਾਲਟਨ
(ਕੈਨੇਡਾ) 'ਚ ਨਗਰ ਕੀਰਤਨ ਹੋਇਆ</p>

<p>ਮਾਲਟਨ(ਕੈਨੇਡਾ)
:-(ਸਾਂਝ ਸਵੇਰਾ) ਧ ੰਨ ਧੰਨ ਸ਼੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਜੀ ਦੇ ਪਹਿਲੇ ਪ੍ਰਕਾਸ਼ ਦੀ ਖੁਸ਼ੀ ਵਿੱਚ
ਕੈਨੇਡਾ ਦੇ ਸੰਘਣੀ ਸਿੱਖ ਵਸੋਂ ਵਾਲੇ ਇਲਾਕੇ ਮਾਲਟਨ ਵਿੱਚ ਟਰਾਂਟੋ ਇਲਾਕੇ ਦਾ ਦੂਸਰਾ ਨਗਰ
ਕੀਰਤਨ ਕੱਲ੍ਹ ਐਤਵਾਰ ਨੰੂ ਬੜੀ ਧੂਮ ਧਾਮ ਨਾਲ ਹੋਇਆ। ਇਸ ਮੌਕੇ ਉਚੇਚੇ ਤੌਰ ਤੇ ਦਮਦਮੀ
ਟਕਸਾਲ ਦੇ ਮੁੱਖੀ ਬਾਬਾ ਠਾਕੁਰ ਸਿੰਘ ਅਤੇ ਬਾਬਾ ਮਾਨ ਸਿੰਘ ਪਿਹੋਵੇ ਵਾਲਿਆਂ ਨੇ ਸ਼ਮੂਲੀਅਤ ਕੀਤੀ।
ਇਸ ਤੋਂ ਇਲਾਵਾ ਸ਼੍ਰੀ ਗੁਰੂ ਗੋਬਿੰਦ ਸਿੰਘ ਜੀ ਮਹਾਰਾਜ਼ ਜੀ ਨਾਲ ਸਬੰਧਤ "ਗੰਗਾ
ਸਾਗਰ" ਦੇ ਵੀ ਸੰਗਤ ਨੇ ਦਰਸ਼ਨ ਕੀਤੇ। </p>

<p> </p>






</body></text></cesDoc>