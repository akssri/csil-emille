<cesDoc id="pun-w-sanjh-news-00-12-01" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-00-12-01.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 00-12-01</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>00-12-01</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p> </p>

<p>ਜਗਦੇਵ ਸਿੰਘ ਤਲਵੰਡੀ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਦੇ ਨਵੇਂ ਪ੍ਰਧਾਨ </p>

<p>*
ਕਿਰਨਜੋਤ ਕੌਰ ਨਵੀਂ ਜਨਰਲ ਸਕੱਤਰ * ਬੀਬੀ ਨੂੰ ਹਟਾਏ ਜਾਣ ਤੇ ਟੌਹੜਾ ਬਾਗੋ-ਬਾਗ
</p>

<p>ਅੰਮ੍ਰਿਤਸਰ,
30 ਨਵੰਬਰ - ਕਿਸੇ ਸਮੇਂ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਦੇ ਕੱਟੜ ਦੁਸ਼ਮਣ ਰਹੇ ਸੀਨੀਅਰ ਅਕਾਲੀ ਬਜ਼ੁਰਗ ਨੇਤਾ
ਜਥੇਦਾਰ ਜਗਦੇਵ ਸਿੰਘ ਤਲਵੰਡੀ ਨੂ ਅੱਜ ਇਥੇ ਸਿੱਖਾਂ ਦੀ ਮਿੰਨੀ ਪਾਰਲੀਮੈਂਟ ਵਜੋਂ ਜਾਣੀ ਜਾਂਦੀ
ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦਾ ਸਰਬਸੰਮਤੀ ਨਾਲ ਪ੍ਰਧਾਨ ਚੁਣ ਲਿਆ ਗਿਆ । ਜਥੇਦਾਰ
ਤਲਵੰਡੀ ਸਿੱਖ ਗੁਰਦੁਆਰਾ ਐਕਟ 1925 ਲਾਗੂ ਹੋਣ ਤੋਂ ਬਾਅਦ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਦੇ 31ਵੇਂ ਪ੍ਰਧਾਨ
ਹੋਣਗੇ । </p>

<p>          iejl`s ny srbsMmqI n`l hI SRI blbIr
isMG pMnUM, SRI kyvl isMG jgr`auN Eqy bIbI ikrnjoq kOr nUM kRmv`r sInIEr mIq
pRD`n, jUnIEr mIq pRD`n Eqy jnrl sk@qr cux ilE` | srbsMmqI n`l cuxI geI
k`rjk`rnI ivc SRI idlb`g isMG, SRI iSMg`r` isMG, SRI sUb` isMG, SRI hridE`l
isMG, SRI rGbIr isMG, EYfvokyyt mihMdr isMG rom`x`, SRI joigMdr isMG pMjrQ, SRI
jMg bh`dr isMG r`ey qoN iel`v` srb ihMd SRomxI Ek`lI dl dy ANk`r isMG SrIPpur`,
SRI hrbMs isMG mMJpur Eqy Ek`lI dl (EMimRqsr) n`l sMbMiDq SRI bldyv isMG sihjV`
S`ml hn | </p>

<p>          cox iejl`s dupihry iek vjy drb`r s`ihb
smUh ivc siQq qyj` isMG smuMdrI h`l ivc SurU hoieE` | SRomxI kmytI dy sk@qr
f`:gurbcn isMG bcn ny cox pRikirE` SurU kIqI | pRD`n bIbI jgIr kOr ny ipCly
smyN ivc mYNbr~ vloN id@qy gey sihwog leI aunH~ d` DMnv`d kIq` Eqy mYNbr~ nUM
EpIl kIqI ik auh mh`n D`rimk sMsQ` SRomxI kmytI d` siqk`r k`iem r@KidE~ pUrn
sihwog dyx Eqy EijhI koeI g@l n` krn ijs n`l sMsQ` dy siqk`r nUM Tys pu@jdI
hovy | </p>

<p>          ies mgroN SRomxI kmytI dy sk@qr jnrl
Eqy SRomxI kmytI dy n`mzd mYNbr SRI ikrp`l isMG bfUMgr ny Ek`lI dl dy pRD`n
pRk`S isMG b`dl vloN pRD`n Eqy hor Ehudyd`r~ dy Byjy n`v~ d` ilP`P` KoilHE` Eqy
ikh` ik SRomxI kmytI mYNbr~ vloN SRI b`dl nUM id@qy EiDk`r~ dy m@dynzr jQyd`r
jgdyv isMG qlvMfI SRomxI kmytI dy Egly pRD`n hoxgy | aunH~ b`kI n~ vI bolxy
c`hy pr jQyd`r gurcrn isMG tOhV` ny aunH~ nUM tok id@q` Eqy ies g@l qy zor
id@q` ik iek@ly-iek@ly n~ dI pRv`ngI leI j`vy | </p>

<p>          SRI ikrp`l isMG bfUMgr ny jQyd`r
qlvMfI d` n~ pRD`n dy Ehudy leI pyS kIq`, ijs dI q`eId SRI ErivMdrp`l isMG
p@Koky Eqy jgjIq isMG ny kIqI | iksy hor d` n~ pRD`n dy Ehudy leI pyS n` hox qy
bIbI jgIr kOr ny SRI qlvMfI dy pRD`n cuxy j`x d` EYl`n kr id@q` | </p>

<p>          sInIEr mIq pRD`n dy Ehudy leI SRI
blbIr isMG pMnUM, jUnIEr mIq pRD`n leI SRI kyvl isMG Eqy jnrl sk@qr leI bIbI
ikrnjoq kOr dy n~ v@K-v@K mYNbr~ vjoN pyS kIqy gey, ijnH~ d` koeI ivroD n`
hoieE` | </p>

<p>          vrnxwog hY ik jnrl sk@qr SRI hrdlbIr
isMG S`h nUM ies v`r k`rjk`rnI ivc koeI vI Ehud` nhIN id@q` igE` jdoN ik auh
ies v`r pRD`n bxn dI E`s l`eI bYTy sn | srvSRI pMnUM, kyvl isMG, sUb` isMG,
joigMdr isMG pMjrQ, jMg bh`dr isMG r`ey, hrbMs isMG mMJpur nUM hI dub`r`
k`rjk`rnI ivc ilE` igE` hY | </p>

<p>          idlcsp g@l ieh rhI ik jdoN SRI b`dl
vloN Byjy gey k`rjk`rnI dy n~ SRI bfUMgr ny bolxy c`hy q~ jQyd`r tOhV` ny KVHy
ho ky aunH~ nUM tokidE~ E`iKE` ik jy k`rjk`rnI ivc aunH~ dy mYNbr vI ley gey hn
q~ aunH~ vloN pRv`ngI hY, nhIN q~ mnzUr nhIN | jQyd`r qlvMfI ny qurMq ikh` ik
E`pxy mYNbr~ dy n~ idA | aunH~ vloN id@qy n`v~ nUM vI k`rjk`rnI ivc S`ml kr
ilE` igE` | </p>

<p>          ieh g@l vI vrnxwog hY ik E@j svyry
B`eI gurd`s h`l ivc jQydr tOhV` ny p`rtI n`l sbMDq mYNbr~ dI mIitMg ivc ieh
PYsl` ilE` sI ik jy bIbI jgIr kOr nUM hI muV pRD`n EYl`inE` igE` q~ p`rtI vloN
aumIdv`r KVH` kIq` j`vyg` | aunH~ nUM C@f ky hor iksy vI aumIdv`r d` ivroD nhIN
kIq` j`vyg` | </p>

<p>          cox
pRikirE` mukMml hox mgroN jQyd`r tOhV` ny jQyd`r qlvMfI nUM pRD`n bxn qy vD`eI
id@qI | aunH~ rq` v@Kry EMd`z ivc ikh`, ipCly pRD`n ny mYnUM n` q~ sihwog hI
id@q` qy n` hI siqk`r | E@gy vI pq` nhIN imlyg` j~ nhIN | pq` nhIN qlvMfI s`ihb
ikvyN d` vqIr` r@Kxgy | SRI qlvMfI ny ivcoN tokidE~ ikh`, bhuq vDIE` rhyg` |
jQyd`r tOhV` ny ikh` ik SRomxI kmytI kol bhuq syv`d`r hn qy t`sk Pors vI hY |
ies leI kMplYks ivc puils l`aux dI loV nhIN sI | aunH~ zor id@q` ik kMplYks nUM
puils qoN mukq hI r@iKE` j`vy | jQyd`r tOhV` ny bIbI jgIr kOr nUM ht`ry j`x qy
KuSI d` pRgt`v` kIq` | </p>

<p>ਸੁਪਰੀਮ
ਕੋਰਟ ਵਲੋਂ ਬਾਦਲ ਸਰਕਾਰ ਦੀ ਅਪੀਲ ਰੱਦ </p>

<p>ਨਵੀਂ
ਦਿੱਲੀ, 30 ਨਵੰਬਰ - ਸੁਪਰੀਮ ਕੋਰਟ ਨੇ ਪੰਜਾਬ ਦੇ ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਅਤੇ ਰਾਜ
ਸਰਕਾਰ ਵਲੋਂ ਦਾਇਰ ਅਪੀਲਾਂ ਨੂੰ ਰੱਦ ਕਰਦਿਆਂ ਪੰਜਾਬ ਤੇ ਹਰਿਆਣਾ ਹਾਈ ਕੋਰਟ ਦੇ ਉਸ ਫੈਸਲੇ
ਨੂੰ ਬਰਕਰਾਰ ਰੱਖਿਆ ਹੈ ਜਿਸ ਵਿਚ ਪੰਜਾਬ ਦੇ ਸਾਬਕਾ ਮੁੱਖ ਸਕੱਤਰ ਵੀ.ਕੇ.ਖੰਨਾ ਵਲੋਂ ਦੋ
ਸੀਨੀਅਰ ਆਈ.ਏ.ਐਸ. ਅਫਸਰਾਂ ਖਿਲਾਫ ਸੀ.ਬੀ.ਆਈ. ਜਾਂਚ ਸ਼ੁਰੂ ਕਰਨ ਨੂੰ ਜਾਇਜ਼ ਕਰਾਰ ਦਿੱਤਾ
ਗਿਆ ਸੀ । ਜਾਂਚ ਦੇ ਆਦੇਸ਼ ਅਫਸਰਾਂ ਦੀ ਆਮਦਨ ਦੇ ਵਸੀਲਿਆਂ ਨਾਲੋਂ ਵਧੇਰੇ ਜਾਇਦਾਦ ਬਣਾਉਣ ਬਾਰੇ
ਕੀਤੀ ਜਾਣੀ ਸੀ । </p>

<p>          jsits EYs.jy.r`A Eqy jsits
wU.sI.bYnrjI qy E`D`rq bYNc ny ikh` hY ik kys ivc s`hmxy E`ey q@Q~ muq`bk h`eI
kort vloN sux`ieE` PYsl` shI hY | EpIl krq`v~ ny ijhVIE~ dlIl~ pyS kIqIE~ hn,
auh SRI KMn` vloN ley PYsly dy pRsMg ivc vznd`r nhIN | vrnxwog hY ik h`eI kort
ny sUb` srk`r vloN SRI KMn` ivru@D kIqI j` rhI EnuS`snI k`rv`eI vI r@d kr id@qI
sI | ies qoN pihl~ 7 PrvrI, 1997 nUM qqk`lI mu@K mMqrI rijMdr kOr B@Tl dy ilKqI
hukm~ auqy qqk`lI mu@K sk@qr SRI KMn` ny E`eI.ey.EYs. EPsr ibkrmmIq isMG ivru@D
vsIilE~ n`loN vDyry j`ied`d iek@TI krn Eqy ieMdrjIq isMG ibMdr` vloN pMj`b
ikRkt EYsosIeySn, ijs dy auh pRD`n sn, nUM 20 eykV zmIn dyx sbMDI j~c-pVq`l
sI.bI.E`eI. qoN krv`aux dy hukm j`rI kIqy sn |
</p>

<p>ਹਰਪ੍ਰੀਤ
ਕੌਰ ਕੇਸ ਵਿਚ ਡਾਕਟਰ ਦੀ ਜ਼ਮਾਨਤ ਬਾਰੇ ਫੈਸਲਾ ਅੱਜ
</p>

<p>          cMfIgVH, 30 nvMbr - SRomxI gurduE`r`
pRbMDk kmytI dI s`bk` pRD`n bIbI jgIr kOr dI DI hrpRIq kOr dy kql dI s`ijS dy
mulzm f`:blivMdr isMG dI EMiqRm zm`nq d` PYsl` pihlI dsMbr qk mulqvI kr id@q`
igE` | EMiqRm zm`nq leI mulzm vloN id@qI ErzI qy suxv`eI E@j izlH` qy sYSn j@j
SRI EYc.EYs.B@l` dI Ed`lq ivc hoeI | </p>

<p>          bIqy k@lH Ed`lq nUM EMiqRm zm`nq leI
id@qI ErzI ivc ptISnr ny ikh` sI ik aus d` kys n`l koeI sbMD nhIN | n`ly puils
kol drj EYP.E`eI.E`r. j~ kmljIq isMG vloN pMj`b qy hirE`x` h`eI kort ivc d`ier
kIqy kys ivc aus d` n~ S`ml nhIN | kmljIq isMG E`pxy E`p nUM hrpRIq kOr d` pqI
d@s irh` hY | </p>

<p>          E@j hoeI suxv`eI dOr`n sI.bI.E`eI. dy
vkIl ny Ed`lq nUM d@isE` ik kys dI pu@Cig@C leI hux mulzm aunH~ nUM sihwog dy
irh` hY | ies qoN pihl~ sI.bI.E`eI. mulzm qoN pu@Cig@C leI auhdI igRPq`rI qy
zor dy rhI sI | </p>

<p>          izkrwog hY ik f`:blivMdr isMG dI
EMiqRm zm`nq dI ErzI Ed`lq pihl~ iek v`r r@d kr cu@kI hY | f`:blivMdr isMG
ijhV` ik izlH` jlMDr dy ipMf p@qV kl~ ivc bqOr f`ktr q`ien`q hY, auqy hrpRIq
kOr d` grBp`q kr`aux ivc SmUlIEq d` doS hY |   </p>

<p>ਕਨਵੈਨਸ਼ਨ
ਤੀਜੇ ਬਦਲ ਲਈ ਰਾਹ ਪੱਧਰਾ ਕਰੇਗੀ - ਚੰਦੂਮਾਜਰਾ </p>

<p>          cMfIgVH, 30 nvMbr - srb ihMd SRomxI
Ek`lI dl dy jnrl sk@qr pRo:pRym isMG cMdUm`jr` ny ikh` hY ik 2 dsMbr nUM
cMfIgVH ivc ho rhI knvYnSn qIjy bdl leI r`h p@Dr` krygI | aus idn srk`r dIE~
nIqIE~ lok cyqn` muihMm SurU kIqI j`vygI | </p>

<p>          E@j ieQy pRYs kl@b ivc pRYs k`nPrMs
nUM sMboDn kridE~ pRo:cMdUm`jr` ny E`iKE` ik ies vyly cox~ iek mj`k bx geIE~
hn, ikauN jo h`km iDr pYsy dI KulH ky vrqoN krdI hY Eqy srk`rI mSInrI d`
durupwog huMd` hY | ies vyly r`jnIqI lok syv` dI Q~ pyt syv` bx geI hY | pMj`b
dIE~ v@fIE~ p`rtIE~ B`jp`, Ek`lI dl (b`dl) Eqy k~grs vloN lok~ nUM dovyN h@QI
lu@itE` j` irh` hY | lok nvyN r`h dI ql`S ivc hn | ies k`nPrMs ivc horn~ qoN
iel`v` SRI r`m jyTml`nI, SRI subr`mnIEm sv`mI, sI.bI.E`eI. dy s`bk` f`ierYktr
SRI joigMdr isMG Eqy keI hor kOmI nyq` pu@j rhy hn | ieh knvYnSn qIjI iDr
aus`rn leI plytP`rm d` kMm krygI | </p>

<p>          knvYnSn
ivc s~Jy morcy dy E`gUE~ qoN iel`v` lok Bl`eI, K@bIE~ p`rtIE~ Eqy hor hm`ieqI
p`rtIE~ dy nyq` vI E` rhy hn |
</p>

<p> </p>






</body></text></cesDoc>