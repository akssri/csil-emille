<cesDoc id="pun-w-sanjh-news-00-09-12" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-00-09-12.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 00-09-12</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>00-09-12</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>





 
  
  <p>ਫੂਨ 905-789-7787, ਫੈਕਸ: 905-789-7717 ਜਾਂ ਈ-ਮੇਲ ਕਰੋ  </p>
  
  
  <p>ਸਤੰਬਰ
  12, 2000</p>
  
 


<p>ਪੁਲਿਸ ਹਿਰਾਸਤ
ਵਿਚ ਸਾਬਕਾ ਸਰਪੰਚ ਦੀ ਹੋਈ ਮੌਤ ਦੀ ਅਦਾਲਤੀ ਜਾਂਚ ਕਰਵਾਉਣ ਦੇ ਹੁਕਮ </p>

<p>ਹੁਸ਼ਿਆਰਪੁਰ, 11 ਸਤੰਬਰ - ਪੁਲਿਸ
ਦੁਆਰਾ ਗ੍ਰਿਫਤਾਰ ਕੀਤੇ ਥਾਣਾ ਗੜ੍ਹਸ਼ੰਕਰ ਦੇ ਪਿੰਡ ਗੋਗੋ ਦੇ ਸਾਬਕਾ ਸਰਪੰਚ ਤੇ ਡੋਡਿਆਂ ਦੇ ਨਾਮੀ
ਸਮੱਗਲਰ ਚੂਹੜ ਸਿੰਘ ਜਿਸ ਦੀ ਬੀਤੇ ਦਿਨ ਸਿਵਲ ਹਸਪਤਾਲ ਵਿਖੇ ਮੌਤ ਹੋ ਗਈ ਸੀ, ਦੇ ਘਰਦਿਆਂ ਵਲੋਂ
ਜਿਥੇ ਪੁਲਿਸ ਤਸ਼ੱਦਦ ਦੇ ਦੋਸ਼ ਲਗਾਏ ਜਾ ਰਹੇ ਹਨ, ਉਥੇ ਪੁਲਿਸ ਵਲੋਂ ਉਸ ਦੀ ਮੌਤ ਪੁਰਾਨੀ
ਬਿਮਾਰੀ ਕਾਰਨ ਹੋਈ ਦੱਸੀ ਜਾਂਦੀ ਹੈ । </p>

<p>ਪੋਸਟ ਮਾਰਟਮ ਦੀ ਰਿਪੋਰਟ ਅਨੁਸਾਰ ਚੂਹੜ
ਸਿੰਘ ਦੇ ਸ਼ਰੀਰ ਤੇ ਕਿਸੇ ਕਿਸਮ ਦੀ ਸੱਟ ਦਾ ਨਿਸ਼ਾਨ ਨਹੀਂ ਸੀ ਪਰ ਉਸ ਦੇ ਸੱਜੇ ਫੇਫੜੇ ਵਿਚ ਪੀਕ
ਪਈ ਹੋਈ ਸੀ । ਉਸ ਦਾ ਮਿਹਦਾ ਤੇ ਦਿਲ ਵੀ ਵਧਿਆ ਹੋਇਆ ਸੀ । ਡਾਕਟਰਾਂ ਦੀ ਰਾਇ ਮੁਤਾਬਕ
ਉਸ ਦੀ ਮੌਤ ਇਨ੍ਹਾਂ ਵਿਚੋਂ ਕਿਸੇ ਇਕ ਕਾਰਨ ਕਰਕੇ ਹੋ ਸਕਦੀ ਹੈ । ਇਸ ਤੋਂ ਇਲਾਵਾ ਚੂਹੜ ਸਿੰਘ
ਨੇ ਖੁਦ ਡਾਕਟਰਾਂ ਨੂੰ ਦੱਸਿਆ ਕਿ ਉਸ ਨੂੰ ਪਿਛਲੇ ਇਕ ਹਫਤੇ ਤੋਂ ਉਲਟੀਆਂ ਨਾਲ ਖੂਨ ਆ ਰਿਹਾ
ਸੀ । ਚੂਹੜ ਸਿੰਘ ਦਾ ਪੋਸਟਮਾਰਟਮ ਚਾਰ ਡਾਕਟਰਾਂ ਦੇ ਇਕ ਬੋਰਡ ਵਲੋਂ ਇਨਕੁਆਇਰੀ ਅਫਸਰ ਦੀ
ਹਾਜ਼ਰੀ ਵਿਚ ਵੀਡਿਓ ਕੈਮਰੇ ਦੇ ਸਾਹਮਣੇ ਕਰਵਾਇਆ ਗਿਆ । ਬੇਸ਼ੱਕ ਪੋਸਟਮਾਰਟਮ ਦੀ ਰਿਪੋਰਟ ਨਾਲ
ਪੁਲਿਸ ਮੱਤ ਨੂੰ ਬਲ ਮਿਲਿਆ ਹੈ ਪਰ ਮ੍ਰਿਤਕ ਦੇ ਪਰਿਵਾਰ ਦੀ ਤਸੱਲੀ ਨਹੀਂ ਹੋਈ ਹੈ । ਪਰਿਵਾਰ
ਵਲੋਂ ਕੇਸ ਦੀ ਜਾਂਚ ਹਾਈ ਕੋਰਟ ਦੇ ਇਕ ਜੱਜ ਵਲੋਂ ਕਰਵਾਉਣ ਦੀ ਮੰਗ ਕੀਤੀ ਗਈ ਹੈ । ਚੂਹੜ
ਸਿੰਘ ਨੂੰ ਇਕ ਔਰਤ ਮਹਿੰਦਰ ਕੌਰ ਸਮੇਤ ਮਾਹਿਲਪੁਰ ਪੁਲਿਸ ਨੇ 6 ਸਤੰਬਰ ਨੂੰ ਗ੍ਰਿਫਤਾਰ ਕਰਕੇ
ਉਨ੍ਹਾਂ ਕੋਲੋਂ 48 ਬੋਰੀਆਂ ਡੋਡੇ ਤੇ ਚੂਰਾ-ਪੋਸਤ ਬਰਾਮਦ ਕੀਤੀਆਂ ਸਨ । ਇਸੇ ਦਿਨ ਇਨ੍ਹਾਂ ਨੂੰ
ਇਲਾਕਾ ਮੈਜਿਸਟਰੇਟ ਦੀ ਅਦਾਲਤ ਵਿਚ ਪੇਸ਼ ਕਰਕੇ ਪੁਲਿਸ ਰਿਮਾਂਡ ਲੈ ਲਿਆ ਗਿਆ ਸੀ । ਪੁਲਿਸ
ਅਨੁਸਾਰ ਉਸ ਸਮੇਂ ਚੂਹੜ ਸਿੰਘ ਦੇ ਵਕੀਲ ਜਾਂ ਕਿਸੇ ਰਿਸ਼ਤੇਦਾਰ ਵਲੋਂ ਨਾ ਤਾਂ ਸ਼ਰੀਰਕ ਤਕਲੀਫ ਬਾਰੇ
ਕੁਝ ਕਿਹਾ ਗਿਆ ਤੇ ਨਾ ਹੀ ਪੁਲਿਸ ਤਸ਼ੱਦਦ ਦੀ ਸ਼ਿਕਾਇਤ ਕੀਤੀ ਗਈ । 9 ਸਤੰਬਰ ਨੂੰ ਇਸ ਨੂੰ
ਅਦਾਲਤ ਵਿਚ ਪੇਸ਼ ਕਰਨਾ ਸੀ ਜਿਸ ਕਾਰਨ ਚੂਹੜ ਸਿੰਘ ਨੂੰ ਹੁਸ਼ਿਆਰਪੁਰ ਲਿਜਾਇਆ ਗਿਆ । </p>

<p>ਇਸੇ ਦੌਰਾਨ ਜ਼ਿਲ੍ਹਾ ਡਿਪਟੀ ਕਮਿਸ਼ਨਰ ਸ:
ਇਕਬਾਲ ਸਿੰਘ ਸਿੱਧੂ ਨੇ ਚੂਹੜ ਸਿੰਘ ਦੀ ਪੁਲਿਸ ਹਿਰਾਸਤ ਵਿਚ ਹੋਈ ਮੌਤ ਦੀ ਅਦਾਲਤੀ ਜਾਂਚ
ਕਰਵਾਉਣ ਦੇ ਹੁਕਮ ਜਾਰੀ ਕੀਤੇ ਹਨ । ਇਸ ਮਾਮਲੇ ਦੀ ਜਾਂਚ ਐਸ.ਡੀ.ਐਮ. ਕਰਨਗੇ । </p>

<p>ਫੌਜੀ ਸ਼ਹੀਦਾਂ ਦੇ
ਮਾਪੇ ਸਰਕਾਰ ਤੇ ਫੌਜ ਨਾਲ ਖਫ਼ਾ </p>

<p>ਜਲੰਧਰ, 11 ਸਤੰਬਰ - ਅਪਰੇਸ਼ਨ ਵਿਜੈ,
ਰਕਸ਼ਕ ਅਤੇ ਠੀਨੇ ਵਿਚ ਸ਼ਹਾਦਤ ਪਾ ਗਏ ਫੌਜੀ ਜਵਾਨਾਂ ਦੇ ਮਾਪੇ ਪ੍ਰਭਾਵਿਤ ਪਰਿਵਾਰਾਂ ਨੂੰ ਮਦਦ ਦੇਣ
ਦੀ ਗਲਤ ਨੀਤੀ ਕਾਰਨ ਸਰਕਾਰ ਅਤੇ ਫੌਜ ਨਾਲ ਸਖਤ ਨਾਰਾਜ਼ ਹਨ । ਮਾਪਿਆਂ ਨੇ ਹੁਕਮਰਾਨ ਭਾਰਤੀ ਜਨਤਾ
ਪਾਰਟੀ ਉਪਰ ਕਾਰਗਿਲ ਜੰਗ ਨੂੰ ਲੋਕ ਸਭਾ ਦੀ ਪਿਛਲੀ ਚੋਣ ਵਿਚ ਆਪਣੀ ਜਿੱਤ ਲਈ ਵਰਤਣ ਦਾ ਗੰਭੀਰ
ਦੋਸ਼ ਵੀ ਲਾਇਆ ਹੈ । </p>

<p>ਸ਼ਹੀਦ ਫੌਜੀ ਪਰਿਵਾਰਾਂ ਦੇ ਇਨ੍ਹਾਂ ਮੈਂਬਰਾਂ
ਨੇ ਅੱਜ ਇਥੇ ਬੁਲਾਈ ਇਕ ਪ੍ਰੈਸ ਕਾਨਫਰੰਸ ਵਿਚ ਦੋਸ਼ ਲਾਇਆ ਕਿ ਪ੍ਰਭਾਵਿਤ ਪਰਿਵਾਰਾਂ ਨੂੰ ਮਦਦ
ਦੇਣ ਬਾਰੇ ਫੌਜ ਦੀ ਤਾਜਾ ਨੀਤੀ ਕਾਰਨ ਸ਼ਹੀਦ ਫੌਜੀਆਂ ਦੇ ਪਰਿਵਾਰ ਟੁੱਟ ਗਏ ਹਨ । ਫੌਜ ਸ਼ਹੀਦਾਂ
ਦੀਆਂ ਵਿਧਵਾਵਾਂ ਨੂੰ 25 ਤੋਂ ਲੈ ਕੇ 30 ਲੱਖ ਰੁਪਏ ਨਕਦ ਸਹਾਇਤਾ ਅਤੇ ਹੋਰ ਸਹੂਲਤਾਂ ਦੇ ਰਹੀ ਹੈ
ਜਦੋਂ ਕਿ ਇਨ੍ਹਾਂ ਸ਼ਹੀਦਾਂ ਦੇ ਮਾਪਿਆਂ ਨੂੰ ਇਸ ਰਕਮ ਵਿਚ ਕੋਈ ਹਿੱਸਾ ਨਹੀਂ ਦਿੱਤਾ ਜਾ ਰਿਹਾ ।
ਸ਼ਹੀਦ ਫੌਜੀਆਂ ਦੇ ਮਾਪੇ ਇਸ ਰਕਮ ਵਿਚੋਂ ਇਕ ਤਿਹਾਈ ਹਿੱਸੇ ਦੀ ਮੰਗ ਕਰ ਰਹੇ ਹਨ । </p>

<p>ਪ੍ਰੈਸ ਕਾਨਫਰੰਸ ਵਿਚ ਸ਼ਾਮਲ ਕੀਰਤੀ ਚੱਕਰ
ਵਿਜੇਤਾ ਸ਼ਹੀਦ ਮੇਜਰ ਰਮਨ ਦਾਦਾ ਦੇ ਪਿਤਾ ਸਾਬਕਾ ਮੇਜਰ ਰਵੀ ਦਾਦਾ ਨੇ ਕਿਹਾ ਕਿ ਮਦਦ ਦੀ ਠੀਕ ਵੰਡ
ਬਾਰੇ ਉਨ੍ਹਾਂ ਨੇ ਇਹ ਮਾਮਲਾ 6 ਫਰਵਰੀ ਨੂੰ ਜਲੰਧਰ ਫੇਰੀ ਸਮੇਂ ਪ੍ਰਧਾਨ ਮੰਤਰੀ ਸ਼੍ਰੀ ਅਟਲ ਬਿਹਾਰੀ
ਵਾਜਪਾਈ ਨਾਲ ਵੀ ਉਠਾਇਆ ਸੀ । ਪ੍ਰੰਤੂ ਹਾਲੇ ਤਕ ਇਸ ਮਾਮਲੇ ਵਿਚ ਕੁਝ ਨਹੀਂ ਹੋਇਆ । ਮੇਜਰ
ਰਵੀ ਦਾਦਾ ਮੁਤਾਬਕ ਫੌਜ ਦੀ ਨਵੀਂ ਮਦਦ ਨੀਤੀ ਕਾਰਨ ਸ਼ਹੀਦਾਂ ਦੇ ਪਰਿਵਾਰਾਂ ਅੰਦਰ ਬਖੇੜੇ ਖੜੇ ਹੋ
ਗਏ ਹਨ ਅਤੇ ਸ਼ਹੀਦਾਂ ਦੀਆਂ ਵਿਧਵਾਵਾਂ ਇਹ ਮਦਦ ਲੈ ਕੇ ਆਪਣੇ ਮਾਪਿਆਂ ਕੋਲ ਜਾ ਬੈਠੀਆਂ ਹਨ ਜਾਂ
ਫਿਰ ਉਨ੍ਹਾਂ ਨੇ ਫੌਜ ਵਲੋਂ ਦਿੱਤੀ ਖੁੱਲ੍ਹ ਕਾਰਨ ਮੁੜ ਵਿਆਹ ਕਰਾ ਕੇ ਘਰ ਵਸਾ ਲਏ ਹਨ । ਆਪਣੀ
ਪੀੜਾ ਸਾਂਝੀ ਕਰਦਿਆਂ ਸ਼੍ਰੀ ਦਾਦਾ ਨੇ ਕਿਹਾ ਕਿ ਬਜ਼ੁਰਗ ਮਾਪੇ ਆਪਣੇ ਆਪ ਨੂੰ ਬੇਸਹਾਰਾ ਪਾ ਰਹੇ ਹਨ
। </p>

<p>ਸ਼ਹੀਦ ਮੇਜਰ ਕਮਲ ਗੁਲਜਾਰ ਸਿੰਘ ਦੇ ਪਿਤਾ
ਸਾਬਕਾ ਮੇਜਰ ਕਰਤਾਰ ਸਿੰਘ ਦਾ ਕਹਿਣਾ ਹੈ ਕਿ ਸਰਕਾਰ ਨੇ ਉਨ੍ਹਾਂ ਤੋਂ ਪੈਟਰੋਲ ਪੰਪ ਅਤੇ ਗੈਸ
ਏਜੰਸੀਆਂ ਅਲਾਟ ਕਰਨ ਲਈ ਅਰਜੀਆਂ ਲਈਆਂ ਸਨ ਪ੍ਰੰਤੂ ਹੁਣ ਉਨ੍ਹਾਂ ਨੂੰ ਕਿਹਾ ਜਾ ਰਿਹਾ ਹੈ ਕਿ
ਇਨ੍ਹਾਂ ਦੀ ਅਲਾਟਮੈਂਟ ਲਈ ਸ਼ਹੀਦ ਦੀ ਵਿਧਵਾ ਤੋਂ ਸ਼ਹਿਮਤੀ ਲਿਖਾ ਦੇ ਦੇਵੋ । </p>

<p>ਪ੍ਰੈਸ ਕਾਨਫਰੰਸ ਵਿਚ ਹਾਜ਼ਰ ਸ਼ਹੀਦ ਕੈਪਟਨ
ਮਨਦੀਪ ਸਿੰਘ ਦੇ ਪਿਤਾ ਕਮਲਜੀਤ ਸਿੰਘ ਨੇ ਦੱਸਿਆ ਕਿ ਭਾਰਤੀ ਫੌਜ ਦੇ ਮੁਖੀ ਜਨਰਲ ਵੀ.ਪੀ.ਮਲਿਕ
ਨੇ ਜੁਲਾਈ ਵਿਚ ਜਲੰਧਰ ਫੇਰੀ ਸਮੇਂ ਉਨ੍ਹਾਂ ਨੂੰ ਭਰੋਸਾ ਦਿਵਾਇਆ ਸੀ ਕਿ ਸ਼ਹੀਦਾਂ ਦੇ ਮਾਪਿਆਂ
ਲਈ ਫੌਜ ਇਕ ਮਦਦ ਪੈਕੇਜ ਦਾ ਐਲਾਨ ਕਰੇਗੀ । ਪ੍ਰੰਤੂ ਹਾਲੇ ਤਕ ਕੁਝ ਨਹੀਂ ਹੋਇਆ । </p>

<p>ਸ਼ਹੀਦਾਂ ਦੇ ਮਾਪਿਆਂ ਨੇ ਫੌਜ ਦੇ
ਲ਼ੈਫਟੀ.ਜਨਰਲ ਐਸ.ਐਸ.ਗਰੇਵਾਲ ਦੇ ਉਸ ਬਿਆਨ ਦਾ ਵੀ ਬੁਰਾ ਮਨਾਇਆ ਜਿਸ ਵਿਚ ਉਸ ਨੇ ਸ਼ਹੀਦਾਂ
ਦੇ ਮਾਪਿਆਂ ਦੀ ਨਿੰਦਾ ਕਰਦਿਆਂ ਫੌਜੀ ਮਦਦ ਨਿਯਮਾਂ ਵਿਚ ਸੋਧ ਕਰਨ ਦੀ ਮੰਗ ਰੱਦ ਕਰ ਦਿੱਤੀ ਹੈ ।
ਸ਼ਹੀਦ ਪਰਿਵਾਰਾਂ ਨੇ ਫੌਜੀ ਜਨਰਲ ਨੂੰ ਆਪਣਾ ਬਿਆਨ ਵਾਪਸ ਲੈਣ ਲਈ ਕਿਹਾ ਹੈ । </p>

<p>ਢੀਂਡਸਾ ਤੇ
ਬਰਨਾਲਾ ਧੜਿਆਂ ਦੀ ਖਿੱਚੋਤਾਣ ਸਿਖਰਾਂ ਤੇ </p>

<p>ਸੁਨਾਮ, 11 ਸਤੰਬਰ - ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ
(ਬਾਦਲ) ਦੇ ਸਾਬਕਾ ਮੁੱਖ ਮੰਤਰੀ ਸੁਰਜੀਤ ਸਿੰਘ ਬਰਨਾਲਾ ਤੇ ਕੇਂਦਰੀ ਕੈਬਿਨਟ ਮੰਤਰੀ ਸ਼੍ਰੀ ਸੁਖਦੇਵ
ਸਿੰਘ ਢੀਂਡਸਾ ਦੇ ਧੜਿਆਂ ਦੀ ਖਿੱਚੋਤਾਣ ਅਜੇ ਵੀ ਸਿਖਰਾਂ ਤੇ ਹੈ । ਸੁਨਾਮ ਦੀ ਉਪ ਚੋਣ ਉਤੇ ਇਸ
ਧੜੇਬੰਦੀ ਦਾ ਅਸਰ ਪੈ ਸਕਦਾ ਹੈ । ਅਕਾਲੀ ਦਲ (ਬਾਦਲ) ਦੇ ਉਮੀਦਵਾਰ ਪਰਮਿੰਦਰ ਸਿੰਘ ਢੀਂਡਸਾ ਦੇ
ਪ੍ਰਚਾਰ ਲਈ ਅਜੇ ਤਕ ਬਰਨਾਲਾ ਧੜੇ ਦਾ ਕੋਈ ਆਗੂ ਨਹੀਂ ਆਇਆ । ਇਥੇ ਪਰਮਿੰਦਰ ਢੀਂਡਸਾ ਦੇ
ਨਾਮਜ਼ਦਗੀ ਪੱਤਰ ਦਾਖਲ ਕਰਨ ਵੇਲੇ ਵੀ ਕੋਈ ਆਗੂ ਜਾਂ ਸਰਗਰਮ ਵਰਕਰ ਸ਼ਾਮਲ ਨਹੀਂ ਹੋਇਆ । ਮੁੱਖ
ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਵਲੋਂ ਪੰਜਾਬ ਦੇ ਅਹੁਦੇਦਾਰਾਂ ਦੀ ਬੁਲਾਈ ਮੀਟਿੰਗ ਵਿਚ ਨਾ ਹੀ ਸੁਰਜੀਤ
ਸਿੰਘ ਬਰਨਾਲਾ ਸ਼ਾਮਿਲ ਹੋਏ ਤੇ ਨਾ ਹੀ ਜਨਰਲ ਸਕੱਤਰ ਬਲਦੇਵ ਸਿੰਘ ਮਾਨ ਆਏ । ਇਸ ਤੋਂ ਪਹਿਲਾਂ
ਜਦੋਂ ਪੱਤਰਕਾਰਾਂ ਨੂੰ ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਨੂੰ ਪੁੱਛਿਆ ਗਿਆ ਤਾਂ ਉਨ੍ਹਾਂ ਸਪਸ਼ਟ
ਕਿਹਾ ਕਿ ਸ਼੍ਰੀ ਬਰਨਾਲਾ ਚੋਣ ਮੁਹਿੰਮ ਵਿਚ ਜਰੂਰ ਸ਼ਾਮਲ ਹੋਣਗੇ । </p>

<p>ਬਰਨਾਲਾ ਧੜੇ ਪੱਖੀ ਯੂਥ ਅਕਾਲੀ ਦਲ ਦੇ
ਸੂਬਾ ਮੀਤ ਪ੍ਰਧਾਨ ਸ਼੍ਰੀ ਗੁਰਿੰਦਰਪਾਲ ਸਿੰਘ ਧਨੌਲਾ ਵੀ ਹਾਲੇ ਤਕ ਚੋਣ ਮੁਹਿੰਮ ਵਿਚ ਨਹੀਂ ਆਏ ।
ਬਰਨਾਲਾ ਧੜੇ ਦੇ ਸੂਤਰਾਂ ਦੀ ਮਿਲੀ ਜਾਣਕਾਰੀ ਅਨੁਸਾਰ ਇਹ ਗੱਲ ਵੀ ਸਾਹਮਣੇ ਆਈ ਹੈ ਕਿ ਇਹ ਧੜਾ
ਸਵਰਗੀ ਸੰਤ ਹਰਚੰਦ ਸਿੰਘ ਲੌਂਗੋਵਾਲ ਦੀ ਭੈਣ ਸ਼ਿਆਮ ਕੌਰ ਨੂੰ ਚੋਣ ਲੜਾਉਣਾ ਚਾਹੁੰਦਾ ਸੀ । ਇਸ
ਧੜੇ ਦੇ ਵਰਕਰ ਵੀ ਅਜੇ ਚੋਣ ਮੁਹਿੰਮ ਵਿਚ ਨਹੀਂ ਤੁਰੇ । ਇਹ ਵੀ ਚਰਚਾ ਹੈ ਕਿ ਇਹ ਧੜਾ ਪਿਛਲੇ
ਵਰ੍ਹੇ ਸ਼੍ਰੀ ਸੁਖਦੇਵ ਸਿੰਘ ਢੀਂਡਸਾ ਦੇ ਧੜੇ ਵਲੋਂ ਲੋਕ ਸਭਾ ਚੋਣਾਂ ਵਿਚ ਕੀਤੇ ਵਿਰੋਧ ਦਾ ਬਦਲਾ ਲੈਣ
ਦੀ ਤਾਕ ਵਿਚ ਹੈ । ਉਂਜ ਭਾਵੇਂ ਇਸ ਇਲਾਕੇ ਵਿਚ ਬਰਨਾਲਾ ਧੜੇ ਦਾ ਬਹੁਤਾ ਪ੍ਰਭਾਵ ਨਹੀਂ । </p>

<p>ਜਸਟਿਸ ਗੁਰਨਾਮ
ਸਿੰਘ ਦੇ ਪੁੱਤਰ ਅਤੇ ਸੱਤ ਹੋਰਨਾਂ ਵਿਰੁੱਧ ਪਰਚਾ ਦਰਜ </p>

<p>ਲੁਧਿਆਣਾ, 11 ਸਤੰਬਰ - ਲੁਧਿਆਣਾ
ਪੁਲਿਸ ਨੇ ਪੰਜਾਬ ਦੇ ਸਾਬਕਾ ਮੁੱਖ ਮੰਤਰੀ ਜਸਟਿਸ ਗੁਰਨਾਮ ਸਿੰਘ ਨੇ ਲੜਕੇ ਗੁਰਬੀਰ ਸਿੰਘ ਅਤੇ
ਗੁਰੂ ਨਾਨਲ ਗਰਲਜ਼ ਕਾਲਜ ਦੀ ਪ੍ਰਿੰਸੀਪਲ ਸਮੇਤ ਸੱਤ ਵਿਅਕਤੀਆਂ ਖਿਲਾਫ ਧੋਖਾਦੇਹੀ ਦੇ ਕਥਿਤ ਦੋਸ਼
ਅਧੀਂ ਮੁਕੱਦਮਾ ਦਰਜ ਕੀਤਾ ਹੈ । ਇਸ ਸਬੰਧੀ ਗੁਰੂ ਨਾਨਕ ਗਰਲਜ਼ ਕਾਲਜ ਮਾਡਲ ਟਾਊਨ ਦੀ ਪ੍ਰਬੰਧਕ
ਕਮੇਟੀ ਮੈਂਬਰਾਂ ਗੁਰਿੰਦਰ ਗਰੇਵਾਲ ਅਤੇ ਜਗਦੇਵ ਸਿੰਘ ਦੀ ਸ਼ਿਕਾਇਤ ਤੇ ਪੁਲਿਸ ਨੇ ਗੁਰਬੀਰ ਸਿੰਘ,
ਹਰਮੀਤ ਸਿੰਘ, ਰਣਬੀਰ ਸਿੰਘ, ਪ੍ਰਿਥੀਪਾਲ ਸਿੰਘ, ਪ੍ਰਿੰਸੀਪਲ ਜਸਬੀਰ ਕੌਰ ਮਿਨਹਾਸ, ਐਸ.ਐਸ.ਸਿੰਘ
ਅਤੇ ਗੁਰਮੀਤ ਸਿੰਘ ਖਿਲਾਫ ਧਾਰਾ 406/420 ਅਧੀਨ ਮੁਕੱਦਮਾ ਦਰਜ ਕੀਤਾ ਹੈ । ਪੁਲਿਸ ਪਾਸ ਲਿਖਵਾਈ
ਮੁਢਲੀ ਰਿਪੋਰਟ ਵਿਚ ਸ਼ਿਕਾਇਤਕਰਤਾ ਨੇ ਦੱਸਿਆ ਹੈ ਕਿ ਉਕਤ ਕਥਿਤ ਦੋਸ਼ੀਆਂ ਨੇ ਜਾਅਲੀ ਦਸਤਖਤ
ਅਤੇ ਦਸਤਾਵੇਜਾਂ ਦੇ ਆਧਾਰ ਤੇ ਟਰੱਸਟ ਦੀ ਰਕਮ ਨੂੰ ਖੁਰਦ-ਬੁਰਦ ਕਰ ਦਿੱਤਾ । ਇਸ ਮਾਮਲੇ ਦੀ ਜਾਂਚ
ਦਾ ਕੰਮ ਐਸ.ਪੀ ਸਿਟੀ (ਆਈ) ਨੂੰ ਸੌਂਪਿਆ ਗਿਆ ਸੀ ਅਤੇ ਉਸ ਤੋਂ ਬਾਅਦ ਉਕਤ ਕਥਿਤ ਦੋਸ਼ੀਆਂ
ਖਿਲਾਫ ਪੁਲਿਸ ਨੇ ਕੇਸ ਦਰਜ ਕਰ ਲਿਆ ਹੈ ।  </p>

<p>ਜਿਥੇ-ਜਿਥੇ ਵੀ
ਸਿੱਖ ਗੁਰੂਆਂ ਦੇ ਬੁੱਤ ਲੱਗੇ ਹੋਏ ਹਨ ਉਹ ਆਪਸੀ ਵਿਚਾਰ-ਵਟਾਂਦਰੇ ਤੋਂ ਬਾਅਦ ਹਟਾ ਦਿੱਤੇ ਜਾਣ
- ਜਗੀਰ ਕੌਰ </p>

<p>ਜਲੰਧਰ, 11 ਸਤੰਬਰ - ਸ਼੍ਰੋਮਣੀ
ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਪ੍ਰਧਾਨ ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਨੇ ਕਿਹਾ ਹੈ ਕਿ ਸਿੱਖ ਧਰਮ ਵਿਚ ਮੂਰਤੀ
ਪੂਜਾ ਦਾ ਕੋਈ ਸਥਾਨ ਨਹੀਂ ਹੈ ਅਤੇ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਸਿੱਖ ਗੁਰੂਆਂ ਦੀ ਮੂਰਤੀਆਂ ਲਾਉਣ ਦੀ
ਇਜਾਜ਼ਤ ਨਹੀਂ ਦੇਵੇਗੀ । </p>

<p>ਅੱਜ ਸਥਾਨਕ ਸਰਕਟ ਹਾਊਸ ਵਿਖੇ
ਪੱਤਰਕਾਰਾਂ ਨਾਲ ਗੱਲਬਾਤ ਕਰਦਿਆਂ ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਪੰਜਾਬ ਵਿਚ ਜਿਨ੍ਹਾਂ ਥਾਵਾਂ ਤੇ ਗੁਰੂਆਂ ਦੀ
ਮੂਰਤੀਆਂ ਲੱਗੀਆਂ ਹੋਈਆਂ ਹਨ ਉਨ੍ਹਾਂ ਨੂੰ ਆਪਸੀ ਵਿਚਾਰ ਵਟਾਂਦਰੇ ਤੋਂ ਬਾਅਦ ਹਟਾ ਦੇਣਾ ਚਾਹੀਦਾ
ਹੈ । ਇਹ ਪੁੱਛਣ ਤੇ ਕਿ ਕਿਨ੍ਹਾਂ-ਕਿਨ੍ਹਾਂ ਥਾਵਾਂ ਤੇ ਗੁਰੂਆਂ ਦੇ ਬੁੱਤ ਲੱਗਣ ਦੀ ਜਾਣਕਾਰੀ ਸ਼੍ਰੋਮਣੀ
ਕਮੇਟੀ ਨੂੰ ਮਿਲੀ ਹੈ ਤਾਂ ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਇਸ ਬਾਰੇ ਜਾਣਕਾਰੀ ਇਕੱਠੀ ਕੀਤੀ ਜਾ ਰਹੀ ਹੈ ।
ਉਂਝ ਉਹ ਇਨ੍ਹਾਂ ਧਾਰਮਿਕ ਸਥਾਨਾਂ ਤੇ ਜਾਂਦੇ ਹੀ ਨਹੀਂ ਜਿਥੇ ਗੁਰੂਆਂ ਦੇ ਬੁੱਤ ਲੱਗੇ ਹੁੰਦੇ ਹਨ ।
</p>

<p>ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਨੇ ਕਿਹਾ ਕਿ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਵਲੋਂ ਧਰਮ ਪ੍ਰਚਾਰ ਦਾ ਕੰਮ ਕੀਤਾ ਜਾ ਰਿਹਾ
ਹੈ । ਪੱਛਮੀ ਸਭਿਅਤਾ ਦੇ ਸਿੱਖ ਧਰਮ ਤੇ ਹੋ ਰਹੇ ਹਮਲਿਆਂ ਨੂੰ ਦੇਖਦਿਆਂ ਇਨ੍ਹਾਂ ਦਾ ਮੁਕਾਬਲਾ ਕਰਨ
ਲਈ ਪਿਛਲੇ ਡੇਢ-ਦੋ ਸਾਲ ਤੋਂ ਧਰਮ ਪ੍ਰਚਾਰ ਵਿਚ ਤੇਜੀ ਲਿਆਂਦੀ ਗਈ ਹੈ ਅਤੇ ਸਿੱਖ ਧਾਰਮਿਕ ਸੰਸਥਾਵਾਂ
ਨੂੰ ਸ਼੍ਰੀ ਅਕਾਲ ਤਖਤ ਸਾਹਿਬ ਦੀ ਮਾਣ-ਮਰਿਆਦਾ ਪੂਰੀ ਤਰ੍ਹਾਂ ਲਾਗੂ ਕਰਲ ਲਈ ਕਿਹਾ ਜਾ ਰਿਹਾ ਹੈ </p>

<p> ਪਿਛਲੀਆਂ
ਖਬਰਾਂ
</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>ਵਿਚਾਰ
ਮੰਚ </p>

<p>ਉਨਟਾਰੀਓ ਗੁਰੂਦੁਆਰਾਜ਼ ਕਮੇਟੀ ਲਈ ਸਵੈ-ਪੜਚੋਲ੍ਹ ਦੀ
ਘੜੀ....ਸੁਖਮਿੰਦਰ ਸਿੰਘ ਹੰਸਰਾ </p>

<p>ਗੁਰਸਿੱਖਾਂ
ਵਿੱਚ ਆਪਸੀ ਫੁੱਟ ਸਮੁੱਚੇ ਭਾਈਚਾਰੇ ਲਈ ਦੁਖਾਂਤ ਹੈ। ਭਾਵੇ ਕਿ ਸਾਨੰੂ ਅਜਿਹਾ ਅਨੁਭਵ,  Pu@t dy nqIjy inklx qoN b`Ed hI huMd` hY pr
is@K isD~q dI mu@FlI buinE`d ies g@l dI guE`hI idMdI hY ik Pu@t inhsMsy pMQk
ih@q iv@c nhIN hY| </p>

<p>          is@K ieiqh`s iv@c Pu@t dIE~ Enyk~
aud`hrx~ imldIE~ hn ijnH~ dy is@ty G`qk hI inkly hn| Egr do dh`ikE~ dIE~ Gtn`v~
hI ivc`r leIey q~ vI ieh mMnx` pvyg` ik Pu@t DuMdlHy Biv@K d` jnm d`q` hY| </p>

<p>          "Pu@t" iksy pRIv`r, sMsQ`,
gru@p  j~ iksy kOm iv@c pY j`vy q~ ieh
qb`hI hI krv`auNdI hY| ies qoN b@cx d` ie@ko ie@k qrIk` hY ik s`nMU ie@k isD~q
d` p@l` PV ky c@lx dI E`dq p`auxI c`hIdI hY| ieh isD~q B`eI caup` isMG iC@br jI
vloN rihqn`imE~ iv@c s`fy leI ieauN EMikq kIq` hY ijvyN: jo is@K, is@K d`
mUMh iPtk`ry so BI qnK`hIE`, jo is@K, is@K nMU bur` E`Ky kRoD n`l so BI
qnK`hIE`, jo is@K, is@K dI p@g nMU h@Q p`ey so BI qnK`hIE`, jo is@K, is@K dy
d`VHy nMU h@Q p`ey, Eru lVy so BI qnK`hIE`, jo is@K, is@K dy kys~ nMU h@Q p`ey,
so BI qnK`hIE`, jo is@K n`l h@Qo-p`eI lV`eI kry, so BI qnK`hIE`  E`id| </p>

<p>ਉਪਰੋਤਕ
ਰਹਿਤਨਾਮਿਆਂ ਦੇ ਸੰਦਰਭ ਵਿੱਚ ਅਗਰ ਅਸੀਂ ਇਨ੍ਹਾਂ ਕੁ ਆਪਣੇ ਆਪ ਨੰੂ ਸੰਜਮ ਵਿੱਚ ਰੱਖਣ ਦੀ ਆਦਤ
ਪਾ ਲਈਏ ਤਾਂ ਫੁੱਟ ਵਰਗੀ ਨਾਮੁਰਾਦ ਬਿਮਾਰੀ ਤੋਂ ਛੁਟਕਾਰਾ ਮਿਲ ਸਕਦਾ ਹੈ। </p>

<p>          is@K ieiqh`s iv@c Pu@t d` k`l`  pRsq`v drj hY, ijhV` drs`auNd` hY ik ikvyN
is@K~ dI Pu@t k`rx ienH~ d` dunIE~ qoN r`j Ku@isE` Eqy is@K E@j q@k aus Pu@t d`
sMq`p hMF` rhy hn Eqy gul`mI dI c@kI iv@c ips rhy hn| Egr ipCly do dh`ikE~ dy
ieiqh`s v@l nzr m`rIey q~ sB qoN v@f` Eqy G`qk duK~q ijs nMU hr (suihrd) is@K
mihsUs krd` hY auh vI Ek`lIE~ dI Pu@t ivcoN hI pYd` hoieE` sI| Ek`lIE~ dI Pu@t
dy k`rx hI B`rq srk`r 1984 iv@c drb`r s`ihb aupr hml` krn leI Syr bxI sI Eqy
is@K dunIE` Br iv@c zlIl hoey| </p>

<p>           Egr K`VkU dOr v@l J`qI m`rIey q~ pMj`b dIE~ cox~ dy b`eIk`t qoN
auprMq K`VkU sP~ iv@c pYd` hoeI Pu@t k`rx E@j K`VkU lihr ivc`r goStIE~ q@k sImq
ho ky rih geI hY|(ies qoN ieh B`v nhIN lY lYx` c`hId` ik K`ilsq`n dI lihr iv@c
KVoq E`eI hY ikauNik auh inrMqr j`rI hY) </p>

<p>          siQqI EYnI gMBIr bx cu@kI hY ik is@K
sm`j ivcoN koeI vI EijhI sMsQ` j~ lIfr nhIN ijs nMU "dUsr`" pRv`inq
kr irh` hovy| hr koeI iehI khI j` irh` hY   myrI nIqI pMQk hY Eqy
dUsry pMQ d` nuks`n kr rhy hn| EYn` ku@J v`pr j`x qoN b`Ed vI pMQk sP~ iv@c
Pu@t pRbl hY Eqy hMk`r  dI hrkq iv@c
koeI hlImI nhIN E`eI sgoN E`pixE~ d` glH` db`aux iv@c m`x mihsUs kIq` j` irh`
hY| </p>

<p>          ieQy ieh vrnxwog hY ik  jdoN aunt`rIA iv@c gurUduE`r` kmytIE~ d`
E`psI tkr`A is@K kOm d` nuks`n kr irh` sI Eqy E`psI tkr`A iv@c kOmI SkqI  nSt ho rhI sI q~ ies nUMU T@l p`aux Eqy kOmI
SkqI nMU kOm dy ih@q~ iv@c vrqx leI iksy aupr`ly dI jrUrq sI| </p>

<p>          EijhI soc ivcoN au.gu.k. d` jnm hoieE`
sI| ies sMsQ` nMU sQ`pq krn leI bV` sMGrS krn` ipE` sI| ies nMU sQ`pq krn leI
sMGrS krq` Eqy mu@Fly smyHN dOr`n ies kmytI d` imE`r aupr cu@kx v`ly ies dy
spoksmYn sR: irpsoDk isMG gryv`l ny Ed`r` s~J svyr` n`l g@l b`q dOr`n d@isE` ik
"ieh sMsQ` is@K kOm dI imMnI p`rlImYNt dy pYtrn qy bx`eI geI sI ijQy
gurUduE`irE~ dy pRqIin@D E` ky E`pxy vKryvyN nij@T skx"| </p>

<p>          aunt`rIA gurUduE`r` kmytI aunt`rIA
iv@c is@K kOm dI bulMd E`v`z bx ky auBrnI c`hIdI sI ijs n`l is@K kOm dI
pRqIin@Dq` Tos qrIky n`l ie@k plytP`rm qoN ho skdI| ieh koiSS au@qrI EmrIk` dy
is@K~ vloN 1984 iv@c ivSv is@K sMsQ` sQ`pq krky vI kIqI geI sI ijs nMU ku@J
SkqIE~ vloN ausy smyHN hI nk`r` krn dIE~ koiSS~ E`rMB kr id@qIE~ geIE~ sn
PlsrUp ieh sMs`r p@Dr dI sMsQ` dUsrIE~ lokl sMsQ`v~ v~g  ku@J ku is@K~ dI sMsQ` hI bx ky rih geI hY|
B`vyN ik ies sMsQ` ny dsq`r Eqy ikrp`n dy kys iv@c E`pxI pihc`x bx` leI hY Eqy
kYnyf` d` hr isE`sqd`n ies sMsQ` dy kMm k`r qoN j`xUM hY pr ieh sMsQ` smu@cy
is@K~ dI sMsQ` bnx iv@c EsPl hoeI| </p>

<p>          hux ieho h`l au.gu.k. d` hoieE` hY|
ieh sMsQ`, ijs qoN lok~ nMU bhuq E`s~ sn E`psI tkr`A d` iSk`r ho ky mihz ngr
kIrqn kmytI bx ky rih geI hY ijs d` izkr ies kmytI vloN krv`eI geI ie@k k`nPrMs
iv@c ifksI rof gurUduE`r` dI kmytI mYNbr sRd`rnI hrmyl kOr ny vI kIq` sI| ieQy
ieh vI kihx` v`ijb hY| ies kmytI nMU bn`aux dy mksd nMU m@dy nzr r@KidE~ Egr
ies kmytI dy kMm~ nMU pVcoilE` j`vy q~ ieh g@l auBr ky s`hmxy E`auNdI hY ik ijs
mksd leI ieh kmytI bx`eI geI aus inS`ny qoN ieh au@k cu@kI hY| gurUduE`irE~ dy
E`psI q`lmyl nMU vD`aux dI bj`ey ies kmytI dy Ehudyd`r~ aupr p@Kp`q d` doS l@g
irh` hY|   </p>

<p>          aunt`rIA gurUduE`r`z kmytI dI cox
pRikirE` ies sMsQ` dy ies duK~q d` mu@F ikh` j` skd` hY| cox smyHN ies dy s`ry
mYNbr gurUduE`irE~ vloN E`pxy fYlIgyt Byjy j~dy hn Eqy ienH~ fYlIgyt~ vloN
iesdI vrikMg kmytI dI cox j~ inwukqI kIqI j~dI hY| </p>

<p>          ivc`rn v`l` mu@d` ieh hY ik izE`d`qr
gurUduE`irE~ iv@c pRbMDk kmytIE~, sMgq dI sihmqI lYx qoN ibnH~ bx`eIE~ j~dIE~
hn| bhuqy gurUduE`irE~ iv@c mYNbriSp hI nhIN hY Eqy s`l~ b@DI gru@p~ d` kbz`
hY| B`vyN auQy pRbMD cMg` hI hovy pr sMgq ivcoN iksy nMU syv` d` mOk`, k`bz DVy
dI imhrb`nI qoN ibnH~ nhIN iml skd` | Egr sMgq d` gurUduE`irE~ dIE~ kmytIE~
bn`aux iv@c koeI K`s wogd`n nhIN ilE` j~d` q~ ienH~ kmytIE~ vloN sQ`pq kIqI
j~dI ies kmytI vloN kimaUntI dI pRqIinDq` vI sMquStI v`lI nhIN ho skdI| </p>

<p>          inrsMdyh aunt`rIA gurUduE`r`z kmytI dI
sQ`pn` kOm dy p@K nMU auB`rn leI kIqI geI sI Eqy SurU SurU iv@c sR: irpsoDk
isMG gryv`l dI Egv`eI hyT ies kmytI  dI
cMgI k`rguz`rI s`hmxy E`eI sI| EPsos ies g@l d` hY ik s`fI sus`ietI iv@c lMmy
Ersy qoN pYd` hoey ruJ`n, ik jdoN koeI cIz qr@kI krn l@gdI hY q~ ies dIE~ l@q~
iK@cx v`lIE~ SkqIE~ isr cu@k lYNdIE~ hn, ies sMsQ` dy joVIN bih igE` hY|           EsIN aunt`rIA gurUduE`r`z kmytI nMU
Enyk~ v`r mIfIey n`l sbMD sud`rn leI bynqIE~ kIqIE~ hn q~ jo doh~ iDrH~ dI SkqI
nMU s~Jy rUp iv@c B`eIc`rk qr@kI leI l`ieE` j` sky| au. gu. k. ny ies idS` v@l
kdm pu@tidE~ ies s`l vYs`KI qoN b`Ed mIfIey n`l Akivl gurUduE`r` s`ihb ivKy
ie@k imlxI d` pRogR`m jrUr bx`ieE` sI pr auh Fu@kvyN qrIky n`l s@d` n~ dyx
bdOlq sPl n~ ho sikE`| </p>

<p>          EsIN ie@k v`r iPr ies kmytI dy
ijMmyv`r num`ieMidE~ Eqy smu@cy gurUduE`r` s`ihb dy mYNbr~ nMU bynqI krdy h~ ik
hux isr joV ky ivc`r krn d` vyl` E` igE` hY| au. gu. k. dI sQ`pn` qoN lY ky hux
q@k dI k`rguz`rI d` EiDEYn krn` jrUrI ho igE` hY Eqy Pr`KidlI n`l E`pxIE~
kmzorIE~ svIk`r krky aunH~ nMU dUr krn dI rucI pRbl hoxI c`hIdI hY| q~ ik ies
kOmI sMsQ` nMU kOm dy ih@q~ leI srgrm kIq` j` sky| ShId~ dy b@cy rulH rhy hn
Eqy pMQk sMGrS sMzId` iDE`n dI mMg krd` hY|   </p>

<p>          ies kmytI dy mYNbr~ nMU, kimaUntI dy
isE`xy isr~ nMU iek@qr krky "sP` ivC`", "duibD` dUr" krky
ivc`r~ d` dOr SurU krn c`hId` hY Eqy ies iv@c pYd` hoeIE~ qryV~ imt`aux dI
koiSS krnI c`hIdI hY| B`v hux is@K sm`j dI pRPu@lq` leI aunt`rIA gurUduE`r`z
kmytI nMU svY-pVcolH krnI zrUrI ho geI hY Eqy b`h~ aul`r ky kimaUntI dy GrIN
bYTy suihrd is@K~ nMU E@gy ilE`aux leI wqnSIl hox` c`hId` hY | Ed`r` s~J svyr`
pMQ dI cVHdI kl` leI, auproqk idS` v@l suihrdq` n`l grz-rihq sihwog dyx leI
vcnb@D hY| </p>

<p> </p>






</body></text></cesDoc>