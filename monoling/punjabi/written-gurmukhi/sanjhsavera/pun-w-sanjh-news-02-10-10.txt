<cesDoc id="pun-w-sanjh-news-02-10-10" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-02-10-10.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 02-10-10</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>02-10-10</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਬੂਟਾ
ਸਿੰਘ ਦੇ ਨਜ਼ਦੀਕੀ ਵਪਾਰੀ ਨੂੰ ਕਾਰ ਸਮੇਤ ਜਿੰਦਾ ਸਾੜਿਆ
ਮੋਗਾ
:(ਸਾਂਝ ਸਵੇਰਾ ਬਿਊਰੋ)  ਸਾਬਕਾ ਕੇਂਦਰੀ ਮੰਤਰੀ ਬੂਟਾ ਸਿੰਘ ਦੇ ਇਕ ਬਹੁਤ ਨਜ਼ਦੀਕੀ ਵਪਾਰੀ
ਨੂੰ ਕੁਝ ਅਣਪਛਾਤੇ ਲੋਕਾਂ ਨੇ ਉਨ੍ਹਾਂ ਦੀ ਕਾਰ ਵਿਚ ਹੀ ਜਿੰਦਾ ਜਲਾ ਦਿਤਾ। ਘਟਨਾ ਅੱਜ ਸਵੇਰੇ ਦੀ
ਹੈ। ਇਹ ਪਤਾ ਨਹੀਂ ਲੱਗ ਸਕਿਆ ਕਿ ਉਕਤ ਵਪਾਰੀ ਨੂੰ ਕਿਸਨੇ ਅਤੇ ਕਿਓ ਜਲਾਇਆ। ਫਿਲਹਾਲ, ਇਸ
ਦਰਦਨਾਕ ਕਾਂਢ ਦੇ ਬਾਦ ਪੂਰੇ ਇਲਾਕੇ ਵਿਚ ਖੋਫ ਦਾ ਮਾਹੌਲ ਹੈ। ਮੈਹਣਾ ਪੁਲਿਸ ਨੇ ਕਤਲ ਦੇ ਦੋਸ਼
ਵਿਚ ਧਾਰਾ 302, 427, ਅਤੇ 436 ਦੇ ਤਹਿਤ ਮਾਮਲਾ ਦਰਜ ਕਰਕੇ ਕਾਰਵਾਈ ਸ਼ੁਰੂ ਕਰ ਦਿੱਤੀ ਹੈ। 

b`dl EYs.jI.pI.sI. cox~ dyrI n`l krv`aux` c`huMdI hY
: K`ls`
luiDE`x`;- (s~J svyr` ibaUro) srb ihMd SRomxI Ek`lI dl dy wuv` m`milE~
dy ieMc`rj Eqy s`bk` ivD`iek ibkrmjIq isMG K`ls` ny pRk`S isMG b`dl qy doS
l`ieE`ik auh SRomxI gurduE`r` pRbMDk kmytI dI cox~ dyrI n`l krv`aux` c`hudy
hn|srb ihMd Ek`lI dl EYs.sI./EYs.tI/ ivMg dI bYTk nUM sMboiDq krdy hoey SRI
K`ls` ny ikh` ik b`dl nUM fr hY ik id@lI gurduE`r` pRbMDk kmytI dI qrH~ SRomxI
gurduE`r` pRbMDk kmytI dI cox~ ivc vI auhn~ dI p`rtI nUM h`r d` s`hmx` krn` pY
skd` hY|</p>

<p> 
is@K m`milE~ ivc dKlEMd`zI kr rhy hn EmirMdr :
bMfUgr
E`dmpur : (s~J svyr` ibaUro) &amp;#145; mu@K mMqrI EmirMdr E`pxy EiDk`r~ d`
glq pRwog kr is@K~ dy D`rimk m`milE~ Eqy EYs.jI.pI.sI. dy nvMbr ivc hox v`lIE~
cox~ ivc dKlEMd`zI kr rhy hn| ies dKlEMd`zI nUM jykr kYptn ny bMd n` kIq` q~
ausdy nqIzy gMBIr ho skdy hn|&amp;#146; ieh g@l SRomxI gurduE`r` pRbMDk kmytI dy
pRD`n pRo: ikRp`l isMG bMfUgr ny E@j gurduE`r` Kuh s`ihb Kurdpur ivc khI| </p>

<p>
jMmU kSmIr &amp;#145;c sKq sur@iKE` hyT vot~ dI igxqI
E@j
jMmU : (s~J svyr` ibaUro) jMmU kSmIr ivD`nsB` cox~ leI peIE~ vot~ dI
igxqI sKq sur@iKE` pRbMD~ hyT Blky SurU hovygI| ies igxqI leI 18 kyNdr sQ`pq
kIqy gey hn| sUqr~ Enus`r igxqI d` kMm svyry 8.00 vjy SurU ho j`vyg` qy dupihr
b`Ed nqIjy imlxy SurU ho j`vyg` qy dupihr b`Ed nqIzy imlxy SurU ho j`xgy| ies
v`r ku@l 87&amp;#146; coN 85 sIt~ leI vot~ peIE~ sn qy do sIt~ lyh qy nubr` (l@d`K)
qoN do aumIdv`r ibn~ muk`bl` ij@q gey sn| ieh dovyN sonm v~gcuk qy n`v~g irgizn
E`z`d aumIdv`r sn| vot~ dI igxqI s`ry dy s`ry 14 izlH` sdr muk`m~ &amp;#145;qy
ikSqv`V, B@drv`h qy r`mbn(fof` izlH`) dy qihsIl hYf@kuE`rtr~ qy Eqy bdg`m ijlHy
&amp;#145;c do v@K v@K h`l~ &amp;#145;c hovygI|
srk`rI sUqr~ Enus`r ijlH~ EiDk`rI, igxqI eyjMt, ingr`n qy cox kimSn v@loN Byjy
gey ingr`n igxqI dI ingr`nI krngy| ienH~ cox~ &amp;#145;c r`j dy 56 l@K &amp;#145;coN
46 pRqISQ votr~ ny ih@s` ilE`| cox~ leI 7025 poilMg bUQ bx`ey gey sn qy vot~
c`r gyV~ &amp;#145;c peIE~ ku@l 584 aumIdv`r cox mYd`n &amp;#145;c hn|
cox ieiqh`s &amp;#145;c ieh pihlI v`r hY ik vot~ ielYktR`ink voitMg mSIn~ zrIey
peIE~| Blky pihl` nqIj` dupihr qoN pihl~ E`aux dI sMB`vn` hY| Blky v`ly nqIijE~
&amp;#145;c nYSnl k`nPrMs dy pRD`n qy ies dy mu@K mMqrI dy Ehudy dy d`Evyd`r aumr
Ebdu@l` qy pIplz fYmokryitk p`rtI dI mIq pRD`n mihbUb` muPqI smyq keI pRmu@K
E`gUE~ dI iksmq d` PYsl` hox` hY|

kyNdr Eqy r`j srk`r v@loN Jony qy id@qy bons k`rn
BMblBUs` ipE`
KMn` :(s~J svyr` ibaUro) byS@k pMj`b dIE~ v@K v@K mMfIE~ &amp;#145;c Jony dI
E`md zor~ &amp;#145;qy hY, pRMqU kyNdr v@loN 20 rupey Eqy r`j srk`r v@loN 20 rupey
kuieMtl soky dy m`mly &amp;#145;qy bons dyx d` v@fI p@Dr qy BMblBus` ipE` hoieE`
hY| r`jsI p`rtIE~ Eqy iks`n jQybMdIE~ v@loN EYny EMdoln~, DrinE~ Eqy ryl roko
pRogr`m~ dy b`vjUd iks`n~ nUM iek Pu@tI kOfI vI P`lqU nhIN imlI Eqy Jon` ipCly
s`l v~g 560 rupey pRqI kuieMtl hI mMfIE~ ivc ivk irh` hY| 
 </p>

<p>ਹੁਣ ਨਰਾਜ਼ ਵਿਧਾਇਕਾਂ ਦੀਆਂ ਸ਼ਿਕਾਇਤਾਂ ਸੁਣਿਆਂ ਕਰਨਗੇ ਮੁੱਖ ਮੰਤਰੀ ਦੇ
ਸਾਹਿਬਜ਼ਾਦੇ
ਜਲੰਧਰ
:-(ਸਾਂਝ ਸਵੇਰਾ ਬਿਊਰੋ) ਪੰਜਾਬ ਫ਼#145;ਚ ਕਾਂਗਰਸ ਦੇ ਵਿਧਾਇਕ ਦਲ ਫ਼#145;ਚ ਨਰਾਜ਼ ਸਮਝੇ ਜਾਂਦੇ
ਧੜੇ ਦੇ ਚਾਰ ਵਿਧਾਇਕਾਂ ਵੱਲੋਂ ਮੰਤਰੀਆਂ ਵਿਰੁੱਧ ਲਾਈਆਂ ਸ਼ਿਕਾਇਤਾਂ ਸੁਣ ਕੇ ਮੁੱਖ ਮੰਤਰੀ
ਪੰਜਾਬ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਹੱਕੇ ਬੱਕੇ ਰਹਿ ਗਏ ਤੇ ਉਨਾਂ੍ਹ ਇਨ੍ਹਾਂ ਨਾਰਾਜ਼ ਵਿਧਾਇਕਾਂ ਦੀਆਂ
ਸਮੱਸਿਆਵਾਂ ਦਾ ਹੱਲ ਪੱਕੇ ਤੌਰ ਤੇ ਕਰਦਿਆਂ ਮੀਟਿੰਗ ਫ਼#145;ਚ ਬੈਠੇ ਆਪਣੇ ਸਾਹਿਬਜ਼ਾਦੇ ਦੇ
ਹਵਾਲੇ ਇਹ ਜਿੰਮਾ ਲਾ ਦਿੱਤਾ ਹੈ ਕਿ ਜੇਕਰ ਕੋਈ ਮੰਤਰੀ ਇਨ੍ਹਾਂ ਵਿਧਾਇਕਾਂ ਦੀ ਗੱਲ ਨਹੀਂ ਸੁਣਦਾ
ਤਾ ਉਸਦੀ ਪੁੱਛਗਿੱਛ ਕੀਤੀ ਜਾਵੇ। ਅੱਜ ਮੁੱਖ ਮੰਤਰੀ ਦੀ ਰਿਹਾਇਸ਼ ਫ਼#145;ਤੇ ਸਵੇਰੇ ਸਾਢੇ 10
ਵਜੇ ਹੋਈ ਮੀਟਿੰਗ ਫ਼#145;ਚ ਮੁੱਖ ਮੰਤਰੀ ਨੇ ਇਨ੍ਹਾਂ ਵਿਧਾਇਕਾਂ ਨੂੰ ਪੂਰਨ ਭਰੋਸਾ ਦਿੱਤਾ ਕਿ
ਭੱਵਿਖ ਫ਼#145;ਚ ਉਨ੍ਹਾਂ ਨੂੰ ਕਿਸੇ ਕਿਸਮ ਦੀਆਂ ਸਮੱਸਿਆਵਾਂ ਨਹੀਂ ਆਉਣ ਦਿੱਤੀਆਂ ਜਾਣਗੀਆਂ।
ਮੁੱਖ ਮੰਤਰੀ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਨੇ ਇਨ੍ਹਾਂ ਵਿਧਾਇਕਾਂ ਨੂੰ ਆਉਂਦੇ ਦੋ ਹਫਤਿਆਂ ਫ਼#145;ਚ
ਕਿਧਰੇ ਨਾ ਕਿਧਰੇ ਫ਼#145;ਅਡਜਸਟਫ਼#146; ਕਰਨ ਦਾ ਵੀ ਭਰੋਸਾ ਦਿੱਤਾ। 

EbU slym nUM B`rq ilE`aux ivc keI s`l vI l@g skdy
hn: Srm`
nvI id@lI:(s~J svyr` ibaUro) sI.bI.E`eI dy muKI SRI pI.sI Srm` ny E@j
ieQy ikh` ik Epr`D muKI EbU slym nUM purqg`l nUM B`rq v`ps ilE`aux ivc l@b` sm~
l@g skd` hY Eqy ieh sm~ s`l~ ivc vI ho skd` hY aun~ ikh` ik purqg`l dyS du
k`nUMn dy EiqEMq sKq ho k`rn EbU slym nUM B`rq ilE`aux` soK` kMm nhI| purqg`l
wUrpIn B`eIc`ry dy doS~ dy k`nUMn n`l b@J` hoieE` hY Eqy aus leI EbU slym nUM
B`rq hv`ly krn` EiqEMq EoK` hY| </p>

<p>
EsI ies smyN ieiqh`s dy cor`hy qy KVy h~: muS@rP
iesl`m`b`d:(s~J svyr` ibaUro) p`iksq`n dy r`StrpqI sRI muS@rP ny ikh` hY
ik p`iksq`n Blky iek nvyN lokr`jI wu@g ivc d`Kl ho irh` hY| Blky dyS &amp;#145;c
hox v`lIE~ E`m cox~ qoN pihlI S`m E@j r`q tYlIivjn Eqy ryfIA qo dyS v`sIE~ dy
n~ E`pxy pRs`rn ivc sRI muSr@P ny ikh` ik EsI ies smy ieiqh`s dy iek cor`hy qy
KVy hoey h~ Eqy Blky qoN iek nv~ lokr`jI wu@g SurU krn v`ly h~|  sRI
muSr@P ijn~H Poj dI vrdI p`eI hoeI sI ,ny TyT aurdU ivc sMboDn kridE~ ikh` ik
cox~ inrp@K Eqy E`z`d`n` hoxgIE~ Eqy s@q` dyS dy nvY bxn v`ly pRD`n mMqrI dy
hv`ly kr idqI j`vygI| aun~H ikh` ik mYN dyS v`sIE~ nUM ieh wkIn idv~aud~ h~ ik
mY s`rIE` k`rjk`rI q`kq~ 
nvyN pRD`n mMqrI nUM dy idE~g` Eqy aus qo N b`Ed E`pxy mu@K EYgzYkitv d` Ehud`
vI C@f idE~g`|  

p`ik &amp;#145;c E`m cox` E@j Brosywogq` &amp;#145;qy
suE`lIE` inS`n
iesl`m`b`d:(s~J svyr` ibaUro) p`iksq`n ivc iqMn s`l pihl~ nv`z SrIP
srk`r d` qKq` pltx qo b`Ed iek-iek krky sB srk`rI q`kq~ hiQE` cu@ky jnrl muSr@P
dyS ivc lokr`j dI bh`lI leI kom~qrI B`eIc`ry dIE~ iq@KIE~ nzr~ Eqy E`z`d qy
inrp@K cox~ nUM lY ky S@k~ dy Gyr hyT Blky E`m cox~ krv` rhy hn pr dyS dIE~ pRmuK
mnu@KI EiDk`r jQybMdIE~ ny ies dI Brosywogq` aupr sv`lIE` inS`n l` idqy hn|
ien~H cox~ dI K`s g@l isrP iehI nhI kI lokr`j bh`lI d` v`Ed` PojI srk`r pUr` kr
rhI hY, sgo Esl g@l ieh hY ik jnrl muSr@P sMivD`nk qbdIlIE~ pihl~ hI kr cu@ky
hn cox~ dI inrp@Kq` d` fMk` vj`aux leI ivdyS~ qo 300 drSk vI s@d cu@ky hn| sRI
mu@SrP dy ivroDIE~ d` kihx` hY ik cox~ ivc D~dlI dI hux PojI srk`r nUM S`ied
loV nhI rih geI ikaNik auh doh~ pRD`n mMqrI E~ nv`z SrIP qy bynjIr Bu@to qo
iel`v~ v@fy -v@fy nyq`v~ nUM cox mYd`n &amp;#145;co k@F cu@kI hY |

l`dyn ny muS@rP d` qKq` ault`aux d` s@d` id@q`
pySv`r :(s~J svyr` ibaUro) iek ic@TI jo ik iesl`imk E@qv`dI As`m` ibn l`dyn
v@loN ilKI dsI j~dI hY, ivc ikh` igE` hY ik p`iksq`n dy r`StrpqI pRvyz muS@rP
dI srk`r d` qKq` ault`ieE` j`vy ikauNik auh EPg`insq`n sYink muihMm ivc EmrIk`
d` sihwogI sI|
iek p@qr jo ik kuJ EPg`n riPaUzI kYNp~ ivc vMifE` igE` hY, aus ivc ikh` igE` hY
ik iesl`mI mu@l` molvI E`pxy lok~ nUM &amp;#145;duSmx~ ivru@D jh`d&amp;#146; ivc S`ml
hox dI Egv`eI krn| ieh p@qr iriPauzI kYNp auq@r-p@CmI Sihr pyS`v`r ivc vMfy gey
jo ik EPg`n sIm` dy nyVy hn|
p`iksq`nI kOm dy n~ ilKy gey ies p@qr ivc ieh vI ikh` igE` hY, &amp;#147; myry
p`iksq`nI musilm Br`....Srmn`k muS@rP qoN Cutk`r` p`A| myr` ieh s@d` ivSyS qOr
qy mol`ixE~ leI hY&amp;#148;| 

slm`n K`n &amp;#145;26 rupey&amp;#146; dI idh`VI auqy kMm
krd` hY 
muMbeI :- EYSo Er`m dI ijMdgI dy E`dI Eqy iek iek vkq dy K`xy aupr 8 qoN
10 hz`r rupey Krc kr dyx v`ly iPlm EiBnyq` slm`n K~ nUM muMbeI dy krYPrf m`rkIt
l`kEp ivc hux ElsI ijMdgI d` sv`d vyKx nUM iml irh` hY iPlmI Kln`iek~ dI Q~ aus
nUM hux m@Cr~ n`l jUJx` pY irh` hY| v@K v@K Epr`d~ ivc sz~ j@t rhyy kYdIE~ n`l
aus nUM sm~ ibq`aux` pY irh` hY| ausnUM t@uty j~ gMdy igl`s ivc c`h pIxI pY rhI
hY| slm`n K`n nUM puils ihr`sq ivc ijMdgI S`ied kuJ sKq l@g rhI hy| aus nUM
sImYNt dy nMgy PrS aupr bYTx` pYNd` hY| b`kI Epr`DIE~ dy n`l iek hI pK`n` Gr dI
vrqoN krnI pYNdI hY| K`kI vrdI v`ly puls krmc`rIE~ dy hukm~ dI p`lx` krnI pYNdI
hY| ausnUM E`m kYdIE~ v~g roz`n` 26 rupey B@q` imld` hY| ies ivc auh svyry d`
n`Sq`, dupihr d` K`x` Eqy r`q dI rotI lY skd` hY| slm`n ny pihl~ q~ svyry kuJ
K`x qoN ienk`r kr id@q` sI, pr r`qIN iek kOlI d`l qy c`vl K`Dy| igRPq`rI v`ly
idn qoN ies ny iek hI pYNt qy tI Srt pihnI hoeI hY|
 </p>

<p>ਆਈ.ਏ.ਐਸ. ਅਧਿਕਾਰੀ ਅਰੋੜਾ ਵਿਜੀਲੈਂਸ ਜਾਂਚ ਦੇ ਘੇਰੇ ਫ਼#145;ਚ </p>

<p>ਜਗਰਾਓ
:- ਬਾਦਲ ਸਰਕਾਰ ਵੇਲੇ ਪੰਜਾਬ ਦੇ ਮੁੱਖ ਸਕੱਤਰ ਰਹੇ ਅਤੇ ਮੌਜੂਦਾ ਰਾਜ ਦੇ ਸੇਲ ਟੈਕਸ
ਟ੍ਰਿਬਿਊਨਲ-2 ਦੇ ਪ੍ਰੀਜ਼ਾਇਡਿੰਗ ਅਫਸਰ ਐਨ.ਕੇ ਅਰੋੜਾ ਆਈ.ਏ.ਐਸ ਦੀ ਬੇਨਾਮੀ ਜਾਇਦਾਦ ਬਾਰੇ
ਵਿਜੀਲੈਂਸ ਵਿਭਾਗ ਵੱਲੋਂ ਜਾਂਚ ਫ਼#145;ਚ ਲਿਆਂਦੀ ਤੇਜ਼ੀ ਤੋਂ ਜਾਪਦਾ ਹੈ ਕਿ ਇਸ ਆਈ.ਏ.ਐਸ.
ਅਫਸਰ ਵਿਰੁੱਧ ਵਿਭਾਗ ਜਲਦੀ ਹੀ ਕੇਸ ਦਾਇਰ ਕਰਨ ਜਾ ਰਿਹਾ ਹੈ।

ivjIlYNs v@loN jlMDr ingm dy 10 EiDk`rI igRPq`r
jlMDr : cOksI ibaUro ny v@fI k`rv`eI kridE~ PrzI ibl p` ky sQ`nk ngr
ingm nUM 9.5 l@K d` cUn` l`aux dy doS &amp;#145;c ingm dy iek sh`iek kimSnr smyq 10
hor EiDk`rIE~ nUM igRPq`r kIq` hY| EiDk`rIE~ dI igRPq`rI n`l ingm dy b`kI
mulzm~ &amp;#145;c vI hlcl pYd` ho geI hY| ingm v@loN Ejy t`aUn pl`inMg ivB`g
&amp;#145;c j~c krnI b`kI hY| t`aUn pl`inMg ivB`g dy krmc`rIE~ dI kiQq imlIBugq
k`rn iblfr m`PIE` pMj`b imauNspl k`rporySn k`nUMn dI aulMGx` krky iek ivS`l
bhumMzlI iem`rq aus`rn &amp;#145;c sPl ho igE`| ijs k`rn ingm nUM 100 kroV rupey
qoN v@D d` cUn` l@igE`|
 </p>

<p> </p>






</body></text></cesDoc>