<cesDoc id="pun-w-sanjh-news-01-04-29" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-01-04-29.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 01-04-29</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>01-04-29</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਫੈਡਰੇਸ਼ਨ ਮਹਿਤਾ ਦੀ ਪ੍ਰਧਾਨਗੀ ਨੰੂ ਲੈ ਕੇ ਪ੍ਰੋ. ਸਰਚਾਂਦ ਤੇ
ਭਾਈ ਗਰੇਵਾਲ ਵਿਚਾਲੇ ਦੌੜ ਤੇਜ਼ ਹੋਈ</p>

<p>ਅੰਮ੍ਰਿਤਸਰ
,28 ਅਪੈ੍ਰਲ ਪ੍ਰੋ. ਸਰਚਾਂਦ ਸਿੰਘ ਖਿਆਲਾ ਨੰੰੰੰੰੰੰੰੰੰੂ ਸਿੱਖ ਸਟੂਡੈਂਟਸ਼ ਫੈਡਰੇਸ਼ਨ (ਮਹਿਤਾ)ਦਾ
ਪ੍ਰਦਾਨ ਚੁਣੇ ਜਾਣ ਦੀ ਸੰਭਾਵਨਾ ਹੈ । ਸਥਾਨਕ ਭਾਈ ਗੁਰਦਾਸ ਹਾਲ ਵਿਖੇ 5 ਮਈ ਨੂੰ ਫੈਡਰੇਸ਼ਨ ਦੇ
ਹੋ ਰਹੇ ਅਜਲਾਸ ਦੀਆ ਤਿਆਰੀਆਂ ਜੋਰਾਂ ਤੇ ਹਨ । ਪਤਾ ਲੱਗਾ ਹੈ ਕਿ ਸ: ਗੁਰਚਰਨ ਸਿੰਘ ਗਰੇਵਾਲ ਵੀ
ਇਸ ਵਿਦਿਆਰਥੀ ਜਥੇਬੰਦੀ ਦੇ ਪ੍ਰਧਾਨਗੀ ਦੇ ਤਕੜੇ ਦਾਅਵੇਦਾਰ ਹਨ ਪਰ ਇਸ ਦੇ ਬਾਵਜੂਦ ਪ੍ਰੋ:
ਸਰਚਾਂਦ ਸਿੰਘ ਦੇ ਪ੍ਰਧਾਨ ਬਣਨ ਦੇ ਜਿਆਦਾ ਆਸਾਰ ਨਜਰ ਆ ਰਹੇ । ਇਸ ਫੈਡਰੇਸਨ ਦੇ ਦੋ ਮੋਢੀ
ਨੇਤਾਵਾਂ ਸ: ਰਜਿੰਦਰ ਸਿੰਘ ਮਹਿਤਾ ਅਤੇ ਸ: ਅਮਰਜੀਤ ਸਿੰਘ ਚਾਵਲਾ  ny soRmxI Ek`lI dl b`dl ivc s`iml hox n`l ies jQybMdI d` punrgTn
Ejy vI ienH~ doh~ nyq`v~ dy ies`irE~ qy hox` hY | s: iekb`l isMG quMg pihl~
pRD`ngI dy Ehudy dy aumIdv`r sn pr kuJ srk`rI Ehudy lYx d` ijE`d` moh hox k`rn
auh hux pRD`ngI dI d`Evyd`rI qoN l~By ho gey hn | ieh vI pq` l@g` hY ik s:
gryv`l jo sRomxI gurduE`r` pRbMDk kmytI dy pRD`n j: qlvMfI dy pirv`r dy vI k`PI
nyVy hn | kuJ smyN leI PYfrysn dI pRD`ngI d` Ehud` lY ky sRomxI Ek`lI dl b`dl
ivc Cl~g lg`aux dy c`hv`n hn | s: gryv`l dy ihm`ieqI vI c`huMdy hn ik jykr auh
PYfrysn hoxgy q~ Ek`lI dl ivc aunH~ nUM cMg` sQ`n iml skd` hY | s: EmrjIq isMG
c`vl` vI s: gryv`l nUM pRD`n bx`aux dy h@k ivc hn pr pRD`n bx`aux dI q`kq s:
rijMdr isMG mihq` dy h@Q ivc hY, jo ieh pRD`ngI EMimRqsr ivc hI r@Kx` c`huMdy
hn | ies leI Fu@kvyN aumIdv`r pRo: src~d isMG hI hn | jo ies smyN PYfrysn dy
k`rguj`rI pRD`n vI hn | </p>

<p>ਬਾਦਲ
ਜੀ ਵਾਅਦਾ ਨਿਭਾਓ ਜਾਂ ਪੰਚਾਇਤ ਦੇ ਪੈਸੇ ਮੋੜੋ </p>

<p>ਅਮਰਗੜ੍ਹ, 27 ਅਪ੍ਰੈਲ - ਇਲਾਕਾ ਮੰਨਵੀ ਦੇ
ਲੋਕ ਉਨ੍ਹਾਂ ਦੇ ਨੇੜੇ ਕੋਈ ਹਸਪਤਾਲ ਨਾ ਹੋਣ ਕਾਰਨ ਬਹੁਤ ਦੁਖੀ ਹਨ । ਇਲਾਜ ਆਦਿ ਕਰਾਉਣ ਲਈ
ਇਹਨਾਂ ਲੋਕਾਂ ਨੂੰ 10 ਕਿਲੋਮੀਟਰ ਦੂਰ ਅਮਰਗੜ ਜਾਂ ਮਲੇਰਕੋਟਲਾ ਜਾਣਾ ਪੈਂਦਾ ਹੈ । ਗ੍ਰਾਮ ਪੰਚਾਇਤ
ਮੰਨਵੀ ਵਲੋਂ ਰੋਡ ਕਰਾਸ ਕੋਲ 25 ਬੈਂਡ ਦਾ ਹਸਪਤਾਲ ਬਣਾਉਣ ਲਈ 14-5-81 ਨੂੰ ਇਕ ਲੱਖ ਰੁਪਏ
ਜਮਾਂ ਕਰਵਾਏ ਜਾ ਚੁੱਕੇ ਹਨ । ਇਸ ਪਿੰਡ ਵਿਚ 28-11-80 ਨੂੰ ਸਿਵਲ ਡਿਸਪੈਂਸਰੀ ਦਾ ਨੀਂਹ ਪੱਥਰ
ਉਸ ਸਮੇਂ ਦੇ ਪਟਿਆਲਾ ਮੰਡਲ ਦੇ ਡਿਪਟੀ ਕਮਿਸਨਰ ਸ੍ਰੀ ਅਮਰੀਕ ਸਿੰਘ ਪੁੰਨੀ ਨੇ ਰੱਖਿਆ ਸੀ ।
ਪਿੰਡ ਦੇ ਲੋਕਾਂ ਨੂੰ ਨੀਂਹ ਪੱਥਰ ਤੇ ਖਰਚਾ ਕਰਨ ਤੋਂ ਇਲਾਵਾ ਕੁਝ ਨਸੀਬ ਨਹੀ ਹੋਇਆ । ਹੁਣ ਇਹ
ਵੱਖ ਵੱਖ ਨੀਂਹ ਪੱਥਰ ਸਰਕਾਰ ਦੀ ਕਾਰਗੁਜਾਰੀ ਨੂੰ ਦਰਸਾੁੳਂਦੇ ਹਨ । ਮੁੱਖ ਮੰਤਰੀ ਸ਼੍ਰੀ ਪ੍ਰਕਾਸ਼
ਸਿੰਘ ਬਾਦਲ ਨੇ ਸੱਤਾ ਸੰਭਾਲਣ ਤੋਂ ਪਹਿਲਾਂ ਪਿੰਡ ਮੰਨਵੀ ਵਿਖੇ ਖੇਡ ਮੇਲੇ ਤੇ ਇਨਾਮ ਵੰਡ ਸਮਾਰੋਹ
ਸਮੇ ਇਹ ਹਸਪਤਾਲ ਪਹਿਲ ਦੇ ਆਧਾਰ ਤੇ ਬਣਾਉਣ ਦਾ ਐਲਾਨ ਕੀਤਾ ਸੀ । ਇਸ ਸਮੇ ਉਸ ਸਮੇ
ਸ੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ ਸ੍ਰੀ ਗੁਰਚਰਨ ਸਿੰਘ ਟੌਹੜਾ ਨੇ ਵੀ ਅਤੇ ਸ੍ਰੋਮਣੀ
ਕਮੇਟੀ ਜਨਰਲ ਸਕੱਤਰ ਸ੍ਰੀ ਸੁਖਦੇਵ ਸਿੰਘ ਢੀਂਡਸਾ ਨੇ ਵੀ ਸ੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਦੀ ਸਰਕਾਰ ਆਉਣ ਤੇ
ਪਹਿਲ ਦੇ ਆਧਾਰ ਤੇ ਇਹ ਹਸਪਤਾਲ ਬਣਾਉਣ ਦਾ ਐਲਾਨ ਕੀਤਾ ਸੀ ਪਰੰਤੂ ਇਹ ਹੁਣ ਤਕ ਫੌਕੇ ਸਾਬਤ
ਹੋਏ ਹਨ । ਇਲਾਕੇ ਦੇ ਲੋਕਾਂ ਦੀ ਮੰਗ ਹੈ ਕਿ ਪਿੰਡ ਮੰਨਵੀ ਵਿਖੇ ਜਲਦੀ ਹਸਪਤਾਲ ਬਣਾਇਆ
ਜਾਵੇਗਾ ਨਹੀ ਤਾਂ ਪੰਚਾਇਤ ਵਲੋਂ ਭਰੇ ਇਕ ਲੱਖ ਰੁਪਏ ਵਿਆਜ ਸਮੇਤ ਵਾਪਸ ਕੀਤੇ ਜਾਣ । </p>

<p>ਸ੍ਰੋਮਣੀ
ਕਮੇਟੀ ਚੋਣਾਂ ਮਿੱਥੇ ਸਮੇਂ ਤੇ ਹੋਣ ਦੀ ਸੰਭਾਵਨਾ ਨਹੀਂ </p>

<p>          luiDE`x`, 28 EpRYl - B`vyN mu@K mMqrI
pRk`S isMG b`dl qy sRomxI kmytI dy pRD`n jgdyv isMG qlvMfI ny ikh` hY ik kmytI
cox~ imQy smyN qy hI hoxgIE~ pr ienH~ cox~ dy imQy smyN qy hox dI sMB`vn` nhIN
hY | Ejy qk sRomxI kmytI cox~ leI votr dI aumr 21 s`l qoN Gt` ky 18 s`l krn
b`ry notIiPkysn vI nhI hoieE` | soRmxI kmytI dy jnrl h`aus dIE~ cox~ ipClI v`r
EkqUbr 1996 ivc hoeIE~ sn | is@K gurduE`r` EYkt Enus`r ieh cox~ pMj s`l~ dy
vkPy hoxIE~ jrUrI hn | 30 m`rc nUM kmytI dI bjt mIitMg ivc PYsl` kIq` igE` sI
ik votr dI aumr 21 s`l qoN Gt` ky 18 s`l kr id@qI j`vy qy sihjD`rIE~ qoN vot d`
h@k Koh ilE` j`vy | gurduE`r` cox kimSn dy cIP kimSnr jsits hrbMs isMG
(irt`ierf) ny sMprk krn qy d@isE` ik aunH~ nUM sRomxI kmytI d` votr ilst~ dI
soD b`ry p`s kIq` mq` nhIN imilE` qy sRomxI kmytI cox~ leI is@K gurduE`r` EYkt
ivc soD hoxI vI jrUrI hY | s: b`dl qy j: qlvMfI ny d`Ev` kIq` hY ik sRomxI cox~
im@Qy smyN EkqUbr ivc Ev@S ho j`xgIE~ | iesy dor`n Ek`lI dl (E) dy jnrl sk@qr
pRo: jgmohn isMG ny E@j sRI b`dl qy sRI qlvMfI qy is@K B`eIc`ry nUM guMmr`h krn
d` doS lg`auNdIE~ ikh` ik auh votr sUcIE~ dI soD krv`aux qoN ibn~ hI cox~ b`ry
ibE`n d`g rhy hn | auDr isE`sI m`ihr~ d` kihx` hY ik mu@K mMqrI b`dl ieh nhI
c`huMdy ik sihjD`rIE~ qy sRomxI kmytI dIE~ cox~ leI vot d` h@k KoihE` j`vy,
iesy leI auh c`huMdy hn ik ieh ivD`n sB` cox~ qoN ip@CoN hI krv`eIE~ j`x | dUjy
p`sy B`jp` qy E`r.EYs.EYs. lIfriSp vI sRomxI kmytI cox~ ivc sihjD`rIE~ qoN vot
d` h@k Kohx dy ivruD hn |   </p>

<p>ਕਿਸਾਨਾਂ ਨੰੂ ਸਬਸਿਡੀਆਂ ਦੇਣਾ ਔਖਾ ਕੰਮ :ਢੀਡਸਾ
</p>

<p>    luiDE`x` 28 ERpYl kydrI K`d qy rs`ieq mqrI suKdyv isMG FIfs` ny
ikh` ik ivSv vp`r smJoqy qo dys dI iks`nI nUM bc`aux leI srk`r is@Dy qor qy
sbisfIE~ nhI dy skdI | ieQy p@qrk`r~ dor`n g@lb`q dor`n sRI FINfs` ny ikh` ik
b`hrly dys B`vyN iks`n~ nUM pMj`h PIsdI qk sbisfIE~ vI dy rhy hn pr B`rq ivc
Eijh` sMBv nhI hY ikauik EmrIk` vrgy mulk~ ivc KyqI krdy lok~ dI igxqI bhuq G@t
hY jdik B`rq q~ hY hI KyqI pRD`n dys | k~grs dI kOmI prD`n sonIE` g~DI dI sKq
E`locn` kridE~ aunH~ ik auh ibn` vj`h sMsd ivc Etl ibh`rI v`jp`eI dy gl pY geI
| sRI v`jp`eI dI ihm`ieq ivc bolidE~ auh ieh vI kih gey ik sRI mqI sonIE` g~DI
ivdysI EOrq hY aus nUM B`rqI sMsikRq d` pq` hI nhI hY | vrxnwog hY ik soRmxI
Ek`lI dl ny v`jp`eI srk`r nUM ibn~ Srq ihm`ieq id@qI hoeI hY Eqy ies auqy pMj`b
dIE~ h@kI mMg~ kyNdr kol n` auT`aux dy doS vI lgdy rhy hn | ies qoN pihl~ kOmI
Kyl~ sbMDI E`rgyn`eIijMg kmytI dI pihlI mIitMg dor`n kyNdrI mMqrI ny d@isE` ik
kuJ idn~ ivc hI luiDE`x` ivKy Kyf~ dy pRbMD leI mu@K dPqr sQ`ipq kr id@q`
j`vyg` | aunH~ ikh` ik fI.ky. tMfn nUM kOmI Kyf~ d` f`ierYktr lg`ieE` igE` hY |
Kyf~ b`ry kuE`lIiPkysn vI kmytI vI CyqI bx`eI j`vygI | aunH` ikh` ik Kyf~ sqMbr
qk krv` id@qIE~ j`xgIE~ Kyf~ sbMDI pUr` pRbMD 15 jUn qk mukMml kr ley j`xgy |
Kyf~ ivc v@K v@K 28 Kyf~ s`ml kIqIE~ j` rhIE~ hn ijn~ Sihr~ ivc muk`bly krv`ey
j`xy hn aunH~ ivc luiDE`x`, jlMDr, pitE`l`, E`nMdpur s`ihb Eqy moh`lI s`ml hn |
ienH~ Kyf~ d` audG`tn luiDE`x` ivc r`StrpqI 16 sqMbr nUM krngy |   </p>

<p>ਸਿਧਾਂਤ
ਨੂੰ ਸਮਰਪਿਤ ਅਕਾਲੀ ਦਲ ਇਕੱਠੇ ਹੋ ਗਏ ਹਨ - ਟੌਹੜਾ </p>



<p>ਟੋਰਾਂਟੋ,
28 ਅਪ੍ਰੈਲ - ਜਿਹੜੇ ਅਕਾਲੀ ਦਲ ਸਿਧਾਂਤ ਨੂੰ ਸਮਰਪਿਤ ਹਨ ਅਤੇ ਸ੍ਰੀ ਅਕਾਲ ਤਖਤ ਸਾਹਿਬ ਦੀ
ਸਰਵਉਚਤਾ ਨੂੰ ਮੰਨਦੇ ਹਨ ਉਹ ਹੁਣ ਇਕੱਠੇ  ho
gey hn | ieh sbd sRomxI gurduE`r` pRbMDk kmytI dy s`bk` pRD`n, mYNbr r`j sB`
Eqy E`l ieMfIE` sRomxI dl dy pRD`n j: gurcrn isMG tOhV` ny ie@k mul`k`q dOr`n
khy | j: tohV` ny ikh` ik 4 Ek`lI dl sMq sm`j dy pRD`n b`b` srbjoq isMG bydI dy
wqn~ sdk` iek ho gey hn ijs d` EYl`n myry v`ps pMj`b j`x qy kr id@q` j`vyg` |
ieh eyk` k`PI ivc`r~ mgroN kIq` igE` hY ijs dy nqIjy vI E`auxy vI surU ho gey
hn | aunH~ s@q smuMdroN p`r E`ky vI mu@K mMqrI pRk`S isMG b`dl q vrHidE~ ikh`
ik auh E@j bI.jy.pI. , E`r.EYs. EYs. qy iSv sYn` dI k`TpuqlI bx ky rih gey hn |
jQyd`r tohV` ny  ieh vI ikh` sI ik jdo
Ek`lI dl d` r`j E`ieE` sI q` s: b`dl ny Ek`n ikq` sI ik aun` puils EPsr~ ivru@D
k`rv`eI kIqI j`vgI ijn` ny glq kMm ikqy | jQyd`r tOhV` ny ikh` ik ivdys~ ivc vs
rhy is@K` dI k`lI sUcI pihl` qo Gtn dI bj`ey vDI hY |E@j b`dl prIv`r s@q` ivc
BeIv`l bx` ilE` hY |jQyd`r tOhV` EMdrlIE~ g@l~ dsidE` ikh` ik mY s:b`dl dy
ruJyivE~ nMU m@uK r@Kdy hoey aus vkq ieh ikh` ik s:suKdyv isMG b`dl nMU dl dy
k`rjk`rI pRD`n bx` dyx` c`hd` hY qy ies gl qoN mYnMU p`rtI qo l~By kr idq`
igE`| s:bdl n`l eyk` krx b`ry pu@Cy j`x qy aun` ikh` ik b`dl Eqy aus dI prtI sRI
Ek`l qKq j` ky E`pxI Bu@l bKs` lvy qy BeI rxjIq isMG nMU Ek`l qKq s`ihb d`
jQyd`r mMn lYx q` mY auhn` n`l iPr iml j`v~g` |   </p>

<p>ਮਾਨ
ਵਲੋਂ ਅਕਾਲੀ ਭਾਜਪਾ ਗਠਜੋੜ ਤੇ ਕਾਂਗਰਸ ਨੂੰ ਹਰਾਉਣ ਲਈ ਤੀਸਰੇ ਮੋਰਚੇ ਦੀ ਮਜਬੂਤੀ ਤੇ ਜੋਰ</p>

<p>          Pgv`V`,
28 EpRYl - Ek`lI dl (E) dy pRD`n s: ismrnjIq isMG m`n ny EglIE~ ivD`n sB` cox~
ivc sRomxI Ek`lI dl B`jp` gTjoV Eqy k~grs nUM hr`aux leI qIsry morcy nUM mjbUq
krn dI loV auqy jor id@q` hY | s: m`n ny E@j ie@Qy gurduE`r` CyvIN p`qs`hI
hdIE`b`d ivKy p@qrk`r~ n`l g@lb`q kridE~ ikh` ik srb ihMd sRomxI Ek`lI dl dy
pRD`n gurcrn isMG tohV` dy EmrIk` qoN v`ps prqx ip@CoN qIsr` mjbUq morc` giTq
kIq` j`vyg` | aunH~ qihlky mu@dy qy sMsd dI k`rv`eI rokx leI k~grs dI inKyDI
kIqI | s: m`n ny cotI dy K`VkU zPrv`l dI igRPq`rI nUM sp@St E`qm-smrpx d@isE` |
jdoN pMj`b ivc E@qv`d muV pYd` hox dI sMB`vn` b`ry pu@iCE` q~ m`n ny ikh` ik
ies b`ry mu@K mMqrI pRk`S isMG b`dl ijE`d` j`xk`rI r@Kdy hn ikauNik auh Eksr hI
aunH~ n`l m`ry gey K`VkUE~ dy Bog sm`roh~ qy j~dy rhy hn | B`rq bMgl`dys srh@d
qy 16 bI.EYs.EYP. dy jv`n~ dy iGnOxy kql b`ry pRSn d` auqr idMidE~ s: m`n ny
dos l`ieE` ik kyNdr srk`r dlyrI n`l sm@isE~ nUM h@l nhI kr skI | aunH~ ies
mMdB`gI Gtn` leI kyNdrI gRih mMqrI EYl.ky. Efv`nI qoN qurMq EsqIPy dI mMg kIqI
| s: m`n ny E@j l`auNidE~ ikh` ik bMg`l dI K`VI ivc h`l hI ivc p`iksq`n Eqy
 bMgl`dys vloN v@fy p@Dr qy kIqIE~ gupq
s~JIE~ nyvI msk~ ip@Cy cIn d` h@Q hY | aunH~ ikh` ik kOmI jmhUrI gTjoV srk`r dI
E`pxy guE~FI dys~ n`l dosq`n` sbMD k`iem krn leI ivdys nIqI ivc n`k`m rihx
ip@Cy ies Kyqr ivc B`rq ivroDI gu@t auBr irh` hY | ieQoN qk ik jykr sRIlMk`
q`iml sm@isE` ivc auliJE` huMd` q~ auh vI ienH~ msk~ ivc jrUr s`iml huMd` |
aun~H ikh` ik EYm.fI.ey. srk`r ny nyp`l smyq dys dIE~ srh@d~ n`l lgdy s`ry dys~
n`l sbMD ivg`V ley hn  Eqy p`iksq`n dIE~
nyvI iksqIE~ d` msk~ leI imE`nm`r vl vDx` r`StrI sur@iKE` leI icMq` d` ivS` hY
| pr ieh sB kuJ svrgI pRD`n mMqrI ieMdr` g~DI vloN 1971 ivc p`iksq`n nUM doP`V
krn d` is@t` hY | </p>

<p>ਕਣਕ ਨੂੰ ਅੱਗ ਦਾ ਸਿਲਸਿਲਾ
ਜਾਰੀ</p>

<p>ਜਲੰਧਰ : ਪੰਜਾਬ 'ਚ ਪਿਛਲੇ ਕੁਝ ਦਿਨਾਂ ਤੋਂ ਕਣਕ ਦੀ ਫਸਲ ਨੂੰ ਅੱਗ ਲੱਗਣ ਦੀਆਂ
ਘਟਨਾਵਾਂ ਹਰ ਰੋਜ਼ ਵਾਪਰਨ ਕਰਕੇ ਕਿਸਾਨਾਂ ਦਾ ਭਾਰੀ ਨੁਕਸਾਨ ਹੋ ਰਿਹਾ ਹੈ। ਪ੍ਰਾਪਤ ਹੋ ਰਹੀਆਂ
ਰਿਪੋਰਟਾਂ ਤੋ ਪਤਾ ਲੱਗਦਾ ਹੈ ਕਿ ਇਹ ਘਟਨਾਵਾਂ ਜ਼ਿਆਦਾਤਰ ਬਿਜਲੀ ਦੀਆਂ ਤਾਰਾਂ ਜਾ ਟਰਾਂਸਫਾਰਮਰ 'ਚੋ ਨਿਕਲਦੇ ਚੰਗਿਆੜਿਆ ਕਾਰਨ ਵਾਪਰ
ਰਹੀਆਂ ਹਨ।, ਜਿਸ ਤੋਂ ਸਾਨੂੰ ਬਿਜਲੀ ਬੋਰਡ ਵਿਭਾਗ ਦੀ ਕਾਰਗੁਜ਼ਾਰੀ ਦਾ ਪਤਾ ਲੱਗਦਾ ਹੈ । ਅੱਜ ਵੀ
ਸੂਬੇ'ਚ ਕਈ ਏਕੜ ਕਣਕ
ਦੀ ਖੜੀ ਫਸਲ ਸੜਨ ਦੀਆਂ ਰਿਪੋਰਟਾਂ ਪ੍ਰਾਪਤ ਹੋਈਆਂ ਹਨ।</p>

<p>ਨਜ਼ਦੀਕੀ ਪਿੰਡ ਕੰਗਰੌੜ (ਨਵਾਂ ਸ਼ਹਿਰ) ਵਿਖੇ ਜਗਜੀਤ ਸਿੰਘ ਪੁੱਤਰ
ਮੋਹਣ ਸਿੰਘ ਦੀ ਤਿੰਨ ਕਿੱਲੇ ਕਣਕ ਦੀ ਫਸਲ ਬਿਜਲੀ ਦੇ ਇੱਕ ਟਰਾਂਸਫਾਰਮਰ ਤੋਂ ਨਿਕਲੀਆਂ
ਚੰਗਿਆੜੀਆਂ ਕਾਰਨ ਅੱਗ ਲੱਗ ਕੇ ਸੁਆਹ ਹੋ ਗਈ। ਅੱਜ 11 ਵਕੇ ਦੇ ਕਰੀਬ ਬਿਜਲੀ ਬੰਦ ਹੋਣ
ਪਿੱਛੋਂ ਜਦ ਬਿਜਲੀ ਆਈ ਤਾਂ ਕਣਕ ਦੇ ਖੇਤਾਂ ਵਿੱਚ ਬਿਜਲੀ ਵਿਭਾਗ ਦੇ ਲੱਗੇ ਟਰਾਂਸਫਾਰਮਰ ਤੋਂ
ਚੰਗਿਆੜੀਆਂ ਡਿੱਗਣ ਕਾਰਨ ਪਹਿਲਾਂ ਅੱਗ ਇੱਕ ਖੇਤ ਨੂੰ ਲੱਗੀ ਅਤੇ ਬਾਅਦ ਵਿੱਚ ਤੇਜ਼ੀ ਨਾਲ ਫੈਲਦੀ
ਹੋਈ ਅੱਗ ਨੇ ਉਪਰੋਕਤ ਕਿਸਾਨ ਦੀ ਕਣਕ ਦੀ ਫਸਲ ਤਿੰਨ ਏਕੜ ਨੂੰ ਲਪੇਟ ਵਿੱਚ ਲੈ ਲਿਆ। ਅੱਗ
ਨੂੰ ਦੇਖਦਿਆਂ ਹੀ ਪਿੰਡ ਦੇ ਲੋਕਾਂ ਨੇ ਇੱਕਠੇ ਹੋ ਕੇ ਅੱਗ 'ਤੇ ਕਾਬੂ ਪਾ ਲਿਆ ਤੇ ਹੋਰ ਖੇਤਾਂ ਨੰੂ ਅੱਗ ਲੱਗਣੋਂ ਬਚਾਅ
ਲਿਆ। ਸੋਸ਼ਲ ਡੈਮੋਕਰੇਟਿਕ ਪਾਰਟੀ ਦੇ ਪ੍ਰਧਾਨ ਜੈ ਗੋਪਾਲ ਧੀਮਾਨ ਨੇ ਕਿਹਾ ਕਿ ਪੰਜਾਬ ਭਰ ਵਿੱਚ
ਅੱਗ ਲੱਗਣ ਕਾਰਨ ਹਜ਼ਾਰਾਂ ਏਕੜ ਕਣਕ ਸੜ ਕੇ ਸੁਆਹ ਹੋ ਗਈ। ਬਹੁਤੇ ਕਿਸਾਨਾਂ ਦੇ ਪੱਲੇ ਇੱਕ ਦਾਣਾ
ਵੀ ਨਹੀਂ ਪਿਆ। ਕਰਜ਼ਾ ਲੈ ਕੇ ਅਤੇ ਮਾਮਲੇ 'ਤੇ ਖੇਤ ਲੈ ਕੇ ਖੇਤੀ ਕਰਨ ਵਾਲੇ ਕਿਸਾਨਾਂ 'ਤੇ ਜੋ ਕਹਿਰ ਪਿਆ ਹੈ, ਉਹ ਕਿਸਾਨਾਂ ਲਈ ਅਸਹਿ ਹੈ। ਇਸ
ਪਾਸੇ ਸਰਕਾਰ ਦੀਆਂ ਕਿਸਾਨਾਂ ਮਾਰੂ ਨੀਤੀਆਂ ਅਤੇ ਦੂਜੇ ਪਾਸੇ ਮੌਸਮ ਦੀ ਖਰਾਬੀ ਤੇ ਅੱਗ ਲੱਗਣ ਦੀਆਂ
ਘਟਨਾਵਾਂ ਕਾਰਨ ਕਿਸਾਨਾਂ ਦਾ ਦੀਵਾਲਾ ਨਿਕਲ ਗਿਆ ਹੈ, ਪ੍ਰੰਤੂ ਆਪਣੇ ਆਪ ਨੂੰ ਕਿਸਾਨ ਹਿਤੈਸ਼ੀ ਅਖਵਾਉਣ
ਵਾਲੀ ਸਰਕਾਰ ਨੇ ਹਾਲੇ ਤੱਕ ਪੀੜਤ ਕਿਸਾਨ ਹਿਤੈਸ਼ੀ ਅਖਵਾਉਣ ਵਾਲੀ ਸਰਕਾਰ ਨੇ ਹਾਲੇ ਤੱਕ ਪੀੜਤ
ਕਿਸਾਨਾਂ ਨੂੰ ਮੁਆਵਜ਼ਾ ਦੇਣ ਦਾ ਐਲਾਨ ਵੀ ਨਹੀਂ ਕੀਤਾ। ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਹਰ ਰੋਜ਼ ਅੱਗ ਲੱਗਣ
ਦੀਆਂ ਘਟਨਾਵਾਂ ਦੀਆਂ ਖਬਰਾਂ ਪ੍ਰਤੱਖ ਕਾਰਨ ਦੱਸਦੇ ਹੋਏ ਛਪਦੀਆਂ ਹਨ, ਜਿਨ੍ਹਾਂ ਵਿੱਚ ਅੱਗ ਲੱਗਣ ਦਾ
ਕਾਰਨ ਬਿਜਲੀ ਬੋਰਡ ਦੀਆਂ ਮਾੜੀਆਂ ਸੇਵਾਵਾਂ ਦਰਸਾਇਆ ਜਾਂਦਾ ਹੈ, ਪ੍ਰੰਤੂ ਸਰਕਾਰ ਇਸ ਸਬੰਧ ਵਿੱਚ
ਚੁੱਪ ਵੱਟੀ ਬੈਠੀ ਹੈ। ਉਨ੍ਹਾਂ ਦੋਸ਼ ਲਾਉਂਦਿਆ ਕਿਹਾ ਕਿ ਬਿਜਲੀ ਦੀਆਂ ਨੰਗੀਆਂ ਤਾਰਾਂ, ਢਿੱਲੀਆਂ
ਤੇ ਟੱੁਟੀਆਂ ਹੋਈਆਂ ਸਰਵਿਸ ਲਾਈਨਾਂ ਖੇਤਾਂ ਵਿਚਲੇ ਟਰਾਂਸਫਾਰਮਰ, ਘਟੀਆ ਸਾਜ਼ੋ ਸਮਾਨ ਅਤੇ
ਅਧਿਕਾਰੀਆਂ ਦੀ ਅਣਗਹਿਲੀ ਹੀ ਅੱਗ ਲੱਗਣ ਦੀਆਂ ਘਟਨਾਵਾਂ ਲਈ ਜ਼ਿੰਮੇਵਾਰ ਹਨ। ਸ਼੍ਰੀ ਧੀਮਾਨ ਨੇ
ਸਰਕਾਰ ਤੋਂ ਮੰਗ ਕੀਤੀ ਹੈ ਕਿ ਫਸਲਾਂ ਨੂੰ ਅੱਗ ਲੱਗਣ ਦੀਆਂ ਘਟਨਾਵਾਂ ਦੀ ਜਾਂਚ ਕਰਵਾਉਣ ਲਈ ਵਿਸ਼ੇਸ਼
ਕਮਿਸ਼ਨ ਨਿਯੁਕਤ ਕੀਤਾ ਜਾਵੇ ਅਤੇ ਬਿਜਲੀ ਬੋਰਡ ਨੂੰ ਇਸ ਸਬੰਧੀ ਦੋਸ਼ੀ ਮੰਨਦਿਆਂ ਸਖਤ ਕਾਰਵਾਈ
ਕੀਤੀ ਜਾਵੇ।</p>

<p>ਹਾਈ ਕਮਾਨ ਅਮਰਿੰਦਰ ਸਿੰਘ
ਨੂੰ ਬਦਲਣ ਲਈ ਰਾਜ਼ੀ ?</p>

<p>ਨਥਾਣਾ :- ਮਜੀਠਾ, ਸੁਨਾਮ ਅਤੇ ਨਵਾਂ ਸ਼ਹਿਰ ਵਿਧਾਨਸਭਾ ਹਲਕਿਆਂ
ਦੀਆਂ ਉਪ ਚੋਣਾਂ ਵਿਚ ਹਾਰ ਪਿੱਛੋਂ ਕਾਂਗਰਸ ਨੂੰ ਨਮੋਸ਼ੀ ਤਾਂ ਹੋਈ ਹੀ ਸੀ, ਪੰਜਾਬ ਦੀ ਇਕਾਈ ਦੀ
ਅੰਦਰੂਨੀ ਧੜੇਬੰਦੀ ਵੀ ਵਧੀ ਹੈ। ਲੁਧਿਆਣਾ ਰੈਲੀ'ਚ ਹੋਈ ਖਿੱਚ ਧੂਹ ਕਾਰਨ ਇਹ ਮਸਲਾ ਵਧੇਰੇ ਵਿਗੜ ਗਿਆ ਸੀ।
ਓਧਰ ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਨੇ ਚੋਣਾਂ ਨੂੰ ਮੁੱਖ ਰੱਖ ਕੇ ਮੁਹਿੰਮ ਵਿੱਢੀ ਹੋਈ ਹੈ। ਕਾਂਗਰਸ ਦੀ ਕੌਮੀ
ਪ੍ਰਧਾਨ ਸ਼੍ਰੀਮਤੀ ਸੋਨੀਆ ਗਾਂਧੀ ਅਤੇ ਪਾਰਟੀ ਦੇ ਪੰਜਾਬ ਮਾਮਲਿਆਂ ਦੇ ਇੰਚਾਰਜ ਮੋਤੀ ਲਾਲ ਵੋਰਾ
ਭਾਵੇਂ ਪੰਜਾਬ ਪ੍ਰਦੇਸ਼ ਕਮੇਟੀ'ਚ ਕਿਸੇ ਤਬਦੀਲੀ ਲਈ ਤਿਆਰ ਨਹੀਂ ਸਨ ਪ੍ਰੰਤੂ ਹੁਣ ਪਾਰਟੀ ਦੇ ਖਰਾਬ ਹੋ ਰਹੇ ਅਕਸ ਨੰੂ
ਦੇਖਦਿਆਂ ਪਾਰਟੀ ਹਾਈ ਕਮਾਨ ਇਸ ਮਸਲੇ'ਤੇ ਨਜ਼ਰਸਾਨੀ ਕਰਨ ਲਈ ਤਿਆਰ ਹੈ।</p>

<p>ਭਾਵੇਂ ਪਿਛਲੇ ਸਮੇਂ ਦੌਰਾਨ ਪੰਜਾਬ ਕਮੇਟੀ ਦੀ ਪ੍ਰਧਾਨਗੀ ਵਾਸਤੇ
ਸਾਬਕਾ ਮੁੱਖ ਮੰਤਰੀ ਰਾਜਿੰਦਰ ਕੌਰ ਭੱਠਲ ਅਤੇ ਸੰਸਦ ਮੈਂਬਰ ਜਗਮੀਤ ਸਿੰਘ ਬਰਾੜ ਤਕੜੇ ਦਾਅਵੇਦਾਰ
ਰਹੇ ਹਨ ਪਰ ਇਸ ਅਹੁਦੇ ਲਈ ਪਾਰਟੀ ਦੇ ਸੀਨੀਅਰ ਆਗੂ ਬਲਰਾਮ ਜਾਖੜ ਫਿਰੋਜ਼ਪੁਰ ਜ਼ਿਲੇ ਦੇ ਪਿੰਡ
ਪੰਜਕੋਸੀ ਦੇ ਸਰਪੰਚ ਤੋਂ ਲੈ ਕੇ ਲੋਕ ਸਭਾ ਦੇ ਸਪੀਕਰ ਅਤੇ ਕੇਂਦਰੀ ਖੇਤੀਬਾੜੀ ਮੰਤਰੀ ਦੇ ਉਚ
ਅਹੁਦਿਆਂ 'ਤੇ ਰਹਿ ਚੁੱਕੇ
ਹਨ। ਉਨ੍ਹਾਂ ਦਾ ਕਿਸਾਨੀ ਹਲਕਿਆਂ 'ਚ ਵੀ ਆਧਾਰ ਮੰਨਿਆ ਜਾਂਦਾ ਹੈ। ਉਧਰ ਆਧੁਨਿਕ ਢੰਗ ਤਰੀਕਿਆਂ ਨਾਲ ਖੇਤੀ ਕਰਨ ਵਾਲੇ
ਪੰਜਾਬ ਦੇ ਚੋਣਵੇਂ ਫਾਰਮਾਂ ਦੇ ਮਾਲਕ ਕਿਸਾਨ, ਜੋ ਹੁਣ ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਦੇ ਨਾਲ ਹਨ, ਸ਼੍ਰੀ ਜਾਖੜ
ਦੇ ਪੰਜਕੋਸੀ ਸਥਿਤ ਫਾਰਮ ਤੋਂ ਸੇਧਾਂ ਲੈਂਦੇ ਰਹੇ ਹਨ। </p>

<p> </p>






</body></text></cesDoc>