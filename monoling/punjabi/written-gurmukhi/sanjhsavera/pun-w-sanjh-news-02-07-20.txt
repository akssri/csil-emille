<cesDoc id="pun-w-sanjh-news-02-07-20" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-02-07-20.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 02-07-20</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>02-07-20</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਸਾਂਝ
ਸਵੇਰਾ ਦੀ ਵੈਬ ਸਾਈਟ 'ਤੇ ਇੱਕ ਦਿਨ ਵਿੱਚ 10 ਹਜ਼ਾਰ ਤੋਂ ਵੱਧ ਹਿੱਟਾਂ ਆਉਣੀਆਂ ਸ਼ੁਰੂ ਹੋਈਆਂ</p>

<p>ਬਰੈਮਪਟਨ: ਸਾਂਝ
ਸਵੇਰਾ ਦੀ ਇੰਟਰਨੈਟ ਉਪਰ ਲਾਂਚ ਕੀਤੀ ਵੈਬ ਸਾਈਟ ਨੰੂ ਤਕਰੀਬਨ ਦੋ ਸਾਲ ਤੋਂ ਉਪਰ ਦਾ ਵਕਤ ਹੋ
ਗਿਆ ਹੈ। ਇਹ ਵੈਬ ਸਾਈਟ ਅਜਿਹੀ ਇੱਕ ਪਹਿਲੀ ਵੈਬ ਸਾਈਟ ਹੈ ਜੋ ਹਰ ਰੋਜ਼ ਪੰਜਾਬ ਦੀਆਂ ਖਬਰਾਂ
ਪ੍ਰਕਾਸ਼ਤ ਕਰਦੀ ਹੈ। ਇਸ
ਹਫਤੇ ਇਸ ਵੈਬ ਸਾਈਟ ਦੇ ਪਾਠਕਾਂ ਦੀ ਗਿਣਤੀ ਇੱਕ ਦਿਹਾੜੀ ਵਿੱਚ 10 ਹਜ਼ਾਰ ਤੋਂ ਵੱਧ ਚੁੱਕੀ ਹੈ।</p>

<p>ਇਸ ਹਫਤੇ ਦੀਆਂ
ਹਿੱਟਸ ਦਾ ਵੇਰਵਾ ਇਸ ਪ੍ਰਕਾਰ ਹੈ ਜੁਲਾਈ 14 ਐਤਵਾਰ 7477, ਸੋਮਵਾਰ 8993, ਮੰਗਲਵਾਰ 9431,
ਬੁੱਧਵਾਰ 9715, ਵੀਰਵਾਰ 10699, ਸ਼ੁਕਰਵਾਰ 10608 ਹਨ। ਸਾਂਝ ਸਵੇਰਾ ਇੰਮੀਗਰੇਸ਼ਨ, ਵਕੀਲ ਅਤੇ
ਹੋਰਨਾਂ ਸੇਵਾਵਾਂ ਮੁਹੱਈਆਂ ਕਰਨ ਵਾਲੇ ਅਦਾਰਿਆਂ ਨੰੂ ਇਸ ਸਾਈਟ ਤੇ ਐਡਵਰਟਾਈਜ਼ ਕਰਨ ਲਈ
ਬੇਨਤੀ ਕਰਦੀ ਹੈ ਤਾਂ ਕਿ ਜਿਥੇ ਇਸ ਸਾਈਟ ਰਾਹੀਂ ਤੁਸੀਂ ਆਪਣੇ ਅਦਾਰੇ ਦਾ ਦੁਨੀਆਂ ਭਰ ਦੇ
ਪੰਜਾਬੀਆਂ ਤੱਕ ਸੁਨੇਹਾ ਪਹੁੰਚਾ ਸਕੋਂਗੇ ਉਥੇ ਤੁਸੀਂ ਇਸ ਸਾਈਟ ਨੰੂ ਚੱਲਦੇ ਰੱਖਣ ਲਈ ਵੀ
ਮਦਦ ਕਰੋਂਗੇ।</p>

<p>ਸੋਕੇ
ਦੇ ਹਾਲਾਤ 'ਚ ਕੈਪਟਨ ਸਰਕਾਰ ਕੁੱਝ ਕਰੇ</p>

<p>ਚੰਡੀਗੜ੍ਹ, 20
ਜੁਲਾਈ (ਸਾਂਝ ਸਵੇਰਾ) : ਸੀ.ਪੀ.ਆਈ. ਦੇ ਸਕੱਤਰ ਡਾ. ਜੋਗਿੰਦਰ ਦਿਆਲ ਨੇ ਮੰਗ ਕੀਤੀ ਹੈ ਕਿ
ਪੰਜਾਬ ਵਿਚ ਸੋਕੇ ਦੀ ਸਥਿਤੀ ਦੇ ਟਾਕਰੇ ਲਈ ਕੇਂਦਰ ਤੋਂ ਰਾਹਤ ਲੈਣ ਵਾਸਤੇ ਮੁੱਖ ਮੰਤਰੀ ਕੈਪਟਨ
ਅਮਰਿੰਦਰ ਸਿੰਘ ਸਰਬ-ਪਾਰਟੀ ਵਫਦ ਲੈ ਕੇ ਦਿੱਲੀ ਜਾਣ। ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਇਸ ਕੁਦਰਤੀ ਆਫਤ ਦਾ
ਪਾਰਟੀ ਪੱਧਰ ਤੋਂ ਉਪਰ ਉਠ ਕੇ ਟਾਕਰਾ ਕਰਨਾ ਚਾਹੀਦਾ ਹੈ। </p>

<p>ਵਲਟੋਹਾ
ਵੱਲੋਂ ਅੱਗੇ ਆਤਮ ਸਮੱਰਪਣ</p>

<p>ਚੰਡੀਗੜ੍ਹ, 20
ਜੁਲਾਈ (ਸਾਂਝ ਸਵੇਰਾ) : ਪੰਜਾਬ ਅਧੀਨ ਸੇਵਾਵਾਂ ਚੋਣ ਬੋਰਡ ਦੇ ਸਾਬਕਾ ਮੈਂਬਰ ਸ੍ਰੀ ਵਿਰਸਾ ਸਿੰਘ
ਵਲਟੋਹਾ ਨੇ ਆਖਰਕਾਰ ਅੱਜ ਪੰਜਾਬ ਚੌਕਸੀ ਬਿਊਰੋ ਸਾਹਮਣੇ ਚੰਡੀਗੜ੍ਹ ਵਿਚ ਆਤਮ ਸਮੱਰਪਣ ਕਰ
ਦਿੱਤਾ। ਭਰੋਸੇਯੋਗ ਸੂਤਰਾਂ ਦਾ ਕਹਿਣਾ ਹੈ ਕਿ ਸ਼੍ਰੀ ਵਲਟੋਹਾ ਨੇ ਅਧੀਨ ਸੇਵਾਵਾਂ ਚੋਣ ਬੋਰਡ ਦਾ
ਮੈਂਬਰ ਬਣਨ ਪਿੱਛੋਂ ਆਪਣੀ ਆਮਦਨੀ ਨਾਲੋਂ ਵੱਧ ਜ਼ਮੀਨ ਜਾਇਦਾਦ ਬਣਾਈ ਹੈ। </p>

<p>ਨਗਰ
ਨਿਗਮ ਦਾ ਇੰਸਪੈਕਟਰ ਅਤੇ ਸੇਵਾਦਾਰ ਵਿਜੀਲੈਂਸ ਵੱਲੋਂ ਗ੍ਰਿਫਤਾਰ</p>

<p>ਲੁਧਿਆਣਾ, 20
ਜੁਲਾਈ (ਸਾਂਝ ਸਵੇਰਾ) : ਵਿਜੀਲੈਂਸ ਵਿਭਾਗ ਨੇ ਅੱਜ ਨਗਰ ਨਿਗਮ ਲੁਧਿਆਣਾ ਦੇ ਬਿਲਡਿੰਗ
ਇੰਸਪੈਕਟਰ ਜਸਦੇਵ ਸਿੰਘ ਸੇਖੋਂ ਅਤੇ ਚਪੜਾਸੀ ਸਰਬਜੀਤ ਸਿੰਘ ਨੂੰ 7000 ਰੁਪਏ ਦੀ ਰਿਸ਼ਵਤ
ਲੈਂਦਿਆਂ ਰੰਗੇ ਹੱਥੀਂ ਗ੍ਰਿਫਤਾਰ ਕਰ ਲਿਆ ਹੈ। ਇਹ ਰਿਸ਼ਵਤ ਉਕਤ ਅਧਿਕਾਰੀਆਂ ਨੇ ਸ਼ਿਮਲਾਪੁਰੀ
ਵਾਸੀ ਰਵੀ ਸ਼ਰਮਾ ਪਾਸੋਂ ਪ੍ਰਾਪਤ ਕੀਤੀ ਦੱਸੀ ਜਾਂਦੀ ਹੈ। ਇਹ ਰਿਸ਼ਵਤ ਮਕਾਨ ਦਾ ਨਕਸ਼ਾ ਪਾਸ ਕਰਵਾਉਣ
ਲਈ ਦਿੱਤੀ ਗਈ। </p>

<p>ਬਾਦਲ
ਦੇ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਦੀ ਜਾਂਡ ਜੁਡੀਸ਼ੀਅਲ ਕਮਿਸ਼ਨ ਤੋਂ ਕਰਵਾਵਾਂਗੇ : ਅਮਰਿੰਦਰ</p>

<p>ਜਲੰਧਰ, 20
ਜੁਲਾਈ (ਸਾਂਝ ਸਵੇਰਾ) : ਪੰਜਾਬ ਦੇ ਮੁੱਖ ਮੰਤਰੀ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਨੇ ਅੱਜ ਆਖਿਆ ਹੈ ਕਿ
ਰਾਜ ਸਰਕਾਰ ਪੰਜਾਬ ਦੇ ਸਾਬਕਾ ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਖਿਲਾਫ ਲੱਗੇ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਦੇ
ਦੋਸ਼ਾਂ ਦੀ ਜਾਂਚ ਲਈ ਜੁਡੀਸ਼ੀਅਲ ਕਮਿਸ਼ਨ ਕਾਇਮ ਕਰਨ ਲਈ ਵਚਨਬੱਧ ਹੈ। ਉਨ੍ਹਾਂ ਕਿਹਾ,
"ਜੁਡੀਸ਼ੀਅਲ ਕਮਿਸ਼ਨ ਪਹਿਲਾਂ ਹੀ ਕਾਇਮ ਕੀਤਾ ਜਾ ਚੁੱਕਾ ਹੈ। ਜਸਟਿਸ ਗਰਗ ਵੱਲੋਂ ਇਸ ਕਮਿਸ਼ਨ
ਦੇ ਮੁਖੀ ਵਜੋਂ ਅਸਤੀਫਾ ਦੇਣ ਕਾਰਨ ਅਸੀ ਪੰਜਾਬ ਅਤੇ ਹਰਿਆਣਾ ਦੇ ਹਾਈ ਕੋਰਟ ਨੂੰ ਇਨ੍ਹਾਂ ਦੋਸ਼ਾਂ
ਦੀ ਜਾਂਚ ਲਈ ਕਿਸੇ ਜੱਜ ਨੂੰ ਨਿਯੁਕਤ ਕਰਨ ਦੀ ਬੇਨਤੀ ਕੀਤੀ ਹੈ ਅਤੇ ਛੇਤੀ ਹੀ ਇਸ ਮੰਤਵ ਲਈ
ਕਿਸੇ ਜੱਜ ਦੀ ਨਿਯੁਕਤੀ ਕਰ ਦਿੱਤੀ ਜਾਵੇਗੀ।"</p>

<p>ਡਾਕਟਰ
ਦੀ ਅਣਗਹਿਲੀ ਕਾਰਨ ਔਰਤ ਦੀ ਮੌਤ; ਲੋਕਾਂ ਵੱਲੋਂ ਆਵਾਜਾਈ ਠੱਪ</p>

<p>ਫਤਹਿਗੜ੍ਹ
ਸਾਹਿਬ, 20 ਜੁਲਾਈ (ਸਾਂਝ ਸਵੇਰਾ) : ਸਿਵਲ ਹਸਪਤਾਲ ਫਤਹਿਗੜ੍ਹ ਸਾਹਿਬ ਵਿਚ ਇਕ ਔਰਤ ਵੀਨਾ ਰਾਣੀ
ਪਤਨੀ ਸ਼੍ਰੀ ਨਰੇਸ਼ ਕੁਮਾਰ ਵਾਸੀ ਸਰਹੰਦ ਦੀ ਹੋਈ ਮੌਤ ਨੂੰ ਲੈ ਕੇ ਅੱਜ ਉਸ ਦੇ ਵਾਰਸਾਂ ਅਤੇ ਲੋਕਾਂ
ਨੇ ਜ਼ਬਰਦਸਤ ਨਾਅਰੇਬਾਜ਼ੀ ਕੀਤੀ ਅਤੇ ਕਰੀਬ ਦੋ ਘੰਟੇ ਆਵਾਜਾਈ ਠੱਪ ਕੀਤੀ। ਪ੍ਰਾਪਤ ਸੂਚਨਾ ਅਨੁਸਾਰ
ਸ਼੍ਰੀਮਤੀ ਵੀਨਾ ਰਾਣੀ ਦਾ ਸਿਵਲ ਹਸਪਤਾਲ ਫਤਹਿਗੜ੍ਹ ਸਾਹਿਬ ਵਿਚ ਆਪਰੇਸ਼ਨ ਹੋਇਆ ਸੀ ਅਤੇ ਹਸਪਤਾਲ
ਦੀ ਸੰਬੰਧਤ ਡਾਕਟਰ ਵੱਲੋਂ ਉਸ ਨੂੰ ਪੀ.ਜੀ.ਆਈ. ਚੰਡੀਗੜ੍ਹ ਲਈ ਰੈਫਰ ਕਰ ਦਿੱਤਾ ਗਿਆ ਪ੍ਰੰਤੂ
ਉਹ ਮੌਤ ਦਾ ਸ਼ਿਕਾਰ ਹੋ ਗਈ।</p>

<p> </p>






</body></text></cesDoc>