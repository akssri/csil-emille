<cesDoc id="pun-w-sanjh-news-01-01-05" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-01-01-05.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 01-01-05</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>01-01-05</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਸ਼ੁੱਕਰਵਾਰ, ਜਨਵਰੀ 05, 2001</p>

<p>ਦਲਜੀਤ ਸਿੰਘ ਬਿੱਟੂ ਹੱਤਿਆ ਕੇਸ ਵਿਚੋਂ ਬਰੀ
</p>

<p>ਪਟਿਆਲਾ,
4 ਜਨਵਰੀ - ਪਟਿਆਲਾ ਜ਼ਿਲ੍ਹੇ ਅੰਦਰ ਨਾਭਾ ਵਿਖੇ ਸਖਤ ਸੁਰੱਖਿਆ ਜੇਲ੍ਹ ਵਿਚ ਬਣੀ ਵਿਸ਼ੇਸ਼ ਟਾਡਾ
ਅਦਾਲਤ ਦੇ ਵਿਸ਼ੇਸ਼ ਜੱਜ ਸ਼੍ਰੀ ਜੇ.ਐਸ.ਚਾਵਲਾ ਨੇ ਪ੍ਰਮੁੱਖ ਖਾੜਕੂ ਦਲਜੀਤ ਸਿੰਘ ਬਿੱਟੂ ਨੂੰ
ਪਟਿਆਲਾ ਦੇ ਸਾਬਕਾ ਜ਼ਿਲ੍ਹਾ ਪੁਲਿਸ ਮੁਖੀ ਅਰਵਿੰਦਰ ਸਿੰਘ ਬਰਾੜ ਅਤੇ ਐਸ.ਪੀ. ਆਰ.ਐਸ.ਗਿੱਲ ਦੀ
ਹੱਤਿਆ ਕਰਨ ਦੇ ਦੋਸ਼ ਵਾਲੇ ਕੇਸ ਵਿਚੋਂ ਬਰੀ ਕਰ ਦਿੱਤਾ ਹੈ । ਦੋਵਾਂ ਪੁਲਿਸ ਅਧਿਕਾਰੀਆਂ ਤੇ 14
ਦਸੰਬਰ 1987 ਨੂੰ ਸਥਾਨਿਕ ਐਨ.ਆਈ.ਐਸ. ਵਿਚ ਸਵੇਰ ਵੇਲੇ ਹਮਲਾ ਹੋਇਆ ਸੀ, ਜਿਸ ਵੇਲੇ ਉਹ
ਸੈਰ ਕਰ ਰਹੇ ਸਨ । ਇਨ੍ਹਾਂ ਪੁਲਿਸ ਅਧਿਕਾਰੀਆਂ ਤੇ ਅਸਾਲਟਾਂ ਨਾਲ ਹਮਲਾ ਕੀਤਾ ਗਿਆ ਤੇ ਦੋਵੇਂ
ਅਧਿਕਾਰੀ ਮੌਕੇ ਤੇ ਹੀ ਮਾਰੇ ਗਏ ਸਨ । ਪੁਲਿਸ ਨੇ ਇਥੇ ਥਾਣਾ ਸਿਵਲ ਲਾਈਨ ਵਿਚ ਧਾਰਾ 302, 34
ਆਈ.ਪੀ.ਸੀ. ਅਧੀਨ ਅਤੇ ਆਰਮਜ਼ ਐਕਟ ਅਤੇ 3/4 ਟਾਡਾ ਐਕਟ ਅਧੀਨ ਕੇਸ ਦਰਜ ਕੀਤਾ ਸੀ । ਦਲਜੀਤ
ਸਿੰਘ ਬਿੱਟੂ ਨੂੰ 1996 ਵਿਚ ਸੀ.ਆਈ.ਏ. ਪਟਿਆਲਾ ਵਲੋਂ ਗ੍ਰਿਫਤਾਰ ਕੀਤਾ ਗਿਆ ਸੀ ਅਤੇ ਅਸਾਲਟ
ਤੇ ਮੈਗਜ਼ੀਨ ਵੀ ਉਸਦੇ ਪਿੰਡ ਤੋਂ ਬਰਾਮਦ ਕੀਤੀ ਸੀ । </p>

<p>          aus vyly m`ry gey puils EiDk`rIE~ dy
EMg r@iKEk ny Ed`lq nUM d@isE` sI ik aus ny c`r hml`vr~ ivcoN iek is@K wuvk
dljIq isMG ib@tU nUM pihc`ixE` sI | bc`E p@KI vkIl ny Ed`lq ivc dlIl id@qI ik
hmly qoN pihl~ EMg r@iKEk ny ib@tU nUM kdy nhIN dyiKE` sI | puils kys s`bq nhIN
kr skI qy Ed`lq ny ib@tU nUM brI kr id@q` |   </p>

<p>ਹਥਿਆਰਬੰਦ ਡਕੈਤ ਘਰ ਦੇ 5 ਜੀਆਂ ਨੂੰ ਜ਼ਖਮੀ ਕਰਕੇ
ਨਕਦੀ ਤੇ ਸੋਨੇ ਦੇ ਗਹਿਣੇ ਲੁੱਟ ਕੇ ਫਰਾਰ </p>

<p>ਲੁਧਿਆਣਾ,
4 ਜਨਵਰੀ - ਬੀਤੀ ਅੱਧੀ ਰਾਤ ਪਿੰਡ ਭੱਟੀਆ ਵਿਚ ਅੱਧੀ ਦਰਜਨ ਦੇ ਕਰੀਬ ਹਥਿਆਰਬੰਦ ਡਕੈਤ ਇਕ ਘਰ
ਵਿਚ ਦਾਖਲ ਹੋ ਕੇ ਲੱਖਾਂ ਰੁਪਏ ਦੀ ਨਕਦੀ ਅਤੇ ਸੋਨੇ ਦੇ ਗਹਿਣੇ ਲੁੱਟ ਕੇ ਫਰਾਰ ਹੋ ਗਏ । ਜਾਂਦੇ
ਹੋਏ ਉਹ ਘਰ ਦੇ ਪੰਜ ਜੀਆਂ ਨੂੰ ਜ਼ਖਮੀ ਕਰ ਗਏ । </p>

<p>          Gtn` bIqI r`q krIb 12.30 vjy dy krIb
aus v`prI jdoN ipMf B@tIE` ivc E@DI drjn hiQE`rbMd nk`bpoS fkYq kMD~ t@p ky
svrgI rGbIr isMG ryKI dy Gr d`Kl ho gey | kiQq doSIE~ ny pihl~ mu@K gyt nUM
lohy dIE~ r`f~ m`r ky qoV id@q` | gyt tu@tx qoN dI E`v`z sux ky Gr ivc mOjUd
ivEkqI j`g pey | kiQq doSIE~ vloN Gr ivc d`Kl huMidE~ pihl~ tYlIPon dI q`r k@t
id@qI | jdoN Gr ivc mOjUd aukq mYNbr b`hr inkly q~ kiQq doSIE~ ny SRImqI ryKI,
aus dy pu@qr Eqy pu@qrI qy lohy dIE~ r`f~ n`l hml` kr id@q` ijs n`l auh sKq
zKmI ho gey | ies dOr`n Gr ivc mOjUd nOkr~ auQoN B@jx l@gy q~ lutyirE~ ny aunH~
nUM vI zKmI kr id@q` | </p>

<p>          lutyirE~ vloN aukq mYNbr~ nUM Gr ivc
bxy iek b`QrUm ivc bMd kr id@q` | b`Ed ivc lutyirE~ ny s`ry Gr dI ql`SI leI Eqy
Gr dIE~ Elm`rIE~, l`kr qoV ky aunH~ ivc peI l@K~ rupey dI nkdI Eqy sony dy
gihxy lY ky Pr`r ho gey | lutyry Gr ivc pey tI.vI., vI.sI.E`r., fYk vI n`l lY
gey | pq` l@g` ik f`kU lu@t dOr`n auQy hI GMt` E`r`m n`l bYT ky K~dy-pINdy rhy
| Gr dy mYNbr svyry 6 vjy b`QrUm d` drv`j` qoV ky b`hr E`ey Eqy puils nUM sUicq
kIq` | Gtn` dI sUcn` imldy hI s`ry auc puils EiDk`rI mOky qy phuMcy Eqy aunH~
ny zKmIE~ nUM isvl hspq`l d`Kl krv`ieE` | doSIE~ dy mUMh F@ky hoey sI Eqy aunH~
dy h@Q ivc bMdUk~ sI | puils ny ies sbMDI D`r` 395/25/24/59 EDIn kys drj kr
ilE` hY | </p>

<p>ਕਿਸਾਨ ਜਥੇਬੰਦੀਆਂ ਵਲੋਂ ਅਲਟੀਮੇਟਮ
</p>

<p>ਜਲੰਧਰ, 4
ਜਨਵਰੀ - ਪੰਜ ਕਿਸਾਨ ਜਥੇਬੰਦੀਆਂ ਨੇ ਕਿਸਾਨਾਂ ਦੀਆਂ ਮੰਗਾਂ ਦੇ ਹੱਕ ਵਿਚ ਆਵਾਜ਼ ਬੁਲੰਦ ਕਰਦਿਆਂ
ਸਰਕਾਰ ਨੂੰ 17 ਜਨਵਰੀ ਤਕ ਦਾ ਅਲਟੀਮੇਟਮ ਦਿੱਤਾ ਹੈ । ਕਿਸਾਨ ਜਥੇਬੰਦੀਆਂ ਨੇ ਪੰਜਾਬ ਦੇ ਕਿਸਾਨਾਂ
ਦੀ ਹਿੱਤੂ ਹੋਣ ਦਾ ਦਾਅਵਾ ਕਰਨ ਵਾਲੀ ਸਰਕਾਰ ਨੂੰ ਕਿਹਾ ਹੈ ਕਿ ਜੇ ਕਿਸਾਨਾਂ ਦੇ ਮਸਲੇ ਹੱਲ ਨਾ
ਕੀਤੇ ਗਏ ਤਾਂ 18 ਜਨਵਰੀ ਨੂੰ ਪੰਜਾਬ ਵਿਚ ਧਰਨੇ ਤੇ ਮੁਜਾਹਰੇ ਕੀਤੇ ਜਾਣਗੇ । </p>

<p>          pMj`b
iks`n sB` dy jnrl sk@qr lihMbr isMG q@gV ny ikh` ik kyNdr qy pMj`b srk`r dy
l`irE~ qy hux bhuq` icr ivSv`s nhIN kIq` j` skd` | aunH~ ny ikh` ik iks`n~ dIE~
mMg~ mnv`aux leI Jony dI ivkrI vyly dy Gol v~g hux iks`n muV lMmyN sMGrS leI
iqE`r ho rhy hn | iks`n~ dIE~ mu@K mMg~ ivc gMny d` B`E 120 rupey kuieMtl, Jony
dI ivkrI dy bk`ey dyx`, krz` n` dy skx v`ly iks`n~ dIE~ igRPq`rIE~ rokx` Eqy
E`lUE~ d` G@to-G@t B`E 250 rupey kuieMtl kIqy j`x` S`ml hY | SRI q@gV ny ikh`
ik krzy n`l sbMDq jMg~ qy zor dy leI iks`n hr izlH` p@Dr qy sihk`rI bYNk~ E@gy
Drny m`rngy Eqy bYNk EiDk`rIE~ nUM mMg-p@qr dy ky ifptI kimSnr dy dPqr~ v@l
m`rc krngy | b`kI mMg~ leI ifptI kimSnr nUM mMg p@qr id@qy j`xgy | aunH~ ikh`
ik jy 18 jnvrI dy EYkSn qoN b`Ed vI srk`r ny koeI k`rv`eI n` kIqI q~ pMj iks`n
jQybMdIE~ dI Egv`eI hyT 15 m`rc nUM iks`n cMfIgVH ivc morc` SurU kr dyxgy |
</p>

<p>ਜਰਮਨੀ ਵਿਚਲੀਆਂ ਪੰਥਕ ਜਥੇਬੰਦੀਆਂ ਵਲੋਂ ਜਥੇਦਾਰ
ਤਲਵੰਡੀ ਦੇ ਫੈਸਲਿਆਂ ਦੀ ਹਮਾਇਤ </p>

<p>ਫਰੈਂਕਵਰਟ,
4 ਜਨਵਰੀ - ਜਰਮਨੀ ਦੀਆਂ ਪੰਥਕ ਜਥੇਬੰਦੀਆਂ ਬੱਬਰ ਖਾਲਸਾ ਇੰਟਰਨੈਸ਼ਨਲ, ਇੰਟਰਨੈਸ਼ਨਲ ਸਿੱਖ ਯੂਥ
ਫੈਡਰੇਸ਼ਨ (ਭਾਈ ਦਲਜੀਤ ਸਿੰਘ), ਇੰਟਰਨੈਸ਼ਨਲ ਦਲ ਖਾਲਸਾ, ਇੰਟਰਨੈਸ਼ਨਲ ਸਿੱਖ ਯੂਥ ਫੈਡਰੇਸ਼ਨ
(ਭਾਈ ਲਖਬੀਰ ਸਿੰਘ) ਤੇ ਕਾਮਾਗਾਟਾ ਮਾਰੂ ਦਲ ਦੇ ਮੁਖੀਆਂ ਜਥੇਦਾਰ ਰੇਸ਼ਮ ਸਿੰਘ, ਭਾਈ ਅਵਤਾਰ
ਸਿੰਘ ਹੁੰਦਲ, ਭਾਈ ਜਸਵੀਰ ਸਿੰਘ, ਭਾਈ ਗੁਰਮੀਤ ਸਿੰਘ ਖਨਿਆਣ, ਭਾਈ ਜਗਮੋਹਨ ਸਿੰਘ ਮੰਡ,
ਭਾਈ ਸੁਰਿੰਦਰ ਸਿੰਘ ਭਿੰਡਰ, ਭਾਈ ਲਖਵਿੰਦਰ ਸਿੰਘ ਮੱਲੀ ਤੇ ਭਾਈ ਹਰਮੀਤ ਸਿੰਘ ਭਕਨਾ ਨੇ ਇਥੇ
ਜਾਰੀ ਇਕ ਸਾਂਝੇ ਪ੍ਰੈਸ ਬਿਆਨ ਵਿਚ ਕਿਹਾ ਕਿ ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ
ਜਥੇਦਾਰ ਜਗਦੇਵ ਸਿੰਘ ਤਲਵੰਡੀ ਵਲੋਂ ਪਾਕਿਸਤਾਨ ਵਿਚਲੇ ਗੁਰਧਾਮਾਂ ਦੇ ਦਰਸ਼ਨਾਂ ਲਈ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ
ਵਲੋਂ ਮੁੜ ਜਥੇ ਭੇਜਣੇ ਸ਼ੁਰੂ ਕਰਨ ਤੇ ਗੁਰਦੁਆਰਾ ਚੋਣਾਂ ਲਈ ਸਹਿਜਧਾਰੀਆਂ ਦੀਆਂ ਵੋਟਾਂ ਉਤੇ
ਪਾਬੰਦੀ ਲਾਉਣ ਦੀ ਸ਼ਲਾਘਾ ਕਰਦਿਆਂ ਇਨ੍ਹਾਂ ਨੂੰ ਪੰਥ ਦੀ ਚੜਦੀ ਕਲ੍ਹਾ ਵਾਲੇ ਫੈਸਲੇ ਕਰਾਰ ਦਿੱਤਾ ।
ਇਨ੍ਹਾਂ ਪੰਥਕ ਨੇਤਾਵਾਂ ਨੇ ਜਥੇਦਾਰ ਤਲਵੰਡੀ ਵਲੋਂ ਆਰ.ਐਸ.ਐਸ. ਵਲੋਂ ਕੀਤੇ ਜਾ ਰਹੇ ਸਿੱਖ
ਵਿਰੋਧੀ ਪ੍ਰਚਾਰ ਦਾ ਮੂੰਹ ਤੋੜਵਾਂ ਉਤਰ ਦੇਣ ਵਾਲੇ ਦ੍ਰਿੜ੍ਹ ਇਰਾਦੇ ਦੀ ਪ੍ਰਸ਼ੰਸਾ ਕਰਦਿਆਂ ਕਿਹਾ ਕਿ
ਪੰਥਕ ਜਥੇਬੰਦੀਆਂ ਇਸ ਮਾਮਲੇ ਵਿਚ ਉਨ੍ਹਾਂ ਨੂੰ ਪੂਰਾ ਸਹਿਯੋਗ ਦੇਣਗੀਆਂ । </p>

<p>          ibE`n ivc ikh` igE` hY ik is@K pMQ
jQyd`r qlvMfI qoN bhuq v@fIE~ E`s` r@Kd` hoieE` mMg krd` hY ik qKq s`ihb`n dy
aunH~ jQyd`r~ nUM bdilE` j`vy ijnH~ nUM ipCly smyN dOr`n is@K pMQ dy m`x-siqk`r
nUM B`rI s@t m`rI hY | ienH~ jQyd`r~ ny pMQ pRsqI dI Q~ KudgrzI nUM pihl id@qI
|ienH~ jQybMdIE~ ny mMg kIqI hY ik Drm-wu@D morcy qoN lY ky E@j qk B`rq dIE~
nrkI jylH~ ivc f@ky inrdoS is@K nOjv`n~ nUM irh`E kr`aux qy aunH~ dy pIVq
pirv`r~ dI mdd krn leI SRomxI gurduE`r` pRbMDk kmytI E`px Prz pUr` kry | pMQk
jQybMdIE~ dy muKIE~ ny ikh` ik jQyd`r qlvMfI d` jIvn sMGrSmeI irh` qy hux E`pxy
jIvn dy E`KrI pihr aunH~ nUM EijhI pMQk prMpr` k`iem krn d` mOk` imilE` hY ijs
aupr is@K nsl~ m`x kr skx | </p>

<p>ਇੰਦਰਾ ਕਤਲ ਕੇਸ ਵਿਚੋਂ ਬਰੀ ਬਲਬੀਰ ਸਿੰਘ ਵਲੋਂ
ਸਰਕਾਰ ਵਿਰੁੱਧ 52 ਲੱਖ ਰੁਪਏ ਦੇ ਹਰਜਾਨੇ ਦਾ ਦਾਅਵਾ
</p>

<p>ਨਵੀਂ
ਦਿੱਲੀ, 4 ਜਨਵਰੀ - ਉਮਰੇ ਦਰਾਜ਼ ਮਾਂਗ ਕੇ ਲਾਏ ਬੇ ਚਾਰ ਦਿਨ, ਦੋ ਆਰਜੂ ਮੇਂ ਕੱਟ ਗਏ ਦੋ
ਇੰਤਜ਼ਾਰ ਮੇਂ । ਦਿੱਲੀ ਵਿਚ ਗੁਰਦੁਆਰਾ ਨਾਨਕ ਪਿਆਓ ਦੀ ਇਕ ਨੁਕਰ ਵਿਚ ਬੈਠਾ ਬਲਬੀਰ ਸਿੰਘ
ਮੁਗਲ ਬਾਦਸ਼ਾਹ ਬਹਾਦਰ ਸ਼ਾਹ ਜਫਰ ਦੇ ਇਸ ਸ਼ੇਅਰ ਨਾਲ ਆਪਣੀ ਜ਼ਿੰਦਗੀ ਦੇ ਬੀਤੇ 16 ਸਾਲਾਂ ਦੀ ਕਹਾਣੀ
ਨੂੰ ਦਰਸਾਉਂਦਾ ਹੈ ।ਇੰਦਰਾ ਗਾਂਧੀ ਕਤਲ ਮਾਮਲੇ ਵਿਚੋਂ ਬਰੀ ਹੋਇਆ ਬਲਬੀਰ ਸਿੰਘ ਹੁਣ ਇਕੱਠੇ
ਤੌਰ ਤੇ ਭਾਰਤ ਸਰਕਾਰ ਨਾਲ ਉਸ ਸੰਤਾਪ ਅਤੇ ਬੇਇੱਜ਼ਤੀ ਵਿਰੁੱਧ ਲੜਾਈ ਲੜ ਰਿਹਾ ਹੈ, ਜਿਸ ਵਿਚੋਂ
ਉਸ ਨੂੰ ਗੁਜ਼ਰਨਾ ਪਿਆ ਹੈ । ਉਸ ਨੇ ਸਰਕਾਰ ਵਿਰੁੱਧ ਹੱਤਕ ਇੱਜ਼ਤ ਦਾ ਦਾਅਵਾ ਕੀਤਾ ਹੈ ਅਤੇ
ਗਲਤ ਤੌਰ ਤੇ ਦੋਸ਼ੀ ਬਣਾਏ ਜਾਣ ਅਤੇ ਦੋ ਵਾਰ ਮੌਤ ਦੀ ਸਜ਼ਾ ਸੁਣਾਏ ਜਾਣ ਲਈ 52 ਲੱਖ ਰੁਪਏ ਦਾ
ਹਰਜ਼ਾਨਾ ਮੰਗਿਆ ਹੈ । ਉਹ ਕਹਿੰਦਾ ਹੈ, ਉਹ ਮੈਂਨੂੰ ਮੇਰੀ ਜ਼ਿੰਦਗੀ, ਮੇਰੇ ਪਰਿਵਾਰ ਨਾਲ ਖੁਸ਼ੀ
ਦੇ ਪਲ ਵਾਪਸ ਨਹੀਂ ਦੇ ਸਕਦੇ । ਜਿਹੜੇ ਆਦਮੀ ਮੌਤ ਦੇ ਘਰ ਵਿਚੋਂ ਵਾਪਸ ਆਇਆ ਹੋਵੇ ਉਸ ਲਈ
ਕੋਈ ਯੋਗ ਮੁਆਵਜ਼ਾ ਨਹੀਂ ਹੋ ਸਕਦਾ । </p>

<p>          31 EkqUbr, 1984 blbIr isMG dI w`d
ivcoN DuMdl` nhIN ipE` | pRD`n mMqrI dy sur@iKE` ivMg ivc iek sb ieMspYktr
blbIr isMG ieMdr` g~DI dI irh`ieS qy qYn`q sI | ijs idn auh kql kIqI geI, aus
idn aus ny S`m 7.30 vjy ifaUtI qy h`zr hox` sI | aus ny S`m nUM 6 vjy Gr qoN
rv`n` hox qoN pihl~ s`r` idn E`pxy pirv`r n`l guj`irE` | aus nUM ieh pq` l@g
cu@ikE` sI ik pRD`n mMqrI nUM aus dy sur@iKE` Emly ny golI m`r id@qI hY | is@K
hox krky blbIr isMG pRD`n mMqrI dy Gr j`x ivc iPkr mihsUs krd` sI, pr ifaUtI qy
h`zr n` hox` j~c krn v`ilE~ dI E@K~ ivc vDyry S@k pYd` krn br`br sI | </p>

<p>          jdoN auh irh`ieS qy phuMc` aus nUM
pRD`n mMqrI dy sur@iKE` sYkSn ivc kMm krn v`ly horn~ is@K~ n`l iek p`sy KVH` hox
leI ikh` igE` | kuJ icr b`Ed aus nUM Gr j`x leI kih id@q` igE` |Gr ivc pirv`r
ny cu@p-c`p r`q d` K`x` K`D` Eqy sO gey | qVky l@gB@g 4 vjy puils d` iek sh`iek
kimSnr Eqy kuJ isp`hIE~ ny blbIr isMG nUM jg`ieE` Eqy aus nUM n`l lY gey | Egly
c`r s`l EgnI pRIiKE` sI | blbIr isMG kihMd` sI, myry ivru@D kdy vI koeI m`ml`
nhIN sI | mYN Gr ivc gUVHI nINd su@q` ipE` sI, jdoN kql hoieE` sI | auh kihMdy
hn ik mYN s`ijS ivc ih@s` ilE`, pr mYN muSikl n`l hI sqvMq, ibEMq j~ ikhr nUM
j`xd` sI | byEMq isMG nUM v`rd`q dI Q~ qy hI m`r id@q` igE` Eqy sqvMq isMG Eqy
kyhr isMG nUM sz` sux`ey j`x qoN b`Ed P~sI dy id@qI geI | </p>

<p>          1986 ivc id@lI dI sYSnz Ed`lq Eqy
h`eIkort ny blbIr isMG nUM doSI Tihr`ieE` Eqy mOq dI sz` sux`eI | suprIm kort
ny lMbI k`nUMnI lV`eI qoN b`Ed aus nUM 1988 ivc brI kr id@q` | auh kihMd` hY,
qusIN PYsly nUM pVHo | ies kihMd` hY ik iesqg`s` blbIr isMG ivru@D doS s`bq
nhIN kr sikE` Eqy aus nUM POrI irh`E kIq` j~d` hY | iqMn bYNc` d` iek sm`n
PYsl` sI | mYN ieh j`nx` c`huMd` h~ ik srk`r ny iks E`D`r qy mYNnUM doSI
bx`ieE` | </p>

<p>          igRPq`rI dy kuJ idn b`Ed id@lI puils
ny aus nUM nOkrI qoN muE@ql kr id@q` Eqy kuJ idn b`Ed gRih mMqr`ly dI isP`irS
qy B`rq dy r`StrpqI ny ibn~ iksy ivB`gI j~c dy aus nUM nOkrI qoN brK`sq kr
id@q`| blbIr isMG dI pqnI Eqy aus dy c`ry b@cy iv@qI Eqy sm`jI sMq`p ivcoN lMGy
hn | aunH~ ny bhuqy idn Ed`lq~ ivc j~ iqh`V jylH dy lohy dy gyt~ E@gy guz`ry hn
| E@j blbIr isMG gurduE`r` n`nk ipE`A ivc QoVHI qnK`h qy iek sur@iKE` EiDk`rI
vjoN kMm kr irh` hY | auh kihMd` hY, ieh sMGrS m`ieE` leI nhIN sgoN myry E`qm
snm`n leI hY | iksy nUM myry Eqy myry pirv`r nUM d`gI bx`aux Eqy b@c ky j`x d`
EiDk`r nhIN | </p>

<p>ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਤੋਂ ਹਿਰਾਸਤ
ਵਿਚ ਪੁੱਛਗਿੱਛ ਦੀ ਲੋੜ - ਸੀ.ਬੀ.ਆਈ. </p>

<p>ਨਵੀਂ ਦਿੱਲੀ, 4
ਜਨਵਰੀ -ਸੀ.ਬੀ.ਆਈ. ਲਈ ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਸਾਬਕਾ ਪ੍ਰਧਾਨ ਬੀਬੀ ਜਗੀਰ
ਕੌਰ ਦੀ ਧੀ ਹਰਪ੍ਰੀਤ ਕੌਰ ਦੀ ਭੇਤ ਭਰੀ ਹਾਲਤ ਵਿਚ ਹੋਈ ਮੌਤ ਲਈ ਚਾਰਜਸ਼ੀਟ ਦਾਇਰ ਕਰਨ ਤੋਂ
ਪਹਿਲਾਂ ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਤੋਂ ਹਿਰਾਸਤ ਵਿਚ ਪੁੱਛਗਿੱਛ ਜਰੂਰੀ ਹੈ । ਇਹ ਜਾਣਕਾਰੀ ਏਜੰਸੀ ਵਿਚਲੇ
ਉਚ ਪੱਧਰੀ ਸੂਤਰਾਂ ਨੇ ਦਿੱਤੀ ।</p>

<p>ਬੀਬੀ ਨੂੰ ਪੰਜਾਬ
ਅਤੇ ਹਰਿਆਣਾ ਹਾਈਕੋਰਟ ਨੇ ਪੇਸ਼ਗੀ ਜ਼ਮਾਨਤ ਦਿੱਤੀ ਹੋਈ ਹੈ ਅਤੇ ਉਹ ਅਦਾਲਤ ਦੀ ਹਦਾਇਤ
ਅਨੁਸਾਰ ਦੋ ਵਾਰ ਸੀ.ਬੀ.ਆਈ. ਕੋਲ ਪੁੱਛਗਿੱਛ ਲਈ ਪੇਸ਼ ਹੋ ਚੁੱਕੀ ਹੈ । ਸੂਤਰਾਂ ਨੇ ਕਿਹਾ ਕਿ
ਇਸ ਮਾਮਲੇ ਵਿਚ ਚਾਰਜਸ਼ੀਟ ਦਾਇਰ ਕਰਨ ਤੋਂ ਪਹਿਲਾਂ ਬੀਬੀ ਤੋਂ ਮੁਕੰਮਲ ਪੁੱਛਗਿੱਛ ਦੀ ਲੋੜ ਹੈ ।
ਸੀ.ਬੀ.ਆਈ. ਨੇ ਬੀਬੀ ਦੀ ਜ਼ਮਾਨਤ ਰੱਦ ਕਰਾਉਣ ਲਈ ਸੁਪਰੀਮ ਕੋਰਟ ਵਿਚ ਅਪੀਲ ਕੀਤੀ ਹੋਈ ਹੈ । 
</p>

<p>ਦੰਗਾ ਪੀੜਤਾਂ ਦੇ ਬੱਚਿਆਂ ਨੂੰ ਮਾਨਤਾ ਪ੍ਰਾਪਤ ਸਕੂਲਾਂ
ਵਿਚ ਮੁੱਫਤ ਸਿੱਖਿਆ ਦਾ ਐਲਾਨ </p>

<p>ਚੰਡੀਗੜ੍ਹ:-
ਪੰਜਾਬ ਸਰਕਾਰ ਨੇ ਦੰਗਾ ਪੀੜ੍ਹਤਾਂ ਦੇ ਮੁੜ ਵਸੇਬੇ ਅਤੇ ਉਨ੍ਹਾਂ ਨਾਲ ਹਮਦਰਦੀ ਪ੍ਰਗਟ ਕਰਦੇ ਹੋਏ ਰਾਜ
ਦੇ ਮਾਨਤਾ ਪ੍ਰਾਪਤ ਸਕੂਲਾਂ ਵਿਚ ਪਹਿਲੀ ਜਮਾਤ ਤੋਂ ਲੈਕੇ ਹਾਇਰਸੈਕੰਡਰੀ ਸਕੂਲ ਵਿਚ ਸਿੱਖਿਆ
ਮੁੱਫਤ ਕਰ ਦਿੱਤੀ ਹੈ। ਇਹ ਜਾਣਕਾਰੀ ਦੰਗਾ ਪੀੜ੍ਹਤ ਐਸੋਸੀਏਸ਼ਨ ਦੇ ਪ੍ਰਧਾਨ ਸ੍ਰ: ਸੁਰਜੀਤ ਸਿੰਘ
ਦੁਗਰੀ ਵੱਲੋਂ ਸਾਡੇ ਪੱਤਰਕਾਰ ਨੂੰ ਸਰਕਾਰ ਵੱਲੋ ਜਾਰੀ ਕੀਤੇ ਹੁਕਮਾਂ ਦੀ ਕਾਪੀ ਦਿਖਾਉਂਦੇ ਹੋਏ
ਕਿਹਾ ਕਿ ਉਹ ਕੱਲ੍ਹ ਚੰਡੀਗੜ੍ਹ ਵਿਖੇ ਰਾਜ ਦੇ ਮੁੱਖ ਸਕੱਤਰ ਵੱਲੋਂ ਸੱਦੀ ਮੀਟਿੰਗ ਵਿੱਚ ਦੰਗਾ
ਪੀੜ੍ਹਤਾਂ ਨੂੰ ਦੇਣ ਵਾਲੀਆਂ ਸਹੂਲਤਾਂ ਦੇ ਏਜੰਡੇ ਤੇ ਬਹਿਸ ਵੀ ਕੀਤੀ। ਉਹਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਲੁਧਿਆਣਾ
ਨਗਰ ਨਿਗਮ ਦੰਗਾ ਪੀੜ੍ਹਤਾਂ ਨੂੰ ਦੁਕਾਨਾਂ ਗੰਦੇ ਨਾਲੇ ਦੇ ਉਪਰ ਦੇ ਰਹੀਂ, ਜੋ ਕਿ ਦੰਗਾ ਪੀੜ੍ਹਤਾਂ
ਨੂੰ ਮੰਜੂਰ ਨਹੀਂ ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਜਦੋਂ ਦਰਿਆ ਵਿੱਚ ਹੜ੍ਹ ਆਉਦਾਂ ਹੈ ਤਾਂ ਸਭ ਤੋਂ ਪਹਿਲਾਂ
ਇਨ੍ਹਾਂ ਦੁਕਾਨਾਂ ਨੂੰ ਨੁਕਸਾਨ ਪਹੁੰਚੇਗਾ। ਸਰਕਾਰ ਦੰਗਾ ਪੀੜ੍ਹਤਾਂ ਨੂੰ ਇਸ ਜਗ੍ਹਾਂ ਤੇ ਦੁਕਾਨ ਨਾ
ਦੇਕੇ ਪੁੱਡਾ ਦੀ ਖਾਲੀ ਜਗ੍ਹਾਂ ਦਿੱਤੀ ਜਾਵੇ, ਜਿਸ ਦੀ ਉਸਾਰੀ ਖੁੱਦ  El`tIE` kryg`| </p>

<p>          s: surjIq isMG ny ikh` ik auh ies
sbMDI r`j dy mu@K mMqrI sR: pRk`S isMG b`dl n`l vI imly ijMnH~ ny Bros~ id@q`
ik dMg` pIVHq~ nUM ieh duk`n~ pu@f` duE`r` id@qI j`vygI| k@lH cMfIgVH ivKy hoeI
mu@K sk@qr SRI E`r.EYs m`n n`l mIitMg ivc horn~ qoN iel`v`  SRI bI.ky SRIv`sqv~(lok b`fIz sk@qr),
EYs.EYs. sMDU(kimSnr ngr ingm luiDE`x`), EYc. E`r gMgUr( f`ierYktr mu@V vsyb`
ivB`g), SRI duby (sk@qr is@iKE` ivB`g),  Eqy dMg` pIVHq~ v@loN ijMnH~ ny mIitMg ivc surjIq isMG, sR: gurdyv
isMG, EYs.EYs slUj`, sR: bldyv isMG, joigMdr isMG, mnmohx isMG p@pU E`id ny
ih@s` ilE`| ies mIitMg iv@c dMg` pIVHq~ nUM mk`n El`tmYNt, l`l k`rf bn`aux
v`sqy,  dMg` pIVHq EMghIx nUM mu@Pq mk`n
dyx sbMDI vI ivc`r kIqy gey| mu@K sk@qr ny mIitMg ivc Bros` idv`ieE` ik  eyjMfy muq`bk iek mhIny ivc s`ry PYsly lY ley
j`xgy| SRI surjIq isMG ny smUh dMg` pIVHq pirv`r v@loN srk`r d` ies EYl`n qy
DMnv`d kIq`| 
</p>

<p>ਕੈਪਟਨ ਅਤੇ ਟੰਡਨ ਦੇ ਰਾਜਨੈਤਿਕ ਵਜੂਦ ਟਕਰਾਏ
</p>

<p>ਚੰਡੀਗੜ੍ਹ:
ਭਾਜਪਾ ਦੇ  sInIEr nyq` Eqy pMj`b dy lokl
b`fIz mMqrI blr`m jI d`s tMfn duE`r` ipCly idnI iv@q mMqrI kYptn kMvljIq isMG
d` n~ ley ibnH~ hI aunH~ dy iKl`P id@qI ibE`nb`zI dy ip@Cy kyvl dPqrI msl` hI
nhIN hY| blik ies ivc k`PI h@d q@k r`jnYiqk jVH~ vI gihrI juVI hY| tMfn dy
krIbI lok~ dy Enus`r iek hI ijlHy qo ivD`iek hox dy k`rx don~ dy r`jnYiqk vjUd
tkr` rhy hn| tMfn r`jpur` Eqy kYptn bnUV ivD`nsB` hlky qoN ivD`iek  hn Eqy ieh donoN hI pitE`l` ijlHy  dy EMdr E`audyN hn| sUqr~ dy Enus`r E@lg E@lg
dl~ qoN hox dy b`vjUd iek hI ijlHy  n`l
sbMDq hox dy k`rx ienH~ nyq`v~ dy E`psI sbMD k`PI iPky pY rhy hn| B`jp` dy
nyVly sUqr~ d` kihx` hY ik kYptn ny B`jp` dy kuJ sInIEr nyq`v~ dy n`l sMprk kr
aunH~ nUM Ek`lI dl ivc S`iml krn dI EsPl koiSS kIqI sI| ienH~ ivcoN fyr`b@sI
Kyqr ivc ku@J nyq`v~ qoN iel`v~ ijlH~ B`jp` pRD`n dy n~  ley j` rhy hn| ijMnH~ dy n`l kYptn ny sMprk
krky Ek`lI dl ivc lY j`x d` wqn kIq` Eqy auhn~ nUM Ek`lI dl ivc v@fy Ehudy dyx
d` Bros` vI id@q`| jykr pitE`l` ijlHy qoN B`jp` ivc syNDm`rI huMdI q~ auh is@Dy
qoNr qy tMfn dI BUimk~ qy sv`lIE~ inS`n l@gd`| </p>

<p>          pMj`b pRdyS B`jp` dy sMgTn mMqrI
Eivn`S j`sv`l ny ies b`ry p@qrk`r~ nUM d@isE` ik auhn~ nUM EijhIE~ iSk`ieq~
imlIE~ q~ hn pr auh ies iSk`ieq n`l sbMDq  B`jp` nyq`v~ n`l sMprk krn dI koiSS kr rhy hn, ijMnH~ dy n`l kYptn
duE`r` sMprk krn dI g@l kIqI j` rhI hY| j`sv`l ny ikh` ik auhn~ E@j pitE`l`
ivKy sbMDq B`jp` nyq`v~ nUM dPqr ivc s@idE` hY, auhn~ dy S`m q@k phuMcx dI
aumId hY| iehI nyq` d@s skdy hn ik  shI
pojISn kI hY ? n`l hI auhn~ ieh vI ikh` ik keI v`r kuJ glqPihmIE~ vI pYd` ho
j~dIE~ hn, kuJ vrkr~ dI kmzorI vI hMudI hY, jo tutx nUM iqE`r bYTy hox Eqy g@l
Ku@Lx qy EijhI g@l~ krdy hn|  </p>

<p>ਬਾਦਲ
ਮਾਰਚ ਵਿਚ ਚੋਣਾਂ ਕਰਾਏਗਾ : ਅਮਰਿੰਦਰ ਸਿੰਘ </p>

<p>ਫਗਵਾੜਾ:-
ਪੰਜਾਬ ਪ੍ਰਦੇਸ਼ ਕਾਂਗਰਸ ਕਮੇਟੀ(ਆਈ) ਦੇ ਪ੍ਰਧਾਨ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਸਿੰਘ ਨੇ ਕਿਹਾ ਹੈ ਕਿ
ਪੰਜਾਬ ਵਿਚ ਵਿਧਾਨਸਭਾ ਚੋਣਾਂ ਹਰ ਹਾਲਤ ਵਿਚ ਮਾਰਚ ਵਿਚ ਹੀ ਹੋਣਗੀਆਂ। ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ
ਬਾਦਲ ਚੋਣਾਂ ਨਾ ਹੋਣ ਦਾ ਦਾਅਵਾ ਕਰਕੇ ਸਿਰਫ ਲੋਕਾਂ ਨੂੰ ਗੁੰਮਰਾਹ ਕਰ ਰਹੇ ਹਨ। </p>

<p>          E@j duiphr pMj`b dy mrhUm mMqrI  coDrI jgq r`m sUMF dI brsI sm`gm ivc B`g lYx
qoN pihl~ aunH~ ieQyp@qrk`r~ n`l g@lb`q kridE~ ikh` ik isrP cox~ dI EMdrUnI
iqE`rI k`rn hI pMj`b dI Ek`lI-B`jp` srk`r DV`DV EYl`n kr rhI hY| jdoN aun~ qoN
pu@iCE~ ik sRI b`dl ny spSt ikh` hY ik cox~ im@Qy my qy hI hoxgIE~, B`vyN aunH~
qoN hlPIE` ibE`n lY lvo| ies dy jv`b ivc aunH~ ikh` ik sRI b`dl kor` JUT bol
rhy hn ikauNik ijhVy SRI b`dl  Ek`l qKq
auqy shuM K` ky mu@kr skdy hn aunH~ dy hlPIE` ibE`n auqy qusI ikvyN wkIn kr
skdy ho| </p>

<p>          aunH~ ikh` ik sRI b`dl v@loN DV`DV jo
sMgq drSn pRogr`m Eqy PMf vMfy j` rhy hn  ie sB cox~ k`rn hI hY| aunH~ ikh` ik hux q~ sRI b`dl bI.fI.A  d` kMm kr irh` hY| ijhVIE~ pyNfU ivk`s leI
gR~t~ peIE~ hn aus nUM dyx d` EiDk`r bI.fI.A kol hY| aus nUM Kud SRI b`dl vMf
rhy hn| </p>

<p>          aunH~ srk`r v@loN k@lH ley PYsly ik
pMj s`l~ qoN ikr`eyd`r j~ k`bz nUM hI m`lk smiJE` j`vyg` dy jv`b  ivc aunH~ ikh` ik sRI b`dl dy smrQk~ ny hI sB
qoN v@D srk`rI jmIn~ auqy kiQq kbzy kIqy hoey hn| aunH~ dy kbzy jrUr j`iez bxngy
z` B`jp` E`gU Q`-Q~ kiQq kbzy kr rhy hn| bhuqy Sihr~ ivc B`jp` dy EgUE~ ny KoKy
r@K ky kImqI Q`v~ auqy kiQq kbzy kIqy hoey hn| iesy qrH~ ipMf~ ivc Ek`lI vrkr~
ny v@fIE~ jmIn~ auqy kiQq kbzy kIqy hoey hn| aunH` ikh` ik lok hux Ek`lI-B`jp`
srk`r dIE~ nIqIE` qoN byh@d pRyS`n hn| ieh lok hux ieMk` v@l E` rhy hn| k@lH
fk`l` Eqy GnOr ivKy hoey sm`gm~ ny ieh is@D kr id@q` hY | aunH~ ikh` ik r`j ivc
jy ieMk` dI srk`r bxI q~ pihl~ b`dl srk`r dy iBRSt`c`r dy iKl`P kimSn ibT`v~gy|
aunH~ ikh` ik ieMk` pRD`n sonIE` g~DI PrvrI j~ m`rc ivc pMj`b E`auxgy qy ies
mOky r`j p@DrI rYlI nUM sMboDn krngy| </p>

<p>ਬਾਦਲ ਦਾ ਉਦਯੋਗਪਤੀਆਂ ਬਾਰੇ ਨਰਮ ਵਤੀਰਾ ਚੋਣਾਂ
ਵੱਲ ਇਸ਼ਾਰਾ </p>

<p>ਲੁਧਿਆਣਾ:
ਰਾਜ ਦੇ ਉਦਯੋਗਪਤੀ ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਵੱਲੋਂ ਅਪਣਾਏ ਜਾ ਰਹੇ ਨਰਮ ਵਤੀਰੇ ਨੂੰ
ਚੋਣਾਂ ਦਾ ਸੰਕੇਤ ਸਮਝ ਰਹੇ ਹਨ। ਬੀਤੀ ਸ਼ਾਮ ਜਦ ਂਮੁੱਖ ਮੰਤਰੀ ਲੁਧਿਆਣਾ ਦੇ ਕੁਝ  coxvNy audwogqIE~ Eqy snEqI E`gUE~ n`l mIitMg
krn leI pI.ey.wU ivc pu@jy q~ sRI b`dl ny K@lHuy idl n`l  shulq~ dy g@Py vMfxy SurU kr id@qy| aunH~
snEqk`r~ dI hr g@l nUM mMinE~| SRI b`dl mu@K qOr auqy luiDE`x` nyVy l`Fov`l ivc
bxn v`ly 1250 eykV dy nvIn shuLq~ v`ly EMqrr`StrI p@Dr dy snEqI Pokl puE`ieMt
ivcly El`tIE~ nUM pl`t~ dy m`lk`n` h@k dyx d` EYl`n kridE~ ikh` ik auh
snE`qk`r~ dy iksy kMm ivc vI EiV@k` nhIN p`auxgy qy ijvyN quh`nUM TIk lgd` hY
auh kMm aunH~ kolN krv`eI j`A| </p>

<p>          jdoN SRI b`dl snEqk`r~ dIE~ mMg~ mMn
rhy sn q~ iek snEqI E`gU ny aunH~ nUM ikh` ik mu@K mMqrI s`ihb aun~H mMg~ mMnx
d` EYl`n hI krIA ijhVIE~ pUrIE~  hox
j`x, b`Ed ivc ieh n` khIA ik srk`r kol PMf nhIN hn|| ieh sux ky SRI b`dl muskr`
pey qy ikh`, iPkr n` kro| sB ku@J kr~gy SR b`dl ny ivkrI kr n`l sbMDq msilE~,
ibjlI borf Eqy hor srk`rI ivB`g~ dIE~ v@KrIE~ mIitMg~ s@dx dy hukm id@qy hn|
aunH~ snEqk`r~ nUM ikh~, s`ry msly h@l ho j`xgy| icMq~ n` kro E@j q~ pl`t d@so
ikMny ikMny c`hIdy hn| sRI b`dl ny ikh` ik l`Fov`l nyVy bxnv`l` Pokl puE`ieMt jo
240 kroV rupey dI l`gq n`l bxyg` ijs d` kMm do mhIinE~ dy EMdr EMdr c`lU ho
j`vyg`| aunH~ ikh` ik B`rq srk`r v@loN cIn dI qrz auqy bx`ey j`x v`l` ivSyS
E`riQk zyn(EYs.eI.jYf) dI skIm qihq ies Pokl puE`ieMt ivc 500 eykV zmIn r`KvI
hovygI ijs ivc l@gx v`lIE~ snEqI iek`eIE~ nUM auqp`dn iqE`r krn qoN b`hrly dyS~
ivc Byjx q@k s`rIE~ shUlq~ imlxgIE~ Eqy kyNdr srk`r dIE~ hd`ieq~ Enus`r tYks~
ivc Cot vI imlygI| aunH~ ikh` ik l`Fov`l Pokl puE`ieMt ivSv pRis@D hovyg`| SRI
b`dl ny ikh` ik  ies Pokl puE`ieMt ivc
dr`md-br`md leI KuSk bMdrg`h vI bx`eI j`vygI| pRdUiSq snEqI iek`eIE~ leI s`J~
trItmYNt pl~t, k`rob`rI v`Dy leI vp`r kyNdr, ibjlI spl`eI leI 4 sb stySn,
tYlIPon EYkscyNj, 5 eykV, tYlIPon EYkscyNj, 5 eykV ivc pMj q`r` hotl Eqy 75
eykV ivc golP kors bx`aux dI qjvIz r@KI geI hY| </p>

<p>            ਦਰਿਆ ਸਤਲੁਜ ਦੇ
ਕੰਢੇ ਤੇ ਸਥਾਪਤ ਹੋਣ ਕਾਰਨ ਉਨਾਂ੍ਹ ਹੜ੍ਹਾਂ ਦੀ ਸ਼ੰਕਾ ਦੂਰ ਕਰਦਿਆਂ ਕਿਹਾ ਕਿ ਦਰਿਆ ਉਪਰ ਬਣੇ
ਬੰਨ੍ਹ ਨੂੰ 10 ਕਰੋੜ ਰੁਪਏ ਦੀ ਲਾਗਤ ਨਾਲ 60-70 ਫੁੱਟ ਚੋੜ੍ਹਾ ਅਤੇ ਮਜ਼ਬੂਤ ਬਣਾ ਕੇ ੳੇਪਰ ਸੜਕ
ਬਣਾ ਦਿੱਤੀ ਜਾਵੇਗਾੀ। ਸ਼੍ਰੀ ਬਾਦਲ ਨੇ ਕਿਹਾ ਕਿ ਉਹ ਚਾਹੁੰਂਦੇ ਹਨ ਕਿ ਪੰਜਾਬ ਉਦਯੋਗਿਕ ਤਰੱਕੀ
ਤੇਜ਼ ਹੋਵੇ ਅਤੇ ਇਸ ਲਈ ਉਹ ਹਰ ਸੰਭਵ ਸਹੂਲਤ ਦੇਣ ਲਈ ਤਿਆਰ ਹੈ। </p>

<p> </p>






</body></text></cesDoc>