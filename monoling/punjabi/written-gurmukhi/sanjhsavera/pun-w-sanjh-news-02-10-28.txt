<cesDoc id="pun-w-sanjh-news-02-10-28" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-02-10-28.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 02-10-28</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>02-10-28</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਸਰਦਾਰਨੀ
ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਮੈਂਬਰਾਂ ਤੇ ਅੱਖ ਰੱਖਦੀ ਹੈ</p>

<p>ਬਾਦਲ
ਢਾਣੀ(ਹਰਿਆਣਾ):- 12 ਨਵੰਬਰ ਨੂੰ ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਪ੍ਰਧਾਨਗੀ ਦੀ ਚੋਣ
ਲਈ ਚੱਲ ਰਹੇ 'ਧਰਮਯੁੱਧ' ਦੀ ਤਿਆਰੀ ਲਈ ਮਾਨਸਾ ਦੇ ਲਾਗਲੇ ਜਿਲ੍ਹੇ ਸਿਰਸਾ
ਨੇੜੇ ਪਿੰਡ ਬਾਲਾਸਰ ਦੇ ਨਜ਼ਦੀਕ " ਬਾਦਲ ਢਾਣੀ" ਵਿਖੇ ਫਾਰਮ ਹਾਊਸ ਵਿੱਚ ਇੱਕਠੇ
ਕੀਤੇ ਬਾਦਲ ਪੱਖੀ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਮੈਂਬਰਾਂ ਨਾਲ ਅੱਜ ਜ਼ਿਲ੍ਹੇ ਦੇ ਪੱਤਰਕਾਰਾਂ ਦੀ ਟੀਮ ਮੁਲਾਕਾਤ ਕਰਨ
ਲਈ ਗਈ, ਜਿਸ ਵਿਚ " ਸਾਂਝ ਸਵੇਰਾ" ਦੇ ਪੱਤਰਕਾਰ ਵੀ ਸ਼ਾਮਲ ਸੀ। ਇਥੇ 88 ਮੈਂਬਰ ਹਨ
ਬਾਦਲ ਪ੍ਰਤੀ ਆਪਣੀ ਵਫਾਦਾਰੀ ਦਾ ਮੁਜਾਹਰਾ ਕਰਨ ਲਈ ਉਥੇ ਇੱਕਠੇ ਹੋਏ ਹਨ। ਇਨ੍ਹਾਂ ਉਪਰ
ਸਰਦਾਰਨੀ ਅੱਖ ਰੱਖਦੀ ਹੈ। </p>

<p>ਬਾਦਲਾ
ਢੱਕੀ ਹੀ ਰਿੱਝਣ ਦੇਅ</p>

<p>ਮੁੱਲਾਂਪੁਰ
ਦਾਖਾ: ਬਿਹਤਰ ਤਾਂ ਇਹੀ ਹੈ ਕਿ ਸਾਬਕਾ ਮੁੱਖ ਮੰਤਰੀ ਸ. ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਢੱਕੀ ਹੀ ਰਿੱਝਣ
ਦੇਣ, ਜੇ ਇਸ ਬੰਦ ਕਿਤਾਬ ਦਾ ਇੱਕ ਵੀ ਵਰਕਾ ਖੁੱਲ੍ਹ ਗਿਆ ਤਾਂ ਗੱਲ ਬਹੁਤ ਦੂਰ ਜਾ ਸਕਦੀ
ਹੈ।  ਧਮਕੀ ਵਰਗੀ ਇਹ ਸਲਾਹ ਪੰਜਾਬ ਮੰਡੀ ਬੋਰਡ ਦੇ ਸਾਬਕਾ ਚੇਅਰਮੈਨ ਅਤੇ ਗੈਰ ਸਿਆਸੀ 7
ਮੈਂਬਰੀ ਕਮੇਟੀ ਦੇ ਕਨਵੀਨਰ ਜਥੇਦਾਰ ਮੱਲ ਸਿੰਘ ਘੁਮਾਣ ਨੇ ਆਪਣੇ ਨਿਵਾਸ ਸਥਾਨ ਵਿਖੇ ਪੱਤਰਕਾਰਾਂ
ਨਾਲ ਗੱਲਬਾਤ ਕਰਦਿਆਂ ਦਿੱਤੀ।</p>

<p>ਮੁਫਤੀ
ਵੱਲੋਂ 'ਪੋਟਾ' ਉਤੇ ਅਮਲ ਨਾ
ਕਰਨ ਦਾ ਵਾਅਦਾ</p>

<p>ਨਵੀਂ ਦਿੱਲੀ :
ਜੰਮੂ ਕਸ਼ਮੀਰ ਵਿਚਲੀ ਪੀ.ਡੀ.ਪੀ. ਕਾਂਗਰਸ ਕੁਲੀਸ਼ਨ ਨੇ ਕੇਂਦਰ ਨੂੰ ਆਖਿਆ ਹੈ ਕਿ ਉਹ ਰਾਜ 'ਚ ਸ਼ਾਂਤੀ ਦੀ ਬਹਾਲੀ ਲਈ ਨਵੇਂ ਵਿਧਾਇਕਾਂ ਅਤੇ ਲੋਕ
ਰਾਏ ਦੀ ਨੁਮਾਇੰਦਗੀ ਕਰਨ ਵਾਲੇ ਹੋਰ ਆਗੂਆਂ ਨਾਲ ਬਿਨਾਂ ਸ਼ਰਤ ਗੱਲਬਾਤ ਕਰੇ। ਇਸ ਤੋਂ ਇਲਾਵਾ
ਕੁਲੀਸ਼ਨ ਨੇ ਵਿਵਾਦਮਈ ਵਿਸ਼ੇਸ਼ ਆਪਰੇਸ਼ਨ ਗਰੁੱਪ (ਐਸੱ.ਓ.ਜੀ.) ਇਕ ਤਰ੍ਹਾਂ ਨਾਲ ਭੰਗ ਕਰਨ ਅਤੇ
ਰਾਜ 'ਚ ਪੋਟਾ ਨਾ ਲਾਗੂ ਕਰਨ ਦਾ
ਫੈਸਲਾ ਲਿਆ ਹੈ।</p>

<p>ਤਲਵੰਡੀ
ਵੱਲੋਂ ਏਕਤਾ ਧੜੇ ਦੀ ਹਮਾਇਤ ਦਾ ਸੰਕੇਤ</p>

<p>ਅੰਬਾਲਾ :
ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ(ਬ) ਦੀ ਅਨੁਸ਼ਾਸ਼ਨੀ ਕਮੇਟੀ ਦੇ ਮੁਖੀ ਜਗਦੇਵ ਸਿੰਘ ਤਲਵੰਡੀ ਦੇ ਅੱਜ ਸਵੇਰੇ
ਇਤਿਹਾਸਕ ਗੁਰਦੁਆਰਾ ਸੀਸਗੰਜ਼ 'ਚ ਦਿੱਤੇ ਭਾਸ਼ਨ ਦੇ ਆਧਾਰ ਤੇ
ਕਿਹਾ ਜਾ ਸਕਦਾ ਹੈ ਕਿ ਉਹ ਵੀ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਪ੍ਰਧਾਨ ਦੀ ਚੋਣ ਲਈ ਗਠਿਤ 7 ਮੈਂਬਰੀ ਕਮੇਟੀ ਦੇ
ਸਰਬਸੰਮਤੀ ਦੇ ਯਤਨਾਂ ਦੇ ਹਮਾਇਤੀ ਹਨ। ਇਹ ਚੋਣ 12 ਨਵੰਬਰ ਨੂੰ ਹੋਣੀ ਹੈ।</p>

<p>ਜਥੇਦਾਰ
ਘੁਮਾਣ ਦੀ ਮੁਹਿੰਮ ਨੇ ਬਾਦਲ ਖੇਮੇ ਵਿਚ ਹਲਚਲ ਮਚਾਈ</p>

<p>ਚੰਡੀਗੜ੍ਹ :
ਨਵੰਬਰ ਮਹੀਨੇ ਵਿਚ ਹੋਣ ਵਾਲੀ ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ ਦੀ ਚੋਣ ਇਸ ਵੇਲੇ
ਪੰਜਾਬ ਦੀ ਰਾਜਸੀ ਫਿਜ਼ਾ ਵਿਚ ਦਿਨੋ-ਦਿਨ ਗਰਮਾਹਟ ਲਿਆ ਰਹੀ ਹੈ। ਜਥੇਦਾਰ ਮੱਲ ਸਿੰਘ ਘੁਮਾਣ
ਵੱਲੋਂ ਛੇੜੀ ਇਸ ਮੁਹਿੰਮ ਨੂੰ ਸਿੱਖ ਜਗਤ ਵਲੋਂ ਭਰਵਾਂ ਹੁੰਗਾਰਾ ਮਿਲਣ ਦੇ ਨਾਲ ਨਾਲ ਕੋਈ ਵੀ
ਰਾਜਸੀ ਦਲ ਇਸ ਖਿਲਾਫ ਖੁੱਲ੍ਹ ਕੇ ਬੋਲਣ ਦਾ ਹੀਆ ਨਹੀਂ ਕਰ ਰਿਹਾ । ਪਤਾ ਲੱਗਾ ਹੈ ਕਿ ਪ੍ਰਕਾਸ਼
ਸਿੰਘ ਬਾਦਲ ਇਸ ਅਚਾਨਕ ਆਈ ਚੁਣੌਤੀ ਅੱਗੇ ਕਾਫੀ ਘਬਰਾ ਗਏ ਹਨ। </p>

<p>ਕੈਪਟਨ
ਤੋਂ ਖਿਝੇ ਢੀਂਡਸਾ ਨੇ ਰਵੀਇੰਦਰ ਤੇ ਟੌਹੜਾ ਉਤੇ ਗੁੱਸਾ ਝਾੜਿਆ</p>

<p>ਸੁਨਾਮ :
ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਦੇ ਸਕੱਤਰ ਜਨਰਲ ਸ਼੍ਰੀ ਸੁਖਦੇਵ ਸਿੰਘ ਢੀਂਡਸਾ ਨੇ ਦੋਸ਼ ਲਾਇਆ ਕਿ ਪੰਜਾਬ ਦੇ
ਸਮੱੁਚੇ ਪ੍ਰਸ਼ਾਸ਼ਨ ਨੂੰ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਦੀ ਬਜਾਏ ਟੌਹੜਾ ਅਤੇ ਰਵੀਇੰਦਰ ਸਿੰਘ ਚਲਾ ਰਹੇ
ਹਨ। ਸਾਰਾ ਪ੍ਰਸ਼ਾਸ਼ਨ ਹੀ ਉਨਾਂ੍ਹ ਦਾ ਜਵਾਬਦੇਹ ਬਣ ਕੇ ਰਹਿ ਗਿਆ ਹੈ। ਜਦੋਂ ਕਿ ਉਨ੍ਹਾਂ ਕੋਲ
ਸੰਵਿਧਾਨਿਕ ਤਾਕਤ ਹੈ। </p>

<p>ਪੰਜਾਬ
ਸਟੇਸ਼ਨਰੀ ਵਿਭਾਗ ਵਿਚ ਵੱਡੀ ਪੱਧਰ 'ਤੇ ਘਪਲੇਬਾਜ਼ੀ</p>

<p>ਚੰਡੀਗੜ੍ਹ :
ਪੰਜਾਬ ਦੇ ਪ੍ਰਸਿੱਧ ਅਤੇ ਸਟੇਸ਼ਨਰੀ ਵਿਭਾਗ ਵਿਚ ਵੋਟਰ ਲਿਸਟਾਂ ਦੇ ਟੈਂਡਰਾਂ ਵਿਚ ਕਥਿਤ ਤੌਰ 'ਤੇ ਵੱਡੀ ਪੱਧਰ 'ਤੇ ਕੀਤੇ ਗਏ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਦਾ ਮਾਮਲਾ ਸਾਹਮਣੇ
ਆਇਆ ਹੈ। ਇਸ ਸਬੰਧੀ ਚੰਡੀਗੜ੍ਹ ਆਸ ਪਾਸ ਦੇ ਕਈ ਪ੍ਰਿੰਟਿੰਗ ਪ੍ਰੈਸੱਾਂ ਦੇ ਮਾਲਕਾਂ ਨੇ
ਕੰਟੋਰਲਰ, ਡਿਪਟੀ ਕੰਟਰੋਲਰ 'ਤੇ ਦੋਸ਼ ਲਾਇਆ ਹੈ ਕਿ ਉਨ੍ਹਾਂ
ਨੇ ਵੋਟਰ ਲਿਸਟਾਂ ਛਾਪਣ ਲਈ ਭਰੇ ਗਏ ਟੈਂਡਰਾਂ ਵਿੱਚ ਵੱਡੀ ਪੱਧਰ ਤੇ ਨਿਯਮਾਂ ਨੂੰ ਬਦਲ ਕੇ ਆਪਣੇ
ਚਹੇਤੇ ਪ੍ਰਿੰਟਿੰਗ ਪ੍ਰੈੱਸ ਮਾਲਕਾਂ ਨੂੰ ਛਪਾਈ ਦੇ ਆਰਡਰ ਦੇ ਦਿੱਤੇ। </p>

<p>ਪਾਕਿ
ਚੋਣਾਂ 'ਚ ਕੱਟੜਪੰਥੀਆਂ
ਦੀ ਜਿੱਤ ਭਾਰਤ ਲਈ ਖਤਰੇ ਦੀ ਘੰਟੀ</p>

<p>ਨਵੀਂ ਦਿੱਲੀ :
ਭਾਰਤ ਦੇ ਗੁਆਂਢੀ ਮੁਲਕ ਪਾਕਿਸਤਾਨ 'ਚ ਹੁਣੇ ਹੋ ਕੇ ਹਟੀਆਂ ਅਸੰਬਲੀ
ਚੋਣਾਂ 'ਚ ਕੱਟੜਪੰਥੀਆਂ ਪਾਰਟੀਆਂ ਦੇ
ਗੱਠਜੋੜ ਨੂੰ ਮਿਲੀ ਤਾਕਤ ਨਾਲ ਭਾਰਤ-ਪਾਕਿ ਦੇ ਸਬੰਧਾਂ 'ਚ ਹੋਰ ਵਧੇਰੇ ਕਸ਼ੀਦਗੀ ਆਉਣ ਦੀਆਂ ਸੰਭਾਵਨਾਵਾਂ ਪ੍ਰਗਟਾਈਆਂ ਜਾ ਰਹੀਆਂ
ਹਨ। ਭਾਰਤੀ ਖੁਫੀਆ ਏਜੰਸੀ ਦੇ ਇਕ ਸਾਬਕਾ ਅਫਸਰ ਨੇ ਇਸ ਸਬੰਧੀ ਆਪਣੇ ਵਿਚਾਰ ਪ੍ਰਗਟ ਕਰਦਿਆਂ
ਕਿਹਾ ਕਿ ਪਾਕਿਸਤਾਨ ਅਸੰਬਲੀ ਚੋਣਾਂ ਦੇ ਨਤੀਜਿਆਂ ਨਾਲ ਜਿੱਥੇ ਅੱਤਵਾਦ ਖਿਲਾਫ ਅੰਤਰਾਸ਼ਟਰੀ ਪੱਧਰ 'ਤੇ ਅਮਰੀਕਾ ਵੱਲੋਂ ਛੇੜੀ ਗਈ ਲੜਾਈ ਨੂੰ ਢਾਹ
ਲੱਗਣ ਦਾ ਖਤਰਾ ਹੈ। </p>

<p> </p>






</body></text></cesDoc>