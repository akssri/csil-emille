<cesDoc id="pun-w-sanjh-news-02-09-10" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-02-09-10.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 02-09-10</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>02-09-10</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਜਦੋਂ
ਵਾੜ ਹੀ ਖੇਤ ਨੰੂ ਖਾਣ ਲੱਗੀ</p>

<p>(ਸਾਂਝ ਸਵੇਰਾ)
ਕਿਸਾਨੀ ਨੂੰ ਵਪਾਰਕ ਬੈਂਕਾ ਤੇ ਸ਼ਾਹੂਕਾਰਾ ਬੈਕਾਂ ਦਾ ਢਾਂਚਾ ਹੁਣ ਉਲਟਾ ਕਿਸਾਨਾਂ ਦਾ ਆਰਥਿਕ ਸ਼ੋਸ਼ਣ
ਕਰਨ ਵਾਲਾ ਬਣ ਗਿਆ ਹੈ ਕਿਉਂਕਿ ਇਨਾਂ੍ਹ ਬੈਕਾਂ ਵਲੋ ਵਪਾਰਕ ਬੈਕਾਂ ਨਾਲੋ ਨਾ ਸਿਰਫ ਵੱਧ ਵਿਆਜ
ਵਸੂਲ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ ਸਗੋ ਹਿੱਸੇਦਾਰੀ ਆਦਿ ਦੀਆਂ ਕਈ ਹੋਰ ਵੀ ਸ਼ਰਤਾਂ ਕਿਸਾਨਾਂ ਤੋ ਪੂਰੀਆ
ਕਾਰਵਾਈਆਂ ਨੂੰ ਲਾਭ ਹੋਣ ਦੀ ਥਾਂ ਆਰਥਿਕ ਤੌਰ ਤੇ ਨੁਕਸਾਨ ਹੀ ਹੋ ਰਿਹਾ ਹੈ। </p>

<p>ਭ੍ਰਿਸ਼ਟਾਚਾਰ
ਵਿਰੋਧੀ ਮੁਹਿੰਮ ਦੇ ਬਾਵਜੂਦ ਪਰਨਾਲਾ ਥੈਂ ਦੀ ਥੈਂ</p>

<p>ਜਲੰਧਰ:- (ਸਾਂਝ
ਸਵੇਰਾ) ਸਥਾਨਕ ਜ਼ਿਲ੍ਹਾ ਕਾਂਗਰਸ ਕਮੇਟੀ ਸ਼ਹਿਰੀ ਵੱਲੋਂ ਤਿਆਰ ਕੀਤੀ ਗਈ ਵੈਬਸਾਈਟ www.dccjalandharcity.com 'ਤੇ ਲੋਕਾਂ ਵੱਲੋਂ ਕੀਤੀਆਂ ਸ਼ਿਕਾਇਤਾਂ ਨੇ ਪੰਜਾਬ
ਸਰਕਾਰ ਦੇ ਕਈ ਮਹਿਕਮਿਆਂ 'ਚ ਬਾਦਲ ਸਰਕਾਰ ਦੇ ਸਮੇਂ ਤੋਂ
ਹੀ ਚੱਲਿਆ ਆ ਰਿਹਾ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਨਾ ਰੁਕਣ ਦੀਆਂ ਅਨੇਕਾਂ ਸ਼ਿਕਾਇਤਾਂ ਪ੍ਰਾਪਤ ਹੋਈਆਂ ਹਨ ਜਿਸ ਨਾਲ
ਇਹ ਗੱਲ ਵੀ ਉਭਰ ਕੇ ਸਾਹਮਣੇ ਆਈ ਹੈ ਕਿ ਕੈਪਟਨ ਸਰਕਾਰ ਵੱਲੋਂ ਆਰੰਭੀ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਵਿਰੋਧੀ
ਮੁਹਿੰਮ 'ਚ ਅਫਸਰਸ਼ਾਹੀ ਮੁੱਖ ਅੜਿੱਕਾ
ਬਣਦੀ ਜਾ ਰਹੀ ਹੈ। </p>

<p>ਗ੍ਰਿਫਤਾਰੀ
ਦੇ ਡਰੋਂ ਅਕਾਲੀ ਕਾਂਗਰਸੀਆਂ ਦੀ ਸ਼ਰਨ 'ਚ</p>

<p>ਭੁੱਚੋਂ ਮੰਡੀ
:-(ਸਾਂਝ ਸਵੇਰਾ) ਸਾਬਕਾ ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਦੇ ਸਹੁਰਾ ਪਰਿਵਾਰ ਦੀ ਕਥਿਤ ਘਪਲੇਬਾਜ਼ੀ
ਦੀ ਜਾਂਚ ਸ਼ੁਰੂ ਹੋਣ ਨਾਲ ਅਕਾਲੀ ਹਲਕਿਆਂ ਵਿਚ ਅਫਰਾ ਤਫਰੀ ਅਤੇ ਸਹਿਮ ਦਾ ਮਾਹੌਲ ਲਗਾਤਾਰ ਬਣਿਆ
ਹੋਇਆ ਹੈ। ਭਰੋਸੇਯੋਗ ਸੂਤਰਾਂ ਤੋਂ ਪ੍ਰਾਪਤ ਜਾਣਕਾਰੀ ਅਨੁਸਾਰ ਕਿਸੇ ਸਮੇਂ ਆਪਣੇ ਨੂੰ ਖੁਦਾ ਏ
ਖਾਸ ਸਮਝਣ ਵਾਲੇ ਅਤੇ ਦੇਸ਼ ਕਥਿਤ ਘਪਲੇਬਾਜ਼ੀ ਨਾਲ ਸਿੱਧੇ ਜਾ ਅਸਿੱਧੇ ਤੌਰ ਤੇ ਜੁੜੇ ਅਕਾਲੀਆਂ ਨੇ
ਸੰਭਾਵਿਤ ਪਰਚੇ ਅਤੇ ਗ੍ਰਿਫਤਾਰੀਆਂ ਦੇ ਡਰੋਂ ਕਾਂਗਰਸੀਆਂ ਦੀ ਸ਼ਰਨ ਵਿਚ ਜਾਣਾ ਸ਼ੁਰੂ ਕਰ ਦਿੱਤਾ
ਹੈ। </p>

<p>ਅੰਬਾਲਾ
ਤੇ ਰਾਜਸਥਾਨ ਵਿੱਚ ਇਕੋ ਦਿਨ ਦੋ ਲੜਾਕੂ ਜਹਾਜ਼ ਡਿੱਗੇ, ਪਾਇਲਟ ਬੱਚ ਗਿਆ!</p>

<p>ਨਵੀਂ ਦਿੱਲੀ :-
(ਸਾਂਝ ਸਵੇਰਾ) ਭਾਰਤੀ ਹਵਾਈ ਫੌਜ਼ ਦੇ ਦੋ ਮਿੱਗ-21 ਜਹਾਜ਼ ਸੋਮਵਾਰ ਨੂੰ ਹਾਦਸਾਗ੍ਰਸਤ ਹੋ ਕੇ
ਹਰਿਆਣਾ ਤੇ ਰਾਜਸਥਾਨ ਵਿੱਚ ਆ ਡਿੱਗੇ, ਪਰ ਦੋਵਾਂ ਦੇ ਪਾਇਲਟ ਸੁਰੱਖਿਅਤ ਬਚ ਗਏ। ਇਨ੍ਹਾਂ
ਜਹਾਜ਼ਾਂ ਦੀਆਂ ਉਡਾਣਾਂ ਕਈ ਹਫਤੇ ਬੰਦ ਰੱਖੀਆਂ ਗਈਆਂ ਸਨ </p>

<p>ਗੁਰਚਰਨ
ਸਿੰਘ ਫੇਰੂਰਾਏ ਵਿਰੁੱਧ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਦੇ ਕੇਸ ਦਰਜ</p>

<p>ਰਾਏਕੋਟ(ਸਾਂਝ
ਸਵੇਰਾ):- ਵਿਜੀਲੈਂਸ ਬਿਊਰੋ ਵੱਲੋਂ ਸਾਬਕਾ ਐਸ.ਐਸ.ਪੀ. ਗੁਰਚਰਨ ਸਿੰਘ ਫੇਰੂਰਾਏ ਦੇ ਪੱੁਤਰ
ਅਤੇ ਧੀ ਵੱਲੋਂ ਵੱਢੀ ਦੇ ਕੇ ਪੰਜਾਬ ਲੋਕ ਸੇਵਾ ਕਮਿਸ਼ਨ ਰਾਹੀਂ ਨੌਕਰੀ ਹਾਸਲ ਕਰਨ ਦੇ ਦੋਸ਼ ਅਧੀਨ
ਕੇਸ ਦਰਜ ਕਰਨ ਤੋਂ ਦੋ ਦਿਨ ਬਾਅਦ ਇਥੋਂ ਦੀ ਪੁਲੀਸ ਨੇ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਰੋਕੂ ਕਾਨੂੰਨ ਅਧੀਨ ਸ਼੍ਰੀ
ਫੇਰੂਰਾਏ ਵਿਰੁੱਧ ਕੇਸ ਦਰਜ ਕੀਤਾ ਹੈ। </p>

<p>ਭੱਠਲ
ਵਲੋਂ ਪ੍ਰਵਾਸੀ ਪੰਜਾਬੀਆਂ ਨੰੂ ਪੰਜਾਬ ਵਿੱਚ ਵਪਾਰ ਕਰਨ ਦਾ ਸੱਦਾ</p>

<p>ਵੈਨਕੂਵਰ (ਸਾਂਝ
ਸਵੇਰਾ):- 'ਮੌਕਾਪ੍ਰਸਤ ਸਿਆਸਤਦਾਨਾਂ ਅਤੇ
ਭਾਰਤ ਵਿਰੋਧੀ ਸ਼ਕਤੀਆਂ ਦੇ ਹੱਥ ਠੋਕਿਆਂ ਕਾਰਨ ਮੰਦਹਾਲੀ ਦੇ ਮੂੰਹ ਪਏ ਪੰਜਾਬ ਨੂੰ ਮੁੜ ਲੀਹਾਂ ਤੇ
ਲਿਆਉਣ ਕਈ ਪ੍ਰਵਾਸੀ ਪੰਜਾਬੀਆਂ ਨੂੰ ਜ਼ੋਰਦਾਰ ਹੰਭਲਾ ਮਾਰਨਾ ਚਾਹੀਦਾ ਹੈ। ਸੀਨੀਅਰ ਕਾਂਗਰਸੀ
ਆਗੂ ਅਤੇ ਪੰਜਾਬ ਦੀ ਖੇਤੀਬਾੜੀ ਮੰਤਰੀ ਬੀਬੀ ਰਾਜਿੰਦਰ ਕੌਰ ਭੱਠਲ ਨੇ ਇਥੇ ਵੈਨਕੂਵਰ ਦੇ ਸਾਊਥ
ਇਲਾਕੇ ਵਿੱਚ ਪੰਜਾਬੀਆਂ ਦੇ ਭਾਰੀ ਇੱਕਠ ਨੂੰ ਸੰਬੋਧਨ ਕਰਦਿਆਂ ਕਹੀ। </p>

<p>ਮਾਲਟਨ
(ਕੈਨੇਡਾ) 'ਚ ਨਗਰ ਕੀਰਤਨ ਹੋਇਆ</p>

<p>ਮਾਲਟਨ(ਕੈਨੇਡਾ)
:-(ਸਾਂਝ ਸਵੇਰਾ) ਧ ੰਨ ਧੰਨ ਸ਼੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਜੀ ਦੇ ਪਹਿਲੇ ਪ੍ਰਕਾਸ਼ ਦੀ ਖੁਸ਼ੀ ਵਿੱਚ
ਕੈਨੇਡਾ ਦੇ ਸੰਘਣੀ ਸਿੱਖ ਵਸੋਂ ਵਾਲੇ ਇਲਾਕੇ ਮਾਲਟਨ ਵਿੱਚ ਟਰਾਂਟੋ ਇਲਾਕੇ ਦਾ ਦੂਸਰਾ ਨਗਰ
ਕੀਰਤਨ ਕੱਲ੍ਹ ਐਤਵਾਰ ਨੰੂ ਬੜੀ ਧੂਮ ਧਾਮ ਨਾਲ ਹੋਇਆ। ਇਸ ਮੌਕੇ ਉਚੇਚੇ ਤੌਰ ਤੇ ਦਮਦਮੀ
ਟਕਸਾਲ ਦੇ ਮੁੱਖੀ ਬਾਬਾ ਠਾਕੁਰ ਸਿੰਘ ਅਤੇ ਬਾਬਾ ਮਾਨ ਸਿੰਘ ਪਿਹੋਵੇ ਵਾਲਿਆਂ ਨੇ ਸ਼ਮੂਲੀਅਤ ਕੀਤੀ।
ਇਸ ਤੋਂ ਇਲਾਵਾ ਸ਼੍ਰੀ ਗੁਰੂ ਗੋਬਿੰਦ ਸਿੰਘ ਜੀ ਮਹਾਰਾਜ਼ ਜੀ ਨਾਲ ਸਬੰਧਤ "ਗੰਗਾ
ਸਾਗਰ" ਦੇ ਵੀ ਸੰਗਤ ਨੇ ਦਰਸ਼ਨ ਕੀਤੇ। </p>

<p> </p>






</body></text></cesDoc>