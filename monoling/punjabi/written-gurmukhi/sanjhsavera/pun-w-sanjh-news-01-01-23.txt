<cesDoc id="pun-w-sanjh-news-01-01-23" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-01-01-23.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 01-01-23</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>01-01-23</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਮੰਗਲਵਾਰ, ਜਨਵਰੀ 23, 2001</p>

<p>ਮੇਰੇ ਬਿਆਨ ਨੂੰ ਤੋੜ ਮਰੋੜ ਕੇ ਪੇਸ਼ ਕੀਤਾ ਗਿਆ : ਪ੍ਰੋ ਮਨਜੀਤ ਸਿੰਘ</p>

<p>ਆਨੰਦਪੁਰ
ਸਾਹਿਬ: ਤਖਤ ਸ੍ਰੀ ਕੇਸਗੜ੍ਹ ਸਾਹਿਬ ਦੇ ਜਥੇਦਾਰ ਪ੍ਰੋ: ਮਨਜੀਤ ਸਿੰਘ ਨੇ ਆਨੰਦਪੁਰ ਵਿਖੇ ਸ਼੍ਰੀ ਗੁਰੂ
ਹਰਿ ਰਾਏ ਸਾਹਿਬ ਦੀ ਯਾਦ ਵਿਚ ਸਥਾਪਤ ਕੀਤੇ ਨਸ਼ਾ ਛੁਡਾਊ ਹਸਪਤਾਲ ਵਿਖੇ ਗੱਲਬਾਤ ਕਰਦਿਆਂ ਕਿਹ
ਕਿ ਅੱਜ ਸੰਸਾਰ ਭਰ ਵਿਚ ਨੌਜਵਾਨ ਅਤਿ ਖਤਰਨਾਕ ਪ੍ਰਕਾਰ ਦੇ ਨਸ਼ਿਆਂ ਵਿਚ ਗਲਤਾਨ ਹੋ ਕੇ ਆਪਣੇ ਜੀਵਨ
ਨਾਲ ਖਿਲਵਾੜ ਕਰ ਰਹੀ ਹੈ। ਇਸ ਲਈ ਸਮਾਜ ਦਾ ਹਰ ਵਰਗ ਭਾਵੇਂ ਸਰਕਾਰਾਂ ਹੋਣ ਜਾਂ ਪੁਲਿਸ ਅਧਿਕਾਰੀ
ਜਾਂ ਸਕੂਲਾਂ-ਕਾਲਜਾਂ ਵਿਚਲਾ ਮਾਹੌਲ ਹੋਵੇ। ਇਕ ਪ੍ਰਕਾਰ ਦੇ ਜਿੰਮੇਵਾਰ ਸਮਝੇ ਜਾ ਸਕਦੇ ਹਨ। ਖਾਲਸੇ
ਦੀ ਜਨਮ ਭੂਮੀ ਸ਼੍ਰੀ ਆਨੰਦਪੁਰ ਸਾਹਿਬ ਤੋਂ ਇਕ ਵੱਡੇ ਪੱਧਰ 'ਤੇ ਨਸ਼ਾ ਵਿਰੋਧੀ ਮੁਹਿੰਮ ਸ਼ੁਰੂ ਕੀਤੀ ਗਈ ਹੈ ਜੋ ਪੰਜਾਬ ਦੀ
ਜਵਾਨੀ ਨੂੰ ਇਸ ਤੋਂ ਮੁਕਤ ਕਰਾਉਣ ਲਈ ਹਰ ਯਤਨ ਉਪਰਾਲਾ ਕਰੇਗੀ। ਪ੍ਰੋ: ਮਨਜੀਤ ਸਿੰਘ ਨੇ ਦੁੱਖ
ਪ੍ਰਗਟ ਕਰਦਿਆਂ ਕਿਹਜ ਕਿ ਬੀਤੇ ਦਿਨ ਜਲੰਧਰ ਵਿਖੇ ਅਰਬਨ ਇਸਟੇਟ ਕਿਹਾ ਕਿ ਬੀਤੇ ਦਿਨ ਜਲੰਧਰ
ਸੈਂਟਰ ਦਾ ਉਦਘਾਟਨ ਵਾਲੇ ਸਮਾਗਮ ਸਮੇਂ ਕੁਝ ਸਿੱਖ ਵਿਰੋਧੀ ਸ਼ਕਤੀਆਂ ਨੇ ਮੇਰੇ 'ਤੇ ਸ਼੍ਰੀ ਅਕਾਲ ਤਖਤ ਦੇ ਜਥੇਦਾਰ ਗਿ:
ਜੋਗਿੰਦਰ ਸਿੰਘ ਵੇਦਾਂਤੀ ਦੇ ਬਿਆਨ ਨੂੰ ਤੌੜ ਮਰੋੜ ਕੇ ਪੇਸ਼ ਕੀਤਾ ਸੀ। ੳੇੁਨ੍ਹਾਂ ਸਪਸ਼ੱਟ ਕਰਦਿਆਂ
ਫਿਰ ਦੁਹਰਾਇਆ ਕਿ ਨਸ਼ਿਆਂ ਦੇ ਰੁਝਾਨ ਵਧਾਉਣ ਵਿਚ ਸਮਾਜ ਦੀਆਂ ਸਭ ਧਿਰਾਂ ਜਿੰਮੇਵਾਰ ਹਨ ਨਾਂਕਿ
ਪੰਜਾਬ ਦੇ ਮੁੱਖ ਮੰਤਰੀ ਸ: ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਦੀ ਸਰਕਾਰ ਜਿੰਮੇਵਾਰ ਹੈ। ਉਨ੍ਹਾਂ ਨੇ ਸਿੱਖ ਜਗਤ
ਨੁੰੂ ਸੁਚੇਤ ਕਰਦਿਆਂ ਕਿਹਾ ਕਿ ਉਹ ਅਜਿਹੀ ਕਾਰਵਾਈਆਂ ਤੋਂ ਸੁਚੇਤ ਰਹਿਣ। ਤਖਤ ਸ੍ਰੀ ਕੇਸਗੜ੍ਹ
ਸਾਹਿਬ ਦੇ ਜਥੇਦਾਰ ਪ੍ਰੋ: ਮਨਜੀਤ ਸਿੰਘ ਨੇ ਸਮੂਹ ਸਿੱਖ ਪ੍ਰੇਮੀਆਂ, ਪੰਜਾਬੀ ਪ੍ਰੇਮੀਆਂ ਨੂੰ ਅਪੀਲ
ਕੀਤੀ ਹੈ ਕਿ ਉਹ ਨਸ਼ਿਆਂ ਵਿਚ ਗ੍ਰਸੀ ਹੋਈ ਨੌਜਵਾਨੀ ਨੂੰ ਕੱਢਣ ਲਈ ਹਰ ਸੰਭਵ ਉਪਰਾਲਾ ਕਰਨ।</p>

<p>ਮੇਰੀ ਜਾਨ ਨੂੰ ਅਜੇ ਵੀ ਖਤਰਾ : ਮਿੱਠੂ</p>

<p>ਜਗਰਾਉਂ
: ਬਹੁਚਰਚਿਤ ਜੱਸੀ-ਮਿੱਠੂ ਪ੍ਰੇਮ ਕਹਾਣੀ ਜਿਸ ਦੀ ਨਾਇਕਾ ਜੱਸੀ ਆਪਣੇ ਇਸ਼ਕ ਨੂੰ ਸਿਰੇ ਚਾੜ੍ਹਨ
ਲਈ ਸੱਤ ਸਮੁੰਦਰਂ ਪਾਰ ਆ ਕੇ ਪ੍ਰੇਮ ਦੇ ਦੁਸ਼ਮਣਾਂ ਦੇ ਹੱਥਾ ਵਿਚ ਇਸ਼ਕ ਦੀ ਬਲੀ ਚੜ੍ਹ ਗਈ ਸੀ, ਦੇ
ਪ੍ਰੇਮੀ ਮਿੱਠੂ ਨੇ ਜੱਸੀ ਦੇ ਕਤਲ ਵਿਚ ਨਾਮਜ਼ਦ ਦੋਸ਼ੀ ਪੰਜਾਬ ਪੁਲਿਸ ਦੇ ਹੌਲਦਾਰ ਜੁਗਿੰਦਰ ਸਿੰਘ ਦੀ
ਗ੍ਰਿਫਤਾਰੀ 'ਤੇ ਭਾਵੇਂ
ਸਤੁੰਸ਼ਟੀ ਪ੍ਰਗਟਾਈ ਹੈ ਪ੍ਰੰਤੂ ਇਸ ਦੇ ਨਾਲ ਹੀ ਮਿੱਠੂ ਆਪਣੀ ਜਾਨ ਨੂੰ ਪੈਦਾ ਹੋਏ ਖਤਰੇ ਨੂੰ
ਘੱਟ ਹੋਇਆ ਮਹਿਸੂਸ ਨਹੀਂ ਕਰਦਾ। ਅੱਜ ਜਗਰਾਉਂ ਵਿਖੇ ਪੱਤਰਕਾਰਾਂ ਨਾਲ ਗੱਲਬਾਤ ਕਰਦਿਆਂ
ਸੁਖਵਿੰਦਰ ਸਿੰਘ ਮਿਠੂ ਨੇ ਕਿਹਾ ' ਪਿਛਲੇ ਸਾਲ 8 ਜੂਨ 1999 ਨੂੰ ਦੇ ਉਸ ਦਿਨ ਤੋਂ ਜਿਸ ਦਿਨ ਮੇਰੀ ਪਤਨੀ ਜਸਵਿੰਦਰ ਕੌਰ
ਜੱਸੀ ਤੇ ਮੈਨੂੰਮ ਜੱਸੀ ਦੀ ਮਾਤਾ ਮਲਕੀਤ ਕੌਰ ਤੇ ਮਾਮਾ ਸੁਰਜੀਤ ਸਿੰਘ ਬਦੇਸ਼ਾ ਦੁਆਰਾ ਦਿੱਤੀ
ਗਈ 'ਸੁਪਾਰੀ' ਕਾਰਨ ਗੁੰਡਾ ਅਨਸਰਾਂ ਵੱਲੋਂ ਮਾਲੇਰਕੋਟਲਾ
ਤੋਂ ਨਾਰਕੇ ਜਾਂਦਿਆਂ ਮੈਨੂੰ ਗੰਭੀਰ ਜ਼ਖਮੀ ਕਰ ਦਿੱਤਾ ਗਿਆ ਸੀ ਅਤੇ ਜੱਸੀ ਨੂੰ ਚੁੱਕ ਕੇ ਕਤਲ ਕਰ
ਦਿੱਤਾ ਗਿਆ ਸੀ, ਮੈਂ ਆਪਣੇ ਆਪ ਨੂੰ ਕਦੇ ਵੀ ਕਿਸੇ ਪਲ ਲਾਲਚ ਤੇ ਧਮਕੀਆਂ ਭਰੇ ਫੋਨਾਂ 'ਤੇ ਸੁਨੇਹਿਆਂ ਨੇ ਮਾਨਸਿਕ ਰੂਪ ਵਿਚ
ਦੁੱਖੀ ਕਰੀ ਰੱਖਿਆ ਹੈ ਤੇ ਮੇਰੇ ਘਰ ਤੇ ਫਾਇਰਿੰਗ ਤੱਕ ਹੋ ਚੁੱਕੀ ਹੈ। ਬੀਤੇ ਦਿਨ ਅਖਬਾਰਾਂ ਵਿਚ
ਛੱਪੀਆਂ ਖਬਰਾਂ ਕਿ ਜੱਸੀ ਦੇ ਮਾਮੇ ਸੁਰਜੀਤ ਸਿੰਘ ਦੇ ਬਰਨਾਲਾ ਤੋਂ ਵਿਧਾਇਕ ਸ: ਮਲਕੀਤ ਸਿੰਘ ਦੇ
ਬਰਨਾਲਾ ਤੌਂ ਵਿਧਾਇਕ ਸ: ਮਲਕੀਤ ਸਿੰਘ ਕੀਤੂ ਨਾਲ ਨਜ਼ਦੀਕੀ ਸਬੰਧ ਹਨ ਅਤੇ ਇਨ੍ਹਾਂ ਹੀ ਸੰਬੰਧਾਂ
ਨੂੰ ਵਰਤਦਿਆਂ ਜੱਸੀ ਨੇ ਮਾਮੇ ਨੇ ਕੈਨੇਡਾ ਵਿਚ ਦੇਣ ਦੇ ਯਤਨ ਕੀਤੇ ਸਨ, ਬਾਰੇ ਬੋਲਦਿਆਂ ਮਿੱਠੂ
ਨੇ ਕਿਹਾ ਅੱਜ ਉਹ ਵਿਧਾਇਕ ਮਲਕੀਤ ਸਿੰਘ ਕੀਤੂ ਦੀ ਬਦੌਲਤ ਹੀ ਜਿੰਦਾ ਹੈ ਜਿਸ ਨੇ ਲੋੜ ਵੇਲੇ
ਉਸਦੀ ਹਮੇਸ਼ਾ ਮਦਦ ਕੀਤੀ ਹੈ। ਮਿੱਠੂ ਨੇ ਆਖਿਆ ਕਿ ਜਦੋਂ ਤੱਕ ਜੱਸੀ ਕਤਲ ਕਾਂਡ ਦੇ ਮੁੱਖ ਦੋਸ਼ੀ
ਉਸਦੀ ਮਾਂ ਮਲਕੀਤ ਕੌਰ ਤੇ ਉਸਦਾ ਮਾਮਾ ਸੁਰਜੀਤ ਸਿੰਘ ਗ੍ਰਿਫਤਾਰ ਨਹੀਂ ਹੁੰਦੇ, ਉਦੋਂ ਤੱਕ ਉਹ
ਆਪਣੀ ਜਾਨ ਪੂਰਾ ਖਤਰਾ ਮਹਿਸੂਸ ਕਰਦਾ ਹੈ। ਉਸਨੇ ਜਾਗਰਾਉਂ ਪੁਲਿਸ ਦੇ ਵਰਤਾਓ ਤੇ ਭਾਰੀ ਰੋਸ
ਪ੍ਰਗਟ ਕਰਦਿਆਂ ਕਿਹਾ ਕਿ ਜੇਕਰ ਜਾਗਰਾਓ ਪੁਲਿਸ ਸਹੀ ਰੋਲ ਅਦਾ ਕਰਦੀ ਤਾਂ ਜੱਸੀ ਕਤਲ ਨਹੀਂ ਸੀ ਹੋ
ਸਕਦਾ। ਉਸਨੇ ਜਾਗਰਾਓ ਪੁਲਿਸ ਦੇ ਵਰਤਾਓ ਤੇ ਬਾਰੀ ਰੋਸ ਪ੍ਰਗਟ ਕਰਦਿਆਂ ਕਿਹਾ ਕਿ ਜਗਰਾਓ ਪੁਲਿਸ
ਸਹੀ ਰੋਲ ਅਦਾ ਨਹੀ ਕਰਦੀ ਤਾਂ ਜੱਸੀ ਦਾ ਕਤਲ ਨਹੀ ਹੋ ਸਕਦਾ। ਉਸਨੇ ਦੱਸਿਆਂ ਕਿ ਅੱਜ ਤਕ ਜਦੋਂ
ਉਸਦੇ ਦੋਸਤਾਂ ਤੇ ਰਿਸ਼ਤੇਦਾਰਾਂ ਤੱਕ ਨੂੰ ਧਮਕੀਆਂ ਦਿੱਤੀਆਂ ਜਾ ਰਹੀਆਂ ਹਨ, ਉਸਦੇ ਘਰ'ਤੇ 23 ਨਵੰਬਰ ਨੂੰ ਫਾਇਰਿੰਗ ਤੱਕ ਹੋ
ਚੁੱਕੀ ਨੂੰ ਤਦ ਵੀ ਜਗਰਾਓ ਪੁਲਿਸ ਉਸਨੂੰ ਸਵੈ-ਰੱਖਿਆ ਲਈ ਹਥਿਆਰ ਦੇਣ ਨੂੰਤਿਆਰ ਹੈ।</p>

<p>ਕਨਿਸ਼ਕ
ਕਾਂਡ ਵਿਚ ਰਾਅ ਦਾ ਹੱਥ : ਮਾਨ </p>

<p>ਬੰਗਾ, 22
ਜਨਵਰੀ - ਕਨਿਸ਼ਕ ਕਾਂਡ ਵਿਚ ਫੜੇ ਦੋ ਕੈਨੇਡੀਅਨ ਸਿੱਖਾਂ ਤੇ ਨਾਜ਼ਾਇਜ ਕੇਸ ਪਾਏ ਗਏ ਹਨ, ਕਿਉਂਕਿ
ਇਸ ਕਾਂਡ ਵਿਚ ਰਾਅ ਦਾ ਪੂਰਾ ਹੱਥ ਹੈ । ਇਹ ਵਿਚਾਰ ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ (ਅੰਮ੍ਰਿਤਸਰ) ਦੇ ਪ੍ਰਧਾਨ
ਤੇ ਸਭਾ ਮੈਂਬਰ ਸਿਮਰਨਜੀਤ ਸਿੰਘ ਮਾਨ ਨੇ ਇੱਥੋਂ ਦੇ ਦੋ ਕਿਲੋਮੀਟਰ ਦੂਰ ਪਿੰਡ ਪੂੰਨੀਆਂ ਵਿਖੇ
ਚਲਦਾ ਵਹੀਰ ਦੇ ਬਾਨੀ ਭਾਈ ਰਜਿੰਦਰ ਸਿੰਘ ਨੂੰ ਸ਼ਰਧਾਂਜਲੀ ਭੇਟ ਕਰਦਿਆਂ ਕਿਤੇ । </p>

<p>          aunH~ ikh` ies k~f b`ry EiDk`rIE~ nUM
pihl~ hI pq` sI ies krky aunH~ EiDk`rIE~ ny ies zh`j dIE~ E`pxIE~ itkt~ r@d
krv` leIE~ sn | aunH~ mMg kIqI ik zyl~ ivc ipCly 16 s`l~ qoN f@ky hz`r~ is@K
nOjv`n~ nUM irh`E kIq` j`vy ikauNik kql kys ivc vI sz` 14 s`l huMdI pr ienH~
nUM ibn~ kys cl`ieE~ hI zyl~ ivc 16 s`l ho gey hn | </p>

<p>          sRI m`n ny pMj`b srk`r qy q`bVqoV hmly
kridE~ ikh` ik ijn~ nuks`n iks`n~ d` ies srk`r vloN hoieE` hY, aun~ pihly kdy
nhIN hoieE` | aunH~ ikh` ik E`auNdIE~ bl`k sMmqI, izl` pRISd~, ivD`n sB` Eqy
sRomxI kmytI dIE~ cox~ aunH~ dI p`rtI iek@ly qor qy lVygI | aunH~ ikh` ik 25
jnvrI nUM jlMDr ivKy dl vloN rYlI kIqI j`vygI ijs ivc B`rI igxqI ivc is@K s`iml
hoxgy | sm`gm nUM Evq`r isMG pinE`lI, B`g isMG ropV, sRI svrn isMG pUMnIE~,
mohn isMG mskIn, inhMg EmrIk isMG, drb`r` isMG prIh`r Eqy srpMc kyhr isMG
pMUnIE~ ny vI sMboDn kIq` | ies auprMq sRI m`n ny B`eI rijMdr isMG dI w`d ivc
gyt bx`aux leI nINh p@Qr vI r@iKE` |   </p>

<p>ਮਾਣਹਾਨੀ
ਦੇ ਕੇਸ ਵਿਚ ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਅਤੇ ਜਸਪਾਲ ਸਿੰਘ ਢਿਲੋਂ ਨੂੰ ਸੰਮਨ ਜਾਰੀ, ਟਰਾਂਟੋ 'ਚ ਬੀਬੀ ਦੇ
ਚਹੇਤਿਆਂ ਨੰੂ ਬੇਨਕਾਬ ਕਰਨਾ ਜਰੂਰੀ </p>

<p>ਯੂ.ਟੀ. ਜੂਡੀਸਲ
ਮੈਜਿਸਟਰੇਟ ਸ੍ਰੀ ਨਵਲ ਕੁਮਾਰ ਨੇ ਸ੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਸਾਬਕਾ ਬੀਬੀ ਜਗੀਰ ਕੌਰ
ਅਦਾਲਤ ਅੱਗੇ ਪੇਸ ਹੋਣ ਲਈ 22 ਮਾਰਚ ਲਈ ਸੰਮਨ ਜਾਰੀ ਕੀਤੇ ਹਨ । ਅਦਾਲਤ ਨੇ ਕੇਸ ਦੇ ਇਕ ਹੋਰ
ਮੁਲਜ਼ਮ ਜਸਪਾਲ ਸਿੰਘ ਢਿੱਲੋਂ ਨੂੰ ਨਾਲ ਹੀ ਸੰਮਨ ਜਾਰੀ ਕੀਤੇ ਹਨ । ਗੁਰੂ ਆਸਰਾ ਟਰੱਸਟ ਦੀ
ਪ੍ਰਧਾਨ ਸ੍ਰੀਮਤੀ ਕੁਲਬੀਰ ਕੌਰ ਧਾਮੀ ਨੇ ਬੀਬੀ ਵਿਰੁੱਧ ਮਾਣਹਾਨੀ ਦਾ ਕੇਸ ਕੀਤਾ ਹੋਇਆ ਹੈ । </p>

<p>          pihl~ Ed`lq ny bIbI nUM pys hox leI
E@j v`sqy noits id@q` sI pr noits q`mIl hox dI irport hox dI Ed`lq nUM nhI imlI
ies krky E@j dub`r` sMmn j`rI kIqy gey | sRImqI D`mI ny Ed`lq nUM id@qI ErzI ivc
dos l`ieE` sI ik bIbI jgIr kOr ny aus ivru@D dys ivdys ivc ikq`bc` vMifE` ijs
n`l aunH~ dy vk`r nUM B`rI s@t l@gI | ieh ikq`bc` sRI iF@loN r`hIN pRk`isq kIq`
igE` sI |          iSk`ieqkrq` ny Ed`lq
nUM idqI ErzI ivc d@isE` ik auh muh`lI ivc iek tr@st cl` rhI hY ijs ivc 1984
isK kqlyE`m Eqy puils vDIkIE~ k`rn En`Q hoey b@icE~ d` muPq p`lx posx kIq` j~d`
hY | ptIsnr Enus`r SRI iF@lo vloN pRk`isq ikq`bcy k`rn aunh~ dI
byvj`h bdn`mI hoeI ies leI sRI iF@loN ivru@D vI k`rv`eI kIqI j`vy |</p>

<p>ਇਥੇ ਇਹ
ਵਰਨਣਯੋਗ ਹੈ ਕਿ ਇਹੀ ਕਿਤਾਬਚਾ ਟਰਾਂਟੋ ਵਿੱਚ ਮੌਜੂਦ ਬੀਬੀ ਦੇ ਕੱੁਝ ਚਹੇਤੇ ਟਰਾਂਟੋ ਵਿਚ ਕਈ
ਵਾਰ ਵੰਡ ਚੱੁਕੇ ਹਨ। ਇਨ੍ਹਾਂ ਘੜਮ ਚੌਧਰੀਆਂ ਨੇ ਜਦੋਂ ਦੀ ਟਰਾਂਟੋ 'ਚ ਗੁਰ ਆਸਰਾ ਫਉਂਡੇਸ਼ਨ ਹੋਂਦ
ਵਿਚ ਆਈ ਹੈ, ਟਰੱਸਟ ਵਿਰੁੱਧ ਆਪਣੀਆਂ ਸਰਗਰਮੀਆਂ ਤੇਜ਼ ਕੀਤੀਆਂ ਹੋਈਆਂ ਹਨ। ਪਿਛਲੇ ਦਿਨੀਂ
ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਦੇ ਇਨ੍ਹਾਂ ਚੇਲਿਆਂ ਨੇ ਇਹ ਕਿਤਾਬਚਾ ਟਰਾਂਟੋ ਵਿਚ ਪ੍ਰਕਾਸ਼ਤ ਹੁੰਦੀਆਂ ਅਖਬਾਰਾਂ ਦੇ
ਦਫਤਰਾਂ ਮੂਹਰੇ ਰਾਤ ਬਰਾਤੇ ਰੱਖ ਦਿੱਤਾ ਸੀ ਪਰ ਬੇਸ਼ਰਮਾਂ ਦੀ ਇੱਕ ਵੀ ਅਖਬਾਰ ਨੇ ਮੰਨ ਨੇ ਇਸ ਦੀ
ਖਬਰ ਨਹੀਂ ਛਾਪੀ। ਅਦਾਰਾ ਸਾਂਝ ਸਵੇਰਾ, ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਦੇ ਇਨ੍ਹਾਂ ਭਗਤਾਂ ਦਾ ਪਰਦਾ ਫਾਸ਼ ਕਰਨ ਲਈ
ਪੂਰੀ ਵਾਹ ਲਾ ਰਿਹਾ ਹੈ ਤਾਂ ਕਿ ਸੰਗਤ ਸਾਹਮਣੇ ਇਨ੍ਹਾਂ ਸਿੱਖ ਵਿਰੋਧੀ ਅਨਸਰਾਂ ਨੰੂ ਨੰਗਿਆਂ ਕੀਤਾ
ਜਾ ਸਕੇ। ਸੁਰਾਖ ਮਿਲੇ ਹਨ ਕਿ ਇਹ ਘੜੱਮ ਚੌਧਰੀ ਟਰਾਂਟੋ ਦੀ ਸਿਆਸਤ ਵਿਚ ਕਾਫੀ ਸਰਗਰਮ ਹਨ।
</p>

<p>ਨਜ਼ਰਬੰਦ ਸਿੱਖ ਨੌਜਵਾਨਾਂ ਦੀ ਰਿਹਾਈ ਦੇ ਆਸਾਰ </p>

<p>*ਗ੍ਰਹਿ
ਮੰਤਰਾਲੇ ਤੇ ਪੰਜਾਬ ਸਰਕਾਰ ਦਰਮਿਆਨ ਵਿਚਾਰ-ਵਾਰਤਾ ਜ਼ੋਰਾਂ ਤੇ </p>

<p>ਚੰਡੀਗੜ੍ਹ,
22 ਜਨਵਰੀ - ਕੇਂਦਰੀ ਗ੍ਰਹਿ ਮੰਤਰਾਲਾ ਦੇਸ ਦੀਆਂ ਵੱਖ ਵੱਖ ਜ਼ੇਲਾਂ ਵਿਚ ਬੰਦ ਸਿੱਖ ਨੌਜਵਾਨਾਂ ਦੀ
ਰਿਹਾਈ ਪੰਜਾਬ ਸਰਕਾਰ ਨਾਲ ਵਿਚਾਰ ਵਟਾਂਦਰਾ ਕਰ ਰਿਹਾ ਹੈ । ਇਂਨ੍ਹਾਂ ਨੌਜਵਾਨਾਂ ਵਿਚ ਖਾੜਕੂ ਵੀ
ਸਾਮਿਲ ਹਨ ਪੰਜਾਬ ਸਰਕਾਰ ਨੇ ਆਪਣੇ ਤੋਰ ਉਤੇ ਹੀ ਪੰਜਾਬ ਦੇ ਮੁਖ ਸਕੱਤਰ ਆਰ.ਐਸ.ਮਾਨ ਦੀ
ਅਗਵਾਈ ਹੇਠ ਕਮੇਟੀ ਬਣਾ ਰੱਖੀ ਹੈ ਅਤੇ ਇਸ ਕਮੇਟੀ ਵਿਚ ਪ੍ਰਮੁੱਖ ਸਕੱਤਰ ਅਤੇ ਪੁਲਿਸ ਦੇ
ਡਾਇਰੈਕਟਰ ਜਨਰਲ ਮੈਂਬਰ ਹਨ । ਇਹ ਕਮੇਟੀ ਟਾਡਾ ਕੇਸਾਂ ਦਾ ਸਮਾ ਬੱਧ ਰੀਵਿਊ ਕਰਦੀ ਰਹਿੰਦੀ ਹੈ
। ਇਸ ਸੌ ਵਧ ਸਿੱਖ ਨੌਜਵਾਨ ਜਿਨਾਂ ਵਿਚ ਖਾੜਕੂ ਵੀ ਸਾਮਿਲ ਹਨ ਜਾਂ ਤਾਂ ਵੱਖ ਵੱਖ ਕੇਸਾਂ ਵਿਚ
ਮੁਕੱਦਮੇ ਦਾ ਸਾਹਮਣਾ ਕਰ ਰਹੇ ਹਨ, ਜਾਂ ਫਿਰ ਸਜ਼ਾ ਭੁਗਤ ਰਹੇ ਹਨ । </p>

<p>          ies vyly pMj`b dIE~ zyl~ ivc 70 nzrbMd
j~ hv`l`qI hn | aunH~ ivcoN kuJ auGy K`VkU vI hn ijn~ ivc gurcrn isMG g`m`,
dljIq isMG ib@tU, rySm isMG, mnijMdr isMG, igE`n isMG. inS`n isMMG kl`nOr Eqy
mnjIq isMG aurP l`l isMG dy n~E vrxnwog hn | </p>

<p>          kuJ sm~ pihl` kyNdrI gRih mMqrI l`l
ikRSn Efv`nI ny sMkyq idq` sI ik Eijhy s`ry nOjv`n irh`E kIqy j` skdy hn |
pMj`b dIE~ zyl~ qoN iel`v` kuJ is@K nOjv`n r`ijsQ`n, jMmU , iqh`V zyl Eqy bUfYl
zyl ivc vI bMd hn | ienH~ nOjv`n~ dI igxqI 50 hY | </p>

<p>          sUqr~ ny d@isE` ik pMj`b ivc E`m vrgy
h`l`q pYd` hox dy kuJ sm~ ip@CoN 500 n`loN v@D K`VkUE~ nUM zyl~ qo irh`E kr
idq` igE` sI | j~ zm`nq auqy C@f idqy gey sn | v@K v@K izilE~ ivc puils nUM
hd`ieq~ j`rI kr id@qIE~ geIE~ hn ik aunH~ dIE~ srgrmIE~ auqy nzr r@Ky | </p>

<p>          hv`l`qIE~ Eqy sz` Bugq rhy nOjv`n~ dI irh`eI
dy p@K ivc iek k`rn ieh d@isE` j` irh` hY ik auh pihl~ hI lMmI kYd Bugq cu@ky
hn Eqy auh k@tV K`VkUE~ v`lI EauD vI ivh` cu@ky hn | pMj`b dIE~ jyl~ ivc bMd
hv`l`qIE~ dI igxqI B`vyN krIb 25 hY Eqy dys hor ih@isE~ ivc siQq zyl~ ivc bMd
hv`l`qIE~ dI igxqI 10-15 hY pr aunH~ irh`E krn dy r`h ivc iek qknIkI sm@isE` E`
rhI hY | ieh sm@isE` ieh hY ik ienH~ ivcoN kuJ nUM sz` hoeI hoeI hY | hv`l`qIE~
dy sbMD ivc q~ r`j E`p hI aunH~ nUM E`m m`PI dy ky irh`E kr skd` hY pr ijn~H
nUM sz` ho cu@kI hY Eqy sz` k@t rhy hn aunH~ nUM irh`E krn leI iek v@KrI k`rj
ivDI Epn`auxI pvygI jo lMmI vI hY Eqy guMJld`r vI | zyl~ ivc is@K nOjv`n~ nUM
irh`E krn dI vDyry mMg r`strI is@K sMgq ny kIqI hY Eqy b`d ivc srbihMd sRomxI
Ek`lI dl smyq kuJ isE`sI p`rtIE~ ny ieh mMg auT` id@qI | srb ihMd sRomxI Ek`lI
dl ny q~ is@K nOjv`n~ dIE~ irh`eI leI kuJ Q`eIN rYlIE~ vI kIqIE~ | </p>

<p>          ivdys~
ivc rih ky kMm kridE~ kuJ is@K jQybMidE~ zyl~ ivc bMd is@K nOjv`n~ dy mu@dy nUM
lYky kyNdr srk`r Eqy pMj`b srk`r dI E`locn` krdIE~ E` rhIE~ hn | dys dIE~ zyl~
ivc bMd nOjv`n~ dI igxqI vD` cV`HE dy pys kIqI j~dI rhI hY aud`hrx vjoN pMj`b
dIE~ zyl~ ivc krIb 15000 kYdI r@Kx dI smr@Q` hY pr iek is@K jQybMdI ny h`l hI
ivc ikh` hY ik B`rq dIE~ zyl~ ivc 50,000 qoN v@D is@K  nOjv`n Ejy vI sVH rhy hn | </p>

<p>ਵਾਜਪਾਈ
ਤੇ ਬਾਦਲ ਸਰਕਾਰਾਂ ਕਿਸਾਨਾਂ ਦੀ ਆਰਥਿਕ ਤਬਾਹੀ ਦੀਆਂ ਜ਼ਿੰਮੇਵਾਰ : ਸਿਮਰਨਜੀਤ ਸਿੰਘ ਮਾਨ ਦਾ ਦੋਸ਼ </p>

<p>ਨਵਾਂ ਸਹਿਰ , 22
ਜਨਵਰੀ:    SRomxI Ek`lI dl(EimRqsr)
dy pRD`n Eqy mYNbr p`rlImYNt s. ismrnjIq isMG m`n ny E`iKE` ik E`r.EY@s.EY@s.
dIE~ srgrmIE~ Eqy sMs`r vp`r sMsQ` dy dyS dI E`riQkq` 'qy  m`rU Esr~ n`l dyS ivc isvl v`r l@gx d` ^qr`
pYd` ho igE` hY|aunH~ E`iKE` ik ijs qrH~ E`r.EYs.EYs. is@K Drm ivc d^l dy rhI
hY, ieh bhuq G`qk hY| gurduE`irE~ ivc r`m`iex d` p`T Eqy mMdr~ ivc sEI gurU
gRMQ s`ihb jI dy pRk`S krn dIE~ g@l~ krn` is@K Drm ivc is@DI d^lEMd`zI hY|
aunH~ ikh` ik b`brI msijd pihl~ v`lI jgH` qy bxnI c`hIdI hY Eqy r`m mMdr aus
qoN 15 gz dI dUrI qy bxn` c`hId` hY| </p>

<p>          sMs`r vp`r sMsQ` qy glb`q kridE~ SRI
m`n ny E`iKE` ik s`dy dyS nUM hux 10 s`l qk q~ sMs`r vp`r sMsQ` dy E`dyS mMnxy
pYxgy ikauNik 10 s`l  dy smyN leI smJOq`
ho cu@k` hY| aunH~ ikh` ik B`rq dy sMs`r vp`r sMsQ` d` KyqI E`riQkq` qy m`rU
Esr ipE` hY, aus leI byEMq isMG Eqy b`dl srk`r~ izmyv`r hn| ikauNik ienH~
srk`r~ ny n` hI iks`n~ nUM Eqy n` hI snEqk`r~ nUM muk`bly leI iqE`r kIq`| </p>

<p>          aunH~ ikh` im b`dl cox mYnIPYsto ivc
sMs`r vp`r sMsQ` d` ivroD drh mrn dI g@l E`Kdy hn| SRI m`n ny SRI b`dl nUM sv`l
kIq` ik jykr aunH~ dy mn ivc scmu@c hI pMj`b dy iks`n~ leI ipE`r hY q~ auh
pMj`b dy iks`n~ dy ih@q~ leI pRD`n mMqrI Etl ibh`rI v`jp`eI qy db`A ikauN nhIN
p`auNdy ? aun~ E`iKE` ik E~Dr` pRdyS dy m@uK mMqrI cMdr b`bU n`iefU ny pMj`b
qoN d@Kx j`x v`lIE~ ijns~ qy rok lg` id@qI hY jo ik pMj`b dy iks`n~ qy iks`nI
n`l DoK` hY|ies n`l d@Kx dy E~Dr` pRdyS,kyrl`, q`imln`fU nUM pMj`b d` En`j nhIN
j` skyg`| ies sm@isE` dy h@l leI SRI b`dl kuJ nhIN kr rhy| aunH~ KulHI mMfI
qihq p`iksq`n Eqy hor dyS~ n`l vp`r KolHx leI b`G` b`rfr nUM KolHx dI mMg
kridE~ E`iKE` ik ies n`l s`fIE~ KyqI ijxs~ Eqy hor ijxs~ nhIN rulxgIE~|          jMmU kSmIr sm@isE` b`ry aunH~ E`iKE`
ik jMmU- kSmIr ivc 1953 v`l` styts bh`l hox` c`hId` hY qy r`ieSum`rI hoxI
c`hIdI hY Eqy wU.EYn.A. nUM d^l dyx` c`hId` hY| </p>

<p>          aunH~ d@isE` ik p`rtI vloN doE`b` Kyqr
dI iek v@fI k`nPrMs 25 jnvrI nUM pRq`p b`Z jlMDr ivKy kIqI j` rhI hY,ijs ivc
kyNdr Eqy b`dl srk`r dIE~ is@K ivroDI qy pMj`b ivroDI nIqIE~ nUM nMg` kIq`
j`vyg`|aunH~ ikh` b`dl s`ihb s@q` sMB`ldy pMj`b dIE~ mMg~ Bul j~dy hn|qy cox~
vyly pMj`b dIE~ mMg~ w`d E` j~dIE~ hn| </p>

<p>ਬਾਦਲ
ਵੱਲੋਂ ਚੌਲ ਪ੍ਰੋਸੈਸਿੰਗ ਕੇਂਦਰ ਦੇ ਉਦਘਾਟਨ ਦੌਰਾਨ ਕਿਸਾਨਾਂ ਤੇ ਲਾਠੀਚਾਰਜ਼
</p>

<p>ਨੌਸ਼ਿਹਿਰਾ
ਪੰੰਨੂੰਆਂ  22 jnvrI :korIEn qkn`lojI n`l
sQ`ipq kIqy gey m`rkPYf dy plyTy r`eIs posYisMg kyNdr d` a`dG`tn E@j ieQy s:
pRk`S isMG b`dl  ny kridE~ ivSv vp`r
sMgTn n`l hoey smJOqy qy shI p`aux leI k~grs  srk`r nUM doSI Tihr`ieE`| </p>

<p>          ies sm`gm dOr`n iks`n sMGrS kmytI
v@loN hz`r~ hI iks`n~ ny b`dl srk`r dIE~ iks`n m`rU nIqIE~ qy Jony dI Ed`iegI
ivcoN ktOqI ivr@uD n`Eryb`zI kIqI q~ puils dy EYs.EYs.pI smyq puls ny iks`n~ qy
jord`r l`TIc`rz kIq`|b`Ed ivc s`ry iks`n E`gUE~ nUM ihr`sq ivc lYY ilE`|pr b`dl
s`ihb ny k~grs qy doS l`auNidE~ ikh` ik k~grs Eijhy lok~ nUM Byj ky Eijh` kMm
krv`auNdI hY| </p>

<p>          b`Ed ivc ies m`mly qy ros pRgt`aux leI
Eqy ihr`sq 'c ley E`gUE~ nUM Cuf`Ax leI iks`n~ ny srh`lI Q`xy d` Gyr`au
kIq`|pMj`b iks`n sB` dy E`gU k`mryf jgq`r isMG krmpur`, iks`n E`gU rqn isMG
rMD`v` , kMvlpRIq isMG pMnUM, pRgt isMG j`m`r`ey,bldyv isMG BYl,sqn`m isMG
idA,sivMdr isMG cuq`l` Eqy jgbIr isMG nUM ihr`sq ivc lYx dI inKyDI kIqI|</p>

<p> </p>

<p> </p>






</body></text></cesDoc>