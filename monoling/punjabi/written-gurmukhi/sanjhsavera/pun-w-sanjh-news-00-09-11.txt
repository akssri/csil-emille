<cesDoc id="pun-w-sanjh-news-00-09-11" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-00-09-11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 00-09-11</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>00-09-11</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>





 
  
  <p>ਫੂਨ 905-789-7787, ਫੈਕਸ: 905-789-7717 ਜਾਂ ਈ-ਮੇਲ ਕਰੋ  </p>
  
  
  <p>ਸਤੰਬਰ
  11, 2000</p>
  
 


<p>ਖਾਲੜਾ ਅਗਵਾ
ਕਾਂਡ ਦੇ ਚਸ਼ਮਦੀਦ ਗਵਾਹ ਸ੍ਰ: ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ ਪੰਜਾਬ ਸਰਕਾਰ ਦੀ ਕੁੜਿੱਕੀ ਵਿਚੋਂ ਰਿਹਾਅ</p>

<p>ਸ੍ਰੀ ਅੰਮ੍ਰਿਤਸਰ ਸਾਹਿਬ - ਪਿਛਲੇ ਦਿਨੀ
ਬ੍ਰਿਟਸ਼ ਹੋਮ ਸੈਕਟਰੀ ਜਾਰਜ ਸਟੇਅ ਦੀ ਸ੍ਰੀ ਦਰਬਾਰ ਸਾਹਿਬ ਫੇਰੀ ਸਮੇਂ ਪੁਲਿਸ ਅਧਿਕਾਰੀਆਂ ਦੀਆਂ
ਅੱਖਾਂ ਸਾਹਮਣੇ ਪੰਜਾਬ ਦੀਆਂ ਜ਼ੇਲ੍ਹਾਂ ਵਿਚ ਬੰਦੀ ਬਣਾਏ ਹੋਏ ਅਨੇਕਾਂ ਸਿੱਖਾਂ ਦੀ ਅਵਾਜ਼ ਨੰੂ ਇਕ
ਮੰਗ ਪੱਤਰ ਦੇ ਰੂਪ ਵਿਚ ਅੰਤਰਰਾਸ਼ਟਰੀ ਪੱਧਰ ਤੇ ਪਹੁੰਚਾਉਣ ਵਾਲੇ ਸ੍ਰ: ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ
ਉਪਰ ਪਾਏ ਕੇਸ ਦਾ ਉਸ ਵਕਤ ਨਿਪਟਾਰਾ ਹੋ ਗਿਆ ਜਦੋਂ ਪੰਜਾਬ ਪੁਲੀਸ ਨੇ ਉਸ ਦਾ ਹੋਰ ਅਦਾਲਤੀ
ਰਿਮਾਂਡ ਮੰਗਣ ਤੋਂ ਨਾਂਹ ਕਰ ਦਿੱਤੀ ਫ਼#145;ਤੇ ਕਿਹਾ ਕਿ ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ ਦੀ ਉਨ੍ਹਾਂ ਨੰੂ
ਅੰਮ੍ਰਿਤਸਰ ਬੈਂਕ ਡਕੈਤੀ ਕੇਸ ਵਿਚ ਲੋੜ ਨਹੀਂ ਹੈ । ਅਦਾਲਤ ਵਿਚ ਜੱਜ ਨੇ ਪੁਲਿਸ ਅਧਿਕਾਰੀਆਂ ਨੰੂ
ਟਿੱਚਰ ਭਰੇ ਲਹਿਜੇ ਵਿਚ ੀਕਹਾ ਕਿ ਉਸ ਦਾ ਹੋਰ ਰਿਮਾਂਡ ਲੈ ਸਕਦੇ ਹਨ । ਇਥੇ ਇਹ ਵਰਨਣਯੋਗ ਹੈ ਕਿ
ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ ਨੰੂ ਬ੍ਰਿਟਸ਼ ਹੋਮ ਸੈਕਰਟਰੀ ਫ਼#145;ਤੇ ਪੱਤਰਕਾਰਾਂ ਦੀ ਮੌਜਦੂਗੀ ਵਿਚ ਪੁਲੀਸ
ਦੀ ਜਿਪਸੀ ਵਿਚ ਸੁੱਟਿਆ ਗਿਆ ਸੀ ਫ਼#145;ਤੇ ਬਾਦ ਵਿਚ ਪੁਲੀਸ ਨੇ ਕੁਝ ਹਫਤੇ ਪਹਿਲਾਂ ਅੰਮ੍ਰਿਤਸਰ
ਵਿਚ ਹੋਏ ਇਕ ਬੈਂਕ ਡਕੈਤੀ ਕੇਸ ਜਿਸ ਵਿਚ ਮੈਨੇਜਰ ਫ਼#145;ਤੇ ਗਾਰਡ ਦੀ ਹੱਤਿਆ ਹੋਈ ਸੀ ਦੇ
ਦੋਸ਼ੀ ਵਜੋਂ ਕੇਸ ਪਾਇਆ ਗਿਆ ਸੀ । ਪ੍ਰਕਾਸ਼ ਬਾਦਲ ਦੇ ਅਖੌਤੀ ਇਨਸਾਫ ਪੰਸਦ ਰਾਜ ਵਿਚ ਉਸ ਦੇ
ਸਿਪਾਹਸਲਾਰਾਂ ਨੇ ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ ਨੰੂ ਇਕ ਖਤਰਨਾਕ ਅੱਤਵਾਦੀ ਬਣਾ ਕੇ ਪੇਸ਼ ਕਰਨ ਲਈ
ਅਖਬਾਰਾਂ ਵਿਚ ਆਪਣੇ ਏਜੰਟਾਂ ਰਾਹੀਂ ਉਸ ਨੰੂ ਬਾਬਾ ਗੁਰਬਚਨ ਸਿੰਘ ਮਾਨੋਚਾਹਲ ਦੇ ਪੈ੍ਰਸ ਸਕੱਤਰ
ਫ਼#145;ਤੇ ਖਾਲਿਸਤਾਨ ਜ਼ਿੰਦਾਬਾਦ ਨਾਂ ਦੀ ਇਕ ਖਾੜਕੂ ਜਥੇਬੰਦੀ ਦੇ ਨੇਤਾ ਵਜੋਂ ਪੇਸ਼ ਕੀਤਾ ਸੀ ।</p>

<p>ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ ਉਪਰ ਪਾਏ ਝੂਠੇ
ਕੇਸ ਦਾ ਐਮਨਸਟੀ ਇੰਟਰਨੈਸ਼ਨਲ ਨੇ ਸਖਤ ਨੋਟਿਸ ਲੈ ਕੇ ਹਿੰਦੋਸਤਾਨੀ ਗ੍ਰਹਿ ਮੰਤਰੀ ਲਾਲ ਕ੍ਰਿਸ਼ਨ
ਅਡਵਾਨੀ, ਪੰਜਾਬ ਦੇ ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਫ਼#145;ਤੇ ਪੰਜਾਬ ਪੁਲਿਸ ਮੁਖੀ ਸਰਬਜੀਤ
ਸਿੰਘ ਨੰੂ ਚਿੱਠੀਆਂ ਭੇਜੀਆਂ ਸਨ । ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ ਉਪਰ ਪਹਿਲਾਂ ਵੀ ਇਕ ਵਾਰ ਝੂਠਾ ਕੇਸ
ਪਾ ਕੇ ਉਸ ਨੰੂ ਕਾਨੰੂਨੀ ਜਕੜ ਵਿਚ ਫਸਾਉਣ ਦੀ ਕੋਸ਼ਿਸ਼ ਕੀਤੀ ਜਾ ਚੁੱਕੀ ਹੈ । ਦੇਸਾਂ ਬਦੇਸ਼ਾਂ
ਵਿਚੋਂ ਵੀ ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ ਦੇ ਹੱਕ ਵਿੱਚ ਚਿੱਠੀਆਂ ਦੀ ਮੁਹਿੰਮ ਇੱਕ ਦਿਨ ਵਿੱਚ ਹੀ ਜ਼ੋਰਾਂ
ਤੇ ਪੁੱਜ ਗਈ ਸੀ ਜਿਸ ਨਾਲ ਭਾਰਤ ਸਰਕਾਰ ਦਾ ਖੂਨੀ ਚਿਹਰਾ ਨੰਗਾ ਹੋਣਾ ਸ਼ੁਰੂ ਹੋ ਗਿਆ ਸੀ।
ਕੈਨੇਡਾ ਤੋਂ ਵਰਲਡ ਸਿੱਖ ਆਰਗੇਨਾਈਜੇਸ਼ਨ ਵਲੋਂ ਕੈਨੇਡਾ ਦੇ ਪ੍ਰਧਾਨ ਮੰਤਰੀ ਨੰੂ ਚਿੱਠੀ ਲਿੱਖ ਕੇ
ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ ਦੀ ਹਿਫਾਜ਼ਤ ਦੀ ਮੰਗ ਕੀਤੀ ਗਈ ਸੀ। ਇਸ ਤੋਂ ਇਲਾਵਾ ਇੰਗਲੈਂਡ ਵਿਚੋਂ
ਰਾਜਨੀਤਕਾਂ ਨੰੂ ਚਿੱਠੀਆਂ ਲਿੱਖੀਆਂ ਜਾ ਚੁੱਕੀਆਂ ਸਨ ਜਿਸ ਕਰਕੇ ਪੰਜਾਬ ਸਰਕਾਰ ਨੰੂ ਰਾਜੀਵ ਨੰੂ
ਛੱਡਣਾ ਪਿਆ। ਰਾਜੀਵ ਸਿੰਘ ਰੰਧਾਵਾ, ਸੰਤਬਰ 1995 ਵਿਚ ਪੰਜਾਬ ਪੁਲੀਸ ਵਲੋਂ ਸ੍ਰ: ਜਸੰਵਤ ਸਿੰਘ
ਖਾਲੜਾ ਨੰੂ ਅਗਵਾ ਕਰਨ ਦਾ ਚਸ਼ਮਦੀਦ ਗਵਾਹ ਹੈ । ਸ਼੍ਰ: ਜਸਵੰਤ ਸਿੰਘ ਖਾਲੜਾ ਨੰੂ ਉਸ ਵਕਤ ਦੇ
ਪੁਲੀਸ ਮੁਖੀ, ਕੇ. ਪੀ. ਐਸ. ਗਿੱਲ ਫ਼#145;ਤੇ ਸੀਨੀਅਰ ਪੁਲਿਸ ਅਧਿਕਾਰੀ ਅਜੀਤ ਸੰਧੂ ਦੀ ਸ਼ਹਿ
ਹੇਠ ਸ਼ਹੀਦ ਕਰ ਦਿੱਤਾ ਗਿਆ ਸੀ ਕਿਉਂਕਿ ਸ੍ਰ: ਖਾਲੜਾ ਨੇ ਪੰਜਾਬ ਦੀਆਂ ਸ਼ਮਸ਼ਾਨਘਾਟਾਂ ਵਿਚ ਜਾ ਕੇ
ਪੁਲਿਸ ਮੁਕਾਬਲਿਆਂ ਵਿਚ ਐਲਾਨੀਆਂ ਹੋਈਆਂ ਲਾਵਾਰਿਸ ਲਾਸ਼ਾਂ ਦੀ ਪਹਿਚਾਣ ਸਥਾਪਿਤ ਕਰਨ ਦਾ ਕੰਮ
ਸ਼ੁਰੂ ਕਰ ਦਿੱਤਾ ਸੀ । ਅਜੋਕੇ ਸਮੇਂ ਦੀ ਅਖੌਤੀ ਪੰਥਕ ਸਰਕਾਰ ਉਨ੍ਹਾਂ ਦੋਸ਼ੀ ਪੁਲਿਸ ਕਰਮਚਾਰੀਆਂ
ਨੰੂ ਬੇਕਸੂਰ ਸਿੱਧ ਕਰਵਾਉਣ ਲਈ ਮੁਕੱਦਮੇ ਲੜ ਰਹੀ ਹੈ ਫ਼#145;ਤੇ ਉਸ ਦੇ ਰਾਜ ਵਿਚ ਖਾਲੜਾ
ਅਗਾਵ ਕਾਂਡ ਦੇ ਚਸ਼ਮਦੀਦ ਗਵਾਹਾਂ ਨੰੂ ਪਰੇਸ਼ਾਨ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ । ਬਾਦਲ ਦੀ ਅਕਾਲੀ ਸਰਕਾਰ ਨੇ
ਪਿਛਲੇ ਦੋ ਦਹਾਕਿਆਂ ਦੌਰਾਨ ਵਾਪਰੇ ਦਹਿਸ਼ਤਵਾਦ ਦੀ ਜਾਂਚ ਕਰਵਾਉਣ, ਸਰਕਾਰੀ ਦਹਿਸ਼ਤਵਾਦ ਫੈਲਾਉਣ
ਵਾਲੇ ਪੁਲਿਸ ਅਧਿਕਾਰੀਆਂ ਨੰੂ ਸਜ਼ਾਵਾਂ, ਪੰਜਾਬ ਦੀਆਂ ਹੱਕੀ ਮੰਗਾਂ, ਸ੍ਰੀ ਆਨੰਦਪੁਰ ਸਾਹਿਬ ਦਾ
ਮਤਾ, ਜ਼ੇਲ੍ਹਾਂ ਵਿਚ ਕੈਦ ਬੇਕਸੂਰ ਸਿੱਖ ਨੌਜਵਾਨਾਂ ਦੀ ਰਿਹਾਈ ਆਦਿਕ ਮੰਗਾਂ ਨੰੂ ਵਿਸਾਰ ਕੇ
ਕੁਣਬਾਪ੍ਰਸਤੀ ਅਧੀਨ ਪੰਜਾਬ ਦੀ ਸਿਆਸਤ ਤੇ ਆਪਣੀ ਪਕੜ ਨੰੂ ਮਜ਼ਬੂਤ ਬਣਾਉਣਾ ਇਕੋ ਇਕ ਮਨੋਰਥ
ਬਣਾ ਲਿਆ ਹੈ ।</p>

<p>ਸਾਬਕਾ ਸਰਪੰਚ
ਦੀ ਪੁਲਿਸ ਹਿਰਾਸਤ ਵਿਚ ਮੌਤ </p>

<p>ਹੁਸ਼ਿਆਰਪੁਰ, 10 ਸਤੰਬਰ - ਥਾਣਾ
ਗੜ੍ਹਸ਼ੰਕਰ ਦੇ ਪਿੰਡ ਗੋਗੋ ਦੇ ਸਾਬਕਾ ਸਰਪੰਚ ਚੂਹੜ ਸਿੰਘ ਜਿਸ ਨੂੰ 6 ਸਤੰਬਰ ਨੂੰ ਪੁਲਿਸ ਨੇ 48
ਬੋਰੀਆਂ ਡੋਡੇ ਤੇ ਚੂਰਾ-ਪੋਸਤ ਸਮੇਤ ਗ੍ਰਿਫਤਾਰ ਕੀਤਾ ਸੀ, ਦੀ ਬੀਤੀ ਰਾਤ ਸਿਵਲ ਹਸਪਤਾਲ
ਹੁਸ਼ਿਆਰਪੁਰ ਵਿਖੇ ਮੌਤ ਹੋ ਗਈ । ਚੂਹੜ ਸਿੰਘ ਨੂੰ ਇਕ ਹੋਰ ਮੁਲਜ਼ਮ ਸਮੇਤ 7 ਸਤੰਬਰ ਨੂੰ
ਜਾਇੰਟ ਪੁੱਛਗਿੱਛ ਕੇਂਦਰ ਅੰਮ੍ਰਿਤਸਰ ਭੇਜਿਆ ਗਿਆ ਸੀ । 9 ਸਤੰਬਰ ਨੂੰ ਬਾਅਦ ਦੁਪਹਿਰ ਜਦੋਂ
ਅੰਮ੍ਰਿਤਸਰ ਤੋਂ ਹੁਸ਼ਿਆਰਪੁਰ ਲਿਆਂਦਾ ਜਾ ਰਿਹਾ ਸੀ ਤਾਂ ਰਸਤੇ ਵਿਚ ਉਸਨੇ ਸਿਹਤ ਖਰਾਬ ਹੋਣ ਦੀ
ਸ਼ਿਕਾਇਤ ਕੀਤੀ ਸੀ । ਉਸ ਨੂੰ ਸਿਵਲ ਹਸਪਤਾਲ ਹੁਸ਼ਿਆਰਪੁਰ ਲਿਜਾਇਆ ਗਿਆ ਜਿਥੇ ਰਾਤ 12.30
ਵਜੇ ਦੇ ਕਰੀਬ ਉਸ ਦੀ ਮੌਤ ਹੋ ਗਈ । ਮਾਹਿਰ ਡਾਕਟਰਾਂ ਦੀ ਟੀਮ ਵਲੋਂ ਮ੍ਰਿਤਕ ਦਾ ਪੋਸਟਮਾਰਟਮ
ਕਰਵਾਇਆ ਗਿਆ ਅਤੇ ਵੀਡਿਓ ਫਿਲਮ ਵੀ ਤਿਆਰ ਕੀਤੀ ਗਈ । ਮ੍ਰਿਤਕ ਦੇ ਪਰਿਵਾਰ ਨੇ ਇਲਜ਼ਾਮ
ਲਾਇਆ ਹੈ ਕਿ ਉਸ ਦੀ ਮੌਤ ਪੁਲਿਸ ਤਸ਼ੱਦਦ ਕਾਰਨ ਹੋਈ । </p>

<p>ਪੁਲਿਸ ਅਨੁਸਾਰ 7 ਸਤੰਬਰ ਨੂੰ ਅੰਮ੍ਰਿਤਸਰ
ਲਿਜਾਣ ਤੋਂ ਪਹਿਲਾਂ ਚੂਹੜ ਸਿੰਘ ਦਾ ਮੈਡੀਕਲ ਕਰਵਾਇਆ ਗਿਆ ਸੀ । ਨਿਯਮਾਂ ਅਨੁਸਾਰ ਦੋਵਾਂ ਮੁਲਜ਼ਮਾਂ
ਨੂੰ ਮੈਜਿਸਟਰੇਟ ਦੇ ਸਾਹਮਣੇ ਪੇਸ਼ ਕਰਕੇ ਪੁਲਿਸ ਰਿਮਾਂਡ ਮਿਲਣ ਤੋਂ ਬਾਅਦ ਜਾਇੰਟ ਇੰਟੈਰੋਗੇਟਰ
ਸੈਂਟਰ ਅੰਮ੍ਰਿਤਸਰ ਭੇਜਿਆ ਗਿਆ ਸੀ । ਅੰਮ੍ਰਿਤਸਰ ਤੋਂ ਵਾਪਸ ਆਉਂਦਿਆਂ ਚੂਹੜ ਸਿੰਘ ਨੇ ਸਿਹਤ
ਠੀਕ ਨਾ ਹੋਣ ਬਾਰੇ ਦੱਸਿਆ ਤਾਂ ਉਸ ਨੂੰ ਸਿਵਲ ਹਸਪਤਾਲ ਪਹੁੰਚਾਇਆ ਗਿਆ । ਬੀਤੀ ਰਾਤ ਜਦੋਂ
ਉਸ ਦੀ ਮੌਤ ਹੋ ਗਈ ਤਾਂ ਇਸ ਦੀ ਸੂਚਨਾ ਤੁਰੰਤ ਜ਼ਿਲ੍ਹਾ ਮੈਜਿਸਟਰੇਟ ਨੂੰ ਦਿੱਤੀ ਗਈ ਜਿਨ੍ਹਾਂ ਨੇ
ਐਸ.ਡੀ.ਐਮ. ਜ਼ਿਲ੍ਹਾ ਹੁਸ਼ਿਆਰਪੁਰ ਨੂੰ ਮੌਤ ਦੇ ਕਾਰਨਾਂ ਦੀ ਜਾਂਚ ਕਰਨ ਦੇ ਹੁਕਮ ਦਿੱਤੇ ਹਨ ।
ਦੂਜੇ ਪਾਸੇ ਮ੍ਰਿਤਕ ਚੂਹੜ ਸਿੰਘ ਦੀ ਭੈਣ ਕਮਲ ਕੌਰ ਅਤੇ ਹਰਜਿੰਦਰ ਕੌਰ ਨੇ ਇਕ ਹਲਫੀਆ ਬਿਆਨ
ਰਾਹੀਂ ਦੋਸ਼ ਲਾਇਆ ਕਿ ਚੂਹੜ ਸਿੰਘ ਦੀ ਮੌਤ ਪੁਲਿਸ ਤਸ਼ੱਦਦ ਕਾਰਨ ਹੋਈ । ਉਨ੍ਹਾਂ ਇਹ ਵੀ ਦੋਸ਼
ਲਾਇਆ ਕਿ ਪੁਲਿਸ ਨੇ ਉਸ ਉਤੇ ਝੂਠਾ ਕੇਸ ਪਾਇਆ ਸੀ । ਉਨ੍ਹਾਂ ਮੰਗ ਕੀਤੀ ਕਿ ਇਸ ਕੇਸ ਦੀ
ਜਾਂਚ ਹਾਈ ਕੋਰਟ ਦੇ ਜੱਜ ਤੋਂ ਕਰਵਾਈ ਜਾਵੇ । ਵਰਨਣਯੋਗ ਹੈ ਕਿ ਚੂਹੜ ਸਿੰਘ ਦੇ ਖਿਲਾਫ
ਐਨ.ਡੀ.ਪੀ.ਐਸ. ਐਕਟ ਅਧੀਨ ਪਹਿਲਾਂ ਵੀ ਕਈ ਕੇਸ ਦਰਜ ਹੋ ਚੁੱਕੇ ਹਨ । </p>

<p>ਹੈਲਮਟ ਬਾਰੇ
ਆਈ.ਜੀ. ਦੇ ਬਿਆਨ ਦਾ ਤਿੱਖਾ ਵਿਰੋਧ </p>

<p>ਚੰਡੀਗੜ੍ਹ, 10 ਸਤੰਬਰ - ਸਿੱਖ ਜਥੇਬੰਦੀਆਂ
ਤੇ ਸੰਸਥਾਵਾਂ ਦੀ ਸਾਂਝੀ ਐਕਸ਼ਨ ਕਮੇਟੀ ਦੀ ਮੀਟਿੰਗ ਅੱਜ ਇਥੇ ਜਿਸ ਵਿਚ ਚੰਡੀਗੜ੍ਹ ਪੁਲਿਸ ਦੇ
ਇੰਸਪੈਕਟਰ ਜਨਰਲ ਭੀਮ ਸੈਨ ਬੱਸੀ ਵਲੋਂ ਦਿੱਤੇ ਬਿਆਨ ਕਿ ਦੋਪਹੀਆ ਸਿੱਖ ਇਸਤਰੀ ਵਾਹਨ ਚਾਲਕਾਂ
ਲਈ ਲੋਹ-ਟੇਪ (ਹੈਲਮਟ) ਪਹਿਨਣਾ ਜਰੂਰੀ ਹੋਵੇਗਾ, ਦਾ ਸਖਤ ਨੋਟਿਸ ਲਿਆ ਗਿਆ । </p>

<p>ਕਮੇਟੀ ਦੇ ਕਨਵੀਨਰ ਰਾਜਿੰਦਰ ਸਿੰਘ ਨੇ
ਕਿਹਾ ਕਿ ਇਕ ਪਾਸੇ ਸ਼੍ਰੀ ਬੱਸੀ ਸਿੱਖਾਂ ਦੇ ਜਜ਼ਬਾਤਾਂ ਦੀ ਕਦਰ ਕਰਨ ਦੀ ਗੱਲ ਕਰ ਰਹੇ ਹਨ ਅਤੇ ਦੂਜੇ
ਪਾਸੇ ਉਹ ਲੋਹ-ਟੇਪ ਦੇ ਮਾਮਲੇ ਨੂੰ ਉਛਾਲ ਕੇ ਬੇਲੋੜੀ ਭੜਕਾਹਟ ਪੈਦਾ ਕਰ ਰਹੇ ਹਨ । ਸਾਂਝੀ
ਐਕਸ਼ਨ ਕਮੇਟੀ ਦੇ ਕਨਵੀਨਰ ਨੇ ਕਿਹਾ ਕਿ ਪਿਛਲੀ ਵਾਰ ਜਦੋਂ ਪ੍ਰਧਾਨ ਮੰਤਰੀ ਅਟਲ ਬਿਹਾਰੀ ਵਾਜਪਾਈ
ਚੰਡੀਗੜ੍ਹ ਆਏ ਸਨ ਤਾਂ ਉਸ ਸਮੇਂ ਉਨ੍ਹਾਂ ਨੇ ਜਨਤਕ ਤੌਰ ਤੇ ਐਲਾਨ ਕੀਤਾ ਸੀ ਕਿ ਜਦੋਂ ਭਾਰਤੀ
ਜਨਤਾ ਪਾਰਟੀ ਦੀ ਸਰਕਾਰ ਬਣੇਗੀ ਤਾਂ ਟਰੈਫਿਕ ਨਿਯਮਾਂ ਵਿਚ ਢੁਕਵੀਂ ਸੋਧ ਕੀਤੀ ਜਾਵੇਗੀ । ਇਹ
ਭਰੋਸਾ ਉਨ੍ਹਾਂ ਨੇ ਪੰਜਾਬ ਰਾਜ ਭਵਨ ਵਿਚ ਸਿੱਖ ਜਥੇਬੰਦੀਆਂ ਦੇ ਵਫਦ ਨੂੰ ਵੀ ਦਿੱਤਾ ਸੀ । </p>

<p>ਐਕਸ਼ਨ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨਗੀ ਮੰਡਲ ਨੇ
ਚੰਡੀਗੜ੍ਹ ਪ੍ਰਸ਼ਾਸਨ ਨੂੰ ਚੇਤਾਵਨੀ ਦਿੱਤੀ ਹੈ ਕਿ ਉਹ ਕੋਈ ਅਜਿਹੀ ਕਾਰਵਾਈ ਨਾ ਕਰੇ ਜਿਸ ਨਾਲ
ਸਿੱਖਾਂ ਦੇ ਜਜ਼ਬਾਤਾਂ ਨੂੰ ਠੇਸ ਪਹੁੰਚਦੀ ਹੋਵੇ । ਕਮੇਟੀ ਨੇ ਸਿੱਖ ਇਸਤਰੀਆਂ ਨੂੰ ਅਪੀਲ ਕੀਤੀ ਕਿ
ਉਹ ਹੈਲਮਟ ਨਾ ਪਹਿਨਣ ਅਤੇ ਇਸ ਬਾਰੇ ਸਰਕਾਰੀ ਸਖਤੀ ਦਾ ਬਹਾਦਰੀ ਨਾਲ ਵਿਰੋਧ ਕਰਨ । </p>

<p>ਖਰੜ ਨੇੜੇ ਦੋ
ਬੱਸਾਂ ਦੀ ਸਿੱਧੀ ਟੱਕਰ ; 9 ਮੌਤਾਂ </p>

<p>40 ਜ਼ਖਮੀਆਂ ਵਿਚੋਂ ਅੱਠ ਦੀ ਹਾਲਤ
ਗੰਭੀਰ </p>

<p>ਖਰੜ , 10 ਸੰਤਬਰ - ਖਰੜ-ਮੋਰਿੰਡਾ ਸੜਕ
ਤੇ ਅੱਜ ਰਾਤੀ 8 ਵਜੇ ਦੇ ਕਰੀਬ ਹੋਏ ਇਕ ਂਭਿਆਨਕ ਸੜਕ ਹਾਦਸੇ ਵਿਚ 6 ਵਿਅਕਤੀ ਮਾਰੇ ਗਏ ਅਤੇ
40 ਹੀ ਜ਼ਖਮੀ ਹੋ ਗਏ ।ਜ਼ਖਮੀਆਂ ਨੰੂ ਖੜਰ ਦੇ ਸਿਵਲ ਹਸਪਾਤਲ ਵਿਚ ਲਿਆਂਦਾ ਗਿਆ ਜਿੱਥੇਂ 9 ਦੀ
ਹਾਲਤ ਜ਼ਿਆਦਾ ਗੰਭੀਰ ਹੋਣ ਕਾਰਨ ਪੀ.ਜੀ. ਆਈ. ਚੰਡੀਗੜ੍ਹ ਭੇਜ ਦਿੱਤਾ ਗਿਆ । ਕੁਝ ਜ਼ਖਮੀ
ਮੁੱਢਲੀ ਸਹਾਇਤਾ ਪ੍ਰਾਪਤ ਕਰਨ ਮਗਰੋਂ ਘਰੋ-੍ਹਰੀ ਚਲੇ ਗਏ । ਸਰਵਿਸ ਅਨੁਸਾਰ ਤਿੰਨ ਜ਼ਖਮੀਆਂ ਦੀ
ਪੀ.ਜੀ.ਆਈ ਵਿਚ ਮੌਤ ਹੋਣ ਕਾਰਨ ਮੌਤਾਂ ਦੀ ਗਿਣਤੀ 9 ਹੋ ਗਈ । ਇਨ੍ਹਾਂ ਤਿੰਨ ਮ੍ਰਿਤਕਾਂ ਦੇ ਨਾਂ
ਗੁਰਦੇਵ ਸਿੰਘ,ਸ਼ਮਸ਼ੇਰ ਸਿੰਘ ਤੇ ਅਮਰੀਕ ਸਿੰਘ ਦੱਸੇ ਗਏ ਹਨ । </p>

<p>ਮੋਕੇ ਤੇ ਜਾ ਕੇ ਇਕੱਤਰ ਕੀਤੀ ਜਾਣਕਾਰੀ
ਅਨੁਸਾਰ ਇੱਕ ਬੱਸ ਨੰ: ਆਰ ਜੇ-31 ਪੀ 2017 ਜੋ ਨਾਰਦਰਨ ਇੰਡੀਆ ਟਰੈਵਲਜ਼ ਦੀ ਸੀ, ਖਰੜ ਵਲੋਂ
ਮੋਰਿੰਡਾ ਆ ਰਹੀ ਸੀ । ਦੂਜੇ ਪਾਸਿਓਂ ਪੰਜਾਬ ਰੋਡਵੇਜ਼ ਚੰਡੀਗੜ੍ਹ ਦੀ ਬੱਸ ਨੰ: ਪੀ ਬੀ 12 ਸੀ
9017 ਖਰੜ ਵੱਲ ਆ ਰਹੀ ਸੀ । ਉਨ੍ਹਾਂ ਦੋਵਾਂ ਦੀ ਸਿੱਧੀ ਟੱਕਰ ਹੋ ਗਈ । ਇਸ ਹਾਦਸੇ ਦੌਰਾਨ
ਪੰਜਾਬ ਰੋਡਵੇਜ਼ ਦੀ ਬੱਸ ਦੇ ਪਿੱਛੇ ਆਉਂਦੀ ਇਕ ਮਾਰੂਤੀ ਕਾਰ ਵੀ ਬੱਸ ਨਾਲ ਬੁਰੀ ਤਰ੍ਹਾਂ ਵੱਜੀ ।
ਇਸ ਕਾਰ ਦਾ ਨੰਬਰ ਸੀ ਐਚ 01 ਕਿਊ 5625 ਹੈ । ਇਸ ਕਾਰ ਦੇ ਟੁਕੜੇ-ਟੁਕੜੇ ਹੋ ਗਏ । ਇੰਜ
ਹੀ ਇਕ ਨਵਾਂ ਸਕੂਟਰ ਪ੍ਰਾਈਵੇਟ ਬੱਸ ਦੇ ਹੇਠਾਂ ਜਾ ਵੜਿਆ । </p>

<p>ਸਿਵਲ ਹਸਪਤਾਲ ਵਿਚਲੇ ਕੁਝ ਜ਼ਖਮੀਆਂ ਨੇ
ਦੱਸਿਆ ਕਿ ਪੰਜਾਬ ਰੋਡਵੇਜ਼ ਦੀ ਬੱਸ ਅੰਮ੍ਰਿਤਸਰ ਤੋਂ ਚੰਡੀਗੜ੍ਹ ਜਾ ਰਹੀ ਸੀ । ਇਸ ਤਰ੍ਹਾਂ
ਪ੍ਰਾਈਵੇਟ ਬੱਸ ਦੇ ਇਕ ਮੁਸਾਫਿਰ ਨੇ ਦੱਸਿਆ ਕਿ ਇਸ ਬੱਸ ਵਿਚ 25 ਸਵਾਰੀਆਂ ਸਨ ਅਤੇ ਉਹ
ਆਪਣੇ ਪਰਿਵਾਰ ਸਮੇਤ ਇਸ ਰਾਹੀਂ ਅਬੋਹਰ ਜਾ ਰਿਹਾ ਸੀ । </p>

<p>ਮੌਕੇ ਤੇ ਮੌਜੂਦ ਪਿੰਡ ਮਾਸੂਪੁਰ ਦੇ
ਬਲਵਿੰਦਰ ਸਿੰਘ ਨੇ ਦੱਸਿਆ ਕਿ ਉਹ ਕੁਝ ਦੂਰ ਸਾਈਕਲ ਤੇ ਜਾ ਰਿਹਾ ਸੀ ਕਿ ਜਦੋਂ ਬਹੁਤ ਧਮਾਕੇ
ਨਾਲ ਇਹ ਟੱਕਰ ਹੋਈ । ਉਹ ਤੁਰੰਤ ਪਿੰਡ ਰੁੜਕੀ ਪੱਕੀ ਵਿਚ ਗਿਆ ਅਤੇ ਲੋਕਾਂ ਨੂੰ ਇਕੱਠਾ ਕੀਤਾ
। ਪਿੰਡ ਦੇ ਗੁਰਦੁਆਰੇ ਤੋਂ ਲਾਊਡ ਸਪੀਕਰ ਰਾਹੀਂ ਸੂਚਨਾ ਦਿੱਤੀ ਗਈ । ਪਿੰਡ ਦੇ ਲੋਕਾਂ ਨੇ
ਪੰਜਾਬੀਆਂ ਦੀ ਰਵਾਇਤ ਅਨੁਸਾਰ ਮਾਨਵਤਾ ਵੱਲ ਆਪਣਾ ਫਰਜ਼ ਨਿਭਾਉਂਦੇ ਹੋਏ ਜ਼ਖਮੀਆਂ ਨੂੰ ਬੱਸ
ਵਿਚੋਂ ਕੱਢਿਆ ਅਤੇ ਹਸਪਤਾਲ ਪਹੁੰਚਾਇਆ । ਖਰੜ ਦੇ ਡੀ.ਐਸ.ਪੀ. ਰੁਪਿੰਦਰ ਸਿੰਘ ਅਤੇ
ਐਸ.ਐਚ.ਓ. ਜੀ.ਪੀ.ਸਿੰਘ ਦੀ ਅਗਵਾਈ ਹੇਠ ਵੱਡੀ ਗਿਣਤੀ ਵਿਚ ਪੁਲਿਸ ਮੁਲਾਜ਼ਮ ਵੀ ਮੌਕੇ ਤੇ
ਪਹੁੰਚ ਗਏ । </p>

<p>ਘਟਨਾ ਵਾਲੀ ਥਾਂ ਤੇ ਦ੍ਰਿਸ਼ ਬਹੁਤ ਹੀ
ਭਿਆਨਕ ਸੀ ਤੇ ਪ੍ਰਾਈਵੇਟ ਬੱਸ ਬੁਰੀ ਤਰ੍ਹਾਂ ਨੁਕਸਾਨਗ੍ਰਸਤ ਸੀ । ਲੋਕਾਂ ਦਾ ਸਾਮਾਨ ਖਿਲਰਿਆ ਪਿਆ
ਸੀ । ਸਿਵਲ ਹਸਪਤਾਲ ਖਰੜ ਵਿਚ ਡਾਕਟਰਾਂ ਦੀ ਟੀਮ ਨੇ ਜ਼ਖਮੀਆਂ ਨੂੰ ਮੁਢਲੀ ਸਹਾਇਤਾ ਦਿੱਤੀ । ਡਾ.
ਐਚ.ਐਸ.ਓਬਰਾਏ ਤੇ ਡਾ.ਰਾਜੀਵ ਭੱਲਾ ਨੇ ਦੱਸਿਆ ਕਿ 9 ਜ਼ਖਮੀ ਪੀ.ਜੀ.ਆਈ. ਭੇਜੇ ਜਾ ਚੁੱਕੇ
ਹਨ । ਕੁਝ ਜ਼ਖਮੀਆਂ ਨੂੰ ਮੋਰਿੰਡਾ ਦੇ ਸਿਵਲ ਹਸਪਤਾਲ ਵਿਚ ਦਾਖਲ ਕਰਾਏ ਜਾਣ ਦੀ ਵੀ ਖਬਰ ਹੈ ।
ਚਾਰ ਵਿਅਕਤੀਆਂ ਨੂੰ ਮ੍ਰਿਤਕ ਹਾਲਤ ਵਿਚ ਖਰੜ ਅਤੇ ਇਕ ਨੂੰ ਮੋਰਿੰਡਾ ਹਸਪਤਾਲ ਪਹੁੰਚਾਇਆ ਗਿਆ
ਹੈ । ਚੰਡੀਗੜ੍ਹ-ਲੁਧਿਆਣਾ ਸ਼ਾਹਰਾਹ ਤੇ ਆਵਾਜਾਈ ਠੱਪ ਰਹੀ । ਇਹ ਜਾਮ ਰਾਤ ਦੇ 10 ਵਜੇ ਤਕ
ਜਾਰੀ ਸੀ । </p>

<p>ਅੱਤਵਾਦ ਪੀੜਿਤ
ਪਰਿਵਾਰਾਂ ਨੂੰ ਮਦਦ ਦਾ ਭਰੋਸਾ </p>

<p>ਅੰਮ੍ਰਿਤਸਰ, 10 ਸਤੰਬਰ - ਪੁਨਰਵਾਸ
ਵਿਭਾਗ ਪੰਜਾਬ ਦੇ ਸਕੱਤਰ ਸ਼੍ਰੀ ਬੀ.ਕੇ.ਸ਼੍ਰੀਵਾਸਤਵਾ ਨੇ ਅੱਜ ਅੱਤਵਾਦ ਪੀੜਿਤ ਪਰਿਵਾਰਾਂ ਪਾਸੋਂ
ਉਨ੍ਹਾਂ ਨੂੰ ਦਰਪੇਸ਼ ਸਮੱਸਿਆਵਾਂ ਸੁਣੀਆਂ । ਪੱਤਰਕਾਰਾਂ ਨਾਲ ਗੱਲਬਾਤ ਕਰਦਿਆਂ ਸ਼੍ਰੀਵਾਸਤਵਾ ਨੇ ਕਿਹਾ
ਕਿ ਅੱਜ ਉਹ ਇਹ ਅਧਿਐਨ ਕਰਨ ਆਏ ਸਨ ਕਿ ਪਰਿਵਾਰਾਂ ਨੂੰ ਕਿਸ ਤਰ੍ਹਾਂ ਦੀਆਂ ਸਮੱਸਿਆਵਾਂ ਦਾ
ਸਾਹਮਣਾ ਕਰਨਾ ਪੈ ਰਿਹਾ ਹੈ ਅਤੇ ੳਹ ਸਰਕਾਰ ਪਾਸੋਂ ਕਿਸ ਤਰ੍ਹਾਂ ਦੀ ਮਦਦ ਚਾਹੁੰਦੇ ਹਨ । ਉਨ੍ਹਾਂ
ਕਿਹਾ ਕਿ ਉਹ ਸਾਰੇ ਡਿਪਟੀ ਕਮਿਸ਼ਨਰਾਂ ਨੂੰ ਪੱਤਰ ਲਿਖਣਗੇ ਕਿ ਜਦੋਂ ਪੀੜਤ ਪਰਿਵਾਰਾਂ ਵਲੋਂ ਸਰਕਾਰੀ
ਨੌਕਰੀ ਲੈਣ ਜਾਂ ਸਹਾਇਤਾ ਲੈਣ ਦੀਆਂ ਅਰਜੀਆਂ ਆ ਜਾਣ ਤਾਂ ਉਹ ਉਨ੍ਹਾਂ ਸਾਰੇ ਪਰਿਵਾਰਾਂ ਨੂੰ ਇਕ
ਨਿਰਧਾਰਤ ਸਮੇਂ ਤੇ ਸੱਦਣ ਤੇ ਉਨ੍ਹਾਂ ਨਾਲ ਗੱਲਬਾਤ ਕਰਨ । </p>

<p>ਸ਼੍ਰੀ ਵਾਸਤਵਾ ਨੇ ਕਿਹਾ ਕਿ ਪੀੜਤ ਪਰਿਵਾਰਾਂ
ਪਾਸੋਂ 30 ਸਤੰਬਰ ਤੱਕ ਅਰਜੀਆਂ ਮੰਗੀਆਂ ਗਈਆਂ ਹਨ । ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਉਹ ਅਰਜੀਆਂ ਦੀ ਪ੍ਰਾਪਤੀ
ਮਗਰੋਂ ਕੇਸਾਂ ਦੀ ਵੱਖ-ਵੱਖ ਕੈਟਾਗਰੀ ਬਣਾਉਣਗੇ । ਇਸ ਮਗਰੋਂ ਵੱਖ-ਵੱਖ ਵਿਭਾਗਾਂ ਵਿਚ ਖਾਲੀ
ਪਈਆਂ ਅਸਾਮੀਆਂ ਦੇ ਵੇਰਵੇ ਇਕੱਠੇ ਕੀਤੇ ਜਾਣਗੇ ਤਾਂ ਜੋ ਲੋੜਵੰਦਾਂ ਨੂੰ ਨੌਕਰੀਆਂ ਦਿੱਤੀਆ ਜਾ
ਸਕਣ । ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਉਹ ਹਦਾਇਤ ਜਾਰੀ ਕਰਨਗੇ ਕਿ ਆਉਣ ਵਾਲੀਆਂ ਅਰਜੀਆਂ ਦੇ ਪੂਰੇ ਵੇਰਵੇ
ਰੱਖੇ ਜਾਣ ਅਤੇ ਜੇਕਰ ਕਿਸੇ ਦੀ ਅਰਜੀ ਰੱਦ ਕੀਤੀ ਜਾਵੇ ਤਾਂ ਉਸਨੂੰ ਲਿਖਤੀ ਤੌਰ ਤੇ ਰੱਦ ਕਰਨ ਦੇ
ਕਾਰਨ ਦੱਸੇ ਜਾਣ । ਇਸ ਮੌਕੇ ਸ਼੍ਰੀ ਵਾਸਤਵਾ ਕੋਲ ਨੌਕਰੀ ਦੀ ਫਰਿਆਦ ਕਰਨ ਆਏ ਪਿੰਡ ਮੱਲ੍ਹੀਆਂ
ਦੇ ਦਿਲਬਾਗ ਸਿੰਘ ਨੇ ਕਿਹਾ ਕਿ ਉਸ ਦੇ ਪਿਤਾ ਨੂੰ 23 ਜੂਨ, 1990 ਨੂੰ ਮਾਰ ਦਿੱਤਾ ਸੀ । ਉਸ
ਦੇ ਪਿਤਾ ਨੇ ਦੁਨਾਲੀ ਬੰਦੂਕ ਨਾਲ ਮੁਕਾਬਲਾ ਵੀ ਕੀਤਾ ਸੀ । ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ ਪੰਜਵੜ ਨੇ ਉਸ ਦੇ
ਪਿਓ ਨੂੰ ਇਸ ਲਈ ਮਾਰਿਆ ਸੀ ਕਿੳਂਕਿ ਇਕ ਕੇਸ ਵਿਚ ਪੰਜਵੜ ਦੇ ਸਾਥੀ ਨੂੰ ਪੁਲਿਸ ਕੋਲ
ਫੜਾਇਆ ਸੀ । ਉਹ +2 ਪਾਸ ਹੈ ਤੇ ਸਰਕਾਰ ਤੋਂ ਨੌਕਰੀ ਚਾਹੁੰਦਾ ਹੈ । </p>

<p>ਤੇਜਾ ਵੀਲ੍ਹਾ (ਗੁਰਦਾਸਪੁਰ) ਦੀ ਨਿਰਮਲਾ
ਰਾਣੀ ਨੇ ਕਿਹਾ ਕਿ ਉਸ ਦਾ ਪੁੱਤਰ 1988 ਵਿਚ ਮਾਰ ਦਿੱਤਾ ਸੀ । ਉਸ ਨੇ ਆਪਣੀ ਲੜਕੀ ਪ੍ਰੋਮਿਲਾ
ਕੁਮਾਰੀ ਲਈ ਸਰਕਾਰ ਤੋਂ ਨੌਕਰੀ ਮੰਗੀ ਸੀ ਪਰ ਨਹੀਂ ਮਿਲੀ । ਪਿੰਡ ਮਾੜੀ ਮੇਘਾ ਦੀ ਆਸ਼ਾ ਡੌਲੀ
ਨੇ ਕਿਹਾ ਕਿ ਉਸ ਦੇ ਭਰਾ ਨੂੰ ਅੱਤਵਾਦੀਆਂ ਨੇ ਮਾਰ ਦਿੱਤਾ ਸੀ ਪਰ ਕਿਸੇ ਜੀਅ ਨੂੰ ਨੌਕਰੀ ਨਹੀਂ
ਮਿਲੀ । </p>

<p> ਪਿਛਲੀਆਂ
ਖਬਰਾਂ
</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>ਵਿਚਾਰ
ਮੰਚ </p>

<p>ਉਨਟਾਰੀਓ ਗੁਰੂਦੁਆਰਾਜ਼ ਕਮੇਟੀ ਲਈ ਸਵੈ-ਪੜਚੋਲ੍ਹ ਦੀ
ਘੜੀ....ਸੁਖਮਿੰਦਰ ਸਿੰਘ ਹੰਸਰਾ </p>

<p>ਗੁਰਸਿੱਖਾਂ
ਵਿੱਚ ਆਪਸੀ ਫੁੱਟ ਸਮੁੱਚੇ ਭਾਈਚਾਰੇ ਲਈ ਦੁਖਾਂਤ ਹੈ। ਭਾਵੇ ਕਿ ਸਾਨੰੂ ਅਜਿਹਾ ਅਨੁਭਵ,  Pu@t dy nqIjy inklx qoN b`Ed hI huMd` hY pr
is@K isD~q dI mu@FlI buinE`d ies g@l dI guE`hI idMdI hY ik Pu@t inhsMsy pMQk
ih@q iv@c nhIN hY| </p>

<p>          is@K ieiqh`s iv@c Pu@t dIE~ Enyk~ aud`hrx~
imldIE~ hn ijnH~ dy is@ty G`qk hI inkly hn| Egr do dh`ikE~ dIE~ Gtn`v~ hI ivc`r
leIey q~ vI ieh mMnx` pvyg` ik Pu@t DuMdlHy Biv@K d` jnm d`q` hY| </p>

<p>          "Pu@t" iksy pRIv`r, sMsQ`,
gru@p  j~ iksy kOm iv@c pY j`vy q~ ieh
qb`hI hI krv`auNdI hY| ies qoN b@cx d` ie@ko ie@k qrIk` hY ik s`nMU ie@k isD~q
d` p@l` PV ky c@lx dI E`dq p`auxI c`hIdI hY| ieh isD~q B`eI caup` isMG iC@br jI
vloN rihqn`imE~ iv@c s`fy leI ieauN EMikq kIq` hY ijvyN: jo is@K, is@K d`
mUMh iPtk`ry so BI qnK`hIE`, jo is@K, is@K nMU bur` E`Ky kRoD n`l so BI
qnK`hIE`, jo is@K, is@K dI p@g nMU h@Q p`ey so BI qnK`hIE`, jo is@K, is@K dy
d`VHy nMU h@Q p`ey, Eru lVy so BI qnK`hIE`, jo is@K, is@K dy kys~ nMU h@Q p`ey,
so BI qnK`hIE`, jo is@K n`l h@Qo-p`eI lV`eI kry, so BI qnK`hIE`  E`id| </p>

<p>ਉਪਰੋਤਕ
ਰਹਿਤਨਾਮਿਆਂ ਦੇ ਸੰਦਰਭ ਵਿੱਚ ਅਗਰ ਅਸੀਂ ਇਨ੍ਹਾਂ ਕੁ ਆਪਣੇ ਆਪ ਨੰੂ ਸੰਜਮ ਵਿੱਚ ਰੱਖਣ ਦੀ ਆਦਤ
ਪਾ ਲਈਏ ਤਾਂ ਫੁੱਟ ਵਰਗੀ ਨਾਮੁਰਾਦ ਬਿਮਾਰੀ ਤੋਂ ਛੁਟਕਾਰਾ ਮਿਲ ਸਕਦਾ ਹੈ। </p>

<p>          is@K ieiqh`s iv@c Pu@t d` k`l`  pRsq`v drj hY, ijhV` drs`auNd` hY ik ikvyN
is@K~ dI Pu@t k`rx ienH~ d` dunIE~ qoN r`j Ku@isE` Eqy is@K E@j q@k aus Pu@t d`
sMq`p hMF` rhy hn Eqy gul`mI dI c@kI iv@c ips rhy hn| Egr ipCly do dh`ikE~ dy
ieiqh`s v@l nzr m`rIey q~ sB qoN v@f` Eqy G`qk duK~q ijs nMU hr (suihrd) is@K
mihsUs krd` hY auh vI Ek`lIE~ dI Pu@t ivcoN hI pYd` hoieE` sI| Ek`lIE~ dI Pu@t
dy k`rx hI B`rq srk`r 1984 iv@c drb`r s`ihb aupr hml` krn leI Syr bxI sI Eqy
is@K dunIE` Br iv@c zlIl hoey| </p>

<p>           Egr K`VkU dOr v@l J`qI m`rIey q~ pMj`b dIE~ cox~ dy b`eIk`t qoN
auprMq K`VkU sP~ iv@c pYd` hoeI Pu@t k`rx E@j K`VkU lihr ivc`r goStIE~ q@k sImq
ho ky rih geI hY|(ies qoN ieh B`v nhIN lY lYx` c`hId` ik K`ilsq`n dI lihr iv@c
KVoq E`eI hY ikauNik auh inrMqr j`rI hY) </p>

<p>          siQqI EYnI gMBIr bx cu@kI hY ik is@K
sm`j ivcoN koeI vI EijhI sMsQ` j~ lIfr nhIN ijs nMU "dUsr`" pRv`inq
kr irh` hovy| hr koeI iehI khI j` irh` hY   myrI nIqI pMQk hY Eqy
dUsry pMQ d` nuks`n kr rhy hn| EYn` ku@J v`pr j`x qoN b`Ed vI pMQk sP~ iv@c
Pu@t pRbl hY Eqy hMk`r  dI hrkq iv@c
koeI hlImI nhIN E`eI sgoN E`pixE~ d` glH` db`aux iv@c m`x mihsUs kIq` j` irh`
hY| </p>

<p>          ieQy ieh vrnxwog hY ik  jdoN aunt`rIA iv@c gurUduE`r` kmytIE~ d`
E`psI tkr`A is@K kOm d` nuks`n kr irh` sI Eqy E`psI tkr`A iv@c kOmI SkqI  nSt ho rhI sI q~ ies nUMU T@l p`aux Eqy kOmI
SkqI nMU kOm dy ih@q~ iv@c vrqx leI iksy aupr`ly dI jrUrq sI| </p>

<p>          EijhI soc ivcoN au.gu.k. d` jnm hoieE`
sI| ies sMsQ` nMU sQ`pq krn leI bV` sMGrS krn` ipE` sI| ies nMU sQ`pq krn leI
sMGrS krq` Eqy mu@Fly smyHN dOr`n ies kmytI d` imE`r aupr cu@kx v`ly ies dy
spoksmYn sR: irpsoDk isMG gryv`l ny Ed`r` s~J svyr` n`l g@l b`q dOr`n d@isE` ik
"ieh sMsQ` is@K kOm dI imMnI p`rlImYNt dy pYtrn qy bx`eI geI sI ijQy
gurUduE`irE~ dy pRqIin@D E` ky E`pxy vKryvyN nij@T skx"| </p>

<p>          aunt`rIA gurUduE`r` kmytI aunt`rIA
iv@c is@K kOm dI bulMd E`v`z bx ky auBrnI c`hIdI sI ijs n`l is@K kOm dI
pRqIin@Dq` Tos qrIky n`l ie@k plytP`rm qoN ho skdI| ieh koiSS au@qrI EmrIk` dy
is@K~ vloN 1984 iv@c ivSv is@K sMsQ` sQ`pq krky vI kIqI geI sI ijs nMU ku@J
SkqIE~ vloN ausy smyHN hI nk`r` krn dIE~ koiSS~ E`rMB kr id@qIE~ geIE~ sn
PlsrUp ieh sMs`r p@Dr dI sMsQ` dUsrIE~ lokl sMsQ`v~ v~g  ku@J ku is@K~ dI sMsQ` hI bx ky rih geI hY|
B`vyN ik ies sMsQ` ny dsq`r Eqy ikrp`n dy kys iv@c E`pxI pihc`x bx` leI hY Eqy
kYnyf` d` hr isE`sqd`n ies sMsQ` dy kMm k`r qoN j`xUM hY pr ieh sMsQ` smu@cy is@K~
dI sMsQ` bnx iv@c EsPl hoeI| </p>

<p>          hux ieho h`l au.gu.k. d` hoieE` hY|
ieh sMsQ`, ijs qoN lok~ nMU bhuq E`s~ sn E`psI tkr`A d` iSk`r ho ky mihz ngr
kIrqn kmytI bx ky rih geI hY ijs d` izkr ies kmytI vloN krv`eI geI ie@k k`nPrMs
iv@c ifksI rof gurUduE`r` dI kmytI mYNbr sRd`rnI hrmyl kOr ny vI kIq` sI| ieQy
ieh vI kihx` v`ijb hY| ies kmytI nMU bn`aux dy mksd nMU m@dy nzr r@KidE~ Egr
ies kmytI dy kMm~ nMU pVcoilE` j`vy q~ ieh g@l auBr ky s`hmxy E`auNdI hY ik ijs
mksd leI ieh kmytI bx`eI geI aus inS`ny qoN ieh au@k cu@kI hY| gurUduE`irE~ dy
E`psI q`lmyl nMU vD`aux dI bj`ey ies kmytI dy Ehudyd`r~ aupr p@Kp`q d` doS l@g
irh` hY|   </p>

<p>          aunt`rIA gurUduE`r`z kmytI dI cox
pRikirE` ies sMsQ` dy ies duK~q d` mu@F ikh` j` skd` hY| cox smyHN ies dy s`ry
mYNbr gurUduE`irE~ vloN E`pxy fYlIgyt Byjy j~dy hn Eqy ienH~ fYlIgyt~ vloN
iesdI vrikMg kmytI dI cox j~ inwukqI kIqI j~dI hY| </p>

<p>          ivc`rn v`l` mu@d` ieh hY ik izE`d`qr
gurUduE`irE~ iv@c pRbMDk kmytIE~, sMgq dI sihmqI lYx qoN ibnH~ bx`eIE~ j~dIE~
hn| bhuqy gurUduE`irE~ iv@c mYNbriSp hI nhIN hY Eqy s`l~ b@DI gru@p~ d` kbz`
hY| B`vyN auQy pRbMD cMg` hI hovy pr sMgq ivcoN iksy nMU syv` d` mOk`, k`bz DVy
dI imhrb`nI qoN ibnH~ nhIN iml skd` | Egr sMgq d` gurUduE`irE~ dIE~ kmytIE~
bn`aux iv@c koeI K`s wogd`n nhIN ilE` j~d` q~ ienH~ kmytIE~ vloN sQ`pq kIqI
j~dI ies kmytI vloN kimaUntI dI pRqIinDq` vI sMquStI v`lI nhIN ho skdI| </p>

<p>          inrsMdyh aunt`rIA gurUduE`r`z kmytI dI
sQ`pn` kOm dy p@K nMU auB`rn leI kIqI geI sI Eqy SurU SurU iv@c sR: irpsoDk
isMG gryv`l dI Egv`eI hyT ies kmytI  dI
cMgI k`rguz`rI s`hmxy E`eI sI| EPsos ies g@l d` hY ik s`fI sus`ietI iv@c lMmy
Ersy qoN pYd` hoey ruJ`n, ik jdoN koeI cIz qr@kI krn l@gdI hY q~ ies dIE~ l@q~
iK@cx v`lIE~ SkqIE~ isr cu@k lYNdIE~ hn, ies sMsQ` dy joVIN bih igE` hY|           EsIN aunt`rIA gurUduE`r`z kmytI nMU
Enyk~ v`r mIfIey n`l sbMD sud`rn leI bynqIE~ kIqIE~ hn q~ jo doh~ iDrH~ dI SkqI
nMU s~Jy rUp iv@c B`eIc`rk qr@kI leI l`ieE` j` sky| au. gu. k. ny ies idS` v@l
kdm pu@tidE~ ies s`l vYs`KI qoN b`Ed mIfIey n`l Akivl gurUduE`r` s`ihb ivKy
ie@k imlxI d` pRogR`m jrUr bx`ieE` sI pr auh Fu@kvyN qrIky n`l s@d` n~ dyx
bdOlq sPl n~ ho sikE`| </p>

<p>          EsIN ie@k v`r iPr ies kmytI dy
ijMmyv`r num`ieMidE~ Eqy smu@cy gurUduE`r` s`ihb dy mYNbr~ nMU bynqI krdy h~ ik
hux isr joV ky ivc`r krn d` vyl` E` igE` hY| au. gu. k. dI sQ`pn` qoN lY ky hux
q@k dI k`rguz`rI d` EiDEYn krn` jrUrI ho igE` hY Eqy Pr`KidlI n`l E`pxIE~
kmzorIE~ svIk`r krky aunH~ nMU dUr krn dI rucI pRbl hoxI c`hIdI hY| q~ ik ies
kOmI sMsQ` nMU kOm dy ih@q~ leI srgrm kIq` j` sky| ShId~ dy b@cy rulH rhy hn
Eqy pMQk sMGrS sMzId` iDE`n dI mMg krd` hY|   </p>

<p>          ies kmytI dy mYNbr~ nMU, kimaUntI dy
isE`xy isr~ nMU iek@qr krky "sP` ivC`", "duibD` dUr" krky
ivc`r~ d` dOr SurU krn c`hId` hY Eqy ies iv@c pYd` hoeIE~ qryV~ imt`aux dI
koiSS krnI c`hIdI hY| B`v hux is@K sm`j dI pRPu@lq` leI aunt`rIA gurUduE`r`z
kmytI nMU svY-pVcolH krnI zrUrI ho geI hY Eqy b`h~ aul`r ky kimaUntI dy GrIN
bYTy suihrd is@K~ nMU E@gy ilE`aux leI wqnSIl hox` c`hId` hY | Ed`r` s~J svyr`
pMQ dI cVHdI kl` leI, auproqk idS` v@l suihrdq` n`l grz-rihq sihwog dyx leI
vcnb@D hY| </p>

<p> </p>






</body></text></cesDoc>