<cesDoc id="pun-w-sanjh-news-02-07-22" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-02-07-22.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 02-07-22</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>02-07-22</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਆਪਣੇ
ਸੰਵਿਧਾਨਕ ਫਰਜ਼ਾਂ ਬਾਰੇ ਬਿਲਕੁੱਲ ਸਪੱਸ਼ਟ ਹਾਂ : ਡਾ: ਕਲਾਮ</p>

<p>ਨਵੀਂ ਦਿੱਲੀ :-
ਰਾਸ਼ਟਰਪਤੀ ਏ.ਪੀ.ਜੇ. ਕਲਾਮ ਨੇ ਕਿਹਾ ਹੈ ਕਿ ਉਨਾਂ੍ਹ ਨੂੰ ਪੁੂਰਾ ਇਲਮ ਹੈ ਕਿ ਰਾਸ਼ਟਰਪਤੀ
ਸੰਵਿਧਾਨ ਦਾ ਰਾਖਾ ਹੁੰਦਾ ਹੈ। ਨਾਲ ਹੀ ਉਨ੍ਹਾਂ ਇਨਾਂ੍ਹ ਖਦਸ਼ਿਆਂ ਨੂੰ ਰੱਦ ਕੀਤਾ ਹੈ ਕਿ ਗੈਰ
ਸਿਆਸੀ ਪਿਛੋਕੜ ਅਤੇ ਸੰਘ ਪਰਿਵਾਰ ਦੀ ਇਮਦਾਦ ਕਾਰਨ ਆਪਣੇ ਫਰਜ਼ ਚੰਗੀ ਤਰ੍ਹਾਂ ਨਾਲ ਨਿਭਾਉਣ ਤੋਂ
ਅਸਮਰਥ ਹੋਣਗੇ। </p>

<p>ਜੰਮੂ
ਕਸ਼ਮੀਰ 'ਚ ਕੇਂਦਰੀ
ਰਾਜ ਛੇਤੀ</p>

<p>ਨਵੀਂ ਦਿੱਲੀ :-
ਕੇਂਦਰ ਸਰਕਾਰ ਨੇ ਜੰਮੂ ਕਸ਼ਮੀਰ ਵਿਚ ਰਾਸ਼ਟਰਪਤੀ ਰਾਜ ਲਾਗੂ ਕਰਨ ਦਾ ਫੈਸਲਾ ਲਗਪਗ ਕਰ ਲਿਆ ਹੈ।
ਇਸ ਬਾਰੇ ਰਸਮੀ ਐਲਾਨ ਅਗਲੇ ਮਹੀਨੇ ਕੀਤਾ ਜਾਣਾ ਹੈ। ਵਾਜਪਾਈ ਸਰਕਾਰ ਦੀ ਦਲੀਲ ਹੈ ਕਿ ਸੂਬੇ
ਵਿੱਚ ਅਕਤੂਬਰ ਮਹੀਨੇ ਨਿਰੱਪਖ ਅਤੇ ਭੈਅ ਮੁਕਤ ਚੋਣਾਂ ਲਈ ਰਾਸ਼ਟਰਪਤੀ ਰਾਜ ਲਾਗੂ ਕਰਨਾ ਜ਼ਰੂਰੀ
ਹੈ। </p>

<p>ਜਥੇਦਾਰ
ਵੇਦਾਂਤੀ ਦੇ ਖਿਲਾਫ ਦੋਸ਼ਾਂ ਦੇ ਸਬੂਤ ਸੌਂਪੇ</p>

<p>ਅਮ੍ਰਿੰਤਸਰ :-
ਸ਼੍ਰੋਮਣੀ ਖਾਲਸਾ ਪੰਚਾਇਤ ਲੁਧਿਆਣਾ ਨੇ ਸ਼੍ਰੀ ਅਕਾਲ ਤਖਤ ਦੇ ਜਥੇਦਾਰ ਗਿਆਨੀ ਜੋਗਿੰਦਰ ਸਿੰਘ
ਵੇਦਾਂਤੀ ਉਤੇ ਲਾਏ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਦੇ ਦੋਸ਼ਾਂ ਦੇ ਸਬੂਤ ਅੱਜ ਇਕ ਮੈਂਬਰੀ ਕਮਿਸ਼ਨ ਅਤੇ ਸ਼੍ਰੋਮਣੀ
ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ ਕਿਰਪਾਲ ਸਿੰਘ ਬੰਡੂਗਰ ਨੂੰ ਸੌਂਪ ਦਿੱਤੇ। </p>

<p>ਹਰਿਆਣਾ
ਦੇ ਸਿੱਖ ਵੱਖਰੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਮੰਗ ਤੇ ਡਟੇ</p>

<p>ਅੰਬਾਲਾ :-
ਸਿੱਖ ਕੰਨਿਆ ਪਾਠਸ਼ਾਲਾ ਵਿੱਚ ਇੱਥੇ ਹੋਈ ਹਰਿਆਣਾ ਦੇ ਸਿੱਖ ਆਗੂਆਂ ਦੀ ਮੀਟਿੰਗ 'ਚ ਇਹੀ ਸਹਿਮਤੀ ਬਣੀ ਕਿ ਇਸ ਸੂਬੇ ਦੇ ਸਿੱਖਾਂ ਨੂੰ
ਵੱਖਰੀ ਹਰਿਆਣਾ ਸਿੱਖ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਤੋਂ ਘੱਟ ਕੁਝ ਵੀ ਮਨਜ਼ੂਰ ਨਹੀਂ।</p>

<p>ਹਿੰਦੂ
ਅੱਤਵਾਦੀ ਤਾਂਗੜੀ ਨੂੰ ਪੁਲਿਸ ਹਿਰਾਸਤ ਵਿਚ ਭੇਜਿਆ</p>

<p>ਲੁਧਿਆਣਾ:-
ਸ਼ਿਵ ਸੈਨਾ (ਰਾਸ਼ਟਰਵਾਦੀ) ਦੇ ਆਗੂ ਜਗਦੀਸ਼ ਤਾਂਗੜੀ ਦਾ ਅਦਾਲਤ ਨੂੰ ਅੱਜ ਦੋ ਰੋਜ਼ਾ ਪੁਲਿਸ ਰਿਮਾਂਡ
ਦੇ ਦਿੱਤਾ। ਸ਼੍ਰੀ ਤਾਂਗੜੀ ਨੂੰ ਅੱਜ ਸ਼ਾਮੀਂ ਡਿਊਟੀ ਮੈਜਿਸਟ੍ਰੇਟ ਸ਼੍ਰੀਮਤੀ ਅਨੀਤਾ ਬਾਲੀ ਦੀ ਅਦਾਲਤ
ਵਿਚ ਪੇਸ਼ ਕੀਤਾ ਗਿਆ। ਪੁਲਿਸ ਨੇ ਦੋ ਹਫਤੇ ਪਹਿਲਾਂ ਉਨਾਂ੍ਹ ਦੇ ਵਿਰੁੱਧ ਕਈ ਕੇਸ ਦਰਜ ਕੀਤੇ ਸਨ।
ਉਨਾਂ੍ਹ ਤੇ ਧੋਖਾਧੜੀ, ਫਿਰੌਤੀ, ਗੁਜਰਾਤ ਦੇ ਦੰਗਿਆਂ ਲਈ ਹਥਿਆਰ ਭੇਜਣ, ਅਸ਼ਲੀਲ ਸਮੱਗਰੀ ਰੱਖਣ
ਤੇ ਵੇਚਣ ਦੇ ਦੋਸ਼ ਹਨ। ਸ਼੍ਰੀ ਤਾਂਗੜੀ ਨੇ ਬੀਤੀ ਰਾਤ ਐਸ.ਪੀ. ਹਰੀਸ਼ ਕੁਮਾਰ ਦੇ ਸਾਹਮਣੇ ਆਤਮ
ਸਮਰਪਣ ਕੀਤਾ ਸੀ। ਸਮਝਿਆ ਜਾਂਦਾ ਹੈ ਕਿ ਸ਼ਿਵ ਸੈਨਾ (ਬਾਲ ਠਾਕਰੇ) ਦੇ ਸੀਨੀਅਰ ਆਗੂ ਰਵਿੰਦਰ
ਅਰੋੜਾ ਦੇ ਕਹਿਣ ਤੇ ਸ਼੍ਰੀ ਤਾਂਗੜੀ ਨੇ ਆਤਮ ਸਮਰਪਣ ਕੀਤਾ ।</p>

<p>ਪੁਲਿਸ
ਦੇ ਸਦਕ`
hI pMj`b ivc K`VkUv`d Kqm hoieE` : blYk QMfr ikq`b</p>

<p>ਲੁਧਿਆਣਾ:-
ਸੰਨ 1992 ਵਿਚ ਤਤਕਾਲੀ ਮੁੱਖ ਮੰਤਰੀ ਸ਼੍ਰੀ ਬੇਅੰਤ ਸਿੰਘ ਦੀ ਸਰਕਾਰ ਵੱਲੋਂ ਪੰਜਾਬ ਵਿਚ
ਦਹਿਸ਼ਤਗਰਦੀ ਖਤਮ ਕਰਨ ਦਾ ਜਿਹੜਾ ਦਾਅਵਾ ਕੀਤਾ ਗਿਆ ਸੀ, ਉਸ ਲਈ ਬੇਅੰਤ ਸਿੰਘ ਸਰਕਾਰ ਨਹੀਂ
ਸਗੋਂ ਉਸ ਤੋਂ ਪਹਿਲਾਂ ਰਾਸ਼ਟਰਪਤੀ ਰਾਜ ਦੌਰਾਨ ਪੰਜਾਬ ਪੁਲਿਸ ਵੱਲੋਂ ਅੱਤਵਾਦੀਆਂ ਵਿਰੁੱਧ ਚਲਾਈ
ਗਈ ਮੁਹਿੰਮ ਜਿੰਮੇਵਾਰ ਸੀ। ਬੇਅੰਤ ਸਿੰਘ ਨੇ ਤਾਂ ਸਗੋਂ ਸਰਕਾਰ ਸੰਭਾਲਣ ਤੋਂ ਤੁਰੰਤ ਬਾਅਦ
ਅੱਤਵਾਦੀ ਜਥੇਬੰਦੀਆਂ ਨਾਲ ਗੱਲ ਕਰਨ ਦੀ ਖੁੱਲ੍ਹੀ ਪੇਸ਼ਕਸ਼ ਕਰਕੇ ਅੱਤਵਾਦੀਆਂ ਦੇ ਹੌਂਸਲੇ ਵਧਾਉਣ ਵਿੱਚ
ਹਿੱਸਾ ਪਾਇਆ ਸੀ। ਇਹ ਗੱਲ ਅਮ੍ਰਿੰ੍ਰਤਸਰ ਦੇ ਸਾਬਕਾ ਡਿਪਟੀ ਕਮਿਸ਼ਨਰ ਸ਼੍ਰੀ ਸਰਬਜੀਤ ਸਿੰਘ ਨੇ
ਆਪਣੀ ਨਵੀਂ ਛਪੀ ਪਹਿਲੀ ਕਿਤਾਬ " ਆਪਰੇਸ਼ਨ ਬਲੈਕ ਥੰਡਰ" ਵਿਚ ਕਹੀ ਹੈ।</p>

<p>ਬਾਦਲ
ਵੱਲੋਂ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਵਿਰੋਧੀ ਮੁਹਿੰਮ ਨੂੰ ਠੁੱਸ ਕਰਨ ਦਾ ਸੱਦਾ</p>

<p>ਆਲਮਗੀਰ(ਲੁਧਿਆਣਾ)
:- ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ ਨਾਲ ਸਬੰਧਤ ਵਜ਼ੀਰਾਂ ਵਿਰੁੱਧ ਮੁੱਖ ਮੰਤਰੀ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ ਵੱਲੋਂ
ਵਿੱਢੀ ਭ੍ਰਿਸ਼ਟਾਚਾਰ ਵਿਰੋਧੀ ਮੁਹਿੰਮ ਨੂੰ ਠੁੱਸ ਕਰਨ ਲਈ ਅੱਜ ਗੁਰਦੁਆਰਾ ਮੰਜੀ ਸਾਹਿਬ ਆਲਮਗੀਰ
ਵਿੱਚ ਸ਼੍ਰੋਮਣੀ ਅਕਾਲੀ ਦਲ (ਬਾਦਲ) ਵੱਲੋਂ ਕਾਨਫਰੰਸ ਕੀਤੀ ਗਈ ਅਤੇ ਐਲਾਨ ਕੀਤਾ ਗਿਆ ਕਿ ਦਲ
ਸੂਬੇ ਭਰ ਵਿਚ ਰਾਜਸੀ ਕਾਨਫਰੰਸਾਂ ਦਾ ਸਿਲਸਿਲਾ ਜਾਰੀ ਰੱਖੇਗਾ।</p>

<p>ਸਾਂਝ
ਸਵੇਰਾ ਦੀ ਵੈਬ ਸਾਈਟ 'ਤੇ ਇੱਕ ਦਿਨ ਵਿੱਚ 10 ਹਜ਼ਾਰ ਤੋਂ ਵੱਧ ਹਿੱਟਾਂ ਆਉਣੀਆਂ ਸ਼ੁਰੂ ਹੋਈਆਂ</p>

<p>ਬਰੈਮਪਟਨ: ਸਾਂਝ
ਸਵੇਰਾ ਦੀ ਇੰਟਰਨੈਟ ਉਪਰ ਲਾਂਚ ਕੀਤੀ ਵੈਬ ਸਾਈਟ ਨੰੂ ਤਕਰੀਬਨ ਦੋ ਸਾਲ ਤੋਂ ਉਪਰ ਦਾ ਵਕਤ ਹੋ
ਗਿਆ ਹੈ। ਇਹ ਵੈਬ ਸਾਈਟ ਅਜਿਹੀ ਇੱਕ ਪਹਿਲੀ ਵੈਬ ਸਾਈਟ ਹੈ ਜੋ ਹਰ ਰੋਜ਼ ਪੰਜਾਬ ਦੀਆਂ ਖਬਰਾਂ
ਪ੍ਰਕਾਸ਼ਤ ਕਰਦੀ ਹੈ। ਇਸ
ਹਫਤੇ ਇਸ ਵੈਬ ਸਾਈਟ ਦੇ ਪਾਠਕਾਂ ਦੀ ਗਿਣਤੀ ਇੱਕ ਦਿਹਾੜੀ ਵਿੱਚ 10 ਹਜ਼ਾਰ ਤੋਂ ਵੱਧ ਚੁੱਕੀ ਹੈ।</p>

<p>ਇਸ ਹਫਤੇ ਦੀਆਂ
ਹਿੱਟਸ ਦਾ ਵੇਰਵਾ ਇਸ ਪ੍ਰਕਾਰ ਹੈ ਜੁਲਾਈ 14 ਐਤਵਾਰ 7477, ਸੋਮਵਾਰ 8993, ਮੰਗਲਵਾਰ 9431,
ਬੁੱਧਵਾਰ 9715, ਵੀਰਵਾਰ 10699, ਸ਼ੁਕਰਵਾਰ 10608 ਹਨ। ਸਾਂਝ ਸਵੇਰਾ ਇੰਮੀਗਰੇਸ਼ਨ, ਵਕੀਲ ਅਤੇ ਹੋਰਨਾਂ
ਸੇਵਾਵਾਂ ਮੁਹੱਈਆਂ ਕਰਨ ਵਾਲੇ ਅਦਾਰਿਆਂ ਨੰੂ ਇਸ ਸਾਈਟ ਤੇ ਐਡਵਰਟਾਈਜ਼ ਕਰਨ ਲਈ ਬੇਨਤੀ ਕਰਦੀ
ਹੈ ਤਾਂ ਕਿ ਜਿਥੇ ਇਸ ਸਾਈਟ ਰਾਹੀਂ ਤੁਸੀਂ ਆਪਣੇ ਅਦਾਰੇ ਦਾ ਦੁਨੀਆਂ ਭਰ ਦੇ ਪੰਜਾਬੀਆਂ ਤੱਕ
ਸੁਨੇਹਾ ਪਹੁੰਚਾ ਸਕੋਂਗੇ ਉਥੇ ਤੁਸੀਂ ਇਸ ਸਾਈਟ ਨੰੂ ਚੱਲਦੇ ਰੱਖਣ ਲਈ ਵੀ ਮਦਦ ਕਰੋਂਗੇ।</p>

<p> </p>






</body></text></cesDoc>