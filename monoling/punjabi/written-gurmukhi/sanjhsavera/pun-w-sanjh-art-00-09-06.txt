<cesDoc id="pun-w-sanjh-art-00-09-06" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-art-00-09-06.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 00-09-06</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>00-09-06</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਗੁਰਮਤਿ ਪ੍ਰਚਾਰ</p>









<p>ਦੂਜਿਆਂ ਦੀਆਂ ਨਜ਼ਰਾਂ ਫ਼#145;ਚ ਫ਼#145;ਸ੍ਰੀ
ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਜੀਫ਼#146;</p>

<p>ਸ਼ਹੀਦਾਂ ਦੇ ਸਿਰਤਾਜ ਪ੍ਰਤੱਖ ਹਰਿ ਸ੍ਰੀ
ਗੁਰੂ ਅਰਜਨ ਦੇਵ ਜੀ, ਬਾਬਾ ਬੁੱਢਾ ਜੀ ਦੇ ਸੀਸ ਤੇ ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਟਿਕਾ ਆਪ ਨੰਗੇ
ਪੈਰੀਂ ਚੌਰ ਕਰਦੇ ਆ ਰਹੇ ਸਨ। ਦੁਨੀਆਂ ਦੇ ਇਤਿਹਾਸ ਵਿਚ ਸੰਸਾਰ ਦੀਆਂ ਨਜ਼ਰਾਂ ਨੇ ਪਹਿਲੀ ਵਾਰ
ਕਿਸੇ ਗ੍ਰੰਥ ਦਾ ਇਤਨਾ ਸਤਿਕਾਰ ਹੁੰਦਾ ਵੇਖਿਆ। ਗੁਰੂ ਸਾਹਿਬਾਂ ਨੇ ਆਪਣੇ ਜਿਉਂਦੇ ਜੀਅ, ਸ਼ਬਦ
ਗੁਰੂ ਦੇ ਸਿਧਾਂਤ ਅਨੁਸਾਰ ਗੁਰੂ ਗੰ੍ਰਥ ਸਾਹਿਬ ਨੂੰ ਗੁਰੂ ਪਦਵੀ ਦੇ ਕੇ, ਆਪਣੀ ਉਮਤ ਨੂੰ ਇਸਦੇ
ਲੜ ਲਾਇਆ। ਇਸ ਲਈ ਜੋ ਸਤਿਕਾਰ ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਜੀ ਦਾ ਸਿੱਖ ਧਰਮ ਵਿਚ ਹੈ, ਉਹ
ਦੂਸਰੇ ਮੱਤਾਂ ਵਾਲੇ ਆਪਣੇ ਧਾਰਮਿਕ ਗ੍ਰੰਥਾਂ ਦਾ ਨਹੀਂ ਕਰਦੇ। ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਜੀ ਦੀ
ਮਹਾਨਤਾ ਬਾਰੇ ਜੋ ਅਨਮਤੀ ਵਿਦਵਾਨਾਂ ਦੇ ਖਿਆਲ ਹਨ, ਉਹ ਸਾਡੇ ਲਈ ਹੋਰ ਵੀ ਗੌਰਵਸ਼ਾਲੀ ਅਤੇ
ਵੰਗਾਰ ਦੀ ਹੈਸੀਅਤ ਰਖਦੇ ਹਨ।</p>

<p>1.) ਅੰਗਰੇਜ ਇਤਿਹਾਸਕਾਰ ਮਕਾਲਿਫ,
ਜਿਸਨੇ ਗੁਰਬਾਣੀ ਅਤੇ ਗੁਰੂ ਸਾਹਿਬਾਂ ਦੇ ਜੀਵਨ ਤੇ ਸਿੱਖ ਰਿਲੀਜਨ ਨਾਂ ਦਾ ਗ੍ਰੰਥ ਇੰਗਲਿਸ਼ ਵਿਚ
ਲਿਖਿਆ ਜਿਸਦੇ ਛੇ ਭਾਗ ਹਨ। ਉਹ ਆਖਦਾ ਹੈ, "ਸਿਖ ਧਰਮ ਅਤੇ ਜਿੱਥੋਂ ਤੱਕ ਇਸਦੇ ਮੁਖ
ਸਿਧਾਤਾਂ ਦਾ ਸੰਬੰਧ ਹੈ, ਦੂਸਰੇ ਲਗ ਭਗ ਸਾਰੇ ਧਰਮਾਂ ਨਾਲੋਂ ਭਿੰਨਤਾ ਤੇ ਵਿਸ਼ੇਸ਼ਤਾ ਰਖਦਾ ਹੈ।
ਸੰਸਾਰ ਵਿਚ ਜੋ ਵਡੇ ਵਡੇ ਪ੍ਰਚਾਰਕ ਹੋਏ ਹਨ, ਉਨ੍ਹਾਂ ਵਿਚੋਂ ਕੋਈ ਵੀ ਆਪਣੀ ਲਿਖੀ ਹੋਈ ਇਕ
ਲਾਈਨ ਵੀ ਪਿਛੇ ਛਡ ਕੇ ਨਹੀਂ ਗਿਆ। ਜੋ ਕੁਝ ਉਨ੍ਹਾਂ ਪ੍ਰਚਾਰਿਆ, ਉਸਦਾ ਪਤਾ ਸਾਨੂੰ ਜਾਂ ਤਾਂ
ਪ੍ਰਚੱਲਤ ਰਵਾਇਤਾਂ ਜਾਂ ਹੋਰਨਾਂ ਦੀਆਂ ਲਿਖਤਾਂ ਤੋਂ ਲਗਦਾ ਹੈ। ਜਿਵੇਂ ਸ਼ੁਕਰਾਤ ਅਤੇ ਫੈਸਾਗੋਰਸ ਦੀਆਂ
ਸਿਖਿਆਵਾਂ ਦਾ ਪਤਾ ਅਫਲਾਤੂਨ ਪਲੈਟੋ ਅਤੇ ਜਾਨੋਫਰ ਦੀਆਂ ਲਿਖਤਾਂ ਤੋਂ ਹੀ ਲਗਦਾ ਹੈ। ਬੁੱਧ ਆਪਣੀ
ਸਿਖਿਆ ਦੀ ਯਾਦ ਆਪਣੀ ਕਿਸੇ ਵੀ ਲਿਖਤ ਵਿਚ ਨਹੀਂ ਛੱਡ ਗਿਆ। ਪੱਛਮੀ ਲੋਕ ਜਿਸਨੂੰ ਕਨਫਿਊਸ਼ਿਸ਼
ਕਹਿੰਦੇ ਹਨ, ਆਪਣੇ ਸਮਾਜਕ ਤੇ ਇਖਲਾਕੀ ਸਿਧਾਤਾਂ ਬਾਰੇ ਕੋਈ ਲਿਖਤ ਨਹੀਂ ਛਡ ਕੇ ਗਿਆ। ਈਸਾਈ
ਮਤ ਦੇ ਬਾਨੀ ਨੇ ਵੀ ਆਪਣੇ ਸਿਧਾਤਾਂ ਨੂੰ ਕਿਸੇ ਲਿਖਤ ਰੂਪ ਵਿਚ ਪੇਸ਼ ਨਹੀਂ ਕੀਤਾ। ਉਨ੍ਹਾਂ
ਸੰਬੰਧੀ, ਮੈਥਿਊ, ਲੂਕਾ, ਜ਼ੌਹਨ ਤੇ ਮਤੀ ਆਦਿ ਸੰਤਾਂ ਦੀਆਂ ਲਿਖਤਾਂ ਤੇ ਹੀ ਭਰੋਸਾ ਕਰਨਾ ਪੈਂਦਾ
ਹੈ। ਅਰਬ ਦੇ ਪੈਗੰਭਰ ਹਜਰਤ ਮੁਹੰਮਦ ਸਾਹਿਬ ਨੇ ਵੀ ਕੁਰਾਨ ਦੇ ਸਪਾਰਿਆ ਨੂੰ ਆਪਣੀ ਕਲਮ ਨਾਲ
ਨਹੀਂ ਲਿਖਿਆ, ਸਗੋਂ ਉਸਨੂੰ ਖਲੀਫਿਆਂ ਜਾਂ ਨਾਮ ਲੇਵਿਆ ਨੇ ਹੀ ਲਿਖਿਆ ਜਾਂ ਇਕੱਤਰ ਕੀਤਾ।
ਭਾਰਤੀ ਵੇਦ ਜਿਨ੍ਹਾਂ ਨੂੰ ਬ੍ਰਹਮਾ ਕ੍ਰਿਤ ਆਖਿਆ ਜਾਂਦਾ ਹੈ ਉਹ ਵੀ ਆਪਣੇ ਉਚਾਰਣ ਤੋਂ ਹਜ਼ਾਰਾਂ ਸਾਲ
ਬਾਅਦ ਲਿਖਤ ਰੂਪ ਵਿਚ ਆਏ। ਪ੍ਰੰਤੂ ਸਿੱਖ ਗੁਰੂਆਂ ਦੀ ਬਾਣੀ ਉਨ੍ਹਾਂ ਦੇ ਆਪਣੀ ਹੱਥੀਂ ਲਿਖੀ ਤੇ
ਸੰਭਾਲੀ ਹੋਈ ਹੀ ਨਹੀਂ ਮਿਲਦੀ, ਸਗੋਂ ਗੁਰੂ ਗੋਬਿੰਦ ਸਿੰਘ ਜੀ ਆਪਣੇ ਜੀਵਨ ਕਾਲ ਵਿਚ ਹੀ ਇਸ
ਮਹਾਨ ਗ੍ਰੰਥ ਨੂੰ ਗੁਰੂ ਪਦਵੀ ਦੇ ਕੇ ਆਪਣੀ ਉਮਤ ਨੂੰ ਇਸਦੇ ਲੜ ਲਾ ਗਏ।"</p>

<p>2.) ਈਸ਼ਵਰੀ ਪ੍ਰਸ਼ਾਦ ਆਪਣੀ
ਪੁਸਤਕ "The greatest book of
synthesis worthy of reverence."
ਵਿਚ ਆਈਨੇ ਅਕਬਰੀ ਦਾ ਹਵਾਲਾ ਦੇਂਦੇ ਹੋਏ ਲਿਖਦੇ ਹਨ ਕਿ, "ਇਹ ਪਹਿਲਾ ਧਾਰਮਿਕ ਗ੍ਰੰਥ
ਹੈ ਜਿਸ ਵਿਚ ਦੇਸ਼ਾਂ ਨਸਲਾਂ ਦੀਆਂ ਸਾਰੀਆਂ ਵਿੱਥਾਂ ਨੂੰ ਮੇਟ ਕੇ ਹਰ ਹਿੰਦੂ, ਮੁਸਲਮਾਨ ਤੇ ਅਛੂਤ ਦੇ
ਪਵਿਤ੍ਰ ਰੂਹਾਨੀ ਬਚਨਾਂ ਨੁੰ ਇਕੱਤਰ ਕਰਕੇ ਇਕ ਸਮਾਨ ਸਤਿਕਾਰ ਦਿਤਾ ਗਿਆ ਹੈ। ਅਕਬਰ ਬਾਦਸ਼ਾਹ ਨੇ
ਇਸਨੂੰ ਮਹਾਨ ਪਵਿਤ੍ਰ ਤੇ ਵੱਖ-2 ਖਿਆਲਾਂ ਦੇ ਮੇਲ ਦਾ ਗ੍ਰੰਥ ਮੰਨ ਕੇ ਸਿਰ ਝੁਕਾਇਆ ਸੀ।"</p>

<p>3.) ਡਾ: ਚੌਧਰੀ ਖੁਦਾ-ਦਾਦ
ਕਿਹਾ ਕਰਦੇ ਸਨ ਕਿ ਫ਼#145;ਹਰ ਇਕ ਧਰਮ ਦਾ ਤੁਲਨਾਤਮਿਕ ਅਧਿਐਨ ਕਰਨ ਦਾ ਅਸੂਲ ਗੁਰੂ ਗ੍ਰੰਥ
ਸਾਹਿਬ ਨੇ ਹੀ ਆਰੰਭ ਕੀਤਾ।ਫ਼#146;</p>

<p>4.) ਮਿਸਟਰ ਟ੍ਰੰਪ ਨੇ ਗੁਰੂ
ਗ੍ਰੰਥ ਸਾਹਿਬ ਨੂੰ ਫ਼#145;ਭਾਰਤੀ ਬੋਲੀਆਂ ਦਾ ਖਜਾਨਾਫ਼#146; ਆਖਿਆ ਹੈ। ਕਿਉਂਕਿ ਗੁਰੂ ਗ੍ਰੰਥ
ਸਾਹਿਬ ਅੰਦਰ ਚੌਦਾਂ ਦੇ ਕਰੀਬ ਭਾਸ਼ਾਵਾਂ ਵਰਤੀਆਂ ਗਈਆਂ ਹਨ।</p>

<p>5.) ਜੇ ਐਚ ਗਾਰਡਨ ਨੇ
ਫ਼#145;ਪਵਿਤਰ ਕਾਨੂੰਨ ਦੀ ਕਿਤਾਬਫ਼#146; ਆਖਿਆ ਹੈ।</p>

<p>6.) ਸਾਧੂ ਟੀ ਐਲ ਵਾਸਵਾਨੀ -
ਫ਼#145;ਇਸਨੂੰ ਸਾਰੀ ਦੁਨੀਆਂ ਦਾ ਧਾਰਮਿਕ ਗੰ੍ਰਥਫ਼#146; ਮੰਨਦਾ ਹੈ।</p>

<p>7.) ਸੰਸਾਰ ਪ੍ਰਸਿੱਧ ਇਤਿਹਾਸਕਾਰ ਆਰ
ਨੋਲਡ ਟਾਇਨਬੀ, ਜਿਸ ਨੇ ਸੰਸਾਰ ਭਰ ਦੀਆਂ ਪ੍ਰਮੁੱਖ ਇੱਕੀ ਕੌਮਾਂ ਦਾ ਮੁਤਾਲਿਆ ਕਰਕੇ, ਦਸ
ਭਾਗਾਂ ਵਿਚ ਸੰਸਾਰ ਦਾ ਸ਼ੋਸ਼ਲ ਇਤਿਹਾਸ ਲਿਖਿਆ ਹੈ, ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਬਾਰੇ ਆਖਦਾ ਹੈ ਕਿ-
"ਇਨਸਾਨ ਦਾ ਧਾਰਮਿਕ ਭਵਿਸ਼ਅ ਸਪਸ਼ਟ ਨਾ ਹੋਵੇ, ਪਰ ਇਕ ਗੱਲ ਪੱਕੀ ਹੈ ਕਿ ਜਿਉਂਦੇ ਜਾਗਦੇ
ਧਰਮ ਹੁਣ ਅਗੇ ਨਾਲੋਂ ਬਹੁਤਾ ਪ੍ਰਭਾਵ ਇਕ ਦੂਸਰੇ ਤੇ ਪਾਉਣਗੇ ਅਤੇ ਆਉਣ ਵਾਲੀ ਧਰਮਾਂ ਦੀ ਪਰਸਪਰ
ਗੋਸ਼ਟੀ ਵਿਚ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦੀ ਆਵਾਜ਼ ਨਿਸਚੇ ਕਰਕੇ ਸੰਸਾਰ ਭਰ ਲਈ ਵਿਸ਼ੇਸ਼ ਮਹੱਤਵ ਵਾਲੀ
ਹੋਵੇਗੀ।"</p>

<p>8.) ਡਾ: ਰਾਧਾ ਕ੍ਰਿਸ਼ਨਨ ਨੇ
ਆਖਿਆ ਸੀ ਕਿ "ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦੀਆਂ ਸਤਿਨਾਮ ਵਾਲੀਆਂ ਧੁਨੀਆਂ ਜਿੰਨ੍ਹਾਂ ਨੂੰ ਨਿਰਮਲ
ਵੇਦਨਾ ਅਤੇ ਅਪਾਰ ਸ਼ਰਧਾ ਵਿਚ ਗੜੂੰਦ ਹੋ ਗਾਇਆ ਗਿਆ ਹੈ, ਨੂੰ ਸਾਗਰਾਂ ਦੀਆਂ ਵਿਥਾਂ ਅਤੇ ਪਹਾੜਾਂ
ਦੀਆਂ ਰੋਕਾਂ ਰਾਹ ਦੇਣਗੀਆਂ।"</p>

<p>9.) ਡਾ: ਗ੍ਰੀਨਲੀਜ ਦੇ ਅਨੁਸਾਰ
ਫ਼#145;ਗੁਰੂ ਗ੍ਰੰਥ ਦੀ ਬਾਣੀ ਧੁਰ ਅੰਦਰਲੇ ਅਨੁਭਵ ਤੇ ਪ੍ਰਕਾਸ਼ ਤੋਂ ਹੈ।ਫ਼#146; ਉਹ ਆਖਦਾ ਹੈ ਕਿ
"ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦੇ ਅਧਿਐਨ ਤੇ ਖੋਜ ਦਾ ਮੈਨੂੰ ਬਹੁਤ ਲਾਭ ਹੋਇਆ। ਜਿਤਨਾ ਮੈਂ ਇਸ
ਵਿਚ ਡੂੰਘਾ ਗਿਆ ਓਨਾ ਹੀ ਮੇਰਾ ਪਿਆਰ ਇਸਦੇ ਨਾਲ ਵਧਦਾ ਗਿਆ।" ਅਤੇ ਕਹਿੰਦਾ ਹੈ,
"ਸਦੀਆਂ ਤੋਂ ਬਾਣੀ ਰਾਹੀਂ ਗੁਰੂ ਆਪਣੇ ਸ਼ਰਧਾਲੂਆਂ ਨਾਲ ਗੱਲਾਂ ਕਰਦਾ ਆ ਰਿਹਾ ਹੈ।"</p>

<p>10.) ਭਾਰਤ ਦਾ ਕੌਮੀ ਤਰਾਨ੍ਹਾ (ਜਨ, ਗਨ,
ਮਨ) ਲਿਖਣ ਵਾਲੇ ਡਾ: ਰਵਿੰਦਰ ਨਾਥ ਟੈਗੋਰ ਤੋਂ ਇਕ ਵਾਰ ਵਿਨੋਭਾ-ਭਾਵੇ ਨੇ
ਪੁਛਿਆ ਕਿ ਫ਼#145;ਅੰਤਰ-ਰਾਸ਼ਟਰੀ ਬ੍ਰਹਮੰਡੀ ਤਰਾਨ੍ਹਾ ਕਿਹੜਾ ਹੋ ਸਕਦਾ ਹੈਫ਼#146; ਤਾਂ ਟੈਗੋਰ ਨੇ
ਝੱਟਪੱਟ-ਸ੍ਰੀ ਗੁਰੂ ਨਾਨਕ ਦੇਵ ਜੀ ਦਾ ਸ਼ਬਦ - ਫ਼#145;ਗਗਨ ਮੈ ਥਾਲ ਰਵਿ ਚੰਦ ਦੀਪਕ ਬਨੇਫ਼#146;
ਵਾਲਾ ਸ਼ਬਦ ਪੜਕੇ ਸੁਣਾਇਆ, ਅਤੇ ਕਿਹਾ ਕਿ ਫ਼#145;ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਵਿਚ ਪ੍ਰਜਵਲਤ ਸ਼ਬਦ ਜੋਤੀ
ਸਹਜੇ ਹੀ ਮਨੁੱਖ ਮਾਤਰ ਨੂੰ ਉਹ ਕੁਝ ਪ੍ਰਦਾਨ ਕਰ ਸਕਦੀ ਹੈ ਜਿਸਦੀ ਪ੍ਰਾਪਤੀ ਲਈ ਉਸਦੀ ਆਤਮਾ
ਵਿਆਕੁਲ ਹੈ।ਫ਼#146;</p>

<p>11.) ਮਿਸ ਪਰਲ ਐਸ ਬਕ ਕਹਿੰਦੀ
ਹੈ ਕਿ, "ਮੈਂ ਵੱਡੇ-2 ਧਰਮਾਂ ਦੀਆਂ ਇਸ਼ਟ ਪੁਸਤਕਾਂ ਦਾ ਅੀਧਐਨ ਕੀਤਾ ਹੈ, ਪਰ ਜੋ ਖਿਚ ਅਤੇ
ਅਪੀਲ ਮਨ ਤੇ ਦਿਲ ਨੂੰ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਕਰਦਾ ਹੈ, ਉਹ ਹੋਰ ਕਿਸੇ ਥਾਂ ਤੋਂ ਨਹੀਂ ਮਿਲਦੀ ....
ਇਸ ਗ੍ਰੰਥ ਵਿਚ ਕੋਈ ਅਜੀਬ ਨਵੀਨਤਾ ਲਗਦੀ ਹੈ ..... ਇਸਦਾ ਉਪਦੇਸ਼ ਹਰ ਧਰਮ ਵਾਲੇ ਲਈ ਹੈ,
ਕਿਸੇ ਇਕ ਲਈ ਨਹੀਂ।"</p>

<p>12.) ਇਕ ਇਸਾਈ ਮਿਸ਼ਨਰੀ, ਮਿਸ
ਚਾਰਲਟ ਮੇਰੀ ਟੱਕਰ, ਪੰਜਾਬੀ ਸਿਖਕੇ, ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦਾ ਅਧਿਐਨ ਕਰਕੇ ਲਿਖਦੀ ਹੈ ਕਿ,
"ਜਿੱਥੋਂ ਤੱਕ ਮੈਂ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਪੜਿਆ ਅਤੇ ਪੜ ਕੇ ਮਹਿਸੂਸ ਕੀਤਾ ਹੈ ਕਿ ਇਹ ਧਰਮ
ਗ੍ਰੰਥ ਬੜਾ ਬਚਿਤਰ ਹੈ, ਇਸ ਵਿਚ ਹੈਰਾਨ ਕਰਨ ਵਾਲੀ ਨਿਰਮਲਤਾ ਤੇ ਆਤਮਕ ਸਤਿਆ ਹੈ। ਜੇ ਕਰ ਹਰੀ
ਸ਼ਬਦ ਦੀ ਥਾਂ ਯਸੂਹ ਮਸੀਹ ਦਾ ਨਾਂ ਲਿਖਿਆ ਜਾਵੇ ਤਾਂ ਇਹ ਪ੍ਰਾਚੀਨ ਇਸਾਈ ਰਿਸ਼ੀਆਂ ਤੇ ਤਪਸਵੀਆਂ ਦੀ
ਰਚਨਾ ਪ੍ਰਤੀਤ ਹੁੰਦੀ ਹੈ। ਇਹ ਆਤਮਕ ਸੱਧਰਾਂ ਦੀ ਤ੍ਰਿਪਤੀ ਪੂਰਨ ਗ੍ਰੰਥ ਹੈ, ਮੈਨੂੰ ਸ਼ਰਮ ਆਉਂਦੀ
ਹੈ ਕਿ ਮੈਂ ਇਨ੍ਹਾਂ ਗਰੀਬ ਸਿੱਖਾਂ ਤੋਂ ਆਤਮਕ ਪ੍ਰਪੱਕਤਾ ਵਿਚ ਬਹੁਤ ਪਿਛੇ ਹਾਂ।"</p>

<p>13.) ਸ੍ਰੀ ਰਣਬੀਰ ਸੰਪਾਦਕ ਮਿਲਾਪ
ਦੇ ਅਨੁਸਾਰ, ਫ਼#145;ਕੋਈ ਮਨੁੱਖੀ ਜਬਾਨ ਐਸੀ ਨਹੀਂ ਜੋ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦਾ ਅਧਿਐਨ ਕਰਨ
ਮਗਰੋਂ ਸ਼ਰਧਾ ਸਤਿਕਾਰ ਵਿਚ ਕੁਝ ਨਾ ਬੋਲੇ।ਫ਼#146;</p>

<p>14.) ਸ੍ਰੀ ਐਚ ਐਲ ਬਰਨਾਡ ਸ਼ਾਹ
ਦੇ ਅਨੁਸਾਰ, ਫ਼#145;ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦਾ ਧਰਮ ਸਾਇੰਸ ਦੀ ਕਸਵੱਟੀ ਤੇ ਪੂਰਾ ਉਤਰਦਾ ਹੈ। ਇਹ
ਧਰਮ ਸਮੁੱਚੇ ਵਿਸ਼ਵ ਦਾ ਧਰਮ ਹੈ, ਜਿਸ ਵਿਚ ਹਰ ਪ੍ਰਕਾਰ ਦੇ ਮਨੁੱਖ ਲਈ ਸੰਦੇਸ਼ ਮੌਜੂਦ ਹੈ। ਗੁਰੂ
ਗ੍ਰੰਥ ਸਾਹਿਬ ਅੰਦਰ ਪੂਰਨ ਸਚਾਈ ਹੈ ਅਤੇ ਇਹ ਨਵੇਂ ਯੁਗ ਲਈ ਧਰਮ ਹੈ।ਫ਼#146;</p>

<p>15.) ਇੰਗਲੈਂਡ ਦੇ ਪ੍ਰਸਿਧ ਫਿਲਾਸਫਰ ਫ਼#145;ਰੱਸਲਫ਼#146;
ਦਾ ਕਹਿਣਾ ਹੈ ਕਿ ਫ਼#145;ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਸੰਸਾਰ ਨੂੰ ਤੀਸਰੀ ਵਿਸ਼ਵਜੰਗ ਦੇ ਐਟਮ ਤੇ
ਹਾਈਡਰੋਜਨ ਬੰਬਾਂ ਦੀ ਮਾਰ ਤੋਂ ਬਚਾ ਸਕਦਾ ਹੈ, ਜੇਕਰ ਸਿੱਖ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦੀ ਆਵਾਜ ਨੂੰ
ਸਮੁੱਚੀ ਮਨੁੱਖਤਾ ਤੱਕ ਪਹੁੰਚਾਉਣ ਦਾ ਯਤਨ ਕਰਨ।ਫ਼#146;</p>

<p>16.) ਬੰਗਾਲੀ ਲੇਖਕ- ਜਨਿੰਦਰ ਮੋਹਨ,
ਜਪੁਜੀ ਸਾਹਿਬ ਦਾ ਬੰਗਾਲੀ ਭਾਸ਼ਾ ਵਿਚ ਅਨੁਵਾਦ ਕਰਨ ਉਪਰੰਤ, ਇਸਨੂੰ ਫ਼#145;ਗਣ ਗੀਤਾਫ਼#146;
ਭਾਵ ਲੋਕਾਂ ਦੀ ਗੀਤਾ ਕਹਿਕੇ ਸਨਮਾਨ ਕਰਦਾ ਆਖਦਾ ਹੈ ਕਿ ਫ਼#145;ਜੇ ਕਰ ਜਪੁਜੀ ਸਾਹਿਬ ਲੋਕਾਂ ਤੱਕ
ਪਹੁੰਚਾਇਆ ਜਾਵੇ ਤਾਂ ਇਕ ਨਵਾਂ ਆਤਮਿਕ ਇਨਕਲਾਬ ਆ ਸਕਦਾ ਹੈ।ਫ਼#146;</p>

<p>17.) ਡਾ: ਧਰਮ ਪਾਲ ਅਨੁਸਾਰ
ਫ਼#145;ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਅੱਜ ਦੇ ਵਿਗਿਆਨਕ ਤੇ ਬੌਧਿਕ ਯੁਗ ਵਿਚ ਵੀ ਆਉਣ ਵਾਲੀਆਂ ਪੀੜੀਆਂ
ਨੂੰ ਧਰਮ ਤੋਂ ਬੇਮੁਖ ਹੋਣ ਤੋਂ ਬਚਾਉਂਦਾ ਰਹੇਗਾ। ਮਨੁੱਖ ਦੀਆਂ ਸਾਰੀਆਂ ਸਮੱਸਿਆਵਾਂ ਦਾ ਸਮਾਧਾਨ
ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਵਿਚ ਆਪਣੇ ਢੰਗ ਨਾਲ ਮਿਲਦਾ ਹੈ। ਜਿਸਦੀ ਜਿੰਦਗੀ ਦੇ ਕਿਸੇ ਨਾ ਕਿਸੇ ਮੋੜ ਤੇ
ਇਨਸਾਨ ਨੂੰ ਲੋੜ ਪੈਂਦੀ ਹੈ।ਫ਼#146;</p>

<p>18.) ਸਵਾਮੀ ਰਾਮ ਤੀਰਥ ਦੰਡੀ
ਸੰਨਿਆਸੀ-ਭਾਰਤੀ ਧਰਮਾਂ ਤੇ ਸੰਸਕ੍ਰਿਤ ਦੇ ਮੰਨੇ ਪ੍ਰਮੰਨੇ ਸਕਾਲਰ, ਜਿਨ੍ਹਾਂ ਨੇ ਵੇਦਾਂ, ਪੁਰਾਣਾਂ,
ਸਿਮਰਤੀਆਂ, ਉਪਨਿਸ਼ਦਾਂ ਤੇ ਖੋਜ ਕਰਕੇ ਪੁਸਤਕਾਂ ਲਿਖੀਆਂ, ਉਹ ਸਿੱਖ ਧਰਮ ਫਲਸਫਾ ਪੜਕੇ ਸਿੱਖ ਸਜ
ਗਏ ਅਤੇ ਉਨ੍ਹਾਂ ਨੇ ਸਰਵੋਤਮ ਧਰਮ ਗ੍ਰੰਥ ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਅਤੇ ਸਰਵੋਤਮ ਪੰਥ ਖਾਲਸਾ ਪੰਥ
ਨਾਂ ਦੀਆਂ ਦੋ ਪੁਸਤਕਾਂ ਵੀ ਲਿਖੀਆ, ਉਹਨਾਂ ਦਾ ਕਹਿਣਾ ਹੈ ਕਿ, "ਜੀਵਨ ਭਰ ਸਾਧੂ ਸੰਤੋਂ ਕੀ
ਸੰਗਤ ਕੀ, ਤਪ-ਸਾਧੇ, ਤੀਰਥੋਂ ਪਰ ਭਰਮਣ ਕੀਆ, ਅੰਤ ਇਸੀ ਨਿਸਚੇ ਪਰ ਪਹੁੰਚਾ ਹੂੰ ਕਿ ਜੋ ਗੁਰੂ
ਘਰ (ਭਾਵ ਗੁਰੂ ਗ੍ਰੰਥ) ਮੇਂ ਹੈ ਔਰ ਕਹੀਂ ਨਹੀਂ।"</p>

<p>19.) ਸਵਾਮੀ ਬ੍ਰਹਮਾ ਨੰਦ ਦੀ
ਆਤਮਾ ਦੀ ਪੁਕਾਰ ਹੈ, ਕਿ- ਫ਼#145;ਸਾਰੀ ਜਿੰਦਗੀ ਬੜੀ ਖਾਕ ਛਾਣੀ ਪਰ ਅੰਮ੍ਰਿਤ ਦੇ ਖਜਾਨੇ ਸ੍ਰੀ
ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਅਤੇ ਗੁਰਸਿੱਖੀ ਸਿਧਾਂਤਾਂ ਅਨੁਸਾਰ ਜਿੰਦਗੀ ਬਤੀਤ ਕਰਨ ਦਾ ਅਵਸਰ ਪ੍ਰਾਪਤ ਨਾ ਹੋ
ਸਕਿਆ। ਮੁਕਤੀ ਦੇ ਖਜ਼ਾਨੇ ਨੂੰ ਛਡ ਕੇ ਹੋਰ ਕਿਧਰੇ ਭਟਕਣਾ ਬਿਲਕੁਲ ਬੇਅਰਥ ਹੈ, ਕਾਸ਼, ਮੈਨੂੰ ਇਸ
ਗਾਡੀ ਰਾਹ ਦੀ ਪਹਿਲੇ ਪਹਿਚਾਣ ਹੋ ਜਾਂਦੀ। ਹੁਣ ਤਾਂ ਇਸ ਅੰਮ੍ਰਿਤ ਨੂੰ ਭੁੰਚਣ ਲਈ ਇਕ ਹੋਰ ਜਨਮ
ਲੈਣਾ ਪਵੇਗਾ।ਫ਼#146; ਇਹ ਸ਼ਬਦ ਕਹਿਕੇ ਆਪਣੇ ਚੇਲੇ ਸਤਿਆ ਨੰਦ ਦੇ ਸਾਹਮਣੇ ਪ੍ਰਾਣ ਤਿਆਗ ਦਿਤੇ।</p>

<p>20.) ਪ੍ਰਸਿਧ ਕਾਨੂੰਨਦਾਰ ਡਾ:
ਅੰਬੇਦਕਾਰ ਨੇ 1931 ਵਿਚ ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦੀ ਹਿੰਦੀ ਬੀੜ ਦਾ ਡੂੰਘਾ ਅਧਿਐਨ ਕਰਨ
ਤੋਂ ਬਾਅਦ ਐਲਾਨ ਕੀਤਾ ਕਿ "ਅਛੂਤ ਤੇ ਪਛੜੀਆਂ ਸ੍ਰੇਣੀਆਂ ਦੀ ਆਤਮਕ ਤੇ ਸ਼ੋਸ਼ਲ ਤ੍ਰਿਪਤੀ
ਜਿਤਨੀ ਗੁਰਮਤ ਕਰ ਸਕਦੀ ਹੈ, ਉਤਨੀ ਕਿਸੇ ਹੋਰ ਮੱਤ ਤੋਂ ਆਸ ਨਹੀਂ ਕੀਤੀ ਜਾ ਸਕਦੀ।" ਡਾ:
ਸਾਹਿਬ ਨੇ ਆਪਣੇ ਕੋ੍ਰੜਾਂ ਦੀ ਗਿਣਤੀ ਵਿਚ ਸਾਥੀਆਂ ਸਮੇਤ ਸਿੱਖ ਸਜਣ ਦੀ ਇਛਾ ਪ੍ਰਗਟ ਕੀਤੀ, ਪਰ
ਕੁਝ ਹਿੰਦੁਸਤਾਨੀ ਲੀਡਰਾਂ ਦੀ ਈਰਖਾ ਦਾ ਸਿੱਟਾ ਅਤੇ ਸਾਡੇ ਕੁਝ ਖੁਦਗਰਜ, ਅਨਾੜੀ ਸਿਆਸੀ ਸਜਣਾਂ
ਦੀ ਸੌੜੀ ਸੋਚ ਦਾ ਸਦਕਾ ਇਹ ਸੁਨਹਿਰੀ ਮੌਕਾ ਹੱਥੋਂ ਚਲਾ ਗਿਆ।</p>

<p>ਇਹ ਕੁਝ ਚੋਣਵੇਂ ਗੈਰ ਸਿੱਖ ਵਿਦਵਾਨਾਂ
ਦੇ ਵਿਚਾਰ ਹਨ। ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਜੀ ਦੇ ਸ਼ਰਧਾਲੂਆਂ ਦਾ ਘੇਰਾ ਬੜਾ ਵਿਸ਼ਾਲ ਹੈ। ਸਾਨੂੰ ਅਹਿਸਾਸ
ਹੋ ਜਾਣਾ ਚਾਹੀਦਾ ਹੈ ਕਿ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਦੀ ਸਿਖਿਆ ਸਾਰੀ ਮਨੁੱਖਤਾ ਲਈ ਹੈ, ਨਾ ਕਿ ਕੇਵਲ
ਸਿੱਖਾਂ ਲਈ। ਗੈਰ ਸਿੱਖ ਵਿਦਵਾਨਾਂ ਦੇ ਉਤਸ਼ਾਹੀ ਅਤੇ ਸ਼ਰਧਾ ਭਰੇ ਬੋਲ, ਅਸਲ ਵਿਚ ਉਨ੍ਹਾਂ ਪੈਂਤੀ
ਦੈਵੀ ਮਹਾਂਪੁਰਖਾਂ ਅਤ ਮਹਾਨ ਆਤਮਾਂਵਾਂ ਦੀਆਂ ਅਵਾਜਾਂ ਹਨ, ਜੋ ਸਾਰੀ ਦੁਨੀਆਂ ਨੂੰ ਸੁਣਨੀਆਂ
ਚਾਹੀਦੀਆਂ ਹਨ। ਜੇ ਕਰ ਅਜੇ ਤੱਕ ਨਹੀਂ ਸੁਣੀਆਂ ਗਈਆਂ ਤਾਂ ਇਸਦੇ ਸਭ ਤੋਂ ਵੱਧ ਕਸੂਰਵਾਰ ਉਹ
ਸਿੱਖ ਹਨ, ਜੋ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਨੂੰ ਕੇਵਲ ਸਿੱਖਾਂ ਦਾ ਹੀ ਗੁਰੂ ਸਮਝ ਕੇ ਸੰਤੁਸ਼ਟ ਹੋਏ ਬੈਠੇ
ਹਨ।</p>

<p>ਜੇ ਕਰ ਅੱਜ ਤੋਂ ਪੰਜ ਸੌ ਸਾਲ ਪਹਿਲਾਂ
ਆਵਾਜਾਈ ਦੇ ਸੀਮਤ ਸਾਧਨਾਂ ਦੇ ਬਾਵਯੂਦ ਸ੍ਰੀ ਗੁਰੂ ਨਾਨਕ ਦੇਵ ਜੀ ਅਤੇ ਬਾਕੀ ਗੁਰੂ ਸਾਹਿਬਾਨ,
ਭਾਰਤ ਦੇ ਵੱਖ-2 ਦੁਰਾਡੇ ਇਲਾਕਿਆਂ ਵਿਚ ਜਾ ਕੇ, ਸੰਤਾਂ, ਭਗਤਾਂ ਤੇ ਦਰਵੇਸ਼ਾਂ ਦੀ ਬਾਣੀ ਇਕਤਰ ਕਰ
ਸਕਦੇ ਹਨ। ਕੀ, ਅੱਜ ਅਸੀਂ ਉਨ੍ਹਾਂ ਅਸਥਾਨਾਂ ਤੇ ਜਾ ਕੇ, ਭਗਤ ਕਬੀਰ, ਬਾਬਾ ਫਰੀਦ, ਰਵਿਦਾਸ,
ਨਾਮਦੇਵ ਆਦਿ ਭਗਤਾਂ ਦੇ ਸ਼ਰਧਾਲੂਆਂ ਨਾਲ ਗੁਰ-ਭਾਈ ਹੋਣ ਦੀ ਸਾਂਝ ਜਾਂ ਨੇੜਤਾ ਪੈਦਾ ਨਹੀਂ ਕਰ
ਸਕਦੇ?</p>

<p>ਅੱਜ ਲੱਖਾਂ ਦੀ ਗਿਣਤੀ ਵਿਚ ਕਬੀਰ ਪੰਥੀ,
ਫਰੀਦ ਪੰਥੀ ਤੇ ਰਵਿਦਾਸ ਪੰਥੀ ਹਨ। ਜਦੋਂ ਉਨ੍ਹਾਂ ਨੂੰ ਪਤਾ ਲਗੇਗਾ ਕਿ ਸਿੱਖ ਉਨ੍ਹਾਂ ਦੇ ਗੁਰੂਆਂ
ਦੀ ਬਾਣੀ ਨੂੰ ਰੱਬੀ ਬਾਣੀ ਵਾਂਗ ਸ਼ਰਧਾ ਸਤਿਕਾਰ ਨਾਲ ਪੜਦੇ ਤੇ ਮੱਥਾ ਟੇਕਦੇ ਹਨ, ਤਾਂ ਗੁਰੂ ਗ੍ਰੰਥ
ਸਾਹਿਬ ਦੇ ਅਦਬ ਸਤਿਕਾਰ ਅਤੇ ਮਾਨਵ ਸਮਾਜ ਵਿਚ ਧਾਰਮਿਕ ਇਨਕਲਾਬ ਆ ਸਕਦਾ ਹੈ। ਸਾਨੂੰ ਤਾਂ ਪਹਿਲਾਂ
ਹੀ ਅਕਾਲ ਪੁਰਖ ਵੱਲੋਂ ਬ੍ਰਹਿਮੰਡੀ ਨਾਅਰਾ ਬਖਸ਼ਿਆ ਗਿਆ ਹੈ।</p>

<p>"ਨਾਨਕ ਨਾਮ ਚੜਦੀ ਕਲਾ   
ਤੇਰੇ ਭਾਣੇ ਸਰਬੱਤ ਦਾ ਭਲਾ।"</p>









<p>ਸਭ ਦਾ ਸਾਂਝਾ ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ</p>

<p>ਜਦੋਂ ਅਸੀਂ ਸੰਸਾਰ ਦੇ
ਵੱਖ ਵੱਖ ਧਰਮਾਂ ਦੇ ਧਰਮ ਗ੍ਰੰਥਾਂ ਦਾ ਅਧਿਐਨ ਕਰਦੇ ਹਾਂ ਤਾਂ ਇਕ ਗੱਲ ਉਭਰਕੇ ਸਾਹਮਣੇ ਆਉਂਦੀ ਹੈ
ਕਿ ਉਨ੍ਹਾਂ ਗ੍ਰੰਥਾਂ ਵਿਚਲਾ ਉਪਦੇਸ ਕਿਸੇ ਵਿਸ਼ੇਸ਼ ਵਰਗ ਨੂੰ ਮੁੱਖ ਕਰਕੇ ਦਿੱਤਾ ਗਿਆ ਹੈ। ਉਦਾਹਰਣ
ਦੇ ਤੌਰ ਤੇ ਵੇਦਾ ਜਾਂ ਹਿੰਦੂ ਗ੍ਰੰਥਾ ਦਾ ਉਪਦੇਸ ਕੇਵਲ ਹਿੰਦੂਆਂ ਜਾਂ ਬ੍ਰਾਹਮਣਾ ਵਾਸਤੇ ਰਾਖਵਾਂ ਹੈ।
ਉਸਨੂੰ ਮੁਸਲਮਾਨ ਤੇ ਹੋਰ ਕੋਈ ਨੀਵੀਂ ਜਾਤ ਵਾਲਾ ਜਾਂ ਇਸਤਰੀ ਨਹੀਂ ਲੈ ਸਕਦਾ, ਮੰਨੂੰ ਸਿਮਰਤੀ
ਦੇ ਬੋਲਾਂ ਅਨੁਸਾਰ ਜੇ ਕੋਈ ਹੋਰ ਵੇਦ ਪਾਠ ਸੁਣ ਲਵੇ ਤਾਂ ਉਸਦੇ ਕੰਨਾਂ ਵਿਚ ਸਿੱਕਾ ਢਾਲ ਕੇ ਪਾ
ਦੇਣਾ ਚਾਹੀਦਾ ਹੈ। ਜੇਕਰ ਸ਼ੂਦਰ ਵੇਦ ਮੰਤਰ ਪੜੇ ਜਾਂ ਉਸਨੂੰ ਕੋਈ ਪੜਾਵੇ ਤਾਂ ਉਹ ਵੀ ਨਰਕਾਂ ਵਿਚ
ਜਾਵੇਗਾ। ਜੇਕਰ ਵੇਦਾਂ ਦਾ ਪਾਠ ਕਰਨਾ, ਜਾਂ ਸਿਖਿਆ ਲੈਣੀ ਹੈ ਤਾਂ ਬ੍ਰਾਹਮਣ ਜਾਂ ਖਤਰੀਆਂ ਦੇ ਘਰ ਜਨਮ
ਲੈਣਾ ਜਰੂਰੀ ਹੈ।</p>

<p>ਇਸੇ ਤਰ੍ਹਾਂ ਪੈਗੰਬਰੀ ਮਤ
ਨੇ ਵੀ ਆਪਣੇ ਸਿਧਾਂਤ ਅਤੇ ਨਿਯਮਾਂ ਨੂੰ ਆਪਣੇ ਸ਼ਰਧਾਲੂਆਂ ਤਕ ਹੀ ਸੀਮਤ ਕਰਨ ਦੇ ਆਦੇਸ਼ ਦਿੱਤੇ
ਹਨ। Old testamonial, ਜੋ ਯਹੂਦੀਆਂ ਦਾ ਧਾਰਮਿਕ ਗ੍ਰੰਥ
ਹੈ, ਵਿਚ ਜਦ ਯਹੋਵਾ (ਪ੍ਰਭੂ) ਦੀ ਬਖਸ਼ਿਸ਼ ਦਾ ਵਰਣਨ ਕੀਤਾ ਜਾਂਦਾ ਹੈ ਤਾਂ ਉਹ ਮਿਹਰ ਯਹੂਦੀਆਂ ਤੱਕ
ਹੀ ਸੀਮਤ ਰੱਖੀ ਜਾਂਦੀ ਹੈ, ਅਤੇ ਕਿਹਾ ਜਾਂਦਾ ਹੈ ਕਿ ਮੈ ਸਵਰਗ ਵਿਚ ਉਨ੍ਹਾਂ ਲੋਕਾਂ ਦੀ ਹੀ ਰਖਵਾਲੀ
ਕਰਾਂਗਾ, ਜਿਹੜੇ ਯਹੂਦੀ ਬਣਨਗੇ।</p>

<p>ਈਸਾ ਜੀ ਦੀ ਬਾਣੀ ਕਹਿ
ਰਹੀ ਹੈ ਕਿ ਮੈਂ ਉਨ੍ਹਾਂ ਦਾ ਹੀ ਦੁੱਖ ਕਟਾਂਗੇ ਜੋ ਇਸਾਈ ਬਣਨਗੇ।</p>

<p>ਹਜਰਤ ਮੁਹੰਮਦ ਸਾਹਿਬ ਜੀ
ਦੀਆਂ ਸਿਫਾਰਸ਼ਾਂ ਦਾ ਪਾਤਰ ਮੁਸਲਮਾਨ ਹੀ ਬਣ ਸਕਦਾ ਹੈ। ਪਾਕ ਕੁਰਾਨ ਵਿਚ ਇਕ ਗਾਥਾ ਦਾ ਵਰਣਨ ਕੀਤਾ
ਗਿਆ ਹੈ, ਕਿ ਜਦੋਂ ਹਜਰਤ ਮੁਹੰਮਦ ਸਾਹਿਬ ਦੇ ਬਾਬਾ ਇਸ ਦੁਨੀਆਂ ਤੋਂ ਜਾਣ ਲਗੇ ਤਾਂ ਮੁਹੰਮਦ
ਸਾਹਿਬ ਉਸਦੇ ਪਾਸ ਗਏ ਅਤੇ ਆਪਣੇ ਬਾਬੇ ਦਾ ਸਿਰ ਆਪਣੀ ਗੋਦ ਵਿਚ ਰਖਿਆ ਤਾਂ ਉਨ੍ਹਾਂ ਦੀਆਂ
ਅੱਖਾਂ ਵਿਚੋਂ ਅੱਥਰੂ ਵਹਿ ਤੁਰੇ। ਆਪਣੇ ਬਾਬੇ ਦੀ ਅੰਤਮ ਇੱਛਾ ਪੁੱਛਣ ਤੇ ਜਦੋਂ ਉਨ੍ਹਾਂ ਨੇ ਕਿਹਾ
ਕਿ ਮੈਂ ਆਪਣੇ ਪਿਤਾ ਪੁਰਖੀ ਧਰਮ ਵਿੱਚ ਹੁਣ ਤੱਕ ਪਰਪੱਕ ਰਿਹਾ ਹਾਂ ਤੇ ਇਸੇ ਵਿਚ ਹੀ ਤੁਰ ਜਾਣਾ
ਚਾਹੁੰਦਾ ਹਾਂ, ਪਰ ਇਕ ਪੈਗੰਬਰ ਦੀਆਂ ਅੱਖਾਂ ਵਿਚ ਅਥਰੂ ਦੇਖ ਕੇ ਹੈਰਾਨੀ ਹੋਈ ਹੈ ਅਜਿਹਾ ਕਿਉਂ?
ਤਾਂ ਮੁਹੰਮਦ ਸਾਹਿਬ ਨੇ ਆਪਣੇ ਬਾਬੇ ਨੂੰ ਕਿਹਾ, ਕਿ ਮੈਂ ਤੁਹਾਡਾ ਰਿਣੀ ਹਾਂ, ਤੁਸੀਂ ਮੈਨੂੰ ਮੇਰੇ
ਮਾਂ ਬਾਪ ਦੇ ਤੁਰ ਜਾਣ ਤੇ ਮੈਨੂੰ ਪਾਲਿਆ ਪੋਸਿਆ ਅਤੇ ਮੇਰੀ ਹਰ ਪ੍ਰਕਾਰ ਰਖਿਆ ਕੀਤੀ, ਮੈਂ
ਇਨ੍ਹਾਂ ਅਹਿਸਾਨਾ ਦਾ ਬਦਲਾ ਅਗਲੇ ਸੰਸਾਰ ਵਿਚ ਵੀ ਨਹੀਂ ਚੁਕਾ ਸਕਾਂਗਾ, ਭਾਵੇਂ ਕਿ ਅੰਤਮ ਸਮੇਂ ਮੈਂ
ਅੱਲਾ ਦੀ ਹਜੂਰੀ ਵਿਚ ਉਨ੍ਹਾਂ ਦੇ ਪਾਸ ਹੀ ਬੈਠਾ ਹੋਵਾਂਗਾ। ਤੁਸੀਂ ਨਾ ਤਾਂ ਮੈਨੂੰ ਅੱਲਾ ਦਾ ਪੈਗੰਬਰ
ਸਵੀਕਾਰ ਕੀਤਾ ਹੈ ਅਤੇ ਨਾ ਹੀ ਉਸ ਧਰਮ ਵਿਚ ਪਰਵੇਸ਼ ਕੀਤਾ ਹੈ, ਜਿਸਦੀ ਸਿਫਾਰਸ਼ ਮੈਂ ਉਸ ਸਮੇਂ
ਅੱਲਾ ਦੇ ਪਾਸ ਕਰਨੀ ਹੈ, ਜਦੋਂ ਸਾਰਿਆਂ ਨੂੰ ਪਰਖਿਆ ਜਾਵੇਗਾ।</p>

<p>ਅਜਿਹੀ ਧਾਰਣਾ ਸ੍ਰੀ ਗੁਰੂ
ਗ੍ਰੰਥ ਸਾਹਿਬ ਵਿਚ ਨਹੀਂ ਆਈ। ਸੰਸਾਰ ਭਰ ਦੇ ਧਾਰਮਿਕ ਇਤਿਹਾਸ ਵਿਚ ਸਿਧਾਂਤਕ ਤੌਰ ਤੇ
ਸਾਂਝੀਵਾਲਤਾ ਦੇ ਮਹਾਨ ਸੰਕਲਪ ਨੂੰ ਜੇ ਕਰ ਕਿਤੇ ਅਮਲੀ ਜਾਮਾ ਪਹਿਨਾਇਆ ਗਿਆ ਹੈ ਤਾਂ ਉਹ ਕੇਵਲ
ਸ੍ਰੀ ਗੁਰੂ ਗੰ੍ਰਥ ਸਾਹਿਬ ਹੀ ਹੈ। ਜਿਥੇ ਬਾਬਾ ਸ਼ੇਖ ਫਰੀਦ ਜੀ ਦੀ ਬਾਣੀ-</p>

ਫਰੀਦਾ ਬੇ ਨਿਵਾਜਾ
ਕੁਤਿਆ 

ਏਹ ਨਾ ਭਲੀ
ਰੀਤਿ।। 

ਕਬਹੀ ਚਲ ਨ
ਆਇਆ 

ਪੰਜੇ ਵਖਤ
ਮਸੀਤਿ।। 

  

<p>ਅੰਕਤ ਕਰਕੇ ਧਾਰਮਿਕ ਗ੍ਰੰਥਾਂ ਦੇ ਇਤਿਹਾਸ ਵਿਚ ਇਨਕਲਾਬ ਲਿਆ ਦਿੱਤਾ ਹੈ। ਸਿੱਖ
ਧਰਮ ਅੰਦਰ ਭਾਵੇਂ ਮਸੀਤ ਜਾਣ ਦਾ ਕੋਈ ਆਦੇਸ਼ ਨਹੀਂ, ਪਰ ਸਤਿਗੁਰਾਂ ਦੀ ਪਾਵਨ ਬਾਣੀ ਸਮੁੱਚੀ
ਮਨੁੱਖਤਾ ਲਈ ਹੈ, ਇਸ ਲਈ ਇਸ ਵਿਚ ਵਰਗਾਂ, ਜਾਤਾਂ, ਵੱਖ-2 ਧਰਮਾਂ ਦਾ ਵਰਣਨ ਕਰਦੇ ਹੋਏ
ਸਤਿਗੁਰੂ ਮਜ਼ਬੀ ਵੱਟਾਂ ਨੂੰ ਢਾਹ ਕੇ ਇਨਸਾਨੀਅਤ ਦੇ ਮਹਾਨ ਸੰਕਲਪ ਨੂੰ ਮੂਰਤੀਮਾਨ ਕਰਦੇ ਹੋਏ
ਆਖਦੇ ਹਨ ਕਿ, ਹੇ ਮੁਸਲਮਾਨ, ਮੁਸਲਮਾਨ ਬਣਨਾ ਬਹੁਤ ਕਠਨ ਹੈ, ਕਿਉਂਕਿ ਜਦ ਤਕ ਤੇਰੇ ਬਾਹਰਲੇ
ਕਰਮ ਇਨਸਾਨੀਅਤ ਦਾ ਰੂਪ ਧਾਰਨ ਨਹੀਂ ਕਰਦੇ, ਭਾਵ ਤੇਰਾ ਰੋਜਾ ਤੇ ਨਮਾਜ, ਸਬਰ-ਸੰਤੋਖ, ਧੀਰਜ ਤੇ
ਭਗਤਰੀ ਭਾਵ ਦਾ ਰੂਪ ਧਾਰਨ ਨਹੀਂ ਕਰ ਸਕੇ, ਤਾਂ ਇਹ ਸਭ ਬੇਅਰਥ ਹਨ। ਜੇ ਕਰ ਤੂੰ ਆਪਣੇ ਆਪ ਨੂੰ
ਪੱਕਾ ਨਮਾਜ਼ੀ ਸਿਧ ਵੀ ਕਰ ਲਵੇਂ ਪਰ ਤੇਰੇ ਕਰਮ ਅੱਲਾ ਦੇ ਰਾਹ ਤੇ ਨਾ ਜਾਣ ਵਾਲੇ ਹੋਣ ਤਾਂ ਤੂੰ
ਮੁਸਲਮਾਨ ਕਹਾਵਣ ਦਾ ਹੱਕਦਾਰ ਨਹੀਂ, ਗੁਰੂ ਨਾਨਕ ਪਾਤਸਾਹ ਅਸਲੀ ਮੁਸਲਮਾਨ ਦੀ ਤਸਵੀਰ ਪੇਸ਼ ਕਰਦਿਆਂ
ਆਖਦੇ ਹਨ ਕਿ-</p>

<p> </p>

ਮੁਸਲਮਾਨ ਕਹਾਵਣ
ਮੁਸਕਲ 

ਜਾ ਹੋਇ ਤਾਂ
ਮੁਸਲਮਾਨ ਕਹਾਵੈ।। 

ਅਵਲਿ ਅਉਲਿ ਦੀਨ
ਕਰ ਮਿਠਾ 

ਮਸਕਲ ਮਾਨਾ ਮਾਲੁ
ਮੁਸਾਵੈ।। 

ਹੋਇ ਮੁਸਲਿਮ ਦੀਨ
ਮੁਰਾਣੈ 

ਮਰਣ ਜੀਵਣ ਕਾ ਭਰਮ
ਚੁਕਾਵੈ।। 

ਰਬੁ ਕੀ ਰਜਾਇ
ਮੰਨੈ ਸਿਰ ਉਪਰ 

ਕਰਤਾ ਮੰਨੇ ਆਪ
ਗਵਾਵੈ।। 

ਤਉ ਨਾਨਕ ਸਰਬ
ਜੀਆ 

ਮਿਹਰੰਮਤ ਹੋਇ ਤਾ
ਮੁਸਲਮਾਨ ਕਹਾਵੈ।। 

(ਮਾਝ ਕੀ ਵਾਰ
ਸਲੋਕ ਮ.1, ਪੰਨਾ 141) 

<p> </p>

<p>ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਅੰਦਰ ਕੇਵਲ ਸਿਖ ਦੀ ਹੀ ਤਰੀਫ ਨਹੀਂ ਕੀਤੀ ਹੋਈ ਕਿ-ਜਨ
ਨਾਨਕ ਧੂੜ ਮੰਗੈ ਤਿਸ ਗੁਰਸਿਖ ਕੀ ਜੋ ਆਪ ਜਪੈ ਅਵਰਾ ਨਾਮ ਜਪਾਵੈ।। ਜਾ ਗੁਰਸਿਖਾ ਕੀ ਹਰ ਧੂੜਿ
ਦੇਹਿ ਹਮ ਪਾਪੀ ਭੀ ਗਤਿ ਪਾਹਿ।।</p>

<p>ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਅੰਦਰ ਤਾਂ ਉਸ ਬ੍ਰਾਹਮਣ ਨੂੰ ਪੂਜਣ ਜੋਗ ਆਖਿਆ ਹੈ ਜਿਹੜਾ
ਬ੍ਰਹਮ ਨੂੰ ਜਾਣਦਾ ਹੋਇਆਂ ਬ੍ਰਹਮ ਕਰਮ ਕਰਦਾ ਹੈ ਅਤੇ ਆਪਣੇ ਆਪ ਨੂੰ ਬ੍ਰਹਮ ਦੀ ਰਜਾ ਅਨੁਸਾਰ
ਚਲਾਂਦਾ ਹੈ, ਸਦਾਚਾਰਕ ਜੀਵਨ ਦਾ ਧਾਰਨੀ ਹੈ ਅਤੇ ਫੋਕਟ ਕਰਮ ਨਹੀਂ ਕਰਦਾ-</p>

ਬ੍ਰਹਮਣ ਸੋ ਜੁ
ਬਿੰਦੇ ਬ੍ਰਹਮ 

ਜਪੁ ਤਪੁ ਸੰਜਮ
ਕਮਾਵੈ ਕਰਮ।। 

ਸੀਲ ਸੰਤੋਖ ਕਾ
ਰਖੇ ਧਰਮ 

ਬੰਧਨ ਤੋੜੇ ਹੋਵੈ
ਮੁਕਤਿ।। 

ਸੋਈ ਬ੍ਰਹਮਣ ਪੂਜਣ
ਜੁਗਤਿ।। 

(ਪੰਨਾ 1411) 

 


ਸੁਖਮਨੀ ਸਾਹਿਬ
ਅੰਦਰ ਸ੍ਰੀ ਗੁਰੂ ਅਰਜਨ ਦੇਵ ਜੀ ਪੜੇ ਲਿਖੇ ਪੰਡਤ ਬਾਰੇ ਆਖਦੇ ਹਨ ਕਿ- 

 


ਪੰਡਿਤ ਸੋ ਜੋ ਮਨ
ਪ੍ਰਬੋਧੈ 

ਰਾਮ ਨਾਮ ਆਤਮ
ਮਹਿ ਸੋਧੈ।। 

ਰਾਮ ਨਾਮ ਸਾਰ ਰਸ
ਪੀਵੈ 

ਉਸ ਪੰਡਿਤ ਕੈ
ਉਪਦੇਸ ਜਗੁ ਜੀਵੈ।। 

ਅਤੇ 

ਖਤਰੀ ਸੋ ਜੁ ਕਰਮਾ
ਕਾ ਸੂਰ ਆਖਿਆ ਹੈ। 

ਪ੍ਰਥਾਇ ਸਾਖੀ
ਮਹਾਪੁਰਖ ਬੋਲਦੇ ਸਾਂਝੀ ਸਗਲ ਜਹਾਨੈ।। ਅਨੁਸਾਰ ਗੁਰੂ ਨਾਨਕ ਪਾਤਸ਼ਾਹ ਵਲੋਂ ਕੀਤੇ ਹੋਏ ਜਨੇਊ ਦੇ
ਸਹੀ ਅਰਥ ਤਾਂ ਸਾਨੂੰ ਕਕਾਰਧਾਰੀ ਖਾਲਸੇ ਨੂੰ ਵੀ ਹਲੂਣ ਕੇ ਰੱਖ ਦਿੰਦੇ ਹਨ, ਗੁਰੂ ਸਾਹਿਬ ਦੇ ਬੋਲ
ਹਨ- 

 


ਦਇਆ ਕਪਾਹ
ਸੰਤੋਖ ਸੂਤ ਜਤ ਗੰਢੀ ਸਤ ਵਟ।। 

ਏਹੁ ਜਨੇਊ ਜੀਅ
ਕਾ ਹਈ ਤਾ ਪਾਂਡੇ ਘਤੁ।। 

ਨਾ ਇਹੁ ਤੁਟੈ ਨਾ
ਮਲ ਲਗੇ ਨਾ ਇਹੁ ਜਲੈ ਨਾ ਜਾਇ।। 

ਧੰਨ ਸੋ ਮਾਣਸ
ਨਾਨਕਾ ਜੋ ਗਲਿ ਚਲੈ ਪਾਇ।। (ਆਸਾ ਦੀ ਵਾਰ) 

 


ਜੋਗੀ ਨੂੰ ਜੋਗ ਦੀ
ਜੁਗਤਿ ਸਮਝਾਦਿਆਂ, ਸਗਲ ਜਮਾਤੀ ਹੋਣ ਦਾ ਉਪਦੇਸ਼ ਕਰਦੇ ਹਨ- 

 


ਮੁੰਦਾ ਸੰਤੋਖ ਸਰਮ
ਪਤਿ ਝੋਲੀ ਧਿਆਨ ਕੀ ਕਰਹਿ ਬਿਭੂਤ।। 

ਖਿੰਥਾ ਕਾਲ
ਕੁਆਰੀ ਕਾਇਆ ਜੁਗਤਿ ਡੰਡਾ ਪ੍ਰਤੀਤ।। 

ਆਈ ਪੰਥੀ ਸਗਲ
ਜਮਾਤੀ ਮਨ ਜੀਤੇ ਜਗ ਜੀਤ।। (ਜਪੁਜੀ ਸਾਹਿਬ) 

 


ਪੁਲਾੜ ਦੇ ਖੋਜ
ਵਿਗਿਆਨੀ ਦਾ ਭਰਮ ਦੂਰ ਕਰਕੇ, ਮਾਰਗ ਦਰਸਨ ਕਰਦੇ ਜਪੁਜੀ ਸਾਹਿਬ ਅੰਦਰ ਆਖਦੇ ਹਨ ਕਿ- 

 


ਕੇਤੇ ਇੰਦ ਚੰਦ
ਸੂਰ ਕੇਤੇ ਮੰਡਲ ਦੇਸ।। 

 


ਅਤੇ ਧਰਤੀ ਹੇਠਲੇ
ਬਲਦ ਦੀ ਪੁਰਾਤਨ ਮਨੌਤ ਨੂੰ ਢਾਂਹੁੰਦੇ ਹੋਏ ਆਖਦੇ ਹਨ ਕਿ, 

 


ਧਰਤੀ ਹੋਰ ਪਰੇ
ਹੋਰ ਹੋਰ 

ਤਿਸਤੇ ਭਾਰ ਤਲੇ
ਕਵਣ ਜੋਰ।। 

<p> </p>

<p>ਇਸ ਤਰ੍ਹਾਂ ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਅੰਦਰ ਸਮਾਜ ਦੇ ਹੋਰ ਵੱਖ ਵੱਖ ਕਿਤੇ ਵਾਲਿਆਂ
ਕਿਸਾਨਾਂ, ਵਪਾਰੀਆਂ, ਸੰਨਿਆਸੀਆਂ, ਗ੍ਰਿਹਸਥੀਆਂ, ਨੌਕਰਾਂ, ਚਾਕਰਾਂ, ਦੁਕਾਨਦਾਰਾਂ, ਡਾਕਟਰਾਂ,
ਅਧਿਆਪਕਾਂ ਗੱਲ ਕੀ ਹਰ ਇਕ ਮਨੁੱਖ ਨੂੰ ਭਾਵੇਂ ਉਹ ਗਰੀਬ-ਅਮੀਰ, ਰਾਜਾ, ਪਰਜਾ, ਸਾਧ ਜਾਂ ਸੰਤ ਹੈ
ਨੂੰ ਉਸਦੇ ਮੁਤਾਬਕ ਇਕੋ ਜਿਹਾ ਉਪਦੇਸ ਦੇ ਕੇ ਮਾਨਸ ਕੀ ਜਾਤ ਨੂੰ ਇਕ ਪਹਿਚਾਨਣ ਦੀ ਸਮਰਥਾ
ਬਖਸਿਸ਼ ਕੀਤੀ ਗਈ।</p>

<p>ਸਾਨੂੰ ਦੁਨੀਆਂ ਦੇ ਕਿਸੇ ਵੀ ਧਾਰਮਿਕ ਗੰ੍ਰਥ ਵਿਚੋਂ ਅਜਿਹੇ ਸਰਬ ਸਾਂਝੇ ਪਾਵਨ ਬੋਲ
ਨਹੀਂ ਮਿਲਦੇ-</p>

<p> </p>

ਅਵਲਿ ਅਲਹ ਨੂਰੁ
ਉਪਾਇਆ 

ਕੁਦਰਤਿ ਕੇ ਸਭ
ਬੰਦੇ।। 

ਏਕ ਨੂਰ ਤੇ ਸਭ
ਜਗ ਉਪਜਿਆ 

ਕਉਣ ਭਲੇ ਕੋ
ਮੰਦੇ।। 

(ਭਗਤ ਕਬੀਰ ਜੀ,
ਪੰਨਾ ???) 

ਅਤੇ 

ਖਤ੍ਰੀ ਬ੍ਰਹਮਣ ਸੂਦ
ਵੈਸ 

ਉਪਦੇਸ ਚਹੁੰ
ਵਰਨਾ ਕਉ ਸਾਂਝਾ।। 

<p> </p>

<p>ਐਸਾ ਸਰਬ ਸਾਂਝਾ ਉਪਦੇਸ ਬਖਸ਼ਿਸ਼ ਕਰਦਿਆਂ, ਗੁਰੂ ਸਾਹਿਬ ਨੇ ਕਿਸੇ ਮਨੁੱਖ ਨੂੰ
ਤਰਲਾ, ਮਿੰਨਤ, ਲਾਲਚ ਜਾਂ ਸਰਾਪ ਨਹੀਂ ਦਿੱਤਾ, ਸਗੋਂ ਤਰਸ ਵਿਚ ਹੋ ਕੇ ਪ੍ਰੇਰਨਾ ਕਰਦੇ ਹਨ ਕਿ-</p>

<p>ਪ੍ਰਾਣੀ ਤੂੰ ਆਇਓ ਲਾਹਾ ਲੈਣ ਲਗਾ ਕਿਤ ਕੁਫਕੜੇ ਸਭ ਮੁਕਦੀ ਚਲੀ ਰੈਣ ।।</p>

<p>ਸਾਡੀ ਸਾਰੇ ਨਾਨਕ ਨਾਮ ਲੇਵਾ ਗੁਰਸਿਖਾਂ ਦੀ ਜਿੰਮੇਵਾਰੀ ਬਣਦੀ ਹੈ ਕਿ ਅਸੀਂ ਚਾਰ
ਸੌ ਸਾਲਾ ਪ੍ਰਕਾਸ਼ ਦਿਵਸ ਨੂੰ ਹੀ ਮੁਖ ਰਖਕੇ ਸ੍ਰੀ ਗੁਰੂ ਗ੍ਰੰਥ ਸਾਹਿਬ ਜੀ ਦੀ ਸਰਬੱਤ ਦੇ ਭਲੇ ਵਾਲੀ
ਸਰਬ ਸਾਂਝੀ ਵਿਚਾਰਧਾਰਾ ਨੂੰ ਸਮੁੱਚੀ ਮਨੁੱਖਤਾ ਤੱਕ ਪਹੁੰਚਾਉਣ ਦਾ ਉਪਰਾਲਾ ਕਰੀਏ।</p>

<p> </p>






</body></text></cesDoc>