<cesDoc id="pun-w-sanjh-news-00-10-18" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-00-10-18.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 00-10-18</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>00-10-18</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਬੀਬੀ ਵਲੋਂ
ਸੀ.ਬੀ.ਆਈ. ਨਾਲ ਲੁਕਣਮੀਚੀ: ਆਪਣੇ ਡੇਰੇ ਦੇ ਸਮਾਗਮ ਵਿੱਚ ਨਾ ਪਹੁੰਚੀ </p>

<p>ਜਲੰਧਰ, 17 ਅਕਤੂਬਰ- ਕੇਂਦਰੀ ਜਾਂਚ
ਬਿਊਰੋ ਨੇ ਚਾਹੇ ਸ਼੍ਰੋਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਪ੍ਰਧਾਨ ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਨੰੂ ਹਾਲੇ
ਹਰਪ੍ਰੀਤ ਕਤਲ ਕੇਸ ਵਿੱਚ ਫੌਰੀ ਤੌਰ ਤੇ ਗ੍ਰਿਫਤਾਰ ਕਰਨ ਦਾ ਕੋਈ ਸੰਕੇਤ ਨਹੀਂ ਦਿੱਤਾ ਪਰ ਬੀਬੀ ਜਗੀਰ
ਕੌਰ ਕੇਂਦਰੀ ਜਾਂਚ ਬਿਊਰੋ ਨੰੂ ਅਜਿਹਾ ਕੋਈ ਮੌਕਾ ਦੇਣ ਦੇ ਪੱਖ ਵਿੱਚ ਵੀ ਨਹੀਂ ਜਾਪਦੇ । ਇਸੇ
ਕਾਰਨ ਬੀਬੀ ਕੱਲ੍ਹ ਕਪੂਰਥਲਾ ਜ਼ਿਲੇ ਵਿੱਚ ਬੇਗੋਵਾਲ ਸਥਿਤ ਆਪਣੇ ਸਹੁਰੇ ਸੰਤ ਪ੍ਰੇਮ ਸਿੰਘ
ਮੁਰਾਲੇਵਾਲਿਆਂ ਦੇ ਜਨਮ ਦਿਹਾੜੇ ਕਰਵਾਏ ਗਏ ਸਮਾਰੋਹ ਵਿੱਚ ਵੀ ਸ਼ਾਮਲ ਨਹੀਂ ਹੋਏ । </p>

<p>ਬੀਬੀ ਉਦੋਂ ਤੋਂ ਹੀ ਪੱਤਰਕਾਰਾਂ ਅਤੇ
ਆਮ ਜਨਤਾ ਨੰੂ ਮਿਲਣ ਤੋਂ ਕਤਰਾ ਰਹੇ ਹਨ ਜਦੋਂ ਤੋਂ ਉਨ੍ਹਾਂ ਖਿਲਾਫ ਉਨ੍ਹਾਂ ਦੀ ਬੇਟੀ ਹਰਪ੍ਰੀਤ ਕੌਰ
ਦੇ ਕਥਿਤ ਕਤਲ ਕੇਸ ਵਿੱਚ ਉਨ੍ਹਾਂ ਵਿਰੁੱਧ ਕੇਂਦਰੀ ਜਾਂਚ ਬਿਊਰੋ ਨੇ ਐਫ.ਆਈ.ਆਰ. ਦਰਜ ਕੀਤੀ
ਹੈ । ਸ਼ੋ੍ਰਮਣੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੀ ਪ੍ਰਧਾਨ ਦੇ ਹਮਾਇਤੀਆਂ ਨੇ ਡੇਰਾ ਬੇਗੋਵਾਲ ਵਿੱਚ
ਸੰਤ ਪ੍ਰੇਮ ਸਿੰਘ ਮੁਰਾਲੇਵਾਲੀਆਂ ਦੀ ਯਾਦ ਵਿੱਚ ਕਰਵਾਏ ਗਏ ਧਾਰਮਿਕ ਸਮਾਗਮ ਵਿੱਚ ਆਖਿਆ ਕਿ
ਬੀਬੀ ਸਿਹਤ ਖਰਾਬ ਹੋਣ ਕਰਨ ਇਸ ਸਮਾਰੋਹ ਵਿੱਚ ਸ਼ਮੂਲੀਅਤ ਨਹੀਂ ਕਰ ਸਕੇ । </p>

<p>ਸਾਬਕਾ ਵਿਧਾਇਕ
ਮਨਜੀਤ ਸਿੰਘ ਭੱਠਲ ਵੱਲੋਂ ਗੋਲੀ ਮਾਰ ਕੇ ਖੁਦਕੁਸ਼ੀ </p>

<p>ਬਰਨਾਲਾ/ਲੁਧਿਆਣਾ, 17 ਅਕਤੂਬਰ-ਹਲਕਾ
ਧਨੌਲਾ ਦੇ ਸਾਬਕਾ ਵਿਧਾਇਕ ਅਤੇ ਪੰਜਾਬ ਪ੍ਰਦੇਸ਼ ਕਾਂਗਰਸ ਦੇ ਮੈਂਬਰ ਮਨਜੀਤ ਸਿੰਘ ਭੱਠਲ ਨੇ ਅੱਜ
ਆਪਣੇ ਸਿਰ ਵਿੱਚ ਗੋਲੀ ਮਾਰ ਕੇ ਖੁਦਕੁਸ਼ੀ ਕਰ ਲਈ । ਉਹ 64 ਵਰ੍ਹਿਆਂ ਦੇ ਸਨ ਅਤੇ ਰਾਜਿੰਦਰ ਕੌਰ
ਭੱਠਲ ਦੇ ਵੱਡੇ ਭਰਾ ਸਨ । ਉਨ੍ਹਾਂ ਨੇ ਅੱਜ ਸਵੇਰੇ ਕਰੀਬ 8 ਵਜੇ ਆਪਣੇ ਜੱਦੀ ਪਿੰਡ ਭੱਠਲਾਂ ਵਿਖੇ
ਆਪਣੇ ਰਿਵਾਲਵਰ ਨਾਲ ਆਪਣੇ ਸਿਰ ਵਿੱਚ ਗੋਲੀ ਮਾਰ ਲਈ । ਇਸ ਪਿੱਛੋਂ ਉਨ੍ਹਾਂ ਨੰੂ ਕਸਬਾ ਧਨੌਲਾ
ਦੇ ਸਿਵਲ ਹਸਪਤਾਲ ਲਈ ਲਿਆਦਾ ਗਿਆ । ਹਾਲਤ ਗੰਭੀਰ ਹੋਣ ਕਰਕੇਟ ਉਨ੍ਹਾਂ ਨੰੂ ਮੁੱਢਲੀ ਡਾਕਟਰੀ
ਸਹਾਇਤਾ ਦੇ ਕੇ ਲੁਧਿਆਣਾ ਭੇਜ ਦਿੱਤਾ ਗਿਆ । ਡੀ.ਐਮ.ਸੀ. ਦੇ ਡਾਕਟਰਾਂ ਨੇ ਉਨ੍ਹਾਂ ਨੰੂ
ਬਚਾਉਣ ਦੀ ਪੂਰੀ ਕੋਸ਼ਿਸ਼ ਕੀਤੀ ਪਰ ਕਰੀਬ ਡੇਢ ਵਜੇ ਉਹ ਦਮ ਤੋੜ ਗਏ । </p>

<p>ਸੂਤਰਾਂ ਨੇ ਦੱਸਿਆ ਕਿ ਗੋਲੀ ਉਨ੍ਹਾਂ ਦੇ
ਸਿਰ ਦੇ ਆਰ-ਪਾਰ ਲੰਘ ਹਿੋਈ ਸੀ । ਮਨਜੀਤ ਸਿੰਘ ਭੱਠਲ ਜਿਹੜੇ ਸੁਤੰਤਰਤਾ ਸੰਗਰਾਮੀ ਬਾਬਾ ਹੀਰਾ
ਸਿੰਘ ਭੱਠਲ ਤੇ ਸੁਤੰਤਰਤਾ ਸੰਗਰਾਮਾਂ ਮਾਤਾ ਹਰਨਾਮ ਕੌਰ ਦੇ ਪੁੱਤਰ ਸਨ । ਉਥੇ ਕਾਂਗਰਸ ਪਾਰਟੀ ਦੀ
ਸੀਨੀਅਰ ਆਗੂ ਅਤੇ ਪੰਜਾਬ ਦੀ ਸਾਬਕਾ ਮੁੱਖ ਮੰਤਰੀ ਬੀਬੀ ਰਾਜਿੰਦਰ ਕੌਰ ਭੱਠਲ ਦੇ ਵੱਡੇ ਭਾਈ ਸਨ
। ਉਹ ਫਰਵਰੀ 1997 ਦੀਆਂ ਵਿਧਾਨ ਸਭਾ ਦੀਆਂ ਚੋਣਾਂ ਵਿੱਚ ਹੁਣ ਸਿੰਚਾਈ ਰਾਜ ਮੰਤਰੀ ਗੋਬਿੰਦ
ਸਿੰਘ ਲੋਗੋਵਾਲ ਤੋਂ 3082 ਵੋਟਾਂ ਦੇ ਫਰਕ ਨਾਲ ਹਾਰ ਗਏ ਸਨ ਪਰ 1992 ਦੀਆਂ ਚੋਣਾਂ ਵਿੱਚ ਹਲਕਾ
ਧਨੌਲਾ ਤੋਂ ਵਿਧਾਇਕ ਚੁਣੇ ਗਏ ਸਨ । ਉਨ੍ਹਾਂ ਨੇ ਗੋਬਿੰਦ ਸਿੰਘ ਲੋਗੋਵਾਲ ਜਿਹੜੇ ਉਸ ਸਮੇਂ ਦੇ
ਅਕਾਲੀ ਦਲ (ਕਾਬਲ ਸਿੰਘ) ਦੇ ਉਮੀਦਵਾਰ ਸਨ ਨੰੂ ਹਰਾਇਆ ਸੀ । </p>

<p>ਬੂਟਾ
ਸਿੰਘ ਵਲੋਂ ਅਪੀਲ ਦਾਇਰ </p>

<p>ਨਵੀਂ ਦਿੱਲੀ, 17 ਅਕਤੂਬਰ-ਸਾਬਕਾ
ਕੇਂਦਰੀ ਮੰਤਰੀ ਬੂਟਾ ਸਿੰਘ ਜਿੰਨਾਂ ਨੰੂ ਝਾਰਖੰਡ ਮੁਕਤੀ ਮੋਰਚਾ ਰਿਸ਼ਵਤ ਕੇਸ ਵਿੱਚ ਤਿੰਨ ਸਾਲ ਦੀ
ਬਾਮੁਸ਼ੱਕਤ ਕੈਦ ਦੀ ਸਜ਼ਾ ਸੁਣਵਾਈ ਗਈ ਸੀ ਨੇ ਇਸ ਫੈਸਲੇ ਖਿਲਾਫ ਦਿੱਲੀ ਹਾਈਕੋਰਟ ਵਿੱਚ ਅਪੀਲ
ਦਇਰ ਕੀਤੀ ਹੈ । ਇਸ ਕੇਸ ਵਿੱਚ ਉਨ੍ਹਾਂ ਦੇ ਨਾਲ ਸਾਬਕਾ ਪ੍ਰਧਾਨ ਮੰਤਰੀ ਸ੍ਰੀ ਪੀ.ਵੀ.ਨਰਸਿਮਹਾ
ਰਾਓ ਨੰੂ ਵੀ ਇਹੀ ਸਜ਼ਾ ਸੁਣਾਈ ਗਈ ਸੀ । </p>

<p>ਸ੍ਰੀ ਬੂਟਾ ਸਿੰਘ ਨੇ ਦਾਇਰ ਕੀਤੀ ਅਪੀਲ
ਵਿੱਚ ਦਾਅਵਾ ਕੀਤਾ ਹੈ ਕਿ ਵਿਸ਼ੇਸ਼ ਜੱਜ ਨੇ ਉਨ੍ਹਾਂ ਨੰੂ ਇਸ ਰਿਸ਼ਵਤ ਕੇਸ ਵਿੱਚ ਗਲਤ ਸਜ਼ਾ ਦਿੱਤੀ
ਹੈ ਜਦਕਿ ਉਨ੍ਹਾਂ ਖਿਲਾਫ ਰਿਸ਼ਵਤ ਦੇਣ ਸਬੰਧੀ ਕੋਈ ਸਬੂਤ ਨਹੀਂ ਮਿਲਿਆਂ ਹੈ । </p>

<p>ਕੇਂਦਰੀ ਪੈਕੇਜ
ਵਿਰੁੱਧ ਰੋਹ ਪ੍ਰਗਟਾਉਣ ਵਾਲੇ ਕਾਂਗਰਸੀ ਵਰਕਰਾਂ ਤੇ ਲਾਠੀਚਾਰਜ </p>

<p>ਚੰਡੀਗੜ੍ਹ, 17 ਅਕਤੂਬਰ-ਕੇਂਦਰ ਸਰਕਾਰ
ਵਲੋਂ ਪੰਜਾਬ ਦੇ ਕਿਸਾਨਾਂ ਲਈ ਐਲਾਨੇ ਧੋਖੇ ਭਰੇ ਪੈਕੇਜ ਵਿਰੁੱਧ ਰੋਸ ਪ੍ਰਗਟ ਕਰਨ ਲਈ ਪੰਜਾਬ ਦੇ
ਮੁੱਖ ਮੰਤਰੀ ਪ੍ਰਕਾਸ਼ ਸਿੰਘ ਬਾਦਲ ਦੀ ਕੋਠੀ ਦਾ ਘਿਰਾਓ ਕਰਨ ਜਾ ਰਹੇ ਕਾਂਗਰਸ ਵਰਕਰਾਂ, ਜਿਹੜੇ ਕਥਿਤ
ਤੌਰ ਉਤੇ ਪੁਲਿਸ ਤੇ ਪਥਰਾਓ ਕਰ ਰਹੇ ਸਨ, ਤੇ ਪੁਲਿਸ ਨੇ ਅੱਜ ਇਥੇ ਮਟਕਾ ਚੌਕ ਵਿਖੇ ਪਾਣੀ ਦੀਆਂ
ਤੇਜ਼ ਬੁਛਾੜਾਂ ਮਾਰ ਕੇ ਅੱਥਰੂ ਗੈਸ ਛੱਡ ਕੇ ਅਤੇ ਲਾਠੀਚਾਰਜ ਕਰਕੇ 25 ਵਿਅਕਤੀਆਂ ਨੰੂ ਜ਼ਖਮੀ ਕਰ
ਦਿੱਤਾ ਅਤੇ 600 ਤੋਂ ਵੱਧ ਨੂੰ ਗ੍ਰਿਫਤਾਰ ਕਰ ਲਿਆ । ਜ਼ਖਮੀਆਂ ਵਿਚ ਤਿੰਨ ਪੁਲਿਸ ਮੁਲਾਜ਼ਮ ਵੀ
ਦੱਸੇ ਜਾ ਰਹੇ ਹਨ । </p>

<p>ਕਾਂਗਰਸ ਵਿਧਾਇਕ ਪਾਰਟੀ ਦੇ ਆਗੂ ਚੌਧਰੀ
ਜਗਜੀਤ ਸਿੰਘ ਨੇ ਦੱਸਿਆ ਕਿ ਜਦੋਂ ਕਾਂਗਰਸ ਵਰਕਰ ਪ੍ਰਦੇਸ਼ ਕਾਂਗਰਸ ਦੇ ਪ੍ਰਧਾਨ ਕੈਪਟਨ ਅਮਰਿੰਦਰ ਸਿੰਘ
ਦੀ ਅਗਵਾਈ ਹੇਠ ਪੁਲਿਸ ਦੇ ਬੈਰੀਕੇਡ ਤੋੜ ਕੇ ਅੱਗੇ ਵੱਧ ਰਹੇ ਸਨ ਤਾਂ ਪੁਲਿਸ ਨੇ ਕਾਰਵਾਈ ਕਰਕੇ
200 ਤੋਂ ਵੱਧ ਕਾਂਗਰਸ ਵਰਕਰਾਂ ਨੂੰ ਜ਼ਖਮੀ ਕਰ ਦਿੱਤਾ । 666 ਕਾਂਗਰਸ ਵਰਕਰ ਨੇ ਗ੍ਰਿਫਤਾਰੀਆਂ
ਦਿੱਤੀਆਂ । ਪੁਲਿਸ ਨੇ ਜ਼ਖਮੀਆਂ ਦੀ ਗਿਣਤੀ ਕੇਵਲ 25 ਦੱਸੀ ਜਿਨ੍ਹਾਂ ਵਿਚ ਤਿੰਨ ਪੁਲਿਸ ਮੁਲਾਜ਼ਮ ਵੀ
ਸ਼ਾਮਲ ਹਨ । ਮੁੱਖ ਮੰਤਰੀ ਦੀ ਕੋਠੀ ਦੇ ਘੇਰਾਵ ਲਈ ਨਿਕਲਣ ਤੋਂ ਪਹਿਲਾਂ ਕਾਂਗਰਸ ਵਰਕਰਾਂ ਨੇ ਧਰਨਾ
ਦਿੱਤਾ । </p>

<p> </p>






</body></text></cesDoc>