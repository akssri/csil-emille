<cesDoc id="pun-w-sanjh-news-00-12-15" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-sanjh-news-00-12-15.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Sanjh Savera" internet news (www.sanjhsavera.com), news stories collected on 00-12-15</h.title>
<h.author>Sanjh Savera</h.author>
<imprint>
<pubPlace>Ontario, Canada</pubPlace>
<publisher>Sanjh Savera</publisher>
<pubDate>00-12-15</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-23</date>
</creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ਭਾਈ ਰਣਜੀਤ ਸਿੰਘ ਨੰੂ ਮੁੜ ਜਥੇਦਾਰ ਸਥਾਪਤ ਕਰਨ ਦੀ ਮੰਗ ਉਭਰੀ </p>

<p>ਸ਼੍ਰੋਮਣੀ ਗੁਰੂਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਵਲੋਂ ਪਹਿਲੇ ਇਤਿਹਾਸਕ
ਫੈਸਲੇ ਵਿੱਚ ਬੀਬੀ ਦੀ ਧਰੀ ਇੱਟ ਰਹਿਣ ਨਹੀਂ ਦਿੱਤੀ ਅਤੇ ਬੀਬੀ ਅਤੇ ਉਸਦੇ ਚਹੇਤਿਆਂ ਵਲੋਂ
ਪਾਕਿਸਤਾਨ ਗੁਰੁਧਾਮਾਂ ਦੇ ਦਰਸ਼ਨ ਕਰਨ ਲਈ ਭੇਜੇ ਜਾਂਦੇ ਜਥਿਆਂ ਉਪਰ ਲਾਈ ਰੋਕ ਨੰੂ ਜਥੇਦਾਰ
ਤਲਵੰਡੀ ਨੇ ਪ੍ਰਧਾਨ ਬਨਣ ਸਾਰ ਹੀ ਉਲਟਾ ਦਿੱਤਾ ਹੈ। ਇਸੇ ਹੀ ਰੌਸ਼ਨੀ ਵਿੱਚ ਸਿੱਖ ਸਟੂਡੈਂਟਸ
ਫੈਡਰੇਸ਼ਨ ਦੇ ਪ੍ਰਧਾਨ ਸ੍ਰ: ਹਰਮਿੰਦਰ ਸਿੰਘ ਗਿੱਲ ਨੇ ਮੰਗ ਕੀਤੀ ਹੈ ਭਾਈ ਰਣਜੀਤ ਸਿੰਘ ਨੰੂ ਮੁੜ
ਤੋਂ ਜਥੇਦਾਰ ਬਣਾਇਆ ਜਾਣਾ ਚਾਹੀਦਾ ਹੈ। ਫੈਡਰੇਸ਼ਨ ਆਗੂ ਨੇ ਕਿਹਾ ਕਿ ਬੀਬੀ ਦੇ ਰਾਜ ਕਾਲ
ਦੌਰਾਨ ਸਿੱੰਘ ਸਾਹਿਬ ਭਾਈ ਰਣਜੀਤ ਸਿੰਘ ਨੰੂ ਜਥੇਦਾਰੀ ਤੋਂ ਲਾਹੁਣਾ ਗਲਤ ਸੀ। ਉਨ੍ਹਾਂ ਕਿਹਾ ਕਿ
ਫੈਡਰੇਸ਼ਨ ਉਨ੍ਹਾਂ ਦੀ ਬਹਾਲੀ ਲਈ ਸੰਘਰਸ਼ ਜਾਰੀ ਰੱਖੇਗੀ। ਫੈਡਰੇਸ਼ਨ ਆਗੂ ਨੇ ਕਿਹਾ ਕਿ ਫੈਡਰੇਸ਼ਨ
21 ਦਸੰਬਰ ਨੰੂ ਜਲੰਧਰ ਵਿੱਚ, ਜੇਲ੍ਹਾਂ ਵਿੱਚ ਬੰਦ ਸਿੱਖ ਨੌਜੁਆਨਾਂ ਦੀ ਰਿਹਾਈ ਲਈ ਮੁਜ਼ਾਹਰਾ
ਕਰਨਗੇ। </p>



<p>          ਦਿੱਲੀ ਦੇ ਅਕਾਲੀ
ਆਗੂ ਸ੍ਰ: ਪਰਮਜੀਤ ਸਿੰਘ ਸਰਨਾ ਨੇ ਸ਼੍ਰੋਮਣੀ ਕਮੇਟੀ ਵਲੋਂ ਪਾਕਿਸਤਾਨ ਜਥੇ ਭੇਜਣ ਦੇ ਲਏ ਫੈਸਲੇ
ਦੀ ਸ਼ਲਾਘਾ ਕਰਦਿਆਂ ਕਿਹਾ ਕਿ ਬੀਬੀ ਜਗੀਰ ਕੌਰ ਨੇ ਇਹ ਫੈਸਲਾ ਕਰਕੇ ਸਿੱਖ ਕੌਮ ਦਾ ਬਹੁਤ ਨੁਕਸਾਨ
ਕੀਤਾ ਹੈ। ਪ੍ਰਮਜੀਤ ਸਿੰਘ ਸਰਨਾ ਪਾਕਿਸਤਾਨ ਗੁਰਪੁਰਬ ਦੇ ਮੌਕੇ ਤੇ ਜਥਾ ਲੈ ਕੇ ਜਾਂਦੇ ਰਹੇ ਹਨ।
</p>

<p>ਬੱਬਰ ਅਕਾਲੀ ਦਲ ਦੇ ਬਾਨੀ ਸ.ਕਰਤਾਰ ਸਿੰਘ
ਨਾਰੰਗ ਨਹੀਂ ਰਹੇ </p>

<p>ਲੁਧਿਆਣਾ, 14 ਦਸੰਬਰ - ਬੱਬਰ ਅਕਾਲੀ ਦਲ ਨੇ ਬਾਨੀ ਪ੍ਰਧਾਨ
ਸ.ਕਰਤਾਰ ਸਿੰਘ ਨਾਰੰਗ ਸੰਖੇਪ ਬਿਮਾਰੀ ਉਪਰੰਤ ਅਕਾਲ ਚਲਾਣਾ ਕਰ ਗਏ ਹਨ । ਉਨ੍ਹਾਂ ਦਾ ਅੰਤਿਮ
ਸੰਸਕਾਰ ਨਵੀਂ ਦਿੱਲੀ ਵਿਖੇ ਕਰ ਦਿੱਤਾ ਗਿਆ । ਇਸ ਮੌਕੇ ਵੱਖ-ਵੱਖ ਵਰਗਾਂ ਨਾਲ ਸਬੰਧਤ ਆਗੂਆਂ
ਨੇ ਹਿੱਸਾ ਲਿਆ । ਦਿੱਲੀ ਗੁਰਦੁਆਰਾ ਪ੍ਰਬੰਧਕ ਕਮੇਟੀ ਦੇ ਪ੍ਰਧਾਨ ਸ.ਅਵਤਾਰ ਸਿੰਘ ਹਿੱਤ, ਮੈਂਬਰ
ਧਰਮ ਪ੍ਰਚਾਰ ਕਮੇਟੀ ਸ.ਇੰਦਰਪਾਲ ਸਿੰਘ ਖਾਲਸਾ, ਮੈਂਬਰ ਦਿੱਲੀ ਕਮੇਟੀ ਸ.ਮਨਜੀਤ ਸਿੰਘ ਕਾਲਕਾ,
ਸ.ਤੀਰਥ ਸਿੰਘ ਅਕਾਲੀ ਆਗੂ, ਸ.ਗੁਰਦੇਵ ਸਿੰਘ ਬਟਾਲਵੀ ਅਤੇ ਸ.ਸੁਰਿੰਦਰਪਾਲ ਸਿੰਘ ਸਮੇਤ ਕਈ
ਪਤਵੰਤੇ ਹਾਜਰ ਸਨ ਜਿਨ੍ਹਾਂ ਨੇ ਸ.ਨਾਰੰਗ ਦੇ ਚਲਾਣੇ ਤੇ ਦੁੱਖ ਪ੍ਰਗਟ ਕਰਦਿਆਂ ਹਮਦਰਦੀ ਪ੍ਰਗਟ ਕੀਤੀ ।
ਸ.ਬੱਬਰ ਨਮਿਤ ਰੱਖੇ ਪਾਠ ਦਾ ਭੋਗ ਉਪਰੰਤ ਕੀਰਤਨ ਅਤੇ ਅੰਤਿਮ ਅਰਦਾਸ 17 ਦਸੰਬਰ ਦਿਨ ਐਤਵਾਰ
ਦੁਪਹਿਰ 2.30 ਤੋਂ 4.30 ਵਜੇ ਤਕ ਗੁਰਦੁਆਰਾ ਸੰਤ ਪੁਰਾ ਤਿਲਕ ਨਗਰ, ਦਿੱਲੀ ਵਿਖੇ ਹੋਵੇਗੀ । </p>

<p>          ieQy
vrnxwog hY ik s.n`rMg dy donoN lVky srk`rI E@qv`d d` iSk`r ho ky ShId ho gey sn
| auh E`p vI keI kys~ ivc aulJy hoey sn | </p>

<p>ਚੰਡੀਗੜ੍ਹ ਵਿਚ ਪੰਜਾਬੀ ਭਾਸ਼ਾ ਨੂੰ ਬਣਦਾ
ਰੁਤਬਾ ਦਿਵਾਉਣ ਲਈ ਰੈਲੀ </p>

<p>ਚੰਡੀਗੜ੍ਹ, 14 ਦਸੰਬਰ - ਪੰਜਾਬੀ ਅਤੇ ਪੰਜਾਬ ਵਿਕਾਸ ਮੰਚ ਵਲੋਂ ਅੱਜ
ਫਿਰ ਪੰਜਾਬੀ ਭਾਸ਼ਾ ਨੂੰ ਚੰਡੀਗੜ੍ਹ ਵਿਚ ਬਣਦਾ ਰੁਤਬਾ ਦਿਵਾਉਣ ਲਈ ਬੱਸ ਸਟੈਂਡ ਦੇ ਮੁੱਖ ਚੌਂਕ
ਵਿਖੇ ਪ੍ਰਸਿੱਧ ਲੇਖਕ ਸ.ਹਰਭਜਨ ਸਿੰਘ ਹਲਵਾਰਵੀ ਦੀ ਅਗਵਾਈ ਹੇਠ ਰੋਸ ਪ੍ਰਦਰਸ਼ਨ ਕੀਤਾ ਗਿਆ । ਇਸ
ਮੌਕੇ ਮਨਿੰਦਰ ਗਿੱਲ ਵੀ ਹਾਜ਼ਰ ਹੋਏ । </p>

<p>          ies
pRdrSn ivc v@fI igxqI ivc pMj`bI g`iek qy sMgIqk`r, lok-g`iek kl` mMc cMfIgVH
dy cyErmYn qy g`iek hrdIp dI Egv`eI hyT S`iml hoey | v@K-v@K pMj`bI sMsQ`v~ dy
E`gU pirv`r~ smyq pRdrSn ivc S`ml hoey Coty b@cy h@Q~ ivc m`to PV ky jo m~ bolI
nUM Bu@l j`vyg`, auh k@Q~ v~g ru@l j`vyg`, pMj`b dI r`jD`nI cMfIgVH ivc pMj`bI
B`S` l`gU kro dy n`Ery l` rhy sn | </p>

<p>          ies mOky
s.hrBjn isMG hlv`rvI ny E`pxI qkrIr ivc B`vuk huMidE~ ikh` ik pMj`b dI r`jD`nI
ivc hI s`fI m~ bolI pMj`bI rulI peI hY | aunH~ du@K z`ihr kIq` pMj`b qoN
fYpUtySn qy wU.tI. pRS`sn ivc E`ey EiDk`rI vI pMj`bI B`S` pRqI suihrd nhIN hn |
s.hlv`rvI ny SMk` j`ihr kIqI ik cMfIgVH ivc pMj`bI B`S` dy Kqm hox n`l pMj`bIE~
dI Biv@K ivclI pIVHI Eqy siBE`c`r n`loN tu@t j`vygI jo iek v@f` duK~q hovyg` |
aunH~ smUh pMj`bI ihqYSIE~ nUM cMfIgVH ivc pMj`bI B`S` d` bxd` ruqb` idv`aux
leI j@do-jihd krn d` s@d` id@q` Eqy mMc dI pihlkdmI dI sr`hn` kIqI | </p>

<p>          mMc dI
cyErprsn hrijMdr kOr qy pRD`n qrlocn isMG ny ikh` ik auh smu@cIE~ pMj`bI
sMsQ`v~ Eqy pMj`bI ihqYSIE~ nUM iek QVHy ho ky iek@T` krky m~ bolI pMj`bI leI
jh`d inrMqr j`rI rihxgy | aunH~ cMfIgVH pRS`sn nUM icq`vnI id@qI ik jykr pMj`bI
ihqYSIE~ dy sMGrS Eqy E`v`z nUM E@KoN-proKy krn dI nIqI n` C@fI q~ nyVy Biv@K
ivc sMGrS nUM hor ivS`l kr id@q` j`vyg` | g`iek hrdIp ny E`pxI hI sur ivc m~
bolI pMj`bI dI mh@qq` Eqy ies dI r`KI krn d` hok` idMidE~ EYl`n kIq` ik
gIq-sMgIq dy Kyqr dy smUh kl`k`r ies sMGrS ivc v@D-cVH ky S`iml hoxgy |</p>

<p>ਮਨੁੱਖੀ ਅਧਿਕਾਰ ਕਮਿਸ਼ਨ ਵਲੋਂ ਪੰਜਾਬ
ਪੁਲਿਸ ਨੂੰ ਨੋਟਿਸ </p>

<p>ਚੰਡੀਗੜ੍ਹ, 14 ਦਸੰਬਰ - ਪੰਜਾਬ ਮਨੁੱਖੀ ਅਧਿਕਾਰ ਕਮਿਸ਼ਨ ਨੇ
ਅੰਮ੍ਰਿਤਸਰ ਵਿਚ ਤਿੰਨ ਕਥਿਤ ਖਾੜਕੂਆਂ ਵਿਰੁੱਧ ਦਰਜ ਐਫ.ਆਈ.ਆਰ. ਦੇ ਸਬੰਧ ਵਿਚ ਆਪਣੀ
ਹਦਾਇਤ ਤੇ ਅਮਲ ਨਾ ਕਰਨ ਲਈ ਪੰਜਾਬ ਪੁਲਿਸ ਨੂੰ ਨੋਟਿਸ ਜਾਰੀ ਕੀਤਾ ਹੈ । ਕਮਿਸ਼ਨ ਦਾ ਪੂਰਾ
ਬੈਂਚ ਅਗਲੇ ਹਫਤੇ ਇਸ ਮਾਮਲੇ ਉਪਰ ਵਿਚਾਰ ਕਰੇਗਾ । ਸੇਵਾ ਮੁਕਤ ਜਸਟਿਸ ਜੇ.ਐਸ.ਸੇਖੋਂ ਨੇ
ਪਹਿਲਾਂ ਜਾਰੀ ਹਦਾਇਤ ਵਿਚ ਪੁਲਿਸ ਨੂੰ ਕਿਹਾ ਸੀ ਕਿ ਐਫ.ਆਈ.ਆਰ. ਰੱਦ ਕਰ ਦਿੱਤੀ ਜਾਵੇ ਅਤੇ
ਹਰ ਇਕ ਵਿਅਕਤੀ ਨੂੰ 50 ਹਜ਼ਾਰ ਰੁਪਏ ਹਰਜਾਨਾ ਦਿੱਤਾ ਜਾਵੇ । ਇਨ੍ਹਾਂ ਵਿਅਕਤੀਆਂ ਨੂੰ ਝੂਠੇ ਤੌਰ
ਤੇ ਮਾਮਲੇ ਵਿਚ ਫਸਾਇਅ ਗਿਆ ਸੀ । </p>

<p>          kimSn dy
cyErprsn syv`mukq cIP jsits vI.ky.KMn` ny ies g@l qy ieqr`j kIq` hY ik EMimRqsr
dy EYs.EYs.pI. vloN iek smyN kimSn nUM id@qy gey vcn ik aus dy hukm qy Eml kIq`
j`vyg`, dy b`vjUd puils ny EMimRqsr dI iek Ed`lq ivc iqMn~ ivEkqIE~ ivru@D cl`x
pyS kr id@q` hY | </p>

<p>          puils ny
15 jul`eI 1998 nUM drj kIqy m`mly ivc 4 dsMbr nUM cl`x pyS kIq` | iqMn ivEkqIE~
r`jIv isMG, srbjIq isMG Eqy rCp`l isMG auqy sUby ivc K`VkUpuxy nUM muV surjIq
krn dy mksd n`l t`eIgrz E`P is@K lYNf n~ dI iek nvIN K`VkU jQybMdI bx`aux d`
doS l`ieE` igE` | </p>

<p>          iek
ivEkqI srbjIq isMG dy ipq` ny EiDk`r kimSn kol kIqI iSk`ieq ivc puils dy p@K
nUM vMg`irE` | ies m`mly dI j~c EYfISnl f`ierYktr jnrl ey.pI.Btn`gr ny kIqI |
aunH~ E`pxI irport ivc it@pxI kIqI ik iqMn~ nUM m`mly ivc JUTy qOr qy S`ml kIq`
igE` hY, ikauNik ienH~ ivcoN iek r`jIv isMG K`lV` m`mly ivc mu@K gv`h hY | ieh
m`ml` pMj`b puils vloN ExpC`qIE~ l`S~ dy smUihk sMsk`r sbMDI hY | </p>

<p>          Btn`gr
ny E`pxI irport ivc it@pxI kridE~ ikh` ik r`jIv isMG nUM K`lV` m`mly ivc puils
dI bolI bolx dy wqn ivc m`mly ivc Ps`ieE` igE` hY | </p>

<p>          pr
E`eI.jI.(mukdmyb`zI) ny 20 nvMbr nUM kimSn nUM ic@TI ilK ky bynqI kIqI hY ik
E`pxy PYsly qy muV ivc`r kIq` j`vy | ਆਈ.ਜੀ. ਨੇ ਕਿਹਾ ਕਿ ਕਿਉਂਕਿ
ਐਫ.ਆਈ.ਆਰ. ਦਰਜ ਕੀਤੀ ਜਾ ਚੁੱਕੀ ਹੈ ਇਸ ਕਰਕੇ ਮਾਮਲਾ ਅਦਾਲਤ ਦੇ ਵਿਚਾਰ ਅਧੀਨ ਬਣ ਚੁੱਕਾ
ਹੈ ਅਤੇ ਐਫ.ਆਈ.ਆਰ. ਨੂੰ ਰੱਦ ਕਰਨ ਚੱਲ ਰਹੀ ਪੁਲਿਸ ਜਾਂਚ ਵਿਚ ਦਖਲਅੰਦਾਜ਼ੀ ਕਰਨ ਦੇ ਤੁੱਲ
ਹੋਵੇਗਾ । ਆਈ.ਜੀ. ਨੇ ਮਾਮਲਾ ਦਰਜ ਕਰਨ ਵਾਲੇ ਥਾਣੇ ਦੇ ਮੁੱਖ ਅਫਸਰ ਲੱਖਾ ਸਿੰਘ ਵਿਰੁੱਧ
ਜਾਬਤਾ ਕਾਰਵਾਈ ਸ਼ੁਰੂ ਕਰਨ ਤੇ ਵੀ ਕਿੰਤੂ ਕੀਤਾ । </p>

<p>          syv`
mukq cIP jsits KMn` ny m`mly dy irivaU sbMDI kihx leI puils dy EiDk`r nUM shI
Tihr`ieE` pr ies g@l qy ieqr`z kIq` ik puils ny m`ml` Kqm nhIN kIq` | </p>

<p>          aunH~ d@isE` ik jykr sUb`
puils ny kimSn dy hukm dI aulMGx` krnI j`rI r@KI q~ kimSn pMj`b Eqy hirE`x`
h`eI kort qk phuMc krn b`ry ivc`r kryg` |  </p>



<p>ਜਦ ਰਾਜ ਬੱਬਰ ਮੰਤਰੀ ਨਾਲ ਉਲਝ ਪਏ </p>

<p>ਨਵੀ ਦਿੱਲੀ, 14 ਦਸੰਬਰ - ਉਘੇ ਫਿਲਮ ਐਕਟਰ ਅਤੇ ਸਮਾਜਵਾਦੀ
ਪਾਰਟੀ ਦੇ ਨੇਤਾ ਰਾਜ ਬੱਬਰ ਅੱਜ ਉਸ ਸਮੇਂ ਕੇਂਦਰੀ ਰਾਜ ਮੰਤਰੀ ਸ੍ਰੀ ਸ਼ਾਹਨਵਾਜ਼ ਹੁਸੈਨ ਨਾਲ ਉਲਝ
ਪਏ ਜਦ ਲੋਕ ਸਭਾ ਵਿਚ ਅਯੁੱਧਿਆ ਮਸਲੇ ਤੇ ਬਹਿਸ ਦੌਰਾਨ ਹੁਸੈਨ ਨੇ ਬਾਬਰੀ ਮਸਜਿਦ ਡੇਗੇ ਜਾਣ
ਲਈ ਕਾਂਗਰਸ ਅਤੇੇ ਸਮਾਜਵਾਦੀ ਪਾਰਟੀ ਨੂੰ ਦੋਸੀ ਠਹਿਰਾਇਆ । </p>

<p>          BVky
hoey sp` mYNbr b@br ny mMqrI nUM ikh` ik auh kor` JUT bol rhy hn | aunH~ ikh`
ik qusI kyvl E`pxI kursI bx`aux leI E`pxI E`qm` igrvI r@K idqI hYY | ies qy
srk`rI bYNc vI BVk pey | gu@sy ivc E`ey mMqrI ny r`j b@br nUM ikh` ik ieh koeI
iPlmI dunIE` nhI ijQy JUT-mUT dI lV`eI huMdI hY | qusIN ieQy E`pxy E`p nUM hIro
n` smJo Eqy E`pxI SIt qy bYT j`A | ies mOky qy b@br ny spIkr dy mMc E@gy j`x
leI kuJ kdm pu@ty pr E`pxI p`rtI dy muKI mul`iem isMG w`dv vloN ieS`r` krn qy
E`pxI SIt qy E` gey | </p>

<p>ਹਰਪ੍ਰੀਤ ਕੇਸ: ਦੋ ਉਚ ਪੁੁਲਿਸ ਅਫਸਰਾਂ
ਤੋਂ ਪੁੱਛ-ਪੜਤਾਲ </p>

<p>ਕਪੂਰਥਲਾ, 14 ਦਸੰਬਰ - ਕੇਂਦਰੀ ਜਾਂਚ ਬਿਊਰੋ (ਸੀ.ਬੀ.ਆਈ.) ਦੀ
ਇਕ ਟੀਮ ਨੇ ਅੱਜ ਹਰਪ੍ਰੀਤ ਕੌਰ ਕਤਲ  kys dy
sbMD ivc kpUrQl` dy EYs.EYs.pI. iekb`l isMG Eqy EYs.pI. (hYfkuE`tr) munIS c`vl`
qoN krIb do GMty puC-pVq`l kIqI | </p>

<p>          SRomxI
gurduE`r` pRbMDk kmytI dI s`bk` pRD`n bIbI jgIr kOr dI DI hrpRIq kOr aurP rozI
21 EpRYl nUM Pgv`V` ivc ByqBry h`lq ivc cl vsI sI | sI.bI.E`eI. dI tIm ny dov~
EiDk`rIE~ qoN p@uiCE` ik 21 EpRYl v`ly idn auh kI kr rhy sn Eqy aunH~ nUM
hrpRIq kOr dI mOq dI kpUrQl` ivc kdoN imlI sI | </p>

<p>        ਇਹ ਟੀਮ ਪਿਛਲੇ
ਕੁਝ ਦਿਨਾਂ ਤੋਂ ਪੰਜਾਬ ਵਿਚ ਹੈ ਇਹਨੇ ਇਸ ਕਾਂਡ ਦਾ ਨਿੱਕਾ-ਨਿੱਕਾ ਵੇਰਵਾ ਇੱਕਤਰ ਕਰਨ ਲਈ
ਬੇਗੋਵਾਲ ਦਾ ਵੀ ਦੌਰਾ ਕੀਤਾ ਸੀ । ਟੀਮ ਨੇ ਭੁਲੱਥ ਦੇ ਸਾਬਕਾ ਐਸ.ਐਚ.ਓ. ਬਲਕਾਰ ਸਿੰਘ ਤੋਂ ਵੀ
ਪੁਛ-ਪੜਤਾਲ ਕੀਤੀ ਸੀ ਕਿਉਂਕਿ ਉਹ ਬੀਬੀ ਜਗੀਰ ਦੇ ਨੇੜੇ ਸਮਝੇ ਜਾਂਦੇ ਹਨ । ਸ੍ਰੀ ਬਲਕਾਰ ਸਿੰਘ
ਇਸ ਵੇਲੇ ਜਲੰਧਰ ਵਿਖੇ ਸੀ.ਆਈ.ਏ. ਇੰਸਪੈਕਟਰ ਹਨ ਅਤੇ ਸਮਝਿਆ ਜਾਂਦਾ ਹੈ ਕਿ ਉਹ ਬੀਬੀ
ਜਗੀਰ ਕੌਰ ਦੀ ਕਥਿਤ ਸਰਪ੍ਰਸਤੀ ਸਦਕਾ ਹੀ ਭੁਲੱਥ ਪੁਲਿਸ ਸਟੇਸਨ ਦੇ ਐਸ.ਐਚ.ਓ. ਰਹੇ ਹਨ । </p>

<p> </p>






</body></text></cesDoc>