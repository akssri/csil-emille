<cesDoc id="pun-w-media-lot4bc" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-media-lot4bc.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>BHARAT DA LOH PURUSH</h.title>
<h.author>KAUR UPKAR</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - PUNJABI TRIBUNE - 31.10.84 - CHANDIGARH</publisher>
<pubDate>1984</pubDate>
</imprint>
<idno type="CIIL code">lot4bc</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 4.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-23</date></creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;518&gt;&lt;KAUR U. - BHARAT LOH PURUSH&gt;&lt;PRIYA KUMAR&gt;</p>

<p>&lt;     ਸਰਦਾਰ ਵਲਭ ਭਾਪ ਪਟਲਲ ਨੂਤ ਭਾਰਤ ਦਾ `ਲਸ਼ਹ ਪੁਰੁਸ਼' ਕਿਹਾ ਜਾਥਦਾ 
ਹਲ਼ ਕਿਫਥਕਿ ਨਪ ਦਹਰਿਡਹਹ ਵਿਸ਼ਵਾਸ, ਪਕਹਕਲ ਩ਰਾਦਲ ਤਲ ਠਸ਼ਸ ਵਿਚਾਰਾਥ ਦੀ ਸ਼ਖਸੀਧਤ 
ਦਲ ਮਾਲਕ ਸਨ ਾ ਨਪ ਨਲ ਨਾ ਸਿਰਫ ਦਲਸ ਦੀ ਨਜਾਦੀ ਲਪ ਘਾਲਣਾ ਘਾਲੀ 
ਸੀ, ਸਗਸ਼ਥ ਬਿਖਰਲ ਹਸ਼ਯ ਦਲਸ਼ ਨੂਤ ਩ਕਮੁਠਹਠ ਵੀ ਕਰ ਦਿਤਾ ਾ </p>

<p>     ਸਰਦਾਰ ਵਲਭ ਭਾਪ ਪਟਲਲ ਦਾ ਜਨਮ 31 ਧਕਤੂਬਰ, 1875 ਨੂਥ 
ਕਰਮਸਾਡਹਹ ਵਿਖਲ ਗੁਜਰਾਤ ਵਿਚ ਹਸ਼਩ਨ ਾ ਨਪ ਦਲ ਪਿਤਾ ਝਾ਴ਲਰ ਭਾਪ ਪਟਲਲ 
ਨੀਵੀਥ ਮਧਹਧ ਸ਼ਹਰਲਣੀ ਦਲ ਕਿਸਾਨ ਸਨ ਾ ਨਪ ਨਲ ਮੁਢਲੀ ਸਿਖਿਨ ਗੁਜਰਾਤੀ 'ਚ 
ਪਹਰਾਪਤ ਕੀਤੀ ਾ ਫਸ ਸਮਲਥ ਧਜ ਵਾਥਗ ਵਿਦਿਧਕ ਸਹੂਲਤਾਥ ਫਪਲਬਧ ਨਹੀਥ 
ਸਨ ਾ ਩ਸ ਲਪ ਨਪ ਨਲ 22 ਵਰਹਹਿਨਥ ਦੀ ਫਮਰ ਵਿਚ ਮਲ਼ਟਹਰਿਕ ਪਾਸ ਕੀਤੀ ਾ 
਩ਸ ਤਸ਼ਥ ਮਗਰਸ਼ਥ ਨਪ ਨਲ ਵਕਾਲਤ ਦੀ ਪਹਰੀਖਿਨ ਪਾਸ ਕੀਤੀ ਧਤਲ ਾ ਩ਸ ਤਸ਼ਥ 
ਮਗਰਸ਼ਥ ਨਪ ਨਲ ਵਕਾਲਤ ਦੀ ਪਹਰੀਖਿਨ ਪਾਸ ਕੀਤੀ ਧਤਲ ਬਲਸਾਡ ਵਿਖਲ 
ਫ਷ਜਦਾਰੀ ਵਕਾਲਤ ਸ਼ੁਰੂ ਕਰ ਦਿਤੀ ਾ 1910 ਵਿਚ ਨਪ ਩ਤਗਲਲ਼ਥਡ ਵਿਚ 
ਬਲ਼ਰਿਸਟਰੀ ਦੀ ਤਾਲੀਮ ਹਾਸਲ ਕਰਨ ਲਪ ਚਲਲ ਗਯ ਧਤਲ 1913 ਵਿਚ 
ਬਲ਼ਰਿਸਟਰ ਬਣ ਕਲ ਩ਤਗਲਲ਼ਥਡ ਤਸ਼ਥ ਵਾਪਸ ਪਰਤਲ ਤਲ ਧਹਿਮਦਾ-ਬਾਦ ਵਿਚ 
ਪਹਰਲ਼ਕਟਿਸ ਸ਼ੁਰੂ ਕਰ ਦਿਤੀ ਾ ਨਪ ਨਪਣਲ ਩ਸ ਪਲਸ਼ਲ ਵਿਚ ਩ਤਨਲ ਮਾਹਰ ਸਨ 
ਕਿ ਜਜਹਜ ਤਕ ਵੀ ਨਪ ਨਲ ਕਤਨੀ ਕਤ-~ਰਾਫਥਦਲ ਸਨ ਾ </p>

<p>     1915 ਵਿਚ ਨਪ ਗਾਥਧੀ ਜੀ ਦਲ ਸਤਪਰਕ ਵਿਚ ਨਯ ਧਤਲ ਛਲਤੀ ਹੀ ਗਾਥਧੀ 
ਜੀ ਦਲ ਩ਤਨਲ ਨਲਡਲ ਹਸ਼ ਗਯ ਕਿ ਸਹਰੀ ਰਾਜ ਗਸ਼ਪਾਲ ਧਚਾਰੀਨ ਜੀ ਮਜਾਕ 'ਚ ਕਹਿਣ 
ਲਗਲ : `ਗਾਥਧੀ ਜੀ ਤਲ ਪਟਲਲ ਦਾ ਰਿਸ਼ਤਾ ਤਾਥ ਰਾਮ-ਲਛਮਣ ਵਾਲਾ ਹਲ਼ ਾ' 1917 
ਵਿਚ ਨਪ ਮਿਫਥਸਪਲ ਕਮਲਟੀ, ਧਹਿਮਦਾਬਾਦ ਦਲ ਮਲ਼ਥਬਰ ਚੁਣਲ ਗਯ ਤਲ 1924 ਤਸ਼ਥ 
1928 ਤਕ ਩ਸ ਦਲ ਪਹਰਧਾਨ ਰਹਲ ਾ </p>

<p>     1931 ਵਿਚ ਨਪ ਩ਤਡੀਧਨ ਨਲ਼ਸ਼ਨਲ ਕਾਥਗਰਸ ਦਲ ਪਹਰਧਾਨ ਚੁਣਲ ਗਯ ਾ 
1931-32 ਦੀ ਸਿਵਲ-ਨਾ-ਫੁਰਮਾਨੀ ਲਹਿਰ ਸਮਲ ਨਪ 16 ਮਹੀਨਲ ਜਲਲਹਹ 'ਚ 
ਕਲ਼ਦ ਰਹਲ ਾ 1934 ਵਿਚ ਰਿਹਾ ਹਸ਼ਣ ਤਸ਼ਥ ਮਗਰਸ਼ 1935 ਤਸ਼ਥ 1940 ਤਕ ਨਪ 
ਕਾਥਗਰਸ ਪਾਰਲੀਮਾਨੀ ਸਬ ਕਮਲਟੀ ਦਲ ਪਹਰਧਾਨ ਰਹਲ ਾ </p>

<p>     8 ਧਗਸਤ, 1942 ਨੂਤ ਬਤਬਪ ਵਿਖਲ ਕਾਥਗਰਸ ਨਲ ਨਪਣਾ ਩ਤਿਹਾਸਕ ਮਤਾ 
`ਧਤਗਰਲਜਸ਼ ਭਾਰਤ ਛਡ ਜਾਓ' ਪਾਸ ਕੀਤਾ ਾ 9 ਧਗਸਤ ਨੂਥ ਕਾਥਗਰਸ ਵਰਕਿਤਗ 
ਕਮਲਟੀ ਦਲ ਬਾਕੀ ਮਲ਼ਥਬਰਾਥ ਨਾਲ ਨਪ ਨੂਤ ਗਹਰਿਫਤਾਰ ਕਰਕਲ `ਧਹਿਮਦ ਨਗਰ' ਵਿਚ 
ਨਜਰਬਤਦ ਕਰ ਦਿਤਾ ਗਿਨ ਾ </p>

<p>     ਸਤਤਬਰ, 1946 ਵਿਚ ਪਤਡਤ ਜਵਾਹਰ ਲਾਲ ਨਹਿਰੂ ਦੀ ਧਗਵਾਪ ਹਲਠ 
ਜਿਹਡੀ ਧਤਤਹਰਿਮ ਸਰਕਾਰ ਬਣਾਪ ਗਪ, ਫਸ ਵਿਚ ਨਪ ਨੂਤ ਗਹਰਹਿ ਤਲ ਸੂਚਨਾ- 
ਪਹਰਸਾਰ ਮਹਿਕਮਿਨਥ ਦੀ ਜਿਤਮਲਵਾਰੀ ਦਿਤੀ ਗਪ ਾ ਦਲਸ਼ ਦੀ ਨਜਾਦੀ ਤਸ਼ਥ ਮਗਰਸ਼ਥ 
ਨਪ ਦੀ ਸਭ ਤਸ਼ਥ ਵਡਹਡੀ ਸਫਲਤਾ ਭਾਰਤ ਦੀਨਥ 526 ਰਿਨਸਤਾਥ ਨੂਤ ਩ਕਠਹਠੀਨਥ 
ਕਰਕਲ 26 ਰਾਜਾਥ ਵਿਚ ਸਤਗਠਨ ਕਰਨਾ ਸੀ ਾ ਜੂਨਾਗਡਹਹ, ਹਲ਼ਦਰਾਬਾਦ ਧਤਲ 
ਕਸ਼ਮੀਰ ਜਿਹਲ ਮਾਮਲਿਨਥ ਨੂਤ ਨਪ ਨਲ ਬਹੁਤ ਹੀ ਦੂਰ-ਧਤਦਲਸ਼ੀ ਨਾਲ ਨਿਪਟਾ਩ਨ ਾ </p>

<p>     ਨਪ ਦਲਸ਼ ਦੀ ਖਾਤਰ ਹਰ ਸਮਲਥ ਕਾਰਜਸ਼ੀਲ ਰਹਿਤਦਲ ਾ ਩ਕ ਵਾਰ ਗਾਥਧੀ 
ਜੀ ਨਲ ਨਪ ਨੂਤ ਧਰਾਮ ਕਰਨ ਲਪ ਕਿਹਾ ਤਾਥ ਨਪ ਨਲ ਫਤਹਤਰ ਦਿਤਾ ਕਿ `ਜਲ ਮਲ਼ਥ 
ਦਲਸ਼ ਦੀ ਖਾਤਰ ਕਤਮ ਕਰਦਿਨਥ ਕੁਝ ਦਿਨ ਪਹਿਲਾਥ ਮਰ ਗਿਨ ਤਾਥ ਩ਸ ਨਾਲ 
ਕਸ਼ਪ ਫਰਕ ਨਹੀਥ ਪਲ਼ਥਦਾ ਾ' ਨਪ ਨਲ ਨਪਣੀ ਲਗਨ ਨਾਲ ਕਤਮ ਜਾਰੀ ਰਖਿਨ, 
ਭਾਵਲਥ ਨਪ ਬਿਮਾਰ ਰਹਿਣ ਲਗ ਪਯ ਸਨ ਾ 12 ਦਸਤਬਰ, 1950 ਨੂਤ ਨਪ 
ਬਤਬਪ ਩ਲਾਜ ਲਪ ਗਯ, ਪਰ ਤਿਤਨ ਦਿਨਾਥ ਪਿਛਸ਼ਥ 15 ਦਸਤਬਰ, 1950 ਨੂਤ 
ਦਿਲ ਦਲ ਦ਷ਰਲ ਕਾਰਨ ਚਲ ਵਸਲ ਾ ਨਪ ਦੀ ਮ਷ਤ ਦੀ ਖਬਰ ਸੁਣਦਿਨਥ ਹੀ 
ਨਹਿਰੂ ਜੀ ਨਲ ਕਿਹਾ, ``ਲਗਦਾ ਯ ਮਲ਼ਥ ਧਤਗਹੀਣ ਹਸ਼ ਗਿਨ ਹਾਥ ਾ' ਨਹਿਰੂ ਜੀ 
ਦਲ ਩ਸ ਕਥਨ ਤਸ਼ਥ ਪਟਲਲ ਜੀ ਦੀ ਦਲਸ਼ ਲਪ ਧਹਿਮੀਧਤ ਸਪਸ਼ਟ ਹਸ਼ ਜਾਥਦੀ 
ਹਲ਼ ਾ </p>

<p>+&gt;
               0 
</p></body></text></cesDoc>