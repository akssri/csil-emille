<cesDoc id="pun-w-socsci-law-lot6g" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-socsci-law-lot6g.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>COUNSTITUTION &amp; LAW</h.title>
<h.author>ANANDA P.L.</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - DHINGRA VIRINDER - JULLUNDHUR</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">lot6g</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 12-17.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-23</date></creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;2141&gt;&lt;ANAND P.L. - CONSTITUTION AND LAW&gt;&lt;SHASHI BHUSHAN&gt;</p>

<p>&lt;     ਸਤਘ ਦਲ ਵਖਹਖ-ਵਖਹਖ ਩ਕਲਹਲਿਨਥ ਨੂਥ ਨਪਣਲ ਨਪਣਲ ਖਲਤਰ ਵਿਚ ਪੂਰਣ 
ਨਜਾਦੀ ਪਹਰਾਪਤ ਸੀ ਾ ਫਨਹਹਾਥ ਦੀ ਕਲਥਦਰੀ ਸ਼ਾਸਨ-ਪਹਰਣਾਲੀ ਵਾਥਗ ਨਪਣੀ ਵਖਹਖਰੀ 
ਧਤਲ ਸੁਤਤਤਰ ਸ਼ਾਸਨ ਪਹਰਣਾਲੀ ਸੀ ਾ ਩ਨਹਹਾਥ ਸਭ ਕੁਝ ਹੁਤਦਲ ਹਸ਼ਯ ਵੀ ਕਲਥਦਰੀ ਸਰਕਾਰ 
(ਜਿਹਡੀ ਮਾਸਕਸ਼ ਵਚਿ ਸਥਿਤ ਸੀ, ਩ਤਨਾਥ ਩ਕਾਪਨਥ ਤਲ ਕਾਫੀ ਨਿਗਰਾਨੀ ਰਖਹਖਦੀ 
ਸੀ ਾ </p>

<p>     ਸਤਖਲਪ ਵਿਚ 1918 ਦਲ ਸਤਵਿਧਾਨ ਵਾਥਗ 1924 ਦਾ ਸਤਵਿਧਾਨ ਵੀ ਕਹਰਾਥਤੀਕਾਰੀ 
ਮਨਸ਼ਰਥ ਨਾਲ ਬਣਾ਩ਨ ਗਿਨ ਸੀ ਧਤਲ ਫਹ ਮਨਸ਼ਰਥ ਸੀ ਕਿਰਤੀ ਵਰਗ ਜਾਥ ਸਰਵ 
ਸਾਧਾਰਨ ਨੂਤ ਸ਼ਕਤੀਧਾਰੀ ਬਣਾਫਣਾ ਾ ਸਤਵਿਧਾਨ ਦਾ ਪਹਿਲਾ ਧਨੁਛਲਦ ਹੀ ਩ਸ 
ਗਲ ਦਾ ੟ਲਾਨ ਕਰਦਾ ਹਲ਼ ਕਿ ``ਬੁਰਜੁਨਵਾਦੀ ਸਮਾਜ ਦਾ ਦਬਣ ਲਪ ਮਨੁਖਹਖ 
ਦਲ ਦੁਨਰਾ ਮਨੁਖਹਖ ਦੀ ਲੁਟਖਸੁਟ ਖਤਮ ਕਰਨ ਲਪ ਜਿਸ ਵਿਚ ਨਾ ਵਰਗ ਭਲਦ 
ਰਹਿਣਗਲ ਜਾਥ ਰਾਜ ਦੀ ਸ਼ਕਦੀ ਰਹਲਗੀ ਧਤਲ ਸਰਵਹਾਰਾ ਵਰਗ ਦੀ ਤਾਨਾਸ਼ਾਹੀ ਨੂਤ 
ਖਤਮ ਕਰਨ ਲਪ 1924 ਦਲ ਸਤਵਿਧਾਨ ਨੂਥ ਬਣਾ਩ਨ ਗਿਨ ਾ'' </p>

<p>     ਫਪਰਲੀ ਵਿਨਖਿਨ ਤਸ਼ਥ ਩ਸ ਗਲਹਲ ਨੂਤ ਮਹਿਸੂਸ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹਲ਼ ਕਿ 
1924 ਦਾ ਸਤਵਿਧਾਨ 1918 ਦਲ ਸਤਵਿਧਾਨ ਦਲ ਟਾਕਰਲ ਵਿਚ ਵਧਲਰਲ ਫਦਾਰਵਾਦੀ ਸੀ ਾ 
1924 ਤਸ਼ਥ ਹੀ ਰੂਸ ਦੀ ਨਵੀਥ ਨਰਥਕ ਨੀਤੀ ਸ਼ੁਰੂ ਹੁਤਦੀ ਹਲ਼ ਾ ਪਤਜਸਾਲਾ ਯਸ਼ਜਨਾਵਾਥ 
ਦਲ ਕਾਰਨ ਸਸ਼ਵੀਧਤ ਰੂਸ ਵਿਚ ਸਮਾਜਕ ਤਲ ਨਰਥਕ ਖਲਤਰਾਥ ਵਿਚ ਬਹੁਤ ਵਧਲਰਲ 
ਫਨਹਨਤੀ ਹਸ਼ਪ ਾ ਦੂਜੀ ਪਤਜ-ਸਾਲਾਥ ਯਸ਼ਜਨਾ ਦੀ ਸਮਾਪਤੀ ਤਕ ਦਲਸ਼ ਵਿਚ ਪੂਤਜੀਵਾਦ 
ਪੂਰਨ ਰੂਪ ਨਾਲ ਖਤਮ ਹਸ਼ ਚੁਕਾ ਸੀ ਾ 1935 ਵਿਚ ਰੂਸੀ ਨਲਤਾਵਾਥ ਨਲ ਦਾਵਾ ਕੀਤਾ 
ਕਿ ਰੂਸੀ ਧਰਥ-ਵਿਵਸਥਾ ਦਾ ਪੂਰਨ ਰੂਪ ਨਾਲ ਸਮਾਜੀ-ਕਰਣ ਹਸ਼ ਚੁਕਹਕਾ ਹਲ਼ ਧਤਲ 
ਵਰਗ ਭਲਦ ਖਤਮ ਕਰ ਦਿਤਹਤਲ ਗਯ ਹਨ ਾ </p>

<p>     ਜਿਸ ਤਰਹਹਾਥ ਬਦਲਦੀਨਥ ਹਸ਼ਪਨਥ ਹਾਲਤਾਥ ਨਲ 1918 ਦਲ ਸਤਵਿਧਾਨ ਵਿਚ ਤਬਦੀਲੀ 
ਲਾਜਮੀ ਕਰ ਕਲ 1924 ਨਲ ਸਤਵਿਧਾਨ ਦੀ ਰਚਨਾ ਜਰੂਰੀ ਕਰ ਦਿਤਹਤੀ ਸੀ ਩ਸਲ ਤਰਹਹਾਥ 
ਬਦਲ-~ਦੀਨਥ ਹਸ਼ਪਨਥ ਹਾਲਤਾਥ ਨਲ ਸਤਨ 1924 ਦਲ ਸਤਵਿਧਾਨ ਵਿਚ ਤਬਦੀਲੀ ਲਿਨਣਾ 
ਜਰੂਰੀ ਕਰ ਦਿਤਹਤਾ ਩ਸ ਲਪ ਩ਸ ਕਾਰਨ ਤਸ਼ਥ 1936 ਵਿਚ ਨਵਾਥ ਸਤਵਿਧਾਨ ਬਣਾ਩ਨ 
ਗਿਨ ਾ </p>

<p>     7 ਫਰਵਰੀ 1935 ਨੂਥ ਸਸ਼ਵੀਧਤ ਸਮਾਜਵਾਦੀ ਗਣ-ਰਾਜ ਸਤਘ ਦੀ ਕਲਥਦਰੀ ਕਾਰਜ- 
ਕਾਰੀ ਕਮਲਟੀ ਨਲ ਩ਕ ਸਤਵਿਧਾਨ ਕਮਿਸ਼ਨ ਬਣਾ਩ਨ ਜਿਸ ਵਿਚ 31 ਮਲ਼ਥਬਰ ਸਨ ਧਤਲ 
ਸਟਾਲਿਨ ਩ਸ ਦਲ ਸਭਾਪਤੀ ਸਨ ਾ ਩ਸ ਸਤਵਿਧਾਨ ਕਮਿਸ਼ਨ ਦੁਨਰਾ ਬਣਾਯ ਸਤਵਿਧਾਨ 
ਦਲ ਖਰਡਲ ਨੂਤ ਕਲਥਦਰੀ ਕਾਰਜਕਾਰੀ ਕਮਲਟੀ ਦੀ ਪਹਰਜੀਡੀਧਮ ਨਲ ਪਹਰਵਾਨ ਕੀਤਾ ਧਤਲ ਲਸ਼ਕ- 
ਰਾਯ ਜਾਨਣ ਲਪ ਩ਸ ਨੂਤ ਪਹਰਕਾਸ਼ਿਤ ਕਰਵਾ਩ਨ ਗਿਨ ਾ ਜਨਤਾ ਵਿਚ ਬਹੁਤ ਸਾਰੀਨਥ 
ਸਭਾਵਾਥ ਹਸ਼ਪਨਥ ਡਲਢ ਲਖਹਖ ਦਲ ਲਗਪਗਹਗ ਸਸ਼ਧਾਥ ਪਲਸ਼ ਕੀਤੀਨਥ ਗਪਨਥ, ਪਰ ਫਨਹਹਾਥ ਵਿਚਸ਼ਥ 
ਕਲਵਲ 43 ਸਸ਼ਧਾਥ ਪਹਰਵਾਨ ਹਸ਼ਪਨਥ ਾ ਧਤਤ ਵਿਚ ਧਠਵੀਥ ਧਖਲ ਸਤਘੀ ਸਸ਼ਵੀਧਤ ਦੀ 
ਕਾਥਗਰਸ ਦੁਨਰਾ ਸਟਾਲਨ ਸਤਵਿਧਾਨ ਨੂਤ 5 ਦਸਤਬਰ 1936 ਨੂਤ ਪਹਰਵਾਨ ਕਰ ਲਿਨ 
ਗਿਨ ਾ ਕਿਫਥਕਿ ਩ਸ ਸਤਵਿਧਾਨ ਦਾ ਸਰੂਪ ਤਿਨਰ ਕਰਨ ਵਾਲਲ ਕਮਿਸ਼ਨ ਦਲ ਸਭਾਪਤੀ 
ਸਟਾਲਨ ਸਨ ਩ਸ ਲਪ ਩ਸ ਸਤਵਿਧਾਨ ਨੂਤ 1936 ਦਾ ਜਾਥ ਸਟਾਲਨ ਸਤਵਿਧਾਨ ਨਖਿਨ 
ਜਾਥਦਾ ਹਲ਼ ਾ 1936 ਤਸ਼ਥ ਹਾਲਲ ਤਕ ਩ਸ ਸਤਵਿਧਾਨ ਵਿਚ ਕਪ ਵਾਰ ਸਸ਼ਧਾਥ ਹਸ਼ ਚੁਕੀਨਥ 
ਹਨ ਧਤਲ ਩ਹੀ ਸਤਵਿਧਾਨ ਰੂਸ ਦਾ ਚਾਲੂ ਸਤਵਿਧਾਨ ਹਲ਼ ਾ </p>

<p>     1936 ਦਾ ਸਤਵਿਧਾਨ ਨਪਣਲ ਸਰੂਪ ਵਿਚ ਬਹੁਤ ਕੁਝ 1924 ਦਲ ਸਤਵਿਧਾਨ ਨਾਲ 
ਮਿਲਦਾ ਜੁਲਦਾ ਹਲ਼ ਾ 1924 ਦਲ ਸਤਵਿਧਾਨ ਵਾਥਗ ਩ਹ ਵੀ ਸਮਾਜਵਾਦੀ ਨਦਰਸ਼ਾਥ ਤਲ 
ਤੁਰਨ ਨੂਤ ਤਿਨਰ ਹਲ਼ ਧਤਲ ਩ਸ ਦਲ ਦੁਨਰਾ ਹੀ ਸਰਵਹਾਰਾ ਤਾਨਾਸ਼ਾਹੀ ਨੂਤ ਕਾ਩ਮ ਕਰਨ 
ਦਾ ਯਤਨ ਕੀਤਾ ਗਿਨ ਹਲ਼ ਾ ਩ਸ ਗਲ ਤਸ਼ਥ ਕਸ਼ਪ ਵੀ ਨਾਥਹ ਨਹੀਥ ਕਰ ਸਕਦਾ ਕਿ 1936 
ਦਾ ਸਤਵਿਧਾਨ ਸਥਸਾਰ ਦਲ ਹਸ਼ਰ ਸਤਵਿਧਾਨਾਥ ਦਲ ਟਾਕਰਲ ਵਿਚ ਩ਕ ਵਖਹਖਰੀ ਧਤਲ ਨਵੀਥ ਪਹਰਣਾਲੀ 
ਦਾ ਸਤਵਿਧਾਨ ਹਲ਼ ਾ ਩ਸ ਸਤਵਿਧਾਨ ਦਲ ਧਧੀਨ ਜਿਹਡੀ ਨਵੀਥ ਪਹਰਣਾਲੀ ਦਾ ਸਤਚਾਰ ਹਸ਼਩ਨ 
਩ਸ ਦਲ ਫਲਸਰੂਪ ਸਸ਼ਵੀਧਤ ਰੂਸ ਨਲ ਥਸ਼ਡਹਹਲ ਚਿਰ ਵਿਚ ਮਹਾਨ ਤਰਕਹਕੀ ਕੀਤੀ ਹਲ਼ ਾ ਧਜਹਜ 
ਸਸ਼ਵੀਧਤ ਸਤਘ ਸਤਸਾਰ ਦੀਨਥ ਸ਼ਕਤੀਨਥ ਵਿਚਸ਼ਥ ਩ਕ ਹਲ਼ ਧਤਲ ਩ਕ ਵਿਸ਼ਵ-ਸ਼ਕਤੀ ਬਲਾਕ ਦਾ 
ਨਲਤਾ ਵੀ ਬਣ ਗਿਨ ਹਲ਼ ਾ ਸਤਸਾਰ ਰਾਜਨੀਤੀ ਵਿਚ ਩ਹ ਩ਕ ਸ਼ਲਾਘਾ ਯਸ਼ਗ ਭੂਮਿਕਾ ਨਿਭਾ 
ਰਿਹਾ ਹਲ਼ ਾ </p>

<p>     ਸਟਾਲਨ ਸਤਵਿਧਾਨ ਵੀ ਨਲਸ਼ਚਨਾਵਾਥ ਤਸ਼ਥ ਬਚਿਨ ਨਹੀਥ ਰਹਿ ਸਕਿਨ ਾ ਕੁਝ 
ਪਛਹਛਮੀ ਲਲਖਕਾਥ ਨਲ ਩ਸ ਨੂਤ ਲਸ਼ਕ-ਤਤਤਰ ਦਲ ਸਿਧਾਥਤਾਥ ਦਾ ਭਿਨਨਕ ਮਖ਷ਲ ਤਲ ਰਾਜਨੀਤਕ 
ਛਲ-ਕਪਟ ਦੀ ਜੀਫਥਦੀ ਜਾਗਦੀ ਫਦਾਹਰਣ ਦਸੀ ਹਲ਼ ਾ ਕੁਝ ਰਾਜਨੀਤੀ ਸ਼ਾਸਤਰੀ ਨਖਦਲ 
ਹਨ ਕਿ ਸਟਾਲਨ ਨਲ ਲਸ਼ਕ-ਤਤਤਰੀ ਨਜਾਦੀਨਥ ਦਾ ਖਾਤਮਾ ਕਰ ਕਲ ਩ਕ ਕਠਸ਼ਰ ਤਾਨਾਸ਼ਾਹੀ 
ਦੀ ਸਥਾਪਨਾ ਕੀਤੀ ਹਲ਼ ਾ ਸਟਾਲਨ ਦਲ ਯੁਗਹਗ ਨੂਤ ਕੁਝ ਦਾਰਸ਼ਨਿਕ ਵਿਗਡਿਨ ਩ਕ ਰਾਸ਼ਟਰੀ 
ਸ਼ਾਸਨ ਨਖਦਲ ਹਨ, ਪਰ ਩ਨਹਹਾਥ ਨਲਸ਼ਚਕਾਥ ਨਲ ਕਲਵਲ ਸਟਾਸਨ ਦਾ ਕਠਸ਼ਰ ਰੂਪ ਹੀ 
ਦਲਖਿਨ ਹਲ਼ ਾ ਫਨਹਹਾਥ ਨਲ ਸਟਾਲਨ ਦੁਨਰਾ ਰਾਜਨੀਤਕ ਵਿਰਸ਼ਧ ਦਲ ਕਾਰਨ ਨਾਗਰਿਕਾਥ ਦਲ 
ਦਿਤਹਤਲ ਗਯ ਮ਷ਤ ਦਥਡ ਧਤਲ ਦਲਸ਼ ਨਿਕਾਲਲ ਹੀ ਦਲਖਲ ਹਨ ਾ ਪਰ ਩ਸ ਦਲ ਨਾਲ ਹੀ ਨਾਲ 
ਧਰਥ-ਵਿਵਸਥਾ ਦਾ ਸਮਾਜੀਕਰਣ, ਪਤਜ-ਸਾਲਾ ਯਸ਼ਜਨਾਵਾਥ ਦੁਨਰਾ ਫਦਯਸ਼ਗਿਕ ਖਲਤਰਾਥ 
ਵਿਚ ਫਨਹਨਤੀ, ਨਧੁਨਿਕ ਸ਼ਸਤਰਾਥ ਨਾਲ ਲਲ਼ਸ, ਸਸ਼ਵੀਧਤ ਫ਷ਜਾਥ ਦਾ ਸਤਗਠਨਕ, ਖਲਤੀ 
ਦਾ ਯਤਤਰੀ-ਕਰਨ ਤਲ ਸਮੂਹਕ ਖਲਤੀ-ਫਰਮਾਥ ਦੀ ਸਥਾਪਨਾ, ਸਿਖਿਨ ਦਾ ਵਿਨਪਕ ਪਹਰਸਾਰ 
ਜਾਤੀ ਭਲਦਭਾਵਾਥ ਦਾ ਨਿਰਾਕਰਣ, ਧਰਮ ਨਿਰਪਖਹਖ ਰਾਜਨੀਤੀ, ਬਲਕਾਰ ਨਾਗਰਿਕਾਥ ਨੂਤ 
ਨਰਥਕ ਮਦਦ, ਕਿਰਤੀ ਵਰਗ ਸਮਾਜਕ ਸਨਮਾਨ, ਸ਼ਸ਼ਸ਼ਕ (ਲਸ਼ਟੂ) ਵਰਗਾਥ ਦੀ 
ਸਮਪਤੀ ਨਦਿ ਮਹਤਹਤਵ ਪੂਰਨ ਤਬਦੀਲੀਨਥ ਵੀ ਤਾਥ ਸਟਾਲਨ ਦੀ ਧਗਵਾਪ ਵਿਚ 
ਹਸ਼ਪਨਥ ਸਨ ਾ </p>

<p>
     ਸਸ਼ਵੀਧਤ ਸਮਾਜਵਾਦੀ ਗਣਰਾਜ ਸਤਘ ਦਲ ਸਤਵਿਧਾਨ ਨੂਤ ਩ਕ ਚਿਤਾਵਨੀਪੂਰਨ 
ਮਸ਷ਦਾ ਨਖਿਨ ਗਿਨ ਹਲ਼ ਾ ਕਿਫਥਕਿ ਸਤਸਾਰ ਦੀ ਰਾਜਨੀਤਕ ਤਲ ਨਰਥਕ ਸਤਸਥਾਵਾਥ 
ਦਲ ਩ਤਿਹਾਸ ਵਿਚ ਩ਹ ਩ਕ ਲਾਸਾਨੀ ਪਹਰਯਸ਼ਗ ਹਲ਼ ਾ ਩ਸ ਸਤਵਿਧਾਨ ਦੀਨਥ ਪਹਰਮੁਖਹਖ 
ਵਿਸ਼ਲਸ਼ਤਾਵਾਥ ਹਲਠ ਲਿਖੀਨਥ ਹਨ :- </p>

<p>     ਸਸ਼ਵੀਧਤ ਸਤਵਿਧਾਨ ਸਤਸਾਰ ਦਾ ਪਹਿਲਾ ਸਮਾਜਵਾਦੀ ਸਤਵਿਧਾਨ ਹਲ਼ ਾ ਸਤਵਿਧਾਨ ਦਲ 
ਧਨੂਛਲਦ ਵਿਚ ਩ਹ ਸਪਸ਼ਟ ਕੀਤਾ ਗਿਨ ਹਲ਼ ਕਿ ``ਸਸ਼ਵੀਧਤ ਸਤਘ ਮਜਦੂਰਾਥ ਧਤਲ ਕਿਸਾਨਾਥ 
ਦਾ ਩ਕ ਸਮਾਜਵਾਦੀ ਰਾਜ ਹਲ਼ ਾ'' ਧਤਲ ਸਤਵਿਧਾਨ ਵਿਚ ਸਮਾਜਵਾਦੀ ਸਿਧਾਥਤਾਥ ਦਾ ਵਰਣ਩ 
ਕਰਕਲ ਫਨਹਹਾਥ ਨੂਤ ਵਿਚਾਰ ਵਿਚ ਲਿਨਣ ਲਪ ਫਚਿਤ ਕਾਨੂਤਨਾਥ ਦੀ ਵਿਵਸਥਾ ਕੀਤੀ ਗਪ ਹਲ਼ ਾ </p>

<p>     ਸਸ਼ਵੀਧਤ ਗਣਰਾਜ ਦਾ ਸਤਵਿਧਾਨ ਩ਕ ਸਤਵਿਧਾਨ ਕਮਿਸ਼ਨ ਦੁਨਰਾ ਲਿਖਿਨ ਗਿਨ 
ਸੀ ਾ ਩ਸ ਕਮਿਸ਼ਨ ਦੀ ਪਹਰਧਾਨਗੀ ਸਟਾਲਨ ਨਲ ਕੀਤੀ ਸੀ ਩ਸ ਲਪ ਩ਸ ਸਤਵਿਧਾਨ ਨੂਤ 
ਸਟਾਲਨ ਸਤਵਿਧਾਨ ਵੀ ਨਖਿਨ ਜਾਥਦਾ ਹਲ਼ ਾ ਸਤਵਿਧਾਨ ਕਮਿਸ਼ਨ ਦੁਨਰਾ ਪਲਸ਼ ਕੀਤਲ ਸਰੂਪ 
ਨੂਤ ਲਸ਼ਕਮਤ ਸਤਗਹਰਹਿ ਲਪ ਪਹਰਕਾਸ਼ਤ ਕੀਤਾ ਗਿਨ ਾ ਲਗਪਗਹਗ ਡਲਢ ਲਖਹਖ ਸਸ਼ਧਾਥ ਸੁਝਾਪਨਥ 
ਗਪਨਥ ਜਿਸ ਵਿਚਸ਼ਥ ਕਲਵਲ 43 ਨੂਤ ਮਾਣਤਾ ਦਿਤੀ ਗਪ, 15 ਦਸਤਬਰ 1936 ਨੂਤ ਧਠਵੀਥ 
ਧਖਲ ਸਤਘੀ ਸਸ਼ਵੀਧਤਾਥ ਦੀ ਕਾਥਗਰਸ ਨਲ ਩ਸ ਨੂਤ ਪਹਰਵਾਨ ਕਰ ਲਿਨ 1936 ਦਲ ਩ਸ 
ਸਤਵਿਧਾਨ ਵਿਚ 146 ਧਨੁਛਲਦ ਹਨ ਧਤਲ ਩ਹ 13 ਧਧਿਨਵਾਥ ਵਿਚ ਵਤਡਿਨ ਹਸ਼਩ਨ ਹਲ਼ ਾ </p>

<p>     ਧਧਿਨ਩ 1. ਸਮਾਜਕ ਸਤਗਠਨ ਧਧਿਨ਩ 2. ਸਤਘ ਪਹਰਣਾਲੀ ਧਧਿਨ਩ 3. ਸਤਘ 
ਦਲ ਸ਼ਾਸਨ ਦਲ ਫਚਹਚ ਧਤਗ ਧਧਿਨ਩ 4. ਗਣਰਾਜਾਥ ਵਿਚ ਰਾਜ ਸ਼ਕਤੀ ਧਧਿਨ਩ 5 ਤਲ 
6. ਸਤਘ ਤਲ ਸਾਲਸੀ ਰਾਜ ਦਲ ਪਹਰਬਤਧਕੀ ਯਤਤਰ ਧਧਿਨ਩ 7, 8 ਤਲ ਸਥਾਨਕ ਸ਼ਾਸਨ ਤਲ 
ਨਿਨਥਪਾਲਕਾ, ਧਧਿਨ਩ 10 ਨਾਗਰਿਕਾਥ ਦਲ ਬੂਨਿਨਦੀ ਧਧਿਕਾਰਾਥ ਤਲ ਕਰਤਵਹਵ ਧਧਿਨ਩ 
11. ਨਾਮਜਦ ਕਰਨ ਤਲ ਚੁਣਨ ਧਧਿਨ਩ 12. ਸਤਘ ਦਲ ਝਤਡਲ, ਚਿਤਨਹਹ ਧਤਲ ਰਾਜਧਾਨੀ 
ਧਧਿਨ਩ 13. ਸਤਵਿਧਾਨ ਦੀ ਸਸ਼ਧ ਵਿਧੀ ਦਾ ਵਰਣਨ ਕਰਦਾ ਹਲ਼ ਾ </p>

<p>     ਸਸ਼ਵੀਧਤ ਸਤਵਿਧਾਨ ਟਾਕਰਲ ਵਿਚ ਨਵਾਥ ਤਲ ਩ਕ ਧਨਸ਼ਖਲ ਸਰੂਪ ਦਾ ਸਤਵਿਧਾਨਕ ਹਲ਼ ਾ 
ਵਿਸ਼ਿਤਸਕੀ ਤਾਥ ਸਸ਼ਵੀਧਤ ਰੂਸ ਨੂਤ ਨਵੀਥ ਕਿਸਮ ਦਾ ਰਾਜ ਨਖਦਲ ਹਨ ਾ ਨਵੀਨਤਾ ਹਸ਼ਣ 
ਤਲ ਵੀ ਩ਹ ਸਤਵਿਧਾਨ ਧਤਲ ਦਲਸ਼ ਦੀ ਪਹਰਣਾਲੀ ਕੁਝ ਨਿਸ਼ਚਿਤ ਸਿਧਾਥਤਾਥ ਤਲ ਨਧਾਰਤ ਹਨ ਾ 
ਸਟਾਲਨ ਨਲ ਸਤਵਿਧਾਨ ਦੁਨਰਾ ਮਾਰਕਸਵਾਦ ਤਲ ਲਲ਼ਨਿਨਵਾਦ ਦਲ ਸਿਧਾਥਤਾਥ ਨੂਤ ਧਮਲ 
ਵਿਚ ਲਿਨਣ ਦਾ ਯਤਨ ਕੀਤਾ ਹਲ਼ ਾ </p>

<p>     ਮਾਰਕਸਵਾਦ ਲਲ਼ਨਿਨਵਾਦ ਦੀ ਪਹਰਦਾਰਥਕ ਦਲਣ ਹਲ਼ ਾ ਵਿਰਸ਼ਧਸ਼ਤਮਕ ਪਦਾਰਥਵਾਦ 
਩ਸ ਸਿਧਾਥਤ ਦਲ ਦੁਨਰਾ ਩ਤਿਹਾਸ ਦੀ ਨਰਥਕ ਵਿਨਖਿਨ ਕੀਤੀ ਜਾਥਦੀ ਹਲ਼ ਾ ਩ਸ ਸਿਧਾਥਤ 
ਦਲ ਧਨੁਸਾਰ ਕਿਸਲ ਵਲਲਲ ਦਲ ਰਾਜਨੀਤਕ, ਧਾਰਮਕ ਧਤਲ ਸਮਾਜਕ ਵਿਚਾਰ ਧਤਲ ਸਾਰੀਨਥ 
ਸਤਸਥਾਵਾਥ ਫਸ ਵਲਲਲ ਦੀਨਥ ਨ਱ਥਕ ਹਾਲਤਾਥ ਦਲ ਸਿਟਹਟਲ ਹੁਤਦਲ ਹਨ ਾ ਕਿਸਲ ਵੀ ਵਲਲਲ ਦੀ 
ਸਮਾਜਕ ਤਲ ਸ਼ਿਖਸ਼ਣਕ ਪਹਰਣਾਲੀਨਥ ਕਿਸਲ ਧਮੂਰਤ ਵਿਚਾਰਾਥ ਦਾ ਸਿਟਹਟਾ ਨਾ ਹਸ਼ ਕਲ ਵਿਵਸਥਾ 
ਦਾ ਹੀ ਪਹਰਤਖਹਖ ਸਿਟਹਟਾ ਹੁਤਦੀਨਥ ਹਨ ਾ ਦੂਜਲ ਸ਼ਬਦਾਥ ਵਿਚ ਵਿਰਸ਼ਧਾਤਮਕ ਪਦਾਰਥਵਾਦ ਦਾ 
ਭਾਵ ਩ਹ ਹਲ਼ ਕਿ ਸਮਾਜ ਦਾ ਨਰਥਕ ਢਾਥਚਾ ਧਰਥਾਤ ਜਨਤਾ ਦਲ ਧਨ ਦਲ ਫਤਪਾਦਨ ਦੀ 
ਵਿਧੀ ਹੀ ਨਿਰਣਾ਩ਕ ਹੁਤਦਲ ਹਨ ਾ ਸਸ਼ਵੀਧਤ ਰੂਸ ਦਾ ਩ਤਿਹਾਸ ਵੀ ਩ਸ ਗਲਹਲ ਨੂਤ ਪਹਰਮਾਣਤ 
ਕਰਦਾ ਹਲ਼ ਾ </p>

<p>     ਩ਸਲ ਩ਤਿਹਾਸ ਦੀ ਨਰਥਕ ਵਿਨਖਿਨ ਦਲ ਸਿਧਾਥਤ ਵਿਚ ਵਰਗ ਘਸ਼ਲ ਸਿਧਾਥਤ 
ਲੁਕਿਨ ਹਲ਼ ਾ ਩ਸ ਵਰਗ ਦਲ ਘਸ਼ਲ ਤਸ਼ਥ ਭਾਵ ਩ਸ ਚੀਜ ਤਸ਼ਥ ਹਲ਼ ਕਿ ਸਮੁਚਾ ਩ਤਿ-~ਹਾਸ 
ਦਲ ਵਰਗਾਥ ਦਲ ਲਗਾਤਾਰ ਸਤਘਰਸ਼ਾਥ ਦਾ ਸਿਟਹਟਾ ਹਲ਼ - ਩ਹ ਦਸ਼ਵਲਥ ਵਰਗ ਹਨ ਸ਼ਸ਼ਸ਼ਕ ਵਰਗ 
ਸਸ਼ਖਤ ਵਰਗ ਾ ਪੂਤਜੀਵਾਦੀ ਪਹਰਣਾਲੀ ਵਿਚ ਪੂਤਜੀਪਤੀ ਸ਼ਸ਼ਸ਼ਕ ਵਰਗ ਨਾਲ ਸਤਬਧਤ ਹੁਤਦਲ 
ਹਨ ਤਲ ਨਿਰ-ਸਮਤਾ ਨਾਲ ਸਸ਼ਖਤ ਵਰਗ ਦਾ ਸ਼ਸ਼ਸ਼ਨ (ਲੁਟ-ਖਸੁਟ) ਕਰਦਲ ਹਨ ਾ 
ਫਲਸਰੂਪ ਕਿਰਤੀ ਜਸ਼ ਕਿ ਸਸ਼ਖਤ ਵਰਗ ਹੁਤਦਾ ਹਲ਼, ਪੂਤਜੀਪਤੀ ਵਰਗ ਦਲ ਵਿਰੁਧ ਜਦਸ਼-ਜਹਿਦ 
ਕਰਦਾ ਹਲ਼ ਧਤਲ ਩ਸ ਤਲ ਯਥਜਲ ਦਲ ਧਨੁਸਾਰ ਜਿਤਹਤ ਸਦਾ ਕਿਰਤੀ ਵਰਗ ਦੀ ਹਸ਼ਵਲਗੀ ਾ 
ਜਦ ਜਦਸ਼-ਜਹਿਦ ਦੀ ਸਮਾਪਤੀ ਹਸ਼ ਜਾਵਲਗੀ ਤਾਥ ਫਲਰ ਰਾਜ ਦੀ ਵੀ ਲਸ਼ਡ ਨਾ ਰਹਲਗੀ ਾ 
਩ਸ ਦੀ 1917 ਦੀ ਩ਸ ਵਰਗ ਘਸ਼ਲ ਦੀ ਜੀਫਥਦੀ ਜਾਗਦੀ ਫਦਾਹਰਣ ਹਲ਼ ਾ </p>

<p>     ਰਾਜ ਦੀ ਲਸ਼ਡ ਸਰਵਹਾਰਾ ਦੀ ਤਾਨਾਸ਼ਾਹੀ ਦਲ ਧਧੀਨ ਜਰੂਰ ਧਨੁਭਵ ਹੁਤਦੀ ਹਲ਼ 
ਕਿਫਥਕਿ ਰਾਜ ਦਲ ਦੁਨਰਾ ਹੀ ਸਰਵਹਾਰਾ ਵਰਗ ਦਲ ਰਾਜ ਦਾ ਸਥਰਕਸ਼ਣ (ਸਰਪਰਸਤੀ) 
ਹਸ਼ ਸਕਦਾ ਹਲ਼ ਾ ਩ਸ ਦਲ ਤਹਿਤ ਕਿਰਤੀ ਵਰਗ ਦਲ ਹਥਹਥ ਸਮੁਚੀਨਥ ਸ਼ਕਤੀਨਥ ਩ਕਠਹਠੀਨਥ ਹਸ਼ 
ਜਾਥਦੀਨਥ ਹਨ ਾ ਪਰ ਤਾਨਾਸ਼ਾਹੀ ਸਾਮਵਾਦੀ ਦਲ ਦੀ ਩ਕਾਪ ਵਿਚ ਫਸ ਸਤਘਰਸ਼ ਜਾਥ 
ਕਹਰਾਥਤੀ ਦਾ ਸਿਟਹਟਾ ਹਲ਼ ਜਸ਼ ਕਿ ਸ਼ਸ਼ਸ਼ਨ ਵਰਗ ਤਲ ਸ਼ਸ਼ਸ਼ਕ ਵਰਗ ਦਲ ਵਿਚ ਚਲ ਰਿਹਾ ਹਲ਼ ਾ ਲਲਨਿਨ 
ਦਲ ਵਿਚਾਰ ਧਨੁਸਾਰ ਸਰਵਹਾਰਾ ਵਰਗ ਩ਕ ਧਜਿਹਲ ਰਾਜ ਦੀ ਸਥਾਪਨਾ ਕਰਦਾ ਹਲ਼ ਜਿਸ 
ਵਿਚ ਸਮੁਚੀ ਵਿਵਸਥਾ ਦਾ ਕਲਥਦਰ ਕਿਰਤੀ ਵਰਗ ਦਲ ਕਬਜਲ ਵਿਚ ਹਲ਼ ਾ ਩ਸ ਗਲਹਲ ਦਾ 
਩ਹ ਭਾਵ ਨਹੀਥ ਕਿ ਸਰਵਹਾਰਾ ਦੀ ਤਾਨਾਸ਼ਾਹੀ ਵਿਚ ਲਸ਼ਕਤਤਤਰ ਦੀ ਸਥਾਪਨਾ ਨਹੀਥ ਹਸ਼ 
ਸਕਦੀ ਕਿਫਥਕਿ ਸਸ਼ਵੀਧਤ ਰੂਸ ਵਿਚ ਩ਸ ਤਾਨਾਸ਼ਾਹੀ ਦਲ ਨਾਲ ਹੀ ਲਸ਼ਕਚਤਤਰ ਦੀ ਵੀ 
ਸਥਾਪਨਾ ਕੀਤੀ ਗਪ ਹਲ਼ ਾ ਲਲਨਿਨ ਦਲ ਵਿਚਾਰ ਵਿਚ ਸਸ਼ਵੀਧਕਤ ਰੂਸ ਸਰਵਹਾਰਾ ਵਰਗ ਦੀ 
ਤਾਨਾਸ਼ਾਹੀ ਵਿਚ ਪਹਿਲੀ ਵਾਰ ਲਸ਼ਕਤਤਤਰ, ਧਮੀਰਾਥ ਦਾ ਲਸ਼ਕਤਤਤਰ ਹਸ਼ਣ ਕਰ ਕਲ ਸਰਵ 
ਸਾਧਾਰਣ ਦਾ ਲਸ਼ਕਤਤਤਰ ਬਣਿਨ ਹਲ਼ ਾ </p>

<p>     1918 ਤਲ 1924 ਦਲ ਸਤਵਿਧਾਨ ਵਾਥਗ 1936 ਦਾ ਸਤਵਿਧਾਨ ਵੀ ਫਤਪਾਦਨ ਦਲ 
ਪਦਾਰਥਕ ਸਾਧਨਾਥ ਤਲ ਨਿਜਹਜੀ ਮਲਕੀ ਨੂਤ ਖਤਮ ਕਰਕਲ ਩ਸ ਸਮਾਜਵਾਦੀ ਰਾਜ ਕਾ਩ਮ 
ਕਰਨ ਦਾ ੟ਲਾਨ ਕਰਦਾ ਹਲ਼ ਾ ਸਤਵਿਧਾਨ ਦਲ ਪਹਿਲਲ ਧਨੁਛਲਦ ਦਲ ਧਨੁਸਾਰ ਸਸ਼ਵੀਧਤ 
ਗਣਰਾਜ ਸਤਘ ਕਿਰਤੀਨਥ ਤਲ ਕਿਸਾਨਾਥ ਦਾ ਸਮਾਜਵਾਦੀ          ਕੀਤਾ ਗਿਨ ਹਲ਼ ਾ 
਩ਸਦਲ ਧਨੁਸਾਰ ਦਲਸ਼ ਦਲ ਫਤਪਾਦਨ ਦਲ ਸਮੁਚਲ ਸਾਧਨਾਥ ਦਾ ਸਮਾਜੀਕਰਣ ਕਰ 
ਦਿਤਹਤਾ ਗਿਨ ਹਲ਼ ਧਤਲ ਦਲਸ਼ ਦਾ ਸਤਪੂਰਨ ਨਰਥਕ ਜੀਵਨ ਩ਕ ਰਾਸ਼ਟਰੀ ਜੀਵਨ ਦਲ 
ਧਨੁਸਾਰ ਦਿਖਾ਩ਨ ਜਾਥਦਾ ਹਲ਼ ਾ ਸਮੁਚੀ ਸਤਪਤੀ ਦਾ ਵੀ ਸਮਾਜੀਕਰਣ ਕਰ ਦਿਤਹਤਾ 
ਗਿਨ ਹਲ਼ ਾ ਫਲਸ਼ਰਿਸਕੀ ਦਾ ਵਿਚਾਰ ਹਲ਼, ``ਸਟਾਲਨ ਸਤਵਿਧਾਨ ਦਲ ਦੁਨਰਾ ਰੂਸ ਵਿਚ 
ਫਤਪਾ-~ਦਨ ਦਲ ਸਾਧਨਾਥ ਤਲ ਨਿਜਹਜੀ ਮਾਲਕੀ ਖਤਮ ਹਸ਼ ਗਪ ਹਲ਼ ਧਤਲ ਩ਸ ਨਧਾਰ 
ਤਲ ਩ਹ ਨਖਿਨ ਜਾ ਸਕਦਾ ਹਲ਼ ਕਿ ਸਸ਼ਵੀਧਤ ਸਤਘ ਩ਕ ਸਮਾਜਵਾਦੀ ਰਾਜ ਹਲ਼,'' </p>

<p>     ਧਨੁਛਲਦ 12 ਦਲ ਧਨੁਸਾਰ ਕਤਮ ਕਰਨਾ ਹਰਲਕ ਧਰਸ਼ਗ ਨਾਗਰਿਕ ਲਪ ਩ਕ 
ਮਾਨ ਦਾ ਵਿਸ਼ਾ ਧਤਲ ਕਰਤਵਹਵ ਹਲ਼ ਾ ਸਮਾਜਵਾਦ ਵਿਚ ਧਰਸ਼ਗ ਨਾਗਰਿਕ ਲਪ ਨਪਣੀ 
ਰਸ਼ਜੀ ਲਪ ਕਤਮ ਕਰਨਾ ਜਰੂਰੀ ਹਲ਼ ਾ ਩ਸ ਦਾ ਬੁਨਿਨਦੀ ਸਿਧਾਥਤ ਹਲ਼ ``ਜਸ਼ ਕਤਮ ਨਹੀਥ 
ਕਰਲਗਾ ਫਸ ਨੂਤ ਖਾਨਾ ਨਹੀਥ ਮਿਲਲਗਾ'' </p>

<p>     (i) ਸਸ਼ਵੀਧਤ ਦਲਸ਼ ਵਿਚ ਸਮਾਜਵਾਦੀ ਸਤਪਤੀ ਜਿਸ ਦਲ ਫਪਰ ਸਮੁਚਲ ਰਾਜ 
ਦਾ ਕਬਜਾ ਹਲ਼ ਾ (ii) ਸਹਿਕਾਰੀ ਸਤਪਤੀ, ਜਿਹਡੀ ਸਰਕਾਰੀ ਸਭਾ ਦਲ ਮਲ਼ਥਬਰਾਥ ਦਲ 
ਧਧੀਨ ਹੁਤਦੀ ਹਲ਼ ਾ (iii) ਸਾਮੂਹਿਕ ਖਲਤੀ-ਖਲਤਰ ਜਿਹਡਾ ਸਥਾਨਕ ਕਿਸਾਨਾ ਦੀ 
ਸਾਥਝੀ ਸਤਪਤੀ ਹਲ਼ ਾ ਖਣਿਜ ਪਦਾਰਥ ਜਲ, ਰਲਲ ਰਸਤਲ, ਬਲ਼ਥਕ ਨਦਿ ਰਾਜ ਦੀ ਸਤਪਤੀ 
ਦਲ ਧਧੀਨ ਨਫਥਦਲ ਹਨ ਾ ਸਮਾਜਵਾਦ ਧਰਥ-ਵਿਵਸਥਾ ਵਿਚ ਕਸ਼ਪ ਵਿਧਕਤੀ ਕਿਸਲ 
ਦੂਜਲ ਵਿਧਕਤੀ ਦੀ ਕਿਰਤ ਨੂਤ ਖਰੀਦ ਕਲ ਫਸ ਦਾ ਨਪੀਡਨ ਨਹੀਥ ਕਰ ਸਕਦਾ ਾ 
ਪੁਸਤਕਾਥ, ਖਾਦ ਪਦਾਰਥਾਥ, ਕਪਡਲ ਰਲਡੀਓ, ਮਲਟਰ ਨਦਿ ਨਿਜਹਜੀ ਸਤਪਤੀ ਦਲ ਰੂਪ ਵਿਚ 
ਪਹਰਵਾਨ ਹਨ ਾ </p>

<p>     ਸਸ਼ਵੀਧਤ ਰਾਜ ਦੀ ਧਰਥ ਵਿਵਸਥਾ ਰਾਸ਼ਟਰੀ-ਨਰਥਕ ਯਸ਼ਜਨਾਵਾਥ ਦਲ ਦੁਨਰਾ 
਩ਕਤਹਤਰ ਹੁਤਦੀ ਹਲ਼ ਾ ਰਾਸ਼ਟਰੀ ਨਰਥਕ ਯਸ਼ਜਨਾ ਦਾ ਫਦਲਸ਼ ਰਾਸ਼ਟਰੀ ਧਨ ਦਾ ਵਾਧਾ 
ਕਰਨਾ, ਜਨਤਾ ਦਲ ਮਾਲੀ ਤਲ ਸਭਿਨਚਾਰਕ ਪਧਹਧਰ ਨੂਤ ਫਚਹਚਾ ਚੁਕਣਾ ਧਤਲ ਸਸ਼ਵੀਧਤ 
ਗਣਤਤਤਰ ਦੀ ਸੁਤਤਤਰਤਾ ਦੀ ਰਖਿਨ ਕਰਨਾ ਹਲ਼ ਾ ਸਸ਼ਵੀਧਤ ਗਣਤਤਤਰ ਵੀ ਹਰਲਕ 
ਵਿਧਕਤੀ ਨੂਤ ਰਸ਼ਜੀ ਦਲਣਾ, ਸਿਖਿਨ ਦਾ ਪਹਰਬਤਧ ਕਰਨਾ ਧਤਲ ਬਲਕਾਰ ਵਿਧਕਤੀਨਥ ਨੂਤ 
ਨਰਥਕ ਮਦਦ ਦਲਣਾ ਰਾਜ ਦਾ ਕਰਤਵਹਵ ਮਤਨਿਨ ਜਾਥਦਾ ਹਲ਼ ਾ </p>

<p>     ਧਨੁਛਲਦ 13 ਸਸ਼ਵੀਧਤ ਸਤਘ ਨੂਤ ਩ਕ ਸਤਘੀ ਸਤਘ ੟ਲਾਨ ਕਰਦਾ ਹਲ਼ ਜਿਸ 
ਦਾ ਨਿਰਮਾਣ ਸਸ਼ਵੀਧਤ ਸਮਾਜਵਾਦੀ ਗਣਰਾਜਾਥ ਦਲ ਩ਛਹਛਕ ਸਤਗਠਨ ਦਲ ਨਧਾਰ ਤਲ 
ਹਸ਼਩ਨ ਹਲ਼ ਾ ਵਿਲਿਧਮ ਦਲ ਧਨੁਸਾਰ ``ਸਸ਼ਵੀਧਤ ਰੂਸੀ ਸਤਘ ਦਲ ਨਲਤਾਵਾਥ ਦਾ ਦਾਵਾ ਹਲ਼ 
ਕਿ ਸਸ਼ਵੀਧਤ ਸਤਘ ਨਵੀਥ ਕਿਸਮ ਦਲ ਸਤਘ ਦੀ ਫਸਾਰੀ ਕਰਦਾ ਹਲ਼ ਜਿਹਡਾ ਪੁਤਜੀਵਾਦੀ 
ਵਿਵਸਥਾ ਦਲ ਸਤਘ ਤਸ਼ਥ ਪੂਰੀ ਤਰਹਹਾਥ ਭਿਤਨ ਹਲ਼ ਾ'' ਸਤਯੁਕਤ ਰਾਜ ਧਮਰੀਕਾ ਧਤਲ 
ਸਵਿਟਜਰਲਲ਼ਥਡ ਜਿਹਲ ਦਲਸ਼ਾਥ ਤਸ਼ਥ ਩ਹ ਩ਸ ਲਪ ਭਿਤਨ ਹਲ਼ ਕਿਫਥਕਿ ਩ਹ ਵਖਹਖ-ਵਖਹਖ 
ਧਤਗੀ ਩ਕਾਪਨਥ ਦਾ ਩ਛਹਛਤਰ ਹਲ਼ ਾ ਮਾਰਕਸ ਯਥਜਲਸ ਧਤਲ ਫਨਹਹਾਥ ਦਲ ਪਿਛਸ਼ਥ ਧਤਲ 
ਸਟਾਲਿਨ ਵੀ ਸਿਧਾਥਤ ਰਾਜ ਦਲ ਸਤਘੀ ਰੂਪ ਦਲ ਵਿ਱ਸ਼ਧੀ ਸਨ ਾ ਲਲਕਿਨ ਦਲ ਧਨੁਸਾਰ 
``ਸਤਘਵਾਦ ਩ਸ ਲਪ ਬਲਲਸ਼ਡੀ ਹਲ਼ ਕਿਫਥਕਿ ਪੂਤਜੀਵਾਦ ਧਤਲ ਸਮਾਜਵਾਦ ਦਸ਼ਹਾਥ ਦਲ 
ਸਥਪੂਰਨ ਵਿਕਾਸ ਲਪ ਵਡਹਡਲ ਤਸ਼ਥ ਵਡਹਡਲ ਧਤਲ਼ ਕਲਥਦਰੀਕਹਰਿਤ ਰਾਜਾਥ ਦੀ ਲਸ਼ਡ ਹਲ਼ ਾ'' </p>

<p>     ਸਸ਼ਵੀਧਤ ਰੂਸ ਦੀਨਥ ਧਤਗੀ ਩ਕਾਪਨਥ ਨੂਤ ਯੁਨੀਧਨ ਰਿਪਬਲਕ ਨਖਿਨ 
ਜਾਥਦਾ ਹਲ਼ ਾ ਩ਨਹਹਾਥ ਦੀ ਗਿਣਤੀ 15 ਹਲ਼ ਾ ਩ਨਹਹਾਥ ਦਲ ਨਾਥ ਹਨ :- ਰੂਸੀ ਸਤਘ, ਯੂਕਲਨ, 
ਬਾ਩ਲਸ਼ ਰੂਸ, ਫਜਬਕਿਸਤਾਨ, ਕਜਾਖਿਸਤਾਨ, ਤਾਜਿਕਸਤਾਨ, ਤੁਰਕਮਨਿਸਤਾਨ, 
ਜਾਰਜਿਨ, ਧਜਬਲ਼ਜਾਨ, ਲਿਨੁਨਨਿਨ, ਲਟਾਵਿਨ, ਨਿਰਗੀਜਿਸਤਾਨ, ਯਸਟਸ਼ਨੀਨ, 
ਮਸ਼ਲ ਾ ਧਰਮਾਨੀਨ, ਩ਨਹਹਾਥ ਸਾਰੀਨਥ ਯੂਨੀਧਨ ਰਿਪਬਲਕਾਥ ਨੂਤ ਬਰਾਬਰ ੟ਲਾਨ 
ਕੀਤਾ ਗਿਨ ਹਲ਼ ਾ </p>

<p>     ਸਤਵਿਧਾਨਿਕ ਰੂਪ ਨਾਲ ਸਤਘੀ ਰਾਜ ਦਲ ਸਾਰਲ ਸਾਧਾਰਨ ਲਛਣ ਸਸ਼ਵੀਧਤ ਸਤਘ 
ਵਿਚ ਹਨ, ਧਮਰੀਕੀ ਸਤਘ ਵਾਥਗ ਩ਹ ਵੀ ਩ਕ ਲਿਖਤੀ ਤਲ ਕਠਸ਼ਰ ਸਤਵਿਧਾਨ ਰਖਿਨ 
ਗਿਨ ਹਲ਼ ਾ ਰੂਸ ਵਿਚ ਧਲਿਖਤੀ ਸਤਘੀ ਸ਼ਾਸਨ ਤਲ ਵਖਹਖ ਵਖਹਖ ਩ਕਾਪਨਥ ਸ਼ਾਸਨ ਦਲ 
ਸ਼ਕਤੀਨਥ ਦੀ ਸਪਸ਼ਟ ਵਥਡ ਹਲ਼ ਾ ਸਤਵਿਧਾਨ ਦਲ 15 ਧਨੁਛਲਦ ਦਲ ਧਨੁਸਾਰ ``ਸਤਘੀ 
ਗਣਰਾਜਾਥ ਦੀ ਪਹਰਭੂਤਾ ਕਲਵਲ 14ਵਲਥ ਧਨੁਛਲਦ ਵਿਚ ਵਰਨਣ ਫਪਬਤਧਾਥ ਤਲ ਤਹਿਤ ਹਲ਼ ਾ 
ਧਰਥਾਤ ਸਤਵਿਧਾਨ ਬਚੀਨਥ ਹਸ਼ਪਨਥ ਸ਼ਕਤੀਨਥ ਗਣਰਾਜਾਥ ਨੂਤ ਦਿਤਦਾ ਹਲ਼ ਾ ਩ਥਸ਼ਥ ਦਾ 
ਵਿਧਾਨ ਮਤਡਲ ਵੀ ਦਸ਼ ਸਦਨੀ ਹਲ਼ ਾ ਩ਥਲ ਫਪਰਲਲ ਸਦਨ ਵਿਚ ਵਖਹਖ ਵਖਹਖ ਰਾਸ਼ਟਰੀ 
਩ਕਾਪਨਥ ਧਤਲ ਹਲਠਲਲ ਸਦਨ ਵਿਚ ਸਾਰਲ ਦਲਸ਼ ਨੂਤ ਜਨਤਾ ਦਲ ਨਧਾਰ ਤਲ ਪਹਰਤੀਨਿਧਤਾ 
ਮਿਲੀ ਹਲ਼ ਾ ਩ਸ ਦਲ ਩ਲਾਵਾ ਸਤਘ ਦੀਨਥ ਩ਕਾਪਨਥ ਦਲ ਨਪਣਲ ਵਖਰਲ ਸਤਵਿਧਾਨ ਧਤਲ 
ਸ਼ਾਸਨ ਪਹਰਣਾਲੀ ਦਲ ਢਤਗ ਹਨ ਧਤਲ ਩ਨਹਹਾਥ ਵਖਹਖ ਵਖਹਖ ਩ਕਾਪਨਥ ਦੀ ਖਲਤਰੀ ਹਲ਼ਲਤ ਵਿਚ 
ਕਸ਼ਪ ਦਖਲ ਨਹੀਥ ਦਿਤਦਾ, ਪਰ ਩ਨਹਹਾਥ ਸਤਘੀ ਲਛਣਾਥ ਦਲ ਨਾਲ ਹੀ ਸਾਰਲ ਸਸ਼ਵੀਧਤ 
ਸਤਘ ਦਲ ਸਤਵਿਧਾਨ ਵਿਚ ਧਪਰ-ਸਤਘੀ ਲਛਣ ਵੀ ਮ਷ਜੂਦ ਹਨ ਧਤਲ ਩ਨਾਥ ਹੀ ਲਛਣਾਥ 
ਦਲ ਕਾਰਨ ਸਸ਼ਵੀਧਤ ਰੂਸ ਦਾ ਸਤਘ ਭਾਰਤ ਧਤਲ ਸਤਯੁਕਤ ਰਾਜ ਧਮਰੀਕਾ ਦਲ ਸਤਘਾਥ ਤਸ਼ਥ 
ਵਖਰਾ ਹਸ਼ ਜਾਥਦਾ ਹਲ਼ ਾ ਩ਹ ਲਛਣ ਹਨ :- </p>

<p>     (ਓ) ਸਸ਼ਵੀਧਤ ਗਣਰਾਜ ਦਾ ਸਤਘ ਸਾਮਾਨ ਸਸ਼ਵੀਧਤ ਸਮਾਜਵਾਦੀ ਗਣਤਤਤਰ 
(਩ਕਾਪਨਥ) ਦਲ ਩ਛਹਛਕ ਮਿਲਣ ਦਲ ਨਧਾਰ ਤਲ ਬਣਿਨ ਹਸ਼਩ਨ ਹਲ਼ ਾ ਸਤਵਿਧਾਨ ਸਤਘ 
ਦੀਨਥ ਸਾਰੀਨਥ ਩ਕਾਪਨਥ ਨੂਤ ਸਮਾਨ ਸਮਝਦਾ ਹਲ਼, ਪਰ ਸਤਘ ਦਾ ਩ਕ ਩ਕਾਪ 
ਸਮਜਵਾਦੀ ਸਤਯੁਕਤ ਸਸ਼ਵੀਧਤ ਗਣਰਾਜ ਸਸ਼ਵੀਧਤ ਸਤਘ ਦੀਨਥ ਩ਕਾਪਨਥ ਰੂਸੀ ਦਾ 
ਪਹਰਮੁਖਹਖ ਭਾਗ ਹਲ਼ ਾ ਩ਸ ਩ਕਾਪ ਦਾ ਖਲਤਰਫਲ ਸਮੁਚਲ ਸਤਘ ਦਲ ਖਲਤਰਫਲ ਦਾ ਤਿਤਨ 
ਚ਷ਥਾਪ ਭਾਗ ਹਲ਼ ਧਤਲ ਸਮੁਚੀ ਜਨਤਾ ਦਾ ਧਧਹਧਾ ਭਾਗ ਩ਸਲ ਗਣਰਾਜ ਵੀ ਰਹਿਤਦਾ 
ਹਲ਼ ਾ ਩ਸ ਦਲ ਮਗਰਸ਼ਥ ਵੀ ਸਾਰਲ ਗਣਰਾਜਾਥ ਨੂਤ ਸਮਾਨ ਸਮਝਣਾ ਨਪਣਲ ਨਪ ਨੂਥ 
ਭੁਲਲਖਾ ਦਲਣਾ ਨਹੀਥ ਤਾਥ ਹਸ਼ਰ ਕੀ ਹਲ਼ ਾ </p>

<p>+&gt;
               0 
</p></body></text></cesDoc>