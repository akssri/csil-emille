<cesDoc id="pun-w-translat-literature-lot17cq" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-translat-literature-lot17cq.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Yugan Di Dhumdh Ate Asti Agg</h.title>
<h.author>Zibran,Khaleel</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Lahore Book Shop-Ghati Diyan Pariyan - Ludhiana</publisher>
<pubDate>1980</pubDate>
</imprint>
<idno type="CIIL code">lot17cq</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 30-27.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-23</date></creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;2470&gt;&lt;Zibran,Khaled-Yugan Di Dhundh&gt;&lt;Sanjeev Gupta&gt;</p>

<p>          ਅਣਹੋਂਦ ਜਾਂ ਷ਾਇਦ ਕਿਸੇ ਹੋਂਦ ਤੋਂ ਹੋਣ ਵਾਲਾ ਇਹ ਨਵਾਂ ਜਿਹਾ
ਅਨੁਭਵ ਸੀ। ਉਨਾ ਚਿਰ ਇਹ ਵਧਦਾ ਜ੍ਞਾ ਤੇ ਵਿਕਾਸ ਕਰਦਾ ਜ੍ਞਾ ਜਿਂਨਾ
ਚਿਰ ਇਸ ਨੇ ਉਸ ਦੀ ਸਾਰੀ ਅਧਿਆਤਮਕ ਹਸਤੀ ਨੂਂ ਆਪਣੇ ਕਲਾਵੇ ਵਿਚ
ਨਹੀਂ ਲੈ ਜ੍ਞਾ। ਇਸ ਅਨੁਭਵ ਨੇ ਉਸ ਦੀ ਰੂਹ ਨੂਂ ਉਸਾਹ ਨਾਲ ਭਰ ਦਿਤਾ
ਜੋ ਸੌਤ ਦੀ ਕਿਰਪਾਲਤਾ ਦੇ ਨਿਕਟ ਸੀ। ਇਹ ਇਕ ਅਜਿਹੀ ਭਾਵਨਾ ਸੀ
ਜਿਸ ਦਾ ਜਨਮ ਇਕ ਨਿਂਦਰਾਲੇ ਪਲ ਦੇ ਵਿਸ਼ਾਲ ਲੋਕ ਵਿਚੋ ਹੋਇਆ ਸੀ। ਉਹ
ਪਲ ਜਿਸ ਨੇ ਯੁਗ੍ਗ ਦੇ ਨਮੂਨੇ ਘਡ਼ੇ ਹਨ ਜਿਵੇ ਇਕ ਬੀਜ ਵਿਚੋਂ ਕੌਮਾਂ ਜਨਮਦੀਆਂ
ਹਨ।
          ਅਲੀ ਨੇ ਖੇਹ ਹੋਏ ਮਂਦਰ ਵਲ ਦੇਖਿਆ ਤੇ ਉਸਦੀ ਥਕਾਵਟ ਨੇ ਉਸ ਦੀ
ਆਤਮਕ ਚੇਤਨਾ ਨੂਂ ਜਾਗ੃ਤ ਕੀਤਾ। ਪੂਜਾ-ਸਥਾਨ ਦੇ ਖਂਡਰ ਉਸ ਦੀ ਨਜ਼ਰ
ਵਿਚ ਆਏ ਅਤੇ ਟੁਟ੍ਟੀਆਂ ਭਜ੍ਜੀਆਂ ਦੀਵਾਰਾਂ ਦੀਆਂ ਨੀਹਾਂ ਬਡ਼ੀਆਂ ਸਪ਷ਟ ਹੋ
ਗਈਆਂ। ਉਸਦੀਆਂ ਅਕ੍ਖਾਂ ਪਥਰਾ ਗਈਆਂ ਤੇ ਉਸਦਾ ਦਿਲ ਬਡ਼ੀ ਤੇਜੀ ਨਾਲ
ਧਡ਼ਕਡ਼ ਲਗ੍ਗਾ ਅਤੇ ਫਿਰ ਅਚਾਨਕ ਉਸਦੀਆਂ ਅਕ੍ਖਾ 'ਚ ਰੋ਷ਨੀ ਆ ਗਈ ਤੇ
ਉਹ ਦੇਖਣ ਲਗਾ ਅਤੇ ਫਿਰ ਸੋਚ-ਵਿਚਾਰ ਕਰਨ ਲਗ੍ਗਾ ਅਤੇ ਇਸ ਵਿਚਾਰ ਦੇ
ਧੁਂਦਕਾਰੇ ਅਤੇ ਸੋਚ ਹੀ ਗਡ਼ਬਡ਼ ਵਿਚ ਯਾਦਾਂ ਦੇ ਕੁਝ ਆਕਾਰ ਪ੍ਰਗਟ ਹੋਏ। ਉਸ
ਨੂਂ ਯਾਦ ਆਇਆ ਕਿ ਇਹ ਥਂਮ ਬਡ਼ੀ ਸ਼ਾਨ ਤੇ ਬਡ਼ੇ ਮਾਣ ਨਾਲ ਬਿਲਕੁਲ ਸਿਦ੍ਧੇ
ਕਡ਼੍ਹੇ ਸਨ। ਇਸ ਡਰ ਪੈਦਾ ਕਰਨ ਵਾਲੀ ਦੇਵੀ ਦੀ ਮੂਰਤੀ ਸਾਹਮਣੇ ਚਾਂਦੀ ਦੇ
ਲੈਂਪ ਤੇ ਧੁਪਦਾਨਾਂ ਦੀ ਉਸ ਨੂਂ ਯਾਦ ਆਈ। ਉਸ ਨੂਂ ਯਾਦ ਆਇਆ ਕਿ
ਸਾਹਿਤਕਾਰ ਯੋਗ ਪਾਦਰੀ ਉਸ ਹਾਥੀ ਦਂਦ ਤੇ ਸੋਨੇ ਨਾਲ ਜਡ਼ੇ ਪੂਜਾ ਸਥਾਨ ਤੇ
ਭੇਟਾ ਧਰਦੇ ਸਨ। ਉਸ ਨੂਂ ਯਾਦ ਆਇਆ ਕਿ ਕੁਡ਼ੀਆਂ ਢੋਲਕੀ
ਵਜਾਉਂਦੀਆਂ ਸਨ ਤੇ ਜੁਆਨ ਇਸ ਹੁਸ਼ਨ ਤੇ ਇਸ਼ਕ ਦੀ ਦੇਵੀ ਦੀ ਉਸਤਤ ਵਿਚ
ਭਜਨ ਗਾਉਂਦੇ ਸਨ। ਉਸ ਨੂਂ ਇਹ ਸਭ ਯਾਦ ਆਇਆ ਤੇ ਇਹ ਸੁਰਤਾਂ ਉਸ ਨੂਂ
ਜਾਪਿਆ ਕਿ ਉਸ ਦੀਆਂ ਧੁਰ ਅਂਦਰਲੀਆਂ ਡੁਘਾਈਆਂ ਵਿਚਲੀ ਅਂਤਾਂ ਦੀ ਖਾਮੋ਷ੀ
ਵਿਚ ਸੁਤੀਆਂ ਚੀਜ਼ਾਂ ਮੁਡ਼ ਹਰਕਤ ਵਿਚ ਆ ਰਹੀਆਂ ਸਨ। ਪਰ ਯਾਦਾਸ਼ਤ
ਸਿਵਾਏ ਪੁਛਾਵਿਆਂ ਜਿਹੇ ਆਕਾਰਾਂ ਦੇ ਕੁਝ ਵਾਪਸ ਨਾ ਲਿਯਾ ਸਕੀ। ਜਿਸ ਨੂਂ ਅਸੀਂ
ਆਪਣੇ ਪੁਬਲਿਆਂ ਜਨਮਾਂ ਵਿਚੋਂ ਦੇਖਦੇ ਹੁਂਦੇ ਸਾਂ ਤੇ ਨਾ ਹੀ ਇਕ ਵਾਰ
ਸੁਣੀਆਂ ਆਵਾਜਾਂ ਦੀ ਗੂਂਜ ਤੋਂ ਬਿਨਾਂ ਇਹ ਹੋਰ ਕੁਝ ਸਾਡੇ ਕਂਨਾਂ ਤਕ੍ਕ ਲਿਆ
ਸਕੀ। ਪਰ ਫਿਰ ਹਣ ਇਹ ਕਿਹਡ਼ੀ ਕੁਡ਼ੀ ਸੀ ਜੋ ਇਨ੍ਹਾਂ ਯਾਦਾਂ ਨੂਂ ਇਕ ਤਂਬੂ
ਵਿਚ ਜਨਮੇ ਤੇ ਆਪਣੇ ਜੀਵਨ ਦੀ ਬਹਾਰ ਵਿਚ ਭੇਡਾਂ ਨੂਂ ਜਂਗਲ ਵਿਚ ਚਾਰਦੇ
ਨੌਜਵਾਨ ਨਾਲ ਜੋਡ਼ਦੀ ਸੀ?
          ਅਲੀ ਉਠਿਆ ਅਤੇ ਖਂਡਰਾਂ ਵਿਚ ਟੁਟ੍ਟੇ ਫੁਟ੍ਟੇ ਪਤ੍ਥਰਾਂ ਉਪਰ ਚਲਣ ਲਗ੍ਗਾ।
ਇਨ੍ਹਾਂ ਯਾਦਾਂ ਨੇ ਉਸਦੇ ਮਨ ਦੀ ਦ੃ਸ਼ਟੀ ਤੋਂ ਵਿਸਰੀ ਯਾਦ ਦੇ ਪਰਦੇ ਨੂਂ ਪਰੇ
ਕਰ ਦਿਤਾ, ਜਿਵੇਂ ਕੋਈ ਤੀਵੀਂ ਸ਼ੀਸ਼ੇ ਤਾਂ ਜਾਲੇ ਸਾਫ ਕਰ ਦਿਂਦੀ ਹੈ। ਜਦੋਂ ਉਹ
ਸਂਦਰ ਵਾਲੀ ਥਾਂ ਦੇ ਠੀਕ ਵਿਚਕਾਰ ਪਹੁਂਚਾ ਤਾਂ ਅਹਲ ਸਿਦ੍ਧਾ ਖਡ਼ਾ ਹੋ ਜ੍ਞਾ
ਜਿਵੇਂ ਉਸ ਦੇ ਪੈਰਾਂ ਨੂਂ ਧਰਤੀ ਦੀ ਚੁਂਬਕੀ ਷ਕਤੀ ਨੇ ਜਕਡ਼ ਲਿਆ ਹੋਵੇ। ਅਤੇ
ਫਿਰ ਅਚਾਨਕ ਉਸ ਨੇ ਆਪਣੇ ਸਾਹਮਣੇ ਥਰਤੀ ਉਪਰ ਇਕ ਟ੍ਟੁਟੀ ਹੋਈ ਮੂਰਤੀ
ਦੇਖੀ ਅਤੇ ਉਹ ਬਿਨਾ ਕੁਝ ਸੋਚੇ ਸਮਝੇ ਉਸ ਮ੍ਵਹਰੇ ਡਂਡਉਤ ਬਂਦਨਾ ਕਰਨ ਲਈ
ਲਂਮਾ ਪੈ ਗਿਆ। ਉਸ ਦੀਆਂ ਭਾਵਨਾਵਾਂ ਦਾ ਹਡ਼੍ਹ ਵਹਿ ਤੁਰਿਆ ਜਿਵੇਂ ਨਂਗੇ
ਜ਼ਖਮ ਵਿਚੋਂ ਖੂਨ ਵਹਿਂਦਾ ਹੈ। ਉਸ ਦੇ ਦਿਲ ਦੀ ਧਡ਼ਕਡ਼ ਜਿਵੇਂ ਵਧਦੀ ਘਟਦੀ
ਜਿਵੇਂ ਸਮੁਂਦਰ ਦੀਆਂ ਲਹਿਰਾਂ ਚਡ਼੍ਹਦੀਆਂ ਉਤਰਦੀਆਂ ਹਨ। ਇਸ ਨਜਾਰੇ ਨੇ
ਉਸ ਨੂਂ ਨਿਮਾਣਾ ਜਿਹਾ ਬਣਾ ਦਿਤਾ ਅਤੇ ਉਸ ਨੇ ਇਕ ਲਂਮਾ ਹਉਕਾ ਲਿਆ ਤੇ
ਆਪਣੇ ਦੁਖ ਕਾਰਨ ਰੋਣ ਲਗ੍ਗਾ ਕਿਉਂਕਿ ਉਸ ਨੂਂ ਅਜਿਹੀ ਇਕ੍ਕਲ ਮਹਿਸੂਸ ਹੋਈ
ਜੋ ਜ਼ਖਮੀ ਕਰਨ ਵਾਲੀ ਸੀ ਅਤੇ ਅਜਿਹੀ ਦੂਰੀ ਜੋ ਵਿਨਾਸ਼ ਕਰਨ ਵਾਲੀ ਸੀ,ਉਸ
ਦੀ ਰੂਹ ਨੂਂ ਉਸ ਸੁਰਣੀ ਰੂਹ ਤੋਂ ਵਿਛੋਡ਼ਨ ਵਾਲੀ ਸੀ ਜੋ ਉਸ ਸਮੇਂ ਤੋ ਪਹਿਲਾਂ
ਵੀ ਸੀ ਜਦੋਂ ਉਹ ਇਸ ਜਿਂਦਗੀ ਵਿਚ ਅਜੇ ਦਾਖਲ ਨਹੀਂ ਹੌਇਆ ਸੀ। ਉਸ ਨੇ
ਮਹਿਸੂਸ ਕੀਤਾ ਕਿ ਉਸ ਦੀ ਆਤਮਾ ਉਸ ਜਲਦੀ ਹੋਈ ਲਾਟ ਦਾ ਅਂਸ਼ ਹੈ ਜਿਸ
ਨੂਂ ਆਦਿ ਕਾਲ ਵਿਚ ਆਪਣੀ ਮੌਤ ਤੋਂ ਵਕ੍ਖ ਕੀਤਾ ਜ੍ਞਾ ਸੀ। ਉਸ ਨੇ ਮਹਿਸੂਸ
ਕੀਤਾ ਕਿ ਉਸ ਦੀਆਂ ਜਲ ਰਹੀਆਂ ਹਡ੍ਡੀਆਂ ਵਿਚ ਖਂਭਾਂ ਦੀ ਹਲਕੀ ਜਿਹੀ ਫਡ਼-
ਫਡ਼ਾਹਟ ਹੋ ਰਹੀ ਹੈ ਅਤੇ ਉਸ ਦੇ ਨਸ੍ਸਲ ਹੋਏ ਦਿਮਾਗੀ ਸੈਲ੍ਲਾਂ ਦੁਆਲੇ ਬਲਵਾਨ
ਪ੍ਰੇਮ ਭਾਵਨਾ ਉਸ ਦੇ ਦਿਲ ਅਤੇ ਰੂਹ ਦੇ ਕਾਬਜ਼ ਹੋ ਰਹੀ ਹੈ। ਪਿਆਰ ਨੇ ਰੂਹ ਨੂਂ
ਰੂਹ ਵਿਚਲੀਆਂ ਗੁਜ੍ਝੀਆਂ ਚੀਜਾਂ ਦਾ ਪਤਾ ਦਿਤਾ ਅਤੇ ਆਪਣੇ ਕਂਮਾ ਦੁਆਰਾ
ਗਿਣਤੀ ਮਿਣਤੀ ਦੇ ਸਂਸਾਰ ਨਾਲੋਂ ਉਸ ਦੇ ਮਨ ਨੂਂ ਵਕ੍ਖ ਕਰ ਦਿਤਾ। ਪਿਆਰ
ਜਿਸ ਨੂਂ ਅਸੀਂ ਉਸ ਵੇਲੇ ਬੋਲਦਾ ਸੁਣਦੇ ਹਾਂ ਜਦੋਂ ਜੀਵਨ-ਜਿਹਬਾ ਚੁਪ੍ਪ ਹੁਂਦੀ ਹੈ
ਅਤ ਅਂਧਕਾਰ ਆਪਣੇ ਅਂਧੇਰੇ ਵਿਚ ਸਭ ਚੀਜ਼ਾਂ ਛੁਪਾ ਦਿਂਦਾ ਹੈ ਉਦੋਂ ਅਸੀਂ ਇਸ
ਨੂਂ ਅਗ੍ਗ ਦੇ ਥਂਮ ਵਾਂਗੂ ਚਮਕਦਾ ਦੇਖਦੇ ਹਾਂ। ਇਹ ਪਿਆਰ ਇਹ ਦੇਵਤਾ ਅਲੀ
ਅਲ ਹੁਸੈਨੀ ਦੀ ਰੂਹ ਵਿਚ ਇਸ ਘਡ਼ੀ ਉਤਰ ਆਇਆ ਹੈ ਤੇ ਉਸ ਅਂਦਰ ਇਸ
ਨੇ ਮਧੁਰ ਤੇ ਕਟੁ ਅਨੁਭਵ ਜਾਗ੃ਤ ਕਰ ਦਿਤੇ ਹਨ ਜਿਵੇਂ ਸੂਰਜ ਫੁਲਾਂ ਦੇ ਨਾਲ
ਨਾਲ ਕਂਡੇ ਵੀ ਦਿਂਦਾ ਹੈ।
          ਇਹ ਪਿਆਰ ਹੈ ਕੀ? ਇਹ ਕਿਧਰੋਂ ਆ ਜਾਂਦਾ ਹੈ? ਇਹ ਉਸ ਨੌਜਵਾਨ
ਤੋਂ ਕੀ ਚਾਹੁਂਦਾ ਹੈ ਜੋ ਮਂਦਰਾਂ ਦੇ ਖਂਡਰਾਂ ਵਿਚ ਆਪਣੇ ਇਜ੍ਜਡ਼ ਸਮੇਤ ਆਰਾਮ
ਕਰ ਰਿਹਾ ਹੈ। ਇਹ ਕੋਈ ਸਰਾਬ ਹੈ ਜੋ ਉਸਦੀਆਂ ਰਗਾ ਵਿਚੋਂ ਵਹਿਣ ਲਗ੍ਗੀ
ਹੈ ਜਿਸ ਨੂਂ ਕਦੇ ਮੁਟਿਆਰ ਦੀਆਂ ਪਿਆਰ ਭਰੀਆਂ ਤਕਣੀਆਂ ਵਿਚਲਿਤ ਨਹੀਂ
ਕਰ ਸਕੀਆਂ? ਇਹ ਕਿਹਾ ਦੇਵੀ ਸਂਗੀਤ ਹੇ ਜੋ ਇਸ ਬਂਦੇ ਦੇ ਕਂਨਾ ਵਿਚ ਗੂਂਜ
ਰਿਹਾ ਹੈ ਜਿਸ ਨੇ ਅਜੇ ਇਸਤਰੀਆਂ ਦੇ ਮਧੁਰ ਗੀਤ ਨਹੀਂ ਸੁਣੇ।
          ਇਹ ਪਿਆਰ ਕੀ ਚੀਜ਼ ਹੈ ਤੇ ਕਿਧਰੋਂ ਆ ਜਾਂਦਾ ਹੈ? ਇਹ ਅਲੀ
ਤੋਂ ਕੀ ਚਾਹੁਂਦਾ ਹੈ ਜੋ ਲੋਕਾਂ ਤੋਂ ਦੂਰ ਤੁਹਾਡੇ ਆਪਣੀਆਂ ਭੇਡਾਂ ਦੀ ਪਰਵਰਿ਷ ਤੇ
ਬਂਸਰੀ ਵਜਾਉਣ ਵਿਚ ਰੁਝਿਆ ਹੋਇਆ ਹੈ, ਕਿ ਇਸ ਦਾ ਬੀਜ ਉਸ ਅਂਦਰ
ਅਣਜਾਣੇ ਹੀ ਮਾਨਵ ਕ੃ਤ ਸੁਹਪ੍ਪਣ ਨੇ ਬੀਜਿਆ ਹੈ? ਕੀ ਇਹ ਤੇਜ ਰੋਸ਼ਨੀ ਹੈ
ਜਿਸ ਨੂਂ ਧੁਂਦ ਦੇ ਪਰਦੇ ਨੇ ਕਜ੍ਜਿਆ ਹੋਇਆ ਹੇ ਤੇ ਹੁਣ ਪਰਦਾ ਪਾਡ਼ ਕੇ
ਉਸ ਦੀ ਰੂਹ ਦੇ ਖਾਲੀਪਣ ਨੂਂ ਭਰਨ ਲਗ੍ਗਾ ਹੈ? ਕੀ ਇਹ ਟਿਕੀ ਰਾਤ ਵਿਚ ਉਸ
ਨੂਂ ਚੁਡ਼ਾਉਣ ਲਈ ਆਇਆ ਸੁਹਾਵਣਾ ਸੁਪਨਾ ਹੈ? ਜਾਂ ਸਚ੍ਚ ਹੈ ਜੋ ਆਦਿ ਕਾਲ
ਵਿਚ ਵੀ ਸੀ ਤੇ ਅਂਤ ਕਾਲ ਤਕ੍ਕ ਵੀ ਰਹੇਗਾ?
          ਅਲੀ ਨੇ ਆਪਣੀਆਂ ਹਂਝੁਆਂ ਭਰੀਆਂ ਅਕ੍ਖਾਂ ਮੀਟ ਲਇਆਂ ਅਤੇ ਭਿਖਿਆ
ਮਂਗ ਰਹੇ ਭਿਖਾਰੀ ਵਾਂਗ ਆਪਣੇ ਹਤ੍ਥ ਫੈਲਾ ਦਿਤੇ। ਉਸਦੀ ਰੂਹ ਉਸ ਅਂਦਰ
ਕਂਬਣ ਲਗ੍ਗ ਗਈ ਤੇ ਇਸ ਕਾਂਬੇ ਨੇ ਟ੍ਟੁਟਵੀਆਂ ਸਿਸਕੀਆਂ ਦਾ ਰੂਪ ਧਾਰ ਲਿਆ
ਜਿਸ ਵਿਚ ਦੁਕ੍ਖ ਭਰੀਆਂ ਷ਕਾਇਤਾਂ ਅਤੇ ਇਚ੍ਛਾ ਅਗਨੀ ਸ਼ਾਮਿਲ ਹੈ। ਉਸ ਨੇ
ਬਡ਼ੀ ਹੌਲੀ ਜਿਹੀ ਆਵਾਜ ਵਿਚ ਜੋ ਸ਼ਬਦਾ ਦੀ ਮਾਂਦੀ ਧੁਨੀ ਕਾਰਨ ਇਕ ਡਂਢੀ
ਆਹ ਜਿਹੀ ਸੀ, ਆਖਿਆ:
          "ਆਪ ਕੌਣ ਹੋ ਜੇ ਮੇਰੇ ਦਿਲ ਦੇ ਨੇਡ਼ੇ ਹੁਂਦੇ ਹੋਏ ਵੀ ਅਕ੍ਖਾਂ ਤੋਂ ਓਹਲੇ ਹੋ,
ਮੈਂਨੂਂ ਮੈਥੋਂ ਵਿਛੋਡ਼ ਰਹੇ ਹੋ, ਮੇਰੇ ਵਰਤਮਾਨ ਨੂਂ ਦੂਰ ਦੁਰਾਡੇ ਅਤੇ ਭੂਲ੍ਲੇ ਵਿਸਰੇ ਯੁਗਾਂ
ਨਾਲ ਜੋਡ਼ ਰਹੇ ਹੋ? ਕੀ ਤੁਸੀਂ ਸ਼ਾਇਦ ਜਿਨ੍ਨ ਦੀ ਮਹਾਰਾਣੀ ਦੀ ਆਤਮਾ ਹੋ ਜੇ ਪਤਾਲ
ਵਿਚੋਂ ਇਸ ਲਈ ਪ੍ਰਗਟ ਹੌਈ ਹੈ ਕਿ ਮੇਰੀ ਚੇਤਨਾ ਨੂਂ ਗੁਲਾਮ ਬਣਾ ਕੇ ਲਵੇ ਤੇ
ਮੇਰੇ ਕਬੀਲੇ ਦੇ ਲੋਕਾਂ ਵਿਚ ਮੇਰਾ ਮੌਜੂ ਬਣਾਉਣਾ ਚਾਹੂਂਦੀ ਹੈ? ਆਪ ਕੌਣ ਹੋ
ਤੇ ਇਹ ਕੇਹੀ ਤੀਬਰ ਤ੃਷ਨਾ ਹੈ ਜੋ ਮੈਨੂਂ ਫਨਾਹ ਕਰਪ ਰਹੀ ਹੈ ਤੇ ਮੇਰੇ ਦਿਲ ਦੇ
ਕਾਬੁ ਪਾ ਰਹੀ ਹੈ? ਇਹ ਕਿਹੀਆਂ ਭਾਵਨਾਵਾਂ ਹਨ ਜੋ ਮੈਨੂਂ ਅਗ੍ਗ ਤੇ ਰੌ਷ਨੀ ਨਾਲ
ਭਰ ਰਹੀਆਂ ਹਨ। ਮੈਂ ਕੌਣ ਹਾਂ ਤੇ ਇਹ ਨਵਾਂ ਮੈਂ ਜਿਸ ਨੂਂ ਮੈਂ "ਮੋਂ" ਕਹਿ ਕੇ
ਪੁਕਾਰਦਾ ਹਾਂ, ਮੇਰੇ ਬਿਲਕੁਲ ਅਜਨਵੀ ਹੈ? ਜੀਵਨ -ਚ਷ਮੇ ਦਾ ਪਾਣੀ ਕੀ
ਮੈਂ ਹਵਾ ਦੇ ਜ਼ਰਿਆਂ ਸਮੇਤ ਪੀ ਗਿਆ ਹਾਂ ਜਿਸ ਨਾਲ ਮੈਂ ਇਕ ਦੇਵਤਾ ਬਣ
ਜ੍ਞਾ ਹਾਂ ਜੋ ਸਭ ਗੁਪਤ ਚੀਜ਼ਾ ਨੂਂ ਸੁਣ ਤੇ ਦੇਖ ਸਕਦਾ ਹੈ, ਕੀ ਮੈਂ ਸ਼ੈਤਾਨ ਦੀ
ਸ਼ਰਾਬ ਪੀ ਲਈ ਹੈ ਅਤੇ ਸਚ੍ਚ ਮੁਚ੍ਚ ਦੀਆਂ ਚੀਜ਼ਾਂ ਮੈਨੂਂ ਦਿਸ਼ਣ ਹਟ ਗਈਆਂ
ਹਨ?
          ਉਹ ਕੁਝ ਸਮਾਂ ਚੁਪ੍ਪ ਰਿਹਾ ਤੇ ਫਿਰ ਉਸ ਦੀਆਂ ਭਾਵਨਾਵਾਂ ਨੇ ਜੋਰ ਫਡ਼ਿਆ
ਤੇ ਆਤਮਾ ਚਡ਼੍ਹਦੀ ਕਲਾ ਵਿਚ ਆਈ। ਉਹ ਫਿਰ ਬੋਲੀਆ:-
          "ਤੁਹਾਨੂਂ ਆਤਮਾ ਪ੍ਰਗਟ ਕਰਦੀ ਹੈ ਤੇ ਆਪਣੇ ਨੇਡ਼ੇ ਲਿਆਉਂਦੀ ਹੈ ਅਤੇ
ਜਿਸ ਨੂਂ ਰਾਤ ਛੁਪਾ ਲੈਂਦੀ ਹੈ ਤੇ ਦੁਰਾਡੇ ਦੀ ਚੀਜ਼ ਬਣਾ ਦਿਂਦੀ ਹੈ। ਤੇ ਮੇਰ
ਸੁਪਨਿਆਂ ਦੇ ਅਂਬਰਾਂ ਵਿਚ ਮਂਡਰਾਉਣ ਵਾਲੀ ਸੁਂਦਰ ਆਤਮਾ ਤੁਸਾਂ ਮੇਰੀ ਹੋਂਦ
ਅਂਦਰ ਸੁਤੀਆਂ ਭਾਵਨਾਵਾਂ ਨੂਂ ਜਗਾ ਦਿਤਾ ਹੈ ਜਿਵੇਂ ਵਰਫ ਦੀ ਤਹਿ ਥਲੇ ਬੀਜ਼ਾਂ
ਨੂਂ ਹਰਕਤ ਵਿਚ ਲਿਆਂਦਾ ਹੋਵੇ ਤੇ ਖੇਤਾਂ ਦੀ ਪ੍ਰਾਣਦਾਤੀ ਸੁਗਂਧ ਸਮੀਰ ਵਾਂਗ
ਭਾਵਨਾਵਾਂ ਚਲ ਰਹੀਆ ਹੋਣ, ਅਤੇ ਮੇਰੀ ਚੇਤਨਾ ਨੂਂ ਟੁਬਿਆ ਅਤੇ ਬਿਆਕੁਲ
ਕੀਤਾ ਜਿਵੇਂ ਹਵਾ ਰੁਖਾਂ ਦੇ ਪਤ੍ਤਿਆਂ ਨੂਂ ਛੇਡ਼ਦੀ ਹੈ। ਜੇ ਕਰ ਤੁਸੀਂ ਸ਼ਹੀਰ ਸਹਿਤ
ਤੇ ਸਾਰਵਾਨ ਹੋ ਤਾਂ ਪਰਮਾਤਮਾ ਲਈ ਮੈਨੂਂ ਜਰਾ ਆਪਣੇ ਦਰਸ਼ਨ ਦੇਣ ਦੀ ਬੇਣਲ
ਕਰੋ ਅਤੇ ਜੇਕਰ ਆਪ ਷ਰੀਰ ਰਹਿਤ ਹੋ ਤਾਂ ਆਪ ਮੇਰੀ ਨੀਂਦ ਨੂਂ ਹੁਕਮ ਕਰੋ
ਉਹ ਮੇਰੀਆਂ ਪਲਕਾਂ ਬਂਦ ਕਰ ਦੇਵੇ ਤਾਂ ਕਿ ਮੈਂ ਸੁਪਨੇ ਵਿਚ ਤੁਹਾਨੂਂ ਦੇਖ ਸਕਾਂ।
ਆਪ ਮੈਨੂਂ ਛੁਹਨ ਦਿਓ, ਮੈਨੂਂ ਆਪਣੀ ਆਵਾਜ਼ ਸੁਣਨ ਦਿਓ, ਉਸ ਪਰਦੇ ਨੂਂ
ਲੀਰਾਂ ਲੀਰਾਂ ਕਰ ਦਿਓ ਜਿਸ ਨੇ ਮੇਰੀ ਸਾਰੀ ਹੋਂਦ ਨੂਂ ਢਰਿਆ ਹੋਇਆ ਹੈ ਅਤੇ
ਉਨ੍ਹਾਂ ਤਂਦਾਂ ਨੂਂ ਨਸ਼ਟ ਕਰ ਦਿਓ ਜਿਨ੍ਹਾਂ ਨੇ ਮੇਰੀ ਰੂਹਾਨੀਅਤ ਨੂਂ ਲਕੋਇਆ
ਹੋਇਆ ਹੈ। ਜੇਕਰ ਆਪ ਅਂਬਰਾਂ ਦੇ ਵਾਸੀ ਹੋ ਤਾਂ ਮੈਨੂਂ ਪਂਖ ਦਿਓ ਤਾਂ ਕਿ ਮੈਂ
ਤੁਹਾਡੇ ਮਗਰ ਅਂਬਰਾਂ ਵਿਚ ਉਡ ਆਵਾਂ। ਜਾਦੂ ਨਾਲ ਮੇਰੀਆਂ ਪਲਕਾਂ
ਛੁਹਵੇਂ ਅਤੇ ਜੇਕਰ ਆਪ ਕੋਈ ਪਰੀ ਹੋ ਤਾਂ ਮੈਂ ਜਿਨ੍ਨ ਦੀਆਂ ਗੁਪਤ ਥਾਵਾਂ ਤੇ
ਆਪਦੇ ਮਗਰ ਮਗਰ ਆਵਾਂ। ਆਪਣਾ ਆਦਿਖ੍ਖ ਹਤ੍ਥ ਮੇਰੇ ਦਿਲ ਤੇ ਰਕ੍ਖ ਕੇ ਮੈਨੂਂ
ਆਪਣੇ ਵਿਚ ਕਰ ਲਓ ਜੇਕਰ ਆਪ ਕਿਸੇ ਨੁਂ ਆਪਣੇ ਮਗਰ ਆਉਣ ਦੇਣ ਦੀ
ਪਰਵਾਨਗੀ ਦੇਣ ਦੇ ਸਮਰਥ ਹੋ।"
          ਇਉਂ ਅਲੀ ਨੇ ਇਸ ਤਰ੍ਹਾਂ ਦੇ ਸ਼ਬਦ ਅਂਧਕਾਰ ਦੇ ਕਂਨਾਂ ਵਿਚ ਗੁਣਗੁਣਾਏ
ਜੇ ਉਸ ਦੇ ਧਰ ਅਂਦਰੋਂ ਗੀਤਾਂ ਦੀ ਗੂਂਜ ਬਣ ਕੇ ਪ੍ਰਗਟ ਹੋਏ ਸਨ। ਉਸ ਦੀ
ਦ੃਷ਟੀ ਤੇ ਆਲੇ ਦੁਆਲੇ ਦੇ ਦਰਮਿਆਨ ਰਾਤ ਦੇ ਭੂਤ-ਪ੍ਰੇਤ ਫਿਰਨ ਲਗੇ ਜਿਵੇਂ
ਉਹ ਗਰਮ ਗਰਮ ਹਂਝੁਆਂ ਵਿਚੋਂ ਨਿਕਲ ਰਹੀ ਸੁਗਂਧੀ ਹੋਵੇ। ਮਂਦਰ ਦੀਆਂ ਕਂਧਾਂ
ਉਪਰ ਸਤਰਂਗੀ ਪੀਂਘ ਦੇ ਰਂਗਾ ਵਿਚ ਜਾਦੂਮਈ ਤਿਤ੍ਤਰ ਬਣਨ ਲਗ੍ਗੇ।
          ਇਉਂ ਕੁਝ ਸਮਾਂ ਲਂਘ ਜ੍ਞਾ। ਉਸ ਨੂਂ ਆਪਣੇ ਅਬਰੂਆਂ ਵਿਚ ਆਨਂਦ
ਤੇ ਦੁਕ੍ਖ ਵਿਚ ਖੁਸ਼ੀ ਮਹਿਸੂਸ ਹੋਈ। ਉਹ ਆਪਣੇ ਹਿਰਦੇ ਦੀ ਧਡ਼ਕਣ ਸੁਣ ਰਿਹਾ
ਸੀ। ਉਸ ਦੀਆਂ ਅਕ੍ਖਾਂ ਸਭ ਚੀਜ਼ਾਂ ਤੋਂ ਪਾਰ ਕਿਸੇ ਨਜਾਰੇ ਨੂਂ ਦੇਖ ਰਹੀਆਂ ਸਨ
ਮਾਨੇ ਉਸ ਦੇ ਜੀਵਨ ਦੇ ਚਿਤ੍ਤਰ ਉਸ ਦੀਆਂ ਅਕ੍ਖਾਂ ਸਾਹਮਣੇ ਮਿਟ ਰਹੇ ਹੋਣ ਅਤੇ
ਇਨ੍ਹਾਂ ਦੀ ਥਾਂ ਇਕ ਅਜਿਹਾ ਸੁਪਨਾ ਲੈ ਰਿਹਾ ਸੀ ਜੋ ਆਪਣੇ ਸੁਹਪਣ ਕਾਰਨ
ਹੈਰਾਨੀ ਜਨਕ ਤੇ ਕਾਲਪਨਿਕ ਚਿਤ੍ਤਰਾਂ ਨੂਂ ਸਿਰਜਣ ਕਾਰਨ ਡਰਾਉਣਾ ਸੀ ਜਿਵੇਂ
ਇਕ ਪੈਗਂਬਰ ਦੇਵੀ ਚੇਤਨਾ ਦੀ ਪ੍ਰਾਪਤੀ ਲਈ ਨਛਤਰਾਂ ਨੂਂ ਅਂਬਰਾਂ ਵਿਚ ਨਿਹਾਰਦਾ
ਹੈ, ਇਸ ਤਰ੍ਹਾਂ ਉਹ ਆਉਣ ਵਾਲੇ ਪਲਾਂ ਨੂਂ ਉਡੀਕਣ  ਲਗ੍ਗਾ। ਉਸ ਦੇ ਤੇਜ਼
ਹਉਕਿਆਂ ਨੇ ਉਸ ਦੇ ਸ਼ਾਂਤ ਸਵਾਂਸਾ ਨੂਂ ਰੋਕ ਦਿਤ੍ਤਾ ਅਤੇ ਉਸ ਦੀ ਆਤਮਾ ਉਸ ਦੇ
ਸ਼ਰੀਰ ਨੂਂ ਤਿਆਗ ਕੇ ਦੁਆਲੇ ਮਂਡਰਾਉਣ ਲਗ੍ਗੀ ਤੇ ਫਿਰ ਉਸਦੇ ਸਰੀਰ
ਵਿਚ ਵਾਪਸ ਆ ਗਈ ਮਾਨੋ ਉਹ ਇਨ੍ਹਾਂ ਖਂਡਰਾਂ ਵਿਚ ਕਿਸੇ ਗੁਆਚੇ ਪਿਆਰੇ ਨੂਂ
ਲਭ ਰਹੀ ਹੋਵੇ।
          ਇਉਂ ਦਿਨ ਚਡ਼੍ਹ ਆਇਆ ਤੇ ਸੁਗਂਧ ਸਮੀਰ ਦੀ ਧੀਮੀ ਗਤੀ ਕਾਰਨ ਚਪ੍ਪ
ਕਂਬਣ ਲਗ੍ਗੀ। ਵਿਸ਼ਾਲ ਵਾਤਾਵਰਣ ਵਿਚ ਇਕ ਅਜਿਹੀ ਮੁਸਕਾਨ ਫੈਲ ਗਈ
ਜਿਵੇ ਕਿਸੇ ਘੂਕ ਸੁਤੇ ਪਏ ਦੀ ਹੁਂਦੀ ਹੈ ਜਿਸ ਨੇ ਸੁਪਨੇ ਵਿਚ ਆਪਣੀ ਮਹਿਬੂਬਾ
ਦੇਖੀ ਹੋਵੇ। ਟੁਟ੍ਟੀਆਂ ਫੁਟੀਆਂ ਕਂਧਾਂ ਦੇ ਮਘੋਰਿਆਂ ਵਿਚੋਂ ਪਂਛੀ ਨਿਕਲ ਆਏ ਅਤੇ
ਥਂਮਾਂ ਦੇ ਆਸ ਪਾਸ ਚਹਿਕਣ ਲਗ੍ਗੇ ਤੇ ਗਾਉਣ ਲਗ੍ਗੇ ਤੇ ਇਕ ਦੁਸਰੇ ਨਾਲ ਬੋਲ
ਸਾਂਝੇ ਕਰਨ ਲਗ੍ਗੇ ਅਤੇ ਸਵੇਰ ਦੇ ਆਗਮਨ ਦੀ ਸੂਚਨਾ ਦੇਣ ਲਗ੍ਗੇ।
          ਅਲੀ ਉਠਕੇ ਖਡ਼ਾ ਹੋਇਆ ਅਤੇ ਉਸਨੇ ਆਪਣੇ ਤਪਦੇ ਮਤ੍ਥੇ ਤੇ ਆਪਣਾ
ਹਤ੍ਥ ਰਖਿਆ ਅਤੇ ਆਪਲੇ ਵਲ ਧੁਂਦਲੀਆਂ ਨਜ਼ਰਾਂ ਨਾਲ ਦੇਖਿਆ ਅਤੇ ਫਿਰ
ਆਦਮ ਵਾਂਗ, ਜਦੋਂ ਉਸ ਦੀਆਂ ਅਕ੍ਖਾਂ ਪਰਮਾਤਮਾ ਦੇ ਸਵਾਸ ਨਾਲ ਖੋਲੀਆਂ
ਗਈਆਂ ਸਨ, ਉਸ ਨੇ ਸਭ ਚੀਜ਼ਾਂ ਵਲ ਦੇਖਿਆ ਅਤੇ ਹੈਰਾਨ ਹੋਇਆ ਉਹ
ਆਪਣੇ ਭੇਡਾਂ ਦੇ ਇਜ੍ਜਣ ਵਲ ਚਲੇ ਗਿਆ ਅਤੇ ਉਨ੍ਹਾਂ ਨੂਂ ਪੁਚਕਾਰਨ ਲਗ੍ਗਾ।
ਉਹ ਉਠੀਆਂ ਤੇ ਅਂਗਡ਼ਈਆਂ ਲੈਂਣ ਮਗਰੋਂ ਹਰੇ ਮੈਦਾਨ ਵਲ ਜਾਣ ਲਈ ਉਸ
ਮਗਰ ਹੋ ਲਈਆਂ।
          ਅਲੀ ਆਪਣੇ ਇਜ੍ਜਡ਼ ਦੇ ਅਗ੍ਗੇ ਅਗ੍ਗੇ ਚਲਣ ਲਗ੍ਗਾ ਤੇ ਉਸ ਦੀਆਂ ਹਿਰਨ
ਵਰਗੀਆਂ ਅਕ੍ਖਾਂ ਸਥਿਰ ਵਾਤਾਵਰਣ ਵਲ ਦੇਖਣ ਲਗ੍ਗੀਆਂ। ਉਸ ਦੀਆਂ ਧੁਰ
ਅਂਦਰ ਦੀਆਂ ਭਾਵਨਾਵਾਂ ਨੇ ਹੋਂਦ ਦੀਆਂ ਗੁਝੀਆਂ ਤੇ ਗੁਪਤ ਗਲ੍ਲਾਂ ਸਮਝਾਉਣ
ਲਈ ਉਡਾਰੀ ਮਾਰੀ ਅਤੇ ਦਰਸਾਉਣ ਲਈ ਕਿ ਯੁਗਾਂ ਵਿਚ ਕੀ ਬੀਤ ਚੁਕ੍ਕਾ
ਹੈ ਤੇ ਕੀ ਬਾਕੀ ਰਹਿ ਜ੍ਞਾ ਹੈ, ਇਕ ਝਲਕਾਰੇ ਵਿਚ ਦਰਸਾਇਆ ਤੇ ਇਕ
ਝਲਕਾਰੇ ਨਾਲ ਇਹ ਸਭ ਕੁਝ ਉਸ ਨੂਂ ਭੁਲਾਂ ਕੇ ਮੁਡ਼ ਉਸ ਦੀ ਮੋਹ ਮਾਇਆ ਦੀ
ਦੁਨੀਆਂ ਵਿਚ ਵਾਪਸ ਲੈ ਆਂਦਾ। ਅਤੇ ਉਸ ਨੂਂ ਆਪਣੇ ਤੇ ਆਪਣੀ ਰੂਹ ਵਿਚ
ਇਕ ਪਰਦਾ ਕਿਹਾ ਮਹਿਸੂਸ ਹੋਇਆ ਜਿਵੇਂ ਅਕ੍ਖ ਤੇ ਰੋਸ਼ਨੀ ਵਿਚ ਪਰਦਾ ਹੁਂਦਾ
ਹੈ। ਉਸ ਨੇ ਹਉਕਾ ਲਿਆ ਅਤੇ ਹਉਕੇ ਨਾਲ ਉਸ ਦੇ ਦਿਲ ਵਿਚੋਂ ਅਗ੍ਗ ਦੀ
ਲਾਟ ਜਿਹੀ ਨਿਕਲੀ।
          ਉਹ ਇਕ ਦਸ਼ਮੇ ਪਾਸ ਆਇਆ ਜਿਸ ਦੀ ਕਲ ਦੀ ਆਵਾਜ਼ ਖੇਡਾਂ
ਦੇ ਰਹਸ ਦਾ ਐਲਾਨ ਕਰ ਰਹੀ ਸੀ ਤੇ ਉਹ ਇਸ ਕਂਡੇ ਬੈਂਤ ਦੇ ਦੇ ਦਰਖਤ ਹੇਠ
ਬੈਠ ਗਿਆ ਜਿਸ ਦੀਆਂ ਟਹਿਣੀਆਂ ਹੇਠ ਪਾਣੀ ਨੂਂ ਛੂਹ ਰਹੀਆਂ ਸਨ ਮਾਨੋ ਉਹ
ਉਸ ਦੀ ਅਂਮ੃ਤ ਜਿਹੀ ਮਧੁਰਤਾ ਮਾਣ ਰਹੀਆ ਹੋਣ। ਡੇਢਾਂ ਸਿਰ ਸੁਟੀ ਘਾਹ ਚੁਗ
ਰਹੀਆਂ ਸਨ ਤੇ ਸਵੇਰ ਦੀ ਤ੍ਰੇਲ ਉਨ੍ਹਾਂ ਦੀ ਜਤ੍ਤ ਤੇ ਚਮਕ ਰਹੀ ਸੀ।
          ਅਜੇ ਕੁਝ ਪਲ ਹੀ ਗੁਜ਼ਰੇ ਸਨ ਕਿ ਅਲੀ ਨੇ ਆਪਣੇ ਹਿਰਦੇ ਦੀ ਤੇਜ਼
ਧਡ਼ਕਣ ਮਹਿਸੂਸ ਕੀਤੀ ਅਤੇ ਉਸ ਦੀ ਰੂਹ ਫਿਰ ਕਂਬਣ ਲਗ੍ਗੀ। ਉਹ ਸੁਤੇ
ਵਿਅਕਤੀ ਵਾਂਗ ਹਿਲਿਆ ਜੁਲਿਆ ਅਤੇ ਉਸ ਨੇ ਇਧਰ ਉਧਰ ਦੇਖਿਆ ਜਿਸ
ਨੂਂ ਸੂਰਜ ਦੀ ਗਰਮੀ ਨੇ ਜਗਾਇਆ ਹੋਵੇ। ਉਸੇ ਦੇਖਿਆ ਕਿ ਇਕ ਕੁਡ਼ੀ
ਕਂਧੇ ਤੇ ਗਾਗਰ ਚੁਕ੍ਕੀ ਦਰਖਤਾਂ ਵਿਚੋਂ ਹੌਲੀ ਹੌਲੀ ਚਸ਼ਮੇ ਵਲ੍ਲ ਆ ਰਹੀ ਹੈ।
ਉਸ ਦੇ ਪੈਰ ਨਂਗੇ ਤੇ ਤ੍ਰੇਲ ਨਾਲ ਭਿਜ੍ਜੇ ਹੋਏ ਹਨ। ਜਦੋਂ ਚਸ਼ਮੇ ਕਿਨਾਰੇ ਪੁਜ੍ਜ
ਕੇ ਉਹ ਘਡ਼ਾ ਭਰਨ ਲਈ ਝੁਕੀ ਤਾਂ ਉਸ ਸਾਹਮਣੇ ਕਿਨਾਰੇ ਵਲ੍ਲ ਦਿਖਿਆ ਤਾਂ
ਉਸਦੀਆਂ ਨਜਰਾਂ ਅਲਾ ਦੀਆਂ ਨਜ਼ਰਾਂ ਨਾਲ ਜਾ ਟਕਰੀਆਂ। ਉਹ ਇਕ ਦਮ
ਘਬਰਾ ਕੇ ਚੀਕ ਪਈ ਤੇ ਗਾਗਰ ਧਰਤੀ ਤੇ ਸੁਟ ਕੇ ਥੋਡ਼੍ਹਾ ਪੀਛੇ ਹਟ੍ਟ ਗਈ।
ਇਹ ਅਜਿਹੇ ਵਿਅਕਤੀ ਦਾ ਅਮਲ ਸੀ ਜਿਸ ਨੂਂ ਚਿਰ ਗੁਆਚਾ ਆਪਣਾ ਪ੍ਰੀਤਮ
ਮੁਡ਼ ਮਿਲ ਜ੍ਞਾ ਹੋਵੇ।
          ਪਲ ਗੁਜ਼ਰ ਜ੍ਞਾ ਤੇ ਇਸ ਦੇ ਛਿਣ ਅਜਿਹੇ ਦੇਵਿਆਂ ਵਾਂਗ ਸਨ ਜੋ ਦੋ
ਦਿਲਾਂ ਦੇ ਮਿਲਣ-ਮਾਰਗ ਨੂਂ ਰਸ਼ਨਾ ਰਹੇ ਹੋਣ। ਚੁਪ੍ਪ ਵਿਚੋਂ ਅਜਿਆ ਮਧੁਰ ਸਂਗੀਤ
ਪੈਦਾ ਕਰ ਰਹੇ ਸਨ ਜੋ ਉਨ੍ਹਾਂ ਦੀ ਧੁਂਦਲੀ ਯਾਦ ਦੇ ਪਰਛਾਵੇਂ ਉਭਾਰ ਰਿਹਾ ਸੀ
ਤੇ ਉਨ੍ਹਾਂ ਦੋਨਾਂ ਨੂਂ ਇਕ ਇਕ ਅਜਿਹੇ ਸਥਾਨ ਤੇ ਦੇਖ ਰਹੇ ਸਨ ਜੋ ਇਸ ਚ਷ਮੇ ਤੇ
ਇਨ੍ਹਾਂ ਦਰਖਤਾਂ ਤੋਂ ਦੂਰ ਪਰਛਾਇਆਂ ਤੇ ਮੁਰਤਾਂ ਨਾਲ ਘਿਰਿਆ ਹੋਇਆ ਹੈ।
ਦੋਨੋ ਇਕ ਦੂਸਰੇ ਵਲ੍ਲ ਪਿਆਰ ਮਂਗਵੀਵਾਂ ਨਜ਼ਰਾਂ ਨਾਲ ਦੇਖ ਰਹੇ ਹਨ ਤੇ ਦੇਨਾਂ
ਨੂਂ ਇਕ ਦੁਸਰੇ ਦੀਆਂ ਅਕ੍ਖਾਂ ਵਿਚ ਪਿਆਰ ਨਜਰੀ ਆਇਆ। ਦੋਨਾਂ ਨੇ ਪ੍ਰੇਮ-
ਕਂਨਾਂ ਨਾਲ ਇਕ ਦੂਸਰੇ ਦੀਆਂ ਆਹਾਂ ਸੁਣੀਆਂ।
          ਉਨ੍ਹਾਂ ਦੋਨਾਂ ਦੀਆਂ ਰੂਹਾ ਨੇ ਆਪਸ ਵਿਚ ਗਲ੍ਲ ਬਾਤ ਕੀਤੀ। ਜਦੋਂ ਉਨ੍ਹਾਂ
ਦੀਆਂ ਰੂਹਾਂ ਨੂਂ ਪੂਰਨ ਜ੍ਞਾਨ ਪ੍ਰਾਪਤ ਹੋ ਜ੍ਞਾ ਅਤੇ ਉਨ੍ਹਾਂ ਦੇ ਪਰਸਪਰ ਮੇਲ
ਮਿਲਾਪ ਹੋ ਜ੍ਞਾ ਤਾਂ ਅਲੀ ਕਿਸੇ ਗੈਬੀ ਷ਕਤੀ ਦੀ ਪ੍ਰੇਰ਩ਾ ਨਾਲ ਨਦੀ ਪਾਰ
ਕਰਕੇ ਉਸ ਲਡ਼ਕੀ ਪਾਸ ਜਾ ਪੁਜ੍ਜਿਆ ਤੇ ਉਸਨੂਂ ਖਿਚ ਕੇ ਆਪਣੇ ਨੇਡ਼ੇ ਕਰ
ਲਿਆ, ਗਲਵਕਡ਼ੀ ਵਿਚ ਘੁਟ੍ਟਿਆ ਤੇ ਉਸਦੇ ਹਟ੍ਠ ਗਰਦਨ ਤੇ ਅਕ੍ਖਾਂ ਚੁਂਮਣ
ਲਗ੍ਗਾ। ਉਹ ਉਸਦੀਆਂ ਵਾਰਾਂ ਚ ਅਹਿਲ ਰਹੀ ਜਿਵੇਂ ਗਲਵਕਡ਼ੀ ਦੇ ਮਧੁਰ
ਆਨਂਦ ਨੇ ਉਸਦੀ ਮਰਜ਼ੀ ਨੂਂ ਦਬਾ ਦਿਤ੍ਤਾ ਸੀ ਅਤੇ ਸਪਰ੍ਸ਼ ਦੀ ਕੋਮਲਤਾ ਨੇ
ਉਸ ਦੇ ਸਾਰੇ ਬਲ ਨੂਂ ਉਸ ਪਾਸੇ ਲਾ ਲਿਆ ਸੀ। ਉਸਨੇ ਆਪਾ ਇਉਂ ਸਮਰ-
ਪਿਤ ਕਰ ਦਿਤ੍ਤਾਂ ਜਿਵੇਂ ਚਮੇਲੀ ਦੀ ਸੁਗਂਧ ਆਪਣਾ ਆਪਾ ਹਵਾ ਨੂਂ ਸਮਰਪਿਤ
ਕਰ ਦਿਂਦੀ ਹੈ। ਉਸ ਨੇ ਆਪਣਾ ਸਿਰ ਉਸਦੇ ਹਿਕ੍ਕ ਤੇ ਰਖ੍ਖ ਦਿਤ੍ਤਾ ਜਿਵੇਂ ਥਕ੍ਕੇ
ਹੋਏ ਨੂਂ ਆਰਾਮ ਕਰਨ ਲਈ ਥਾਂ ਮਿਲ ਗਈ ਹੋਵੇ ਤੇ ਫਿਰ ਉਹ ਲਂਮੇ ਹਉਕੇ
ਲੈਣ ਲਗ੍ਗੀ । ਇਹ ਰਉਕੇ ਇਸ ਇਸ ਗਲ੍ਲ ਦਾ ਪ੍ਰਗਟਾਵਾ ਸਨ ਕੀ ਉਸ ਦਾ ਛੋਟਾ ਜਿਹਾ
ਦਿਲ ਕਿਂਨਾ ਸਂਤੁਸਟ ਹੈ ਤੇ ਉਸਦੇ ਧੁਰ ਅਂਦਰ ਹਲਚਲ ਮਚ੍ਚ ਗਈ ਹੈ ਜੋ ਹੁਣ
ਤਕ੍ਕ ਅਚੇਤ ਸੀ ਪਰ ਹੁਣ ਜਾਗ ਚੁਕ੍ਕਾ ਹੈ। ਉਸ ਨੇ ਆਪਣਾ ਸਿਰ ਚੁਕੀਆ ਅਤੇ
ਅਲੀ ਦੀਆਂ ਅਕ੍ਖਾਂ 'ਚ ਦੇਖਿਆ, ਇਹ ਤਕ੍ਕਣੀ ਅਜਿਹੇ ਸਜ੍ਜਣ ਦੀ ਤਕਡ਼ੀ ਵਰਗੀ
ਸੀ ਜੋ ਮਨੁਕ੍ਖਾਂ ਦੀ ਰਸਮੀ ਭਾਸ਼ਾ ਨੂਂ ਚਪ੍ਪ ਸਾਹਮਣੇ ਨਿਗੁਣੀ ਸਮਝਦਾ ਹੈ ਤੇ ਰੂਹ
ਦੀ ਭਾ਷ਾ ਵਿਚ ਗਲ੍ਲ ਕਰਦਾ ਹੈ। ਇਹ ਅਜਿਹੇ ਵਿਅਕਤੀ ਦੀ ਤਕ੍ਕਡ਼ੀ ਜਿਹੀ ਸੀ
ਜੋ ਇਸ ਗਲ੍ਲ ਨਾਲ ਸਂਤੁ਷ਟ ਨਹੀਂ ਹੁਂਦਾ ਕਿ ਸ਼ਬਦੀ ਜਮੇ ਵਿਚ ਪਿਆਰ ਦੀ
ਰੂਹ ਹੌਣੀ ਚਾਹੀਦੀ ਹੈ।
          ਦੋਨੋ ਪ੍ਰੇਮੀ ਬੈਂਤ ਦੇ ਦਰਖਤਾਂ ਵਿਚ ਚਲਣ ਲਗ੍ਗੇ ਅਤੇ ਉਨ੍ਹਾਂ ਦੋਨਾਂ ਦੀ
ਅਭਿਂਨਤਾ ਅਜਿਹੀ ਭਾਸ਼ਾ ਸੀ ਜੋ ਦੌਨਾਂ ਦੀ ਇਕਮਿਕਤਾ ਦਾ ਕਹਾਣੀ ਕਹਿ ਰਹੀ
ਸੀ ਅਤੇ ਇਕ ਹੀ ਕਂਨ ਸੀ ਜੋ ਚੁਪ੍ਪ ਵਚੋਂ ਪ੍ਰੇਮ ਦੀ ਪ੍ਰੇਰਨਾ ਬਾਰੇ ਸੁਣ ਰਿਹਾ ਸੀ
ਇਕ ਹੀ ਅਕ੍ਖ ਸੀ ਜੋ ਪ੍ਰਸਂਨਤਾ ਦੀ ਸ਼ਾਂਤੀ ਦੇਖ ਰਹੀ ਸੀ। ਭੈਡਾਂ ਉਨ੍ਹਾਂ ਨੇ ਮਗਰ
ਮਗਰ ਚਲ੍ਲ ਰਹੀਆਂ ਸਨ ਜੋ ਘਾਹ ਤੇ ਫੁਲ੍ਲ ਪਤੀਆਂ ਚਲ ਰਹੀਆਂ ਸਨ ਤੇ ਚਾਰੇ
ਪਾਸਿਉਂ ਮੁਗਧ ਕਰਨ ਵਾਲੇ ਗੀਤ ਗਾਉਂਦੇ ਪਚ੍ਛੀ ਉਨ੍ਹਾਂ ਪਾਸ ਆ ਰਹੇ ਸਨ।
          ਜਦੋਂ ਉਹ ਘਾਟੀ ਕਿਨਾਰੇ ਪਹੁਂਚੇ ਤਾਂ ਸੂਰਜ ਚਡ਼੍ਹ ਚੁਕਾ ਸੀ ਅਤੇ ਉਨ੍ਹਾਂ
ਨੇ ਆਪਣੀ ਸੁਨਹਿਰੀ ਚਾਦਰ ਉਚੇ ਟਿਲਿਆਂ ਤੇ ਵਿਛਾ ਦਿਤ੍ਤੀ ਸੀ। ਉਹ ਦੋਨੋ
ਇਕ ਚਟਾਨ ਪਾਸ ਬੈਠ ਗਏ। ਜਿਸ ਦੀ ਛਾਵੇਂ ਬਨਫ਷ੇ ਦੇ ਫੁਲ਼ ਖਿਡ਼ੇ ਹੋਏ ਸਨ।
ਕੁਝ ਚਿਰ ਬੈਠਣ ਮਗਰੇਂ ਲਡ਼ਕੀ ਨੇ ਅਲੀ ਦੀਆਂ ਕਾਲੀਆਂ ਅਕ੍ਖਾਂ ਚ ਦੇਖਿਆ
ਜਦੋਂ ਸੁਗਂਧ ਸਮੀਹ ਦੇ ਮਂਦ ਮਂਦ ਬਲ੍ਲੇ ਮੁਟਿਆਰ ਦੇ ਵਾਲਾਂ ਨਾਲ ਇਉਂ ਖੇਲ
ਰਹੇ ਸਨ ਮਾਨੋਂ ਕੁਦਰਤ ਦੇ ਅਣ-ਦਿਸਦੇ ਹੋਂਠ ਉਸ ਨੂਂ ਚੁਂਮਣ ਲਈ ਬੇਚੈਨ
ਹਨ। ਉਸ ਨੂਂ ਮਹਸੂਸ ਹੋਇਆ ਕਿ ਜਾਦੂ ਭਰੀਆਂ ਉਂਗਲਾਂ ਉਸਦੀ ਰਸਨਾਂ
ਨੂਂ ਪਿਆਰ ਕਰ ਰਹੀਆਂ  ਹਨ ਤੇ ਉਸ ਦੀ ਇਚ੍ਛਾ ਇਕ ਕਦੀ ਵਾਂਗ ਵੇਵਸ੍ਸ ਹੈ।
ਉਹ ਬੋਲੀ ਅਤੇ ਆਪਣੀ ਵੀਲਾਂ ਜਿਹੀ ਮਿਟ੍ਠੀ ਆਵਾਜ਼ ਵਿਚ ਕਹਿਣ ਲਗ੍ਗੀ।</p>

<p></p>

<p></p>

<p></p>

<p></p>

<p>
               0 
</p></body></text></cesDoc>