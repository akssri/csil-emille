<cesDoc id="pun-w-literature-story-lot12m" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-literature-story-lot12m.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Dhup Diyam Unglan De Nishan</h.title>
<h.author>Singh, Maheep</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Lok Sahitya Prakashan - Sugam Sugandh - Amritsar</publisher>
<pubDate>1989</pubDate>
</imprint>
<idno type="CIIL code">lot12m</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 64-71.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-23</date></creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;2760&gt;&lt;Singh, Maheep - Dhup Diyan Unglan&gt;&lt;Sanjeev Gupta&gt;</p>

<p>          "ਸੁਦੇਸ਼ ਦੀ ਸ਼ਾਦੀ ਹੋ ਗਈ ?"
          "ਹੋਈ ਸੀ। ਪਰ ਉਹ ਵੀ ਉਸੇ ਹਾਦਸੇ ਦਾ ਸ਼ਿਕਾਰ ਹੋਇਆ, ਜਿਸ ਦਾ ਮੈਂ ਹਾਂ, ਇਕ੍ਕਲਾ
ਹੀ ਰਹਿਂਦਾ ਹੈ।" ਉਹ ਬਡ਼ੇ ਫਿਕ੍ਕੇਢਂਗ ਨਾਲ ਮੁਸਕਰਾਇਆ।
          "ਹਾਦਸਾ ?" ਉਹ ਬਡ਼ੀ ਮ੍ਮਧਮ ਅਵਾਜ਼ ਵਿਚ ਫੁਸਫੁਸਾਈ।
          "ਕੀ ਉਹ ਇਸ ਵਕਤ ਘਰ ਹੋਵੇਗਾ ? ਉਹ ਤਾਂ ਕਿਸੇ ਦਫਤਰ ਵਿਚ ਕਂਮ ਕਰਦਾ ਹੈ।"
          "ਹਾਂ! ਉਥੇ ਹੀ ਕਿਸੇ ਰੈਸਤਰਾਂ ਵਿਚ ਬੈਠ ਕੇ ਉਸ ਨੂਂ ਉਡੀਕਾਂਗਾ।"
          ਦੋਹਾਂ ਦੇ ਚਿਹਰਿਆਂ ਤੇ ਇਕ ਛਾਂ ਉਭ੍ਭਰ ਆਈ। ਕੁਝ ਪ੍ਪਲਾਂ ਲਈ ਇਕ ਸ੍ਸਨਾਟਾ ਦੋਹਾ ਦੇ
ਵਿਚਕਾਰ ਆ ਗਿਆ।
          ਨਾਰਾਇਣਾ ਜਾਣ ਵਾਲੀ ਬ੍ਬਸ ਆ ਗਈ ਸੀ।
          "ਤੁਸੀ ਚਾਹੋ ਤਾਂ, ਮੇਰੇ ਕੋਲ ਵੀ ਠਹਿਰ ਸਕਦੇ ਹੋ।"
          "ਤੇਰੇ ਕੋਲ ? ਮੇਰਾ ਮਤਲਬ ਹੈ-ਤੁਹਾਡੇ ਨਾਲ ?"
          "ਹਾਂ।" ਉਹ ਬੋਲੀ, "ਅਸੀਂ ਪਤੀ ਪਤਨੀ ਨਾ ਸਹੀ। ਪੁਰਾਣੇ ਰਿਸ਼ਤੇ ਟੁਟ੍ਟਚੁਕੇ ਹਨ
ਪਰ ਅਸੀ ਇਕ ਦੂਜੇ ਦੇ ਦੁਸ਼ਮਣ ਵੀ ਤਾਂ ਨਹੀਂ। ਠੀਕ ਏ ਨਾ ?
          "ਨਹੀਂ-ਨਹੀਂ", ਉਹ ਛੇਤੀ ਛੇਤੀ ਬੋਲਿਆ, "ਦੁਸ਼ਮਣੀ ਤਾਂ ਉਸ ਵਕਤ ਵੀ ਨਹੀ ਹੋਈ
ਜਦੋਂ ਅਸੀਂ ਟੁਟ ਰਹੇ ਸਾਂ। ਅਜੀਤ ਦਾ ਕੀ ਹਾਲ ਹੈ ?"
          "ਚਂਗਾ ਹੈ।" ਨੀਤਾ ਬਸ੍ਸ ਵਲ ਵਧਣ ਲ੍ਲਗੀ, ਫਿਰ ਉਥੇ ਹੀ ਠਿਠਕ ਗਈ। ਉਹ ਉਥੇ ਹੀ
ਖਡ਼ਾ ਸੀ-ਦੁਚਿਤ੍ਤੀ, ਅਨਿਰਣੇ ਤੇ ਪਰੇਸ਼ਾਨੀ ਵਿਚ ਘਿਰਿਆ ਹੋਇਆ। "ਕੀ ਸੋਚ ਰਹੋ ਹੋ ?"
"ਉਹ ਬੋਲੀ-"ਕੋਈ ਇਤਰਾਜ਼ ਹੈ ? ਮੈਂ ਪਿਛਲੀ ਕੋਈ ਵੀ ਗ੍ਗਲ ਨਹੀਂ ਛੇਡ਼ਾਂਗੀ ।"
          ਉਹ ਅਟੈਚੀ ਚੁਕ ਕੇ ਉਸ ਦੇ ਪਿਛ੍ਛੇ-ਪਿਛ੍ਛੇ ਬ੍ਬਸ ਵਿਚ੍ਚ ਚਡ਼੍ਹ ਗਿਆ। ਤਿਂਨ ਸਵਾਰੀਆਂ
ਵਾਲੀ ਸੀਟ ਤੇ ਬਹਿ ਗਏ, ਖੁਲ੍ਹੇ ਖੁਲ੍ਹੇ, ਫੇਰ ਵੀ ਇਕ ਭੁਲੀ ਵਿਸਰੀ, ਪਰ ਬਡ਼ੀ ਹੀ ਪਛਾਣੀ
ਹੋਈ ਮਹਿਕ ਦੋਹਾਂ ਦੇ ਦਰਮਿਆਨ ਤਰ ਆਈ।
          ਉਸ ਨੇ ਫਿਰ ਅਜੀਤ ਦੀ ਗ੍ਗਲ ਛੇਡ਼ੀ-"ਅਜੀਤ ਤਾਂ ਮਸੂਰੀ ਹੀ ਹੋਵੇਗਾ।"
          "ਹਾਂ, ਇਸ ਸਾਲ ਉਸ ਨੇ ਨੌਵੀਂ ਦਾ ਇਸਤਿਹਾਨ ਦਿਤਾ ਹੈ। ਸਕੂਲ ਬਂਦ ਹੁਂਦੇ ਹੀ ਮੈ
ਵੀ ਮਸੂਰੀ ਚਲੀ ਜਾਵਾਂਗੀ।
          "ਉਹ ਛੁਟ੍ਟੀਆਂ ਵਿਚ ਇਥ੍ਥੇ ਨਹੀਂ ਆਉਂਦਾ ?"
          "ਐਨੀ ਗਰਮੀਆਂ ਵਿਚ ਉਸ ਨੂਂ ਇਥ੍ਥੇ ਬੁਲਾ ਕੇ ਕੀ ਕਰਾਂ, ਅਤੇ ਇਸ ਬਹਾਨੇ ਮੈ ਵੀ ਉਥੇ
ਘੁਂਮ ਫਿਰ ਆਉਂਦੀ ਹਾਂ। ਕਦੀ ਅਸੀ ਸ਼ਿਮਲੇ ਚਲੇ ਜਾਂਦੇ ਹਾਂ....ਪਿਛਲੇ ਸਾਲ ਅਸੀਂ ਦੋਵੇ
ਕਸ਼ਮੀਰ ਚਲੇ ਗਏ ਸਾਂ।"
          ਦੋਹਾਂ ਨੂਂ ਕੁਝ ਯਾਦ ਆਇਆ। ਪਿਛੇ ਰਹੇ ਲਂਮੇ ਰੇਗਿਸਤਾਨ ਵਿਚ ਕਿਤੇ ਕੋਈ ਚਮ-
ਚਮਾਉਂਦੀ ਜਿਹੀ ਚੀਜ਼ ਵਿਖਾਈ ਦਿਤੀ, ਇਕ ਦਮ ਦੁਧ ਵਰਗੀ ਰੌਸ਼ਨੀ ਵਾਲੀ ਕੋਈ ਚੀਜ਼
ਝਿਲਮਿਲ਼ਾ ਰਹੀ ਸੀ।
          ਉਙ ਬੋਲਿਆ-"ਜਦੋਂ ਅਸੀਂ ਪਹਿਲੀ ਵਾਰੀ ਕਸ਼ਮੀਰ ਗਏ ਸਾਂ, ਅਜੀਚ ਬਸ ਇਕ ਸਾਲ
ਦਾ ਹੀ ਸੀ।"
          ਉਹ ਖਿਡ਼ਕੀ ਤੋਂ ਬਾਹਰ ਵੇਖਣ ਲਗ ਪਈ। ਸਡ਼ਕ ਦਾ ਕੋਲਤਾਰ ਗਰਮੀ ਕਰਕੇ ਕਿਤੇ-
ਕਿਤੇ ਪਿਘਲਿਆ ਦਿਖਾਈ ਦੇ ਰਿਹਾ ਸੀ। ਬਂਬਸ ਚਲ ਪਈ।
          ਦੋਵੇਂ ਚੁਪ ਸਨ। ਕਦੀ ਬੇਮਤਲਬ ਖਿਡ਼ਕੀ ਤੋਂ ਬਾਹਰ ਵੇਖਣ ਲਗ ਪੈਂਦੇ ਫੇਰ ਹਰ
ਸਟਾਪ ਤੇ ਚਡ਼੍ਹਨ ਵਾਲੀਆਂ ਸਵਾਰੀਆਂ ਦੇ ਚਿਹਰੇ ਮਾਪਣ ਲਗਦੇ।
          "ਸਂਤੋਸ਼ ਦਾ ਕੀ ਹਾਲ ਹੈ ਤੁਹਾਡੀ ਬੀਵੀ ?" "ਚਂਗੀ ਹੈ।"
          "ਦੋ ਬਚ੍ਚੇ ਹਨ ਨਾ ?"
          "ਹਾਂ ਦੋਵੇਂ ਹੀ ਕੁਡ਼ੀਆਂ ਹਨ।"
          ਨੀਤਾ ਨੇ ਇਂਜ ਸਿਰ ਹ੍ਲਾਇਆ ਜਿਵੇਂ ਉਹ ਸਭ ਕੁਝ ਜਾਣਦੀ ਹੋਵੇ।
          "ਅਜੀਤ ਨੂਂ ਵੇਖਿਆਂ ਮੈਨੂਂ ਤਿਂਨ-ਚਾਰ ਸਾਲ ਹੋ ਗਏ।" ਉਹ ਬੋਲਿਆ।
          "ਇਸੇ ਤਰ੍ਹਾਂ ਹੋਰ ਵੀ ਕਿਂਨੇ ਸਾਲ ਨਿਕਲ ਜਾਣਗੇ।" ਉਹ ਬਹੁਤ ਗੁਮਸੁਮ ਜਿਹੀ ਬੋਲੀ।
          ਬਸ੍ਸ ਵਿਚ ਸਵਾਰੀਆਂ ਭਰਦੀਆਂ ਜਾ ਰਹੀਆਂ ਸਨ। ਉਨ੍ਹਾਂ ਦੇ ਕੋਲ ਇਕ ਮੋਟਾ ਜਿਹਾ
ਬਂਦਾ ਆ ਕੇ ਬੈਠ ਗਿਆ। ਉਨ੍ਹਾਂ ਦੇ ਮੋਢੇ ਜੁਡ਼ ਗਏ ਸਨ ਅਤੇ ਕਦੀ-ਕਦੀ ਦੋਹਾਂ ਅਰਕਾਂ ਕਮਰ
ਤੇ ਚਡ਼੍ਹੀ ਹੋਈ ਮਾਸਲਤਾ ਦਾ ਜਾਇਜ਼ਾ ਲੈਣ ਲਗ ਜਾਂਦੀਆਂ ਸਨ। ਇਂਜ ਲਗ ਰਿਹਾ ਸੀ, ਕਿਤੋਂ
ਬਹੁਤ ਦੂਰ ਤੋਂ ਇਕਦਮ ਸੁਨਸਾਨ ਵਿਚ ਬਂਦ ਦਰਵਾਜ਼ਿਆਂ ਤੇ ਹਲਕੀ-ਹਲਕੀ ਦਸਤਕ ਹੋ ਰਹੀ
ਹੋਵੇ। ਬਸ ਸਟਾਪ ਤੇ ਉਤਰ ਕੇ ਉਹ ਪੈਦਲ ਤੁਰ ਪਏ।
          ਸਾਹਮਣੇ ਅਨੇਕਾਂ ਬਿਲਡਿਂਗਾਂ ਦਿਸਣ ਲਗੀਆਂ। ਨੀਤਾ ਨੇ ਆਪਣੀ ਛਤਰੀ ਬਂਦ ਕਰ ਲਈ
ਅਤੇ ਖਬ੍ਬੇ ਪਾਸੇ ਮੁਡ਼ ਕੇ ਕੁਝ ਅਗ੍ਗੇ ਜਾ ਕੇ ਸ੍ਸਜੇ ਪਾਸੇ ਮੁਡ਼ੀ। ਉਹ ਉਸਦੇ ਪਿਛ੍ਛੇ-ਪਿਛ੍ਛੇ
ਚਲ ਰਿਹਾ ਸੀ। ਉਪਰ ਜਾਣ ਵਾਲੀਆਂ ਪੌਡ਼ੀਆਂ ਵਿਖਾਈ ਦੇਣ ਲਗੀਆਂ।
          "ਮੇਰਾ ਫਲੈਟ ਤੀਜੀ ਮਂਜ਼ਿਲ ਤੇ ਹੈ", ਉਹ ਸਂਕੋਚ ਕਰਦੀ ਹੋਈ ਬੋਲੀ। "ਤੁਹਾਨੂਂ
ਪਰੇਸ਼ਾਨੀ ਤਾਂ ਹੋਵੇਗੀ।"
          ਉਸ ਨੇ ਮੁਸਕਰਾਉਣ ਦੀ ਭਰਪੂਰ ਕੋਸ਼ਿਸ਼ ਕੀਤੀ, ਪਰ ਉਸ ਨੂਂ ਮਹਿਸੂਸ ਹੋਇਆ ਕਿ ਉਸ
ਦੇ ਅਂਦਰ ਬਡ਼ੀ ਟੁਟ੍ਟ-ਫੁਟ੍ਟ ਹੋ ਗਈ ਹੈ। ਉਹ ਇਕ ਦਮ ਉਘਡ਼ ਗਿਆ ਹੈ। ਉਸ ਦੇ ਮਨ ਵਿਚ
ਆ ਰਿਹਾ ਸੀ, ਉਸ ਨੂਂ ਨੀਤਾ ਦੇ ਨਾਲ ਨਹੀਂ ਸੀ ਆਉਣਾ ਚਾਹੀਦਾ। ਸਤ-ਅਠ ਸਾਲ ਦੇ ਟੁਟੇ
ਵਿਚ ਅਚਨਚੇਤ ਮੁਲਾਕਾਤ ਨੂਂ ਉਂਜ ਵੀ ਸਹੇਜ ਸਕਣਾ ਨਹੀਂ ਸੀ। ਪਰ, ਉਹ ਤਾਂ ਉਸ ਦਾ
ਰਿਸ਼ਤਿਆਂ ਤੋ ਬਾਅਦ ਪ੍ਰਾਹੁਣਾ ਬਣ ਕੇ ਉਸ ਦੇ ਨਾਲ ਆਇਆ ਹੈ ਇਥੇ ਰਾਤ ਕ੍ਕਟਣ ਲਈ ਇਹੋ
ਜਿਹੇ ਫਲੈਟ ਵਿਚ ਜਿਥੇ ਨੀਤਾ ਇਕ੍ਕਲੀ ਰਹਿਂਦੀ ਹੈ।
          ਤੀਜੀ ਮਂਜ਼ਿਲ ਤਕ ਆਉਂਦੇ ਆਉਂਦੇ ਉਹ ਸਚ੍ਚਮੁਚ੍ਚ ਹਫ਼ਣ ਲਗ ਪਿਆ। ਨੀਤਾ ਦਾ ਸਾਹ
ਵੀ ਤੇਜ਼ ਹੋ ਗਿਆ ਸੀ।
          ਉਹ ਅਗ੍ਗੇ ਵਧ ਕੇ ਆਪਣੇ ਫਲੈਟ ਦਾ ਬੂਹਾ ਖੋਲ੍ਹਣ ਲਗੀ। ਬੂਹੇ ਦੇ ਬਾਹਰ ਅਨੇਕ ਗਮਲੇ
ਰਖੇ ਹੋਏ ਸਨ। ਸਾਰਿਆਂ ਵਿਚ ਵਖ੍ਖੋ-ਵਖ੍ਖ ਕਿਸਮਾਂ ਦੇ ਖੂਬਸੂਰਤ ਕੈਕਟਸ ਲਗੇ ਸਨ।
          ਅਂਦਰ ਆਉਂਦੇ ਹੀ ਵਡ੍ਡੇ ਕਮਰੇ ਵਿਚ ਸ੍ਸਜੇ ਪਾਸੇ ਸੋਫ਼ਾ ਸੈਟ, ਟੈਲੀਵਿਜ਼ਨ ਅਤੇ
ਕਿਤਾਬਾਂ ਦੀ ਇਕ ਅਸਮਾਰੀ ਰਖੀ ਸੀ। ਖਬ੍ਬੇ ਪਾਸੇ ਡਾਇਨਿਂਗ ਟੇਬਲ ਰਖਿਆ ਸੀ।
          ਉਹ ਸੋਫੇ ਤੇ ਬੈਠ ਗਿਆ। ਨੀਤਾ ਨੇ ਪ੍ਪਖਾ ਚਲਾ ਦਿਤ੍ਤਾ। ਗਰਮੀ, ਹੁਂਮਸ ਅਤੇ ਤਪਸ਼ ਵਿਚ
ਠਂਡੀ ਹਵਾ ਦੇ ਬੁਲੇ ਆਉਣ ਲ੍ਲਗੇ।
          ਨੀਤਾ ਸ਼ਰਬਤ ਦਾ ਜਗ ਅਤੇ ਦੋ ਖਾਲੀ ਗਲਾਸ ਲੈ ਕੇ ਆਈ ਤਾਂ ਉਸ ਵੇਖਿਆ, ਉਸ ਨੇ
ਹਾਊਸ ਕੋਟ ਪਾ ਲਿਆ ਸੀ ਉਸ ਨੇ ਆਪਣੇ ਵਾਲ ਖਲ੍ਲ੍ਹੇ ਛਡ੍ਡ ਦਿਤ੍ਤੇ ਸਨ, ਮੂਂਹ ਤੇ ਤਾਜ਼ੇ ਪਾਣੀ
ਦੀ ਨਮੀ ਫੈਲੀ ਹੋਈ ਸੀ।
          "ਪਾਣੀ ਪੀ ਲਵੋ, ਫੇਰ ਕੁਝ ਅਰਾਮ ਕਰ ਲਵੋ। ਷ਾਮ ਨੂਂ ਕਿਤੇ ਜਾਣਾ ਤਾਂ ਨਹੀਂ ?"
          "ਸੋਚਦਾ ਹਾਂ-ਸੁਦੇਸ਼ ਨੂਂ ਮਿਲ ਲਵਾਂ।"
          "ਠੀਕ ਹੈ, ਪਰ ਧੁਪ੍ਪ ਢਲ ਜਾਣ ਤੋਂ ਬਾਅਦ ਜਾਣਾ!" ਦੋਵੇ ਸ਼ਰਬਤ ਪੀਣ ਲਗ ਪਏ।
          ਉਹ ਉਠੀ। ਇਕ ਚਿਟ੍ਟੀ ਚਾਦਰ ਲਿਆ ਕੇ ਉਸ ਨੇ ਦਿਵਾਨ ਤੇ ਵਿਛੇ ਮਖਮਲੀ ਕਵਰ ਦੇ
ਉਪਰ ਵਿਛਾ ਦਿਤ੍ਤੀ, ਇਕ ਸਰਹਾਣਾ ਰਖ੍ਖ ਦਿਤ੍ਤਾ, ਜਿਸ ਤੇ ਦੁਧ੍ਧ ਵਾਗ ਚਮਕਦਾ ਗਿਲਾਫ
ਚਡ਼੍ਹਿਆ ਸੀ।
          "ਲਓ, ਕੁਝ ਚਿਰ ਆਰਾਮ ਕਰ ਲਓ।" ਕਹਿ ਕੇ ਸੈਂਟਰ ਟੇਬਲ ਤੇ ਰਖਿਆ ਜਗ ਅਤੇ
ਗਿਲਾਸ ਚੁਕਣ ਲ੍ਲਗੀ।
          "ਬਾਥਰੂਮ ਕਿਧਰ ਹੈ?" ਉਸ ਨੂਂ ਜਾਪਿਆ ਉਹ ਬਡ਼ਾ ਮਾਸੂਮ ਬਂਦਾ ਹੈ, ਸਾਹਮਣੇ ਖਡ਼੍ਹੀ
ਜ਼ਨਾਨੀ ਬਹੁਤ ਬਡ਼ੀ ਸ਼ਖਸੀਅਤ ਦੀ ਮਾਲਕਿਨ ਹੈ...ਉਚ੍ਚੀ, ਰੋਹਬਦਾਰ...ਤਾਕਤ ਨਾਲ
ਭਰੀ ਪੂਰੀ...
          ਉਹ ਉਸ ਦੇ ਪਿਛੇ-ਪਿਛੇ ਤੁਰ ਪਿਆ। ਛੋਟੇ ਜਿਹੇ ਗਲਿਆਰੇ ਵਿਚ ਸ੍ਸਜੇ ਪਾਸੇ ਰਸੋਈ,
ਖਬ੍ਬੇ ਪਾਸੇ ਇਕ ਬੈਡਰੂਮ ਅਤੇ ਸਾਹਮਣੇ ਬਾਥਰੂਮ।
          ਉਹ ਬਾਥਰੂਮ ਤੋਂ ਨਿਕਲ ਕੇ ਵਾਸ਼ਬੇਸਿਨ ਵਿਚ ਹਥ੍ਥ ਧੋਣ ਲ੍ਲਗਾ। ਫੇਰ ਜਦੋਂ ਉਹ ਵਡ੍ਡੇ
ਕਮਰੇ ਵਲ੍ਲ ਜਾ ਰਿਹਾ ਸੀ, ਉਸ ਚੋਰ ਅਖ੍ਖਾਂ ਨਾਲ ਵੇਖਿਆ-ਨੀਤਾ ਪਲਂਗ ਤੇ ਲੇਟੀ ਹੋਈ ਕੁਝ
ਪਡ਼੍ਹ ਰਹੀ ਸੀ।
          ਉਹ ਲਂਮਾ ਪੈ ਗਿਆ। ਉਸ ਨੂਂ ਲਗਿਆ ਪਤਾ ਨਹੀਂ ਕਿਂਨੀਆ ਹੀ ਰਲੀਆਂ ਮਿਲੀਆਂ
ਯਾਦਾਂ ਉਹ ਦੇ ਦਿਲ ਦਿਮਾਗ ਵਿਚ ਘੁਲ ਰਹੀਆਂ ਸਨ। ਇਸ ਜਨਾਨੀ ਦੇ ਨਾਲ ਉਸ ਨੇ ਸ੍ਸਤ
ਸਾਲ ਤੋਂ ਵਧੀਕ ਸਮਾਂ ਪਤੀ ਦੇ ਰੂਪ ਵਿਚ ਗੁਜ਼ਾਰਿਆ ਸੀ।
          ਸ਼ਾਦੀ ਦੇ ਪਹਿਲੇ ਦਿਨ ਤੋਂ ਹੀ ਉਸ ਨੂਂ ਲਗਿਆ ਸੀ, ਸਾਰਾ ਕੁਝ ਉਸ ਤਰ੍ਹਾਂ ਦਾ ਨਹੀਂ ਸੀ
ਜਿਵੇਂ ਉਸ ਨੇ ਸੋਚਿਆ ਸੀ। ਉਸ ਨੂਂ ਲਗਿਆ ਸੀ ਨੀਤਾ ਵਿਚ ਕੁਝ ਅਜੀਬ ਕਿਸਮ ਦਾ ਘੁਮਂਡ
ਹੈ। ਜਦੋਂ ਉਹ ਨਹੀਮ ਕਹਿਂਦੀ ਹੈ ਤਾਂ ਉਸ ਦਾ ਭਾਵ' ਨਹੀ ਹੀ ਹੁਂਦਾ ਹੈ ਅਤੇ 'ਹਾਂ' ਕਦੇ
ਵੀ ਨਹੀਂ ਕਹਿਂਦੀ। ਉਸ ਦਾ "ਨਹੀਂ" ਨਾ ਕਹਿਣਾ ਹੀ "ਹਾਂ" ਹੁਂਦਾ ਹੈ। 'ਹਾਂ' ਦੇ ਪਲ ਵਿਚ
ਵੀ ਉਹ ਖੁਲ੍ਹਦੀ ਨਹੀਂ। ਪਤਾ ਨਹੀਂ ਕਿਹਡ਼ੀ ਗ੍ਗਲ ਉਸ ਨੂਂ ਘੇਰੇ ਰਹਿਂਦੀ ਹੈ, ਢਕੇ ਰਹਿਂਦੀ ਹੈ,
ਜਿਸ ਵਿਚੋਂ ਉਹ ਬਾਹਰ ਨਹੀਂ ਨਿਕਲਦੀ। ਉਸ ਨੂਂ ਲਗਿਆ ਸੀ, ਬਡ਼ਾ ਘਟ੍ਟ ਹਸ੍ਸਦੀ ਹੈ।
ਉਹ ਬਸ ਮੁਸਕਰਾਂਦੀ ਹੈ। ਉਸ ਦੀ ਮੁਸਕੁਰਾਹਟ ਵਿਚ ਹੀ ਘੁਮਂਡ ਗੁਮਾਨ ਜਿਹਾ ਝਰਦਾ ਹੈ।
          ਉਸ ਦੇ ਅਂਦਰਲੇ ਮਰਦ ਨੂਂ ਡੂਂਘੀ ਸ੍ਸਟ-ਵਜ੍ਜੀ ਸੀ। ਉਹ ਅਂਦਰ ਹੀ ਅਂਦਰ ਜਿਵੇਂ ਚੀਕ
ਪਿਆ ਸੀ। ਇਂਨਾ ਹੀ ਮਿਜ਼ਾਜ਼ ਹੈ ਤਾਂ ਮੇਰੇ ਨਾਲ ਸ਼ਾਦੀ ਕਰਨ ਦੀ ਕੀ ਲੋਡ਼ ਸੀ। ਉਸ ਨੂਂ
ਲਗਿਆ ਸੀ, ਨੀਤਾ ਨੇ ਜਿਵੇਂ ਉਸ ਨਾਲ ਬਡ਼ੀ ਬੇਦਿਲੀ ਨਾਲ ਵਿਆਹ ਕਰਾਇਆ ਹੋਵੇ।
          ਫਿਰ ਇਹ ਕ਷ਮਕਸ਼ ਸਤ ਸਾਲ ਤਕ ਲਗਾਤਾਰ ਚਲਦੀ ਰਹੀ। ਉਹ ਕਹਿਂਦਾ..."ਤੁਂ
ਕਿਸੇ ਮਜਬੂਰੀ ਨਾਲ, ਆਪਣੀ ਮਰਜ਼ੀ ਦੇ ਖਿਲਾਫ਼, ਕਿਸੇ ਦਬਾਅ 'ਚ ਆ ਕੇ ਮੇਰੇ ਨਾਲ
ਵਿਆਹ ਨਹੀਂ ਕੀਤਾ। ਮੇਰੇ ਤੇ ਤੁਹਾਡੇ ਮਾਪਿਆਂ ਵਿਚ ਗਲਬਾਤ ਹੋਈ ਸੀ। ਉਨ੍ਹਾਂ ਮੈਥੋਂ ਵੀ
ਪੁਛਿਆ ਸੀ। ਮੈਨੂਂ ਇਨਕਾਰ ਕਰਨਾ ਹੁਂਦਾ ਤਾਂ ਇਨਕਾਰ ਕਰ ਦਿਂਦੀ...ਮੇਰੇ ਤੇ ਕੋਈ ਵੀ ਦਬਾਅ
ਨਹੀਂ ਸੀ।"
          "ਕੀ ਤੂਂ ਆਪਣੀ ਮਰਜ਼ੀ ਨਾਲ ਮੇਰੇ ਨਾਲ ਸ਼ਾਦੀ ਕੀਤੀ ਹੈ ?" ਉਹ ਪੁਛਦਾ।
          "ਇਸ ਸ਼ਾਦੀ ਲਈ ਮੈਂ ਕਦੇ ਆਪਣੀ ਅਨਿਛ੍ਛਾ ਨਹੀਂ ਪ੍ਰਗਟ ਕੀਤੀ।"
          "ਅਨਿਛ੍ਛਾ ਪ੍ਰਗਟ ਕਰਨਾ ਇਕ ਗ੍ਗਲ ਹੈ ਅਤੇ ਮਰਜ਼ੀ ਨਾਲ ਕਂਮ ਕਰਨਾ ਇਕਦਮ ਦੂਜੀ
ਗ੍ਗਲ ਹੈ।" ਉਹ ਬਹੁਤ ਜ਼ੋਰ ਦੇ ਕੇ ਕਹਿਂਦਾ।
          "ਦੇਖੋ, ਸ਼ਾਦੀ ਤੋਂ ਪਹਿਲਾਂ ਤਾਂ ਅਸੀਂ ਇਕ ਦੂਜੇ ਨੂਂ ਜਾਣਦੇ ਵੀ ਨਹੀਂ ਸੀ। ਇਸ ਲਈ
ਤੁਹਾਡੇ ਨਾਲ ਸ਼ਾਦੀ ਕਰਨ ਦੀ ਇਛ੍ਛਾ ਮੇਰੇ ਅਂਦਰ ਕਿਵੇਂ ਹੋ ਜਾਂਦੀ ?" ਉਹ ਕਹਿਂਦੀ, "ਉਸ
ਹਾਲਤ ਵਿਚ ਅਨਿਛ੍ਛਾ ਦਾ ਨਾ ਹੋਣਾ ਹੀ ਕਾਫੀ ਸੀ। ਕਿਸੇ ਵੀ ਚੀਜ਼ ਵਿਚ ਇਛ੍ਛਾ ਉਸ ਨੂਂ ਜਾਨਣ,
ਉਸ ਦੇ ਸਂਪਰਕ ਵਿਚ ਆਉਣ ਮਗਰੋਂ ਹੀ ਪੈਦਾ ਹੁਂਦੀ ਹੈ।"
          "ਪਰ ਹੁਣ ਤਾਂ ਤੂਂ ਮੈਨੂਂ ਜਾਣਦੀ ਹੈ, ਇਂਨੇ ਚਿਰ ਤੋਂ ਮੇਰੇ ਨਾਲ ਹੈ, ਪਰ ਫਿਰ ਵੀ
ਤੇਰੇ ਵਿਚ ਮੈਨੂਂ ਉਹ ਤਾਂਘ, ਉਹ ਇਛ੍ਛਿਆ ਨਜ਼ਰ ਨਹੀਂ ਆਉਁਦੀ ਜੋ ਹੋਣੀ ਚਾਹੀਦੀ ਹੈ।" ਉਹ
ਆਖਦਾ।
          ਇਸ ਗ੍ਗਲ ਤੇ ਨੀਤਾ ਚੁਪ ਕਰ ਜਾਂਦੀ। ਉਹ ਕੁਝ ਵੀ ਨਾ ਆਖਦੀ। ਉਟ ਕੇ ਘਰ ਦੇ ਕਂਮ
ਕਾਜ ਵਿਚ ਲਗ ਜਾਂਦੀ, ਜਾਂ ਨਿਕ੍ਕੇ ਅਜੀਤ ਨੂਂ ਦੁਧ੍ਧ ਪਿਆਉਣ ਲਗਦੀ। ਉਹ ਸੋਚਦਾ ਉਹ ਜ਼ਨਾਨੀ
ਬਡ਼ੇ ਦਿਮਾਗ ਵਾਲੀ ਹੈ-ਪਤਾ ਨਹੀਂ ਇਹ ਆਪਣੇ ਆਪ ਨਬਂ ਕੀ ਸਮਝਦੀ ਹੈ।
          ਉਨ੍ਹਾਂ ਸ੍ਸਤਾਂ ਵਰ੍ਹਿਆਂ ਦੇ ਦਰਮਿਆਨ ਨੀਤਾ ਪਹਿਲਾਂ ਤੋਂ ਵਧ੍ਧ ਆਪਣੇ ਘੇਰਿਆ ਵਿਚ
ਘਿਰਦੀ ਗਈ ਅਤੇ ਉਹ ਲਗਾਤਾਰ ਆਪਣੇ ਆਪ ਨੂਂ ਹੋਰ ਛੋਟਾ ਹੁਂਦਾ ਮਹਸੂਸ ਕਰਦਾ ਰਿਹਾ।
ਅਜੀਤ ਦੇ ਜਨਮ ਤੋਂ ਬਾਦ ਤਾਂ ਨੀਤਾ ਦੀ 'ਨਹੀਂ' ਕੁਝ ਹੋਰ ਵਧ੍ਧ ਗਈ ਸੀ। ਉਹ 'ਨਹੀਂ' ਨਾ
ਕਹਿਣ ਦੇ ਇਕ ਦਿਨ ਨੂਂ ਤਰਸਦਾ ਸੀ। ਘਰ ਤੋਂ ਬਾਹਰ ਉਹ ਬੇਚੈਨ ਰਹਿਂਦਾ ਸੀ। ਘਰ
ਆਉਂਦਾ ਤਾਂ ਉਸ ਦੀਆ ਨਸਾਂ ਮਰੋਡ਼ੇ ਖਾਣ ਲਗਦੀਆਂ। ਕਦੀ ਉਸ ਕੋਲੋਂ ਷ੀ਷ੇ ਦਾ ਗਲਾਸ
ਟੁਟ੍ਟ ਜਾਂਦਾ, ਕਦੀ ਚਾਹ ਦਾ ਪਿਆਲਾ ਹਥ੍ਥੋਂ ਤਿਲਕ ਪੈਂਦਾ। ਪਰ ਨੀਤਾ ਕਦੀ ਕੁਝ ਨਾ ਕਹਿਂਦੀ।
ਉਹ ਚੁਪ ਚੁਪੀਤੀ ਟੁਟ੍ਟੇ ਹੋਏ ਗਲਾਸ ਦੇ ਟੁਕਡ਼ਿਆਂ ਨੂਂ ਹੂਂਝ ਕੇ ਕੂਡ਼ੇਦਾਨ ਵਿਚ ਪਾ ਆਉਂਦੀ ਤੇ
ਪੋਚਾ ਚੁਕ ਕੇ ਫ਼ਰ਷ ਸਾਫ ਕਰ ਦਿਂਦੀ।
          ਫੇਰ ਪਤਾ ਨਹੀਂ ਕੀ ਹੋਇਆ-ਉਹ ਅਜੀਤ ਦੇ ਪ੍ਰਤੀ ਬਡ਼ਾ ਸਖ਼ਤ ਤੇ ਹਿਂਸਕ ਹੋ ਗਿਆ।
          ਉਸ ਨੂਂ ਯਾਦ ਅਇਆ, ਉਸ ਨੇ ਅਜੀਤ ਨੂਂ ਇਤ ਚਪੇਡ਼ ਮਾਰ ਦਿਤ੍ਤੀ ਸੀ। ਉਸ ਦੀਆਂ
ਪਂਜੇ ਉਂਗਲਾਂ ਦਾ ਨਕਸ਼ਾ ਅਜੀਤ ਦੀਆਂ ਕੂਲੀਆਂ ਗ੍ਗਲ੍ਹਾਂ ਤੇ ਉਭਰ ਆਇਆ ਸੀ। ਨੀਤਾ ਨੇ
ਦੌਡ਼ ਕੇ ਅਜੀਤ ਨੂਂ ਆਪਣੀ ਹਿਕ੍ਕ ਨਾਲ ਲਾ ਲਿਆ ਸੀ, ਅਤੇ ਬਡ਼ੀ ਤਿਖ੍ਖੀ ਨਜ਼ਰ ਨਾਲ ਉਸ ਵਲ੍ਲ
ਵੇਖ ਕੇ ਪੁਛਿਆ ਸੀ-"ਕਿਸ ਤੋਂ ਬਦਲਾ ਲੈ ਰਹੇ ਹੋ ?"
          ਉਹ ਗੁਂਮ ਸੁਂਮ ਹੋ ਗਿਆ ਸੀ। ਫੇਰ ਹੌਲੀ ਜਿਹੀ ਬੋਲਿਆ ਸੀ-ਆਪਣੇ ਆਪ ਤੋਂ।
          "ਇਸ ਕਂਮ ਲਈ ਇਸ ਮਾਸੂਮ ਨੂਂ ਨਾ ਵਰਤੋ।" ਆਖ ਨੇ ਨੀਤਾ ਦੂਜੇ ਕਮਰੇ ਵਿਚ ਚਲੀ
ਗਈ ਸੀ।
          ਫੇਰ ਬਡ਼ਾ ਕੁਝ ਬਹੁਤ ਤੇਜ਼ੀ ਨਾਲ ਟੁਟ੍ਟਣ ਲਗ ਪਿਆ ਸੀ। ਜਦੋਂ ਬਿਲਕੁਲ ਟੁਟ੍ਟਣ ਦੀ
ਨੌਬਤ ਆਈ ਤਾਂ ਨੀਤਾ ਨੇ ਇਂਨਾ ਹੀ ਕਿਹਾ ਸੀ-"ਕੋਰਟ-ਕਚਹਿਰੀ ਦੇ ਚਕਰ ਕਟ੍ਟਣ ਦੀ ਮੇਰੀ
ਇਛ੍ਛਾਂ ਨਹੀਂ ਹੈ। ਮੈਨੂਂ ਤੁਹਾਡੇ ਤੋਂ ਕੁਝ ਵੀ ਨਹੀਂ ਚਾਹੀਦਾ। ਆਪਣੀ ਨੌਕਰੀ ਤੋਂ ਮੈਂ ਆਪਣਾ
ਅਤੇ ਅਜੀਤ ਦਾ ਗੁਜ਼ਾਰਾ ਚਂਗੀ ਤਰ੍ਹਾਂ ਕਰ ਲਵਾਂਗੀ...ਮੇਰੇ ਵਲੋਂ ਤੁਸੀਂ ਆਜ਼ਾਦ ਹੋ।"
          ਉਸ ਵਕਤ ਉਹ ਰਮੇਸ਼ ਨਗਰ ਦੇ ਡਬਲ-ਸਟੋਰੀ ਮਕਾਨ ਵਿਚ ਰਹਿਂਦੇ ਸਨ। ਉਹ ਆਪਣਾ
ਥੋਡ਼੍ਹਾ ਜਿਹਾ ਸਮਾਨ ਲੈ ਕੇ ਸੁਦੇਸ਼ ਦੇ ਘਰ ਚਲਾ ਗਿਆ ਸੀ।
          ਸੁਦੇਸ਼ ਨੇ ਵਿਗਡ਼ੀ ਗਂਗਲ ਬਨਾਉਣ ਲਈ ਬਡ਼ੀ ਕੋਸ਼ਿਸ਼ ਕੀਤੀ। ਦੋਵੇਂ ਪਰਿਵਾਰਾਂ ਦੇ ਲੋਕਾਂ
ਨੇ ਵੀ ਕੁਝ ਕਰਨ ਦਾ ਬਡ਼ਾ ਯਤਨ ਕੀਤਾ ਸੀ। ਪਰ, ਗ੍ਗਲ ਨਹੀਂ ਸੀ ਬਣਦੀ। ਨੀਤਾ ਪਹਿਲਾਂ
ਤੋਂ ਵੀ ਵਧ੍ਧ ਗਂਭੀਰ ਹੋ ਗਈ ਸੀ। ਉਸ ਦੀ ਇਹ ਗਂਭੀਰਤਾ ਉਸ ਦੇ ਮਰਦ ਹਉਮੇ ਨੂਂ ਡੂਂਘੀ ਚੋਟ
ਪਹੁਂਚਾਉਂਦੀ ਸੀ। ਉਹ ਤਡ਼ਉ ਉਠਦਾ ਸੀ।
          ਉਹਦਾ ਤਬਾਦਲਾ ਸਹਾਰਨਪੁਰ ਦੇ ਜ਼ੋਨਲ ਦਫਤਰ ਵਿਚ ਹੋ ਗਿਆ ਅਤੇ ਵਕਤ ਹੁਮ
ਭਰੀਆਂ ਰਾਤਾਂ ਵਾਂਗ ਸਤਾਂਦਾ ਹੋਇਆ ਅਤੇ ਗਲਾਂਦਾ ਹੋਇਆ ਗੁਜ਼ਰਣ ਲਗ ਪਿਆ। ਤਿਂਨ ਵਰ੍ਹਿਆਂ
ਬਾਅਦ ਉਸ ਨੇ ਸਂਤੋਸ਼ ਨਾਲ ਵਿਆਹ ਕਰਾ ਲਿਆ। ਵਡ੍ਡੇ ਬਾਬੂ ਦੀਆਂ ਛੇ ਕੁਡ਼ੀਆਂ ਵਿਚੋਂ ਇਕ...
ਇਕਦਮ ਸਿਧ੍ਧੀ ਸਾਦੀ...ਮਾਮੂਲੀ ਜਿਹੀ ਕੁਡ਼ੀ ਜਿਹਡ਼ੀ ਛਿਟ ਮਾਤਰ ਹਸ੍ਸ ਪੈਂਦੀ ਅਤੇ ਦੂਜੇ ਹੀ
ਛਿਣ ਮੂਂਹ ਸੁਜਾ ਕੇ ਮਂਜੇ ਤੇ ਪੁਠ੍ਠੀ ਜਾ ਪੈਂਦੀ।
          ਅਚਨਚੇਤ ਤ੍ਰਬਕ ਕੇ ਉਹ ਬੈਠਾ..."ਇਂਜ ਲਗਦਾ ਹੈ ਮੈਂ ਬਡ਼ੀ ਦੇਰ ਸੁਤ੍ਤਾ ਰਿਹਾ।"
          ਨੀਤਾ ਉਸ ਦੇ ਸਾਹਮਣੇ ਸੋਫੇ ਤੇ ਬੈਠੀ ਕਰੇਲੇ ਛਿਲ ਰਹੀ ਸੀ। ਉਹ ਮੁਸਕਰਾਂਦੀ ਹੋਈ
ਬੋਲੀ, ਉਹੀ ਮੁਸਕਰਾਹਟ, ਜਿਸ ਵਿਚੋਂ ਸਦਾ ਉਸ ਨੂਂ ਉਸ ਦਾ ਘੁਮਂਡ ਝਰਦਾ ਹੋਇਆ ਵਿਖਾਈ
ਦੇਂਦਾ ਸੀ...
          "ਤੁਸੀ ਬਡ਼ੇ ਸਵੇਰੇ ਘਰੋ ਤੁਰੇ ਹੋਵੋਗੇ।"
          "ਹਾਂ।" ਉਹ ਉਠਿਆ ਅਤੇ ਬਾਥਰੂਮ ਵਲ੍ਲ ਚਲਾ ਗਿਆ।
          ਮੁਡ਼ ਕੇ ਆਇਆ ਤਾਂ ਨੀਤਾ ਚਾਹ, ਪਕੋਡ਼ੇ ਅਤੇ ਬਰੈਡ-ਸਲਾਈਸ ਲੈ ਆਈ-"ਤੁਹਾਨੂਂ
ਭੁਖ੍ਖ ਲ੍ਲਗੀ ਹੋਵੇਗੀ ?" ਉਹ ਬੋਲੀ।
          "ਨਹੀਂ, ਕੋਈ ਖਾਸ ਨਹੀਂ।" ਉਹ ਦੋ ਸਲਾਈਸਾਂ ਦੇ ਵਿਚਕਾਰ ਪਕੌਡ਼ੇ ਦਬਾਉਂਦਾ
ਬੋਲਿਆ-"ਸਂਤੋ਷ ਨੇ ਦੁਪਿਹਰ ਲਈ ਦੋ ਪਰੌਂਠੇ ਬਣਾ ਦਿਤ੍ਤੇ ਸਨ।"
          ਉਸ ਨੇ ਨੀਤਾ ਦੇ ਚਿਹਰੇ ਨੂਂ ਪਡ਼੍ਹਨਾ ਚਾਹਿਆ। ਉਥੇ ਕੀ ਸੀ? ਵਿਅਂਗ, ਟਿਚਕਰ ਜਾਂ
ਬ੍ਬਸ ਮਾਮੂਲੀ ਕਥਨ, ਉਸੇ ਪੁਰਾਣੇ ਘੁਮਂਡ ਦੇ ਧਾਗੇ ਵਿਚ ਪਰੋਇਆ ਹੋਇਆ, ਜਿਵੇਂ ਕੋਈ ਅਫਸਰ
ਹੁਣ ਕਿਸੇ ਦੂਸਰੇ ਦਫਤਰ ਵਿਚ ਕਂਮ ਕਰਨ ਵਾਲੇ ਆਪਣੇ ਪੁਰਾਣੇ ਮਾਤਹਿਤ ਨੂਂ ਕਹੇ-ਸਾਬਾਸ਼ !
ਮੈਨੂਂ ਪਤਾ ਲਗਾ ਹੈ ਕਿ ਤੁਸੀਂ ਦਫਤਰ ਵਿਚ ਚਂਗੀ ਤਰ੍ਹਾਂ ਕਂਮ ਕਰ ਰਹੇ ਹੋ।
          ਉਹ ਸਂਤੋਸ਼ ਬਾਰੇ ਸੋਚਣ ਲਗਿਆ। ਉਹ ਸਚਿਉਂ ਉਸ ਦਾ ਬਡ਼ਾ ਖਿਆਲ ਰਖਦੀ ਹੈ।
ਪਰੌਂਠੇ ਬਣਾਉਂਦੀ ਹੈ, ਪੂਰੀਆਂ ਤਲਦੀ ਹੈ, ਕਪਡ਼ੇ ਧੋਂਦੀ ਹੈ, ਘਰ ਦੀ ਸਫਾਈ ਕਰਦੀ ਹੈ...
ਉਸ ਦੀਆਂ ਜੁਤ੍ਤੀਆਂ ਪਾਲਿਸ਼ ਕਰਦੀ ਹੈ। ਪਰ, ਹਾਂ, ਇਹ ਕੀ ਹੈ...ਕੀ ਹੈ ਇਹ ਪਰ...ਸਂਤੋਸ਼
ਕਿਸੇ ਗ੍ਗਲ ਲਈ 'ਨਾ' ਨਹੀਂ ਕਰਦੀ। ਉਹ ਸਾਰੀ ਗ੍ਗਲ ਛੇਤੀ ਮਂਨ ਜਾਂਦੀ ਹੈ। ਉਹ ਵੀ ਪਤਾ
ਨਹੀਂ ਲਗਦਾ ਕਿ ਉਸ ਦੀ ਆਪਣੀ ਇਛ੍ਛਾ ਕੀ ਹੈ...ਅਨਿਛ੍ਛਾ ਕਿਥੇ ਹੈ...ਜਿਵੇਂ ਉਹ ਸਿਰਫ
ਰਬਡ਼ ਦੀ ਲਗਦੀ ਹੋਵੇ...ਜਦੋਂ ਚਾਹੋ..ਜਿਵੇਂ ਚਾਹੋ ਉਸ ਨੂਂ ਮਰਜ਼ੀ ਦੀ ਸ਼ਕਲ ਦੇ ਲਵੋ।
          ਫੇਰ ਉਸ ਨੂਂ ਅਹਿਸਾਸ ਹੋਇਆ...ਨੀਤਾ ਇਕ੍ਕਲੀ ਇਸ ਫਲੈਟ ਵਿਚ ਰਹਿਂਦੀ ਹੈ।
          ਉਹ ਬੋਲਿਆ..."ਸੁਦੇਸ਼ ਘਰ ਆ ਗਿਆ ਹੋਵੇਗਾ। ਮੈਂ ਆਪਣੀ ਅਟੈਚੀ ਵੀ ਲੈ ਜਾਂਦਾ
ਹਾਂ।...ਰਾਤ ਉਥੇ ਹੀ ਕਟ੍ਟ ਲਵਾਂਗਾ।"
          "ਕਿਉਂ?" ਨੀਤਾ ਨੇ ਪੁਛਿਆ ਜਿਵੇਂ ਉਹ ਕੋਈ ਬਹੁਤ ਅਨਹੋਣੀ ਗ੍ਗਲ ਕਹਿ ਰਿਹਾ ਹੋਵੇ।
          "ਮੈਂ ਸੋਚਦਾ ਹਾਂ, ਤੁਹਾਨੂਂ ਪਰੇਸ਼ਾਨੀ ਹੋਵੇਗੀ ?"
          "ਤੁਸੀਂ ਸੁਦੇਸ਼ ਨੂਂ ਮਿਲਣਾ ਚਾਹੁਂਦੇ ਹੋ ਤਾਂ ਮਿਲ ਆਓ। ਮੈਂ ਖਾਣਾ ਬਣਾ ਰਹੀ ਹਾਂ। ਉਸ
ਨੂਂ ਮਿਲਣਾ ਜ਼ਰੂਰੀ ਨਾ ਹੋਵੇ ਤਾਂ ਨਾ ਜਾਉ...ਟੀ.ਵੀ. ਲਾ ਦਿਂਦੀ ਹਾਂ...ਬੈਠ ਕੇ ਵੇਖੋ...।" ਆਖ
ਕੇ ਉਹ ਉਥੋਂ ਉਠ ਗਈ। ਜਿਵੇਂ ਹੋਰ ਕੁਝ ਕਹਿਣ ਸੁਨਣ ਦੀ ਲੋਡ਼ ਨਹੀਂ ਸੀ।
          ਉਹ ਉਥੇ ਹੀ ਬੈਠਾ ਕੁਝ ਸੋਚਦਾ ਰਿਹਾ। ਫਿਰ ਉਸ ਨੇ ਫੈਸਲਾ ਕੀਤਾ, ਉਹ ਸੁਦੇਸ਼ ਵਲ
ਨਹੀਂ ਜਾਵੇਗਾ। ਇਥੇ ਹੀ ਬੈਠੇਗਾ, ਟੀ.ਵੀ. ਵੇਖੇਗਾ।
          ਉਸ ਨੇ ਮਹਿਸੂਸ ਕੀਤਾ, ਕਿਂਨੇ ਸਾਲਾਂ ਬਾਅਦ ਉਹ ਘਰ ਵਿਚ ਔਰਤ ਦਾ ਹੋਣਾ ਮਹਿਸੂਸ
ਕਰ ਰਿਹਾ ਹੈ। ਜਿਵੇਂ ਸਿਰਫ ਇਕ ਨੀਤਾ ਦੇ ਨਾਲ ਹੀ ਇਹ ਘਰ ਭਰਿਆ ਹੋਇਆ ਹੈ।
ਇਕੋ ਹੀ ਸਮੇਂ ਵਿਚ ਉਸ ਨੂਂ ਡਰਾਇਂਗ ਰੂਮ ਵਿਚ, ਰਸੋਈ ਘਰ ਵਿਚ ਅਨੁਭਵ ਕੀਤਾ ਜਾ
ਸਕਦਾ ਹੈ।
          ਉਦੋਂ ਵੀ ਇਂਜ ਹੀ ਹੁਂਦਾ ਸੀ। ਨੀਤਾ ਉਸ ਨੂਂ ਸਾਰੇ ਘਰ ਵਿਚ ਭਰੀ ਹੋਈ ਵਿਖਾਈ
ਦਿਂਦੀ ਸੀ, ਜਿਵੇਂ ਕੋਈ ਉਹ ਪਿਪ੍ਪਲ ਦਾ ਬਿਰਛ ਹੋਵੇ, ਜਿਸ ਦੀਆਂ ਟਹਿਣੀਆਂ ਘਰ ਦੇ ਹਰ
ਖੂਂਜੇ 'ਚ ਦਿਸ ਰਹੀਆਂ ਹੋਣ। ਉਹ ਸਿਰਫ ਇਕ ਪੀਲਾ ਪ੍ਪਤਾ ਹੋਵੇ ਜਿਹਡ਼ਾ ਇਕ ਬੁਲ੍ਲੇ ਨਾਲ
ਇਥੇ ਡਿਗ੍ਗ ਪੈਂਦਾ ਹੈ...ਕਦੀ ਕਿਤੇ ਹੋਰ।
          ਉਸ ਨੇ ਅਟੈਚੀ 'ਚੋਂ ਕੁਰਤਾ ਪਜਾਮਾ ਲਿਆ ਅਤੇ ਬਾਥਰੂਮ ਵਿਚ ਬਦਲਣ ਲਈ ਚਲਾ
ਗਿਆ। ਮੁਡ਼ ਕੇ ਆਇਆ ਤਾਂ ਉਸ ਨੇ ਦਿਨ ਦੀ ਪਾਈ ਹੋਈ ਪੈਂਟ ਬੁ਷ਰਟ ਤਹਿ ਕਰਕੇ ਅਟੈਚੀ
ਵਿਚ ਰਖ੍ਖ ਦਿਤ੍ਤੀ। ਫੇਰ ਉਸ ਵੇ ਆਪਣੇ ਆਪ ਹੀ ਟੈਲੀਵੀਜ਼ਨ ਦਾ ਬਟਨ ਆਨ ਕੀਤਾ। ਬ੍ਬਚਿਆਂ ਦੀ
ਕੋਈ ਫਿਲਮ ਦਿਖਾਈ ਜਾ ਰਹੀ ਸੀ, ਉਸਨੇ ਸਾਹਮਣੇ ਟੇਬਲ ਤੇ ਪੈਰ ਫੈਲਾ ਲਏ, ਆਪਣਾ ਸਿਰ
ਸੋਫੇ ਤੇ ਟਿਕਾ ਲਿਆ।
          ਉਪਰ ਪ੍ਪਖਾ ਚਲ ਰਿਹਾ ਸੀ, ਰਸੋਈ ਵਿਚੋਂ ਭਾਂਡਿਆਂ ਦੇ ਖਡ਼ਕਣ ਦੀ ਆਵਾਜ਼ ਆ ਰਹੀ
ਸੀ...ਟੈਲੀਵੀਜ਼ਨ ਤੋਂ ਬ੍ਬਚਿਆਂ ਦਾ ਸ਼ੋਰ ਊਭਰ ਰਿਹਾ ਸੀ। ਉਸ ਨੇ ਵੇਖਿਆ, ਨੇਡ਼ੇ ਨੇਡ਼ੇ ਕੋਈ
ਵੀ ਨਹੀਂ ਹੈ। ਇਸ ਘਰ ਵਿਚ ਸਿਰਫ ਨੀਤਾ ਹੈ ਅਤੇ ਉਹ ਆਪ ਹੈ। ਫਿਰ ਵੀ ਕਿਹੋ ਜਿਹਾ
ਸ਼ੋਰ ਹੈ ਜਿਹਡ਼ਾ ਉਹ ਲਗਾਤਾਰ ਮਹਿਸੂਸ ਕਰ ਰਿਹਾ ਸੀ।
          ਨੀਤਾ ਨੇ ਉਸ ਦੇ ਸਾਹਮਣੇ ਚਾਹ ਦਾ ਗਿਲਾਸ ਰਖ੍ਖ ਦਿਤ੍ਤਾ। ਉਦੋਂ ਵੀ ਉਹ ਟੈਲੀਵੀਜ਼ਨ
ਵੇਖਦਿਆਂ ਚਾਹ ਜ਼ਰੂਰ ਪੀਂਦਾ ਸੀ।
          ਕੁਝ ਦੇਰ ਵਿਚ ਨੀਤਾ ਵੀ ਉਥੇ ਆ ਕੇ ਬੈਠ ਗਈ। ਉਸ ਨੂਂ ਲਗਿਆ...਷ੋਰ ਕੁਝ ਹੋਰ
ਵਧ੍ਧ ਗਿਆ ਹੈ।
          ਦੋਵੇਂ ਟੈਲੀਵੀਜ਼ਨ ਦੇਖਦੇ ਰਹੇ। ਕੋਈ ਨਾਟਕ ਆ ਰਿਹਾ ਸੀ....
          ਮਹਾਂਭਾਰਤ ਵਿਚੋਂ ਧ੍ਰਿਤਰਾਸ਼ਟਰ ਅਤੇ ਗਾਂਧਾਰੀ ਦੀ ਕਹਾਣੀ ਸੀ। ਉਹ ਗਾਂਧਾਰੀ ਜਿਹਡ਼ੀ
ਆਪਣੇ ਪਤੀ ਦੇ ਅਂਨ੍ਹੇ ਹੋਣ ਕਰਕੇ ਆਪਣੀਆਂ ਅਖ੍ਖਾਂ ਉਤੇ ਪ੍ਪਟੀ ਬਂਨ੍ਹ ਲੈਂਦੀ ਹੈ...ਬਾਹਰਲਾ ਕੁਝ
ਵੀ ਨਹੀਂ ਵੇਖਦੀ ਅਤੇ ਅਂਦਰ ਪੂਰਾ ਇਕ ਲਹਿਰਾਂਦਾ ਹੋਇਆ ਸਮੁਂਦਰ ਸਮੇਟ ਲੈਂਦੀ ਹੈ ਅਤੇ
ਫਿਰ ਇਕ ਦੋ ਨਹੀਂ, ਪੂਰੇ ਸੌ ਬ੍ਬਚਿਆਂ ਨੂਂ ਜਨਮ ਦੇਂਦੀ ਹੈ।
          "ਖਾਣਾ ਤਾਂ ਖਬਰਾਂ ਤੋਂ ਬਾਅਦ ਹੀ ਖਾਓਗੇ ਨਾ।"
          ਉਸ ਨੇ ਨੀਤਾ ਵਲ ਵੇਖਿਆ। ਨੀਤਾ ਨੂਂ ਸਭ ਕੁਝ ਯਾਦ ਹੈ। ਜਿਵੇਂ ਦਰਮਿਆਨ ਦੇ
ਇਂਨੇ ਸਾਲ ਬਿਨਾਂ ਕਿਸੇ ਖਰਾਸ਼ ਦੇ, ਬਿਨਾਂ ਕਿਸੇ ਦਾਗ ਦੇ ਸਿਰਫ ਇਕ ਬੁਲ੍ਹੇ ਦੀ ਛੁਅਨ ਦੇ ਕੇ
ਨਿਕਲ ਗਏ ਹੋਣ।
          ਖਾਣਾ ਖਾਂਦੇ ਵਕਤ ਉਸ ਨੂਂ ਲਗਾ, ਜਿਵੇਂ ਵਕਤ ਇਕਦਮ ਸਿਮਟ ਆਇਆ ਹੋਵੇ। ਉਹ
ਕਰੇਲਿਆਂ ਦੀ ਭਰੀ ਹੌਈ ਸਬਜ਼ੀ ਕ੍ਕਚੇ ਅਂਬਾ ਦੀ ਖਟਾਈ ਵਾਲੀ ਹਰ ਹਰ ਦੀ ਦਾਲ, ਖੀਰੇ ਵਾਲਾ
ਰਾਇਤਾ, ਧਨੀਏ-ਪੁਦੀਨੇ ਦਾ ਚਟਨੀ ਅਤੇ ਇਹਨਾਂ ਸਾਰਿਆਂ ਦੇ ਅਂਦਰ ਇਕ ਅਛੂਤੀ ਖੁਸ਼ਬੂ...
ਇਕ ਵਖ੍ਖਰੀ ਤਰ੍ਹਾਂ ਦਾ ਸਵਾਦ।
          ਨੀਤਾ ਨੇ ਉਸ ਬਾਰੇ ਕੁਝ ਜ਼ਿਆਦਾ ਨਹੀਂ ਪੁਛਿਆ...ਬਸ ਇਂਨਾ ਹੀ ਕਿ ਕੁਡ਼ੀਆ ਕਿਂਨੀਆਂ
ਵਡ੍ਡੀਆਂ ਹੋ ਗਈਆਂ ਹਨ। ਸਹਾਰਨਪੁਰ ਵਿਚ ਚਂਗੇ ਸਕੂਲਾਂ ਦਾ ਕੀ ਹਾਲ ਹੈ, ਦਫਤਰ ਵਿਚ
ਉਸ ਦੀ ਅਗਲੀ ਤ੍ਤਰਕੀ ਕਦੋਂ ਹੋਵੇਗੀ...ਜੋ ਕੁਝ ਉਸ ਨੇ ਪੁਛਿਆ, ਉਸ ਦਾ ਉਤ੍ਤਰ ਵੀ ਬਡ਼ਾ
ਛੋਟਾ ਹੀ ਸੀ...ਨੀਤਾ ਨੂਂ ਪੀ.ਜੀ.ਟੀ. ਗਰੇਡ ਮਿਲ ਚੁਕ੍ਕਿਆ ਹੈ, ਤਿਂਨ-ਚਾਰ ਸਾਲ ਤਕ
ਪ੍ਰਿਂਸੀਪਲ ਬਣ ਜਾਣ ਦੀ ਆਸ ਹੈ। ਅਜੀਤ ਨੂ ਉਹ ਆਈ.ਏ.ਐਸ. ਕਂਪੀਟੀਸ਼ਨ ਵਿਚ
ਬਿਠਾਏਗੀ ..."
          "ਤੂਂ ਦੂਜੀ ਸ਼ਾਦੀ ਕਿਉਂ ਨਹੀਂ ਕਰਾਈ ?"
          "ਬਸ ਇਂਜ ਹੀ।" ਉਹ ਬੋਲੀ ਸੀ..."ਤੁਹਾਡੇ ਲਈ ਦੂਜੀ ਸ਼ਾਦੀ ਕਰਾਣਾ ਆਸਾਨ ਸੀ...
ਪਰ, ਮੈਨੂਂ ਸਿਰਫ ਇਕ ਮਰਦਨਹੀਂ ਚਾਹੀਦਾ...ਅਤੇ ਦੁਨੀਆਂ ਵਿਚ...ਜ਼ਿਆਦਾਤਰ ਆਦਮੀ
ਸਿਰਫ ਮਰਦ ਦਿਖਾਈ ਦਿਂਦੇ ਹਨ। ਫੇਰ ਵੀ...ਕਦੇ ਕੁਝ ਠੀਕ-ਠਾਕ ਲਗਿਆ ਤਾਂ ਸ਼ਾਦੀ
ਕਰਾ ਲਵਾਂਗੀ।"
          ਡਰਾਇਂਗ ਰੂਮ ਦੇ ਦੀਵਾਨ ਤੇ ਨੀਤਾ ਨੇ ਉਸ ਦਾ ਬਿਸਤਰ ਲਾ ਦਿਤ੍ਤਾ। ਵਿਚਕਾਰਲੀ
ਮੇਜ਼ ਤੇ ਪਾਣੀ ਦਾ ਜ੍ਜਗ ਅਤੇ ਗਿਲਾਸ ਰਖ੍ਖ ਦਿਤ੍ਤਾ। ਜਦੋਂ ਤਕ ਟੈਲੀਵੀਜ਼ਨ ਚਲਦਾ ਰਿਹਾ,
ਦੋਵੇਂ ਦੇਖਦੇ ਰਹੇ ਅਤੇ ਅਤੇ ਬਿਨਾਂ ਬੋਲੇ ਗ੍ਗਲਬਾਤ ਕਰਦੇ ਰਹੇ। ਫੇਰ ਟੈਲੀਵੀਜ਼ਨ ਦੀ ਅਨਾਉਂਸਰ
ਨੇ ਕਿਹਾ-
          "ਸਾਨੂਂ ਕਲ੍ਹ ਸ਼ਾਮ ਛੇ ਵਜੇ ਤਕ ਦੀ ਇਜਾਜ਼ਤ ਦਿਓ।"
          "ਅਛ੍ਛਾ ਹੁਣ ਤੁਸੀਂ ਆਰਾਮ ਕਰੋ। ਕਹਿ ਕੇ ਨੀਤਾ ਆਪਣੇ ਕਮਰੇ ਵਲ੍ਲ ਜਾਂਦੀ ਹੋਈ
ਬੋਲੀ-"ਮੈਂ ਸਵੇਰੇ ਸ੍ਸਤ ਵਜੇ ਨਿਕਲ ਜਾਂਦੀ ਹਾ। ਤੁਸੀ ਬਾਅਦ ਵਿਚ ਜਾਣਾ ਚਾਹੋ ਤਾਂ ਚਾਬੀ
ਗੁਆਂਢੀਆਂ ਨੂਂ ਦੇ ਜਾਣਾ।"
          "ਨਹੀਂ ਨਹੀਂ।" ਉਹ ਬੋਲਿਆ ਮੈਂ ਤੁਹਾਡੇ ਨਾਲ ਹੀ ਨਿਕਲ ਚਲਾਂਗਾ। ਪਹਿਲਾਂ
ਸੁਦੇਸ਼ ਕੋਲ ਜਾਵਾਂਗਾ। ਫਿਰ ਦਫਤਰ ਦਾ ਕਂਮ ਨਿਬੇਡ਼ ਕੇ ਸਹਾਰਨਪੁਰ ਚਲਾ ਜਾਵਾਂਗਾ।
          ਸਾਰੇ ਫਲੈਟ ਦੀਆਂ ਬ੍ਬਤੀਆਂ ਬੁਝ ਗਈਆਂ ਸਨ। ਉਹ ਸੌਣ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰ ਰਿਹਾ ਸੀ,
ਪਰ ਨੀਂਦ ਕਿਤੇ ਦੂਰ...ਵਧੇਰੇ ਦੂਰ ਵੀਰਾਨਾਂ ਵਿਚ ਭਟਕ ਰਹੀ ਸੀ। ਉਸ ਨੂਂ ਜਾਪਿਆ ਨੀਤਾ
ਵੀ ਆਪਣੇ ਪਲਂਗ ਤੇ ਪਈ ਹੋਈ ਜਾਗ ਰਹੀ ਹੈ। ਸ਼ਾਇਦ ਉਹ ਵੀ ਕਿਤੇ ਵੀਰਾਨੀਆਂ ਵਿਚ
ਭਟਕ ਰਹੀ ਹੋਵੇ਷। ਪਤਾ ਨਹੀਂ ਸ਼ਾਇਦ ਉਹ ਸੌਂ ਹੀ ਗਈ ਹੋਵੇ। ਉਦੋਂ ਵੀ ਉਹ ਸਂ ਜਾਇਆ
ਕਰਦੀ ਸੀ ਅਤੇ ਉਹ ਖਂਦਕਾਖਾਈਆਂ ਵਿਚ ਭਟਕਦਾ ਰਹਿਂਦਾ ਸੀ।
          ਸਵੇਰੇ ਉਸ ਦੀ ਨੀਂਦ ਖੁਲ੍ਹੀ ਤਾਂ ਫਲੈਟ ਦੀ ਖਿਡ਼ਕੀ 'ਚੋਂ ਸੂਰਜ ਦੀਆਂ ਕਿਰਣਾ ਦਿਸਣ
ਲਗ ਪਈਆਂ ਸਨ।
          ਨੀਤਾ ਉਸ ਦੇ ਸਾਹਮਣੇ ਚਾਹ ਦਾ ਗਿਲਾਸ ਰਖ੍ਖਦੀ ਹੋਈ ਬੋਲੀ "ਜੇ ਇਕ੍ਕਠੇ ਹੀ ਨਿਕਲਣਾ
ਹੋਵੇ ਤਾਂ ਛੇਤੀ ਹੀ ਨਹਾ-ਧੋ ਲਵੋਂ।"</p>

<p></p>

<p></p>

<p>               0 
</p></body></text></cesDoc>