<cesDoc id="pun-w-literature-critic-lot7y" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-literature-critic-lot7y.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Galap Chetna</h.title>
<h.author>Singh Behari Jagdeesh</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Waris Shah Foundation - Amritsar</publisher>
<pubDate>****</pubDate>
</imprint>
<idno type="CIIL code">lot7y</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 80 - 85.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-23</date></creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;2030&gt;&lt;Singh Behari Jagdeesh - Galap Chetna&gt;&lt;Amit Bhandari&gt;</p>

<p>&lt;ੋ੍ë ࠀਊë੊੧ੋ ਃ੧ ਆ।੊ੇ                                                       
                            () ੊। ੍਎।                                   ഀ਀     怀䨀搊鰉 䴀ช搊ਉì ਈ੮䤊Ԁ渊Ԋ ؀搊䨉䜊鈊搀ਉ ।. ਃੇìਈ ੬ 鰀䨀搊ꀉ搀 ̀搊 Ā餀搀 ฀䴊Ċ渊Ċ䨊ࠊ਀  
ਆ।੊ੇ ੍੨û `ਆì ਈ੮錀萀✀ Ā權脊 䜀栊 鈀最 怀ࠀ栊鸊漀霊搀䜉✊ Ā鈊渀鈊 蜀䤀؊朊 䜀鐊ࠀ鐊 需氀̊搊脉 ؀搊䨉䜊 ഀ਀ࠀ脊餀搀ਉ ࠀ搊鴉鈀搀脉 ؀脀 ̀Ā朊 䴀؊﬊ 萀ࠀꀊ最 䴀ਊ ਆ।੊ੇ ੊ë ੉ਆ੧ ੉ਁ੮ 餀搀ĉ ̀ ഀ਀䤀ช搊餉鈀ⴀ䤀ଊ萀需搀ਉ ਁ੧ ੉ਁëਁ੊਎ ੉ਂëਁì ਆî ੊ëੋ੨ ੊੫ í ë ੍੨û ੉ਆ੧ ਈ।ਊì 
ì੊ਆ ਃ੧ ੍ 錀最Ċਊ ਆ।ੇ ੉ਊਅëਁ ਆ। ؀脀 萀ࠀꀊ最 ؀搊䨉䜊搊脉 䨀需 ਀鸊漀朊 ࠀ渊ਊ਎। ë  ਆ।ੇ 
ਈ੧ੋ ؊ ̀搊 ࠀ渊ਊਏਁਆ ìਁ। ੍੨û ੉ ਆ।੊ੇ ੊ë ੉ਆ੧ ੍ ੊ëੋ੊।੉ ਈ੮鐊鰀 鈀Ā搊 䴀栊 鈀 鈀最䨊䜊 ഀ਀怀䤀霊萀਀ì' ਊ  ੧ ੍ì Ċ ̀朊 ̀錀渀錊搀脉 ̀搊 錀搀ĉช搊 ؀䴊脀 䴀權 䤀鈊̀搊﬉ 餀Ā؊ ̀朊ਊ  ੉ë ഀ਀退਀ਁ Ȋ鈀 Ā氊ਊ ਁ੧ ਈ ੧ ਈ੨搊脉 Ā朊 錀鸀漀 ؀䴊脀 䴀權 餀搀脉̀ 蜀Ā؊ Ā鈊渀鈊 蜀䤀̊ ฀鈀Ā ̀ ഀ਀萀䬀搊 ਀朊ชĊ䜊ਊ 䴀栊﬊ ؀搊䨉䜊 䨀需 ଀搊䨉朊脊 蜀䤀؊朊 萀ࠀꀊ ؀搊蔉鈀搀 怀鈀脀䨀䜊✊ ؀脀 萀ࠀꀊ最 ࠀ栊ਊ। ਁ੧ 
੯੫ਃ੧ ਆ੍ì ੊ëë ਉë 䨀 ؀搊䨉䜊 ̀朊 茀脀Ā 䨀需 餀̀權脊 蜀䴀 䴀ਊਆ੧ ਆî ਈ ੧ ਈਁì ਃ੧ 
ࠀ 䨀需 需ꀀ̀ 䴀栊 Ā搊脉 蜀䤀̊ 䤀渊䨊栊霊欀ꀊ 䤀搊؉脀 蔀鈀 䤀Ā脊Āਊ ਆ। ̀朊 ̀ਊੋਆ 䨊搊 餀搀脉̀ ഀ਀䴀栊﬊ ഀ਀     怀䨀搊鰉 䴀ช搊ਉì' ੊ë ਈ੮ช錀渀錊 䨀䬀搊 蔀䤀Ċ渊ਊì ।ਁì ਃì ਁ䤊༊權鐊 䤀ȊĀ ؀脀 ਀萀؀ ഀ਀鈀਀ਆ। ੍ì ੍੨û ਎̊ⴊࠀ渊ਊਅ।ਆ ।ì̊搊ਉì ੉਎। ੊ë ੉ਁ੮ ̀朊 餀餀漀ਊ萀脀 茀Ā朊 蔀頀渀頊搀䨉搊脉 ̀ ഀ਀鈀欀蘊 ࠀ頀渀頊ⴀࠀ渊ਊਁìਁ ਆ੍ì ੍íਃìû ਂ੧ ੉ਁ੮ ؀脀 鈀最䨊䜊 蔀䴀 䤀錀搀蔉萀 餀搀脉̀搊 䴀栊 鈀 ഀ਀蜀䤀؊朊 ฀ਊਃ ੉।੍੊੧ ਈ ੧ ੊îਃ ਃ। ੉਎ࠊꀊ 鈀䨀朊脊 鈀਀ਆ। ੍੨û ਈ੯੮੍ì ੇëì ੊ੇ ë੉੧ ਎̊ ഀ਀̀ ࠀ頀䜀鐊渀鐊 ਀ꀊ 鈀最 餀蠀ꀀ 蔀鈀䜀渊䜊萀脀 ਀੍ë  ਆî ਁ養䴀 ̀脀̀ 䴀栊﬊ ࠀਊਁî ੉਎। ੉ਃì ੉ 
੮। ਆî ਈ੮䨊搊؉ 鈀਀ਆ ੇ ਁë ؀䴊脀 䴀栊﬊ 蔀䤀 ࠀ渊ਊ੉ ੊ë ੉ਆî ਈ䨀搊ਉ ਃ੧ ੍ ഀ਀฀栊脊਀ਊ ਃì ੇì-ë ੉í ਆì ਈ੨ਃì ੍ਆû ੍੫ Ā搊脉 䴀權ਊ ੊ੇ ਃ੧  鐀欀䴊搊ⴉ鈀Ā搊 ഀ਀鈀਀ਆ ੊।ੇì ਈĊ搊ࠉ 䨀 蜀䤀؊脀 ؀䤊䴀Ċ 鈀਀ਆì ਈ । ਉ養漀 䤀ช騊̀ 䴀栊﬊ ؀搊ਉì ਃ੧ ਊੇ। ੍੫    
ਃ। ਈ। ਈ੯੮੍।ਃì ੍੫ ੍ ੉ ਆî ੍ëਃì ੍੨ :
                  "ੇ੮ੇ੧ ਊਃ੧ ਃì ।੍ਃì îਆ ੍íਃì û ਂ੫੧ ੫ੇ੧ ।੍ਃ। ੇí੫  
                  ਆ।ੇ੧ । 餀萀鈀搀脉 ̀ ฀搊脉 䤀 ฀栊脊 餀̀權脊 ऀ脀฀ꀊ ฀ਊëû ੉੫ë ਎ਆ 
                  ੉ë 鼀鈀渀鈊ꀀ ਀؀搊脉 ؀ 䤀ਊਃ।û ਋।੊੧ ੍ ੍í  ਎ੇì ਎ੇì  ਈ ഀ਀                  销਀ ਃ਎ì ਁ। ੍੨û ਆ੍ì ਁ। ੬ íਁ੮ਁ੧  ਆì ਎।ਆ ੍íਃ੧û ਆ।ੇ੧ 
                  ਋搊 Ȁ權鸊搀 需䴀脀 ଀搊蘉萀脀 ✀需 錀鸀漀 鈀最 鐀䜀渊䜊 鈀਀ਆ ੫। ਆì 䴊ꀀ搀 䜀權鈊 ഀ਀                  萀錀ꀀ鐀最 ̀朊錊 䜀權 ਀ꀊ萀 ऀ਀ਃ।  ਉਆ੧ । ਋੨  ਊ।權脊 ؀ 蜀鴀搀䜉 ഀ਀                  䴀權蘊ﬀ∀                                                  ഀ਀                                                                  ⠀ࠀ脊؀搊 㜀　⤀㰀⼀瀀㸀ഀ਀ഀ਀㰀瀀㸀     蔀䤀 ؀搊䨉䜊 ̀ 鈀Ȁ搊 茀؀䤀搊ਉ ੊ੇ ੍੫脀 Ā脀؀ ଀栊ꀊ搀脉 䴀؊ⴊⴀ 鈀脀䨀䜊Ⰺ ฀Ԋ 茀Ā朊 鈀਀ਆû 
ਆ੮੍। ਃ। ਈëਁ। ਊਈਆ ੊ë ੍ì í੯ 需鈀渀鈊搀 䴀栊﬊ ฀搊脉 ؀脀 Ā脀؀搊脉 鈀鸀漀萀脀 ̀朊 䨀萀䴀 ഀ਀̀搊 ऀ漊鈀਀ ੍੨û ੊੮ì ੇ੯ì ੍੫ ࠀ鸊漀渊䴊؊搊 需搀䴉脀̀ 䴀栊﬊ 蜀䤀̊搊 餀茀 鈀਀ਃ। ੍੨ ë ੍ 
ਈ੯੮੍-ੇë ੧ ਈ੮權ऊ栊䤊ਊ ਊ ੧û ਈ脊Ā 蜀䤀̊萀脀 ਀搊鈉 ଀栊ꀊ搀脉 頀最Ċ Ā權脊 頀最Ċ 䨀萀䴀 鈀਀੧ 
 䨀䤊搊 䜀栊ꀊ搀 需搀䴉脀̀萀脀 䴀؊﬊ 餀̀ 鈀脀䨀䜊 䨀萀䴀 䨀搊䤉Ċ朊 ਀੯।਎ਃ ਆ੍ì ੍íਃì ਁ। ੉ਃì 
ਃ੫੊੧ ੫ì ਋੨ । ੊।ⴀ䨀搊ਉì ੊ë੍ 䨊搊 䜀栊脊̀萀脀 䴀؊﬊ ฀Ԋ ̀搊 䨀萀䴀 ഀ਀̀䜀渊䜊 䨀需 䨀䤊̊朊 蔀鈀 ऀ氊養 茀ऀ䤊ਊ ਆ।ੇ ੍੫ ।ਃ। ੍੨, ਃ੫ ë ੫ì ë؊ 䨀̀朊䬊 䨀需 ഀ਀਀੍ë  ੊।ੇ੧  ੇ੯੧ ਆ।ੇ ੋ।ਃì 霊搀 䜀栊脊̀ 䴀栊﬊ 頀欀鰊萀脀 ̀權䨊朊脊 ଀栊ꀊ搀脉 ഀ਀ࠀ鸊漀渊䴊萀脀ⴀ䜀錀萀脀 䴀權ꀊ ̀朊 ਀搊䨉養̀ 退਀ਁ ਃ੧ ਈ脊ࠀਊਁ ੉ੇਈ ਆ।ੇ í੯ì ੍੫ ੍ਆû 
ਃ੫੊੧ ਈ ੧ ਈ ੧ ਈਁì ਃ। ਈ頊搀䨉搊脉 ਀ꀊ 鈀最 萀ࠀꀊ 餀漀脀̀鐊 餀蠀ꀀ ̀ Āช脊؀搊 ਀੮ਃì 
੍ਆû ਈ脊Ā 鈀脀䨀䜊 蔀蜀脀 鈀䤀朊 ̀搊 ࠀਊ।੊। ਊ  ੧ 䴊ꀀ 䜀蘊 Ā萀਀ ਆ੍ìû ੍  ਎í਎ੇ   
੊ëਁì ਊ  ੧ ë । ੇ੫ਃì ੍੨û ë੍। ੊ëਁì ë੉ਃì ੉íਁਁ 䤀渊䨊栊蔊頀渀頊搀脉 䴀權䨊朊 Ā朊 ഀ਀餀欀 錀̀ช錀Ā萀਀ 䴊 鈀最 萀ࠀꀊ 餀漀脀̀鐊 䤀脊਀脊Ԁ ऀ漊栊䤊䜊朊 鈀਀ ੉੧û ੍ì ।ꀊ 䴀栊 鈀 蜀䴀 ഀ਀萀ࠀꀊ 餀漀脀̀鐊 䨀需 萀蜀ꀀ 䨀搊䜉朊 䴀ਊ ਎̊ ؀脀 鰀搀䜉̊ ਀੍ëਃì ੍੨, ੉ ਆ।ੇ  ੧ ë੍ì  
੧ਃì 䴊脀̀ 䴀栊﬊ ਀੊ì, ੉í脀̀ਊ, ਈ੮權⸊ 茀฀ਊìਁ ਃë ਊ੍íਁ ੉।朊 䨀茀鈀Ā 蜀䤀̊ ഀ਀餀漀脀̀鐊 䨀需 萀蜀脀̀朊 䴀؊ ࠀਊਁî ੍ ë੉੧ ੉।੍੊੧ ੊ì ੉ੇ-੉਎ࠊꀊ 䨀搊䤉Ċ朊 Ā萀਀ ਆ੍ìû 
੍ ë੉੧ ਆî ੊ì  ੯।੉ ੍ਃ੮ਃ ਁ੫ ੧ ਆ੍ì ੊ਅ  ਃëਃìû ੉ਃ੧ ë੍੧ ੉í਋। ਊ।朊 ഀ਀鰀ࠀ渊ࠊꀊ 鈀਀ਃ। ੍੫ ਈ੮權ऊ栊䤊ਊ ਎養Ā 䤀脀销 鈀䴀脀̀搊 䴀栊 㨀㰀⼀瀀㸀ഀ਀ഀ਀㰀瀀㸀                                                                                                    ∀䜀權鈊 ฀鰀渀鰊 ̀朊 ؀䴊脀 ਀ꀊ最 䴀權謊 䴀脀̀朊 Ā朊 ؀搊 䴀 ࠀȊ渊Ȋਊ ਃ੧û
                      ਁ੧朊 ؀朊鸊漀朊 萀 鈀最 鈀欀蘊 䨀 ਀脊̀搊 ଀鰊鈀最 ਀؀搊脉 ؀䴊脀 ਀੍ë ੉ਃ।û 
                      ਁ੧朊 䜀蘊 蔀䴀 䤀ଊ 錀最鸊 䴀栊﬊ 萀ࠀꀊ 蔀鈀 䴀̊渊̊Ⰺ 蔀鈀 ̀䨀搊ਉ ਁ੮੮  
                      ੇ੧ ਆî ੇ੨ ਃì ੍੨ ਁ੧ ਂ੧ ਈ੍í ੧ ਈ । ਊí੍। ੫ 
                      ੇ੨ਃì ੍੨û ਉë 茀鐀䜀搊 ؀搊 茀鐀搀脉䴀 萀 䤀鈊̀搊 䴀栊 Ā朊 ؀搊 ࠀ頀搀脉䴀 ഀ਀                      餀搀 䤀鈊̀搊 䴀栊﬊ ਀䤊 蜀䤀 ̀䨀搊ਉ ਆ।ੇ ੉ë ࠀ鰊鈀ⴀࠀ鰊鈀 鈀最    ഀ਀                      䜀䴊ⴀ䜀䴀搊ꀉ 䴀脀̀搊 ਀੍ëਃ। ੍੨û ੉੧ ੇ ਎੨ ।੍ë ੉ì ਁ੧搊 ഀ਀                      䨀萀䴀 䴀權 餀搀䨉朊Ⰺ Ā朊 蔀Ȁ權脊 鈀Ԁਊ੧ ਃî 需䜀 餀搀䨉朊脊⸀⸀⸀ﬀ∀ഀ਀                                                                           ഀ਀                      ∀Ā朊 Ā䤀脀 䤀錀 ਀੍ë ੉੫û"
                                                                           
                      "੍।û ਈ Ā脀 蔀䴀 ؀䴊脀 需搀䴉脀̀ﬀ Ā栊؊脀 萀ࠀꀊ Ā搊鈉Ā ̀搊 ࠀĊ搊 ഀ਀                      䴀栊﬊ Ā脀 䜀權鈊搀脉 ؀脀 蜀䜀渊䜊 ਀ꀊ̀搊 䨀朊錊 鈀最 餀 䨀需 錀䬀 䴀脀̀ ഀ਀                      䴀栊脊 蔀䤀朊 䜀蘊 鈀䤀朊 蔀鈀 ̀ ਀ꀊ 鈀最 餀蠀ꀀ搀 Ā栊؊脀 ࠀਊ੊।ਆ ਆ੍ìû" 
                                                                 (ਈਆ। 100)
                                                                           
     ਃ ਈ੮權ऊ栊䤊ਊ ਎養Ā 䤀脀销 鈀脀䨀䜊 ̀搊 䤀搊ȉ 頀鸀渀鸊 鈀最 ̀䜀渊䜊 需䜀搊 餀搀脉̀搊 䴀栊 Ā搊脉 ഀ਀鈀脀䨀䜊 ؀脀 蔀䴀 蔀䴀䤊搊䤉 ਀鸊漀 Ā਀ਊਁ। ਆ।ੇ ੍íਃ। ੍੨ ë ੉।੧ ੉਎। ੊ë ੧ 
`ੇ੮ੇì-੍ë 退਀ਁ' ਃ। ੯਎।ਆ। ਆ੍ì û ੉੧ ਎ਆ੫੉ਂëਁì ੊ë  ਃëਆ ੍     
í̊萀਀। ਃíਆë੊।ꀊ 䤀搊䴉਀ 䨀錀最 ࠀ脊需฀ ̀朊 ฀朊䜊朊 Ā朊 ฀Ȋ渊Ȋ搊 鰀最鈊ꀀ 餀搀脉̀ 䴀栊 Ā搊脉 ഀ਀蜀䤀؊脀 萀ࠀꀊ 蔀鈀 ਀霊ࠀ؊ ̀ 䤀䴊朊䜊 ฀䜀 餀搀脉̀ 䴀栊﬊ ̀權䨊朊脊 䤀䴊朊䜊萀脀 萀ࠀꀊ 萀ࠀꀊ 餀漀脀̀鐊 ഀ਀̀朊 餀漀ਊî ࠀ渊ਊ੉  ਃîì ਆ।ੇ ੉।੧ ̊萀脀 䴀؊﬊ 鐀䜀渊䜊搊脉ⴀ鐀䜀渊䜊搊脉 䨀需 鈀脀䨀䜊 ̀ ഀ਀䤀䴊朊䜊 ̀ࠀ 蜀䤀؊脀 ̀䤊̊ 䴀栊 鈀 蜀䤀̊朊 ଀ਊ। ਃì ੊੍íì ਎ 鐀蘀 䴀栊 茀Ā朊 蜀䤀̊朊 頀欀鰊最 餀䴀朊 ഀ਀਀霊渀霊最 ؀脀 ࠀ搊䜉ꀊ搀 鈀搀उ漊 ฀䬀鈊䜀 䴀權 鐀萀 䴀栊﬊ ̀ࠀ ̀搊 ଀ਊ। ੍؊朊鈊 䨀 鈀脀䨀䜊 ؀搊䜉 鈀騀 ഀ਀䴀搊䤉搊ⴉ฀錊氀䜊 鈀਀ਃ। ੍੨û ਃ੫੊੧ ੍।੉੧-੍।੉੧ ੊ë  ਃî੧ ਆ।ੇ ì੊ਆ ਋ 蔀鈀鴀渀鴊最 ਀੍ë  ਃ। 
搊ਉ  䜀栊脊̀朊 䴀؊﬊ 鈀騀 ̀؀搊脉 ࠀ頀欀脊 䴀 䴀ਊਆ੧ ਃ। ਎।਎। ੉ਆ ਃì ਎੫੍ 䜀栊 鈀最 鈀脀䨀䜊 ഀ਀䴀權ਊ। ਃ੧  ࠀ䴊需 餀搀脉̀搊 䴀栊﬊ 鈀脀䨀䜊 ̀搊 ଀ਊ। ਊ੯। ੍੨搊؉ 䴀脀̀搊 䴀栊﬊ 蜀䤀̊朊 ฀؊ 䨀需 蔀䴀 ഀ਀䬀脊鈀搀 䴀栊 鈀 餀䴀鸊漀 鈀脀䨀䜊 ؀朊 䨀鸊渀鸊最 䨀鸊渀鸊最 茀ऀ漊䤊ਊ। ਆ।ੇ ੊ë੍ 䨊搊蜉ꀀ搀 ࠀ渊ਊ੊।ਆ ਆ੍ì 
ìਁ।, ੍ ੍؊朊鈊 䨀ਊ੧ -ਈ੯੮੍੧ ਁ੧ ਃí੍।î ਊਃ੧ ਆ।ੇ ੊ë੍ 䨊搊蜉ꀀ搀 鈀Ȁ權脊 ฀脊؀ ഀ਀餀搀謉鐀ﬀ ࠀਊ ੊ੇ ਁ। ë੊੧ ੍؊朊鈊 ̀朊 䤀؀朊䴊朊 ؀脀 䴀 蠀鸀鈀 ਀੍ì ੉ìû ੍ ੮ ਃ਎ 
`੍।' ̀脀̀ 䴀栊﬊ 䴀ਊਆ੧ ਁ੧ ੊ੇ ਃ। ੊ë੍ ੍੫ ।ਃ। ੍੨ ਁ੧ ੊ੇ ਊ੯ì íੋì-íੋì 
ਈ ì ਊ।ì ੯ëਃì ੍؊朊鈊 ؀脀 茀਀ਈ   ̀脀̀ 䴀栊﬊ 蜀䴀 萀ࠀꀊ最 䴀Ȋ渊Ȋ ؀搊䜉 䤀਀ì ਃì 
ਃì 權鰊 ਀ꀊ搀蜉脀̀ 䴀栊Ⰺ ଀Ċ渊Ċ搊 䜀栊 鈀最 錀最Ċ搊脉 䨀需 餀搀脉̀ 䴀栊 茀Ā朊 䴀ਊਆ੧ ਃì ਈ੍ëੇì ੊੍íì 
ਃ੧ ਎í੧ ਋ëਃ ؀脀 蜀䤀̊ 䤀鈊 ฀搊脉 ؀搊䜉權脊 䨀 䨀Ԋ朊ਊ੧ ਎੫੍ ਆ।ੇ ਈ।ੇਃì ੍੨û ੊ੇ ਃ੧ 
੊ë੍ ਁ੫ ਂ੫੯੮੍ì ਃ੧ ࠀ頀欀脊 蔀鈀 䨀搊ਉ ë؊ 茀Ā朊 蜀䤀̊搊 ࠀĊⰀ 鈀脀䨀䜊 ؀脀 ฀䜀ꀊ 蜀䤀̊朊 ഀ਀䤀䴊਀੧ ਈë ।ਃ੧ ੍ਆû ੍ ੊੧ਃ੧ ੍ਆ ë ੊ੇ ਊ੯ì ੯íੋ ੍੨û ੫-੮ ਈ ì ë੉੧ 
ਃ।੉ì ਃ। ਈਁ। ੍ ਆ੮੍। ਆî ਆ੍ì ੇ੮  ਃëਃìû
     ੊ੇ ਃ੧ ੍؊朊鈊 ؀搊䜉 䨀萀䴀 鈀਀੊। ੧ ੯íੋ 䴊ꀀ ̀搊 䤀ଊ Ā權脊 䨀鸊渀鸊搀 ਀। ੍ ੍੨ ë 
੉ਆ੧ ੍ ੊ë੍ ਈ ì ੉੮੊੨੫਎ ਆ।ੇ 䨊搊蔉萀 䴀栊﬊ 销鰀欀ⴊ销鰀渀鰊 鈀欀蘊 ਀脊̀搊 ؀䬀霊Ā 䤀ช搊脉 ̀朊 ഀ਀鈀最 蜀䤀؊脀 䨀朊錊ꀀⴀࠀਊ  ੊।੉ਁ੧ ਆ੮੍। ਃ੧  ؀䴊脀 萀蔀萀ﬀ 蜀䤀؊脀 鈀䤀朊 ਀脊̀朊 茀鐀渀鐊最 萀ࠀꀊ最 ഀ਀ࠀ脀鸀最 ̀ ؀฀搊蔉䬀 ؀䴊脀 䜀搊蜉ꀀ ࠀ蘊ﬀ 鈀脀䨀䜊 蔀䤀 䨀搊䤉Ċ朊 䨀 錀漀䬀 䴀栊 鈀 䴀ਊਆ੧ ੉ਃì ਎養漀 ഀ਀؀脀 萀ࠀꀊ ฀ਊ੯ì ਎ਆਃ। ੍੨û ਎ ਎̊搊脉 䨀搊脉鐀 蜀䴀 ࠀĊ؊ ؀脀 怀ࠀ栊ਊ ਃì íਁ੮ਁì' ਆ੍ì 
੉਎ਃ।û ੉ ੇ੮ੇ ਃ।  ਁ। ।ꀊ 蔀䴀 䨀 䴀權 䤀鈊̀搊 䴀栊 鈀 䴀ਊਆ੧ ੮ ਈ੯੮੍ë-ੇëë 
੍੨ ਁ੧ ਃî੉朊 蜀䴀 ̀䴀搊餉 䴀栊﬊ 茀餀䴀朊 ਀脊̀朊 ؀脀 鈀脀䨀䜊 䨀ਊì ਈਁਆì ਎ëੇ । ।  
੮䬀ช朊 Ā權脊 销鰀渀鰊 ؀䴊脀 䤀ﬀ 鈀脀䨀䜊 ̀ 錀漀䬀 ̀搊 蔀鈀 鈀搀ਉ  ੍ ੊ì ੍੨ ë ੍؊朊鈊 䨀ਊ੧ 
੮ ਈ੯੮੍੧-ੇë੧ ਁ੧ । ਎ ؊ 䨀搊䜉朊 ࠀĊ ؀搊䜉 䨀萀䴀 鈀਀।੧ ੍  ਁ渊䴊搊脉 ؀搊䜉 ഀ਀怀䬀䴊̀✊ ̀搊 ̀ਊ। ਈ੮搊ࠉĊ 鈀਀  ੍੨û ੍ ਈ ੧  䨀搊䜉萀脀Ⰰ 䨀搊鈉ऀ漊鈊搀ਉ। ਁ੧ 
੉।-੉ਊਅì ਆî ਈ ੧ ੉ ช ؀搊䜉 需鈀渀ਊëਁ  䤀鈊̀ 䴀栊﬊ 䴀ਊਆ੧ ਆ।ੇ ੊ë੍ 搊 鈀最 ഀ਀蜀䴀 萀ࠀꀊ最 ࠀ਀। ੧ ਈ੨ਁ੯੧ ਈ੮ਈ 䨀 ਀ਊ搊ਉ 䴊 䤀鈊̀ 䴀栊 鈀 蜀䴀 萀̀ช萀脀 ؀脀 鈀騀渀騊 ഀ਀؀䴊脀 䤀ช騊̀Ⰰ 蜀؀渊䴊搊脉 ̀ 鈀錀渀錊 ࠀਊ੊।੍ ਆ੍ì ̊ﬀ 鈀蜀脀鈀 餀最 蜀䴀 萀̀ช ̀ ਀ਁ। ੊ì 
ਈ䨊搊䴉 鈀਀ਃì ੍íਃì ਁ। ੍؊朊鈊 䨀ਊ੧ ਊਃ੧ ਆ।ੇ ੊ë੍ 䨊搊 鈀最 萀ࠀꀊ 䤀脊䨀朊̊؊ⴊ䬀䜀 茀Ā朊 ഀ਀䤀Ā脊Āਊ ੯ëਃì ਊਊ搊̉ ؀搊 鈀਀ਃìû
     ਃੇìਈ ੬ 鰀䨀搊ꀉ搀 ؀朊 鈀脀䨀䜊 ̀朊 鈀਀ਃ। ਀।੍ì ੍ ਃ੉੮੉  ਃì ੫ੋëੋ ìਁì ੍੨ ë 
੉।੧ ੉਎। ੊ë ੇ੯ì ।੍੧ ëਆì ੊ì ਈ੯੮੍ ੇë ।, ੍ ੉íਁਁ ऀ漊栊䤊䜊朊 ؀䴊脀 鈀਀ 
੉ਃìû ਈ ì ਎養漀 ؀搊䜉 䤀؀朊ช搊 ؀䴊脀 餀搀 䤀鈊̀Ⰰ 䤀栊ਊ ਆ੍ì  䤀鈊̀ 茀Ā朊 ؀搊 䴀 鈀䤀朊 ഀ਀ࠀ਀ ਎ëਁ੧ਁ ؀搊䜉 ̀權䤊Ċ 鈀਀ ੉ਃì ੍੨û ੉।੧ ੉਎। ੊ë ਈ੯੮੍ë-ੇëë ੊鐊 䨀 䤀䴊 ഀ਀฀搊茉؀萀脀 䨀需 鈀䤀朊 䤀Ā脊Āਊ Ċ ؀脀 ਀ਊਃ।ੋਁ ਆ੍ì  䤀鈊̀搊﬉ 䤀搊鸉最 䤀ช搊餉 䨀需 退਀ਁ ੉   
ੇ  ੉੧੉-੉ëਊੇ ੍੨û ੍ Ċ ̀搊 鈀̀渊̊ⴊ਀Ċ渊Ċ 茀Ā朊 ਀îਈ-脊鐀 ̀朊錊 鈀最 䴀 蜀䤀؊脀 ࠀ䤊脊̀ ഀ਀餀搀脉 ؀搊 ࠀ䤊脊̀ 鈀਀ਃ। ੍੨û ੉ ੇ ë੉੧ Ċ ̀ 餀䴀؊ⴀ錀਀䤊਀ਁì ਃ੧ ੫ Ȋ ؀䴊脀ﬀ 蔀䤀朊 ഀ਀䜀蘊 茀䤀脀 ̀朊錊̀朊 䴀搊脉 鈀 錀਀䤊਀ਁ ਁ੧ Ȋⴊ଀ਊਈî 餀漀脀̀鐊 餀蠀ꀀ ̀ 蜀฀脊鐀 䨀需 蔀Ā؊  ഀ਀䜀脊਀ 䨀搊鰉 需䜀渊䜊 鈀最 䨀 茀脀Ā 鈀脀䨀䜊 ؀脀 䴀ਊਆ੧ ਆ।ੇ ੍ì ੯ëਃì ë  ਃ। ਉ੯੨੉ੇ। ؊搊 ഀ਀ࠀ栊脊̀搊 䴀栊﬊ ଀搊ਉਁì ਈ䨀朊䬊 䨀需 蔀鈀 怀退਀ਁ ਃì ੊।' ੊। ਊ੯ì ੇਊì ਁ੧ ਏ।ਁਆ। ਋ࠊ਀ 
੍੨û
     ੉।੧ ੉਎। ੊ë ੧੊ੇ ਆ। 䴀 ਀朊ਊ䤊 茀Ā朊 䨀需搀ਉì-ë੍ì ੯ëਃì ਆ੍ì ëਃì 
੉੫ ੊੮ì-੊੮ì ੫ì ੊ë ਊਅî ਎੯ਃî搊脉 䨀ਊì ੯ëਃì ì  ਁ੧ ਎ਊîⰊ ഀ਀頀欀鰊最ⴊ頀欀鰊最 ਀霊渀霊最 䨀 茀Ā茀脀Ā ̀錀̀搊蘉 餀漀脀̀鐊 䨀需欀脊 鐀餀਀ 䴊朊 䴀؊﬊ 蔀䤀 ؀搊䨉䜊 䨀需 ഀ਀怀餀Ā✀ ̀朊 鈀਀ਃ। ̀萀਀। ਆ।੊ੇ। ؀朊 ฀䴀؊Ċⴊ฀養漀̊਀ì  ਀੍੧ ਆ੮੍। ੫੧-੫੧ 
ਊ੮ë ਃ੧ ì੊ਆ ਃ੧ ꀀⴀࠀ渊ਊ੉ ਈ ੧ ਈ।। ਆ।ੇ ੉।੧ ìਁ੧ ੍ਆû
     ਎।-ਊ।ਈ ਁ੧ ਈ ੧ ੉਋ë।鈊 ࠀਊë੊੧ੋ ਆ।ੇ੫ í੮ ੧ 權養漀ⴀ਀੫ì ਃì ਋।ੇ ੊ë 
ਊ੍íਁ ਃî ؀鈀䜀 萀謀 茀餀䴀朊 ਀霊渀霊萀脀 ̀ ̀錀̀搊蘉 餀漀脀̀鐊Ⰰ 蔀䤀 ؀搊䨉䜊 ̀搊 ̀餀搀 ഀ਀฀䴊Ċ渊Ċ䨊ࠊ਀  ੊ëੋ। ੍੨û ੉ੇ ੊ë `੊। ੍਎।✀ ؀搊䨉䜊 ̀搊 萀਀਋ ਁ੧ ਁ ਃ੫੊੧ ìਁî ਃì 
੯ëਃì ਃ੧ ꀀⴀࠀ渊ਊ੉ ਆî ੍ì ਈ੧ੋ ̊朊 䴀؊﬊ 蔀䤀 ؀搊䨉䜊 ̀朊 ࠀ䴊䜀朊 鈀搀脉鸀 䨀需 餀Ā ഀ਀萀ࠀꀊ最 ฀搊䜉鈊搀脉 䜀蘊 䤀ਊ養漀 䜀栊 鈀最 鈀欀鴊 䨀需 ࠀਊë੊੧ੋ ̊搊 ̀錀搀蔉萀 鐀萀 䴀栊﬊ 蔀䤀 鈀搀脉鸀 䨀需 ഀ਀蜀䴀 鈀欀鴊 䨀需 䤀ऊ搊蘉萀脀 萀̀鈀 鈀਀ਆ ਁ੧ ੇ੮ì ੍੫ ਈĊ搊ࠉ 茀Ā朊 鐀餀搀 ฀脊鐀ꀀ 萀謀 䤀搊ԉ ؀脀  ഀ਀਀਀।-਋ੇ। ੍ëਃ। ੍੨û ੉ਃ। ëੇ ੍੨ ë ੍ ੇ੫ ੉ਃ੧ ਎।ੇ। ਃ। ਆí੉।ਆ ̊朊 䴀؊﬊ ഀ਀蜀䴀 ฀搊䜉鈊ꀀ ̀萀脀 ؀氊養䨀搊؉ Ԁ萀脀 ̀搊 䨀 ࠀ਀।-ਈî搊 錀萀䜀 ਀ਃ। ੍੨û  ੊। 蔀؀渊䴊搊脉 ഀ਀鈀鸀漀萀脀 ؀脀 䤀؀朊ช搊 餀搀ꀉ 䜀蘊 ࠀ栊䤊萀脀 ̀ 餀਀îĊ ࠀ栊脊̀ 䴀栊 Ā搊脉 蜀䴀 萀ࠀꀊ Ā؊錊漀搊䴉ഊ਀̀朊 ࠀ栊䤊萀脀 ؀搊䜉 蔀؀渊䴊搊脉 ؀脀 鰀鈀鰀搀脉 錀਀ìਃ ੧ ਃ੧ ਃëਃ। ੍੨û ੍ ਆ੮੍। í੯ì ਃì 
ਊ੧੯੮੯ਁì । ਊਃਆ।਎ì ਆ੍ì ੉੍। 䤀鈊̀搊﬉ 蔀鈀 䨀搊ਉ ਃ੫ ੊ੇ ਁ੧ ੉ਃì ੫ì ਋੨  
ë؊ 鈀搀䜉餀 ̀朊 鈀䤀朊 ऀ脊鈀䤀؊ Ā權脊 ਀।ਁ ਃ੧ ੉਎੧ ਈ ੧  ࠀਊਁ 䴊萀脀 䴀脀̀萀脀 䴀؊ Ā搊脉 ഀ਀蔀鈀 鐀脀鸀搀 蜀؀渊䴊搊脉 ؀脀 頀最鸊漀ꀊ ̀ 鈀欀䬊䬀 鈀਀ਃ। ੍੨û ìਁî ਈ ੧ ਁ੫ ਃí ੧ ੉ ਎í੧ ਃ੧ 
ੇ ਈ੨ ।ਃ। ੍੨û ਋।੊੧ ੉ ੇ੯। ੊ë ੉ਆî ।ਉ੯ì ੉੮। ੇ੮ਃì ੍ਆ ਈ脊Ā 蜀䴀 ഀ਀萀ࠀꀊ ฀搊䜉鈊ꀀ ̀萀脀 Ԁ萀脀 蜀ࠀ渊ࠊਊ ੫  ਆ੍ì   ਃëਃ।û   
     ìਁî ਃì ਆ੯ 䨀需 蜀䴀 萀ࠀꀊ ฀搊䜉鈊ꀀⰀ 蜀䤀̊萀脀 Ā脀؀ Ԁ萀脀 茀Ā朊 蔀鈀 ࠀĀ渊Ċਊ ੉íìਁ  
ਃ। ੍ì ਆ੬ 䴀栊﬊ ฀搊䜉鈊ꀀ ̀萀脀 Ԁ萀脀 ̀朊 ࠀĊ萀脀 茀Ā朊 ฀搊䜉鈊ꀀ ̀朊 ࠀĀ渊Ċਊ ੉íìਁ ਃì ਈਁਆì 
੧ਁਆ। ਆî ੍ `੯੨✊ 䴀 䤀ช騊̀搊 䴀栊﬊ 蔀䤀朊 䜀蘊 蜀؀渊䴊搊脉 ̀搊 ؀搊 Ā搊脉 鈀䴀ꀀ搀 䴀 ฀脊؀̊搊 䴀栊 ഀ਀茀Ā朊 ؀搊 䴀 蜀؀渊䴊搊脉 ̀搊 鈀欀蘊 鈀脀฀ 鈀਀ਃ। ੍੨û ੍ ੧ਁਆ। ਆî `੧ ਊ।ੇ੫ ੊।ੇì ਎੧਎ ੉।੍ਊ' 
੍ëਃ। ੍੨ ਁ੧ ੉íìਁ ਆ।ੇ ìਁ੧ ੉ਃ੧ ਈ੮朊ชⴊ䨀萀䴀 ؀脀 需脀鐀搀 ؀䴊脀 䤀ช騊̀搊﬉ 蔀䤀朊 ഀ਀Āਊ੮੍। ੍ ਎ਅî ਃ੧ ਈਁì ਊੇਊì ؀脀 䨀 需脀鐀搀 ؀䴊脀 䤀ช騊̀搊﬉ 蔀鈀 䨀搊ਉ 
੍ ਆ੮੍। ਈ।੉ ਃëੇ੮ੇì ੇ। ।ਃ। ੍੨û ਂ੧  ੊। ਀䜊ਊ਀ ë੉੧ ੇ੮ੇ੫ ਎ਅî ਆî 
ë੯ ਃëਃ। ੍੨û ìਁî ੉ ੉ਂëਁì ਆî ਈ੮䨊搊؉ ؀䴊脀 鈀਀ ੉ਃ। ਁ੧ ੍ ਊੇਊì ؀搊䜉 ഀ਀鐀䤀渊䤊朊 䴀朊 鈀最 䨀搊ࠉ䤊 ࠀ鰊萀䜀朊 萀 餀搀脉̀搊 䴀栊﬊ ഀ਀     蔀䤀 Āਊ੮੍। ੉ì ੊੧ਃ੧ ੍। ë ìਁî ਈ ੧ ੉।朊 䤀ࠀ؊朊 茀Ā朊 蔀頀渀頊搀䨉搊脉 䤀ਊਃ।؊ ഀ਀茀Ā朊 蜀䤀̊朊 ࠀਊë੊। 蜀Ā權脊 鈀਀ਊ।ਆ  ̀脀̀搊 䴀栊﬊ 蜀䴀 萀ࠀꀊ 餀漀脀̀鐊 ̀搊 鈀最䨊䜊 蔀鈀渀鈊最ⴊ蔀鈀渀鈊 ഀ਀ࠀ渊ਊਏ੫ਆ ਎ëਂì ਊ੨। ੍੨ : ੉ ਈ䨀搊ਉ ਃì ੉੧੊। ؊搊﬉ ࠀਊਁî ੉ ਃ੧ ੊੯ ੊ë ੉ ਆî 
ì ਎ëੇਃ। ੍੨? ë੯।, ਂਈ੮ਈ੯ ਁ੧ íਁ੮ਁìû ੮ ੊। 蜀䴀 需最Ċ؊搊 ̀萀脀 鐀䜀渊䜊搊脉 ഀ਀䨀需 萀 鈀最 萀ࠀꀊ 䬀搊̉ ̀朊 錀漀搊ਉ 䨀朊錊ꀀ最 䬀਀î  ̀脀̀搊 䴀栊﬊ 餀̀ 蜀䤀̊朊 蔀䴀 錀漀搊ਉ 鰀鰀渀鰊̀朊 ഀ਀䴀؊ Ā搊脉 蜀䴀 ਀鸊漀 ؀਀।ੋ਎ì ੉ਂëਂì ੊ë੮ ਈ੍í ।ਃ। ੍੨û ੉ ਎ਆ੫ ੉ਂëਁì ੊ë੮ ੉ਆî 
ੋ搊ਉ ̀ Ā䜊ਊ 䴀脀̀ 䴀栊﬊ 䬀ਊ।ਊ ̀ꀊ 䨀搊䤉Ċ朊 蜀䴀 需最Ċ؊搊 ̀ 䴀 销鸀漀 需਀। ੇ੨ਃ। ੍੨û 
੉íìਁ ੉ਃ੧ ਂਈ੮ਈ੯ ਎। 鈀最 蜀䤀؊脀 销਀੫ ਊ।੍ 鈀鼀渀鼊 ̀脀̀搊 䴀栊﬊ ऀ਀ਃ।-ਉë蜊脀̀搊 蜀䴀 ഀ਀̀䜀渊䜊 ࠀ䴊脀需 餀搀脉̀搊 䴀栊﬊ ଀錀 Ā朊 ̀錀渀錊欀脊 蔀Ȁ渊Ȋ朊 䨀 蜀䴀 蔀鈀渀鈊 需欀ਊì  ਀栊鴊̀搊 䴀栊 ഀ਀ࠀਊਁî ëë ੍ ੫ ਈ੧ੋ।੊ 需欀ਊ ਆ੍ì ੉ ੇ ੍ ੮ ੍ì ਉ੯ë ।ਃ। ੍੨û ੉ਆî 
ਁëਆ ਎੍ìਆ੧ ਃì ੨ਃ ੍੫ ।ਃì ੍੨û ੨ਃ ਃ੧ ਃ੬搊؉ 䴀 蜀䤀؊脀 鰀⸀ ਀⸀ 䴀權 餀搀脉̀ 䴀栊﬊ ഀ਀਀ë੍। ੍੫  ਈë੫ ੍ ਊ੍íਁ ੊渊䴊萀脀 ਀搊̉ 萀ࠀꀊ最 怀̀朊䬊✊ ਀䴀搊ਉ ੇ। ।ਃ। ੍੨û ੉ਃì 
੍।ੇਁ ਃëਆ੫-ਃëਆ ੊ë੯ਃì ੇì ।ਃì ੍੨û ੯ 蜀䴀 ฀ਊਆ ëਆ।朊 ࠀ䴊脀需 餀搀脉̀搊 䴀栊 ࠀਊਁí 
੉ ੉ਂëਁì ੊ë੮ ੊ì ੍ ਈ ੧ ਎।ੇ। ਆî ਋íੇ੮ੇਃ। ਆ੍ì, ੉੫ ਃ੫ਊ।搊 蜀؀渊䴊搊脉 鈀欀䜊 ഀ਀ࠀ䴊脀需ꀀ ̀ 萀਀੯î ੊ë੮ ੮ ë੮ì ੇëਃ। ੍੨ :
                       "ਏ੍। ਈ 䤀ଊ ਀।੯ì íੋì ੍੨, ਈ ì 搊餉漀 錀䬀 ഀ਀                       ଀鐊䨀搊؉ 䤀朊 需搀䴉Ċ搊 䴀脀ﬀ ฀栊脊 ༀ䴊搊脉 䴀脀 销਀ ਈ﬊ഀ਀⬀☀最琀㬀ഀ਀               　 ഀ਀㰀⼀瀀㸀㰀⼀戀漀搀礀㸀㰀⼀琀攀砀琀㸀㰀⼀挀攀猀䐀漀挀㸀�