<cesDoc id="pun-w-socsci-ling-lot16q" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-socsci-ling-lot16q.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>PUNJABI SABAD JOD</h.title>
<h.author>SITAL JEET SINGH</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - PUNJABI VIBAG - PATIALA</publisher>
<pubDate>1981</pubDate>
</imprint>
<idno type="CIIL code">lot16q</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 16-17.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-23</date></creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;2117&gt;&lt;SITAL J. S-PUNJABI SABAD JOD&gt;&lt;Shashi Bhushan&gt;</p>

<p>&lt; ਨਾਇਬ ਨੈਬ
ਨਾਇਣ ਨੈਣ
ਸਹਾਇਤਾ ਸਹੈਤਾ
ਲਾਇਕ ਲੈਕ
ਗਾਇਕ ਗੈਕ</p>

<p>ਬਿਹਾਰੀ ਦਾ ਚਿਂਨ੍ਹ XXX ਵਰਣ ਦੇ ਸਜੇ ਪਾਸੇ ਲਗਦਾ ਹੈ । ਸਿਹਾਰੀ ਵਾਂਗ ਇਸ ਲਗ ਵੀ
`ਓ' ਤੇ `ਅ' ਨਾਲ ਨਹੀਂ ਵਰਤੀ ਜਾਂਦੀ । ਬਿਹਾਰੀ ਦੀ ਸਿਹਾਰੀ ਨਾਲੋਂ ਦੂਣੀ ਮਾਤਰਾ
ਹੁਂਦੀ ਹੈ, ਇਸ ਲਈ ਇਹ ਦੀਰਘ ਸਵਰ ਹੈ ਜਿਵੇਂ `ਈਣਾ', `ਖੀਣ ਖਾਬ', `ਖੀਰ', `ਕੀਮਤ',
`ਸੀਮਾ' ਆਦਿ ।</p>

<p>(ਓ) ਸਂਸਕ੍ਰਿਤ ਦੇ `ਭਾਰਤੀਯ', `ਨਿਂਦਨੀਯ' `ਪੂਰਬੀਯ' ਆਦਿਕ ਸਬਦਾਂ ਵਿਚਲਾ `ਯ'
ਪਂਜਾਬੀ ਵਿਚ ਡਿਗ ਜਾਂਦਾ ਹੈ ਤੇ ਬਿਹਾਰੀ ਉਸ ਦੇ ਉਚਾਰਣ ਨੂਂ ਪੂਰਾ ਕਰ ਦਿਂਦੀ ਹੈ ।
ਇਉਂ ਪਂਜਾਬੀ ਵਿਚ ਉਪਰਲੇ ਸ਼ਬਦ `ਭਾਰਤੀ', `ਨਿਂਦਨੀ', ਤੇ `ਪੂਰਬੀ' ਰੂਪ ਰਹਿ ਜਾਂਦੇ
ਹਨ ।</p>

<p>(ਅ) ਆਮ ਤੌਰ ਤੇ ਸਂਸਕ੍ਰਿਤ ਦੇ `ਸਿਹਾਰੀ' ਅਂਤ ਵਾਲੇ ਸ਼ਬਦ ਪਂਜਾਬੀਵਿਚ `ਬਿਹਾਰੀ' ਅਂਤ
ਹੋ ਜਾਂਦੇ ਹਨ, ਜਿਵੇਂ :--</p>

<p>ਸ਼ਕਤਿ ਸ਼ਕਤੀ
ਗਤਿ ਗਤੀ
ਪਤਿ ਪਤੀ
ਭਕਤਿ ਭਗਤੀ
ਮੁਕਤਿ ਮੁਕਤੀ
ਸਮ੍ਰਿਤੀ ਸਿਮਰਤੀ
ਯੁਕਤਿ ਯੁਕਤੀ
      ਜਗਤੀ
ਬੁਧਿ ਬੁਧ੍ਧੀ</p>

<p>ਐਂਕਡ਼ ਦਾ ਚਿਂਨ੍ਹ (_) ਵਰਣ ਦੇ ਹੇਠਾਂ ਲਗਦਾ ਹੈ, `ਝ' ਤੇ `ਅ' ਨਾਲ ਇਸ ਦੀ ਵਰਤੋਂ
ਨਹੀਂ ਹੁਂਦੀ । ਬਾਕੀ ਸਭ ਵਰਣਾਂ ਨਾਲ ਵਰਤਿਆ ਜਾਦਾਂ ਹੈ । ਇਹ ਲਘੂ ਮਾਤਰਾ ਹੈ । `ਓ'
ਤੇ `ਹ' ਅਂਤ ਵਾਲੇ ਸ਼ਬਦਾਂ ਤੋਂ ਸਿਵਾਇ ਬਾਕੀ ਕਿਸੇ ਵੀ ਸ਼ਬਦ ਦੇ ਅਂਤਲੇ ਵਰਣ ਵਿਚ
ਐਂਕਡ਼ (_) ਨਹੀਂ ਆਉਂਦਾ; ਜਿਵੇਂ:--ਦਿਉ, ਘਿਉ, ਪਿਉਂ, ਤਿਉਂ, ਨਹੁਂ, ਸਹੁਂ, ਰਹੁ,
ਸ਼ਹੁ ।</p>

<p>ਆਮ ਤੌਰ ਤੇ ਸਂਸਕ੍ਰਿਤ ਦੇ ਐਂਕਡ਼ ਅਂਤ ਵਾਲੇ ਸ਼ਬਦਾਂ ਦੇ ਐਂਕਡ਼ ਦਾ ਉਚਾਰਣ ਪਂਜਾਬੀ
ਵਿਚ ਡਿਗ ਜਾਂਦਾ ਹੈ ਜਿਵੇਂ :--</p>

<p>ਸਾਧੁ ਸਾਧ
ਗੁਰੁ ਗੁਰੁ</p>

<p>ਜਿਨ੍ਹਾਂ ਸ਼ਬਦਾਂ ਵਿਚ `ਚ' ਦੂਜਾ ਵਰਣ ਹੋਵੇ ਤੇ ਉਸ ਤੋਂ ਪਹਿਲਾ ਵਰਣ ਕਨੌਡ਼ੇ ਦੇ
ਉਚਾਰਣ ਵਾਲਾ ਹੋਵੇ, ਤਾਂ ਉਸ ਦੀ ਥਾਂ ਲਿਖਣ ਵੇਲੇ ਉਹ `ਹ' ਹੇਠ ਐਂਕਡ਼ ਵਿਚ ਬਦਲ
ਜਾਵੇਗਾ; ਜਿਵੇਂ :--</p>

<p>ਬੌਹਤ ਬਹੁਤ
ਬੌਹਤਾ ਬਹੁਤਾ
ਬੌਹ ਵਚਨ ਬਹੁ-ਵਚਨ
ਮੌਹਰਾ ਮਹੁਰਾ
ਸੌਹਰਾ ਸਹੁਰਾ
ਵੈਹਟੀ ਵਹੁਟੀ
ਨੌਂਹ ਨਹੁਂ
ਸੌਂਹ ਸਹੁਂ</p>

<p>ਦੁਲੈਂਕਡ਼ ਦਾ ਦਿਂਨ (=) ਹੁਂਦਾ ਹੈ ਤੇ ਐਂਕਡ਼ ਵਾਂਗ ਵਰਣਾਂ ਦੇ ਹੇਠ ਲਗਦਾ ਹੈ । `ਝ'
ਤੇ `ਅ' ਨਾਲ ਇਸ ਦੀ ਵਰਤੋਂ ਨਹੀਂ ਹੁਂਦੀ ਬਾਕੀ ਸਭ ਵਰਣਾਂ ਨਾਲ ਆਂਉਂਦਾ ਹੈ,
ਜਿਵੇਂ:--ਊਂਘ, ਊਲ, ਜਲੂਲ, ਐਲੂ, ਆਲੂ, ਚੂਡ਼੍ਹੀ ।</p>

<p>ਇਹ ਦੀਰਘ ਮਾਤਰਾ ਹੈ । ਇਹ ਚਿਂਨ੍ਹ ਵਰਣਾਂ ਦੇ ਆਦਿ, ਮਧ੍ਧ ਜਾਂ ਅਂਤ ਵੀ ਆ ਸਕਦਾ ਹੈ ।</p>

<p>ਨਾਂ ਦਾ ਚਿਂਨ੍ਹ XXX ਹੁਂਦਾ ਹੈ ਤੇ ਇਹ ਵਰਣ ਦੇ ਉਤੇ ਲਗਦਾ ਹੈ `ਓ' ਤੇ `ਅ' ਤੋਂ ਛੁਟ
ਇਸ ਦੀ ਵਰਤੋਂ ਸਾਰੇ ਵਰਣਾਂ ਨਾਲ ਹੁਂਦੀ ਹੈ; ਜਿਵੇਂ:-- ਏਕਤਾ, ਏਲਚੀ, ਕੇਲਾ, ਮੇਮ,
ਤੇਲ, ਕੇਲੇ, ਮੇਲੇ, ਸਾਡੇ ਆਦਿ ਇਹ ਦੀਰਘ ਮਾਤਰਾ ਹੈ ।</p>

<p>ਇਹ ਚਿਨ੍ਹ ਵੀ ਵਰਣਾਂ ਦੇ ਆਦਿ, ਸਧ ਤੇ ਅਂਤ ਵਿਚ ਆਉਂਦਾ ਹੈ ।</p>

<p>ਜਿਨ੍ਹਾਂ ਵਰਣਾਂ ਵਿਚ `ਹ' ਦੂਜਾ ਵਰਣ ਹੋਵੇ ਤੇ ਉਸ ਦੇ ਪਹਿਲੇ ਵਰਣ ਉਤੇ ਲਾਂ ਆ ਰਹੀ
ਹੋਵੇ ਤਾਂ ਉਸ ਨਾਂ ਨੂਂ ਸਿਹਾਰੀ ਵਿਚ ਬਦਲਣਾ ਹੀ ਉਚਿਤ ਹੈ ਤੇ ਉਸ ਦਾ ਉਚਾਰਣ ਲਾਂ
ਵਾਲਾ ਹੀ ਰਹਿਂਦਾ ਹੈ, ਜਿਵੇਂ:--</p>

<p>ਸੇਹਤ ਦੀ ਥਾਂ ਸਿਰਤ
ਮੇਹਨਤ " " ਮਿਹਨਤ
ਜੇਹਡ਼ਾ " " ਜਿਹਡ਼ਾ
ਕੇਹਡ਼ਾ " " ਕਿਹਡ਼ਾ
ਸੇਹਰਾ " " ਸਿਹਰਾ
ਮੇਹਰ " " ਮਿਹਰ
ਏਹ " " ਇਹ
ਜੇਹਾ " " ਜਿਹਾ
ਏਨ੍ਹਾਂ " " ਇਨ੍ਹਾਂ
ਏਹਨਾ " " ਇਹਨਾ</p>

<p>ਦੁਲਾਈਆਂ ਦਾ ਚਿਂਨ੍ਹ XXX ਹੁਂਦਾ ਹੈ ਤੇ ਇਹ `ਲਾਂ' ਵਾਂਗ ਵਰਣ ਦੇ ਉਤੇ ਲਗਦਾ ਹੈ ।
`ਓ' ਤੇ `ਝ' ਤੋਂ ਛੁਟ ਇਹ ਸਾਰੇ ਵਰਣਾਂ ਨਾਲ ਵਰਤਿਆ ਜਾ ਸਕਦਾ ਹੈ । ਜਿਵੇਂ ਐਤਵਾਰ,
ਐਬ, ਮੈਲਾ, ਕਹੀਐ, ਆਦਿ;</p>

<p>ਝ, ਢ, ਯ ਵਰਣਾਂ ਨਾਲ ਇਸ ਦੀ ਵਰਤੋਂ ਘਟ ਹੁਂਦੀ ਹੈ ।</p>

<p>ਇਹ ਦੀਰਘ ਮਾਤਰਾ ਹੈ ।</p>

<p>ਇਹ ਚਿਂਨ੍ਹ ਵੀ ਲਾਂ ਵਾਂਗ ਸ਼ਬਦਾਂ ਦੇ ਆਦਿ, ਵਿਚਕਾਰ ਜਾਂ ਅਂਤ ਵਿਚ ਆ ਸਕਦਾ ਹੈ ।</p>

<p>ਜਿਨ੍ਹਾਂ ਸ਼ਬਦਾਂ ਵਿਚ `ਹ' ਦੂਜਾ ਵਰਣ ਹੋਵੇ ਤੇ ਉਸ ਤੋਂ ਪਹਿਲੇ ਵਰਣ ਤੇ ਦੁਲਾਈਆਂ ਆ
ਰਹੀਆਂ ਹੋਣ ਤਾਂ ਉਹ ਦੁਲਾਈਆਂ `ਹ' ਨਾਲ ਸਿਹਾਰੀ ਦੇ ਰੂਪ ਵਿਚ ਬਦਲ ਜਾਂਦੀਆਂ ਹਨ
ਜਿਵੇਂ :-- ਕੈਹਣਾ ਕਹਿਣਾ ਰੈਹਣਾ ਰਹਿਣਾ ਸੈਹਣਾ ਸਹਿਣਾ ਬੈਹਣਾ ਬਹਿਣਾ ਪੈਹਰ ਪਹਿਰ
ਮੈਹਰਾ ਮਹਿਰਾ ਨੈਹਲਾ ਨਹਿਲਾ ਦੈਹਲਾ ਦਹਿਲਾ ਕੈਹਰ ਕਹਿਰ ਸ਼ੈਹਰ ਸ਼ਹਿਰ ਗੈਹਣਾ ਗਰਿਣਾ</p>

<p>ਹੋਡ਼ੇ ਦਾ ਚਿਂਨ੍ਹ XXX ਹੇ, ਤੇ ਇਹ ਵੀ ਲਾਂ, ਦੁਲਾਈਆਂ ਵਾਂਗ ਵਰਣਾਂ ਦੇ ਉਪਰ ਲਗਦਾ
ਹੈ ਇਹ ਚਿਂਨ੍ਹ `ਅ' ਤੇ `ਝ' ਤੋਂ ਛੁਟ ਬਾਕੀ ਸਾਰੇ ਵਰਣਾਂ ਨਾਲ ਲਗਦਾ ਹੈ, `ਓ' ਦੇ
ਨਾਲ ਲਗਣ ਸਮੇਂ ਇਸ ਦਾ ਰੂਪ `ਓ' ਦੇ ਖੁਲ੍ਹੇ ਮੂਂਹ ਵਾਲਾ ਹੋ ਜਾਂਦਾ ਹੈ ਜਿਵੇਂ (ਓ);
ਓਂਕਾਰ, ਓਟ, ਓਪਰਾ, ਕੌਲਾ, ਮੌਕਲਾ, ਧੋ, ਰੋ, ਨੋਟ, ਕੋਟ । ਇਹ ਦੀਰਘ ਮਾਤਰਾ ਹੈ ।</p>

<p>ਕਨੌਡ਼ੇ ਦਾ ਚਿਂਨ੍ਹ XXX ਹੁਂਦਾ ਹੈ ਤੇ ਇਹ ਵੀ ਹੋਡ਼ੇ ਵਾਂਗ ਵਰਣਾਂ ਦੇ ਉਪਰ ਲਗਦਾ ਹੈ
ਇਹ ਚਿਂਨ੍ਹ `ਓ' ਤੇ `ਝ' ਤੋਂ ਬਿਨਾਂ ਬਾਕੀ ਸਭ ਅਖ੍ਖਰਾਂ ਨਾਲ ਲਗਦਾ ਹੈ ।</p>

<p>ਇਹ ਦੀਰਘ ਮਾਤਰਾ ਹੈ । ਇਸ ਦੀ ਅਵਾਜ਼ `ਕਂਨਾ+ਉ' ਦੇ ਬਰਾਬਰ ਹੈ । `ਕਂਨਾ+ਉ' ਵਾਲੇ
ਸ਼ਬਦਾਂ ਵਿਚ ਇਸ ਦੀ ਵਰਤੋਂ ਹੇਠ ਲਿਖੇ ਅਨੁਸਾਰ ਪਰਚਲ੍ਲਤ ਹੈ:--ਕ੍ਰਿਆ ਵਾਲੇ ਸ਼ਬਦ :</p>

<p>ਮਨਾਉਣਾ
ਖਿਡਾਉਣਾ
ਸਤਾਉਣਾ
ਲਾਉਣਾ
ਸਕਾਉਣਾ</p>

<p>2. ਕ੍ਰਿਆ ਤੋਂ ਬਿਨਾਂ ਦੂਜੇ ਸ਼ਬਦਾਂ ਵਿਚ ਆਮ ਤੌਰ ਤੇ ਕਨੌਡ਼ੇ ਦੀ ਵਰਤੋਂ ਹੁਂਦੀ ਹੈ
ਜਿਵੇਂ:--ਕੌਣ, ਧੌਣ, ਚੌਥਾ, ਭੌਣੀ ।</p>

<p>3. ਕਈ ਇਕ ਸ਼ਬਦ ਨਕੌਡ਼ੇ ਨਾਲ ਹੀ ਲਿਖੇ ਜਾਣੇ ਯੋਗ ਹਨ :-- ਗੌਹਰ, ਜੌਹਰ, ਨੌਕਰੀਆਂ ।</p>

<p>ਬਿਂਦੀ ਤੇ ਟਿਪ੍ਪੀ (&lt;?&gt; XXX ਹੈ ਤੇ ਟਿਪ੍ਪੀ ਦਾ XXX ਹੈ । ਇਹ ਦੋਵੇਂ ਚਿਂਨ੍ਹ ਵਰਣਾ
ਦੇ ਉਪਰ ਲਗਦੇ ਹਨ । ਇਨ੍ਹਾਂ ਦੋਹਾਂ ਦੀ ਆਵਾਜ਼ ਨਕ੍ਕ ਰਾਹੀਂ ਨਿਕਲਦੀ ਹੈ ਤੇ ਇਸ
ਨਿਹਾਜ਼ ਨਾਲ ਇਨ੍ਹਾਂ ਵਿਚ ਕੋਈ ਫ਼ਰਕ ਨਹੀਂ ਹੁਂਦਾ ।</p>

<p>ਪਂਜਾਬੀ ਵਿਚ ਲਗਾਂ ਮਾਤਰਾਂ ਨਾਲ ਇਨ੍ਹਾਂ ਦੀ ਵਰਤੋਂ ਇਸ ਤਰ੍ਹਾਂ ਪਰਚਲ੍ਲਤ ਹੈ:--
ਕਂਨਾਂ &lt;?&gt; ਜਿਵੇਂ ਮਾਂ, ਗਾਂ, ਮਰਾਂ, ਕਾਂਗ ਬਿਹਾਰੀ XXX ਪੀਂਘ, ਸੀਂਢ, ਜੀਂਦ, ਹੀਂਗ
ਨਾਂ XXX ਮੇ ਗਣ, ਰੇਂਡੀ, ਦੇਂਦੀ, ਸੇਂਧਾ ਦੁਲਾਈਆਂ XXX ਪੈਂਡਾ, ਕੈਂਨਾ, ਗੈਂਡਾ,
ਤੈਂ ਗਡ਼ ਹੈਡ਼ਾ XXX ਗਂਗਲੂ, ਚੋਂਘਾ, ਮੋਂਗਾ, ਰੋਂਦਾ ਕਨੈਡ਼ਾ XXX ਲੌਂਗ, ਚੌਂਕ,
ਸ਼ੌਂਕ, ਗੌਂਡ ਮੁਕਤਾ ਅਂ, ਕਂ ਜਿਵੇਂ ਸਂਤ, ਵਂਡ, ਪਂਡ ਸਿਹਾਰੀ XXX ਹਿਂਗ, ਬਿਂਦੀ
ਐਂਕਡ਼ XXX ਕੁਂਡ, ਘੁਂਡ, ਖੁਂਗ, ਫੁਂਡ ਦੁਲੈਂਕਡ਼ੇ XXX= ਚੂਂਡਾ, ਡੂਂਘਾ, ਕੂਂਡਾ
(ਓ) `ਓ' ਨਾਲ ਹਮੇਸ਼ਾ ਵਿਂਦੀ ਹੀ ਲਗਦੀ ਹੈ, ਜਿਵੇਂ :-- ਕਿਉਂ, ਉਂਝ, ਉਂਘ, ਇਉਂ,
ਜਿਉਂ ।</p>

<p>ਉਪਰ ਸਵਰਾਂ ਦੀ ਵਿਆਖਿਆ ਹੋ ਚੁਕੀ ਹੈ; ਵਿਅਂਜਨਾ ਦੀ ਵਿਆਖਿਆ ਤੋਂ ਪਹਿਲਾਂ ਅਧਕ ਦੀ
ਵਿਚਾਰ ਉਚਿਤ ਹੈ ।</p>

<p>ਅਧਕ ਵਿਅਂਜਨ ਦੀ ਅਵਾਜ਼ ਨੂਂ ਦੂਹਰੇ ਕਰਨ ਦਾ ਚਿਂਨ੍ਹ (&lt;?&gt;) ਹੈ । ਇਹ ਰਮੇਸ਼ਾ ਦੁਹਰੇ
ਹੋਣ ਵਾਲੇ ਵਰਣ ਦੇ ਉਪਰ ਖ੍ਖਬੇ ਪਾਸੇ ਲਗਦੀ ਹੈ; ਜਿਵੇਂ :--`ਸਚ੍ਚ', ਤੇ `ਅਲ੍ਲਾ'
ਵਿਚ `ਚ' ਤੇ `ਲ' ਦੀ ਅਵਾਜ਼ ਦੂਹਰੀ ਹੈ ਤੇ ਅਧਕ `ਚ' ਤੇ `ਲ' ਦੇ ਉਪਰ ਖਬ੍ਬੇ ਪਾਸੇ
ਲਗੀ ਹੈ ।</p>

<p>(ਓ) ਅਧਕ ਆਮ ਤੌਰ ਤੇ ਦੀਰਘ ਸਵਰਾਂ ਨਾਲ ਨਹੀਂ ਲਗਦੀ । ਇਹ ਕੇਵਲ ਮੁਕਤਾ, ਸਿਹਾਰੀ (ਿ)
ਤੇ ਐਂਕਡ਼ (_) ਵਾਲੇ ਵਰਣ ਉਪਰ ਹੀ ਲਗਦੀ ਹੈ । ਜਿਵੇਂ: ਮਤ੍ਤ, ਨਿਤ੍ਤ, ਚਿਤ੍ਤ
ਪੁਤ੍ਤ, ਤੇ ਗੁਤ੍ਤ ।</p>

<p>(ਅ) ਅਨੁਨਾਸਕ ਵਰਣਾਂ ਤੋਂ ਪਹਿਲਾਂ ਭਾਵੇਂ ਟਿਪ੍ਪੀ ਤੇ ਅਧਕ ਇਕੋ ਕਂਮ ਦਿਂਦੀਆਂ ਹਨ,
ਪਰਂਤੂ ਅਧਕ ਦੀ ਵਰਤੋਂ ਉਥੇ ਉਚਿਤ ਨਹੀਂ ਜਿਵੇਂ: ਨਿਮ, ਜਮ੍ਮ, ਮਨ੍ਨ, ਕਮ੍ਮ, ਕਨ੍ਨ
ਨੂਂ `ਨਿਂਮ', `ਜਿਂਮ', `ਮਂਨ,' `ਕਂਮ' ਤੇ `ਕਂਨ' ਲਿਖਣਾ ਹੀ ਠੀਕ ਹੈ ।</p>

<p>(ਝ) ਭਾਵੇਂ ਮੂਲ ਕਿਰਿਆ ਵਾਲੇ ਸ਼ਬਦਾਂ ਵਿਚ ਅਧਕ ਦੀ ਵਰਤੋਂ ਹੋਈ ਵੀ ਹੋਏ ਪਰ ਜਦ ਉਸ
ਕਿਰਿਆ ਦਾ ਪਰੇਰਣਾਰਥਕ ਰੂਪ ਬਣੇਗਾ ਤਾਂ ਅਧਕ ਡਿਗ੍ਗ ਜਾਂਦੀ ਹੈ ਜਿਵੇਂ `ਖਟ੍ਟਣਾ',
`ਚਟ੍ਟਣਾ' ਤੇ `ਪੁਟ੍ਟਣਾ' ਵਿਚ `ਟ' ਉਪਰ ਅਧਕ ਹੈ ਪਰਂਤੂ `ਖਟਾਉਣਾ', `ਚਟਾਉਣਾ', ਤੇ
`ਪੁਟਾਉਣਾ' ਵਿਚ ਇਹ ਅਧਕ ਡਿਗ੍ਗ ਪਈ ਹੈ ।</p>

<p>(ਸ) ਪਂਜਾਬੀ ਵਿਚ ਸਂਸਕ੍ਰਿਤ-ਮੂਲ ਸ਼ਬਦਾਂ ਦਾ ਅਂਤਲਾ ਅਧ੍ਧਾ `ਯ' ਡਿਗ੍ਗ ਜਾਂਦਾ ਹੈ ਤੇ
ਜੇ `ਯ' ਤੋਂ ਪਹਿਲਾ ਵਰਣ ਵਿਅਂਜਨ ਹੋਵੇ ਤਾਂ ਉਸ ਉਪਰ ਅਧਕ ਲਗ ਜਾਂਦੀ ਹੈ ਜਿਵੇਂ:--
ਦ੍ਰਵਯ = ਦਰ&lt;?&gt;ਵ ਗਰਯ = ਗਦ੍ਦ ਪਰਯ = ਪਦ੍ਦ</p>

<p>ਸਾਹਿਤਯ = ਸਾਹਿਤ੍ਤ
ਮਧਯ = ਮਧ੍ਧ
+&gt;
               0 
</p></body></text></cesDoc>