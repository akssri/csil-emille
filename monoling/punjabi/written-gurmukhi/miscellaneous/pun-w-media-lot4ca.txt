<cesDoc id="pun-w-media-lot4ca" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-media-lot4ca.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>DAIRY DAVDI</h.title>
<h.author>SHARMA R.D.</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - PUNJABI TRIBUNE - 27.10.84 - CHANDIGARH</publisher>
<pubDate>1984</pubDate>
</imprint>
<idno type="CIIL code">lot4ca</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 7.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-23</date></creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;516&gt;&lt;SHARMA - DAIRY DAVDI&gt;&lt;PRIYA KUMAR&gt;</p>

<p>&lt;     ਜਿਤਦਗੀ ਵਿਚ ਜਦਸ਼ਥ ਨਿਰਾਸ਼ਾ ਭਾਰੂ ਹਸ਼ ਜਾਥਦੀ ਹਲ਼ ਤਾਥ ਬਤਦਾ ਸਾਰਲ ਹਥਿਨਰ 
ਸੁਟਹਟ ਕਲ ਹਾਰ ਮਤਨ ਲਲ਼ਥਦਾ ਹਲ਼ ਾ ਩ਸ ਦਲ ਨਾਲ ਹੀ ਜਿਹਡੀ ਮਾਡੀ ਮਸ਼ਟੀ ਢਲਰੀ 
(ਨਸਾਥ ਦੀ) ਫਸ ਨਲ ਬਣਾਪ ਹੁਤਦੀ ਹਲ਼ ਫਹ ਨਪ ਹੀ ਫਸਨੂਤ ਢਾਹ ਦਿਤਦਾ ਹਲ਼ ਾ 
ਭਾਵ ਫਸ ਨੂਤ ਮਨਸ਼ਥ ਕਢਹਢ ਦਿਤਦਾ ਹਲ਼ ਾ ਕਪ ਵਾਰ ਤਾਥ ਸਮੁਚਹਚਲ ਜੀਵਨ ਪਹਰਤੀ ਹੀ 
਩ਤਜ ਹਸ਼ ਜਾਥਦਾ ਹਲ਼ ਪਰ ਜਿਨਦਾ ਕਰਕਲ ਧਸੀਥ ਖਾਸ ਨਿਸ਼ਾਨਲ ਦੀ ਪਹਰਾਪਤੀ ਵਿਚ 
ਫਲਲਹਹ ਹਸ਼ਣ ਤਲ ਜੀਵਨ ਵਿਚ ਕਪ ਵਾਰ ਢਲਰੀਨਥ ਢਾਹ ਲਲ਼ਥਦਲ ਹਾਥ ਾ ਪਹਿਲੀ ਹਾਲਤ 
ਧਤਿ ਮਤਦਭਾਗੀ ਹਲ਼ ਕਿਫਥਕਿ ਫਸ ਦਲ ਧਸਰ ਹਲਠ ਕਪ ਵਾਰ ਜੀਵਨ ਨੂਥ ਖਤਮ 
ਕਰਨ ਲਪ ਵੀ ਰੁਚੀ ਪਲ਼ਦਾ ਹਸ਼ ਜਾਥਦੀ ਹਲ਼ ਤਲ ਸਚਹਚ-ਮੁਚਹਚ ਹੀ ਖੁਦਕਸ਼ੀ ਦੀ 
ਨ਷ਬਤ ਨ ਜਾਥਦੀ ਹਲ਼ ਾ </p>

<p>     ਵਲ਼ਸਲ ਢਲਰੀ ਢਾਫਣ ਦੀ ਹਾਲਤ ਬਤਦਿਯਨਥ ਵਿਚ ਵਖਹਖ ਵਖਹਖ ਕਿਸਮ ਦੀ 
ਹੁਥਦੀ ਹਲ਼ ਾ ਫਨਹੀ ਹੀ ਬਿਪਤਾ ਵਿਚ ਕਪ ਤਾਥ ਪੂਰੀ ਤਰਹਹਾਥ ਬਦਲ ਜਾਥਦਲ ਹਨ 
ਪਰ ਕੁਝ ਹਸ਼ਰ ਩ਸ ਨੂਥ ਬਹੁਤਾ ਗ਷ਲਦਲ ਹੀ ਨਹੀਥ ਭਾਵ ਦਿਲ ਨਹੀਥ ਛਡਹਡਦਲ ਾ 
਩ਤਜ ਮਨਸ਼ਬਲ ਵਿਚ ਵਖਹਖਰਲਪਨ ਕਾਰਨ ਹੁਤਦਾ ਹਲ਼ ਾ ਕਪ ਮਨ ਦਲ ਕਰਡਲ ਹੁਤਦਲ 
ਹਨ ਤਲ ਰਾਬਰਟ ਬਰੂਸ ਵਾਥਗ, `ਹਤਭਲਾ ਮਾਰਸ਼, ਹਤਭਲਾ ਮਾਰਸ਼', ਦਾ ਸਿਲਸਿਲਾ 
ਜਾਰੀ ਰਖਦਲ ਹਨ ਾ ਫਹ ਨਪਣਲ ਕਮਜਸ਼ਰ ਦਿਲ ਸਾਥੀਨਥ ਨਾਲਸ਼ਥ ਸਫਲ ਵਧ 
ਹੁਤਦਲ ਹਨ ਤਲ ਜੀਵਨ ਦੀ ਨਾਥਹ ਨੁਕਰ ਦਲ ਬਾਵਜੂਦ ਵੀ ਩ਸ ਵਿਚਸ਼ਥ ਕੁਝ ਨਾ 
ਕੁਝ ਪਹਰਾਪਤ ਕਰ ਹੀ ਲਲ਼ਥਦਲ ਹਨ ਾ </p>

<p>     ਩ਹ ਠੀਕ ਹਲ਼ ਕਿ ਜੀਵਨ ਦਾ ਡਰਾਮਾ ਕਪ ਹਾਲਤਾਥ ਵਿਚ ਨਿਰਾ ਦੁਖਾਥਤ 
ਹੀ ਹੁਤਦਾ ਹਲ਼ ਤਲ ਮਨਸ਼ਬਲ ਹੁਤਦਿਨਥ ਹਸ਼਩ਨਥ ਵੀ ਬਤਦਲ ਨੂਥ ਹਾਰ ਮਤਨਣੀ ਪਲ਼ ਜਾਥਦੀ 
ਹਲ਼ ਾ ਪਰਤਤੂ ਧਜਿਹਲ ਕਲਸ ਹੁਤਦਲ ਟਾਵਲਥ ਟਾਵਲਥ ਹੀ ਹਨ ਤਲ ਜਿਨਦਾ ਕਰਕਲ ਜੀਵਨ 
ਸੁਖਹਖ ਦੁਖਹਖ ਦਾ ਵਧਦਾ ਘਟਦਾ ਗਰਾਫ ਹੀ ਹੁਤਦਾ ਹਲ਼ ਾ ਩ਸ ਨਮ ਕਿਸਮ ਦਲ 
ਜੀਵਨ ਡਰਾਮਲ ਨੂਥ ਵਿਚਾਰ ਕਲ ਧਸੀਥ ਕੁਝ ਸਿਟਹਟਲ ਕਢਹਢ ਸਕਦਲ ਹਾਥ ਾ </p>

<p>     ਪਹਿਲੀ ਗਲਹਲ ਤਾਥ ਩ਹ ਹਲ਼ ਕਿ ਮਾਡਲ ਮਸ਼ਟਲ ਦੁਖਹਖ ਜਾਥ ਧਸਫਲਤਾ ਨੂਥ ਦਿਲ 
ਤਲ ਨਹੀਥ ਲਾਫਣਾ ਚਾਹੀਦਾ ਾ ਰਹਿ ਗਪਨਥ ਕਮਜਸ਼ਰੀਨਥ ਨੂਤ ਦੂਰ ਕਰਕਲ ਦੁਬਾਰਾ 
ਯਤਨਸ਼ੀਲ ਹਸ਼ ਜਾਣਾ ਚਾਹੀਦਾ ਹਲ਼ ਾ ਨਮ ਕਰਕਲ ਸਫਲਤਾ ਮਿਲ ਹੀ ਜਾਥਦੀ ਹਲ਼ 
ਤਲ ਢਲਰੀ ਦਲ ਥਾਥ ਢਲਰ ਬਣ ਜਾਥਦਾ ਹਲ਼ ਾ ਵਾਹ ਲਗਦੀ ਧਸਾਥ ਦੀ ਢਲਰੀ ਢਾਫਣ 
ਦੀ ਥਾਥ ਩ਸ ਨੂਤ ਸਾਕਾਰ ਕਰਦਲ ਦਿਖਾਫਣਾ ਚਾਹੀਦਾ ਹਲ਼ ਾ ਜੀਵਨ ਦਾ ਪਤਧ 
ਟਸ਼਩ਨਥ ਤਲ ਖਡਹਡਿਨਥ ਵਾਲੀ ਸਡਕ ਤਲ ਕਰਨਾ ਪਲ਼ਥਦਾ ਹਲ਼ ਤਲ ਮਾਡਲ ਮਸ਼ਟਲ ਹਜ਷ਕਲ 
ਲਪ ਸਦਾ ਹੀ ਤਿਨਰ ਰਹਿਣਾ   ਚਾਹੀਦਾ ਹਲ਼ ਾ </p>

<p>     ਜਿਹਡਲ ਦਸ਼ਸਤ ਦਿਲ ਛਡਹਡਣ ਦੀ ਰੂਚੀ ਦਲ ਧਸਰ ਹਲਠ ਹਨ ਜਾਥ ਫਨਹਹਾਥ ਦਾ 
ਦਿਲ ਨਪਲ ਹੀ ਡਿਗ ਪਲ਼ਥਦਾ ਹਲ਼ ਫਨਹਹਾਥ ਨੂਤ ਚਤਗਲ ਵਿਚਾਰਾਥ ਨਾਲ ਩ਸ ਨੂਤ ਘਟਾਫਣਾ 
ਚਾਹੀਦਾ ਹਲ਼ ਾ ਜਲ ਸਰੀਰਕ ਸਿਹਤ ਵਿਚ ਕਸ਼ਪ ਘਾਟ ਹਸ਼ਵਲ ਤਾਥ ਩ਸ ਨੂਥ ਵੀ ਠੀਕ 
ਕਰਨਾ ਚਾਹੀਦਾ ਹਲ਼ ਕਿਫਥਕਿ ਸਿਹਤਮਤਦ ਸਰੀਰ ਵਿਚ ਹੀ ਸਿਹਤਮਤਦ ਮਨ ਨਿਵਾਸ 
ਕਰ ਸਕਦਾ ਹਲ਼ ਾ ਜਿਨਹਹਾਥ ਦਲ ਦਿਲ ਕਰਡਲ ਹੁਤਦਲ ਹਨ ਫਨਹਹਾਥ ਨੂਥ ਩ਸ ਗੁਣ ਨੂਤ 
ਸਤਭਾਲ ਕਲ ਰਖਣਾ ਚਾਹੀਦਾ ਹਲ਼ ਾ </p>

<p>     ਫਥਜ ਵੀ ਜਲ ਧਸੀਥ ਖਾਸ ਹਾਲਤ ਕਾਰਨ ਢਲਰੀ ਢਾਫਣ ਤਲ ਮਜਬੂਰ ਹਸ਼ 
ਜਾਪਯ ਤਾਥ ਸਾਡਲ ਲਪ ਹਮਦਰਦੀ ਦਾ ਕਸ਼ਪ ਸਮੁਤਦਰ ਨਹੀਥ ਵਗਣਾ ਨਰਤਭ ਹਸ਼ 
ਜਾਥਦਾ ਕਿਫਥਕਿ ਧਖਾਣ ਮੁਤਾਬਿਕ ਜਲ ਧਸੀਥ ਹਸਹਸਾਥਗਲ ਤਲ ਸਾਡਲ ਨਾਲ ਹਸ਼ਰ ਵੀ ਹਸਹਸਣ 
ਨੂਤ ਤਿਨਰ ਹਸ਼ ਜਾਣਗਲ ਾ ਪਰ ਜਲ ਰਸ਼ਵਾਥਗਲ ਤਾਥ ਩ਕਲਹਲਿਨਥ ਹੀ ਹਸ਼ਣਾ ਪਲ਼ਥਦਾ ਹਲ਼ ਾ 
ਤਲ ਸਾਥ ਦਲਣ ਲਪ ਕਸ਼ਪ ਨਾਲ ਨਵਾਜ ਮਾਰਨ ਨੂਤ ਤਿਨਰ ਨਹੀਥ ਹਸ਼ਥਦਾ ਾ ਩ਸ 
ਲਪ ਜਿਥਸ਼ਥ ਤਕ ਹਸ਼ ਸਕਲ ਢਲਰੀ ਕਦਲ ਨਹੀਥ ਢਾਹਿਣੀ ਚਾਹੀਦੀ ਸਗਸ਼ਥ ਩ਸ ਨੂਤ 
ਕਾ਩ਮ ਰਖਹਖਣ ਲਪ ਹਰ ਹਨਲਰੀ ਦਾ ਡਟਹਟ ਕਲ ਮੁਕਾਬਲਾ ਕਰਨਾ ਚਾਹੀਦਾ ਹਲ਼ ਤਲ 
ਨਸਾਥ ਦੀ ਢਲਰੀ ਨੂਤ ਫਡਣ ਤਸ਼ਥ ਬਚਾਫਣਾ ਚਾਹੀਦਾ ਹਲ਼ ਩ਸਲ ਵਿਚ ਜੀਵਨ ਦੀ 
ਸਫਲਤਾ ਹਲ਼ ਾ </p>

<p>+&gt;
               0 
</p></body></text></cesDoc>