<cesDoc id="pun-w-literature-juvenile-lot7al" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-literature-juvenile-lot7al.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-23</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Aao Sikhiye Jeevan Jaach</h.title>
<h.author>Amol, S. S.</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Bhasha Vibhag - Patiala</publisher>
<pubDate>1980</pubDate>
</imprint>
<idno type="CIIL code">lot7al</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 60 - 67.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-23</date></creation>
<langUsage>Punjabi</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>&lt;2110&gt;&lt;Amol S. S. - Aao Sikhiye Jeevan Jaach&gt;&lt;Sanjeev Gupta&gt;</p>

<p>&lt;    ਖਲਤੀ ਦਾ ਕਥਮ ਹੀ ਩ਹਸ਼ ਜਿਹਾ ਹਲ਼ ਕਿ ਩ਸ ਵਿਚ ਜਿਥਨਲ ਵਧੀਕ ਨਦਮੀ ਹਸ਼ਣ, ਧਤਲ ਨਪਣਲ 
ਹਸ਼ਣ, ਫਨਾਥ ਹੀ ਵਧੀਕ ਲਾਭ ਹਲ਼ਾ ਨ਷ਕਰੀ ਵਾਲਿਨਥ ਦੀ ਕਮਾਪ ਦਿਸਦੀ ਤਾਥ ਹਲ਼, ਪਰ ਫਸ ਦਾ ਬਹੁਤ 
ਸਾਰਾ ਖਰਚ ਹੀ ਹਸ਼ ਜਾਥਦਾ ਹਲ਼ਾ ਩ਸ ਲਪ ਲਸ਼ਡ ਹਲ਼ ਧਸੀਥ ਪਡਹਹਿਨਥ ਲਿਖਿਨਥ ਨੂਥ ਖਲਤੀ ਵਲ 
ਲਾਪਯਾ ਦਾਣਿਨਥ ਦਾ ਸ਷ਦਾ ਕਰਨ ਲਗਲ ਹੀ ਫਹ ਩ਕ ਮਹੀਨਲ ਦੀ ਤਨਖਾਹ ਕਢਹਢ ਸਕਦਲ ਹਨਾ ਩ਸਲ  
ਤਰਹਹਾਥ ਹਸ਼ਰ ਵੀ ਕਪ ਕਥਮਾਥ ਵਿਚ ਫਨਹਹਾਥ ਦਲ ਪਿਥਡ ਵਿਚ ਨਾ ਹਸ਼ਣ ਕਰਕਲ ਸਾਨੂਥ ਘਾਟਾ ਸਹਿਣਾ 
ਪਲ਼ਥਦਾ ਹਲ਼ਾ</p>

<p>     ਮਸ਼ੀਨਾਥ ਦਲ ਨਫਣ ਦਾ ਩ਕ ਨੁਕਸਾਨ ਩ਹ ਹਸ਼਩ਨ ਹਲ਼ ਕਿ ਧਸੀਥ ਪਸ਼ੂ ਪਾਲਣ ਵਲਸ਼ਥ ਬਲਪਹਰਵਾਹ 
ਹਸ਼ ਗਯ ਹਾਥਾ ਸਰਕਾਰ ਤਾਥ ਩ਸ ਪਾਸਲ ਬਡਾ ਧਿਨਨ ਦਲ ਰਹੀ ਹਲ਼ਾ ਚਥਗੀਨਥ ਨਸਲਾਥ ਦਲ ਪਸ਼ੂ ਪਾਲਨਾ 
ਸਦਾ ਹੀ ਲਾਭਵਥਦ ਹਲ਼ਾ ਜਲ ਹਲ ਵਾਹੁਣ ਲਪ ਸਾਨੂਥ ਪਸ਼ੂਨਥ ਦੀ ਲਸ਼ਡ ਨਹੀਥ ਤਾਥ ਦੁਧ ਲਪ, ਫਨਹਨ 
ਲਪ ਧਤਲ ਹਸ਼ਰ ਧਨਲਕਾਥ ਕਥਮਾਥ ਲਪ ਧਜਲ ਧਸੀਥ ਪਸ਼ੂਨਥ ਦਲ ਲਸ਼ਡਵਥਦ ਹਾਥਾ ਹਸ਼ਰ ਨਹੀਥ ਤਾਥ 
ਗਡਹਡਲ ਵਾਹੁਣ ਲਪ ਤਾਥ ਲਸ਼ਡ ਹਲ਼ ਹੀਾ</p>

<p>     ਚਥਗਲ ਪਸ਼ੂ ਹੀ ਲਾਹਲਵਥਦਲ ਰਹਿਥਦਲ ਹਨਾ ਚਥਗੀ ਨਸਲ ਦੀਨਥ ਗਬਨਥ, ਚਥਗੀ ਨਸਲ ਦੀਨਥ 
ਬਕਰੀਨਥ, ਭਲਡਾਥ, ਬਲਦ ਨਦਿ ਲਾਹਲਵਥਦ ਹਨਾ ਩ਨਹਹਾਥ ਦੀ ਨਸਲ ਵਧਾਫਣ ਲਪ ਯਤਨ ਜਾਰੀ ਰਖਹਖਣਾ 
ਚਾਹੀਦਾ ਹਲ਼ਾ ਪਸ਼ੂ ਬੀਮਾਰ ਹਸ਼ਣ ਤਾਥ ਪਸ਼ੂਨਥ ਦਲ ਹਸਪਤਾਲ ਤਸ਼ਥ ਸਹਾ਩ਤਾ ਲਪ ਜਾ ਸਕਦੀ ਹਲ਼ ਤਲ 
ਚਥਗੀ ਨਸਲ ਲਪ ਵੀ ਫਹ ਸਲਾਹ ਦਲ ਸਕਦਲ ਹਨਾ    </p>

<p>     ਸਾਡਲ ਪਿਥਡਾਥ ਵਿਚ ਩ਹਸ਼ ਜਲਹਲ ਲਸ਼ਕ ਵੀ ਵਸਦਲ ਹਨ ਜਿਨਹਹਾਥ ਕਸ਼ਲ ਜਮੀਨ ਨਹੀਥਾ ਫਹ 
ਜਿਮੀਥਦਾਰਾਥ ਦਲ ਕਪ ਹਸ਼ਰ ਕਥਮ ਕਰਕਲ ਰਸ਼ਟੀ ਕਨਾਫਥਦਲ ਹਨਾ ਜਿਵਲਥ ਪਿਥਡ ਦਾ ਤਰਖਾਣ, 
ਲੁਹਾਰ, ਮਸ਼ਚੀ, ਨਾਪ, ਜੁਲਾਹਲ ਨਦਿਾ</p>

<p>     ਜਿਫਥ ਜਿਫਥ ਜਿਮੀਥਦਾਰ ਸ਷ਖਾ ਹਸ਼ਵਲਗਾ, ਩ਹ ਵੀ ਵਧਲਰਲ ਸੁਖੀ ਹਸ਼ਣਗਲ ਕਿਫਥਕਿ ਫਨਹਹਾਥ 
ਨਲ ਜਿਮੀਥਦਾਰ ਕਸ਼ਲਸ਼ਥ ਹੀ ਨਪਣਾ ਹਿਸਹਸਾ ਲਲ਼ਣਾ ਹਲ਼ਾ ਫਸ ਕਸ਼ਲ ਬਹੁਤਲ ਦਾਣਲ ਹਸ਼ਣਗਲ ਤਾਥ ਫਹ 
ਖੁਸ਼ੀ ਖੁਸ਼ੀ ਬਹੁਤਲ ਦਲਵਲਗਾ, ਬਹੁਤਾ ਕਥਮ ਕਰਾਯਗਾਾ ਪਰ ਜਲ ਫਹ ਨਪ ਭੁਖਾ ਹਲ਼ ਤਾਥ ਫਸ ਦੀ 
ਸਲਵਾ ਕਰਨ ਵਾਲਾ ਕਿਵਲਥ ਰਜਹਜ ਸਕਦਾ ਹਲ਼ਾ</p>

<p>     ਜਿਮੀਥਦਾਰ ਦਾ ਵੀ ਩ਨਹਹਾਥ ਬਿਨਾਥ ਗੁਜਾਰਾ ਨਹੀਥਾ ਗਲਹਲ ਸਚਹਚੀ ਩ਹਸ਼ ਹਲ਼ ਕਿ `ਸਜਹਜਾ 
ਧਸ਼ਵਲ ਖਬਹਬਲ ਨੂਥ ਖਬਹਬਾ ਧਸ਼ਵਲ ਸਜਹਜਲ ਨੂਥ' ਸਾਰਾ ਪਿਥਡ ਩ਕ ਵਡਹਡਾ ਘਰ ਹਲ਼ ਧਤਲ ਪਿਥਡ ਦਲ 
ਵਸਨੀਕ ਫਸ ਘਰ ਦਲ ਜੀਧਾ ਸਭ ਨਲ ਰਲ ਮਿਲ ਕਲ ਗੁਜਾਰਾ ਕਰਨਾ ਹਲ਼ਾ ਩ਕ ਦੀ ਲਸ਼ਡ ਦੂਜਲ ਨਲ 
ਪੂਰੀ ਕਰਨੀ ਹਲ਼ ਤਲ ਦੂਜਲ ਦੀ ਲਸ਼ਡ ਪਹਿਲਲ ਨਲਾ</p>

<p>     ਥਸ਼ਡੀ ਭਸ਼ਥ ਵਾਲਲ ਜਾਥ ਜਿਨਹਹਾਥ ਕਸ਼ਲ ਭਸ਼ਥ ਨਹੀਥ ਰਹੀ ਫਹ ਵੀ ਪਿਥਡ ਵਿਚ ਹਸ਼ਰ ਕਪ ਕਥਮ 
ਕਰ ਸਕਦਲ ਹਨ ਧਤਲ ਩ਹ ਗਲ਼ਰ-ਜਿਮੀਥਦਾਰ ਲਸ਼ਕ ਨਪਣੀ ਨਮਦਨ ਵਧਾਫਣ ਲਪ ਕਪ ਨਵਲਥ ਸਾਧਨ       
ਵਰਤ ਸਕਦਲ ਹਨਾ ਜਿਮੀਥਦਾਰ ਵੀ ਯਹਸ਼ ਜਲਹਲ ਸਾਧਨ ਵਰਤ ਕਲ ਨਪਣੀ ਨਮਦਨ ਹਸ਼ਰ ਵਧਾ ਸਕਦਲ ਹਨਾ   </p>

<p>     ਧਜਹਜ ਕਲਹਹ ਮੁਰਗੀਨਥ ਪਾਲਣ ਧਤਲ ਧਥਡਲ ਵਲਚਣ ਦਾ ਕਥਮ ਬਹੁਤ ਪਹਰਚਲਹਲਤ ਹਸ਼ ਰਿਹਾ ਹਲ਼ਾ 
ਜਲ ਵਲਲਲ ਸਿਰ ਚੂਚਲ ਲਲ਼ ਕਲ ਨਪ ਪਾਲ ਲਯ ਜਾਣ ਤਾਥ ਕਥਮ ਸ਼ੁਰੂ ਕਰਨ ਲਪ ਖਰਚ ਘਟਹਟ ਹੁਥਦਾ ਹਲ਼ਾ</p>

<p>     ਩ਸਲ ਤਰਹਹਾਥ ਸ਼ਹਿਦ ਦੀਨਥ ਮਖਹਖੀਨਥ ਪਾਲਣ, ਸੂਰ ਪਾਲਣ, ਰਲਸ਼ਮ ਦਲ ਕੀਡਲ ਧਤਲ ਮਛਹਛੀਨਥ 
ਪਾਲਣ ਦਲ ਕਿਤਹਤਲ ਪਹਰਚਲਹਲਤ ਹਸ਼ ਰਹਲ ਹਨਾ</p>

<p>     ਵਪਾਰੀ ਰੁਚੀ ਧਤਲ ਢਥਗ ਵਧਲਰਲ ਨਾ ਵੀ ਹਸ਼ਣ ਤਾਥ ਵੀ ਫਪਰ ਦਸਹਸਲ ਕਥਮ ਸ਷ਖਲ ਹੀ ਚਲ 
ਸਕਦਲ ਹਨਾ ਩ਨਹਹਾਥ ਕਥਮਾਥ ਲਪ ਵੀ ਸਰਕਾਰ ਵਲਸ਼ਥ ਕਰਜਲ ਤਲ ਸਹਾ਩ਤਾ ਮਿਲਦੀ ਹਲ਼ਾ ਸਲਾਹ ਧਤਲ 
ਧਗਵਾਪ ਲਪ ਵੀ ਬਹੁਤ ਸਾਰਲ ਪਹਰਬਥਧ ਹਨਾ ਤੁਹਾਡਲ ਪਿਥਡ ਦਲ ਬੀ. ਬੀ. ਓ. ਜਾਥ ਕਪ ਹਸ਼ਰ 
ਧਧਿਕਾਰੀ ਩ਸ ਸਬਥਧੀ ਤੁਹਾਨੂਥ ਜਾਣਕਾਰੀ ਦਲ ਸਕਦਲ ਹਨਾ ਩ਨਹਹਾਥ ਸਭਨਾਥ ਕਥਮਾਥ ਦਲ ਵਖਹਖਰਲ 
ਵਖਹਖਰਲ ਵਿਭਾਗ ਹਨਾ ਚਿਠਹਠੀ ਲਿਖ ਕਲ ਫਨਹਹਾਥ ਕਸ਼ਲਸ਼ਥ ਜਾਣਕਾਰੀ ਪਰਾਪਤ ਕੀਤੀ ਜਾ ਸਕਦੀ ਹਲ਼ਾ 
ਤੁਸੀਥ ਤਾਥ ਹੁਣ ਪਡਹਹ ਸਕਦਲ ਹਸ਼, ਤੁਹਾਨੂਥ ਛਪਲ ਹਸ਼ਯ ਨਿਕਹਕਲ ਨਿਕਹਕਲ ਟਹਰਲ਼ਕਟ 
(ਕਿਤਾਬਡੀਨਥ) ਮਿਲ ਸਕਦਲ ਹਨਾ ਩ਹ ਦਫਤਰ ਵਲਸ਼ਥ ਮੁਫਤ ਭਲਜਲ ਜਾਥਦਲ ਹਨਾ </p>

<p>     ਜਿਮੀਥਦਾਰ ਭਰਾ ਬਾਗ ਲਾ ਕਲ ਜਾਥ ਸਬਜੀਨਥ ਬੀਜ ਕਲ ਜਾਥ ਜਿਥਲ ਪਾਣੀ ਘਟਹਟ ਹਲ਼ ਧਤਲ 
ਜਮੀਨ ਰਲਤਲੀ ਹਲ਼, ਮੂਥਗਫਲੀ ਬੀਜ ਕਲ ਲਖਹਖਾਥ ਰੁਪਯ ਕਮਾ ਰਹਲ ਹਨਾ ਗਥਨਲ ਦੀ ਕਾਸ਼ਤ ਲਪ ਹੁਣ 
ਖਥਡ ਦਲ ਕਾਰਖਾਨਿਨਥ ਵਾਲਲ ਨਪ ਤੁਹਾਡਲ ਘਰ ਪਹੁਥਚ ਕਲ ਬੀਜ, ਖਾਦ ਧਤਲ ਸਲਾਹਾਥ ਦਲਥਦਲ ਹਨਾ</p>

<p>     ਪਲਥਡੂ ਭਰਾਵਾਥ ਦਲ ਵਰਤਣ ਲਪ ਸਥਦ ਬਨਾਫਣ ਦਲ ਨਿਕਹਕਲ ਵਡਹਡਲ ਕਾਰਖਾਨਲ ਪਿਥਡਾਥ ਵਿਚ 
ਲਗਹਗ ਸਕਦਲ ਹਨ ਧਤਲ ਲਗਹਗ ਰਹਲ ਹਨਾ ਕਾਰੀਗਰ ਭਰਾ ਜਲ ਸਹਿਕਾਰੀ ਸਭਾਵਾਥ ਬਣਾ ਕਲ ਩ਨਹਹਾਥ 
ਕਥਮਾਥ ਨੂਥ ਤਸ਼ਰ ਲਲ਼ਣ ਤਾਥ ਦਸ਼ਹੀਥ ਪਾਸੀਥ ਸੁਖ ਹਸ਼ ਸਕਦਾ ਹਲ਼ਾ ਜਿਮੀਥਦਾਰ ਨੂਥ ਨਿਤ ਸ਼ਹਿਰ 
ਭਜਹਜਣ ਦੀ ਲਸ਼ਡ ਨਾ ਰਹਲਗੀ ਧਤਲ ਕਾਰੀਗਰਾਥ ਨੂਥ ਸ਼ਹਿਰ ਦੀ ਮੁਥਾਜੀ ਨਾ ਰਹਲਗੀ ਧਤਲ ਖਰਚ 
ਘਟਹਟ ਹਸ਼ਣ ਕਰ ਕਲ ਫਨਹਹਾਥ ਨੂਥ ਮਾਲ ਸਸਤਾ ਧਤਲ ਚਥਗਾ ਬਨਾਫਣ ਵਿਚ ਸ਷ਖ ਹਸ਼ਵਲਗਾਾ</p>

<p>     ਵਾਧੂ ਫਪਜ ਨੂਥ ਡਬਹਬਿਨਥ ਵਿਚ ਬਥਦ ਕਰਨ ਦਲ ਕਥਮ ਵੀ ਪਿਥਡਾਥ ਵਿਚ ਚਲਾਯ ਜਾ ਸਕਦਲ ਹਨਾ 
ਬਾਗਾਥ ਵਾਲਲ ਫਲਾਥ ਨੂਥ ਜਲ਼ਮ, ਨਚਾਰ, ਚਟਨੀਨਥ ਬਣਾ ਕਲ ਵਲਚ ਸਕਦਲ ਹਨਾ ਸ਼ਹਿਰਾਥ ਵਿਚ ਩ਕ ਩ਕ 
ਵਡਹਡਾ ਵਪਾਰੀ ਹੀ ਫਨਹਹਾਥ ਦਾ ਸਾਰਾ ਮਾਲ ਩ਕਲਹਲਾ ਹੀ ਖਰੀਦ ਸਕਦਾ ਹਲ਼ਾ</p>

<p>     ਸਾਡਾ ਩ਹ ਸਭ ਕੁਝ ਲਿਖਣ ਤਸ਼ਥ ਭਾਵ ਕਲਵਲ ਩ਥਨਾ ਹੀ ਹਲ਼ ਕਿ ਸਮਲਥ ਨਾਲ ਜਸ਼ ਸਹੂਲਤਾਥ, 
ਸਹਾ਩ਤਾ, ਸਾਧਨ ਮਿਲਣ ਲਗਹਗ ਪਯ ਹਨ, ਫਨਹਹਾਥ ਦਾ ਵਧਹਧ ਤਸ਼ਥ ਵਧਹਧ ਲਾਭ ਫਠਾਫਣਾ ਚਾਹੀਦਾ ਹਲ਼ਾ 
਩ਸ ਤਰਹਹਾਥ ਨਪਣੀ ਧਤਲ ਦਲਸ਼ ਦੀ ਨਰਥਕ ਹਾਲਤ ਫਚਹਚੀ ਕਰਨੀ ਚਾਹੀਦੀ ਹਲ਼ਾ ਩ਫਥ ਹੀ ਦਲਸ਼ ਧਤਲ 
ਧਸੀਥ ਖੁਸ਼ਹਾਲ ਹਸ਼ ਸਕਦਲ ਹਾਥਾ ਸਾਡਲ ਬਚਹਚਲ ਧਤਲ ਧਸੀਥ ਨਪ ਸ਷ਖਾ ਤਲ ਚਥਗਾ ਜੀਵਨ ਜੀਫ ਸਕਦਲ 
ਹਾਥਾ</p>

<p>     ਸਚਹਚ ਩ਹੀ ਹਲ਼ ਕਿ ਪਲ਼ਸਾ ਕਮਾਫਣਾ ਯਨਾ ਔਖਾ ਨਹੀਥ ਜਿਥਨਾ ਪਲ਼ਸਾ ਸਾਥਭਣਾ ਔਖਾ ਹਲ਼ਾ 
ਜਿਮੀਥਦਾਰ ਦੀ ਕਮਾਪ ਵਿਚ ਬਡੀ ਬਰਕਤ ਹਲ਼, ਪਰ ਧਸੀਥ ਵਲਖਦਲ ਹਾਥ ਕਿ ਬਹੁਤ ਕਰਕਲ ਫਹ ਵਰਹਹਲ 
ਦਲ ਧਥਤ ਫਤਲ ਫਲਰ ਫਸਲ ਤਰਹਹਾਥ ਹੀ ਕਰਜਾਪ ਹੁਥਦਾ ਹਲ਼ਾ</p>

<p>     ਧਸੀਥ ਩ਹ ਨਹੀਥ ਕਹਿਣਾ ਚਾਹੁਥਦਲ ਕਿ ਜਿਮੀਥਦਾਰ ਕਥਜੂਸ ਮਖਹਖੀ ਚੂਸ ਬਣ ਜਾਯ, ਜਾਥ ਫਹ 
ਕਮੀਨਾ ਹਸ਼ ਜਾਯਾ ਧਸੀਥ ਕਲਵਲ ਩ਹ ਦਸਹਸਣਾ ਚਾਹੁਥਦਲ ਹਾਥ ਕਿ ਜੁਗਤ ਨਾਲ ਖਰਚ ਕੀਤਿਨਥ ਹੀ 
ਬਰਕਤ ਪਲ਼ਥਦੀ ਹਲ਼ਾ ਫਜੂਲ ਖਰਚੀ ਗਰੀਬੀ ਧਤਲ ਪਾਪ ਨੂਥ ਸਦਹਦਾ ਦਲਥਦੀ ਹਲ਼, ਪਰ ਹੁਥਦਾ ਩ਹ 
ਹਲ਼ ਕਿ ਜਦ ਚਾਰ ਮਣ ਦਾਣਲ ਘਰ ਨ ਜਾਥਦਲ ਹਨ, ਤਾਥ ਧਸੀਥ ਤਿਡ ਜਾਥਦਲ ਹਾਥਾ ਕਪ ਭਰਾ ਕਸ਼ਪ 
ਹਿਸਾਬ ਕਿਤਾਬ ਨਹੀਥ ਰਖਹਖਦਲ ਩ਸ ਲਪ ਫਹ ਠੀਕ ਧਨੁਮਾਨ ਨਹੀਥ ਲਾ ਸਕਦਲ ਕਿ ਫਨਹਹਾਥ ਦਲ ਸਿਰ 
ਫਤਲ ਕਿਥਨਾ ਖਰਚ ਹਲ਼ ਜਾਥ ਫਹ ਪਿਛਲਲ ਸਾਲ ਵਿਚ ਕਿਥਨਾ ਖਰਚ ਕਰ ਚੁਕਲ ਹਨਾ</p>

<p>     ਖੁਸ਼ਹਾਲੀ ਧਤਲ ਧਮੀਰੀ ਦਾ ਰਾਹ ਮਿਹਨਤ ਤਲ ਕਮਾਪ ਹੀ ਨਹੀਥ, ਸਥਜਮ ਨਾਲ ਖਰਚ ਭੀ ਹਲ਼ਾ</p>

<p>     ਧਸੀਥ ਩ਹ ਵੀ ਨਹੀਥ ਚਾਹੁਥਦਲ ਕਿ ਸਾਡਲ ਪਾਠਕ ਪਲ਼ਸਲ ਦਲ ਗੁਲਾਮ ਹਸ਼ ਜਾਣਾ ਪਲ਼ਸਾ ਬਡਾ 
ਭਲ਼ਡਾ ਮਾਲਕ ਹਲ਼ ਧਤਲ ਚਥਦਰਾ ਹਾਕਮਾ ਪਰ ਜਲ ਩ਸ ਨੂਥ ਨ਷ਕਰ ਬਣਾ ਲਪਯ ਤਾਥ ਩ਹ ਬਡਾ ਹੀ ਬੀਬਾ 
ਸਲਵਾਦਾਰ ਹਲ਼ਾ
                ਪਲ਼ਸਲ ਨੂਥ ਨ਷ਕਰ ਕਿਵਲਥ ਬਣਾਪਯ?                                 </p>

<p>     ਜਲ ਧਸੀਥ ਪਲ਼ਸਲ ਨੂਥ ਬਹੁਤ ਹੀ ਪਿਨਰ ਕਰਨ ਲਗਹਗ ਪਯ ਤਾਥ, ਩ਹ ਨ਷ਕਰ ਹਸ਼ਣ ਦੀ ਥਾਥ 
ਖਰਾਬੀਨਥ ਦੀ ਜਡਹਹ ਬਣ ਜਾਯਗਾਾ ਩ਸ ਲਪ ਪਲ਼ਸਲ ਨੂਥ ਰੂਡੀ ਵਾਥਗ ਹੀ ਸਮਝਣਾ ਚਾਹੀਦਾ ਹਲ਼ 
ਜਸ਼ ਖਿਲਾਰਿਨਥ ਹੀ ਲਾਭਦਾ਩ਕ ਹੁਥਦੀ ਹਲ਼ਾ ਪਪ ਰਹਲ ਤਾਥ ਬਸ਼ ਛਡਦੀ ਹਲ਼ ਧਤਲ ਨਿਕਥਮੀ ਹਸ਼ ਜਾਥਦੀ 
ਹਲ਼ਾ</p>

<p>     ਸਸ਼ ਪਲ਼ਸਲ ਦੀ ਸਥਭਾਲ ਵਿਚ ਪਹਿਲੀ ਤਲ ਵਡਹਡੀ ਗਲਹਲ ਩ਹ ਹਲ਼ ਕਿ ਩ਸ ਨੂਥ ਲਸ਼ਡ ਧਨੁਸਾਰ 
ਵਰਤਣਾ ਹਲ਼ ਧਤਲ ਹਸ਼ਰ ਪਲ਼ਸਲ ਬਨਾਫਣ ਲਪ ਵਰਤਣਾ ਜਰੂਰੀ ਹਲ਼ਾ ਪਲ਼ਸਾ ਖਰਚਦਿਨਥ ਤਿਥਨ ਗਲਹਲਾਥ 
ਚਲਤਲ ਰਖਹਖਸ਼ :-</p>

<p>     1. ਕੀ ਩ਸ ਨਾਲ ਕਸ਼ਪ ਭਲਾ ਹੁਥਦਾ ਹਲ਼? ਮਲਰਾ, ਮਲਰਲ ਨਪਣਿਨਥ ਦਾ ਜਾਥ ਲਸ਼ਕਾਥ ਦਾਾ
     2. ਕੀ ਩ਸ ਨਾਲ ਕਸ਼ਪ ਸੁਖ ਨਿਕਲਦਾ ਹਲ਼? ਘਰ ਲਪ, ਘਰ ਦਲ ਜੀਨਥ ਲਪ, ਬਹੁਤਲ ਲਸ਼ਕਾਥ   
        ਲਪਾ   
     3. ਕੀ. ਩ਸ ਨਾਲ ਩ਜਤ ਵਧਦੀ ਹਲ਼? ਮਲਰੀ, ਮਲਰਲ ਪਰਵਾਰ ਦੀ, ਮਲਰਲ ਦਲਸ਼ ਦੀਾ</p>

<p>     ਩ਫਥ ਸਸ਼ਚਿਨਥ ਪਰਤਖਹਖ ਹਸ਼ ਜਾਥਦਾ ਹਲ਼ ਕਿ ਨਪਣਾ ਕਥਮ ਵਧਾਫਣ ਲਪ ਚਥਗਲ ਬੀਜ ਖਰੀਦਣ 
ਲਪ, ਚਥਗੀ ਖਾਦ ਫਤਲ ਲਾਫਣ ਲਪ, ਜਾਥ ਹਸ਼ਰ ਭਸ਼ਥ ਖਰੀਦਣ ਲਪ ਖਰਚਿਨ ਜਾਣ ਵਾਲਾ ਪਲ਼ਸਾ ਫਜੂਲ   
ਖਰਚੀ ਨਹੀਥਾ ਸਗਸ਼ਥ ਸਾਥਭਿਨ ਗਿਨ ਕਿਫਥਕਿ ਫਸ ਨਾਲ ਧਗਲੀ ਵਾਰੀ ਸਾਨੂਥ ਬਹੁਤੀ ਨਮਦਨ ਹਸ਼ਵਲਗੀਾ 
ਵਪਾਰੀ ਲਸ਼ਕ ਲਖਾਥ ਕਰਸ਼ਡਾਥ ਰੁਪਯ ਵਪਾਰ ਵਿਚ ਲਾਫਥਦਲ ਹਨ, ਩ਹਸ਼ ਹੀ ਫਨਹਹਾਥ ਦੀ ਸਫਲਤਾ ਦਾ 
ਵਡਹਡਾ ਭਲਦ ਹਲ਼ਾ ਫਹ ਫਥਲ ਰੁਪਪਨ ਲਾਫਥਦਲ ਹਨ, ਜਿਥਸ਼ਥ ਲਗਹਗ ਰੁਪਪਨ ਸਵਾ਩ਨ ਡਿਓਡਾ ਹਸ਼ ਕਲ 
ਫਨਹਹਾਥ ਦਲ ਕਸ਼ਲ ਨਵਲਾ ਕਿਫਥਕਿ ਪਲ਼ਸਲ ਨੂਥ ਪਲ਼ਸਾ ਕਮਾਫਥਦਾ ਹਲ਼ਾ</p>

<p>     ਹੁਣ ਤੁਸੀਥ ਪੁਛਸ਼ਗਲ ਫਲਰ ਫਜੂਲ ਖਰਚੀ ਕੀ ਹਲ਼? ਩ਸ ਦਾ ਫਤਹਤਰ ਩ਹ ਹਲ਼ ਕਿ ਫਜੂਲ 
ਖਰਚੀ ਦਲ ਧਥਜਲ ਢਥਗ ਚਾਤਹਰਿਕ ਦੀ ਕਵਿਤਾ ਵਿਚ ਨ ਗਯ ਹਨਾ ਜਸ਼ ਤੁਸੀਥ ਪਿਛਲਲ ਕਾਥਡ ਵਿਚ 
ਪਡਹਹਲ ਹਨਾ ਬਹੁਤ ਸਾਰੀ ਫਜੂਲ ਖਰਚੀ ਧਸੀਥ ਨਪਣੀ ਮੂਰਖਤਾ ਕਰਕਲ ਕਰਦਲ ਹਾਥਾ</p>

<p>     ਸਾਡੀ ਸਭ ਤਸ਼ਥ ਵਡਹਡੀ ਫਜੂਲ ਖਰਚੀ ਨਸ਼ਾ ਹਲ਼ਾ ਸਾਡਲ ਕਪ ਭੁਲਡ ਭਰਾ ਨਸ਼ਿਨਥ ਫਤਲ ਰੁਪਪਨ 
ਖਰਚਦਲ ਹਨ ਧਤਲ ਹਡਹਡ ਭਥਨ ਕਲ ਕੀਤੀ ਕਮਾਪ ਬਸ਼ਤਲ ਵਿਚ ਹੀ ਰਸ਼ਡਹਹ ਦਲਥਦਲ ਹਨਾ ਩ਥਲ ਹੀ ਬਸਹਸ 
ਨਹੀਥ, ਨਸ਼ਲ ਪੀ ਕਲ ਫਹ ਨਿਕਾਰਲ ਹਸ਼ ਜਾਥਦਲ ਹਨਾ ਕਪ ਨਸ਼ਲ ਧਜਿਹਲ ਵੀ ਹਨ ਜਿਨਹਹਾਥ ਨੂਥ ਵਰਤ 
ਕਲ ਬਥਦਾ ਪਹਿਰਾਥ ਬਧਹਧੀ ਬਲ਼ਠਾ ਜਾਥ ਫਥਘਲਾਫਥਦਾ ਹੀ ਰਹਿਥਦਾ ਹਲ਼ਾ ਜਿਵਲਥ ਧਫੀਮ, ਭਥਗ, 
ਡਸ਼ਡਲ, ਤਮਾਕੂ ਨਦਿਾ</p>

<p>     ਜਿਸ ਨਦਮੀ ਨਲ ਧਠਹਠ ਘਥਟਲ ਕਥਮ ਕਰਨਾ ਸੀ, ਫਸ ਨਲ ਧਠਹਠ ਘਥਟਲ ਫਥਘਲਾਫਥਦਿਨਥ ਲਥਘਾ 
ਦਿਤਹਤਲ ਧਤਲ ਨਸ਼ਲ ਦਾ ਖਰਚ ਵਖਹਖਰਾਾ ਫਲਰ ਦਵਾਪਨਥ ਦੀ ਲਸ਼ਡ ਪਲ਼ਣੀ ਹਲ਼ ਧਤਲ ਫਚਲਚੀ ਖੁਰਾਕ ਖਾ 
ਕਲ ਮਸਾਥ ਹੀ ਸਰੀਰ ਨੂਥ ਮੁਡ ਨਸ਼ਾ ਵਰਤਣ ਜਸ਼ਗਾ ਬਣਾਫਣਾ ਹਲ਼ਾ</p>

<p>     ਩ਕ ਵਿਦਵਾਨ ਨਲ ਲਿਖਿਨ ਹਲ਼ ਕਿ ਨਸ਼ਾ ਪੀਣ ਨਾਲ ਮਲਰਲ ਤਿਥਨ ਦਿਨ ਫਜੂਲ ਜਾਥਦਲ ਹਨਾ ਩ਕ 
ਦਿਨ ਪਾਪ ਕਰਨ ਵਿਚ ਜਸ਼ ਮਲ਼ਥ ਨਸ਼ਾ ਪੀ ਕਲ ਕਰਦਾ ਹਾਥ, ਦੂਜਾ ਦਿਨ ਫਸ ਨਸ਼ਲ ਦਲ ਧਸਰ ਦਾ ਦੁਖ 
ਭਸ਼ਗਦਿਨਥ, ਤੀਜਲ ਦਿਨ ਫਸ ਦਾ ਪਛਤਾਵਾ ਕਰਦਿਨਥਾ </p>

<p>     ਕਪ ਭਲਲ ਲਸ਼ਕ ਨਖਦਲ ਹਨ, ਨਸ਼ਲ ਨਾਲ ਧਸੀਥ ਗਮ ਗਲਤ ਕਰਦਲ ਹਾਥਾ ਮਲ਼ਥ ਤਾਥ ਕਹਾਥਗਾ, ਫਹ 
ਗਮ ਗਲਤ ਨਹੀਥ ਕਰਦਲ ਸਗਸ਼ਥ ਗਮ ਸਹਲਡਦਲ ਹਨ ਧਤਲ ਪਾਗਲ ਪਨ ਦਾ ਰਸ਼ਗ ਨਪ ਸਦਹਦਦਲ ਹਨਾ  </p>

<p>     ਚਾਲੀ ਵਰਹਹਲ ਜਜਹਜ ਦੀ ਕੁਰਸੀ ਸਥਭਾਲੀ ਰਖਹਖਣ ਤਸ਼ਥ ਪਿਛਹਛਸ਼ਥ ਩ਕ ਜਜਹਜ ਨਲ ਲਿਖਿਨ ਹਲ਼ 
ਕਿ ਩ਥਨਲ ਸਾਲਾਥ ਦਲ ਮੁਕਦਹਦਮਿਨਥ ਵਿਚ ਩ਕ ਵੀ ਦਸ਼ਸ਼ ਧਜਿਹਾ ਨਹੀਥ ਨ਩ਨ ਜਿਸ ਦਲ ਪਿਛਲ ਕਿਸਲ 
ਨਾ ਕਿਸਲ ਤਰਹਹਾਥ ਸ਼ਰਾਬ ਦਾ ਹਥਹਥ ਨਹੀਥ ਸੀਾ ਲਡਾਪਨਥ ਵਿਚ ਫ਷ਜਾਥ ਨਲ ਯਨੀ ਤਬਾਹੀ ਨਹੀਥ 
ਕੀਤੀ ਹਸ਼ਣੀ ਜਿਥਨੀ ਸਾਡਲ ਦਲਸ਼ ਦੀ ਜਨਤਾ ਦੀ ਤਬਾਹੀ ਨਸ਼ਿਨਥ ਨਾਲ ਹਸ਼ ਰਹੀ ਹਲ਼ਾ</p>

<p>     ਦੂਜਾ ਩ਕ ਹਸ਼ਰ ਫਜਾਡਾ ਜੂਯ ਦਾ ਹਲ਼ਾ ਧਸੀਥ ਕਪ ਵਾਰ ਰਾਤਸ਼ ਰਾਤ ਬਾਦਸ਼ਾਹ ਬਣ ਜਾਣ ਦਾ 
ਸੁਪਨਾ ਲਲ਼ ਲਲ਼ਥਦਲ ਹਾਥ ਧਤਲ ਜੂਯ ਵਿਚ ਰੁਪਪਨ ਫਜਾਡਦਲ ਹਾਥਾ ਸਾਨੂਥ ਚਲਤਲ ਰਖਹਖਣਾ ਚਾਹੀਦਾ 
ਹਲ਼ ਕਿ ਜੂਨ ਕਿਨਲ ਨਾ ਜਿਤਿਨ, ਸਭ ਜੂਯ ਜਿਤਲਾ</p>

<p>     ਤੀਜਾ ਮਘਸ਼ਰਾ ਸਾਡਲ ਧਨ ਦਲ ਭਡਸ਼ਲਲ ਵਿਚ ਓਦਸ਼ਥ ਹੁਥਦਾ ਹਲ਼ ਜਦਸ਼ਥ ਧਸੀ ਮੁਕਦਹਦਮਿਨਥ ਵਿਚ 
ਫਸ ਜਾਥਦਲ ਹਾਥਾ ਩ਹ ਮੁਕਦਹਦਮਲ ਤਲ ਝਗਡਲ ਕਪ ਵਾਰ ਪੀਡਹਹੀਨਥ ਤਕਹਕ ਤੁਰਲ ਜਾਥਦਲ ਹਨਾ 
ਸਿਨਣਿਨਥ ਨਲ ਤਾਥ ਤਤਹਤ ਕਢਹਢਿਨ ਹਲ਼ ਕਿ ਲਥਮਲ ਮੁਕਦਹਦਮਲ ਤਲ ਲਥਮੀਨਥ ਲਡਾਪਨਥ ਵਿਚ ਦਸ਼ਵਲਥ 
ਧਿਰਾਥ ਦੀ ਹੀ ਗਲਤੀ ਹੁਥਦੀ ਹਲ਼ਾ ਕਸ਼ਪ ਧਿਰ ਵੀ ਸਚਹਚੀ ਨਹੀਥ ਹੁਥਦੀਾ</p>

<p>     ਘਰਾਥ ਦਲ ਝਗਡਲ ਨਿਬਲਡਨ ਲਪ ਕਚਹਿਰੀ ਜਾਣਾ ਩ਫਥ ਹੀ ਹਲ਼ ਜਿਵਲਥ ਧਸੀਥ ਩ਕ ਬਿਲੀ 
ਖਰੀਦਣ ਪਿਛਲ ਗਬ ਵਲਚ ਰਹਲ ਹਸ਼ਪਯਾ ਕਿਫਥਕਿ ਧਸੀਥ ਜਾਣਦਲ ਹਾਥ ਕਿ ਮੁਕਦਹਦਮਿਨਥ ਵਿਚ --</p>

<p>                          ਜਿਤਿਨ ਸਸ਼ ਹਾਰਿਨਾ
                          ਹਾਰਿਨ ਸਸ਼ ਮਾਰਿਨਾਾ</p>

<p>     ਗਲਹਲ ਩ਹ ਹਲ਼ ਕਿ ਜਿਤਦਾ ਓਹੀ ਹਲ਼ ਜਸ਼ ਧਥਨਹਹਲ ਵਾਹ ਰੁਪਪਨ ਖਰਚਲ, ਩ਸ ਲਪ ਜਿਤਣ ਵਾਲਾ 
ਪਲ਼ਸਲ ਵਲਸ਼ਥ ਹਾਰ ਜਾਥਦਾ ਹਲ਼ ਧਤਲ ਫਲਰ ਫਹ ਨਪਣਲ ਵਲ਼ਰੀ ਨੂਥ ਮ਷ਕਾ ਦਲਥਦਲ ਹਨ ਕਿ ਫਹ ਵਲ਼ਰੀ 
ਜਿਤਲਾ</p>

<p>     ਸਾਡਲ ਪਿਥਡਾਥ ਵਿਚ ਪਥਚਾ਩ਤਾਥ ਹਨਾ ਚਾਰ ਭਰਾਵਾਥ ਦੀ ਸਾਲਸੀ ਪਾਪ ਜਾ ਸਕਦੀ ਹਲ਼ਾ ਘਰ 
ਵਿਚ ਨਬਲਡਾ ਹਸ਼ ਸਕਦਾ ਹਲ਼ਾ ਧਥਤ ਧਸਾਥ ਦਸ਼ਹਾਥ ਧਿਰਾਥ ਩ਥਲ ਹੀ ਰਹਿਣਾ ਹਲ਼, ਕਿਫਥ ਨਾ ਰਲ ਕਲ 
ਰਹੀਯਾ</p>

<p>     ਕਚਹਿਰੀ ਜਾ ਕਲ ਖਰਚ ਤਸ਼ਥ ਬਿਨਾਥ ਹਸ਼ਰ ਕੁਝ ਵੀ ਪਹਰਾਪਤ ਨਹੀਥ ਹੁਥਦਾਾ ਕਚਹਿਰੀ ਜਾਣਾ 
ਤਾਥ ਦਸ਼ਹਾਥ ਧਿਰਾਥ ਲਪ ਘਰ ਫੂਕ ਤਮਾਸ਼ਾ ਦਲਖਣਾ ਹਲ਼ ਜਿਸ ਵਿਚ ਧਸੀਥ ਨਪ ਹੀ ਸਵਾਹ ਹੁਥਦਲ ਹਾਥ 
ਧਤਲ ਸਵਾਹ ਹੀ ਸਾਡਲ ਪਲਲ ਪਲ਼ਥਦੀ ਹਲ਼ਾ ਕਸ਼ਲਲ ਧਤਲ ਨਿਘ ਲਸ਼ਕਾਥ ਦਲ ਹਿਸਲ ਨਫਥਦਾ ਹਲ਼ਾ ਮਕਡੀ 
ਦਲ ਜਾਲਲ ਵਾਥਗ ਨਿਕੀਨਥ ਮਖਹਖੀਨਥ ਹੀ ਮਾਰੀਨਥ ਜਾਥਦੀਨਥ ਹਨਾ ਵਡਹਡਲ ਮਖਹਖ ਩ਸ ਜਾਲ ਵਿਚਸ਼ਥ 
ਬਚ ਜਾਥਦਲ ਹਨਾ</p>

<p>     ਜਲ ਧਸੀਥ ਸਿਨਣਲ ਹਸ਼ਪਯ ਤਾਥ ਪਹਿਲਾਥ ਹੀ ਕਾਨੂਥਨ ਦਾ ਸਤਿਕਾਰ ਕਰੀਯਾ ਕਸ਼ਪ ਗਲ ਕਾਨੂਥਨ 
ਵਿਰੁਧ ਕਰੀਯ ਹੀ ਨਾ ਤਾਥ ਸਾਨੂਥ ਮੁਕਦਹਦਮਿਨਥ ਵਿਚ ਫਸਣ ਦੀ ਲਸ਼ਡ ਹੀ ਨਹੀਥ ਰਹਲਗੀਾ 
ਮੁਕਦਹਦਮਾ ਵੀ ਤਾਥ ਩ਕ ਪਿਥਜਰਾ ਹਲ਼ਾ ਩ਸ ਵਿਚ ਫਸਣਾ ਸ਷ਖਾ ਹਲ਼ ਪਰ ਨਿਕਲਣਾ ਬਹੁਤ ਔਖਾਾ</p>

<p>     ਛਸ਼ਟੀਨਥ ਛਸ਼ਟੀਨਥ ਬਚਤਾਥ ਵਡਹਡਲ ਭਥਡਾਰ ਜਮਹਹਾਥ ਕਰ ਸਕਦੀਨਥ ਹਨਾ ਰਸਮਾਥ ਧਤਲ ਗਹਿਣਿਨਥ 
ਫਤਲ ਤਾਥ ਧਸੀਥ ਹਜਾਰਾਥ ਰੁਪਯ ਖਰਚ ਦਿਥਦਲ ਹਾਥਾ ਩ਹ ਧਜਿਹਲ ਖਰਚ ਹਨ ਜਸ਼ ਵਾਧੂ ਹੀ ਹੁਥਦਲ 
ਹਨਾ ਧਸੀਥ ਨਪਣਲ ਵਲਸ਼ਥ ਨਪਣੀ ਗਰੀਬੀ ਲੁਕਾਫਣ ਦਾ ਯਤਨ ਕਰਦਲ ਹਾਥ, ਩ਸ ਪਿਛਸ਼ ਕਪ ਵਾਰ    
ਕਰਜਲ ਵੀ ਚੁਕ ਲਲ਼ਥਦਲ ਹਾਥ ਧਤਲ ਹਸ਼ਰ ਗਰੀਬ ਹੁਥਦਲ ਹਾਥਾ ਧਿਨਨ ਨਾਲ ਵਲਖੀਯ ਤਾਥ ਪਤਾ ਲਗਲਗਾ 
ਕਿ ਗਰੀਬੀ ਯਨੀ ਦੁਖਦਾਪ ਨਹੀਥ ਸਗਸ਼ਥ ਗਰੀਬੀ ਨੂਥ ਲੁਕਾਫਣ ਦਾ ਯਤਨ ਬਹੁਤਲ ਦੁਖਾਥ ਦਾ ਕਾਰਨ 
ਹਲ਼ਾ</p>

<p>     ਗਰੀਬੀ ਲੁਕਾਫਣ ਦਾ ਯਤਨ ਨਾ ਕਰੀਯ ਪਰ ਨਪਣੀਨਥ ਲਸ਼ਡਾਥ ਪੂਰੀਨਥ ਕਰਕਲ ਕੁਝ ਕੁ ਸੁਖ ਦਾ 
ਖਿਨਲ ਕਰੀਯਾ ਲਸ਼ਡਾਥ ਧਤਲ ਸੁਖ ਫਤਲ ਖਰਚਣਾ ਪਾਪ ਨਹੀਥ ਪਰ ੟ਸ਼ ਦਿਖਾਵਲ ਤਲ ਹਥਕਾਰ ਲਪ 
ਖਰਚਣਾ ਦਸ਼ਸ਼ ਹਲ਼ਾ </p>

<p>     ਩ਫਥ ਜਲ ਧਸੀਥ ਸਸ਼ਚ ਕਲ ਖਰਚ ਕਰਨ ਲਗ ਪਪਯ ਤਾਥ ਨਿਕਲ ਨਿਕਲ ਕਥਮਾਥ ਵਿਚਸ਼ਥ ਭੀ ਥਸ਼ਡੀ 
ਥਸ਼ਡੀ ਬਚਤ ਹਸ਼ ਸਕਦੀ ਹਲ਼ਾ ਩ਫਥ ਬਚਾਯ ਰੁਪਯ ਨੂਥ ਧਸੀਥ ਚਥਗੀ ਤਰਹਹਾਥ ਸਾਥਭਣ ਦੀ ਜਾਚ ਸਿਖਹਖ 
ਲਪਯ ਤਾਥ ਩ਹਸ਼ ਰੁਪਪਨ ਹਸ਼ਰ ਵਧਦਾ ਜਾਥਦਾ ਹਲ਼ਾ ਫਹ ਪੁਰਾਣੀ ਗਲਹਲ ਹੁਣ ਛਡਹਡ ਦਲਣੀ ਚਾਹੀਦੀ ਹਲ਼ 
ਕਿ ਰੁਪਯ ਨੂਥ ਜਮੀਨ ਵਿਚ ਦਬਹਬ ਛਡਿਨ ਜਾਥ ਕਿਸਲ ਘਡਲ ਜਾਥ ਭਾਥਡਲ ਵਿਚ ਪਾ ਕਲ ਰੁਪਯ ਲੁਕਾ 
ਛਡਲਾ ਩ਹਸ਼ ਰੁਪਪਨ ਜਲ ਬਲ਼ਥਕ ਵਿਚ ਜਮਹਹਾਥ ਹਸ਼ਵਲ ਜਾਥ ਬਾਰਾਥ ਸਾਲਾ ਸਰਟੀਫੀਕਲਟ ਲਯ ਹਸ਼ਣ ਜਾਥ 
ਸਰਕਾਰ ਨੂਥ ਹੀ ਕਰਜਲ ਤਲ ਦਿਤਹਤਾ ਹਸ਼ਵਲ ਤਾਥ ਕੁਝ ਸਾਲਾਥ ਵਿਚ ਩ਹ ਰਕਮ ਡਿਓਡੀ ਦੂਣੀ ਹਸ਼ 
ਜਾਥਦੀ ਹਲ਼ਾ</p>

<p>     ਕਪ ਹਾਲਤਾਥ ਵਿਚ ਩ਹ ਗੁਥਜਾ਩ਸ਼ ਵੀ ਹਲ਼ ਕਿ ਲਸ਼ਡ ਪਲ਼ਣ ਫਤਲ ਤੁਸੀਥ ਰੁਪਪਨ ਵਾਪਸ ਲਲ਼ 
ਸਕਦਲ ਹਸ਼ ਭਾਵਲਥ ਤੁਸਾਥ ਫਹ ਰੁਪਪਨ ਕੁਝ ਸਾਲਾਥ ਲਪ ਜਮਹਹਾਥ ਕਰਾ਩ਨ ਹਸ਼ਵਲਾ ਹੁਣ ਤਾਥ ਬਲ਼ਥਕ 
ਵੀ ਧਤਲ ਡਾਕਖਾਨਲ ਵੀ ਥਾਥ ਥਾਥ ਖੁਲਹਹ ਗਯ ਹਨਾ ਫਨਹਹਾਥ ਦਾ ਲਾਭ ਚਾਹੀਦਾ ਹਲ਼ ਧਤਲ ਰੁਪਿਨ 
ਚਸ਼ਰੀ ਹਸ਼ਣ ਜਾਥ ਗਵਾਚਣ ਦਾ ਡਰ ਵੀ ਮੁਕਾ ਦਲਣਾ ਚਾਹੀਦਾ ਹਲ਼ਾ </p>

<p>     ਵਲਖਿਨ ਗਿਨ ਹਲ਼ ਕਿ ਧਸੀਥ ਕਪ ਵਾਰ ਨਿਕਥਮੀਨਥ ਤਲ ਭਲ਼ਡੀਨਥ ਚੀਜਾਥ ਫਤਹਤਲ ਵਾਧੂ ਖਰਚ 
ਕਰਦਲ ਹਾਥਾ ਩ਸ ਦਾ ਩ਕ ਦਹਰਿਸ਼ਟਾਥਤ ਩ਹ ਹਲ਼ ਕਿ ਧਸੀਥ ਦੁਧ, ਲਸਹਸੀ, ਮਖਹਖਣ ਨਦਿ ਫਤਮ ਵਸਤਾਥ 
ਛਡਹਡ ਕਲ ਚਾਹ ਤਲ ਸਸ਼ਡਲ ਦੀਨਥ ਬਸ਼ਤਲਾਥ ਓਤਹਤਲ ਵਾਧੂ ਪਲ਼ਸਲ ਖਰਚ ਦਲਥਦਲ ਹਾਥਾ ਬਜਾਰ ਜਾ ਕਲ 
ਮਠਿਨਪਨਥ ਖਾਥਦਲ ਹਾਥ ਪਰ ਫਲ ਓਤਹਤਲ ਪਲ਼ਸਾ ਨਹੀਥ ਖਰਚਦਲਾ ਩ਹਸ਼ ਗਲਹਲ ਧਸੀਥ ਕਪਡਿਨਥ ਸਬਥਧੀ 
ਕਰਦਲ ਹਾਥਾ ਕਪਡਲ ਦੀ ਕੀਮਤ ਦਸ਼ ਰੁਪਯ ਗਜ ਧਤਲ ਫਸ ਫਤਹਤਲ ਛਪੀਨਥ ਬੂਟੀਨਥ ਲਪ ਧਸੀਥ ਧਠਹਠ 
ਰੁਪਯ ਹਸ਼ਰ ਦਲ ਕਲ ਖੁਸ਼ੀ ਖੁਸ਼ੀ ਫਸ ਨੂਥ ਦਸ ਰੁਪਯ ਗਜ ਲਲ਼ ਨਫਥਦਲ ਹਾਥਾ ਭਾਵਲਥ ਕਪਡਾ ਹਸ਼ਰ 
ਵੀ ਨਿਕਥਮਾ ਹਸ਼ਵਲਾ</p>

<p>     ਦਾਨ ਸਬਥਧੀ ਵੀ ਸਾਡਲ ਵਿਚਾਰ ਬਹੁਤ ਸਾਰਲ ਸੁਧਾਰ ਦੀ ਮਥਗ ਕਰਦਲ ਹਨਾ ਸਾਥਝਲ ਕਥਮਾਥ ਲਪ 
ਤਲ ਭਲਲ ਕਥਮਾਥ ਲਪ ਦਾਨ ਦਲਣਾ ਚਥਗੀ ਗਲਹਲ ਹਲ਼ਾ
+&gt;
               0 
</p></body></text></cesDoc>