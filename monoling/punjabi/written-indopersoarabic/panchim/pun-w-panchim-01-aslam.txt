<cesDoc id="pun-w-panchim-01-aslam" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-01-aslam.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>جس وسوں وچ مولوی غلام رسول ہوراں داستان امیر حمزہ لکھی اوس وچ پاسلیاں کمیریاں دے جیون دے ہور رنگ وی آ ہے تے اوہناں رنگاں نوں ویکھن وکھاون والی شاعری وی- جیویں نظام لہار دے ڈھولے-</p>
<p>تیرھویں صدی زمانہ زور دا</p>
<p>تیرھویں صدی ہجری دے اخیرلے ادھ وچ ای انگریز راج پنجابے آیا تے پکا تھیا- تی ایسے واری مولوی غلام رسول جمے تے پو۵رے ہوئے - سو نظام لہار دے ڈھولے مولوی صاحب دے ہانی نیں-</p>
<p>بھیڑان دے ڈھولے کال دے ناں توں ٹردے نیں-</p>
<p>کال بلیندی</p>
<p>سیانے گویڑ دے نیں جو نر پردھانی توں پہلوں کوئی وارا نار پردھانی دا وی گزریا ہے- کجھنیں تھائیں اسوہدے ایرے وی لبھدے نیں- خبری ڈھولیاں وچ اوس پرانی نار پر دھان رہریت دی رہنداے کوئی- کال ' کل ' کالی - نارماسں دیوی- کیتا ساانبھنی ' ونڈ دیونی' رکھنہار' پالنہار- کالے رنگی ' آونے وہانے رنگی- دھرتی رنگی ہن دکھ وچ اے- وجوگ دے ہر کھ تے کاوڑ پاروں کال بن گئی اے- ویلے دا مارو ویہن- بھکھ تھڑ' موت' بندے کھاونی 'نوین اوتر ورتاری دا' اوتری جوڑ دا نشان- براجے راج دا نشان- پر کالی رنگت ویر تے کاوڑ وی اے-پیڑ وی اے وجوگ پیڑ'سنجوگ سیاپا وی اے- ایہہ ویہر تے کاوڑ اک کھنجے جہان دا بوہا کھڑ کیندی اے- اک ان ہوئے جہان دا- جیہڑا کال دی پیڑ دے اندرے اندر ہوون نوں تانگھدا اے- کال آپ آپنی دوج ہے - اوہ آپنی اتھاہ بھکھ راہیں ایہو دول پئی رمزدی اے- اوتر ورتاری دا نشان ای اے کال تے وارد دا اوترا جوڑ - نارو چلتر تے ول چھل دا نشان اے- جبی کنوں ترٹ کے اوہدا مالک بٹیا جنا- کال دے سر تے سوار نواں پردھان نر- نواں گھر والا- نویں رزق چودھر چڑھیا تھڑیا تھڑیا' بی وساہیا- دھگان تے ول چھل راہیں گھر دی بھکھ دا ان پورنا کھوہ پوردا-کال آپنے ایس نویں مالک نوں رزق دے آ ہر کان نت پئی چوبھڑدی اے- جیویں پئی آ ہدی ہووے- بھکھ پاون والیا ہن میٹ بھکھ- ایہدا جوڑ جوڑا بندی دی نویں وسیبی ہستی دا نشان اے - قدرت کنوں وکھ تھی کے'دو ڈک ہوئی ہستی دا' قدرت تے بندے دی جنی تے جنے دی دوئی دا- جاپن تے جاچن دی دوئی دا- ایہہ قدرت دا نواں سروپ اے- بندے دی وار دی ساجی نویں''قدرت''دا-</p>
<p>کال دی بھکھ اک رنگ دی نابگری ہے- آپنے سر سریر اتے سوار نر نارد دی سیج حاکمی ہیٹھ رہن توں نابری- اتوں اوہ نارد نوں حیلے وسیلے آپنی نت بھکھدی بھکھ دا بالن سہیڑن لئی چو بھڑدی اے- پر وچوں اوہدا ایہہ آ ہر ستے بندے دی ہستی دے پاڑ دی اسگ نوں بھکھا کے بجھاون دا آ ہر اے خاک اندر سموون دا' جتھون ایہہ اگ نکلی اے- وکھریویں نوں اوہدا اوڑک وکھا کے اکائی وچ مارن دا آ ہر-</p>
<p>ڈھولا بار واسیاں دی اپج اے- بار واس ہالیاں پالیاں کمیریاں دی- بار دا ویہار وی نر پردھان اے- پر ایہہ پرانا ٹابری ویہار اے- جیہدی ورتن وچ نار پردھانی دی نہیں تے نار آور دی چھجی بھجی یاد اجے جیوندی اے- مہکدی آ کھ لئیو- ڈھولا ایسے چھثجی یاد دی جاگوں جمدا ای- ڈھولا بھیڑ کہانی کریندا اے)انج ڈھولے ہور کہانیاں وی کریندے نیں( پر ایہدا ناں ''ڈھولا''عشق کہانی وچوں آیا اے- ڈھولا جنیاں دا جوڑیا جنیاں دا الایا اے پر ایہنوں گاوندے جنے انج نیں جیویں تر یمتیں وین پیاں کریندیاں ہوون- کرلاٹ دے رنگ وچ- آپنی مردانی آ کڑ چھڈ کے جنی دے دکھ دی واج وچ- کال واج وچ - ڈھولا تران کہانی اے پر ایہہ تران ہے دھنگان ٹھلن والا- ڈاسڈھ بھنن والا- ایہہ تران نہیں ماڑیاں ولوں تراسن دا پرتاوا ہے- مواڑے بھ تر کے نہیں اٹؒدے- دکھی تھی کے اٹؒدے نیں- اوہ مرن مارن وچ پھنڈکوے نہیں- ویر ویاہ کے وی ویریاں نوں مار کے وی روندے نیں-</p>
<p>نظام لوہار دی کہانی ماجھے دی کہانی اے- ماجھے دا پلا بار نال بدھا ہویا اے- ماجھے وچ پرانے شہر نیں باتشاہگی راج گڑھ- لہور امر تسر قصور پر اتھویں صدی دے تریجے پائ ایہنان شہراں دیاں برونہاں اتے اجے بار پسری پئی اے- سو نظام لوہار دی کہانی بار واسیاں دی کہانی اے- اوہنان دی جوڑی تے ورتائی-</p>
<p>کال بلیندی تے نارد مارے سدالاردا</p>
<p>شاہو کاراں دتیاں عرضیاں</p>
<p>ساہما کر لئو ایس لوہار دا</p>
<p>آہندا نظام اساں لٹنے کھوجے تے کھتری</p>
<p>کوئی ورلا ورلا گھر سنیار دا</p>
<p>کمی تے چمی نوں کوئی نہ چھیڑنا</p>
<p>بر نہیں کرنا بھرا جٹ  کنگال دا</p>
<p>الاردا سد ویہار دے الار ہوون دا ہو کا اے- رزق ونڈ تے چروکنی ان ساویں ٹری آوندی اے- اپجاو نہاراں کول اوتنا ای رہندا اے جتنے نال مساں جیون تے اگلی فصل اگاون جوگے رہن-وادھو مالک چا کھڑ دے نیں یاں سرکار اگراہ گھندی اے- نارد دا سد ایس قدیمی ان ساویں ویہار وچ اک نویں الار دا ہو کا اے- انگریز راج نے ہالہ اگراہی دا نواں پر بندھ ٹوریا اے- بدھے ہالے دے بھار پارون واہک ودھیری کنگال تھی گئے نیں- کنگال جٹ روک ہالہ تارن لئی شاہو کاراں دے قرضائی تھی کے یاں پیلی اوہناں اگی گہنے پاوندے نیں یاں ویچ دیندے نیں- ماجھے وچوں نویاں نہراں لنگھن پارون ریل تے سڑکاں دی نویں سہولت پاروں بوھئیں دا مل بن گیا اے- تے مالکی کلے جی دے ناویں ہوون پاروں بھوئیں دا گہنے پاون ویچن سوکھا تھی گیا اے- بھوئیں دے سرو یاج گھنن دی سوکھا جاپدا اے- بھولے جٹ شاہوکار دیاں وہیاں تے سہج سبھا انگوٹھا لا دیندے نیں تے نویان کچہریان وچوںشاہو کار ایہنان وہیاں دے سر تے سو کھے ای بھئیں آپنے ناویں لوا لیندے نیں-</p>
<p>)1870 ( دی سرکاری رپورٹ دسدی اے جو مولوی غلام رسول دے ضلعے وچ سو وچوںستر واہی وان قرضائی نیں-(</p>
<p>کئیں شاہو کار مالک زمیندار بن گئے نیں تے جٹ اوہنان دے راہک- آپنی پیلی تے ای- جٹا تر وی سووڑ دا بھار کمیاں سیپیاں اتے پیا اے- نظام تے اوہدے بیلی ایسے کنگالی دی مار پاروں آپنا کھسا رزق پرناون لئی جتھے بند تھئے نیں- اوہناں دا کھسا سرزق شاہو کاران وپاریاں کول جا جڑ یا اے- سو قانون راہیں لٹن واسلیان نوں دھڑکا ای بے قانونی لٹ دا-</p>
<p>ساہما کر لئو ایس لوہار دا</p>
<p>''ساہما کر لئیو'' وچ کیڈا یقین تے کیڈی بے یقینی اے- سرکار اتے-ہما تے چنتا- ''ایس لوہار'' وچ کیڈی کھج اے گھن تے ڈر- انگریز راج وچ نویں ابھردے زروال میل دی کرنی سوچنی ایہناں دونہہ پیراں تے ای ٹردی اے- نویاں حاکماں ولوں ایہو ادھ یقینا لاگ بے یقینا یقین آپنے وادھے لئی آپنی راکھی لئی تے خلقت ولوں ایہو کھج'گھن تے ڈر- ''ایس لوہار''وچ ''لوہار''کوئی کسب نہیں اک کتے دا ناں نہیں-اک جات دا ناں اے- اک کمین جات' جیہڑی شاہو کاراں لئی پوری بندہ جون نہیں- جاتان اصلوں کسب کتے ای آش ہن- کم ونڈ دی بنائی میل بندی سکے بندسس کرن لئی پرانیاں حاسکمان دیاں ریت وانان کسب نوں جات بنا دتا' قدرت دی دھرون دتی ہستی- بندے دیاں کرماں دا پھل- انمٹ 'ان وٹ- جات باندھ نوں پاکی پلیتی تے بھٹ ور تارے راہیں بندے دے ہڈیں رچایا گیا- بندے دی سوچنی کرنی نوں جات باندھ قدرتی جاین لگ پئی- وسیب وچ بندے کسبان سر بندیاں ادھ بندیان کبندیاں تے ان بندیاں وچ ونڈیج گئے-تے ایہہ ونڈ پیڑھیان نال بجھ کے جوثنان دی ونڈ بن گئی- جات ورتارا اصلوں حکم دین والیاں تے منن والیاں کم لین والیاں تے کرن والیاں دی ونڈ نوں پکیرا کرن واسطے ای ہا- کرنہاراں اتے کروا ونہاراں دی حاکمی نوں اٹل بناون واسطے- سو بھوئیں والان نوں تے جات ورتارا نت ای وارے وچ ہا پر وپاری میل نوں جدوں آپنے وپار دے ودھالئی  بھوئیں وال حاکماں نال کھیہناپیا تان جات ورتارے بارے اوہناں دی سوچ وچ لف وی آئی تے اوہناں وچوں اگل ہتھ ڈھانیاں نے وسیب وچ نت ابھردیان بندیائی پال لہراں دا پکھ وی پوریا- انھویں صدی دے دوجے ادھے اتھاواں وپاری میل انگریزی وپار شاہی دے حکم ہیٹھ اے- نویں موکلی جڑت بنا کے نویاں حاکماں ساہویں ہوون لئی جات تروڑ سدھار لہراں نوں وی پانی پیا دیندا اے- پر کامیاں کسیاں دی ویہر دے تروہ پارون ہڈ رچے جات ورتارے دا پلا وی ہتھوں نہیں چھڈدا-</p>
<p>ساہما کر لئو ایس لوہار دا</p>
<p>دوہائی اے نویں حاکماں اگے ایہہ کمیں ادھ بندے' ایہہ ہوڑی پشو جون سانوں مارن لگے جے- سانوں بچائو- تے آپ وی بچوایہناں کولوں-</p>
<p>ساہوکاراں دی عرضی وچ ماڑیاں ہینیاں دی جیہڑی تصویر الیکی گئی اے- نظام دا متا اوس توں وکھری الیکدا اے- متے دی سہج الائ وچ اک وکھرا سبھا پیا بولدا اے- متے وچ کیتی میلاں دی ونڈ وچ اک وکھری سرت اے- نظﷲام دے متے وچ ماڑے آپنی تصویر پئے الیکدے نیںآپنیاں نوں وکھالن کیتے- آپنی پچھان آش پ پئے کریندے نیں' آپنے جوگی-</p>
<p>نظام دے متے دی ونڈ دسدی اے جو اوہدی ڈھانی دی لٹ لوبھ لئی نہیں لوڑ لئی اے- لوبھ والیاں کولوں وادھو کھوہ کے لوڑ والیان نوں دیون دا پر الا- ایہہ تگڑیاں چاتراں دی لٹدا تروڑ اے- ایہہ ماڑیاں دی سیاست اے- ایہہ تگڑیاں دے لاگو کیتے ویہار نوں سمجھن دا سٹہ اے-</p>
<p>متے دی ونڈ جاتان نوں اوہناں دا اصل روپ پر تاوندی اے- ایتھے جاتاں جوناں نہیں- رزق ویہار دی کیتی کماں دی ونڈ اے- جاتاں کتیاں دے ناں نیں- کتیاں والے رزق دی اپج وچ آپنی سیر پطاروں وکھو وکھری میلاں وچ جڑ ویندے نیں- نظام داس متا ایس میل بندی نوں نتاردا اے- کرنہاراں تے سانبھنہاراں نوں وکھو وکھرا کریندا اے- ایس نتارے نوں آپنی سیاست دا مڈھ مول بنیندا اے-</p>
<p>متے دی بریک ونڈ ویکھوÎ</p>
<p>ورلا ورلا گھر سنیار دا</p>
<p>سنیارے ہن تاں کسبی گھاڑو پر ایہناں دے گاہک پیسے وال ای نیں- چوکھی گاہکی والے سنیارے سونے چاندی دا وپار وی کریندے نیں- ''ورلا ورلا''والی ونڈ ماڑیان گھاسڑو سنیاریاں تے سکھالیان وپاری سنیاریاں دی ونڈ جاپدی اے- اجیہی ونڈ جٹاں وچ وی کیتی گئی اے-</p>
<p>برا نہیں کرنا بھرا جٹ کنگال دا </p>
<p>ایتھے جٹ جات ناں نہیں- واہی کرن والیاں دا ناں اے- واہی واناں وچ مالک چودھری وی نہیں واہک وی تے راہک وی- کنگال دا لفظ ایہو فرق کریندا اے-</p>
<p>نظام دے متے دی بریک ونڈ بیلاں ہینیاں نوں سانگن دا سیاسی آ ہر ای- متے دے الائ وچ کوئی بڑھک یان پھلائ نہیں اک ہر کھ بھریا دھیان اے- ایہہ دسیندا اے جو لٹ دی سیاست اک مجبوری اے جیہڑی ایہناں ماڑیاں نوں پالنی پئی اے آپنے کسب چھڈ کے - ایہہ مجبوری</p>


</body></text></cesDoc>