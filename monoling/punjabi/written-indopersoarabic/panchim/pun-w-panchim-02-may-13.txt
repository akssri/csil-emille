<cesDoc id="pun-w-panchim-02-may-13" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-may-13.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>اُلتھا، سودھ: مقصود ثاقب</p>

<p>ٹیلیویژن بالاں دے دماغ بجی کردا اے</p>

<p>جرنل آف کمپیریٹو نیورالوجی )Journal of Comparative Neurology( نے شکاگو یونیورسٹی دی اِک سٹڈی رپورٹ چھاپی اے، جیہڑی آکھدی اے پئی بال دی زِندگی دے پہلے تِن سال دماغ دی نشو و نُما لئی بڑے ضروری ہوندے نیں- ایہناں سالاں وِچ دماغ دے عصبی خلیے جُڑ کے کم کرنا شُروع کردے نیں-</p>
<p>اصل وِچ جمن توں لے پنجاں سالاں تیکر دی عُمر وِچ جے دماغ دے ایہناں خلیاں دا آپو وِچ عصبی رِشتے دا ایہہ ویلا گواچ جاوے تاں ایہہ باقی دی ساری عُمر کِسے طرحاں وی پُورا نہیں ہو سکدا- دماغ دی نشوو نُما دے جِنے وی کم نیں، ایہناں عُمر دے کُجھ خاص حِصیاں وِچ ای سِرے چڑھنا ہوندا اے- ایس کر کے جیہڑا بال ایس سارے ویلے وِچ دماغ دِیاں ایہناں خلیاں نوں لوڑیندی آنگس نہیں دے سکدا ، اوہ اگوں لئی تُھڑیا رہ جاندا اے-</p>
<p>سائنسدان دسدے نیں پئی جدوں دماغ دے خلیے اِک دُوجے نال تاراں جوڑدے پئے ہوندے نیں اودوں بال آپنے وساہ نال جیہڑیاں وکھ وکھ جاچاں سِکھدے نیں اوہ ساری عُمر اوہناں دے لیکھے لگدیاں نیں، اوہناں دے کم آوندیاں نیں-</p>
<p>مثال دے طور تے اِک چھوٹا جیہا بچہ اوس سنگیت نوں سنجاندا اے، جیہڑا اوہنے ماں دے ڈِھڈ وِچ  سُنیا ہوندا اے- سج جمے بال اجے بول وی نہیں سکدے ہوندے تاں وی اوہ نِکا نِکا سِدھا حساب کرن جوگے ہوندے نیں- بچے ویڑھے وِچ  کھیڈن دے یاں کچی پکی پڑھن دے زمانے بولی چھیتیتے سوکھیاں سِکھ جاندے نیں- وڈے سکولاں وِچ جان دی عُمرے ایہہ کم سوکھا نہیں ہوندا-</p>
<p>دماغ دِیاں تاراں )اعصابی سِسٹم(</p>
<p>جمن مگروں دماغی خُلیاں دا آپو وِچ جیہڑا اعصابی رِشتہ بندا اے اوہنوں دماغ دی وائرنگ دا ناں دیندے نیں- ایہہ اعصابی تاراں سگویاں ٹیلیفون دِیاں تاراں واکن کم کردِیاں نیں تے اِک دماغی خُلیے دا سُنیہا دُوجے نوں اپڑاندیاں نیں- ایہہ زِندگی دے پہلے دو سالاں وِچ کروڑاں دی گِنتی وِچ بندیاں نیں- ایہناں دا اینا وادُھو بنن ایس لئی ہوندا اے پئی ویکھن بولن سوچن تے محسوسن پِچھے دماغ وِچ جیہڑا تانا پیٹا کم کردا اے اوہ تھوڑ پاروں کِدھرے وی کھلووے نہ-</p>
<p>ویکھن دی سکت دین والے دماغی خلیے پہلے سال وِچ ای بڑی چھیتی نال آپنی وائرنگ دا کم پورا کر لیندے نیں- ایہناں نوںکم کرن جوگ ہوون لئی بندیاں، شیواں تے حرکتاں نال چوکھے واہ دی لوڑ نہیں ہوندی-</p>
<p>واجاں دی سُنجان تے نکھیڑ کرن والے دماغی خلیے بال دے جمدیاں سار نویں رِشتیاں پاروں کمے لگ جاندے نیں، اوہناںدے مکمل ہوون کم 12 سال دی عُمر تائیں ہوندا رہندا اے- سیانیاں دے آکھن موجب اوہدا ایہہ سارا ویلا ای سنگیت تے بولی سِکھن دا ودھیا ویلا ہوندا اے- </p>
<p>سوچن تے سوچ نال حرکت وِچ آون والا دماغی انگ )Prefrontal Cortex( آپنے ودھن پُھلن دا عمل شُروع تاں بال دے جم پین مگروں ای شُروع کر دیندا اے پر ایہہ جا کے اوس عُمرے ای مکمل ہوندا اے، جدوں بچہ بچی بالغ ہونا شُروع کردے نیں-</p>
<p>دماغی خُلیاں دا اِک دُوجے نال اعصابی رِشتہ ای دماغ نوں ایس قابل کردا اے کہ اوہ واہ پیندیاں گلاں نال آپنے آپ کم کرن جوگا ہو سکدا اے-</p>
<p>جیہڑے سائینیپس )دماغی خُلیاں دے سِگنل اپڑاون والے جوڑ( واجاں، چھوہ )لمس(، جھاکی )Sight(، مُشک یاں سواد نال نہیں جاگدے )نہیں متحرک ہوندے( اوہ مک جاندے نیں- گھٹ ودھ ایہناں جوڑاں دی ادھی گنتی کم نہ کر سکن پاروں مُک جاندی اے-</p>
<p> دماغ دے وڈا ہوون لئی کھیڈ</p>
<p>شُروع ویلے وِچ بھرویں ذہنی جاگ )Mental Stimulation( دماغ نوں وڈا کرن وِچ بھرپور حِصہ پاوندی اے- سانوں ایہہ سمجھن دی گھنی لوڑ اے پئی بال وکھ وکھ عُمراں وِچ کیہ کُجھ سِکھن دے قابل ہوندا اے- ایہناں عُمراں وِچ اوہنوں ڈھیراں معلومات نال بھر دینا ٹھیک نہیں ہوندا، کیونجو اوہ اجے ایس معلومات نوں سانبھن جوگا نہیں ہوندا- خاص کر کے اوہنوں ٹیلیویژن دے حوالے تاں اُکا نہیں کرنا چاہیدا-</p>
<p>ذہنی جاگ دے ودھیا ڈھنگ تاں ایہہ نیں پئی جدوں بال اصلوں چھوٹا ہوندا اے تاں اوہدے نال گلاں کیتیاں جاون تے جدوں تھوڑا وڈا ہو جاندا اے تاں اوہنوں پڑھ کے سُنایا جاوے- دماغ کھیڈن نال وی ودھدا اے---- ٹی وی ایہہ چیزاں بالاں نوں مِلن نہیں دیندا- </p>
<p>ٹی وی بالاں دے آپ مُہارے خیالی کھیڈاں کھیڈن دے عمل نوں مار دیندا اے- آپ مُہاریاں خیالی کھیڈاں نے ای دماغ نوں بال عُمرے 'پاتھ ویز'  بنا کے دینے ہوندے نیں، جیہڑے دماغ دے ودھارے لئی پُج کے ضروری ہوندے نیں-</p>
<p>ایہو وجہ اے کہ محدود کھیڈن نشوونُما وِچ ایہو جیہی گڑبڑ کر دیندا اے، جیہدا ساری عُمر ازالہ نہیں ہو سکدا-</p>
<p>ایس گل نوں کیلیفورنیا یونیورسٹی دے پروفیسر میرین کلیوز ڈائمنڈ نے تجربے نال اِنج ثابت کیتا اے کہ اوہناں پنجرے وِچ ڈکے چوہیاں نوں کھیڈن لئی کھڈونے دے دِتے، ایس توں مگروں جدوںایہناں چوہیاں دے دماغ دے سائز نوں کھڈونیاں توں بِنا رہن والے چوہیاں دے دماغ نال میچیا گیا تاں کھڈونیاں والے چوہیاں دے دماغ دا سائز وڈا نِکلیا- ایہناں دے دماغ دے خُلیاں نوں اِک دُوجے نال جوڑن والیاں تاراں دُوجیاں چوہیاں دے مُقابلے وِچ ودھ تگڑیاں سن- </p>
<p>ایتھوں ای پروفیسر ڈائمنڈ ہوراں تت کڈھیا پئی کھڈونیاں نال کھیڈن، پڑھن تے بولن بالاں دی بولی اُتے پکڑ نوں تگڑا کردا اے تے اوہناں دیاں تخلیقی صلاحیتاں نوں اُکساوندا اے-</p>
<p>بِنا کِسے روک ٹوک دے بال جدوں مگن ہو کے کھیڈدے نیں تاں ایہدے نال بالاںدی آپ مُہاری ودھن پُھلن دی طاقت دے ڈکے کھُلدے نیں- بالاں نوں آپنے احساس تے جذبے نوں باریک کرن دی مدد وی ایتھوں ای لبھدی اے-</p>
<p>خراب تے چِرکا بولن</p>
<p>ڈاکٹر سیلی وارڈ بالاں وِچ بولن دی صلاحیت دے وڈے سیانے نیں، اوہ آکھدے نیں پئی امریکہ وِچ جے پنج بال بولن وِچ چِر لاندے نیں تاں اوہناں وِچ گھٹو گھٹ اِک بال دے ویلے سِر نہ بول سکن دی وڈی وجہ ٹیلیویژن اے-</p>
<p>اوہناں نتارا کر کے دسیا اے پئی ٹیلیویژن دی جھاکی دے پِچھے جیہڑا رولا سُنایا جاندا اے اوہ بالاں نوں چھیتی کیتیاں بولن دی جاچ نہیں آون دیندا-</p>
<p>اٹھاں مہینیاں دے ہو کے وی نہ اوہ آپنا ناں سیاندے نیں تے نہ ای ''جُوس'' ، ''برِکس'' جیہے بُنیادی لفظ بول سکدے نیں- تِناں سالاں دے ہو کے وی اوہناں دی بولی دو سالاں دے بال جِنی ہوندی اے- ایہو جیہے بالاں دی زِندگی نوں ڈاڈھا دھکا وجدا اے-</p>
<p>اوہ پڑھن لِکھن وِچ پُورے نہیں پیندے- اوہناں نوں پُوری طرحاں تے صاف بولنا نہیں آوندا ، اِنج اوہناں دی ساری پڑھائی پچھڑی رہ جاندی اے-</p>
<p>حالاں بال ٹی وی توں نویں لفظ سُندے نیں- پر ٹی وی دے لفظ اُنج نہیں ہوندے جیویں عام بولن ہوندا اے- ایس کر کے نِرا ٹی وی ویکھن نال یاں وڈیاں ہوون نال چنگا چوکھا بول لین دا ول نہیں آ جاندا-</p>
<p>بال جے ٹی وی ویکھ رہے ہوندے نیں تاں اوہ اوس ویلے گلاں نہیں کر رہے ہوندے- بال عام طور تے کِسے اِک لفظ نال بولنا شُروع کردے نیں، پھیر اِک فِقرے تے آوندے نیں، ایس توں اگے فِقریاں دے گروپ شُروع ہوندے نیں- جیہڑا بال ٹی وی ویکھن تے ویلا لاندا اے اوہ آپنے گلاں باتاں کرن دے ویلے دا نُقصان کردا اے- اوہنوں اِک تاںروانی نال بولنا نہیں آوندا ، دُوجا اوہ پُورے پُورے فِقرے بول تے لِکھ نہیں سکدا-</p>
<p>بالاں نوں پڑھ کے سُنایاں تے گلاں کیتیاںاوہناں نوں بولن دی جاچ آوندی اے- </p>
<p>سوچن دی مناہی</p>
<p>سوچن لئی ضروری اے پئی جو کُجھ تُسیں جاندے او اوہدے توں اگے دا اندازہ کر سکدے ہووو تے آپنے عِلم نوں ہورناں ماملیاں نال جوڑ کے ورت سکدے ہووو- ایہہ گل بالاں نوں سکول توں ای آوندی اے، ٹی وی توں نہیں-</p>
<p>اِک بڑا ای آہری بال ٹی وی اگے بہندا اے تاں اوہ ویہلا ہو جاندا اے کیونجو ٹیلیویژن چاہوندا ای ایہو اے پئی اوہنوں ویکھن والا ویکھن توں بِنا کُجھ نہ کرے-  ایہدے نال دماغ تے وجود دوویں ای نِسل ہو جاندے نیں- ایہہ دسن دی لوڑ نہیں پئی ٹی وی آپنے ساہمنے بیٹھے بال نوں سوچن سمجھن دی کُھل وی نہیں دیندا-</p>
<p>جیہڑا بال سوچدا نہیں اوہ کُجھ سِکھدا وی نہیں- سو جیہڑا بال سِکھدا نہیں اوہ پڑھائی لِکھائی وِچ اگے نہیں جا سکدا-</p>
<p>Growing Up On Television'' ناں دی کتاب وِچ کیٹ موڈی سانوں دسدے نیں پئی ٹی وی آپنے ویکھن والے نوں کیویں سوچن نہیں دیندا، ''ٹی وی اُتے تصویر ہر پنج یاں چھ سیکنڈاں وِچ بدلدی اے، بھاویں کیمرے دے رُخ بدلن نال بدلیجے تے بھاویں کوئی نویں وکھالی ساہمنے لیاون پاروں- ایہدے نال اِک مِنٹ وِچ ذہن نوں کئیں وار جھٹکے وجدے نیں- ویلے دی ایس کٹ وڈھ دا ذہن عادی ہو جاندا اے تے اوہدا اِک تار سوچن مُک جاندا اے-''</p>
<p>تخلیقی صلاحیت دا گھٹن</p>
<p>ٹیلیویژن چیزاں نوں تصور کرن دی گُنجائش نہیں چھڈدا- ڈاکٹراں نے 40 سال ایس موضوع اُتے ریسرچ کیتی اے تے ایہو ثبوت مِلیا اے پئی ٹی وی بالاں وِچ تخلیقی تصور مُکاوندا اے- </p>
<p>یکسوئی دا گھاٹا</p>
<p>دماغ دے سیانے ڈاکٹراں نے سکول ٹیچراں دے حوالے نال دسیا اے پئی جیہڑے بال ٹی وی تے ہِل جاندے نیں اوہ پڑھائی لِکھائی وِچ یکسوئی نہیں کر سکدے- ایہدی بُہت وڈی وجہ اوہناں ٹی وی پروگرام یاں فِلم دی رفتار دسی اے- کوئی تصویر بال تِن سیکنڈ ویکھدا اے، کوئی پنج سیکنڈ تے کوئی ست سیکنڈ- تصویراں جھٹا پٹ بدلدیاں نیں ایس کر کے بال دا دھیان وی بدلدا ٹُریا جاندا اے تے کِسے اِک گل اُتے نہیں ٹِکدا-</p>
<p>بالاں نوں اِک خاص ویلے مگروں اشتہار ویکھن دی اجیہی عادت ہو جاندی اے پئی اوہناں نوں پہلاں ای محسوس ہو جاندا اے پئی ہُن اشتہار دا وقفہ ہونا اے- اوڈز باڈکن اِک پیشہ ور کہانی سُناون والا اے، اوہ ہر سال کوئی دس ہزار بندیاں نوں کہانی سُناوندا اے، جیہناں وِچ بُہتے بال ہوندے نیں- اوہ آکھدا اے پئی جدوں بالاں دے اندر فِکس ہوئی گھڑی اوہناں نوں اشتہار دے وقفے دی دس پاندی اے تاں اوہ کہانی سُندے سُندے کاہلے پین لگ جاندے نیں-</p>
<p>ٹیلیویژن ویکھن دا اِک ہور وڈا نُقصان اے پئی ایہدے نال نِیندر اُڈ جاندی اے- بالاں نوں نِیندر آ  وی رہی ہوندی اے پر اوہ بِسترے اُتے لمے پین نوں تیار نہیں ہوندے- </p>
<p>ٹی وی اُتے گھڑی مُڑی تصویراں بدلدیاں ویکھ ویکھ کے بچیاں دا نروس سِسٹم کمزور ہو جاندا اے- کیمرہ ٹیکنیک دے استعمال پاروں بالاں نوں ازادی نال آپنا ذہن اِستعمال کرن دی وی عادت نہیں رہندی-</p>
<p></p>











<p></p>
<p>ٹی وی پڑھن لِکھن جوگا نہیں چھڈدا</p>

<p>مشہور تعلیمی ماہر نفسیات جین ایم ہیلی آکھدے نیں کہ ٹیلیویژن دا بُہتا ویکھن بالاں دی تعلیمی کارگُزاری اُتے مندا اثر پاوندا اے خاصکر اوہناں دی پڑھن دی صلاحیت اُتے- </p>
<p>کتاب پڑھن دا عمل گہرائی نال غور تے کُجھ ویلا منگدا اے پر ٹیلیویژن دی وجہ توں بالاں دا ذہن دو مِنٹاں وِچ ٹی وی توں بُہت کجھ لے لیندا اے- ایس کر کے پڑھ کے سمجھن دے عمل توں بال سوڑے پے جاندے نیں- </p>
<p>ٹی وی ویکھنا اِک طرفہ عمل اے- بال نوں جو مِلدا اے اوہ لئی جاندا اے، اوہنوں آپنا سوچن دی اجازت نہیں ہوندی- اِنج اوہدا ذہن اِک تھاں پتھر ہو جاندا اے- دوجے پاسے بچہ جدوں آل دوالے دِیاں شیواں نوں ویکھ کے، آوازاں نوں سُن کے، محسوس کر کے، سُنگھ کے، چکھ کے جیہڑا ہُنگارا دیندا اے، اوہدے نال اوہدے دماغ وِچ سِگنل اپڑاون والیاں تاراں تگڑیاں ہوندیاں نیں تے اوہدا دماغ ودھدا اے- </p>
<p>سِگنل نویں راہ بناوندے نیں جیہڑے پڑھن، سمجھن تے تجزیاتی سوچ لئی بُہت ضروری ہوندے نیں- ایہناں نال توجہ قائم رہندی اے تے مسئلے حل کرن دا گُن پیدا ہوندا اے-</p>
<p>جدوں اِک سیانے بال نوں بنی بنائی سوچ نہ دِتی جاوے تے اوہدے ساہمنے تصویراں چلدیاں پئیاں ہوون تاں اوہدا ذہن لازمی طور اُتے آپنے تصور نال اِک تصویر بناون دا کم کرے گا- ایہدے نال اوہدے دماغ وِچ نویں رِشتے جاگن گے، اوہدا ذہن اِک تصور نوں مکمل طور اُتے سمجھن لئی تجزیہ کرے گا تے مسئلے حل کرے گا- دماغ جِنا چوکھا کم کرے گا اونا ای چوکھا قابل ہووے گا-</p>
<p>ٹی وی دی وجہ توں بچہ چوکھا تصور نہیں کر سکدا کیونجو تصور کیتیاں چیزاں تاں پہلاں ای اوہدے ساہمنے ہوندیاں نیں- سکول وِچ یاں ہور تھاواں اُتے جدوں بال نوں تخلیقی سوچ دی لوڑ پیندی اے تاں اوہدے ذہن وِچ ایس کم لئی سِگنلاں نوں مِلاون والا نظام نہیں ہوندا- بچے نوں جاچ ای نہیں ہوندی کہ اوہ آپنے ماسٹر ولوں پڑھائی جا رہی تاریخ نوں تصور کر سکے، اوہنوں اندازہ ای نہیں ہوندا کہ تاریخ دے اوس منظر وِچ لوکاں دِیاں اوازاں کیویں دِیاں ہوندِیاں نیں- بچہ بہتیرا ٹِل ماردا اے پر اوہدا دماغ اوہدا ساتھ نہیں دیندا-</p>
<p>بولی دی کمزور ورتوں</p>
<p>ٹیلیویژن ویکھن نال بولی استعمال کرن دی جاچ وی نہیں آوندی- ٹیلیویژن نے تاں نظر ای آونا ہوندا اے، ایہدے نال بچے دے ذہن دا کھبا پاسا ماریا جاندا اے- بولیاں سِکھن لئی ایہہ کھبا پاسا ای ضروری ہوندا اے-</p>
<p>بولی دی جاچ تاں پڑھن دے عمل ، دو طرفی گل بات تے دُوجیاں نال رل کے کھیڈیاں ای آوندی اے---- ٹی وی ویکھیاں نہیں-</p>
<p>بچیاں دی حساب کتاب کرن دی صلاحیت دا پھکا ماریا جاندا اے- </p>



<p></p>
<p> بالاں نوں ٹی وی توں بچاون دِیاں تدبِیراں</p>

<p>-1 جِنا ہو سکے ٹی وی نوں بند ای کیتی رکھو- سیانے آکھدے نیں باراں سال دی  عُمر تائیں بالاں نوں ٹی وی ویکھن توں بچائی رکھو- اوہناں نوں کتاباں پڑھن دی عادت پائو- جے بال فِلم ویکھنی چاہوندا اے تاں لازمی طور اُتے اوہنوں پہلوں کوئی اِک کتاب ضرور پڑھا لوو-</p>
<p>ٹی وی نوں کوشش کر کے کپڑا پا کے رکھو یاں ایہو جیہی تھاں اُتے دھرو جِتھوں ایہہ چھیتی کیتیاں نظر نہ آوے- اِنج بالاں نوں ٹی وی گھٹ وکھالی دیوے گا تے اوہناں دا ایہنوں ویکھن ول دھیان وی تھوڑا جاوے گا- ہُن سِدھی گل اے پئی بالاں نوں ٹی وی توں دُور رکھن لئی سختی تاں نہیں کیتی جا سکدی- کوشش کرو کہ جے بال نے ٹی وی ویکھنا ای اے تاں جِتھوں تیکر ہو سکے اوہدے نُقصان گھٹ کیتے جاون- اِک تاں ہے کہ بڑے دھیان نال پروگرام دا چنائو کرو تے پھیر ٹی وی ویکھدیاں بچے نوں ٹی وی نال اکلیاں نہ چھڈو- آپ کول بیٹھو تے اوہدے نال ہولی ہولی گلاں وی کردے رہوو-</p>
<p>ٹی وی لگا ہووے تاں لائٹ بلدی رہن دیو- ایہہ ٹی وی دے اثراں نوں چوکھی مار نہیں کرن دیوے گی- لائٹ بلدی ہووے تاں ٹی وی اُتے اونی نظر نہیں پوے گی، جِنی انھیرے وِچ ٹی وی لگا ہووے تاں پیندی اے-ٹی وی توں چار فُٹ </p>
<p>دے فاصلے تے بیٹھو- ٹیلیویژن ویکھ لوو تاں باہر کِسے پارک یاں سمندر کنڈھے جاون دا پروگرام بنا لوو-</p>
<p>-2 آپنے بالاں نوں چوکھیاں کتاباں پڑھ کے سُنائو، خاص کر کے اوہ کتاباں جیہناں وِچ چوکھیاں تصویراں نہ ہوون- بالاں نوں ودھ توں ودھ کہانیاں سُنائو- بال ساڈے نِکے ہوندیاں دِیاں گل بڑے راضی ہو کے سُندے نیں- کہانیاں نال بچیاں دے اندرلی کہانی گھڑن دی سکت بندی اے- </p>
<p>-3 فِطرت برداشت، دِل دماغ نوں سُکھ، ادب عِزت سِکھاوندی اے- فِطرت دی رنگا رنگی حواس نوں جگاندی اے- اج بُہت سارے بالاں نوں باہر فطرت وِچ جانا بور لگدا اے تاں ایہدی وجہ صِرف تے صِرف ٹیلیویژن اے-</p>
<p>اسیں صحیح معنیاں وِچ اودوں ای کُجھ سِکھ سکنے آں جدوں ساڈے سارے حواس ساتھ دے رہے ہوون تے سانوں جانکاری ایس ڈھنگ نال دِتی جا رہی ہووے کہ ساڈا ذہن اوہنوں جذب کر سکے- فِطرت حقیقت اے تے ٹیلیویژن کُوڑی حقیقت-</p>
<p>-4 آپنے تے آپنے بال دے حواس دی سانبھ کرو- ساڈا آل دوالا رولے والا اے، ایہدے نال ساڈے حواس تھک جاندے نیں- بچہ جو کُجھ ویکھدا، سُندا، سُنگھدا، چکھدا تے چُھوندا اے اوہدے ودھن پُھلن لئی بُہت ضروری اے- سانوں چاہیدا اے پئی اسیں سوہنیاں، سُتھریاں تے سچیاں شیواں بچے دے آل دوالے ہر ویلے رکھئیے- بال دُنیا نوں کیویں ہنڈھاندا اے، ایہہ ساری گل بچے دے چھوٹے ہوندے لائ توں سُرت سانبھن تیکر دے ویلے اُتے منحصر اے- </p>
<p>-5 بچیاں دا ہتھ پَیر تے سارے وجود نوں ہلاون جُلاون انتہائی مقصدی ہوندا اے- بچے گھروں باہر نِکل کے جیہڑا نسدے بھجدے، چھالاں ماردے، رُکھاں اُتے چڑھدے تے رسیاں ٹپدے نیں ایہدے نال اوہناں دے وجودتے دماغ نوں بڑی طاقت لبھدی اے-</p>
<p>سگواں ای اوہناں دے گھروکے آہر پاہر، نِکیاں نِکیاں شیواں نوں بناندے رہن نال وی اوہناں دے ذہن دے نویں راہ کُھلدے نیں- </p>
<p></p>
<p>courtesy: UTUSAN KONSUMER </p>
















<p>اُلتھا، سودھ: فائزہ </p>

<p>سفیدے دا رُکھ: نِری بربادی</p>

<p>سفیدا پہلی وار برصغیر وِچ انگریز آپنی جنگل پالیسی پاروں لیائے- ایس توں پہلاں سفیدے دے ایتھے ہوون دا کوئی تاریخی ثبوت نہیں لبھدا- انگریزاں آپنے راج وِچ جدوں لمیاں چوڑیاں زمیناں اباد کیتیاں، ریل دِیاں پٹڑیاں تے سڑکاں وچھائیاں اودوں ای اوہناں نوں جنگلاں دی سانبھ رکھ دا خیال آیا- انگریزاں نوں ریل دی پٹڑی دی بڑی لوڑ سی، ایہدے نال اندرون علاقیاں وِچ منڈیاں کھولیاں جا سکدیاں سن تے فوجاں نوں سُکھالا اِک تھائوں دُوجی تھاں لیایا لیجایا جا سکدا سی-</p>
<p>ریل دی پٹڑی وچھاون لئی سلیپراں دی لوڑ پئی تاں انگریز تے دیسی تاجراں وپاریاں لئی مُنافعیاں دے مُونہہ کُھل گئے- اوہناں نے سلیپراں دی سپلائی لئی اینی کُو لکڑ وڈھ چھڈی کہ جنگلاں دے جنگل رڑے ہو گئے- لکڑ دا کال پے گیا- ایہدا اندازہ ایتھوں لایا جا سکدا اے کہ 1854ئ وِچ سگر چھائونی دی اساری دا کم لکڑ نہ لبھن پاروں چھ مہینے ٹھپ ہویا رہیا- </p>
<p>لیفٹیننٹ کرمپس نے آپ منیا ، ''سگر چھائونی دوالے واڑ لاون لئی چنگی لکڑ دیاں 20 چھٹَیاں وی نہیں سن لبھدیاں-'' )Fencing the Forests: Mehesh R, 1996( ایتھوں جنگلاں دوالے واڑاں لوانا وی انگریزی پالیسی دا حِصہ بنیا- لوک جیہڑے صدیاں توں رُکھ اُگاندے سانبھدے پئے سن، اوہناں دے رُکھاں دے نیڑے جاون اُتے پابندی لگ گئی- ایہدے نال پِنڈاں دی دُنیا دی انگریزاں نال دُشمنی ہور ودھ گئی-</p>
<p>انگریزاں نے کُجھ علاقیاں وِچ رُکھ وڈھن اُتے وی پابندی لا دِتی- جنگل لئی قانون گھڑے گئے- ایہناں زمانیاں وِچ ای انگریزاں نے چھیتی توں چھیتیہو پین والے رُکھاں دی بھال شُروع کیتی- مطلب ایہہ جو لکڑ دی کِدھرے وی تروٹک نہ آوے- اودھروں کاغذ بناون دا کم وی مشینی ہو گیا- ایہدے نال چھیتی ہو پین والی لکڑ دی لوڑ ہور ودھیری ہو گئی- بڑے دیسی تے بدیسی بوٹیاں نوں ازما ویکھیو نیں- اوڑک واڑاں دے کم آون والا بدیسی بُوٹا سفیدا ای ازمائش وِچ پُورا اتریا- اوہناں ایسے دی بیجائی اُتے زور دینا شُروع کر دِتا- ایس بُوٹے نے اودوں وی ایتھوں دی بُہت ساری جنگلی حیاتی تباہ کر دِتی- انگریزاں دا وادھا سی کہ اوہناں ککھ کنڈا وی باہروں لے آندا تے اِنج اوہ ایتھوں دے لوکاں تے ماحول نوں برباد کر کے آپنے راج نوں پکیاں کرن وِچ کامیاب ہوئے- </p>
<p>سفیدے دا کمال اے جو ایہہ بڑی چھیتی ہو پیندا اے تے چنگا چوکھا قد کڈھدا اے، ایہدی لکڑ چھیتی ای کارآمد ہو جاندی اے- پر ایہدے نال سبھ توں ودھ خرابی ایہہ جُڑی ہوئی اے کہ ایہہ زمین دی ساری طاقت چِھک لیندا اے تے  زمین نوں اینا کُو ویہلا کر دیندا اے کہ اوہدے وِچ تیلا اُگاون دی وی سکت نہ رہ جاندی- پر ایس دے باوجود ایس ویلے دُنیا وِچ ہر سال سفیدا اُگاون دا رقبہ اِک لکھ پنجھتر ہزار ہیکٹر دے حساب نال پیا ودھدا اے- کاغذ بناون دی انڈسٹری دے اجارہ دار سفیدے دی بیجائی اُتے ای زور دیندے نیں- ایہدے لئی دُوجیاں حکومتاں نوں مجبور وی کیتا جاندا اے- پر جِتھوں تیکر لوکاں دی گل اے ڈھیر دُنیا سفیدا بیجن دے خلاف پئی ہوندی اے- ہندوستان دی ریاست کرناٹک وِچ تاں ایتھوں تیکر ہویا اے کہ بہوں سارے پِنڈاں دے لوکاں نے اکٹھیاں ہو کے اوہناں نرسریاں نوں پٹ کے کھولا کر چھڈیا اے جِتھوں اوہناں نوں سفیدے دا بِی تے قلماں لے کے دِتیاں سن نیں- </p>
<p>پاکستان وِچ ایہہ رُکھ حکومت نے سیم تھوہر نوں ڈکن لئی لیہ، بھکر تے مظفرگڑھ دے علاقیاں وِچ لوایا سی- ایہناں ضِلعیاں دے لوکاں نے حکومت نوں سفیدے دے نُقصان دے اُلاہمے دینے شُروع کر دِتے نیں- </p>
<p>خانسر پِنڈ، ضِلع بھکر دے ریٹائرڈ ہیڈماسٹر اعجاز خان دسدے نیں پئی اوہناں محکمہ زراعت دے اِک افسر دے آکھن اُتے آپنے رقبے وِچ سفیدا لوایا- ایہہ سفیدا اینا چھیتی ہویا کہ اوہ چونہہ ورھیاں وِچ لکڑ ویچن دے قابل ہو گئے- ایہہ 1996ئ دی گل اے- اعجاز ہوریں آکھدے نیں، ''اودوں دی میری ساری زمین بنجر ہو گئی اے- میں ایہدے وِچ بڑیاں فصلاں ازمائیاں نہیں پر حرام اے جے اِک کِھل وی ہوئی ہووے- ''</p>
<p>بُہت سارے لوک تاں سفیدے دی مُنافع بخشی نوں وی نہیں مندے- لیہ دے محمد حسین ہوریں آکھدے نیں، ''ایس رُکھ دا نہ کوئی میوہ، نہ چھاں، نہ پٹھا دتھا- ایس کڑمے رُکھ دے ہیٹھاں تاں کیہ ایہدے آسے پاسے وی کوئی شے نہیں اُگدی- نفع ایس کِتھوں دینا اے؟''</p>
<p>لوکیں تاں سفیدے نوں سیم تھوہر دا دارُو منن نوں وی نہیں تیار - اوہ سفیدے نوں ای سیم تھوہر دا سبھ توں وڈا کارن دسدے نیں- </p>
<p>ڈائریکٹر آف رینج ریسورس انسٹی ٹیوٹ ڈاکٹر غُلام اکبر ہوریں آکھدے نیں، ''بھاویں ایہہ بڑا ای قیمتی بُوٹا اے پر ایہنوں زرعی زمیناں تے جنگلاں دوالے اُکا نہیں لانا چاہیدا-''</p>
<p>زمین دے ست نوں 20 گُنا چوس جاون والا ایہہ بُوٹا پاکستان وِچ اُگاون اُتے پابندی ہونی چاہیدی اے- حکومت نوں چاہیدا اے کہ اوہ پُن چھان کر کے لوکاں نوں دس پاوے کہ ایہہ کیہڑی زمین وِچ لایا جانا چاہیدا اے تے کیہڑی زمین وِچ نہیں- </p>
<p>بلوچستان، چولستان تے تھر وِچ ایہنیں دِنیں جیہڑا کال پیا اے کُجھ لوکاں دا خیال اے کہ ایہدے وِچ وی نویں ٹیکنالوجی تے ترقی دے نتیجے وِچ ہوون والی ماحولیاتی تباہی دا ہتھ اے- حکومت نوں چاہیدا اے کہ اوہ بدیسی بُوٹے اُگاون دی حوصلہ افزائی نہ کرے تے آپنے ایتھاویں بُوٹیاں دی سانبھ راکھی ول دھیان کرے-</p>
<p>g </p>
<p>courtesy: The NEWS Lahore</p>
















</body></text></cesDoc>