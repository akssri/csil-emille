<cesDoc id="pun-w-panchim-02-mlook" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-mlook.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>


<p>ہڑی=ہتھ دا گہنا؛ہریڑ دی شکل دے پُرچے دانے اخیر وِچ پُھمن، ہتھکڑی دی اِک قِسم- ہتھوڑی=کنگن، ہتھکڑی-م ک، پیریں بیڑیاں ہتھ ہتھوڑیاںنی؛ قصہ گوپی چند از کالی داس- م ک، بھابی کریں رعائتاں جوگیاں دیاں ہتھی بُچیاں پا ہتھوڑیاں نی'' دو بند ۳۱۴، دُرگ=انھا غار، بے آباد کھوہ-آلے= اُچی آوازاں- نکھُٹا= ختم ہویا، مُکیا ہویا- عِشق کماون= عِشق دے سارے تقاضے پُورے کرن- پوشاکی= پوشاک- ایہہ تک= ہُنے ویکھو- پرے پریرے= ہور وی پرانہہ- کھلوت= کھلو جاون دا عمل، صبر، توقف، انتظار- جھورا= افسوس، فِکر، اندیشہ- منوں= من توں، دِلوں- ونجاواں= ضائع کراں، جِند نک وِچ آئی=ناک میں دم آ گیا- سرپوش= سِر دا ڈھکن- اُڑ کے= جُھک کے- طوق= گل وِچ لمکیا ہس جیہڑا غُلامی دی نشانی سی- عبیر=مُشک گلاب تے صندل نال بنیا دُھوڑا- بھورا= ذرا جِنا ٹُکڑا- شیشے= بوتلاں- مساں مساں= بصد مُشکل- اوکھت= اوکھیائی، تکلیف-</p>


</body></text></cesDoc>