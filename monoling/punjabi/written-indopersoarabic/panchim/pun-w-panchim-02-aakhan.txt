<cesDoc id="pun-w-panchim-02-aakhan" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-aakhan.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>ہن میں آکھاں تے آکھاں کیہ</p>

<p>چھَڈ  لُٹّن  کھوہن  روپیّے  دی</p>
<p>سُن  گھوکر  گھُمدے   پہیّے  دی</p>
<p>سَب     اِکّو     مِکّو    ہو   جانا</p>
<p>ایہہ  مَرضی   اَزلوں  مہیّے  دی</p>
<p>جے    آہر  کریسیں  اجھکن   دا</p>
<p>جو   لِخیا   لیکھیں  جاسی   ڈھیہہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>   ہن میں آکھاں تے  آکھاں کیہ</p>

<p>بِن  مالش   کُولے  پِنڈے  نوں</p>
<p>نہ    واجاں  مار   درِندے   نوں</p>
<p>جے  ہڈّاں  وِچ  نئیں  جان  کوئی</p>
<p>پا گھُٹ گھُٹ جَپھیاں  تھِندے نوں</p>
<p>ایہہ  مُڈھ  قدِیموں  رِیتاں نیں</p>
<p>تگڑے   دا  سَے  ای  ستّے  ویہہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>  	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>	</p>
<p>کوئی   پالے   چور   اُچکّے  توں</p>
<p>لَے   آندی  خلقت   نَکّے  توں</p>
<p>جے   کھیڈ  مخولے  پَے  جانیں</p>
<p>کِھچ   لینیں   بیگیاں    یَکّے   توں</p>
<p>جَد تک نہ وَنڈسیں توں  مُورت</p>
<p>کَم  رہسی  تیرا  پِیہہ  پِیہہ  پِیہہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>   	ہن میں آکھاں تے  آکھاں کیہ</p>

<p>کِیہ  ہاں تے  نہ  دِی  کھیڈ بنی</p>
<p>سَچ   رلیاں  کُوڑ   چھلیڈ   بنی</p>
<p>سَبھ  بھِیڑ  بھَڑَکّے  جھاکے نیں</p>
<p>رُکھ  کنکاں  چڑیاں  بھیڈ   بنی</p>
<p>میں  جِدھر جِدھر  ویہناں   واں</p>
<p>ہر   پاسے   پَیندی   اِک  شبیہہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>

<p>آ    بَیٹھاں  لام    اُڈیکاں   وِچ</p>
<p>پے   اُچّے   شور  شریکاں  وِچ</p>
<p>توں   اَوندا    اَوندا   آویں    گا</p>
<p>جِند  پھاتھی  کالیاں  لیکاں  وِچ</p>
<p>جَد  دِید نے  لاہنی بھکھ  تریہہ</p>
<p>میں  آپ رَکھیساں  روزے  تیہہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>   ہن میں آکھاں تے  آکھاں کیہ</p>

<p>کوئی   ٹَنگیا  صوم  صلاتاں  وِچ</p>
<p>کوئی  سِدھیاں پُٹھیاں باتاں وِچ</p>
<p>کوئی   آن  کھلوندا   فِر ساہنواں</p>
<p>کوئی  تکدا  جھیتاں جھاتاں  وِچ</p>
<p>کیویں  اَکھ  ٹکاواں تیرے  تے</p>
<p>جد چار چوفیرے ہیہہ  ہیہہ  ہیہہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>

<p>کوئی  رَہندا  انت  حجاباں  وِچ</p>
<p>کوئی  پھولا  پھول کتاباں  وِچ</p>
<p>کوئی   بیٹھا   فیر   اُمیداں   لا</p>
<p>کوئی   اَوسی  گِنن  حساباں  وِچ</p>
<p>ایہہ  حالت  ہوئی  شاہدؔ  دی</p>
<p>خود   آپ  گواچا   لَبھدا   لِیہہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>	ہن میں آکھاں تے  آکھاں کیہ</p>
<p>.۔۔۔.</p>

</body></text></cesDoc>