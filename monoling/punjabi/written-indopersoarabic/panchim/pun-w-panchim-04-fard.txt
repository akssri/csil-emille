<cesDoc id="pun-w-panchim-04-fard" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-04-fard.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>





<p>بِسمِ اللہ ِ الرحم'نِ الرحیم</p>

<p>سہ شنبہ دہم ماہ ذی  قعدہ ۶۱۷ ھ</p>

<p>حضرت سُلطان جیؒ نے اپنی مُبارک مجلس میں فرمایا، ''ایک دِن شیخ الاسلام فریدالدیں قدس سرہ' کا ارشاد ہوا: ایک شخص مجھ سے وابستہ ہوا، پھر وہ یہاں سے چلا گیا- وابستگی کچھ عرصے تک برقرار رہی، پھر وہ بات نہ رہی وابستگان میں ایک اور آدمی بھی تھا- وہ بھی مجھ سے دُور چلا گیا- کافی دِن دُور رہا- کچھ عرصے تک اس کی وابستگی رہی پھر وہ بھی مجھ سے بے تعلق ہو گیا-''</p>
<p>اس ارشاد کے بعد شیخؒ اس دُعاگو کی طرف ریخ کیا اور ارشاد کرتے ہوئے فرمایا، ''ایک یہ شخص ہے جب سے مجھ سے وابستہ ہوا  ہے اسی مزاج پر قائم ہے-''</p>
<p>حضرت سُلطان جیؒ جب ان الفاظ پر پہنچے تو آپ پر گریہ غالب ہوا- اسی عالم میں آپ نے فرمایا، ''آج تک ان کی محبت نہ صِرف برقرار ہے بلکہ روز بروز بڑھتی جاتی ہے-''</p>
<p>اسی محبت، شیفتگی اور خلوص کی روداد ''فریدؒ و فرد فریدؒ'' کا موضوع ہے-</p>
<p>یہ روداد بارگاہِ محبوبی میں انتہائی ادب، احترام اور اس گزارش کے ساتھ پیش ہے؎</p>
<p>خسرو غریب است و گدا افتادہ در شہرِ شُما</p>
<p>باشد کہ از بہرِ خدا سوئے غریباں بنگری</p>






<p>نووارد بزرگ مسجد میں داخل ہوئے، کشیدہ قامت، پُررعب جسامت، تورانی ہاتھ پائوں، نورانی چہرہ، روشن پیشانی، درویشانہ وضع، سادہ انداز، یہ محسوس ہوا کہ اخلاقِ نبوی صلی اللہ علیہ وسلم کا ایک پیکر ہے جو انتہائی مسکنت لیکن بزرگانہ عظمت کے ساتھ مسجد میں داخل ہوا ہے- آنے والے بزرگ نے ایک نظر ڈالی سارا ماحول روشن ہو گیا- سامنے ایک نوجوان بیٹھ مطالعے میں غرق تھا- اٹھارہ اُنیس برس کا سن- سرشوری اور سرکشی کے دِن- مگر یہاں کچھ اور ہی کیفیت، چہرے پر مسکینی، عاجزی اور گہرے تفکر کی جھلک- حالات اور ماحول سے بے نیاز- کِسی اور ہی دُھن میں محو- بزرگ بڑھے- تحیتہ المسجد کی نیت کی- دوگانے سے فارغ ہوئے تو نوجوان کی طرف رُخ کیا- نرمی اور ملائمیت سے دریافت کیا، ''مسعود کیا پڑھ رہے ہو؟'' نوجوان نے نظریں اٹھائی، نگاہیں چار ہوئیں تو محسوس ہوا، بجلی کی ایک رو ہے کہ آنکھوں کے راستے دِل میں اُترتی چلی جا رہی ہے- سارے بدن پر کپکپی طاری ہو گئی- علم و عرفان کا سمندر ساہمنے موجزن تھا- معرفت کا ایک اُجالا تھا کہ چاروں طرف پھیل رہا تھا- ''یہ روشنی دِل و جاں میں در آئی'' بے اختیاری کے عالم میں جواب دیا- ''نافع پڑھ رہا ہوں'' نرمی سے ارشاد ہوا، ''کیا تم سمجھتے ہو کہ تمہارا نفع اس نافع کے مطالعے میں ہے؟''</p>

</body></text></cesDoc>