<cesDoc id="pun-w-panchim-04-akath" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-04-akath.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>بولے شیخ فرید پیارے اللہ لگ</p>
<p>ایہہ تن ہوسی خاک نِمانی گور گھرے- 1 </p>
<p>آج مِلاوا شیخ فرید ٹاکِم کُونجڑِیاں من مچندڑِیاں- رہائو</p>
<p>جے جانا مر جائیے گُھم نہ آئیے</p>
<p>جُھوٹھی دُنیا لگ نہ آپ ونجائیے- 2 </p>
<p>بولئیے سچ دھرم جُھوٹھ نہ بولئیے</p>
<p>جو گُر دسے واٹ مُریدا جولئیے- 3 </p>
<p>چھیل لنگھدے پار گوری من دھیریا</p>
<p>کنچن ونے پاسے دلوتِ چِیریا- 4 </p>
<p>شیخ حیاتی جگ نہ کوئی تِھر رہیا</p>
<p>جِس آسن ہم بیٹھے کیتے بہس رہیا- 5 </p>
<p>کتک کُونجاں چیت ڈئو ساوِن بِجُلیا</p>
<p>سیالے سوہندیاں پِر گل باہڑِیاں- 6 </p>
<p>چلے چلن ہار وچارا لئے منو</p>
<p>گنڈھیندیاں چھ ماہ تُڑندیاں ہِک کِھنو- 7 </p>
<p>جِمی پُچھے اسمان فریدا کھیون کِنج گئے</p>
<p>جالن گوراں نال اُلامے جِی سہے- 8 </p>



<p>1 </p>
<p>پریم اور دھیان … دو شبد جِس نے ٹھیک طرح سمجھ لئے، اُس کو دھرموں کے سارے معنے سمجھ آ گئے-</p>
<p>دو ہی راہیں ہیں- ایک راہ ہے پریم کی، ہِردے کی، ایک راہ ہے دھیان کی، بُدھی کی- دھیان کے مارگ پر بُدھی  کو نرول کرنا ہے… اتنا نرول کہ بُدھی باقی ہی نہ رہ جائے، شُونیہ )خلا( ہو جائے- پریم کے مارگ پر ہِردے کو شُدھ کرنا ہے، اِتنا شُدھ کہ ہِردے گُم ہو جائے، پریمی گُم جائے- دونوں مارگوں سے شُونیہ  کی اُپ لبھدی کرنی ہے، مِٹنا ہے- کوئی سوچ کی نفی کر کے مِٹے گا: کوئی واسنا )ہوس( کی کاٹ کر کے مِٹے گا-</p>
<p>پریم ہے واسنا سے مُکتی- دھیان ہے وِچار سے مُکتی دونوں ہی آپ کو مِٹا دیں گے اور جہاں آپ نہیں ہیں وہاں ہی پرماتما ہے-</p>
<p>دھیان نے پرماتما کے لئے اپنے شبد گھڑے ہیں: ستیہ، موکش، نِروان- پریمی نے اپنے شبد سِرجنا کیے ہیں- پرماتما پریمی کا شبد ہے- ستیہ دھیانی کا شبد ہے- پر فرق شبدوں کا ہے- اشارہ ایک ہی کی اور ہے- جب تک دو ہیں تب تک سنسار ہے، جیسے ہی ایک بچا سنسار گُم ہو گیا-</p>
<p>شیخ فرید پریم کے مُسافر ہیں، اور جیسا پریم گیت فرید نے گایا ہے، اُن جیسا کِسی نے نہیں گایا- کبیر بھی پریم کی کتھا کرتے ہیں، لیکن دھیان کی بھی بات کرتے ہیں- دادُو بھی پریم کی کتھا کرتے ہیں، پر دھیان کی بات کو بالکُل نہیں بھولتے- نانک بھی پریم کی بات کرتے ہیں، پر وہ دھیان سے گُھلی مِلی ہے- فرید نے شُدھ پریم کے گیت گائے ہیں، دھیان کو چھیڑا تک نہیں، پریم ہی کو دھیان جانا ہے- اِس لئی پریم کی اتنی خالص کہانی کہیں اور نہیں مِلے گی-پریم کو سمجھ لیا تو فرید کو سمجھ لیا- فرید کو سمجھ لیا تو پریم کو سمجھ لیا-</p>
<p>پریم کے ضِمن میں کُچھ باتیں ایسے ہی ہو جائیں، ذرا دھیان سا بن جانے دیں- پہلی بات، جِس کو آپ پریم کہتے ہیں، فرید جی اُس کو پریم نہیں کہتے- آپ  کا پریم تو پریم کا دھوکا ہے، پریم کا دِکھلاوا ہے- وہ پریم ہے ہی نہیں، صِرف پریم کی نقل ہے، جعلی سِکہ ہے اور اِس لئے تو اُس پریم سے سوائے دُکھ کے آپ نے  کُچھ اور نہیں جانا-</p>
<p>کل ہی فرانس سے آئی ایک سنیاسن مُجھے رات کو پوچھ رہی تھی کہ پریم میں بُہت دُکھ ہے، آپ کیا کہتے ہیں؟ جِس پریم کو آپ نے جانا ہے اُس میں بُہت دُکھ ہے، اِس میں کوئی شک نہیں- لیکن وہ پریم کے کارن نہیں، وہ آپ کے کارن ہے- آپ ایسے پاتر )کِردار( ہیں کہ امرت بھی زہر ہو جاتا ہے- آپ پاتر کا اُلٹ ہیں، پاتر نہیں ہیں، اِس لئے پریم بھی زہریلا ہو جاتا ہے- پھر اُس کو آپ پریم کہیں گے تو فرید جی کو سمجھنا بُہت مُشکل ہو جائے گا- کیونکہ فرید جی تو پریم  کا لُطف بتائیں گے، پریم میں دھیان کی یکسوئی اور پرماتما کو پائیں گے- آپ نے تو پریم میں صِرف دُکھ ہی جانا ہے، تکلیف، چِنتا، خلفشار- پریم میں تو آپ نے ایک طرح کی بگڑی ہوئی روگی حالت کو ہی جانا ہے- پریم کو آپ نے دوزخ کی طرح ہی تو جانا ہے- آپ کے پریم کی تو بات ہی نہیں ہو رہی-</p>
<p>جِس پریم کی فرید جی بات کر رہے ہیں، وہ تو تبھی پیدا ہوتا ہے جب آپ مِٹ جاتے ہیں- آپ کی قبر پر اُگتا ہے پُھول اُس پریم کا- آپ کی راکھ سے پیدا ہوتا ہے وہ پریم- آپ کا پریم تو ہنکار کی سجاوٹ ہے- آپ پریم میں دوسرے کو شے بنا دیتے ہیں- آپ کے پریم کی کوشش میں دوسرے کو ملکیت بنانا ہوتا ہے- آپ چاہتے ہیں، آپ جِس کو پریم کریں وہ مُٹھی میں آ جائے، آپ مالک ہو جائیں- دوسرا بھی یہ ہی چاہتا ہے- آپ کے پریم کے نام پر ملکیت کا جھگڑا چلتا ہے-</p>
<p>جِس پریم کی فرید جی بات کر رہے ہیں، وہ ایسا پریم ہے، جہاں آپ دوسرے کو آپنا آپ  دے دیتے ہیں- جہاں آپ اپنی مرضی سے دوسرے کی بھینٹ ہو جاتے ہیں، جہاں آپ کہتے ہیں : تیری مرضی، میری مرضی- لڑائی جھگڑے کا تو سوال ہی نہیں- یقینی طور پر ایسا پریم دو لوگوں کے درمیان نہیں ہو سکتا- ایسا پریم دو ایک سی حالتوں میں کھڑی دو سوچوں کے درمیان نہیں ہو سکتا- ایسے پریم کی چھوٹی موٹی جھلک شاید گُورو کے پاس مِلے، پُوری جھلک تو پرماتما کے پاس سے ہی مِلے گی- ایسا پریم میاں بیوی کا نہیں ہو سکتا، دوست دوست کا نہیں ہو سکتا- دوسرا جب آپ کے ہی جیسا ہے تو آپ کیسے اپنے آپ کو بھینٹ کر سکیں گے؟ شک پڑے گا من کو- من  ہزار بار ڈرے گا- یہ دوسرے پر بھروسہ ہو نہیں سکتا کہ سب چھوڑ دو، کہ کہہ سکو کہ تیری مرضی، میری مرضی ہے- اِس کی مرضی میں بُہت بُھول چُوک دِکھائی دے گی- یہ تو بھٹکاوا ہو جائے گا- یہ تو اپنے ہاتھوں اپنی آنکھیں پھوڑ لینا ہو جائے گا- یہ تو اپنی پُھونک سے اپنی عقل کا جو چھوٹا سا دیپ جگا رکھا ہے اُسے بُجھانا ہو جائے گا- یہ تو بے عقلی میں داخل ہونا ہو جائے گا-</p>
<p>اِنسان اور اِنسان کے درمیان پریم محدود ہی رہے گا- آپ چھوڑیں گے تو شرطوں سے چھوڑیں گے- اگر آپ تھوڑی دیر دوسرے کو اپنی ملکیت سونپیں گے تو بھی پُوری ملکیت نہیں سونپیں گے، تھوڑی بچا لیں گے، واپسی کا راستہ رہے، اگر کل واپسی کرنا پڑی ، بھینٹ ہونے سے مُکرنا پڑا تو آپ مُکر سکیں- کہیں ایسا نہ ہو کہ پھر واپسی کی راہ نہ بچے- آپ سیڑھی ہٹا نہیں دیں گے، لگائے رکھیں گے-</p>
<p>عام پریم بِنا شرط کے نہیں ہو سکتا- غیر مشروط نہیں ہو سکتا- اور پریم جب تک بے شرط نہ ہو تب تک پریم ہی نہیں ہوتا- آپ سے اوپر کوئی، جِس کو دیکھ کے آپ کو آکاش کے بادلوں کا خیال آئے- جِس کی اور آپ نے آنکھیں اُٹھانی ہوں تو جیسے سُورج کی اور کوئی آنکھ اُٹھائے، اُس کے اور آپ کے درمیان ایک لمبا فاصلہ ہو- ایک نہ مِل سکنے والی کھائی ہو، جِس میں آپ کو پرماتما کی تھوڑی سی حِس جاگے… اُس کو ہی ہم نے گُرو کہا ہے-</p>
<p>گُرو مشرق کی نِرالی کھوج ہے- مغرب اِس رس سے محروم ہی رہ گیا ہے اُس کو گُرو کا کوئی پتہ نہیں یہ راہ جانا ہی نہیں مغرب نے- مغرب والوں کو بس دو دوستوں کا ہی پتہ ہے، سیکھنے والا طالب عِلم معلوم ہے، میاں بیوی معلوم ہے، پریمی پریمکا معلوم ہے، لیکن فرید جی جِس کی بات کریں گے… گُرو اور چیلا، مُرشد اور مُرید… اُس کا کُچھ پتہ نہیں ہے-</p>
<p>مُرشد اور مُرید کا معانی ہے کوئی ایسا اِنسان جِس کے اندر سے آپ کو پرماتما کی جھلک مِلی، جِس کے اندر آپ نے آسمان دیکھا، جِس کی کِھڑکی میں سے آپ نے وِراٹ میں جھانکا- اُس کی کِھڑکی چاہے چھوٹی ہی ہو… کِھڑکی کے بڑے ہونے کی ضرورت بھی نہیں ہے، لیکن کِھڑکی میں سے جو جھانکا، وہ آسمان تھا- تب نذر ہوا جا سکتا ہے- تب آپ پُورا خود کو چھوڑ سکتے ہیں-</p>






<p>صفحہ 16 </p>
<p>ابھی توں جو بھاو کے نزدیک وہ اپنا ہے، جو بھاو کے نزدیک نہیں وہ پرایا ہے- پھِر تو وہ جو اپنا ہے اُس کے لئے جذبے کا اظہار کرے گا، جو اپنا نہیں اُس کے لئے جذبے کو روک لے گا- ابھی حالت بِلکُل اُلٹی ہے- اِسی لئے چھوٹے بچوں میں معصومیت ہوتی ہے- اور جیسس جیسے لوگوں نے کہا ہے کہ جو چھوٹے بچوں کی طرح ہوں گے، وہ ہی میرے پرماتما کے راج میں داخل ہو سکیں گے-</p>
<p>فرید جی کا پریم من سے اُمنڈتا ہے- من تو آپ بِلکُل ہی بھول گئے ہیں آپ جو کُچھ بھی کرتے ہیں وہ دماغ سے چلتا ہے- ہِردے سے آپ کے رشتے ٹوٹ گئے ہیں- اِس لئے تھوڑی سی تربیت ضروری ہے ہِردے کے لئے- </p>
<p>یہاں میرے پاس لوگ آتے ہیں- اُن کو میں کہتا ہوں کہ تھوڑا آپ من کو بھی جگائیں، ہِردے کو دِل کو- کبھی آکاش میں گھومتے ہوئے، پِھرتے ہوئے بادلوں کو دیکھ کر ناچو بھی، جیسے مور ناچتا ہے- بادل اُمنڈ اُمنڈ کر آنے لگے، ایسے لمحوں میں آپ بیٹھے ہوئے کیا کر رہے ہیں؟ بادلوں نے گُھنگھرو بجا دئیے، ڈھول پر تھاپ دے دی، آپ بیٹھے کیا کر رہے ہیں؟ ناچیں! پنچھی گیت گاتے ہیں! سُر میں سُر مِلائیں! درختوں میں پھول کِھلتے ہیں: آپ بھی کِھل جائیں! تھوڑا، چاروں طرف جو وِراٹ پھیلا ہوا ہے، جِس کے ہاتھ بے شمار شکلوں میں آپ کے نزدﷲیک آئے ہیں… کبھی پھول، کبھی سُورج کی کِرنیں، کبھی پانی کی لہر… اِس سے تھوڑا من کا رِشتہ جوڑیں- بیٹھے سوچتے نہ رہیں- اِندردُھنش آکاش میں کِھلے، تو آپ یہ نہ سوچیں کہ اِندردُھنش کی فزِکس کیا ہے، یہ کہ روشنی غُبار میں سے گُزر کر سات رنگوں میں بٹ جاتی ہے- ایسے پانی کے ذریعوں سے گُزر کر سُورج کی کِرنیں سات رنگوں میں مُشکل ہو گئی ہیں… یہ احمقانہ پن نہ اپنائیں- اِس پنڈتانہ رویے کی وجہ سے آپ محروم ہی رہ جائیں گے- قوس و قزح کی خوبصورتی گُم ہو جائے گی- ہاتھ میں فزکس کی کتاب رہ جائے گی- جِس میں کوئی بھی قوس و قزح نہیں ہے- آپ اِس کے رُوپ سے لُطف اندوز ہوں- قوس و قزح آسمان میں کِھلی ہوئی ہے، آپ اپنے من میں قوس وقزح کو کِھلنے دیں- خُود بھی ایسے ہی رنگ برنگے ہو جائیں- تھوڑی دیر کے لئے آپ اپنی بے رنگ زِندگی میں رنگ اُتر آنے دیں!</p>
<p>چاروں طرف سے پرماتما بُہت رُوپوں میں اپنا ہاتھ پھیلاتا ہے، لیکن آپ اپنے ہاتھ پھیلاتے ہی نہیں، ورنہ مِلن ہو جائے- یہی پڑھائی ہو جائے گی- یہاں سے ہی آپ بیدار ہوں گے، اور آہستہ آہستہ آپ کو پہلی بار پتہ چلے گا بھید کا، کہ وِچار کا پریم کیا ہے من کا پریم کیا ہے-</p>
<p>وِچار کا پریم بِکنے والی چیز ہی بنا ہے، کیونکہ وِچار چالاک ہے- وِچار یعنی حساب- وِچار یعنی ترک- وِچار کی وجہ سے آپ معصوم  ہو ہی نہیں پاتے- وِچار ہی تو بُرا تصور ہے، من معصوم ہوتا ہے- من کا کوئی بُرا تصور نہیں ہے اور وِچار کی وجہ سے آپ کبھی آہستگی ، روانی اور خود سپردگی کا تجربہ نہیں کر سکتے، کیونکہ وچار کہتا ہے: چوکس رہو- کوئی دھوکا نہ دے جائے- چاہے وِچار آپ کو ہر دھوکا سے بچا لے، لیکن وِچار ہی آخر میں دھوکا ہو جاتا ہے- اور من کے کارن شاید آپ کو بُہت کُچھ کھونا پڑے، لکن جِس نے من کو پا لیا اُس نے سب کُچھ پالیا- </p>
<p>فرید جی جِس پریم کی بات کریں گے، وہ من ہے- اور آپ کو اُس کی تھوڑی سی تربیت لینی پڑے گی- اور تربیت مُشکل نہیں ہے- کِسی یون۹یورسٹی میں بھرتی ہونے کی ضرورت نہیں ہے- اچھا ہے کہ من کی تربیت کے لئے کوئی وسیلہ نہیں ہے، نہیں تو ادارے اِس کی تربیت دینے کا بھی بندوبست کر دیتے اور اِسے خراب کر ڈالتے- اگر کہیں معصومیت سِکھانے کے لئے کسرت خانے جیسی جگیہیں ہوتیں تو وہاں معصومیت بھی سِکھا دی جاتی، آپ اُس میں بھی طاق ہو جاتے، تب آپ اُس سے بھی محروم ہو جاتے-</p>
<p>من کی سِکھلائی کا کوئی راہ نہیں ہے، صِرف تھوڑا عقل کو ڈھیلا چھوڑنا پڑتا ہے- من سدا سے موجود ہے، آپ لے کے آئے ہیں، من آپ کی رُوح ہے، آتما ہے-</p>
<p>اب فرید جی کے اِن بچنوں کو سمجھنے کی کوشش کرتے ہیں:</p>
<p> بولے شیخ فرید پیارے اللہ لگ''</p>
<p>شیخ فرید جی کہتے ہیں: میرے پیارو، اللہ سے جوڑ لو اپنی پریت-</p>
<p>'بولے شیخ فرید پیارے اللہ لگ'</p>
<p>ایسا ہی اِس کا ترجمہ ہمیشہ سے کیا گیا ہے کہ فرید جی کہتے ہیں: میرے پیارو اللہ سے جوڑ لو اپنی پریت- پر مُجھے لگتا ہے کہ لفظی طور پر تو یہ معانی ٹھیک ہیں- لیکن فرید جی کا گہرا اشارا چُوک گیا ہے- میں اِس کے ترجمہ میں تھوڑا فرق کرنا چاہتا ہوں- </p>
<p>'بولے شیخ فرید پیارے اللہ لگ' شیخ فرید جی کہتے ہیں : پیارو، اللہ کے ساتھ جُڑ جائو- اللہ کے ساتھ لگ جانا کو میں کہہ رہا ہوں من کی سِکھلائی- اللہ کے ساتھ اگرآپ نے پریم کرنے کی کوشش کی تو آپ وہی پریم کریں گے جو آپ اب تک کرتے ہرے ہیں- آپ کی بھی مجبوری ہے- آپ نیا پریم کہاں سے لائیں گے؟ آپ اللہ کے ساتھ بھی پریم کریں گے تو وُہی پریم کریں گے جو اب تک کرتے رہے ہیںۃ بے شمار بھگت وُہی تو کرتے ہیں- کوئی رام کی مُورتی سجا کے بیٹھا ہے، تو اُس نے رام کی مُورتی یوں سجا لی ہے جیسے کہ کوئی پریمیکا اپنے پتی کو سجاتی ہے، ہِیرے جواہرات لاد دیے ہیں، سونے چاندی کا سامان اکٹھا کر دیا ہے، بھوگ لگا دیتا ہے، بِستر پر سُلا دیتا ہے، اُٹھا دیتا ہے، مندر کے دروازے بند ہو جاتے ہیں، کُھل جاتے ہیں- </p>
<p>اگر آپ نے پرماتما کو پِتا کی طرح دیکھا تو آپ اُس کی سیوا میں اُسی طرح لگ جائیں جیسے آپ کو اپنے پِتا کی سیوا میں لگ جانا چاہئیے- اگر آپ نے پرماتما کو محبوبہ کے رُوپ میں دیکھا ہے جیسے صُوفی دیکھتے ہیں، تو آپ اُس کے گیت ویسے ہی گانے شُروع کر دیں گے جیسے کہ لیلی نے منجوں کے گائے- لیکن اِن سب میں آپ نے جو پریم جانا، اُسی کو آپ پرماتما پر تھوپ رہے ہیں، نئے پریم کی روشنی نہیں ہو رہی ہے-</p>
<p>اِسی لئے تو ہزاروں بھگت ہیں، لیکن کوئی ایک آدھ بھگوان کو پاتا ہے- کیونکہ آپکی  بھگتیآپ کے ہی پریم کا دُہرائو ہوتی ہے- اگر آپ کے پریم سے ہی پرماتما نے مِلنا ہوتا تو کبھی کا مِل گیا ہوتا- آپ پھر سے پرماتما کے ساتھ وہی رِشتے ناطے بنا لیتے ہیں جو آپ نے اِنسانوں کے ساتھ بنائے ہیں- آپ اُس کے ساتھ بھی وُہی باتیں کرتے ہیں، جو آپ نے اِنسانوں کے ساتھ کی تھیں- آپ کِسی کے پریم میں پڑ گئے تھے، آپ نے کہا تھا، تُم سے خوبصورت کوئی بھی نہیں، اب آپ پرماتما سے یہی کُچھ کہتے ہیں کہ تُم ایسے ہو، تُم ویسے ہو- آپ وُہی باتیں کر رہے ہیں لفظ باسی ہیں، اُدھار کے ہیں-</p>
<p>اِسی لئے میں فرید جی کے وچن کے معنے کرتا ہوں… پیارے اللہ لگ' تو اللہ سے مُنسلک ہو جا- اب اللہ سے  مُنسلک ہو جانے کا مطلب کیا ہوگا؟ وُہی مطلب ہو گا کہ چاروں طرف سے استتو نے گھیرا ہوا ہے، اللہ نے تمہیں احاطہ کیا ہوا ہے- تُم ہی الگ تھلگ ہو، اللہ تو لگا ہی ہوا ہے تُم بھی لگ جائو- اللہ نے تو تُمہیں اِس طرح سے بھی احاطہ کیا ہوا ہے جِس طرح مچھلی کو سمندر نے احاطہ کیا ہوا ہے-  </p>

<p>صفحہ 20 </p>
<p>تُم وہی باتیں کر رہے ہو لفظ باسی ہیں، اُدھار ہیں-</p>
<p>اِسی لئے میں فرید جی کے وچن کا مطلب کرتا ہوں--- پیارے اللہ لگے، تو اللہ سے لگ جا- اب اللہ سے لگ جانا کے معنے کیا ہوں گے؟ وُہی معنے ہوں گے کہ چاروں طرف سے استِتو نے گھیرا ہوا ہے، اللہ نے تُجھے گھیرا ہی ہوا ہے- تو ہی الگ تھلگ ہے، اللہ تو لگا ہی ہوا ہے تُو بھی لگ جا- اللہ نے تو تُجھے اِسی طرح گھیرا ہوا ہے جیسے مچھلی  کو ساگر نے گھیرا ہوا ہو- اللہ تو تیرے ساتھ لگا ہی ہوا ہے، کیونکہ اللہ نہ لگا ہو تو تُو بچ ہی نہیں سکے گا، ایک پل نہ جی سکے گا، سانس ہی نہیں چلے گی، دِل ہی نہیں دھڑکے گا- وُہ تو اللہ تیرے ساتھ لگا ہے، اِس لئے دھڑکتا ہے- چلتی ہے سانس، زِندگی ہے- تُو غلط بھی جائے تو بھی اللہ لگا ہوا ہے، چور بھی ہو جائے، قاتل بھی ہو جائے تو بھی اللہ لگا ہوا ہے کیونکہ اللہ ہے بغیر قاتل بھی سانس نہیں لے سکے گا، چور کا دِل بھی دھڑکے گا- بُرے ہو یا بھلے، اللہ فِکر نہیں کرتا، وہ لگا ہی ہوا ہے-</p>
<p>اصلی سوال اب  یہ ہے کہ ہم بھی اُس کے ساتھ کِس طرح لگ جائیں  جیسے کہ وہ ہمارے ساتھ لگا ہے غیر مشروط- کہتا نہیں کہ تُم اچھے ہو تو ہی تُمہارے اندر سانس لوں گا، سادُھو ہو جائو تو ہی دِل دھڑکے گا، جو سادُھو نہیں وُہ قانون کے خلاف گئے، دائیں چلے بائیں نہیں چلے سڑک پر، اب سانس نہیں چلے گی- اللہ اگر ایسا کنجوس ہوتا کہ صِرف سادھووں میں دھڑکتا، غیر سادھووں میں نہ دھڑکتا، تو جہان بے رونق ہو جاتا، تو رام ہی رام ہوتے، راون دِکھائی نہ دیتے اور تُم سوچ سکتے ہو، کہ رام ہی رام ہوں دُنیا میں تو کِس طرح کی بے رونق ہو جائے دُنیا- لے کر اپنی اپنی کمان سب کھڑے ہیں، کوئی نہیں سامنے جِس کو اپنا تیر ماریں! سیتا جی کھڑے ہیں، کوئی چُرانے والا نہیں ہے! رام کہانی آگے بڑھتی نہیں!</p>
<p>نہ، پرماتما رام میں بھی سانس لے رہا ہے، راون میں بھی اور ذرا فرق نہیں رکھ رہا ، دونوں میں ہی لگا ہوا ہے- پرماتما بے شرط، بغیر پاک ناپاک کا خیال کئے ، بغیرتمہاری اہلیت نااہلیت کا خیال کئے، تُمہارے ساتھ ہے- تُم اُس کے ساتھ نہیں ہو- ساگر تو تُمہارے ساتھ ہے، تُم اُس کے ساتھ لڑ رہے ہو!</p>
<p>''بولے شیخ فرید پیارے اللہ لگ''</p>
<p>فرید جی کہتے ہیں، پیارو، اللہ سے لگ جائو، ویسے ہی جیسے اللہ تُمہارے ساتھ لگا ہے-</p>
<p>ہِندُو کے اللہ سے مطلب نہ مُسلمان کے اللہ سے مطلب - نہیں تو شرط ہو جائے گی- مندر کے اللہ سے مطلب نہ مسجد کے اللہ سے مطلب، نہیں تو شرط ہو جائے گی- جیسے وہ بے شرط لگا ہے… نہ پُوچھتا ہے کہ تُم ہِندو ہو، نہ پوچھتا ہے کہ تُم مُسلمان ہو، نہ پوچھتا ہے کہ تُم جین ہو، بُدھ ہو، کہ عیسائی ؟ پوچھتا ہی نہیں، نہ پوچھتا ہے کہ عورت ہو یا مرد- نہ پوچھتا ہے کہ کالے ہو یا گورے - کُچھ پوچھتا ہی نہیں- بس تُمہارے ساتھ لگا ہے، اِسی طرح تُم بھی نہ پوچھو کہ تُو مسجد کا  یا مندر کا یا تو قُرآن کا یا پُران کا یا تُو گورے کا یا کالے کا… تُم بھی لگ جائو-</p>
<p>مندر اور مسجد کے اللہ نے مصیبت کھڑی کی ہوئی ہے- تُم اُس اللہ کو تلاش کرو جو سب میں بستا ہے، ہر طرف موجود ہے، جِس نے سب طرف اپنا مندر بنایا ہے، کہیں درخت کی ہریالی میں کہیں جھرنے کے سُر میں، کہیں پہاڑ پر یکسوئی میں ، کہیں بازار کے شور شرابے میں جِس نے بُہت طرح سے اپنا مندر بنایا ہے- سارا جہان اُس کے ہی مندر کے ستون ہیں- سارا آسمان اُس کے ہی مندرکا نظارہ ہے-  ساری دُنیا کی وُسعت اُسی کی زمین ہے- تُم اِس اللہ کو پہچاننا شُروع کرو، اِس سے لگو-</p>
<p>بولے شیخ فرید پیارے اللہ لگ-</p>
<p>ایہہ تن ہوسی خاک نمانی گور گھرے</p>
<p>لگنے میں اِتنی دیر نہ کرو کیونکہ جلدی ہی قبر میں سڑو گے- یہ دیر تو کاک ہو جائے گی اور اِس کا گھر نگوڑی قبر میں جا بنے گا- اِس سے تُم ضرورت سے زیادہ لگ گئے ہو- جِس سے لگنا چاہئیے اُس کو بھول گئے- جِس سے نہیں لگنا تھا اُس سے چِمٹے رہے- جو تُمہاری زِندگی کی زِندگی ہے، اُس سے تُم نے ہاتھ دُور کر لئے اور جو پل بھر کے لئے تُمہاری آرام گاہ ہے، راہ گُزر پر سرائے میں رُک گئے ہو رات بھر کے لئے- سرائے کو تو پکڑ لیا، خود کو چھوڑ دیا ہے-</p>
<p>یہ جِسم… 'ایہہ تن ہوسی خاک' یہ جِسم تو جلدی ہی خاک ہو جائے گا! 'نِمانی گور گھرے' اور کِسی نگوڑی قبر میں اِس کا گھر بن جائے گا- تُم اِس سے نہ جکڑے رہو- اگر پکڑنا ہی ہے تو زِندگی کی ریکھا کو پکڑو- لہروں کو کیا پکڑتے ہو  پکڑنا ہے تو ساگر کو پکڑ لو- کیونکہ لہریں تو تُم پکڑ بھی نہ سکو گے اور وُہ مِٹ جائیں گی، تُم مُٹھی مِیچ بھی نہ پائو گے اور وُہ نِکل جائیں گی- </p>
<p>لہروں کو پکڑنا جِسم کو پکڑنا ہے- جِسم ترنگ ہے، پانچ تت کی ترنگ ہے، پانچ تتوں کے اِس وسیع ساگر میں اُٹھی ایک بڑی لہر ہے… ستر سال تک بنی رہتی ہے، اسی سال تک بنی رہتی ہے- لیکن وُسعت کے سامنے ستر اسی سال کیا ہیں، پل بھی بھی نہیں ہیں- جو وقت تُم سے پہلے ہو گُزرا اور جو وقت تُم سے بعد ہو گا، اُس کو حساب میں لو تو تُم جِتنا وقت ہو وُہ صِفر کے برابر ہے- یہ لہر ہے- اِس کو تو تُم زور سے پکڑ لیا ہے اور جِس میں یہ لہر اُٹھی ہے، اُس کو تُم بُھول ہی گئے ہو- جو بُنیاد ہے اُس کو بھول گئے، پتوں پر بھٹک رہے ہو-</p>
<p>'ایہہ تن ہوسی خاک نِمانی گور گھرے'</p>
<p>'اج مِلاوا شیخ' یہ بول مُجھے بُہت پیارا ہے- یہ بُہت نِرالا ہے- اِس کو بُہت غور سے جاگ کے سمجھنے کی کوشش کرنا-</p>
<p>آج اُس پریتم سے مِلن ہو سکتا ہے، شیخ اگر تو اُن خواہشوں کو قابو کر لو جو تیرے من کو بے چین کر رہی ہیں-</p>
<p>اج مِلاوا شیخ - آج مِلن ہو سکتا ہے تیرا- یہ بُہت بدل دینے والا بول ہے کیونکہ سادگی، تُمہارے پنڈت پروہت کہتے ہیں کہ جنم جنم کا پاپ ہے، اُس کو کاٹنا پڑے گا، تب مِل ہو سکے گا- اور شیخ کہتے ہیں، اج مِلاوا شیخ… یہ آج ہو سکتا ہے بات اِسی لمحے ہو سکتی ہے- اِس کو ایک پل بھی ٹالنے کی ضرورت نہیں ہے کیونکہ پرماتما آج بھی اِتنا ہی حاصل ہے جِتنا کبھی تھا، اور اِتنا ہی سدا رہے گا- اُس کا حصول رتی برابر بھی نہیں کم ہو گا- تُم نے گُناہ کئے یا ثواب اِس سے کُچھ فرق نہیں پڑتا- تُم جِس دِن بھی اُس سے لگنے پر راضی ہو، اُس کی آغوش سدا ہی لامتناہی ہے- تُمہارے گُناہ رُکاوٹ نہیں بن سکیں گے- تُم ہی کیا ہو، تُمہارے گُناہ کا کِتنا مول ہو سکتا ہے؟ لہر ہی مِٹ جاتی ہے، تو لہر کی مٹک کِتنی رُک سکتی ہے؟</p>
<p>اِس کو تھوڑا ہم سمجھ لیں-</p>
<p>یہ بھی اِنسان کا ہنکار ہے کہ میں نے بُہت گُناہ کئے ہیں- یہ تُمہیں کٹھن لگے گا سمجھنے میں- تُم کہو گے یہ کیسا ہنکار؟ لیکن اِس سے بھی ایسا لگتا ہے کہ میں کُچھ ہوں- اور اِس سے ایسا بھی لگتا ہے کہ جب تک میں اِن سارے گُناہوں کو نہ کاٹ لوں گا، تب تک پرماتما نہیں مِلے گا- جیسے پرماتما کا مِلن میرے کِسی عمل پر منحصر ہے- اِس لئے کرم کا نظریہ ہنکاریوں کو خُوب جما، خُوب جچا- ہنکاریوں کے ہنکار کو تائید مِل گئی کہ ہم ہی نے کرم کئے تھے، اِس لئے ہم بھٹک رہے ہیں، ہم ہی کرم جب ٹھیک کریں گے تو پُہنچ جائیں گے- لیکن ہم میں چُھپا ہوا ہے اندر-</p>
<p>یہی فرق ہے بھگتوں کا- بھگت کہتے ہیں پُہنچیں گے اُس کے پرشاد سے- تُمہاری کوششوں سے نہیں پُہنچیں گے- تُمہارا جتن ہی تو اٹکا رہا ہے- یہ خیال کہ میرے کرنے سے کُچھ ہو گا، یہ ہی تو تُمہیں بھٹکا رہا ہے- کرم نہیں بھٹکا رہے، عامل کا خیال بھٹکا رہا ہے- اصلی گُناہ اعمال میں نہیں ہے، کرنے والے کے خیال میں ہے- اِس لئے کرشن نے ارجن سے گیتا میں کہا- تُو کرنے والے نہ بن- تُو عاجز ہو جا- پِھر تُمہیں کُچھ بھی کرم نہیں چھوئے گا- کرم تو کر، لیکن اِس طرح کر جیسے وہ ہی تیرے سے  کر رہا ہے، تُو درمیان میں نہیں ہے- تُو بانس کی پونگری ہو جا، اُس کو ہی گانے دے گیت- وہ  گائے تو ٹھیک نہ گائے تو ٹھیک- تُو بیچ میں کوشش نہ کر- تُو خود کو بیچ میں نہ لا، خود کو بیچ میں کھڑا نہ کر-</p>
<p>شیخ فرید کہتے ہیں- آج ہی ہو سکتا ہے مِلن- یہ بُہت ہِمت والے فقیروں نے کہا ہے- تُمہارا من تو خود بھی ڈرے گا کہ یہ کیسے ہو سکتا ہے آج- اصل میں سچائی یہ ہے کہ تُم آج چاہتے بھی نہیں- تُم چاہتے ہو کہ کوئی سمجھائے کہ کل ہو سکتا ہے، آج نہیں کیونکہ آج اور دوسرے کام کرنے ہیں، ہزار دھندے پُورے کرنے ہیں- اِس میں تو مُلتوی کرنے، ٹال دینے کا راستہ نہیں رہے گا-</p>
<p>اِس لئے میں تُم سے کہتا ہوں کہ کرم کا نظریہ ہنکاریوں کو خُوب جچا اور ہنکاریوں کو یہ بات بھی خُوب جچی کہ جب تک ہم کرموں کو بدل نہیں دیں گے، ناپاک کو پاک سے مٹا نہیں دیں گے، کالے کو سفید سے پوچ نہ دیں گے-  جب تک ترازو توازن میں نہ ہو جائے گا، پاک ناپاک برابر نہ ہو جائیں گے تب تک چُھٹکارہ نہیں ہو سکے گا- </p>
<p>اِس طرح مشرق میں، جہاں کہ پُنرجنم کے نظریہ کو، کرم کے نظریہ کو بڑی قبولیت مِلی، دھرم کے اُلٹ کا بُہت پھیلائو ہوا- ہونا نہیں چاہئیے تھا- اگر اصلیت میں لوگوں نے اِس لئے قبولیت دی تھی پُنر جنم کا نظریہ کو کہ وہ مذہبی تھے تو مشرق میں انقلاب ہو جانا چاہئیے تھا، مشرق میں سُورج اُجاگر ہو جاتا- نہیں ہوا-</p>
<p>مشرق جتنا بے ایمان ہے- مغرب میں اُتنے بے ایمان آدمی نہیں پائے جاتے- اور مشرق جِتنا انیتک ہے اور جِتنا بھرشت ہے، اِس جیسی بھرشٹاچاری اور انیتکتا کہیں نہیں پائی جاتی- بڑے سے بڑے مادہ پرست مُلکوں میں بھی اِس جیسی انیتی نہیں ہے- وجہ کیا ہے؟</p>
<p>مشرق کے پاس ٹال مٹول ہے- ہم ٹال سکتے ہیں- اُن کے پاس آسانی نہیں ہے اُن کی یہ زِندگی سب کُچھ ہے- اچھے ہونا ہے تو یہ ہی زِندگی ہے- ہمارے پاس بُہت زِندگی ہے- ہمیں کوئی جلدی نہیں ہے- مشرق کے من میں جلدی نہیں ہے- اِس لئے جو چل رہا ہے چلنے دو، جو ہو رہا ہے ہونے دو- ہم کِسی جلدی میں نہیں ہیں کہ انقلاب ابھی ہو جائے- وقت ہمارے پاس ضرورت سے زیادہ ہے- آج نہیں ہو گا، کل ہو جائے گا، کل نہیں ہو گا، پرسوں سہی- ہمیں بُہت تسلی ہے- تسلی آڑ بن گئی ہے- جہاں جہاں تسلی بُہت ہو جاتی ہے، وہاں انقلاب نامُمکن ہو جاتا ہے-</p>
<p>اگر آج تُم کو پتہ چل جائے کہ آج ہی ہو سکتی ہے یہ بات تو پھر تُم کو الزام خود کو ہی دینا پڑے گا- پھر تُم پر واضح ہو جائے گا کہ اگر ٹالتے ہو تو تُم خود ٹالتے ہو، پوسٹپون کرتے ہو تو تُم خود، پرماتما تو آج پر ہی راضی ہے- تب تُم کو بُہت بے چینی ہو گی- تب تُم کو کِسی بات کی تسلی نہیں رہے گی-</p>
<p>اِس لئے فرید جی کا کہنا ، میں کہتا ہوں بُہت انقلابی ہے- اج مِلاوا شیخ فرید… آج ہو سکتا ہے مِلن، شیخ- دیر اُس کی طرف سے نہیں ہے-</p>
<p>ہمارے دیس میں کہاوت ہے: دیر ہے اندھیر نہیں- میں تُم سے کہتا ہوں، نہ دیر ہے، نہ اندھیر ہے- اُس کی طرف سے کُچھ نہیں ہے، تُمہاری طرف سے دونوں ہیں- اُس کی طرف سے نہ دیر ہے نہ اندھیر ہے- تُمہاری طرف سے دیر ہے- اِس لئے اندھیر ہے- دیر کی وجہ سے اندھیر ہے- ٹالنے کا رویہ ہے… تو آج جیسی بھی صورتحال ہے ٹھیک ہے- کل سب ٹھیک ہو جائے گا- کل کے خوابوں کی بُنیاد پر تُم آج کی گندی صُورتحال کو ٹالتے جا رہے ہو- کل کی اُمید پر آج کی بیماری برداشت کر لیتے ہو- آج کا دوزخ سہہ لیتے ہو، کل بہشت مِلے گی-</p>
<p>اج مِلاوا شیخ فرید… آج اُس محبوب سے مِلن ہو سکتا ہے- کرنا کیا ہے؟ بس ایک چھوٹی سی بات کرنی ہے، 'کُونجڑیاں منہُ مچیندڑیاں' یہ جو من میں سوچوں اور خیالوں کاشور و غُل مچا ہوا ہے، یہ صِرف بند ہو جائے- کرموں کو نہیں بدلنا ہے، صِرف وِچاروں کو پُرسکون کرنا ہے- کرموں کو بدلنا تو نامُمکن ہے- کِتنے جنم تمہارے ہوئے کوئی حساب ہے؟ اُن میں کیا، کیاتُم نے نہیں کیا، کوئی حساب ہے؟ اُس سارے کو بدلنے بیٹھو گے تو یہ کہانی تو ختم ہو جائے گی- یہ تو پھر کھبی ہو ہی نہیں پائے گا- بلکہ بُدھ ہوئے، مہاویر ہوئے، فرید ہوئے، نانک ہوئے، دادُو ہوئے، کبیر ہوئے- اِن کے جیون میں انقلاب برپا ہوتا ہم نے دیکھا- ایسا ہوا ہے، پھِر اِس کے رُونما ہونے کا سبب کیا ہو سکتا ہے کہ جو ہم نے سمجھا ہے کہ کرم بدلنے ہوں گے، وہ بھرم ہے، صِرف سوچیں چھوڑ دینا کافی ہے- ایسا سمجھو کہ ایک آدمی رات کو سُپنا دیکھتا رہا ہے کہ اُس نے بُہت قتل کئے ہیں، بڑی چوریاں کی ہیں، بُہت ظُلم کئے ہیں، اور وہ بُہت پریشان ہے اپنے سُپنے میں  کہ اب کیسے نجات ہو گی، اِتنا گُناہ کر کیا ہے- اب اِن سب کے اُلٹ کیسے اچھے اعمال کروں گا؟ تب تُم جانتے ہو، اُس کو ہلا کر جگا دیتے ہو، آنکھ کُھلتی ہے، سُپنے گُم ہو گئے- کرم نہیں بدلنے پڑتے- سُپنا ٹوٹ جائے، بس! پِھر وُہ خود ہی ہنسنے لگتا ہے کہ یہ میں کیا سوچ رہا تھا- یہ کیا ہو رہا تھا! اور میں سوچ رہا تھا کہ اِس چُھٹکارہ کیسے ہو گا لیکن سُپنا ٹوٹے ہی چُھٹکارہ ہو گیا-</p>
<p>خیالات تُمہارے سُپنے ہیں- کرم نے نہیں باندھا ہوا ، باندھا ہوا ہے خیالوں نے- کرم تو خیالوں کے سُپنوں کے اندر ہو رہا ہے- اگر خیالات ٹوٹ جائیں، سُپنے ٹوٹ جائیں، تُم جاگ جائو گے- سارے جنم تُمہارے جو ہوئے ، وہ ایک گہرا سُپنا تھا- ایک دُکھ کا سُپنا تھا- اُس کو بدلنے کا کوئی سوال نہیں ہے اُس کو مٹانے کا کوئی سوال نہیں ہے- جاگتے ہی وہ نہیں پایا جاتا ہے-</p>
<p>شیخ، اگر تو اُن خواہشات کو قابو کر لے، جو تیرے من کو بے چین کر رہی ہیں- وہ جو تیرے جذبات، تیرے خیالات اور اُن کی جو ترنگیں، جھنجھناہٹیں اور شوروغُل تیرے اندر بپا ہے بس اُس کو تو پُر سکون کر لے-</p>
<p>اُس کو شانت کرنے کے دو ڈھنگ ہیں-</p>
<p>ایک ڈھنگ دھیان ہے-</p>
<p>دھیان خالص عِلم ہے کہ تُم عِلمی بُنیاد پر من کی ترنگوں کو ایک کے بعد ایک شانت کرتے چلے جاتے ہو- لیکن دھیان کا راہ ریگستان کا رستہ ہے- وہاں سایہ دار درخت نہیں ہیں- زرخیز دھرتی نہیں ہے- وہاں چاروں طرف پُھول ہریالی نہیں ہے، پنچھیوں کے گیت نہیں گُونجتے، لا مُنتہا ریت ہے، تپتی ہوئی- دھیان کا راستہ خُشک ہے حساب و شمار کا ہے- </p>
<p>دوسرا راہ ہے پریم کا، کہ تُم اِتنے پریم سے لبریز ہو جائو کہ تُمہاری زِندگی کی تمام کمی پریم بن جائے، تو جو کمی خواہش بن رہی تھی، خیالات بن رہی تھی، ترنگ بن رہی تھی- وہ  اُمنڈ آئے اور ساری پریم میں شامل ہو جائے- اِسی لئے تو تُم بڑے درخت کے نیچے چھوٹا درخت لگائو تو وہ پنپتا نہیں ہے، کیونکہ بڑا درخت سارے رس کو کھینچ لیتا ہے دھرتی میں سے ، چھوٹے درخت کو رس نہیں مِلتا- جو بُہت بڑے درخت ہیں وہ اپنے بیج کو دُور بھیجنے کی کوشش کرتے ہیں، کیونکہ بِیج اگر نیچے ہی گِر جائیں تو وُہ کبھی درخت نہیں بن پائیں گے-</p>
<p>سِمبل کا بڑا درخت اپنے بیجوں کو روئی میں لپیٹ کر دُور بھیجتا ہے تاکہ ہوا میں رُوئی اُڑ جائے- ماہرین کہتے ہیں کہ وہ بُہت چالاک اور سمجھدار درخت ہے- وُہ ترکیب سمجھ گیا ہے کہ اگر بیج نیچے ہی گِرے تو کبھی پنپے گے نہیں، اُس کی نسل ختم ہو جائے گی- وُہ اُس روئی کو تُمہارے سرہانوں کے لئے نہیں بناتا- تُمہارے سرہانوں کے ساتھ سِمبل کا کیا سروکار؟ وہ اپنے بیجوں کو پر لگاتا ہے، رُوئی میں لپیٹ دیتا ہے- رُوئی ہوا میں اُڑ جاتی ہے اور جب تک ٹھیک دھرتی نہ مِل جائے تب تک وہ اُڑتی چلی جاتی ہے جب ایسی دھرتی مِل جاتی ہے، جہاں کوئی درخت بڑا نہیں ہے نزدیک نزدیک، تب وہ دھرتی کو پکڑ لیتا ہے-</p>
<p>بڑے درخت کے نیچے چھوٹا درخت نہیں بڑھتا- ٹھیک تُمہارے اندر جب پریم کا بڑا درخت پیدا ہوتا ہے تو سب چھوٹی بڑی ترنگیں گُم ہو جاتی ہیں- سارا دھرتی میں سے رس ایک ہی پریم میں چلا آتا ہے، پریم واحد تمنا، خواہش بن جاتا ہے- سب لپٹوں کو اپنے اندر سمو لیتا ہے-</p>
<p>سو، ایک تو دھیان ہے کہ تُم ایک ایک ترنگ اور ایک ایک خیال کو یکے بعد دیگرے شانت کرتے جائو- پِھر ایک پریم ہے کہ شانت کِسی کو نہ کرو، سب کو لپیٹ لو ایک بڑی تمنا میں، ایک بڑی خواہش میں- تُم ایک عظیم لَپٹ بن جائو، سبھی لپٹیں اُس میں سما جائیں- یہ دو حل ہیں-</p>
<p>پریم کا راہ بُہت سرسبز ہے- اُس پر پنچھی بھی گیت گاتے ہیں مور بھی ناچتے ہیں- اُس پر ناچ بھی ہے- اُس پر تُم کو کرشن بھی مِلیں گے بنسری بجاتے ہوئے- اُس پر تُم کو چیتنیہ بھی مِلیں گے گیت گاتے ہوئے- اُس پر تُم کو مِیرا بھی ناچتی ہوئی مِلے گی-</p>
<p>دھیان کا راہ بُہت خُشک ہے- اُس پر تُم کو مہاوِیر مِلیں گے لیکن ریگستان جیسا ہے راہ- اُس پر تُم کو بُدھ بیٹھے مِلیں گے، لیکن کِسی پنچھی کی گُونج تُم کو سُنائی نہیں دے گی- تُمہاری مرضی! جِس کو ریگستان سے لگائو ہو… ایسے بھی لوگ ہیں جِن کو ریگستان میں خوبصورتی مِلتی ہے - اپنی اپنی بات ہے-</p>
<p>ایک نوجوان نے آ کے مُجھے کہا کہ جیسی خوبصورتی اُس نے ریگستان میں جانی ہے اُس جیسی اُس نے کہیں اور نہیں جانی- یہ مُمکن ہے- کیونکہ ریگستان میں جو وُسعت ہے وہ کہیں بھی نہیں ہے، سہارا کے ریگستان میں کھڑے ہو کے تو کِنارہ ہی نہیں دِکھائی پڑتا، ریت کا ساگر ہے بے کراں، کہیں کوئی اختتام نہیں معلوم ہوتا- اِس اختتام میں کِسی کو خوبصورتی مِل سکتی ہے، کوئی عجب نہیں- اور جیسی رات ریگستان کی ہوتی ہے اُس جیسی تو کہیں بھی نہیں ہوتی! جیسے ریگستان کی رات میں تارے صاف دِکھائی دیتے ہیں ویسے کہیں نہیں دِکھائی دیتے، کیونکہ ریگستان کہ ہوائوں میں کوئی بھی کثافت نہیں ہوتی، ہوا بالکُل صاف اور شفاف ہوتی ہے، ستارے اِتنے صاف دِکھائی دیتے ہیں کہ ہاتھ بڑھائو اور چھُو لو-</p>
<p>سو دھیان کا اپنا مزہ ہے- دھیان جِس کو ٹھیک لگ جائے وہ اُس طرف چل دے- پریم کا اپنا مزہ ہے- دھیان کا پُورا مزہ تو جب منزل مِلے گی تب آئے گا- دھیان کا مزہ تو پُورا آخر میں آئے گا- پریم کا مزہ قدم بقدم ہے- پریم کی منزل پُورے رستے پر پھیلی ہے- دھیان کی منزل آخر میں ہے اور رستہ بُہت رُوکھا سُوکھا ہے- پریم کی منزل ختم ہونے میں نہیں ہے، قدم قدم پر پھیلی ہے، پُورے راہ پر پھیلی ہے منزل- تُم جہاں رہو گے وہاں ہی مزے میں رہو گے- تُمہاری مرضی! ہر کِسی کو اپنا اپنا انتخاب کر لینا چاہئیے-</p>
<p>آج اُس محبوب سے وصل ہو سکتا ہے، شیخ، اگر تو خیالات کو قابو میں کر لے، اُن ترنگوں کو روک لے جو تُمہیں بے چین کر رہی ہیں-</p>
<p>'کُنجڑیاں منہُ مچِندڑِیاں' جِنہوں نے تیرے من میں بُہت خرابی پیدا کر رکھی ہے، اُن کو تُو بس میں کر لے- </p>
<p>اور دھیان رکھنا، پریم کا راستہ آسان ہے کیونکہ تُم ساری خرابی کو پرماتما کے چرنوں میں سما دیتے ہو- تُم کہتے ہو تُو ہی سنبھال! ہم تیرے ساتھ لگ جاتے ہیں، تو فِکر کر-</p>
<p>تُم ایک بُہت ہی بڑی کشتی میں سوار ہو جاتے ہو-</p>
<p>دھیان کا راستہ چھوٹی کشتی کا ہے-</p>
<p>بُدھ کے دُنیا سے چلے جانے کے بعد دو فِرقے ہو گئے، اُن کے پیروکاروں کے- ایک کا نام ہے ہِینیان- ہِینیان کا مطلب ہوتا ہے چھوٹی کشتی- چھوٹی کشتی والے لوگ- ہِینیان دھیان کا راستہ ہے- اپنی اپنی ڈونگی، دو بھی نہیں بیٹھ سکتے، دو بیٹھ بھی جائیں تو اُلٹ جائے، ایک ہی بیٹھ سکے اور وہ بھی پُورا سنبھل کے ہی چلے- اور اپنے ہی ہاتھ چلانی ہے، اور کوئی سہارہ نہیں ہے، اور بُہت بڑا طُوفان ہے-</p>
<p>اور دوسرے پنتھ کا نام ہے مہایان- مہایان پریم کا راستہ ہے- اِس کا مطلب ہے، بڑی کشتی- جِس میں ہزاروں لوگ سوار ہو سکیں- ہِینیان کہتا ہے بُدھ سے اشارہ لے لو، لیکن بُدھ کا سہارا نہ لو- کیونکہ چلنا تُم کو ہے- مہایان کہتا ہے اشارہ کیا لینا، سہارا ہی لے لیتے ہیں، بُدھ کے کاندھے پر ہی سوار ہو جاتے ہیں- بُدھ جا ہی رہے ہیں، ہم اُن کے ساتھ ہو لیتے ہیں-</p>
<p>مہایان پھیلا بُہت، ہِینیان سُکڑ گیا- کیونکہ ہِینیان تھوڑے سے لوگوں کا رس ہو سکتا ہے- وہ راستہ ہی تنگ ہے- مہایان وسیع ہوا- جو بھی بُدھ مت کا پھیلائو ہوا بعد میں وہ مہایان کی وجہ سے ہوا- کیونکہ بھگتی اور پریم من کو چھوتے ہیں، جگاتے ہیں- کیا پڑی ہے روتے ہوئے جانے کی، جب ہستے ہوئے جایا جا سکتا ہو؟ اور کیا ضرورت ہے متفکر چہرے بنانے کی، جب مُسکراتے ہوئے راستہ طے ہو جاتا ہو؟ اور کیا ضرورت ہے بِلا مقصد تکلیف اُٹھانے کی جب پریم کی بھینی بھینی بہار میں زیارت ہو سکتی ہو؟</p>
<p>آج اُس پیارے سے مِلن ہو سکتا ہے، شیخ، بس تو من کی ساری ترنگوں کو وقف کر دے-</p>
<p>'جے جانا مر جائیے' شیخ کہتے ہیں، اگر پتہ ہوتا کہ مرنا ہی ہو گا تو زِندگی ایسے ہی نہ گنواتے- اِس زِندگی میں جِس کو بھی معلوم ہو جاتا ہے کہ موت ہے، وہ ہی بدل جاتا ہے، اور جِس کو معلوم نہیں ہوتا کہ موت ہے وہ ہی برباد ہو جاتا ہے- مذہب تُمہاری زِندگی میں اُسی دِن اُترتا ہے جِس دِن تُمہاری زِندگی میں موت کی آگاہی آتی ہے، جِس دِن تُم کو لگتا ہے کہ موت آتی ہے- موت کا آنا، موت کے آنے کا خیال، موت کے قدموں کی آہٹ جِس کو سُنائی دے جاتی ہے وُہ آدمی اُسی وقت تغیر پذیر ہو جاتا ہے-  موت جب آ رہی ہو تو چاندی کے ٹھیکروں کا کیا مطلب رہ جاتا ہے؟ موت جب آ رہی رہی ہو تو بڑے محل بنا لینے سے کیا ہو جائے گا؟ موت جب آ رہی ہے، چل ہی پڑی ہے، رستے پر ہی ہے، کِسی بھی پل مِلن ہو جائے گا، تو دُشمنی، رنجش اور کدورت کا کیا مول؟</p>
<p>تُم اِس طرح جیتے ہو جیسے کبھی مرنا ہی نہیں- اِسی لئے تُم غلط جیتے ہو- اگر تُم اِس طرح جینے لگوکہ آج ہی موت ہو سکتی ہے، تُمہاری زِندگی سے غلطی رُخصت ہو جائے گی- غلطی کے لئے وقت درکار ہے- غلطی </p>




</body></text></cesDoc>