<cesDoc id="pun-w-panchim-02-gaana" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-gaana.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>


<p>گانّا -- مہندی</p>

<p>رب سوہنے دی مِہر نال </p>
<p>میری دِھی نایاب )گُڑیا( دے </p>
<p>18 مارچ 2000 نوں ہفتے دیہاڑ 8 وجے راتیں </p>
<p>گانّا بجھیسی تے مہندی لگسی- </p>
<p>ساڈا چائ اے کہ تُسیں</p>
<p> ایس خوشیاں بھرے ویلے نوں </p>
<p>رنگ لاون لئی ساڈے نال ہووو- </p>

<p>	چاہوان</p>

<p>	بلقیس رعنا مُظفر</p>
<p>تھاں: مُعظم منزل، محلہ کمنگراں</p>

<p>اُڈیکنہار</p>
<p>فائزہ رعنا، اقصا رعنا </p>




<p>سنجوگ</p>

<p>رب سوہنے دی مِہراے کہ</p>
<p>میری سپُتری نایاب )گُڑیا( دا ویاہ</p>
<p>19 مارچ 2000 نوں اِتوار دیہاڑے، 2 وجے دوپہریں</p>
<p>ملک عبدالوحید ہوراں دے سپُتر نُورالامین نال ہوسی،</p>
<p>تُہانوں ایس مُبارک ویلے آون دا</p>
<p>اُچیچا سدّا اے-</p>

<p>	چاہوان</p>

<p>	بلقیس رعنا مُظفر</p>

<p>سواگت جنج	:  1:30 وجے	</p>
<p>نکاح	: 2   وجے</p>
<p>وداعی	: 4   وجے</p>

<p>تھاں	: گورنمنٹ ڈِگری کالج برائے خواتین، چنیوٹ</p>

<p> اُڈیکنہار</p>
<p>دانش احمد مُکرم، فائزہ رعنا، اقصا رعنا ، مُعظم علی خاں</p>
<p>)مُعظم منزل، محلہ کمنگراں، چنیوٹ--- فون نمبر 332446 ( </p>



</body></text></cesDoc>