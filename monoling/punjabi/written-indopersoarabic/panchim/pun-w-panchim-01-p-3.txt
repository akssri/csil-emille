<cesDoc id="pun-w-panchim-01-p-3" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-01-p-3.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>رفاقت حُسین مُمتاز</p>

<p>وقت دا جیکر تقاضا ہور سی</p>
<p>ایدکیں اپنا ارادہ ہور سی</p>

<p>لَے گیا کھیسے مداری جھاڑ کے</p>
<p>ایدکیں اوہدا تماشا ہور سی</p>

<p>آ گیا وعدے تے اوہدا شُکریہ</p>
<p>میریاں سوچاں دا خدشہ ہور سی</p>

<p>کاٹھ دی روٹی تے سی اپنی گُزر</p>
<p>یار لوکاں دا ذریعہ ہور سی</p>

<p>نِگھ چِٹھی دا سی کچے ساہ جیہا</p>
<p>چِیکنے حرفاں دا لہجہ ہور سی</p>

<p>سچ اُتے سی رفاقتؔ فیر وی</p>
<p>فیصلہ پرہیا نے کیتا ہور سی</p>

</body></text></cesDoc>