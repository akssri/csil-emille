<cesDoc id="pun-w-panchim-02-ceremony" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-ceremony.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>	</p>
<p>	اِک پردیسی لکھیار دا تہانوں ساریاں نوں سلام۔</p>
<p>	</p>
<p>	میتھوں ماں بولی دی پُچھ  کوئی پَنجی ورھے پوند ہوئی سی۔ اک ونڈیں ایہہ گل نکی جیہی تے معمولی سی۔ وسر تے بھل وی سکدی سی۔  پر پچھن والے فاروقی ہوراں نیں جیویں پچھی سی اوس ونڈ یں  اہدا  نہ گولنا اک پاپ،  گناہ  تے وڈی تقصیر  بن سکدا  سی۔ ایس پھیر وچ ایہہ نری پچھ ای نئیں سی ۔ ایہہ اک گجھی تے  ڈاہڈی چوبھ سی جنھوں جرنا ،سہنا تے نبھانا  کسے ہاری ساری دا جگرا تے کم نئیں سی ۔اہنوں شاہدؔ ورگا لمے تے اُبھے  دا وسنیک ای سہ سکدا سی تے حالے تیکر سہندا پیا اے۔</p>
<p>	</p>
<p>	 پلیٹھی دی ایس پچھ توں لیکے ہُن تیکرجاندیاں تے انجاندیاں میں آپنے آپ نال  ،  آپنے  ٹردے پھردے اتے روز بدلدے آلے دوالے نال تے ہر ملن تے وچھڑن والے سکیاں تے اوبھڑاں دی سجان ،  بول بلاریاں دے چانن تے لشکاریاں نال  تے دیس پردیس دے سوہن سیاپیاں  ،  ولاویاںتے گھنجلاں نال ایسے پُچھ دی پرتیت کردا رہیاں  ۔  ویلے دے ایس لَمے پینڈے وچ کئی وارمریکل ڈنگراں وانگر بے تکے سر وی مارے۔ جنڈ دی کلم کلّی گھوگھی وانگر دل سوڑا وی پیا  تے آپنی جم بھوئنیں پنجاں پانیاں دی سنکھی دھرت توں اُدریواں وی جالیا۔پر  ایس پچھ دیاں پرتیتاں دی ایہہ لڑی حالے وی مکن دا ناں نئیں لیندی  ۔ سگوں انج  لگداے پئی ہنے ای تے ایس لڑی دا مڈھ بھنیجااے۔اک پھیریں دلاں دے اہناں روگاں لئی صحتوند  دھونی ماں بولی دی پنسار ہٹی توں ای ملسی ۔  جتھے  اکھراں دے ساوے پتر چھنگ چھنگا کے وی حالے تیکر ماں بولی دے لفظاں تے معنیاں دیاں جڑھاں کجّی کھلوتے نیں۔ کھبرے کنیاں مدتاں توں  سُچی پنجابی  بولن والے پُترگاہکاں  دا راہ  وی پئے ویہندے نیں۔</p>
<p>	</p>
<p>	 ایس پینڈے پیا  تے کلا  ساں پر انج لگدا سی پئی ایس  لمی سلمی  ڈونگھی واٹ  وچ  ہور بہوںسارے میرے سنگی تے ساتھی نیں۔ ہور کُجھ نئیں تے  دھرتی دے اوہناں وڈکے سچیار تھماں تے بابیاں دیاں یاداں جنہاں نیں پنجاب دیاں رتاں پنجاب  دی مٹی ہوا پانی پھل بوٹیاں دا انسان دے وجود ' بت تے روح نال ٹھیک ٹھیک جوڑ تے ایہناں سُچیاں جوڑاں دےاں ونڈاںتے سجاناں سکھائیاں سن  ۔ اوہ سجاناں جنہاں دے ورد نکے تے وڈے مسیتاں دی پھوڑیاں تے ، ڈیریاں دے وڈے تے کھلے پلنگاں تے تے کڑتل دیاں بنیاں رنگلے پاویاں دیاں منجیاں تے بہ بہ دن رات کردے رہندے سن تے حالے تیکر کر وی رہ نیں ۔ اوہناں سچیاراں  دا ساتھ تے ہر ویلے نالو نال ای سی۔ ایسے پاروں  اکھاں میٹ دل  دے سچے چانن وچ  انجھک تے نڈر ہو ایس راہیں ٹردا رہیاں۔</p>
<p>	</p>
<p>	ظاہر اکھاں کھول کے ایس پاروں نئیں سی ویکھدا پئی انج مینوں اوہناں وڈکیاں  دیاں بُنیاں ہویاں سُچیاں رِیتاں' رواج تے رویتاں اج دے نویں چانن وچ کمب کمب کے ٹُٹدے پرچھاویں ترَٹھے ڈنگراں وانگر شوکدے تے سہمے سہمے دِسدے سن۔ سَیّڑ وانگوں سوں کے اج دیاں  نویاں بولیاں دے کچھوکمیاں کولوں  ہر ہرا کے پچی ہوون دا ول تے دھروں کدے سکھیا  ای نئیں سی۔ جے کوئی سی تے  ڈر ایہو سی پئی کناں وچ تھل متھلیاں  دیاں رس بھریاں لوریاں گوندیاں سفنیاں وچ تھل دی ایسے چیکنی مِٹی تے رڑھدے چقند  گھوڑے جدوں میرے کول  آ کے اُچی ساری  ' تیرے وِیر دا ناں کیہ؟ ' پُچھن گے تے میں اوہناں نوں پرت کیہہ آکھساں۔ایہہ کہندیاں تے ڈُب نہ مرساں پئی ہُن میں کسے دا تیر پتریرپھپھیر ملیر تے مسیر نئیں ۔ میریاں اچیچیاں تے لہو توں ودھ گوڑہیاںایہہ سجاناں ہن  اِکو کزن وِچ گواچ  گیاں نیں۔ ایہو ای نئیں میرے مامے مامیاں، چاچے چاچیاں، تائے تائیاں، ماسڑ تے ماسیاں جیہے صاف سوہنے تے سنکھے رشتے وی انکل تے آنٹی دے درباری  چڑھاوے چڑھ گئے نیں۔ تے ایسے طرحاں ہورکنیاں ای سجاناں تے رشتیاں نوں اج 'قنونی' ٹھپیاں دی وی لوڑاں پئے گیاں نیں  ۔ جیویں کل تیکر اسیں بے قنونی رشتیاں تے رواجاں والے ساں۔</p>
<p>	</p>
<p>	ایہو جیہاں گویڑاں وچ ای ساں جاں پچھلے سال سچیت دے مقصود ثاقب ہوراں نال واہ پے گیا۔ایہہ وی اک کہانی نکلی۔ہویا انج سی۔ ایتھے اٹلانٹا وچ یاراں دے اک چھوٹے جئے اکٹھ وچ میں اک پنجابی 'گدّا' پڑھ بیٹھا۔ سنن والے پچھے پے گئے ' شاہد جی ! تہانوں پتہ نئیں پئی لہور وِچ مقصود ثاقب ہوری ماں بولی دی پھلاں بھری چنگیر چائی کھلوتے نیں۔ تسیں ضرور آپنیاں نظماں غزلاں تے پنجابی لوک گیتاں دے پھل تے کلیاں ایس چنگیر پاوو' ۔ ایہہ سن کے اک پاسے آپنی اوکات تے رونا وی آیا تے دوجے پاسے تسلی  وی ہوئی  ۔ رونا ایس گل دا پئی اج تیکر آپنی ماں بولی نوں ایس قابل نہ جانیا تاں جے گج وج کے اہدے ڈھول وجاندا۔ ڈھارس ایس گل تے ہوئی پئی حالے کجھ دل رکھن والے پنجابی دنیا تے باقی نیں۔ فر کیہ  پرانیاں 'گلاں دے پرچھاویں' ہور لمے ہو گئے۔ پہلی وار مقصود ہوراں نوں آپنی لکھتاں گھلیاں  :  حیران نہ ہووو  ،  ادب دے رشتے لکھتاں توں ای شروع ہوندے نیں  ۔  اوہناں میری تے میں اوہناں دی پھرول کیتی تے دوویں شیخوپوریے نکلے۔  مقصود ہوراں دے 'پنچم ' دی ڈیرے داری دی دس وی پے گئی ۔ ایس سانجھ تے سنگ لگن  دا رنگ ہُن تہاڈے ساہمنے جے۔ اہناں دے کہن تے اسیں پنجابی  بولی نوں سائینسی لکھت دا ذریعہ وی بنان دا آہر کر بیٹھے آں۔ </p>
<p>	</p>
<p>	اخیر تے ہن میں ایہہ آکھنا چاہنا ں ۔ اُنج تے ازلاں توں عرشیں ملدیاں بانگاں تخت لہور تیکر اپڑدیاں رہیاںنیں ۔ ویکھناں ایہہ اے پئی  ہن جدوں لہور تخت تے نئیں سگوں ہمیشاں وانگر لہور ، لہور ای اے ۔ کئی سمندراں پار فرشاں تے گھتیندی   گلاں دے پرچھاویاں دی   بُو  بُو تے  کوک  نوں ایہو  شہر لہور تے اہدی دھرتی دے ہور بھین  بھرا شہر پِنڈ تے وستیاں کیویں نجٹھدے نیں۔</p>

<p>					رب راکھا تے اللہ بےلی</p>
<p>								   پنجابی مان لکھیار</p>
<p>								  ڈاکٹر محمد افضل شاہدؔ</p>
<p>							         سنیل وِل ۔ جارجیا۔امریکہ</p>



</body></text></cesDoc>