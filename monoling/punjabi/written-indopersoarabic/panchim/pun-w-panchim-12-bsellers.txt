<cesDoc id="pun-w-panchim-12-bsellers" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-12-bsellers.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>میسرز اکبر لائبریری</p>
<p>بالمُقابل گورنمنٹ ڈِگری کالج برائے خواتین</p>
<p>کوہاٹ</p>

<p>چودھری محمد شریف آزاد</p>
<p>کوٹلہ ارب علی خان</p>
<p>ضلع گُجرات</p>

<p>میسرز پیر زادہ بُک سینتڑ</p>
<p>ائیر پورٹ روڈ</p>
<p>گِلگِت</p>

<p>میسرز شمع بُک ایجنسی</p>
<p>بیک روڈ</p>
<p>مردان</p>

<p>میسرز</p>
<p>اتحاد نیوز ایجنسی</p>
<p>ایبٹ آباد</p>

<p>میسرز بُخاری نیوز ایجنسی</p>
<p>نزد گرلز ہائی سکول</p>
<p>احمد پور شرقیہ</p>

<p>میسرز خالد بُک سٹال</p>
<p>مُسلم بازار</p>
<p>گُجرات</p>


<p>حاجی گُل بخش بخشالوی</p>
<p>بخشالوی پبلشرز اینڈ نیوز ایجنٹ</p>
<p>کھاریاں</p>

<p>میسرز بنگش بُک ڈپو</p>
<p>اُردُو بازار</p>
<p>سیالکوٹ شہر</p>

<p>میسرز راجہ برادرز</p>
<p>مُتصل مُسلم کمرشل بینک لِمیٹیڈ</p>
<p>رحیم بازار</p>
<p>ڈیرہ اسماعیل خان</p>

<p>میسرز</p>
<p> رحمت بُکسٹال اینڈ نیوز ایجنٹ</p>
<p>نزد ریلوے پُل</p>
<p>اوکاڑہ</p>

<p>میسرز ماڈرن بُک ڈِپو</p>
<p>عزیز شہید روڈ</p>
<p>سیالکوٹ</p>

<p>جناب فضل ربی راہی</p>
<p>شعیب سنز، ادھیانہ بازار</p>
<p>جی ٹی روڈ، مینگورہ--- سوات</p>

<p>میسرز آزاد بُک ڈِپو</p>
<p>313 - صدر بازار</p>
<p>حیدرآباد --- سِندھ</p>

<p>میسرز نقش کتاب گھر</p>
<p>480 - ڈی، سکیم نمبر 2 </p>
<p>سیٹلائٹ ٹائون</p>
<p>مِیر پور خاص، سِندھ</p>

<p>میسرز مکتبہ دانیال</p>
<p>وکٹوریہ چیمبرز  2 </p>
<p>عبداللہ ہارو روڈ--- کراچی 3 </p>

<p>مسعود اختر زیب صاحب</p>
<p>القائم بُک ڈپو، مین بازار</p>
<p>خوشاب</p>

<p>میسرز نیشنل نیوز ایجنسی</p>
<p>اسد چیمبرز، گرائونڈ فلور</p>
<p>ثمبو ناتھ روڈ، نزد پاسپورٹ آفس</p>
<p>صدر--- کراچی</p>

<p>میسرز رحمان نیوز ایجنسی</p>
<p>جنگی سٹریٹ</p>
<p>آئی/ ایس کابلی گیٹ</p>
<p>پشاور</p>

<p>میسرز اسلامی کُتب خانہ</p>
<p>جدید بازار </p>
<p>رحیم یار خاں</p>

<p>میسرز کریمی بُک ہائوس</p>
<p>4 - بالمقابل چاندنی سینما</p>
<p>حیدرآباد- سِندھ</p>
<p>میسرز مکتبہ ہمدانی</p>
<p>رام دِین بازار</p>
<p>جہلم</p>

<p>ملک تاج محمد صاحب</p>
<p>ملک نیوز ایجنسی</p>
<p>اخبار مارکیٹ--  راولپِنڈی</p>

<p>میسرز بیکن بُکس</p>
<p>گول باغ، گُلگشت کالونی</p>
<p>مُلتان</p>

<p>میسرز گوشئہ ادب</p>
<p>سرکلر روڈ</p>
<p>کوئٹہ</p>

<p>میسرز افضل نیوز ایجنسی</p>
<p>چوک یادگار</p>
<p>پشاور</p>

<p>جناب حاتم بھٹی </p>
<p>شمع بُک سٹال، بھوانہ بازار </p>
<p>فیصل آباد</p>

<p>میسرزگُلزار نیوز ایجنسی</p>
<p>اخبار مارکیٹ، ہسپتال روڈ</p>
<p>لاہور</p>



</body></text></cesDoc>