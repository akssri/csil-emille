<cesDoc id="pun-w-panchim-12-arzi" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-12-arzi.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>فائزہ</p>

<p>ہِک نظم</p>

<p>کھیچل تاں ہوسی</p>
<p>ہِک درخاست لِکھوینی اے باجی</p>
<p>چُوہڑی دی سامی اے</p>
<p>انگریزی سکول وِچ</p>
<p>گل اجیہی جوڑیا جے</p>
<p>کلیجہ پھڑیجے اگلیاں دا</p>
<p>دسیا جے</p>
<p>بالاں دے ڈِھڈ پیندی کھوہ</p>
<p>ماواں توں جھلنی اوکھی</p>
<p>تتی وائ وِچ سڑنی آں</p>
<p>پر اوہلا رکھنا پوسی</p>
<p>اوہلا رکھیا جے</p>
<p>بندہ میرا پیراں بھار کھلو نہیں سکدا</p>
<p>پُلس دا پِنجیا</p>
<p>بناوو کائی بات اچرج</p>
<p>مینوں سُوڈھدے وتن</p>
<p>مُل تے پینا تُہاڈے لِکھن دا</p>
<p>اسیں کیہ، ساڈی ہستی کیہ</p>
<p>دو گرانہیاں ٹُکر</p>
<p>تسلا کرایا جے میرے ولوں</p>
<p>کم کریساںحدوں ودھ</p>
<p>کائی گھاٹا کائی تھوڑ</p>
<p>نہ ہوسی محنت دی</p>
<p>سارے کم مُکا چلی آں</p>
<p>گند ہُونجھ کے پوچی لا چلی آں</p>
<p>آکھو تاں آٹا وی گُنھی جاواں باجی؟</p>
<p>…</p>



</body></text></cesDoc>