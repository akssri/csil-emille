<cesDoc id="pun-w-panchim-02-p-6" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-p-6.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>اختر فتح پوری</p>

<p>نعت</p>

<p>عقل فِکر دی کِیہ مجال اوتھے جیہڑا بخشیا رب مقام تینوں</p>
<p>اوہ جاندا اے صِرف شان تیری جِس بھیجیا درود سلام تینوں</p>
<p>اوس جا تے جھات نہیں پا سکدا بھاویں ولی ہووے بھاویں نبی ہووے</p>
<p>سڑن پَر جبریل دے اوس تھاویں جیہڑا بخشیا رب مقام تینوں</p>
<p>پھٹیا طُور تے موس'ی نے غش کھادا کوئی جھال نہ تیری جھلدا اے</p>
<p>سِینے چاک کردا پتھراں کالیاں دے جیہڑا بخشیا رب کلام تینوں</p>
<p>لبھے نسل کِتھے تیرے سنگیاں دی جیہناں مال تے جان نوں وار دِتا</p>
<p>گلے ہس کے موت دے جا لگے جدوں منیا اوہناں اِمام تینوں</p>
<p>بلی اگ وِچ حق پُکاردے رہے ہنجوں نال اوہ اگ نوں ٹھاردے رہے</p>
<p>جیہڑے موت توں زِندگی واردے رہے بخشے رب نے اوہ غُلام تینوں</p>
<p>دُنیا اُلجھناں دے وِچ پے گئی اے نہیں چُھٹدی جان مُصیبتاں توں</p>
<p>کرے دُور مُصیبتاں ساریاں نوں جیہڑا بخشیائے رب نظام تینوں</p>
<p>رُکھ سبھ جہان دے بنن قلماں بنے سیاہی سمندراں ساریاں دی</p>
<p>تیری مدح دا حق نہ ادا ہووے رب آکھیائے خیرالانام تینوں</p>
<p>شور کلر دے سپ دا مُونہہ چُماں کراں صُلح خونخوار بگھیاڑ دے نال</p>
<p>اوس نال نہ صُلح کراں اخترؔ جیہڑا بولدا اے بدکلام تینوں</p>
<p>۰۰۰</p>

</body></text></cesDoc>