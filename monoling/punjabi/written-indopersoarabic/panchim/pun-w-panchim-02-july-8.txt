<cesDoc id="pun-w-panchim-02-july-8" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-july-8.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>مارگریٹ گریوز</p>
<p>پنجابی رنگ: نادر علی</p>

<p>ہریاندھ</p>

<p>شالا اکلا بھی کوئی نہ ہووے --- اوہ وی کُومے جیہا- نہ کوئی اوہدا اگا نہ کوئی اوہدا پِچھا--- پیو ہوشیں توں پہلوں مر گیا ماں موئی تے دساں ورھیاں دا آہیا--- دساں مرلیاں دا اِک ٹُک آہیا--- پر اوتھے اُگے کوئی شے نہ--- ڈبھ اُگنی تے کنڈے- اِک کُومے دی بکری آہی اوہ شودھی سیالے وِچ بُھکھ نال مر گئی- کِسے ترس کھا کے لوڈھے ویلے روٹی دے چھوڑنی- مائی ہساں پیالہ لسی دا- ادھی روٹی اوس راتیں کھانی تے ادھی وڈے ویلے لسی نال-</p>
<p>پر پالے وِچ بُھکھ ودھیری لگنی- اِک دینہہ اوہ بیٹھا ڈبھ دے سُکے کھتے پیا رووے- اوس نوں لگا کوئی ہور بھی کول پیا روندا سی- ویکھیا تے ہرے رنگ دا اِک پری دا بچہ- پری اوس اکھیں تے نہیں سی ڈِٹھی پر سُنی سُنائی گل آہی پِنڈوں لہندے پریاں دی وستی دی- اوس نِکے جیہے بچے نوں تلی تے چا لیا- پریاں زباناں جاندیاں ہوئیاں:</p>
<p>''کون ایں وائی توں کون ایں؟</p>
<p>''میں تے بُھکھن بھانا ہویا تینوں کاہدا رون اے؟''</p>
<p>بچے بریک جیہی اواز وِچ کہیا:</p>
<p>''ہری پری دا بچہ ہاں ہریاندھ اے میرا ناں</p>
<p>شام انھیرا پے گیا اے  میں کلا کِتھے جاں''</p>
<p>کُومے کہیا، '' رات رہ جا میرے کول دینہہ چڑھے گھر ٹُرجائیں-''</p>
<p>''چھاہ ویلے سیوے دے ویلے آوسی میری ماں</p>
<p>ہرا میں راتیں باہر رہیا تے مر جاواں گا تھاں''</p>
<p>اِک پوہ دا مہینہ تے ہریاندھ اصلوں ٹھنڈا ٹھار جُوں ڈدھ دا بچہ ہووے!</p>
<p>کُومے اوہنوں روٹی دِتی- راتیں اگ بالی اِک جُلا پاٹا اُتے پایا تے ہریاندھ نوں ہوش آیا- ہُن اوہ ہسے تے گاویں:</p>
<p>'' اُتے ہووے نِگھی جُلی کُھل جان میرے انگ</p>
<p>کُومیا! من موہنیا! جو منگنا ای اج منگ!''</p>
<p>کُوما ہسیا، ''میرے ٹُک وِچ ککھ نہیں اُگدا- ڈبھ اے تے سُول نیں- مینوں ہاڑی چھولے دے- نالے ہولاں چباں دا- نالے گرمیاں وِچ بُھنا کے کھاں دا!''</p>
<p>ہریاندھ وڈے ویلے ماں نوں دسیا کُومے کِنج میری جان بچائی اے- ہری پری کول ہر شے دا بِی سی- اوس کہیا ، ''چھولے پچھیترے نیں پر ہو ویسن-''</p>
<p>''پوہ نہ پون کَکر تے مانہہ نہ وسے مینہہ</p>
<p>چھولے تیرے اُگسن تیرا ہری پری نال نینہہ!</p>
<p>دساں مرلیاں وِچ اینے چھولے ہوئے پئی سانبھے نہ جان-</p>
<p>اگلے سال مُڑ ساونی وِچ کُومے ہر شے دا بِی پایاپر اُگی کوئی شے - مُڑ ڈبھ دی ڈبھ! پری دُوجی واری کدے نہیں آوندی پر کُوما اینا رویا، نالے اوس گاویا :</p>
<p>پوہ نہ پون ککر تے مانہہ نہ وسے مینہہ</p>
<p>چھولے تیرے اُگسن تیرا ہری پری نال نینہہ!</p>
<p>بریک جیہی ہسن دی اواز آئی-</p>
<p>''کیہ لایا ای کُومیا؟ کوئی رُت دا لا توں بی!</p>
<p>پوہ نہ لگن مُنجیاں تے کنک نہ ساون جی!</p>
<p>کُومے ہریاندھ تے ہری پری نے سارے بے موسمے بِی چُنے---  مُڑ چھولے لائے--- ہاڑ آیا تے مُنجی دی پنیری لائی--- ساریاں فصلاں رج ہوئیاں اوس دو وِگھے حِصے تے بھی لائے-</p>
<p>''جِس ویلے سِر بیجیا اُسدا بنیا ڈھو!</p>
<p>ہاڑی پکن ہاڑِیاں تے ساونی کتیں ہو!''</p>
<p>ہُن ہر فصل اوس رُت نال لانی---  مائی ہسی اوس نال سِیر پایا---  ہری پری نوں اوس ہر فصل تے یاد کرنا تے چِڑیاں نوں دانہ پانا- آہدے نیں چِڑی پری دی بھین اے- رب دِیاں رب جانے- کُوما کہے پری ویکھی اے تے کون منے گا---  ٹی وی نے لوکاں دی مت  مار چھوڑی اے!</p>
<p>v</p>














</body></text></cesDoc>