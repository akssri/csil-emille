<cesDoc id="pun-w-panchim-02-p-5" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-p-5.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>راجہ صادق اللہ</p>

<p>گاون لُون پہاڑ دا</p>
<p>)چَھند مِرزا صاحباں(</p>

<p>اسی پُتر لُون پہاڑ دے ہر چڑھدی لام چڑھے</p>
<p>نِت چوآ اتھرو کیردا پُت جا پردیس مرے</p>

<p>اسی مِٹھے ڈھولے گاوندے تے کھارے ساڈے کُھوہ</p>
<p>اِتھ رکڑ تَسیاں کھیتیاں مِینہہ ورھدا وگدی جُوہ</p>

<p>اسی ہالی چھیڑو آجڑی اسی کاناں دے مزدُور</p>
<p>اسی نوکر ہر سرکار دے اسی ہُھکھ ہتھوں مجبور</p>

<p>ایہہ دھرتی سانوں وانجدی تے ماواں سانوں رون</p>
<p>کِت عُمراں پِچھوں پرتیے تے سنگی پُچھن کون</p>

<p>وت ست گھرے دیاں پھیریاں سانوں گنگا راج کٹاس</p>
<p>اسی جم پل دھرتی اوس دے جیہدے پُت کٹدے بَن باس</p>

<p>اسی مالی کامے اردلی اسی قاتل تے مقتول</p>
<p>سانوں دوش نہ دینا دوستو ساڈا ساہ سینے وِچ سُول</p>

<p>اسی رَتّ حدّاں تے ڈولھدے مردے پار اُرار</p>
<p>ساڈے لہو دے دِیوے بالدے نِت حاجی شیخ کراڑ</p>

<p>اسی بالن اوس تندور دا جیہڑا منگے پُتر چھوہر</p>
<p>اسی جم پل لُون پہاڑ دے تے کرگل ساڈی گور</p>

<p>چن چمکے کلر کہار تے نِت چہکے چِت چکور</p>
<p>اسی کالے پانی کٹیے گھر پَیلاں پاوندے مور</p>

<p>سانوں یاد ستاوندی نِم دی جیہدی ٹیشی بیٹھے بَھور</p>
<p>اساں مار اُڈاری اُڈنا تے ہونا ہور دا ہور</p>

<p>اسی پُتر لُون پہاڑ دے...............................</p>









<p>شہزاد قیصر</p>

<p>تِن کافیاں</p>

<p>1 </p>
<p>بندیا بُھل نہ وچن الستی، بندیا بُھل نہ وچن الستی</p>

<p>توں نہ کھاویں بُھل بُھلیکھا،جِیں توں چاہیں بَندا لیکھا</p>
<p>اوڑک لیکھ وی بَندا دستی، بندیا بُھل نہ وچن الستی</p>

<p>رب باجھوں جگ کُوڑ اجایا،اصلوں ہر شے تھیوے مایا</p>
<p>بھاویں ہوئے اُچیائی پستی،بندیا بُھل نہ وچن الستی</p>

<p>بندیا رب نوں راضی کر لَے، ویلا تھوڑا بُہتا کم لَے</p>
<p>اندر باہر چھاوے مستی، بندیا بُھل نہ وچن الستی</p>

<p>مِیراںؔ نوں مُڑ چیتا آیا، سجدے دے وِچ یار منایا</p>
<p>اِکو تھیوے نیستی ہستی، بندیا بُھل نہ وچن الستی</p>



<p>2 </p>

<p>عِشق فقر دے بول انمول،عِشق فقر دے بول انمول</p>

<p>عِشقوں اُٹھدے حُجب امکان، تیڈے اندر زمیں اسمان</p>
<p>سُن دے بُوہے باریاں کھول، عِشق فقر دے بول انمول</p>

<p>اڑیا رب دی سیوا پُوجا، اصلوں غیر نہ تیجا دُوجا</p>
<p>عِشق فقر تیں کرپرچول، عِشق فقر دے بول انمول</p>

<p>بندیا مُرشد دے لڑ لگیں، غیب الغیب خزانے ٹھگیں</p>
<p>اوتھے نہ کوئی تکڑی تول، عِشق فقر دے بول انمول</p>

<p>ظاہر دے وِچ باطن لوڑیں، ہیرے تھیسن لکھ کروڑیں</p>
<p>مِیراںؔ ہِک ہِک نواں نرول، عِشق فقر دے بول انمول</p>







<p>3 </p>

<p>اڑیا دم دم پاویں جھات، تیری چمکے اصلی دھات</p>

<p>ہر سُو لکھاں ویری اڑیا، حِرص طمعے نے گُجھا پھڑیا</p>
<p>تیرے اندر باہر گھات، اڑیا دم دم پاویں جھات</p>

<p>چھڈ ویری توں سبق پڑھنا، ہونی انہونی توں ڈرنا</p>
<p>دیویں خوف حُزن نوں مات، اڑیا دم دم پاویں جھات</p>

<p>خوابے دے چِ گُناہ ثواب، اِتّھاں جاگ دے مول حساب</p>
<p>مُرشد باجھ نہ بندی بات، اڑیا دم دم پاویں جھات</p>

<p>عِشقوں بندہ جھاتی پاوے، اندر باہر نظریں آوے</p>
<p>مِیراںؔ سجن سنگ دِن رات، اڑیا دم دم پاویں جھات</p>
<p>۰۰۰</p>









</body></text></cesDoc>