<cesDoc id="pun-w-panchim-02-pan-j" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-pan-j.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>

<p>احسان باجوہ</p>

<p>کُھوہ دے انگ</p>

<p>	کُھوہ پنجاب وسیب اندر اِک اُچیری تھاں رکھدا اے- جدوں توں اِنسان نے ایس بھاگ وتی دھرتی اُتے اکھ کھولی اے اودوں توں لے اوہ آپنی تے آپنے ڈنگر وچھے دی تریہہ مکاون لئی پانی دی ورتوں کردا آ رہیا اے- واہی بِیجی ول اُلار توں مگروں اوہنوں آپنی فصل واڑی دی تریہہ بُجھاون لئی پانی دے قُدرتی وسیلیاں مِینہہ تے ندی نالیاں دے نال نال کُھوہ کھٹن دی لوڑ پئی-</p>
<p>	پُوری دُنیا دے ادب اندر کُھوہ دا ذِکر اذکار مِلدا اے- کنعان وستی دے وسنیک حضرت یوسفؑ ہوراں نوں اوہناں دے بھراواں وَیر پاروں کُھوہے سُٹ دِتا- بابل شہر دے اوس کھوہ دا ناں قرآن اندر موجود اے، جیہدے وِچ دونہہ فرشتیاں نوں قیامت تیک تاڑ دِتا گیا - حضرت موس'یؑ دا کھوہ نال وی واہ پیا اے- پنجاب اندر سیالکوٹ شہر دے نیڑے پِنڈ کرول دے اُکا نیڑے پُورن دا کُھوہ اج وی موجود اے جِتھے راجے سلوان دے پُتر پُورن دے ہتھ پَیر وڈھ کے اوہنوں سٹیا گیا- ایسراں ای تاریخ دے پنیاں اُتے ہور کئی تریخی کھُوہواں دا ذِکر اذکار مِلدا اے-</p>
<p>	کُھوہ دی موجودہ شکل آپنے کئی سمیاں دے پندھ مگروںبنی اے- کُھوہ نے اِنسان نوں حیاتی دِتی اے- دھرتی دی سُندرتا نوں ودھیرا کیتا اے- کُھوہ نے رکڑ بنجر دھرتی ماں نوں سُوہے ساوے تےبسنتی چولے پوا دِتے نیں- اج دی سائنس نے بھاویں کُھوہ دی اجوکی شکل وِچ ٹیوب ویل تے ٹربائن دا رُوپ دھار لیا اے پر آپنے کل نال سانجھ رکھنا تے آپنے کل نوں گولی رکھنا ویلے دی لوڑ اے- ہر اِنسان دا آپنے کل نال رِشتہ قائم رہنا ضروری اے-</p>
<p>	کُھوہ نے آپنے ارتقائی سفر چوں لنگھدیاں آپنے کِنے ای ناں رکھوائے جیہڑے تھلے لِکھے جا رہے نیں:</p>
<p>	1 - وڈا کُھوہ:- دونہہ ہلٹاں والا کُھوہ جیہدے اگے دو ماہلاں چلدیاں سن- ایہہ اجیہیاں تھاواں اُتے بنائے جاندے سن جِتھے بھوئیں دے تلے وِچ پانی گھٹ ہوندا سی-</p>
<p>	2 - پُنیالا کُھوہ:- پانی دے پتن تائیں بور کر کے بنایا اجیہا کُھوہ جیہدے چوں پانی آپنے آپ باہر نِکلدا رہندا اے- اجیہے کُھوہ اج وی ضلع نارووال دی تحصیل شکرگڑھ دے اُتر پہاڑ وِچ جموں دے نیڑے وسدے پِنڈ سُکھو چک تے آلے دوالے دے پِنڈاں وِچ موجود نیں- اوس وسوں وِچ اجیہے ٹیو ویل وی ویکھن نوں لبھدے نیں جیہناں دی نال چوں اٹھے پہر آپے ای پانی وگدا رہندا اے-</p>
<p>	3 - کُھوہی:- پِنڈاں وِچ پِین والے پانی کڈھن لئی بنائی جاندی اے- ایہدا قُطر کُھوہ توں بڑا گھٹ ہوندا اے تے ایہدے اُتے چرکھڑی راہیں بوکا لمکا کے ورتوں دا پانی کڈھیا جاندا اے-</p>
<p>	4 -بوڑا کُھوہ:- اوہ جیہدے اُتوں کنڈھے ڈھٹھے ہوئے ہون- ایہہ بالاں دی اِک کھیڈ وی اے جیہدے وِچ دو دو گیٹیاں نال دو دو بال کھیڈدے نیں-</p>
<p>	5 - کچا کُھوہ- پکیاں زمیناں والے علاقے اندر کھٹیا گیا اجیہا کُھوہ جیہدے اندر اِٹاں دی اُساری نہیں کیتی جاندی-</p>
<p>	6 - انھا کُھوہ:- اجیہا کُھوہ جیہدا پانی ڈُونگھا ہووے تے اُتوں ویکھیاں تارے وانگوں نظریں آوے تے اوہ کُھوہ ان وگدا ہووے-</p>
<p>	7 - ٹیوبی کُھوہ- جیہدے اندر بور کر کے پانی اُتانہہ لیان لئی پائپ جاں ٹیوب وگائی گئی ہووے- </p>
<p>	8 - کھُوہٹا:- نِکا کُھوہ</p>
<p>	 کُھوڑا:- نِکا کُھوہ</p>
<p>	کُھوہڑی:- نِکی کُھوہی</p>
<p>	موٹا کُھوہ:- جیہدے اُتے کانجن نہیں پائی جاندی-</p>
<p>	کُھوہ دی اُساری پِچھوں اوہدے چوں پانی کڈھن لئی اِک پُورا سِسٹم تیار کیتا جاندا اے تے اوس سِسٹم دے ناں ایہہ نیں-</p>
<p>	گادھی: ست اٹھ فُٹ لمی اِک اجیہی لکڑ ہوندی اے جیہدے پواند والے پاسے دو پھانگ ہووے- دو پھانگ دے دونہہ سِریاں دے اخیر اُتے دونہاں سِریاں نوں مِلان لئی اِک لکڑ لا دِتی جاندی اے- اِنج تِن نُکری بیٹھن والی تھاں بن جاندی اے- اوس تھاں نوں پرالی دے کھبڑاں نال اُن لیا جاندا اے- گادھی دا سرہاند والا حِصہ ڈھول اُتے رکھ کے ڈھول دے ہباریاں نال نکلے جاں تیر دے اگاڑی پچھاڑی رسیاں نال نرڑ کے بنھ دِتا جاندا اے-</p>
<p>	تِیر/ترکلا: تیر اوس لٹھ نوں آکھدے نیں جو دو ڈھائی اِنج قُطر موٹے لوہے دا بنیا ہوندا اے- ڈھول دے سینٹر وِچ ہباریاں دے جوڑ وِچکار بنائے سوراخ اندر ٹھوکیا جاندا اے- تیر دا پِٹھلا حِصہ بھرونی وِچ بنی چُوتھی اندر رکھیا جاندا اے جدوں کہ اوہدا اُتلا حِصہ کانجن وِچ بنی چُوتھی وِچ پھسا دِتا جاندا اے جاں کانجن دے اِک پاسے تیر دی موٹائی برابر گول پتری دے اندر رکھ کے پتری نوں دوہیں ولّیں کانجن وِچ لمے کِل ٹھوک دِتے جاندے نیں جو اوہ تیر تے ڈھول دے بھار نوں سہار سکے-</p>
<p>	ڈھول: جیویں کہ ناں ای پیا دسدا اے پئی کوئی گول شے اے- ایہہ تیر دے وِچ ٹُھکیا ہوندا اے- ایہدا قُطر ساڈھے تِن چار فُٹ ہوندا اے- ایہدے وِچ لوہے دِیاں دو موٹیاں پتریاں نوں گولائی وِچ جوڑ کے ہیٹھلی پتری نوں اُتلی نال جوڑ دِتا جاندا اے تے دوہاں گول پتریاں نوں آپو وِچ جوڑدیاں نیں- سوا اِنچ دی واٹ تے دوہاں پتریاں نوں لگیاں ہوئیاں ادھی اِنچ موٹیاں لوہے دِیاں کِلیاں جیہناں نوں کُبّے آکھیا جاندا اے- باہرلے دوہاں موٹیاں پتریاں چوں اُتلے ہیٹھلے پترے نالوں چار ہور پترے چونہواں پاسیاں توں اندر تیر ول نوں آوندے نیں- اوہناں نوں ہبارے آکھیا ویندا اے- ڈھول دا کم چرکھڑی نوں گیڑن ہوندا اے-</p>
<p>	چرکھڑی: ڈھول نالوں تِن چوتھائی حِصہ گھٹ قُطر دا اِک گول چکر جیہڑا اِکو موٹی پتری دا گول کر کے بنایا جاندا اے تے ایس پترے وِچ ڈھائی ڈھائی اِنچ دِیاں کِلیاں ٹُھکیاں ہوندیاں نیں جیہناں نوں ''بُوڑئیے'' آکھیا جاندا اے- چرکھڑی نوں کُھلیاں کر کے ایہدے بوڑئیے ڈھول دے کُبّیاں وِچ پھسا دِتا جاندا اے- چرکھڑی دا ادھا حِصہ زمین وِچ جھری مار کے اوہدے وِچ رکھیا جاندا اے تے اوہدا سینٹر بھوئیں دے برابر رکھیا جاندا اے- ایہدے وِچ اُنج ای چار ہبارے دُھر وِچکار آ کے رلدے نیں تے اوتھے اِک گولائی بنائی جاندی اے- ایس گولائی وِچ لٹھ نوں پھسا دِتا جاندا اے تے وِچ اِک کُنجی ٹھوک دِتی جاندی اے تاں جو چرکھڑی چوں لٹھ تِلکے نہ- جد گادھی اگے ڈنگر جوئے جاندے نیں تاں گادھی ڈھول نوں گیڑدی اے- ڈھول دے کُبے چرکھڑی دے بُوڑیاں وِچ پھسدے اگے ٹُردے نیں تے اِنج چرکھڑی گِڑ کے پُورے کُھوہ نوں گیڑدی اے- ایہدے بارے اِک لوک گیت دا ٹوٹا ایہہ آکھدا اے</p>
<p>کُبے بوڑئیے اِنج مِلدے جیویں بھیناں مِلدے وِیر </p>
<p>لٹھ پلانے اِنج پھردی جیویں سپ پِھرے دِلگِیر</p>
<p>	لٹھ: ایہہ وی تیر جِنے گھیرے دی لوہے دی بنی ہوندی اے- ایہدا اِک سِرا چرکھڑی چوں لنگھا کے بھرّونی دی چُوتھی وِچ پھسا دِتا جاندا اے تے دوجا سِرا پلانے اُتے رکھ کے کُھوہ دے وِچ لمکے بائڑ وِچ ٹھوک کے اوہدا اگلا سِرا کُھوہ تے پئی لکڑ دی چُوتھی وِچ پھسا دِتا جاندا اے- ایہہ لٹھ باراں تیرھاں فُٹ لمی ہوندی اے- سارے کُھوہ نوں گیڑن دا سربندھ ایہہ لٹھ ای کردی اے ایسے پاروں آکھیا جاندا فلانا اِنج پکا پِیڈھا اے جیویں لوہے دی لٹھ ہووے-</p>
<p>	بائڑ: ایہہ وی ڈھول تے چرکھڑی وانگوں گول شکل دا بنیا ہوندا اے پر ایہدا گھیر ڈھول توں دُونا ہوندا اے- ایہنوں وی کھلہا کر کے جھلن دے اُتے پلانے دے وِچکار کُھوہ وِچ لمکا دِتا جاندا اے- لٹھ نوں بائڑ دے ہباریاں وِچ بنی گولائی وِچ ٹھوک دِتا جاندا اے- بائڑ دونہہ موٹیاں پتریاں دا بنیا ہوندا اے تے اوہدیاں دوہاں پتریاں وِچکار گھٹ ودھ اِک فُٹ دی واٹ رکھی جاندی اے- بائڑ دِیاں دوواں پتریاں وِچکار گِٹھ گِٹھ دی واٹ اُتے پھلھے لدے جاندے نیں- ایہہ سرئیے دا چھ ست سُوتر دا اِک ٹوٹا ہوندا اے- ایہہ پھلھے بائڑ دے دونہہ پاسیاں دے پتریاں وِچ موریاں کر کے جھنڈاں کر کے جوڑ دِتے جاندے نیں- پُرانے ویلیاں وِچ ایہہ لکڑ دِے بندے سن- </p>
<p>	چپے: ایہہ ہتھ دے چپے جِنی پتری ہوندی اے جیہنوں ہر پھلھے دے اُتے ربٹاں راہیں جوڑ دِتا جاندا اے- ایہہ چپے پھلھیاں اُتے اگلے پاسے نوں اُلار وِچ لدے جاندے نیں- ایہناں نوں جوڑن دا کارن ماہل نوں تِلکن توں ڈکنا ہوندا اے-</p>
<p>	ماہل:- ٹِنڈاں نوں جوڑ کے بائڑ دے اُتے پا کے کُھوہ وِچ لمکا دِتا جاندا اے- جے پانی نیڑے ہووے تاں ماہل وِچ گھٹ ٹِنڈاں جوڑیاں جاندیاں نیں پر پانی نِیواں ہوون کارن ودھ ٹِنڈاں جوڑنیاں پَیندیاں نیں-</p>
<p>	تکہ/کُتا:لوہے دا بنیا کوئی فُٹ لما اِک پترا جیہنوں چرکھڑی دے نال اوس پاسے لایا جاندا اے جِدھروں چرکھڑی ہیٹھوں اُتانہہ نوں پِھردی اے- تُکے دا اگلے پاسے مُونہہ اگے دندا بنایا جاندا اے- جدوں چرکھڑی اُتانہہ نوں جاندی اے تاں تُکا ہر بوڑئیے نال وجدا اے تے ٹن دی آواز آوندی اے- کُھوہ سِدھا گِڑے تاں تُکا پرت پرت کے بُوڑیاں اُتے وجدا اے پر جدوں کُھوہ کھلہار دِتا جاوے تاں تُکا کُھوہ نوں پِچھانہہ نوں پُٹھانہیں گِڑن دیندا- جے تُکا نہ ہووے تاں کُھوہ کھلہارن سمے تیزی نال پُٹھا گِڑدا اے تے اِنج کوئی نُقصان )جان( وی ہو سکدا اے- کیوں جو پانی دِیاں بھریاں ٹِنڈاں دا بھار کُوہ نوں پُٹھیاں گیڑ دیندا اے- ایس کم توں تُکا ڈکدا اے- تُکے دا دُوجا سِرا قبضے دی شکل وِچ زمین تے لگی پھٹی اُتے لا کے پھٹی وِچ لمے کِل ٹھوک دِتے جاندے نیں تاں جو تُکا اگانہہ پِچھانہہ نہ سرکے-</p>
<p>	بھرّونی: ایہہ موٹی لکڑ ہوندی اے عام طور تے پکی لکڑ- ایہہ ڈھول دے تھلے اُتے چرکھڑی دے نال زمین وِچ گڈی ہوندی اے- ایہدے وِچ دو چُوتھیاں ہوندیاں نیں اِک تاں تِیر دا سِرا اڑُنگن لئی تے دُوجی چُوتھی وِچ لٹھ دا سِرا پھسایا جاندا اے- ایہہ لٹھ تے تیر نوں سہارا دین لئی لائی جاندی اے-</p>
<p>	پلانا: ڈھول دے آلے دوالے گھٹ ودھ چھ فُٹ گھیر دا گول چوپھیرا جیہدے وِچ جوگ )گادھی اگے جُپے ڈنگراں دی جوڑی( گُھمدی اے-</p>
<p>	اریڑی: ٹِنڈاں نوں جوڑن والا اٹھ کُو اِنچ لما کِل جو دونہہ ٹِنڈاں نوں آپو وِچ جوڑدا اے تے ٹِنڈاں دے دوہیں پاسیں لائیاں پتریاں دِیاں موریاں چوں لنگھا کے اریڑی نوں اِنج دی جھنڈ کیتی جاندی اے کہ پتریاں ہِل جُل سکن-</p>
<p>	لر: ٹِنڈاں دے دونہہ ولیں لگیاں پتریاں جو اریڑی راہیں ٹِنڈاں نوں آپو وِچ جوڑدیاں نیں- دو لر ٹِنڈ دے سجے کھبے ربٹاں نال اِنج لائے جاندے نیں پئی ٹِنڈ دے سجے کھبے لان مگروں اوہ ٹِنڈ دے اگے پِچھے ڈیڈھ ڈیڈھ اِنچ ودھے ہوئے ہوون-</p>
<p>	ٹِنڈ: کُھوہ چوں پانی کڈھن لئی بنائی جاندی اے- پُرانے وقتاں وِچ ایہہ مِٹی دی بنائی جاندی سی تے مُنج دِیاں وریاں نال ماہل دی شکل وِچ بنھی جاندی سی- فیر لوہے دِیاں ٹِنڈاں بنن لگ پئیاں ایہدے تِن پاسے چورس تے اُتلا نِیم گول ہوندا اے- ماہل وِچ ایہناں نوں اِنج پرویا جاندا اے پئی ایہناں سبھناں دا مُونہہ اِک پاسے نوں ہوندا اے- مِٹی دِیاں ٹِنڈاں نوں لوٹے وی آکھیا جاندا سی- </p>
<p>	پاڑچھا: ایہہ بائڑ دے تھلے اِنج رکھیا جاندا کہ جدوں کُھوہ وگدا اے تاں ٹِنڈاں چوں پانی ایہدے وِچ ڈُلھدا رہوے- ایہہ لوہے دی پتری دا کوئی ساڈھے چار پنج فُٹ لما تے دو کُو فُٹ چوڑا ہوندا اے تے ایہنوں جلھن اُتے ٹکایا جاندا اے- فُٹ کُو اُچا ہوندا -</p>
<p>	نسار: پاڑچھے دے نال جُڑی ہوئی ہوندی اے تے ایہہ وی پتری دی بنائی جاندی اے- ایہہ چار اِنچ اُچی چھ اِنچ چوڑی تے چھ ست فُٹ لمی اِک آڈ ورگی ہوندی اے- پاڑچھے چوں نِکل کے کُھوہ دے اُتوں دی اولُو جاں چوبچے تیکر اپڑدی اے- پاڑچھے چوں پانی نسار راہیں پے کے اولُو جاں چوبچے تیکر اپڑدا اے- اگوں پانی اولُو چوں آڈ راہیں پے کے پیلیاں تیکر اپڑدا اے-</p>
<p>	چنّے: مِٹی جاں اِٹاں دے بنائے جاندے نیں- پلانے دے دولے باہر وار کوئی اٹھ فُٹ دی اُچائی تیکر بنائے جاندے نیں- ایہہ دو ہوندے نیں تے ایہناں اُتے کانجن پائی جاندی اے-</p>
<p>	کانجن: ستاراں اٹھاراں فُٹ دی کوئی فُٹ ڈیڈھ قُطر دی پکی لکڑ ہوندی اے، جیہنوں پلانے دے دوّلے بنے چنّیاں اُتے ٹِکا دِتا جاندا اے- ایہدے وِچکار ڈھول چوں اُتانہہ گئے تیر نوں ایہدے اندر بنی چُوتھی وِچ پھسا دِتا جاندا اے- کانجن کُھوہ دے ڈھول نوں جو رکھدی اے-</p>
<p>	جلھن: نَو فُٹ لمی تے ڈیڈھ کُو فُٹ موٹائی دی لکڑ ہوندی اے جیہنوں کُھوہ دے دونہہ کنڈھیاں اندر ٹِکا دِتا جاندا اے- ایہدے اُتے لٹھ دی چُوتھی بنائی جاندی اے تے پاڑچھا وی ایہدے اُتے ای رکھیا جاندا اے- ایہہ وی کِسے پکی لکڑ دی بنائی جاندی اے-</p>
<p>	کوٹھی: کُھوہ کھٹن مگروں ایہدے وِچ گولائی وِچ کسی جان والی اُساری نوں کوٹھی آکھدے نیں- ایہہ ڈیڈھ اِٹ دی چنائی دی پرکار رکھ کے گول بنائی جاندی اے- ایہدے تھلے لکڑی دا چک بنا کے رکھیا جاندا اے تے چک دی گولائی دے اُتے کوٹھی دی اُساری کیتی جاندی اے-</p>
<p>	چک: ڈِنگیاں لکڑاں دے موٹے پھٹے چِیر کے بنایا جاندا اے- کوئی چار اِنچ موٹے پھٹے چِیر کے اوہناں نوں جوڑ جوڑ کے کُھوہ دی گولائی دے مُطابق بنایا جاندا اے- ایہہ وِچوں اُکا خالی ہوندا اے- ایہدے وِچ ار وغیرہ کوئی نہیں ہوندا- چک نوں گِلی مِٹی اُتے پدھرا رکھ کے اوہدے اُتے اُساری کرن لگ پیندے نیں تے کُجھ اُساری کرن مگروں نال ای چانبھ نال کُھوہ دی ٹوبھی کردے نیں- مِٹی ریت جیوں جیوں باہر نِکلدی اے تاں چک تے کوٹھی دی اُساری تھلے ٹُردے نیں- جدوں اُساری پانی نیڑے اپڑدی اے تاں مُڑ ہور اُساری کر لئی جاندی اے جِتھے کر کے پُوری ریت پے جاوے تے ٹوبھا سمجھ لوے پئی پانی وی پُورا ہو گیا اے اوتھے ٹوبھی بند کر کے کوٹھی دی اُساری پُوری کر لیندے نیں- کوٹھی دی اُساری عام طور اُتے زمین دے لیول تیکر ای کیتی جاندی اے- کئی تھانواں تے ہڑھاں والیاں علاقیاں وِچ ایہہ بھوئیں توں من مرضی نال اُچیری وی کر لئی جاندی اے تاں جو ہائیں مائیں ہڑھ دا پانی کُھوہ وِچ پے کے اوہنوں پُور نہ دیوے-</p>
<p>	چانبھ: اِک وڈی ساری کہی جیہدا دستہ عام کہی نالوں نِکا پر اوہدا پھل عام کہی نالوں کوئی چھ گُنا ہوندا اے- اوہدے پین نوں رسہ پا کے اوہدے دستے نال بنھ دِتا جاندا اے- اوس رسے نوں لڑ آکھدے نیں- اوس لڑ نوں اِک ہور موٹے رسے نال گنڈھیا جاندا اے تے اوس رسے نوں کُھوہ دی من تے کھلوتے بندے رل کے اودوں کِھچدے نیں جدوں ٹوبھا پانی وِچ ٹُبھی مار کے ٹوبھی کرن پانی وِچ تھلے جا کے اوس چانبھ نوں ریت وِچ کھوبھ دیندا اے تے فیر لانبھے ہو کے پانی دے اندروں ای لما ساہ لیندا اے تاں پانی وِچ بُڑبُڑیاں وجدیاں ویکھ کے کُھوہ دی من اُتے کھلوتے بندے رل کے اوس رسے نوں اُتانہہ کِھچدے نیں- اِنج مِٹی تے ریت ٹبھن دا کم پُورا کیتا جاندا اے- ٹوبھی کرن والے بندے نوں ٹوبھا آکھدے نیں-</p>
<p>	گھاری: کُھوہ دی کوٹھی عام طور تے تاں نہیں یرکدی پر میرا زمیناں وِچ جے کِسے پاسیوں زمین دے اندر رئون پے جائے تے اوہ رئون کُھوہ دے نیڑے ہووے تاں اوہ رئون وِچو وِچ رئون دے پپانی نوں کُھوہ دی کوٹھی تیکر لے جاندی اے- باہر وار پانی کوٹھی دِیاں اِٹاں اندروں مِٹی کھور کے کڈھ دیندا اے تے فیر ہولی ہولی کوٹھی دِیاں کُجھ اِٹاں اندر کُھوہ وِچ ڈِگ پیندیاں نیں- کوٹھی وِچ مگھارا ہو جاندا اے- اوہنوں گھاری آکھدے نیں- گھاری نوں بند وی کیتا جا سکدا اے-</p>

<p>	کُھوہاں دا سارا جگاڑ پُرانے ویلیاں وِچ لکڑ دا ہوندا سی ڈھول چرکھڑی تے بائڑ وی لکڑی دے ہوندے سن- تھوڑے ورھے اگدوں حسن ابدال توں لہندے پہاڑ دی گُٹھے پہاڑاں ول نوں پیدل جاندیاں ہویاں میں لکڑ دے ڈھول چرکھڑی والا اِک کُھوہ وگدا ہویا ویکھیا سی تے اوہدے اُتے مِٹی دِیاں ٹِنڈاں دے تھاں وریاں نال ٹین دے ڈالڈے والے ڈبے بدھے ہوئے سن پر اودن کیمرہ میرے کول نہیں سی نہیں تاں اوہدی فوٹو بنا لینا بڑا ضروری سی-		</p>
<p>ASD</p>















<p>پنجاب:دوآبیاں تے شہراں دا حال</p>
<p>3 </p>

<p>حاجی پور ار بُڈھاوڑ</p>
<p>	پاہو پاہ پنجاں کوہاں دے سنھ پُر ہن- تِناں وِچ چار یا پنج پنج سو گھر اتے سو کُو ہٹ بسدی ہے- اتے بیاہ ندی تے اِک وڈا نالا پُھٹکے بُڈھے وڑ دی بسوں وِچی چلدا ہے- اوس تھیں چھوٹیاں چھوٹیاں کوہلاں کھٹ کے اوس گِردے دی زمین وِچ چلائیاں ہوئیاں ہن- جد کدے کِسے نوں لوڑ بندی ہے تاں آپو آپنی کھیتی نوں اوہناں کوہلاں وِچوں پانی دے لیندا ہے- اتے کئی گھراٹ اوس نالے پُر پِھردے ہن- ہاڑ دی رُتے اوہ دھرتی وڈی سُہاونی ار سیل دی جگہ ہے- نہراں ار کوہلاں ار امباں دی بہتات کر کے اوس جگہ دی ہریائی منوکھ نوں کشمیر چیتے کراوندی ہے- اوہ مُلک کنڈھی دا مُلک ہے- اتے حاجی پور دے نیڑے چڑھدے رُخ اِک نلا ہے- اوہ اوتھوں نِکل کے دو کُو کوہاں پُر بیاہ ندی نال جا رلدا ہے اتے بیاہ ندی تلواڑے دے گھاٹوں حاجی پور تے اٹھ کوہ ار دریائو ستلُج پنجاہ کوہ ہے-</p>
<p>ہُسیارپور)ہُشیار پور(</p>
<p>	ہُسیار پور ار ہریانہ ار بھونگا ار بہادر نگر ار بجواڑا ہوئے شہر کولو کول- اوہناں دی عمارت پکی کچی دوہاں طرحاں دی ہے اتے بازار اچھے آباد ہن- اوس گِردے دھرتی بُہت ہی تر اَر ہری اتے رُکھ بُہت ہن- پہاڑ دی نیڑ کر کے اوس مُلکھ وِچ نہراں اَر کوہلاں دا پانی کونگ ورگا نِرمل ہے- اَتے ہر پرکار)قِسم، ون( دے درخت گِنتیوں باہرے ہن- سبھ توں ودھیک امباں دے بُوٹے تاں اِتنے ہن جو گِنتی وِچ نہیں آ سکدے اَتے اجیہے مِٹھے تے سواد والے ہوندے ہن جو سارے دوآبیاں وِچ ہور تھائیں اوہو جیہے گھٹ لبھدے ہن- ایہناں شہراں وِچوں مشہور اَر وڈا ہُسیار پور ہے- اتے اوہ ہُسیار خان دا بسایا ہویا ہے- اوہدی عمارت بہُتی پکی اَر بازار دِیاں ہٹاں نو سے )900 ( ہن- اَر بازاراں وِچ پکیاں اِٹاں دا فرش بنیا ہویا ہے- اتے اِس شہر وِچ سفید باریک کپڑا بُہت چنگا بنیدا ہے- اوس نوں سریساف آکھدے ہن- سوداگر لوک ہر پاسے تے اوتھے جا کے خرید کردے ہن- شہروں باہر دروازے دے نیڑے اِک چوہ ہے جو پہاڑوں آوندا ہے اتے بجواڑا نیڑے دی پہاڑ دی کنڈھی وِچ ہے- اوہ اگلے بارے)وارے، زمانے( وِچ اِک وڈا شہر ہویا ہے- ہُن ویران ہو گیا ہویا ہے شہروں باہر پچھم دے رُخ راجے سنسار چند کٹوچ دا بنایا ہویا اِک پکا قلعہ ہے- اوس قلعے دے چھ بُرج اتے اندر وار دو کُھوہے ہن- بیاہ ندی اِس قلعے تے بائی کوہ اتے ستلُج بتی )32 ( کوہ ہے-</p>
<p>ماہل پور</p>
<p>	ماہل پور دے پرگنے دی بروبر اَتے اِکسار زمین اے- اوہدی آب و ہوا بُہت چوکھی اَر چنگی اے- اَر امباں دے بُوٹے اَتے بُہت ہن اَتے ہر ذات دا اناج اَر ہر پرکار دا کماد اوتھے پیدا ہوندا ہے- اَتے پانی دے نلے وگدے ہن- اوتھے کھیتی برکھا نال ہوندی ہے- پر کِدھرے کِدھرے چڑس نال سِنجدے ہن اوتھوں ستلج دا دریائو اُنّی اَر بیاہ ندی بتّی کوہ ہے اَتے اِک سو ستائی پِنڈ اوہدے نال لگدے ہن-</p>
<p>گڑھ شنکر</p>
<p>	ماہل پور دے لاقے وِچوں اِک قصبہ ہے جو اُچی گھاٹی پر بسدا ہے، اوہدی عمارت کِدھرے پکی اَتے بُہتی کچی ہے- بازار دِیاں ہٹاں سو کُو ہون گِیاں- اَتے چھوٹی بئیں دا نِکاس اِس نگر تے نیڑے لہندے کوہاں پُر ہے- اتے بسوں )وسوں( دے لاگے اُتر دے رُخ اوس بئیں پُر اِک پکا پُل سا- ہُن اوہدے دروازے پُور ہو گئے ہن- بئیں دا پانی اوہدے کول دیوں مُڑ جاندا ہے- گڑھ شنکر تے بیاہ ندی چالیہہ کوہ اَر ستلج پندرھاں کوہ ہے-</p>
<p>رہوں</p>
<p>	رہوں اِک پکا شہر ہے جو اُچے تلے پُر بسدا ہے- اوہدِیاں گلیاں اتے بازاراں وِچ پکا فرش بنیا ہویا ہے اتے ہٹاں چھ کُو سو ہن- اوس شہر وِچ گاڑھا کپڑا جیہنوں بافتا یا گھانٹی کہندے ہن بُہت ہی چنگا بندا ہے- اوتھے دا قلعہ پکا ار شہر دے وِچکار ہے- شہر پناہ بنی پکی سی پر ہُن ڈھے گئی ہے- اوہناں کُھوہاں دے پانی دی ڈُونگھیائی جو شہر دے لاگے ہن پنجی گز ہے تے کِدھرے کِدھرے اِس تے بھی ودھیک ہے- پر شہروں باہر دساں بارھاں جگہ اوپر مِٹھا پانی نِکل آوندا ہے- ہاڑی ساون نوں ہر پرکار دا اناج تے کماد ار کپاہ پیندا ہوندا ہے-</p>
<p>پھلور</p>
<p>	پھلور دریائو ستلج دے کنڈھے ڈاہے پر اِک پُرانا شہر ہے ار دِلّی لہور دا راج گھاٹ بھی اوسے پتن ہے- اگلے بادشاہاں دے سمے وِچ ایہہ اِک وڈا شہر ہو ہٹیا ہے ہُن سِکھاں دی ماردھاڑ دے سبب ویران ہو گیا ہے جاں انگریزاں صاحباں دی فوج نے اُرار لے کنڈھے لُدھیانے وِچ آ کے چھائونی پائی اَتے وڈا پکا اَر مضبوط قلعہ بنا لیا تاں مہاراجے رنجیت سِنگھ نے بھی اوہدے ساہمنے اِس شہر پھلور دے وِچ آپنی فوج بہالی اَتے اوہنوں آپنے مُلک دا بنّا ٹھہرایا اَتے بادشاہی سراں دے دوالے وڈی ڈُونگھی اَر پکی کھائی بنوا کے اوہنوں قلعہ ٹھہرایا اَتے اٹھ ہزار فوج اوتھے سدا رہنی رہی- ہُن قلعے دے پاہروآںاَر کارداراں چُھٹ اوتھے ہور فوج نہیں رہندی جِس ویلے کُجھ لوڑ ہوندی ہے اوس ویلے فوج آ جاندی ہے- اتے جاں فوج آ جاندی ہے تاں فیر رونق ہو جاندی ہے اتے ہٹاں دو کُو سو تیکر ہو گئیاں ہن-</p>
<p>تلون</p>
<p>	تلون اِک قدیمی شہر مُسلمان راجپوتاں دی بارسی )وارثی( وِچ ہے- جاں نادرشاہ بادشاہ نے اِس مُلک وِچ آ کے دِلّی دی بادشاہی نوں ہیٹھ اوپر کر سٹیا اَتے ہر زمیندار آپنے آپنے گھر وِچ ٹھانے میر بن بیٹھا تاں اِس شہر دا اِک عنایت خان نامے زمیندار فوج والا ہو گیا اَتے ہور کئی گراواں سمیت ایہہ شہر اوہدے ہتھ لگا- اوس تے پِچھے اوہدے پُت نورنگ خان نے اوہدی گدی سنبھالی- نورنگ خان نواب آدینہ بیگ خان دے سمے وِچ دھن مان ار فوج والا تھا اتے دریائو ستلج دے گھاٹ اوہدے حُکم وِچ سن- اوس تے مگروں اوہدا پوتا محمود خان اوس دی جگہ بیٹھا اتے سدا سِکھاں نال لڑدا رہیا- </p>



</body></text></cesDoc>