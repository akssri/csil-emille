<cesDoc id="pun-w-panchim-02-btitle" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-02-btitle.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>اِِک آدرش وادی دی موت</p>



<p>	)کہانیاں(</p>


<p>	عذرا وقار</p>

<p> </p>


<p> سُچیت کِتاب گھر</p>
<p>مہینہ وار پنچم، فلیٹ نمبر 4 ، علی آر</p>
<p>کیڈ، مُون مارکیٹ، گُلشن راوی، لہور</p>
<p>(: 7410333  ای-میل: suchet@brain.net.pk </p>





















<p>Monthly PANCHAM January, February 2000</p>
<p>Flt. 4, Ali Arcade, Moon Market, Gulshan Ravi, LAHORE </p>
<p>(7410333. E-mail suchet@brain.net.pk</p>

<p> ماں بولی دا چانن کرن والے سوہنے تے رانگلے</p>
<p> سِٹِکر </p>
<p>جیہناں اُتے وڈکے شاعراں دے ال'ہی بول وی ورتائے ہوئے نیں- </p>
<p>ایہہ سِٹِکر</p>
<p> تُہاڈے گھراں دواراں تے آوا جاوی دِیاں سبھناں تھاواں اُتے </p>
<p>چیتر رُت دے رنگ کِھڑا سکدے نیں-</p>

<p>مُل</p>
<p>20 سِٹِکررلویں مِلویں: 15 روپے</p>
<p>	10 سِٹِکراِکو جیہے	 : 10 روپے</p>

<p>مہینہ وار پنچم دے سالانہ خریداراں نوں اِک اِک پیکٹ رسالے دے نال گھر اپڑ جاسی-</p>
<p>چاہوان سجناں نوں چِٹھی پاون تے اِک اِک پیکٹ نمونے پاروں وی گھلیا جا سکدا اے-</p>
<p>چوکھے سِٹِکر خریدن والے چاہواناں نوں اُچیچی چھوٹ ہوسی-</p>
<p>سُچیت کِتاب گھر</p>
<p>فلیٹ نمبر 4 ، علی آرکیڈ، مُون مارکیٹ، گُلشن راوی، لہور</p>


<p>مہینہ وار</p>

<p>جنوری، فروری 2000 ئ  </p>


<p>مہینہ وار</p>

<p>جنوری، فروری 2000 ئ  </p>

<p>مہینہ وار</p>

<p>جنوری، فروری 2000 ئ  </p>







<p>شارب</p>

<p>نظم</p>
<p>سویرے سویرے جوان پُت دی پت لتھی</p>
<p>تے دوپہریں بُڈھے وارے میری</p>
<p>دوویں واری پت لاہن والیاں نوں</p>
<p>اسیں آپے گھر سد کے لیائے</p>
<p>تے کھوا پیا کے ٹوریا</p>

<p>ریشم دی رسی دا ایہو گھاٹا اے</p>
<p>بُہتیاں پکیاں گنڈھاں دے لئیے تے</p>
<p>کٹ کے لمبائی گھٹانی پَیندی اے</p>
<p>نرم گنڈھاں وی دھرتی چِ دبیاں روڑ بن جاندِیاں نیں</p>
<p>تے فولادی دِل پگھر پگھر کے پانی</p>
<p>عقل آپنی چنگی لگدی اے تے شکل دوسرے دی</p>
<p>ڈُبدے نوں کنڈھا نہ مِلے تے اوہنے لہر ای پھڑنی اے</p>
<p>سِروں اُچے ٹہن وڈھیاں</p>
<p>قد اُچے نہیں ہوندے</p>
<p>چھاں گواچ جاندی اے</p>
<p>……</p>



<p>پاکستان وِچ پہلی وار کِسے رسالے دا لوک ادب پراگا</p>

<p>مہینہ وار پنچم ولوں</p>
<p>لوک کتھی میئیں کمال دِین دی ویل</p>
<p>پنجابی دِیاں 14 لوک کہانیاں</p>
<p>جیہڑیاں لوک جیون دِیاں پرتاں دے نال نال تاریخ تے سیاست دے </p>
<p>گُجھے پینترے وی اُگھیڑدِیاں نیں-</p>
<p>l</p>
<p>میئیں کمال دِین نال پُچھ دس</p>
<p>l</p>
<p>کھوجکار لِکھیار پروفیسر سعید بُھٹا دِیاں گلاں</p>
<p>l</p>
<p>سودھی فائزہ، مقصود ثاقب دا مُکھ بول</p>

<p>مُل : 35/- روپے</p>

<p>اُچیچا پراگا</p>




<p>پنجابی </p>
<p>لوک رِیت</p>
<p>دی </p>
<p>ویل</p>






<p>سُچیت کِتاب گھر</p>
<p>مہینہ وار پنچم، فلیٹ نمبر 4 ، علی آرکیڈ، </p>
<p>مُون مارکیٹ، گُلشن راوی، لہور</p>
<p>(: 7410333  ای-میل: suchet@brain.net.pk </p>














<p>ہِیر دمودر دے کرِداراں دا نفسیاتی ویروا</p>

<p>شاہانا نوابی</p>


<p>ہِیر دمودر دے کرِداراں دا نفسیاتی ویروا</p>

<p>شاہانا نوابی</p>


<p>ہِیر دمودر دے کرِداراں دا نفسیاتی ویروا</p>

<p>شاہانا نوابی</p>


</body></text></cesDoc>