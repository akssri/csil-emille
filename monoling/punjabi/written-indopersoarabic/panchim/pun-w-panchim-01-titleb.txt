<cesDoc id="pun-w-panchim-01-titleb" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-01-titleb.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>



<p>Monthly PANCHAM November,December 2000</p>
<p>Flt.11, Sharaf Mansion, 16- Queens Road. LAHORE</p>
<p>(6308265 E-mail: suchet2001@yahoo.com  suchet@pop.pol.com.pk</p>


<p>بھلا ہویا گُڑ مکھیاں کھادا اسیں بِھن بِھن توں چُھٹیاسے</p>
<p>]</p>

<p>پنجابی زبان، ادب، وسیب، رہتل تے تاریخ</p>
<p>بارے پنجابی، اُردُو، انگریزی وِچ چھپی ہر چون کتاب</p>
<p> لئی </p>
<p>سُچیت کتاب گھر )رہتل مرکز(</p>
<p>فلیٹ11 ، شرف مینشن، چوک گنگا رام</p>
<p>16 کوئینز روڈ-لہور فون نمبر 6308265 </p>
<p>نوں چیتے رکھو</p>

<p>اِنشااللہ چھیتی ای بِسمِ اللہ اِکٹھ وی ہوسی </p>

<p> </p>













</body></text></cesDoc>