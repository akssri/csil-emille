<cesDoc id="pun-w-panchim-12-prcha-1" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-12-prcha-1.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>


<p>گورنمنٹ گرلز انٹر کالج فیروزوالہ</p>
<p>اُردُو اعلی'</p>

<p>سالِ دوئم)داخلہ ٹیسٹ(	کُل نمبر 100 	وقت تین گھنٹے</p>

<p> 1 - سیاق و سباق کے حوالے سے کِسی ایک نثری ٹُکڑے کی تشریح کیجیے-</p>
<p>)الف-( ''بُت تراش فقط صُورت کی نقل اُتار سکتا ہے- مصور صُورت کے ساتھ رنگ کو بھی جھلکا دیتا ہے اور ناٹک کرنے 	والا بشرطیکہ شاعر نے اِس کے لئے الفاظ مُہیا کر دئیے ہوں، صُورت اور رنگ کے ساتھ حرکت بھی پیدا کر دیتا ہے- مگر شاعری باوجودیکہ اشیائے خارجی کی نقل میں تینوں فنون کا کام دے سکتی ہے اِس کو تینوں سے اِس بات پر فوقیت ہے کہ انسان کا بطون صرف شاعری ہی کی قلمرو ہے- نہ وہاں مصوری کی رسائی ہے نہ بُت تراشی کی اور نہ ناٹک کی-''</p>
<p>)ب-( ''چلئیے چھوڑئیے ہمیں کیا- میں تو اپنے گُمشُدہ لمحوں کا حساب لگانے بیٹھی تھی، وہ لمحے جِن میں سرسوں کی سی نرمی اور شرینہہ کے پھولوں کی خوشبُو تھی، جانے کہاں گُم ہو گئے- شک ہے کہ اِن کے سپنے میری نیم خوابیدہ آنکھوں میں موجود تھے- اور شاید میں اِنہی سپنوں پر گُزارہ کر لیتی کہ میری بیٹی نے مُجھے جھنجھوڑ کر جگا دیا، وہ باہر سے بھاگی بھاگی آئی اور مُجھ سے لپٹ کر کہنے لگی، ''امی ہمارا بھی کوئی گائوں ہوتا تو کِتنا مزا آتا-'' 	</p>
<p>2 - مندرجہ ذیل اشعار میں سے کِسی تین اجزائ کی تشریح بحوالہ متن کیجیے	</p>
<p>	)الف( کہ حِکمت کو اِک گُمشُدہ مال سمجھو</p>
<p>	جہاں پائو اپنا اِسے مال سمجھو</p>
<p>	)ب( جونہی چاہتی ہے میری رُوح مدہوش</p>
<p>	کہ لائے ذرا لب پہ فریادِ پُر جوش</p>
<p>	اجل آ کے کہتی ہے خاموش خاموش</p>
<p>	)ج( درختوں نے برگوں کے کھولے ورق، کہ لیں طوطیاں بوستان دا سبق</p>
<p>	سماں قُمریاں دیکھ اِس آن کا، پڑھیں بابِ پنجم گُلستان کا</p>
<p>	)د( تیری نگاہ ناز سے دونوں مُراد پا گئے</p>
<p>	عقل، غیاب و جُستجُو! عِشق، حضور و اِضطراب</p>
<p>	)ح( ہے آج سجن کی پیاس بُہت، کیوں دِلِ مسکیں ہے اُداس بُہت</p>
<p>	نس نس میں ہے شوق کی باس بُہت، آنکھوں سے لگی ہیں کیوں جھڑیاں</p>
<p>3 - کِسی ایک ادیب پر سوانحی اور تنقیدی نوٹ لِکھیے-</p>
<p>	)الف( ڈاکٹر مولوی عبدالحق- )ب( اِبنِ اِنشاؔ-)ج( مِرزا مُحمد رفیع سوداؔ-</p>
<p>4 -مندرجہ ذیل اِصناف ِ نظم و نثر میں سے کِسی دو پر تفصیلی نوٹ لِکھئیے</p>
<p>	ناول- ڈرامہ- رُباعی-</p>
<p>5 - مُندرجہ ذیل میں سے کِسی دو کی تعریف بمع امثال کیجیے</p>
<p>	صنعتِ تضاد- حُسن تعلیل- صِنعت ایہام</p>
<p>6 - درج ذیل موضوعات میں سے کِسی ایک کا جواب دیجیے</p>
<p>	1 - فورٹ ولیم کالج کی ادبی خِدمات - 2 - انجمنِ پنجاب کی تحریک-</p>
<p>7 - مندرجہ ذیل نظموں میں سے کِسی ایک کا مرکزی خیال لِکھیے</p>
<p>         کِسان- توسیع شہر-  برکھا رُت</p>
<p>8 - افسانہ ''ہملوگ'' میں کِس معاشرتی روئیے کی نشاندہی کی گئی ہے؟ </p>
<p>                 یا</p>
<p>	منشیات کے انسداد کے لئے ہم پر کیا کیا ذمہ داریاں عائد ہوتی ہیں، بیان کیجیے</p>
<p>9- حضرت رحمان بابا کی نظم ''عیب جوئی کی مذمت '' پر اظہارِ رائے کیجیے</p>
<p>                یا</p>
<p>	فیض کی نظم ''زِنداں کی ایک شام'' کا جائزہ پیش کیجیے-</p>


</body></text></cesDoc>