<cesDoc id="pun-w-panchim-12-perma" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-panchim-12-perma.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Andrew Hardie</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-06-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Articles, etc., from "Panchim" periodical</h.title>
<h.author>Panchim</h.author>
<imprint>
<pubPlace>Lahore, Pakistan and UK</pubPlace>
<publisher>Panchim</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-06-17</date>
</creation>
<langUsage>Punjabi (Shahmukhi / Indo-Perso-Arabic writing system)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>





<p>مُل)اُچیچا پراگا(: 30/-  </p>
<p>سالانہ چندہ : 200/- روپے</p>
<p>چیک راہیں: 225/- روپے</p>
<p>پردیس: 60 امریکی ڈالر</p>
<p>چندہ یاں دان مہینہ وار پنچم لہور دے ناں ای ہووے-</p>


<p>ایڈیٹر پبلشر مقصود احمد ثاقب نے یاسر عُمیر پرنٹنگ پریس، الاکو مینشن، پٹیالہ گرائونڈ، لہور توں چھپوا کے دفتر مہینہ وار پنچم، کمرہ نمبر1 ، فلیٹ نمبر4 ، علی آرکیڈ، مُون مارکیٹ، گُلشن راوی، لہور- </p>
<p>فون نمبر 7410333  توں ورتایا-</p>






<p>اول حمد خُدا دا وِرد کیجے، عِشق کیتا سو جگ دا مُول میاں</p>
<p>v</p>
<p>دُوئی نعت رسول مقبولؐ والی، جیں دے حق نزول لولاک کیتا</p>















<p>مُکھ بول</p>

<p>ایس واری نویں ورھے دے نال نویں صدی دا وی مُڈھ بجھا- چاہیدا سی اُچھاڑ نواں کردے تے اندروں اوہو کُجھ لے کے آن حاضری دیندے جو سوکھا لبھ پوندا اے، پر رُوح نہیں مندا -خبرے سوکھیاں ہوون سُکھاوندا نہیں-عام پدھر تےبھولے بھائیں کیتا جاندا سوال گھڑی مُڑی آن ساہواں ہوندا سی، اِکیویں صدی  وِچ پَیر دھرن تے پنجابی دی گل---- کیویں ہوسی؟ مطلب ایہہ پئی وِیہوِیں صدی وِچ تاں چلو جیویں ہویا، پنجابی دِیاں گلاں کر لئیاں تے جیویں ہویا سُن وی لئیاں پر اِکیہوِیں صدی وِچ کیہ بنسی- دابا ای بڑا اے نویں صدی دا ساڈے مناں اُتے-ویلے نال رلن دی چِنتا ای پِچھا نہیں چھڈدی ساڈا- حالاں سوچنا چاہیدا اے کہ ویلا کیہ اے تے ایہدے نال رلن کیہ اے؟ ون سوّنیاں شیواں تے اوہناں اگے ادھینگی-جیویں اِکیہویں صدی نے سانوں مالا مال کر دینا ہووے ایہناں سارِیاں شیواں نال، پنجابی ہوئی تاں ایہہ شیواں کیویں مِلسن- پنجابی تے چروکنی چھڈی ہوئی اے.......کیہ کُجھ کھٹ لیا اے؟ بُھکھ نال لِپکن نوں ای تاں کھٹی منی بیٹھے آں- ہور کیہ اے؟ پر اُچھاڑ وٹایاں کیہ بندا اے، وٹانا تاں کُجھ ہور پیسی- کیہ؟ رل مِل سوچ کے کُجھ آکھیا جا سکدا اے- ہتھلا پراگا تاں ایس ایس ونجارہ بیدی دی ویل اے، جیہنے سارا جیون پنجاب دی لوک رِیت دے ناں لا چھڈیا اے-</p>
<p>b</p>








<p>بِسمِ اﷲِ الرّحم'نِ الرّحِیم</p>













<p>ہِیر دمودر دے کرداراں دا نفسیاتی ویروا</p>


<p>تحقیقی مقالہ</p>
<p> ایم. اے پنجابی 1998-99 ئ</p>




<p>شاہانہ نوابی</p>







<p>سُچیت</p>

<p>سبھ حق راکھویں</p>









<p>پہلی وار :  مارچ 2000 ئ</p>
<p>مُل: 100/-  روپے</p>









<p>سُچیت کتاب گھر</p>
<p>نے یاسر عُمیر پرنٹنگ پریس، الاکو مینشن، پٹیالہ گرائونڈ، لہور توں چھپوا کے دفتر مہینہ وار پنچم، فلیٹ نمبر 4 ، علی آرکیڈ، مُون مارکیٹ، گُلشن راوی، لہور توں ورتائی-</p>
<p>فُون نمبر 7410333 </p>







<p>آپنے رب تے اوہدے حبیبؐ دے ناں</p>














<p>چوری ویکھے ہِیر سیالے، مُونہوں نہ مُول الائے</p>
<p>دھرتی اُتے لِیکاں کھٹے، آکھ نہ مُونہوں سُنائے</p>
<p>اندر گل ہنڈھائے نِینگر، دِل وِچ فِکر ٹکائے</p>
<p>جے سچ جاناں سِرجن، تاں پیراں پلو پائے</p>
















<p>ویروا</p>

<p>	-- 	مُڈھلی گل	9</p>

<p>	1 	دمودر دا سما	 15 </p>
<p>	2 	دمودر دا جیون	25 </p>
<p>	3 	دمودر دے سمے تیکر ہِیر ادب دی رِیت	49</p>
<p>	4 	ہِیر دی کہانی دمودر دی زبانی	59</p>
<p>	5 	ہِیر دمودر دے کِرداراں دا نفسیاتی ویروا	75</p>

<p>	-- 	ورتیاں کتاباں	166	</p>








<p>1</p>
<p> دمودر دا سما</p>

<p>2</p>
<p> دمودر دا جیون</p>

<p>3 </p>
<p>دمودر دے سمے تیکر ہِیر ادب دی رِیت</p>

<p>4 </p>
<p>ہِیر دی کہانی دمودر دی زبانی</p>

<p>5</p>
<p> ہِیر دمودر دے کِرداراں دا نفسیاتی ویروا</p>

<p>ورتیاں کتاباں</p>


<p>اے- انج دمودر دا اکھیں ڈٹھا والی رمز وی کھل جاندی اے- اساڈی جاچے تاں ایہہ وچاربہت ای سلاہن جوگ اے-</p>

<p>ناہس چاک چروکا ہیرے! میں بھی راٹھاں جایا</p>

<p>کیتا فکر من اندر لڈن، پچھوتانا ناہیں</p>

<p>	دمودر نے وی دوجیاں کویاں طرح اپنے قصے وچ پنجاں پیراں نوں اک خاص مقام</p>

<p>ہولی ٹُریں تے مٹھا بولیں، پہلوں منگیں پانی</p>

<p>تاں رامو سد انایا سہتی، جاوو تخت ہزارے</p>


<p>حوالے	</p>
<p>حوالے 	</p>
<p>حوالے	</p>
<p>حوالے 	</p>
<p>حوالے</p>

<p>ہیر</p>
<p>رانجھا</p>
<p>لڈن</p>
<p>سہتی</p>
<p>چوچک</p>
<p>کندی</p>
<p>کیدو</p>
<p>کھیڑا</p>
<p>معظم</p>
<p>ہسی</p>
<p>نورا</p>
<p>رامو باہمن</p>









</body></text></cesDoc>