<cesDoc id="pun-s-cg-asiannet-03-07-05" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-s-cg-asiannet-03-07-05.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Mrs Manorama Chopra)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-31</pubDate>
</publicationStmt>
<sourceDesc>
<recording type="audio">
<respStmt><resp>Location recording by</resp>
<name>Andrew Hardie</name></respStmt>
<equipment>audio tapes</equipment>
<date>03-07-05</date>
<broadcast>
<bibl><title>Punjabi Programme</title>
<author>BBC Asian Network</author>
</bibl>
</broadcast>
</recording>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Spoken text only has been transcribed.
</samplingDesc>
</encodingDesc>
<profileDesc>
<creation>
<date>03-07-28</date>
<rs type="city">Lancaster, UK</rs>
</creation>
<langUsage>Contemporary spoken Punjabi language used in the UK
</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
</profileDesc>
<revisionDesc>
</revisionDesc>
<textClass>
<channel mode="s">radio broadcast</channel>
<constitution type="single"></constitution>
<derivation type="original"></derivation>
<domain type="art"></domain>
<factuality type="fact"></factuality>
<translations>
</translations>
</textClass>
<particDesc>
<person id="PF021" sex="F" age="unknown"><occupation code="1"></occupation></person>
<person id="PM001" sex="M" age="unknown"><occupation code="1"></occupation></person>
<particLinks>
</particLinks>
</particDesc>
<settingDesc>
<setting who="PF021 PM001">
<name type="country">UK</name>
<locale>radio station</locale>
<activity>presenting a radio programme</activity>
</setting>
</settingDesc>
</cesHeader>
<text>
<body>


<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<u id="1" who="PF021">ਸ਼ਾਮ ਕੇ ਆਠ ਬਜੇ ਹੇਂ ਅਬ ਆਪ ਸੁਰਿੰਦਰ ਕੌਰ ਸੇ ਖ਼ਬਰੇਂ ਸੁਣੇਗੇ । ਅੱਜ ਦੇ ਮੁਖ ਸਮਾਚਾਰ ।</u> 

<u id="2" who="PF021">ਮੌਸਕੋ ਵਿਚ ਇਕ <omit extent="1 word" cause="sound quality"></omit> ਦੌਰਾਨ ਬੰਬ ਧਮਾਕਾ ।</u>

<u id="3" who="PF021"><foreign lang="eng"> C. I. A . officers </foreign> ਇਕ ਟੇਪ ਦੀ ਜਾਂਚ ਕਰ ਰਹੇ ਹਨ ।</u>

<u id="4" who="PF021">ਬਗਦਾਦ ਵਿਚ ਇਕ ਜੋਰਦਾਰ ਬੰਬ ਧਮਾਕੇ ਵਿਚ ਸੱਠ ਤੋ ਜਿਆਦਾ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ।</u> 

<u id="5" who="PF021">ਬ੍ਰਿਕਮੇਨ ਤੇ ਢਾਈ ਮਿਲਿਅਨ ਪੌਂਡ ਖਰਚ ਕਰਣ ਦਾ ਵਾਦਾ ਕਿਤਾ ਗਿਆ ਹੈ ।</u> 
<u id="6" who="PF021">ਸਾਰਸ ਦੀ ਬਿਮਾਰੀ ਨਤੇ ਹੁਣ ਕਾਬੂ ਹੋ ਗਿਆ ਹੈ । ਹੁਣ ਖ਼ਬਰਾਂ ਵੇਰਵੇ ਨਾਲ ।</u>

<u id="7" who="PF021">ਮਾਸਕੋ ਵਿਚ ਰਾਤ ਨੂੰ <omit 
extent="1 word" cause="sound quality"></omit> ਦੀ ਭੀੜ ਵਿਚ ਹੋਏ ਇਕ ਆਤਮਘਾਤੀ ਬੰਬ ਧਮਾਕੇ ਨਾਲ ਸੋਲਾਂ ਲੋਕ ਮਾਰੇ ਗਏ ਹਨ ਅਤੇ ਕਈ ਹੋਰ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ਹਨ । ਇਹ ਹਮਲਾ ਦੌ ਔਰਤਾਂ ਨੇ ਕੀਤਾ । ਏਸ ਹਮਲੇ ਦੀ ਜਿਮੇਵਾਰੀ ਚੇਚਨ <foreign lang="eng">militants</foreign> ਨੂੰ ਦਿੱਤੀ ਗਈ ਹੈ । ਜਿਹਨਾਂ <omit extent="1 word" cause="sound quality"></omit> ਆਦਮੀਆਂ ਨੂੰ <omit extent="1 word" cause="sound quality"></omit> ਦਿੱਤੀ ਗਈ ਸੀ । ਉਹ ਇਕ ਬੰਬ ਧਮਾਕੇ ਵਿਚ ਮਾਰੇ ਗਏ ਹਨ । ਇਸ ਜੋਰ ਦਾਰ ਧਮਾਕੇ ਵਿਚ ਸੱਠ ਤੋ ਜਿਆਦਾ ਹੋਰ ਲੋਕ ਵੀ ਜਖਮੀ ਹੋ ਗਏ ਹਨ ਅਤੇ ਕਈਆਂ ਦੀ ਹਾਲਤ ਗੰਭੀਰ ਦੱਸੀ ਗਈ ਹੈ ।</u>

<u id="8" who="PF021"><foreign lang="eng"> C. I. A. officers</foreign> <omit extent="1 word" cause="sound quality"></omit> ਇਕ <foreign lang="eng"> A. D. A. tape </foreign>  ਦੀ ਜਾਂਚ ਕਰ ਰਹੇ ਹਨ ਉਹ ਪਤਾ ਲਗਾਉਣਾ ਚਾਹੂੰਦੇ ਹਨ ਇਹ ਆਵਾਜ਼ ਸੱਚ ਮੁਚ ਪਹਿਲੇ ਇਰਾਕੀ <foreign lang="eng"> leaders </foreign> ਦੀ ਹੈ ਯਾਂ ਨਹੀਂ । ਇਹ <foreign lang="eng"> report Arabic televison Al-Jajira </foreign> ਤੋ ਦਿਖਾਈ ਗਈ ਹੈ । ਇਸ ਵਿਚ ਇਰਾਕੀਆਂ ਨੂੰ <foreign lang="eng"> Police force</foreign> ਦਾ ਵਿਰੋਧ ਕਰਣ ਲਈ ਕਿਹਾ ਗਿਆਂ ਹੈ । <foreign lang="eng"> U. S. officer Shoan Gibson </foreign> ਨੇ ਕਿਹਾ ਹੈ ਕੇ ਉਹਨਾਂ ਦਾ ਖਿਆਲ ਹੈ ਕੇ ਇਹ ਟੇਪ ਅਸਲੀ ਨਹੀਂ ਹੈ । ਇਹ ਖ਼ਬਰਾਂ ਤੁਸੀਂ ਸੁਰਿੰਦਰ ਕੌਰ ਦੀ ਜਬਾਨੀ ਬੀ ਬੀ ਸੀ ਏਸ਼ੀਅਨ ਨੇਟਵਰਕ ਤੇ ਸੁਣ ਰਹੇ ਹੋ ।</u>

<u id="9" who="PF021"><foreign lang="eng">World Health organization</foreign> ਨੇ ਇਹ ਐਲਾਨ ਕੀਤਾ ਹੈ ਕੇ ਹੁਣ <foreign lang="eng">sars virous</foreign> ਤੇ ਕਾਬੂ ਪਾ ਲਿਆ ਗਿਆ ਹੈ ।ਇਸ ਸੰਸਥਾ ਨੇ <foreign lang="eng">doctors</foreign> ਅਤੇ ਸਾਇੰਸਦਾਨਾ ਦੇ ਕੰਮ ਦੀ ਬਹੁਤ ਪ੍ਰਸ਼ੰਸਾ ਕੀਤੀ ਹੈ । ਇਸ ਬਿਮਾਰੀ ਨਾਲ ਪਰਭਾਵਿਤ ਹੋਏ ਦੇਸ਼ਾਂ ਦੀ <foreign lang="eng"> list </foreign> ਤੋ ਹਟਾਉਣ ਲਈ ਭਾਰਤ ਹੁਣ ਆਖਿਰੀ ਦੇਸ਼ ਸੀ ।</u>

<u id="10" who="PF021">ਬ੍ਰਿਕਮੈਨ ਦੀ ਮਦਰ ਹੈਰੀ ਟੇਜ ਨੂੰ ਸੰਭਾਲ ਕੇ ਰੱਖਣ ਲਈ ਇਕ <foreign lang="eng"> re- genration </foreign> ਸਕੀਮ ਕਾਇਮ ਕਰ ਰਹੀ ਹੈ । ਬ੍ਰਿਕਮੈਨ ਨੇ ਕੜੀ ਮਅਇਲ ਦੇ ਨਾਮ ਨਾਲ ਵੀ ਜਾਨਿਆ ਜਾਂਦਾ ਹੈ । ਇਸ ਇਲਾਕੇ ਵਿਚ ਕੋਈ ਸੱਤਰ ਹਜਾਰ ਬੰਗਲਾ ਦੇਸ਼ੀ ਲੋਕਾਂ ਦੀ <foreign lang="eng"> community </foreign> ਹੈ । ਇਸ ਕਮਜੋਰ ਇਲਾਕੇ ਦੇ <foreign lang="eng">bussiness</foreign> ਵਿਚ ਫ਼ਾਇਦਾ ਹੋਵੇਗਾ ਤੇ <foreign lang="eng"> central London </foreign> ਵਿਚ <foreign lang="eng">property brokers</foreign> ਦੀ ਬਰਾਬਰੀ ਹੋਵੇਗੀ । ਬ੍ਰਿਕਮੈਨ ਦੇ ਢਾਈ ਮਿਲਿਅਨ ਪੌਂਡ ਦੁਕਾਨਾਂ ਦੇ <foreign lang="eng"> repair </foreign> ਕਰਣ ਤੇ ਖਰਚ ਕਰਣ ਦਾ ਵਾਦਾ ਕਿਤਾ ਗਿਆ ਹੈ ।</u>

<u id="11" who="PF021"><foreign lang="eng"> united states </foreign> ਤੋ ਸੋਰੈਨਾ ਵਿਲਿਅਮ ਨੇ ਆਪਣਾ ਵਿਮਬਲਡਨ <foreign lang="eng"> singal's tital </foreign> ਕਿਤਾਬ ਬਰਕਰਾਰ ਰੱਖਿਆ ਹੈ । ਉਸਨੇ ਆਪਣੀ ਭੇਣ ਵੀਨਸ ਨੂੰ <foreign lang="eng"> three sets</foreign> ਵਿਚ ਹਰਾਇਆ ਹੈ । ਜਿ ਕੇ <foreign lang="eng"> Four to six, six to four, six ti two</foreign> ਨਲ ਛੇ ਨੰਬਰਾਂ ਚ <foreign lang="eng"> singal's championship </foreign> ਚ ਜਿਤ ਪ੍ਰਾਪਤ ਕੀਤੀ ਹੈ । ਵੀਨਸ ਵਿਲਿਅਮ ਦੇ ਢਿਡ ਵਿਚ ਦਰਦ ਸੀ ਜਦੋ ਉਹ ਮੈਚ ਖੇਲਣ ਲਈ ਗਈ ਸੀ । ਉਹ ਕੁਝ ਮਿੰਟਾ ਲਈ ਕੋਰਟ ਛੱਡ ਕੇ ਗਈ ਸੀ ।</u>

<u id="12" who="PF021">ਹੁਣ ਪੇਸ਼ ਹੈ ਮੌਸਮ :- ਰਾਤ ਨੂੰ ਮੌਸਮ ਖੁਸ਼ਕ ਹੋਵੇਗਾ । ਹਲਕੀ ਹਲਕੀ ਹਵਾ ਚਲੇਗੀ । ਘੱਟ ਤੋਂ ਘੱਟ ਤਾਪਮਾਨ ਗਿਅਰਾਂ ਡਿਗਰੀ ਸੈਲਸੀਅਸ ਬਵੰਜਾ ਡਿਗਰੀ ਫ਼ਾਰੇਨਹਾਇਟ ਹੋਵੇਗਾ । ਕੱਲ ਐਤਵਾਰ ਹੈ ਮੌਸਮ ਖੁਸ਼ਕ ਹੋਵੇਗਾ ਅਤੇ ਕਿਤੇ ਕਿਤੇ ਧੁੱਪ ਨਿਕਲੇਗੀ । ਹਲਕੀ ਹਲਕੀ ਹਵਾ ਚਲੇਗੀ । ਵੱਧ ਤੋ ਵੱਧ ਤਾਪਮਾਨ ਵੀਹ ਡਿਗਰੀ ਸੈਲਸੀਅਸ, ਅਠਾਟ ਡਿਗਰੀ ਫ਼ਾਰੇਨਹਾਇਟ ਹੋਵੇਗਾ ਸੋਮਵਾਰ ਨੁੰ ਮੌਸਮ ਖੁਸ਼ਕ ਅਤੇ ਧੁਪਦਾਰ ਹੋਵੇਗਾ । <foreign lang="eng"> B. B. C Asian Network. </foreign> ਤੋ ਖ਼ਬਰਾਂ ਖਤਮ ਹੋਈਆਂ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="13" who="PM001">ਸਭ ਤੋ ਪਹਿਲਾਂ ਆਪਣੇ ਪਿਆਰੇ ਪਿਆਰੇਸਰੋਯਿਆਂ ਨੂੰ ਪਿਆਰਭਰੀ ਸੱਤ ਸ੍ਰੀ ਅਕਾਲ, ਨਮਸਕਾਰ ਅਤੇ ਸਲਾਮ-ਅ-ਲੇਕਮ । ਜਨਾਬ ਅੱਠ ਵੱਜ ਕੇ ਤੇ ਬਾਰਾਂ ਮਿੰਟ ਤੇ ਕੁਝ ਸੈਕਿੰਡ ਹੋਏ ਐ । ਤੁਹਾਡਾ ਸਭ ਦਾ ਸੁਆਗਤ ਐ ਇਕ ਵਾਰਿ ਫੇਰ । ਮੇਰੇ ਤੋ ਪਹਿਲਾਂ ਬਲਕਾਰ ਸਾਹਿਬ ਜੀ ਬਹੁਤ ਹੀ ਪਿਆਰਾ <foreign lang="eng">program </foreign> ਕਰਕੇ ਗਏ ਐ । ਉਹਨਾਂ ਦਾ ਸ਼ੁਕਰੀਆ ਅਦਾ ਕਰਾਂਗਾ ਬੜਾ ਪਿਆਰਾ ਬਹੁਤ ਹੀ ਮਿੱਠਾ ਪ੍ਰੋਗ੍ਰਮ ਕੀਤੈ ਉਹਨਾਂ ਨੇ ਹੂਣਯੋ ਲੈ ਕੇ ਪੂਰੇ ਦਸ ਵਜੇ ਤੱਕ ਮੈਂ ਤੁਹਾਡਾ ਸਾਥ ਚਾਹਾਂਗਾ ਔਰ ਉਮੀਦ ਐ ਕੇ ਤੁਸੀਂ ਮੇਰਾ ਸਾਥ ਦਿਓਗੇ ਅੱਜ ਬੜੀ ਨਿਘੀ ਨਿਘੀ ਸ਼ਾਮ ਐ ਤੁਹਾਨੂੰ ਸਭ ਨੂੰ ਮੁਬਾਰਕ ਕਹਿ ਰਹੀ ਐ । ਔਰ ਅਸੀਂ ਅੱਲ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਕੇ ਬਹੁਤ ਹੀ ਪਿਆਰੇ ਪਿਆਰੇ ਗੀਤ ਔਰ ਤੁਹਾਡੇ ਪੈਗਾਮ ਤੁਹਾਡੇ <foreign lang="eng">Message</foreign> ਤੁਹਾਡੇ ਦੋਸਤਾਂ ਤੱਕ ਪਹੂੰਚਾਵਾਂਗੇ । ਤੁਸੀਂ ਸਾਡਾ <foreign lang="eng">programe digital satelite channal</foreign>  ਅੱਠ ਸੋ ਉੱਨਤਰ ਤੇ ਸੁਣ ਰਹੇ ਓ ਤੇ <foreign lang="eng">Internet</foreign> ਰਾਂਹੀ ਵੀ ਦੁਨਿਆਂ ਭਰ ਵਿਚ ਸੁਣ ਰਹੇ ਐ । ਮੈਨੂੰ ਦਲਜੀਤ ਮੀਰ ਕਹਿੰਦੇ ਨੇ ਤੇ ਮੇਰਾ ਸਾਥ ਦੇ ਰਹੇ ਐ ਸ਼ੈਫਾਲੀ । ਸ਼ੈਫਾਲੀ ਤੁਹਾਡੇ ਪੈਗਾਮ ਲੈ ਕੇ ਮੇਰੇ ਤੱਕ ਆਉਦੇ ਨੇ ਔਰ ਮੈਏ ਤੁਹਾਡੇ ਪੈਗਾਮ ਤੁਹਾਡੀਆਂ ਫਰਮਾਇਸ਼ਾਂਪੂਰੀਆਂ ਕਰਣ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰਦੈਂ ਸਾਡਾ ਨੰਬਰ <foreign lang="eng">08 459 440 445 </foreign> ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="9" who="PM001">ਜਨਾਬ ਜਿਸ ਤਰਾਂ ਤੁਹਾਨੂੰ ਪਹਿਲਾਂ ਦੱਸਿਆ ਸੀ ਜਿਸ ਤਰਾਂ ਕੇ ਅੱਜ ਸੱਤਰਾਂ ਮਿਰਚ ਕੁਛ ਤੁਹਾਡੀਆਂ ਫਰਮਾਇਸ਼ਾਂ ਕੁਛਸਾਡੇ ਮਨ ਪਸੰਦ ਗੀਤ ਹੋਣਗੇ । ਉਹਨਾਂ ਦੀ ਸ਼ੁਰੂਆਤ ਅਸੀਂ ਕੀਤੀ ਹੰਸ ਰਾਜ ਹੰਸ ਦੇ ਮਾਹੀਏ ਤੋਂ ਹੀਰ ਰੱਬਾ ਨਾ ਜਮਦੀ ਦੁਖ ਪੈਂਦਾ ਨਾ ਰਾਂਝੇ ਨੂੰ ਇਹ ਤੇ ਧੁਰੌ ਲਿਖੀਆਂ ਆਇਆਂ ਸਨ । ਜੇ ਇਹ ਨਾਂ ਲਿਖਿਆਂ ਹੂਦਿਆਂ ਤਾਂ ਫੇਰ ਇਹ ਕਹਾਨੀਆਂ ਕਿੱਸੇ ਇਹ ਇਸ਼ਕ ਦੇ ਚਰਚੇ ਕਿਓ ਹੂੰਦੇ । ਮੇਰਾ ਇਸ਼ਕ ਸੀ ਨਾਮ ਹਜੂਰ ਤੇਰੇ ਤੁੰ ਇਹ ਵੰਡ ਦਿੱਤਾ ਕਿਸੇ ਹੋਰ ਤਾਈਂ । ਰੱਬ ਸਾਰੀਂਆਂ ਬੁਰਾਇਆਂ ਨੂੰ ਬਖਸ਼ ਦਿੰਦਾ ਪਰ ਨਾ ਬਖਸ਼ਦਾ ਇਸ਼ਕ ਦੇ ਚੋਰ ਤਾਂਈਂ । ਜੇ ਇਸ਼ਕ ਨੇ ਚੋਰ ਬਣ ਗਏ ਨੇ, ਉਹਨਾਂ ਨੂੰ ਰੱਬ ਨੀ ਬਖਸ਼ਦਾ । ਐਸਾ ਠੋਕ ਕੇ ਜੜਿਆ ਏ ਪੱਥਰਾਂ ਨੂੰ, ਐਸਾ ਬੀੜ ਕੇ ਜੜਿਆ ਨਿਹੱਥੀਆਂ ਨੂੰ ਤੜਫਨ ਲਈ ਨਾ ਖੁਲੀ ਵੀ ਥਾਂ ਦਿੱਤੀ । ਏਸ ਜੱਗ ਨੇ ਇਸ਼ਕ ਦੇ ਫਤਿਆਂ ਨੂੰ । ਜਨਾਬ ਇਸ਼ਕ ਦੇ ਵਿਚ ਜਦੋਂ ਬੰਦਾ ਕੱਲਿਆਂ ਹੋ ਜਾਂਦਾ ਐ, ਪਾਗਲ ਹੋ ਜਾਂਦੈ, ਫੇਰ ਉਹਨੂੰ ਪਤਾ ਨੀ ਲੱਗਦਾ ਉਹ ਕੀ ਕਹਿ ਰਿਹੈ ਇਹਦੇ ਲਈ ਸਭ ਚੀਜ਼ਾਂ ਸਭ ਗੱਲਾਂ ਕੌੜੀਆਂ ਲੱਗਦੀਆਂ ਨੇ ਐਵੇ ਕਿਸੇ ਨੂੰ ਕਹਿਏ ਦਿਲ ਤੋੜ ਵੀ ਦਿੰਦੈ ਦਿਲ ਭੰਨ ਵੀ ਦਿੰਦੈ ਟੁਕੜੇ ਟੁਕਰੇ ਵੀ ਕਰ ਦਿੰਦੈ <omit extent="1 word" cause="sound quality"></omit> ਇਸ਼ਕ ਬਾਂਝ ਤਾਂ ਜੱਗ ਮਰ ਜਾਣ ਵਾਲਾ ਹੋਇਆ ਕੀ ਜੇ ਹੀਰ ਤੋ ਘੱਟ ਲੈ ਤੁ ਤੇਰਾ ਮਾਹੀਆ ਵੀ ਹੋਰ ਬਨਾਉਣ ਵਾਲਾ ਇਸ਼ਕ ਦੀਆਏ ਗੰਲਾਂ ਕੁਛ ਮਹੱਬਤ ਦੀਆਂ ਕੁਸ਼ ਪਿਆਰ ਦੀਆਂ ਗੱਲਾਂ ਕੁਛ ਵਿਛੋੜੇ ਦੀਆਂ ਗੱਲਾਂ ਕੁਛ ਤੁਹਅਡੇ ਪੇਗ਼ਾਮ ਕੁਛ <foreign lang="eng"> Message </foreign> ਅਸੀਂ ਕੋਸ਼ਿਸ਼ਕਰਾਂਗੇ ਪੂਰੇ ਛੇ ਵਜੇ ਪੂਰੇ ਦਸ ਵਜੇ ਤੱਕ ਤੁਹਾਡੇ ਗੀਤ ਔਰ ਜਿਹੜੀਆਂ ਤੁਹਅਡੀਆਂ ਫਰਮਾਇਸ਼ਾਂ ਨੇ ਐਹ ਅਸੀਂ ਪੂਰੀਆਂ ਕਰੀਏ ।<foreign lang="eng"> Messages </foreign> ਲਈ <foreign lang="eng">08 459 440 445</foreign> ਸਾਡਾ ਨੰਬਰ ਹੈ ।ਨੰਬਰ ਇਕ ਵਾਰੀ ਫੇਰ ਅਸੀਂ ਦੱਸਿਏ <foreign lang="eng">08 459 440 445</foreign> ਸਾਡਾ ਨੰਬਰ ਹੈ । </u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="10" who="PM001"><omit extent="1 word" cause="sound quality"></omit>ਤੋ ਤੁਸੀਂ ਸੁਣਿਆਂ ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਔਰ ਸਾਡੇ ਕੋਲ ਐਸ ਵਕਤ ਅੱਠ ਵੱਜ ਕੇ ਤੇ ਇੱਕੀ ਮਿਨਟ ਹੋਏ ਨੇ । <vocal desc="er"></vocal> ਜਿਸ ਤਰਾਂ ਅਸੀਂ ਪਹਿਲਾਂ ਦੱਸਿਆ ਸੀ ਤੁਹਾਡੀਆਂ ਫਰਮਾਇਸ਼ਾਂ ਦੇ ਗੀਤ ਸੁਨਾਵਾਂਗੇ ਅਸੀਂ । <foreign lang="eng">08 459 440 445</foreign> ਸਾਡਾ ਨੰਬਰ ਐ ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਕੇ ਜਿਹੜੇ ਤੁਸੀਂ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਕਿਤੀ ਐ । ਉਹਨੂੰ ਅਸੀਂ ਜਰੂਰ ਲੱਭ ਕਰ ਤੁਹਾਨੂੰ ਸੁਨਾਉਣਾ ਐ ।</u>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<u id="11" who="PM001">ਇਹ ਸਨ ਸਾਡੇ <foreign lang="eng">B. B. C Asian Network.</foreign> ਦੇ ਕੁਛ ਹੋਰ <foreign lang="eng">program </foreign> ਜਿਹਨਾਂ ਦੇ ਵੇਰਵਾ ਤੋੜੀ ਦੇਰ ਬਾਅਦ ਦਿਆਂਗੇ ਅਸੀ ਏਹ ਤੁਹਾਨੂੰ । ਆਓ ਪਹਿਲਾਂ ਚੱਲੀਏ ਅਸੀਏ ਏਸ ਗੀਤ ਵੱਲ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>


<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="12" who="PM001">ਗੀਤ ਸੁਣਿਆ ।</u>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="13" who="PM001"><omit extent="1 word" cause="sound quality"></omit> ।</u>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="14" who="PM001">ਅੱਠ ਵੱਜ ਕੇ ਤੇ ਛੱਬੀ ਮਿੰਟ ਹੋਏ ਨੇ ਸਾਡਾ ਤਾਂ ਤੁਹਾਡਾ ਸਾਥ ਰਹੇਗਾ ਪੂਰੇ ਦਸ ਵਲੇ ਤੱਕ ਸਿਫਰ ਅੱਠ ਚਾਰ ਸੋ ਉਨ ਅੱਠ ਚਾਰ ਸੋ ਚਾਲਿ, ਚਾਰ ਸੋ ਪੇਂਤਾਲੀ <foreign lang="eng">08 459 440 445</foreign> ਤੁਸੀਂ ਏਸ ਵਕਤ ਜੋਗਿੰਗ ਕਰ ਰਹੇ ਓ ਯਾਂ ਫੇਰ ਕਿਸੇ ਦੁਕਾਨ ਚ ਯਾਂ ਫੇਰ <foreign lang="eng"> factory </foreign>ਵਿਚ ਕੰਮ ਕਰ ਰਹੇ ਹੋਵੋਗੇ ਯਾਂ ਫੇਰ ਘਰ ਬੈਠੇ ਸਾਡਾ <foreign lang="eng">program </foreign> ਸੁਣ ਰਹੇ ਹੋ ਤੇ ਤੁਹਾਡਾ ਇਕ ਵਾਰੀ ਫੇਰ ਅਸੀਏ ਸੁਆਗਤ ਕਰਦੇ ਐ ਖੁਸ਼ਾਮਦੀਦ ਕਹਿਨੇ ਐ ਤੁਸੀਂ ਸਾਡਾ <foreign lang="eng">program </foreign> ਪੂਰੇ ਦਸ ਵਜੇ ਤੱਕ ਸੁਣੋਗੇ । ਸ਼ੈਫਾਲੀ ਜੀ ਤੁਹਾਡੇ ਪੈਗਾਮ ਮੇਰੇ ਤੱਕ ਪਹੂੰਚਾ ਰਹੇ ਨੇ ਸਾਨੂੰ ਇਕ ਫੋਨ ਆਇਆ ਕਰਣ ਜੀਤ ਜੀ ਕੈਕਬਰਾ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਨੇ ਉਹਨਾਂ ਨੇ ਕਿਹੈ ਕੇ ਉਹਨਾਂ ਦੇ <foreign lang="eng">husband</foreign>
ਨਰੇਸ਼ ਕੁਮਾਰ ਜੋ ਕੇ ਆਪਣਾ ਜਨਮ ਦਿਨ ਮਨਾ ਰਹੇ ਨੇ ਕੱਲ ਔਰ ਉਹਨਾਂ ਨੇ ਕਿਹੈ ਐਹਨਾਂ ਦੇ ਨਾ ਇਕ ਗੀਤ ਐ ਗੁੱਸਾ ਨਾ ਕਰਿਏ ਪਿਛੋ ਹਾਕ ਮਾਰੀਏ । ਉਥੋ ਦੇ ਤੁਸੀਂ ਦੱਸਦੇ ਕੇ ਇਹ <foreign lang="eng">Artist</foreign> ਕੌਣ ਐ ਔਰ ਸੀ ਡੀ ਦਾ ਕੀ ਨਾਮ ਐ ਵੈਸੇ ਇਹ ਗੀਤ ਸੁਣਿਆ ਤੇ ਹੈ ਸਾਡੇ ਕੋਲ ਹੋਣਾ ਵੀ ਐ । ਅਸੀ ਬੜੀ ਮੇਹਰਬਾਨੀ ਜਿ ਤੁਹਾਡੀ । ਔਰ ਤੁਸੀਂ ਆਪਣੇ ਵੱਲੋਂ ਤੇ ਤੁਹਾਡੇ ਬੱਚੇ ਅਰੁਨ ਅਤੇ ਅਸ਼ਮੀ ਅਤੇ ਸ਼ੀਬਾ ਉਹਨਾਂ ਵਲੋ ਉਹਨਾਂ ਨੂੰਬਹੁਤ ਬਹੁਯ ਜਨਮ ਦਿਨ ਮੁਬਾਰਕ ਕਹੀੰਦੇ ਓ ਸਾਡੇ ਵਲੋ ਵੀ ਨਰੇਸ਼ ਕੁਮਾਰ ਜੀ ਤੁਹਾਨੂੰ ਬਹੁਤ ਬਹੁਤ ਜਨਮ ਦਿਨ ਮੁਬਾਰਕ ਅਸੀਂ ਦੁਆ ਮਰਦੇ ਐ ਕੇ ਪ੍ਰਮਾਤਮਾਂ ਤੁਹਾਨੂੰ ਤੰਦਰੂਸਤੀ ਬਖਸ਼ੇ ਅਤੇ ਤੁਹਾਨੂੰ ਏਸੇ ਤਰਾਂ ਤੁਹਾਡੇ ਪਰਿਵਾਰ ਦਾ ਪਿਆਰ ਤੁਹਾਡੇ ਨਾਲ ਰਹੇ । ਸ਼ਫੀਕ ਸਾਹਿਬ ਲੰਦਨ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਸਬਨ ਉਹਨਾਂ ਨੇ ਕਿਹੈ ਕੇ ਦਲਜੀਤ ਜੀ ਅਸੀਂ ਤੁਹਾਡਾ ਪੂਰਾ ਹਫਤਾ ਇੰਤਜਾਰ ਕਰਦੇ ਐ ਤੁਹਾਡੇ ਪਰੋਗ੍ਰਾਮ ਦਾ ਔਰ ਵੀ <foreign lang="eng">program </foreign> ਦਾ ਆਨੰਦ ਮਾਨ ਰਹੇ ਐਂ । ਔਰ ਚਲਦੇ ਐ ਅਸੀਂ ਏਸ ਗੀਤ ਵੱਲ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="15" who="PM001">ਤੁਸੀਂ ਬਹੁਤ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਂ ਸੁਣਿਆ ਸੁਰਜਿਤ ਖਾਨ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਅੱਠ ਵੱਜ ਕੇ ਤੇ ਬੱਤੀ ਮਿੰਤ ਹੋਏ ਨੇ । ਜੇਕਰ ਤੁਸੀ ਹੁਣੇ ਹੁਣੇ ਡੀ ਡੀ ਔਨ ਕੀਤੈ ਤੁਸੀਂ ਇਹ <foreign lang="eng">B. B. C Asian Network.</foreign> ਦਾ ਇਹ ਪੰਜਾਬੀ <foreign lang="eng">program </foreign> ਸੁਣ ਰਹੇ ਓ ਦਲਜੀਤ ਮੀਰ ਦੇ ਨਾਲ ਸਾਡੇ <foreign lang="eng">program </foreign> <foreign lang="eng">digital satelite channal 869 </foreign> ਤੇ ਵੀ ਸੁਣੇ ਜਾ ਰਹੇ ਨੇ <foreign lang="eng"> Internet</foreign> ਦੇ ਜਰਿਏ ਤੁਸੀਂ ਸਾਡੇ ਪ੍ਰੋਗ੍ਰਮ ਦੁਨਿਆਂ ਭਰ ਵਿਚ ਕਿਤੇ ਵੀ ਸੁਣ ਸਕਦੇ ਓ । ਫਿਲਹਾਲ ਫਰਮਾਇਸ਼ਾਂ ਸਾਨੂੰ ਕੁਝ ਹੋਰ ਆਇਆਂ ਨੇ ਜਿਸ ਤਰਾਂ ਕੇ ਸ਼ਫੀਕ ਸਾਹਿਬ ਲੰਦਨ ਤੋ ਕਹਿ ਰਹੇ ਸੀ ਦਿਲਦਾਰ ਸਿੰਘ ਪਰਦੇਸੀ ਦਾ ਗੀਤ ਸੁਣਾ ਦਿਓ । ਜਰੂਰ ਸੁਣਾਵਾਂਗੇ ਜੀ ਤੁਹਾਨੂੰ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆਂ ਤੁਸੀਂ ਸਾਨੂੰ ਏਨੀ ਦੂਰੋਂ ਯਾਦ ਕੀਤੈ । ਨਾਜ਼ੀਆ ਬਰਘਿੰਗਮ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਸਨ ਉਹਨਾਂ ਨੇ ਕਿਹੈ ਕੇ <vocal desc="er"></vocal> ਦਲਗੀਰ ਸ਼ਾਹ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਕ ਗੀਤ ਹੈਗਾ <vocal desc="er"></vocal> ਜਨਾਬ ਸਾਡੇ ਕੋਲ ਹੋਇਆ ਤੇ ਜਰੂਰ ਸੁਨਾਵਾਂਗੇ ਨਹੀਂ ਤੇ ਕੋਈ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ ਤੁਹਾਡੇ ਨਾਂ ਜਰੂਰ ਕਰਾਂਗੇ । ਬਹਰਹਾਲ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਤੁਸੀਂ ਏਨੀ ਦੂਰੋ ਸਾਨੂੰ ਫੋਨ ਕੀਤੈ ਤੇ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਐ ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਜੇ ਇਹ ਗੀਤ ਸਾਨੂੰ ਲੱਭ ਗਿਆ ਤੇ ਜਰੂਰ ਸੁਣਵਾਂਗੇ ਤੁਹਾਨੂੰ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event> 

<event desc="music" dur="approximately 10 seconds"></event>

<u id="16" who="PM001">ਜਦੋ ਹੋਲੀ ਜਿਹੀ ਲੈਣਾ ਮੇਰਾ ਨਾਂ ਮੇਂ ਥਾਂ ਮਰ ਜਾਨੀ ਐ ਇੰਦਰਜਿਤ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਤੁਸੀਂ ਇਹ ਗੀਤ ਸੁਣਿਆਂ ਐ ਇਸ ਗੀਤ ਦੇ ਵਿਚ ਇਕ ਲਫਜ਼ ਸੀਗਾ <vocal desc="er"></vocal> ਮਤਲਬ ਗੀਤ ਦੇ ਵਿਚ ਮਰਦਾਂ ਦੇ ਵਾਦੇ ਝੂਠੇ ਝੂਠੇ ਇਕਰਾਰ ਨੇ ਇਹ ਕੈਸੀ ਗੱਲ ਐ ਮਰਦਾਂ ਨੂੰ ਔਰਤਾਂ ਦੋਸ਼ ਦੇ ਦਿੰਦੀਐ, ਮਰਦਾ ਔਰਤਾਂ ਨੂੰ ਦੇਂਦੇ ਨੇ <vocal desc="er"></vocal> ਮਹੱਬਤ ਦੇ ਵਿਚ ਗੁਸੇ ਗਿਲੇ ਹੂੰਦੇ ਰਹਿੰਦੇ ਐ ਜਨਾਬ । ਲੇਕਿਨ ਤਾਲੀ ਦੋਹਾਂ ਹੱਥਾ ਨਾਲ ਵੱਜਦੀ ਹੇ । ਕਿਸੇ ਨੇ ਕਿਹੈ <vocal who="PM001" desc="laugh"></vocal> ਮੈਂ ਜਿਆਦਾ ਈ <foreign lang="eng">detail</foreign> ਐ ਫੇਰ ਕਦੀ ਤੁਹਾਨੂੰ ਦੱਸਾਂਗੇ । ਫਰਾਈਡੇ ਸਾਨੂੰ ਲੈਸਟਰ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਨੇ ਉਹਨਾਂ ਨੇ ਸਾਨੂੰ ਸੱਤ ਸ੍ਰੀ ਅਕਾਲ ਕਿਹੈ । ਉਹਨਾਂ ਨੇ ਗੁਰਦਾਸ ਮਾਨ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਕ ਗੀਤ ਉਹਨਾਂ ਕਿਹੈ ਇਹ ਗੀਤ ਸਾਨੂੰ ਲਿਖ ਰਹੇ ਨੇ ਵਰਜੀ ਗੀਤ ਅਤੇ ਧਰਮਿੰਦਰ ਇਹ ਗੀਤ ਤੁਹਾਡੇ ਨਾਂ ਕਰਾਂਗੇ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="16" who="PM001">ਮੁੜ ਮੁੜ ਯਾਦ ਸਤਾਵੇ ਪਿੰਡ ਦੀਆਂ ਗਲੀਆਂ ਦੀ ਕਿਸੇ <foreign lang="eng">writer</foreign> ਨੇ ਕਿਹੈ ਰੱਬ ਨੇ ਪਿੰਡ ਤੇ ਮਨੁਖ ਨੇ ਸ਼ਹਿਰ ਬਣਾਏ ਨੇ । ਬੜੀ ਬਾਰੀਕ ਗੱਲ ਐ ਬੜੀ ਗਹਰੀ ਗੱਲ ਐ । ਰੱਬ ਨੇ ਪਿੰਡ ਅਤੇ ਮਨੁਖ ਨੇ ਸ਼ਹਿਰ ਬਣਾਏ ਨੇ । ਬੜਾ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਂ ਸੁਣਿਆ ਹੁਣ ਸਾਡੇ ਕੋਲ ਕੁਛ <foreign lang="eng">dedication</foreign> ਨੇ ਨਾਜ਼ੀਆ ਬਰਘਿੰਗਮ ਤੋ ਫੋਨ ਤੁਸੀਂ ਕੀਤੈ ਤੁਸੀਂ ਇਕ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਐ ਅਸੀਂ ਲੱਭ ਰਹੇ ਐ ਉਹ ਗੀਤ ਜੇ ਲੱਭ ਗਿਆ ਤਾਂ ਜਰੂਰ ਸੁਣਾਵਾਂਗੇ ਅਸੀਂ । ਨਹੀਂ ਤੇ ਕੋਈ ਹੋਰ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ ਤੁਹਾਡੇ ਨਾਂ ਕਰਾਂਗੇ । ਤਨਵੀਰ ਅਹਮਦ ਸਾਹਿਬ ਤੁਸੀਂ ਵੁਲਵਾਹੰਟਨ ਤੋ ਫੋਨ ਕੀਤੈ ਅੱਲਾ ਕਰੇ ਦਿਨ ਨਾ ਚੜੇ, ਇਹ ਗੀਤ ਅਬਰਾਰ ਉਲ ਹੱਕ ਦਾ ਗਾਇਆ ਹੋਇਆ ਐ ਮੇਰੇ ਖਿਆਲ ਯਾਂ ਫੇਰ ਇਕ ਬਿਸਮਿਲਾਹ ਦੀ ਨੇ ਨਹੀਂ ਗਾਇਆ ਹੈ, ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਕੇ ਤੁਸੀਂ ਕਿਹੈ ਤੁਹਾਡੇ ਮਾਮਾ ਸਬੀਰ ਹੁਸੈਣ ਅਤੇ ਤੁਹਾਡੇ <foreign lang="eng">cousin</foreign> <vocal desc="er"></vocal> ਸ਼ਕੀਲ ਐਹਮਦ <vocal desc="er"></vocal> ਉਹਨਾਂ ਦੇ ਨਾਂ ਕਰਣਾ ਚਾਹੂੰਦੇ ਓ ਤਨਵੀਰ ਜੀ ਤੁਸੀਂ ਫੋਨ ਕੀਤੈ ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਜੇ ਲੱਭ ਗਿਆ ਤਾਂ ਜਰੂਰ ਸੁਨਾ ਵਬਂਗੇ ਅਸੀਂ ਜਿਸਤਰਾਂ ਅਸੀਂ ਪਹਿਲਾਂ ਕਹਿਨਦੇ ਐ ਕੇ ਜੇਕਰ ਸਾਨੂੰ ਲੱਭ ਜਾਂਦੇ ਨੇ ਗੀਤ ਤੁਹਾਡੀਆਂ ਫਰਮਾਇਸ਼ਾਂ ਦੇ ਅਸੀਂ ਉਹ, ਸਾਨੂੰ ਬੜੀ ਖੁਸ਼ੀ ਹੁੰਦੀ ਂ ਐ ਔਰ ਜੇ ਨਹੀਂ ਲੰਭਦੇ ਤਾਂ ਫੇਰ ਅਸੀਂ ਤੁਹਾਨੂੰ ਵੀ ਮਾਯੁਸਿ ਹੂੰਦੀ ਏ ਲੇਕਿਨ ਅਸੀਂ ਤੁਹਾਨੂੰ ਉਦਾਸ ਨੀ ਕਰਣਾ ਚਾਹੂੰਦੇ । ਲੇਕਿਨ ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰਦੇ ਐ ਉਹਦੇ ਵਰਗਾ ਮਿਲਦਾ ਜੁਲਦਾ ਗੀਤ ਕੋਈ ਨਾ ਕੋਈ ਜਰੂਰ ਤੁਹਾਡੇ ਨਾਂ ਕਰ ਦੇਈਏ । ਫਿਲਹਾਲ ਚਲਦੇ ਐ ਅਸੀਏਸ ਗੀਤ ਵੱਲ ਏਸ ਤੋ ਬਾਅਦ ਦਿਲਦਾਰ ਸਿੰਘ ਪਰਦੇਸੀ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਕ ਗੀਤ ਕਰਾਂਗੇ ਤੁਹਾਡੇ ਸਭ ਦੇ ਨਾਂ ਕਰਾਂਗੇ ਇਕ ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਐ । ਜੋ ਕੇ ਜਗਦੀਸ਼ ਸਾਨੂੰ ਬਰ ਘਿੰਗਮ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਸੀ ਉਹਨਾਂ ਨੇ ਵੀ ਇਸਦੀ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਦਿਲਦਾਰ ਪਰਦੇਸੀ ਦੀ ਆਵਾਜ਼ ਸੁਨਣ ਵਾਸਤੇ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="17" who="PM001">ਜੀ ਹਾਂ ਜਨਾਬ ਏਸ ਵਕਤ ਦਸ ਮਿਨਟ ਬਾਕੀ ਨੇ ਨੌ ਵੱਜਣ ਵਿਚ ਸਾਡਾ ਤੇ ਤੁਹਾਡਾ ਸਾਥ ਪੂਰੇ ਦਸ ਵਜੇ ਤੱਕ ਰਹੇਗਾ ਸਾਨੂੰ ਬਹੁਤ ਸਾਰੇ ਸਰੋਤੇ ਫੋਨ ਕਰ ਰਹੇ ਨੇ ਔਰ ਫਰਮਾਇਸ਼ ਭੇਜ ਰਹੇ ਨੇ ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰ ਰਹੇ ਆਂ ਜਨਾਬ ਬਲਕਿ ਮੈਂ ਵੀ ਕਰ ਰਿਹਾਂ ਐ ਸ਼ੈਫਾਲੀ ਜੀ ਵੀ ਬਹੁਤ ਕਰ ਰਹੀ ਐ ਜਿਹੜਾ ਗੀਤ ਤੁਸੀਂ ਕਹਿਨੇ ਐ ਸੁਣਨ ਵਾਸਤੇ ਅਸੀਂ ਉਹ ਤੁਹਾਨੂੰ ਸੁਣਾ ਸਕੀਏ । ਜਗਦੀਸ਼ ਜੀ ਤੁਸੀਂ ਬਰਘਿੱਗਮ ਤੋ ਫੋਨ ਕੀਤਾ ਸੀ ਏਸ ਤੋ ਪਹਿਲਾਂ ਸਾਨੂੰ । ਹੋਰ ਵੀ ਸਾਡੇ ਸਰੋਤਿਆਂ ਨੇ ਫੋਨ ਕੀਤਾ ਸੀ ।ਹਾਂ ਦਿਲਦਾਰ ਸਿੰਘ ਪਰਦੇਸੀ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਕ ਗੀਤ ਸੁਨਨਾ ਚਾਹਿਆ ਉਮੀਦ ਐ ਕੇ ਇਹ ਗੀਤ ਤੁਹਾਨੂੰ ਸਭ ਨੂੰ ਪਸੰਦ ਆਏਗਾ । ਬੜਾ ਪਿਆਰ ਭਰਿਆ ਗੀਤ ਐ ਏਹੇ । ਕਿਸੇ ਦੀ ਕਲਪਨਾ ਕਿਸੇ ਦੇ ਵਿਚਾਰ ਨੇ ਕਿਸੀ ਦੀ ਸੋਚ ਐ ਇਹ ਆਪਣੀ <foreign lang="eng">sentiments</foreign> ਆਪਣੀ ਐ ਕੀ ਸੁਨਣ ਲਈ ਤੇ ਕੀ ਲਿਖਣ ਲਈ ਕੇ ਉਹ ਕੀ ਸੋਚ ਰਿਹੈ । ਔਰ ਉਹ ਸੋਚਾਂ ਸੋਚਾਂ ਵਿਚ ਕੀ ਕਹਿ ਰਿਹੈ ।ਇਹ ਦਿਲਦਾਰ ਸਿੰਘ ਪਰਦੇਸੀ ਦੱਸਣਗੇ ਤੁਹਾਨੂੰ ਕੇ ਆਪਣੇ ਮਾਹੀ ਨੂੰ ਆਪਣੇ ਸਾਥੀ ਨੂੰ ਕੀ ਕਹਿੰਦੇ ਐ । ਯਾਂ ਕਹਿਣਗੇ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="18" who="PM001">ਲਓ ਜੀ ਇਹ ਤਾਂ ਗੀਤ ਸੀਗਾ ਦਿਲਦਾਰ ਸਿੰਘ ਪਰਦੇਸੀ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਜਗਦੀਸ਼ ਸਿੰਘ ਦੇ ਨਾ ਇਹਦੀ ਸਾਡੇ ਕੁਝ ਹੋਰ ਸਰੋਤਿਆਂ ਨੈ ਵੀ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਸੀ । ਲੱਕੀ ਸੋਲੰਕੀ ਜੀ ਬਰਘਿੰਗਮ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਨੇ ।ਤੁਸੀਂ <foreign lang="eng">program </foreign> ਮਨੋਰੰਜਨ ਕਰ ਰਹੇ ਓ ਤੁਸੀਂ ਕਿਹੈ ਇਕ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ ਸੁਨਾ ਦਿਓ । ਅਗਲਾ ਗੀਤ ਤੁਹਾਡੇ ਨਾਂ ਕਰਾਂਗੇ । ਅਤੇ ਸ਼ੀਲਾ ਜੀ ਬਰਘਿੰਗਮ ਤੋ ਤੁਸੀਏ ਹੰਟੀਜ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਗੀਤ ਸੁਨਨਾ ਚਾਹਿਆ ਐ ਮੈਨੂੰ ਲੱਗਦੈ ਕੇ ਏਹੀ ਗੀਤ ਐ । ਇਹ ਗੀਤ ਸਾਨੂੰ ਨੌ ਵਜੇ ਦੀਆਂ ਖਬਰਾਂ ਤੱਕ ਪਹੂੰਚਾਏਗਾ । ਖਬਰਾਂ ਤੋ ਬਾਅਦ ਫੇਰ ਮੁਲਾਕਾਤ ਹੋਏਗੀ <foreign lang="eng">program </foreign> ਦੇ ਦੂਸਰੇ ਭਾਗ ਵਿਚ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="19" who="PM001">ਐਸ ਵਕਤ ਸਾਡੇ <foreign lang="eng">studeo</foreign>
ਦੀ ਘੜੀ ਦੇ ਮੁਤਾਬਿਕ ਢੀਕ ਨੌੰ ਵੱਜੇ ਹਨ ਹੁਣ ਤੁਸੀਏ ਖ਼ਬਰਾਂ ਸੁਣੋਗੇ ਸੁਰਿੰਦਰ ਕੌਰ ਦੀ ਜਬਾਨੀ ।</u>

<u id="20" who="PF021">ਅੱਜ ਦੇ ਮੁਖ ਸਮਾਚਾਰ :-ਸੁਣੇਗੇ । ਅੱਜ ਦੇ ਮੁਖ ਸਮਾਚਾਰ :-</u>

<u id="21" who="PF021">ਮੌਸਕੋ ਵਿਚ ਇਕ <omit extent="1 word" cause="sound quality"></omit> ਦੌਰਾਨ ਬੰਬ ਧਮਾਕਾ ।</u> 

<u id="22" who="PF021"><foreign lang="eng"> C. I. A . officers </foreign> ਇਕ ਟੇਪ ਦੀ ਜਾਂਚ ਕਰ ਰਹੇ ਹਨ ।</u> 

<u id="23" who="PF021">ਬਗਦਾਦ ਵਿਚ ਇਕ ਜੋਰਦਾਰ ਬੰਬ ਧਮਾਕੇ ਵਿਚ ਸੱਠ ਤੋ ਜਿਆਦਾ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ।</u> 

<u id="24" who="PF021">ਬ੍ਰਿਕਮੇਨ ਤੇ ਢਾਈ ਮਿਲਿਅਨ ਪੌਂਡ ਖਰਚ ਕਰਣ ਦਾ ਵਾਦਾ ਕਿਤਾ ਗਿਆ ਹੈ । ਸਾਰਸ ਦੀ ਬਿਮਾਰੀ ਨਤੇ ਹੁਣ ਕਾਬੂ ਹੋ ਗਿਆ ਹੈ । ਹੁਣ ਖ਼ਬਰਾਂ ਵੇਰਵੇ ਨਾਲ ।</u>

<u id="25" who="PF021">ਮਾਸਕੋ ਵਿਚ ਰਾਤ ਨੂੰ <omit extent="1 word" cause="sound quality"></omit> ਦੀ ਭੀੜ ਵਿਚ ਹੋਏ ਇਕ ਆਤਮਘਾਤੀ ਬੰਬ ਧਮਾਕੇ ਨਾਲ ਸੋਲਾਂ ਲੋਕ ਮਾਰੇ ਗਏ ਹਨ ਅਤੇ ਕਈ ਹੋਰ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ਹਨ । ਇਹ ਹਮਲਾ ਦੌ ਔਰਤਾਂ ਨੇ ਕੀਤਾ । ਏਸ ਹਮਲੇ ਦੀ ਜਿਮੇਵਾਰੀ ਚੇਚਨ <foreign lang="eng">militants</foreign> ਨੂੰਦਿੱਤੀ ਗਈ ਹੈ ।</u>

<u id="26" who="PF021">ਕੱਲ ਪਾਕਿਸਤਾਨ ਕੋਟਾ ਵਿਚ ਮੋਸਕ ਤੇ ਹੋਏ ਹਮਲੇ ਦੌਰਾਨ ਕਈ ਲੋਕ ਮਾਰੇ ਗਈ ਸਨ । ਹੁਣ ਉਹਨਾਂ ਦੇ ਪਰਿਵਾਰਾਂ ਨੇ ਮਰਣ ਵਾਲਿਆਂ ਨੂੰ ਦਫਨਾਉਣਾ ਸ਼ੁਰੂ ਕਰ ਦਿੱਤਾ ਹੈ । ਇਹ ਹਮਲਾ ਸ਼ੀਆ ਮੁਸਲਿਮ ਸ਼ਰਧਾਲੁਆਂ ਤੇ ਕੀਤਾ ਗਿਆ ਸੀ । ਕੋਟ ਵਿਚ <foreign lang="eng">officials</foreign> ਨੇ ਦੱਸਿਆ ਹੈ ਕੇ ਮਰਣ ਵਾਲਿਆਂ ਦੀ ਗਿਣਤੀ ਤਰਵੰਜਾ ਹੋ ਚੁਕੀ ਹੈ । ਇਸ ਹਮਲੇ ਦੀ ਜਿਮੇਵਾਰੀ ਸੁਨੀ ਮੁਸਲਮਾਨ <foreign lang="eng">extrimist</foreign> ਦੀ ਲੱਗਦੀ ਹੈ ।</u>

<u id="27" who="PF021">ਜਿਹਨਾਂ ਸੱਤ ਨਵੇ <foreign lang="eng">Irakies police force</foreign> ਦੇ ਆਦਮੀਆਂ ਨੂੰ <foreign lang="eng">Americans</foreign> ਵਲੋਂ <foreign lang="eng">training</foreign> ਦਿੱਤੀ ਗਈ ਸੀ ਉਹ ਇਕ ਬੰਬ ਧਮਾਕੇ ਵਿਚ ਮਾਰੇ ਗਏ ਹਨ । ਇਸ ਜੋਰਦਾਰ ਧਮਾਕੇ ਵਿਚ ਹੋਰ ਵੀ ਸੱਠ ਤੋ ਜਿਆਦਾ ਹੋਰ ਲੋਕ ਵੀ ਜਖਮੀ ਹੋ ਗੇਏ ਹਨ ।ਅਤੇ ਕਈਆਂ ਦੀ ਹਾਲਤ ਗੰਭੀਰ ਦੱਸੀ ਗਈ ਹੈ ।</u>

<u id="28" who="PF021"><foreign lang="eng">C. I. A. officers</foreign> <omit extent="1 word" cause="sound quality"></omit> ਇਕ <foreign lang="eng">A. D. A. tape</foreign> ਦੀ ਜਾਂਚ ਕਰ ਰਹੇ ਹਨ ਉਹ ਪਤਾ ਲਗਾਉਣਾ ਚਾਹੂੰਦੇ ਹਨ ਇਹ ਆਵਾਜ਼ ਸੱਚ ਮੁਚ ਪਹਿਲੇ ਇਰਾਕੀ <foreign lang="eng">leaders</foreign> ਦੀ ਹੈ ਯਾਂ ਨਹੀਂ । ਇਹ <foreign lang="eng">report Arebic televison Al-Jajira</foreign> ਤੋ ਦਿਖਾਈ ਗਈ ਹੈ । ਇਸ ਵਿਚ ਇਰਾਕੀਆਂ ਨੂੰ <foreign lang="eng">Police force</foreign> ਦਾ ਵਿਰੋਧ ਕਰਣ ਲਈ ਕਿਹਾ ਗਿਅ ਹੈ । <foreign lang="eng">U. S. officer Shoan Gibson </foreign> ਨੇ ਕਿਹਾ ਹੈ ਕੇ ਉਹਨਾਂ ਦਾ ਖਿਆਲ ਹੈ ਕੇ ਇਹ ਟੇਪ ਅਸਲੀ ਨਹੀਂ ਹੈ । ਇਹ ਖ਼ਬਰਾਂ ਤੁਸੀਂ ਸੁਰਿੰਦਰ ਕੌਰ ਦੀ ਜਬਾਨੀ ਬੀ ਬੀ ਸੀ ਏਸ਼ੀਅਨ ਨੇਟਵਰਕ ਤੇ ਸੁਣ ਰਹੇ ਹੋ ।</u>

<u id="29" who="PF021"><foreign lang="eng">World Health orgenisition</foreign> ਨੇ ਇਹ ਐਲਾਨ ਕੀਤਾ ਹੈ ਕੇ ਹੁਣ <foreign lang="eng">sars virous</foreign> ਤੇ ਕਾਬੂ ਪਾ ਲਿਆ ਗਿਆ ਹੈ । ਇਸ ਸੰਸਥਾ ਨੇ ਡੋਕਟਰਸ ਅਤੇ ਸਾਇੰਸਦਾਨਾ ਦੇ ਕੰਮ ਦੀ ਬਹੁਤ ਪ੍ਰਸ਼ੰਸਾ ਕੀਤੀ ਹੈ । ਇਸ ਬਿਮਾਰੀ ਨਾਲ ਪਰਭਾਵਿਤ ਹੋਏ ਦੇਸ਼ਾਂ ਦੀ <foreign lang="eng">list</foreign> ਤੋ ਹਟਾਉਣ ਲਈ ਭਾਰਤ ਹੁਣ ਆਖਿਰੀ ਦੇਸ਼ ਸੀ ।</u>

<u id="30" who="PF021"> ਬ੍ਰਿਕਮੈਨ ਦੀ ਮਦਰ ਹੈਰੀ ਟੇਜ ਨੂੰ ਸੰਭਾਲ ਕੇ ਰੱਖਣ ਲਈ ਇਕ <foreign lang="eng">re- genration</foreign> ਸਕੀਮ ਕਾਇਮ ਕਰ ਰਹੀ ਹੈ । ਬ੍ਰਿਕਮੈਨ ਨੇ ਕੜੀ ਮਅਇਲ ਦੇ ਨਾਮ ਨਾਲ ਵੀ ਜਾਨਿਆ ਜਾਂਦਾ ਹੈ । ਇਸ ਇਲਾਕੇ ਵਿਚ ਕੋਈ ਸੱਤਰ ਹਜਾਰ ਬੰਗਲਾ ਦੇਸ਼ੀ ਲੋਕਾਂ ਦੀ <foreign lang="eng">comunity</foreign> ਹੈ । ਇਸ ਕਮਜੋਰ ਇਲਾਕੇ ਦੇ <foreign lang="eng">bussiness</foreign> ਵਿਚ ਫ਼ਾਇਦਾ ਹੋਵੇਗਾ ਤੇ <foreign lang="eng">central London</foreign> ਵਿਚ <foreign lang="eng">property brokers</foreign> ਦੀ ਬਰਾਬਰੀ ਹੋਵੇਗੀ । ਬ੍ਰਿਕਮੈਨ ਦੇ ਢਾਈ ਮਿਲਿਅਨ ਪੌਂਡ ਦੁਕਾਨਾਂ ਦੇ <foreign lang="eng">repair</foreign> ਕਰਣ ਤੇ ਖਰਚ ਕਰਣ ਦਾ ਵਾਦਾ ਕਿਤਾ ਗਿਆ ਹੈ ।</u>

<u id="31" who="PF021"><foreign lang="eng">united states</foreign> ਤੋ ਸੋਰੈਨਾ ਵਿਲਿਅਮ ਨੇ ਆਪਣਾ ਵਿਮਬਲਡਨ <foreign lang="eng">singal's tital</foreign> ਕਿਤਾਬ ਬਰਕਰਾਰ ਰੱਖਿਆ ਹੈ । ਉਸਨੇ ਆਪਣੀ ਭੇਣ ਵੀਨਸ ਨੂੰ <foreign lang="eng">three sets</foreign> ਵਿਚ ਹਰਾਇਆ ਹੈ । ਜਿ ਕੇ <foreign lang="eng">Four to six, six to four, six ti two</foreign> ਨਲ ਛੇ ਨੰਬਰਾਂ ਚ <foreign lang="eng">singal's championship</foreign> ਚ ਜਿਤ ਪ੍ਰਾਪਤ ਕੀਤੀ ਹੈ । ਵੀਨਸ ਵਿਲਿਅਮ ਦੇ ਢਿਡ ਵਿਚ ਦਰਦ ਸੀ ਜਦੋ ਉਹ ਮੈਚ ਖੇਲਣ ਲਈ ਗਈ ਸੀ । ਉਹ ਕੁਝ ਮਿੰਟਾ ਲਈ ਕੋਰਟ ਛੱਡ ਕੇ ਗਈ ਸੀ ।</u>

<u id="32" who="PF021">ਹੁਣ ਪੇਸ਼ ਹੈ ਮੌਸਮ :-ਰਾਤ ਨੂੰ ਮੌਸਮ ਖੁਸ਼ਕ ਹੋਵੇਗਾ । ਹਲਕੀ ਹਲਕੀ ਹਵਾ ਚਲੇਗੀ । ਘੱਟ ਤੋਂ ਘੱਟ ਤਾਪਮਾਨ ਗਿਅਰਾਂ ਡਿਗਰੀ ਸੈਲਸੀਅਸ ਬਵੰਜਾ ਡਿਗਰੀ ਫ਼ਾਰੇਨਹਾਇਟ ਹੋਵੇਗਾ । ਕੱਲ ਐਤਵਾਰ ਹੈ ਮੌਸਮ ਖੁਸ਼ਕ ਹੋਵੇਗਾ ਅਤੇ ਕਿਤੇ ਕਿਤੇ ਧੁੱਪ ਨਿਕਲੇਗੀ । ਹਲਕੀ ਹਲਕੀ ਹਵਾ ਚਲੇਗੀ । ਵੱਧ ਤੋ ਵੱਧ ਤਾਪਮਾਨ ਵੀਹ ਡਿਗਰੀ ਸੈਲਸੀਅਸ, ਅਠਾਟ ਡਿਗਰੀ ਫ਼ਾਰੇਨਹਾਇਟ ਹੋਵੇਗਾ ਸੋਮਵਾਰ ਨੁੰ ਮੌਸਮ ਖੁਸ਼ਕ ਅਤੇ ਧੁਪਦਾਰ ਹੋਵੇਗਾ । <foreign lang="eng">B. B. C Asian Network.</foreign> ਤੋ ਖ਼ਬਰਾਂ ਖਤਮ ਹੋਈਆਂ ।</u> 

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="33" who="PM001">ਜੀ ਹਾਂ <foreign lang="eng">program </foreign> ਦੇ ਦੂਸਰੇ ਭਾਗ ਵਿਚ ਤੁਹਾਡਾ ਸਵਾਗਤ ਹੈ ਦਲਜੀਤ ਮੀਰ ਵਲੋ ਅਤੇ ਮੇਰੇ ਸਾਤੀ ਜਿਹੜੇ ਮੇਰਾ ਸਾਥ ਦੇ ਰਹੇ ਨੇ ਸ਼ੈਫਾਲੀ ਔਰ ਤੁਹਾਡੇ ਪੈਗ਼ਾਮ ਮੇਰੇ ਤੱਕ ਪਹੂੰਚਾ ਰਹੇ ਨੇ <foreign lang="eng">08 459 440 445</foreign> ਹੈ ਸਾਡਾ ਨੰਬਰ ਕਈ ਵਾਰੀ ਬੰਦਾ ਰੌਬ ਦੇ ਵਿਚ ਆਕੇ ਬਹੁਤ ਸਾਰੀਆਂ ਗੱਲਾਂ ਕਹਿ ਜਾਂਦਾ ਐ ਲੇਕਿਨ ਕਈ ਵਾਰੀ ਰੌਬ ਦੇ ਵਿਚ ਵੀ ਪਿਆਰ ਹੂੰਦੈ ਔਰ ਉਹ ਪਿਆਰ ਝਲਕਦੈ ਨਜ਼ਰਾਂ ਨਾਲ ਗੁਸੇ ਦੇ ਵਿਚ ਪਿਆਰ <vocal who="PM001" desc="laugh"></vocal> ਝਲਕਦੈ । ਕਿਸੇ ਕਿਸੇ ਨੂੰ ਨਜ਼ਰ ਆਉਦੈ ਲੇਕਿਨ ਕੋਈ ਕੋਈ ਹੂੰਦੈ ਜਿਹੜਾ ਐਦਾਂ ਕਹਿਦੈ ਹੂੰਦੈ ਜਿਹਰਾ ਸੁਨ ਵੀ ਨਾ ਸਕੇ ।</u>

<u id="34" who="PM001">ਵੇ ਤੁੰ ਕੀ ਥਾਨੇਦਾਰ ਲੱਗਿਐ, ਜਿਹੜਾ ਮੇਰੇ ਤੇ ਰੌਬ ਚਲਾਏਂਗਾ । ਰੌਬ ਤਾਂ ਹੀ ਚਲਾ ਸਕਦੈ ਬੰਦਾ ਜੇ ਕਿਸੇ ਨੂੰ ਮਾਨ ਹੋਵੇ ਕਿਸੇ ਤੇ, ਅਗਰ ਪਿਆਰ ਐ ਤੇ ਦਿਲੋ ਚਾਹੂਦੈ ਕਿਸੇ ਨੂੰ । ਐਵੇਂ ਕਿਸੇ ਤੇ ਰੌਬ ਨੀ ਨਾ ਚਲਾ ਸਕਦਾ । ਸਾਡੇ ਕੋਲ ਕੁਛ <vocal desc="er"></vocal><foreign lang="eng">dadication</foreign> ਐ ਰੈਨਾ ਜੀ ਤੁਸੀਂ ਫੋਨ ਕੀਤੈ <vocal desc="er"></vocal> <foreign lang="eng">common street</foreign> ਤੋ ਰੈਨਾ ਜੀ ਤੁਹਾਡੇ ਦੋਸਤ ਹੈਗੇ ਨੇ ਸੈਂਡੀ ਔਰ ਚੰਦਨ ਅਤੇ <vocal desc="er"></vocal> ਪਦਮੇਸ਼ ਤੁਸੀਂ ਐਸ ਵਕਤ ਸਾਡਾ <foreign lang="eng">program </foreign> ਸੁਣ ਰਹੇ ਓ <foreign lang="eng">Injoy</foreign> ਕਰ ਰਹੇ ਓ ਤੁਸੀਂ ਕਿਹੈ ਪੁਰਾਨੀ ਸੀ ਦੀ ਵਿਚੋ ਸੁਰਜੀਤ ਖਾਂ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਕ ਗੀਤ ਤੁਸੀ ਸੁਨਣਾ ਚਾਹੂੰਦੇ ਓ । ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਤੁਸੀਂ ਐਸ ਵਕਤ ਸਾਡਾ <foreign lang="eng">program </foreign> <foreign lang="eng">Injoy</foreign> ਕਰ ਰਹੇ ਓ, ਔਰ ਸਾਨੂੰ ਯਾਦ ਕਰ ਰਹੇ ਓ । ਲਓ ਜੀ ਚਲਦੇ ਐ ਗੀਤ ਵੱਲ ਗੀਤ ਤੁਹਾਡੇ ਦੋਸਤ ਸੈਂਡੀ, ਪਦਮੇਸ਼ ਅਤੇ ਚੰਦਨ ਦੇ ਨਾਂ । ਰੱਖਿਆ ਗਿਆ ਐ <foreign lang="eng">Thank you </foreign> ਜੀ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="35" who="PM001">ਸੁਰਜੀਤ ਖਾਂ ਦੀ ਆਵਾਜ਼ ਰੈਨਾ ਜੀ ਤੁਹਾਡੇ ਨਾਂ ਅਤੇ ਤੁਹਾਡੇ ਦੋਸਤ ਸੈਂਡੀ, ਚੰਦਨ ਅਤੇ ਪਦਮੇਸ਼ ਦੇ ਨਾਂ ਇਹ ਗੀਤ ਕੀਤਾ । ਅਗਲਾ ਗੀਤ ਨਿਰਮਲ ਸਿੰਘ ਸਟਿੰਫਹਰਟ ਤੋ ਸਾਨੂੰ ਫੋਨ ਕਰ ਰਹੇ ਸਨ ਬੁਰਾ ਨਾ ਮੋਨਿਓ ਤੁਸੀਂ ਸੁਖਵਿੰਦਰ ਸਿੰਘ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਕੋਈ ਗੀਤ ਸੁਨਣਾ ਚਾਹੂੰਦੇ ਓ । ਲਓ ਜੀ ਅਬੀ ਗੀਤ ਜਿਹੜਾ ਤੁਸੀਏ ਦੱਸਿਆ ਐ ਉਹ ਵੀ ਸੁਨਾਵਾਂਗੇ ਅਸੀਂ ਤੁਹਅਨੂੰ ਫਿਲਹਾਲ ਅਸੀਂ ਅਗਲਾ ਗੀਤ ਤੁਹਾਨੂੰ ਚਲਤੇ ਚਲਤੇ ਸੁਣਾਵਾਗੇ । ਔਰ ਇਸਦਾ ਔਂਕਾਰ ਸਿੰਘ ਜੀ ਬਰਘਿੰਗਮ ਤੋ ਫੋਨ ਕੀਤੈ ਤੁਸਂੀ ਵੀ ਸਾਡੇ ਜਿਹੜੇ <foreign lang="eng">frinds</foreign> ਨੇ ਰਾਣਾ ਮਿੰਟੂ ਅਤੇ ਸੋਨੂੰ । ਤੁਸੀਂ ਕਿਹੈ ਕੇ ਮਿੰਟੂ ਦਾ ਅੱਜ ਜਨਮ ਦਿਨ ਐ । ਉਹਨੂੰ <foreign lang="eng">Happy birth day </foreign> ਕਹਿ ਰਹੇ ਓ ਔਰ ਉਹਨੂੰ ਇਕ ਕਲੀ ਸੁਨਣਾ ਚਾਹੂਦੇ ਓ ਹਰਭਜਨ ਮਾਨ ਦੀ ਆਵਾਜ਼ ਵਿਚ । ਇਹ ਵੀ ਸੁਨਾਵਾਂਗੇ ਅਸੀਂ ਤੁਹਾਨੂੰ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਤੁਸੀਂ ਸਾਨੂੰ ਯਾਦ ਕੀਤਾ । ਸੁਖਵਿੰਦਰ ਜੀ ਤੁਸੀਂ ਵੁਲਵਅਹੰਟਨ ਤੋ ਸਾਡਾ <foreign lang="eng">program </foreign> ਸੁਣ ਰਹੇ ਓ । ਅਤੇ ਤੁਸੀਂ ਸਾਡੀ ਆਵਾਜ਼ ਬਾਰੇ ਵੀ ਕੁਝ ਕਿਹਾ ਐ । ਇਹ ਤੇ ਨਹੀ ਐ ਬਹਰਹਾਲ ਸੁਖਵਿੰਦਰ ਸਿੰਘ <vocal who="PM001" desc="laugh"></vocal> ਜੀ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਤੁਸੀਂ ਸਾਡੇ <foreign lang="eng">program </foreign> ਸੁਣ ਰਹੇ ਓ । ਇਹ ਗੀਤ ਤੁਹਾਡੇ ਨਾ ਵੀ ਕਰਾਂਗੇ ਅਸੀਂ । ਸੁਖਵਿੰਦਰ ਸਿੰਘ ਦੀ ਆਵਾਜ਼ ਵਿਚ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event> 

<event desc="music" dur="approximately 10 seconds"></event>

<u id="36" who="PM001">ਸੁਖਵਿੰਦਰ ਸਿੰਘ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਫਿਲਮ ਚਲਤੇ ਚਲਤੇ ਦੇ ਵਿਚੋ ਇਹ ਗੀਤ ਲਿਆ ਸੀ ਅਸੀਂ । ਉਮਿਦ ਐ ਤੁਹਾਨੂੰ ਪਸੰਦ ਆਇਆ ਹੋਨੈ । ਤੇਰੀ ਮੇਰੀ ਇਂਉ ਟੁੱਟਗੀ ਸੋਹਣਿਏ ਜਿਵੇਂ ਟੁਟਿਆ ਅੰਬਰ ਚੋ ਤਾਰਾ । ਕਦੀ ਕਦੀ ਇੰਝ ਵੀ ਹੂੰਦੈ ਜਨਾਬ । ਚੁਪ ਕਰਕੇ ਬੇੜੀ ਬਹਿ ਜਾਣਾ ਤੇਨੂੰ ਖਬਰ ਨੀ ਹੋਣ ਦੇਣੀ । ਏਸ ਤਰਾਂ ਦੀਆਂ ਵੀ ਗੱਲਾਂ ਬਾਤਾਂ ਹੂੰਦੀਆਂ ਨੇ ਏਸ ਤਾਰਂ ਅਜੀਬ ਅਜੀਬ ਕਿੱਸੇ ਹੂੰਦੇ ਨੇ ਜਨਾਬ । ਔਰ ਫੇਰ ਏਸ ਤਾਰਂ ਦੇ ਗੀਤ ਗਾਏ ਜਾਂਦੇ ਨੇ ਤੇ ਸੁਣੇ ਜਾਂਦੇ ਨੇ ਏਸ ਤਾਰਂ ਦੇ ਗੀਤ ਲਿਖੇ ਜਾਏਦੇ ਨੇ ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਏ ਸੁਣਿਆ ਸੁਖਵਿੰਦਰ ਸਿੰਘ ਦੀ ਆਵਾਜ਼ ਵਿਚ । ਕੁਛ ਫਰਮਾਇਸ਼ਾਂ ਹੋਰ ਨੇ ਕੁਛ ਪੈਵਾਮ ਹੋਰ ਨੇ ਇਸ ਤੋ ਬਾਅਦ ਵਿਚ ਪੇਸ਼ ਕਰਾਂਗੇ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="37" who="PM001">ਜੀ ਹਂਾ ਏਹਨਾਂ ਦਾ <foreign lang="eng">program </foreign>, ਅੰਜੁਮ ਰਸ਼ੀਦ ਦਾ <foreign lang="eng">program </foreign> ਹੂੰਦੈ <foreign lang="eng">Monday to Friday</foreign> ਔਰ ਤੁਸੀਂ <foreign lang="eng">Talk time</foreign> ਤੇ ਇਹਨਾਂ ਦਾ <foreign lang="eng">program </foreign> ਸੁਣ ਸਕਦੇ ਐ ਔਰ ਮੈਂ ਤੁਹਾਨੂੰ ਇਕ ਸੂਚਨਾ ਹੋਰ ਵੀ ਤੁਹਾਨੂੰ ਦੱਸਾਂ ਕੇ ਕੱਲ ਸ਼ਾਮ ਚਾਰ ਵਜੇ ਅਸ਼ਵਿਨ ਬਲੀ <omit extent="1 word" cause="sound quality"></omit> ਪੇਸ਼ ਕਰਣਗੇ <omit extent="1 word" cause="sound quality"></omit> । ਉਹਨਾਂ ਦਾ <foreign lang="eng">program </foreign> ਤੁਸੀਂ ਜਰੂਰ ਤੁਸੀਂ ਸੁਣਿਆ ਕਰਿਓ ਕੱਲ ਸ਼ਾਮੀ ਚਾਰ ਵਜੇ । ਕੰਲਅਸ਼ਵਿਨ ਬੱਲੀ ਦਾ <foreign lang="eng">program </foreign> ਸੁਨਿਓ ਚਾਰ ਵਜੇ । ਔਰ ਸਪਨਾ ਦੱਤਾ ਜੀ ਹਿੰਦੀ ਚ <foreign lang="eng">program </foreign> ਕਰਦੇ ਨਰ ਦੋਪਹਿਰ ਦੌ ਵਜੇ ਉਹ ਵੀ <foreign lang="eng">program </foreign> ਬਹੁਤ ਅੱਛਾ ਹੂੰਦੈ । ਉਹ ਵੀ ਤੁਸੀਏ <foreign lang="eng">program </foreign> ਜਰੂਰ ਸੁਨਣਾ । ਔਰ <vocal desc="er"></vocal> ਅਕਰਮ ਬਾਈ <omit extent="1 word" cause="sound quality"></omit> ਮਤਲਬ ਰਾਤ ਨੂੰ ਉਹਨਾਂ ਦੇ ਵੀ <foreign lang="eng">program </foreign> ਹੂਦੇ ਤੁਸੀਂ ਜਰੂਰ ਸੁਨੀਓ । ਔਰ ਬਹੁਤ ਸਾਰੇ ਸਾਡੇ ਜਿਹੜੇ ਸਾਡੇ ਏਜੈਂਟ ਨੇ ਉਹ ਵੀ ਮਤਲਬ <foreign lang="eng">Monday to Friday</foreign> ਸਾਡੇ ਪਰੋਗਰਾਮ ਹੁਦੇ ਨੇ ਜਿਹੜੇ ਕੇ <foreign lang="eng">Break Fast </foreign> ਤੋ ਲੈ ਕੇ ਦੁਪਹਿਰ ਤੱਕ । ਔਰ ਕਿਸੇ ਸਮੇ ਧੀਰੇ <foreign lang="eng">program </foreign> ਸੁਣਦੇ ਓ ਲੰਦਨ ਤੋ ਔਰ ਸਾਡੇ ਜਿਨੇ ਵੀ ਸੈਂਟਰ ਐ, ਵੱਖੋ ਵੱਖ ਜਬਾਨਾ ਦੇ ਵਿਚ <foreign lang="eng">language programme</foreign> ਹੋਰ ਸਾਡੇ ਕਈ <foreign lang="eng">program </foreign> ਹੂਔਦੇ ਨੇ ਉਹਨਾਂ ਦੇ ਵੀ ਤੁਸੀਏ ਲੁਤਫ ਉਠਾਉਦੇ ਓ ਅਲੱਗ ਅਲੱਗ । ਤੁਸੀਂ ਆਪਣੇ <foreign lang="eng">community</foreign> ਦੇ ਵਿਚ ਕੁਛ ਵੀ ਮਤਲਬ ਕੋਈ <foreign lang="eng">Function</foreign> ਹੋ ਰਿਹੈ ਹੈ ਉਹਦੇ ਵਾਸਤੇ ਤੁਸੀਏ ਕੋਈ ਸੂਚਨਾ ਦੇਣਾ ਚਾਹੁਦੇ ਓ ਯਾਂ <foreign lang="eng">Advertisement</foreign> ਵਾਸਤੇ ਤੁਸੀ ਏਹ ਆਪਣੀ ਸੁਛਨਾ ਦੇਣਾ ਚਾਹੂੰਦੇ ਓ ਯਾਂ <foreign lang="eng">Information</foreign> ਦੇਣਾ ਚਾਹੂੰਦੇ ਓ । ਤੁਸੀਏ ਜਰੂਰ ਸਾਨੂੰ ਦੱਸ ਸਕਦੇ ਓ ਚਿੱਠੀ ਪਾ ਸਕਦੇ ਓ ਅਸੀਏ ਜਰੂਰ ਤੁਹਾਡੀਆਂ ਸੂਚਨਾਵਾਂ, ਤੁਹਾਡੀ <foreign lang="eng">information</foreign> ਆਪਣੇ <foreign lang="eng">program </foreign> ਦੇ ਜਰਿਏ ਜਰੂਰ ਪੇਸ਼ ਕਰਾਂਗੇ । ਨੌਂ ਵੱਜ ਕੇ ਤੇ ਬਾਈ ਮਿਨਟ ਹੋ ਰਹੇ ਹਨ <foreign lang="eng">program </foreign> ਨੂੰ ਅਸੀਂ ਦਸ ਵਜੇ ਤੱਕ ਲੈ ਕੇ ਜਾਵਾਂਗੇ, ਇਹ ਪੰਜਾਬੀ <foreign lang="eng">program </foreign> ਚਲੇਗਾ ਦਸ ਵਜੇ ਤੱਕ ਤੇ ਦਸ ਵਜੇ ਖ਼ਬਰਾਂ ਹੋਣਗੀਆਂ ਤੇ ਖ਼ਬਰਾਂ ਤੋ ਬਾਅਦ ਤੁਸੀਂ ਗੁਜਰਾਤੀ <foreign lang="eng">program </foreign> ਤੁਸੀਂ ਸੁਣਦੇ ਓ । <vocal desc="er"></vocal> ਅਗਲੇ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਸਾਨੂੰ ਕੁਛ ਦੇਰ ਪਹਿਲਾਂ ਕੀਤੀ ਸੀ ਆਓ ਚਲਦੇ ਐ ਅਸੀਂ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="38" who="PM001">ਇਹ ਗੀਤ ਸੁਣਿਆ <vocal who="PM001" desc="laugh"></vocal> ਤੁਸੀਂ ਅਬਰਾਰ ਉਲ ਹੱਕ ਦੀ ਆਵਾਜ਼ ਵਿਚ । ਕਇਆਂ ਨੂੰ ਵਿਆਹ ਕਰਾਉਣ ਦਾ ਬੜਾ ਸ਼ੌਂਕ ਹੂੰਦੈ । ਇਹ ਉਹ ਲੱਡੂ ਐ ਜਿਹਨੇ ਖਾਧਾ ਉਹ ਵੀ ਪਛਤਾਇਆ ਜਿਹਨੇ ਨਹੀਂ ਖਾਧਾ ਉਹ ਵੀ ਪਛਤਾਇਆ । ਜਿਹਨਾਂ ਨੇ ਖਾਧਾ ਉਹ ਤੇ ਜਿਆਦਾ ਈ ਪਛਤਾਉਦੇ ਨੇ । ਏਸ ਤਰਾਂ ਪਛਤਾਉਦੈ ਫੇਰ ਉਹ <vocal who="PM001" desc="laugh"></vocal> ਬਹੁਤ ਭੈੜੀਆਂ ਭੈੜੀਆਂ ਆਵਾਜ਼ਾਂ ਕੱਢ ਕੇ ਬਹੁਤ ਜਿਆਦਾ ਅਵਾਜਾਂ ਹਸਦੇ ਹਸਦੇ । ਨੌਂ ਵੱਜ ਕੇ ਤੇ ਸਤਾਈ ਮਿੰਟ ਹੋਏ ਨੇ ਪੂਰੇ ਅਠਾਈ ਮਿੱੰਟ ਹੋਏ ਨੇ ਸਾਡੇ ਤੇ ਤੁਹਾਡੇ ਸਾਥ ਰਹੇਗਾ ਦਸ ਵਜੇ ਤੱਕ <foreign lang="eng">08 459 440 445</foreign> ਸਾਡਾ ਨੰਬਰ ਹੈ ਕੁਛ ਮਿਲਿਆਂ ਜੁਲਿਆਂ ਫਰਮਾਇਸ਼ਾਂ ਨੇ ਸਾਡੇ ਦੋਸਤਾਂ ਨੇ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਸੀ ।ਇਹ ਮਿੰਟੂ ਆਪਣਾ ਜਨਮ ਦਿਨ ਮਨਾ ਰਹੇ ਐ ਉਹਨਾਂ ਦੇਨਾਂ ਉਹ ਸੁਨਣਾ ਚਾਹੂੰਦੇ ਸੀ ਹਰਭਜਨ ਮਾਨ ਦੀ ਆਵਾਜ਼ ਦੇ ਵਿਚ ਉਹਨਾਂ ਦੇ ਦੋਸਤਾਂ ਨੇ ਸਾਨੂੰ ਫੋਨ ਕੀਤਾ ਸੀ ਔਂਕਾਰ ਜੀ । ਕਿਕਲੀ ਉਹਨਾਂ ਦੇ ਨਾਂ ਕਰਾਂਗੇ ਅਸੀਂ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="39" who="PM001">ਜੀ ਹਾਂ ਬਹੁਤ ਪਿਆਰਾ ਤੁਸੀਂ ਇਹ ਗੀਤ ਸੁਣਿਆ ਹੈ ਹਰਭਜਨ ਮਾਨ ਅਤੇ ਗੁਰਸ਼ਰਨ ਏਨਾ ਦੋਨਾਂ ਭਰਾਂਵਾਂ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਹ ਕਲੀ ਸੀ ਮਿਰਜੇ ਦੀ । ਔਰ ਸਾਡੇ ਦੋਸਤਾਂ ਨੇ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਐ ਰਾਜਾ ਸਾਨੂੰ ਨੌਟਿੰਘਮ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਐ ਉਹਨਾਂ ਨੇ ਸਾਨੂੰ ਇਕ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਐ, ਜੇ ਮੈਂ ਹੂੰਦੀ ਸੋਹਣਿਆ ਸੋਨੇ ਦੀ ਕਬੂਤਰੀ ਜਰੂਰ ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰ ਹੇ ਹੈਂ ਜੀ ਇਹ ਗੀਤ ਸਾਡੇ ਕੋਲ ਹੋਇਆ ਤੇ ਜਰੂਰ ਸੁਨਾਵਾਂਗੇ । ਨਹੀਂ ਤੇ ਕੋਈ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ ਤੁਹਾਡੇ ਨਾ ਕਰਾਂਗੇ ਇਹ ਗਫੂਰ ਸਾਹਿਬ ਤੁਸੀ ਨੋਟਿੰਘਮ ਤੋ ਫੋਨ ਕੀਤੈ ਤੁਸੀਂ ਅਤਾ ਉਲਾ ਖਾਂ ਸਾਹਿਬ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਕੋਈ ਗੀਤ ਸੁਨਨਾ ਚਾਹੂੰਦੇ ਓ । ਅਸੀ ਜਰੂਰ ਸੁਨਾਉਣੇ ਐ ਜੀ ਭਾਵੇਂ ਥੋੜਾ ਜਿਹਾ ਭਾਵੇਂ ਸੁਨਾਇਏ ਤੁਹਾਨੂੰ । ਜਰੂਰ ਥੋੜਾ ਜਿਹਾ ਸੁਣਾਵਾਂਗੇ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਤੁਸੀਂ ਸਾਨੂੰ ਯਾਦ ਕੀਤੈ । ਬਿੰਦਰਾ ਜੀ ਤੁਸੀਏ ਸਾਨੂੰ ਬ੍ਰਿਸਟਨ ਤੋ ਸਾਨੂੰ ਫੋਨ ਕੀਤੈ ਤੁਸੀਏ ਹੰਟਰਸ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਕ ਗੀਤ ਸੁਨਨਾ ਚਾਹੂੰਦੇ ਓ ਜਰੂਰ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਜੀ ਜਿਨਾ ਕ ਸਾਡੇ ਕੋਲ ਵਕਤ ਬਚਿਆ ਫਰਮਾਇਸ਼ਾਂ ਸਾਡੇ ਕੋਲ ਜਿਆਦਾ ਨੇ ਪਰਮਿੰਦਰ ਸੰਧੁ ਬਲਿਐਹਮਦ ਤੋ ਸਾਨੂੰ ਯਾਦ ਕਰ ਰਹੇ ਨੇ ਅਤੇ ਉਹਨਾਂ ਦੇ ਨਾਲ ਕੁਛ ਮਿੱਤਰ ਬੈਠੈ ਨੇ ਪਰਮਿੰਦਰ ਅਟਵਾਲ ਔਰ ਡਿਮਪਲ ਕੁਮਾਰ ਅਤੇ ਸੁੱਖੀ ਉਹਨਾਂ ਨੇ ਵੀ ਫਰਮਾਇਸ਼ ਕਰੀ ਐ ਜਨਾਬ ਤੁਹਾਡਾ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਕੇ ਤੁਸੀਂ ਏਸ ਵਕਤ ਸਾਨੂੰ ਯਾਦ ਕਰ ਰਹੇ ਓ । ਬੀ ਬੀ ਸੀ ਏਸ਼ੀਆਨ ਨੈਟਵਰਕ ਤੁਸੀਏ ਸੁਣ ਰਹੇ ਓ ਸਾਡੇ<foreign lang="eng">program </foreign> ਸੁਣਦੇ ਓ । ਗੁਲਾਮ ਅਲੀ ਸਾਹਿਬ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਤੁਸੀਏ ਗੀਤ ਸੁਨਨਾ ਚਾਹਿਆ ਐ ਕੋਈ ਇਹ ਗੀਤ ਤੁਹਾਡੇ ਨਾਂ ਕਰਾਂਗੇ ਸ਼ਿਵ ਕੁਮਾਰ ਬਟਾਲਵੀ ਦੀ ਰਚਨਾ, ਕੀ ਪੁਛਦੇ ਓ ਹਾਲ ਫਕੀਰਾਂ ਦਾ ਉਮੀਦ ਐ ਕੇ ਤੁਹਾਨੂੰ ਪਸੰਦ ਆਏਗਾ ਪਰਮਿੰਦਰ ਜੀ ਤੁਹਾਨੂੰ ਤੇ ਤੁਹਾਡੇ ਦੋਸਤ ਪਰਵਿੰਦਰ ਅਟਵਾਲ, ਡਿਮਪਲ ਅਤੇ ਸੁੱਖੀ ਨੂੰ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="40" who="PM001">ਕੀ ਪੁਛਦੇ ਓ ਹਾਲ ਫਕੀਰਾਂ ਦਾ ਸਾਡੇ ਨਦਿਓ ਵਿਛੜੇ ਨੀਰਾਂ ਦਾ ਸ਼ਿਵ ਕੁਮਾਰ ਬਟਾਲਵੀ ਦੀ ਰਚਨਾ ਔਰ ਗੁਲਾਮ ਅਲੀ ਸਾਹਿਬ ਦੀ ਆਵਾਜ਼ ਗੁਲਾਮ ਅਲੀ ਸਾਹਿਬ ਦਾ ਜਿਕਰ ਅਇਆ ਤਾਂ ਕੱਲ ਗੁਲਾਮ ਅਲੀ ਸਾਹਿਬ ਦੇ ਨਾਲ ਮੁਲਾਕਾਤ ਹੋਈ, ਫੋਨ ਦੇ ਜਰਿਏ । ਪਰਸੋਂ ਹੀ ਆਏ ਸਨ ਅਮੈਰਿਕਾ ਤੋ ਵਾਪਿਸ ਤੇ ਅੱਜ ਪਾਕਿਸਤਾਨ ਗਏ ਨੇ ਵਾਪਿਸ । ਔਰ ਜੁਲਾਈ <vocal desc="er"></vocal> <foreign lang="eng">end 0f this month</foreign> ਵਾਪਿਸ ਆ ਰਹੇ ਹਨ ਤੇ ਹੋ ਸਕਦੈ ਕੇ ਇਥੇ ਉਹਨਾਂ ਦੇ ਕੁਛ <foreign lang="eng">shows</foreign> ਵਗੈਰਹਾ ਹੌਣ ਤੇ ਜਰੂਰ ਤੁਹਾਨੂੰ ਪਤਾ ਲੱਗੂ <foreign lang="eng">B. B. C Asian Network.</foreign> ਦੇ ਜਰਿਏ । ਗੁਲਾਮ ਅਲੀ ਸਾਹਿਬ ਇਕ ਐਸਾ ਨਾਂ ਏ ਸਾਡੇ ਸ਼ਾਇਰ <vocal desc="er"></vocal> ਆਰਟਿਸਟਾਂ ਦੇ ਵਿਚ ਜਿਹਨਾਂ ਨੂੰ ਬੱਚਾ ਜਵਾਨ ਬਜੁਰਗ ਸਭ ਜਾਣਦੇ ਨੇ । ਔਰ ਇਤਨਾ ਮਾਹਿਰ ਐ ਇਹ ਫਨਕਾਰ ਜਿਹਨਾਂ ਨੇ ਆਪਣੀ ਆਵਾਜ਼ ਦੇ ਜਰਿਏ, ਆਪਣੇ ਖਤਾਂਬਾ ਦੇ ਆਪਣੀਆਂ ਰਚਾਨ ਦੇ, ਗੀਤਾਂ ਦੇ ਜਰਿਏ ਪੁਰੀ ਦੁਨਿਆਂ ਦਾ ਦਿਲ ਚ ਐ । ਸਾਡੇ ਕੋਲ ਕੁਛ ਪੈਗਾਮ, ਐ ਤਾਰਿਕ ਸਾਹਿਬ ਜੀ ਨੰੇਟਿੰਘਮ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਐ ਤੁਸੀਂ ਕਹਿ ਰਹੇ ਓ ਜਨਾਬ ਸ਼ੌਕਤ ਅਲੀ ਸਾਹਿਬ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਗੀਤ ਸੁਨਨਾ ਚਾਹਿਆ ਐ । ਅਸੀਂ ਘੜੀ ਵੱਲ ਵੀ ਵੇਖਨੇ ਐ ਤੇ ਤੁਹਾਡੀਆਂ ਫਰਮਾਇਸ਼ਂਾ ਵੱਲ ਵੀ ਵੇਖ ਰਹੇ ਆਂ ਲੇਕਿਨ ਕੌਸ਼ਿਸ਼ ਜਰੂਰ ਕਰਾਂਗੇ ਜਨਾਬ ਕੋਈ ਨਾ ਕੋਈ ਗਿਤ ਤੁਹਾਡੇ ਨਾ ਜਰੂਰ ਕਰੀਏ । ਹਰਜੀਤ ਸਿੰਘ ਜੀ ਸੰਧੂ ਤੁਸੀਂ ਵੀ ਫੋਨ ਕੀਤੈ ਲੈਸਟਰ ਤੋ ਬਹੁਤ ਬਹੁਤ ਤੁਹਾਡਾ ਧੋਨਵਾਦ ਐ <foreign lang="eng">program </foreign> <foreign lang="eng">enjoy</foreign> ਕਰ ਰਹੇ ਓ ਕਰਨੈਲ ਸਿੰਘ ਜੀ ਤੁਸੀਂ ਲੈਹਸਣ ਤੋ ਫੋਨ ਕੀਤੈ ਤੁਸੀਂ ਕਿਹੈ ਕੇ ਕੋਈ ਵੀ ਗੀਤ ਸੁਰਜੀਤ, <vocal desc="er"></vocal> ਸੁਰਜੀਤ <omit extent="1 word" cause="sound quality"></omit> ਦਾ ਸੁਣਾ ਦਿਓ । ਜਨਾਬ ਕੋਸ਼ਿਸ਼ ਕਰਦੇ ਐ ਅਸੀਂ । ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਤੁਹਾਡਾ । ਵਿਰਾਜਾ ਅਜ਼ਰ ਜੋ ਕੀ ਰੈਡਿਸ਼ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਸਨ, ਬੱਬੂ ਮਾਨ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਕ ਗੀਤ ਸੁਨਨਾ ਚਾਹਿਅ ਐ ਜਨਾਬ ਜਰੂਰ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ । ਮਸੂਦ ਸਾਹਿਬ ਬ੍ਰਿਟੇਨ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਸਨ ਤੁਸੀਂ ਕਿਹੈ ਕੇ ਕੋਈ ਵੀ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ, ਜਿਹੜੀ ਏਦਾ ਦੀ <foreign lang="eng">dadication</foreign> ਆਉਦੀ ਐ ਬਈ ਕੋਈ ਵੀ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ ਤੁਸੀਂ ਆਪਣੀ <foreign lang="eng">choice</foreign> ਦਾ <vocal who="PM001" desc="laugh"></vocal> ਸਾਡੇ ਨਾਂ ਕਰ ਦਿਓ ਇਹ ਮੈਨੂੰ ਬਹੁਤ ਅੱਛਾ ਲਗਦੈ । ਵੈਸੇ ਤੇ ਤੁਸੀਂ <foreign lang="eng">dadication</foreign> ਤੁਸੀਂ ਕਰ ਸਕਦੇ ਓ ਗੀਤ ਤੁਸੀਂ ਆਪਣੀ ਫਰਮਾਇਸ਼ ਦਾ ਦੱਸ ਸਕਦੇ ਈ ਲੇਕਿਨ ਜਦੋ ਮੇਰੀ ਪਸੰਦ ਦਾ ਸੁਣੋ ਸਾਡੇ <foreign lang="eng">producer</foreign> ਘਰ ਦੇ ਤੇ ਨਹੀ ਨੇ । ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਤੇ ਜਰੂਰ ਕਰਦੇ ਐ ਕੇ ਗੀਤ ਸਾਨੂੰ ਲੱਭ ਜੇ ਤੇ ਤੁਹਾਡੇ ਨਾਂ ਕਰੀਏ ਬਹਰਹਾਲ ਤੁਹਾਡੇ, ਸਾਡੇ ਲਈ ਤੁਹਾਡਾ ਜਿਹੜਾ ਪਿਅਰ ਐ ਨਾ ਤੁਹਾਡਾ ਨਾ ਪੜ੍ਹਨਾ ਤੁਹਾਡਾ ਪੈਗ਼ਾਮ ਪੜ੍ਹਨਾ, ਤੁਹਾਡੇ <foreign lang="eng">message</foreign> ਪੜ੍ਹਨਾ ਸਾਡੇ ਲਈ ਇਹ ਬਹੁਤ ਫਖ਼ਰ ਆਲੀ ਗੱਲ ਐ । ਔਰ ਬੜੀ ਮਤਲਬ ਮਿੱਠੀ ਗੱਲ ਐ ਤੁਸੀਂ ਸਾਨੂੰ ਯਾਦ ਕਰਦੇ ਓ ਸਾਡੇ <foreign lang="eng">B. B. C Asian Network.</foreign> ਦੇ <foreign lang="eng">program </foreign> ਸੁਣਦੇ ਓ । ਰਾਜ ਲੈਸਟਰ ਤੋ ਫੋਨ ਕਰਦੇ ਐ ਉਹਨਾਂ ਨੇ <foreign lang="eng">happy birth day </foreign> ਕਿਹੈ ਆਪਣੇ <foreign lang="eng">friend Dvinder Singh</foreign> , ਔਰ ਦਵਿੰਦਰ ਜੀ ਤੁਹਾਨੂੰ ਸਾਡੇ ਵਲੋ ਬਹੁਤ ਬਹੁਤ ਜਨਮ ਦਿਨ ਮੁਬਾਰਕ ਅਗਰ <vocal desc="er"></vocal> ਤੁਸੀ <vocal desc="er"></vocal> ਸਾਨੁੰ ਥੋੜਾ ਜਿਹਾ ਹੋਰ <foreign lang="eng">detail</foreign> ਚ ਦੱਸ ਦਿੰਦੇ ਤੇ ਜਿਹੜਾ ਗੀਤ ਤੁਸੀਂ ਸੁਨਨਾ ਚਾਹਿਆ ਐ ਤੁਹਾਡੇ ਨਾ ਅਸੀਂ ਜਰੂਰ ਕਰਦੇ ਰਾਜ ਵਲੋ ਤਮਾਮ ਦੋਸਤਾ ਵਲੋ, ਰਿਸ਼ਤੇਦਾਰਾਂ ਵਲੋ ਤੇ ਮੇਰੇ ਵਲੋ ਖਾਸ ਕਰਕੇ ਸਾਡੇ ਪੁਰੇ ਸਟਾਫ ਵਲੋ ਦਵਿੰਦਰ ਜੀ ਨੂੰ ਬਹੁਤ ਬਹੁਤ ਜਨਮ ਦਿਨ ਮੁਬਾਰਕ ਅਸੀਂ ਦੁਆ ਕਰਦੇ ਐ ਪਰਮਾਤਮਾ ਉਹਨਾਂ ਨੂੰ ਲੰਬੀਆਂ ਉਮਰਾਂ ਬਖਸ਼ੇ ਤੇ ਏਸੇ ਤਰਾਂ ਤੁਸੀਂ ਦਵਿੰਦਰ ਜੀ ਮਾਨਦੇ ਰਹੋ । ਆਓ ਚਲਦੇ ਐ ਏਸ ਗੀਤ ਵੱਲ ਔਰ ਇਹ ਗੀਤ ਵੇਸੇ ਤੇ ਮੈ ਲਾਉਣਾ ਪੂਰਾ ਸੀ, ਇਹ ਕੀ ਗੱਲ ਇਹ ਅੱਧ ਚੋ ਸ਼ੁਰੂ ਹੋ ਗਿਆ ਏਸ ਤਰਾਂ ਤਾਂ ਕਦੀ ਵੀ ਨੀ ਹੋ ਸਕਦਾ ਚੰਲਿਏ ਪੁਰਾ ਈ ਲਾਵਾਂਗੇ ਅਸੀਂ । ਬੜੀ ਮਿੱਠੀ ਆਵਾਜ਼ ਸਿ ਸੁਰਿੰਦਰ ਪਰਦੇਸੀ ਦੀ ਤੁਹਾਡੇਸਭ ਦੇ ਨਾ । ਅੱਖਂਾ ਤੇ ਬਹੁਤ ਲੜਦੀਆਂ ਨੇ ਜਨਾਬ ਤੇ ਲੜ ਕੇ ਤੇ ਉਹ ਆਪਣੇ ਆਪਣੇ <foreign lang="eng">control</foreign>  ਚ ਰਹਿਣ ਤੇ ਬੜੀ ਅੱਛੀ ਗੱਲ ਐ ਨਹੀਂ ਤੇ ਅੱਖਂਾ ਖਰਾਬ ਹੋਣ ਲੱਗ ਜਾਂਦੀਆਂ ਨੇ ਤੇ ਫੇਰ ਦਰਦ ਹੁਦੈ ਅੱਖਾਂ ਚ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="41" who="PM001">ਬਹੁਤ ਹੀ ਮਿੱਠੀ ਆਵਾਜ਼, ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਂ ਸੁਣਿਆ ਸੁਰਿੰਦਰ ਪਰਦੇਸੀ ਦੀ ਆਵਾਜ਼ ਦੇ ਵਿਚ ਉਮੀਦ ਐ ਕੇ ਤੁਹਾਨੂੰ ਇਹ ਪਸੰਦਾ ਅਇਅ ਹੋਣੈ । ਔਰ ਜਨਾਬ ਜਦੋ ਏਸ ਤਰਾਂ ਦੇ ਗੀਤ ਤੁਸੀਂ ਸੁਣਦੇ ਓ ਤੇ ਜਰੂਰ ਦਿਲ ਨੂੰ ਕੁਛ ਨਾ ਕੁਛ ਹੂੰਦੈ । ਤੇ ਫੇਰ ਬੋਦੇ ਨੂੰ ਬੰਦੇ ਦਾ ਦਿਲ ਕਰਦੈ ਕਿਸੇ ਨੂੰ ਖਤ ਲਿਖਾਂ । ਕਿਤੇ ਲੋਕ ਕੇ ਖਤ ਲਿਖਾਂ । ਤੇ ਖਤ ਵਿਚ ਉਹ ਕੁਛ ਲਿਖਦਾਂ ਪੜ੍ਹਦਿਆਂ ਸਾਰ ਮੇਰਾ ਸਜਣ ਜਿਹੜਾ ਦੂਰ ਬੈਠਾ ਐ ਇਹ ਵਾਪਿਸ ਅ ਜਾਏ । ਅੱਖਂ ਲੜਣ ਤੋ ਬਾਅਦ ਏਹੋ ਜਿਹੀ ਕੋਸ਼ਿਸ਼ ਜਰੂਰ ਹੂੰਦੀ ਐ ਜਨਾਬ । ਅੱਲਾ ਕਰੇ ਕਿਸੇ ਨੁੰ ਗੌਰ ਦੇ ਨਾਲ ਨਾ ਹੀ ਤੱਕਿਅ ਜਾਵੇ ਤਾਂ ਅੱਛੀ ਗੱਲ ਐ । ਨਾ ਈ ਦੇਖਿਆ ਜਾਵੇ ਕਿਸੇ ਨੂੰ ਗੌਰ ਦੇ ਨਾਲ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="42" who="PM001">ਇਹ ਬੜੇ ਪੂਰਾਣੇ ਪਿਆਰ ਭਰੇ ਖਤ ਦਿਲ ਨੂੰ ਕੀ ਲੱਗਦੇ ਨੇ ਹੁਣ ਕੀਲੱਗਦੇ ਨੇ <vocal who="PM001" desc="laugh"></vocal> ਉਹ ਖਤ । ਜਦੋਂ ਪੜੀਆ ਕਰਦੇ ਸੀ ਬਹੁਤ ਅੱਛੇ ਲੱਗਦੇ ਸੀ ਲਿਫਾਫਾ ਵੀ ਦੇਖਿਏ ਤਾਂ ਬਹੁਤ ਖੁਸ਼ੀ ਹੂੰਦੀ ਸੀ ਪੜ੍ਹ ਕੇ ਵੀ ਦੇਖੀਏ ਤਾਂ ਬੜੀ ਖੁਸ਼ੀ ਹੂੰਦੀ ਸੀ । ਮਨ ਨੂੰ ਬੜੀ ਤਸੱਲੀ ਲੱਗਦੀ ਸੀ । ਹਾਂ ਔਰ ਹੁਣ ਹੁਣ ਮੇਰੇ ਕੀ ਲੱਗਦੇ ਨੇ <vocal who="PM001" desc="laugh"></vocal> ਬਹੁਤ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਂ ਸੁਣਿਆ । ਲੱਕੀ ਗੁਲਾਮ ਦੀ ਆਵਾਜ਼ ਦੇ ਵਿਚ । ਸਾਡੇ ਕੁਝ ਦੋਸਤ ਜਿਹਨਾਂ ਨੇ ਫਰਮਾਇਸ਼ਾਂ ਕੀਤੀਆਂ ਨੇ ਉਹਨਾਂ ਤੋ ਮਾਫੀ ਚਾਹਾਂਗੇ ਗੀਤ ਲੱਭਣ ਤੋ ਬਾਵਜੁਦ ਵੀ ਸਾਨੂੰ ਇਹ ਗੀਤ ਨਹੀਂ ਲੱਭੇ । ਸ਼ੈਫਾਲੀ ਜੀ ਵੀ ਤੁਹਾਡੇ ਤੋ ਮਾਫੀ ਚਾਹੂੰਦੇ ਐ । ਬਹਰਹਾਲ ਆਉਣ ਵਾਲੇ <foreign lang="eng">program </foreign>ਾ ਚ ਜਰੂਰ ਸ਼ਾਮਿਲ ਕਰਾਂਗੇ ਅਸੀਂ । <vocal desc="er"></vocal> ਗੀਤਾਂ ਦਾ ਬਹਾਨਾ ਹੂੰਦੈ ਲੇਕਿਨ, ਤੁਹਾਡੇ ਨਾ ਪੜ੍ਹ ਕੇ ਤੁਹਾਡੇ <vocal desc="er"></vocal> ਦੋਸਤਾਂ ਨੂੰ ਤੁਹਾਡੇ ਰਿਸ਼ਤੇਦਾਰਾਂ ਨੂੰ ਤੁਹਾਡੇ ਪੈਗ਼ਾਮ ਦੇ ਕੇ ਸਾਨੂੰ ਬੜੀ ਖੁਸ਼ੀ ਹੂੰਦੀ ਐ । ਉਮੀਦ ਐ ਤੁਸੀਂ ਸਾਡੀ <foreign lang="eng">problem</foreign> ਸਮਝਦੇ ਓ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="43" who="PM001">ਬਹੁਤ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਂ ਸੁਣਿਆ ਐ ਮਦਨ ਮੱਦੀ ਦੀ ਆਵਾਜ਼ ਦੇ ਵਿਚ । ਔਰ ਅਗਲਾ ਗੀਤ ਸਾਡੇ ਕੁਝ ਦੋਸਤਾਂ ਨੇ ਫਰਮਾਇਸ਼ ਕਿਤੀ ਸੀ । ਜਜੀ ਬੀ ਦੀ ਆਵਾਜ਼ ਦੇ ਵਿਚ ਇਹ ਗੀਤ ਤੁਹਾਡੇ ਨਾਂ ਕਰਦੇ ਐਂ ਜਨਾਬ । ਅੱਠ ਮਿੰਟ ਰਹਿ ਗਏ ਨੇ ਦਸ ਵੱਜਣ ਵਿਚ ਦਸ ਵਜੇ ਖ਼ਬਰਾਂ ਹੋਣਗੀਆਂ ਗੁਜਰਾਤੀ ਵਿਚ । ਤੇ ਉਸ ਤੋ ਬਾਅਦ ਗੁਜਰਾਤੀ <foreign lang="eng">program </foreign> ਲੈ ਕਰ ਆਓਣਗੇ ਮਹੇਸ਼ ਆਈਦਸਾਨੀ ਉਹ ਤੁਹਾਡਾ ਸਾਥ ਦੇਣਗੇ ਪੁਰੇ ਬਾਰਾਂ ਵਜੇ ਤੱਕ । ਜਜੀ ਬੀ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਤੁਸੀ ਇਹ ਗੀਤ ਸੁਣਿਆ ਬੜਾ ਪਿਆਰਾ ਪੰਜ ਮਿੰਟ ਬਾਕੀ ਹਨ ਦੱਸ ਵੱਜਣ ਵਿਚ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਤਮਾਮ ਸਰੋਤਿਆਂ ਦਾ ਜਿਹਨਾਂ ਨੇ ਸਾਡਾ ਸਾਥ ਦਿੱਤਾ । <vocal desc="er"></vocal> ਔਰ ਮੈਂ ਆਪਣੇ <vocal desc="er"></vocal> ਸਾਥੀ ਜਿਹਨਾਂ ਨੇ ਮੇਰਾ ਸਾਥ ਦਿੱਤਾ ਸ਼ੈਫਾਲੀ ਉਹਨਾਂ ਦਾ ਵੀ ਮੈਂ ਸ਼ੂਕਰੀਆ ਅਦਾ ਕਰਦੈ ਮੇਰੇ <foreign lang="eng">producer</foreign> <omit extent="1 word" cause="sound quality"></omit> ਉਹਨਾਂ ਦਾ ਵੀ ਮੈਂ ਸ਼ੁਕਰੀਆ ਅਦਾ <vocal desc="er"></vocal> ਕਰਾਂਗਾ । ਉਹਨਾਂ ਨੇ ਏਸ <foreign lang="eng">program </foreign> ਨੂੰ ਕਾਮਯਾਬ ਕੀਤਾ । ਪੰਜ ਮਿੰਟ ਤੋ ਬਾਅਦ ਆਉਣਗੇ ਮਹੇਸ਼ ਆਈਦਸਾਨੀ ਗੁਜਰਾਤੀ <foreign lang="eng">program </foreign> ਲੈ ਕੇ ਮੈਨੂੰ ਇਜਾਜਤ ਦਿਓ ਇਸ ਆਖਿਰੀ ਗੀਤ ਨਾਲ ਤੁਹਾਡੇ ਤੋ ਇਜ਼ਾਜਤ ਚਾਵਂਾਗਾ । ਜਿੰਦਗੀ ਰਹੀ ਤੇ ਅਗਲੇ ਹਫਤੇ ਫੇਰ ਮੁਲਾਕਾਤ ਹੋਵੇਗੀ ਤੁਸੀਂ ਆਪਣਾ ਵੀ ਖਿਆਲ ਰੱਖਿਓ ਤੇ ਦੂਜਿਆਂ ਦਾ ਵੀ ਖਿਆਲ ਰੱਖਿਓ । ਜਿਥੇ ਵੀ ਰਹੋ ਖੁਸ਼ ਰਹੋ । ਪਰਮਾਤਮਾ ਤੁਹਾਨੂੰ ਬਹੁਤ ਬਹੁਤ ਜਿਆਦਾ ਖੁਸ਼ ਰੱਖੇ । ਔਰ ਇਹ ਗੀਤ ਮੈਂ <foreign lang="eng">actualy</foreign> <vocal desc="er"></vocal> ਸੁਨਾਉਣਾ ਚਾਹੂੰਨੈ । ਐਤਾਂ ਸੀ ਡੀ ਕੁਛ ਨਖਰੇ ਕਰਦੀ ਐ ਮੈਨੂੰ ਲੱਗਦੈ ।</u>

</body>
</text>
</cesDoc>