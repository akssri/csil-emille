<cesDoc id="pun-s-cg-asiannet-03-05-31" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-s-cg-asiannet-03-05-31.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Manoj Chopra)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-08</pubDate>
</publicationStmt>
<sourceDesc>
<recording type="audio">
<respStmt><resp>Location recording by</resp>
<name>Andrew Hardie</name></respStmt>
<equipment>audio tapes</equipment>
<date>03-05-31</date>
<broadcast>
<bibl><title>Punjabi Programme</title>
<author>BBC Asian Network</author>
</bibl>
</broadcast>
</recording>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Spoken text only has been transcribed.
</samplingDesc>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-06</date>
<rs type="city">Lancaster, UK</rs>
</creation>
<langUsage>Contemporary spoken Punjabi language used in the UK
</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
</profileDesc>
<revisionDesc>
</revisionDesc>
<textClass>
<channel mode="s">radio broadcast</channel>
<constitution type="single"></constitution>
<derivation type="original"></derivation>
<domain type="art"></domain>
<factuality type="fact"></factuality>
<translations>
</translations>
</textClass>
<particDesc>
<person id="PF028" sex="F" age="unknown"><occupation code="1"></occupation></person>
<person id="PF021" sex="F" age="unknown"><occupation code="1"></occupation></person>
<person id="PM001" sex="M" age="unknown"><occupation code="1"></occupation></person>
<particLinks>
</particLinks>
</particDesc>
<settingDesc>
<setting who="PF021 PF028 PM001">
<name type="country">UK</name>
<locale>radio station</locale>
<activity>presenting a radio programme</activity>
</setting>
</settingDesc>
</cesHeader>
<text>
<body>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="1" who="PF028">ਬੀ ਬੀ ਸੀ ਏਸਿਯਨ ਨੇਟਵਰਕ ਤੇ <omit extent="1 word" cause="sound quality"></omit> ਤੇ ਮੁਤਾਬਿਕ ਆਠ ਵਜੇ ਐ ਅਬ ਆਪ ਸੁਰਿੰਦਰ ਕੌਰ ਸੇ ਖਬਰੇ ਸੁਨੇਂਗੇ ।</u>

<u id="2" who="PF021">ਅਜ ਦੇ ਮੁਖ ਸਮਾਚਾਰ ਸ਼ਾਨੇ ਪੰਜਾਬ ਰੇਲ ਗਡੀ ਦੇ ਇੰਜਨ ਵਿਚ ਅਗ ਲਗਨ ਵਾਲੇ ਖਬਰ ਮਿਲਿ ਐ, ਇੰਡਿਆ ਵਿਚ ਜਿਆਦਾ ਗਰਮੀ ਨਾਲ ਕੋਈ ਅਠ ਸੌ ਦੇ ਕਰੀਬ ਲੋਕ ਮਾਰੇ ਗਏ, ਹੈਦਰਾ ਬਾਦ ਦੇ ਇਕ ਹੋਰ ਧਮਾਕੇ ਵਿਚ ਚਾਰ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ਹੈ, ਟੋਨੀ ਵਿਡਲੀ ਬਰਿਟੇਨ ਦੀ ਸਬ ਤੋ ਵਡੀ ਯੁਨਿਅਨ ਦੇ ਲੀਡਰ ਚੁਨੇ ਗਏ, ਏਅਰ ਫ੍ਰਾਂਸ ਕਾਂਕੋਟ <foreign lang="eng">air service</foreign> ਨੂੰ ਸਤਾਈ ਸਾਲ ਬਾਅਦ ਬੰਦ ਕਰ ਦਿਤਾ ਗਿਆ । ਹੁਣ ਖਬਰਾਂ ਵਿਸਤਾਰ ਨਾਲ ।</u>

<u id="3" who="PF021">ਇੰਡਿਆ ਦੇ ਉਤਰੀ <foreign lang="eng">state</foreign> ਪੰਜਾਬ ਵਿਚ ਰੇਲਵੇ ਵਰਕਰ ਨੇ ਲਗੀ ਹੁਈ ਅਗ ਨਾਲ ਜਲਦੇ ਹੁਏ ਇੰਜਨ ਵਿਚ ਬੜੀ ਹੋਸ਼ਿਆਰੀ ਨਾਲ <foreign lang="eng">passanger</foreign> ਟ੍ਰੇਨ ਤੋ ਜੁਦਾ ਕੀਤਾ । ਸ਼ਾਨੇ ਪੰਜਾਬ ਦੇ ਇੰਜਨ ਤੋ ਅਗ ਅਗ ਦੀ
ਆਂ ਲਪਟਾਂ ਦਿਖਾਈ ਦੇ ਰਹਿਆਂ ਸਨ ਜਦੋ ਰੇਲ ਗਡੀ <foreign lang="eng">central</foreign> ਪੰਜਾਬ ਵਿਚ ਫਗਵਾੜਾ <foreign lang="eng">station</foreign> ਤੇ ਰੁਕੀ ਇਸ ਘਟਨਾ ਵਿਚ ਕਿਸੀ ਦਾ ਵੀ ਜਖਮੀ ਹੋਨ ਬਾਰੇ<foreign lang="eng">report</foreign> ਨਹੀ ਮਿਲਿ <pause dur="2"></pause> ।</u>

<u id="4" who="PF021">ਇੰਡਿਆ ਦੇ ਦਖਨੀ <foreign lang="eng">state</foreign> ਆਂਧ੍ਰ ਪ੍ਰਦੇਸ਼ ਵਿਚ ਬਹੁਤ ਗਰਮੀ ਪੈਣ ਨਾਲ ਕਾਫਿ ਲੋਕਾਂ ਦੀ ਮੌਤ ਹੋ ਗਈ ਐ । <foreign lang="eng">authority</foreign> ਨੇ ਦਸਿਆ ਐ ਕਿ ੧੮ ਦਿਨਾਂ ਦੀ ਗਰਮੀ ਨਾਲ ਮਰਨ ਵਾਲੇਆਂ ਦੀ ਗਿਨਤੀ ਕੋਈ ਸਤ ਸੌ ਪਿਚਨਵੇ ਹੋ ਗਈ ਹੈ । ਨਰਗੋੰਡਾ ਵਿਚ ਇਕ ਸੌ ਅਠਵੰਜਾ ਲੋਕਾਂ ਦੀ ਮਰਨ ਵਾਰੇ <foreign lang="eng">report</foreign> ਦਿਤੀ ਗਈ ਐ । ਪਿਛਲੇ ਚੌਬੀ ਘੰਟਿਆ ਵਿਚ ਪ੍ਰਾਂਤ ਦੇ ਦੁਸਰੇ ਹਿਸਿਆਂ ਚੋ ਵੀ ਕਈ ਮੌਤਾ ਹੋਨ ਵਾਰੇ <foreign lang="eng"> report </foreign> ਮਿਲਿ ਐ । ਗੰਟੂਰ ਵਿਚ ਇਕ ਸੌ ਚੌਦਾਂ ਮੌਤਾ ਅਤੇ ਪਛਮੀ ਗੋਦਾਵਰੀ ਵਿਚ ਇਕ ਸੌ ਛੇ ਮੌਤਾਂ ਮੌਤਾਂ ਦੇ ਹੋਨ ਵਾਰੇ ਪਤਾ ਲਗਾ ਐ । ਇਸ ਵਕਤ ਇਥੇ ਤਾਪਮਾਨ ੪੫ ਡਿਗ੍ਰੀ ਸੇਲਸਿਅਸ ਹੈ ਅਤੇ ਦਸਿਆ ਗਿਆ ਐ ਕਿ ਹਲੇ ਅਗਲੇ ੪੮ ਘੰਟੇਆਂ ਵਿਚ ਕੋਈ ਵੀ ਤਬਦੀਲੀ ਆਉਨ ਦੀ ਆਸ ਨਈ ਐ । <pause dur="2"></pause> ।</u>

<u id="5" who="PF021">ਹੈਦਰਾ ਬਾਦ ਤੋ ਡਿਗ੍ਰੀ ਲੇ ਜਾਨ ਵਾਲੀ ਬਸ ਤੇ ਇਕ ਧਮਾਕਾ ਹੋਨ ਨਾਲ ਚਾਰ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ਹਨ ਏ ਸ਼ਹਰ ਵਿਚ ਦੁਸਰਾ ਧਮਾਕਾ ਹੋਇਆ ਐ ਇਸਤੋ ਪਹਿਲਾਂ ਇਸ ਮਹਿਨੇ ਦੇ ਸ਼ੁਰੂ ਵਿਚ ਇਕ ਬਸ ਤੋ ਧਮਾਕਾ ਹੋਇਆ ਸੀ ਜਿਸ ਵਿਚ ਗਿਆਰਾਂ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ਸਨ । ਇਹਨਾ ਹਮਲੇਆਂ ਦੀ ਕਿਸੀ ਨੇ ਵੀ ਜਿਮੇਵਾਰੀ ਨਹੀ ਲੀ ਐ । ਏ ਖਬਰਾਂ ਤੁਸੀ ਬੀ ਬੀ ਸੀ ਏਸ਼ਿਯਨ ਨੇਟਵਰਕ ਸੁਰਿੰਦਰ ਕੌਰ ਦੀ ਜੁਬਾਨੀ ਸੁਨ ਰੇ ਹੋ । ਲੇਫਟਿਨੇੰਟ ਟੋਨੀ ਵਿਡਲੀ ਨੇ ਬ੍ਰਿਟੇਨ ਦੀ ਸਬ ਤੋ ਵਡੀ ਯੁਨਿਅਨ ਵਿਚ <foreign lang="eng">Transport</foreign> ਅਤੇ ਜਨਰਲ ਲੀਡਰ ਸ਼ਿਪ ਜਿਤ ਲਈ ਐ । ਟੋਨੀ ਵਿਡਲੀ ਨੇ ਜੈਕ ਡ੍ਰਾਮਿਨ ਨੂੰ ਹਰਾਇਆ ਅਤੇ ਉਹ ਮਾਰਿਫ ਦੀ ਜਗਹ ਲੈਨਗੇ ਜੋ ਕਿ ਅਕਟੁਬਰ ਵਿਚ ਰਿਟਾਅਰ ਹੋ ਰਹੇ ਹਨ ਮਿਸਟਰ ਵੁਡਲੀ ਖੁਲੀ ਤਰਾਂ ਸਰਕਾਰ ਦੇ ਆਲੋਚਕ ਹਨ ਅਤੇ ਉਹਨਾ ਨੇ ਕੰਮ ਕਰਨ ਵਾਲੇਆਂ ਦੇ ਹਕਾਂ ਦੀ ਹਿਫਾਜਤ ਨਾ ਕਰਨ ਦਾ ਦੋਸ਼ ਸਰਕਾਰ ਨੂੰ ਦਿਤਾ ਐ । <pause dur="2"></pause> ।</u>

<u id="6" who="PF021">ਜਾਰਜ ਬੁਸ਼ ਸੇਂਟ ਪੀਟਰ ਬਰਗ ਵਿਚ ਅਪਨੇ ਸਬੀ ਗਲ ਬਾਤ ਰਸ਼ਿਅਨ ਪ੍ਰੇਸੀਡੇਂਟ ਗਲੈਡਮਿਰ ਪੁਤਿਨ ਨਾਲ ਕਰਨਗੇ ਉਹਨਾ ਦੀ ਇਰਾਕ ਦੀ ਲੜਾਈ ਬਾਰੇ ਮਤ ਭੇਦ ਹੋਨ ਤੋ ਬਾਦ ਇਹ ਉਹਨਾ ਦੀ ਪਹਿਲੀ ਆਮਨੇ ਸਾਮਨੇ ਗਲ ਬਾਤ ਹੋਵੇਗੀ । ਹਾਲ ਹੀ ਵਿਚ <foreign lang="eng">mister Bush</foreign> ਨੇ ਯੂਰੋਪ ਤਥਾ <foreign lang="eng">United state</foreign> ਨੂੰ <foreign lang="eng">terrorism aids</foreign> ਅਤੇ ਗਰੀਬੀ ਨੂੰ ਖਤਮ ਕਰਨ ਲਈ ਇਕਥੇ ਹੋਕਰ ਕੰਮ ਕਰਨ ਲਈ ਪ੍ਰੇਰਿਤ ਕਿਤਾ ਐ । <pause dur="2"></pause> ।</u>

<u id="7" who="PF021">ਏਅਰ ਪ੍ਰਾਂਸ ਕਾਨਫ੍ਰੇਂਸ ਅਤੇ ਆਖਰੀ ਉਡਾਨ ਤੋ ਬਾਦ ਪੈਰਿਸ ਵਿਚ ਉਤਰਿਆ ਇਹ ਉਡਾਨ <foreign lang="eng">New york</foreign> ਤੋ ਪੈਰਿਸ ਆਈ । ਬ੍ਰਿਟਿਸ਼ ਏਅਰ ਵੇਜ ਦੀ ਤਰਾਂ ਏਅਰ ਫ੍ਰਾਂਸ ਵੀ ਉਹਨਾ ਦੇ ਕਾਂਕੋਡਸ ਨੂੰ ੨੭ ਸਾਲਾਂ ਦੀ ਸਰਵਿਸ ਦੇ ਬਾਦ ਉਸਦੀ ਉੜਾਨ ਬੰਦ ਕਰ ਰੇ ਹਨ ।</u>

<u id="8" who="PF021">ਹੁਨ ਪੇਸ਼ ਐ ਮੌਸਮ ਅਜ ਦਾ ਮੌਸਮ ਖੁਸ਼ਕ ਗਰਮ ਅਤੇ ਧੁਪ ਦਾਰ ਐ ਪਰੰਤੁ ਗਰਮੀ ਦੇ ਬਾਦ ਸ਼ਾਯਦ ਕਿਤੇ ਕਿਤੇ ਬਦਲਾਂ ਦੀ ਗੜਗੜਾਹਟ ਦੇ ਬਾਦ ਬਾਰਿਸ਼ ਹੋਵੇਗੀ । ਤਾਪਮਾਨ ਵਧ ਤੋ ਵਧ ੩੦ ਡਿਗ੍ਰੀ ਸੇਲਸਿਅਸ ਰਹੇਗਾ । ਬੀ ਬੀ ਸੀ ਏਸ਼ਿਯਨ ਨੇਟਵਰਕ ਤੋ ਖਬਰਾਂ ਖਤਮ ਹੋਇਆਂ ।</u>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="9" who="PM001">ਜੀਹਾਂ ਜਨਾਬ ਇਕ ਬਾਰੀ ਫਿਰ ਤੁਹਾਡਾ ਸਵਾਗਤ ਕਰਦੇ ਐ ਬੀ ਬੀ ਸੀ ਏਸ਼ਿਯਨ ਨੇਟਵਰਕ ਤੇ ਤੁਸੀ ਏ ਪੰਜਾਬੀ <foreign lang="eng">programme</foreign> ਸੁਨਨ ਜਾ ਰਹੇ ਹੋ ਦਲਜੀਤ ਮੀਰ ਦੇ ਨਾਲ ਔਰ ਮੇਰਾ ਤੇ ਤੁਹਾਡਾ ਸਾਥ ਰਹੇਗਾ ਪੁਰੇ ਦਸ ਬਜੇ ਤਕ ਅਠ ਵਜ ਕੇ ਦਸ ਮਿਨਟ ਹੋਨ ਵਾਲੇ ਜਿਦਾਂ ਤੁਸੀ ਜਾਨਦੇ ਹੋ ਸਾਡਾ ਹਰ ਸ਼ੁਕ੍ਰਵਾਰ ਨੂੰ ਏ ਪ੍ਰੋਗ੍ਰਾਮ ਹੁੰਦਾ ਐ <vocal desc="er"></vocal> ਅਠ ਵਜੇ ਤੋ ਲੈਕੇ ਦਸ ਵਜੇ ਤਕ ਔਰ ਮੇਰਾ ਤੋ ਪਹਿਲਾਂ ਖਾਮਖਾ ਜੀ ਬਹੁਤ ਹੀ ਪਿਆਰਾ <foreign lang="eng">programme</foreign> ਐ <omit extent="1 word" cause="sound quality"></omit> ਔਰ ਅਜ ਵੀ ਤੁਸੀ ਹੁਨ <foreign lang="eng">programme</foreign> ਦਾ ਮਜਾ ਲੇਵਾਂ ਮੈ ਤੁਹਾਡਾ ਸਾਥ ਚਾਹਾਂਗਾ ਪੁਰੇ ਦਸ ਵਜੇ ਤਕ <omit extent="1 word" cause="sound quality"></omit> ਤੁਸੀ ਮੇਰਾ ਸਥ ਦੋਗੇ ਜਰੂਰ ਕਿਉਕਿ ਤੁਸੀ ਹਮੇਸ਼ਾ ਮੇਰਾ ਸਾਥ ਦਿੰਦੇ ਹੋ ਬੜੀ ਸੋਨੀ ਧੁਪਨਿਕਲੀ ਐ ਔਰ ਧੁਪ ਦੋ ਤਿੰਨ ਦਿਨਾਂ ਦੀ ਅਛਿ ਮਤਲਬ <vocal desc="er"></vocal> ਚਲ ਰੀ ਐ ਰ ਮੈਨੁ ਬਹੁਤ ਮਜਾ ਆ ਰਿਆ ਐ ਔਰ ਉਮੀਦ ਐ ਕਿ ਤੁਸੀ ਵੀ <foreign lang="eng">garden</foreign> ਬੈਕੇ ਸਾਡਾ <foreign lang="eng">programme</foreign> ਸੁਨ ਰੇ ਹੋ ਤੇ ਰੇਡਿਯੋ ਲਾਇਆ ਐ ਤੇ <foreign lang="eng">garden</foreign> ਮੇ ਸੈਟ ਕਰ ਰੇ ਹੋ ਯਾ ਕੁਛ ਵੀ ਕਰ ਰੇ ਹੋ ਤੁਸੀ ਤੇ ਜਨਾਬ ਅਸੀ ਇਕ ਬਾਰਿ ਫਿਰ ਤੁਹਾਡਾ ਸਵਾਗਤ ਕਰਦੇਆਂ ਔਰ ਅਸੀ ਹੁਨ ਚਲਾਂਗੇ ਅਗਲੇ ਗੀਤ ਵਲ ਔਰ ਅਜ ਦੇ ਇਸ <foreign lang="eng">programme</foreign> ਵਿਚ ਜੋ ਗੀਤ ਸੰਗੀਤ ਤੁਹਾਡੇ ਪੈਗਾਮ ਤੁਹਾਡੇ <foreign lang="eng">massage</foreign> ਐ ਕੁਛ ਹਸੀ ਮਜਕ ਦਿਆਂ ਗਲਾਂ ਔਰ ਕੁਛ ਅਸੀ ਤੁਹਾਡੇ ਖਾਸ ਪੈਗਾਮ ਲਾਈਵ ਔਰ ਨੇਰ ਲੇਨਾ ਚਾਵਾਂਗੇ । ਕਿਉਕਿ ਗਰਮਿਆਂ <omit extent="1 word" cause="sound quality"></omit> ਅਸੀਂ ਚਾਵਾਂਗੇ ਕਿ ਗਰਮਿਆਂ ਨੂੰ ਵੀ ਬਹੁਤ ਤੁਸੀ ਥੋੜਾ ਜਿਆ ਮਤਲਬ <foreign lang="eng">programme</foreign> ਮਰ ਮਤਲਬ ਥੋੜਾ ਜਿਆ ਤਬਦੀਲ ਕਰਿਏ ਕਿਉਕਿ ਰ ਫਿਰ ਤੁਸਿ ਅਪਨਾ ਮੈਸੇਜ ਦਿੰਦੇ ਹੋ ਪੇਗਾਮ ਦਿੰਦੇ ਹੋ ਬੜੇ ਪਿਆਰੇ ਪਿਆਰੇ ਕਿਉਕਿ ਅਸੀ ਚਾਹਾਂਗੇ ਮੈਸੇਜ ਦੇ ਬਾਦ ਸਾਡਾ ਜਿਹੜਾ ਨੰਬਰ ਐ <foreign lang="eng">08 459 440 445</foreign> ਦੇ ਜਰਿਏ ਤੁਸੀ ਸ਼ੈਫਾਲੀ ਜੀ ਨੂੰ ਅਪਨਾ ਨੰਬਰ ਲਿਖਾਔ ਔਰ ਅਸੀ ਤੁਹਾਡਾ ਗਲ ਬਾਤ ਕਰਾਂਗੇ ਕੁਛ ਗਪ ਸ਼ਪ ਮਾਰਾਂਗੇ ਔਰ ਕੁਛ ਨਈ ਟਾਇਪ ਦੀ ਪੁਰਾਨੀ ਗਲ ਬਾਤ ਕਰਾਂਗੇ ਤੇ ਤੁਹਾਡੀ ਗੀਤਾਂ ਦੀ ਫਰਮਾਇਸ਼ਾ ਜਿਹੜੀਆਂ ਨੇ ਉਹ ਵੀ ਅਸੀ ਕੋਸ਼ੀਸ਼ ਕਰਾਂਗੇ ਕਿ ਪੁਰੀ ਕਰਿਏ ਅਗਰ ਨਾ ਵੀ ਪੁਰੀ ਕਰ ਸਕੇ ਤੇ ਅਸੀ ਅਪਨੀ ਪਸੰਦ ਦੀ ਗੀਤ ਤੁਹਾਡੇ ਨਾਲ ਜਰੂਰ ਕਰਾਂਗੇ ਹੁਨਚਲਦੇ ਐ ਅਸੀ ਇਸ ਗੀਤ ਵਲ ਤੇ ਫਿਰ ਤੁਹਾਡਾ ਸਵਾਗਤ ਜੀ ਆਇਆ ਨੂੰ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="10" who="PM001">ਚਲੋ ਜੀ ਬਹੁਤ ਪਿਆਰਾ ਗੀਤ ਅਸੀ ਸੁਨਾ ਐ ਮਹੋਮਦ ਰਫੀ ਅਤੇ ਆਸ਼ਾ ਭੌਸਲੇ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਫਿਲਮ ਸੁਖਿ ਪਰਿਵਾਰ ਪੰਜਾਬੀ ਦੀ ਫਿਲਮ ਅਗਰ ਤੁਸੀ ਹੁਨੇ ਹੁਣੇ <foreign lang="eng">radio on</foreign> ਕੀਤੈ ਤਾਂ ਅਸੀਂ ਇਕ ਵਾਰੀ ਫੇਰ ਤੁਹਾਡਾ ਸਵਾਗਤ ਕਰਦੇ ਆਂ, ਜੀ ਆਇਆ ਨੂੰ ਕਹਿੰਦੇ ਐ । ਅਜ ਕਲ ਵੈਸੇ ਬਰਗਲ ਫੀਲ ਕਰਨੇ ਵਾਲਾ ਮੌਸਮ ਐ ਅਗਰ ਤੁਸੀ ਵੀ ਬਾਦਲ ਫੀਲ ਕਰ ਰੇ ਹੋ ਯਾ ਗਾਰਡਨ ਵਿਚ ਹੋ ਬਾਹਰ ਕਿਸੀ ਗਾਰਡਨ ਦੇ ਵਿਚ ਤਾਂ ਬੜਾ ਸਹੀ ਕਰ ਰੇ ਹੋ ਤੁਹਾਡਾ ਤੇ ਤੁਹਾਡੇ ਫੁਲਾਂ ਨੁੰਪਾਣੀ ਦੇ ਰਹੇ ਓ । ਵੈਸੇ ਇਹ ਮੌਸਮ ਮੌਸਮ ਦੀ ਗਲ ਐ ਪਰ ਇਥੇ ਦੇ ਮੌਸਮ ਦਾ ਕੁਛ ਪਤਾ ਨੀ ਲਗ ਰਿਆ ਨਾ ਮੌਸਮ ਦਾ ਨਾ ਕੈਂਪ ਦਾ ਨਾ <omit extent="1 word" cause="sound quality"></omit> ਲੋਕਾਂ ਦਾ <vocal who="PM001" desc="laugh"></vocal> ਕਦੋ ਬਦਲ ਜਾਂਦਾ ਐ ਬੰਦਾ ਕੁਛ ਪਤਾ ਨੀ ਲਗਦਾ ਕਿ ਦਿਤਾ ਮੇਰਾ ਲਪੇਟੀ ਜਾਵੇ ਬੜੇਆ ਵਾਲੀ ਗਲ ਸੁਨਕੇ ਜਗ ਹੋਰ ਨਾ ਭਲੇਤਾਂ ਪਾਵੇ <pause dur="2"></pause> ਅਵਰੇ ਦੀ ਕਿ ਜਿੰਦਗੀ ਕੜੀ ਰਾਤ ਨੂੰ ਕੜਰਾਰੇ ਵਿਚ ਲੇਂਦੀਜਿਨਾ ਮਹੋਬਤ ਕਰਨੀ ਹਰ ਬੰਦਾ ਤਿਆਰ ਐ ਇਸ਼ਕ ਦੀ ਹਰ ਬਾਜਿ ਖੇਲਨ ਨੂੰ ਹਰ ਬੰਦਾ ਤਿਆਰ ਐ ਲੇਕਿਨ ਇਸਦਾ ਅੰਜਾਮ ਕਿ ਐ ਇਹਦਾ <foreign lang="eng">result</foreign> ਕਿ ਐ ਇਹਦਾ <foreign lang="eng">result</foreign> ਕਿ ਐ ਓਹ ਦਸਨਾ ਕਿ ਐ ਉਹਦਾ ਸਿਧਾ ਕਿ ਨਿਕਲਿਆ ਏ ਬੰਦਾ ਸੋਚਨ ਤਭੀ ਸੋਚਦਾ ਐ ਜਦੋ ਇਸ਼ਕ ਕਰਦਾ ਐ ਮਹੋਬਤ ਕਰਦਾ ਐ <pause dur="2"></pause> ਸ਼ਾਯਦ ਇਸੀ ਕਰਕੇ ਦੁਨਿਆ ਕਹੰਦੀ ਐ ਯਾ ਰਾਇਟਰਾਂ ਦੇ <omit extent="1 word" cause="sound quality"></omit> ਕਹਿੰਦੇ ਨੇ ਕਿ ਪਿਆਰ ਕਿ ਕਰੀ ਜਾਂਦਾ ਐ ਪਿਆਰ ਹੋ ਜਾਂਦਾ ਐ ਔਰ ਜਿਹੜੀ ਚੀਜ ਹੋ ਹੀ ਗਈ ਐ ਤੋ ਫਿਰ ਉਹਦਾ ਅਫਸੋਸ ਕਿ ਉਹ ਤਾਂ ਫਿਰ ਕੁਦਰਤ ਵਲੋ ਆਇਆ ਨਾ ਜਿਹੜਾ ਮੇਲ ਮਿਲਾਪ ਜਿਹੜਾ ਵੀ ਪਿਆਰ ਮਹੋਬਤ ਅਤੇ ਫੈਸਨ ਇਵੇਂਟ ਓਹ ਕੁਦਰਤ ਵਲੋ ਆਇਆ ਨਾ ਭਵਰੇ ਦੀ ਕਿ ਜਿੰਦਗੀ ਕੜੀ ਰਾਤ ਨੂੰ ਕੜਾਵੇ ਵਿਚ ਰੇਂਦੀ ਹਾਲਾ ਕਿ ਉਹਨੁ ਪਤਾ ਐ ਜਿਸਦੇ ਦਵਾਰੇ ਮੈ ਮੰਡਰਾ ਰਿਹਾਂ ਜਿਸਦਾ ਮੈ ਆਸ਼ਿਕ ਹਾਂ ਅਗਰ ਮੈ ਵੀ ਉਤੇ ਬੈ ਗਿਆ ਰਸ ਚੂਸਨ ਲਈ ਮੈ ਵੀ ਉਤੇ ਬੈ ਗਿਆ ਤੋ ਇਸਨੇ ਬੰਦ ਹੋ ਜਾਨਾ ਐ । ਭਵਰੇ ਦੀ ਕਿਜਿੰਦਗੀ ਕੜੀ ਰਾਤ ਨੂੰ ਕੜਾ ਵਿਚ ਲੇਂਦੀ ਵਿਚੋ ਵਿਚ ਸਾਂ ਘੁਟਤਾ ਦਿਨ ਚਠਤਾ ਲਸ਼ ਡਿਗ ਪੈਂਦੀ । ਸਾਰੀ ਰਾਤ ਬੈਚਾਰਾ ਕਲੀ ਦੇ ਕਿਨੇਰੇ ਵਿਚੋ ਉਸ ਗਲ ਘੁਟਨੀ ਦੇ ਵਿਚ ਸਾਂ ਘੁਟ ਘੁਟ ਕੇ ਮਰ ਜਾਤਾ ਐ ਦਿਨ ਚੜਤਾ ਐ ਕਲੀ ਖੁਲਦੀ ਐ ਜਵਾਨ ਹੁੰਦੀ ਐ ਅਪਨੀ ਫਿਤਰਤ ਨੂੰ ਨਿਸਾਰ ਤੇ ਭਵਰੇ ਦੀ ਲਾਸ਼ ਡਿਗ ਪੈਂਦੀ ਐ ਓਹ ਮਰਿਆ ਪੈਆ ਹੁੰਦਾ ਐ ਇਸ਼ਕ ਪਿਛੋ ਮਹੋਬਤ ਪਿਛੋ ਉਸਦਾ ਰਸ ਚੁਸਨ ਪਿਛੇ ਫਿਰ ਤਾਂਹੀ ਤਾਂ ਕਹਿੰਦੇ ਨੇ ਕਿ ਪਧਰਾਂ ਦੀ ਨੂੰ ਕਿ ਆਖਿਏ ਕਿ ਫੂਲਾਂ ਨੂੰ ਤਰਸ ਨਾ ਆਵੇ ਫੂਲ ਵੀ ਤਸਰ ਨੀ ਕਰਦੇ ਪਥਰਾਂ ਨੂੰ ਤੋ ਅਸੀ ਕਿ ਆਕਿਏ <pause dur="2"></pause> ਆਔ ਜਨਾਬ ਅਸੀ <omit extent="1 word" cause="sound quality"></omit> ਦੇ ਇਸ <foreign lang="eng">Programme</foreign> ਨੂੰ ਦਸ ਵਜੇ ਤਕ ਪਹੁੰਚਾਨਾ ਐ ਨਾਲ ਤੁਹਾਡੇ ਪੈਗਾਮ ਵਾਲੀ ਸ਼ੈਫਾਲੀ ਤੁਹਾਡੇ ਪੈਗਾਮ ਤੁਹਾਡੇ ਮੈਸੇਜ ਆਏ ਨੇ <foreign lang="eng">08 459 440 445 Fax massage ne 07 786 20 2001 </foreign> ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="11" who="PM001">ਤੇਰੀ ਭਿਜਦੀ ਕੁਰਤੀ ਲਾਲ ਪਸੀਨੇ ਨਾਲ ਉੜੇ ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀ ਸੁਨਿਆ ਐ ਹਰਭਜਨ ਮਾਨ ਦੀ ਆਵਾਜਚ <pause dur="2"></pause> ।</u>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="12" who="PM001">ਜੀ ਹਾਂ ਮੈ ਹਾਂ ਦਲ ਜੀਤ ਮੀਰ ਯਾਨਿ ਕਿ ਮੇਰਾ ਨਾਂ ਦਲਜੀਤ ਮੀਰ ਐ ਔਰ ਮੇਰਾ ਸਾਥ ਦੇ ਰਹੇ ਐ ਸ਼ੈਫਾਲੀ ਜੀ ਸਾਡਾ ਤੇ ਤੁਹਾਡਾ ਸਾਥ ਰਵੇਗਾ ਕਰੀਬ ਦਸ ਵਜੇ ਤਕ ਪਚੀ ਮਿੰਟ ਹੋਨ ਵਾਲੇ ਨੇ ਨੌ ਵਜੇ ਤੁਸੀ ਫਿਰ ਪੰਜਾਬੀ ਵਿਚ ਖਬਰਾਂ ਸੁਨੋਗੇ ਸੁਰਿੰਦਰ ਕੌਰ ਦੀ ਜੁਬਾਨੀ ਔਰ ਤੁਹਾਡੀ ਕੁਛ ਫਰਮਾਇਸ਼ਾ ਤੁਹਾਡੇ ਕੁਛ ਪੈਗਾਮ ਆਏ ਨੀ ਮੇਰੇ ਕੋਲ ਅਸੀ ਓਹ ਵੀ ਕੋਸ਼ੀਸ਼ ਕਰਾਂਗੇ ਤੇ <vocal desc="er"></vocal> ਤੁਹਾਡੀਆਂ ਫਰਮਾਇਸ਼ਾਂ ਅਸੀ ਪੁਰੀਆਂ ਕਰਿਏ ਔਰ ਮਨ ਚਲਦੇ ਐ ਅਗਲੇ ਗੀਤ ਵਲ ਬਹੁਤ ਹੀ ਪਿਅਰਾ ਗੀਤ ਅਸੀ ਸੁਨਿਆ ਐ ਤੁਹਾਡੇ ਸਬ ਤੇ ਲਈ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="13" who="PM001">ਜੀ ਹਾਂ ਇਹ ਸੀ ਇਬ੍ਰਾਹਮ ਜੀ ਤੁਸੀ ਸੁਨਿਆ ਦਲਜੀਤ ਮਿਰ ਦੇ ਨਾਲ ਕੁਛ ਸਾਨੁ ਫੋਨ ਆਏ ਨੇ ਸ਼ਫਰੀਕ ਸਾਹਿਬ ਤੁਸੀ ਗੁਲਾਮ ਅਲੀ ਸਾਹਿਬ ਦੀ ਆਵਾਜ ਵਿਚ ਕਿਹਾ ਐ ਗੀਤ ਮਾਈ ਨੀ ਮਾਈ ਮੇਰੇ ਗੀਤ ਵਿਚ <foreign lang="eng">actually</foreign> ਯੇ ਮਸੂਰ ਸਾਹਿਬ ਨੇ ਗਾਇਆ ਐ ਯਾ ਫਿਰ ਜਗਜੀਤ ਸਿੰਗ ਨੇ ਗਾਇਆ ਐ ਮੈ ਦੋਨੋ ਮੇਂਸੇ ਕੋਸ਼ੀਸ਼ ਕਰਾਂਗਾ ਤੇ ਕੋਈ ਵੀ ਮੇਰੇ ਯਾ ਤੁਹਾਡੇ ਨਾ ਜਰੂਰ ਕਰਾਂਗਾ <omit extent="1 word" cause="sound quality"></omit> ਤੁਹਾਡੀ ਫਰਮਾਇਸ਼ ਦਾ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕ੍ਰਿਆ । ਦਰਸ਼ਨ ਸਿੰਗ ਜੀ ਤੁਸਿ ਫੋਨ ਕਿਤਾ ਐ ਤੁਸੀ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਕਿਤੀ ਐ ਰਾਜ ਸਿੰਗ ਪਰਦੇਸੀ ਜੀ ਦੀ ਅਸੀ ਇਹ ਕੋਸ਼ੀਸ਼ ਕਰਾਂਗੇ ਕਿ ਅਗਰ ਓਹ ਗੀਤ ਸਾਨੁ ਲਬ ਜਾਏ ਤੇ ਜਰੂਰ ਸੁਨਾਵਾਂਗੇ ਬਹੁਤ ਬੁਹੁਤ ਸ਼ੁਕ੍ਰਿਆ ਤੁਸੀ ਸਾਨੁ ਫੋਨ ਕਿਤਾ ਐ ਕੁਲਦੀਪ ਜੀ ਤੁਸੀ ਵਿ ਕਿਹਾ ਐ ਕੋਈ ਪਿਅਰਾ ਜਿਆ ਗੀਤ <vocal desc="er"></vocal> ਸੁਨਨਾ ਚਾਹੁੰਦੇ ਹੋ ਸਾਡੇ <foreign lang="eng">programme</foreign> ਸੁਨਨਾ ਤੁਸੀ ਔਰ ਜਗਦੀਸ਼ ਜੀ ਤੁਹਾਡਾ ਫੋਨ ਅਇਆ ਐ ਬਰਹਿੰਗਮ ਤੋ ਤੁਸੀ <foreign lang="eng">programme</foreign> <foreign lang="eng">enjoy</foreign> ਕਰ ਰੇ ਹੋ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕ੍ਰਿਆ । ਮਸੂਦ ਸਾਹਿਬ ਨਿਟਨ ਤੋ ਤੁਸੀ ਫੋਨ ਕਿਤਾ ਐ ਤੁਸੀ ਕੈਰੇ ਹੋ ਰਫੀ ਸਾਹਿਬ ਦਾ ਕੋਈ ਵੀ ਪੁਰਾਨਾ ਗੀਤ ਸੁਨਾਦੇਔ ਜਰੂਰ ਸੁਨਾਵਾਂਗੇ ਜਨਾਬ । ਤਾਰੀਖ ਅਜੀਜ ਸਾਹਿਬ <omit extent="1 word" cause="sound quality"></omit> ਤੋਂ ਫੋਨ ਕਰ ਰੇ ਨੇ ਉਹਨਾ ਨੇ ਕਿਹਾ ਕੋਈ ਵੀ ਪਿਅਰਾ ਗੀਤ ਸੁਰਿੰਦਰ ਸ਼ਰਮਾ ਦੀ <vocal desc="er"></vocal> ਆਵਾਜ ਚ ਸੁਨਾ ਦੇਔ ਔਰ ਫਰੀਹ ਤੁਸੀ ਨੈਨੀਵਾਲ ਤੋ ਫੋਨ ਕਿਤਾ ਐ <omit extent="1 word" cause="sound quality"></omit> ਸੁਨਨਾ ਚਾਹੰਦੇ ਹੋ ਤੁਸੀ ਪਾਰੁਲ ਹਕ ਦੀ ਆਵਾਜ ਚ ਜਰੂਰ ਕੋਸ਼ੀਸ਼ ਕਰਦੇ ਐ ਅਸੀ ਔਰ ਸੋਨਿਆ ਬੈਟੋ ਤੋ ਸਹੀ <vocal desc="er"></vocal> ਸੁਨ ਰੇ ਸੀ ਕਲ 	ਕੋਈ ਪਿਆਰਾ ਜਿਆ ਗੀਤ <vocal desc="er"></vocal> ਕਹਿੰਦੇ ਨੇ ਸਾਰੀ ਰਾਤ ਕਰਦੇ ਜਰੂਰ ਸੁਨਾਵਾਂਗੇ ਜਨਾਬ ਔਕਾਰ ਜੀ ਔਸਵਰਗ ਤੋ ਫੋਨ ਕਰ ਰੇ ਨੇ ਸਤ ਸ੍ਰੀ ਆਕਾਲ ਜੀ ਤੁਸੀ ਔਰ ਪੁਤ ਜਟਾਂ ਦੇ ਬਨਾੰਦੇ ਨੇ ਵਖਰੇ ਤੁਸੀ ਅਪਨੇ <foreign lang="eng">Friend</foreign> <omit extent="1 word" cause="sound quality"></omit> ਮਹਿੰਦਰ ਉਹਨਾ ਮਿਲਾਵ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ ਜਰੂਰ ਕੋਈ ਪਿਆਰਾ ਜਿਆ ਗੀਤ ਅਸੀ ਜਰੂਰ ਕਰਾਂਗੇ ਜਨਾਬ । ਫਿਲਹਾਲ ਅਸੀ ਚਲਦੇ ਐ ਇਸ ਗੀਤ ਵਲ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="14" who="PM001">ਕੋਈ ਗੜਬੜ ਹੋ ਰੀ ਐ ਅਜ ਸੀ ਡੀਆਂ ਸਾਡੀ ਸਾਥ ਨੀ ਦੇ ਰੀ ਐ ਲੇਕਿਨ ਅਸੀ ਕੋਸ਼ੀਸ਼ ਕਰ ਰੇ ਐ ਜਨਾਬ ਕਿ ਕੁਛ ਨਾ ਕੁਛ <vocal desc="er"></vocal> ਸਬ ਕੁਛ ਠੀਕ ਹੋ ਜਾਏ ਤੋ ਬੜੀ ਅਛੀ ਗਲ ਐ ਵਰਨਾ । ਇਸ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਸਾਨੁ ਸ਼ਰੀਕ ਸਾਹਿਬ ਨੇ ਕਿਤੀ ਸੀ ਮਾਏ ਨੀ ਮਾਈ ਮੇਰੇ ਗੀਤਾਂ ਦੇ ਨੈਨਾ ਵਿਚ ਬਿਰਹਾ ਦੀ ਰੜਕ ਪਵੇ <omit extent="1 word" cause="sound quality"></omit> ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="15" who="PM001">ਮਈ ਨੀ ਮਈ ਮੇਰੇ ਗੀਤਾਂ ਦੇ ਨੈਨਾ ਵਿਚ ਬਿਰਹ ਦਾ ਰੰਗ ਪਵੇ ਅਦੀ ਅਦੀ ਰਾਤਿ ਉਠਰਾਨ ਮਾਏ ਮਿਤਰਾਂ ਨੇ ਮਾਏ ਸਾਨੁ ਨੀੰਦ ਨਾ ਪਵੇ ਬਹੁਤ ਪਿਆਰਾ ਕਲਾਮ । ਸ਼ੁਭਾ ਗਠਾਨੀ ਜੀ ਦੀ ਆਵਾਜ ਵਿਚ ਤੁਸੀ ਇਹ ਸੁਨਿਆ ਜਗਜੀਤ ਸਿੰਗ ਜੀ ਦੀ ਸੁਰੀਲੀ ਅਤੇ ਮਿਠੀ ਆਵਾਜ ਦੇ ਵਿਚ । ਬਾਰਹ ਮਿੰਟ ਬਾਕਿ ਹਨ ਨੌ ਵਜਨ ਵਿਚ ਤੁਹਾਡੇ ਪੈਗਾਮ ਤੁਹਾਡੇ ਮੈਸੇਜ ਤੁਹਾਡੀ ਫਰਮਾਇਸ਼ਾਂ ਸਾਨੁ ਮਿਲ ਰਇਆਂ ਹਨ ਅਸੀ ਜਰੂਰ ਕੋਸ਼ੀਸ਼ ਕਰਾਂਗੇ ਗਾਨੇ ਨੂੰ ਸ਼ਾਮਿਲ ਕਰਿਏ ਅਗਰ ਕੋਈ ਗੀਤ ਨਾ ਲਬਿਆ ਤੋ ਉਹਦਿ ਮਾਫੀ ਚਾਹਾਂਗੇ । ਲੇਕਿਨ ਕੋਈ ਉਹਦਾ ਮਿਲਿਆ ਜੁਲਿਆ ਗੀਤ ਅਸੀ ਤੁਹਾਡੇ ਨਾਂ ਜਰੂਰ ਕਰਾਂਗੇ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕ੍ਰਿਆ ਤੁਹਾਡਾ ਤੁਸੀ ਸਾਡੇ <foreign lang="eng">Programme</foreign> ਦਾ ਆਨੰਦ ਮਾਨ ਰੇ ਹੋ ਤਮਾਮ ਸਾਡੇ ਸ੍ਰੋਤੇ <foreign lang="eng">077 86 20 2001</foreign> ਸਾਡੇ ਫੈਕਸ ਮੈਸੇਜ ਦਾ ਨੰਬਰ ਐ ਔਰ <foreign lang="eng">08 459 440 445</foreign> ਸਾਡਾ ਟੇਲੌਫੋਨਨੰਬਰ ਐ ਸ਼ੈਫਾਲੀ ਤੁਹਾਡੇ ਪੈਗਾਮ ਲੈਨ ਈਸ਼ਕ ਦੀ ਗਲ ਚਲਿ ਐ ਔਰ ਇਸ਼ਕ ਦੇ ਵਿਚ ਨਿੰਦ੍ਰਾ ਗਵਾਚ ਗਈ ਹੋ ਜਨਾਬ ਤਾਂ ਨੀੰਦ ਕਿਥੇ ਰੈਂਦੀ ਐ ਇਸ਼ਕ ਦਾ ਛੜਨਾ ਐ ਜਿਹੜਾ ਉਹ ਬਹੁਤ ਅਛਾ ਐ ਬਹੁਤ ਅਛਾ ਐ ਇਸਦੇ ਵਾਰੇ ਕਿਸੀ ਸ਼ਾਯਰ ਨੇ ਇੰਜ ਕਿਹਾ ਐ <pause dur="2"></pause> ਜਰਾ ਗੌਰ ਨਾਲ ਸੁਨਿ <pause dur="2"></pause> ਜਿਹੜਾ ਜਿਹੜਾ ਨੇ ਸਚਾ ਇਸ਼ਕ ਕਿਤਾ ਐ ਸਚਾ ਇਸ਼ਕ ਨਿਭਾਇਆ ਐ ਉਹੀ ਜਾਨਦੇ ਐ ਪਿਆਰ ਇਸ਼ਕ ਮਹੋਬਤ ਕਿ ਐ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="16" who="PM001">ਜੀ ਔਔਔ ਬੀ ਬੀ ਸੀ ਏਸ਼ਿਯਨ ਨੇਟਵਰਕ ਤੇ ਤੁਸੀ ਸਾਡਾ <foreign lang="eng">programme</foreign> ਡੀਜਿਟਲ ਸੇਟੇਲਾਟ ਚੈਨਲ <foreign lang="eng">869</foreign> ਦੇ ਜਰਿਏ ਔਰ ਇੰਟਰਨੇਟ ਦੇ ਜਰੀਏ <vocal desc="er"></vocal> ਪੁਰੀ ਦੁਨਿਆ ਭਰ ਸਾਡੇ <foreign lang="eng">programme</foreign> ਨੂੰ ਸੁਨ ਰਹੇ ਹੋ ਔਰ ਅਜ ਦੇ ਅਜ ਦੇ <foreign lang="eng">programme</foreign> ਤੁਸੀ ਦਲਜੀਤ ਮੀਰ ਦੀ ਜੁਬਾਨੀ ਸੁਨ ਰੇ ਹੋ ਮੇਰਾ ਸਾਥ ਦੇ ਰਹੀ ਐ ਸ਼ੇਫਾਲੀ ਔਰ ਅਗਰ ਤੁਹਾਡੇ ਪੈਗਾਮ ਤੁਹਾਡੇ ਮੈਸੇਜ ਵਿਚ ਕੋਈ ਕਮੀ ਰਹੀ ਯਾ ਸਾਡੇ ਕੋਲ ਕੋਈ ਗਲਤੀ ਹੋ ਗਈ ਹੋ ਤੋ ਜਰੂਰ ਸਾਨੁ ਦਸਿਆ ਕਰੋ ਔਰ ਹੈਲੋ ਔਕਾਰ ਸਿੰਗ ਜੀ ਤੁਸੀ ਬਰਹਿੰਗਮ ਤੋ ਪੋਨ ਕਿਤਾ ਐ ਤੁਸੀ ਕਿਹਾ ਯਾ ਤੋ ਫਿਰ ਸੁਰਜੀਤ ਕੌਰ ਦਾ <foreign lang="eng"> remix </foreign> ਗਾਨਾ ਯਾ <vocal desc="er"></vocal> ਜਾਨ <vocal desc="er"></vocal> ਜਾਨ ਜਾਨ ਕੇ ਦਿਲ ਯੇ ਨਚਦੀ ਦੁਪੱਟਾ ਮੇਰੀ ਪਗ ਮਾਰਦਾਂ ਆਹਾਂ ਬਹਤੁ ਪਿਅਰਾ ਗੀਤ ਐ ਇਹ ਔਰ ਯਾ ਫਿਰ ਗੁਰਦਾਸ ਮਾਨ ਜੀ ਦੀ ਤੁਸੀ <omit extent="1 word" cause="sound quality"></omit> ਦੇ ਵਿਚੋ ਕੋਈ ਗੀਤ ਸੁਨਨਾ ਚਾਹੁੰਦੇ ਹੋ ਤੁਹਾਨੁ ਸੁਰਜੀਤ ਕੌਰ ਦਾ ਗੀਤ ਸੁਨਾ ਰੇ ਹੈ ਉਮੀਦ ਹੈ ਤੁਹਾਨੁ ਪਸੰਦ ਆਂਏਗਾ ਹੁਨ ਚਲਦੇ ਐ ਅਸੀ ਗੀਤ ਵਲ ਮੈ ਤਮਾਮ ਤੁਹਾਡੇ ਦੋਸਤਾਂ ਨਾਂ ਕਰਦੇ ਐ ਏ ਨੀ ਹੈ ਮੇਰੇ ਖਿਆਲ ਏ ਗੀਤ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="17" who="PM001">ਬੋਤਲਾਂ ਸ਼ਰਾਬ ਦੀਆਂ ਅਖਾਂ ਤੇਰੀਆਂ ਬਈ ਇਸ ਤਰਾਂ ਯਦੀ ਅਖਾਂ ਨਾਲ ਈ ਬੰਦ ਤੇ ਨਸ਼ਾ ਹੋਨ ਲਗ ਗਿਆ <vocal who="PM001" desc="laugh"></vocal> <pause dur="2"></pause> ਏ ਤਾਂ ਠੀਕ ਐ ਉਹਨਾ ਦਾ ਮੰਦਾ ਹਾਲ ਹੋ ਜਾਏਗਾ ਸ਼ਰਾਬ ਦੀ ਦੁਕਾਨ ਤੇ <omit extent="1 word" cause="sound quality"></omit> ਖੈਰ ਕੁਛ ਅਖਾਂ ਹੁੰਦੀਆਂ ਹੀ ਐਸੀਆਂ ਐ ਮਸਤਾਨੀਆਂ ਕੇ ਜਿਹਨਾਂ ਨੂੰ ਵੇਖਦਿਆਂ ਨਸ਼ਾ ਹੂੰਦੈ ਔਰ ਇਹ ਕੁਦਰਤ ਵਲੋਂ ਹੂੰਦਿਐਂ ਹਰ ਬੰਦੇ ਨੂੰ ਏਸ ਤਰਾਂ ਦੀ ਅੱਖ ਤੋ ਬਚਨੁੰ <vocal who="PM001" desc="laugh"></vocal> ਅੱਲਾ ਤਾਲਾ ਕਰਵਾ ਦਵੇ । ਕੁਛ ਅੱਖਾਂ ਐਸੀਆਂ ਹੂੰਦੀਐਂ ਜਿਹਨਾਂ ਨੂੰ ਵੇਖਿਆਂ ਨਸ਼ਾ ਹੂੰਦੈ ਸਰੂਰ ਹੂੰਦੇ ਕੁਛ ਅੱਖਾਂ ਦੇ ਵਿਚ ਕੁਦਰਤੀ ਕ੍ਰਿਸ਼ਮਾਂ ਗੂੰਦੇ ਯਾਂ ਤਾਂ ਬਹੁਤ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਂ ਸੁਰਜੀਤ ਸਾਹਿਬ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਸੁਣਿਐ । ਔਰ ਜਨਾਬ ਔਕਾਰ ਸਾਹਿਬ ਜੀ ਤੁਹਾਡੇ ਨਾਂ ਤੇ ਤੁਹਾਡੇ ਦੋਸਤਾਂ ਦੇ ਨਾ ਕੀਤੈ ਅਸੀਂ ।ਤੁਸੀਂ ਏਸ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਸੀ ਤੁਹਾਡੇ ਦੋਸਤ ਅਮਰੀਕ, ਬਰਾੜਾ ਮਿਲਕੀ ਤੇ ਸੋਨੀ ਉਮੀਦ ਐ ਕੇ ਤੁਸੀਂ ਏਸ ਗੀਤ ਦਾ ਆਨੰਦ ਮਾਨਿਆ ਹੋਏਗਾ ਸਾਡੇ ਦੋਸਤ <vocal desc="er"></vocal> ਫੋਨ ਕਰ ਰਹੇ ਸਨ <vocal desc="er"></vocal> ਮਸੂਦ ਭਾਜੀ ਸਾਨੂੰ ਬਲਿਟਨ ਤੋ ਉਹਨਾਂ ਨੇ ਕਿਹਾ ਕੇ ਰਫੀ ਸਾਹਿਬ ਦਾ ਇਕ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ ਸੁਣਾ ਦਿਓ ਅਸੀਂ ਰਫੀ ਸਾਹਿਬ ਦਾ ਇਕ ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਸੁਨਾਉਦੇ ਹਾਂ ਔਰ ਇਹ ਗੀਤ ਅਸੀਂ ਫਿਲਮ ਦੇ ਵਿਚੋਂ ਲਿਐ ਜਨਾਬ ਔਰ ਇਹ ਫਿਲਮ ਮੇਰੇ ਖਿਆਲ ਸੀਗੀ 'ਕਣਕਾਂ ਦੇ ਓਹਲੇ' ਆਓ ਸੁਣ ਕੇ ਦੇਖਦੇ ਐ ਏਸ ਗੀਤ ਨੂੰ । ਹੈ ਤੇ ਬਹੁਤ ਪਿਆਰਾ ਗੀਤ ਹੈ ਏਹੇ ਅਗਰ ਚਲ ਗੀ ਸੀ ਡੀ ਤਾਂ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="18" who="PM001">ਮਹੱਮਦ ਰਫੀ ਸਾਹਿਬ ਨੂੰ ਸੁਣਿਆ ਤੁਸੀਂ ਫਿਲਮ <vocal who="PM001" desc="laugh"></vocal> 'ਕਣਕਾਂ ਦੇ ਓਹਲੇ', 'ਕਣਕਾਂ ਦੇ ਓਹਲੇ' ਦੇ ਵਿਚੋਂ ਇਹ ਗੀਤ ਲਿਆ ਸੀ ਅਸੀਂ, 'ਕਣਕਾਂ ਦੇ ਓਹਲੇ ' ਧਰਮੇਂਦਰ ਅਤੇ ਆਸ਼ਾ ਪਾਰੇਖ ਦੀ ਫਿਲਮ ਸੀ ਏਹੇਔਰ ਉਮੀਦ ਐ ਕੇ ਤੁਸੀਂ ਵੇਖੀ ਹੋਣੀ ਐ ਇਹ ਫਿਲਮ ਔਰ ਬੜਾ ਪਿਆਰਾ ਰੋਲ ਕੀਤਾ ਸੀ ਆਸ਼ਾ ਪਾਰੇਖ ਨੇ । ਆਸ਼ਾ ਪਾਰੇਖ ਨੇ ਇਹਦੇ ਵਿਚ ਪੰਜਾਬੀ ਖੜੀ ਬੋਲੀ ਸੀ ਜੇ ਤੁਸੀਂ ਦੇਖੀ ਹੋਣੀ ਐ ਇਹ ਫਿਲਮ ।</u>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="19" who="PM001"> ਅਗਰ ਕਿਤੇ ਵੀ ਤੁਸੀਂ ਸਾਡਾ <foreign lang="eng">Programme</foreign> ਸੁਣ ਰਹੇ ਓ ਦਰਬਾਰ ਦੀ ਘੜੀ ਦੇ ਮੁਤਾਬਿਕ ਠੀਕ ਸੱਤ ਮਿੰਟ ਬਾਕੀ ਹਨ ਨੌ ਵੱਜਣ ਵਿਚ । ਪਰ ਇਥੇ ਅੱਜ ਬੜਾ ਪਿਆਰਾ ਮੌਸਮ ਐ <omit extent="1 word" cause="sound quality"></omit> ਦਾ ਬੜਾ ਅੱਛੈ । ਲੈਸਟਰ ਵਿਚ ਵੀ ਅੱਜ ਕਾਫੀ ਧੁਪ ਐ <vocal desc="er"></vocal>ਦੋ ਦਿਨਾਂ ਤੋਂ ਕਾਫੀ ਧੁਪ ਐ ਨਿਕਲੀ ਹੋਈ ਐ ਬੜੀ ਅੱਛੀ ਮਤਲਬ ਧੁਪ ਐ ਸਾਡੇ ਮੁਲਕਾਂ ਦੇ ਵਿਚ ਤਾਂ ਚਾਲੀ ਬਿਆਲੀ ਪੰਤਾਲੀ ਸੰਤਾਲੀ ਐਸ ਟਾਇਮ ਮਤਲਬ ਗਰਮੀ ਹੋਈ ਐ ਪਰ ਤੁਸੀਂ ਉਥੇ ਦੇਖ ਲਓ ਸਾਡੇ ਜਿਹੜੇ ਭੈਣ ਭਾਈ ਗਰਮੀ ਦੇ ਵਿਚ ਵੀ ਮੇਹਨਤ ਦੇ ਨਾਲ ਆਪਣੇ ਢਿਡ ਪਾਲਦੇ ਨੇ ਆਪਣੇ ਬੱਚੇ ਪਾਲਦੇ ਨੇ ਉਹਨਾਂ ਦੇ ਸਦਕੇ ਜਾਇਏ ਕੇ ਆਪਣੇ ਪਸੀਨਿਆਂ ਨੂੰ ਵਹਾ ਵਹਾ ਕੇ ਆਪਣੇ ਪਰਿਵਾਰਾਂ ਨੂੰ ਪਾਲਦੇ ਨੇ । ਇਥੇ ਥੋੜੀ ਜਿਹੀ ਗਰਮੀ ਹੂੰਦੀ ਐ ਅਸੀਂ ਕਹਿਨੇ ਐ ਹਾਏ ਜੀ ਪੱਖਾ ਲਿਆ ਦਿਓ । <omit extent="1 word" cause="sound quality"></omit> <foreign lang="eng">one line</foreign> ਬੜੇ ਬੜੇ ਨਖਰੇ ਕਰਦੇ ਐ ਅਸੀਂ । ਚਲੋ ਐਹ ਗੀਤ ਸੁਨਾਇਏ ਅਸੀਂ ਤੁਹਾਨੂੰ ਵਿਚੌਂ ਹੱਸਦੀ ਤੇ ਉਤੋਂ ਨਖਰੇ <vocal who="PM001" desc="laugh"></vocal> । ਪਰ ਗਰਮੀ ਸਰਦੀ ਬਰਦਾਸ਼ਤ ਕਰਨੀ ਚਾਹੀਦੀ ਐ ਗਰਮੀ ਵੀ ਕੁਦਰਤ ਦੇ ਮੌਸਮ ਐ ਕੁਛ ਲੋਗ ਗਰਮੀ ਨੀ ਬਰਦਾਸ਼ਤ ਕਰ ਸਕਦੇ ਕੁਛ ਸਰਦੀ ਕੁਛ ਭੈੜਾ ਤਨ ਏਸਾ ਹੀ ਬਣਾਇਆ ਹੈ ਰਬ ਨੇ ਸਾਡੇ ਮੁਲਕਾਂ ਦੀ ਗੱਲ ਕਰੋ ਤੇ ਏਸ ਵਕਤ ਪਤਲੇ ਵੈਲ ਦੇ ਕਪੜੇ ਪਾਏ ਨੇ ਕੁੜਤੀ ਸਾਨੂ ਉਹ ਵੇਲਾ ਯਾਦ ਆਦਾ ਹੈ ਜਦੋਂ ਬਰਫ ਲੈਣ ਜਾਂਦੇ ਹੂੰਦੇ ਸੀ ਤੇ ਬਰਫ ਵੀ ਨੀ ਸੀ ਕਦੇ ਕਦੇ ਮਿਲਦੀ ਹੂੰਦੀ ਸੀ ਕਹਿੰਦੇ ਜੀ ਮੁਕ ਗਈ ਐ ਯਾਂ ਫੇਰ ਬਲੈਕ ਚ ਮਿਲਦੀ ਹੂੰਦੀ ਸੀ ਬਰਫ ਕਿਨੀ ਗਰਮੀ ਹੂੰਦੀ ਸੀ ਮਲਮਲ ਦਾ ਕੁੜਤਾ ਹੋਵੇ ਔਰ ਬਾਹਰ ਮੰਜਾ ਡਾਹਿਆ ਹੋਵੇ ਬੋਹੜ ਦੇ ਥੱਲੇ ਗਰਮੀਆਂ ਦੇ ਵਿਚ । ਤਾਂ ਆ ਹਾ ਹਾ ਕਿਆ ਬਾਤ ਐ ਉਹਦੇ ਨਜਾਰੇ ਈ ਵੱਖਰੇ ਹੂੱਦੇ ਐ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="20" who="PM001">ਲਓ ਜੀ ਤੁਸੀਂ ਇਹ ਗੀਤ ਸੁਣਿਆ ਦਲਜੀਤ ਸਿੰਘ ਦੀ ਆਵਾਜ਼ ਦੇ ਵਿਚ ਔਰ ਮੌਸਮ ਦੇ ਹਿਸਾਬ ਨਾਲ ਅਸੀਂ ਤੁਹਾਨੂੰ ਇਹ ਗੀਤ ਸੁਨਾਇਆ ਹੈ, ਏਥੇ ਤਾਂ ਫੇਰ ਵੀ ਅਸੀਂ ਥੁੜੇ ਜਿਹੇ ਮੋਟੇ ਕਪੜੇ ਪਾ ਸਕਦੇ ਐ ਜੇ ਗਰਮੀ ਹੂੰਦੀ ਐ ਅਪਣੇ ਮੁਲਕਂ ਦੇ ਵਿਚ ਜਿਨੀ ਗਰਮੀ ਪੈ ਰਹੀ ਹੈ ਇਥੇ ਤਾਂ ਕੁੜਤਾ ਵੀ ਨੀ ਚਲਣਾ ਬੁਨੈਣ ਵੀ ਨੀ ਚਲਨੀ ਬੜੀ ਗਰਮੀ ਜੀ ਬੜਾ ਪਸੀਨਾ ਆਉਦਾ ਐ ਬਹਰਹਾਲ ਤੁਹਾਡੇ <vocal desc="er"></vocal> ਤੁਹਾਡਾ ਸਾਥ ਚਾਹਾਂਗੇ ਪੂਰੇ ਦਸ ਵਜੇ ਤੱਕ ਔਰ ਦਸ ਵਜੇ ਤੱਕ ਸਾਡੀ ਗਲਬਾਤ ਚਲਦੀ ਰਵੇਗੀ ਔਰ ਧੀਰੇਧੀਰੇ ਗੀਤ ਔਰ ਤੁਹਾਡੇ <foreign lang="eng">massage</foreign> ਤੁਹਾਡੇ ਪੈਗਾਮ ਅਸੀਂ ਸ਼ਾਮਿਲ ਰੱਖਂਗੇ ਔਰ ਐਸ ਵੇਲੇ <omit extent="1 word" cause="sound quality"></omit> ਨੌ ਵੱਜੇ ਹਨ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="21" who="PM001">ਜੀ ਹਾਂ ਸਾਡੇ <foreign lang="eng">studio</foreign> ਦੀ ਘੜੀ ਵਿਚ ਠੀਕ ਨੌ ਵਜੇ ਹਨ ਹੁਣ ਤੁਸੀਂ ਪੰਜਾਬੀ ਵਿਚ ਖ਼ਬਰਾਂ ਸੁਣੌ ਸੁਰਿੰਦਰ ਕੂਰ ਦੀ ਜਬਾਨੀ ।</u>

<u id="22" who="PF021">ਅੱਜ ਦੇ ਮੁਖ ਸਮਾਚਰ :-ਸ਼ਾਨੇ ਪੰਜਾਬ ਰੇਲ ਗਡੀ ਦੇ ਇੰਜਨ ਵਿਚ ਅਗ ਲਗਨ ਵਾਲੇ ਖਬਰ ਮਿਲਿ, ਐ ਇੰਡਿਆ ਵਿਚ ਜਿਆਦਾ ਗਰਮੀ ਨਾਲ ਕੋਈ ਅਠ ਸੌ ਦੇ ਕਰੀਬ ਲੋਕ ਮਾਰੇ ਗਏ, ਹੈਦਰਾ ਬਾਦ ਦੇ ਇਕ ਹੋਰ ਧਮਾਕੇ ਵਿਚ ਚਾਰ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ਹੈ, ਟੋਨੀ ਵਿਡਲੀ ਬਰਿਟੇਨ ਦੀ ਸਬ ਤੋ ਵਡੀ ਯੁਨਿਅਨ ਦੇ ਲੀਡਰ ਚੁਨੇ ਗਏ, ਏਅਰ ਫ੍ਰਾਂਸ ਕਾਂਕੋਟ <foreign lang="eng">air service</foreign> ਨੂੰ ਸਤਾਈ ਸਾਲ ਬਾਅਦ ਬੰਦ ਕਰ ਦਿਤਾ ਚਗਿਆ ਹੁਣ ਖਬਰਾਂ ਵਿਸਤਾਰ ਨਾਲ ।</u>

<u id="23" who="PF021">ਇੰਡਿਆ ਦੇ ਉਤਰੀ <foreign lang="eng">state</foreign> ਪੰਜਾਬ ਵਿਚ ਰੇਲਵੇ ਵਰਕਰ ਨੇ ਲਗੀ ਹੁਈ ਅਗ ਨਾਲ ਜਲਦੇ ਹੁਏ ਇੰਜਨ ਵਿਚ ਬੜੀ ਹੋਸ਼ਿਆਰੀ ਨਾਲ <foreign lang="eng">passanger</foreign> ਟ੍ਰੇਨ ਤੋ ਜੁਦਾ ਕੀਤਾ । ਸ਼ਾਨੇ ਪੰਜਾਬ ਦੇ ਇੰਜਨ ਤੋ ਅਗ ਅਗ ਦੀਆਂ ਲਪਟਾਂ ਦਿਖਾਈ ਦੇ ਰਹਿਆਂ ਸਨ ਜਦੋ ਰੇਲ ਗਡੀ <foreign lang="eng">central</foreign> ਪੰਜਾਬ ਵਿਚ ਫਗਵਾੜਾ <foreign lang="eng">station</foreign> ਤੇ ਰੁਕੀ ਇਸ ਘਟਨਾ ਵਿਚ ਕਿਸੀ ਦਾ ਵੀ ਜਖਮੀ ਹੋਨ ਬਾਰੇ<foreign lang="eng">report</foreign> ਨਹੀ ਮਿਲੀ ।</u>

<u id="24" who="PF021"> <pause dur="2"></pause> ਇੰਡਿਆ ਦੇ ਦਖਨੀ <foreign lang="eng"> state </foreign> ਆਂਧ੍ਰ ਪ੍ਰਦੇਸ਼ ਵਿਚ ਬਹੁਤ ਗਰਮੀ ਪੈਣ ਨਾਲ ਕਾਫਿ ਲੋਕਾਂ ਦੀ ਮੌਤ ਹੋ ਗਈ ਐ । <foreign lang="eng">authority</foreign> ਨੇ ਦਸਿਆ ਐ ਕਿ ੧੮ ਦਿਨਾਂ ਦੀ ਗਰਮੀ ਨਾਲ ਮਰਨ ਵਾਲੇਆਂ ਦੀ ਗਿਨਤੀ ਕੋਈ ਸਤ ਸੌ ਪਿਚਨਵੇ ਹੋ ਗਈ ਹੈ । ਨਰਗੋੰਡਾ ਵਿਚ ਇਕ ਸੌ ਅਠਵੰਜਾ ਲੋਕਾਂ ਦੀ ਮਰਨ ਵਾਰੇ <foreign lang="eng">report</foreign> ਦਿਤੀ ਗਈ ਐ । ਪਿਛਲੇ ਚੌਬੀ ਘੰਟਿਆ ਵਿਚ ਪ੍ਰਾਂਤ ਦੇ ਦੁਸਰੇ ਹਿਸਿਆਂ ਚੋ ਵੀ ਕਈ ਮੌਤਾ ਹੋਨ ਵਾਰੇ <foreign lang="eng"> report </foreign> ਮਿਲਿ ਐ । ਗੰਟੂਰ ਵਿਚ ਇਕ ਸੌ ਚੌਦਾਂ ਮੌਤਾ ਅਤੇ ਪਛਮੀ ਗੋਦਾਵਰੀ ਵਿਚ ਇਕ ਸੌ ਛੇ ਮੌਤਾਂ ਮੌਤਾਂ ਦੇ ਹੋਨ ਵਾਰੇ ਪਤਾ ਲਗਾ ਐ । ਇਸ ਵਕਤ ਇਥੇ ਤਾਪਮਾਨ ੪੫ ਡਿਗ੍ਰੀ ਸੇਲਸਿਅਸ ਹੈ ਅਤੇ ਦਸਿਆ ਗਿਆ ਐ ਕਿ ਹਲੇ ਅਗਲੇ ੪੮ ਘੰਟੇਆਂ ਵਿਚ ਕੋਈ ਵੀ ਤਬਦੀਲੀ ਆਉਨ ਦੀ ਆਸ ਨਈ ਐ । <pause dur="2"></pause> ।</u>

<u id="25" who="PF021"><foreign lang="eng">U.s.a</foreign>ਤੋਂ <omit extent="1 word" cause="sound quality"></omit> ਨੇ ਦਸਿ ਹੈ ਕਿ ਅਮਰੀਕਾ ਨੂੰ ਜਿਸ ਆਦਮੀ ਦੀ ਤਲਾਸ਼ ਸੀ ਉਸਨੁ ਗਿਰਫਤਾਰ ਕਰ ਲਿਆ ਐ ਆਰਿਕਰਡਲ ਦਾ <omit extent="1 word" cause="sound quality"></omit> ਨਾਲ ਸੰਬੰਧ ਹੋਨ ਦਾ ਸ਼ਕ ਐ ਜਿਸ ਵਿਚ ਉਨੀਸ ਸੌ ਛਿਆਨਵੇ ਵਿਚ ਅਟਾਲੰਟਾ ਦਿਆਂ <foreign lang="eng">olympic</foreign> ਖੇਡਾਂ ਦੌਰਾਨ ਬੰਬ ਧਮਾਕੇ ਦੀ ਘਟਨਾ ਵੀ ਸ਼ਾਮਲ ਐ ਇਸ ਘਟਨਾ ਵਿਚ ਇਕ ਔਰਤ ਦੀ ਮੌਤ ਹੋ ਗਈ ਸੀ ਦਸਿਆ ਗਿਆ ਐ ਕਿ ਰੋਡਾਲਫ ਨੂੰ <foreign lang="eng">by chance north cereble</foreign> ਵਿਚ ਵੇਖਿਆ ਗਿਆ ਸੀ ਤੇ ਫਿਰ ਉਸ ਨੁ ਗਿਰਫਤਾਰ ਕਿਆ ਗਿਆ ਐ ।</u>

<u id="26" who="PF021">ਹੈਦਰਾ ਬਾਦ ਤੋ ਡਿਗ੍ਰੀ ਲੇ ਜਾਨ ਵਾਲੀ ਬਸ ਤੇ ਇਕ ਧਮਾਕਾ ਹੋਨ ਨਾਲ ਚਾਰ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ਹਨ ਏ ਸ਼ਹਰ ਵਿਚ ਦੁਸਰਾ ਧਮਾਕਾ ਹੋਇਆ ਐ ਇਸਤੋ ਪਹਿਲਾਂ ਇਸ ਮਹਿਨੇ ਦੇ ਸ਼ੁਰੂ ਵਿਚ ਇਕ ਬਸ ਤੋ ਧਮਾਕਾ ਹੋਇਆ ਸੀ ਜਿਸ ਵਿਚ ਗਿਆਰਾਂ ਲੋਕ ਜਖਮੀ ਹੋ ਗਏ ਸਨ । ਇਹਨਾ ਹਮਲੇਆਂ ਦੀ ਕਿਸੀ ਨੇ ਵੀ ਜਿਮੇਵਾਰੀ ਨਹੀ ਲੀ ਐ । ਏ ਖਬਰਾਂ ਤੁਸੀ ਬੀ ਬੀ ਸੀ ਏਸ਼ਿਯਨ ਨੇਟਵਰਕ ਸੁਰਿੰਦਰ ਕੌਰ ਦੀ ਜੁਬਾਨੀ ਸੁਨ ਰੇ ਹੋ ।</u>

<u id="27" who="PF021">ਲੇਫਟਿਨੇੰਟ ਟੋਨੀ ਵਿਡਲੀ ਨੇ ਬ੍ਰਿਟੇਨ ਦੀ ਸਬ ਤੋ ਵਡੀ ਯੁਨਿਅਨ ਵਿਚ <foreign lang="eng">Transport</foreign> ਅਤੇ ਜਨਰਲ ਲੀਡਰ ਸ਼ਿਪ ਜਿਤ ਲਈ ਐ । ਟੋਨੀ ਵਿਡਲੀ ਨੇ ਜੈਕ ਡ੍ਰਾਮਿਨ ਨੂੰ ਹਰਾਇਆ ਅਤੇ ਉਹ ਮਾਰਿਫ ਦੀ ਜਗਹ ਲੈਨਗੇ ਜੋ ਕਿ ਅਕਟੁਬਰ ਵਿਚ ਰਿਟਾਅਰ ਹੋ ਰਹੇ ਹਨ ਮਿਸਟਰ ਵੁਡਲੀ ਖੁਲੀ ਤਰਾਂ ਸਰਕਾਰ ਦੇ ਆਲੋਚਕ ਹਨ ਅਤੇ ਉਹਨਾ ਨੇ ਕੰਮ ਕਰਨ ਵਾਲੇਆਂ ਦੇ ਹਕਾਂ ਦੀ ਹਿਫਾਜਤ ਨਾ ਕਰਨ ਦਾ ਦੋਸ਼ ਸਰਕਾਰ ਨੂੰ ਦਿਤਾ ਐ । <pause dur="2"></pause> ਜਾਰਜ ਬੁਸ਼ ਸੇਂਟ ਪੀਟਰ ਬਰਗ ਵਿਚ ਅਪਨੇ ਸਬੀ ਗਲ ਬਾਤ ਰਸ਼ਿਅਨ ਪ੍ਰੇਸੀਡੇਂਟ ਵਾਲਦੀਮੀਰ ਪੁਤਿਨ ਨਾਲ ਕਰਨਗੇ ਉਹਨਾ ਦੀ ਇਰਾਕ ਦੀ ਲੜਾਈ ਬਾਰੇ ਮਤ ਭੇਦ ਹੋਨ ਤੋ ਬਾਦ ਇਹ ਉਹਨਾ ਦੀ ਪਹਿਲੀ ਆਮਨੇ ਸਾਮਨੇ ਗਲ ਬਾਤ ਹੋਵੇਗੀ ।</u>

<u id="28" who="PF021">ਹਾਲ ਹੀ ਵਿਚ <foreign lang="eng">MisterBush</foreign> ਨੇ ਯੂਰੋਪ ਤਥਾ <foreign lang="eng">United state</foreign> ਨੂੰ <foreign lang="eng">terrorism aids</foreign> ਅਤੇ ਗਰੀਬੀ ਨੂੰ ਖਤਮ ਕਰਨ ਲਈ ਇਕਥੇ ਹੋਕਰ ਕੰਮ ਕਰਨ ਲਈ ਪ੍ਰੇਰਿਤ ਕਿਤਾ ਹੈ । ਅਕੈਡਮਿਕ ਫਿਲਮ ਨਿਰਦੇਸ਼ਕ ਅਨਿਲ ਬਿਸਵਾਸ ਦਾ ਦਿੱਲੀ ਵਿਚ ਦੇਹਾਂਤ ਹੋ ਗਿਆ ਹੈ ਉਹਨਂ ਦੀ ਉਮਰ ਉਨਾਣਵੇਂ ਸਾਲ ਸੀ <foreign lang="eng">Mr. Bisvas</foreign>  ਦਾ ਜਨਮ ਉਨੀ ਸੌ ਚੋਦਾਂ ਵਿਚ ਜੋ ਕੇ ਹੁਣ ਬੰਗਲਾ ਦੇਸ਼ ਵਿਚ ਹੈ ਹੋਇਆ ਸੀ ਉਹਨਾਂ ਨੇ ਉਨੀ ਸੌ ਤੀਹ ਵਿਚ ਹਿੰਦੀ ਫਿਲਮਜ਼ ਨਾਲ ਕੰਮ ਕਰਣਾ ਸ਼ੁਰੂ ਕੀਤਾ ਸੀ ਅਤੇ ਉਹਨਾਂ ਨੇ <foreign lang="eng">singer Mukesh </foreign> ਅਤੇ ਤਾਰਿਕ ਮਹਿਮੂਦ ਨੂੰ ਫਿਲਮ <foreign lang="eng">Industory</foreign>
ਨਾਲ ਮਿਲਾਇਆ ਸੀ ।</u>

<u id="29" who="PF021">ਪੁਲਿਸ ਨੂੰ ਖਿਆਲ ਹੈ ਕੇ ਦੈਲਫਾਸਟ ਕੇ ਡੈਲਫਾਸਟ ਚਦੇ ਇਕ ਇਲਾਕੇ ਤੋ ਇਕ ਲਾਪਤਾ ਅਦਮੀ ਦਾ ਕਤਲ ਹੋ ਗਿਆ ਹੈ ਐਲਨ ਨੂੰ ਆਖਿਰੀ ਵਾਰ ਬੁਧਵਾਰ ਰਾਤ ਨੂੰ ਸ਼ੈਂਕੀਨ ਇਲਾਕੇ ਵਿਚ ਉਸਦੇ ਘਰੋਂ ਜਾਂਦੇ ਦੇਖੀਆ ਗਿਆ ਸੀ । ਉਹ ਹਾਲ ਹੀ ਵਿਚ <foreign lang="eng">northern Iland</foreign> ਵਿਚ ਵਾਪਿਸ ਆਇਆ ਸੀ । ਉਸਤੋਂ ਪਹਿਲਂਾ ਉਹ ਨੌਲੈਸਟ ਦੰਗਿਆਂ ਕਾਰਣ ਉਥੋ ਭੱਜਿਆ ਹੋਇਆ ਸੀ ।</u>

<u id="30" who="PF021"><pause dur="2"></pause> ਏਅਰ ਫ੍ਰਾਂਸ ਕਾਨਫ੍ਰੇਂਸ ਆਪਣੀ ਆਖਰੀ ਉਡਾਨ ਤੋ ਬਾਦ ਪੈਰਿਸ ਵਿਚ ਉਤਰਿਆ ਇਹ ਉਡਾਨ <foreign lang="eng">new york</foreign> ਤੋ ਪੈਰਿਸ ਆਈ । ਬ੍ਰਿਟਿਸ਼ ਏਅਰ ਵੇਜ ਦੀ ਤਰਾਂ ਏਅਰ ਫ੍ਰਾਂਸ ਵੀ ਉਹਨਾ ਦੇ ਕਾਂਕੋਡਸ ਨੂੰ ੨੭ ਸਾਲਾਂ ਦੀ ਸਰਵਿਸ ਦੇ ਬਾਦ ਉਸਦੀ ਉੜਾਨ ਬੰਚਦ ਕਰ ਰੇ ਹਨ ।</u>

<u id="31" who="PF028">ਹੁਨ ਪੇਸ਼ ਐ ਮੌਸਮ ਅਜ ਦਾ ਮੌਸਮ ਖੁਸ਼ਕ ਗਰਮ ਅਤੇ ਧੁਪ ਦਾਰ ਐ ਪਰੰਤੁ ਗਰਮੀ ਦੇ ਬਾਦ ਸ਼ਾਯਦ ਕਿਤੇ ਕਿਤੇ ਬਦਲਾਂ ਦੀ ਗੜਗੜਾਹਟ ਦੇ ਬਾਦ ਬਾਰਿਸ਼ ਹੋਵੇਗੀ । ਤਾਪਮਾਨ ਵਧ ਤੋ ਵਧ ੩੦ ਡਿਗ੍ਰੀ ਸੇਲਸਿਅਸ ਰਹੇਗਾ । ਬੀ ਬੀ ਸੀ ਏਸ਼ਿਯਨ ਨੇਟਵਚਰਕ ਤੋ ਖਬਰਾਂ ਖਤਮ ਹੋਇਆਂ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event> 

<event desc="music" dur="approximately 10 seconds"></event>

<u id="32" who="PM001">ਤੁਸੀਂ ਇਹ ਖ਼ਬਰਾਂ ਸੁਣ ਰਹੇ ਸੀ ਸੁਰਿੰਦਰ ਕੌਰ ਦੀ ਜਬਾਨੀ । ਨੌ ਵੱਜ ਕੇ ਤੇ ਪੰਜ ਮਿੰਟ ਹੋਏ ਹਨ । <foreign lang="eng">programme</foreign> ਦੇ ਦੂਸਰੇ ਭਾਗ ਵਿਚ ਯਾਨੀ ਆਖਿਰੀ ਭਗ ਵਿਚ ਤੁਹਾਡਾ ਸੁਆਗਤਹੈ ਖੈਰ ਮਕਦਮ ਅਗਰ ਤੁਸੀਂ ਹੁਣੇ<foreign lang="eng">radio on </foreign>
ਕੀਤੈ ਤੇ ਤੁਸੀਂ ਇਹ ਪੰਜਾਬੀ <foreign lang="eng">programme</foreign> ਸੁਣ ਰਹੇ ਹੈ ਸਾਡਾ ਤੇ ਤੁਹਾਡਾ ਸਾਥ ਰਹੇਗਾ ਪੂਰੇ ਦਸ ਵਜੇ ਤੱਕ ਸਾਡਾ ਨੰਬਰ ਉਹੀ ਹੈ <foreign lang="eng">08 459 440 445</foreign> ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="33" who="PM001">ਸੁਰਿੰਦਰ ਕੌਰ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਤੁਸੀਂ ਇਹ ਗੀਤ ਸੁਣੀਐ ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਔਰ ਸਾਡੇ ਕੋਲ ਕੁਛ <foreign lang="eng">Massage </foreign> ਨੇ ਹਰਮਿੰਦਰ ਸਿੰਘ ਜਗਤਾਰ ਜੱਗਾ ਤੁਸੀਂ <vocal desc="er"></vocal> ਕਿਹੈ ਕੇ ਕੌਣ ਚੜ੍ਹਦੇ ਸੂਰਜ ਨੂੰ ਸਲਾਮ ਕਰਦੈ ਡੁਬਦੇ ਨੂੰ ਕੌਣ ਪੁਛਦਾ । ਇਹ ਮੇਰਾ ਖਿਆਲ ਗੀਤ ਐ ਏਸ ਗੀਤ ਦੀ ਤੁਸੀਏ ਮੇਰੇ ਖਿਆਲ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਐ ਵਾਕਿਆ ਜੀ ਵੇਸੇ ਇਹ ਗੱਲ ਬਹੁਤ ਸਹੀ ਹੈ ਚੜ੍ਹਦੇ ਸੂਰਜ ਨੂੰ ਹਰ ਬੰਦਾ ਸਲਾਮ ਕਰਦੈ ਡੁਬਦੇ ਸੂਰਜ ਨੂੰ ਕੌਣ ਪੁਛਦੈ <vocal who="PM001" desc="laugh"></vocal> ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰਂਗੇ ਜੇ ਇਹ ਗੀਤ ਸਾਨੂੰ ਲੱਭ ਗਿਆ ਤਾਂ ਅਸੀਂ ਜਰੂਰ ਸੁਨਾਵਾਂਗੇ ਅਸੀਂ ਸੁਨੀਆ ਨੀ ਹਾਲੇ ਇਹ ਗੀਤ ਬਹੁਰਹਾਲ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਹਰਮਿੰਦਰ ਜੀ ਤੁਸੀਂ ਸਾਨੂੰ ਯਾਦ ਕੀਤੈ । ਬੇਨਾਮ ਜੀ ਲੈਹਸਣ ਤੋ ਤੁਸੀਂ ਫੋਨ ਕਿਤੈ ਤੁਸੀਂ ਕਿਹਾ ਹੈ ਕੇ ਤੁਸੀਂ ਗੀਤ ਵੰਝਲੀ ਦਾ ਮੇਰੇ ਖੀਆਲ ਐ ਹੀਰ ਰਾਂਝਾ ਦਾ ਇਹ ਗੀਤ ਐ । ਇਹ ਗੀਤ ਤਂ ਨੀ ਹੀਰ ਰਾਂਝਾ ਦੇ ਵਿਚੋ ਅਗਲਾ ਗੀਤ ਅਸੀਂ ਤੁਹਾਨੂੰ ਸੁਨਾਉਣ ਜਾ ਰਹੇ ਐ ਉਹ ਵੀ ਮਿਠਾ ਗੀਤ ਐ ਸੁਣ ਵੰਝਲੀ ਦੀ ਮੱਠੀ ਤਾਨ ਵੇ ਮੈਂ ਤਾਂ ਹੋ ਹੋਗਈ ਕੁਰਬਾਨ ਵੇ ਵਿਚ ਵੰਜਲੀ ਦਾ ਜਿਕਰ ਈ ਅਸੀਂ ਸੁਨਾਨਾ ਏ ਜਨਾਬ <vocal who="PM001" desc="laugh"></vocal> ਅਸੀਂ ਕੋਸ਼ੀਸ਼ ਕਰਾਂਗੇ । ਦੂਸਰੀ ਵੰਝਲੀ ਅਸੀਏ ਤੁਹਾਨੂੰ ਸੁਣਾਉਣ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="34" who="PM001">ਫਿਲਮ ਹੀਰ ਰਾਂਝਾ ਦੇ ਵਿਚੋ ਇਹ ਨਗਮਾਂ ਸੁਣੀਆ ਤੁਸੀਂ ਮਨਪ੍ਰਿਤ ਕੌਰ ਮਨਹਰ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਸਾਡੇ ਕੁਛ ਦੋਸਤਂ ਦੀਆਂ ਫਰਮਾਇਸ਼ਾਂ ਨੇ ਗੁਰਤੇਜ ਜੀ ਸਾਨੂੰ ਨਿਉਜ਼ੀ ਲੈਂਡ ਤੋਂ ਫੋਨ ਕਰਦੇ ਹਨ ਉਹਨਾਂ ਨੇ ਕੁਝ <foreign lang="eng">massage </foreign> ਵੀ ਦੇਨਾ ਸੀਗਾ । ਬਹਰ ਹਾਲ ਅਸੀਂ ਉਹਨਾਂ ਨਾਲ ਗੱਲਬਾਤ ਵੀ ਕਰਾਗੇ । ਉਹਨਾ ਦਾ <foreign lang="eng">massage</foreign> ਵੀ ਲਵਾਂਗੇ ਗੁਰੁਤੇਜ ਜੀ ਤੁਸਿ ਅਸੀ ਤੁਹਾਡਾ ਬਹੁਤ ਆਭਾਰੀ ਐ ਕਿ ਤੁਸੀ ਅਕਸਰ ਸਾਡਾ <foreign lang="eng">programme</foreign> ਸੁਨਦੇ ਹੋ,ਔਰ ਸਾਡੇ <foreign lang="eng">programme</foreign> ਦੇ ਵਿਚ ਹਿਸਾ ਲੇਂਦੇ ਹੋ ਬਹਰਹਾਲ ਤੁਸੀ ਜਿਹੜੀ ਤੁਹਾਡੀ ਫਰਮਾਇਸ਼ ਹੁੰਦੀ ਐ ਤੁਸੀ ਸਾਨੁੰ ਜਰੂਰ ਦਸਿਆ ਕਰੋ ਅਸੀ ਕੋਸ਼ੀਸ ਕਰਾਂਗੇ ਕਿ ਗੀਤ ਅਸੀ ਤੁਹਾਡੇ ਨਾਂ ਜਰੂਰ ਲਾਇਏ ਸਾਡੇ ਲਿਏ ਏਹੀ ਕਾਫਿ ਐ ਕਿ ਤੁਸੀ ਇੰਨੇ ਦੂਰ ਬੈਠੇ ਬਹਾਨਾ ਨੀ ਕਿਤਾ ਸਮੁੰਦਰੋ ਪਾਰ ਨਿਉ ਜ਼ੀਲੈਂਡ ਤੋ ਤੁਸੀ ਸਾਨੁ ਫੋਨ ਕਰਦੇ ਹੋ ਏ ਸਾਡੇ ਲਈ ਬੜੀ ਖੁਸ਼ੀ ਦੀ ਗਲ ਐ ਤੁਹਾਡੇ ਦੋਸਤ ਤੁਹਾਡੇ ਪਰੀਵਾਰ ਦੇ ਵਲੋ ਸਾਰੇਆਂ ਨੂੰ ਸਤ ਸ੍ਰੀ ਆਕਾਲ ਸਾਡੇ ਵਲੋ ਔਰ ਅਸੀ ਚਲਦੇ ਆਂ ਅਗਲੇ ਗੀਤ ਵਲ ਔਰ ਇਹ ਗੀਤ ਅਸੀ ਮੈੰਬਰ ਹੁਸੈਨ ਪਰਿਵਾਰ ਵਲੋ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="35" who="PM001">ਜੀ ਹਾਂ ਤੁਸੀ ਏ ਪੰਜਾਬੀ <foreign lang="eng">programme</foreign> ਸੁਨ ਰੇ ਹੋ ਮੇਰੇ ਨਾਲ ਗੀਤ ਸੁਨਿਆ ਐ ਮਸੂਦ ਸਾਹਿਬ ਤੁਸੀਂ ਲੈਹਸਣ ਤੋ ਫੋਨ ਕੀਤੈ ਤੁਸੀਂ ਕਹਿ ਰਗੇ ਓ ਤੁਸੀਂ <foreign lang="eng">programme</foreign> ਦਾ <foreign lang="eng">enjoy </foreign> ਕਰ ਰਹੇ ਓ ਤੁਸੀਂ ਸਾਨੂੰ ਲੈਹਸਣ ਤੋਫੋਨ ਕੀਤਾ ਔਰ ਮਹਿੰਦਰ ਸਿੰਘ ਜੀ ਵੁਲਵਾਹੰਟਨ ਤੋ ਤੁਸੀਂ ਕਿਹੈ ਕੋਈ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ ਸੁਣਾ ਦਿਓ ਅਲੀ ਜੀ ਤੁਸੀਂ ਵੀ ਫੋਨ ਕੀਤੈ ਔਰ ਅਲੀ ਸਾਡੇ ਦੂਸਰੇ ਐ ਜਿਨਾ ਨੇ ਲੈਹਸਣ ਤੋ ਫੋਨ ਕੀਤੈ ਤੁਸੀਂ ਕਿਹੈ ਕੋਈ ਪਿਆਰਾ ਜਿਹਾ ਗੀਤ ਸੁਣਾ ਦਿਓ ਮਨਮੋਹਨ ਵਾਰਿਸ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਸੁਨਣਾ ਚਾਹੂੰਦੇ ਐ । ਲਾਲੀ ਜੀ ਫੋਨ ਕਰ ਰਹੇ ਸਨ ਔਰ ਮਹੋਮਦ ਸ਼ਫੀਕ ਸਾਹਿਬ ਬਰਘਿੰਗਮ ਤੋ ਫੋਨ ਕਰ ਰਹੇ ਸਨ ਉਹਨਾਂ ਨੇ ਕਿਹੈ ਕੇ ਅਤਾ ਉਲਾ ਖਾਨ ਸਾਹਿਬ ਦੀ ਜਬਾਨੀ ਕੋਈ ਪੰਜਾਬੀ ਗੀਤ ਯਾਂ ਕੋਈ ਗਜ਼ਲ ਸੁਣਾ ਦਿਓ । ਜਨਾਬ ਅਸੀਂ ਫਿਲਹਾਲ ਚਲਦੇ ਐਂ, ਤੁਹਾਨੂੰ ਸੁਨਾਉਣੇ ਐਂ ਅਬਰਾਰ ਉਲ ਹੱਕ ਦੀ ਆਵਜ਼ ਵਿਚ ਇਕ ਗੀਤ ਮੇਰੇ ਕੁਝ ਦੋਸਤਾਂ ਨੇ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਸੀ ਜੱਗਾ ਸੁਣਨ ਦੀ ਉਹਨਾਂ ਦੀ ਫਰਮਾਇਸ਼ ਸੀ, ਲੇਕਿਨ ਮੈਂ ਮਾਫੀ ਚਾਹੂੰਨੈ ਕੇ ਇਹ ਗੀਤ ਜਿਹੜਾ ਐ, ਅਸੀਂ ਇਹ ਗੀਤ ਤੁਹਾਨੂੰ ਸੁਨਾਉਣਾ ਸੀਗਾ ਜੱਗਾ ਉਹ ਸੀ ਡੀ ਸਾਨੂੰ ਨਈ ਲੱਭੀ । ਲੇਕਿਨ ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰਦੇ ਐ ਇਹ ਗੀਤ ਤੁਹਾਨੂੰ ਜਰੂਰ ਪਸੰਦ ਆਵੇ ਇਹ ਅਬਰਾਰ ਉਲ ਹੱਕ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਹ ਗੀਤ ਹੈਗਾ ਔਰ ਮੈਨੂੰ ਉਮੀਦ ਕਰਦੈ ਇਹ ਗੀਤ ਤੁਹਾਨੂੰ ਪਸੰਦ ਆਏਗਾ । ਉਹ <foreign lang="eng">c. D. </foreign>
ਸਾਡੇ ਕੋਲ ਸੀਗੀ ਤਾਂ ਜਰੂਰ ਕਿਓਂਕਿ ਉਹਦੇ ਵਿਚੋਂ ਗੀਤ ਅਸੀਂ ਅਕਸਰ ਲਾਉਣੇ ਐਂ <vocal desc="er"></vocal> ਤੁਸੀਂ ਮੇਰਾ ਖਿਆਲ ਕਿਹੜਾ ਗੀਤ ਸੀ, ਪ੍ਰੀਤੋ ਮੇਰੇ ਨਾਲ ਵਿਆਹ ਕਰ ਲੈ । ਲੇਕਿਨ ਉਹਦੇ ਵਿਚ ਜੱਗਾ ਵੀ ਸੀ । ਪਰ ਉਹ ਸੀ ਡੀ ਨੀ ਸਾਨੂੰ ਲੱਭੀ ਫਿਲਹਾਲ ਅਸੀ ਚਲਦੇ ਐਂ ਐਸ ਗੀਤ ਵੱਲ ।</u>

<event desc="music" dur="approximately 10 seconds"></event> 

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="36" who="PM001">ਲਓ ਜੀ ਅਬਰਾਰ ਉਲ ਹੱਕ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਤੁਸਾਿਂ ਇਹ ਗੀਤ ਸੁਣਿਐ । ਕੁਛ ਸਾਡੇ ਸਰੋਤਿਆਂ ਦੀਆਂ ਫਰਮਾਇਸ਼ਾਂ ਕੁਛ ਉਦਾਸ ਗੀਤ ਵੀ ਸੁਨਣੇ ਚਾਹੂੰਦੇ ਐ ਕੁਛ ਮਤਲਬ ਐ ਪਿਆਰ ਭਰੇ ਗੀਤ ਵੀ ਸੁਣਨਾ ਚਾਹੂੰਦੇ ਨੇ ਅਸੀਂ ਮਿਲੇ ਜੁਲੇ ਗੀਤ ਸੁਨਾਉਣੇ ਐ ਤੁਹਾਨੂੰ ਭੰਗੜੇ ਆਲੇ ਗੀਤ ਵੀ ਸੁਣਾ ਰਹੇ ਐ । ਔਰ ਸਾਨੂੰ ਦਰਸ਼ਨ ਜੀ ਫੋਨ ਕਰ ਰਹੇ ਸਨ ਹਮੇਸ਼ਾ ਸਾਨੂੰ ਫੋਨ ਕਰਦੇ ਨੇ ਤੁਸੀਂ ਕਿਹੈ ਕੇ ਤੁਸੀਂ ਸਾਡਾ <foreign lang="eng">programme</foreign> ਸੁਣ ਰਹੇ ਓ ਔਰ ਤੁਸੀਂ ਗੁਰਦਾਸ ਸਿੰਘ ਪਰਦੇਸੀ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਇਹ ਗੀਤ ਸੁਨਾਣਾ ਚਾਹਿਆ ਐ ਲਓ ਜੀ ਇਹ ਗੀਤ ਤੁਹਾਡੇ ਨਂਾ ਕਰਦੇ ਆਂ ਅਸੀਂ । ਕਾਲੀਆਂ ਚਲਣਗੀਆਂ ਹਵਂਾਵਾਂ ।</u>

<event desc="music" dur="approximately 10 seconds"></event> 

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>
 
<event desc="announcement clip" dur="approximately 10 seconds"></event>

<u id="37" who="PM001">ਜੀ ਹਾਂ ਜਨਾਬ ਸਾਡੇ ਤੇ ਤੁਹਾਡਾ ਸਾਥ ਪੂਰੇ ਅੱਠ ਵੱਜਣ ਤੱਕ ਦਸ ਵਜੇ ਤੱਕ ਰਹੇਗਾ ਅੱਠ ਵਜੇ ਤਾਂ ਅਸੀਂ <foreign lang="eng">programme</foreign> ਸ਼ੁਰੂ ਕੀਤੈ ਜਨਾਬ ਤੁਹਾਡੇ ਪੈਗਾਮ ਤੁਹਾਡੇ <foreign lang="eng">massage</foreign> ਲੈ ਰਹੇ ਆਂ ਬੜੀ ਖੁਸ਼ੀ ਐ ਕੇ ਤੁਸੀਂ ਸਾਡੇ ਪ੍ਰੋਗ੍ਰਮ ਗੱਡੀ ਦੇ ਵਿਚ ਵੀ ਬੈਠੇ ਆਉਂਦੇ ਜਾਂਦੇ ਕਿਤੇ ਸੁਣ ਰਹੇ ਓ ਬੜਾ ਅੱਛਾ ਲੱਗ ਰਿਹੈ ਬਈ ਤੁਹਾਡਾ ਸਾਥ ਸਾਡੇ ਨਾਲ ਐ ਵਰਨਾ ਅਸੀਂ ਕੱਲੇ ਤੇ ਕੁਝ ਨਹੀਂ ਨਾ ਕਰ ਸਕਦੇ ਕੋਈ ਟਾਹਲੀਆਂ ਚ ਟਾਹਲੀਆਂ ਚਲਾਇਆਂ ਹਵਾ ਬਣ ਕੇ । ਆ ਹਾ ਕਿਆ ਸੁਚ ਐ ਕਿਸੇ ਲਿਖਾਰੀ ਦੀ ਬੰਦੇ ਦਾ ਵਜੁਦ ਕਿਤੇ ਹੋਵੇ ਉਸਦਾ ਮਨ ਉਸਦਾ ਜਹਨ ਕਿਤੇ ਹੋਰ ਹੂੰਦੈ ਇਹਨੂੰ ਕਹਿੰਦੇ ਐ ਪਿਆਰ । ਉਸਦਾ ਧੜ ਜਿਹੜੈ ਧੜ ਜਿਹਨੂੰ ਕਹਿਨੇ ਐਂ ਅਸੀਂ ਜਿਹਨੂੰ ਸ਼ਰੀਰ ਕਹਿੰਦੇ ਐ ਉਹ ਭਾਵੇਂ ਕਿਨੀ ਮਰਜੀ ਭੀੜ ਦੇ ਵਿਚ ਹੋਵੇ ਲੇਕਿ ਉਹਦਾ ਜਹਨ ਤਨਹਾ ਹੋਵੇ ਕਿਓ ਤਨਹਾ ਕਓਂਕਿ ਉਹ ਕਿਸੇ ਦੀ ਯਾਦ ਦੇ ਵਿਚ ਗੁਆਚਿਆ ਹੂੰਦੈ ਕਦੀ ਕਦੀ ਅਸੀ ਭੀੜ ਦੇ ਵਿਚ ਖਲੋ ਕੇ ਵੀ ਕੱਲੇ ਕੱਲੇ ਨਜ਼ਰ ਆਉਣੇ ਐ ਯਾਂ ਫਿਰ ਕੱਲੇ ਖਲੋਤੇ ਹੋਇਏ ਤਾਂ ਐਸ ਤਰਾਂ ਲੱਗਦੈ ਕੇ ਅਸੀਂ, ਖਾਸ ਕਿਸੇ ਵੱਡੇ ਹਜੂਮ ਦੇ ਵਿਚ ਖਲੋਤੇ ਐਂ ਸਾਡੇ ਆਲੇ ਦੁਆਲੇ ਮੱਖੀਆਂ ਵਾਗੂੰ ਲੋਕ ਐ । ਇਹ ਮਨ ਦੀ ਸੋਚ ਐ ਤੇ ਮਨ ਦੀ ਕਹਾਨੀ, ਇਹ ਚੰਦਰਾ ਜਿਹੜਾ ਮਨ ਐ ਨਾ । ਇਹ ਕਹਿੰਦਾ ਕੁਝ ਹੋਰ ਐ, ਕਰਾਉਂਦਾ ਕੁਝ ਹੋਰ ਐ । ਕੀ ਕਰੇ ਬੰਦਾ ਉਡਾਰੀਆਂ ਮਾਰਦੈ ॥ ਬੇਸ਼ਕ ਹੁਣ ਤੇ ਬੜੀ ਤਰੱਕੀ ਕੀਤੀ ਐ <foreign lang="eng">computer </foreign> ਨੇ ਅਸੀਂ ਇਕ ਬਟਨ ਦੱਬਦੇ ਐ ਤੇ ਉਹਦੇ ਚੋਏ ਬਹੁਤ ਸਾਰੀਆਂ <foreign lang="eng">Informations</foreign> ਸਾਨੂੰ ਮਿਲਦੀਐਂ । ਲੇਕਿਨ ਮਨ ਤੋਏ ਵੱਡਾ ਜਿਹੜਾ ਸਾਡਾ ਮਨ ਐ ਏਸ ਤੋਂ ਵੱਡਾ ਕੋਈ <foreign lang="eng">computer</foreign> ਨੀ ਹੈਗਾ । ਵੇਖੋ ਨਾਂ ਪਹਿਲਾਂ <foreign lang="eng">computer </foreign> ਨੂੰ <foreign lang="eng">on</foreign> ਕਰਾਂਗੇ ਬਟਨ ਘੁਮਾਵਾਂਗੇ । ਉਹਨੂੰ ਅਸੀ ਮਤਲਬ <foreign lang="eng">set </foreign> ਕਰਾਂਗੇ ਫੇਰ ਅਸੀਂ ਕਿਸੇ ਮੁਲਕ ਦੇ ਵਿਚ ਜਾਵਾਂਗੇ । ਆਪਣੇ ਮਨ ਨੂੰ ਤੁਸੀਂ ਇਕ ਸੈਕਿੰਡ ਇਕ ਸੈਕਿੰਡ ਤੋ ਵੀ ਪਹਿਲਾਂ ਪਲਕ ਝਪਕਦਿਆਂ ਆਪਣੇ ਪਿੰਡ ਪਹੂੰਚ ਜੋ । ਤੁਸੀਂ ਆਪਣੇ ਪਿੰਡ ਦੇ ਉਹਨਾਂ ਸਾਥੀਆਂ ਕੋਲ ਪਹੂੰਚ ਜੋ, ਜਿਹਨੂੰ ਤੁਸੀਂ ਯਾਦ ਕਰਦੇ ਓ ।ਜਿਹਦੇ ਬਾਰੇ ਤੁਸੀਂ ਸੋਚਦੇ ਓ ਏਨਾ ਟਾਇਮ ਨੀ ਲੱਗਦਾ । ਏਹਨੂੰ ਕਹਿੰਦੇ ਨੇ, <vocal who="PM001" desc="laugh"></vocal> ਮਨ ਦਾ ਕੀ ਏ ਏਹਨੂੰ ਚਿਰ ਨੀ ਲੱਗਦਾ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event> 

<u id="38" who="PM001">ਇਹ ਆਵਾਜ਼ ਸੀ ਸੁਰਿੰਦਰ ਜੀ ਮਕਸੂਦ ਪੁਰੀ ਦੀ ਔਰ ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਏ ਸੁਣਿਆ ਉਹਨਾਂ ਦੀ ਆਵਾਜ਼ ਦੇ ਵਿਚ । ਪੱਚੀ ਮਿਨਟ ਰਹਿ ਗੇ ਨੇ ਸਾਡੇ ਤੇ ਤੁਹਾਡੇ ਦਰਮਿਆਨ ਅੱਗਲਾ ਗੀਤ ਅਸੀਂ ਤੁਹਾਨੂੰ ਸੁਨਾਉਨੇ ਐ, ਦੋਗੀਤ ਅਸੀ ਤੁਹਾਨੂੰ ਸੁਨਾਉਣੇ ਐ ਜਗਜੀਤ ਸਿੰਘ ਅਤੇ ਚਿਤ੍ਰਾ ਸਿੰਘ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਸਾਡਾ ਨੰਬਰ ਐ ੦੮ ੪੫੯ ੪੪੦ ੪੪੫, ਵੇਸੇ ਸਾਡਾ ਨੰਬਰ ਜਿਹੜਾ ਹੈਗਾ ਤੁਹਾਡੇ ਦਿਮਾਗ ਤੇ, ਜ਼ਹਨ ਤੇ ਛਾਇਆ ਹੋਇਆ ਐ, ਲੇਕਿਨ ਫੇਰ ਵੀ ਅਸੀ ਦੱਸਨਾ ਆਪਣਾ ਫਰਜ਼ ਸਮਝਦੇ ਐ ਹੈਨਾ ਤੁਹਾਡਾ ਪਿਆਰ । ਗੁੱਸਾ ਤੇ ਨੀ ਕੀਤਾ ਨਾਂ ਤੁਸੀਂ <omit extent="1 word" cause="sound quality"></omit> ਬਹੁਤ ਹੀ ਪਿਆਰੀਆਂ ਬੋਲੀਆਂ ਨੇ ਤੁਸੀਂ ਕਿਤੇ ਵੀ ਐਸ ਵਕਤ ਸਾਡਾ <foreign lang="eng">programme</foreign> ਸੁਣ ਰਹੇ ਓ, ਤੁਹਾਨੂੰ ਜੀ ਆਇਆਂ ਕਹਿਨੇ ਆਂ ਅਸੀਂ ਜੀ ਆਇਆਂ ਕਹਿਨੇ ਆਂ ਖੁਸ਼ਾਮਦੀਦ ਕਰਦੇ ਆਂ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="39" who="PM001">ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਤੁਸੀਂ ਸੁਣਿਆ । ਇਹ ਗੀਤ ਸੀਗਾ ਜਗਜੀਤ ਸਿੰਘ ਚਿਤ੍ਰਾ ਸਿੰਘ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਉਮੀਦ ਐ ਕੇ ਤੁਹਾਨੂੰ ਪਸੰਦ ਆਇਆ ਹੋਣੈ । ਔਰ ਜਨਾਬ <omit extent="1 word" cause="sound quality"></omit> ਤੋ ਸਾਡੇ ਕੋਲ ਕੁਝ ਪੈਗਾਮ ਨੇ ਵੀਰ ਸਿੰਘ ਜੀ ਸਾਨੂੰ ਫੋਨ ਕਰ ਰਹੇ ਸੀ ਉਹਨਾਂ ਨੇ ਕਿਹੈ ਕੇ <foreign lang="eng">programme</foreign> ਨੂੰ ਅਸੀਂ <foreign lang="eng">enjoy</foreign> ਕਰ ਰਹੇ ਐ ਕੁਝ ਗੀਤਾਂ ਦੀ ਫਰਮਾਇਸ਼ ਉਹਨਾਂ ਨੇ ਕੀਤੀ ਐ ਅਸੀਂ ਜਰੂਰ ਲੱਭਾਂਗੇ ਬਹਰਹਾਲ ਆਹ ਗੀਤ ਅਸੀਂ ਜਿਹੜਾ ਹੁਣੇ ਸੁਨਾਇਆ ਐ ਉਹਨਾਂ ਨੂੰ ਬਹੁਤ ਪਸੰਦ ਆਇਆ । ਇਕ ਗੀਤ ਦੀ ਤੁਸੀਂ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਐ ਮਹੰਮਦ ਸਬੀਰ ਅਤੇ ਰਣਜੀਤ ਕੌਰ ਦੀ ਆਵਾਜ਼ ਵਿਚ ।ਮਣਕੇ ਉਹਨਾਂ ਨੇ ਗਾਇਅ ਸੀ ਮਨਕੀ ਖੁਹ ਦੇ ਵਿਚੋਏ ਭਰਦੀ ਸੀ ਜਦੋ ਪਾਣੀ ਕੀ ਮੈ ਕੋਲ ਜਾ ਕੇ ਬੇਨਤੀ ਗੁਜਾਰੀ ਮਤਲਬ ਬੜੀ ਹੀ ਪਿਆਰੀ ਜਿਹੀ ਕਹਾਣੀ ਆਲਾ ਇਹ ਗੀਤ ਐ ਅਸੀਂ ਜਰੁਰ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਸਾਨੂੰ ਲੱਭ ਗਿਆ ਤਾਂ ਜਰੂਰ ਸੁਨਾਵਾਂਗੇ ਚਲਦੇ ਐਂ ਸ਼ੌਕਤ ਅਲੀ ਸਾਹਿਬ ਜੀ ਦੇ ਵੱਲ ।</u>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<u id="40" who="PM001">ਸ਼ੌਕਤ ਅਲੀ ਸਾਹਿਬ ਜੀ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਤੁਸੀਏ ਬਹੁਤ ਹੀ ਪਿਆਰਾ ਗੀਤ ਸੁਣਿਆ । ਸਾਡਾ ਪਿੰਡ ਤੇਨੂੰ ਪਿਆਰਾ ਨੀ ਕਦੀ ਯਾਦ ਕਰਦੀ ਹੀ ਰਹਿ ਜਾਏਂਗੀ ਉਮੀਦ ਐ ਕੇ ਤੁਹਾਨੂੰ ਪਸੰਦ ਆਇਆ ਹੋਨੈ ।ਸ਼ਾਹ ਜੀ ਤੁਸੀਂ ਬਰਘਿੰਗਮ ਤੋਏ ਫੋਿਨ ਕੀਤੈ ਤੁਸੀਂ ਅਬਰਾਰ ਉਲ ਹੱਕ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਗੀਤ ਸੁਨਨਾ ਚਾਹਿਆ ਐ । ਇਕ ਗੀਤ ਅਸੀਂ ਹੁਣੇ ਸੁਨਾਇਆ ਐ ਅਸੀਏ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਕੋਈ ਹੋਰ ਗੀਤ ਲੱਭੀਏ ਜੇ ਸਾਨੂੰ ਲੱਭਿਆ । ਔਹ ਜੀ ਇਕ ਸੀ ਡੀ ਸੀਗੀ ਸਾਨੂੰ ਲੱਭ ਨੀ ਰਹੀ ਉਹਦੇ ਵਿਚ ਬਹੁਤ ਪਿਆਰੇ ਪਿਆਰੇ ਗੀਤ ਐ ਅਕਸਰ ਅਸੀਂ ਸੁਨਾਇਆ ਕਰਦੇ ਐ ਬਹਰਹਾਲ ਉਹਨਾਂ ਸਾਰੇ ਈ ਗੀਤ ਪਿਆਰੇ ਗਾਏ ਨੇ ਅਸੀਂ ਕੋਸ਼ਿਸ਼ ਕਰਾਂਗੇ ਹੋਰ ਗੀਤ ਤੁਹਾਡੇ ਨਾਂ ਕਰੀਏ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ । ਮਹਿੰਦਰ ਜੀ ਤੁਸੀ ਵੀ ਫੋਨ ਕੀਤੈ ਔਰ ਮੁਕੇਸ਼ ਜੀ ਤੁਸੀਂ ਬਰਘਿੰਗਮ ਤੋਏ ਫੋਨ ਕੀਤੈ ਤੁਸੀ ਕਿਹੈ ਕੇ ਤੁਹਾਡੇ ਨਾਂ ਕਰ ਦਿਏ ਅਸੀਂ । ਔਰ ਪੰਜਾਬ ਕੌਰ ਜੀ ਹਰਿਆਨਾ ਤਪੋ ਫੋਨ ਕਰ ਰਹੇ ਸੀ ਨਉਹਨਾਂ ਨੇ ਕਿਹੈ ਸੱਤ ਸ੍ਰੀ ਅਕਾਲ, <foreign lang="eng">programme</foreign> <foreign lang="eng">enjoy </foreign> ਕਰ ਰਹੇ ਆਂ ਅਸੀਂ ਔਰ ਤੁਸੀਂ ਸੁਖਵਿੰਦਰ ਸਿ-ਘ ਦੀ ਆਵਾਜ਼ ਵਿਚ ਗੀਤ ਸੁਣਨਾ ਪਸੰਦ ਕਰਦੇ ਓ । ਬਿਲੂ ਚੋਹਾਨ ਲੈਸਤਰ ਤੋਂ ਫੋਨ ਕਰਦੇ ਐ ਉਹਨਾ ਨੇ ਰਾਣੀ ਰਣਜੀਤ ਦੀ ਅਵਾਜ਼ ਵਿਚ, ਇਸ਼ਕ ਬਿਮਾਰ ਸੁਨਨਾ ਚਾਹਿਆ ਜਰੂਰ ਕੋਸ਼ਿਸ਼ ਕਰਦੇ ਐ ਜੀ ਅੱਜ ਤੁਹਾਨੂੰ ਇਹ ਜਰੀਰ ਸੁਨਾਉਣੈ ।ਫਿਲਹਾਲ ਅਸੀਂ ਚਲਦੇ ਐਂ ਏਸ ਗੀਤ ਵੱਲ ਔਰ ਇਹ ਗੀਤ ਐ ਹਰਭਜਨ ਤਲਵਾਰ ਦਿ ਆਵਾਜ਼ ਵਿਚ ।</u>

<event desc="music" dur="approximately 10 seconds"></event> 

<event desc="song" dur="approximately 4 minutes 30 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<event desc="announcement clip" dur="approximately 10 seconds"></event>

<event desc="music" dur="approximately 10 seconds"></event>

<u id="41" who="PM001">ਲਓ ਜੀ ਇਸ਼ਕ ਦੀ ਮਾਰ ਦੀ ਚੱਲ ਰਈ ਏ ਔਰ ਬਿਲੂ ਜੀ ਤੁਸੀਂ ਏਸ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਸੀ ਐਸੀ ਪਈ ਇਸ਼ਕ ਦੀ ਮਾਰ, ਇਸ਼ਕ ਦੀ ਮਾਰ ਵੈਸੈ ਈ ਬੜੀ ਭੈੜੀ ਐ, ਬੜੀ ਡਾਢੀ ਐ ਔਰ ਜਿਹੜਾ ਬੰਦਾ ਸਹਿ ਲੈਂਦੇ ਉਹੀ ਪਾਸ ਹੂੰਦੈ ਇਸ਼ਕ ਦੇ ਇਮਤਿਹਾਨ ਦੇ ਵਿਚ ਬਹੁਰਹਾਲ ਕਿਓਂ ਬਿਲੂ ਭਾ ਜੀ ਤੁਹਾਡਾ ਕੀ ਖਿਆਲ ਐ ਤੁਸੀਂ ਏਸ ਗੀਤ ਦੀ ਫਰਮਾਇਸ਼ ਕੀਤੀ ਸੀ ਉਮੀਦ ਐ ਕੇ ਤੁਹਾਨੂੰ ਇਹ ਗੀਤ ਪਸਔਦ ਆਇਆ ਹੋਣੈ । ਸੱਤ ਮਿੰਟ ਬਾਕੀ ਨੇ ਮੈਂ ਸ਼ੁਕਰੀਆ ਕਰਾਂਗਾ ਤਮਾਮ ਸਰੋਤਿਆਂ ਦਾ ਜਿਹਨਾਂ ਨੇ ਮੇਰੇ ਇਸ ਸਾਡੇ ਅੱਜ ਦੇ <foreign lang="eng">programme</foreign> ਦੇ ਵਿਚ ਸਾਥ ਦਿੱਤਾ ਮੀਨੂ ਜੀ ਤੁਹਾਡਾ ਵੀ ਫੋਨ ਆਇਆ ਤੁਸੀ ਵੀ ਕਿ ਹੈ ਕੇ <foreign lang="eng">programme</foreign> ਬਹੁਤ ਅੱਛੈ । ਤੁਸੀ <foreign lang="eng">programme enjoy</foreign> ਕੀਤਾ ਬਹੁਤ ਬਹੁਤ ਸ਼ੁਕਰੀਆ ਤਮਾਮ ਸਰੋਤਿਆਂ ਦਾ ਸ਼ੁਕਰੀਆ ਅਦਾ ਕਰਂਗੇ ਜਿਨਾਂ ਨੇ ਇਸ ਪ੍ਰੋਗ੍ਰਮ ਦਾ ਆਨੰਦ ਮਾਨਿਆ । ਆਉਣ ਵਾਲੇ <foreign lang="eng">programme</foreign> ਦੇ ਵਿਚ ਵੀ ਹਾਜਿਰ ਹੋਵਾਂਗੇ ਤੁਹਾਡੇ ਵਾਸਤੇ ਪਿਆਰੇ ਪਿਆਰੇ ਗੀਤ ਤੁਹਾਡੇ <foreign lang="eng">Massage</foreign> ਲੈ ਕੇ । ਗੁਰਤੇਜ ਜੀ ਅਸੀਂ ਫਿਰ ਤੋਂ ਤੁਹਾਡਾ ਧੰਨਵਾਦ ਕਰਦੇ ਐ ਤੁਸੀਂ ਏਨੀ ਦੂਰੋ ਸਾਨੂੰ ਤਾਦ ਕਰਦੇ ਓ । ਆਸਿਫ ਜੀ ਤੁਹਾਨੂੰ ਤੁਹਾਡੇ ਪਰਿਵਾਰ ਨੂੰ ਸਾਡੇ ਵਲੋ ਬਹੁਤ ਬਹੁਤ ਪਿਆਰ ਭਰੀ ਸੱਤ ਸ੍ਰੀ ਅਕਾਲ । ਸ਼ੈਫਾਲੀ ਦਾ ਮੈਂ ਸ਼ੁਕਰਿਆ ਅਦਾ ਕਰਾਂਗਾ ਔਰ ਸਰਿਤਾ ਜੀ ਦਾ ਜਿਹਨਾਂ ਨੇ ਇਸ <foreign lang="eng">programme </foreign> ਨੂੰ <foreign lang="eng"> produce</foreign> ਕੀਤਾ ਤਮਾਮ ਤੁਹਾਡੇ ਉਮਰ ਦਾ ਵੀ ਤੁਹਾਡਾ ਸਦਕਾ ਤੁਸੀਂ ਏਸ <foreign lang="eng">programme</foreign> ਨੂੰ ਕਾਮਯਾਬ ਕੀਤਾ ॥ ਮੇਰੀ ਅਤੇ ਤੁਹਾਡੀ ਅਗਲੀ ਮੁਲਾਕਾਤ ਹੋਏਗੀ ਅਗਲੇ ਹਫਤੇ ਸ਼ੁਕਰਵਾਰ ਨੂੰ । ਤੁਸੀਂ ਆਪਣਾ ਖਿਆਲ ਰੱਖਿਓ ਅਤੇ ਆਪਣੇ ਨਾਲ ਨਾਲ ਦੁਜਿਆਂ ਦਾ ਵੀ ਖਿਆਲ ਰੱਖਿਓ ਅਸੀਂ ਚਲਾਂਗੇ ਇਕ ਗੀਤ ਵੱਲ ਲੇਕਿਨ ਉਸ ਤੋਂ ਬਾਅਦ ।</u>

<event desc="english News" dur="approximately 10 seconds"></event>

<event desc="Announcement clip" dur="approximately 10 seconds"></event>

<u id="42" who="PM001">ਜੀ ਹਾਂ ਜਨਾਬ ਹੁਣ ਅਸੀਂ ਚਲਦੇ ਐ, ਏਸ ਆਖਿਰੀ ਗੀਤ ਵੱਲ ਦਸ ਵਜੇ ਖਬਰਾਂ ਹੋਣਗੀਆਂ ਗੁਜਰਾਤੀ ਵਿਚ ਗੁਜਰਾਤੀ ਵਿਚ ਖਬਰਾਂ ਤੋਂ ਬਾਅਦ ਫੇਰ ਆਉਣਗੇ ਮਹੇਸ਼ ਭਾਈ ਲਖਾਨੀ ਜੋ ਕੇ ਬਹੁਤ ਹੀ ਪਿਆਰਾ <foreign lang="eng">programme</foreign> ਲੈ ਕੇ ਗੁਜਰਾਤੀ ਵਿਚ ਹਾਜਿਰ ਹੋਣਗੇ ਪੂਰੇ ਬਾਰਾਂ ਵਜੇ ਤੱਕ ਉਮੀਦ ਐ ਕੇ ਤੁਸੀਂ ਉਹਨਾਂ ਦਾ ਸਾਥ ਦਿਓਗੇ ਪੂਰੇ ਬਾਰਾਂ ਵਜੇ ਤੱਕ ਔਰ ਹੁਣ ਮੈਏ ਚਲਦੈ ਏਸ ਆਖਰੀ ਗੀਤ ਵੱਲ <vocal who="PM001" desc="laugh"></vocal> । ਮੈਂਚ ਤੁਹਾਡੇ ਕੋਲੋਂ ਸਭ ਕੋਲੋ ਇਜਾਜਤ ਚਾਹਾਂਗਾ ਉਮੀਦ ਐ ਫੇਰ ਮੁਲਾਕਾਤ ਹੋਏਗੀ ਤੁਸੀਂ ਆਪਣਾ ਖਿਆਲ ਰੱਖਿਓ <foreign lang="eng">good bye and god bless you all</foreign></u>

</body>
</text>
</cesDoc>