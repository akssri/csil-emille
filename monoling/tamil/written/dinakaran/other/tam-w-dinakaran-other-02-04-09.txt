<cesDoc id="tam-w-dinakaran-other-02-04-09" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-02-04-09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-04-09</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-04-09</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>''சித்ரபானு'' தரும்
தனிச்சிறப்புகள்</p>

<p>நமது முன்னோர்கள் தமிழ் ஆண்டுகள் அறுபது என்று கணக்கிட்டுள்ளனர். அவை பிரபவ
முதல் அட்சய வரையில் ஆகும். இந்த ஆண்டு எண் வரிசையில் பதினாறாவது ஆண்டான சித்ரபானு
தமிழ்ப் புத்தாண்டாக மலர்கிறது. இதுவே தமிழர் கொண்டாடும் பண்டிகைகளுள் தலைசிறந்ததாகும்.
</p>

<p>இந்த ஆண்டு முன்பாகவே சனிக்கிழமை வளர்பிறையில் துவிதையை, அசுவனி நட்சத்திரம்
சித்தயோகம் கூடிய சுபநன்னாளில் விடியற்காலை 3.35 மணிக்கே கேது திசை- புதன் புத்தியில்
பிறக்கிறது. </p>

<p>சித்ரபானு ஆண்டிற்கு சனி பகவான் அரசனாக, சேனாதிபதியாக, அர்க்காதிபதி யாக
மேகாதிபதியாக விளங்கப்போவது குறிப்பிடத்தக்கது. புத்தாண்டில் முதலில் தொடங்கும் மாதம்
சித்திரை ஆகும். சித்திரை வருடப்பிறப்பு என்பர். சித்திரை முதல் நாளில் மேஷ ராசியில்
சூரியன் தோன்றுகிறார். </p>

<p>இந்த வருடம் பிறப்பின் போது மக்கள் விடியற் காலையில் நீராடி புதிய ஆடை
உடுத்துவர். ஆலயம் சென்று வழிபடுவர், குடும்பத்தில் மூத்த பெரியவர்களிடம் நல்லாசி
பெற்றுக்கொள்வர். </p>

<p>பஞ்சாங்க படனம் </p>

<p>பொது இடங்களிலும் கோவில்களிலும் ''பஞ்சாங்கம் படிக்கும் வழக்கம் இன்றும்
காணப்படுகிறது. இதனை ''பஞ்சாங்க படனம்'' என்பர். படனம் என்றால் படித்தல் என்பது
பொருள். எனவே முழுமையாக பஞ்சாங்கம் படித்தல் என்பது பொருள். பஞ்சாங்கத்தில்
ஐந்து அங்கம் உண்டு. அவை திதி, வாரம், நட்சத்திரம், யோகம், காணம் ஆகும்.
இவற்றைக்கொண்டு நல்ல நாளைக்குறிப்பிடுவர். புத்தாண்டு தினத்தில் ஒருநல்ல செயல்
தொடங்கினால் ஆண்டு முழுவதும் நல்லதாக தொடரும் என்பது தமிழர் நம்பிக்கை. இந்நன்னாளில்
அறுசுவை உணவு சமைப்பர். கசப்பான வேப்பம் பூப்பச்சடி, புளிப்பும் இனிப்பும் கலந்த
மாங்காய் பச்சடி முக்கியமாக இடம் பெறும். நம் வாழ்க்கையில் கசப்பானவற்றையும் ஏற்று
எதிர் நீச்சல் போட்டு முன்னேற வேண்டும் என்பது கருத்து. </p>

<p>புதுவருடம் பஞ்சாங்கத்தை இறைவழிபாட்டின் போது வைத்து பின்னர் எடுத்து
படிக்கும் மரபும் உண்டு. </p>

<p>ஆண்டு பலன்கள் </p>

<p>பல்லாண்டுகளுக்கு முன்பு கர்க்க முனிவர் எழுதிய ஜோதிட நூலில் ஆண்டு பலனை
ஆராய்ந்து வகுத்துள்ளார். அதை மூலமாக வைத்து, தௌிவான பாடல்களாக, இடைக்காடர் தமிழில்
வெண்பாக்களாக இயற்றியுள்ளார். அவர் சித்ரபானு ஆண்டிற்கு பலன்களாக கூறியவற்றை
நோக்குவோம். </p>

<p>இந்த ஆண்டில் அதிக மழை பெய்யும், நீர்நிலைகள் நிரம்பும், விதைப்பயிர்கள்
சிறப்பாக எங்கும் விளையும். அரசு நலந்தருவதாக அமையும். பூகம்பம், எரி மலை,
வௌ்ளப்பெருக்கு முதலான தீங்குகள் சிற்சில பகுதிகளில் ஏற்பட வாய்ப்புண்டு. பலவகையில்
நாட்டுக்கு அச்சுறுத்தல்கள் வந்தாலும், எதிர்த்து நலமாக வெற்றியாக, வளமாக வாழ வழி உண்டு.
இந்த கருத்துக்களை வழங்கும் ''சித்ரபானில் சிறக்க மழை மிகுந்து'' என வரும் வெண்பா பாடல்
தௌிவாக கூறுகிறது. வழக்கமாக வரும் சூரிய சந்திர கிரகணங்கள் இவ்வாண்டு இந்தியாவில்
தெரியாது. சித்ரபானு ஆண்டில் தான் யாதிபதி சந்திரனால் அதிக மழையும், சுபிட்சமும்
இரசாதிபதி சுக்கிரனால் நல்ல மழையும் வளமும் பெருக வாய்ப்பு உண்டு. </p>

<p>பொதுவாக பெண்களுக்கு நல்ல முன்னேற்றமும், சமுதாய மதிப்பும் மேலோங்கும்.
சித்ரபானு ஆண்டின் மகர சங்கராந்தி பலனும் தௌிவாக சொல்லப்பட்டுள்ளது. இந்த தேவதை
மகோதரி ஆவாள். சிங்கவாகனம் ஏறிக்காட்சி தருபவள். இவள் மலர்ச்சியும் மகிழ்ச்சியுமான
முகத்துடன் தென்்திசை நோக்கி கிழக்கு முகமாக செல்வதால் பல்வேறு துறையில் பலன்கள்
கூறப்பட்டுள்ளன. </p>

<p>இன்றைய நாட்டின் பரபரப்பான பயங்கரமான சூழ்நிலையிலிருந்து விலக்கி முற்றிலும்
அமைதியும் மகிழ்ச்சியும் அளிப்பாய் சித்ரபானுவே என்று வேண்டு வோம். இன்று நிலவும்
இனக்கலவரம், சமயப்பூசல், கொலை, கொள்ளை முதலான தீயசக்திகளை வேரோடு களைவாய்
சித்ரபானுவே. </p>

<p>AC / DC என்பது எதை
குறிக்கிறது?</p>

<p>HEP முறையில் நீரை பீறிட்டு வரும்படியாக
வௌியிட்டு ஜெனரேட்டரிலுள்ள டர்பனை சுழற்றவைத்து அதன் மூலம் காந்தத்தை சுழற்ற மின்புலம்
உண்டாக்கப் படுகிறது. இது ''டிரான்ஸ் பர்கள்'' மூலம் எல்லா இடங்களுக்கும் அனுப்பப்படு கிறது.
இதுவே AC் எனப்படுகிற Alternating Current ஆகும். பாட்டரி செல் போன்ற ஒருவழி
பாய்ச்சல் கொண்ட மின்சக்தி கொண்டவை DC் எனப்படுகிற Direct Current ஆகும். மின் உற்பத்தி நிலையத்திலிருந்து மின்சக்தியை
கோபுரங்கள் மூலம் வோல்டேஜ் அதிகப்படுத்தி அதேசமயம் மின்சக்தி இதன்மூலம் அனுப்பப்
படுகிறது. மின் உற்பத்தி நிலையம் அதிகபட்சமாக 2000 மெகா வாட்ஸ் உற்பத்தி செய்யும்.
</p>

<p>துணை மின் அஞ்சல் நிலையங்களிலிருந்து வோல்டேஜ் குறைத்து கம்பங்களிலோ
நிலத்தடியிலோ ஒயர்கள் மூலம் நம் வீடு போன்றவைகளின் பயன்பாட்டிற்கு வருகிறது.</p>

<p>அன்பின் மேன்மை</p>

<p>''நீங்கள் ஏக சிந்தையும், ஏக அன்புமுள்ளவர்களாக இருந்து, இணைந்து ஆத்துமாக்களாய்
ஒன்றையே சிந்தித்து, என் சந்தோஷத்தை நிறைவாக்குங்கள்'' </p>

<p>-(பிலி 2 : 2) </p>

<p>மேற்கண்ட வேதவசனம் நமக்கு உணர்த்துவது என்ன? இறைவனைக் குறித்த
ஏகசிந்தனையும், பிறர்போல் நாம் கொள்ளும் அன்புமே கடவுளை மகிழ்ச்சிப் படுத்துகிறது.
</p>

<p>மனித நேயம் வளர அன்பே அடிப்படையாகும். அன்பு பெருகும் இடத்தில் பகை, பிணக்கு,
போட்டி, பொறாமை, கோபம் எதற்கும் இடமில்லை. அன்பு- கருணையின் ஊற்று; பொறுமையின்
முன்னோடி; நீதியின் நிலைக்களம் சமாதானத்தின் உறை விடம். ஆகவே தான் அன்பைப்
பிரதானமாக வலிபுறுத்தினார், இயேசு. உன்னி டத்தில் நீ அன்பு கூர்்வது போல பிறனிடத்திலும்
அன்பு கூர்வாயாக என அவர் கூறியது ஆழமாகச் சிந்திக்கத்தக்கது. நீங்கள் ஒருவரில் ஒருவர்
அன்பு கூறுகள் என்றே தம் சீடர்களுக்கும் கூறுங்கள் என்றே தம் சீடர்களுக்கும் அவர் போதித்
தார். உங்்கள் சததுருக்களைச் சிநேகியுங்கள்; உங்களை நிந்திக்கிறவர்களுக்காகவும்,
உங்களைத் துன்பப்படுத்துகிறவர்களுக்காகவும் ஜெபம் பண்ணுங்கள் என இயேசு கூறுவதில் அன்பின்
மேன்மை வௌிப்படுகிறது. ஒருவன் உன்னை வலது கன்ன த்்தில் அறைந்தால் அவனுக்கு மறு
கன்னத்தையும் திருப்பிக்காட்டு என்று சொல்லும்போது அன்பின் பரிபக்குவ நிலையைக்
காண்கிறோம்.அன்பென்னும் நதி நம் உள்ளத்தில் பிறப்பெடுத்தால் எவ்வித வேறுபாட்டுக்கும்
இடமிராது. நம்முடைய அன்பு தழைக்கும் போது, உலகை அச்சுறுத்தும் பயங்கரவாதமும், தீவிரவாதமும்
ஒழிந்து போகும். தீமைகளும், துயரங்களும் ஓடிப்போகும். அன்பென்னும் நதியினில் குளிப்போம்;
அவனியை சமாதான பனியில் நனைப்போம்.</p>

<p> </p>






</body></text></cesDoc>