<cesDoc id="tam-w-dinakaran-other-01-07-03" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-01-07-03.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-07-03</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-07-03</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>மக்கள் பெருக்கம் பாதியாக
குறைப்பது எப்படி?</p>

<p>ஏழெட்டுப் பிள்ளைகளுடன்  பேருந்து ஏறியவரை </p>

<p>நடத்துனர் கேட்டார் எரிச்சலுடன் </p>

<p>''பாதி பிள்ளைகளை வீட்டில் விட்டு  மீது பிள்ளைகளை அழைத்து
வரக்கூடாதா?'' </p>

<p>அவர் சொன்னார் அமைதியாக </p>

<p>''ஆமாம் இவர்கள் </p>

<p>பாதி பேர்களே </p>

<p>மீதி வீட்டில்'' இருபதாம் நூற்றாண்டின் இறுதிக் காலத்துத்துணுக்கு இது. ''அரிது,
அரிது மானிடராய் பிறத்தலரிது'' எனப்படும் மானுடப் பெருக்கத்தை நினைத்து பார்த்து நிம்மதி
இழக்கும் தருணம் இது. உலகத்தின் மக்கள் தொகை 600 கோடியைத் தாண்டிய அபார சாதனையை
ஆய்ந்து பார்க்கும் நேரமிது. இதில் பாரதத்தின் பங்களிப்பாக நூறு கோடியைத் தாண்டிய
வேகத்தைப்பற்றி பரிசீலனை செய்யும் சமயமிது. இந்த வேகத்தைக் கட்டுப்படுத்த ஆழ்ந்து
சிந்தித்து தொலைநோக்குத் திட்டங்கள் தீட்ட வேண்டிய வேளையிது. உலக நிலப்பரப்பில் 4
விழுக்காடு மட்டுமே உள்ள நம்நாடு உலக மக்கள் தொகையில் 17 விழுக்காட்டை சுமந்து
கொண்டிருக்கிறது. </p>

<p>இறப்பு தொகை குறைப்பு </p>

<p>சென்ற தலைமுறையில் ''முப்பது கோடி முகமுடையாள்'' என்று பாடிய பாரதியைக் கூட
ஏமாற்றி விட்டு இன்று கோடிகளில் சதமடித்து விட்டது. அதுவும் பாரதி காலத்து அகண்ட
பாரதத்தில் அல்ல. எஞ்சியுள்ள நிலப்பரப்பிலேயே இந்த அளவிற்கு மக்கள் தொகை
பெருகிவிட்டதென்றல், இலங்கை, பாகிஸ்தான், வங்கம் தேசம் இவற்றையும் உள்ளடக்கிய நாடாக
இருப்பின் சொல்லத்தேவையே இல்லை. மக்களை கூட்டம் கூட்டமாக அழித்த பிளேக், காலரா,
பெரியம்மை போன்ற கொள்ளை நோய்களைக் கட்டுப்படுத்திய மருத்துவத்தாலும், கோடிக்கணக்கான
மனிதர்களை பலியிட்ட போர்களைத் தடுத்துவிட்ட போரிலாச் சிந்தனை யாலும் இறப்புத்தொகை
பெருமளவு குறைக்கப்பட்டது. இவற்றால் தப்பிப் பிழைத்தவர்கள் செய்த இனப் பெருக்கமும்
சேர்ந்து இருபதாம் நூற்றாண்டின் புவியை அடர்த்தியாக்கி விட்டது. அடிப்படைத் தேவையான
உணவு, உடை, உறையுள் கூட இன்னும் நிறைவு செய்யாமல், வறுமைக்கோட்டைத் தாண்டாமல்
இருக்கும் இந்நாட்டுக்கு இந்த அடர்த்தி அதிர்ச்சியைத் தந்தது. இருப்பினும் இத்தகைய வேகமான
வளர்ச்சியைக் கட்டுப்படுத்த வேண்டும் என்று தொலைநோக்கோடு சிந்தித்தவர்கள் ஒரு சிலர்
மட்டுமே. எடுத்துக் காட்டாக 19-02-1938-ம் நாள் நடந்த ஹரிபுரா காங்கிரஸ் மாநாட்டின் தலைமை
உரையில் நேதாஜி சுபாஷ் சந்திர போஸ் அவர்கள் ''வறுமையும், பிணியும் அறி யாமையும்,
வாட்டி எடுக்கின்ற நம் நாட்டில் மக்கள்தொகைப் பெருக்கம் என்பது விரும்பத் தக்கதல்ல,
கட்டுப்படுத்தாமல் இப்படியே விட்டால் போடுகிற திட்டங்களின் பலன் நிச்சயமாகக்
கிடைக்காது'' என்று குறிப்பிட்டார். சில பேராயக் கட்சியினர் இவருடைய இக்கருத்துக்கு'' இது
கடவுளை நிந்திக்கும் செயல்'' என்று கூறி கண்டனம் தெரிவித்தனர். பிறப்பு விகிதம்
குறைப்பதைப் பற்றி சிந்திக்கவே அஞ்சினர். அப்படிப்பட்ட அந்தக் காலத்தில்
அதற்கும் பத்து ஆண்டுகளுக்கு முன்பாகவே நம் தமிழ் மண்ணில் புரட்சிக் கவிஞர்
பாரதிதாசன் அவர்கள் </p>

<p>''தோன்றியுள்ள மக்கள் நலம் யாவும்- இங்கு </p><p>
தோன்றாத மக்கள்
தந்ததாகும்'' </p>

<p>என்று அளவான மக்கள் தொகையின் மேன்மைய, வளத்தை நுட்பமாக எடுத்துரைத் தார்.
மக்கள் தொகைக் கட்டுப்படுத்தலால் கிடைக்கும் வசதி வாய்ப்புகளை மட்டும் கூறுவ தோடு
நில்லாமல் எவ்வாறு கட்டுப்படுத்தலாம் என்பதை இந்தியாவிலேயே முதன் முதலாக </p>

<p>''காதலுக்கு வழிவைத்துக் கருப்பாதை சாத்தக் கதவொன்று கண்டறிவோம் இதிலென்ன
குற்றம்'' என்று குடும்பநல அறுவை சிகிச்சையை அன்றைக்கே அறிமுகப்படுத்துகிறார். </p>

<p>பாதியாக குறையும் </p>

<p>இக்கருத்தில் மேற்கண்ட மானுடப் பற்றாளர்களுக்கு முன்னோடியாக இருந்து, புரட்சிக்
கவிஞரின் இக்கவிதைகளுக்கு மூலக்கரு தந்த தந்தை பெரியார் ''மகப்பேறு கூடுதலாக இருந்து
விடுமானால் அது ஒன்றே மக்களிடத்தில் உள்ள அன்பு நாணயம் போன்ற எல்லாவற்றையும்
கெடுத்துவிடும். பிள்ளைகளின் நலத்திற் கென்றும், அவர்களைக் காப்பாற்ற வேண்டியும் தவநான
வழிகளிலும் பணம் சமபாதிக்க வேண்டிய கட்டாயம் ஏற்பட்டு விடுகின்றது'' என்று பெரிய
குடும்பத்தின் அவலத்தைக் கோடிட்டுக் காட்டுகிறார். அனைத்து வழிகளிலும் இம்மானுடம் மேம்பட
வேண்டும் என்று நினைத்த பெரியார் ''கருத்தரித்தல் குறித்தான அறிவு அதை தவிர்பதற்கான
வழி வகைகள் யாவற்றையும் பெண்கள் அறிந்திருக்க வேண்டும்; மகப்பேற்றை விரும்புவதோ,
விரும்பாததோ அது மகளிரின் உரிமையின் பாற்பட்டதாக இருக்க வேண்டும். ஆண் குழந்தையையும்,
பெண் குழந்தையையும் சமமாகப் பார்க்கும் நிலை வந்துவிட்டால் மக்கள் பெருக்க விகிதம்
பாதி குறைந்து விடும்'' என்று நுட்பமான செய்தியை முதன்மையாக சுட்டிக் காட்டுகிறார். இத்தகைய
அறிவார்ந்த பெண்ணுரிமை சார்ந்த செயல்பாடுகளால்தான் குடும்ப நலத்திட்டம் குறிக்கோளை
நிறைவு செய்ய முடியும். </p>

<p>www.top100lyrics.com/</p>

<p>உலகின் முதல் தரமான பாடல்கள் பற்றிய 100 வெப்சைட்டுகள்
பட்டியலிடும் வெப் தளம் இது. இந்த 100 தளங்களில் ஒவ்வொன்று பற்றியும் விளக்கமான
புள்ளிவிவரங்கள் தரப்படுகின்றன. உதாரணமாக லிரிக்ஸ் பிளேஸ் என்ற தளம். இதில் மிகப்
புதிய, இனிமையான 20 ஆயிரம் பாடல்கள் நமக்கு விருந்து படைக்க காத்திருப்பதாக
தெரிவிக்கப்படுகிறது. ஜோஸ் லிரிக்ஸ் என்றொரு தளம். இதில் ஆயிரக்கணக்கான பாடல்கள்
அவ்வப்போது இணைக்கப்படுவதாக கூறப்பட்டுள்ளது. எல்லா வகையான இசை சம்பந்தப்பட்ட
பாடல்களும் உள்ளதாக அறிவிக்கப்படுகிறது. அது போல டாப்லிரிக்ஸ் நெட் என்ற தளம், சர்ச்
என்ஜின் மூலம் பல்லாயிரக்கணக்கான, லேட்டஸ்ட் பாடல்களை கேட்டு மகிழ முடியும். இந்த
பட்டியலில் உள்ள மற்றொரு தளம் ஏ இசெட் லிக்ஸ்டாட்காம் எனப்படுவதாகும்.</p>

<p> </p>

<p>கீதா உபதேசம்</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>கிருஷ்ணர், அர்ஜுனனுக்கு விளக்கமாகச் சொல்கிறார். யார் யாருக்கு யோகம்
சாத்தியமில்லை? என்று. </p>

<p>அளவுக்கதிகமாக உணவு கொள்பவர்களுக்கோ, (சாப்பாடே இல்லாமல்) பட்டினி
கிடப்பவர்களுக்கோ யோகம் கைவராது </p>

<p>அதே மாதிரி அதிகமாகத் தூங்குபவர்களுக்கும், உறக்கத்தைக் கைவிட்டுக் கண்
விழித்திருப்பவர்களுக்கும்கூட யோகம் சாத்தியமில்லை அர்ஜுனா </p>

<p>அடுத்ததாக பகவான் யோக லட்சியத்தையும், அதை அடையும் முறையையும்
விளக்குகிறார். அந்த முறையைப் பல வருஷங்கள் தொடர்ந்து பயிற்சி செய்து வந்திருக்கும்
சிலர், எவ்வித ஆன்மீக முன்னேற்றமும் காண முடியவில்லையே, ஏன்? சிலருக்கு இலட்சியத்தை
அடைய முடியாது. அவர்களுக்கு இடையூறாக இருப்பது எது? இப்படி யெல்லாம் சிலருக்கு ஐயங்கள்
ஏற்படலாம். சாதகத்தில் வெற்றி பெற என்னென்ன நடவடிக்கைகளை முன் ஜாக்கிரதையாக
எடுத்துக்கொள்ள வேண்டும் என்பதையும் சொன்னால், அப்பொழுது முழுமையான
விஞ்ஞானக் கருத்தாக அமைந்துவிடும் அல்லவா? ஒரு சாதகர் எந்த எந்த
இடங்களில் தடுமாற்றத்துக்கு ஆளாகலாம் என்பதையும் சில பாடல்கள் தெரிவிக்கின்றன.
</p>

<p>தியானம் நிறைவேற உடல், மன அறிவு மார்க்கங்களில் ஒருவர் மேற்கொள்ளும்
நடவடிக்கைகள் மிதமானவையாக இருக்க வேண்டும். ஏனெனில், மிதமிஞ்சிய கொள்கைகளின்
விளைவாக, வீண் பரபரப்பு, உணர்ச்சிக் குழப்பம் முதலியவை தொடர்ச்சியாக வந்து, நமது
பொருளுறைகளைத் தாக்குகின்றன. ஒருமுக இசைவு பெற்றிருக்க வேண்டிய ஒருவர் சீர்குலைந்து
விடுகிறார். எனவே தான் முன்னே சொன்னதுபோல உணவு, தூக்கம், ஓய்வு முதலியவற்றிலெல்லாம்
மிதமாக இருக்க வேண்டும் என்று கண்டிப்பான கட்டளை சொல்லப்பட்டது. </p>

<p>''உணவு கொள்ளுதல்'' என்பதும் மிகையாக இருக்கக் கூடாது. இந்த உணவு கொள்ளுதல்
என்று கூறப்படும்போது எல்லா வகையான புலன் நுகர்வுகளும், மன உணர்ச்சிகளும், அறிவின்
இயக்கமும் அதில் அடங்கியுள்ளன. எனவே உணவு என்பது சாப்பாடு மட்டுமல்ல; எல்லா அகத்துறை
அனுபவங்களும் புலன்கள் மூலம் நாம் பெறும் உணர்ச்சிகளையும் உள்ளடக்கியதே. </p>

<p> </p>






</body></text></cesDoc>