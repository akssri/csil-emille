<cesDoc id="tam-w-dinakaran-other-00-11-04" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-00-11-04.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-11-04</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-11-04</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சென்னை,
நவ. 4- தமிழகத்தில் நேற்று ஒருசில இடங்களில் லேசான மழை பெய்தது. </p>

<p>முன் அறிவிப்பு:-
தமிழ்நாட்டில் பரவலாக மழை அல்லது இடியுடன் கூடிய மழை பெய்யக்கூடும். </p>

<p>சென்னை:- சென்னை மற்றும்
அதன் சுற்றுப்புறங்களில் வானம் மேகமூட்டத்துடன்் காணப்படும். சில இடங்களில் லேசான மழை
பெய்யக்கூடும். </p>

<p>வெப்பநிலை:-
நுங்கம்பாக்கம்:- அதிக அளவு 31.5 டிகிரி செல்சியஸ். குறைந்த அளவு 23.7டிகிரி
செல்சியஸ். ஈரப்பதம் 79சதவீதம். </p>

<p>மீனம்பாக்கம்:- அதிக
அளவு 32 டிகிரி செல்சியஸ். குறைந்த அளவு 24.1டிகிரி செல்சியஸ். ஈரப்பதம் 85
சதவீதம். </p>

<p>www.dinosaur.museum.org</p>

<p> </p>

<p>   
அமெரிக்காவில் உள்ளது கேன் ஜூவான் கவுன்ட்டி. இங்குள்ள யுட்டா பகுதியில் பிளான்டிங்
நகரில் அமைந்துள்ளது டைனோசர் மியூசியம். இந்த மியூசியத்தில் டைனோசர் உலகின் முழு
வரலாறும் விளக்கமாக சித்தரிக்கப்பட்டுள்ளது. டைனோசர்களின் எலும்புக் கூடுகள், தோல்
படிவங்கள், முட்டைகள், கால் பதித்த தடயங்கள், கிராபிக்ஸ் வடிவங்கள், தத்ரூபமான
டைனோசர் சிற்பங்கள், டைனோசர் முட்டைகள் ஆகியன உலகின் நான்கு திசைகளில் இருந்தும்
கொண்டு வரப்பட்டு இங்கு காட்சிக்கு வைக்கப்பட்டுள்ளன. </p>

<p>   
இந்த மியூசியம் ஒரு லாபம் கருததாத, பொதுத்துறையைச் சேர்ந்த தரும நிறுவனம் ஆகும். இந்த
வெப் தளம் யுட்டா ஹுமானிட்டீஸ் கவுன்சில் என்ற அமைப்பின் நிதியுதவி ஆதாரத்துடன்
செயல்படுகிறது. </p>

<p>காவடி தத்துவம்</p>

<p> </p>

<p>   
முருகப் பெருமானுக்குரிய பிரார்த்தனைகளில் ஒன்றான யயகாவடி எடுக்கும் 
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>பழக்கத்திற்கு ஒரு வரலாற்றுப் பின்னணி உள்ளது. </p>

<p>   
இடும்பன் என்பவன் மிகவும் பக்திமான். அவன் சூரபத்மனுக்குப் போர்க்கலையைப் போதிக்கும்
ஆசிரியனாக இருந் தான். சூரபத்மன் கொடுமைகள் புரிய ஆரம்பிக்கவே அவனை விட்டு அகன்று
அகஸ்தியரிடம் சென்று அடைந்தான். அகஸ் தியரின் கட்டளைக்கிணங்க கயிலை சென்று மலைகள் இருபுற
மும் தொங்க காவடியாகக் கட்டி எடுத்துக் கொண்டு வந்தான். முரு கன் இவ்விரு மலைகளையும்
திருவாவினன் குடியில் நிலை பெறச் செய்யவும், இடும்பனுக்கு அருளவும் விரும்பி, ஒரு
திருவிளையாடல் நடத்தினார். அப்படி வந்து கொண்டிருந்த இடும்பன் வழி தெரியாது திகைத்த போது
முருகன் குதிரை மீது செல்லும் அரசன்போலத்் தோன்றி இடும்பனை, ஆவினன்குடிக்கு அழைத்து வந்து
சற்று இளைப்பாறி விட்டுச் செல்லும்படிக் கூறினார். </p>

<p>   
அப்படி காவடியை இறக்கி வைத்த இடும்பன் மீண்டும் புறப் படும்போது காவடியைத் தூக்க முடியாமல்
திண்டாடுகிறான். திகைத்து நின்று ஆராய்ந்தபோது மலை மீது ஒரு சிறுவன் கோவணாண்டியாக கையில்
தண்டேந்தி நிற்பதைக் கண்டான். மலையை விட்டு இறங்கு என்று இடும்பன் வேண்டுகிறான். இம்மலை
தனக்கே சொந்தம் என்று சிறுவன் உரிமை கொண்டாடுகிறான். கோபமடைந்த இடும்பன்
அச்சிறுவனைத் தாக்க முயன்றபோது வேரற்ற மரம்போல விழுந்தான். </p>

<p>   
இடும்பனின் மனைவியும், அகஸ்தியரும் வேண்டிக்கொள்ளவே முருகன் இடும்பனுக்கு அருளாசி
புரிந்தார். இடும்பனது கட்டொழுங்கையும், குரு பக்தியையும் பாராட்டிய முருகன் அவனையே தனது
காவல் தெய்வமாக நியமித்ததுடன் இடும்பனைப் போல சந்தனம், பால், மலர் போன்ற
பொருட்களுடன் காவடி எடுத்து தன் சன்னதிக்கு வருபவர்களுக்கெல்லாம் அருள் பாலிப்பதாகவும்
வாக்களித்தார். முருகனின் ஆறுபடைக்கும் முருக பக்தர்கள் காவடி எடுத்துக் கொண்டு
நெடுந்தொலைவிலிருந்து புனித பாத யாத்திரை செல்வது தமிழர்களின் பல ஆண்டு பழக்க மாகும்.
தமிழர்களின் மீது பல பண்பாட்டு படையெடுப்புகள் நடந்தும் இப்போதும் தொலைந்து போகாத
இறையுணர்வை காட்டுவதுதான் காவடி தத்துவமாகும். </p>

<p>   
-அண்ணா அன்பழகன், அந்தணப்்பேட்டை. </p>

<p> </p>






</body></text></cesDoc>