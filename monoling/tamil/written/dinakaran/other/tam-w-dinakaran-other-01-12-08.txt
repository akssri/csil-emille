<cesDoc id="tam-w-dinakaran-other-01-12-08" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-01-12-08.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-12-08</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-12-08</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ஒளி அலை, ஒளி ஆண்டு
என்பதெல்லாம் என்ன?</p>

<p> </p>

<p>ஒலியைப்பற்றி பல காலமாக பல ஆய்வுகள் வந்துள்ளன.
எப்படி உருவாகியிருந் தாலும். ஒளி பரவல் அலையாகத்தான் செல்கிறது என்ற உண்மைக்கு ஆதாரத்தை
காட்டியவர் ஆங்கில பௌதிகவாதியாளர் தாமஸ் யங். ஒளி பல வண்ணங்களில் ஒவ்வொன்றும்
ஒவ்வொரு அலை நீளத்தில் பரவுகிறது என்பதை அறிவித்தார். இந்த அலை தூர கணக்கே
பின்னாளில் கோள்களுக்கிடையே உள்ள தூரத்தை அளக்க அடிப்படையாக இருந்தது. பூமியிலிருந்து
சூரியனின் தூரத்தை, ஒளியின் அலையை அனுப்பி அது செல்லும் காள அளவை
பெருக்கிக்கொள்ளும்போது சரியான தூரத்தை கணக்கிட முடியும். அவ்வாறு ஓர் ஒளி ஒரு
வருடத்தில் பயணிக்கும் காலளவு ஒரு வினாடிக்கு 2,97,600 கி.மீ. ஆகும். இதுவே ஒளியாண்டு
ஆகும். </p>

<p>ஒளியை ஆராயும் கொள்கை
''குவாண்டம்-தியரி'' எனப்படும்</p>

<p> </p>

<p> </p>

<p>தேவனின் இல்லம்</p>

<p> </p>

<p>தன் மனைவியில் அன்பு கூறுகிறவன் தன்னில்தான் அன்பு கூறுகிறான்.
தெய்வப் பயத்யோடு ஒருவருக்கொருவர் கீழ்ப்படிந் திருங்கள். -எபேசியர் 5: 21, 28.
</p>

<p>என்று வேத வசனம் கூறுகிறது. உண்மைதான். தம்பதிதான் தத்தம் தவறுகளை
புரிந்துகொண்டு ஒருவருக்கொருவர் விட்டுக் கொடுத்து தேவ விசுவாசத்தோடு நடந்து கொண்டால்
உங்கள் இல்லம் அமைதியான அன்பான இல்லமாக மாறி விடும். அன்பும், அமைதியும் உறையும்
இடங்களே தேவனுக்குப் பிரியமான இடம். எனவே உங்கள் இல்லமும் மகிமைகள் நிறைந்த தேவனின்
இல்லமாக மாறி விடும். </p>

<p>இன்து தம்பதியர் சிலர் தங்களுக்குள் பிரதிபலனை எதிர்பார்த்து தோல்வியைத்
தழுவி ஒருவருக்கொருவர் துன்பத்திற்கு ஆளாகிறார்கள் ஆகவேதான் புருஷன் தன் மனைவிக்கு செலுத்த
வேண்டிய கடமையைச் செலுத்தக் கடவன். புருஷர்களே, நீங்கள் விவேகத்துடன் அவர்களுடன்
வாழ்ந்து உங்களோடுகூட அவர்களும் நித்திய ஜீவனாகிய கிருபையைச் சுதந்தரித்துக்
கொள்ளுகிறவர்களானபடியினால், அவர்களுக்குச் செய்ய வேண்டிய கனத்தைச் செய்யுங்கள் என்று
கணவனுக்கும் </p>

<p>(1 பேதுரு 3: 7, 1 கொரிந்தியர் 7: 3) </p>

<p>மயிரைப் பின்னி, பொன்னாபரணங்களை அணிந்து, உயர்ந்த வஸ்திரங்களை உடுத்திக்
கொள்ளுதலாகிய புறம்பான அலங்கரிப்பு உங்களுக்கு அலங்காரமாயி ராமல், அழியாத
அலங்கரிப்பாயிருக்கிற சாந்தமும், அமைதலுமுள்ள ஆவியாகிய இருதயத்தில் மறைந்திருக்கிற குணமே
உங்களுக்கு அலங்காரமாயிருக்கக் கடவது. அதுவே தேவனுடைய பார்வையில் விலையேறப்பெற்றது (1
பேதுரு 3:3, 4) என்று, மனைவிக்கும், வேதம் இப்படி உபதேசித்திருக்கிறது. </p>

<p>காற்று கடலோடு சேரும்போது அலையாகிறது. மூங்கிலோடு சேரும் போது இசையாகிறது.
மனிதனோடு சேரும்போது உயிராகிறது. அதுபோல் கணவனின் கடமையறிந்து மனைவியும், மனைவியின்
குணமறிந்து, கணவனும் கலக்கும் போதுதான் வாழ்க்கை அர்த்தமாகிறது. </p>

<p>எனவே மேற்கண்ட வேத வசனங்கள் கூறுவது போன்று அவ்வார்த்தையை வாழ்வில்
செயல்படுத்துங்கள். தேவன் உங்களுக்குத் துணையாவார். சிந்தியுங்கள்.</p>

<p> </p>






</body></text></cesDoc>