<cesDoc id="tam-w-dinakaran-other-00-08-26" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-00-08-26.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-08-26</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-08-26</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சென்னை,
ஆக.26- தமிழ்நாட்டில் நேற்று முன்தினம் ஒரு சில இடங்களில் பலத்த மழை பெய்தது. மழை அளவு
சென்டி மீட்டரில் வருமாறு:- </p>

<p>வால்பாறை-11 செ.மீ.,
செங்கோட்டை- 7, தோவாளை, நாகர்கோவில்-6, சேரன் மகாதேவி, அம்சாசமுத்திரம்,
பொள்ளாட்சி-2. </p>

<p>முன் அறிவிப்ப்பு:-
தமிழகம் மற்றும் புதுவையில் ஆங்காங்கு மழையோ அல்லது இடியுடன் கூடிய மழையோ பெய்யக் கூடும்.
</p>

<p>சென்னை:- சென்னை மற்றும்
அதன் சுற்றுப் புறங்களில் வானம் மேக மூட்டத்துடன் காணப்படும். சில இடங்களில் லேசான மழை
பெய்யலாம். </p>

<p>வெப்பநிலை:
நுங்கம்பாக்கம் அதிக அளவு 35.1 டிகிரி செல்சியஸ். குறைந்த அளவு 26.5 டிகிரி செல்சியஸ்,
ஈரப்பதம் 61 சதவீதம். </p>

<p>மீனம்பாக்கம்:- அதிக
அளவு 34.6 டிகிரி செல்சியஸ். குறைந்த அளவு 26.4 டிகிரி செல்சியஸ். ஈரப்பதம் 60
சதவீதம். </p>

<p> </p>

<p>www.scaredsites.com</p>

<p>   
சமாதானமும், சக்தியும் தரவல்ல உலகின் முக்கிய தலங்கள் பற்றி இந்த வெப் தளத்தில்
பேசப்படுகிறது. </p>

<p>    புதை
பொருள் ஆராய்ச்சியாளரும், புகைப்பட நிபுணருமான மார்ட்டின் கிரே என்பவர் உலகில் உள்ள
70 நாடுகளுக்கு ஒரு யாத்ரீகராக சென்று ஒவ்வொரு நாட்டிலும் அவர் தரிசித்த அற்புத தலம்
பற்றிய அரிய தகவல்களை அந்த வெப்தளத்தில் திரட்டித் தந்துள்ளார். தலம் பற்றிய
விவரங்கள் மட்டுமல்லாது அவை ஒவ்வொன்றையும்- நாம் நேரில் தரிசிப்பதைப் போல
உயிரோட்டமான, தத்ரூபமான புகைப்படங்களையும் இந்த தளத்தில் இணைத்துள்ளார்.
ஆயிரத்துக்கும் மேற்பட்ட புகைப்படங்கள் இணைக்கப் பட்டுள்ளன. </p>

<p> </p>

<p>வீர தீர ஆஞ்சநேயர்</p>

<p>   
காஞ்சிபுர மாவட்டம், செங்கல்பட்டு வட்டத்தில், செங்கல் பட்டின் மையப்பகுதியில்
அருள்மிகு கோதண்டராமசுவாமி திருக்கோயில் உள்ளது. 
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>இக்கோவிலில் வீர ஆஞ்சநேயர்
சன்னதி உள்ளது. இங்கு சனீஸ்வரனை அடக்கிய பெருமானாக சுவாமி காட்சியளிக்கின்றார்.
அனுமனின் பல்வேறு அரும் செயல்கள் பற்றி ஆனந்த இராமாயணம் எனப்படும் தெலுங்கு
இராமாயணத்தில் விரிவாகக் கூறப்பட்டுள்ளது. இங்கு உள்ளது போன்ற அனுமன் உருவம்
அரிதாகத்தான் காணப்படும். </p>

<p>   
இராமாயணத்தில், அனுமனின் பங்கு மகத்தானது. சீதையின் இருப்பிடத்தினை முதலில் அறிந்தவன்
அவனே. சீதையை சிறையிலிருந்து மீட்க, இலங்கையின் மீது படையெடுக்க முடிவு செய்யப்பட்டது.
இதன்படி வான ரங்களின் உதவியுடன் பாலம் அமைக்கப்பட்டன. </p>

<p>   
அனுமன், பாலம் அமைக்கும் பணியில் ஈடுபட்டபோது, சனி பகவான் அனுமனைப் பிடிக்க வேண்டி
அனுமன் அருகில் சென் றார். அனுமன் தான் இராமனின் திருப்பணியில் ஈடுபட்டுள்ளதாக வும்,
தன்னைப் பிறகு வந்து பிடித்துக் கொள்ளலாம் எனக் கூறி னார். ஆனால், கொண்ட கடமை யிலிருந்து
தவறாத சனி, அனு மனை விட்டு அகல மறுத்து அவர் தலையில் ஏறி அமர்ந்து விட்டார்.
</p>

<p>   
தலைபாரம் அழுத்தியதால் அனுமன் தனியாக அமர்ந்தது கண்ட வானரங்கள், அனுமன் வேலை செய்யப்
பயந்து அமர்ந்து விட்டதாக எண்ணி நகைத்தன. இதனைக் கண்ட அனுமன் சனிபகவானை, தம்
கால்களில் பிடித்துக் கொள்ளக் கூறினார். தலைவழியாக கால்களில் இறங்கத் தொடங்கியபோது,
அனுமன் சனியினைத் தன் கால்களின் கீழ் அழுத்தி அவர் மீது ஏறி நின்றார். சனீஸ்வரன்
அனுமனின் பலத்தினை அறிந்து அவரிடம் மன்னிப்புக் கேட்டார். </p>

<p>   
மேலும் இராமபிரானின் திருப்பணியில் ஈடுபட்ட போது அவருக்குத் தொல்லைக்்
கொடுத்ததற்காக மன்னிப்புக் கேட்டதுடன், அனுமனை வணங்கும் எவருக்கும் துன்பம்
விளைவிப்பதில்லை எனவும் உறுதி கூறினார். எனவே சனீஸ்வரன் துன்பத்திற்கு ஆளாகா மல்
இருக்கப் பலர் இத்தலத்திற்கு வந்து வேண்டுதலை நிறை வேற்றுகின்றனர். இன்றும் இக்கோவில்
திருமணம் வேண்டி, வேலை வேண்டி, உடல் நலம் வேண்டி பக்தர்கள் தேங்காய், புதிய துண்டு, துணி
போன்றவற்றை அனுமன் சன்னதியில் கட்டி வைக் கின்றனர். வேண்டுதல் நிறை வேறியவுடன்
அதனைக் கோயிலுக்கே காணிக்கையாக்கி விடுகின்றனர். </p>

<p>- கிருஷ்்ணபாரதி,
பொள்ளாச்சி. </p>

<p> </p>






</body></text></cesDoc>