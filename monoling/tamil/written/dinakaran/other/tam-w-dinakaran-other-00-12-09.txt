<cesDoc id="tam-w-dinakaran-other-00-12-09" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-00-12-09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-12-09</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-12-09</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சென்னை,
டிச.9- தமிழகத்தில் நேற்று முன்தினம் வறண்ட வானிலை காணப்பட்டது. </p>

<p>முன் அறிவிப்பு:-
தமிழகம் மற்றும் புதுவையில் ஒரு சில இடங்களில் லேசான மழை பெய்யலாம். </p>

<p>சென்னை:- சென்னை மற்றும்
அதன் சுற்றுப்புறங்களில் வானம் மேக மூட்டத்துடன் காணப்படும். குறைந்தபட்ச வெப்ப நிலை 21
டிகிரி செல்சியசாக இருக்கும் </p>

<p>வெப்பநிலை:- நுங்கம்பாக்கம்:-
அதிக அளவு 29.1 டிகிரி செல்சியஸ். குறைந்த அளவு 21 டிகிரி செல்சியஸ். ஈரப்பதம்
84.4 சதவீதம். </p>

<p>மீனம்பாக்கம்:- அதிக
அளவு 29.2 டிகிரி செல்சியஸ். குறைந்த அளவு 19.6 டிகிரி செல்சியஸ். ஈரப்பதம் 75
சதவீதம். </p>

<p>www.ind.cricket.org</p>

<p> </p>

<p> </p>

<p>   
இண்டர்நெட்டில் கிரிக்கெட்டின் வீடு இந்த தளம் என இதன் அமைப்பாளர்களால்
வர்ணிக்கப்படுகிறது. இதில் ஹோம் என்ற தலைப்பின் கீழ் சைட் மேப், வேலை வாய்ப்புகள்,
பிரஸ் ஆபீஸ். அதிகாரப்பூர்வ வெப் தளங்கள் ஆகியவை பற்றிய தகவல்கள் அடங்கியுள்ளன.
சீரிஸ் என்ற தலைப்பின் கீழ் உலகில் எந்தெந்த பகுதிகளில் எந்தந்தெ நாட்டு கிரிக்கெட்
அணிகள் மோத உள்ளன. மோதிக் கொண்டிருக்கின்றன என்ற செய்திகள், விரிவாக
சொல்லப்படுகிறது. அது போல டொமஸ்டிக் என்ற தலைப்பில் உள்நாட்டு வௌிநாட்டு போட்டி
நிகழ்ச்சிகள் பற்றி பேசப்படுகிறது. லேட்டஸ்ட் என்ற் தலைப்பின் கீழ் தற்போது உள்ள
கிரிக்கெட் நிலவரங்கள் தரப்படுகின்றன. டேட்டா பேஸ் என்ற தலைப்பின் கீழ்
புகைப்படங்கள், வீரர்கள், புள்ளி விவரங்்கள் தரப்படுகின்றன.</p>

<p> </p>

<p>திருக்கார்த்திகை
தீபம்</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>   
திருக்கார்த்திகை தீபவிழா கார்த்திகை மாதம் கிருத்திகை நட்சத்திரத்தில் கொண்டாடப்
படுகிறது. விளக்கு ஏற்றிக் கொண்டாடப் படுவதால் இதைத் தீப விழா என்று அழைக் கின்றோம்.
இவ்விழா, சிவனையும், முருகனையும் போற்றி வழிபடும் விழா ஆகும். சிவன் நிலம், நீர், தீ,
காற்று, ஆகாயம் ஆகிய ஐம்பூதங்களின் வடிவமாக விளங்குகிறார். </p>

<p>சிவபெருமான்
திருவண்ணாமலையில் ஒளி வடிவில் எழுந்தருளியிருப்பதால்் அண்ணாமலையார் என்றும் அருணாசல
ஈசுவரர் என்றும் அழைக்கப்படுகிறார். ஒருமுறை திருமாலுக்கும் நான்முகனுக்கும் தம்முன் யார்
பெரியவர்? என்ற போட்டி நிலவியது. சிவபெருமான் அவர்கள் இருவரின் ஆனவத்தைப்
போக்குவதற்காக-தானே முதலும், முடிவும் இல்லாத, அடிமுடி காண முடியாத பேரொளியாகத் தோன்
றினார். இருவருள் இப்பே ரொளியின் அடியையோ முடியையோ காண்பவரே பெரியவர் என்று கூறிக்
கொண்டு அடியைத் திரு மாலும், முடியை நான் முகனும் தேட முடிவு செய்தனர். </p>

<p>திருமால் வராக அவதாரம்
எடுத்துச் சிவபெருமானின் திரு வடியை தேடிச் சென்றார். நான் முகன் அன்னப்பறவை வடி வெடுத்துக்
திருமுடியைக் காணச் சென்றார். இருவராலும் சிவனின் அடியையோ, முடியையோ காணயியலவில்லை.
அப்போது சிவ பெருமானின் திருமுடியிலிருந்்து விழுந்த தாழம்பூ ஒன்று கீழ்நோக்கி வருவதைக்
கண்டார். தாழம்பூவிடம் தன் நிலையை விளக்கிதான் சிவபெருமானின் திருமுடியை
கண்டுவிட்டதாகக் கூறுமாறு வேண்டினார். தாழம் பூவும் ஒப்புக் கொண்டது. உடனே கீழே பறந்து
வந்த நான்முகன் சிவபெருமானின் திருவடியைத் தான் கண்டதாகக் கூறினார். தாழம்பூவும் அவருக்கு
பரிந்து பொய் சாட்சி கூறியது. உண்மை அறி்ந்த சிவபெருமான் பொய் சொன்ன நான்முகனுக்குக்
கோவில் வழிபாடு கிடையாது என்றும், பொய் சாட்சி சொன்ன தாழம்பூ தன் வழிபாட்டிற்கு
ஏற்றுக் கொள்ளப் படாது என்றும் சாபமிட்டு மறைந்தார். </p>

<p>இவ்வாறு சிவனின் அடி முடியை
காணாத, காணமுடியாத பேரொளியாக திருவண்ணாமலையில் காட்சி தந்த நாளைதான் நாம் திருக்கார்த்திகை
தீபத் திருநாளாகக் கொண்டாடுகிறோம். இன்னாளில் திருவண்ணாமலை உச்சியில் தீபம்
ஏற்றப்படும். தமிழர்களின் முக்கிய பண்டிகைகளில் முக்கியமானதொரு விழா வான இந்நாளில்
தமிழ் வீடுகளில் எல்லாம் அகல் விளக்கேற்றி வழிபடுவது ஒரு சிறப்பு. </p>

<p>-
கே.ரவிக்குமார்,சென்னை-53 </p>

<p> </p>






</body></text></cesDoc>