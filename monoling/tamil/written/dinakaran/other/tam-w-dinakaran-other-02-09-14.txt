<cesDoc id="tam-w-dinakaran-other-02-09-14" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-02-09-14.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-09-14</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-09-14</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>வருங்கால பாரதமே வருக</p>

<p>  </p>

<p>சோனியாவை வௌி நாட்டுக்காரர் என்று குற்றம் சாட்டி ஜெயலலிதா
அகில இந்திய அரசியலில் பரபரப்பை ஏற்படுத்தியது ரத்தத்தின் ரத்தங்களுக்கு உற்சாகத்தை
ஏற்படுத்தி விட்டது போலும். தேசிய அளவில் அம்மா 3-வது அணி அமைக்கிறார். அதற்கான ஆயத்த
ஏற்பாடுகளைச் செய்ய விரைவில் அகில இந்திய சுற்றுப்பயணம் மேற்கொள்வார் என்று தெரிவித்து
இருக்கிறார்கள். </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>இதனால் அம்மா இப்போது எந்த விழாவில் கலந்து கொண்டாலும்
அம்மாவை வருங்காலப் பிரதமராகப் பார்க்க ஆரம்பித்து விட்டார்கள். சமீபத்தில் கலைவாணர்
அரங்கில் 4 திருமணங்களை அம்மா நடத்தி வைத்தார். அதில் அரங்கை சுற்றிலும் அம்மா
கட்அவுட்களை வைத்து அசத்தி இருந்த தோடு வருங்கால பாரதமே வருக என்று அகில இந்திய ஆசையையும்
தெரிவித்து இருந்தார்கள். அம்மாவை குளிரவைக்க எந்த எல்லைக்கும் போவார்கள் என்பது
சரிதான். </p><p>
  </p>

<p>ஜெயலலிதா நடத்திய அவசர ஆலோசனை</p>

<p>  </p>

<p>காவிரி பிரச்சினையில் ஆணைய கூட்டத்தை பிரதமர் அவசரமாக
கூட்டியதும் தனக்கு உடல் நலம் சரியில்லை என்று முதல் மந்திரி ஜெயலலிதா அறிவித்தார். 3
நாட்களுக்குப் பிறகு ஜெயலலிதா கோட்டைக்கு வந்ததும் அவசரம் அவசரமாக மந்திரிகளையும்,
அதிகாரிகளையும் அழைத்துப் பேசினாராம். அப்போது முக்கிய அதிகாரி ஒருவர் காவிரி
பிரச்சினையில் அனைத்துக் கட்சி கூட்டம் கூட்டாதது ஒரு குறையாக எதிர்க்கட்சிகளால்
விமர்சிக்கப்படும் என்று கூறினாராம். கர்நாடகத்தில் இதுவரை 4 முறை அனைத்துக் கட்சி
கூட்டம் கூட்டப்பட்டதும் சுட்டிக்காட்டப்பட்டதாம். </p>

<p>இதைத் தொடர்ந்து காவிரி பிரச்சினை குறித்து தமிழ்நாட்டில்
அனைத்துக் கட்சி கூட்டம் கூட்ட ஜெயலலிதா சம்மதித்தாராம். அனைத்துக்கட்சி தலைவர்கள்,
டெல்டா பகுதி விவசாய அமைப்புகளின் தலைவர்கள் கூட்டம் கூட்டி மத்திய அரசைக் கண்டித்தும்,
சுப்ரீம் கோர்ட்டை வற்புறுத்தியும் தீர்மானம் நிறைவேற்ற முடிவு செய்யப்பட்டுள்ளதாம்.
</p><p>
  </p>

<p>தத்துவம் விளக்கும்
கனகசபை</p>

<p>
 
 
<gap desc="illustration"></gap>உள்ளம் பெருங்கோயில் ஊனுடம்பு ஆலயம் என்பார் திருமூலர். இதயம்
எவ்வாறு இயங்குமோ அதுபோன்ற அமைப்பில் செயல்படும் சிவாலயம் தில்லை நடராஜர் ஆலயம்.
</p>

<p>இதன் பிரகாரங்கள் நமது உடலாக உள்ளன. கனகசபை நமது இதயமாக
உள்ளது. கனகசபைக்கு நேரடியாக நுழைவாயில் இல்லாமல் வலப்புறமும், இடப்புறமும் உள்ளன.
நடராஜர் ஆடும் சபைக்கு படிகள் ஐந்து. அவை ந-ம-சி-வா-ய என்ற ஐந்தெழுத்து மந்திரத்தைக்
குறிக்கின்றன. சுவரில் காணப்படும் 96 சதுர வடிவான ஜன்னல் துளைகள் 96 தத்துவங்களைக்
குறிக்கின்றன. </p>

<p>கனக சபையில் 4,6,18,28 என்று தூண்கள் உள்ளன. இவைகள் நான்கு
வேதங்களையும், அறு வேதாகமங்களையும், 18 புராணங்களையும், 28 சிவாகமங்களையும்
குறிக்கின்றன. கனகசபையின் மேற்கூரையில் காணப்படும் 64 சதுரப் பிரிவுகள் 64 கலைகளையும்
குறிக்கும் வகையில் உள்ளன. </p>

<p>கனகசபையின் மேலே வேயப்பட்டிருக்கும் தங்கக்கூரையில் உள்ள 81000
தங்கத் தகடுகள் நாம் ஒரு நாள் பொழுது சுவாசிக்கும் சுவாச எண்ணிக்கையைக் குறிப்பது. இதில்
அடிக்கப்பட்டிருக் கும் 72000 ஆணிகள் நம்்் உடம்பில் உள்ள நாடிகளைக் குறிக்கும்.
கனகசபையின் மேல் உள்ள 9 கலசங்கள் நவசக்திகளையும், மண் டபத்தில் உள்ள ஐந்து பீடங்கள்
திருமூர்த்திகள், மகேசுவர, சதாசிவம் ஆகியோரைக் குறிக்கும். கனகபையில் உள்ள நடராஜரின்
உடுக்கை ஆக்கலையும், கையில் உள்ள நெருப்பு அழித்தலையும், அபயம் அமைப்பு காத்தலையும்
குறிக்கிறது. உயரே தூக்கியுள்ள கால் சரணாகதியைக் குறிப்பதாகத் திகழ்கின்றது.
</p><p>
  </p><p>
  </p>

<p>- பி.முத்துராமலிங்கம், சிதம்பரம். </p>

<p> </p>






</body></text></cesDoc>