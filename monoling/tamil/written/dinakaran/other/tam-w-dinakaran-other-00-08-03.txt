<cesDoc id="tam-w-dinakaran-other-00-08-03" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-00-08-03.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-08-03</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-08-03</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ஓசியில் கிடைக்கும் ஓசோன்
பிராணக்காற்று</p>

<p>உலகிலுள்ள ஒவ்வொரு
மனிதனுமே, தாயின் கர்ப்பப்பையில் தியான நிலையில் இருந்துதான் பிறக்கிறான். ஏனென்றால்,
தாயின் கர்ப்பபைகூட ஒரு பிரபஞ்சம்தான். தாயின் கர்ப்பபையிலுள்ள முழு வளர்ச்சி
பெற்ற குழந்தையானது, உணர்வு நிலை அல்லது விழிப்புநிலை அல்லது தனக்கு தேவையான னாபிராண
சக்தியை தாயின் தொப்புள் கொடியின் மூலம் பெறுகின்ற நிலை என்பது, ஒவ்வொரு அதிகாலை
பொழுதான 3 முதல் 6 வரையிலான பிரம்ம முகூர்த்தத்தில் தான் நடைபெறுகிறது. மேலும், பிறந்த
குழந்தையானது முதல் மூன்று மாதத்திற்கு சுத்தமான பிராணசக்தி அதிகமாக படர்கின்ற அதிகாலை நேரமான
பிரம்ம முகூர்த்தத்தில் விழித்து, சுறுசுறுப்புடன் இயங்கி கொண்டிருப்பதையும் நாம் காணலாம்.
</p>

<p>ஆக சூரியன் உதயத்திற்கு
முந்தைய நேரமாக பிரம்ம முகூர்த்தம் என்கிற அதிகாலை நேரத்தில்தான் குயில்கள் கூவுகின்றன,
சேவல் கள் கொக்கரிக்கின்றன, காக்கைகள் கரைகின் றன, பிறந்த மூன்று மாதத்திற்குட்பட்ட
குழந்தை களும் விழித்து அழுகின்றன. இந்த அடிப்படை யில் மனிதனும் குழந்தை பருவத்தில் மற்ற
ஜீவராசிகளைப் போலவே இயற்கையாகவே அதிகாலை பொழுதில் தான் விழித்தெழுகிறான்.
</p>

<p>பிரம்ம முகூர்த்தம்
என்கிற அதிகாலை தியான பயிற்சியை குழந்தைகள், முதியவர்கள், இளைஞர்கள், கர்ப்பிணி
பெண்கள் என அனைத்து தரப்பினருமே கடைபிடிக்கலாம். அடியேன் சொல்லுகின்ற அதிகாலை தியான
பயிற்சி என்பது மிகச்சிறந்த நாடிச் சுத்தி முறையாகும். பிரம்ம முகூர்த்தம் என்பது
முதன்மையான நேரம் ஆகும். அதாவது, சூரிய உதயத்திற்கு முந்திய 90 நிமிடங்களை குறிப்பதாகும்.
இந்த பிரம்ம முகூர்த்த நேரத்தில், பிரபஞ்சமாகிய அண்டவௌியில் உள்ள ஓசோன் கலந்த
பிராணவாயு (ஆக்சிஜன்) மிகுதியாக காற்றிலே கலந்து இருக்கும். இந்த நேரத்தில் நாம்
திறந்த வௌியில் கிழக்கு முகமாக நின்றவாறு நமது இரு கைகளையும் தலைக்கு மேலே உயர்த்தி,
கைகளை கூப்பியவாறு (கும்பிடும் நிலை) நிற்க வேண்டும். விழிகள் இரண்டும் விண்ணை நோக்கி
இருக்க வேண்டும். </p>

<p>பிராண சக்தி</p>

<p>இப்பொழுது மென்மையாக நமது
விழிகள் இரண்டையும் மூட வேண்டும். சில நொடிகளுக்கு பின்பு நமது குதிகால்களை சிறிது
தூக்கியவாறு, அதாவது பெருவிரல்கள் மட்டும் தரையில் பதிந்த நிலையில் நிற்க வேண்டும்.
குறைந்தபட்சம் 21/2 நிமிடங்கள் நின்றாலே போதுமானது.
இந்த தருணத்தில் நமது சுவாசம் சற்று மிகுதியாகும். அதாவது நமக்கு மேல்மூச்சு, கீழ்மூச்சு
வாங்கத் தொடங்கும், இந்தநிலை மிகவும் ஆரோக்கிய மானது. ஏனென்றால் இப்பொழுதுதான் நமது
உடலிலுள்ள ஒவ்வொரு அணுக்களும் தனக்கு தேவையான பிராணசக்தியை கவர்ந்திழுத்து கொள்கிறது.
இதன் காரணமாக நமது இதயத்திற்கு, மூளைக்கு சுத்தமான பிராணசக்தி கலந்த ரத்தம் கிடைப்பதால்
நமது உடலிலுள்ள நோய் எதிர்ப்பு தன்மை மிகுதியாகும். இதற்கு அடுத்து, கிழக்கு முகமாக
தரையில் அமர்ந்து, முதுகு வளையாதப்படி சம்மணமிட்டு விழிகளை மூடியவாறு குறைந்தபட்சம் 5
நிமிடம் முதல் 30 நிமிடம் வரை தியான நிலையில் இருந்தாலே போதுமானது. முதலில் நாம் நின்ற
நிலையில் தியான பயிற்சியை மேற்கொண்டதால் ஏற்பட்ட சுவாசத்தின் விரைவு, உட்கார்ந்த
நிலை தியானத்தின் போது மென்மையாகும். இதனால் நமது உடலிலுள்ள 72,000 நாடி நரம்்புகளுக்கு
தேவையான பிராண சக்தியை நமது அணுக்கள் பெற்று விடுவதால் நமது உடலெங்கும் சுவாசக் காற்றின்
குளுமை பரவுவதை நாம் உணரலாம். </p>

<p>இந்த பிராணசக்தி எந்த
அளவிற்கு நமது உடலில் நிறைந்திருக்கின்றதோ அதன் அளவிற்கேற்ப நமது ஆரோக்கியமும்,
ஆயுளும் தீர் மானிக்கப்படுகிறது. நின்ற நிலையில் கண்களை மூடியவாறு கிழக்கு முகமாக நின்று
வழிபடுவதை இயற்கை வழிப்பாட்டு முறை அல்லது சூரிய வழிப்பாட்டு முறை எனக்கூறலாம். இந்த
வழிப்பாடு முறையெல்லாம் அன்றைய கால குருகுல கல்வி முறையில் போதிக்கப்பட்டதாகும்.
கர்ப்பிணி பெண்கள் இப்பயிற்சியினை மேற்கொண்டால், அவர்களுக்கு எளிய சுகப் பிரசவம்தான்
உருவாகும். அதுமட்டுமல்ல, கருவில் வளர்கின்ற குழந்தைக்கும் தியானம் என்கிற உரு ஏற்படுவதால்,
31/2 வயதில் ஞானமடைந்து திருஞான
சம்பந்தரைபோல, ஏதேனும் ஒரு செயலில் தனது அறிவுப் புலமையை வௌிப்படுத்தி சமுதாயத்தில்
பிரகாசிக்ககூடும் என்பது உறுதி. </p>

<p>வெட்ட வௌியில்...</p>

<p>பிரபஞ்சம் என்கிற
திறந்தவௌி அல்லது வெட்டவௌியில்தான் பிரபஞ்ச மின் கதிர்களும், பிராண சக்திகளும்
நிறைந்துள்ளன. விலங்கினங்கள் எல்லாம் வெட்டவௌியில்தான் வாழ்ந்துக் கொண்டிருக்கின்றன.
வெட்டவௌி தன்னை மெய்யென்று இருப்போர்க்கு பட்டயம் ஏதுக்கடி - குதம்பாய் பட்டயம்
ஏதுக்கடி? என்று குதம்பைச் சித்தர் பாடல் ஒன்று கூறுகிறது. ஏன்? நமது மண்ணிலுள்ள ஏழை,
எளியவர்கள்கூட, சுத்தமும், சுகாதாரமும் போதிய உணவும் இன்றி வெட்டவௌியில்தான்
இன்றைக்கும் வாழ்ந்து கொண்டிருக்கிறார்கள். அதனால்தான் நீரிழிவு, ரத்தகொதிப்பு,
மாரடைப்பு போன்றவைகள் பெரும்பாலும் அவர்களை தாக்குவதில்லை. ஏனென்றால் இவைகள் எல்லாம்
நோய் கிருமி களால் வருகின்ற வியாதி அல்ல என்பதனை நாம் புரிந்து கொள்ள வேண்டும். வசதி
படைத்தவர்கள் சுத்தத்துடனும், சுகாதாரமுள்ள உணவுகளையும் வைட்டமின் மருந்து மாத்திரை
களையும் உட்கொண்டும் கூட, நாற்பத்தைந்து வயதிற்குள்ளாக நீரிழிவு, ரத்தகொதிப்பு,
மாரடைப்பு போன்றவைகளால் பாதிக்கப்பட்டு மாண்டு விடுகிறார்கள் ஏன்? </p>

<p>அவர்கள் வாழ்வது
பிரபஞ்ச காற்று புகாத குளிர்சாதனம் பொருத்தப்பட்ட கண்ணாடி அறைக்குள். ஆதலால்
போதிய அளவு பிராண சக்தியை அவர்களால் பெற இயலாமல் போய் விடுகிறது. அதனால்தான் ஏழை,
எளியவர்கள் என்ற பாகுபாடின்றி, இயற்கையான காற்றினை இலவசமாக பெறுவதற்கு, இயற்கையன்னை
வழிவகை செய்துள்ளாள். ஆக, இயற்கையான பிராணசக்தியை பெறுவதற்கு, சிறந்த வழிமுறை தான்
பிரம்ம முகூர்த்த தியான பயிற்சி ஆகும். </p>

<p>விலை கொடுத்து வாங்கும்
ஏசி காற்று மனித உயிரை பறிக்கிறது. ஓசியில் கிடைக்கின்ற ஓசோன் கலந்த பிராணக் காற்று
மனித உயிரை பலப்படுத்துகின்றது என்பதனை நாம் புரிந்து கொண்டு பிரம்ம முகூர்த்த தியான
பயிற்சியினை மேற்கொள்வோமாக. </p>

<p>எல்.நாச்சியார்
அம்மாள், </p><p>
44, பெரிய தட்டார்குடி தெரு,
</p><p>
திருநெல்வேலி - 1.
</p>

<p> </p>

<p> </p>

<p> </p>

<p>சென்னை,
ஆக.3-தமிழகம் மற்றும் புதுவையில் கடலோர பகுதிகளில் பரவலாக மழை பெய்யும். அடுத்த இரு
தினங்கள் வரை இந்த நிலை நீடிக்கும். </p>

<p>சென்னை:- சென்னை மற்றும்
அதன் சுற்றுப்புங்களில் வானம் பரவலாக மேக மூட்டத்துடன் காணப்படும்.மாலையிலோ இரவிலோ
மழை தூறல் இருக்கலாம். </p>

<p>வெப்பநிலை:-
நுங்கம்பாக்கம்:- அதிக அளவு 34.5 டிகிரி செல்சியஸ், குறைந்த அளவு 25.6 டிகிரி
செல்சியஸ், ஈரப்பதம் 80 சதவீதம். </p>

<p>மீனம்பாக்கம்:- அதிக
அளவு 34.0 டிகிரி செல்சியஸ், குறைந்த அளவு 24.8 டிகிரி செல்சியஸ், ஈரப்பதம் 82
சதவீதம் </p>

<p>திருநள்ளாறின் சிறப்பு</p>

<p> நவகிரகங்களில்
மிகச் சக்தி வாய்ந்தது சனி. இவருக்கு பல கோவிலக்ள் தனிச் சன்னிதானம் அமைத்து
வழிபட்டாலும் திருநள்ளாறு பெரும் சிறப்பை பெற்றுள்ளது. இந்த தியான நிலை சனி பகவானுக்கு
வரலாறு உண்டு. விதர்ப்ப நாட்டு மன்னன் தனது மகன் தமயந்திக்கு திருமண ஏற்பாட்டைச் செய்ய,
சுயம்வரம் நடத்த இளவரசர்களுக்கு அழைப்பு விடுத்தார். தமயந்தியை மணக்க மன்னர்களும்,
தேவர்களும் ஆசை கொண்டனர். நவக்கிரகங்களும் ஆசை கொண்டது. தமயந்தி நளனை விரும்புவதை
நவக்கிரக நாயகர்கள் அறிந்திருந்தால் அவர்கள் அனைவரும் நளனைப் போல வேடமணிந்து சுயம்
வரத்திற்கு வந்திருந்தனர். தமயந்தி மாலையுடன் வந்தபோது அனைவரும் நளனைப் போல
இருப்பதைக் கண்டு அதிர்ச்சியடைந்தாள். இருப்பினும் தேவர்கள், நவக்கிரக நாயகர்களின்
காவல்கள் தரையில் படாமல் இருப்பதையும், நளன் கால்கள் தரையில் ஊன்றி இருப்பதையும்
கண்டு நாளனுக்கு மாலை அணிவித்து நளனை மணந்தாள். </p>

<p> பெரிதும் ஆத்திரம்
அடைந்த சனிபகவான் நளனை ஆட்டிப் படைக்க எண்ணி, ஒரு முறை நளன் கால் கழுவும்போது காலின்
பின்பகுதியில் தண்ணீர் படாமல் போகவே அதன் வழியே சனி புகுந்து கொண்டார். சனி பிடித்த
நாளே நளன் தனது நாடட்டை சூதாட்டத்தில் இழந்து, மனைவிு மக்களுடன் காட்டுக்கு சென்று
அவர்களை அனாதையாக்கி விட்டு ஓடி விட்டான். தீயில் சிக்கிய கார்கோடகன் எனும் பாம்பை
காப்பாற்றும்போது அது தீண்டி கரிய நிறம் அடைகிறான். இப்படி பல்வேறு இன்னல்கள்
அடைந்தான். இறுதியில் திருநள்ளாறு வந்து தீர்த்தத்தில் குளித்து இறைவனை வணங்கிய பின்னர்
சனி அவனை விட்டு விலகி மனைவி குழந்தைகளுடன் இணைந்ததாக வரலாறு கூறுகிறது. </p>

<p> சனி பகவான் ஒருவரை
ஏழரை ஆண்டு பிடிப்பார். அந்த ஏழரை ஆண்டுகாலமும் கொடிய பலன்களை கொடுக்க மாட்டார். ஒரு
குறிப்பிட்ட காலத்தில்தான் சனியின் ஆட்டம் அதிகம் இருக்கும். ஆனால் நளனைப் பொருத்தவரை
ஏழரை ஆண்டு காலமும் கொடிய பலனை சனி கொடுத்தார். இதற்காக சனி பகவான் வருந்தி தவறுக்கு
பரிகாரம் தேடி சிவனை நோக்கி திருநள்ளாறில் தவக்கோலத்தில் இருந்தாராம். இங்குள்ள
பிள்ளையார் மற்றும் சிவனிடம தமது குறைகளை எடுத்துக் கூறினால் அவர்கள் மூலம் சனி பகவான்
நமக்கு அருள் பாவித்து துன்பங்களை விலக்குவார் என்பது ஐதீகம். </p>

<p> கே.ரவிக்குமார்,
சென்னை-53. </p>

<p> </p>






</body></text></cesDoc>