<cesDoc id="tam-w-dinakaran-other-01-06-09" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-01-06-09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-06-09</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-06-09</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>www.ifc.org/</p>

<p>உலக அளவில் வறுமையின் கொடுமையை குறைத்திடவும், மக்களின்
வாழ்க்கை தரத்தை உயர்த்திடவும் லட்சிய வேட்கையுடன் செயல்படும் தளம் இது. அதற்காக
வளரும் நாடுகளில் தனியார் துறையில் முதலீடுகளை ஊக்குவிக்க வழிவகை செய்கிறது. இந்த தளம்
ஐ.எஃப்.சி. எனும் இன்டர்நேஷனல் ஃபைனான்ஸ் கார்ப்பரேஷன் அமைப்பு இந்த முதலீடு
ஊக்குவிப்பை முனைப்புடன் செயல்படுத்துகிறது. உலக வங்கி குழுவில் இந்த அமைப்பு ஒரு
அங்கத்தினராக உள்ளது. 1956-ல் இது நிறுவப்பட்டது. வளரும் நாடுகளில் தனியார் முதலீட்டு
நிறுவனங்களும் கடன் வழங்குதல், ஈகுவிட்டி ஃபைனான்ஸ் உதவி போன்ற செயல்பாடுகளில் உலகின்
மிகப் பெரிய நிறுவனமாக இது விளங்குகிறது. மற்றும் தொழில் நிறுவனங்களுக்கும் நாடுகளின்
அரசாங்கங்களுக்கு தொழில்நுட்ப உதவி மற்றும் ஆலோசனைகளையும் வழங்கி வருகிறது. </p>

<p> </p>

<p>கோபுராபுரம்
சனிபகவான்</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>நவக்கிரக நாயகர்களில் ஒரு ராசியில்் அதிக நாட்கள் அதாவது இரண்டரை ஆண்டுகள்
ஆட்சி செய்யக்கூடிய பேரும், புகழும் பெற்றவர் சனி பகவான் ஆவார். சனி என்றாலே
தெய்வங்கள் முதல் மானிடர்கள்வரை அலறி அடித்துக்கொண்டு ஓடுவார்கள். ஆனால் எப்பொழுதும்
கெடுதலே செய்யாதவர், தனக்கென தனிச் சிறப்பும், ஈஸ்வரன் என்று போற்றக்கூடியவருமான
சனி பகவான் குடி கொண்டிருக்கும் தலம்தான் கோபுராபுரம். </p>

<p>    திருமுதுகுன்றத்திலிருந்து (விருத்தா சலம்) ஆலடி போகும்
வழியில் அமைந் துள்ள கோபுராபுரம் என்ற கிராமத்தில் ஆதிபரமேஸ்வரன் கோயில் உள்ளது.
அங்கு தனி சனீஸ்வரன் காக்கை வாகனத்தில் ஆதி பரமேஸ்வரனை பூஜித்து வணங்கும் காட்சியை
காணமுடி கிறது. அந்தக் கோயிலில்தான் சனி பகவான் பரமேஸ்வரனை பூஜித்து அருள் பெற்றதாக
வரலாறு. சனீஸ்வர பகவான் அருளுக்கு புகழ் பெற்ற திருநள்ளாரைப் போலவே சிறப்பு இந்த
திருத்தலத்திற்கும் உண்டு. சனீஸ்வரனுக்கு மிகுந்த, உகந்த கோயில் இந்த கோபுராபுரம்.
கோரிக்கை எதுவாக இருந்தாலும் இங்கு வந்து வேண்டிக் கொண்டால் சிறப்பாக மங்களகரமாக
வெற்றிக் கிட்டும் இது உண்மையும்கூட </p>

<p>    சனி பகவானை வணங்கும்போது: </p>

<p>சங்கடம்
தீர்க்கும் சனிபகவானே </p><p>
மங்களம் பொங்க மனம்
வைத்தருள்வாய் </p><p>
சச்சரவின்றி சனிச்சர தேவே
</p><p>
இச்சகம் வாழ இன்னருள் தா தா
</p>

<p>    என்று வணங்கி தொழும்போது சகலவிதமான கஷ்டங்களும் நீங்கி
வாழ்க்கையில் சுபிட்சம் உண்டாகும். </p>

<p>    எல்லா மனிதர்களையும் சனி பகவான் ஆட்டிப் படைப்பான்
என்கின்ற எண்ணம் அனைவருக்கும் உள்ளது. கோபுராபுரம் சனி பகவான் அப்படிப்பட்டவர் அல்ல.
அடுத்து காரைக்கால் அருகில் உள்ள திருநள்ளார் சனீஸ்வர பகவானுக்கு சனி திசை உள்ளவர்கள்
மட்டுமே செல்ல வேண்டும், மற்றவர்கள் செல்லக் கூடாது என்கின்ற எண்ணமும் பலருக்கு இன்றும்
இருந்து வருகின்றது. ஆனால் கோபுராபுரம் சனி ஈஸ்வரன் ஆலயத்திற்கு சனி திசை நடப்பவர்கள்
மட்டுமல்ல, யார் வேண்டுமானாலும் வந்து வணங்கி அருள் பெறலாம் என்பதிலிருந்து கோபுராபுரம்
சனி பகவானின் பெரும் அருளை புரிந்து கொள்ளுங்கள். சனி பிடித்தாலும், சளி பிடித்தாலும்
விடாது என்பதை கோபுராபுரம் சனி பகவான் பொய்யாக்கிடுவார். </p>

<p> </p>






</body></text></cesDoc>