<cesDoc id="tam-w-dinakaran-other-01-07-07" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-01-07-07.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-07-07</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-07-07</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>www.lastplace.com/</p>

<p>  </p>

<p>ஹேரம் ஆஃப் தி ட்ரூவி விர்ச்சுவல் வெப் ஆர்ட் மியூசியம் என்ற
இந்த தளம் பற்றி சொல்லப்படுகிறது. 1997 மே 24-ல் இந்த தளம் தொடங்கப்பட்டது.
சைபர்ஸ்பேஸ்.காம் என்ற வெப் தள மையத்தின் ஒரு அங்கமாக செயல்படுகிறது. இந்த தளம்.
இந்தியாவின் முதல் சர்வதேச டிஜிட்டல் ஆர்ட் எக்சிபிஷனின் இணை- ஸ்பான்சராக செயல்படுவதை
மிகப் பெருமையாக கருதுகிறது இந்த தளம் என குறிப்பிடப்படுகிறது. உங்களால் கற்பனை செய்ய
முடிந்த எல்லாமும் உண்மையே என்ற உலகப் புகழ் பெற்ற ஓவியர் பிகாசோவின் பொன்மொழியை
எடுத்துக் காட்டி நம்மை வரவேற்கிறது இந்த தளம் சைபர் ஆர்ட்டிஸ்ட்ஸ் தங்களது
கருத்துக்களையும், கற்பனையையும் தாங்கள் வாழும் உலகைப் பார்த்தே உருவாக்குகிறார்கள்
என்ற விளக்கம் தரப்படுகிறது.  </p>

<p> </p>

<p>வள்ளலாரின்
அருள்மொழிகள்</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>ஆண்-பெண் என்ற பால் வேற்றுமையின்றி, மக்கள் எல்லோருக்குமே கல்வி அளிக்க
வேண்டும். குறிப்பாக ஞான மார்க்கக் கல்வியை பெண்களுக்கு அளிக்க வேண்டும்.
</p>

<p>மண்ணுயிர்கள் அனைத்தும் மகிழ்ச்சி அடையும் வகையில் நாம் பணிபுரிய வேண்டும்.
மற்ற உயிர்களின் பண்பான மகிழ்ச்சியைக் கண்டு நாம் மகிழ வேண்டும். </p>

<p>நாம் பல ஜன்மங்களையும், தப்பி, மேலான இந்த மனிதப் பிறவி எடுத்தது சிவத்தின்
திருவருளைப் பெறுவதற்கே. </p>

<p>நாம் இறையருளை அடைதலுக்குத் தியானமே முக்கிய காரணமாக இருக்கிறது. ஆதலால் இடை
விடாமல் நல்ல மனதோடு தியானிக்க வேண்டும். </p>

<p>புலன் இச்சைக்காக வாழும் வாழ்க்கைதான் வாழ்வு என்று எண்ணிக் கொண்டு அதற்காக
விஞ்ஞானத்தைப் பயன்படுத்துவது புதுமை வாழ்வாக இருக்காது. </p>

<p>முதலும், முடிவும் இல்லாமல் இருப்பதே இயற்கை. </p>

<p>நம்மைச் சுற்றியிருக்கும் உயிர்களுக்கு உயிர் விளக்கும் பணியும், அறிவு
விளக்கப் பணியும் ஆற்றிவர வேண்டும். </p>

<p>நீ பெற்றிருக்கும் அனுபவத்தை உலகுக்குத் தெரிவித்து விடு. </p>

<p>எந்த வேலை செய்யினும், எந்த விவகாரஞ் செய்யினும் சிவ சிந்தனையோடு
செய்து பழகுதல் வேண்டும் </p>

<p>சாதி, மதம், சமயம் என்பது சங்கடம். இவை சாத்திரச் சேறு ஆகும். </p>

<p>அருட்பெருஞ்ஜோதியை சாதியும், மதமும் காண முடியாது. அவை பொய்; அவைகளை
விட்டு விட்டால்தான் அருட்பெருஞ்ஜோதி தோன்றும். </p>

<p>நாள்தோறும் ஆண்டவனை வணங்கி, வாழ்த்துதல் வேண்டும். இல்லையேல் உலகத்தின்
உயிர்களிடத்தில் அன்பும், அருளிரக்கமும் ஒரு சிறிதேனும் ஏற்படாது. </p>

<p>அறங்கள் செய்யாதிருப்பதும், அடுத்தாள் கரம் கூப்பி வணங்கும்போது, பதிலுக்குக் கை
கூப்பி வணங்காதிருப்பதும் கொடுமை. </p>

<p>பசியினால் அன்புற்று, வருந்துகின்ற ஏழைகளுக்கு ஆகாரம் கொடுத்து அவர்களின்
வருத்தத்தை மாற்றுவதே ஜீவ காருண்யம் ஆகும். </p>

<p>-ரா.பாலசுப்பிரமணியம்,
அவிநாசி. </p>

<p> </p>






</body></text></cesDoc>