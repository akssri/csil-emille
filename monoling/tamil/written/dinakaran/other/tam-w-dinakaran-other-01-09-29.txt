<cesDoc id="tam-w-dinakaran-other-01-09-29" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-01-09-29.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-09-29</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-09-29</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>பல்லி விழுந்த உணவு
விஷமாவதேன்?</p>

<p>பல்லி
விழுந்த உணவை சாப்பிட்டவர்கள் பலி என்பதை அடிக்கடி நீங்கள் பத்திரிகையில்
படித்திருக்கலாம். உணவில் பல்லி விழுந்தால் அந்த உணவு உடனே விஷமாகி விடும். பல்லியின்
டாக்சின் உணவில் கலப்பதால்தான் உணவு விஷமாகிறது. உணவில் பல்லி விழுவது மட்டுமல்ல,
பல்லியின் எச்சில் உணவில் விழுந்தாலும் உணவு விஷமாகிவிடும். பல்லி விழுந்த உணவை
சாப்பிட்டவர்களுக்கு முதலுதவியாக அவர்களை வாந்தி எடுக்க வைக்க வேண்டும். உடனே
மருத்துவமனைக்கு அழைத்துச் செல்ல வேண்டும். பல்லி விழுந்த உணவை சாப்பிட்டவர்களில் மயக்க
அல்லது வாந்தியெடுப்பவர்களுக்கு மட்டுமல்ல, அந்த உணவை சாப்பிட்ட அனைவருக்கும் சிகிச்சை தர
வேண்டும். </p>

<p>அர்ச்சனை பூக்கள்</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>இறைவனை வழிபடும்போது பூவும், நீரும் அடிப்படையாகக் கொண்டு இறைவனை
வழிபடுகின்றோம். இதைத்தான் திருமூலர், பூவோடு நீர் சுமந்்தேந்தி என்கிறார். அதுபோல
திருநாவுக்கரசரும், சலம், பூவோடு தூபம் மறந்தறியேன் என்கிறார்். மேலும், புண்ணியம்
செய்வோருக்கு பூவுண்டு, நீருண்டு என்று சான்றோர்கள் பகர்ந்தனர். </p>

<p>பூசைக்குரிய பூக்களை போற்றிப் பாதுகாக்கவேண்டும். உரிய பூக்களைக் கொண்டு
உரிய நேரத்தில் இறைவனை வழிபட வேண்டும். இறைவனுக்கு எட்டுவகை பூக்கள் பூசைக்கு மிக
உகந்்்்்தவை என ஆகமங்களில் கூறப்பட்டுள்ளன. புன்னை, செண்பகம், வௌ்ளருக்கு, நந்தியாவர்த்தம்,
பாதிரி, அலரி, நிலோற்பவம், செந்தா மரை ஆகிய எட்டு மலர்களும் பூசைக்குரிய
முக்கியப்பூக்கள் ஆகும். </p>

<p>இனி ஒவ்வொரு தெய்வங்களும் உரிய மலர்களைத் தெரிந்து கொள்வோம். </p>

<p>சிவபெருமான்: </p>

<p>வில்வம், கொன்றை, </p>

<p>விநாயகர்: </p>

<p>அருகம்புல், வௌ்ளருக்கு, எருக்கு, வில்வம், ஊமத்தை, நொச்சி, நாயுருவி,
கத்திரி, அலரி, மருது, மாதுளை, செண்பகம், பாதிரி, சூரியகாந்தி. </p>

<p>முருகன்: வெட்சி மலர், கடம்பூ </p>

<p>உமையவள்: நீலம்பூ, மல்லிகை, தாமரை. </p>

<p>திருமகள்: நெய்தல், செந்தாமரை </p>

<p>கலைமகள்: வெண்்்்தாமரை </p>

<p>- இதுபோன்று உக்கிரமான தெய்வங்களுக்கு செந்நிறப்பூக்கள் உகந்ததாகும். பொதுவாக
மணமற்ற பூக்களை இறைபூசையில் பயன்படுத்தக் கூடாது. </p>

<p>காலங்களும், பூக்களும்: </p>

<p>சாமந்தி, கொன்றை, செண்பகம் முதலிய மஞ்சள்்் நிறப் பூக்கள் விடியற்கால
பூசைக்குரியன. செந்தாமரை, செவ்வரளி, பாதிரி முதலிய சிவப்பு நிறப் பூக்கள் பகற்பொழுது
பூசைக்குரியன. மல்லிகை, முல் லை, தும்பை, வௌ்ளருக்கு முதலிய பூக்கள் மாலை, யாம கால
பூசைக்குரியன. நீலோற்பல மலரைத் தவிர மற்ற நீல நிறப் பூக்களை பூசைக்குப் பயன்படுத்தக்
கூடாது. </p>

<p> </p>






</body></text></cesDoc>