<cesDoc id="tam-w-dinakaran-other-00-09-07" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-00-09-07.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-09-07</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-09-07</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ஒலிம்பிக், அன்று எப்படி நடந்தது?</p>

<p>   
உலகத்தின் அத்தனை பிரிவு மக்களின் கவனத்தையும் 15 நாட்களுக்கு தன்னிடத்தில்
நிறுத்திவைத்திருக்கும் ஒலிம்பிக் போட்டிகள், ஆஸ்திரேலியாவின் சிட்னி நகரில்
செப்டம்பர் 15 முதல் நடைபெறப்போகிறது. தற்போது நடை பெறப்போகும் ஒலிம்பிக் போட்டிகளை
தொலைக் காட்சி வாயிலாக நாம் காணலாம். ஆனால் பண்டைய காலத்தில் எப்படி ஒலிம்பிக்
போட்டி கள் நடைபெற்றது. </p>

<p>ஒலிம்பிக் எப்படி தோன்றியது?</p>

<p>   
கிரேக்கர்கள் பல்வேறு ராஜ்யத்தினராக பிரிந்திருந்த போதிலும், அனைவரையும் தாங்கள்
கிரேக்கர்கள் என்று அவ்வப்பொழுது உணரும் படி செய்து வந்தவை டெல்பிகோவில் வழிபாடு தான்.
அந்த வழிபாட்டு விழாவில் ஒரு இறைச்சடங் காக கடைப்பிடிக்கப்பட்ட விளையாட்டுவிழா தான்
ஒலிம்பிக் கிரேக்க நாட்டில் பெலொப் பொனேசியாவின் வடமேற்கிலுள்ள ஒலிம்பியா என்ற
இடத்தில் இந்த விளையாட்டு போட்டி கி.மு. 776-ம் ஆண்டு தொடங்கி, நான்கு வருடத்திற்கு ஒரு
முறை நடைபெற்று வந்தது. இந்த நான்கு வருடகாலத்தை ஒலிம்பியாட் என் றார்கள். எந்த ஒரு
சம்பவத்தை குறிப்பிட்டு சொல்ல வேண்டு மானாலும், இத்தனையாவது ஒலிம்பியாட்டின் இத்தனையாவது
வருடத்தில் என்றுதான் கிரேக்கத்தில் சொல்வார்களாம். ஒலிம்பிக் விளையாட்டு போட்டியை
விளை யாட்டு விழா என்றே புனிதமாக கருதினார்கள் கிரேக்கர்கள். கிரிஸின் எல்லா
ராஜ்யங்களும் இந்தவிழாவில் வெகு உற்சாகத்துடன் கலந்து கொண்டன. பெரிய ராஜ்யமென்றும்,
சிறிய ராஜ்யமென்றும் வித்தியாசம் கிடையாது. ஆனால் கிரேக்கர்கள் மட்டுமே இந்த விழாவில்
கலந்து கொள்ளும் உரிமை இருந்தது. கிரேக்க ரல்லாத அந்நியர், அடிமைகள் ஆகியோர் இதில்
கலந்து கொள்ள முடியாது. பெண்களுக்கும் அனுமதியில்லை. </p>

<p>   
ஒலிம்பிக் விழாவை முன்னிட்டு, கிரிஸ் முழுவதும் அமைதி நிலவியிருக்க வேண்டுமென்ற நியதியை
கண்டிப்பாக கிரேக்கர்கள் கடைப்பிடித்தார்கள். ஒரு ராஜ்யமும் மற்றொரு ராஜ்யமும் பகைமை
கொண்டிருந்தால், தற்காலிகமாக அந்தப்பகைமையை மறந்துவிட்டு இரண்டு ராஜ்யங்களும்
சந்தோஷத்துடன் ஒலிம்பிக் விழாவில் கலந்துகொண்டன. போர் நடைபெற்றுக்கொண்டிருந்தால்
இந்த விழா காலத்தல் அந்த போர் நிறுத்தப்பட்டுவிடும். இரண்டு ராஜ்யங்களும் உற்சாகமாக
விழாவில் பங்கு கொள்ளும் ஒலிம்பிக் விளையாட்டு விழா தொடங்குவதற்கு பத்து நாட்களுக்கு முன்
பிருந்தே மக்கள் ஒலிம்பியாவில் கூட ஆரம்பித்து விடுவார்களாம். வௌிநாடுகளிலிருந்து வியா
பாரிகளும் தங்கள் சரக்குகளை கொண்டு வந்து கொட்டுவார்கள். உள்ளூர் வியாபாரிகளும்
சரக்குகளை கொண்டு வந்து குவிப்பார்கள். பல்துறை நிபுணர்களும் தங்கள் திறமையை வௌிப்படுத்த
இங்கே வருவதுண்டாம். </p>

<p>ஒலிம்பிக் போட்டி எப்படி நடந்தது?</p>

<p>   
ஒலிம்பிக் விளையாட்டு போட்டி நடைபெறப் போகும் முதல் நாள் போட்டியில் கலந்து
கொள்ளப்போகிற ஒவ்வொருராஜ்யத்தை சேர்ந்த வர்களும் அழகிய உடைகளை அணிந்து ரதங் களில்
ஊர்வலமாக வந்தார்கள். எல்லோரும் வந்து சேர்ந்ததும் ஜியஸ் தெய்வத்திற்கு பூஜை
செய்வார்கள். பூஜை முடிந்ததும் போட்டியில் கலந்து கொள்ள வந்திருப்போர் அனைவரும்
போட்டியில் கலந்து கொள்ள உரிமையுடையவர் கள்தானா என பரிசோதிக்கப்படுவார்கள்.
அதன்பின் தாங்கள் கிரேக்கர்கள் என்றும் விளையாட்டு விதிகளை அனுசரிப்பதாகவும்
உறுதிமொழி எடுத்துக்கொள்வார்கள் இதற் கிடையே தாங்கள் போதிய பயிற்சி பெற்றிருப் பதாக
நிரூபிக்க வேண்டும். இவையெல்லாம் முடிய முதல் நாள் பூராவும் ஆகிவிடும். அடுத்த மூன்று
நாட்களில் போட்டிகள் நடைபெறும். முதன் முதலாக ஓட்டம் மட்டும் இருந்தது. அதன் பின்தான்
தாண்டுதல், மல்யுத்தம், குத்துச் சண்டை, ஈட்டிஎறிதல் என்று பல்வகை விளை யாட்டு
போட்டிகள் நடைபெறும். ஒலிம்பிக் போட்டியின் இறுதியில்தான் ரதங்களின் பந்தயம்
நடைபெறும். </p>

<p>வெற்றி பெற்றவர்களுக்கு என்ன பரிசு?</p>

<p>   
ஒலிம்பிக் விளையாட்டுகள் எல்லாம் முடிந்ததும் கடைசிநாள் வெற்றி பெற்றவர்களுக்கு பரிசு
வழங்குவார்கள். பரிசு என்ன தெரியுமா ஆலிவ் மரத்து இலைக் கொத்துகளினால் செய்யப்பட்ட
கிரீடம். பரிசு வழங்கும் தினத்தன்று அதிக மக்கள் கூடுவார்கள். பரிசுக் குரியவர்களை
பார்க்கவேண்டும் என்ற ஆர்வத்தால்தான், விழாவில் விழா சம்பந்தபட்ட அதிகாரி இன்னார்
இன்ன விளையாட்டில் பரிசு பெற்றார் என்று அவரு டைய பெயர், தந்தை பெயர், ஊரின் பெயர்
எல்லாவற்றையும் உரக்கச்சொல்லி அவரை பரிசு பெற்றுக்கொள்ளுமாறு அழைப் பான். பரிசு
பெறுகிறவள் முன்னே வரும்போது கூட்டத்தினர் கரகோஷம் செய்து தங்கள் சந்தோ ஷத்தையும்
பாராட்டுதலையும் தெரிவிப்பார்கள். </p>

<p>வீரர்களுக்கு சொந்த ஊரில் கிடைக்கும்
மரியாதை</p>

<p>  </p><p>
    வெற்றி
அடைந்தவர்களை ஊதா வர்ண உடை அணிவித்து நான்கு குதிரைகள் பூட்டிய ரதத்தில் உட்கார வைத்து,
ஊர்வலமாக சொந்த ஊருக்கு அழைத்து வருவார்கள். ரதத்திற்கு பின்னால் வெற்றி வீரனின்
உற்றார், உறவினர், ஊரார் ஆகிய அனைவரும் ஆடிக்கொண்டும், பாடிக் கொண்டும் வருவார்கள்.
நகர எல்லை அவர் வந்ததும் ஊர்வலம் நின்றுவிடும். ஊரின் மதிற் சுவரின் ஒரு சிறு பாகத்தை
இடித்து ரதம் செல்வதற்கு வழி செய்வார்கள். இந்த வழியாக ரதம் ஊருக்குள் பிரவேசிக்கும்.
வெற்றி வீரர் ஊர்வந்து சேர்ந்ததும் வெற்றி அடைந்தவனுடன் உறவினருக்கும் விருந்துகள்
நடைபெறும், ஊர் மக்கள் தங்களால் இயன்ற வரை பொன் னும், வௌ்ளியையும் பரிசுகளாக
அளிப்பீர். ஆயுள்பூரா வும் அவனுக்கு மரியாதை செய்யப்ப டும், ஊர் திருவிழாவில்
நாடகங்கள் நடைபெறு கிறபோதும் அவனுக்குதான் முதல் மரியாதை. வரி செலுத்து வதிலிருந்து அவனுக்கு
விலக்கு அளிக்கப்படும். அவனுடைய உருவச்சிலையை ராஜ்யத்தின் மையத்தில் நிறுவுவார்கள்.
</p>

<p>தடை செய்யப்பட்டது</p>

<p>   
ஒலிம்பிக் விழாவில் விளையாட்டு போட்டி மட்டும் நடைபெறாது. கவிஞர்கள் தங்கள்
கவிதைகளை பாடிமகிழ்வார்கள். நாடக ஆரியர்கள் தங்கள் இயற்றிய நாடகங்களை
அரங்கேற்றுவார்கள், மேடை பேச்சுகள் நடைபெறும் இவையெல்லாம் வந்தோர்களை மகிழ்விக்கவும்
தங்களுடைய திறமைகளை வௌி உலகிற்கு தெரியப்படுத்தவும்தான். இப்படி கோலகலமாக நடைபெற்று
வந்த ஒலிம்பிக் போட்டிகள் கி.மு.776-ம் ஆண்டு முதல் கி.பி.394-ம் ஆண்டு வரை 1200
வருடங்கள் தொடர்ந்து நடைபெற்று வந்தது. என்னகாரணத்தினாலோ, தியோடோசியஸ் என்றும்
மன்னனால் இந்த ஒலிம்பிக் விழா தடைசெய்யப்பட்டது. சுமார் 1500 ஆண்டுகாலம் நடக்காமல்
இருந்த இந்த ஒலிம்பிக் போட்டியை 1896-ல் மீண்டும் கொண்டு வந்தவர் பிரெஞ்சு
நாட்டை சேர்ந்த கொபர்ட்டீன் பிரபு அதன்பின் தொடர்ந்து நடைபெற்றுவரும் நவீன ஒலிம்பிக்
உலகப்போரின் காரணமாக 1916 மற்றும் 1940 மற்றும் 1944 ஆண்டுகளில் மட்டும் நடைபெற
வில்லை. </p><p>
  </p>

<p>கோவீ.ராஜேந்திரன்
பி.காம். டி.எம்.இ, </p><p>
13. தி.காலனி, பசுமலை,
</p><p>
மதுரை -625 004.
</p>

<p> </p>

<p>சென்னை,
செப். 7- தமிழ்நாட்டில் பல இடங்களில் மிதமான மழை பெய்தது. மழை அளவு சென்டி மீட்டரில்
வருமாறு:- </p>

<p>மதுரை 4, சிதம்பரம்,
திருச்செங்கோடு, திண்டிவனம் 3, விழுப்புரம், செங்கம் 2. </p>

<p>முன்அறிவிப்பு:- தமிழகம்
மற்றும் புதுவையில் கடலோர பகுதிகளில் பரவலாக மழை பெய்யும். அடுத்த இரு தினங்கள் வரை
இந்த நிலை நீடிக்கும். </p>

<p>சென்னை:- சென்னை மற்றும்
அதன் சுற்றுப்புறங்களில் வானம் மேகமூட்டத்துடன் காணப்படும். இன்று மாலை அல்லது இரவு லேசாக
மழைபெய்யும் </p>

<p>வெப்பநிலை:-
நுங்கம்பாக்கம்:- அதிக அளவு 36 டிகிரி செல்சியஸ், குறைந்த அளவு 23.1டிகிரி செல்சியஸ்,
ஈரப்பதம் 79சதவீதம். </p>

<p>மீனம்பாக்கம்:-அதிக
அளவு 35.4 டிகிரி செல்சியஸ், குறைந்த அளவு 24.1டிகிரி செல்சியஸ், ஈரப்பதம் 80
சதவீதம். </p>

<p>www.unesco.org</p>

<p> </p>

<p>   
உலக நாடுகள் ஒரே கூரையின் கீழ் கொண்டு வரும் உலகப் பொது அமைப்பு ஐக்கிய நாடுகள் சபை
இதன் ஒரு முக்கிய அங்கம் ஐக்கிய நாடுகள் கல்வி, விஞ்ஞானம் மற்றும் கலாச்சார
இயக்கமான யுனெஸ்கோ அமைப்பு. </p>

<p>    உலக
மக்கள் அனைவரையும் ஒருங்கிணைத்து, அவர்களின் ஒட்டுமொத்த கலாச்சார பரிமாற்றங்களுக்கும்,
கல்வித் தொடர்புகளுக்கும், விஞ்ஞான ஒத்துழைப்புகளுக்கும் மிகப் பெரிய பாலமாக
அமைந்திருப்பது யுனெஸ்கோ. இது இயங்கும் விதம், உயரிய நோக்கம், அளப்பெரிய தொண்டுகள்,
செயற்கரிய சேவைகள், உலக குழந்தைகள் மீது காட்டும் கரிசனம், அறிவியல், சமூக வளர்ச்சிக்கு
இட்டுச் செல்லும் பணிகள் அதி நவீன விஞ்ஞான வளர்ச்சி பயன்களை அகிலமெங்கும்
கொண்டு சேர்க்கும் ஆக்கப்பூர்வ பரிவர்த்தனைகள்- இவை அனைத்தையும் அழகுற விளக்கும்
அற்புதமான தளம் இது. </p>

<p> </p>

<p>பழநி</p>

<p> </p>

<p>   
தி்ருமுருகனின் மூன்றாவது படைவீடுதான் பழனியாகும். திருவாவினன்குடி 
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>என்கின்ற திருப்பெயரை கொண்டிருக்கும்
இத்திருத்தலம் தமிழர்களின் வாழ்க்கையோடு நெருங்கிய தொடர்பு கொண்டதாகும். திருவாவினன்குடி
என்பதற்கு விளக்கம் உள்ளது. திரு என்கின்ற திருமகளும், ஆ என்ற காமதேனுவும், இ என்ற சூரிய
பகவானும், கு என்ற தேவியும், டி என்ற தேவர்களும் கந்தவேல் பெருமானை நாமணக்க தொழுத
திருத்தலமாதலால் இதற்கு திருவாவினங்குடி என்ற பெயராயிற்று என்பது புராண விளக்கம். பொதினி
என்பது திருவாவினன்குடியின் பழைய சிறப்பு பெயர் என்று அகநானூற்றில் படிக்கும்போது நமக்கு
தெரிய வருகின்றது.் முருகப் பெருமானை பிரிந்த வருத்தத்தில் சிவபெருமானும், பார்வதி
தேவியும் தங்கள் அருள் மைந்தன் முருகனை பின்தொடர்ந்து திருவாவினன்குடி வந்து பழம் நீ என்று
முருகனுக்கு பெயர் சூட்டியதால் பழநி என்ற பெயர் வந்தது என்று கந்த புராணம் இயம்புகிறது.,
</p>

<p>   
இத்திருத்தலத்தில் இடும் பனை காவல் தெய்வமாக்கினார் முருகப் பெருமான். அன்று முதலே
திருமுருகப் பெருமானின் திருத்தலங்களில் காவடி எடுக் கும் வழக்கம் ஏற்பட்டது. திருவாவினன்குடி
ஆலயம் பழனி மலையின் அடிவாரத்தில் வையாபுரி ஏரிக்கரையில் உள்ளது. சிறிது தொலைவில்
சரவணப் பொய்கையும் காணப்படுகிறது. இங்கிருந்துதான் மக்கள் காவடி எடுத்து செல்வார்கள்.
</p>

<p>   
இத்திருத்தலத்தின் தலவிருட்சம் நெல்லிமரமாகும். பழநியில் குமரப்பெருமான் ஆண்டிக்
கோலத்தின் மூலமாக, ஆனந்தமயமான ஆண்டவனை அடைய ஆன்மாக்கள் முதலில் பற்றை ஒழிக்க
வேண்டும். பற்றை ஒழித்த நிலை நீடிக்க வேண்டுமானால் மனதை இறைவனிடத்தில் செலுத்த வேண்டும்
என்று நமக்கு விளக்குகிறார். </p>

<p>   
பழங்காலத்தில் வாழ்ந்த ஏழு வள்ளல்களில் ஒருவன் பேகன். மயிலுக்குப் போர்வை வழங்கிய
அந்த வள்ளலின் பெருந் தன்மையை சங்க நூல்கள் பாராட்டுகின்றன. அவனுடைய முழுப் பெயர் வையாவிக்
கோப்பெரும்பேகன் என்பதாகும்். ஆவியர் குலத்தில் பிறந்தவன் பேகன். இக்குலத்தினர் தலை
நகராக கொண்ட இடமே இந்த ஆவினன்்குடியாகும். இப்போதும் சித்திரை, கார்த்திகை
மாதங்களில் மிகப்பெரிய விழாக்கள் பழநியில் நடப்பதை பல லட்ச மக்கள்
கண்டுகளிக்கிறார்கள். </p>

<p>   
-கி.மனோகரன், பொள்ளாச்சி. </p>

<p> </p>






</body></text></cesDoc>