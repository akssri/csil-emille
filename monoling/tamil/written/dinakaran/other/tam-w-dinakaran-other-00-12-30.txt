<cesDoc id="tam-w-dinakaran-other-00-12-30" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-00-12-30.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-12-30</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-12-30</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>தமிழ்நாட்டில்
நேற்று பல இடங்களில் மழை பெய்தது. மழை அளவு சென்டி மீட்டரில் வருமாறு:- </p>

<p>உளுந்தூர்பேட்டை-14,
சீர்காழி-12, சிதம்பரம்-11, கோவில்பட்டி, முஷ்ணம்-10, கொடைக்கானல்-7, மயிலாடுதுறை,
உடுமலைபேட்டை, கடலூர்-6, தம்மம்பட்டி-5, பொள்ளாச்சி-4, விழுப்புரம், சங்கரன்கோவில்,
இரணியல், பெரம்பலூர், மணப்பாறை-3, கும்பகோணம், பாபநாசம், திருக்காட்டுப்பள்ளி,
செங்கோட்டை, செங்குன்றம், நடு வட்டம்-2, சென்னை, காஞ்சிபுரம், தாம்பரம், ஆரணி,
ஆம்பூர், மேலூர், திருமங்கலம், கோவை, ஊட்டி-1. </p>

<p>முன் அறிவிப்பு:-
தமிழ்நாட்டில் பெரும்பாலான பகுதிகளில் பலத்த மழை பெய்யக் கூடும். </p>

<p>சென்னை:- சென்னை மற்றும்
அதன் சுற்றுப்புறங்களில் வானம் மேக மூட்டத்துடன் காணப்படும். ஒரு சில இடங்களில் லேசான
மழை பெய்யலாம். </p>

<p>வெப்பநிலை:-
நுங்கம்பாக்கம்:- அதிக அளவு 84.9 டிகிரி செல்சியஸ். குறைந்த அளவு 72.7 டிகிரி
செல்சியஸ். ஈரப்பதம் 84 சதவீதம். </p>

<p>மீனம்பாக்கம்:- அதிக
அளவு 83.8 டிகிரி செல்சியஸ். குறைந்த அளவு 73.9 டிகிரி செல்சியஸ். ஈரப்பதம் 85
சதவீதம். </p>

<p>www.indianmusicsearch.com/home</p>

<p>இந்திய இசை
மற்றும் திரைப்படப் பாடல்களை கேட்டு மகிழ உதவும் தளம். மேலும் இந்திய இசை சம்பந்தமான
165 வெவ்வேறு தளங்களுடன் இணைப்பு வசதியும் செய்து தரப்பட்டுள்ளது. இந்தி, இசை, தமிழ்
இசை, தெலுங்கு இசை என இந்தியாவின் பல்வேறு மொழிகளிலும் உள்ள கிளாசிக்கல், கிராமிய
இசை, கர்நாடக இசை, பக்தி இசை, திரை இசை, மெல்லிசை, கஜல் போன்ற அனைத்து பிரிவு இசை
சம்பந்தப்பட்ட பிரத்தியேக தளம் இது. இசைப்பிரியர் களுக்கும், மாணவர்களுக்கும் மிகுந்த
பயன் தரும் தளம் என்பது இதன் சிறப்பு. </p>

<p>மார்கழித் திங்கள்</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>   
தனுர் மாதம் என்று சொல்லப்படும் இம்மாதத்தில் முப்பது நாட்களும் பாவையர் நோன்பு
நோற்பர். மாதங்களில் நான் மார்கழி என்று கிருஷ்ணபகவான் கீதையில் கூறுகிறார்.
வில்லிபுத்தூரில் விசேஷமான பூஜைகள் நடைபெறும் மாதம் இது. காரணம், பெரியாழ்வார்
கண்டெடுத்த பெண் பிள்ளையாகிய ஆண்டாள் பிறந்த ஊர். ஆண்டாள், தமிழை ஆண்டாள்.
அப்பிராட்டி அருளிச் செய்த திருப்பாவைப் பாசுரங்களை இந்த மார்கழி மாதத்தில்
விடியற்காலையில் பாடி ஒருநாள் செய்யும் பூஜை, ஒரு வருஷம் பூஜை செய்த பலனைக் கொடுக்கும்.
சைவர்களும், வைணவர் களும் கொண்டாடும் மாதம் இது. வீடு களின் வாசலில் நீர் தௌித்து,
அழகான கோலங்கள் இட்டு, நடுவில் பரங்கிப் பூக்களை வைத்து மங்கலமாகக் கொண் டாடுவர்.
வைணவக் கோயில்களில் இராப்பத்து, பகல்பத்து என்ற முறையில் நாலாயிர திவ்வியப்
பிரபந்தம் பாடப் படும். சிவாலயங்களில் மாணிக்கவாசகர் அருளிய திருவெம் பாவையும்,
திருப்பள்ளி யெழுச்சியும் பாடப்படும். இந்த மார்கழி மாதத்தில்தான் வைகுண்ட ஏகாதசியும்,
ஆருத்ரா தரிசனமும் வருகின்றன. ஆருத்ரா தரிசனத்திற்குமுன் பத்து நாட்கள் மாணிக்க வாசக சுவாமியின்
உற்சவம் சிவாலயங்களில் நடைபெறுவது வழக்கம். கிராமப்புறங்களிலும், நகரத்தின் வௌியே
புறநகர்ப் பகுதியிலும் விடியற்காலையில் பக்தர்கள் ஹார்மோனியத்தைக் கழுத்தில்
தொங்கவிட்டபடி, பஜனை பாடல்கள், திருப்பாவை, திருவெம்பாவை பாடல்கள் பாடி வருவதைக்
காணலாம். </p>

<p>   
இந்த மார்கழி மாதத்தை சிலர் பீடை மாதம் என்று சொல் வதுண்டு. இதற்கு காரணம் மிகுந்த
குளிர் இக்காலக்கட்டத்தில் அதிகம் இருப்பதனால் இப்படி சிலர் சொல்லியிருக்கலாம். ஆனால்
உண்மையில் கடவுளுக்கு என்றே தன்னை மக்கள் முழுமையாக அர்ப்பணிக்கின்ற மாதம் இதுதான்.
பெருமாள் கோவில்களில் எல்லாம் இம்மாதத்தில்் விமரிசையாக விழாக்கள் நடைபெறும். வைணவ
ஆலயங்களில் திருபள்ளி எழுச்சி என்கின்ற வழிபாட்டு முறை பிரசித்தி பெற்றதாகும். சிறு சிறு
குழந்தைகளுக்கு எல்லாம் இந்த மாதத்தில் திருப்பாவை திருவெம்பாவையில் வருகின்ற மார்கழி
திங்கள் மதி நிறைந்த நன்னாள்.. போன்ற பாடல்களை பாடி பக்தியுடன் திகழ பெரியவர்கள்
குழந்தைகளை பயிற்றுவிக்க வேண்டும். மொத்தத்தில் பக்தி மணம் கமழும் மாதம் இது என்றால்
மிகையில்லை. </p>

<p>   
-தி.தெய்வநாயகம், சாத்தான்குளம். </p>

<p> </p>






</body></text></cesDoc>