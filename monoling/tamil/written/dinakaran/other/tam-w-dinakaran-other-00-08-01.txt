<cesDoc id="tam-w-dinakaran-other-00-08-01" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-00-08-01.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-08-01</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-08-01</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சென்னை,
ஆக. 1- </p><p>
தமிழகத்தில் நேற்று
முன்தினம் ஒரு சில இடங்களில் மழை பெய்தது. மழை அளவு சென்டி மீட்டரில் வருமாறு:-
</p>

<p>கீரனூர், இலுப்பூர், தலா
6 செ.மீ. திருப்பத்தூர், மணப்பாறை, பெருங்களூர் தலா 5 செ.மீ. பெரும்புதூர், திருச்சி,
உதக மண்டலம், சிட்டம்பட்டி தலா 4 செ.மீ. சேலம் பெரம்பலூர், மேட்டுப் பட்டி தலா 3
செ.மீ.தாம்பரம், ஆம்பக்ஷர், விழுப்பும் தைலா 2 செ.மீ.போளூர், பாலக்கோடு, செங்கம்,
ஜெயங்கொண்டம் தலா 1 செ.மீ. </p>

<p>முன் அறிவிப்பு:-
தமிழகம், புதுவையில் சில இடங்களில் மழை அல்லது இடியுடன் கூடிய மழை பெய்யும். அடுத்த இரு
தினங்கள் வரை இந்த நிலை நீடிக்கும். </p>

<p>சென்னை:- சென்னை மற்றும்
அதன் சுற்றுப்புறங்களில் வானம் பரவலாக மேகமூட்டத்துடன்் காணப்படும் மாலையிலோ இரவிலோ
மழை தூறல் இருக்கலாம். . </p>

<p>வெப்பநிலை:-
நுங்கம்பாக்கம்: அதிக அளவு 34.8 டிகிரி செல்சியஸ். குறைந்த அளவு 26. 1 டிகிரி
செல்சியஸ். ஈரப்பதம் 82 சதவீதம். </p>

<p>மீனம்பாக்கம்:-அதிக
அளவு 34.2 டிகிரி செல்சியஸ். குறைந்த அளவு 25.6 டிகிரி செல்சியஸ். ஈரப்பதம ்82
சதவீதம். </p>

<p> </p>

<p> </p>

<p>ஒளிரும் பூச்சிகள்</p>

<p> மின்மினிப்
பூச்சிகளின் உடலில் இருந்து வெட்டும் ஒளியானது மற்ற ஒளியைப் போன்றதுதான், ஆனால்
இவ்வொளி சூடு இல்லாத ஒன்று. இந்த வகை ௌிச்சம் லுமினஸென்ஸ் வகையாகும். </p>

<p> மின்மினிப்
பூச்சிகளின் இந்த வௌிச்சம் லூஸிப்பெரின், லுஸிஃப்ரேஸ் என்கிற கெமிக்கல்களால்
ஏற்படுகிறதாம். லூஸிஃபெரின் பிராண வாயுவுடன் சேரும்போது ஒளி பிறக்கிறதாம். அதே
வௌிச்சத்தை விஞ்ஞானிகள் சோதனைச் சாலையில் ஏற்படுத்தி யிருந்தாலும் அந்த
வௌிச்சம் உண்டாக தேவையான லூஸிப்பெரின், லூஸிஃப்ரேஸ் கெமிக்கல்களை தனியாக உருவாக்க
முடியவில்லை. இந்த கெமிக்கல்களை மின்மினிப் பூச்சியிடம் இருந்தே
பெற்றிருக்கிறார்களாம். </p>

<p> </p>

<p>பன்னாரி மாரியம்மன்</p>

<p> எல்லா
சக்திகளுக்கும் காரணமாக ஆதி சக்தியாக இருப்பது பராசக்தி. இதையே 
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>அன்னை, அம்பாள், துர்க்கா, காளி,
சண்டிகா, மாரி என்று பல பெயர்களால் கூறி அருள்மிகு அம்பாளை தியானம் செய்கின்றோம்.
</p>

<p> எங்கும் நிறைந்த
பொருளாய் கருதப்படும்போது அவளுக்கு பூரணி என்றும், பராசக்தி என்றும் இராச ராஜேஸ்வரி
எ்னறும், முத்தொழில்கள் செய்யுமிடத்து பிராமிணி, வைஷ்ணவி, ருத்ராணி என்றும்
ஈஸ்வரனுக்கு ஒப்பாகும்போது அவளுக்கு துர்க்்கை என்றும், கால சொரூபிணியாக கருதப்படும்போது
அவள் காளி என்றும், பிடாரி (பீடை போக்குபவள்) மகா மாரியம்மன் என்றெல்லாம்
அழைக்கப்படுகிறாள். ஒரே தெய்வம் வேறுபட்ட பல பெயர்களால் அழைக்கப்பட்டாலும் எல்லாம்
சக்தி மயமே! </p>

<p> அவற்றில் கொங்கு
நாட்டின் வட மேற்கு எல்லையருகில் வீற்றிருக்கும் அருள்மிகு பண்ணாரி மாரியம்மன் மிகவும்
சக்திவாய்ந்த தெய்வமாகும். இந்த கோவில் ஈரோடு மாவட்டம் சத்தியமங்கலம் வட்டம்
சத்தியமங்கலத்திலிருந்து மைசூர் செல்லும் பாதையில் பன்னிரெண்டு கிலோ மீட்டர் தொலைவில்
தெற்குப்பார்த்த வண்ணமாக உள்ளது. </p>

<p> பண்ணாரி
மாரியம்மன் சுயம்பு வடிவாக அமைந்திருக்கிறது. இந்தத் தலத்தில் மூர்த்தி மிகச்
சிறப்புடையது. அம்பிகை சுயம்புவாக அமைந்திருந்த பண்ணாரி மாரியம்மனை பற்றிய பழைய
புராணமோ, சரித்திரமோ இல்லை. சிலர் கீர்த்தனை, வழி நடைசிந்து முதலியவை எழுதியுள்ளனர்
என்று தெரிய வருகிறது. </p>

<p> அம்பிகையை
பயபக்தியுடன் எப்போதும் துதிக்கும் அன்பர்களுக்கு வேண்டியதை வேண்டிய வண்ணம்
அருள்பாவிக்கின்றாள். தாய் பண்ணாரி, தினமும் பக்தகோடிகள் வந்து அபிஷேக ஆராதனைகள்
செய்தும், பொங்கல் இட்டும், முடி களைந்தும், உருள் தண்டம் முதலியன செய்தும்
வழிபடுகிறார்கள். கண்ணின் வியாதிகள் தேவியின் தீர்த்த்தால் குணமாகின்றது. நரம்பு
பற்றிய வியாதிகள், குருடு சம்பந்தப்பட்ட வியாதிகள் முதலியனவும், அம்மையின் வழிபாட்டில்
நிவர்த்தி ஆகின்றன. அம்பிகைக்கு உயிர் பலிகள் ஏதும் செய்வதில்லை. பண்ணாரி அம்மனை
தரிசிக்க வென்றே பல ஊர்களிலிருந்தும் ஆன்மீக பெருமக்கள் தினமும்  வந்து பண்ணாரி
அம்மாவை மனம் குளிர தரிசிக்கின்றனர். </p>

<p> -எஸ். ஆனந்த
சேகர், திருச்சி. </p>

<p> </p>






</body></text></cesDoc>