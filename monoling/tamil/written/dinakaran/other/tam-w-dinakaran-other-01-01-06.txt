<cesDoc id="tam-w-dinakaran-other-01-01-06" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-01-01-06.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-01-06</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-01-06</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சென்னை,
ஜன.6  தமிழ்நாட்டில்
நேற்று ஒரு சில இடங்களில் லேசான தூறல் மழை பெய்தது. </p>

<p>முன் அறிவிப்பு:- தென்
தமிழ்நாட்டில் லேசான மழை பெய்யலாம். வட தமிழ்நாட்டிலும், புதுவையிலும் வறண்ட வானிலையே
காணப்படும். </p>

<p>சென்னை:- சென்னை மற்றும்
அதன் சுற்றுப்புறங்களில் காலையில் லேசான மேகமூட்டம் காணப்படும். குறைந்த பட்ச வெப்பநிலை
21 டிகிரியாக இருக்கும். </p>

<p>வெப்பநிலை:-
நுங்கம்பாக்கம்:- அதிக அளவு 30 டிகிரி செல்சியஸ். குறைந்த அளவு 21.5 டிகிரி செல்சியஸ்.
ஈரப்பதம் 82 சதவீதம். </p>

<p>மீனம்பாக்கம்:- அதிக
அளவு 31 டிகிரி செல்சியஸ். குறைந்த அளவு 21.0 டிகிரி செல்சியஸ். ஈரப்பதம் 82
சதவீதம் </p>

<p> </p>

<p>www.tamilnaduonline.com</p>

<p>    இந்த தளத்தில் தமிழ்நாட்டைப் பற்றி விளக்கமாக
சொல்லப்படுகிறது. அரசு என்ற தலைப்பின் கீழ் தமிழக அமைச்சரவையில் இடம்பெற்றுள்ள
அமைச்சர்கள் மற்றும் அவர்களின் துறைகள் பற்றி பேசப்படுகிறது. அரசியல், பாதுகாப்பு,
பணிகள் பற்றியும் சொல்லப்படுகிறது. கலை, கலாச்சாரம், என்ற பொருளின் கீழ்
தமிழ்நாட்டின் கலை, கலாச்சார பாரம்பரியம், கலைஞர்கள், கலைப்பொருள் மையங்கள்,
இயல், இசை, நாடக கலை பற்றி சொல்லப்படுகிறது. </p>

<p>   
கல்வி என்ற பிரிவில் தமிழக பள்ளிகள், கல்லூரிகள், பல்கலை கழகங்கள், பற்றிய
தகவல்கள் உள்ளன. மற்றும் தொழில், பொருளாதாரம், கம்ப்யூட்டர், பொழுது போக்கு,
சினிமா, டி.வி., மருத்துவம், விஞ்ஞானம், சுற்றுலா, விளையாட்டு போன்ற வெவ்வேறு
தலைப்புகளின் கீழ் அவை சம்பந்தப்பட்ட எல்லா விவரங்களும் தொகுத்து தரப்பட்டுள்ளன.
</p>

<p> </p>

<p>வைகுண்ட ஏகாதசி</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>வைகுண்ட ஏகாதசி அரங்கநாதனுக்குரிய பண்டிகையாகும். மாதந்தோறும் ஏகாதசி வந்தாலும்
பெரிய சிறப்புக்குரிய ஏகாதசி இதுவே. இன்று ரங்கம் அரங்கநாதன் ஆலயத்தில் சொர்க்க
வாசல் திறக்கப்பட்டு- அரங்கநாதன் சொர்க்க வாசல் நுழையும் </p>

<p>வைபவம் நடை பெறும். இந்த
நன்னாளில் அரங்கநாதனை ரங்கத்தில் தரி சித்தால் மோட் சம் கிட்டும் என்பதனால் தான்
இதனை சொர்க்க வாசல் என்றே </p>

<p>அழைத்தார்கள். வைகுண்ட
ஏகாதசி அன்று பெருமாள் மூலஸ் தானத்திலிருந்து வந்து சொர்க்க வாசலுக்கு அருகில் வந்து
நிற்பார். சர்வ லோக சரண்யனான அரங்கநாதன் கருணை பொங்க தம்மோடு மக்களையும்
மோட்சத்திற்கு அழைத்து வருவதாக ஐதீகம். அரங்க நாதன் முன் செல்ல மக்கள் பின்தொடர
சொர்க்க வாசல் கதவு திறக்கப்படும். அனைவரும் அந்த வாசல் வழியே புகுந்து வருவர். இதுவே
சொர்க்க வாசல் நுழைதலாகும். </p>

<p>வைகுண்ட ஏதாதசி அன்று
விரதமிருப்பது கோடி புண்ணிய மாகும். அன்று விரதமிருப்பது என்பது ஆகாரத்தை விட்டு விட்டு
பகவானை வழிபடும் நாளாக கருத வேண்டும். ஏகாதசி விரதத்தின் போது வெறும் துளசி தீர்த்தம்
மட்டுமே சாப்பிட வேண்டும். ஏகாதசி விரதமிருந்து மறுநாள் துவாதசி அன்று- சுண்டைக்காய்,
நெல்லிக்காய், அகத்தி கீரை சேர்த்து சமையல் செய்து பிறருக்கும் உணவளித்து நாமும் உண்டால்
அரங்கநாதனின் அருள் பரிபூரண மாய் நமக்கு கிட்டும். </p>

<p>வைகுண்ட ஏகாதசி அன்று
விரதமிருப்பதுடன் அன்றிரவு உறங்காமல்-கண்விழித்து அரங்கநாதனை வழிபட்டு அவன் நினைவுடனே
விழித்திருக்க வேண்டும். வைகுண்ட ஏகாதசி பண்டிகை ரங்கத்தில் மட்டுமின்றி,
திருப்பதியிலும், திருவல்லிக் கேணி பார்த்தசாரதி கோவிலிலும் சிறப்பாக
கொண்டாடப்படுகிறது. வைஷ்ணவர்கள் மட்டுமின்றி சிவனை வணங்கும் சைவர்களும் போற்றி
தொழுகின்ற பண்டிகையே வைகுண்ட ஏகாதசி. பெருமாள் கோவில்களில் வைகுண்ட ஏகாதசி அன்று கருட
வாகனத்தில் தரிசனம் அளிப்பார் பெருமாள். பெருமாள் கோவில் அனைத்திலும் வைகுண்ட ஏகாதசி
அன்று பெருமாளை தரிசிப்பது கோடி புண்ணியம் ஆகும். </p>

<p>- வீதி விடங்கன்,
திருவாரூர் </p>

<p> </p>

<p> </p>






</body></text></cesDoc>