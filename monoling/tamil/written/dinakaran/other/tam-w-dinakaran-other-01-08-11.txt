<cesDoc id="tam-w-dinakaran-other-01-08-11" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-other-01-08-11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-08-11</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-08-11</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சர்க்கரை நோயில் பல வகைகள் </p>

<p>முக்கியமாக இரண்டு வகைகள் உள்ளன. </p>

<p>1. இன்சுலின் சார்ந்த சர்க்கரை நோய், (ஐ.டி.டி.எம்). இவ்வகை சர்க்கரை நோய்
நோயாளிகளுக்கு கணையத்தில் இன்சுலின் சுரப்பதில்லை. இந்த நோய் உள்ளவர்களுக்கு வாழ்நாள்
முழுவதும் இன்சுலின் ஊசி தேவைப்படுகிறது. </p>

<p>2. இன்சுலின் சாராத சர்க்கரை நோய்(என்.ஐ.டி.டி.எம்) இந்த வகை சர்க்கரை
நோய் பலருக்கு உள்ளது. இந்த இரண்டு வகைகளைத் தவிர பைப்ரோ கால்குலஸ் பேங்கிரியாடிக்
டயாபடிஸ்,(கணையத்தில் கல் உண்டாவதால் ஏற்படும் டயாப்டீஸ்) ஜென்டேன்ஸ்
டாய்ப்டீஸ்(கர்ப்ப காலத்தில் தோன்றும் டயாப்டீஸ்) போன்றவையும் உள்ளன. ஆனால்
இவ்வகையில் மிகச் சிலரே உள்ளனர். எந்த மாதிரியான சர்க்கரை நோயாக இருந்தாலும்
ஆரம்பத்திலேயே கண்டறிந்து மருத்துவ ஆலோசனை பெற வேண்டும். </p>

<p>டாக்டர் வி.மோகன் சென்னை-86</p>


 
  
  <p> </p>
  
 


<p>                    
f";rD}h; | mf;dPRtuh; </p><p>
  </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>அளப்பரியா ஆனந்தத்துடன் அகில நாயகனாம் ஈஸ்வரனை அடி தொழுது வழிபட்ட
அடியார்கள் சிறந்தோங்கிய திருத்தலங்கள் ஏராளமுண்டு. இத்தலங்கள் அனைத்தும் இன்றளவும்
உலகத் துயரங்களை விலக்கி அடிய வர்களுக்கு ஆனந்த பேரொளியை அருளச் செய்கின்றன. இத்தகைய
கணக்கிலடங்கா தெய்வத்திருத் தலங்களுள் கஞ்சனூரும் ஒன்று. பராசர முனிவர், பக்த
மார்க் கண்டேயன் இவர்களோடு சுக்கி ரன், அக்னிபகவான், ராமன் என தேவர்களும் போற்றி
துதித்த புண்ணிய திருத்தலம் இது. </p>

<p>சுக்கிரன் வழிபட்ட சிவத்தலம் சோழ நாட்டில் கஞ்சனூர் என்ற பெயரில்
விளங்குகிறது. இத்தலம் கும்ப கோணத்திற்கருகில் உள்ளது. </p>

<p>போஜராஜனது வம்சத்தில் கம்சன் என்றொரு அரசன் மதுராபுரியை ஆண்டு வந்தான்.
பெண்ணாசை மிகுந்த அவன் அதன் காரணமாய் உடல் நலம் கெட்டான். அவனது நிலையறிந்த சுக்கிர
பகவான், கஞ்சனூரின் பெருமையை எடுத்துச்சொல்லி வழிபடுமாறு பணித்திட, கம்சராஜனும்
வழிபட்டு நலமடைந்தான். கம்சன் வழிபட்ட கம்சபுரம்தான் கஞ்சனூர் ஆயிற்று என்பர்.
</p>

<p>நான்முகனுக்கு கஞ்சன் என்றொரு நாமம் உண்டு. நான் முகனாகிய கஞ்சன்
வழிபட்டதால் கஞ்சனூர் என்ற பெயர் ஏற்பட்டது என்றும் கூறுவர். </p>

<p>கஞ்சனூர் அக்கினிதேவன் வழிபட்ட தலம். ஆதலால் இத்தல இறைவனுக்கு
அக்னீசுவரன் என்று பெயர் வந்தது. சுயம்புலிங்கத் திருமேனியில் ஜோதியாய் பெருமான்
ஒளிர்ந்து கொண்டிருக்கிறார். அம்பிகையின் பெயர் கற்பக நாயகி. பராசரமுனிவர், இந்திரன்,
மார்க் கண்டேயர், ராமர், ஆஞ்சநேயர், சந்திரன் என்று பலரும் இத்தல இறைவனை
பூஜித்துள்ளனர். </p>

<p>சுக்கிரபகவானும் இத்தலத்துக்கு வந்து வழிபட்டுள்ளார். தான் வழிபட்டதோடு தன்
பொருட்டு நலம் விழைவோர், கஞ்சனூரில் அமர்ந்துள்ள இறைவனை வழிபட்டால்
சிவபெருமானுடைய அருளோடு, தன்னுடைய அருளும் சேர்ந்து தரும் சுக்கிரனின் அருள்பெற்ற பெருமை
இத்தலத்திற்கு உண்டு. இத்தலத்தை வழிபட்டால் சுக்கிர பகவானின் அருள் கிட்டும்.
</p>

<p>- பாளை பசும்பொன் , நெல்லை. </p><p>
 </p>

<p> </p>






</body></text></cesDoc>