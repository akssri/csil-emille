<cesDoc id="tam-w-dinakaran-politics-00-09-16" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-politics-00-09-16.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-09-16</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-09-16</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சோனியா தலைமையில் கட்சி
தேய்ந்தது: காங்கிரஸ் உறுப்பினர்கள் பாதிக்கும் கீழாக குறைந்தனர்</p>

<p> </p>

<p>புதுடெல்லி, செப்.16- இது குறித்து காங்கிரஸ் கட்சியின் மத்திய
தேர்தல் அதிகாரி ராம்நிவாஸ் மிர்தா கூறியதாவது:- </p><p>
</p><p>
காங்கிரஸ் கட்சியில் உறுப்பினர்கள் எண்ணிக்கை குறைந்து வருவது உண்மை தான். 4.56 கோடியாக
இருந்த உறுப்பினர்கள் இப்போது 2.20 கோடியாக குறைந்து இருக் கிறார்கள். இது முழுமையான கணக்கு
இல்லை. தமிழகம், கர்நாடகம் உள்பட இன்னும் சில மாநிலங்களில் இருந்து உறுப்பினர்கள்
எண்ணிக்கை வர வேண்டி உள்ளது. </p><p>
</p><p>
மராட்டியம், பஞ்சாப், ராஜஸ்தான் ஆகிய மாநிலங்களில் உறுப்பினர்கள் எண்ணிக்கை
அதிகரித்துள்ளது. பீகார், குஜராத், கேரளா ஆகிய மாநிலங்களில் பாதிக்கும் குறைவாகவே
உறுப்பினர்கள் சேர்ந்துள்ளனர். </p><p>
</p><p>
சோனியா காரணமா? </p><p>
</p><p>
சோனியா தலைவர் ஆனதால் தான் உறுப்பினர்கள் எண்ணிக்கை குறைந்து விட்டது என்பது சரி அல்ல.
தற்போது உறுப்பினர் சேர்ப்பது உண்மையாக நடப்பதால் இந்த முறை எண்ணிக்கை குறைவாக
இருப்பதாக தெரிகிறது. இது ஆரோக் கியமான வளர்ச்சி தான். கடந்த காலங்களில்
உறுப்பினர்கள் எண்ணிக்கை மிகைப்படுத்தி காட்டப்பட்டு உள்ளது. </p><p>
</p><p>
கட்சியில் உறுப்பினராக சேருபவர்கள் 3 ஆண்டு கட்டணத்தை சேர்த்து கட்ட வேண் டும். தீவிர
உறுப்பினர்கள் கட்சி பத்திரிகையின் சந்தாதாரர் ஆக வேண்டும் என்று கட்சி விதி
திருத்தப்பட்டதாலும் உறுப்பினர் சேர்ப்பு குறைந்து இருக்கலாம். </p><p>
</p><p>
தமிழக தேர்தல் </p><p>
</p><p>
தமிழ்நாடு, பஞ்சாப், ராஜஸ்தான், கேரளா, குஜராத் ஆகிய மாநிலங்களில் பல்வேறு கார
ணங்களால் தேர்தல் அட்ட வணை மாற்றி அமைக்கப்பட்டு உள்ளது. கட்சி தலைவர் தேர்தல்
நவம்பர் 1-ந்தேதி நடக்கும். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>தேர்தலுக்காக விழா நடத்தும்
இயக்கமல்ல தி.மு. கழகம்: முதல்வர் கருணாநிதி பேச்சு</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>




 
  
  <p>தி.மு.க. முப்பெரும்
  விழாவில் பெண் ஆட்டோடி ரைவர்களுக்கு முதலமைச்சர் கருணாநிதி பரிசுகளை வழங்கியபோது
  எடுத்தபடம். அருகில்அமைச்சர்கள் அன்பழகன், ஆற்காடு வீராசாமி உள்ளனர். </p>
  
 




<p>சென்னை, செப்.16 சென்னையில் நடந்த தி.மு.க. முப்பெரும் விழாவில்
விருது பெற்றவர்களை பாராட்டி முதலமைச்சர் கருணாநிதி பேசியதாவது:- </p><p>
</p><p>
ஆண்டுதோறும் </p><p>
</p><p>
விருது பெற்றோர் சார்பில் தம்பி பரமக்குடி தங்கவேலன் நன்றியுரை ஆற்றியிருக்கிறார்.
விருது வழங்கிய தலைமை கழகத்தின் சார்பில் நம்முடைய பொதுச்செயலாளர் பேராசிரியர்
நீண்டதோர் விளக்கவுரை வழங்கியிருக்கின்றார். இன்னும் ஒரு நிகழ்ச்சி இருக்கின்ற
காரணத்தால் நீண்ட நேரம் நான் கருத்துகளை சொல்லு கின்ற வாய்ப்பற்றவனாக உங்கள் முன்னால்
நின்று கொண்டிருக்கின்றேன். </p><p>
</p><p>
ஆண்டுதோறும் நடைபெறு கின்ற அண்ணா பிறந்த நாள் விழா, கழகம் பிறந்த நாள் விழா, மாபெரும்
தலைவர் தந்தை பெரியார் பிறந்த நாள் விழா ஆகிய மூன்று விழாக்களையும் இணைத்து முப்பெரும்
விழாவாக நாம் கொண்டாடி வருகின்றோம். இந்த ஆண்டு திடீரென்று நினைத்து கொண்டு
நடத்தப்படுகின்ற விழா அல்ல இந்த விழா என்பதை நீங்கள் எல்லாம் மிக நன்றாக
அறிவீர்கள். </p><p>
</p><p>
தேறுதல் தேர்தல் </p><p>
</p><p>
காயிதே மில்லத் மறைந்த பிறகு அவர் அடக்கம் செய்யப் பட்ட இடத்திற்கு அடக்கம்
செய்யப்பட்ட அந்த ஆண்டி லிருந்து ஒவ்வொரு ஆண்டும் சென்று வருகின்றவர்கள் நாம். இரண்டு
ஆண்டுகளுக்கு முன்பு அந்த பயணத்தை தொடங்கிய வர்கள் வேறு சில கட்சிக் காரர்கள். அடக்கம்
செய்யப் பட்ட இடத்திற்கு நாம் செல்வது நம்முடைய துன்பத்திற்கு ஒரு தேறுதல் தேடுவதற்காக.
இவர் கௌல்லாம் செல்வது தேர்த லுக்காக. எனவே நாம் எந்த விழா நடத்தினாலும் நம்மு டைய
உணர்வின் அடிப்படையில் நம்முடைய கொள்கை ஆழத்தின் காரணமாக நடத்தப் படுகின்ற
விழாக்களாகவே அமைகின்றன. அதனால்தான் இன்றைக்கு கொள்கையில் சிறந்த மாவீரர்கள் நான்கு
பேருக்கு தலைமை கழகத்தின் சார்பிலே விருதுகள் இங்கே வழங்கப்பட்டிருக்கின்றன. </p><p>
</p><p>
நண்பன் </p><p>
</p><p>
பெரியார் விருது பெற்ற கு.தென்னன் திருவாரூர்க் காரர். 1936-ம் ஆண்டு நான் பிறந்த
திருக்குவளையிலிருந்து திருவாரூர் பள்ளிக்கு படிக்க வந்த அந்த கால கட்டத்திலி ருந்து இணை
பிரியாத நண்பன். எத்தனையோ புயல், சூறாவளி அரசியல் ரீதியாக வீசிய கால கட்டத்திலும்
என்னை விட்டுப் பிரியாத நண்பன் கு.தென்னன். என்னை விட்டு பிரியாத தற்காக பெரியார்
விருது அல்ல. பெரியாருடைய கொள்கைகளை விட்டுப் பிரியாதவர் என்பதற் காக பெரியார் விருது
இன்றைக்கு வழங்கப்பட்டிருக்கின்றது. </p><p>
</p><p>
சிறு பிராயத்திலே நாங்கள் திருவாரூரில் இந்த இயக்கத்தை வளர்ப்பதற்காக அரும்பாடு பட்ட
போது என்னுடைய வீட்டார், அவருடைய வீட்டார், எங்களுடைய சுற்றத்தார், உறவி னர்கள்
எல்லாம், இந்த இரண் டும் உருப்படாது என்கின்ற அருமையான பட்டத்தை எங் களுக்கு சூட்டி
மகிழ்ந்திருக் கின்றார்கள். உருப்படாதவன் கையால் இன்றைக்கு இன் னொரு உருப்படாதவன் விருது
பெற்ற காட்சியை நீங்கள் எல்லாம் கண்டீர்கள். </p><p>
</p><p>
பதவி பெற </p><p>
</p><p>
நம்முடைய பேராசிரியர், மாறன் இங்கே எடுத்து காட்டியதை போல இன்றைய இளைஞர்கள்
படிப்பை முடித்து விட்டு அல்லது படிப்பை பாதியில் நிறுத்தி விட்டு எங்கே சென்றால் என்ன பதவி
பெற லாம் என்ற முனைப்போடு தங்களுடைய வாழ்க்கையை வகுத்து கொள்கின்ற நிலை மையை
காணுகின்றோம். ஆனால் எந்த கட்சியிலே சேர்ந்தால் என்ன நடக்கும் என்றெல்லாம் எண்ணி
பாராமல், சிந்திக்காமல், கணக்கு போடாமல், எந்த கொள்கையை ஏற்று கொண் டோமோ, எந்த
கொள்கைக்காக இந்த கட்சி இருக்கிறதோ எந்த தலைவர் இருக்கிறாரோ அவரை பின்பற்றுகின்ற
கால கட்டத்தில் நாங்கள் எல்லாம் ஏன் இந்த இயக்கத்தில் இருக் கின்ற பல தலைவர்கள் பல
தளகர்த்தர்கள், இந்த இயக்கத் திலே சேர்ந்தவர்கள். அப்படிப் பட்ட இயக்கத்தில் இன்று
நால் வருக்கு நாம் விருதுகளை வழங்கி இருக்கிறோம் என்றால் அவர் களுக்கு பெருமை சேர்க்க
மாத்திரமல்ல. இந்த பெரு மையை நாமும் பெற வேண் டுமே என்ற நிலைமையிலே இன்றுள்ள
இளைஞர்கள் இந்த இயக்கத்திற்காக, இந்த இனத்திற் காக, மொழிக்காக, சமுதாயத்
திற்காக பாடுபட வேண்டும் என்கின்ற அந்த எண்ணத் தோடுதான் இந்த விருது வழங்குகின்ற விழா
ஏற்பாடாகி நடைபெற்று கொண்டிருக் கின்றது. </p><p>
</p><p>
இயக்கம் வளர்த்தவர் </p><p>
</p><p>
அண்ணா விருது பெற்ற தம்பி தங்கவேலன் பரமக்குடி தங்கவேலன் என்றால்தான் திடீரென்று
எல்லோருக்கும் தெரிய கூடிய அளவிற்கு அந்த ஊரின் பெயரையும் இணைத்து கொண்டு பெயர்
பெற்றவர் தங்கவேலன். பேராசிரியர் இங்கே எடுத்து காட்டியதை போல் எங்களை போன்ற கழக
சொற்பொழிவாளர்களை அவ ருக்கு கிடைத்த வசதியான வாகனங்களில் ஏற்றி சென்று கூட்டங்கள் நடத்தியது
மாத்திர மல்ல, அவரே சைக்கிளிலும், நடந்தும் சென்று திராவிட இயக்கத்தை வளர்த்த தீரர்
என்றால் அது மிகையாகாது. </p><p>
</p><p>
நான் இரண்டாவது முறை யாக முதலமைச்சராக இருந்த போது கூட்டுறவு வங்கியினு டைய நிர்வாக
பொறுப்பிலே அவர் இருந்தார். அப்பொழுது தமிழ்நாட்டிலே உள்ள கூட்டுறவு வங்கிகளிலே தலை
சிறந்த வங்கி தங்கவேலன் தலைமையிலே இருந்த வங்கி என்கின்ற அந்த பெயரை பெற்றது எனக்கு
நன்றாக தெரியும். நிர்வாக திறமையி லேயும் சரி, நாணயத்திலும் சரி, தங்க வேலன்
மற்றவர்களுக்கு எடுத்து காட்டாக திகழ்ந்தவர். அந்த கூட்டுறவு வங்கியை அவர் நடத்திய பாங்கு
எல்லோரையும் மகிழ செய்த பாங்காகும். அப் படிப்பட்ட ஒரு தம்பியை இந்த இயக்கம்
பெற்றிருக்கின்றது, நான் பெற்றிருக்கின்றேன். பேராசிரியர் பெற்றிருக் கின்றார். </p><p>
</p><p>
அன்பின் அடையாளம் </p><p>
</p><p>
தம்பி அமுதன் நல்ல எழுத்தாளர். நல்ல பேச்சாளர். நீண்ட நேரப் பேச்சாளர் அல்ல என்று
பேராசிரியர் குறிப் பிட்டார். சில பேர் நீண்ட நேரம் பேசினால் ஏன் பேசுகிறார் என்று
எண்ண தோன்றும். ஆனால் அமுதன் பேசினால் அடடா அதற்குள் முடித்து விட்டாரே என்று எண்ண
தோன்றும். என்பால் அவர் கொண்டிருக்கின்ற அன்பிற்கு அடையாளமாக இன்று ஒரு புத்தகத்தை
அவரே அச்சியற்றி இங்கே வழங்கியதை நான் பார்த்தேன். நான் சிறைச் சாலையில் இருந்தபோது
இன் னொரு சிறைச்சாலையிலிருந்து அவர் எனக்கு எழுதிய கடிதம் அந்த புத்தகத்தில் இடம்
பெற்றிருக்கின்றது. அதேபோல் நான் இருந்த சிறைச்சாலை யிலிருந்து அவருக்கு எழுதிய பதில்
கடிதமும் அந்த புத்தகத் திலே இடம் பெற்றிருக்கின்றது. </p><p>
</p><p>
கடிதம் </p><p>
</p><p>
இன்னொன்று சொல்ல விரும்புகின்றேன். அந்த புத்தகத் திலே அது இடம் பெற்றிருக் கிறதா,
இல்லையா என்று தெரியாது. மேடையில் அவசர மாக பார்த்தேன். ஒரு முறை யிலே திருச்சியிலே
மாநாடு. அந்த மாநாட்டிற்கு நான் செல்ல முடியாத சூழ்நிலை. நான் செல்லவில்லை. அமுதன் அந்த
மாநாட்டிற்கு சென்று மாநாட்டு மேடையிலிருந்து எனக்கு ஒரு கடிதம் எழுதினார். </p><p>
</p><p>
னானாநீ இல்லாத மாநாடு, நிலவில்லாத வானம். எனவே நான் திரும்பிச் செல்லுகிறேன்ை என்றைு
திரும்பிச் சென்றார். அண்ணாவே இருந்து நடத்திய மாநாடு அது. அந்த அளவிற்கு என்பால் அன்பு
கொண்டிருந்த வர் அமுதன். அதனால்தான் இறைக்கு விருதா என்று யாரும் கேட்காதீர்கள். இவ்வளவு
காலம் அவருக்கு விருது அளிக்கப் படாமல் இருந்ததே தவறு என்பதை உணர்ந்து இப் பொழுது அவருக்கு
அளிக்கப் பட்ட விருது இது. </p><p>
</p><p>
பூசல் இன்றி </p><p>
</p><p>
பெங்களூர் திராவிட மணி அவருடைய இயற்பெயர் பூசலிங் கம். கழகத்திலே பூசலில்லாமல்
பெங்களூரில் கழகத்தை உரு வாக்கி, வளர்த்து, ஆளாக்கிய பெருமைக்குரிய தம்பி திராவிட மணி.
அவர் திராவிட முன் னேற்ற கழகத்தின் நிர்வாகத் திற்கு சென்றால் எந்த பொறுப்பை
ஏற்றாலும் அந்த பொறுப்பை நல்ல முறையிலே, உரிய முறையிலே, எல்லோரும் பாராட்டத்தக்க
அளவிற்கு நிறைவேற்றுபவர். சட்டமன்ற உறுப்பினராக கூட பெங் களூரிலே அவர் பணியாற்றி
யிருக்கின்றார். ஒரே குரல் திராவிட முன்னேற்ற கழகத் தின் குரல் பெங்களூர் திராவிட
மணியின் குரலாக ஒலித்த நேரத்திலே திராவிட முன் னேற்ற கழகத்தின் புகழை பரப்பிய பெருமை
தம்பி திராவிட மணிக்கு உண்டு. </p><p>
</p><p>
பாடம் </p><p>
</p><p>
இவர்களை பற்றி எல்லாம் பேச வேண்டுமென்றால் ஒரு நாௌல்லாம் பேசி கொண்டி ருக்கலாம்.
அதன் சுருக்கமாகத் தான் இன்றைக்கு அவர்களுக்கு விருது வழங்கி இருக்கின்றோம். அந்த விருதுகளை
பெற்றுள்ள என்னுடைய அருமை தம்பி மார்கள் வாழ்க, வாழ்க, பல் லாண்டு வாழ்க அவர்களுடைய
வாழ்க்கை வரலாறு வருங்கால இளைஞர்களுக்கெல்லாம் பாட மாக ஆகுக என்று கூறி என்னுடைய
வாழ்த்துரையை நிறைவு செய்கிறேன். </p><p>
</p><p>
இவ்வாறு அவர் பேசினார். </p>

<p> </p>

<p>ஆட்சியில் பங்கு
தரமாட்டோம் இது உறுதி - ஜெயலலிதா மூப்பனாருக்கு மீண்டும் வாய்ப்பூட்டு</p>

<p> </p>

<p>சென்னை, செப். 16- சமீப காலமாக ஜெயலலிதா பேசும் போதும்,
அறிக்கை வௌியிடும்போதும், நாங்கள் மீண்டும் எம்.ஜி.ஆர். ஆட்்சி அமைப்போம். கூட்டணி
ஆட்சி என்பது கிடையாது. யாருக்கும் எங்களுடைய ஆட்சியில் பங்கு தரமாட்டோம் என்று சொல்லி
வருகிறார். </p><p>
</p><p>
ஆனால் அ.தி.மு.க. கூட்டணியில் இடம்பெற்றுள்ள த.மா.கா. தலைவர் மூப்பனார், அடுத்து
தமிழ்நாட்டில் கூட்டணி ஆட்சிதான் அமையும் என்று சொல்கிறார். இந்த கூட்டணியில்
இடம்பெற்றுள்ள காங்கிரஸ் தலைவர் இளங்கோவனும் அதை ஆமோதிக் கிறார். மூப்பனார் சொல்வது
சரியே என்று ஆதரவு குரல் எழுப்புகிறார். இதனால் அ.தி.மு.க.வுக்கும், கூட்டணி
கட்சிகளுக்கும் இடையே மோதல் ஏற்பட்டு இருக்கிறது. </p><p>
</p><p>
ஜெயலலிதா உறுதி </p><p>
</p><p>
இந்த நிலையில் நேற்று அண்ணா சிலைக்கு மாலை அணிவிக்க வந்த ஜெயலலிதா விடம் மீண்டும்
நிருபர்கள், கூட்டணி ஆட்சியா? தனித்து ஆட்சியா? என்று கேட்டார்கள். அதற்கு
ஜெயலலிதா, ஆட்சியில் பங்கு தரவே மாட்டோம் என்று திட்டவட்ட மாக பதில் அளித்தார்.
இதன் மூலம் மூப்பனாருக்கு மேலும் இறுக்கமான வாய்ப்பூட்டை ஜெயலலிதா போட்டுவிட்டார் என்று
எடுத்துக்கொள்ளலாம். </p><p>
</p><p>
நேற்று அண்ணா பிறந்த நாளையொட்டி சென்னை அண்ணா சாலையில் உள்ள அண்ணா சிலைக்கு ஜெயலலிதா
காலை 10.30 மணிக்கு வந்து மாலை அணிவித்தார். அவரை தொடர்ந்து முன்னாள் அமைச்சர்கள்
காளிமுத்து, எஸ்.முத்துசாமி, செங்கோட்டையன், செல்வகணபதி, மதுசூதனன், பட்டாபிராமன்,
ஜெயக்குமார், எம்.பி.க்கள் பி.எச். பாண்டியன், திண்டுக்கல் சீனிவாசன், கே.மலைச்சாமி,
தளவாய ்சுந்தரம் ஆகியோரும் மாலை அணிவித்தார்கள். </p><p>
</p><p>
பங்கு இல்லை </p><p>
</p><p>
பிறகு மேடையிலேயே ஜெயலலிதா நிருபர்களுக்கு பேட்டி அளித்தார். </p><p>
</p><p>
அது வருமாறு:- </p><p>
</p><p>
கேள்வி:- ஆட்சியில் பங்கு என்றும், தமிழகத்தில்இனி கூட்டணி ஆட்சிதான் ஏற்படும் என்றும்
மூப்பனாரும், இளங் கோவனும் தொடர்ந்து கூறி வருகிறார்களே? </p><p>
</p><p>
பதில்:- ஆட்சி- அதிகாரத்தில் கூட்டணி கட்சிகளுக்கு பங்கு தர மாட்டோம் என்பதில் அ.தி.
மு.க. உறுதியாக இருக்கிறது. தேர்தலுக்கு இன்னும் காலம் இருக்கிறது. அப்போது பார்த்துக்
கொள்ளலாம். இப்போது விவாதிக்க தேவை இல்லை. </p><p>
</p><p>
கேள்வி:- இந்த பிரச்சினை தொடர்ந்து விவாதிக்கப்படுவதால் அ.தி.மு.க. கூட்டணியில் பெரிய
கருத்து வேறுபாடு ஏற்பட்டு இருப்பதுபோல கருத்து நிலவுகிறதே? </p><p>
</p><p>
பதில்:- அப்படி கருத்து வேறுபாடு எங்கள் கூட்டணியில் இல்லை. பத்திரிகைகள் தான்
பெரிதுபடுத்துகின்றன. </p><p>
</p><p>
முத்துசாமிக்கு பதவியா? </p><p>
</p><p>
கேள்வி:- இளங்கோவன் மீண்டும், மீண்டும் உங்களை தாக்கிப் பேசி வருகிறாரே? </p><p>
</p><p>
பதில்:- இந்த மாதிரி சின்ன விஷயத்துக்கு எல்லாம் நான் பதில் சொல்ல விரும்பவில்லை. </p><p>
</p><p>
கேள்வி:- அ.தி.மு.க.வில் முன்னாள் அமைச்சர் முத்துசாமி போன்றவர்கள் முக்கியத்துவம்
தரப்படாமல் இருக்கிறார்களே? </p><p>
</p><p>
பதில்:- (சிரித்தபடி) எல்லோரும் முக்கியத்துவத்துடன் தான் இருக்கிறார்கள். உரிய
நேரத்தில் உரிய முக்கியத்துவம் தரப்படும். </p><p>
</p><p>
கேள்வி:- நீங்கள் தென் மாவட் டங்களில் சுற்றுப்பயணம் செய் யப்போவதாக கூறப்பட்டதே அது
எப்போது? </p><p>
</p><p>
பதில்:- என்னுடைய தென் மாவட்ட சுற்றுப்பயண திட்டம் விரைவில் அறிவிக்கப்படும். இவ்வாறு
அவர் கூறினார். </p><p>
</p><p>
அண்ணா சிலைக்கு ஜெயலலிதா மாலை அணிவித்த நிகழ்ச்சியில் எம்.எல்.ஏ.க்கள் தாமரைக்கனி,
விசுவநாதன், பி.ஆர்.சுந்தரம், நடிகர் மன்சூர் அலிகான், முன்னாள் வாரிய தலைவர்கள்
பா.வளர்மதி, கா.லியாகத் அலிகான், அயனா வரம் ராமச்சந்திரன், பி.ஏ. ஜெயச்சந்திரன்,
போளூர் கோவிந்தன், டி.வள்ளுவன் உள்பட முன்னணியினர் கலந்து கொண்டனர். </p>

<p> </p>

<p>உண்மையான திராவிடத்
தலைவர்கள் மீது பழியா? வரலாறு யாரைச் சபிக்கும்; யாரை வாழ்த்தும்? முப்பெரும் விழா
விளக்கும்: கருணாநிதி கடிதம்</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>




 
  
  <p>கருணாநிதி </p>
  
 




<p>சென்னை, செப். 16- உடன்பிறப்பே, </p><p>
</p><p>
இன்று செப்டம்பர் 15. இந்த கடிதத்தை நீ சென்னை நோக்கி வரும் வழியில் - ரயிலிலோ,
பேருந்திலோ, அல்லது வேனிலோ படித்துக்கொண்டி ருப்பாய் என்று எனக்குத் தெரியும். இன்றுதானே
சிங்கநடை யும் சிங்காரத் தென்றல் நடை யும் பொங்கு கடல் நடையும் புரட்சித் தமிழ்
நடையும் தன் புது நடையில் கண்ட பூமான்- நம் அனைவரையும் ஒரு தாயின் வயிறு தாங்காத
காரணத்தினால் தனித் தனித் தாய்கள் வயிற்றில் பிறந்த சகோதரர்கள் எனக் கூறி அண்ணன்
தம்பி பாசம் உணர்த்திய அறிஞர் பெருமகன் - அண்ணா பிறந்தநாள். </p><p>
</p><p>
கவலை </p><p>
</p><p>
இந்த நாளில் சென்னை மாநகரில் உன்னை நானும், என்னை நீயும் கண்டு கலந் துரையாடி மகிழவே
ஆண்டு தோறும் அழைப்பதுபோல் இப்போதும் அழைத்துள்ளேன். வருகிறாய் உன் இணை மயிலுடன். இனிய
மழலையுடன். குடும்பத்தோடு வரும் உன்னை வரவேற்பதில் தான் எனக்கு எவ்வளவு குதூகலம்
தெரியுமா? அந்தக் குதூகல உணர்ச்சியையும் மிஞ்சக்கூடிய கவலை ஒன்று என் இதயக்
கதவிடுக்கில் எட்டிப்பார்த்துக் கொண்டு இருக்கும் என்பதையும் நினை வூட்டுகிறேன் உனக்கு. </p><p>
</p><p>
நிம்மதியிருக்காது </p><p>
</p><p>
அஃதென்ன அப்படிப்பட்ட கவலையெனக் கேட்கிறாயா? அண்ணன் அழைத்தான் என்று ஆயிரம்
வேலைகள் இருப்பினும் அவற்றை ஒத்திவைத்து விட்டுப் புறப்பட்ட உன் பயணம் நல்ல முறையில்
நடந்தேற வேண்டும் என்ற கவலைதான். இதுபோன்ற பெரிய விழாக்கள் ஒவ்வொன்றிலும், நமது
மாநாடுகளிலும் நான் வேண்டிக் கேட்டுக்கொண்டு விடை தருவேனே - நினைவிருக்குமே உனக்கு. </p><p>
</p><p>
நீங்கள் எந்தவித இடையூறு மின்றி ஊர் போய்ச் சேர்ந்தீர்கள் என்று செவியும்
சிந்தையும் குளிரும் செய்தி வரும் வரையில் எனக்கு நிம்மதியிருக்காது. ஆகையால் அன்பு
உடன்பிறப்புகளே, பத்திரமாக நடக்கட்டும் உங்கள் பயணம். பகலில் புறப்படுங்கள்.
வாகனங்களின் வேகத்தை கட்டுப்படுத்திடுங்கள் என்று நான் அப்போது மட்டுமல்ல, இப்போதும்
சொல்கிறேன். </p><p>
</p><p>
தமிழர் கூட்டம் </p><p>
</p><p>
சாலை விபத்துக்கள் சாதா ரணமாகிவிட்ட நிலைமையில் எச்சரிக்கையுடன் இருப்பதே அறிவுடைமை.
விதி யாரை விட்டது என்று கூறி விவேகமில்லாமல் நடந்துகொள்கிற கூட்ட மல்ல நாம்.
விதியெனும் சொல்லுக்கே ஒரு விதி செய்து அதனை விரட்டிய பகுத்தறிவுப் பகலவனின்
பட்டாளத்துச் சிப்பாய்கள் நாம். எனவே உற்சாக மிகுதியில் ஓட்டு வேகமாக என்று உத்தரவிடு
வதும் நல்லதல்ல - ஓட்டுநர்களும் இந்த விஷயத்தில் உரிமை எடுத்துக்கொள்வது சரியும் அல்ல. </p><p>
</p><p>
வா என்று வாஞ்சையுடன் உனையழைத்துக் கடிதம் எழுதி விட்டேன். நீ வந்துவிட்டுத்
திரும்பிப் போய்ச் சேரும் வரையில் எனக்கு உறக்கமிருக்காது. </p><p>
</p><p>
எனவே நான் மகிழுமள வுக்கு முப்பெரும் விழா நிகழ்ச்சிகளில் வௌ்ளம்போல் தமிழர் கூட்டம்
காட்டிடத் திரண்டு வரும் திராவிடத் திருவிளக்கே, நீ வரும்போதும், திரும்பும்போதும் உன்
பயணம் பத்திரமாக இருக்க வேண்டும் என்பதே என் வேண்டுகோள். </p><p>
</p><p>
திருநீல கண்டம் </p><p>
</p><p>
திராவிடத் திருவிளக்கே என உனை அழைக்கிறேன். திராவிடம் எனும் சொல் கண்டு
சீறிப்பாய்வோர் இன்றும் உண்டு. நீராரும் கடலுடுத்த எனும் தமிழ்த்தாய் வாழ்த்திலும் -
ஜனகணமன எனும் நாட்டுப் பாடலிலும் திராவிடம் எனும் சொல்லை எப்படித்தான் இவர்கள்
பொறுத்துக்கொண்டு நிகழ்ச்சிகளின்போது நிற்கின்றனரோ தெரியவில்லை - ஏனெனில் அந்த
அளவுக்கு திரா விடக் கட்சிகளைத் தீண்டினால் திருநீல கண்டம் என்று பேசத்
தொடங்கியிருக்கிறார்கள். பேசுவார்களே தவிர, திராவிடக் கட்சிகளிலொன்றைச் சார்ந்து
தான் இவர்களின் தேர்தல் கூட்டணிகளே வகுக்கப்படுகின்றன. </p><p>
</p><p>
ஒரு வார ஏடு - நாகரிக மாகக் கருத்துக்களையும், கட் டுரைகளையும் வௌியிடுகிற ஏடு - அதன்
வி.ஐ.பி. மேடையில் ஒருவர் நஞ்சு கலந்து எழுதியுள்ளார். அதாவது திராவிடக்
கட்சிகளால்தான் அந்தத் தலை வர்களால்தான் வீரப்பன்களே உருவாகியிருக்கிறார்கள் என்ற
பொருள்படக்கூறிவிட்டு, இத்த கைய தலைவர்களை வருங்கால வரலாறு காறித்துப்பும் என்று முடிவும்
கூறியுள்ளார். </p><p>
</p><p>
திராவிட திருவிளக்கு </p><p>
</p><p>
வரலாறு யாரைக் காறித் துப்பும் என்று சிறு குழந் தைக்குக் கூடத் தெரியும். அந்தக்
கட்டுரையாளரைப்போல கணத்திற்கொரு கட்சி மாறுகிற வர்களின் முகத்தில்தான் வரலாறு அந்த
அபிஷேகத்தை அதிவிமரிசையாகச் செய்யுமே தவிர உண்மையான திராவிடத் தலைவர்கள் மீதல்ல. </p><p>
</p><p>
தமிழர் கூட்டம் என்று கூறும்போது மொழியின் உணர்வையும் - திராவிடத் திருவிளக்கே என்று
கூறும் போது இனத்தின் பெருமையையும் நினைவு கூர்கிறோம். </p><p>
</p><p>
வரலாறு யாரைச் சபிக்கும் யாரை வாழ்த்தும் என்பதை விளக்கமாகத் தெரிந்துகொள் ளத்தான்
இந்த முப்பெரும் விழா. </p><p>
</p><p>
இவ்வாறு எழுதி உள்ளார். </p>

<p> </p>

<p>காளிமுத்து மிரட்டலுக்கு
பயப்பட மாட்டேன்: காங்கிரஸ் கூட்டணி இன்றி அ.தி.மு.க. வெற்றி பெற முடியாது: இளங்கோவன்
பதிலடி</p>

<p> </p>

<p>சென்னை, செப்.16- தமிழ்நாடு காங்கிரஸ் கமிட்டி தலைவர்
இளங்கோவன் சென்னையில் நேற்று நிருபர்களிடம் கூறியதாவது:- </p><p>
</p><p>
14-ந்தேதியுடன் கட்சி உறுப் பினர் சேர்ப்பு பணி முடி வடைந்து விட்டது. உறுப்பினர்
புத்தகம் வாங்கிய வர்கள் உரிய கட்டணத்துடன் மாவட்ட கமிட்டியிடம் கொடுக்க வேண் டும்.
அதில் ஏதாவது பிரச் சினை இருந்தால், நேரடியாக தமிழ்நாடு காங்கிரஸ் கமிட்டி யிடம் வருகிற
20-ந்தேதி கட்ட வேண்டும். </p><p>
</p><p>
சோனியாவுக்கு அழைப்பு </p><p>
</p><p>
மதச்சார்பற்ற கட்சிகள் எல்லாம் ஒன்றாக இணைந்து தந்தை பெரியார் பிறந்த நாள் விழாவை
கொண்டாடுவது வர வேற்கத்தக்கது. அந்த விழாவில் காங்கிரஸ் சார்பில் அகில இந்திய
காங்கிரஸ் தலைவி சோனியாவை, மூப்ப னார் தலைமையில் உள்ள குழு அழைக்க வேண்டும். இதை
மூப்பனாரிடமும் நான் சொல்லி இருக்கிறேன். இந்த விழா தொடர்பாக 18-ந்தேதி நடக்கும்
கூட்டத்துக்கு எங் களுக்கு அழைப்பு வந்து இருக் கிறது. அதில் இதை வலியுறுத்து வோம். </p><p>
</p><p>
கேள்வி:- பெரியார் பிறந்த நாள் விழாவில் நீங்கள் கலந்து கொள்வீர்களா? பதில்:-
காங்கிரஸ் கட்சி கண் டிப்பாக அந்த விழாவில் கலந்துகொள்ளும். </p><p>
</p><p>
புறக்கணிப்பா? </p><p>
</p><p>
கேள்வி:- இதுபோன்ற நிகழ்ச் சியை நீங்கள் புறக்கணிப்பது ஏன்? பதில்:- நான்
புறக்கணிக்க வில்லை. வேலையை பகிர்ந்து கொள்ள மற்ற நிர்வாகிகளை அனுப்புகிறேன். </p><p>
</p><p>
கேள்வி:- காளிமுத்து இனி ஜெயலலிதாவை விமர் சித்தால் இளங்கோவன் நடமாட முடியாது என்று
நேரடியாக உங்களை தாக்கி இருக்கிறாரே? பதில்:- அவருக்கு பதில் சொல்ல விரும்பவில்லை.
என்னை நடமாட முடியாது என்று கூறி இருக்கிறார். 100 வருடத்துக்கும் மேலாக தமிழ் நாட்டில்
எதிர்ப்புக்கும், வர வேற்புக்கும் இடையில் நடை போட்டு வருகிற குடும்பத்தை சேர்ந்தவன்
நான். இதற்கெல் லாம் பயப்பட மாட்டேன். தர்ம புரியில் பஸ்சில் சென்ற கல்லூரி
மாணவிகள் உயிருக்கு உத்தரவாதம் இல்லாத போது, நான் நடமாடினால் எனக்கு ஏதாவது ஆகிவிடுமோ
என்ற நல்லெண்ணத்தில் காளி முத்து அப்படி சொல்கிறார்போல தெரிகிறது. </p><p>
</p><p>
கையூட்டு பெறுவது யார்? </p><p>
</p><p>
கேள்வி:- நீங்கள் கருணாநிதி யின் கைக்கூலி ஆகிவிட்டீர்கள் என்றும் காளிமுத்து கூறி இருக்
கிறாரே? பதில்:- கைக்கூலி, கையூட்டு ஆகியவற்றுக்கு யார் சொந்தக் காரர்கள் என்பது
தமிழக மக்க ளுக்கு தெரியும். </p><p>
</p><p>
கேள்வி:- இப்படி கூட்டணி யில் மோதல் இருந்தால் தேர்தல் வெற்றியை பாதிக் காதா?
பதில்:- இது ராங் சிக்னல் தான். என்னை பொறுத்தவரை இதை எல்லாம் ஒரு பொருட் டாக
நினைக்கவில்லை. மத வெறி கும்பலை எதிர்க்கும் உணர்வோடு அனைவரும் ஒருங்கி ணைந்து வேலை
செய்ய வேண்டும் என்று தான் நான் நினைக்கிறேன். </p><p>
</p><p>
சும்மா இருக்க மாட்டோம் </p><p>
</p><p>
கேள்வி:- வீரப்பன் விவகா ரத்தில் நீங்கள்தானே ஜெயலலி தாவை முதலில் விமர்சித்து
மோதலை உருவாக்கினீர்கள்? பதில்:- காங்கிரஸ் முதல் வரை ராஜினாமா செய்யச்
சொன்னால் நாங்கள் பதில் சொல்லாமல் இருக்க முடியுமா? சோனியாவை விமர்சித்தால்
சும்மா இருக்க முடியுமா? </p><p>
</p><p>
கேள்வி:- கர்நாடக முதல்வர் கிருஷ்ணாகூட பதில் சொல்லும் முன் நீங்கள் சொல்லி
இருக்கிறீர் களே? பதில்:- காங்கிரஸ் அகில இந் திய கட்சி. யார் வேண்டுமானா லும்
பதில் சொல்லலாம். காங் கிரஸ் முதல்வரை ராஜினாமா செய்ய சொல்லி முதலில் பிரச் சினையை
இழுத்தது யார்? </p><p>
</p><p>
சின்ன விசயம் </p><p>
</p><p>
கேள்வி:- இளங்கோவனுடன் உள்ள பிரச்சினை சின்ன விஷ யம் என்று ஜெயலலிதா சொல்லி
இருக்கிறாரே? பதில்:- அது சின்ன விஷயம் தான். இதுபோன்ற சின்ன விஷய மும் வராமல்
பார்த்துக் கொள்ள வேண்டும். </p><p>
</p><p>
கூட்டணி ஆட்சி வரவேற்பு கேள்வி:- மூப்பனார் கூட்டணி ஆட்சி என்கிறார். ஜெய லலிதா,
தனித்துத்தான் ஆட்சி என்று இன்றுகூட கூறி இருக்கிறாரே? </p><p>
</p><p>
பதில்:- ஒவ்வொருவரும் ஒரு கருத்து சொல்கிறார்கள். தேர்தல் நேரத்தில்தான் முடிவு வரும்.
எங்களை பொறுத்த வரை கூட்டணி ஆட்சி என்ற மூப்பனார் கருத்தை வரவேற் கிறோம். </p><p>
</p><p>
அ.தி.மு.க. காரணம் </p><p>
</p><p>
கேள்வி:- 1998 தேர்தலில் ஒன்றும் இல்லாத காங்கிரசுக்கு, 1999 தேர்தலில் அ.தி.மு.க.
வுடன் கூட்டு சேர்ந்ததால்தானே 2 எம்.பி. கிடைத்தது. அப்படி இருக்கும்போது, அ.தி.மு.க.
வுக்கு எதிராக நீங்கள் செயல் படலாமா? பதில்:- கூட்டணிக்கு எதிராக நான் ஒன்றும்
சொல்லவில்லை. 1999 தேர்தலில் ஒருங்கிணைந்து உளப்பூர்வமாக வேலை செய்து இருந்தால் 35
சீட்டுக்கு மேல் வெற்றிபெற்று இருக்கலாம். உதாரணமாக தர்மபுரி, கிருஷ்ணகிரி,
பொள்ளாச்சி, நீலகிரி, திருச்செங் கோடு ஆகிய இடங்களில் தோல்விக்கு அ.தி. மு.க.வும்,
காங்கிரசும்தான் காரணம். இனிமேல் கூட்டணி என் பது, உளப்பூர்வமாக இந்திரா-
எம்.ஜி.ஆர்., ராஜீவ்- எம்.ஜி.ஆர்., ராஜீவ்- ஜெயலலிதா காலத்தில் இருந்ததுபோல அமைய
வேண் டும். கடந்த தேர்தலில் அப்படி உளப்பூர்வமாக வேலை செய்ய வில்லை. </p><p>
</p><p>
உளப்பூர்வமான உறவு </p><p>
</p><p>
கேள்வி:- இப்போது காங் கிரஸ் - அ.தி.மு.க. உறவு உளப் பூர்வமாக இருக்குமா? பதில்:-
முன்புபோல இருக் கக்கூடாது என்று நினைக் கிறேன். வாயால் கூட்டணி என் றால் மட்டும்
போதாது. உளப் பூர்வமாக இருக்க வேண்டும். இரண்டு கைகளும் தட்டினால் தான் சத்தம் வரும். </p><p>
</p><p>
கேள்வி:- இப்படிப்பட்ட கசப்புணர்வோடு தேர்தலை சந்தித்தால் சரியாக இருக்குமா?
பதில்:- இருக்கிற மனக் கசப்பை பேசி தீர்த்துக் கொண் டால் சரியாகிவிடும். கர்நாடக
முதல்வரை ராஜினாமா செய்ய சொன்னதால் வந்த வினை. அதை அவர்கள். சொல்லி இருக்க வேண்டிய
அவசியமே இல்லை. </p><p>
</p><p>
காங். தயவு </p><p>
</p><p>
கேள்வி:- த.மா.கா.வும், காங் கிரசும் சேர்ந்து ஏன் 3-வது அணி அமைக்கக்கூடாது?
பதில்:- இந்த நிமிடம் வரை 3-வது அணி அமைய வாய்ப்பு இல்லை. எதிர்காலத்தில் எப்படி
என்று சொல்ல முடியாது. </p><p>
</p><p>
கேள்வி:- தி.மு.க. அல்லது அ.தி.மு.க.வுடன் சேர்ந்தால் தான் காங்கிரஸ் வெற்றி பெற முடி
யுமா? பதில்:- 2 கட்சிகளுமே காங் கிரஸ் கூட்டு இருந்தால்தான் வெற்றிபெற முடிந்து இருக்
கிறது. திராவிட இயக்கங்கள் எப்போதும் காங்கிரஸ் துணை யோடு தான் வென்றுள்ளது. </p><p>
</p><p>
கேள்வி:- 1996 சட்டசபை தேர்தலில் தி.மு.க.-த.மா.கா. கூட் டணிதானே வெற்றி பெற்றது?
பதில்:- அது ஒரு தடவை தான். பலதடவை காங்கிரசுடன் கூட்டு சேரும் கட்சியே வெற்றி
பெற்றுள்ளது. இவ்வாறு கூறினார். </p>

<p> </p>

<p>புதிய நீதிக்கட்சி:
ஏ.சி.சண்முகம் தொடங்கினார்</p>

<p> </p>

<p>சென்னை, செப்.16- சென்னை தேனாம்பேட்டை காமராஜர் அரங்கில்
நேற்று அனைத்து முதலியார் பேரவை பொதுக்குழு கூட்டம் நடை பெற்றது. ஏ.சி.சண்முகம்
தலைமையிலும், ஜெ.சுத்தானந் தம் முன்னிலையிலும் கூட்டம் நடந்தது. </p><p>
</p><p>
அனைத்து முதலியார் பேரவையும், 10 இயக்கங்களும் சேர்ந்து புதிய கட்சியாக தொடங்குவது என முடிவு
எடுக்கப்பட்டது. இதை இறுதியாக பேசிய ஏ.சி.சண்முகம் அறிவித்தார். அவர் பேசியதாவது:- </p><p>
</p><p>
பிற்படுத்தப்பட்ட மக்களுக்கு சமூக நீதி கிடைக்க வில்லை. அவர்களுக்கு சமூக நீதி கிடைக்க
செய்ய வேண்டும் என்பதற்காக நாங்கள் புதிய கட்சி தொடங்கு கிறோம். </p><p>
</p><p>
அக்டோபர் 1-ந்தேதி, 2-ந்தேதி ஆகிய நாட்களில் சென்னையில் மாநில மாநாடு நடைபெறுகிறது.
அதை சிறப் பாக நடத்தி காட்டுவதை பொறுத்தே தேர்தலில் நம் எதிர் காலம்
தீர்மானிக்கப்படும். </p><p>
</p><p>
இவ்வாறு அவர் பேசினார். </p><p>
</p><p>
புதிய நீதிக்கட்சி </p><p>
</p><p>
பிறகு அவர் புதிய கட்சியின் பெயரை அறிவித்தார். னாபுதிய நீதிக்கட்சி இன்றைு முதல்
தொடங்கப்படுகிறது என்று கூறினார். அதற்கான கொடி யையும் வௌியிட்டார். அது கறுப்பு,
மஞ்சள், சிகப்பு கலரில் இருந்தது. </p><p>
</p><p>
புதிய கட்சி பெயரை ஏ.சி.சண்முகம் அறிவித்த போது னாபுதிய நீதிக்கட்சி என்றைு பெரிதாக
எழுதப்பட்ட ஒரு பதாகை வானத்திலிருந்து தொங்குவது போல இறங்கி வந்தது. தொண்டர்கள் மலர்
தூவி மகிழ்ச்சி ஆரவாரம் செய்தார்கள். மேளதாளம் முழங்கியது. பட்டாசு வெடித்தது. </p><p>
</p><p>
அண்ணா சிலைக்கு மாலை </p><p>
</p><p>
பிறகு ஏ.சி.சண்முகம், ஜெ.சுத்தானந்தம் உள்பட தலைவர்கள் அண்ணா சாலை யில் உள்ள அண்ணா
சிலைக்கு சென்று மாலை அணிவித்து மரியாதை செலுத்தினார்கள். </p><p>
</p><p>
இந்த மாநாட்டில் சி.எஸ். சண்முகசுந்தர முதலியார் பேசும்போது, னாநெசவாளர்கள், விவசாயிகள்,
தொழிலாளர் கள் என்று அனைத்து தரப்பு உழைக்கும் வர்க்க மக்கள் நலன் காக்கவே இந்த
கட்சி தொடங் கப்பட்டு இருக்கிறதுனா என்று கூறினார். மற்றும் செயற்குழு வில் ஐசரி கணேஷ்,
முன்னாள் எம்.எல்.ஏ. விழுப்புரம் பழனியப்பன், கோடம்பாக்கம் குமார், மதுரை முத்து நல்ல
தம்பி, என்.வி.என். செல்வம் ஆகியோரும் பேசினார்கள். </p>

<p> </p>

<p>தி.மு.க. அணிக்கு த.மா.கா.
வந்தால் வரவேற்பேன் -ராமதாஸ்</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>




 
  
  <p>ராமதாஸ் </p>
  
 




<p>மதுரை, செப். 16- பாட்டாளி மக்கள் கட்சி நிறுவனர் டாக்டர்
ராமதாஸ் மதுரையில் நிருபர்களிடம் கூறியதாவது:- </p><p>
</p><p>
வாக்காளர் அட்டை </p><p>
</p><p>
வாக்காளர் அடையாள அட்டையை முறையாக எடுத்து விநியோகிக்க முடியாது. அதில் குழப்பம்தான்
ஏற்படும். அடை யாள அட்டையை இந்த தேர்த லில் அமுல்படுத்த முடியுமா? என்பது தெரியாது.
எனவே வாக்காளர் அட்டை இருந்தால் தான் வாக்களிக்க முடியும் என்பதை தேர்தல் கமிஷனர்
தள்ளுபடி செய்ய வேண்டும். </p><p>
</p><p>
சபாநாயகர் முயற்சி </p><p>
</p><p>
இந்த தேர்தலில் தி.மு.க. அணியில் த.மா.கா.வை சேர்க்க சபாநாயகர் பி.டி.ஆர்.பழனி
வேல்ராஜன் முயற்சி எடுத்து வருகிறார் என்று கேள்விப்படு கிறேன். எந்த அணியில் யார்
சேர்ந்தாலும்எங்களுக்கு கவலை இல்லை. பா.ம.க. தொடர்ந்து தி.மு.க. தலைமையிலான அணி யில்
இருக்கும். இந்த அணியில் எந்த கட்சி சேர்ந்து இருந்தாலும் எங்களுக்கு ஆட்சேபனை கிடை யாது.
அது குறித்து தமிழக முதல்வர்தான் முடிவு செய்ய வேண்டும். </p><p>
</p><p>
ஆட்சியில் பங்கு </p><p>
</p><p>
தமிழ்நாட்டில் கூட்டணி ஆட்சியில் பங்கு கேட்க மாட் டோம். அவர்கள் தர முன் வந் தாலும்
அதை ஏற்க மாட்டோம். தனித்து ஆட்சிதான் வரும். தனித்து ஆட்சி வராவிட்டால் அவர்களுக்கு
நாங்கள் வௌி யில் இருந்து ஆதரவு தருவோம். </p><p>
</p><p>
தேர்தலில் எங்கள் கட்சிக்கு எத்தனை சீட்டுக்கள் என்பது குறித்து தேர்தல் நேரத்தில் பேசு
வோம். த.மா.கா., தி.மு.க. அணிக்கு வந்தால் வரவேற் பேன். பா.ம.க., அ.தி.மு.க. அணி
யில் சேர வாய்ப்பு உள்ளது என்று அ.தி.மு.க.வினர் பேசி வருவது அவர்களது விருப்பத் தைக்
காட்டுகிறது. </p><p>
</p><p>
தீர்ப்பு வரும் </p><p>
</p><p>
தமிழ்நாட்டில் தற்போது ஜாதிக் கட்சிகள் உருவாகி வரு கிறது. இவைகளுக்கான தீர்ப்பு
சட்டமன்ற தேர்தலுக்கு பிறகு தெரிய வரும். </p><p>
</p><p>
தி.மு.க. ஆட்சியை மேடை களில் நான் விமர்சித்து வருவ தாக கூறுவது தவறு. வளர்ச்சி
திட்டங்கள், நலிந்த பிரிவினருக் கானஉதவி, அடிப்படை தேவை களை நிறைவேற்ற அதிகாரிகள்
அதிக அக்கறை எடுத்து பாடு படவில்லை என்று தான் பேசி வருகிறேன். இதே குற்றச்சாட்டை பல
அமைச்சர் களும் வௌிப் படையாக கூறி வருகிறார்கள். </p><p>
</p><p>
மோதலை தவிர்ப்பீர் </p><p>
</p><p>
தாழ்த்தப்பட்ட சமுதாயத் துக்கு பாடுபடும் தலைவர்கள் பிற சமுதாயத்தினருடன் சண்டை போடுவதை
விட்டு விட்டு சமுதாயத்தை முன்னேற செய்யும் காரியங்களில் அதிக அக்கறை காட்ட வேண்டும். </p><p>
</p><p>
தாழ்த்தப்பட்ட சமுதாய உரிமைகளுக்கு பாடுபட வரும் அனைத்து சக்திகளையும் தாழ்த் தப்பட்ட
சமுதாயம் பயன்படுத்தி கொள்ள வேண்டும். </p><p>
</p><p>
மணிமண்டபம் </p><p>
</p><p>
தேவேந்திர குல மக்களின் தலைவர் தியாகி இம்மானுவேல் நினைவு மண்டபத்தை அவரது கல்லறை
உள்ள பரமக்குடியில் அமைக்கும் முயற்சியில் பா.ம.க. கடந்த 4 மாதமாக ஈடுபட்டு வருகிறது. இந்த
மணிமண்டபம் இந்த ஆண்டு இறுதிக்குள் கட்டி முடிக்கப்படும். </p><p>
</p><p>
மணிமண்டப திறப்பு விழா வுக்கு அனைத்து தலைவர்கள், சமுதாய-சமய தலைவர்களை அழைப்போம். </p><p>
</p><p>
பாதிக்க கூடாது </p><p>
</p><p>
நடிகர் ராஜ்குமாரை சந்தன கடத்தல் வீரப்பன் உடனடியாக விடுதலை செய்ய வேண்டும். அவரை
விடுதலை செய்த பிறகு கர்நாடக மாநிலத்தில் வசிக்கும் ஒரு தமிழன் கூட பாதிப்பு அடைய கூடாது.
இதற்கான முழு பொறுப்பை கர்நாடக முதல மைச்சர் ஏற்க வேண்டும். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். பேட்டியின்போது பா.ம.க. துணை பொதுச்செயலாளர் முருகவேல்ராஜன்
உடனிருந்தார். </p>

<p> </p>

<p>மறியல் போராட்டம்
ஏன்? மூப்பனார் விளக்கம்</p>

<p> </p>

<p>சென்னை, செப்.16- த.மா.கா.வினருக்கு அதன் தலைவர் மூப்பனார்
எழுதியுள்ள கடிதத்தில் கூறி இருப்ப தாவது:- </p><p>
</p><p>
சிதம்பரம் செயற்குழுவின் தீர்மானங்களை செயல்படுத் தும் வகையில் மத்திய- மாநில அரசுகளின்
மக்கள் விரோத போக்கினை எதிர்த்து மக்கள் நலனுக்காக 15 அம்ச கோரிக் கைகளை முன்
வைத்து மாபெரும் அற வழி மறியல் போராட்டத்தினை 234 சட்ட மன்ற தொகுதிகளிலும் வரு கின்ற
27-9-2000 அன்று நடத்துவதாக அறிவித்துள்ளோம். </p><p>
</p><p>
அற வழி போர் </p><p>
</p><p>
தமிழ் மாநில காங்கிரஸ் ஒரு பொறுப்புள்ள எதிர் கட்சியாக சட்டமன்றத்தில்,
நாடாளுமன்றத்தில், மக்கள் மன்றங்களில், அரசியல் மேடைகளில் கடந்த நான்கு ஆண்டு களாக சுட்டிக்காட்டி
வந்த மத்திய- மாநில அரசுகளின் தவறுகள், குறைபாடுகள், ஏழை எளிய மக்களின் முதுகொடிக் கும்
எஜமானத்தனம், முடங்கி போன சட்டத்தின் ஆட்சி போன்றவைகளை அரசு ஏற்று கொண்டு சரி
செய்யாததினால் பாராளுமன்ற ஜனநாயகம் அங்கீகரித்துள்ள இந்த அறவழி மறியல் போராட்டத்
தினை நாம் மேற்கொண் டுள்ளோம். </p><p>
</p><p>
மத்திய- மாநில அரசுகள் மக்கள் மீது ஒரு மிக பெரிய விளம்பரயுத்தத்தினை நடத்து கின்றன. </p><p>
</p><p>
அதனால் சமூகத்தின் அனைத்து தர மக்களும் பெரி தும் பாதிக்கப்பட்டுள்ளார்கள். விவசாயிகள்,
தங்கள் விளை பொருட்களுக்கு சரியான விலை இல்லாமலும், இடு பொருளுக்கு அதிக விலை கொடுத்தும்
கடன் சுமையாலும் பெரும் துன்பத் திற்கு உள்ளாகியுள்ளனர். </p><p>
</p><p>
இந்த விளம்பர மாய்மாலத்தால் தமிழக மக்கள் ஏமாந்து விடவில்லை என்பதனையும்,
பாதிக்கப்பட்டவர்களுக்கு நியா யம் வழங்க தமிழ் மாநில காங்கிரஸ் தன்னால் இயன்ற
அனைத்தையும் செய்ய தயாராக இருக்கின்றது என்பதனையும் தமிழக மக்களுக்கு எடுத்து காட்டவே
இந்த அற போராட்டம். </p><p>
</p><p>
ஜனநாயக முயற்சி </p><p>
</p><p>
இந்த அற போராட்டம் தமிழகம் முழுவதும் ஒரு விழிப்புணர்வினை ஏற்படுத்த வேண்டும்
என்பதற்காகவே 234 சட்டமன்ற தொகுதிகளிலும் இந்த ஜனநாயக முயற்சி நடைபெற வேண்டும் என்று
நமது செயற்குழு முடிவெடுத்துள்ளது. </p><p>
</p><p>
சராசரி மனிதனது நலன் பேண நாம் முன்வைக்கின்ற இந்த அற போராட்டத்தினை போன்றதொரு
சாத்வீக எதிர்ப்பினை தமிழக வரலாறு இது வரை சந்தித்ததில்லை என்று சொல்லும் வகையில்
தமிழகத்தின் சிறை சாலைகள் அத்தனையையும் நமது தொண்டர்களும், பொது மக்களும் நிரப்பிட
வேண்டும். ஒவ்வொரு சட்ட மன்ற தொகுதியிலும் ஆயிர கணக்கானோர் இந்த போராட்டத்தில்
கலந்து கொண்டனர் என்ற செய்தி கேட்டாலே ஆளும் வர்க்கம் தனது தவறுகளை திருத்த
முயற்சிக்கும் என்று நான் நம்புகிறேன். திருந்த மறுப்பவர்களை ஜனநாயக வழியில் திருத்தும்
பொறுப்பு நமக்குண்டு. </p><p>
</p><p>
இந்த அற போராட்டத்தில் கலந்து கொள்கின்ற நமது போராட்ட வீரர்கள் நமது இயக்கம்
காந்திய சிந்தனையில் காமராஜர் வழியில் உருவானது என்பதினை நிரூபிக்கின்ற வகை யில் நமது
போராட்டத்தில் எந்த வன்முறையும், அரசியல் அநாகரிகமும் இருக்க கூடாது என்பதனை உங்களுக்கு
மிகவும் கண்டிப்பாக தெரிவித்து கொள்கிறேன். இவ்வாறு கூறியுள்ளார். </p>

<p> </p>

<p>வீரப்பன் மனைவி
முத்துலட்சுமி போடியில் போட்டியிடுகிறார்: திருமணத்துக்கு வந்தவர் பரபரப்பு பேட்டி</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>




 
  
  <p>முத்துலட்சுமி </p>
  
 




<p>தேனி, செப்.16- தேனி மாவட்ட தமிழ் தேசிய இயக்க தலைவர்
ராஜாராம் மகள் ரேணுகாதேவிக்கும், பால கிருஷ்ணனுக்கும் போடியில் உள்ள இல்லத்துப்
பிள்ளைமார் சங்க கட்டிடத்தில் நேற்று திரு மணம் நடைபெற்றது. இதில் கலந்து கொள்ள
சந்தனக்கடத் தல் வீரப்பனின் மனைவி முத்துலட்சுமி நேற்று முன் தினம் மாலை போடி வந்தார்.
அவருடன் மேட்டூர் ஒர்க்ஷாப் காலனியை சேர்ந்த அவரது அக்காள் மகன் தங்கம் (வயது 24)
வந்திருந்தார். </p><p>
</p><p>
பேட்டி </p><p>
</p><p>
அப்போது முத்துலட்சுமி நிருபருக்கு அளித்த பேட்டியில் கூறியுள்ளதாவது:- </p><p>
</p><p>
எனது கணவர் வீரப்பனால் ராஜ்குமார் கடத்தப்பட்ட பிறகு மேட்டூரில் இருந்த எனக்கும் எனது
மகள் விஜயலட்சுமிக்கும் பல தொல்லைகள் ஏற்பட்டன. போலீஸ்காரர்களும், பத்திரிகை
யாளர்களும் வந்து தொந்தரவு செய்தனர். இதனால் நிம்மதி இழந்த நான் அங்கு இருந்து வேறு ஒரு
ஊருக்கு சென்று விட் டேன். இரண்டு மூன்று நாட் கள் தான் மேட்டூரில் இல்லாமல் இருந்தேன்.
மீண்டும் தற்போது மேட்டூருக்கு வந்து விட்டேன். </p><p>
</p><p>
போலீஸ் தொல்லை </p><p>
</p><p>
கடைசியாக கோத்தகிரியில் தங்கி இருந்த போது போலீஸ் தொல்லை காரணமாக வீரப் பனை
விட்டு பிரிந்தேன். அவரை கடந்த 8 வருடங்களாக இதுவரை நான் சந்திக்க வில்லை. ஆனால் என்
மீது அவருக்கு அளவு கடந்த பாசம் உண்டு. நான் எனது வயதிற்கு படாத கஷ்டங்கள் இல்லை.
துன்பங்கள் இல்லை. அவ்வள வையும் அனுபவித்து விட்டேன். அதை விட இனி ஒன்றும் கஷ்டம் வரப்
போவது இல்லை. நான் கஷ்டப் பட்ட சூழ்நிலையில் தேனி மாவட்ட தமிழ் தேசிய இயக்கத்தைச்
சேர்ந்த வர்கள் எனக்கு ஆறுதல் கூறினார்கள். </p><p>
</p><p>
பொது வாழ்க்கை </p><p>
</p><p>
முதல் முறையாக பொது பிரச்சனைக்காக மேட்டூரில் தலை காட்டிய நான் இப்போது இந்த திருமண
விழாவில் கலந்து கொள்ள வந்து இருக்கிறேன். எனக்கு இது சந்தோஷ மாக இருக்கிறது. இதையெல்
லாம் பார்க்கும் போது பொது வாழ்க்கைக்கு வரவேண்டும் என ஆர்வம் ஏற்படுகிறது. </p><p>
</p><p>
தேர்தலில் போட்டி </p><p>
</p><p>
வீரப்பன் மகள் என்பதால் பள்ளியில் படிப்பதற்கு கூட கஷ்டம் ஏற்பட்டது. எனவே நான்
பொதுவாழ்க்கையில் ஈடுபட்டு சட்டமன்ற உறுப்பினர் போன்ற முக்கிய பொறுப்புக்கு வந்தால்
இது போன்ற கெட்ட பெயர் மாறும். அதற்காகவாவது நான் தேர்தலில் போட்டியிடுவேன். </p><p>
</p><p>
நான் இங்கு போட்டியிட வேண்டும் என்று தமிழ் தேசிய இயக்கத்தினர் கேட்டுக் கொண்டனர்.
இதன் அடிப்படையில் போடியில் போட்டியிட்டு மக் களை சந்தித்து ஓட்டு கேட்டு வெற்றி பெறுவேன்.
இவ்வாறு அவர் கூறினார். </p>

<p> </p>






</body></text></cesDoc>