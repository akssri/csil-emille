<cesDoc id="tam-w-dinakaran-politics-03-04-19" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-politics-03-04-19.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-04-19</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-04-19</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>மாவட்டம் முழுவதும்
எழுச்்்்்்சிஅலை-உற்சாக வரவேற்பு ஏற்பாடு: பரபரப்பான அரசியல் சூழ்நிலையில் கருணாநிதி
நாளை வேலூர் வருகை பிரமாண்ட பொதுக்கூட்டத்தில் பேசுகிறார் </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>கருணாநிதி </p>

<p> </p>

<p>வேலூர்்,ஏப்.19- பரபரப்பான அரசியல் சூழ்்நிலையில் தி.மு.கழக
தலைவர் கருணாநிதி இன்று வேலூர் வருகிறார். இங்கு பிரமாண்ட பொதுக்கூட்டத்தில் அவர்
பேசுகிறார். கருணாநிதி வருகையை முன்்னிட்டு மாவட்டம் முழுவதும் எழுச்சி அலை வீசுகிறது. அவருக்கு
உற்சாக வரவேற்பு அளிக்கிறார்்கள். </p><p>
</p><p>
கருணாநிதி </p><p>
</p><p>
அ.தி.மு.க அரசின் மக்கள் விரோத போக்கை கண்டித்து வேலூர் நகராட்சி பயணியர் விடுதி
அருகில் இன்று (சனிக்கிழமை) மாலை 6மணி க்கு தி.மு.க பிரமாண்ட பொதுக் கூட்டம் நடக்கிறது.
பரபரப்பான அரசியல் சூழ்நிலையில் நடை பெறும் இந்தப் பொது க்கூட்்டத்தில் தி.மு.கழக
தலைவர் கருணாநிதி கலந்து கொண்டு சிறப்புரை ஆற்று கிறார். </p><p>
</p><p>
தலைமை </p><p>
</p><p>
கூட்டத்துக்கு வேலூர்்கிழக்கு மாவட்ட தி.மு.க செயலாளர் ஆர்.காந்தி தலைமை தாங்குகிறார்.
தலைமை கழக தேர்தல் பணிக் குழு செயலாளர் முகமது சகி ,திருவண்ணாமலை தெற்கு மாவட்ட செயலாளர்
எ.வ.வேலு எம்.எல்.ஏ ,வேலூர் மேற்கு மாவட்ட செயலாளர் ஆர். மகேந்திரன் ,திருவண்ணாமலை
வடக்கு மாவட்ட செயலாளர் ஆர்.சிவானந்தம்் ஆகியோர் முன்னிலை வகிக்கிறார்கள். வேலூர்
நகர தி.மு.க பொறுப்பாளர்கள் எஸ்.பூ பாலன், இரா .வில்வநாதன் , எம்.நூருல்லா ஆகியோர்
வரவேற்று பேசுகிறார்கள். </p><p>
</p><p>
வீராசாமி-துரைமுருகன் </p><p>
</p><p>
கூட்டத்தில் தி.மு.கழக பொருளாளர் ஆற்காடு வீரா சாமி, வேலூர் மண்டல தி.மு.க பொறுப்பாளர்
-முன்னாள் அமைச்சர் துரை முருகன் ,முன்ன்ாள் அமைச்சர்கள் க.பொன்முடி, கு.பிச்சாண்டி மற்றும்
கட்சி முன்னோடிகள் கலந்து கொண்டு பேசு கிறார்கள். முடிவில் வேலூர்் கிழக்கு மாவட்ட
தி.மு.க பொருளாளர் பி.என்.சுப்பிரமணி நன்றி கூறுகிறார். </p><p>
</p><p>
தொல்காப்பியபூங்கா </p><p>
</p><p>
நாளை (ஞாயிறு) காலை 9மணி முதல் பகல் 12மணி வரை வேலூர் சத்துவாச்சாரி கிருஷ்ணா
மகால் திருமண மண்டபத்தில் தி.மு.கழக தலைவர் கருணாநிதி எழுதிய தொல்காப்பிய பூங்கா
புத்தகம் வௌியீட்டு விழா நடக்கிறது. இந்தப்புத்தகத்தை கருணாநிதி வௌியிடுகிறார். இதை
கட்சி பிரமுகர்கள் மற்றும் தொண்டர்கள் , பொதுமக்கள் பெறுகிறார்கள். </p><p>
</p><p>
எழுச்சி மிகு வரவேற்பு </p><p>
</p><p>
் தி.மு.கழக தலைவர் கருணாநிதி வேலூர் வருகை முன்னிட்டு மாவட்டம் முழு வதும் அவருக்கு எழுச்சி
மிகு வரவேற்பு அளிக்கப்படுகிறது. மாவட்டம் முழுவதும் வரவேற்பு பேனர்்கள் மற்றும் அலங்கார
வளைவுகள் கொடி தோரணங்கள் அமைக ்கப்பட்டு உள்ளன. வேலூர் நகரமே விழாக்கோலம் பூண்டு
உள்ளது. </p><p>
</p><p>
காந்தி அறிக்கை </p><p>
</p><p>
வேலூர்கிழக்குமாவட்ட தி.மு.க செயலாளர் ஆர்.காந்தி வௌியிட்்டுள்ள அறிக்கையில்
கூறிஇருப்பதாவது- </p><p>
</p><p>
தமிழன்்்னைக்கு தன் படைப்பாலே பல மகுடம்சூட்டி மகிழ்ந்்திட்ட தமிழ் ஆய்ந்த தலைவர்
ஐந்தமிழ் அறிஞர் தலைவர் கலைஞர் அவர்கள் தமிழ் இலக்்கண நூலாம்
தொல்காப்பியத்துக்கு பாமரரும் படித்்து புரிந்்திடும் வண்ணம் எழுதிய தொல்காப்பிய பூங்கா
நூல் வௌியிட்டு ,இன்று பல பதிப்பு வௌியிட்டு நூல் விற்பனையில் சாதனை நிகழ்த்துகிறது. </p><p>
</p><p>
பயன்பெற்றிட </p><p>
</p><p>
பல மாவட்டங்களில் தலைவர் கலைஞர் வௌியிட்்டு மகிழ்ந்தார் .நமது மாவட்டத்தில்
தொல் காப்பிய பூங்கா நூல் நாளை (ஞாயிறு) காலை 9மணி முதல் பகல் 12மணி வரை
வேலூர் சத்துவாச்சாரி கிருஷ்ணா மகால் திருமண மண்டபத்தில் தமிழின தலைவர் இலக்கிய கடல; fiy"h;; mth;fs; jdJ bghw;fu';fshy;
tH';Ffpd; whh;. vdnt fHf cld; gpwg;g[fSk; jkpH; Mh;tyh;fs; /bghJkf;fs;
bjhy;fhg;gpa g{';fhE}iy bgw;W gad;bgw;wpl nfl;Lf;bfhs;fpnwd;. </p><p>
</p><p>
,t;thW mjpy; mth; Twp cs;shh;. </p>

<p> </p>

<p> </p>

<p>தமிழ்நாட்டில் பொடா
துஷ்பிரயோகமா? ஆய்வு குழு விசாரிக்கும்மத்திய உள்துறை அதிகாரி பேட்டி </p>

<p> </p>

<p>சென்னை, ஏப். 19- மத்திய உள்துறை செயலாளர் என்.கோபாலசாமி
நிகழ்ச்சி ஒன்றில் கலந்து கொள்வதற் காக சென்னை வந்தார். அப்போது . அவரை நிருபர்கள் பேட்டி
கண்டனர். அதன் விவரம் வருமாறு:- </p><p>
</p><p>
கேள்வி:- தமிழகத்தில் அரசியல்வாதி, பத்திரிகையாளர் மீது பொடா சட்டம் தவறாக
பயன்படுத்தப்பட்டு உள்ளதே? </p><p>
</p><p>
புகார்கள் </p><p>
</p><p>
பதில்:- இதுபோன்ற புகார்கள் சில மாநிலங்களில் இருந்து மத்திய அரசுக்கு வந்து உள்ளது.
பொடா மறுஆய்வு கமிட்டி அமைக்கப்படும் என்று துணை பிரதமர் அத்வானி பாராளுமன் றத்தில்
அறிவித்தார். ஓய்வு பெற்ற பஞ்சாப்,அரியானா ஐகோர்ட்டு நீதிபதி அருண் சசரியா
தலைமையில் 3 உறுப்பினர் கொண்ட கமிட்டி அமைக்கப்பட்டு உள்ளது. அந்த கமிட்டி விரைவில்
செயல்பட தொடங்கும். பொடா சட்டம் தவறாக பயன்படுத்தப்படுவதாக கூறப்படும் புகார்கள் அந்த
கமிட்டிக்கு அனுப்பி வைக்கப்படும். </p><p>
</p><p>
கேள்வி:- தமிழ்நாட்டில் 356-வது பிரிவை (ஆட்சி கலைப்பு) பயன்படுத்தும் சூழ் நிலை
உள்ளதா? </p><p>
</p><p>
பதில்:- இதுபோன்ற அரசியல் சம்பந்தப்பட்ட கேள்விகளுக்கு பதில் அளிக்க விரும்ப வில்லை. </p><p>
</p><p>
நள்ளிரவு கைது </p><p>
</p><p>
கேள்வி:- நள்ளிரவு கைது பற்றி...? </p><p>
</p><p>
பதில்:- இதுபற்றி சுப்ரீம் கோர்ட்டு ஏற்கனவே வழிகாட்டு நெறிமுறை கூறி உள்ளது. மத்திய அரசு
புதிதாக வழி காட்டு நெறி முறை அளிக்க தேவை இல்லை. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p> </p>

<p>ஸ்டாலின் விடுதலை ஆகாமல்
தடுக்க போலீஸ் திடீர் மனு 21-ந் தேதி ஆஜர்படுத்த மாஜிஸ்திரேட் உத்தரவு </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>ஸ்டாலின் </p>

<p> </p>

<p>சென்னை,ஏப்.19- ஸ்டாலினை விடுதலை ஆகாமல் தடுக்க போலீஸ் திடீர்
மனு தாக்கல் செய்து உள்ளது. வருகிற 21-ந் தேதி ஸ்டாலினை ஆஜர்படுத்த மாஜிஸ்திரேட்
உத்தரவிட்டு உள்ளார். </p><p>
</p><p>
வழக்கு </p><p>
</p><p>
ராணிமேரி கல்லூரிக்குள் அத்துமீறி நுழைந்ததாக போலீசார் ஸ்டாலின் உள்பட 6
எம்.எல்.ஏ.க்கள் மீது போலீசார் வழக்கு பதிவு செய்து உள்ளனர். இந்த வழக்கில் 6
பேரையும் போலீசார் கைது செய்து சிறையில் அடைத்தனர். சிறையில் உள்ள 6 பேரையும்
விடுதலை செய்யக்கோரி வக்கீல் ராஜஇளங்கோ ஜாமீன் மனு தாக்கல் செய்தார். இந்த மனுவை
சென்னை எழும்பூர் கோர்்ட்டு மாஜிஸ்திரேட் விசாரித்து 6 பேரையும் ஜாமீனில் விடுதலை
செய்து உத்தரவிட்டார். </p><p>
</p><p>
போலீசார் திடீர் மனு </p><p>
</p><p>
இதை தொடர்ந்து ஸ்டாலின் உள்பட 6 பேரும் ஜாமீனில் விடுதலை ஆக இருந்தனர். இந்த நிலையில்
ஸ்டாலின் மீது போலீசார் புதிய வழக்கை பதிவு செய்து உள்ளனர். அதில் ஸ்டாலினை கைது
செய்ய அவரது வீட்டிற்கு சென்றபோது போலீசாரை தாக்கி கொலை செய்ய முயற்சி செய்து உள்ளனர்
என்று கூறி இருந்தனர். இதனால் வேளச்சேரி போலீசார் ஸ்டாலின் உள்பட 100 பேர் மீது கொலை
முயற்சி வழக்கை பதிவு செய்து உள்ளனர். இந்த வழக்கில் போலீசார் ஸ்டாலினை அவசர
அவசரமாக கடலூர் சிறைக்கு சென்று கைது செய்து அதற்கான ஆதாரத்தை ஸ்டாலினிடம் கொடுத்தனர்.
இதனால் ஸ்டாலின் ஜாமீன் கேட்டு சென்னை செசன்சு கோர்்ட்டில் மனு தாக்கல் செய்து
உள்ளார். அந்த மனு வருகிற 21-ந் தேதி மீண்டும் விசாரணைக்கு வருகிறது. </p><p>
</p><p>
திடீர் மனு </p><p>
</p><p>
இந்த நிலையில் ஸ்டாலினை ராணிமேரி கல்லூரி வழக்கில் இருந்து கோர்ட்டு விடுதலை செய்து
தீர்ப்பு கூறியது. இதனால் ஸ்டாலின் உள்பட 6 பேரும் வருகிற 21-ந் தேதி ஜாமீனில்
விடுதலையாகிறார்கள். இதில் ஸ்டாலின் விடுதலை ஆவதை தடுக்க போலீசார் திட்டமிட்டு சென்னை
சைதாப்பேட்டை மாஜிஸ்திரேட் கோர்ட்டில் ஓரு மனு தாக்கல் செய்தனர். இந்த மனுவில்
போலீசார் கூறி இருப்பதாவது:- </p><p>
</p><p>
ஸ்டாலினை கைது செய்ய போலீசார் சென்றபோது அவரும் அவர்களது கட்சியின் தொண்டர்களும்
ஒன்று சேர்ந்து போலீசாரை தாக்கி உள்ளனர். போலீசாரை கொலை செய்ய முயற்சி செய்து
உள்ளனர் . எனவே ஸ்டாலினை இந்த வழக்கில் கைது செய்து உள்ளோம். அவரை 15 நாள் நீதிமன்ற
காவலில் வைக்க கோர்ட்டு உத்தரவிட வேண்டும். </p><p>
</p><p>
இவ்வாறு மனுவில் கூறி இருந்தனர். </p><p>
</p><p>
உத்தரவு </p><p>
</p><p>
இந்த மனு மாஜிஸ்திரேட் ரவீந்திரன் முன்பு விசாரணைக்கு வந்தது. அப்போது அரசு வக்கீல்
ஆஜராகி 15 நாள் ஸ்டாலினை நீதிமன்ற காவலில் வைக்க அனுமதி தர வேண்டும் என்று கூறினார்.
இதற்கு ஸ்டாலின் தரப்பில் ஆஜரான வக்கீல் குமரேசன் கடும் எதிர்ப்பு தெரிவித்து
வாதாடினார். அவர் கூறும்போது ஸ்டாலின் விடுதலை யாவதை தடுக்க போலீசார் இப்படி பொய்
வழக்கை ஜோடித்து பதிவு செய்து உள்ளனர். தற்போது இதில் காவலில் வைக்க கோர்ட்டு
அனுமதிக்ககூடாது என்று கூறினார். இரு தரப்பு வாதங்களையும் கேட்ட மாஜிஸ்திரேட் ரவீந்திரன்
வருகிற 21-ந் தேதி ஸ்டாலினை சைதாப்பேட்டை 9-வது கோர்ட்டில் ஆஜர்படுத்த வேண்டும் என்று
போலீசாருக்கு உத்தரவிட்டார். ஆகவே கடலூர் சிறையில் உள்ள ஸ்டாலின் வருகிற 21-ந் தேதி
காலை 10.30 மணிக்கு சென்ை ைகொண்டு வரப்பட்டு அன்றைக்கு சைதாப்பேட்டை கோர்ட்டில்
ஆஜர்படுத்தப்படுகிறார். இந்த நிலையில் சென்னை செசன்சு கோர்ட்டில் ஸ்டாலின் தாக்கல்
செய்த மனு வருகிற 21-ந் தேதி விசாரணைக்கு வருகிறது. எனவே ஸ்டாலின் விடுதலை ஆவாரா என்பது
இன்று மாலை தெரிந்துவிடும். </p><p>
</p><p>
5 எம்.எல்.ஏ.க்கள் வருகிற 21-ந் தேதி விடுதலை ஆகிறார்கள் </p><p>
</p><p>
ராணிமேரி கல்லூரி விவகாரம் தொடர்பாக கைதான ஸ்டாலின் உள்பட 6 எம்.எல்.ஏ.க்களையும்
ஜாமீனில் விடுதலை செய்து சென்னை எழுப்பூர் மாஜிஸ்திரேட் உத்தரவிட்டார். இந்த உத்தரவு
நேற்று முன்தினம் மாலை 6 மணிக்கு பிறப்பிக்கப்பட்டதால் நேற்றும் ஜாமீனில் விடுதலை
செய்யப்பட முடியவில்லை. இவர்களுக்கான ஜாமீன் பத்திரம் வருகிற 21-ந் தேதி எழும்பூர்
கோர்ட்டில் தாக்கல் செய்கிறார்கள். அன்றைக்கு கோர்ட்டு அனுமதி பெற்று அன்று மாலை
பொன்முடி உள்பட 5 எம்.எல்.எ.க்கள் மட்டும் ஜாமீனில் விடுதலை ஆவார்கள் என்று தெரிகிறது. </p>

<p> </p>

<p> </p>

<p>இந்து முன்னணி ராமகோபாலன்
பேட்டி:2 குழந்தைக்கு மேல் பெற்றால்ஓட்டுரிமை பறிக்க வேண்டும் </p>

<p> </p>

<p>திருவைகுண்டம், ஏப். 19- 1 குழந்தைக்கு மேல் பெற்றால் அவர்களது
ஓட்டுரி மையை பறிக்க வேண்டும் என இந்து முன்னணி ராமகோபாலன் கூறியுள்ளார். </p><p>
</p><p>
சழனயழூழமழழு; </p><p>
</p><p>
திருவைகுண்டம் இந்து வித்யாலயா பள்ளியில் இந்து முன்னணி பயிற்சி முகாமை மாநில இந்து
முன்னணி அமைப்பாளர் ராம.கோபாலன் தொடங்கி வைத்தார். அப்போது அவர் நிருபர்களுக்கு
பேட்டி அளித்தார். அவர் கூறியதாவது:- </p><p>
</p><p>
தமிழ்நாட்டை பின்பற்றி குஜராத்தில் மதமாற்ற தடை சட்டம் கொண்டு வந்ததை
பாராட்டுகிறோம். நாடு முழுவதும் மத மாற்ற தடைச்சட்டத்தை கொண்டு வர வேண்டும். ஏனென்றால்
தமிழ்நாட்டிலிருந்து கேரளா, பாண்டிச்சேரி ஆகிய பக்கத்து மாநிலங்களுக்கு சென்று மதமாற்றம்
செய்கிறார்கள். பிரதமர் வாஜ்பாய் 5 ஆண்டு காலம் வெற்றிகரமாக ஆட்சியை
நடத்தியுள்ளார். வாஜ்பாய், அத்வானி, வெங்கையா நாயுடு ஆகியோருக்கு இன்னும் 6
மாதங்களுக்குள் 3 கோரிக்கைகளை தேசிய ஜனநாயக கூட்டணி அரசு நிறைவேற்ற வேண்டும் என்று கூறி
உள்ளோம். </p><p>
</p><p>
ய்ழ;்றி ரக;்சூஒற=;ழூநி; </p><p>
</p><p>
அதாவது அயோத்தியில் ராமர் கோவில் கட்ட வேண்டும், பசுவதை தடைச்சட்டம் கொண்டு
வரவேண்டும், மதமாற்ற தடைச்சட்டம் கொண்டு வருவது ஆகிய 3 கோரிக்கைகளை வைத்துள் ளோம்.
இந்த கோரிக்கைகளை நிறைவேற்ற தேசிய ஜனநாயக கூட்டணியில் உள்ள மற்ற கட்சிகளை
வற்புறுத்த வேண்டும். இல்லா விட்டால் கூட்டணி அரசை கலைத்துவிட்டு தேர்தலை பாரதிய ஜனதா
கட்சி சந்திக்க வேண்டும். அவ்வாறு செய்தால் மக்கள் மகத்தான வெற்றியை பாரதிய ஜனதா
கட்சிக்கு தருவார்கள். இந்துக்களின் நீண்ட நாளைய கோரிக்கைகளை நிறைவேற்று வதற்கு பாரதிய
ஜனதா கட்சி ஆட்சியை இழந்தாலும் பரவாயில்லை. இந்துக்களின் கோரிக்கைகளுக்காக தேர்தலை
சந்தித்தால் பாரதிய ஜனதா கட்சியின் ஊழியர்களும், தொண்டர்களும் தெம்புடனும்,
உற்சாகமாகவும் தேர்தல் வேலை செய்வார்கள். </p><p>
</p><p>
2 ்லிக;றி் ்ம;ழ;... </p><p>
</p><p>
மேலும் 2 குழந்தைகளுக்கு மேல் பெற்றால் தேர்தலில் நிற்க தடை விதிக்கப்போவதாக மத்திய
அமைச்சர் சுஷ்மாசுவ ராஜ் கூறியுள்ளார். அதை இந்து முன்னணி பாராட்டகிறது. 2 குழந்தைக்கு
மேல் பெற்றால் தேர்தலில் நிற்க தடை விதித்தால் மட்டும் போதாது, அவர்களின் ஓட்டுரி
மையையும் பறிக்க வேண்டும். </p><p>
</p><p>
கோவையில் போலீஸ்காரர் செல்வராஜ் கொலை வழக்கில் குற்றவாளிகள் அனைவரும் விடுதலை
செய்யப்பட்டுவிட்டனர். இதற்கு 2 காரணங்கள், ஒன்று காவல்துறையினர் சரியாக வழக்கை பதிவு
செய்யவில்லை. இன்னொன்று அரசு வக்கீல் ஒழுங்காக வழக்கை நடத்தவில்லை. போகிற போக்கை
பார்த்தால் தீவிரவாதிகள் மதானி, அன்சாரி, பாட்சா ஆகியோரும் விடுதலையாகி விடுவார்கள்
போல தெரிகிறது. அப்படி ஒரு நிலை ஏற்பட்டால் கலவரம் வெடித்து விடுமோ என்று மக்கள்
மத்தியில் பீதி ஏற்பட்டுள்ளது. கிறிஸ்துவ, முஸ்லிம் கல்வி நிறுவனங்களுக்கு அரசு சலுகைகள்
வழங்குவது போல் இந்துக்கள் தொடங்கும் நிறுவனங்களுக்கும் வழங்க வேண்டும். </p><p>
</p><p>
கோவையிலிருந்து கேரளாவுக்கு ஏராளமான பசுக்கள் கடத் தப்படுகின்றன. கடத்தப்படும் போது
வயதான பசுக்களை அப்படியே போட்டுவிட்டு செல்கின்றனர். இந்த கடத்தலை தடுக்க அங்கு
கண்காணிப்புக்குழுவை அரசு நியமித்தும், அதிகாரிகள் பசுக்களை கடத்துவதற்கு உறுதுணையாக
இருக்கிறார்கள். கோவிலில் விடப்படும் பசுக்களை ஏலம் விடக்கூடாது. அரசு பசுகாப்பகம்
அமைத்து அனைத்து பசுக்களையும் பாதுகாக்க வேண்டும். </p><p>
</p><p>
திருவைகுண்டத்தில் 60 ஆண்டுகளுக்கு பிறகு கள்ளபிரான் கோவிலில் தேரோட்டம் விரைவில்
நடைபெற உள்ளது. இதற்கான முயற்சிகளை எடுத்த டி.வி.எஸ். நிறுவனம் தலைமையிலான இந்திய
கலாச்சார பண்பாட்டு மைய நிர்வாகிகளை இந்து முன்னணி சார்பில் பாராட்டுகிறேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p> </p>

<p>அ.தி.மு.க. தயவால் வெற்றி
பெற்ற கட்சிகள் எங்கள் காதை கடிக்கிறார்கள்சேலத்தில் மந்திரி செம்மலை தாக்கு </p>

<p> </p>

<p>சேலம், ஏப்.19- அ.தி.மு.க. தயவால் வெற்றி பெற்று சட்டமன்ற
உறுப்பினர்கள் ஆன கட்சிகள்்் இன்று எங்கள் காதை கடிக்கிறார்கள் என்று மந்திரி செம்மலை
கூறினார். </p><p>
</p><p>
ஜெ. பிறந்தநாள் </p><p>
</p><p>
சேலம்் அருகே உள்ள பூலாவரியில் வீரபாண்டி ஒன்றிய அ.தி.முக சார்பில் ஜெயலலிதா
பிறந்தநாள் விழா கொண்டாடப்பட்டது. இந்த நிகழ்ச்சிக்கு எஸ்.கே. செல்வம் எம்.எல்.ஏ
தலைமை வகித்தார். </p><p>
</p><p>
செம்மலை </p><p>
</p><p>
விழாவில் 3 ஆயிரம் பேருக்கு இலவச வேட்டி- சேலையும், மாணவர்களுக்கு நோட்டு புத்தகமும்,
விளையாட்டு கருவிகளும் வழங்கப்்பட்டன. இதனை சுகாதார துறை மற்றும் கல்வி மந்திரி செம்மலை
வழங்கினார். </p><p>
</p><p>
பேச்்சு </p><p>
</p><p>
பின் அவர் பேசியதாவது:- </p><p>
</p><p>
தமிழகத்தில் மக்கள் விரோத ஆட்சி நடக்கிறது என்றும், இந்த ஆட்சி மக்களுக்கு நன்மை ஏதும்
செய்்்யவில்லை என்றும் எதிர்கட்சிகள் பிரச்சாரம் செய்து வருகிறது. அவர்கள் மக்களை
திசைதிருப்பி வருகிறார்கள். அவதூறு பிரச்்சாரம் செய்கிறார்கள். </p><p>
</p><p>
எதிர்கட்சிகள்பிரசாரம் </p><p>
</p><p>
இன்று தமிழகத்தில் மழை பொய்த்துவிட்டது. பகவான் கண்திறந்து மழைபெய்ய வைத்தால் எந்்த
கஷ்டமும் இருக்காது. தமிழகத்தில் மழை பெய்யாததுதான் குறையாக இருக்கிறது. இந்த சோதனையான
கட்டத்திலும் அரசு மக்களுக்கு நல்லது செய்து வருகிறது. இதனால் பொது மக்கள் சில
சங்கடங்களையும் தாங்கி கொள்ள வேண்டும். எதிர்கட்சிகளின் பிரச்சாரத்தை நம்பக்கூடாது. </p><p>
</p><p>
நாணயம் </p><p>
</p><p>
ஒரு நாணயத்திற்கு இரண்டு பக்கம் இருப்பது போல் அரசுக்கும் இரண்டு பக்கம் இருக்கிறது. அரசு
வருவாய்் பெருக்கினால்தான் மக்களுக்கு நல்ல திட்டம் கொண்டு வர முடியும். </p><p>
</p><p>
தமிழக முதல்வர் பொருளாதார முன்னேற்றம் காண நடவடிக்கை எடுத்து வருகிறார். இதுமக்களை
பாதித்தாலும் தற்்காலிகமாக இதனை மக்கள் தாங்கி கொள்ள வேண்டும். முதல்வர் அடுத்த
தலைமுறையினரை பற்றி சிந்தித்து வருகிறார். </p><p>
</p><p>
காது கடிப்பு </p><p>
</p><p>
கடந்த தேர்தலில் சிலகட்்சிகள் அ.தி.மு.கவுடன்கூட்டணி சேர்ந்து எம்.எல்.ஏ ஆனார்கள்.
ஆனால் அவர்கள் இன்று நமது காதை கடிக்கிறார்கள். </p><p>
</p><p>
அதி.மு.க மக்கள்இயக்கம். சில கட்சிகளில் தலைவர்கள் ்மட்டும் இருப்பார்கள். ஆனால்
தொண்டர்கள் இருக்க மாட்்டார்கள். ஆனால் அதி.மு.கவில் மக்கள் அணிவகுத்து
இருக்கிறார்கள். </p><p>
</p><p>
பள்ளி </p><p>
</p><p>
பூலாவரியில் இருக்கும் ஆரம்ப சுகாதார நிலையத்தை தரம் உயர்்த்த கேட்டார்கள். இதற்கு
சென்னை சென்று உத்தரவிடுகிறேன். இதன் மூலம் ஆரம்ப சுகாதார நிலை யத்திற்கு 30 படுக்கை கொண்ட
வசதியும், எக்ஸ்ரே மற்றும் ஆபரேசன் வசதியும் செய்து கொடுக்கப்படும். </p><p>
</p><p>
இவ்வாறு அவர் பேசினார். </p><p>
</p><p>
யார், யார் </p><p>
</p><p>
விழாவில் ஆத்தூர் மஞ்சினி முருகேசன் எம்.எல்.ஏ, வீரபாண்டி ஊராட்சி ஒன்றிய தலைவர்
குப்புசாமி, அரியானூர் பழனிசாமி உள்பட பலர் கலந்து கொண்டு பேசி னார்கள். </p><p>
</p><p>
அலுவலகம் </p><p>
</p><p>
முன்னதாக அட்டவணை பூலாவரியில் கிராம நிர்வாக அலுவலகத்தை மந்திரி செம்மலை திறந்து
வைத்தார். </p>

<p> </p>

<p> </p>

<p>நிகழ்ச்சி மேலும் ஒருநாள்
நீடிப்பு: வேலூர் சிறையில் 21-ந்தேதிவைகோவை கருணாநிதி சந்திக்கிறார் துரைமுருகன் தகவல்
</p>

<p> </p>

<p>காட்்பாடி,ஏப்.19- வேலூரில் தி.மு.கழக தலைவர் கருணாநிதி
நிகழ்ச்சி மேலும் ஒரு நாள் நீடிக்கப்பட்டு உள்ளது. வருகிற 21-ந்தேதி அன்்று அவர் வேலூர்
மத்திய சிறையில் வைகோவை சந்திக்கிறார். </p><p>
</p><p>
உற்சாக வரவேற்பு </p><p>
</p><p>
வேலூர் மண்டல தி.மு.க பொறுப்்பாளர் - முன்னாள் அமைச்சர் துரைமுருகன் தின கரன் நிருபரிடம்
கூறியதாவது- </p><p>
</p><p>
வேலூரில் தி.மு.கழக தலைவர் கலைஞர் இன்று முதல் 3நாட்கள் நிகழ்ச்சிகளில் பங்்
கேற்கிறார். வேலூரில் இன்று (சனிக்கிழமை ) மாலை நடைபெறும் பிரமாண்ட பொதுக்கூட்டத்தில்
அவர உரையாற்றுகிறார். இதற்்காக கார் மூலம் சென்னையில் இருந்து புறப்பட்டு வரும் டாக்டர்
கலைஞருக்கு மாவட்ட எல்லையான ஓச்சேரியில் மாலை 4மணி அளவில் மாவட்ட செயலாளர் ஆர்
.காந்தி தலைமையில் உற்சாக வர வேற்பு அளிக்கப்படுகிறது. </p><p>
</p><p>
இதில கழக உடன்பிறப்புகள் மற்றும் பொதுமக்கள் திரளாக பங்கேற்று வரவேற்பு அளிக்க
வேண்டுகிறேன். </p><p>
</p><p>
தொல்காப்பியபூங்கா </p><p>
</p><p>
நாளை (ஞாயிறு) காலை 9மணிக்கு சத்துவாச்சாரி கிருஷ்ணா மகாலில் தொல் ்காப்பிய
பூங்கா புத்தக வௌியீட்டு விழா நடக்கிறது. இதில் தமிழ் அறிஞர்கள் ,பொதுமக்கள்
கழகத்தினர் நேரடியாக வந்து இந்த நூலை கலைஞரிடம் பெற்று செல்கிறார்கள். </p><p>
</p><p>
முன்அனுமதி </p><p>
</p><p>
கிருஷ் ணா மகாலில் புத்தகம் வாங்குவதற்கு முன் அனுமதி பெற்றவர்கள் மட்்டும் மண்டபத்தில்
அனுமதிக்கப்படுவார்கள். மேடையில் கலைஞர் தனது திருக்கரங்களால் புத்தகங்களை
வழங்குவார் எற்கனேவே பெயர் பதிவு பெற்றவர்்கள் மட்டுமே அனுமதிக்கப்படுவார்கள்.
இந்தக்கட்டுப்்பாட்்டை கழக நண்பர்்கள் ஒவ்வொருவரும்் தங்்களுக்கு தாங்்களேகட்டுப்பாடாக
கருதி ஒத்துழைப்பு தருமாறு வேண்டுகிறேன். </p><p>
</p><p>
வைகோவுடன் சந்்திப்பு </p><p>
</p><p>
நாளைமறுதினம் (திங்கட்்கிழமை ) காலை 9மணிக்கு கலைஞர் வேலூர் மத்திய சிறைக்கு
சென்று வைகோவையும் முன்னாள ்்்அமைச்சர் கு.பிச்சாண்டி மற்றும் கழகத ்தினரை
சந்திக்கிறார். அதன் பிறகு சென்னை புறப்பட்டு செல்கிறார். </p><p>
</p><p>
3நாட்களும் வேலூர் ரிவர்வியூ ஓட்டலில் கலைஞர் தங்குகிறார். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
3நாட்களாக </p><p>
</p><p>
வேலூர் சிறையில் வைகோவை தி.மு.கழக தலைவர் கருணாநிதி நாளை (ஞாயிறு)சந்்திக்க
திட்டமிட்டு இருந்தார். ஆனால் அன்று வைகோவை சந்்திக்க ஜெ. அரசு அனுமதி அளிக்க மறுத்்து
விட்டது. இந்்த நிலையில வேலூர் நிகழ்ச்சியை 3நாட்்களாக நீடித்து நாளை மறுதினம்
(திங்கள்) காலை வைகோவை கருணாநிதி சந்தித்து பேசுகிறார். என்பது குறிப்பிடத்்தக்்கது. </p>

<p> </p>

<p> </p>

<p>தஞ்சையில்
குமரிஅனந்தன் பேட்டி: வீராணம் திட்டத்தால் சென்னைக்கு தண்ணீர்வராது: விவசாயிகளின்
கண்ணீர் தான் வரும் </p>

<p> </p>

<p>தஞ்சை,ஏப்.19- தஞ்சையில் நடந்த காங்கிரஸ் ஆலோசனை
கூட்டத்தில் குமரிஅனந்தன் கலந்து கொண்டு நிருபர்களுக்கு பேட்டி அளிக்கையில் தமிழகத்தில்
தண்ணீர் பஞ்சம் தீர தென்னக நதிகளை இணைக்க வேண்டும் என்று கூறினார். </p><p>
</p><p>
தஞ்சை செந்தில் திருமண மண்டபத்தில் பெருந்தலைவர் காமராஜரை பற்றிய நல்லாட்சி
தந்த நாயகன் என்னும் நூல் வௌியிடுவது குறித்து காங்கிரஸ் சார்பில் ஆலோசனை கூட்டம்
நடந்தது. </p><p>
</p><p>
கூட்டத்திற்கு தமிழ்நாடு காங்கிரஸ் கமிட்டி செயற் குழு உறுப்பினர் ஜி.ஆர். மூப்பனார் தலைமை
வகித்தார்.கூட்டத்தில் மாநில ஒழுங்கு நடவடிக்கைக்குழு தலைவர் குமரிஅனந்தன், மாநில செயற்குழு
உறுப்பினர் தஞ்சை வக்கீல் ராமமூர்த்தி, பட்டுக்கோட்டை தொகுதி
என்.ஆர்.ரெங்கராஜன் எம்.எல்.ஏ.,திருவாரூர் மாவட்ட செயலாளர் சிவா.ராஜ மாணிக்கம்,
தஞ்சை வடக்கு மாவட்ட செயலாளர் எம்.ராஜாங்கம், நாகை தெற்கு மாவட்ட தலைவர் பொன்.
பழனிவேல்,தஞ்சை தெற்கு மாவட்ட தலைவர் நாஞ்சி கி.வரதராஜன்,நாகை வடக்கு
மாவட்ட தலைவர் ஜெயபால், தியாகிகள் வி.மாணிக்கம், கரியமால்சேர்வை, டி.வெங்க
டாசலம்,ஜோசப்ராஜ்,செந்தில், முன்னாள் எம்.எல்.ஏ. ஏ.ஆர்.மாரிமுத்து,முரளிதரன் உள்பட
பலர் கலந்து கொண்டு பேசினர். </p><p>
</p><p>
பின்னர் குமரிஅனந்தன் நிருபர்களுக்கு பேட்டி அளித்தார்.அப்போது அவர் கூறியதாவது:- </p><p>
</p><p>
பெருந்தலைவர் காமராஜர் 1903-ம் ஆண்டு ஜூலை 15-ந் தேதி பிறந்தார். 2003-ம் ஆண்டு ஜூலை
மாதத்தோடு சேர்த்து 100 ஆண்டுகள் முடிந்து விட்டது. இதனையொட்டி பெருந் தலைவரின்
புகழையும், தமிழுக்கு ஆற்றியப்பணி களையும், தமிழகத்தில் பட்டி தொட்டில்கள், மூலை
முடுக்கெல்லாம் எடுத்து சொல்லும் நோக்கோடு, முதல் கட்டமாக நீலகிரி மாவட்டத்தில் என்
பணியினை துவக்கி நீலக்கடல்(கன்னியகுமாரி) முடியும் வரை செல்ல இருக்கிறேன். மாவட்ட
காங்கிரஸ் கமிட்டி மற்றும் தியாகிகளின் ஒத்துழைப்போடு இந்த பயணம் நடைபெறு கிறது. </p><p>
</p><p>
தியாகிகளை சந்திப்ப தன் நோக்கம் பெருந்தலைவர் காமராஜரோடு இருந்த தொடர்புகளையும்,
அனுபவங் களையும் பற்றி கேட்டறிவது, மற்றும் காமராஜரோடு எடுத்துக்கொண்ட பழைய
புகைப்படங்களை பெற்று நல்லாட்சி தந்த நாயகன் என்ற நூலை வௌியிடுவது சம்பந்தமாக இந்த
பயணம் தொடர்கிறது. </p><p>
</p><p>
மேலும் தியாகிகளுக்கு மாநில அரசு ரூ.3 ஆயிரம் ஊதியம் வழங்கி வருகிறது. ஆனால் தியாகிகள்
இறந்துவிட்டால் அவர்களது மனைவியாருக்கு மாத ஊதிய மாக ரூ.1500 மட்டுமே வழங்கபடுகிறது.
உடனடியாக மாநில அரசு ரூ.3 ஆயிரம் முழுத் தொகையையும் வழங்க வேண்டும். தியாகிகளுக்கு மருத்துவ
படியாக மாதம் தோறும் ரூ.15 வழங்க படுகிறது. இந்த தொகை போதாது. எனவே மாதம் ஒன்றுக்கு
ரூ.250 வழங்க வேண்டும். </p><p>
</p><p>
மேலும் தமிழ்நாட்டில் தியாகிகள் வாழ்ந்த இடங்களை கண்டு அறிந்து சுதந்திர போராட்ட
தியாகிகள் வாழ்ந்த இடம் எது? என்கிற அறிவிப்பு பலகையை வைக்க வேண்டும்.
தமிழ்நாட்டில் காமராஜர் முதல்வராக இருந்தபோது, பள்ளிகளில் பணி புரியும்
தமிழாசிரியர்களும் தலைமை ஆசிரியர்களாக வரலாம் என்ற ஆணையை பிறப்பித்தார். </p><p>
</p><p>
தமிழ் ஆட்சி மொழி சட்டமாக காமராஜர் காலத்தில் கொண்டுவரப்பட்டது. தமிழ் வளர்ச்சிக்கு
காமராஜர் ஒரு ஆய்வுக்குழு அமைத்தார். அதில் எந்தவித வேறுபாடும் பார்க்காமல்
பேரறிஞர் அண்ணா,ஜீவானந்தம், கி.ஆ.பெ.விஸ்வநாதன், நாவலர் நெடுஞ்செழியன்
போன்றவர்களை உறுப்பினர் களாக்கி செயல்பட வைத்தவர் கர்ம வீரர் காமராஜர். </p><p>
</p><p>
இவ்வாறு தமிழுக்காக பல தொண்டுகளை செய்தவர் காமராஜர். அந்த பெருந் தலைவரின் புகழை பட்டி
தொட்டில் எங்கும் பரப்புவதற்காக தமிழ்நாட்டில் உள்ள அனைத்து மாவட்டங்களிலும்
சுற்றுப்பயணம் மேற்கொண்டு தியாகிகளை சந்தித்து அவர்களது அனுபவங்களை கேட்டறிந்து, நூல்
எழுதி வருகிறேன். </p><p>
</p><p>
இந்த நூலுக்காக காமராஜருடன் நெருங்கி பழகியவர்கள் புகைப்படங்கள் இருந்தால் அனுப்பி
வைக்கும் படி கேட்டுக்கொள்கிறேன். மேலும் வீராணம் திட்டப் பணி ரூ. 720 கோடி
மதிப்பீட்டில் நடந்து வருகிறது. இந்த பணியினால் சென்னைக்கு தண்ணீர் வராது. சிதம்பரம்
தாலுகாவில் வசிக்கும் விவசாயிகளின் கண்களில் இருந்து கண்ணீர்தான் வரும். மேலும் தண்ணீர்
பிரச்சினையை போக்குவதற்கு தென்னக நதி களை இணைப்பதுதான் சாத்தியமாகும். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p>குறிஞ்சிப்பாடி அருகே
பா.ம.க.-விடுதலை சிறுத்தை மோதல்படம் உடைப்பு, கொடிக்கம்பம் சாய்ப்பு திடீர் சாலை
மறியல்-பதட்டம் </p>

<p> </p>

<p>கடலூர், ஏப்.19- குறிஞ்சிப்பாடியில் பா.ம.க.- விடுதலை
சிறுத்தைகள் இடையே மோதல் ஏற்பட்டது. பா.ம.க. கொடிக்கம்பம் உடைக்கப்பட்டதை கண்டித்து
பா.ம.க.வினர் திடீர் சாலை மறியலில் ஈடுபட்டனர். </p><p>
</p><p>
படம் உடைப்பு </p><p>
</p><p>
கடலூர் மாவட்டம் குறிஞ்சிப்பாடி அருகே தம்பிபேட்டை கிராமம் உள்ளது. இங்கு உள்ள
காலனி பகுதியில் விடுதலை சிறுத்தைகள் அமைப்பு சார்பில் திருமாவளவன் படம் போட்ட போர்டு
வைக்கப்பட்டு இருந்தது. இந்த போர்டை யாரோ உடைத்து உள்ளனர். இதனை பார்த்த காலனி
மக்கள் ஆத்திரம் அடைந்தனர். </p><p>
</p><p>
மோதல் </p><p>
</p><p>
அந்த பகுதியை சேர்ந்த பா.ம.க.வினர் தான் போர்டை உடைத்துவிட்டதாக எண்ணினர். இதனை
தொடர்ந்து காலனியை சேர்ந்த விடுதலை சிறுத்தை அமைப்பினர் தம்பிபேட்டை கிராமத்துக்கு
திரண்டு சென்றனர். அங்கு கிராம எல்லையில் உள்ள பா.ம.க. கொடி கம்பத்தை உடைத்ததாக
கூறப்படுகிறது. மேலும் பா.ம.க. தட்டி போர்டையும் அந்த கும்பல் கிழித்து எறிந்துவிட்டு
ஓடிவிட்டதாக தெரிகிறது. இந்த செய்தியை கேள்விப்பட்டதும் பா.ம.க.வினர் அங்கு திரண்டனர்.
அவர்கள் காலனி பகுதிக்கு சென்று விடுதலை சிறுத்தைகள் அமைப்பினரிடம் இந்த சம்பவம்
தொடர்பாக தட்டி கேட்டதாக தெரிகிறது. இது தொடர்பாக இருதரப்பி னருக்கும் இடையே மோதல்
ஏற்படும் சூழ்நிலை உருவானது. </p><p>
</p><p>
சாலை மறியல் </p><p>
</p><p>
இதற்கிடையே பா.ம.க.வினர் திடீர் சாலை மறியலில் ஈடுபட்டனர். இதுபற்றி தகவல்
அறிந்ததும் நெய்வேலி துணை போலீஸ் சூப்பிரெண்டு மற்றும் கடலூர் அதிரடிப்படை போலீசார்
அங்கு விரைந்து சென்றனர். மறியலில் ஈடுபட்டவர்களுடன் அவர்கள் பேச்சுவார்த்தை
நடத்தினர். அதன் பேரில் பா.ம.க.வினர் மறியலை கைவிட்டு கலைந்து சென்றனர். </p><p>
</p><p>
பதட்டம் </p><p>
</p><p>
பா.ம.க.-விடுதலை சிறுத்தைகள் மோதலை தொடர்ந்து அந்த பகுதியில் பதட்டம் ஏற்பட்டுள்ளது.
மேலும் அசம்பாவித சம்பவங்கள் நடைபெறாமல் இருக்க போலீசார் பாதுகாப்பு பணியில்
ஈடுபடுத்தப்பட்டுள்ளனர். </p>

<p> </p>

<p> </p>

<p>ஜெ.
பெருமைப்பட்டுக்கொள்வதில் அர்த்தமில்லை: ஆட்சி மாறினாலும் வீரப்பனின் கடத்தல்
காட்சிகள் மாறவில்லை கருணாநிதி கடிதம் </p>

<p> </p>

<p>சென்னை, ஏப். 19- ஆட்சி மாறினாலும் வீரப்பனின் கடத்தல்
காட்சிகள் மாறவில்லை. எனவே தனிப் பட்ட முறையில் ஜெயலலிதா பெருமைப்பட்டுக் கொள்ள
முடியாது என்று கருணாநிதி கூறினார். </p><p>
</p><p>
வீரப்பனின் சாகசம் </p><p>
</p><p>
1978-ஆம் ஆண்டுவாக்கில் மரங்களை வெட்டிக் கடத்தல் என்று ஆரம்பமான சந்தனக் கடத்தல்
வீரப்பனின் சாகசம்; பின்னர் மனிதர்களைக் கடத்துதல் என்ற அளவுக்கு வளர்ந்து- தமிழகம்,
கர்நாடகம் ஆகிய மாநில அரசுகளும் அதிரடிப் படைகள் அமைத்து, வீரப்பனைப் பிடிப்பதற்காகக் கோடிக்கணக்கில்
செலவிட்ட தையும், அப்படியும்கூட கடத்தப்பட்டவர்களில் சிலரைக் காப்பாற்றி மீட்க
முடியாமல் பலிகொடுக்க நேரிட்டதையும்- அரசியல் காழ்ப்புணர்ச்சிகளுக்கு அப்பாற்பட்டு
சிந்திப்பவர்களால் தௌிவாக உணர முடியும். </p><p>
</p><p>
ஏதோ; தி.மு.க. ஆட்சியில் தான் வீரப்பனின் கடத்தல் விவகாரங்கள் நடைபெற்ற தாக ஒரு
பிரச்சாரம்; அடிப்படை சிறிதுமில்லாது செய்யப்படுகிறது. </p><p>
</p><p>
ராஜ்குமார் கடத்தல் </p><p>
</p><p>
அதுமட்டுமல்ல; அண்மையில் சட்டப்பேரவையில் வீரப்பன் விவகாரம் பற்றிப் பேசிய
முதலமைச்சர் ஜெயலலிதா; நடிகர் ராஜ்குமார் தி.மு.க. ஆட்சியில் தமிழ்நாட்டு எல்லையிலிருந்து
கடத்தப்பட்டார். கர்நாடக முன்னாள் அமைச்சர் நாகப்பா கர்நாடக எல்லையிலிருந்து
கடத்தப்பட்டார். கருணாநிதி ஆட்சி புரிந்த போது தமிழ்நாட்டில் வீரப்பன் கடத்துவது
சாத்தியம், எனது ஆட்சி நடைபெறும்போது தமிழ்நாட்டில் வீரப்பன் கடத்துவது அசாத்தியம் என்று
பெருமைப்பட்டுக் கொண்டார். </p><p>
</p><p>
யாரானாலும் ஒன்றை மறந்து விடக்கூடாது, இரண்டு மாநில எல்லைகளுக்கும் தேவா ரம்தான் நாகப்பா
கடத்தப்பட்டபோதும், கொல்லப்பட்ட போதும் கூட்டு அதிரடிப் படைத் தலைவராக இருந்தார். </p><p>
</p><p>
வீரப்பன் நடத்திய கடத்தல் சம்பவங்கள் தமிழ்நாட்டில் குறிப்பாக ஜெயலலிதா ஆட்சியில்
நடைபெற்றதற்கான பட்டியலையும் இக்கடிதத்தில் குறிப்பிட்டுள்ளேன். எனவே இதில் எந்த
மாநிலம் வீரப்பன் விவகாரத்தில் ஏமாந்தது என்பதோ; அல்லது எந்தக்கட்சி ஆட்சியில்
இந்தக்கடத்தல்கள் நடைபெற்றன என்பதோ முக்கியமல்ல </p><p>
</p><p>
கடத்தல் பட்டியல் </p><p>
</p><p>
இதோ ஆண்டு வாரியாக 1987லிருந்து வீரப்பன் கும்பல் நடத்திய கடத்தல் சம்பவங்கள் யாருடைய
ஆட்சிக்காலத்தில் நடைபெற்றன என்பதை; அந்த ஆண்டுக் கணக்கை வைத்து உன்போன்ற
உடன்பிறப்புகள் அறிந்து கொள்வீர்கள் என்று நம்புகிறேன். </p><p>
</p><p>
14-7-1987 அன்று வீரப்பன் கும்பல் சத்தியமங்கலம் வன அலுவலர் சிதம்பரம் என்ப வரை
சுட்டுக்கொலை செய்தது. </p><p>
</p><p>
7-8-1989 அன்று தமிழ்நாடு பர்கூர் காவல் நிலைய எல்லை யில்,வனக்காவலர் பழனிச் சாமி,
வாச்சர் சுப்பிரமணியம் மற்றும் மஸ்தி என்ற நபர் ஆகிய மூவரையும் வீரப்பனுடைய கும்பல்
கொலை செய்தது. </p><p>
</p><p>
4-7-1991 அன்று பென்னாகரத்தில் வீரப்பன் கும்பல் ஒரு காவலரைச் சுட்டுக்கொன்று விட்டு
துப்பாக்கி ஒன்றையும் திருடிக்கொண்டு தப்பிச் சென்று விட்டது. </p><p>
</p><p>
1993இல் சுரைக்காய் மடுவு என்ற இடத்தில் கண்ணிவெடிகள் வெடித்து ஐந்து காவலர் கள் உள்பட
22 பேர் மாண்டார்கள். காவல்துறை அதிகாரி கோபாலகிருஷ்ணன் படுகாயமடைந்தார். </p><p>
</p><p>
நிபந்தனைகள் </p><p>
</p><p>
3-12-1994 அன்று வீரப்பனால் காவல்துறை துணை கண்காணிப்பாளர் சிதம்பர நாதன் உட்பட மூன்று
பேர் கடத்தப்பட்டனர். அவர்களை விடுவிக்க வீரப்பன் விதித்த நிபந்தனைகளாவன:- </p><p>
</p><p>
* அதிரடிப் போலீஸ் படையை வாபஸ்பெற வேண்டும். </p><p>
</p><p>
* வீரப்பனைத் தேடும் பணியைக் கைவிட வேண்டும். </p><p>
</p><p>
* வீரப்பனுடைய அண்ணன் மாதய்யனை விடுதலை செய்ய வேண்டும். </p><p>
</p><p>
* வீரப்பன் சரண் அடைந்தால் அவன் மீது வழக்கு போடக்கூடாது. </p><p>
</p><p>
இதுகுறித்து 4-12-1994 அன்று கோவை மாவட்ட ஆட்சித்தலைவர் சங்கர், ஐ.ஏ.எஸ். விடுத்த
அறிக்கையில், வீரப்பனால் கடத்தப் பட்டிருக்கிறவர்களுடைய உயிரைக் காப்பாற்றுவதுதான்
முக்கியம். எனவே, வீரப்பன் நிபந்தனைப்படி வீரப்பனைத் தேடும் பணி நிறுத்தப்பட்டு விட்டது
என்று தெரிவித்தார். </p><p>
</p><p>
கோவை கலெக்டர் </p><p>
</p><p>
மீண்டும் 6-12-1994 அன்று அரசின் சார்பில் கோவை மாவட்ட ஆட்சித் தலைவர் நிருபர்களிடம்
பேசும்போது; </p><p>
</p><p>
வீரப்பனிடமிருந்து எனக்கு செய்தி வந்துள்ளது. அந்தச் செய்தியிலே வீரப்பன் கேட்டுக்
கொண்டபடி வீரப்பனோ, அவரது பிரதிநிதியோ என்னைச் சந்திக்கலாம். அவருக்கு எந்த ஆபத்தும்
ஏற்படாது என்று உறுதி அளிக்கப்படுகிறது. மேலும் பணயமாக பிடித்து வைக்கப்பட்டிருப்பவர்களுக்கு
தேவையான உணவுப்பொருளைக் கொண்டு செல்ல தடை எதுவும் இருக்காது. அதற்கான ஏற்பாடுகளும்
செய்யப்பட்டு விட்டன. வீரப்பனுடைய ஆட்கள் மலைக் கிராமங்களுக்கு நேரிலே வந்து, எவ்விதத்
தடையும் இல்லாமல் உணவுப் பொருளை எடுத்துச் செல்லலாம். </p><p>
</p><p>
நிருபர்:- வீரப்பனுடைய தூதுவன் வர, அரசுத் தரப்பில் வாகன வசதி செய்து கொடுக்கிறீர்களே,
இந்த நிலையிலே தூதுவன் துப்பாக்கியுடன் வந்து பேசுகின்றானே, இது சரியா? </p><p>
</p><p>
கலெக்டர்:- அவர் சொந்தப்பாதுகாப்புக்கு துப்பாக்கியுடன் வருகிறார். காரணம் அவர்
காட்டுக்குள் போகிறார், வருகிறார், எனவே துப்பாக்கி ஏந்துகிறார் என்று தெரிவித் தார். </p><p>
</p><p>
2 கோரிக்கைகள் </p><p>
</p><p>
இந்தப் பிரச்சினையிலே அப்போது தலைமைச் செயலாளராக இருந்த அரிபாஸ்கர் ஐ.ஏ.எஸ்.
செய்தியாளர்களிடம் கூறும்போது, வீரப்பனுடைய தளபதி பேபி இரண்டு கோரிக்கைகளைச்
சொன்னார். ஒன்று, வீரப்பன் கோஷ்டியினர் காட்டுக்குள் உணவு கொண்டு வரும்போது, எவ்வித
துன்புறுத்தலுக்கும் ஆளாகக் கூடாது. மற் றொன்று, சிறையிலே உள்ள வீரப்பன் கோஷ்டியை தளபதி
பேபி பார்க்க அனுமதிக்கப்பட வேண்டும். இந்த நிபந்தனைகள் நிறைவேற்றப்பட்டால், மற்ற
கோரிக்கைகள் பிறகு தெரிவிக்கப்படும் என்று சொல்லியிருக்கிறார். </p><p>
</p><p>
இந்தக் கோரிக்கைகளை அரசு பரிசீலனை செய்து வருகிறது. இப்போது அரசினுடைய மிக முக்கியமான
முன்னுரிமை கடத்தப்பட்ட போலீஸ் அதிகாரிகளையும், மற்றவர்களையும் மீட்பதுதான். விசேஷ
அதிரடிப்படையின் பணிகள் நிறுத்தப்பட்டுவிட்டன. இந்தப் படைகளை அனுப்பினால் வீரப்பன்
போலீஸ் அதிகாரி களையும், மற்றவர்களையும் கொன்றுவிடக்கூடும். பேச்சு வார்த்தை முடிந்து,
மீண்டும் அவர்கள் காட்டுக்குள் செல்ல முழு பாதுகாப்பும் அளிக்கப்படும் என்றார். </p><p>
</p><p>
மன்னிப்பு தர வேண்டும் </p><p>
</p><p>
இதற்குப் பிறகு வீரப்பன் தம்பி அர்ஜுனன் செய்தியாளர்களிடம் சொன்னது; நாங்கள் சரண்
அடைய மாட்டோம். தண்டனை அனுபவிக்க மாட்டோம். எங்களுக்கு மன்னிப்பு கொடுத்து, ஜனாதி
பதி எழுத்து மூலம் உத்தரவு கொடுக்க வேண்டும். அப் போதுதான் ஆயுதங்களை ஒப்படைப்போம்
என்பதாகும். </p><p>
</p><p>
அரசு பேச்சு வார்த்தை </p><p>
</p><p>
இதற்குப் பிறகு மீண்டும் தலைமைச் செயலாளர் அரி பாஸ்கர் கூறியதாவது:- </p><p>
</p><p>
ஒரு அடிப்படை விஷயத்தை நாம் மறந்துவிடக் கூடாது. இது உயிர்கள் சம் பந்தப்பட்ட விஷயம்.
போலீஸ் துணை கண்காணிப்பாளர் மற்றும் இரண்டு நபர்களை நாம் மீட்க வேண்டும். </p><p>
</p><p>
செய்தியாளர்:- வீரப்பன் இருக்குமிடம் அரசுக்குத் தெரியுமா? </p><p>
</p><p>
தலைமைச் செயலாளர்:- வீரப்பன் இருக்கும் இடம் அரசுக்குத் தெரியும். ஆனால் 3 பேர்களையும்
மீட்க அரசு பேச்சு வார்த்தை நடத்திக் கொண்டிருக்கிறது. </p><p>
</p><p>
ஜெ. பேச்சு </p><p>
</p><p>
சட்டப்பேரவையில் அப்போது முதல் அமைச்சர் ஜெயலலிதா பேசியதாவது:- </p><p>
</p><p>
வீரப்பனிடம் 3 உயிர்கள் சிக்கிக் கொண்டிருந்த சூழ்நிலையில் அந்த வீரர்களுக்கு எந்த
ஆபத்தும் ஏற்படக்கூடாது என்கிற எண்ணத்தினால்தான் காவல் துறையின ரையும், மாவட்ட ஆட்சித்
தலைவரையும் தகுந்த எச்சரிக்கையுடன் நடந்து கொள்ள வேண்டும் என்று அறிவுறுத்தினேன்.
மாவட்ட ஆட்சித் தலைவர் எனது ஆணையின்பேரில் பேச்சுவார்த்தை நடத்த ஆரம்பித்தார்.
மூன்றாவது நாளில் வீரப்பன் கூட்டத்தைச் சேர்ந்த பேபி மாவட்ட ஆட்சித்தலை வரைச்
சந்தித்துப் பேசினான். பேபி பின்னர் வீரப்பனின் மனைவியையும், வேறு சிலரையும்
பாதுகாப்பாக வைத்துள்ள இடத்திற்குச் சென்று பார்த்து விட்டு வந்தான். </p><p>
</p><p>
வீரப்பனின் தம்பி அர்ச்சுனன் 12-12-1994 அன்று மருத்துவ சிகிச்சை செய்து கொள்வதற்கும்,
வீரப்பனின் நிபந்தனைகளைத் தெரிவிப்பதற்கும் வந்தான். சிறையில் அடைக்கப்பட்டிருக்கின்ற
அவனது சகோதரன் மாதையனைப் பார்ப்பதற்கும், தனது காயத்திற்கு மருத்துவச் சிகிச்சை செய்து
கொள்வதற்கும் அர்ச்சுனனுக்கு அனுமதி வழங்கப்பட்டது. </p><p>
</p><p>
நாம் வௌிப்படுத்துகின்ற எந்தச் செய்தியும் மேற்கொண்டுள்ள பணிக்கு இடையூறாகப் போவது
மட்டுமின்றி மூன்று பேர்களின் உயிருக்கும் ஆபத்தை ஏற்படுத்திவிடும் என்கின்ற எச்சரிக்கை
உணர்வும்தான் பல செய்திகளை நாங்கள் வௌியிட முடியாமல் போனதற்குக் காரணம். </p><p>
</p><p>
இவ்வாறு ஜெயலலிதா 20-3-1995 அன்று பேரவையிலே பேசினார். </p><p>
</p><p>
மீண்டும் கடத்தல் </p><p>
</p><p>
மீண்டும் 1-1-1995 அன்று ஈரோடு மாவட்டம், பர்கூர் காவல்நிலைய எல்லையில் வீரப்பன்
கும்பல் ராஜேந்திரன், சுப்பிரமணியம் என்ற இரு காவலர்களையும், உடை யார் என்ற
வாட்சரையும் கடத்திச்சென்றது. </p><p>
</p><p>
17-2-1996 அன்று ஈரோடு மாவட்டம், ஆசனூர் காவல் நிலைய எல்லையில் வீரப்பன் கும்பல்
மறைந்திருந்து நடத்திய துப்பாக்கிச்சூட்டில் காவல் துறை கண்காணிப்பாளர் தமிழ்ச்செல்வன்
காயமடைந்து அவரது கைவிரல்கள் துண் டிக்கப்பட்டன. செல்வராஜ் என்ற தலைமைக்காவலர்
துப்பாக்கிச்சூட்டில் உயிரிழந்தார். </p><p>
</p><p>
பெருமைப்படமுடியாது </p><p>
</p><p>
எனவே வீரப்பனின் கடத்தல் நடவடிக்கைகள் எல்லா ஆட்சியிலும் நடைபெற்றிருக் கின்றன. மேலே
சொன்ன அனைத்துக் கடத்தல் சம்பவங்களும் தமிழ்நாட்டில் நடந் தவைதான். யாருடைய ஆட்சியில்
இல்லை என்று யாரும் தனிப்பட்ட முறையில் பெருமைப்பட்டுக்கொள்ள முடியாது. </p><p>
</p><p>
ஜெயலலிதா ஆட்சிப் பொறுப்பேற்றதும் 2001ஆம் ஆண்டு ஜூன் திங்களில் அதிரடிப்படையை
தேவாரம் தலைமையிலே மாற்றி அமைத்தார். இந்த அதிரடிப்படைக்கு செய்யப்பட்ட செலவினத்தை
ஒப்பிட்டு நோக்குவதற்காக, அண்மையில் சட்டப்பேரவையில் முதலமைச்சர் ஜெயலலிதா வௌியிட்ட
புள்ளி விவரங்களை அப்படியே தருகிறேன். </p><p>
</p><p>
1999-2000ல் 2 கோடியே 27 லட்சம் ரூபாய்; 2000-2001இல் 3 கோடியே 36 லட்சம் ரூபாய்;
2001-2002ல் 9 கோடியே 47 லட்சம் ரூபாய்; 2002-2003ல் 7 கோடியே 62 லட்சம் ரூபாய். </p><p>
</p><p>
கடத்தல் காட்சிகள் மாறவில்லை </p><p>
</p><p>
ஆட்சிகள் மாறின- அதிரடிப்படை தலைவர்கள் மாறினார்கள்- ஆனால் வீரப்பன் கடத்தல்
விவகாரக் காட்சிகள் எந்த ஆட்சியிலும் மாறவில்லை என்பதை விளக் கவே இக்கடிதம். </p><p>
</p><p>
இவ்வாறு கூறி உள்ளார். </p>

<p> </p>

<p> </p>

<p>சளி, காய்ச்சலால் அவதி:
அன்பழகனிடம் நலம் விசாரித்தார், கருணாநிதி </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>க.அன்பழகன் </p>

<p> </p>

<p>சென்னை, ஏப்.19- தி.மு.க. பொதுச் செயலாளரும் சட்டமன்ற
எதிர்க்கட்சி தலைவருமான க.அன்பழகன் கடந்த ஒரு வார காலமாக சளி, காய்ச்சலால்
அவதிப்பட்டு வருகிறார். தற்போது அவரது உடல் நிலை தேறி வருகிறது. மருத்துவர்கள்
ஆலோசனைப்படி வீட்டிலேயே அன்பழகன் ஓய்வு எடுத்து வருகிறார். </p><p>
</p><p>
டி.ஆர்.பாலு </p><p>
</p><p>
தி.மு.க. தலைவர் கருணாநிதி நேற்று அன்பழகன் வீட்டிற்கு சென்றார். அன்பழகனிடம் நலம்
விசாரித்தார். அவருடன் மத்திய அமைச்சர் டி.ஆர்.பாலுவும் சென்றிருந்தார். </p>

<p> </p>






</body></text></cesDoc>