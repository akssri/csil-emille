<cesDoc id="tam-w-dinakaran-politics-03-06-08" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-politics-03-06-08.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-06-08</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-06-08</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>பா.ஜனதாவுக்கு புதிய
தலைவர் நியமிக்க திட்டமா? வாஜ்பாயுடன் வெங்கய்யா நாயுடு சந்திப்பு </p>

<p> </p>

<p>புதுடெல்லி, ஜூன் 8- பா.ஜ.வில் முரளி மனோகர் ஜோஷி கூறிய
குற்றச்சாட்டுக்கு பதில் அளிக்க வெங்கய்யா நாயுடு மறுத்து விட்டார். இந்த நிலையில் தலைமை
பிரச்சினையை பயன்படுத்தி வெங்கய்யா நாயுடுவை பலிகடா ஆக்க திட்டம் வகுக்கப்பட்டு வருவ
தாக தெரிகிறது. இது தொடர்பாக வாஜ்பாயை நாயுடு நேற்று சந்தித்தார். </p><p>
</p><p>
பாரதிய ஜனதா கட்சி அடுத்த பொதுத்தேர்தலை வாஜ்பாய்-அத்வானி ஆகிய இருவரது தலைமையின்
கீழ் சந்திக்கும் என்று பா.ஜ. தலைவர் வெங்கய்யாநாயுடு கூறியதை தொடர்ந்து கட்சியில்
பிரச்சினை உருவாகியது. </p><p>
</p><p>
வாஜ்பாயை விகாஷ் புருஷ் (முன்னேற்றத்தின் தலைவர்) என்றும் அத்வா னியை (இரும்பு மனிதர்)
என்றும் அவர் வர்ணித்தார். </p><p>
</p><p>
தே.ஜ.கூட்டணிக்கும், பா.ஜ. கட்சிக்கும் வாஜ்பாய் தான் நம்பர் ஒன் தலைவர் என்ற
நிலையில் இருந்து அத்வானியையும் முதலிடத்துக்கு கொண்டு வருவதாக இந்த பேட்டி அமைவதாக
கருதப்பட்டது. </p><p>
</p><p>
சர்ச்சை முடிந்ததா? </p><p>
</p><p>
இதை தொடர்ந்து அடுத்த பொதுத்தேர்தலில் பா.ஜ. கட்சிக்கு அத்வானிஜி தலைமை தாங்கி
வெற்றியை ஈட்டித் தருவார் என்று பிரதமர் வாஜ்பாய் கூறியது பெரும் சர்ச்சையை
ஏற்படுத்தியது. இந்த விஷயத்தை முடிவுக்கு கொண்டு வருவதற்குள் வெங்கய்யா நாயுடு படாதபாடு
பட்டு விட்டார். வாஜ்பாய் தான் நம்பர் ஒன் தலைவர். அத்வானிக்கு 2-வது இடம் தான் என்று
கூறி வெங்கய்யா நாயுடு பிரச்சினைக்கு முற்றுப்புள்ளி வைத்தார். </p><p>
</p><p>
இதுபற்றி கருத்துக் கூறிய அத்வானி இந்த பிரச்சினைக்கு உள்ளேயே நுழையாமல் அடிப்படை
ஆதாரமற்ற சர்ச்சையை பத்திரிகைகள் தான் உருவாக்கி விட்டன. இதற்கு நான் பதில் கூற
வேண்டியதில்லை என்றார். </p><p>
</p><p>
இந்த நிலையில் பா.ஜ. மூத்த தலைவரும், மத்திய மனிதவளமேம்பாட்டுத் துறை மந்திரியுமான
முரளி மனோகர் ஜோஷி, வெங்கய்யாநாயுடுவை தாக்கி பேட்டி அளித்தார். வாஜ்பாயை விகாஷ்
புருஷ் என்று அறிவிக்க கட்சியின் எந்த மட்டத் திலும் முடிவு செய்யவில்லை. இது
வெங்கய்யாவின் தனிப்பட்ட கருத்து தான் என்று அவர் கூறினார். </p><p>
</p><p>
பதிலளிக்க மறுப்பு </p><p>
</p><p>
இதுகுறித்து நேற்று கவுகாத்தில் பா.ஜ. மாநாட்டில் கலந்து கொண்ட வெங்கய்யா நாயுடுவிடம்
நிருபர்கள் கேட்டதற்கு, தலைமை பிரச்சினையைப் பொறுத்த வரையில், பிரதமர், துணைப்பிரதமர்
மற்றும் கட்சித் தலைவராகிய நான் ஆகியோர் விளக்கம் அளித்து விட்டோம். எந்த
பிரச்சினையும் இல்லை. சர்ச்சையும் கிடையாது. கட்சித் தலைவர்கள் பொதுவாக பேசுவது பற்றி
கருத்து கூறுவதில்லை என்ற கட்சியின் பாரம்பரியத்தை நான் கடைப்பிடிக்கிறேன். தலைமை
பிரச்சினையால் பா.ஜ.வின் செல்வாக்கு பாதிக்கப்படவில்லை என்றார். </p><p>
</p><p>
மாற்றமா? </p><p>
</p><p>
இதற்கிடையில் இந்த சூழ்நிலையை பயன்படுத்தி வெங்கய்யாநாயுடுவை கட்சித் தலைவர்
பதவியில் இருந்து தூக்க ஒரு தரப்பினர் முயற்சி செய்து வருகிறார்கள். இந்த வருட இறுதியில் 5
மாநில சட்டசபை தேர்தல் நடைபெற உள்ளது. இந்த தேர்தலுக்கு முன் கட்சித் தலைமையை மாற்ற
வேண்டும். வடமாநிலத்தைச் சேர்ந்த ஒருவர் தலைவராக இருந்தால் தான் இந்த தேர்தலில்
வெற்றி பெற முடியும் என்று பா.ஜ.வில் ஒரு தரப்பினர் கருதுகிறார்கள். </p><p>
</p><p>
புதிய தலைவர் யார்? </p><p>
</p><p>
பா.ஜ. கட்சி பொதுவாக இந்தி பேசும் மாநிலங்களில் தான் செல்வாக்கு பெற்று உள்ளது. இந்த
கட்சிக்கு தென் மாநிலங்களைச் சேர்ந்தவர்கள் அண்மைக் காலங்களில் தலைவராக வந்தனர்.
ஆனால் அவர்கள் அனைவரும் சொற்பகாலத்தி லேயே பதவியை விட்டு விலக நேர்ந்தது. ஆந்தி
ராவைச் சேர்ந்த பங்காரு லட்சுமணன், தமிழ்நாட்டைச் சேர்ந்த ஜனா. கிருஷ்ணமூர்த்தி
போன்றவர்கள் குறுகிய காலத் திலேயே கட்சித்தலைவர் பதவியை துறந்தவர்கள் ஆவர். அதே
வழியில் வெங் கய்யா நாயுடுவையும் தற்போதைய சூழ்நிலையை பயன்படுத்தி பலிகடா ஆக்க
திட்டமிடப்பட்டு இருப்பதாக கூறப்படுகிறது. </p><p>
</p><p>
வெங்கய்யா நாயுடு விலகும் பட்சத்தில், முரளிமனோகர் ஜோஷி, சுஷ்மா சுவராஜ், பிரமோத்
மகாஜன் ஆகியோரது பெயர்கள் அடிபடுகின்றன. </p><p>
</p><p>
வாஜ்பாயுடன் வெங்கய்யா சந்திப்பு </p><p>
</p><p>
இந்த நிலையில் நேற்று மாலை கவுகாத்தியில் இருந்து திரும்பிய வெங்கய்யா நாயுடு உடனடியாக
பிரதமர் வாஜ்பாயை சந்தித்து பேசினார். கவுகாத்தியில் நடைபெற்ற கட்சியின் மாநாடு
விவரம் மற்றும் வட கிழக்கு மாநிலங்களில் கட்சியின் வளர்ச்சி பற்றி நாயுடு எடுத்துக்
கூறியதாக தெரிகிறது. </p><p>
</p><p>
மேலும் கட்சித் தலைமையில் மாற்றம் ஏற்படக்கூடும் என்று வௌியான செய்திகள் பற்றியும்
வெங்கய்யா நாயுடு வாஜ்பாயுடன் விவாதித்ததாக தெரிகிறது. </p>

<p> </p>

<p> </p>

<p>செஞ்சி
ராமச்சந்திரன் விவகாரம்: வாஜ்பாயுடன் எல்.கணேசன் - மு.கண்ணப்பன் சந்திப்பு </p>

<p> </p>

<p>புதுடெல்லி, ஜூன் 8- பிரதமர் வாஜ்பாயை நேற்று எல்.கணேசன்,
மு.கண்ணப்பன் ஆகியோர் சந்தித்து பேசினர். </p><p>
</p><p>
உதவியாளர் கைது </p><p>
</p><p>
ம.தி.மு.க.வைச் சேர்ந்த செஞ்சிராமச்சந்திரன் மத்திய நிதித்துறை இணை அமைச்ச ராக
இருந்தார். அவரிடம் பி.ஏ.வாக பாபு என்கிற பெரும்மாள்சாமி பணியாற்றினார். </p><p>
</p><p>
வருமானவரி அதிகாரிகள் இடமாறுதலுக்காக பாபு லஞ்சம் வாங்கி குவித்தார் என்று அவரை
சி.பி.ஐ. போலீசார் கைது செய்தார்கள். </p><p>
</p><p>
பிரதமருடன் சந்திப்பு </p><p>
</p><p>
இதை தொடர்ந்து பிரதமரின் ஆலோசனைப்படி செஞ்சி ராமச்சந்திரன் தனது மத்திய மந்திரி
பதவியை ராஜிமானா செய்தார். இந்த விவகாரம் டெல்லி அரசியலிலும், தமிழக அரசியலிலும்
பரபரப்பை ஏற்படுத்தியது. </p><p>
</p><p>
இந்த நிலையில் நேற்று மாலை பிரதமர் வாஜ்பாயை அவருடைய இல்லத்தில் ம.தி.மு.க.
அவைத்தலைவர் எல்.கணேசன், மத்திய அமைச்சர் மு.கண்ணப்பன் ஆகியோர் சந்தித்து பேசினார்கள்.
</p><p>
</p><p>
பிரதமரிடம் அப்போது, உதவியாளர் பாபு கைது செய்யப்பட்டது தொடர்பான லஞ்ச ஊழல்
விவகாரத்தில் செஞ்சி ராமச்சந்திரன் எந்த தவறும் செய்யவில்லை என்று எல்.கணேசனும்,
மு.கண்ணப்பனும் விளக்கினார்கள். </p><p>
</p><p>
அதன்பிறகு எல்.கணேசன் நிருபர்களிடம் கூறியதாவது:- </p><p>
</p><p>
செஞ்சி ராமச்சந்திரன் தனது நிலையை பிரதமரிடம் விளக்கும்படி எங்களை கேட்டு
கொண்டார். நாங்கள் பிரதம ரிடம் அதை சொன்னோம். உதவியாளர் பாபு மீது சுமத்
தப்பட்டிருக்கும் குற்றச்சாட்டு களுக்கும், செஞ்சி ராமச்சந்திரனுக்கும் எந்த
தொடர்பும் இல்லை என்பதை பிரதமரிடம் தெரிவித்தோம். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
சி.பி.ஐ. விசாரித்தால் ஒத்துழைப்பேன் </p><p>
</p><p>
செஞ்சிராமச்சந்திரன் பேட்டிநேற்று டெல்லியில்தான் செஞ்சி ராமச்சந்திரன்
இருந் தார். அவர் அங்கு நிருபர்களுக்கு பேட்டி அளித்து கூறியதாவது:- </p><p>
</p><p>
உதவியாளர் பாபு மீதான குற்றச்சாட்டுகளுக்கும் எனக்கும் தொடர்பு கிடையாது. ஆனாலும்
தார்மீக அடிப்படையில் பொறுப்பேற்று நான் மத்திய அமைச்சர் பதவியை ராஜினாமா செய்தேன். </p><p>
</p><p>
என்னிடம் இதுவரை சி.பி.ஐ. விசாரணை நடத்தவில்லை. அப்படி விசாரணை நடத்தினால்
சி.பி.ஐ.க்கு முழு ஒத்துழைப்பு தருவேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p> </p>

<p>லட்சுமணபுரி சி.பி.ஐ.
கோர்ட்டில் ஆஜர்: அத்வானி தூண்டுதலால் பாபர் மசூதி இடிப்பு: கரசேவகர்கள் 5 பேர்
பரபரப்பு குற்றச்சாட்டு </p>

<p> </p>

<p>லட்சுமணபுரி, ஜூன் 8- அத்வானி தூண்டுதலால் தான் பாபர் மசூதி
இடிக்கப்பட்டது என்று கரசேவகர்கள் 5 பேர் பரபரப்பு குற்றச்சாட்டு கூறியுள்ளனர். </p><p>
</p><p>
பாபர் மசூதி இடிப்பு வழக்கு லட்சுமணபுரியில் உள்ள சிறப்பு சி.பி.ஐ. கோர்ட்டில் நடந்து
வருகிறது. இந்த வழக்கு தொடர்பாக சி.பி.ஐ. போலீசார் சமீபத்தில் துணை பிரதமர் அத்வானி,
முரளி மனோகர் ஜோஷி உள்ளிட்டோர் மீது கோர்ட்டில் குற்றப் பத்திரிகை தாக்கல்
செய்தனர். </p><p>
</p><p>
வாக்குமூலம் </p><p>
</p><p>
இந்த நிலையில் மசூதி இடிப்பு வழக்கில் குற்றம் சாட்டப்பட்ட வினோத் வத்சா, சந்தோஷ்,
ஆர்.சி.கத்ரி, அமர்நாத் கோயல், ஆர்.என்.தாஸ் ஆகியோர் சி.பி.ஐ. கோர்ட்டில் நேற்று
ஆஜராகி வாக்குமூலம் அளித்தனர். அதன் பிறகு அவர்கள் கோர்ட்டு வளாகத்தில் நிருபர்களுக்கு
பேட்டி அளித்தபோது கூறியதாவது:- </p><p>
</p><p>
அத்வானி மற்றும் பா.ஜ. மூத்த தலைவர்களின் தூண்டு தலால்தான் பாபர் மசூதி இடிக்கப்பட்டது.
பாபர் மசூதி இடிப்பு வழக்கில் குற்றம் சாட்டப்பட்டுள்ள வி.ஐ.பி.க்கள் வழக்கு தனியாக
எடுக்கப்பட்டு சில சலுகைகள் அவர்களுக்கு வழங்கப்படுகிறது. குற்றம் சாட்டப்பட்ட அனைவரையும்
ஒரே மாதிரி நடத்த வேண்டும். </p><p>
</p><p>
இவ்வாறு அவர்கள் கூறினர். </p><p>
</p><p>
மறுப்பு </p><p>
</p><p>
இந்த குற்றச்சாட்டை அத்வானியின் வழக்கறிஞர் சத்யபால் ஜெயின் மறுத்துள்ளார்.
சண்டிகரில் இதுகுறித்து அவர் கூறுகையில், அத்வானிக்கு எதிராக எந்த ஆதாரமும் இல்லை. இது
அவருக்கு எதிரான ஜோடிக்கப்பட்ட குற்றச்சாட்டு. அத்வானிக்கு எதிராக பேசச்சொல்லி அவர்களை
சிலர் தூண்டியிருக்கிறார்கள். லிபரான் கமிஷன் முன் இதுவரை வைக்கப்பட்டுள்ள ஆதாரங்களில்
எந்த தலைவரும், எந்த சாட்சியும், எந்த சி.பி.ஐ. அதிகாரியும் அத்வானி, முரளி மனோகர்
ஜோஷி, உமாபாரதிக்கு எதிராக நேரடி ஆதாரத்தை சமர்ப்பிக்கவில்லை என்று கூறினார். </p><p>
</p><p>
கரசேவகர்கள் குற்றச்சாட்டு: அத்வானி ராஜினாமா செய்ய வேண்டும் - காங்கிரஸ் </p><p>
</p><p>
அத்வானி தூண்டுதலால் தான் பாபர் மசூதியை இடித்தோம் என்று அந்த வழக்கில்
குற்றம்சாட்டப்பட்ட 5 கரசேவகர்கள் கூறியுள்ளனர். எனவே அத்வானி, முரளி மனோகர் ஜோஷி
ஆகியோர் உடனடியாக பதவி விலக வேண்டும் என்று காங்கிரஸ் வலியுறுத்தியுள்ளது. </p><p>
</p><p>
இதுகுறித்து காங்கிரஸ் செய்தி தொடர்பாளர் ஆனந்த் சர்மா கூறியதாவது:- </p><p>
</p><p>
கரசேவகர்களின் குற்றச்சாட்டு இப்போது உடனடியாக வந்துவிடவில்லை. பாபர் மசூதி
இடிக்கப்பட்ட தினத்தில் இருந்தே அவர்களது (மத்திய அமைச்சர்கள்) பெயர் குற்றம்
சாட்டப்பட்டு வருகிறது. அவர்கள் தான் வி.எச்.பி., பஜ்ரங் தளத்துடன் சேர்ந்து சதி
செய்தார்கள் என்பது அனை வருக்கும் தெரியும். பாபர் மசூதி இடிப்பு வழக்கில் குற்றம்
சாட்டப்பட்ட மந்திரிகள் மத்திய அமைச்சரவையில் நீடிப்பது குறித்து பிரதமர் விளக்கம்
அளிக்க வேண்டும். கிரிமினல் சட்டத்தில் குற்றம் சாட்டப்பட்ட அத்வானி அமைச்சராக
பதவியில் நீடிப்பது ஜனநாயகத்தை அவமதிப்பது போலாகும். அவர்களாக ராஜினாமா செய்யவேண்டும்
அல்லது அவர்களது ராஜினாமா கடிதத்தை பிரதமர் கேட்டுப்பெறவேண்டும். </p><p>
</p><p>
இவ்வாறு கூறினார். </p>

<p> </p>

<p> </p>

<p>மாணவர்களுக்கு பஸ் பாஸ்
திட்டம் ரத்து: ரூ.250 கோடியை 96 கோடியாக குறைத்ததுதான் சீரமைப்பா? போக்குவரத்து
மந்திரிக்கு கருணாநிதி கேள்வி </p>

<p> </p>

<p>சென்னை, ஜூன் 8- மாணவர்களுக்கான பஸ் பாஸ் திட்டத்தை ரத்து
செய்து விட்டு, சீரமைப்புதான் செய் துள்ளோம் என்று சொன்ன போக்குவரத்து மந்திரிக்கு
கருணாநிதி கண்டனம் தெரி வித்தார். தி.மு.க. ஆட்சியில் பஸ் பாஸ் திட்டத்துக்கு வழங்கிய
250 கோடி ரூபாயை 96 கோடி ரூபாயாக குறைத்தது தான் சீரமைப்பு நடவடிக்கையா? என்று அவர்
கேள்வி விடுத்துள்ளார். </p><p>
</p><p>
பஸ் தனியார்மயம் </p><p>
</p><p>
தி.மு.கழக தலைவர் கருணாநிதியிடம் கேட்கப்பட்ட கேள்விகளும் அவற்றுக்கு அவர் அளித்த பதில்களும்
வருமாறு:- </p><p>
</p><p>
கேள்வி:- தமிழக அரசின் பேருந்துகளை தனியார் உடமையாக்கும் திட்டம் என்னவாயிற்று? </p><p>
</p><p>
பதில்:- அரசு போக்குவரத்துக் கழகப் பேருந்துகளை தனியாருக்கு தாரைவார்க்க அரசு தயாராக
இருந்தாலும், அடுத்து வரும் அரசிலே அது என்னவாகுமோ என்ற பயத் திலே அந்தத் திட்டத்திலே
ஈடுபட எந்தத் தனியாரும் ஆர்வத்தோடு முன்வரவில்லை. அரசு போக்குவரத்துத்
தொழிலாளர்களுக்கு மூன்றாண்டுக்கு ஒருமுறை ஊதிய உயர்வு வழங்கப்பட வேண்டும். இந்த ஊதிய
உயர்வு 1-9-2001 முதல் இந்த அரசு பொறுப்புக்கு வந்த பிறகு வழங்கப்படவே இல்லை. </p><p>
</p><p>
போக்குவரத்து தொழிலாளர்கள் பல ஆண்டுகளாகப் பெற்று வந்த எக்ஸ்கிரேசியா தொகை 3,500
ரூபாய் இரண்டாண்டு காலமாக அவர்களுக்கு வழங்கப்படவில்லை. பொங்கல் திருநாள்தோறும்
வழங்கப்பட்டு வந்த ஊக்கத்தொகை ரூ.625 நிறுத்தி வைக்கப்பட்டுள்ளது. இதை எதிர்த்துப்
போராடிய 30 ஆயிரத்திற்கு மேற்பட்ட தொழி லாளர்கள் 17 நாட்கள் சிறையிலே வாடியதுதான்
மிச்சம். இந்த நிலையில்தான் பேருந்துகள் தனியார் மயத்திட்டம் இப்போது உள்ளது. </p><p>
</p><p>
பழிவாங்கும் குணம் </p><p>
</p><p>
கேள்வி:- அரசியல் ரீதியாக ஜெயலலிதா பழிவாங்கும் குணம் கொண்டவர் என்பது அனைவருக்கும்
தெரிந்த ஒன்று. ஆனால் சிறு சிறு விஷயங்களிலே கூட அவர் பழிவாங்கும் செயலிலே ஈடுபடுவார்
என்று கூறுகிறார்களே, அது உண்மைதானா? </p><p>
</p><p>
பதில்:- மருத்துவக்கல்லூரி மாணவர்கள் நடத்திய போராட்டத்திற்கு அரசு டாக்டர்கள்
சங்கத்தினர் ஆதரவு தெரிவித்து ஒருநாள் அடையாள வேலை நிறுத்தம் செய்ததற்காக, அந்தச்
சங்கத்தின் முக்கிய நிர்வாகிகளையெல்லாம் ஜெயலலிதா அரசு வெவ்வேறு ஊர்களுக்கு மாற்றியதும்,
ராணிமேரி கல்லூரி மாணவிகள் நடத்திய போராட்டத்திற்கு தார்மீக ஆதரவு காட்டிய அந்தக்
கல்லூரியின் ஆசிரியைகளையெல்லாம் பல்வேறு இடங்களுக்குப் பந்தாடியிருப்பதும் நீங்கள் கேட்ட
கேள்விகளுக்கு தக்க உதாரணங்களாகும். </p><p>
</p><p>
பதுங்குவது பாயத்தான் </p><p>
</p><p>
கேள்வி:- திட்டமிட்டபடி ஜூலை 2 முதல் காலவரையற்ற வேலை நிறுத்தத்திலே ஈடுபடப்
போவதாக ஆசிரியர், அரசு அலுவலர் சங்கக் கூட்டமைப்பு அறிவித்துள்ளது. ஆனால் ஒவ்வொரு
முறையும் கடைசி நேரத்தில் மிரட்டலுக்கு அஞ்சி போராட்டத்திலிருந்து பின் வாங்கும்
என்.ஜி.ஓ. சங்கம் இந்த முறை என்ன செய்யும்? </p><p>
</p><p>
பதில்:- புலி, பதுங்குவது பாய்வதற்குத்தான் என்று செயலில் காட்டப் போகிறதோ என்னவோ. </p><p>
</p><p>
கேள்வி:- அரசு உதவிபெறும் பள்ளிகளில் ஏற்படும் ஆசிரியர் காலி இடங்களை
நிரப்புவதற்குக்கூட தமிழக அரசு தடை உத்தரவு பிறப்பித்திருக்கிறதே? </p><p>
</p><p>
பதில்:- வீண் சவடால் விளம்பரங்களுக்கு என்றால் நிதி விஷயத்தில் பற்றாக்குறை இல்லை
என்பதும்; இதுபோன்ற காரியமென்றால் கையை அகல விரிப்பதும் இந்த ஆட்சியில் நாம் கண்ட
உண்மைகள்தானே. </p><p>
</p><p>
குறைப்புதான் சீரமைப்பா? </p><p>
</p><p>
கேள்வி:- போக்குவரத்துத் துறை அமைச்சர் பள்ளிக்கூட, கல்லூரி மாணவர்களுக்கு பஸ் பாஸ்
ரத்து செய்யப்பட்டிருப்பதை, ரத்து என்று சொல்வதை விட சீரமைப்பு என்றுதான் குறிப்பிட
வேண்டுமென்று சொல்லியிருக்கிறாரே? </p><p>
</p><p>
பதில்:- அவரே அந்த அறிக் கையிலே தி.மு.கழக ஆட்சியில் பஸ் பாஸ் வழங்கிட 250 கோடி
ரூபாய் ஒதுக்கப்பட்டதாகவும், அந்தத்தொகை தற்போது 96 கோடி ரூபாயாக
குறைக்கப்பட்டிருப்பதாகவும் ஒப்புக் கொண்டுள்ளார். இப்படி குறைப்பதற்குப் பெயர்தான்
ஜெயலலிதா ஆட்சியில் சீரமைப்பு போலும். </p><p>
</p><p>
கேள்வி:- தமிழ் அறிஞர்களின் வாரிசுகளுக்கு மருத்துவக் கல்லூரிகளில் ஆண்டுதோ றும்
சிறப்பு இட ஒதுக்கீட்டின் அடிப்படையில் கிடைத்து வந்த ஆறு இடங்களுக்கு இந்த ஆட்சியிலே
இந்த ஆண்டு முற்றுப்புள்ளி வைக்கப்பட்டு விட்டதாமே? </p><p>
</p><p>
பதில்:- இப்படி எத்த னையோ காரியங்களுக்கு முற்றுப்புள்ளிகள் வைக்கப்பட்டு விட்டன. என்
செய்வது; புள்ளி வைத்துத்தானே கோலம் போட வேண்டும் என்பார்கள். என்ன கோலம்
என்கிறீர்களா? அது தான் இந்த ஆட்சியின் அலங் கோலம். </p><p>
</p><p>
உலக மகா பொய் </p><p>
</p><p>
கேள்வி:- உலக அழகி போட்டி போல பொய்களுக்குள் போட்டி வைத்தால் எந்தப் பொய் உலக
மகாப் பொய்யாக கிரீடம் சூட்டிக்கொள்ளும்? </p><p>
</p><p>
பதில்:- பொய் சொன்னக் குற்றத்திற்காக அரிச்சந்திரன் மயானம் காத்தான் என்ற
பொய்க்குத்தான் அந்தப் பட்டமும் பரிசும் கிடைக்கும். ஆனால் இதுவரையில் நமது புலன்
விசாரணைப் பத்திரிகைகள் அந்த முயற்சியில் ஈடுபடவில்லை என்று தெரிகி றது. </p><p>
</p><p>
கேள்வி:-புலன் விசாரணைப்பத்திரிகை நடத்த முக்கியமாக என்ன தேவைகள்? </p><p>
</p><p>
பதில்:-மிக மிக முக்கியமான தேவை; வெளுத்ததெல்லாம் பால் என்று நம்பி
சுண்ணாம்புத்தண்ணீரையும் குடிக்கிற பாமரத்தனமான வாசகர்கள் </p><p>
</p><p>
கேள்வி:- அரசியலில் ஒரு தலைவரை இழித்தோ பழித்தோ விமர்சிப்பதற்கு என்ன செய்ய
வேண்டும்? </p><p>
</p><p>
பதில்:- அவர் சொல்லாத ஒன்றை சொன்னதாக இட்டுக் கட்டிச் சொல்லி; அதை வைத்து அந்தப்
பொய் மீது விஷமத் தனமான விமர்சனங்களை அடுக்க வேண்டும். ஆனால் உண்மை வௌியாகும்போது
அடுக்கப்பட்ட அட்டைகள் பொல பொலவென விழுந்து விடும். </p><p>
</p><p>
விளம்பரம் நின்றுவிடும் </p><p>
</p><p>
கேள்வி:-மத்திய அரசிலே ஒரு அமைச்சரின் உதவியாளர் செய்த குற்றத்திற்காக அந்த
உதவியாளர் கைது செய்யப்பட்டதும், அந்தத் துறையின் அமைச்சர் தார்மிகப் பொறுப் பேற்று
தன் பதவியை விட்டு விலகியதும் அனைத்து ஏடுகளிலும் வௌிவந்தது. ஆனால் தமிழகத்தில் ஒரு
அமைச்சரின் உதவியாளர் இதே குற்றத்தின் அடிப்படை யில; cjtpahsh;
gjtpapypUe;J ePf;fg;gl;lhh;. Jiwapd; mikr;rhplkpUe;J me;j Kf;fpakhd ,yhfh
kpd;ntfj;jpy; khw;wg;gl;lJ. Mdhy; ,Jgw;wpa bra;jpia jkpHfj;jpny cs;s xl;L
bkhj;j ehnsLfSk; kiwj;J tpl;lnj> </p><p>
</p><p>
gjpy;-,ijbay;yhk; tpsk;guk; bra;jhy; tpsk;guk; epd;W tpLnk </p><p>
</p><p>
g[jpa FGth> </p><p>
</p><p>
nfs;tp-jp.K.f.tpy; cah; epiy bray; jpl;lf;FG vd;gJ ,g;nghJjhd; g[jpjhf
mikf;fg;gl;lJ vd;W rpyh; vGJfpwhh;fns mth;fsJ VLfspny> </p><p>
</p><p>
gjpy;-me;jf;FG Vw; fdnt fHfr;rl;l jpl;lg;g[j;jfj;jpy; ,lk;bgw;Ws;s
FGjhd;-me;jf;FGtpd; Tl;l';fs; eilbgw;W Kf;fpa Kot[fSk; vLf;fg; gl;Ls;sd. me;jr;
bra;jp mt;tg;nghJ gj;jphpiffspYk; te;Js;sJ. </p><p>
</p><p>
,t;thW Twp cs;shh;. </p>

<p> </p>

<p> </p>

<p>இளைஞர் காங். தலைவர்
பதவி: கோஷ்டிகள் உச்சகட்ட முயற்சி: </p>

<p>அகில இந்திய
பொதுச்செயலாளரிடம் சென்னையில் பலம் காட்டினர் </p>

<p> </p>

<p>சென்னை, ஜூன் 8- இளைஞர் காங்கிரஸ் தலைவர் பதவிக்கு
கோஷ்டிகள் உச்சகட்ட முயற்சியில் இறங்கி உள்ளது. சென்னை வந்த அகில இந்திய
பொதுச்செயலாளர் முன் தங்கள் பலத்தை அவர்கள் போட்டி போட்டு காட்டினர். </p><p>
</p><p>
விரைவில் அறிவிப்பு </p><p>
</p><p>
தமிழ்நாடு இளைஞர் காங்கிரஸ் தலைவராக தற்போது சி.டி.மெய்யப்பன் உள்ளார். அவருக்கு
வயதாகி விட்டதாலும், 6 ஆண்டுக்கு மேல் அவரே தலைவராக பதவி வகித்துவிட்டதாலும் புதியவரை
தலைவராக்க மேலிடம் முடிவு செய்தது. இதையடுத்து சி.டி.மெய்யப்பனுக்கு மாநில செயலாளர்
பதவியும் வழங்கப்பட்டு விட்டது. இருந்தாலும் த.மா.கா. இணைப்புக்கு பிறகு இளைஞர் காங்கிரஸ்
தலைவர் பதவிக்கு கடும் போட்டி நிலவுவதால் யாரை நியமிப்பது என டெல்லி திகைத்த வண்ணம்
உள்ளது. </p><p>
</p><p>
33 கார் வரவேற்பு </p><p>
</p><p>
இந்த நிலையில் சமீபத்தில் டெல்லியில் நேர் காணல் நடந்தது. அதில் எம்.எஸ்.காம ராஜ்
உள்பட சிலர் நேரில் கலந்து கொண்டு தங்கள் பணிகளை எடுத்துக் கூறினர். இதையடுத்து
பாண்டிச்சேரியில் 2 நாள் நடக்கும் பயிற்சி முகாமுக்காக இளைஞர் காங்கிரசுக்கு
பொறுப்பு வகிக்கும் அகில இந்திய பொதுச் செயலாளர் முகுல் வாஷ்னிக் நேற்று முன்தினம்
சென்னை வந்தார். இளைஞர் காங்கிரஸ் தலைவர் பதவி போட்டி களத்தில் நிற்பவர்கள்
விமான நிலையத்தில் இரவு 10.30 மணி வரை காத்திருந்து அவருக்கு உற்சாக வரவேற்பு
அளித்தனர். </p><p>
</p><p>
இதில் ஜி.கே.வாசன் தீவிர ஆதரவாளர்களில் ஒருவரான நியூ.மகாலிங்கம் 33 அம்பாசிடர்
கார்களை விமான நிலையத்தில் வரிசையாக நிறுத்தி அனைவரையும் அதிசயிக்க வைத்தார். மேலிட
தலைவர் வந்ததும் அவருக்கு ஆளுயர மாலை, கிரீடம் அணிவித்து வாசன் சார்பாக வரவேற்கிறேன்
என்று அசத்தி உள்ளார். </p><p>
</p><p>
மேலிட தலைவர் ஓட்டம் </p><p>
</p><p>
ஜி.கே.வாசன் ஆதரவை பெற்றுள்ளதாக நெல்லை அமீர்கானும் முகுல் வாஷ்னிக்கிடம் கூறி உள்ளார்.
இளங்கோவன் ஆதரவை பெற்ற எம்.எஸ்.காம ராஜ், முன்னாள் எம்.பி. அன்பரசு மகன் அருள்
அன்பரசு, முன்னாள் எம்.பி. கிருஷ்ணசாமி மகன் விஷ்ணு பிரசாத் உள்பட சிலரும் தங்கள்
பலத்தை காட்டி இருந்தனர். </p><p>
</p><p>
அவர்களது வரவேற்பை பெற்ற மேலிட தலைவர் டெல்லிக்கு சென்றதும் முடிவைச் சொல்வதாகக் கூறி
விட்டு சென்னை நகருக்குள்ளேயே வராமல் பாண்டிச் சேரிக்கு காரில் பறந்து விட்டார். </p>

<p> </p>

<p> </p>

<p>சிறையில் அழகிரியுடன்
பேசியது என்ன? மு.க.ஸ்டாலின் பேட்டி </p>

<p> </p>

<p>சென்னை, ஜூன் 8- சிறையில் சந்தித்தபோது அழகிரியுடன் பேசியது
என்ன? என்பது பற்றி மு.க.ஸ்டாலின் பேட்டியளித்துள்ளார். </p><p>
</p><p>
அழகிரி கைது </p><p>
</p><p>
தி.மு.க. முன்னாள் அமைச்சர் தா.கிருட்டினன் கடந்த 20-ந்தேதி மதுரையில் மர்ம ஆசாமிகளால்
படுகொலை செய்யப்பட்டார் அல்லவா? இந்த கொலை நடந்த 24 மணி நேரத்தில்
மு.க.அழகிரியை போலீசார் கைது செய்தனர். இந்த கொலைக்கும் தனக்கும் சம்பந்தம் இல்லை
என்று அழகிரி கூறினார். அவர் தற்போது திருச்சி சிறையில் அடைக்கப்பட்டுள்ளார். </p><p>
</p><p>
தி.மு.க. துணை பொதுச்செயலாளர் மு.க.ஸ்டாலின் நேற்று திருச்சி சென்று அண்ணன் மு.க.அழகிரியை
சந்தித்தார். இருவரும் 30 நிமிடம் பேசினர். அதன்பிறகு சென்னை திரும்பிய ஸ்டாலின்
ஆங்கிலப் பத்திரிகைக்கு பேட்டி அளித்துள்ளார். </p><p>
</p><p>
அதன் விவரம் வருமாறு:- </p><p>
</p><p>
தேர்தல் பணி </p><p>
</p><p>
நான் அமெரிக்காவில் இருந்து மே 22-ந்தேதி திரும்பிய பிறகு சகோதரரை சந்திக்க
இயலவில்லை. அவரை உடனடியாக குடும்பத்தினர் சந்திக்க வேண்டி இருந்தது. சிறை விதிப்படி ஒரே
நேரத்தில் 3 பேர்தான் சந்தித்து பேச முடியும். அதனால் நான் காத் திருந்தேன். மேலும்
கட்சி தேர்தலும் முக்கிய கட்டத்தை அடைந்தது. அந்த பணியில் ஈடுபட்டு இருந்தேன். அது
எல்லாம் முடிந்துவிட்டதால், இப்போது அழகிரியை சந் தித்து பேச முடிவு செய்தேன். </p><p>
</p><p>
கட்சியில் எனக்கும் அவருக்கும் சண்டை இருப்பதாக பத்திரிகைகள் கூறுகின்றன. </p><p>
</p><p>
சண்டை இல்லை </p><p>
</p><p>
உண்மையில் அமெரிக்காவில் சிகிச்சை பெறும் மத்திய அமைச்சர் முரசொலி மாறனை பார்க்க
சென்று இருந்தபோது அங்கிருந்து நான் அழகிரியுடன் தொலை பேசியில் தொடர்பு கொண்டு நீண்ட
நேரம் பேசினேன். ஆனாலும் எங்களுக்குள் சண்டை இருப்பதான கருத்தை பத்திரிகைகள் இன்னும்
மாற்றிக் கொள்ளவில்லை. கட்சியில் எங்களுக்குள் சண்டை எதுவும் இல்லை என்பதை தலைவர்
கலைஞரும் விளக்கி இருக்கிறார். </p><p>
</p><p>
அழகிரியை போலீஸ் வேனில் மிகவும் வேகமாக மதுரைக்கு அழைத்து சென்று உள்ளனர்.
அதிகாரவர்க்கம் இப்படி நடத்துவது பற்றி எல்லாம் அழகிரி கவலைப்பட வில்லை. அவரை ஒரே ஒரு
விஷயம்தான் பாதித்து உள்ளது. தா.கிருட்டினன் கொலை குற்றச்சாட்டை எத்தனை காலம் தாங்கிக்
கொண்டு இருக்க முடியும்? என்று அவர் என்னிடம் வேதனையுடன் கேட்டார். இதில் குற்றம்
சாட்டுவதை தன்னால் புரிந்து கொள்ள முடியவில்லை என்றும் அவர் கூறினார். </p><p>
</p><p>
இவ்வாறு அவர் கூறி உள்ளார். </p>

<p> </p>

<p> </p>

<p>இந்தியாவின் முதன்மை
மாநிலமாக புதுவையை மாற்றிக் காட்டுவோம்; முதல்வர் ரெங்கசாமி பேட்டி </p>

<p> </p>

<p>பழநி, ஜூன்.8- இந்தியாவிலேயே முதன்மை மாநிலமாக புதுவையை
மாற்றிக் காட்டுவோம் என்று அம்மாநில முதலமைச்சர் ரெங்கசாமி கூறினார். </p><p>
</p><p>
பேட்டி </p><p>
</p><p>
பழநி-கொடைக்கானல் சாலையில் உள்ள ஆத்மா பவுண்டேசனுக்கு வருகை புரிந்த புதுவை முதல்வர்
ரெங்கசாமி நிருபர்களுக்கு அளித்த பேட்டியில் கூறியதாவது:- </p><p>
</p><p>
புதுவையை பொறுத்தவரை வரி உயர்வு இல்லாத பட் ஜெட்டை கொடுத்துள்ளோம். சோனியா
எண்ணப்படியும் பெருந்தலைவர் காமராஜர் ஆற்றிய ஆட்சியின்படியும் மக்களுக்கு தேவையான
அடிப்படை தேவைகளை நிறைவேற்றி வருகிறோம். குறிப்பாக வறுமை கோட்டுக்கு கீழ் உள்ள
மக்களின் வாழ்க்கையை மேம்படுத்த அரசு பாடுபட்டு வருகிறது. </p><p>
</p><p>
அமைதி மாநிலம் </p><p>
</p><p>
புதுவையில் ஒரு நல்ல அமைதியான மாநிலமாக திகழ சட்டம் ஒழுங்கு நல்லமுறையில்
சீரமைக்கப்பட்டு வருகிறது. புதிய தொழிற்சாலைகள் சுற்றுச்சூழல் பாதிக்கப்படா வண்ணம்
அமைக்க நடவடிக்கை எடுத்து வருகிறோம். இதனால் இளைஞர்களுக்கு வேலை வாய்ப்பு கிடைக்க
திட்ட மிடப்பட்டுள்ளது. புதுவையை சிறப்புபொருளாதார மண்டலமாக உயர்த்த சுங்கவரி இல்லாத
துறைமுகமாக நட வடிக்கை எடுக்கப்பட்டு வருகிறது. </p><p>
</p><p>
சிறப்பான கல்வி </p><p>
</p><p>
மாநிலத்தில் உள்ள அனைத்து மாணவ, மாணவிகளுக்கு சிறப்பான நல்ல கல்வி யினை வழங்குவதில்
அரசு கவனமாக இருக்கிறது. தற்போது 82 விழுக்காடு கல்வி யறிவு பெற்ற மாநிலமாக திகழ்கிறது.
அதேபோன்று மருத்துவ வசதி எல்லா மக்களுக்கும் சிறப்பாக கிடைக்க ஏற்பாடு செய்து வருகிறோம்.
</p><p>
</p><p>
முதன்மை மாநிலம் </p><p>
</p><p>
இந்தியாவிலேயே அனைத்து துறையிலும் முதலிடம் வகிக்கின்ற வகையில் மாறிய திட்டப்பணிகள்
செயல் படுத்தப்பட்டு வருகிறது. விவசாயிகளுக்கு உதவுவதற்காக உழவர் உதவியகம் பல இடங்களில்
அமைக்க திட்டமிடப்பட்டு செயல்படுத்தப்பட்டு வருகிறது. அதேபோன்று குடிதண்ணீர் வேளாண்மைக்கு
தேவையான தண்ணீர் தட்டுப்பாடின்றி கிடைக்க செயலாக்கம் செய்து நிலத்தடி நீரை மேம்படுத்த
தேவையான நடவடிக்கை எடுக்கப்பட்டு வருகிறது. </p><p>
</p><p>
இலவச சைக்கிள் </p><p>
</p><p>
அரசு பள்ளிகளில் பயிலும் அனைத்து மாணவ மாணவிகளுக்கும் காமராஜர் திட்ட மான மதிய உணவு
திட்டமும், முன்னாள் பிரதமர் ராஜீவ் காந்தியின் காலை சிற்றுண்டி திட்டமும் நிறைவேற்றப்பட்டு
உள்ளது. எல்லா மாணவ மாணவிகளுக்கும் இலவச சைக்கிள் வழங்கப்படும். </p><p>
</p><p>
உலக வங்கியில் கடன் வாங்க உத்தேசம் இல்லை. மத்திய அரசின் திட்டக்குழு கூட்டத்தில்
கலந்து கொண்டேன். இந்த ஆண்டு புதுவையின் வளர்ச்சி பணிகளுக்காக ரூ.452.7 கோடியினை
மத்திய அரசு ஒதுக்கியுள்ளது. இது கடந்த ஆண்டை விட ரூ.50 கோடி அதிகமாகும். எந்த ஆண்டும்
ஒரே வருடத்தில் புதுவைக்கு ரூ.50கோடி உயர்த்தியது இல்லை. புதுவை முன் மாதிரி மாநிலமாக
செயல்படுவதால் தான் இந்த தொகை கிடைத்துள்ளது. </p><p>
</p><p>
இவ்வாறு கூறினார். </p><p>
</p><p>
பேட்டியின் போது டாஸ் கோ சேர்மன் பாலன், முன்னாள் புதுவை அமைச்சர் கந்தசாமி ஆகியோர்
உடன் இருந்தனர். </p><p>
</p><p>
வரவேற்பு </p><p>
</p><p>
பழநி வந்திருந்த புதுவை முதல்வரை இ.காங்கிரஸ் அகில இந்திய பொதுக்குழு உறுப்பினர்
வி.திருஞான சம்பந்தம், முன்னாள்அ.தி.மு.க. எம்.எல்.ஏ. கே.குப்புச்சாமி, லயன்சு
வட்டாரத்தலைவர் கே.சுப்புராஜ், ஆத்மா பவுண்டேசன் தங்கவேல், ராஜூ மற்றும் பி.டி.சி. நாகராஜன்,
எஸ்.சி.பி.காந்தி ஆகியோர் வரவேற்றனர். </p><p>
</p><p>
சுவாமி தரிசனம் </p><p>
</p><p>
முன்னதாக புதுவை முதல்வர் ரெங்கசாமி ஆத்மா பவுண்டேசன் நிறுவனர் அசோக்ஜி யை சந்தித்து
ஆசி பெற்றார். பின்னர் பெரியாவுடையார் கோவில் மானூர் சுவாமிகள் சமாதி நிலையம்,
மலைக்கோவில் ஆகிய இடங்களுக்கு சென்று சுவாமி தரிசனம் செய்துவிட்டு புதுவை புறப்பட்டுச்
சென்றார். </p><p>
</p><p>
புதுவை முதல்வரின் எளிமை </p><p>
</p><p>
பழநிக்கு நேற்று வந்திருந்த புதுவை முதல்வர் ரெங்கசாமி எளிமையின் சின்னமாக திகழ்ந்தார்.
அவர் காலை 6மணிக்கு பழநிக்கு வந்த போதிலும் தமிழக போலீசார் காலை 10மணிக் குத்தான்
அவரின் பாதுகாப்பு பணிக்கு வந்தனர். அவர் கோவில் களுக்கு செல்லும்போது நாங்கள் உடன்வர
வேண்டுமா என்று முதல்வரின் பாதுகாப்பு அதிகாரிகளிடம் கேட்டபோது அவர்கள் நமக்காக
சிரமப்பட வேண்டாம். அவர்களின் வழக்கமான பணி களை கவனிக்க சொல்லுங்கள் நானும் ஒரு
சாதாரண மனிதன் தானே என்று கூறிவிட்டார். பின்னர் தன்னை சந்திக்க வந்தவர்களிடம்
கைகுலுக்கி நலம் விசாரித்தார். இதனை பார்க்கும்போது தமிழக முதல்வருக்கும், புதுவை
முதல்வருக்கும் பந்தா செய்வதில் மலைக்கும் மடுவுக்கும் உள்ள வித்தியாசம் தெரிந்தது. </p>

<p> </p>

<p> </p>

<p>இடதுசாரி கட்சிகளுடன் கலந்து
பேசி முடிவு: பாராளுமன்ற தேர்தலில் காங்கிரசுடன் கூட்டணி? புதுவையில் நல்லகண்ணு பேட்டி </p>

<p> </p>

<p>புதுச்சேரி, ஜூன் 8-. பாராளுமன்ற தேர்தலில் காங்கிரசுடன் கூட்டணி
அமைப்பது குறித்து இடது சாரி கட்சிகளுடன் கலந்து பேசி முடிவு செய்யப்படும் என்று புதுவையில்
நல்லகண்ணு கூறினார். </p><p>
</p><p>
பேட்டி </p><p>
</p><p>
இந்திய கம்யூனிஸ்டு கட்சியின் தமிழ் மாநில செயலாளர் நல்லகண்ணு புதுவையில்
நிருபர்களிடம் கூறியதாவது:- </p><p>
</p><p>
பாராளு மன்றத்தில் பொடா சட்டத்தை கொண்டு வந்த போதே நாங்கள் எதிர்ப்பு
தெரிவித்தோம். இப்போதுள்ள குற்றப்பிரிவு தடுப்பு சட்டங்களே போதும் என்று கூறினோம்.
ஆனால் பொடாவை ஆதரித்த வர்கள் தான் தற்போது அதனால் பாதிக்கப்பட்டு உள்ளனர். உதாரண
மாக பொடாவை ஆதரித்து பேசிய வைகோ கைது செய்யப்பட்டு கடந்த ஓராண்டு காலமாக சிறையில்
உள்ளார். ஆனால் மத்திய உள்துறை அமைச்சகம் அவரை விடுதலை செய்ய எந்த நடவடிக்கையும்
எடுக்கவில்லை. எனவே பாரதிய ஜனதா கூட்டணியில் உள்ள திராவிட இயக் கங்களும், பா.ம.க.வும்
கூட்டணி குறித்து மறு ஆய்வு செய்ய வேண்டும். </p><p>
</p><p>
தமிழகத்தில் திராவிட இயக் கங்களின் பிளவை பயன்படுத்தி பாரதிய ஜனதா தன் அதிகார
போக்கை காட்டி வருகிறது. எனவே பாரதிய ஜனதாவுடன் கூட்டணி குறித்து திராவிட இயக்கங்கள்
சிந்திக்க வேண்டும். காவிரி நதிநீர் பிரச்சினை யில; cldoahf
,Wjp Kot[ vLf;f ntz:Lk;. ,y;iy badpy; midj;J miz fisa[k; fh;ehlf muR fl;o
Kof;Fk; mgha epiy Vw;gl;LtpLk;. </p><p>
</p><p>
fhtphp bly;lh gFjpapy; cSe;J gaphpl ntz:L bkd;W jkpHf Kjy;th; b$a yypjh
Twpa[s;shh;. jz:zPnu ,y;yhjnghJ vg;go gaph; bra;a Koa[k; vd;gij mth; rpe;jpf;f
ntz:Lk;. jkpHfk; kw;Wk; g[Jitapy; jdpahh; fy;Y}hp fspy; fy;tpf;fl;lzk; mjpfkhf
cs;sJ. ,jdhy; ViH kw;Wk; eLj;ju tFg;ig nrh;e;j khzth;fs; nru Koahj epiy
Vw;gl;Ls;sJ. ,jd; fhuzkhf fy;tpia muR bghWg;gpy; ,Ue;J tpLtpj;J bfhs;Sk; epiy
te;Js;sJ. ,jd; \yk; bgUk;ghyhd ViH khzth;fs; ghjpf;fg;gLthh;fs;. </p><p>
</p><p>
vd;$p.fy;Y}hp </p><p>
</p><p>
jdpahh; vd;$pdPahp'; kw;Wk; kUj;Jt fy;Y}hpfspy; jFjp ,lxJf;fPL mog;gilapy;
khzth;fis nrh;f;fhky; mjpf fl;lzk; th';fp khzth;fis nrh;f;fpwhh;fs;. ,jpy; muR
cldoahf ftdk; brYj;j ntz:Lk;. nkYk; mfpy ,e;jpa bjhHpy;El;g ft[d;rpy;
tpjpfspd;go bray;gl ntz:Lk;. fh;ehlfh tpypUe;J jz:zPh; bgw;Wj; ju kj;jpa muir
jkpHfk; kw;Wk; g[Jit muRfs; tw;g[Wj;j ntz:Lk;. rkr;rPh; thp tpjpg;ghy;
ghjpf;fg;gl;Ls;s g[Jit khepyj;Jf;F Kjy;th; u';frhkp jw;nghJ bgw;W te;Js;s epjp
nghJkhdJ my;y. nkYk; epjpia mjpfk; nfl;L bgw;wpUf;f ntz:Lk;. </p><p>
</p><p>
tuntw;fj;jf;fJ </p><p>
</p><p>
kj;jpa ghujpa $djh my;yhj khw;W Tl;lzp mika ntz:Lbkd;W nrhdpah TwpapUg;gJ tu
ntw;fj;jf;fJ. mjpy; v';fSf;F cld;ghL cz:L. Mdhy; khw;W Tl;lzp bfhs;if
mog;gilapy; mika ntz:Lk;. kjr;rhh; gw;w Tl;lzpahf mike;J tplf;TlhJ. kj;jpapy;
ve;j fhyj;jpYk; jdpf;fl;rp Ml;rp Vw;gl tha;g;gpy;iy. Tl;lzp mikr;ruitjhd;
Vw;gLk;. gp.n$.gpf;F vjpuhd khw;W Tl;lzp Fwpj;J nrhdpah bjhptpj;j fUj;J Fwpj;J
,lJrhhpfSld; fye;Jngrp Kot[ vLg;nghk;. </p><p>
</p><p>
,t;thW mth; Twpdhh;. </p><p>
</p><p>
,e;jpa fk;a{dp!;L fl;rpapd; g[Jit khepy brayhsh; ehuh.fiy ehjd;/ Kd;dhs;
mikr;rh; tpRtehjd;/ efu brayhsh; Mde;j; cldpUe;jdh;. </p>

<p> </p>

<p> </p>

<p>எள் பயிரிட முதல்வர்
வற்புறுத்தல்: காவிரி நீர் உரிமையை ஜெ. கைகழுவி விட்டார்; டாக்டர் ராமதாஸ்
குற்றச்சாட்டு</p>

<p> </p>

<p>சென்னை,
ஜூன் 8- எள் பயிரிடும்படி விவசாயிகளை வலியுறுத்தியதன் மூலம் முதல்வர் ஜெயலலிதா காவிரி
நீர் உரிமையை கை கழுவிவிட்டார் என்று டாக்டர் ராமதாஸ் குற்றம்சாட்டி உள்ளார். </p><p>
</p><p>
ஜெ. ஆலோசனை </p><p>
</p><p>
பா.ம.க. நிறுவனர் டாக்டர் ராமதாஸ் நேற்று வௌியிட்ட அறிக்கையில் கூறி இருப்ப தாவது:- </p><p>
</p><p>
காவிரி டெல்டா விவசாயிகள் மாற்று பயிர்களை பயிரிடுவதற்கான ஆலோசனைகளை அரசு வழங்க
வேண்டுமென்று பெங்களூரில் 2 நாட்களுக்கு முன்பு நடந்த ஒரு கூட்டத்தில் வேண்டுகோள்
விடப்பட்டிருக்கிறது. அந்த அதிர்ச்சியிலிருந்து டெல்டா விவசாயிகள் மீண்டு வருவதற்குள்ளாக,
தமிழக முதலமைச்சரும் மாற்று பயிர் ஆலோசனையை வழங்கியிருக்கிறார். கர்நாடகத்தில்
இத்தகைய ஆலோசனை எதுவும் வழங்கப்படவில்லை. </p><p>
</p><p>
வீராப்பு என்ன ஆயிற்று? </p><p>
</p><p>
கடந்த ஆண்டு தொடர்ச்சியாக மூன்று போகமும் பொய்த்துப்போய்விட்ட நிலையில், இந்த
ஆண்டிலாவது சாகுபடிக்கு தேவையான தண்ணீரை பெற்றாக வேண்டும் என்றும், இதற்காக முதலமைச்சர்
டெல்லியில் முகாமிட்டு பிரதமரையும், சம்பந்தப் பட்ட மற்ற அனைவரையும் சந்தித்து பேச
வேண்டும் என்றும் சில நாட்களுக்கு முன்பு வற்புறுத்தியிருந்தேன். இது ஒன்றும் நடைமுறை
சாத்தியமில்லாதது அல்ல. ஆனாலும் இத்தகைய ஆலோசனையெல்லாம் எங்களுக்கு தேவையில்லை என்
றும், காவிரி நீரை பெற்றுத் தரப்போகிற ஒருவர் உண்டென்றால், அது எங்கள் புரட்சி
தலைவிதான் என்றும் மூத்த அமைச்சர் பன்னீர் செல்வம் வீராப்புடன் கூறியிருந்தார். அந்த
வீராப்பு இப்போது என்ன ஆயிற்று? </p><p>
</p><p>
அ.தி.மு.க. அரசு தவறியது </p><p>
</p><p>
கடந்த ஆண்டு பற்றாக் குறை என்று நாடகமாடி நம்ப வைத்த கர்நாடக அரசு, அங்கே 2 போகம் நெல்
சாகுபடியும், மூன்றாவதாக கரும்பு சாகுபடியும் செய்து விவசாயிகளை பயன்பெற வைத்திருக்கிறது.
தமிழகத்திலோ காவிரி டெல்டா வறண்டு கிடந்தது. கர்நாடகத்தின் இந்த துரோகச் செயலை
அம்பலப்படுத்திட அ.தி.மு.க. அரசு தவறிவிட்டது. இப்போது ராணி மேரி கல்லூரியை
இடித்துவிட்டு, அங்கே தலைமை செயலகம் கட்டும் தங்களது விருப்பத்திற்கு மத்திய அரசின் ஒரு
ஆணை தடையாக இருக்கிறது என்றதும், மாநில உரிமை பறிபோய்விட்டது என்று பொங்கி எழுந்து,
எல்லா மாநில முதலமைச்சருக்கும் கடிதம் எழுதி ஆதரவு கேட்டதைப்போல, காவிரி பிரச்சினையில்
கர்நாடக அரசின் சட்டமீறலை எடுத்துக் காட்டி, அதனை முறியடிக்கும் வகையில் தேசிய அளவில்
பொதுக் கருத்தை உருவாக்க முயற்சி மேற்கொள்ளத் தவறியது ஏன்? அதில் ஆர்வம் காட்டாதது
ஏன்? </p><p>
</p><p>
விவசாயிகள் அதிர்ச்சி </p><p>
</p><p>
இத்தகைய ஆக்கப்பூர்வ மான யோசனைகளை கூறும்போது, ஆத்திரப்படுவதை விட்டுவிட்டு, மாநில
நலனை முன்னிறுத்தி நடவடிக்கைகளை மேற்கொண்டால் நமது உரிமைகளை நிலைநாட்ட முடியும். அதுதான்
ராஜதந்திரமும் ஆகும். அதை விட்டுவிட்டு, மாற்று பயிர்களை பயிரிடுங்கள் என்று அரசே
சொல்வதையும், நெல் பயிரிட்ட விவசாயிகளை எள் பயிரிடும்படி வற்புறுத்துவதையும்
பார்க்கும்போது, காவிரி நீரையும் அதில் நமக்குள்ள உரிமையையும் கை கழுவி விட்டார்களோ
என்று டெல்டா விவசாயிகள் அதிர்ச்சியடைவதும், அச்சம் கொள்வதும் நியாயமானதே ஆகும். </p><p>
</p><p>
புதிய வீராணம் விவேகமாகாது </p><p>
</p><p>
காவிரி தண்ணீர் வராது என்று சொல்லி மாற்று பயிர் செய்யுங்கள் என்று ஆலோ சனை கூறும்
அரசும், முதல மைச்சரும், காவிரியின் கடை மடை பகுதியில் உள்ள வீராணம் ஏரி மட்டும்
நிரம்பி வழியும் என்று கருதி புதிய வீராணம் திட்டத்தை நிறை வேற்ற துடிப்பது ஏன்?
டெல்டா விவசாயிகள் வேண்டுமென்றால் மாற்று பயிர் செய்யலாம்; ஆனால் வீராணம் ஏரி நிரம்பு
வதற்கு மாற்று வழியிருக்கிறதா? செயற்கை மழையினால் வீரா ணத்தை நிரப்பிவிட
முடியுமா? பிறகு ஏன் 720 கோடி ரூபாயை புதிய வீராணம் திட்டத்தில் வீணடிக்க
வேண்டும்? டெல்டா விவசாயிகளே மாற்று பயிர் செய்ய வேண்டும் என்று சொல்லுகிற இந்த
இக்கட்டான நேரத்தில், புதிய வீராணம் திட்டத்தை நிறைவேற்றுவது விவேகமாகாது. எனவே அரசு
அதனை கைவிட வேண்டும். அதற்கு பதிலாக கிருஷ்ணா நதி நீர் உடன்பாட்டின்படி முழு அளவு தண்ணீரை
பெற முயற்சிப்பது தான் தமிழகத்தின் நலனுக்கு சிறந்தது ஆகும். </p><p>
</p><p>
இவ்வாறு கூறி உள்ளார். 

</p>

<p> </p>

<p> </p>

<p>அ.தி.மு.க. தூண்டுதலால்
காங்கிரசை உடைக்க மகேஸ்வரி எம்.எல்.ஏ. சதி: மாநில சிறப்பு அழைப்பாளர்
கே.பி.எஸ்.மணி் குற்றச்சாட்டு </p>

<p> </p>

<p>கோவை, ஜுன் 8- அ.தி.மு.க. தூண்டுதலால் காங்கிரஸ் கட்சியை
உடைக்க மகேஸ்வரி எம்.எல்.ஏ. சதி செய்துள்ளார் என்று மாநில சிறப்பு அழைப்பாளரும்,
கோவை மாநகர துணை தலைவருமான கே.பி.எஸ். மணி குற்றம் சாட்டினார். </p><p>
</p><p>
மகேஸ்வரிக்கு பதில் </p><p>
</p><p>
கோவை மாநகர காங்கிரஸ் நிர்வாகிகள் பட்டியல் தொடர்பாக மாநகர் மாவட்ட தலைவர்
கோவைதங்கம் எம்.எம்.ஏ., மீது மகேஸ்வரி எம்.எல்.ஏ. சரமாரியாக குற்றம் சாட்டினார்
அல்லவா? </p><p>
</p><p>
இந்த குற்றசாட்டுக்கு தமிழ்நாடு காங்கிரஸ் கமிட்டி சிறப்பு அழைப்பாளரும், கோவை மாநகர
காங்கிரஸ் துணை தலைவருமான் கே. பி.எஸ்.மணி நேற்று பதில் அளித்தார். அவர் நிருபர்களிடம்
கூறியதாவது: </p><p>
</p><p>
கோவை மாநகர் மாவட்ட காங்கிரஸ் கமிட்டியில் காங்கிரஸ் கட்சிக்கு மிகுந்த முக்கி
யத்துவம் கொடுக்கப்பட்டு உள்ளது. 35 பேர் நிர்வாகிகளாக நியமிக்கப்பட்டு உள்ளனர். நிர்வாகிகள்
பட்டியலில் 120பேர் இடம் பெற்றுள்ளதாக மகேஸ்வரி எம்.எல்.ஏ., கூறியது தவறானது
நிர்வாகிகள் பட்டியலில் மொத்தம் 69 பேர் மட்டுமே இடம் பெற்று இருக்கின்றனர். இதில்
35பேரில் 30 பேர்் முன்னாள் அமைச்சர் பிரவ் ைநேசிப்பவர்கள். புதியதாக
நியமிக்கப்பட்ட நிர்வாகிகளில் ஒரு சிலர் தவிர பெரும் பாலானோர் முன்னாள் மத்திய
அமைச்சர் ஆர்.பிரபுவை நேரில் சந்தித்து ஆசி பெற்று தான் வந்துள்ளனர். </p><p>
</p><p>
மேலும் இந்த பட்டியல் சட்டமன்ற உறுப்பினர்கள், மாநில நிர்வாகிகள், காங்கிரஸ் மூத்த
தலைவர்கள் தொழிற்சங்க தலைவர்கள் அனைவரிடமும் கலந்து பேசி அவர்களின் ஆலோசனையின்படியும்
ஒப்புதலின்படியும் வௌியிடப் பட்டது.மாவட்டத்திற்கு வந்த மேலிட பார்வையாளர்கள்
பட்டியலில் கையெப்பமிட்ட பின் மாநில தலைவர் சோ.பாலகிருஷ்ணன், செயல் தலைவர்
ஈ.வி.கே.எஸ். இளங்கோவன் ஆகியோர் கையொப்பமிட்ட பிறகுதான் நிர்வாகிகள் பட்டியல்
வௌியிடப்பட்டது. </p><p>
</p><p>
கோவை மாநகரில் நடக்கும் அனைத்து காங்கிரஸ் நிகழ்ச்சிகளுக்கும் எஸ்.மகேஸ்வரி
எம்.எல்.ஏ. பெயரை முக்கியத்தும் அளித்து அவருக்கு அழைப்பிதழையும் முறையாகஅனுப்பி
வருகிறோம். ஆனால் அவர் எந்த கழ்ச்சியிலும் கலந்து கொள்ளவில்லை. தொடர்ந்து
கட்சியினரை புறக்கணித்தார். </p><p>
</p><p>
அ.தி.மு.க. தூண்டுதல் </p><p>
</p><p>
தனது அலுவலகத்திலும், வீட்டிலும் முதல்வர் ஜெயல லிதா படத்தை வைத்துக் கொண்டு, அ.தி.மு.க.
அமைச்சர்களின் தூண்டுதலின் பேரில் காங்கிரஸ் கட்சியில் குழப்பத்தை ஏற்படுத்தி கட்சியை
உடைக்கும்் முயற்சியில் ஈடுபடுகிறார் என்ற சந்தேகம் எழுந்து உள்ளது. </p><p>
</p><p>
சமீபத்தில் நடந்த சாத்தான் குளம் இடைத்தேர்தலில் தமிழகத்தில் உள்ள அனைத்து சட்டமன்ற
உறுப்பினர்கள், மாநில நிர்வாகிகள், மாவட்ட தலைவர்கள் அனைவரும் சென்று தேர்தல்
பணியாற்றிய போது மகேஸ்வரி எம்.எல்.ஏ. மட்டும் தேர்தல் பணிகளை புறக்கணித்ததார். இந்த
செயல் ஆளும் கட்சிக்கு(அ.தி.மு.க) பயந்துதானோ? என்ற சந்தேகம் வலுத்துள்ளது. </p><p>
</p><p>
தனக்கு வேண்டியர்களுக்கு கட்சியில் உறுப்பினர் இல்லாத வர்களுக்கு பதவி கிடைக்க வில்லை
என்ற விரக்தியில் கட்சியை பற்றி விமர்சித்து வருகிறார். அவருடன் இருக்கும் லீலாவதி,
உமாபதி ஆகியோருக்கு பதவி கொடுக்க வில் லையே என ஆதங்கத்தில் இது போன்ற செயலில்
ஈடுபட்டு வருகிறார்.இது கட்சி யின் கட்டுப்பாட்டை மீறிய செயலாகும். மாநகர் மாவட்ட
தலைவர் கோவை தங்கம் காங்்கிரஸ் தொண்டர்களை அரவணைத்து கட்சியை வளர்க்க முனைப்புடன்
ஈடுபட்டு வருவது மகேஸ்வரிக்கு பொறுக்க முடியவில்லை. </p><p>
</p><p>
த.மா.கா. செயல்பட்டு வந்த அலுவலகத்தில் கோவை தங்கம் செயல்படுவதில் எந்த தவறும் இல்லை.
கோபாலபுரம் காமராஜ் பவன் அலுவலக கட்டிடம் சம்பந்தமாக நீதிமன்றத்தில் வழக்கு
நிலுவையில் உள்ளது. எனவே அந்த கட்டிடத்தில் கே.என்.எல்.ஓ. தொழிற்சங்கம் தான்
வாடகைக்கு இயங்கி வருகிறது. </p><p>
</p><p>
எந்த பிரச்சினையாலும் கட்சி மேலிடத்தில் முறையிடலாம். அதனை விடுத்து வௌி யில; brhy;tJ
fl;rpf;F mtg;bgaiu Vw;gLj;Jk; fh';fpu!; fl;rpf;F vjpuhf bray;gl;L tUk; knf!;thp
vk;.vy;.V. kPJ eltof;if vLf;fnfhhp mfpy ,e;jpa fh';fpu!; fkpl;o/ jkpH;ehL
fh';fpu!; fkpl;o/ kw;Wk; xG';F eltof;if FG Mfpatw;Wf;F g[fhh; kD mDg;gp
cs;nshk;. 6 ngh; uh$pdkhf bra;Js;sjhf mwptpj;J cs;sdh;. ,jdhy; fh';fpuRf;F
ghjpg;g[ Vw;glhJ. </p><p>
</p><p>
,t;thW mth; Twpdhh;. </p>

<p> </p>

<p> </p>

<p>ராணிமேரி கல்லூரி
விவகாரம்: ஜெ. அரசு கொள்கை முடிவால் சட்ட சிக்கல் வரும்; வக்கீல் கருத்து </p>

<p> </p>

<p>சென்னை,ஜூன்,.8- ராணிமேரி கல்லூரி விவகாரத்தில் அரசு கொள்கை
முடிவு எடுத்து உள்ளதால் புதிய சட்ட சிக்கல் ஏற்பட்டு உள்ளது என்று வக்கீல் கருத்து
கூறினார். </p><p>
</p><p>
கொள்கை முடிவு </p><p>
</p><p>
ராணிமேரி கல்லூரியை இடித்து விட்டு அந்த இடத்தில் புதிய தலைமை செயலகம் கட்டப்படும்
என்று சட்டசபையில் ஜெயலலிதா அறிவித்தார் அல்லவா? இந்த அறிவிப்பை எதிர்த்து வழக்கு
தாக்கல் செய்யப்பட்டு அந்த வழக்கு நிலுவையில் உள்ளது. இதை முறியடிக்க தமிழக அரசு
சட்டநிபுணர்களுடன் கலந்து பேசி தற்போது கொள்கை முடிவை எடுத்து உள்ளது. இந்த கொள்கை முடிவு
கவர்னர் பரிந்துரைக்கு அனுப்பப்பட்டு உள்ளது. இதற்கான கோப்புகளை கவர்னர் பரிசீலனை
செய்து அரசு கொள்கை முடிவுக்கு சட்டப்படி அனுமதி தர வேண்டும். எனவே கவர்னர் சட்டப்படி
அனுமதி தருவாரா? அல்லது ஐகோர்ட்டில் வழக்கு நிலுவையில் உள்ளது என்பதை காரணம் கூறி
நிராகரிப்பாரா என்பது போக போக தான் தெரியும். </p><p>
</p><p>
இதுபற்றி மூத்த வக்கீல் ஒருவரிடம் கருத்து கேட்டபோது அவர் கூறியதாவது:- </p><p>
</p><p>
.ராணிமேரி விவகாரத்தில் அரசு கொள்கை முடிவு எடுத்து உள்ளதாக கூறப்படுகிறது. . தமிழக
அமைச்சரவை எடுக்கும் முடிவை அப்படியே கவர்னர் ஏற்க வேண்டும் என்பது விதி. எனவே இந்த
கொள்கை முடிவை கவர்னர் நிச்சியம் ஏற்பார் இதை நிராகரிக்கமாட்டார் . </p><p>
</p><p>
சட்ட சிக்கல் </p><p>
</p><p>
ராணிமேரி கல்லூரியை இடித்து விட்டு அந்த இடத்தில் புதிய தலைமை செயலகம் கட்டப்படும்
என்று தமிழக அரசு தற்போது கொள்கை முடிவு எடுத்து உள்ளதால் இதில் பல்வேறு சட்ட சிக்கல்
ஏற்பட்டு உள்ளது. வழக்கமாக சுப்ரீம் கோர்ட்டும் ஐகோர்ட்டும் அரசு கொள்கை முடிவில்
கோர்ட்டு தலையிட முடியாது. இதற்கு கோர்ட்டுக்கு அதிகாரம் இல்லை என்று பல்வேறு வழக்குகளில்
தீர்ப்பு கூறி உள்ளது. இதை அரசு வக்கீல் முதல்வரிடம் கூறி தற்போது அரசு இதில் கொள்கை
முடிவு எடுத்தால் நல்லது என்று கூறி உள்ளார். அதன்படி கொள்கை முடிவை எடுத்து உள்ளனர் . </p><p>
</p><p>
ஐகோர்ட்டில் வழக்கு நிலுவையில் இருக்கும் இந்த நிலையில் அரசு கொள்கை முடிவை அறிவித்து
இருப்பது சட்ட நிபுணர்கள் மத்தியில் பெரும் பரபரப்பை ஏற்படுத்தி உள்ளது. அரசு கொள்கை
முடிவு எடுத்து உள்ளதால் ஐகோர்ட்டில் மாணவிகளும் பொதுநலன் அமைப்புகளும் தாக்கல் செய்த
வழக்குகள் விசாரணைக்கு உகந்தது இல்லாமல் போய்விடும் இதற்கு அதிகமாக வாய்ப்பு உள்ளது
எனவே உடனடியாக மாணவிகள் அரசு கொள்கை முடிவை எதிர்த்து வழக்கு தாக்கல் செய்ய வேண்டும்.
இந்த வழக்கில் உரிய உத்தரவு கிடைப்பது கடினம் தான். </p><p>
</p><p>
இந்த சட்ட சிக்கலை எப்படி எதிர் கொள்வது என்பது பற்றி மாணவிகளும் பேராசிரியர்களும்
அரசியல் தலைவர்களும் வக்கீல்களுடன் ஆலோசனை நடத்தி வருகிறார்கள். இதுதவிர ராணிமேரி
கல்லூரி பேராசிரியர்கள் 8 பேரை இடமாற்றம் செய்து உள்ளனர். எனவே ராணிமேரி கல்லூரியை
இடிப்பதில் ஜெயலலிதா அரசு உறுதியான முடிவு எடுத்து வருகிறது என்பது தௌிவாக தெரிகிறது. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>






</body></text></cesDoc>