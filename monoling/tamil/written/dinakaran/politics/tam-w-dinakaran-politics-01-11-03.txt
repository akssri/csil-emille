<cesDoc id="tam-w-dinakaran-politics-01-11-03" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-politics-01-11-03.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-11-03</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-11-03</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>நாட்டின் பாதுகாப்பு தான் முக்கியம்:
'பொடா' சட்டத்தை அனைத்து கட்சிகளும் ஆதரிக்க வேண்டும், பா.ஜ. செயற்குழு வேண்டுகோள்</p>

<p> </p>

<p>அமிர்தசரஸ், நவ. 3- நாட்டின் பாதுகாப்புதான் முக்கியம். அதற்கு
ஆபத்து வரும் போது அனைவரும் ஒன்று திரண்டு நிற்க வேண்டும். எனவே பொடா சட்டத்தை அனைத்து
கட்சிகளும் ஆதரிக்க வேண்டும் என்று பா.ஜ. தேசிய செயற்குழுவில் பேசிய ஜனா. கிருஷ்ணமூர்த்தி
வேண்டு கோள் விடுத்தார். </p><p>
</p><p>
பாரதிய ஜனதா தேசிய செயற்குழு அமிர்தசரஸ் நகரில் 2 நாள் நடக்கிறது. கட்சியின் தலைவர்
ஜனா.கிருஷ்ணமூர்த்தி முதல்நாளான நேற்று கூட்டத்தை தொடங்கி வைத்து பேசினார். அப்போது
அவர் கூறியதாவது:- </p><p>
</p><p>
பயங்கரவாத தடுப்புச் சட்டத்துக்கு அரசியல் கட்சிகள் பல எதிர்ப்பு தெரிவித்து வரு
கின்றன. ஜனநாயக நாட்டில் மாற்று கருத்துக்கள் இருப்பது சகஜம் தான். ஆனால் நாட்டின்
பாதுகாப்புக்கு ஆபத்து ஏற்படும் போது ஒவ்வொருவரும் அர சுக்கு தோள் கொடுத்து நிற்க
வேண்டும். பயங்கரவாதத்தை எதிர்த்து அரசு எடுக்கும் முயற்சி களுக்கு ஆதரவு அளிக்க வேண்டும். </p><p>
</p><p>
பயங்கரவாதத்தை ஒழிப்பது அரசின் வேலை மட்டும் அல்ல. பயங்கரவாதத்தை வேரறுக்க தேசிய
ஜனநாயக கூட்டணி உறுதி பூண்டுள்ளது. அதே சமயத்தில் அனைவரும் ஒன்று பட்டு
ஒத்துழைக்காவிட்டால் அரசும் பாதுகாப்பு படையும் சேர்ந்து தங்கள் முயற்சிகளில் வெற்றி
அடைய முடியாது. எனவே பயங்கரவாதத்தை எதிர்்த்து நடக்கும் போரில் எதிர்க்கட்சிகள்
பாரபட்சம் காட்ட வேண்டாம் என்று கேட்டுக்கொள்கிறேன். பயங்கர வாதம் தடுக்க அரசு கொண்டு
வந்துள்ள பொடா சட்டத்துக்கு அனைத்து கட்சிகளும் ஒத்து ழைப்பு தர வேண்டும். </p><p>
</p><p>
அண்டை நாட்டு உறவு </p><p>
</p><p>
நமது அண்டை நாடு களுடன் நட்புறவு உருவாக்கவே இந்தியா விரும்புகிறது. அதே போல அண்டை
நாடுகளும் இதே அணுகுமுறையை பின்பற்ற வேண்டும். வங்கதேசத்துடன் தேசிய ஜனநாயக கூட்டணி அரசு
மிக நல்லுறவை வளர்த்துவருகிறது. ஆனால் சமீபத்தில் அங்கு புதிய அரசு பதவிஏற்றதும் நடந்த
சம்பவங்கள் மிகுந்த கவலை அளிக்கிறது. </p><p>
</p><p>
இந்தியாவுடன் மீண்டும் பேச்சுவார்த்தை தொடங்கு வதில் பாகிஸ்தானுக்கு விருப்பம் இருந்தால்
எல்லை தாண்டிய பயங்கரவாதத்தை நிறுத்த வேண்டும். அதுவரை பேச்சு வார்த்தை நடத்தக்கூடாது.
பயங்கரவாதிகளுக்கும் சுதந்திர போராட்ட வீரர்களுக்கும் உள்ள வேறுபாட்டை பாக். உணர
வேண்டும். இந்த விஷயத்தில் இரட்டை நிலை எடுப்பதை பாக். நிறுத்த வேண்டும். </p><p>
</p><p>
தனியார் துறை </p><p>
</p><p>
சமூக பாதுகாப்பு நடவடிக்கைகளை அரசு எடுக்க வேண்டும். பொதுத்துறை ஊழியர்களுக்கு இருப்பது போல
தனியார் துறை ஊழியர்களுக்கும் இன்சூரன்சு பாது காப்பு பயன்கள் கிடைக்க செய்ய வேண்டும். </p><p>
</p><p>
ஆள்குறைப்பில் பதவி இழக்கும் ஊழியர்களுக்கு ஆண்டுக்கு 15 நாள் சம்பளம் என நஷ்டஈடு
வழங்குவதை 45 நாளாக உயர்த்தித்தர வேண்டும். உலக வர்த்தக அமைப்பு ஒப்பந்தத்தில் உள்ள
குறைபாடுகளை நீக்கவும் அரசு உறுதி அளிக்க வேண்டும். </p><p>
</p><p>
இவ்வாறு அவர் பேசினார். </p><p>
</p><p>
வாஜ்பாய்-அத்வானி </p><p>
</p><p>
இந்தகூட்டத்தில் பிரதமர் வாஜ்பாய், அத்வானி உள்பட மூத்த தலைவர்கள், முன்னணி பிரமுகர்கள்
கலந்து கொண்டனர். </p>

<p> </p>

<p>நீதிபதி அசோக்குமார்
சம்மன் அனுப்பிய விவகாரம்: ஜெ. மீதான லண்டன் சொத்து வழக்கு விசாரணைக்கு தடை,
ஐகோர்ட்டு விதித்தது</p>

<p> </p>

<p>சென்னை, நவ.3- லண்டன் சொத்து குவிப்பு வழக்கு விசாரணைக்கு
சென்னை ஐகோர்ட்டு வருகிற 8-ந் தேதி வரை தடை விதித்து உள்ளது. இந்த வழக்கில் ஜெயலலிதா
ஆஜராக நீதிபதி அசோக்குமார் சம்மன் அனுப்பினார். </p><p>
</p><p>
லண்டனில் ஜெயலலிதா, டி.டி.வி.தினகரன் ஆகியோர் ரூ.43 கோடி மதிப்புள்ள சொத்துக்களை
வாங்கி குவித்த வழக்கில் சென்னை முதன்மை செசன்சு நீதிமன்றத்தில் போலீசார்
குற்றப்பத்திரிகை தாக்கல் செய்தனர். இதன் நகலை பெற ஜெயலலிதாவுக்கு 3 முறை கோர்ட்டு
சம்மன் அனுப்பியும் போலீசார் அவ ருக்கு வழங்கவில்லை. இதனால் கடந்த 4-ந்தேதி இந்த
வழக்கு விசாரணைக்கு வந்தபோது, போலீசை நீதிபதி அசோக்குமார் கண்டித்தார். இந்த
வழக்கில் புலன் விசாரணையை செய்ய மேலும் 3 மாதம் அவகாசம் தேவை என்று அரசு வக்கீல்
வெங்கட்ராமன் கேட்டபோது நீதிபதி மறுத்தார். </p><p>
</p><p>
சம்மன் </p><p>
</p><p>
அதோடு ஜெயலலிதா கண்டிப்பாக கோர்ட்டில் நவம்பர் 5-ந் தேதி ஆஜராக வேண்டும் என்றும்,
சம்மனை கோர்ட்டு அதிகாரியே நேரில் வழங்க வேண்டும் என்றும் நீதிபதி அசோக்குமார்
உத்தரவு பிறப்பித்தார். இதன்படி கோர்ட்டு அதிகாரி, சம்மனை ஜெயலலிதாவிடம் நேரில்
அளித்தார். </p><p>
</p><p>
வழக்கு </p><p>
</p><p>
இந்த சம்மனை ரத்து செய்யக்கோரி ஜெயலலிதா சென்னை ஐகோர்ட்டில் வழக்கு தாக்கல்
செய்தார். இந்த வழக்கில் அரசு வக்கீல் ஐ.சுப்பிரமணியம் ஆஜராகி கோர்ட்டு இதில்
சட்டப்படி செய்ய வேண்டியதை செய்யலாம் என்று கூறினார். ஜெயலலிதா தரப்பு வக்கீல் அசோகன்
ஆஜராகி ஜெயலலிதாவுக்கு அனுப்பிய சம்மனை ரத்து செய்ய வேண்டும் இதற்கு தடை விதிக்க
வேண்டும் லண்டன் வழக்கில் விசாரணை நடத்தி முடித்த பிறகு இதன் அறிக்கையை
தனிக்கோர்ட்டில் தான் தாக்கல் செய்து இருக்க வேண்டும். எனவே சென்னை செசன்சு கோர்ட்டில்
தாக்கல் செய்தது சட்டவிரோதமானது. இதுதவிர இந்த வழக்கில் ஆஜராக கோர்ட்டு
ஜெயலலிதாவுக்கு சம்மன் அனுப்பியது சட்டவிரோதமானது என்று கூறினார். </p><p>
</p><p>
8-ந் தேதி வரை தடை </p><p>
</p><p>
இதை கேட்ட நீதிபதி பி.டி.தினகரன் இந்த வழக்கு முக்கிய வழக்கு. இதில் அரசு தரப்பில்
மனுதாரருக்கு ஆதரவாக வாதாட வாய்ப்பு உள்ளது என்று கருதுகிறேன், எனவே இதில் அட்வகேட்
ஜெனரல் கோர்ட்டில் ஆஜராகி பதில் கூற வேண்டும் என்று கூறி வழக்கு விசாரணையை
தள்ளிவைத்தார். அதன்படி இந்த வழக்கு நேற்று நீதிபதி பி.டி.தினகரன் முன்பு விசாரணைக்கு
வந்தது. அப்போது போலீஸ் சார்பாக அரசு வக்கீல் ஐ.சுப்பிர மணியம் ஆஜரானார். நீதிபதி
உத்தரவிட்டதால் அட்வகேட் ஜெனரல் என்.ஆர்.சந்திரன் ஆஜரானார். அவரிடம் நீதிபதி இந்த
வழக்கில் அரசு தரப்பு கருத்தை கூறுங்்கள், இதில் மனுதாரருக்கு ஆதரவாக அரசு செயல்படுகிறதா
இல்லையா என்று தெரியப்படுத்துங்கள் என்று கூறினார். </p><p>
</p><p>
இதற்கு அட்வகேட் ஜெனரல் என்.ஆர். சந்திரன் பதில் கூறும் போது நான் கோர்ட்டுக்கு இந்த
வழக்கில் உதவி செய்ய தயாராக உள்ளேன், அட்வகேட் ஜெனரல் இந்த கோர்ட்டுக்கு உதவி தான்
செய்ய முடியும், மனுதாரருக்கு ஆதரவாக நான் கருத்து கூறமுடியாது, வழக்கு தொடர்பான கோப்புகளை
கோர்ட்டு பெற்று அதன் மீது கோர்ட்டே உரிய உத்தரவு பிறப்பிக்க வேண்டும் என்று கூறினார்.
</p><p>
</p><p>
இதை கேட்ட நீதிபதி பி.டி. தினகரன் சென்னை செசன்சு கோர்ட்டில் உள்ள லண்டன் சொத்து
குவிப்பு வழக்கு விசாரணைக்கு வருகிற 8-ந்தேதி வரை இடைக்கால தடை விதித்து உத்தரவிட்டார்.
</p><p>
</p><p>
உத்தரவு </p><p>
</p><p>
நீதிபதி பிறப்பித்த உத்தரவில் கூறி இருப்பதாவது:- </p><p>
</p><p>
லண்டன் சொத்து குவிப்பு வழக்கில் முக்கிய பிரச்சினைகள் குறித்து கேள்வி எழுப்பப்பட்டு
உள்ளதால் இந்த வழக்கை விசாரணைக்கு ஏற்றுக்கொள்கிறேன். இந்த வழக்கு தொடர்பான
கோப்புகளை அட்வகேட் ஜெனரல் மற்றும் அரசு வக்கீல் கோர்ட்டில் தாக்கல் செய்ய
இருப்பதாகவும் இந்த வழக்கில் நோட்டீசு எடுத்து வழக்கில் பதில் மனு தாக்கல் செய்ய
இருப்பதாகவும் அரசு வக்கீல் கூறி உள்ளார். இதனால் வருகிற 8-ந்தேதி வரை அவகாசம்
கேட்டனர். அதுவரை மனுதாரர் சென்னை செசன்சு கோர்ட்டில் வருகிற 5-ந்தேதி ஆஜராக தடை
விதிக்க வேண்டும், அதில் இருந்து ஆஜராக விலக்கு அளிக்க வேண்டும் என்று மனுதாரர் வக்கீல்
கூறினார். ஒரே குற்றத்திற்காக இரண்டு வழக்கு விசாரணை நடத்தக்கூடாது என்றும் மனுதாரர் வக்கீல்
கூறினார். எனவே இந்த வழக்கில் மறு உத்தரவு பிறப்பிக்கப்படும் வரை சென்னை செசன்சு
கோர்ட்டில் உள்ள மனுதாரர் மீதான லண்டன் சொத்து குவிப்பு வழக்கு விசாரணைக்கு தடை
விதிக்கிறேன். வழக்கு விசாரணையை வருகிற 8-ந்தேதி தள்ளிவைக்கிறேன். </p><p>
</p><p>
இவ்வாறு அவர் உத்தரவில் கூறி உள்ளார். </p>

<p> </p>

<p>மோதல் அரசியல் எனக்கு
வேண்டாம்: பிரபாகரன் அரசியலுக்கு வந்தால் நான் வௌியேறத் தயார், இலங்கை தமிழ்
மந்திரி டக்ளஸ் தேவானந்தா அறிவிப்பு</p>

<p> </p>

<p>கொழும்பு, நவ.3- பிரபாகரன் அரசியலுக்கு வந்தால் நான் வௌியேறத்
தயார். எனக்கு மோதல் அரசியல் வேண்டாம் என்று இலங்கை மந்திரி டக்ளஸ் தேவானந்தா
கூறினார். </p><p>
</p><p>
ஈழ மக்கள் ஜனநாயக கட்சி தலைவரும் இலங்கை மந்திரியுமான டக்ளஸ் தேவானந்தா நேற்று
பேட்டி அளித்தார். அவர் கூறியதாவது:- </p><p>
</p><p>
இலங்கை தமிழர் பிரச்சினையை நாங்கள் 2 வழிகளில் அணுகுகிறோம். தமிழ் மக்களின் அன்றாட
பிரச்சினை, தமிழர்களின் அரசியல் பிரச்சினை. ஒன்றை சமாளிக்க ஒன்றை விட்டுக் கொடுக்க
முடியாது. </p><p>
</p><p>
தேர்தல் அணி </p><p>
</p><p>
தமிழர்கள் கட்சிகள் அமைத்துள்ள தமிழ் ஐக்கிய விடுதலை முன்னணியால் எனக்கு எந்த
பாதிப்பும் இல்லை. அது தேர்தலுக்காக அமைந்த அணி. பாராளுமன்றத்தில் அதிக இடம் பெற
வேண்டும் என்பதே அதன் நோக்கம். மக்கள் கூட்டணி அர சுக்கு நெருக்கடி வந்தபோது அந்த
சந்தர்ப்பத்தை தமிழர் கூட்டணி பயன்படுத்தாதது ஏன்? 9 தமிழ் எம்.பி.க்கள் ஆதரவு
அளித்து இருந்தால் பதிலுக்கு அரசின் சார்பில் போர் நிறுத்தம் செய்ய வேண்டும் என்று
நிபந்தனை விதித்து இருக்கலாம். அதன் மூலம் புலிகளுடன் பேச்சு தொடங்கி இருக்கலாம். 10
எம்.பி.க்களை கொண்ட ஜனதா விமுக்தி பெரமுனா அதை செய்த போது தமிழர் கூட்டணி செய்யாதது
ஏன்? </p><p>
</p><p>
பிரபாகரன் வந்தால் </p><p>
</p><p>
ஈழ மக்கள் ஜனநாயக கட்சி அரசியலில் இருப்பதற்கு காரணமே அதற்கு தேவை இருப்ப தால்தான்.
</p><p>
</p><p>
மக்களுக்காக சிறந்த முறையில் பணியாற்ற ஜனநாயக நீரோட்டத்தில் சேர பிரபாகரன் முன்
வந்தால் எங்கள் கட்சி அரசியலில் இருக்க வேண்டிய அவசியமே இல்லை. அரசியலைவிட்டு நான்
விலகத் தயார். மோதல் அரசியலை நான் விரும்பவில்லை. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>6 மாதத்தில் அறிவிப்புகள்
ஏராளம்- ஆனால் ஒன்றும் நிறைவேறவில்லை: அ.தி.மு.க. அரசின் திட்டங்கள் அனைத்தும்
வெத்துவேட்டுதான், கருணாநிதி கண்டனம்</p>

<p> </p>

<p>சென்னை, நவ. 3- அ.தி.மு.க.வின் 6 மாத ஆட்சியில்
அறிவிக்கப்பட்ட திட்டங்கள் ஏராளம். ஆனால் அந்த திட்டங்கள் அனைத்தும் வெத்து வேட்டுகளாக
மாறி புகையைத் தான் பரப்புகின்றன என்று கருணாநிதி கூறியுள்ளார். </p><p>
</p><p>
தி.மு.கழக தலைவர் கருணாநிதி நேற்று வௌியிட்ட அறிக்கையில் கூறி இருப்பதாவது:- </p><p>
</p><p>
இந்த ஆறு மாத காலத்தில் அ.தி.மு.க. அரசு பதவியேற்றதிலிருந்து அறிவிக்கப்பட்ட திட்
டங்கள் ஏராளம், ஏராளம். இந்த அரசின் சார்பில் தயாரிக்கப் பட்ட ஆளுநர் உரையிலும்,
நிதி நிலை அறிக்கையிலும், மானிய விவாதங்களிலும் பல திட்டங் களை அறிவித்திருந்தார்கள்.
அவற்றில் சிலவற்றை எடுத்துக் கொண்டால்கூட; </p><p>
</p><p>
குடும்பத்துக்கு ஒரு வீடு என்ற வகையில் வீட்டுமனை விரைவு ஒதுக்கீட்டுத் திட்டம் ஒன்று
வகுக்கப்படும் என்று குறிப்பிட்டிருந்தார்கள். ஆனால் இதற்கான பணி எந்த நிலை யில் உள்ளது
என்று தெரியவில்லை. </p><p>
</p><p>
கோப்புகள் வாரம் </p><p>
</p><p>
அடுத்து கோப்புகளையெல்லாம் 90 நாட்களுக்குள் முடிக்க கோப்புகளை முடிக்கும் வாரம் என்ற
திட்டம் செயலாக்கப்படும் என்று அறிவித்திருந்தார்கள். ஆனால் தற் போது முதலமைச்சர்
அலுவலகத்திலேயே ஏராளமான கோப்புகள் குவிந்திருப்பதாக ஒருசில வார ஏடுகள் செய்தி
வௌியிட்டுள்ளன. </p><p>
</p><p>
புதிதாக தொழில்நுட்பப் பல்கலைக்கழகம் ஒன்று கொண்டு வரப்படும் என்று
அறிவித்திருந்தார்கள். 5 லட்சம் பெண்களுக்கு தொழில் முனை வோர் பயிற்சி அளிக்கும்
பெருந் திட்டம் ஒன்றை நிறை வேற்றப் போவதாக சொல்லியிருந்தார்கள். இதுவரை லட்சம்
அல்ல, எத்தனை ஆயிரம் மகளிருக்கு அத்திட்டம் செயல் பட்டுள்ளது என்ற விவரம் தெரியவில்லை.
</p><p>
</p><p>
பட்டியல் நீளும் </p><p>
</p><p>
ஆளுநர் உரையிலே தொடங்கப்பட்ட அறிவிப்புகள் அமைச்சர்கள் தங்கள் மானி யத்தின் மீதான
பதிலுரையிலே அறிவித்த நூற்றுக்கணக்கான திட்டங்கள் என்று தொடர் பட்டியல் நீண்டு கொண்டே
போகின்றது. </p><p>
</p><p>
உழவர் சந்தைகள் மூடப்படும் என்று ஆளுநர் உரையிலே அறிவிக்கப்பட்டது. விவசாயிகள்
எதிர்ப்புக்குரல் கொடுத்த காரணத்தால் செயல்படாத உழவர் சந்தைகள் மூடப்படும் என்று பிறகு
திருத்திக் கொண் டார்கள். தொடர்ந்து உழவர் சந்தைகளை செயல்படாத சந்தைகளாக ஆக்குவதற்கு
அரசே பின்னால் நின்று செயல்பட்டு, அரசின் ஒத்துழைப்பு அந்த சந்தைகளுக்கு இல்லாத
காரணத்தினால் அந்த திட்டம் பராமரிப்பற்ற நிலையிலே உள்ளது. </p><p>
</p><p>
வீரப்பன் எங்கே? </p><p>
</p><p>
பேரவையில் ஜெயலலிதா தாழ்த்தப்பட்டவர்களுக்கு பதவி உயர்வில் இடஒதுக்கீட்டை நிச்ச யம்
செயல்படுத்துவோம் என்றும், அரசாணை எண் 44-ஐ அமுல்படுத்தப்போவ தாகவும் அறிவித்தார்.
அதற்கான அறிவிப்பு எதுவும் இது வரை வரவில்லை. </p><p>
</p><p>
கழக ஆட்சி காலத்தில் வீரப்பனை பிடிக்கவில்லை என்பதற்காகவே என்னையும், கர்நாடக
முதல்வரையும் ராஜினாமா செய்யவேண்டுமென்று கூறிய ஜெயலலிதா ஆட்சிக்கு வந்தவுடன் ஓய்வுபெற்று
உறங்கி கொண்டிருந்தவரை தட்டியெழுப்பி அழைத்துவந்து பதவியும் கொடுத்து இதோ வீரப்பன்
பிடிபட்டுவிட்டான் என்றெல்லாம் பிரதாபம் பேசினார். பிறகு வீரப்பன் பற்றி தகவல் கொடுப்
பவர்களுக்கே 25 லட்சம் பரிசு என்று அறிவித்தார்கள். என்னவாயிற்று? எங்கே
வீரப்பன்? ஏன் பிடிக்கவில்லை? ஆனால் அவரைப்போல நாம் அவர்களை ராஜினாமா செய்ய
வேண்டுமென்று கூறமாட்டோம். ஏனென்றால் நமக்கு கண்ணியம் என்றால் என்ன வென்று தெரியும். </p><p>
</p><p>
செவிலியர் கல்லூரி </p><p>
</p><p>
நிதிநிலை அறிக்கையின் போது ஏட்டளவிலே பல திட்டங்கள் அறிவிக்கப்பட்டன. ஒவ் வொரு
மருத்துவ கல்லூரியிலும் செவிலியர் கல்லூரி அமைக்கப்போவதாகத் தெரி வித்தார்கள்.
கிராமப்புற மக்களுக்கு குடிநீர் வழங்குவதை அதிகரிக்க 690 கோடி ரூபாய் நிதி
ஒதுக்கப்போவதாக அறிவித்தார்கள். தர்மபுரி மாவட்டத்தை இரண்டாகப் பிரிக்கப் போவதாக
அறிவித்தார்கள். அதற்கான அரசாணை பிறப்பிக்கப்பட்டு விட்டதாகத் தெரியவில்லை. தனி அதிகாரியே
இன்னும் நியமிக்கப்படவில்லை. 700 கோடி ரூபாயில் புதிய வீராணம் திட்டம் அறிவிப்புடன்
நின்றுவிட்டதா? தெரிய வில்லை. </p><p>
</p><p>
போலீசுக்கு பணிநேரம </p><p>
</p><p>
காவலர்களின் பணி நேரத்தை 8 மணிநேரம் ஆக்குவது பற்றியும் அவர்களின் ஊதிய உயர்வு
குறித்தும் பரிசீலிக்கப்படும் என்று ஜெயலலிதா சட்டமன்றத்திலே சொல்லியிருந்தார். அது
எப்போது முதல் அமுலுக்கு வரும் என்று இதுவரையில் அறி விக்கப்படவில்லை. </p><p>
</p><p>
மின்னலை விழுங்கியவர் </p><p>
</p><p>
சென்னை-மாமல்லபுரம் இடையில் 1000 கோடி ரூபாய் செலவில் பிரமாண்டமான மாநாட்டுக்கூடம்
கட்டப்படும் என்று சட்டப்பேரவையிலே சுற்றுலாத்துறையின் சார்பில் அறிவித்து, ஏடுகளில் அது
கொட்டை எழுத்துக்களில் வௌிவந்தது. ஆனால் சுற்றுலா துறைக்காக இந்த ஆண்டு ஒதுக்கிய நிதியே
17 கோடி ரூபாய்தான் என்று நிதிநிலை அறிக்கையிலே தெரிவிக்கப்பட்டுள்ளது. 17 கோடி ரூபாய்
நிதி ஒதுக்கிய நிலையில் 1000 கோடி ரூபாய்க்கு எப்படி மாநாட்டுக்கூடம்
கட்டப்போகிறார்களோ? மின்னலையே விழுங்கி மின்சாரத்தை உற்பத்தி செய்யப்
போகின்றவர்களுக்கு இதெல்லாம் சகஜம் தான் </p><p>
</p><p>
தடையில்லா மின்சாரம் </p><p>
</p><p>
மின்துறையில் 4,500 கோடி ரூபாய் இழப்பு என்று முதல் நாள் அந்தத்துறையின் அமைச்சர்
அறிவித்துவிட்டு, மறுநாளே சென்னையில் மட்டுமின்றி தமிழகம் முழுவதும் தடையில்லா மின்சாரம்
வழங்க 4000 கோடி ரூபாயில் மிகப்பிர மாண்டமான திட்டம் தீட்டப்பட் டுள்ளதாக
தெரிவித்தார். சென்னை எண்ணூரில் 6000 கோடி ரூபாயில் பெட்ரோலிய ரசாயன வளாகம் ஒன்று
அமைக்கப்போவதாக தொழில் அமைச்சர் தன் பங்குக்கு அவையிலே தெரிவித் திருக்கிறார். </p><p>
</p><p>
கரும்புக்கு 1000 விலை </p><p>
</p><p>
கரும்பு டன் ஒன்றுக்கு ஆயிரம் ரூபாய் வீதம் பிழிதிறனைப் பற்றி கவலைப்படாமல் பிழி திறன்
8.33 சதவீதமாக இருந்தா லும் வழங்குவது குறித்து பரி சீலித்து நல்ல பதில் அளிக்கப்படும்
என்று அந்த துறையின் அமைச்சர் கூறியிருந்தார். பரி சீலனை எப்போது முடியு மென்று தெரிய
வில்லை. ஆரம்பமானால்தானே; அது முடிவதற்கு. </p><p>
</p><p>
டன்லப் தொழிற் சாலையைத் திறக்க நடவடிக்கை எடுக்கப்படும் என்றும் அமைச்சர்
தெரிவித்திருக்கிறார். அதைப் போலவே காவிரி வடிநில மாவட்டங்களில் 200 கோடி ரூபாய்
செலவில் பாலங்கள் கட்டப்போவதாக அறிவித்திருக்கிறார்கள். நிதி நிலை அறிக்கையிலே
இதற் கான நிதி ஒதுக்கீடு செய்யப் பட்டிருப்பதாகத் தெரிய வில்லை. 150 கோடி ரூபாயில் 1550
புதிய பேருந்துகள் வாங்கப் போவதாகத் தெரிவித்திருக்கி றார்கள். பேருந்து விபத்துக்களும்
சாவுகளும்தான் அன்றாடச் செய்திகளாக வருகின்றன. </p><p>
</p><p>
திருப்பூர் திட்டம் </p><p>
</p><p>
இதுபோல கோடிக்கணக் கான ரூபாய்க்கான திட்டங்களை அறிவித்திருக்கிறார்கள். அந்த
திட்டங்களுக்கெல்லாம் நிதி ஒதுக்கீடு செய்யப்பட வில்லை. திருப்பூர் திட்டம் பற்றி
தற்காலிக முதல்வர் பெரும் அறிவிப்பு செய்தார்; அது என்ன ஆயிற்றோ? </p><p>
</p><p>
திட்டங்களை அறிவிப்பது மாத்திரமல்ல, ஒன்றை சொல்லி விட்டு அதற்கு நாம் பதில் கூறினால்
அதை அப்படியே விட்டுவிடுகின்ற போக்கும் இந்த அரசில் உள்ளது. </p><p>
</p><p>
கஜானா காலி </p><p>
</p><p>
பொறுப்புக்கு வந்ததும் கஜானாவும் காலி, களஞ்சியமும் காலி என்றார்கள். அ.தி.மு.க.
ஆட்சியிலிருந்து இறங்கும்போது தமிழக அரசின் நிதி நிலையில் எந்தவித பற்றாக்குறையும்
இல்லாமல் இருந்தது, இப்போது 210 கோடிக்கு வருவாய் பற்றாக் குறை என்றார்கள். அவர்களது
ஆட்சியிலும் பற்றாக் குறை பட்ஜெட்தான் என்று பதில் கூறினோம். உடனே அதையும் மாற்றிக்
கொண்டார்கள். அதைப்போலவே களஞ்சியமும் காலி இல்லை, 14 லட்சம் டன் அரிசி
கையிருப்பிலே உள்ளது என்று பதில் கூறினேன். உடனே ஜெயலலிதா அது உண்மையல்ல என்றார். உடனே
கழகப் பொருளாளர் ஆற்காடு வீராசாமி விடுத்த அறிக்கையில் அரிசியாக 5,30,000 டன்னும்,
நெல்லாக 19,30,000 டன்னும் கையிருப்பிலே உள்ளது. அந்த நெல்லை அரைத்தால் 9 லட்சம்
டன்னுக்கு மேல் அரிசி கிடைக்கும். மொத்த கையிருப்பு அரிசி 14 லட்சம் டன்னுக்கு அதிகம்.
அதன் மதிப்பு 1000 கோடி ரூபாய்க்கு மேலாகும் என்று பதில் கூறினார். </p><p>
</p><p>
புழுத்த அரிசி </p><p>
</p><p>
முதலிலே களஞ்சியம் காலி என்று சொன்னவர்கள் அதனை மாற்றிக்கொண்டு அரிசி
இருக்கின்றது, ஆனால் அதெல்லாம் புழுத்த அரிசி, கெட்டுப்போன அரிசி என்றார்கள். அந்த
தகவலும் தவறான தகவல் என்பதை விழுப்புரம் அரிசி கிடங்கிற்கு செய்தியாளர்களை அழைத்துச்
சென்று அந்த தொகுதியின் சட்டமன்ற உறுப்பினர் பொன்முடி நிரூபித்துக் காட்டினார். </p><p>
</p><p>
சென்னையில் அமெரிக்க உதவியுடன் 63 கோடி ரூபாய் செலவில் உயிரியல் தொழில் நுட்ப
பூங்கா ஒன்று அமைக்க ஜெயலலிதா முன்னிலையில் ஒப்பந்தம் கையெழுத்திட்டதாக ஏடுகளில்
படத்துடன் செய்தி வந்தது. ஆனால் இதற்கான ஒப்பந்தம் கழக ஆட்சி இருந்த போது போடப்பட்டு,
அது படத்துடன் செய்தியாக வந் ததை எடுத்துக் காட்டிய பிறகு அதற்கு எந்தவிதமான பதிலும் அரசு
தரப்பிலிருந்து வர வில்லை. </p><p>
</p><p>
முன்பணம் சுருங்கியது </p><p>
</p><p>
மொத்த அரசு வருவாயில் 94 சதவிகிதம் அரசு அலுவலர்களுக்கும் ஓய்வூதியதாரர் களுக்கும்
செலவாகி விடுகிறது. மீதி 6 சதவிகிதம்தான் வளர்ச்சி பணிகளுக்கு செலவாகிறது என்று ஜெயலலிதா
தெரி வித்தார். அது தவறான புள்ளி விவரம் என்று சுட்டிக்காட்டப்பட்டது. அதற்குப் பிறகு
அதைப்பற்றி அரசிடமிருந்து எந்த விளக்கமும் இல்லை. </p><p>
</p><p>
ஆண்டுதோறும் தீபாவளி பண்டிகைக்காக அரசு அலுவலர்கள் அனைவருக்கும் வழங்கப்பட்டு வந்த ஆயிரம்
ரூபாய் முன்பணம்; இப்போது கடை நிலை ஊழியர்களுக்கு மட்டும் என்று சுருங்கி விட்டது. </p><p>
</p><p>
வெத்து வேட்டுகள் </p><p>
</p><p>
சில கட்சிகளின் வாக்குறுதிகள் காற்றில் பறந்து விடுவதுண்டு; ஆனால் அ.தி.மு.க. ஆட்சியின்
வாக்குறுதிகளும் அறிவிப்புகளும் வெத்து வேட்டுக்களாக வெறும் புகை பரப்பி, காற்றையே மாசுபடுத்திக்
கொண்டிருக்கின்றன. </p><p>
</p><p>
இவ்வாறு கூறி உள்ளார். </p>

<p> </p>

<p>காங். தலைமையிலான 3-வது
அணியை மக்கள் வரவேற்கிறார்கள்: அ.தி.மு.க.வுடன் மீண்டும் கூட்டணி வேண்டாம், வர்த்தகர்
காங்கிரஸ் தலைவர் எச்.வசந்தகுமார் சிறப்பு பேட்டி</p>

<p> </p>

<p>சென்னை, நவ. 3- அ.தி.மு.க.வுடன் மீண்டும் கூட்டணி வேண்டாம் என்று
சென்னை மேயர் தேர்தலில் போட்டியிட்ட காங்கிரஸ் வேட்பாளர் எச்.வசந்தகுமார் கூறினார். </p><p>
</p><p>
சென்னை மாநகராட்சி காங்கிரஸ் மேயர் வேட்பாளரும் வர்த்தகர் காங்கிரஸ் தலைவரு மான
எச்.வசந்தகுமார் தினகரனுக்கு அளித்த சிறப்பு பேட்டி வருமாறு:- </p><p>
</p><p>
கேள்வி:- இதற்கு முன் ஏதாவது தேர்தலில் போட்டியிட்டிருக்கிறீர்களா? </p><p>
</p><p>
பதில்:- இந்து கல்லூரியில் படித்த போது மாணவர் பேரவைக்கு காங்கிரஸ் சார்பில் தலைவராக
போட்டியிட்டு வெற்றி பெற்றேன். 1969-ல் கல்லூரிக்கு பெருந்தலைவர் காமராஜரை அழைத்து
சென்று நிகழ்ச்சி நடத்தினோம். அதற்கு பிறகு நான் போட்டி யிட்ட ஒரே தேர்தல் சென்னை மாநகராட்சி
மேயர் தேர்தல்தான். </p><p>
</p><p>
அனுபவம் </p><p>
</p><p>
கேள்வி:- வியாபாரியான நீங்கள் மேயர் தேர்தலில் பெற்ற அனுபவம் எப்படி இருந்தது? </p><p>
</p><p>
பதில்:- ஒரு ஜனநாயக நாட்டில் மக்களுக்கு உண்மையான சேவை செய்ய வேண்டும் என்ற எண்ணம் உள்ள
நான் மாநகராட்சி தேர்தலில் போட்டியிடு வதன் மூலம் மக்களுக்கு ஏதா வது செய்ய முடியும் என்று
தான் போட்டியிட்டேன். இப் போதும் கூட மக்களிடம் அரசியல் என்றால் ஒரு மாதிரியாகத்
தான் பேசுகிறார்கள். லஞ்சம்- லாவண்யம் என்று அரசியலை பொதுமக்கள்
வெறுக்கிறார்கள். இப்படி எல்லோரும் வெறுத் தால் அரசியலை தூய்மை படுத்துவது யார்?.
அரசியலை தூய்மை படுத்தினால்தான் உண்மையான ஜனநாயகம் இயங்கும். இந்த தேர்தல் மூலம்
எனக்கு கிடைத்த அனுபவம் சென்னை என்றால் வௌி உலகுக்கு ஏதோ பிரமாண்டமான நகரம், மக்கள்
செல்வச் செழிப்போடு வாழ்கிறார்கள். ஒரு சொர்க்க பூமி என்று கிராமத்தில் உள்ளவர்கள்
நினைப்பார்கள். </p><p>
</p><p>
ஆனால் எனது அனுபவப்படி மெயின் ரோட்டை விட்டு இறங்கி தெருக்களுக்குள் சென்றால் மனிதர்கள்
நடமாடக்கூட முடியாத அளவுக்குத்தான் தெருக்கள் உள்ளன. பகல் நேரத்தில் குண்டு- குழிகள்-
குப்பையை நாம் கண்டு- பார்த்து ஒதுங்கி போகலாம். ஆனால் இரவு நேரத்தில் சிரமமான
சூழ்நிலை உள்ளது. சாக் கடைகள் மூலம் தான் கொசு உற்பத்தியாகும் என்று சொல்வார்கள்.
ஆனால் சாக்கடை அதிகமாக தேங்கி நிற்கும் பல ஏரியாக்களை நான் பார்த்தேன். இவ்வளவு
மோசமான சூழ்நிலையில்தான் நாம் சென்னையில் வாழ்கிறோம். இதுதான் நான் கண்ட அனுபவம். </p><p>
</p><p>
தேர்தல் வன்முறை </p><p>
</p><p>
கேள்வி:- தேர்தலில் நடந்த வன்முறை பற்றி உங்கள் கருத்து என்ன? </p><p>
</p><p>
பதில்:- தேர்தல் நடந்த அன்று காலையில் நடைபெற்ற நிகழ்ச்சிகளை பார்க்கும் போது ஜனநாயக
நாட்டில் நமக்கு எல்லா பாதுகாப்பும் இருக்கிறது. நாம் சுய சிந்தனையோடு விரும்புவோரை
தேர்ந்தெடுக்க லாம் என்ற எண்ணம் இருந்தது. காலை 9.30 மணிக்கு பிறகு அது மறைந்து விட்டது.
தேர்தல் கமிஷன் மிகவும் சக்தி வாய்ந்தது. அவர்கள் நினைத் தால் இங்கே நடக்கிற
நிகழ்ச்சி களையெல்லாம் தடுத்து நிறுத்தி விடலாம் என்ற எண்ணத்தில் அவர்களை தொடர்பு
கொண் டோம். ஆனால் அவர்களால் ஒன்றும் செய்ய முடியாத சூழ்நிலை இருந்தது. எனவே பலம்
உள்ளவன் தன்னால் முடிந்த அளவு தனது சின்னத்தில் முத்திரையை பதித்துக் கொண்டனர். இந்த
போக்கு நீடித்தால் உலகில் 2-வது பெரிய ஜனநாயக நாடு என்று சொல்கிற இந்தியாவில் தேர்தலை
சந்திப்பதற்கே நல்லவர்கள் பயப்படும் நிலை உருவாகி விடும். மக்களுக்கு சேவை செய்ய
வேண்டும் என்ற எண்ணத்தில்தான் நான் மேயர் தேர்தலில் போட்டியிட்டேன். </p><p>
</p><p>
பணபலம் </p><p>
</p><p>
ஆனால் பணபலம், படை பலம், அதிகார பலத்தை காட்டு கிற ஒரு காட்சியை கண்டேன். நாங்கள்
வௌ்ளைக்காரர்களின் கொடுங்கோல் ஆட்சி யையே கண்டவர்கள். அதனால் துவண்டு போக
மாட்டோம். ஆனால் வாக்களித்த மக்களுக்கு ஒரு நம்பிக்கையை தருவது யார்? இதற்கு
ஆளுகிறவர்கள்தான் பதில் தர வேண்டும். </p><p>
</p><p>
கேள்வி:- உங்களுக்கு ஒரு லட்சத்துக்கு மேல் ஓட்டு கிடைத்திருக்கிறதே? </p><p>
</p><p>
மாணிக்க கற்கள் </p><p>
</p><p>
பதில்:- இத்தனை கெடுபிடிகள், அடிதடி, கலாட்டாக்கள், பயமுறுத்தல்கள், மிரட்டல்கள்
அத்தனையையும் தாண்டி சென்னை மாநகர மக்கள் எனக்கு தந்த 1,02,696 வாக்கு களை மாணிக்க
கற்களாகவே நான் நினைக்கிறேன். அவர் களுக்கு நானும், காங்கிரஸ் கட்சியும் முடிந்த
அளவுக்கு சேவைகளை தொடர்ந்து செய்வோம். இப்போதுகூட நான் தனிப்பட்ட முறையில் காமராஜர்
பிறந்த தின விழாவை கல்வி எழுச்சி நாள் என்று தமிழக அரசிடம் இருந்து அறிவிப்பை
பெற்றுள்ளேன். ஆயிரக்கணக்கான மாணவர் களுக்கு நோட்டு- புத்தகங்கள் வழங்கியுள்ளேன். நோயாளிகளுக்கு
இலவசமாக மருந்துகள் வாங்கிக் கொடுக்கிறேன். மாநகராட்சி பள்ளிகளுக்கு டி.வி.க்கள்,
மின்விசிறியும் வழங்கியுள்ளேன். இந்த சேவைகளை தொடர்ந்து செய்வேன். வருகிற 2002 ஜூலை
15-ந்தேதி காமராஜர் 100-வது பிறந்த நாள் விழா கொண்டாடப் படுகிறது. இதற்கு இந்தியா
முழுவதும் தலைவர்களை அழைத்து மிகச்சிறப்பான விழாவை சென்னையில் நடத்த இருக்கிறேன். அன்னை
சோனியா காந்தியையும், காங்கிரஸ் முதல்வர்கள் அத்தனை பேரையும் அழைத்து பிர மாண்டமான
முறையில் தமிழ் நாடு காங்கிரஸ் கமிட்டி தலைவர் இளங்கோவன் ஒத்துழைப்புடன் நடத்த
உள்ளேன். </p><p>
</p><p>
அ.தி.மு.க.வுடன் </p><p>
</p><p>
மீண்டும் கூட்டணி </p><p>
</p><p>
கேள்வி:- மீண்டும் அ.தி.மு.க. கூட்டணிக்கு போக வேண்டும் என்று காங்கிரசில் சிலர்
கூறுகிறார்களே? </p><p>
</p><p>
பதில்:- காங்கிரஸ் தலைமையிலான 3-வது அணியை பொதுமக்கள் வரவேற்கிறார் கள். கட்சியில்
உள்ள சிலர் வேண்டுமானால் எதிர்க்கலாம். பெரும்பான்மையோர் 3-வது அணியை
வரவேற்கிறார்கள். கண்டிப்பாக தொடர வேண்டும். மீண்டும் அ.தி.மு.க. கூட்டணி வேண்டாம். 34
ஆண்டுகள் ஆட்சியில் இல்லாமல் இருந்தவர்கள் மீண்டும் 5 ஆண்டில் இருப்பதில் எந்த சிரமமும்
இல்லை. மக்கள் எப்போது எங்கள் சேவையை விரும்புகிறார்களோ, அதுவரை தொடர்ந்து உழைத்துக்
கொண்டிருப்போம். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>மதுரை துணை மேயர் தேர்தலில்
அதிர்ச்சி தோல்வி: அ.தி.மு.க. மாவட்ட செயலாளர்-மந்திரி மீது ஜெ. கடும் கோபம்,
விரைவில் நடவடிக்கை</p>

<p> </p>

<p>மதுரை, நவ. 3- மதுரை துணை மேயர் தேர்தலில்அ.தி.மு.க.வுக்கு
அதிர்ச்சி தோல்வி ஏற்பட்டதால் மந்திரி- மாவட்ட செயலாளர் மீது ஜெயலலிதா கடும் கோபம்
அடைந்துள்ளார். விரைவில் அவர்கள் மீது நடவடிக்கை இருக்கும் என்று தெரிகிறது. </p><p>
</p><p>
தமிழகம் முழுவதும் பெரும் பரபரப்பு ஏற்படுத்தியது மதுரை துணை மேயர் தேர்தல். ஏனென்றால்
மேயர் தேர்தலில் ்அ.தி.மு.க. தோல்வி அடைந்து விட்டதால் துணை மேயர் பதவியை எப்படியாவது
பிடித்து விட வேண்டும் என்பதில் குறியாய் இருந்தது. தேர்தலில் அ.தி.மு.கவில் 22
கவுன்சிலர்கள் மட்டுமே ஜெயித்தனர். துணை மேயர் பதவியை பிடிக்க 37 ஓட்டு தேவைப்பட்டது.
இதற்காக ஆட்சி அதிகாரம்- பண பலத்தை பயன்படுத்தி சுயேட்சை கவுன்சிலர்களை இழுத்தது.
காங்கிரஸ் கவுன் சிலரையும் இழுத்தனர். உறவை முறித்து தனித்து போட்டியிட்ட மார்க்சிய
கம்யூனிஸ்டின் ஆதரவை கெஞ்சி வாங்கியது. அதோடு த.மா.கா. ஆதரவை மன்றாடி பெற்றது.
கடைசி நேரத்தில் மிசா.பாண்டியனை கூட சேர்த்தனர். </p><p>
</p><p>
இவ்வளவு வேலைகளையும் அ.தி.மு.க. மேலிடம் செய்து கொடுத்து எம்.எஸ்.சுப்புவை வெற்றி பெற
வைக்கும் பொறுப்பை மந்திரி துரைராஜ்- மதுரை மாநகர் மாவட்ட அ.தி.மு.க. செயலாளர் மச்சக்
குமார் மற்றும் நிர்வாகிகளிடம் ஜெயலலிதா ஒப்படைத்தார். 37 ஓட்டு இருக்கிறது வெற்றி
பெற்று விடலாம்என்ற நம்பிக்கை யோடு இருந்தனர். ஆனால் 2 ஓட்டு வித்தியாசத்தில்
அ.தி.மு.க. தோல்வி அடைந்து தி.மு.க. வெற்றி பெற்று விட்டது. அதோடு செல்லாமல் போன ஒரு ஓட்டும்
தி.மு.க. ஓட்டு தான். அதுவும் செல்லுபடி ஆகி இருந்தால் 3 ஓட்டு வித்தியாசம் ஆகி இருக்கும். </p><p>
</p><p>
கட்சி மாறி ஓட்டு </p><p>
</p><p>
கட்சி மாறி விழந்ததில் அ.தி.மு.க. ஓட்டும் உண்டு. தேர்தலுக்கு முன் 4 நாட்களாக அ.தி.மு.க.
கவுன்சிலர்களை கன்னியாகுமரி, திருச்செந்தூர் என பிக்னிக் அழைத்து சென்றனர். தேர்தல்
நாளன்று ஓட்டுப்பதிவுக்கு 5 நிமிடத்துக்கு முன் சர்க்கியூட் அவுசில் மந்திரி துரைராஜ் முன்
சத்தியம் வாங்கி த.மா.கா. உள்பட 32 கவுன்சிலர்களை தோளை பிடித்து எண்ணி தேர்தல்
நடந்த அரங்கத்துக்குள் அனுப்பினர். மார்க்சிய கம்யூ னிஸ்டு கவுன்சிலர்கள் நாங்கள்
மந்திரி முன் சத்தியம் செய்து வாக்களிக்க தயாராக இல்லை நிச்சயம் அ.தி.மு.க.வுக்கு
வாக்களிப்போம் என்று கூறி விட்டனர். இவ்வளவு வேலை களை செய்தும் தன் கட்சி ஓட்டுகளையே
அ.தி.மு.க.வால் வாங்க முடியவில்லை. </p><p>
</p><p>
கட்சி மாறி ஓட்டு போட்டது யார் என்பதை கண்டுபிடிக்க வும் முடியவில்லை. </p><p>
</p><p>
ஜெ. கடும் கோபம் </p><p>
</p><p>
37 ஓட்டு சேர்த்து கொடுத்தும் வெற்றி பெற வைக்க முடியாததால் மந்திரி துரைராஜ், மதுரை
மாநகர் மாவட்ட செயலாளர் மச்சக் குமார் மற்றும் நிர்வாகிகள் மீது ஜெயலலிதா கடும் கோபம்
அடைந்துள்ளதாக கூறப்படு கிறது. இவர்கள் சொன்ன சாக்குபோக்கு விளக்கத்தை ஜெயலலிதா
ஏற்கவில்லை. இவர்கள் திறமை குறைவே காரணம் என்று அ.தி.மு.க.வி னரே கூறுகிறார்கள்.
இதுபற்றி ஜெயலிதாவுக்கு புகார் அனுப்பி உள்ளனர். </p><p>
</p><p>
2 ஒன்றியம் தோல்வி </p><p>
</p><p>
மதுரை புறநகர் மாவட்ட செயலாளர் பொறுப்பில் மந்திரி துரைராஜ் தான் இருக்கிறார். மதுரை
கிழக்கு, மேற்கு ஊராட்சி ஒன்றிய தலைவர் தேர்தலில் அ.தி.மு.க. வுக்கு ஆதரவான நிலை
இருந்தும் தி.மு.க. வெற்றி பெற்று விட்டது. இன்னும் சில ஒன்றிய தலைவர் பதவி தலை தப்பியது
என்ற நிலையிலேயே ஜெயித்து இருக்கிறது. </p><p>
</p><p>
விரைவில் நடவடிக்கை </p><p>
</p><p>
அ.தி.மு.க.வின் இந்த தோல்விக்கு மந்திரி- மாவட்ட செயலாளர்களின் திறமை குறைவே காரணம்
என்று கூறப்படுகிறது. இதனால் விரைவில் இவர்கள் மீது நடவடிக்கை எடுக்கப்படும் என்று
கூறப்படுகிறது. துணை மேயர் தேர்தலுக்கு வேட்பாளர் யார் என்று சொன்னதுமே மதுரை சர்க்கியூட்
அவுசில் மந்திரி மீது அ.தி.மு.க. கவுன்சிலர்கள் கோபத்துடன் பேசி இருக்கிறார்கள். அப்
போதே மந்திரிக்கு நிலைமையை புரிந்து கொள்ளும் சக்தி இல்லாமல் போய் விட்டது என்று
அ.தி.மு.க.வினர் புகார் கூறினர். </p><p>
</p><p>
நீக்க கோரி பேக்ஸ் </p><p>
</p><p>
மதுரையில் எந்த திறமையும் இல்லாத மாவட்ட செயலாளர் இருப்பதால் தான் துணை மேயர்
தேர்தலில் தோற்று விட்டது. அவரை உடனே மாற்ற வேண்டும் என்று அ.தி.மு.க வினர் பலர்
ஜெயலலிதாவுக்கு பேக்ஸ் அனுப்பி இருக்கிறார்கள். வேறு யாரை மாவட்ட செயலாளராக போடலாம்
என்று ஜெயலலிதா ஆலோசித்து வருவதாக கூறப்படுகிறது. </p><p>
</p><p>
ஆணையாளர் மீது புகார் </p><p>
</p><p>
இதை அறிந்த மதுரை மாநகர் மாவட்ட செயலாளர் மச்சக்குமார் துணை மேயர் தேர்தலில்
அ.தி.மு.க. தோல்விக்கு தேர்தலை நடத்திய மாநகராட்சி ஆணையாளர் ஹர்ஷஹாய் மீனா மீது
புகார் கூறி உள்ளார். அவர் நிருபர்களிடம் கூறியதாவது:- </p><p>
</p><p>
துணை மேயர் தேர்தலுக்கு முந்தின நாள் மாநகராட்சி ஆணையாளரை சந்தித்து தேர்தல் நடத்தும்
விதம் குறித்து கேட்டேன். அவர் ஓட்டுச் சீட்டில் வேட்பாளரின் பெயருக்கு நேரே உள்ள
கட்டத்துக்குள் டிக் அடிக்க வேண்டும் என்று கூறினார். இதற்கு நான் எதிர்ப்பு தெரிவித்தேன்.
சின்னம் வைத்து அதில் முத்திரையிடும் முறைப்படி நடத்தலாம் என்றேன். என் யோசனை
ஏற்கப்படவில்லை. </p><p>
</p><p>
எங்கள் கட்சி கவுன்சிலர்கள் மாற்றி ஓட்டு போட வாய்ப்பு இல்லை. ஓட்டு பெட்டி இடம்
மாற்றப்பட்டபோது அதில் போலி சீட்டுகள் சேர்க்கப்பட்டு உள்ளன. இதில் ஆணையாளர் மேயருக்கு
ஆதரவாகி விட்டார். மறுதேர்தல் நடத்த வேண்டும் என்ற எங்கள் கோரிக்கையை ஏற்க மறுத்து
விட்டனர். </p><p>
</p><p>
இவ்வாறு கூறினார். </p><p>
</p><p>
கேலிக்கூத்து </p><p>
</p><p>
ஆளும் கட்சியின் மாவட்ட செயலாளரே தேர்தல் நடத்திய அரசு அதிகாரி மீது குறை கூறி இருப்பது
கேலிக்கூத்தானது. துணை மேயர் தேர்தல் ஓட்டு எண்ணிக்கை நடந்த அரங்கத்துக்குள்
அ.தி.மு.க.வினர் புகுந்து ஓட்டுச் சீட்டுகளை சூறையாட முயன்ற போது தான் போலீசார் தடியடி
நடத்தினர். அதை தொடர்ந்து அ.தி.மு.க. வினர் மாநகராட்சி ஆபீசில் கலவரத்தில்
ஈடுபட்டு அங்கு இருந்த மேஜை- நாற்காலி, மைக், டியூப் லைட், பூந் தொட்டிகளை அடித்து
நொறுக்கினர். இது பற்றி ஆணையாளர் போலீசில் புகார் கூறி உள்ளார். இதன் காரணமாக
ஆணையாளர் மீது அ.தி.மு.க. புகார் கூறுவதாகவும் தெரிகிறது. </p><p>
</p><p>
அ.தி.மு.க.வினர் நடத்திய கலவரத்துக்கு போலீசார் வழக்கு பதிவு செய்து உள்ளனர். இந்த
சூழ்நிலையில் தடியடி நடத்திய போலீசார் மீது அ.தி.மு.க.வினர் புகார் கூறி வருகிறார்கள். </p>

<p> </p>

<p> </p>






</body></text></cesDoc>