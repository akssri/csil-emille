<cesDoc id="tam-w-dinakaran-politics-01-01-17" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-politics-01-01-17.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-01-17</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-01-17</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>நல்லகண்ணு சமாதானம்
செய்தும் பிடிவாதம்: வீரமணியை சந்திக்க ஜெயலலிதா மறுப்பு, மோதல் பின்னணி என்ன?
பரபரப்பு தகவல்கள்</p>

<p> </p>

<p>சென்னை, ஜன. 17- வீரமணி - ஜெயலலிதா மோதல் குறித்து விசாரித்த
போது மேலும் பல பரபரப்பான பின்னணி தகவல்கள் வௌியாகின. </p><p>
</p><p>
தி.மு.க. கூட்டணியிலிருந்து த.மா.கா.வை பிரித்து அ.தி.மு.க. கூட்டணியில் சேர்ப்பதற்கு
பாடுபட்டவர் வீரமணி. கடந்த (1999)-ம் ஆண்டு டிசம்பரில் ஜெயலலிதா அளித்த இப்தார்
விருந்தில் முதன்முதலாக மூப்பனாரை கலந்து கொள்ள வைத்து அ.தி.மு.க.-த.மா.கா. கூட்டணிக்கு
அச்சாரம் போடப்பட்டது. பின்னர் கடந்த ஆண்டு ஜனவரி முதல் வாரத்தில் தி.க. சார்பில்
வல்லத்திலுள்ள பெரி யார் தொழில்நுட்பக் கல்லூரி விழாவில் ஜெயலலிதாவையும்-
மூப்பனாரையும் ஒன்றாக கலந்து கொள்ள செய்து இந்த உறவுக்கு மேலும் பாலமாக இருந்தார்
வீரமணி. இந்த நிகழ்ச்சிக்கு பிறகு ஜனவரி 27-ம் நாள் தனிப்பட்ட முறையில் காங்கிரசைக்கூட
கலந்து பேசாமல் மூன்று சட்டமன்ற இடைத் தேர்தலில் த.மா.கா., அ.தி.மு.க. விற்கு
நிபந்தனையற்ற ஆதரவு அளிக்கும் என்று மூப்பனார் அறிவித்தார். </p><p>
</p><p>
வீரமணி உறுதி </p><p>
</p><p>
அந்த நேரத்தில் பின்னர் வரும் பொதுத்தேர்தலில் ஆட்சியில் பங்கு தரப்படும் என்று
ஜெயலலிதா சார்பில் மூப்பனாருக்கு வீரமணி உறுதி அளித்திருந்ததாக கூறப்படுகிறது. அவ்வப்போது
ஜெயலலிதா தனித்தே ஆட்சி என்று கூறி வந்தாலும், கூட்டணி ஆட்சி என்பதற்கு நான் அந்த
அம்மாவை சம்மதிக்க வைக்கிறேன் என்று வீரமணி, மூப்பனாரிடம் சமாதானம் சொல்லி
வந்தாராம். அ.தி.மு.க.-த.மா.கா.விடையே எப்போதெல்லாம் அறிக்கைப்போர், சண்டைகள்
நடைபெறுகிறதோ அப்போதெல்லாம் வீரமணியும், அவருக்கு துணையாக இருக்கும் ஜெகவீரபாண்டியனுமே
தூது சென்று வந்தார்கள். </p><p>
</p><p>
வீரமணிதான் முதல்வர் </p><p>
</p><p>
கூட்டணி ஆட்சி என்று மூப்பனாரிடம் உறுதியளித்திருந்த தகவல் அறிந்ததுமே ஜெயலலிதா
த.மா.கா.விற்காக என்னிடம் வக்காலத்து வாங்க வேண்டாம் என்று வீரமணியிடம் கடிந்து
கொண்டிருக்கிறார். எந்த போராட்டம் அல்லது கூட்டமாக இருந்தாலும் அ.தி.மு.க.வே தலைமை ஏற்க
வேண்டும் என்றும் கண்டிப்புடன் தெரிவித்திருந்தாராம். மேலும் தி.க. வட்டாரங்களில் அம்மா
தேர்தலில் நிற்க முடியாமல் போனால் வீரமணி தான் முதல்வர் பதவிக்கு முன் நிறுத்தப்படுவார்
என்று பரவலாக பேசப்பட்ட தகவல் அறிந்த ஜெயலலிதாவிற்கு வீரமணியின் மீது இருந்த கோபம்
பன்மடங்காக அதிகரித்தது. </p><p>
</p><p>
சந்திக்க மறுப்பு </p><p>
</p><p>
இந்த நிலையில்தான் இந்த மாதம் முதல் வாரத்தில் ஜெயலலிதாவை சந்தித்த வீரமணி
10-ந்தேதி மதச்சார்பற்ற கட்சிகளின் கூட்டத்தை கூட்ட இருப்பதாக கூறினார். ஜெயலலிதாவும்,
அதற்கு சம்மதம் தெரிவித்திருந் தாராம். பின்னர் நேரடியாக சென்று கூட்ட அழைப்பிதழையும்
கொடுத்து வந்தாராம். ஆனாலும் வீரமணிக்கு செக் வைக்க நினைத்த ஜெயலலிதா தானும் அந்த
கூட்டத்தில் கலந்து கொள்ளாததோடு அன்றைய தினம் கட்சி அலுவலகத் திலேயே நிர்வாகிகள்
பலர் இருந்தும் அவர்களில் யாரையும் கூட அனுப்பாமல் இருந்துவிட்டார். அந்த கூட்டத்தில் மத
நல்லிணக்க மனித சங்கிலி போராட்டத்தை வரும் 22-ந்தேதி மாவட்டம் தோறும் நடத்துவது என்று
முடிவெடுக்கப்பட்டாலும் அதற்கு ஆதரவு தெரி வித்துகூட ஜெயலலிதா இது வரை அறிக்கை
வௌியிடவில்லை. ஜெயலலிதாவை சந்தித்து விளக்கம் அளிக்க முயன்ற வீரமணியையும் ஜெயலலிதா
சந்திக்க மறுத்து விட்டாராம். </p><p>
</p><p>
நாம் இவ்வளவு கஷ்டப்பட்டு, தூதராக செயல்பட்டு கூட்டணியை பலப்படுத்த முயற்சி செய்தால்
அதற்கு அவமரியாதைதான் பலனா என்று வீரமணி அதிருப்தி அடைந்துள்ளாராம். </p><p>
</p><p>
பரிதாப நிலை </p><p>
</p><p>
22-ந்தேதி நடைபெறும் மனித சங்கிலியில் அ.தி.மு.க. கலந்து கொள்ளாவிட்டால் தங்கள் அணி
பலவீனப்படுமே என்ற பயத்தில் வ.கம்யூனிஸ்டு மாநில செயலாளர் நல்ல கண்ணு ஜெயலலிதாவிடம்
பேசி சமாதானப்படுத்தி இருக்கிறார். அப்போது அவரிடம் மனித சங்கிலி போராட்டத்தில்
அ.தி.மு.க. டோக்கன் பார்ட்டிசி பேஷன் ஆக மட்டும் பங்கு கொள்வோம் என்று தெரிவிக்
கப்பட்டதாம். இதனால் இந்த போராட்டத்தில் ஜெயலலிதா பங்கேற்க மாட்டார் என்று
தெரிகிறது. அ.தி.மு.க.வின் ஒரு கூட்டணி கட்சியான வ.கம்யூ னிஸ்டு கட்சிக்கு, தூதருக்காக
(வீரமணி) தூதுபோகும் நிலை ஏற்பட்டு விட்டது என்றால் மற்றொரு கூட்டணி கட்சியான
மார்க்சிஸ்டு கட்சியினர் இன்னும் பரிதாப நிலைக்கு தள்ளப்பட்டுள்ளனர். </p><p>
</p><p>
தூக்கி எறிவார் </p><p>
</p><p>
மார்க்சிஸ்டு கட்சி சார்பில் அந்த கட்சியின் தமிழக பிரிவை ஜெயலலிதா மதிப்பதே
இல்லையாம். எதுவாக இருந்தாலும் அக்கட்சியின் அகில இந்திய பொதுச்செயலாளர் சுர்ஜித்திடமே
ஜெயலலிதா நேரடியாக பேசி விடுகிறாராம். சுர்ஜித்தும், தமிழக கம்யூனிஸ்டு தலைவர்களை கண்டு
கொள்வதில்லையாம். இதனால் அக்கட்சியின் தமிழக தலைவர்களும் மிகவும்
அதிருப்தியடைந்திருக்கிறார்கள். </p><p>
</p><p>
ஜெயலலிதாவின் இத்தகைய நடவடிக்கைகள் குறித்து அவருடன் பழகிய ஒரு தலைவர் கூறுகையில், தேவை
என்றால் மண்டியிட்டு வருவார், காரியம் முடிந்ததும் தூக்கி எறிவார் என்றார். அதற்கு பல
உதாரணங்களையும் அவர் தெரிவித்தார். </p><p>
</p><p>
விரிசலா? </p><p>
</p><p>
இவ்வளவுக்கு பிறகும் கூட்டணி தூதர் வேலையை தொடர வேண்டுமா? என்ற கேள்வியுடன்
வீரமணியும், கூட்டணி கட்சி என்றால் இழுத்த இழுப்புக்கெல்லாம் வரவேண்டுமா? என்ற
கேள்வியுடன் அ.தி.மு.க.வின் மற்ற கூட்டணி கட்சியினர் உள்ளனர். </p><p>
</p><p>
தேர்தலுக்கு இன்னும் குறு கிய நாட்களே இருக்கும் நிலையில் கூட்டணி கட்சியினர் எல்லோரையும்
அரவணைத்து செல்லாத ஜெயலலிதாவின் இத்தகைய நடவடிக்கைகளால் அ.தி.மு.க. கூட்டணி கட்சிகள்
மிகவும் அதிருப்தி அடைந்துள்ளன. 22-ந்தேதி மனித சங்கிலி போராட்டத்திற்கு பிறகே இந்த
நிலையில் மாற்றம் ஏற்படுமா அல்லது மேலும் விரிசல் ஏற்படுமா என்பது தெரியவரும். </p>

<p> </p>

<p>பாபர் மசூதி இடிப்பு
சம்பவம்: லிபரான் கமிஷன் முன் அத்வானி ஆஜராகிறார்</p>

<p> </p>

<p>புதுடெல்லி, ஜன. 17- அயோத்தியில் 1992-ம் ஆண்டு நவம்பர் மாதம்
6-ந் தேதி பாபர் மசூதி இடிக்கப்பட்டது அல்லவா? அது குறித்து நீதிபதி லிபரான் தலைமை
யிலான ஒரு நபர் கமிஷன் விசாரித்து வருகிறது. மத்திய மந்திரிகள் அத்வானி, முரளி மனோகர்
ஜோஷி, உமாபாரதி ஆகியோர் பாபர் மசூதி இடிப்பு வழக்கில் குற்றம் சாட்டப்பட் டுள்ளனர்.
</p><p>
</p><p>
ஆஜராக உத்தரவு </p><p>
</p><p>
இந்தநிலையில் மேற்படி வழக்கில் சம்பந்தப்பட்ட மத்திய விளையாட்டுத்துறை மந்திரி
உமாபாரதி நேற்று லிபரான் கமிஷன் முன் ஆஜரானார். அவரிடம் தீவிர விசாரணை நடத்திய
நீதிபதி லிபரான் பிப் ரவரி 9-ந்தேதியன்று மீண்டும் ஆஜராகும்படி உமாபாரதிக்கு
உத்தரவிட்டார். </p><p>
</p><p>
இந்த வழக்கில் தொடர்புடைய மற்றொரு நபரான அத்வானி மார்ச் 5 மற்றும் 6-ந் தேதிகளில்
வந்து ஆஜராகும்படி நீதிபதி லிபரான் சம்மன் அனுப்பியுள்ளார். மத்திய மனித வள மேம்பாட்டு
மந்திரி முரளி மனோகர் ஜோஷியை மார்ச் 22 மற்றும் 23-ந் தேதிகளில் வந்து ஆஜராகும்படி
கமிஷன் கேட்டுக்கொண்டுள்ளது. இவர்கள் அனைவரும் ஆஜராகி தங்கள் தரப்பு விளக்கத்தை
அளிக்குமாறு கமிஷன் கூறியுள்ளது. சம்பந்தப்பட்ட 3 பேரும் பாபர் மசூதி இடிக்கப்பட்ட போது
அயோத்தியில் இருந்தவர்கள் என்பது குறிப்பிடத் தக்கது. </p><p>
</p><p>
ஜோதிபாசு </p><p>
</p><p>
பாபர் மசூதி இடிப்பு தொடர்பாக மேற்கு வங்க மாநில முன்னாள் முதல்வர் ஜோதிபாசுவும் இந்த
மாதம் 29-ந்தேதியன்று லிபரான் கமிஷன் முன் ஆஜராக உள்ளார். ஆர்.எஸ்.எஸ். தலைவர் பிப்ர
வரி 6-ந்தேதியும், காந்திய வாதி யான தேஷ்பாண்டே ஜனவரி 30-ந்தேதியும் ஆஜராக
உள்ளனர். </p><p>
</p><p>
மசூதி இடிக்கப்பட்ட போது அதை தடுக்க தக்க நடவடிக்கை எடுக்காமல் இருந்த அப்போ தைய
பிரதமர் நரசிம்மராவும் லிபரான் கமிஷன் முன் ஆஜ ராக உள்ளார். பிப்ரவரி 19 மற்றும்
20-ந்தேதிகளில் அவர் ஆஜராகி விளக்கம் அளிப்பார். </p><p>
</p><p>
உமாபாரதி மறுப்பு </p><p>
</p><p>
இதனிடையே லிபரான் கமிஷன் முன் நேற்று ஆஜரான உமாபாரதி அவர் மீதான பல் வேறு
குற்றச்சாட்டுக்களை மறுத்தார். அவர் கூறியதாவது:- </p><p>
</p><p>
மசூதியை இடிக்க வந்த கரசேவகர்களை நான் ஊக்குவிக்கவில்லை. மசூதி இடிபடு வதற்கு 3 நிமிடம்
முன் பாபர் மசூதியை இடித்து தள்ளுங்கள் என்று நான் கோஷம் எதுவும் எழுப்பவில்லை. எனக்கு
எதுவும் நினைவில்லை. </p><p>
</p><p>
ரத்தக்காட்டேரி </p><p>
</p><p>
அதுபோல முன்னாள் பிரதமர் வி.பி.சிங் மற்றும் உ.பி. முன்னாள் முதல்வர் முலாயம் சிங்
யாதவை நான் ரத்தக் காட்டேரிகள் என்று ஒருபோதும் குறிப்பிடவில்லை. மசூதி இடிக்கப்பட்ட
போது நான் கடவுள் நாமத்தை மட்டுமே சொல்லிக் கொண்டிருந்தேன். மசூதி இடிக்கப்பட்ட பின்
முஸ்லிம் மக்களின் வீடுகள் சூறையாடப்பட்டதை நாளிதழ்களை பார்த்துதான் நான் தெரிந்து
கொண்டேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறியுள்ளார். </p>

<p> </p>

<p>தி.ராமமூர்த்தியை மாற்ற
போராடிய தங்கபாலு கொஞ்சி குலாவுவது ஏன்? இளங்கோவன் ஆதரவாளர்கள் கேள்வி</p>

<p> </p>

<p>சென்னை, ஜன. 17- தமிழ்நாடு காங்கிரஸ் பொதுக்குழு உறுப்பினர்கள்
டி.செல்வம், ஏ.கே.சிரஞ்சீவி, ஏ.பி.சூர்யபிரகாஷ், வி.ஏ.பால் பாண்டியன்,
பி.விஷ்ணுவர் தன்ரெட்டி ஆகியோர் விடுத்துள்ள அறிக்கையில் கூறியிருப்பதாவது:- </p><p>
</p><p>
பொங்கல் விழா என்ற பெயரில் சமீபத்தில் தங்க பாலுவின் தூண்டுதலின் பேரில் திண்டிவனம்
ராம மூர்த்தி மற்றும் காங்கிரசில் செயல் படாத மக்கள் தொடர் பற்ற சில நபர்கள்
சேர்ந்து சென்னையில் உள்ள ஒரு ஓட்டலில் பொங்கல் விருந்து என்ற பெயரில் காங்கிரஸ்
தலைவி சோனியா காந்தியை மறைமுகமாக எதிர்ப்பதை எந்த காங்கிரஸ் தொண்டனும்
ஏற்றுக்கொள்ள மாட்டார்கள். கடந்த ஒரு வருடமாக திண்டி வனம் ராமமூர்த்தியை மாற்றக் கோரி
தங்கபாலு மற்றும் அன்றைய நிகழ்ச்சியில் கலந்து கொண்ட அனைத்து நபர்களும்
சோனியாகாந்தியை தீவிரமாக நச்சரித்து வந்ததன்விளைவாக இளங்கோவன் தமிழ்நாடு காங்
கிரஸ் தலைவராக நியமிக்கப் பட்டதை இந்த நாடு நன்கு அறியும். </p><p>
</p><p>
குழைந்து குலாவி </p><p>
</p><p>
இளங்கோவன் பதவிக்கு வந்தவுடனே அனைத்து மாவட்ட தலைவர்கள், மாநில நிர்வாகிகள்,
செயற்குழு உறுப் பினர்கள் மற்றும் காங்கிரஸ் மூத்த தலைவர்கள் உள்ளடங்கிய கூட்டத்தை
ஆஸ்கர் பெர்னாண்டஸ் முன்னிலையில் கூட்டி அனைவரின் கருத்தையும் கேட்டு கலந்து ஆலோசித்ததை
தங்கபாலு ஏனோ மறந்து விட்டார். </p><p>
</p><p>
தொண்டர்களால் புறக் கணிக்கப்பட்ட இந்த அரசியல் அநாகரிக பேர்வழிகள் தங்கள்
சுயநலத்திற்காக காங்கிரசில் ஒட்டிக்கொண்டு எட்டப்பர் வேலை செய்து வருவது டெல்லி
மேலிடத்திற்கும் நன்கு தெரியும். எனவேதான் கடந்த முறை இந்த பித்தலாட்ட பேர்வழிகள்
டெல்லி சென்றபோது சோனி யாகாந்தி இவர்களுக்கு தனியாக நேரம் ஒதுக்கித்தராமல் புறக்
கணித்தார். </p><p>
</p><p>
தங்கபாலுவைப்பற்றி தரம் கெட்ட வார்த்தைகளால் தன் அரசியல் வாழ்வில் பெரும் பகுதி பேசி
வந்த திண்டிவனம் ராமமூர்த்தி எந்த முகத்தோடு இப்போது தங்கபாலுவுடன் குழைந்து, குலாவி
வருகிறார் என்பதை அவர்தான் விளக்க வேண்டும். தன்னை பதவியிலிருந்து நீக்கியவுடன்
சோனியாகாந்தியை விமர்சித்ததை யும், கட்சி அலுவலகத்தை சூரை யாடியதையும்,
எல்லாவற்றுக்கும் மேலாக கட்சியின் கொடி கம்பத்தை வேறோடு வெட்டி சாய்த்ததையும் உண்மை
காங்கிரஸ் தொண்டர்கள் மறந்து விட மாட்டார்கள். </p><p>
</p><p>
பதவி பித்தர்கள் </p><p>
</p><p>
கூட்டணி சித்தாந்தம் பேசும் தங்கபாலுதான் தலைவராக இருந்தபோது 98-ம் ஆண்டு பாராளுமன்ற
தேர்தலில் தோழமை கட்சி வேட்பாளர்களுக்கு காங்கிரஸ் கை சின்னத் தைக்கூட நேரத்தோடு
அளிக்காமல் கூட்டணி வேட்பாளர்களை அவமதித்து அலைக்கழித்து காங்கிரஸ் கட்சியையும்
தோழமை கட்சியையும் படுதோல்வி அடையச் செய்ததை யாரும் மறந்திருக்க மாட்டார்கள்.
அதற்காக யாரிடம் எவ்வளவு கைக்கூலி பெற்றார் என்பதை தொண்டர்கள் மற்றும் அன்றைய
தோழமை கட்சி தலைவர்களுக்கு நன்கு தெரியும். </p><p>
</p><p>
மற்றபடி கட்சி தேர்தலில் பல முறைகேடுகளை செய்து விட்டு கட்சிக்கு முறையாக செலுத்த வேண்டிய
3ரூபாய் சந்தாவை செலுத்தக்கூடிய அருகதை அற்ற இந்த பதவி பித்தர்கள், அரசியல்
விற்பன்னர்கள் முடிந்தால் மாவட்ட சுற்றுப் பயணம் செய்து பார்க்கட்டும். காங்கிரஸ்
தொண்டர்கள் அங்கே அவர்களுக்கு தகுந்த பாடம் கற்பிக்க தயாராக உள்ளார்கள் என்பதை இதன்
வாயிலாக நாங்கள் எச்சரிக்கை செய்கின்றோம். </p><p>
</p><p>
இவ்வாறு அதில் கூறியுள்ளனர். </p>

<p> </p>

<p>மாவட்ட தலைவர் தேர்தலில்
இளங்கோவன் மோசடி, தங்கபாலு ஆதரவாளர் புகார்</p>

<p> </p>

<p>சென்னை, ஜன. 17- தமிழ்நாடு காங்கிரஸ் கமிட்டி பொதுச்செயலாளர்
க. சக்திவேல் வௌியிட்ட அறிக்கையில் கூறியிருப்பதாவது:- </p><p>
</p><p>
ஒரு பொய்யை மறைப்பதற்கு இன்னொரு பொய். அந்த இன்னொரு பொய்யை மறைப் பதற்கு மற்றொரு
பொய் இப் படி இளங்கோவன் பேட்டி என்றாலே வழக்கமாகி போய்விட் டது. தமிழ்நாடு
காங்கிரஸ் கமிட்டி கூட்டமோ, மாவட்டத் தலைவர்கள் கூட்டமோ அல் லது வேறு எந்த கூட்டமோ,
காங்கிரஸ் அமைப்பு தேர்தலோ நடைபெறாமல் காங்கிரஸ் செய லற்று போக வைத்த இளங் கோவன்
நடைபெறாததையெல் லாம் நடைபெற்றதாக பேட்டி யளித்துள்ளார். </p><p>
</p><p>
தமிழ்நாடு காங்கிரஸ் தலைவராக இளங்கோவன் பொறுப்பேற்ற பிறகு எந்தக்கூட்டமும்
நடைபெறவில்லை என்று கூறினால் பதவியேற்றவுடன் ஒரே ஒரு மாவட்ட தலைவர்கள் கூட்டத்தை
நடத்தினேன் என்கிறார். அப்படியென்றால் பொறுப் பேற்ற கடந்த ஆறுமாத கால மாக மாவட்ட
தலைவர்கள் கூட்டம் நடைபெறவில்லை. பதவி யேற்றதிலிருந்து மாநில நிர் வாகிகள் மாநில
செயற்குழு கூட்டம் நடைபெறவில்லை என் பதை ஒப்புக்கொண்டதன் மூலம் காங்கிரஸ் தமிழகத்தில்
செயலற்று போய்விட்டது என்ற கூற்றை உண்மையாக்கியுள்ளார். </p><p>
</p><p>
கற்பனை பெயர் </p><p>
</p><p>
தமிழகத்தில் காங்கிரஸ் அமைப்பு தேர்தல்கள் நடை பெறவில்லை என்பது அனை வரும் தெரிந்த
உண்மை. தேர் தல்களே நடைபெறாமல் டெல்லியில் அமர்ந்து மாநில பொதுக்குழு உறுப்பினர்கள்
பட்டியலை தேர்தல் நடந்தது போல தயாரித்து அதிலே காங் கிரஸ் கட்சியில் இல்லாதவர் களும்,
கற்பனை மனிதர்களும், காங்கிரஸ் கட்சியில் பொறுப்பு வகிக்கிற தகுதியில்லாதவர் களுக்கும்
இடமளிக்கப்பட்டுள்ளதை கண்டு தமிழகத்தில் காங் கிரஸ் தொண்டர்கள் கொதித்து போய்
சோனியா காந்தியிடம் முறையிட்டதன் விளைவாக, தமிழகத்தில் காங்கிரசின் அனைத்து அமைப்பு
தேர்தல் களுக்கும் தடை விதிக்கப்பட்டது. மாநில தலைவர், மாநில செயற்குழு உறுப்பினர்கள்,
மாவட்ட தலைவர்கள் என்று எல்லா தேர்தல்களுக்கும் தடை விதிக்கப்பட்டது தமிழகமே அறியும். </p><p>
</p><p>
இன்னும் சொல்லப் போனால், காங்கிரஸ் அமைப்பு குழுத்தலைவர் ராம்நிவாஸ் மிர்தா
தமிழகத்தில் மாவட்ட காங்கிரஸ் தலைவர்கள் தேர்தல் களுக்கு தடை விதித்ததை
பத்திரிகைகளுக்கு அறிவித்ததே இதே இளங்கோவன்தான். </p><p>
</p><p>
மோசடி </p><p>
</p><p>
உண்மை இவ்வாறிருக்க, மாவட்ட காங்கிரஸ் தலைவர் தேர்தல்கள் நடந்ததுபோலவும் புதிய
மாவட்ட தலைவர்கள் தேர்ந்தெடுக்கப்பட்டது போலவும் இளங்கோவன் அறிவித்துள்ளது முழு
பூசணிக்காயை சோற்றில் மறைக்கும் செயலாகும். </p><p>
</p><p>
தமிழகத்தில் காங்கிரஸ் அமைப்பு தேர்தல்கள் நடை பெறவே இல்லை என்பதும் குறிப்பாக
மாவட்ட காங்கிரஸ் தலைவர் தேர்தல்கள் நடை பெறவில்லை என்பதும் தான் உண்மை. அமைப்பு
தேர்தல்கள் நடைபெறும் வரை தற்பொழுது மாவட்ட தலைவர்களாக இருப்பவர்கள்தான் தொடர்ந்து
மாவட்ட தலைவர்களாக செயல்பட வேண்டும். </p><p>
</p><p>
வேதனை </p><p>
</p><p>
அதிலே இளங்கோவன் தனிப்பட்ட முறையில் எத்தகைய புதிய அறிவிப்பையும் வௌியிட
முடியாது.எனவே இளங்கோவன் தொடர்ந்து தேர்தல் நடைபெற்றதாக கூறி வருவதை
நிறுத்திக்கொள்ளவேண்டும். தமிழ்நாடு காங்கிரஸ் கமிட்டி தலைவராக பொறுப்பான முறையிலே
செயல்பட வேண்டியவர். உண்மைக்கு புறம்பான செய்திகளை கூறியே செயல்பட முனைவது வேதனையானதும்,
ஏற்றுக்கொள்ள முடியாததும் ஆகும். </p><p>
</p><p>
இவ்வாறு அவர் அறிக்கையில் கூறியுள்ளார். </p>

<p> </p>

<p> </p>






</body></text></cesDoc>