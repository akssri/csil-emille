<cesDoc id="tam-w-dinakaran-politics-02-03-28" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-politics-02-03-28.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-03-28</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-03-28</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>கர்நாடகத்தில் நடந்த
மாநிலங்களவை தேர்தல்: சாராய ஆலை அதிபர் விஜயமல்லையா வெற்றி, பா.ஜ. எம்.எல்.ஏ.க்கள்
கட்சி மாறி வாக்களித்தனர்</p>

<p> </p>

<p>பெங்களூர், மார்ச் 28- கர்நாடகத்தில் மாநிலங்கள வைக்கு
சுயேச்சையாக போட்டியிட்ட சாராய ஆலை அதிபர் விஜய மல்லையா வெற்றி பெற்றார். இவருக்கு
பா.ஜ. எம். எல்.ஏ.க்கள் கட்சி மாறி வாக்களித்துள்ளனர். </p><p>
</p><p>
8 மாநில சட்டசபைகளில் இருந்து டெல்லி மாநிலங்களவைக்கு 25 உறுப்பினர்களை தேர்ந்து
எடுக்கும் தேர்தல் நேற்று நடைபெற்றது. எம்.எல். ஏ.க்கள் ஓட்டு போட்டனர். </p><p>
</p><p>
மல்லையா தேர்வு </p><p>
</p><p>
கர்நாடகத்தில் சுயேச்சையாக போட்டியிட்ட சாராய ஆலை அதிபர் விஜயமல்லையா வெற்றி
பெற்றார். மற்றும் காங்கிரஸ் வேட்பாளர்கள் முன்னாள் மத்திய மந்திரி ஜனார்த்தன
பூஜாரி, எம்.வி. ராஜசேகரன், முன்னாள் பெங்களூர் மேயர் பிரேமா கரியப்பா ஆகியோரும்
வெற்றி பெற்றனர். மல்லையா உள்பட இவர்கள் அனைவரும் முதன் மந்திரி எஸ்.எம்.கிருஷ்ணாவை
சந்தித்து வாழ்த்து பெற்றனர். </p><p>
</p><p>
பா.ஜ. கட்சி மாறி ஓட்டு </p><p>
</p><p>
சுயேச்சையாக போட்டியிட்ட விஜயமல்லையாவுக்கு காங்கிரஸ் கட்சி எஞ்சிய வாக்கு களை
போடுவதாக இருந்தது. இது தவிர பா.ஜ. எம்.எல்.ஏ.க்களின் கட்சி மாறிய ஓட்டுகள், ஐக்கிய
மற்றும் மதசார்பற்ற ஜனதா தளம் எம்.எல்.ஏ.க்களின் ஓட்டுகளால் இவர் வெற்றி பெற்று
உள்ளார். </p><p>
</p><p>
பா.ஜ. எம்.எல்.ஏ.க்கள் 6 பேர் கட்சி மாறி வாக்களித்து உள்ளனர். இதன் விளைவாக
பா.ஜ.வின் வேட்பாளர் தாராதேவி சித்தார்தாவுக்கு 6 ஓட்டு குறைவாக கிடைத்தது. விஜய
மல்லையாவுக்கு தேவைக்கு அதிகமாக 2 ஓட்டு கூடுதலாக கிடைத்தது. பா.ஜ. கட்சி தனது 40 எம்.எல்.ஏ.க்களை
கட்டுக் கோப்பாக வைக்க முடியாமல் போனது அரசியலில் அந்த கட்சியின் பரிதாப நிலையை
காட்டுவதாக அமைந்தது. </p><p>
</p><p>
லல்லு, சத்ருகன் வெற்றி </p><p>
</p><p>
பீகாரில் லல்லுபிரசாத் யாதவ்(ரா.ஜ) சத்ருகன்சின்கா (பா.ஜ) மற்றும் பண்டாரி (ரா.ஜ)
நாராயண்சிங் (சமதா) ஆகியோர் தேர்ந்தெடுக்கப்பட்டனர். </p><p>
</p><p>
தே.ஜ.கூட்டணி கட்சிகளான ஐக்கிய ஜனதாதளம், ஜனசக்தி ஆகிய கட்சிகளின் சுயேச்சை
வேட்பாளர் ரஞ்சன் என்பவர் தோற்றது, தே.ஜ. கூட்டணியில் ஏற்பட்ட பிளவின் ஆழத்தை
வௌிப்படுத்தி விட்டது. </p><p>
</p><p>
அதிருப்தியாளர் வெற்றி </p><p>
</p><p>
ஒரிசாவில் ஆளும் பிஜு ஜனதா கட்சியின் அதிருப்தி வேட்பாளரும், முன்னாள் மத்திய
மந்திரியுமான திலீப்ராய் வெற்றி பெற்றார். இவருக்கு ஆளும் கட்சி எம்.எல்.ஏ.க்கள்
கட்சி மாறி வாக்களித்துள்ளனர். மற்றும் பிஜு ஜனதா தளத்தின் 2 வேட்பாளர்களும்,
பா.ஜ.வின் ஒரு வேட்பாளரும் வெற்றி பெற்றனர். </p><p>
</p><p>
14 பேர் கட்சி மாறி ஓட்டு </p><p>
</p><p>
பிஜு ஜனதா தளத்தின் வேட்பாளர்கள் வெற்றி பெற்ற போதிலும் இதன் எம்.எல். ஏ.க்கள்
14பேர் கட்சி மாறி, கட்சியில் இருந்து நீக்கப்பட்ட திலீப்ராய்க்கு வாக்களித் தது, இந்த
கட்சிக்கு விழுந்த பலத்த அடியாக கருதப்படுகிறது. </p><p>
</p><p>
சொரண் வெற்றி </p><p>
</p><p>
ஜார்கண்ட் மாநிலத்தில் ஜார்கண்ட் முக்தி மோர்ச்சா தலைவர் சிப சொரண், ஆளும் பா.ஜ.
வேட்பாளர் அஜய் மரோ ஆகியோரும் வென்றனர். காங்கிரஸ்-ரா.ஜனதாதளம் வேட்பாளர் அகமது
தோல்வி அடைந்தார். </p><p>
</p><p>
மம்தா கட்சிக்கு அடி </p><p>
</p><p>
மேற்கு வங்காளத்தில் திரிணாமுல் காங்கிரஸ் வேட்பாளர் தினேஷ் திரிவேதி வெற்றி பெற்ற
போதிலும், இந்த கட்சியின் எம்.எல்.ஏ.க்கள் 4 பேர் கட்சி மாறி காங்கிரஸ் வேட்பாளர்
அர்ஜுன் செங்குப்தாவுக்கு வாக்களித்துள்ளனர். இது இந்த கட்சிக்கு விழுந்த பலத்த அடியாக
கருதப்படுகிறது. </p><p>
</p><p>
30 எம்.எல்.ஏ.க்கள் கொண்ட காங்கிரசுக்கு ஆதரவாக 33 ஓட்டுகள் கிடைத்தது. ஒரு காங்கிரஸ்
எம்.எல்.ஏ. வாக்களிக்கவில்லை. மே.வங்கத்தில் ஆளும் இடது சாரி வேட்பாளர்கள் 4 பேரும்
வெற்றி பெற்றார்கள். </p><p>
</p><p>
மேகாலயாவில் ஒரே ஒரு இடத்துக்கு நடந்த தேர்தலில் தேசியவாத காங்கிரஸ் வேட் பாளர்
ராபர்ட் கர்சிங் வெற்றி பெற்றார். காங்கிரஸ் எம்.எல்.ஏ.க்கள் 2 பேர் இவருக்கு கட்சி
மாறி வாக்களித்தனர். </p><p>
</p><p>
மாநிலங்களவைக்கு நேற்று 8 மாநிலங்களில் இருந்து மொத்தம் 25 பேர் தேர்ந்து
எடுக்கப்பட்டனர். </p>

<p> </p>

<p> </p>

<p>மந்திரி பொன்னையன்
முன்னிலையில் வாஜ்பாய் கொடும்பாவி கொளுத்தியது, அ.தி.மு.க. நட்புக்கு அடையாளமா?
தமிழக பா.ஜ.வுக்கு கருணாநிதி கேள்வி</p>

<p> </p>

<p>சென்னை, மார்ச்.28- மந்திரி பொன்னையன் முன்னிலையில் வாஜ்பாய்
கொடும் பாவி கொளுத்தியது. அ.தி.மு.க. நட்புக்கு அடையாளமா என்று தமிழக பாரதிய ஜனதாவுக்கு
கருணாநிதி கேள்வி விடுத்தார். </p><p>
</p><p>
தி.மு.க. தலைவர் கருணாநிதி நேற்று மாலையில் அண்ணா அறிவாலயத்தில் நிரு பர்களுக்கு பேட்டி
அளித்தார். அதன் விவரம் வருமாறு:- </p><p>
</p><p>
கேள்வி:- பா.ஜ.க. தலைவர் கிருபாநிதி கூறும்போது தி.மு.க. வின் முடிவு துரதிர்ஷ்ட வசமானது
என்றும் நீங்கள் எடுத்த முடிவை மதிக்கிறோம் என்றும் சொல்லியிருக்கிறாரே? </p><p>
</p><p>
பதில்:- ஒருவரையொருவர் மதித்துக்கொள்வது என்பது அரசியலில் தேவையான ஒரு நாகரிகம். </p><p>
</p><p>
கொடும்பாவி கொளுத்திய நட்பு </p><p>
</p><p>
கேள்வி:- தி.மு.க. முடிவை மேற்கொள்வதற்கு முன்பு அவர்களோடு பேசியிருந்தால் வேறுபாடுகளை
களைந்திருக்கலாம் என்று சொல்லியிருக்கிறார்களே? </p><p>
</p><p>
பதில்:- அ.தி.மு.க.வோடு நட்பாக இருப்போம் என்று அவர்கள் தான் சொன்னார்கள். நாலு
மாதங்களுக்கு முன்புதான் பிரதமர் வாஜ்பாயின் கொடும்பாவியை அமைச்சர் பொன்னையன்
முன்னிலையில் எரித்தார்கள். உச்சநீதி மன்றத்திலே ஜெயலலிதாவிற்கு எதிராக வந்த
தீர்ப்பினையொட்டி, நீதி மன்றத்தையும் பிரதமரையும் அவமதிக்கும் வகையில் ''இது
சட்டத்தின் தீர்ப்பா? சதிகாரர்களின் தீர்ப்பா?'' என்று பொன்னையன் கோஷம்
போட்டது அப்போதைய ஏடுகளில் வௌி வந்தது. அப்படி பிரதமரின் கொடும்பாவியை கொளுத்தி
நான்கு மாதங்கள்கூட ஆகவில்லை. அவர்களிடம் தங்களுக்கு நட்பு என்று சொன்னால் என்ன
காரணம்? வாஜ்பாய் கொடும்பாவியை கொளுத்துவது தான் நட்புக்கு அடையாளம் என்று தமிழ்நாடு
பா.ஜ.க. கருதுகிறதா? </p><p>
</p><p>
பொடோ சட்டம் </p><p>
</p><p>
கேள்வி:- பிரச்சினையின் அடிப்படையில்தான் அ.தி. மு.க.வை ஆதரிப்பதாக கூறு கிறார்களே?
டெல்லியிலிருந்து யாராவது பா.ஜ.க. தலைவர்கள் தொடர்பு கொண்டார்களா? </p><p>
</p><p>
பதில்:- இதைப்பற்றி நான் விரிவாக பேச விரும்பவில்லை. </p><p>
</p><p>
கேள்வி:- நேற்று பாராளுமன்ற கூட்டுக்கூட்டத்தில் பொடோ சட்டம் நிறைவேறி யிருக்கின்றது.
அதிலே பேசிய பலர் சிறுபான்மை சமுதாயத்திற்கு எதிராக அது பயன்படுத்தப்படும் என்று
பேசியிருக்கிறார்களே? </p><p>
</p><p>
பதில்:- அப்படி பயன்படுத்த மாட்டோம் என்று உறுதி அளித்திருக்கிறார்கள்.
எதிர்க்கட்சிகள் சார்பில் சொன்ன திருத்தங்களும் ஏற்றுக்கொள்ளப்பட்டுள்ளன. </p><p>
</p><p>
பஸ் உரிமையாளரிடம் பணம் </p><p>
</p><p>
கேள்வி:- தமிழக அரசு பேருந்துகளை தேசிய மயமாக்குவது பற்றி தமிழ்நாடு காங்கிரஸ் கட்சி
தலைவர் இளங்கோவன் கூறும்போது பேருந்து உரிமையாளர்களிடம் பணம் பெற்றதற்கு ஆதாரம்
இருப்பதாக குற்றம்சாட்டியும் அரசு தரப்பில் ஒன்றும் பதில் இல்லையே? </p><p>
</p><p>
பதில்:- ஆமாம், அவர் பகிரங்கமாக குற்றம்சாட்டியிருக்கிறார். நானும் அதை பற்றி
கேள்விப்பட்டேன். </p><p>
</p><p>
கேள்வி:- அன்னதான திட்டத்தோடு சேர்த்து கோவில்களில் ஆன்மீக வகுப்புகள் நடத்துவதை
பற்றி உங்கள் கருத்து? </p><p>
</p><p>
பதில்:- ஆன்மீக வகுப்புகள் மதப்போதனை என்ற அளவிற்கு கொண்டு போகப்படும் என்ற
அச்சத்தை அ.தி.மு.க.வின் ஆதரவாளராக இருக்கின்ற நண்பர் வீரமணி தெரி வித்திருக்கிறார். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p> </p>

<p>தே.ஜ. கூட்டணியில் அங்கம்
வகிப்பதற்கு பாராட்டு: ''நட்பு தொடரும்'' - கருணாநிதி முடிவை ஏற்றுக்கொள்கிறோம், தமிழக
பா.ஜ. அறிவிப்பு</p>

<p> </p>

<p>சென்னை, மார்ச் 28- நட்பு தொடரும் என்ற கருணாநிதியின் முடிவை
ஏற்பதாகவும், மத்தியில் தே.ஜ. கூட்டணியில் அங்கம் வகிப்பதை பாராட்டுவதாகவும் தமிழக
பா.ஜ. மையக்குழுவில் அறிவிக்கப்பட்டுள்ளது. </p><p>
</p><p>
தமிழகத்தில் பா.ஜ.வுடன் நட்பாக இருப்போம், மத்தியில் உறவு தொடரும் என்று தி.மு.க.
தலைவர் கருணாநிதி அறிவித்தார் அல்லவா? இது பற்றி தமிழக பா.ஜ. தலைவர்களிடம்
விசாரிப்பதற்காக அகில இந்திய பாரதிய ஜனதா தலைவர் ஜனா கிருஷ்ணமூர்த்தி சென்னை
வந்தார். அவர் மாநில தலைவர் டாக்டர் கிருபாநிதி, பொதுச் செயலாளர் இல.கணேசன்
ஆகியோரிடம் பேசினார். இதையடுத்து பா.ஜ. மையக்குழு கூட்டம் கட்சி அலுவலகமான கமலாலயத்தில்
நேற்று காலை 12 மணி அளவில் நடந்தது. </p><p>
</p><p>
இந்த கூட்டத்துக்கு மாநில தலைவர் கிருபாநிதி தலைமை வகித்தார். இதில் அகில இந்திய
துணைத்தலைவர் ராமாராவ், பொதுச்செயலாளர் சஞ்சய் ஜோசி, பொருளாளர் சுகுமாரன்
நம்பியார், சட்டமன்ற குழு தலைவர் கே.என்.லட்சுமணன், மத்திய மந்திரி பொன்.ராதா
கிருஷ்ணன், சி.பி.ராதா கிருஷ்ணன், மாநில பொதுச் செயலாளர்கள் இல.கணேசன், எச்.ராஜா
உள்பட பலர் கலந்து கொண்டனர். </p><p>
</p><p>
இதில் தி.மு.க. கூட்டணி குறித்து ஆலோசிக்கப்பட்டது. அது முடிந்ததும் மாலையில் டாக்டர்
கிருபாநிதி நிருபர்களுக்கு பேட்டி அளித்தார். அப்போது அவர் கூறியதாவது:- </p><p>
</p><p>
கடந்த ஒரு வாரமாக சில நிகழ்வுகள் தமிழகத்தில் பரபரப்பாக எழுதப்பட்டும், பேசப்பட்டும்
வருகிறது. எங்களது தலைவர் ஜனாகிருஷ்ண மூர்த்தியுடன் எல்லா விசயங்களையும் நன்கு ஆராய்ந்து
ஒரு முடிவுக்கு வந்துள்ளோம். அந்த முடிவை உங்களுக்கு தெரிவிக்கிறேன். </p><p>
</p><p>
துரதிர்ஷ்டம் </p><p>
</p><p>
(ஆங்கிலத்தில் உள்ள அறிக்கையை தமிழிலும் மொழி பெயர்த்து படித்தார்) </p><p>
</p><p>
அ.தி.மு.க. பா.ஜ. கூட்டணியில் இருந்து சென்ற பிறகு தி.மு.க. தேசிய ஜனநாயக கூட்டணியில்
அங்கம் வகித்தது. அந்த ஏற்பாட்டின்படி தொடர்ந்து செயல்பட்டு வந்தோம். கடந்த சட்டமன்ற
தேர்தலில் தி.மு.க.வுடன் சேர்ந்து பா.ஜ. போட்டியிட்டது. ஆனால் துரதிர்ஷ்டவசமாக தேர்தல்
முடிவு எங்கள் எதிர்பார்ப்புக்கு தகுந்தாற்போல இல்லை. </p><p>
</p><p>
சிறைக்கு போனோம் </p><p>
</p><p>
இருந்தாலும் உண்மையான ஜனநாயகத்தை பின்பற்றும் நாங்கள் மக்கள் முடிவை மதித்து அப்படியே
நடக்க ஆரம்பித்தோம். எங்கள் தேசிய தலைவர்கள் வழிகாட்டுதலுக்கு இணங்க பா.ஜ.க.
எங்கெங்கு எதிர்க்கட்சியாக இருக்கிறதோ அங்கெல்லாம் தரமான எதிர்க் கட்சியாக விளங்கி
வந்துள்ளது. </p><p>
</p><p>
அதே சமயம் எவ்வப்போது தி.மு.க. எங்கள் உதவியை நாடியதோ அப்போதெல்லாம் பா.ஜ. தனது
உதவியை செய்து கொண்டுதான் வந்தது. தி.மு.க. தலைவர் கருணாநிதி யும், மத்திய அமைச்சர்கள்
2பேரும் கைது செய்யப்பட்டபோது இம்மாநில தலைமை யும், மத்திய தலைமையும், அரசும் தி.மு.க.
வுக்கு ஓடோடி உதவி செய்தது. </p><p>
</p><p>
எங்கள் மாநில தலைவர் கிருபாநிதி, சட்டமன்ற குழு தலைவர் கே.என்.லட்சுமணன் மற்றும் 6
ஆயிரம் தொண்டர் களும் கைது செய்யப்பட்டு சிறை சென்றனர். ஆனால் இப்போது தி.மு.க.
துரதிர்ஷ்டவசமாக மாநில பா.ஜ. உறவை முறித்துக் கொண்டதாக தன்னிச்சையாக அறிவித்துள்ளது. </p><p>
</p><p>
அரசியல் நாகரீகம் இல்லை </p><p>
</p><p>
அரசியல் நாகரீகத்தின்படி தி.மு.க. தலைவர்கள் எங்கள் மாநில தலைவர்களை அழைத் துப் பேசி
ஏதாவது உடன்படாத விசயம் இருந்தால் அதை பேசித்தீர்த்துக் கொண்டிரு க்கலாம். ஆனால்
அடிப்படை அரசியல் நாகரீகம் கூட பா.ஜ.வுக்கு அவர்கள் அளிக்க வில்லை. இதுவரை தி.மு.க.
வுடன் எங்களுக்கு நல்ல உறவு தான் இருந்தது. ஆனால் தற்போது உறவை முறித்துக் கொண்டு நட்பு
மட்டும் இருக்கும் என்றும் மத்தியில் மட்டும் உறவு இருப்பதாக தி.மு.க. கூறி இருக்கிறது.
நாங்கள் அந்த முடிவை மதிக்கிறோம். எனினும் தி.மு.க. தொடர்ந்து தே.ஜ. கூட்டணியில்
அங்கம் வகிப்பதை நாங்கள் போற்றுகிறோம். வரவேற்கிறோம். </p><p>
</p><p>
இவ்வாறு முடிவு எடுக்கப் பட்டுள்ளது. </p><p>
</p><p>
தி.மு.க. - அ.தி.மு.க.வுடன் பா.ஜ. இரட்டை சவாரியா? கிருபாநிதி பதில் டாக்டர்
கிருபாநிதி அந்த முடிவுகளை படித்து முடித்த பிறகு நிருபர்கள் கேள்விகளுக்கு பதில் அளித்தார்.
அது வருமாறு:- </p><p>
</p><p>
கேள்வி:- நீங்கள் அ.தி.மு.க.வுக்கு ஆதரவாக செயல்படுவதால்தான் உறவை முறித்ததாக கருணாநிதி
கூறி உள்ளாரே? </p><p>
</p><p>
பதில்:- நாங்கள் எப்போதும் நடுநிலையோடுதான் இருக்கிறோம். </p><p>
</p><p>
கேள்வி:- சிறுபான்மையினருக்கு எதிராக ஆர்.எஸ். எஸ். கருத்துக்கு ஆதரவளிப் பதாகவும்
கருணாநிதி குற்றம்சாட்டி உள்ளாரே? </p><p>
</p><p>
பதில்:- சிறுபான்மையினருக்கு எதிராக எந்த முடிவும் நாங்கள் எடுக்கவில்லை. </p><p>
</p><p>
சிக்னலுக்கு... </p><p>
</p><p>
கேள்வி:- ஜெயலலிதா முதல்வராக வந்த பிறகு நீங்கள் அ.தி.மு.க.வுடன் மிருதுவாக நடந்து
கொள்கிறீர்களே? </p><p>
</p><p>
பதில்:- அப்படியில்லை. அவர்கள் பிரச்சினை அடிப்படையில் ஆதரவு அளிப்பதாக கூறினார்கள்.
அதே போல நாங்களும் பிரச்சினை அடிப்படையில் ஆதரவு அளிப்பதாக கூறி உள்ளோம். </p><p>
</p><p>
கேள்வி:- மாநில பா.ஜ.வுடன் தி.மு.க. உறவை முறித்தது உங்களுக்கு வருத்தம் அளிக்கிறதா? </p><p>
</p><p>
பதில்:- துரதிர்ஷ்டவசம் என்பது யாருக்கும் வருத்தம் அளிக்காமல் இருக்காது. </p><p>
</p><p>
கேள்வி:- அ.தி.மு.க. சிக்னலுக்கு காத்திருக்கிறீர்களா? </p><p>
</p><p>
பதில்:- யாருடைய சிக்னலுக்கும் நாங்கள் காத்திருக்கவில்லை. </p><p>
</p><p>
இரட்டை சவாரி </p><p>
</p><p>
கேள்வி:- உங்கள் அணியில் ஜெயலலிதா இருந்தபோது ஒவ்வொரு முறை மிரட்டல் விட்டபோதும்
ஜார்ஜ் பெர்னாண்டஸ், பிரமோத் மஹாஜன் ஆகியோரை அனுப்பி சமாதானம்
செய்திருக்கிறீர்கள். அந்த வகையில் தி.மு.க.வுடன் பேசாதது ஏன்? </p><p>
</p><p>
பதில்:- நாங்கள் அவர்களுடைய முடிவை மதிக்கிறோம். ஏற்கனவே உறவை முறித்துக் கொண்டோம்
என்று கூறிய பிறகு பேசுவதற்கு என்ன இருக்கிறது? </p><p>
</p><p>
கேள்வி:- நீங்கள் இரட்டை சவாரி செய்வதாக கருணாநிதி கூறி உள்ளாரே? </p><p>
</p><p>
பதில்:- கலைஞர் தமிழில் பாண்டித்தியம் மிக்கவர். ஆனால் நாங்கள் இரட்டை சவாரி
செய்யவில்லை. எங்கள் கடமையை செய்கிறோம். </p><p>
</p><p>
4 மாதத்துக்கு முன் நடந்தது </p><p>
</p><p>
கேள்வி:- 4 மாதத்துக்கு முன் பிரதமர் வாஜ்பாய் கொடும் பாவியை அ.தி.மு.க. மந்திரி
பொன்னையன் எரித்தது பற்றி கருணாநிதி கூறி இருக்கிறாரே? </p><p>
</p><p>
பதில்:- அது 4 மாதத்துக்கு முன் நடந்தது. இப்போது நடப்பதை பார்க்கவேண்டும். </p><p>
</p><p>
கேள்வி:- தமிழக தே.ஜ. கூட்டணியில் அ.தி.மு.க. சேருமா? </p><p>
</p><p>
பதில்:- தமிழகத்தில் தே.ஜ. கூட்டணி இல்லை. மத்தியில் மட்டும்தான் இருக்கிறது. </p><p>
</p><p>
கேள்வி:- எதிர்காலத்தில் அ.தி.மு.க. கூட்டணியில் சேரும் வாய்ப்பு உள்ளதா? </p><p>
</p><p>
பதில்:- அதை இப்போது சொல்ல முடியாது. எங்கள் கட்சியை பலப்படுத்தும் முயற்சியை நாங்கள்
செய்வோம். </p><p>
</p><p>
யாருக்கு ஆதரவு? </p><p>
</p><p>
கேள்வி:- முந்தைய 13 மாத கால ஆட்சியில் ஜெயலலிதா கொடுத்த தொல்லைகளை இல.கணேசன்
''பட்டது போதும் பெண்ணாலே'' என்று பாடினாரே? மீண்டும் அந்த தொல்லைக்கு தயாராகி
விட்டீர்களா? </p><p>
</p><p>
பதில்:- அது அப்போது நடந்தது. </p><p>
</p><p>
கேள்வி:- இப்போது அவர் மாறிவிட்டாரா? </p><p>
</p><p>
பதில்:- அதை அவரிடம் தான் கேட்க வேண்டும். </p><p>
</p><p>
கேள்வி:- தமிழகத்தில் 3 தொகுதி இடைத்தேர்தலில் உங்கள் நிலை என்ன? </p><p>
</p><p>
பதில்:- அந்த இடைத் தேர்தலில் பங்கேற்பதில்லை என்று ஏற்கனவே முடிவு செய்துள்ளோம். </p><p>
</p><p>
கேள்வி:- அதில் யாருக்கு ஆதரவு கொடுப்பீர்கள்? </p><p>
</p><p>
பதில்:- அதை அப்போது முடிவு செய்வோம். </p><p>
</p><p>
நெல்லையில் பொதுக்குழு </p><p>
</p><p>
கேள்வி:- தமிழக பா.ஜ. பொதுக்குழு, செயற்குழு எப்போது? </p><p>
</p><p>
பதில்:- வருகிற ஏப்ரல் 20, 21 ஆகிய தேதிகளில் நெல்லை யில் நடக்கும். </p><p>
</p><p>
கேள்வி:- ஏற்கனவே அ.தி.மு.க. உங்கள் கூட்டணியில் இருந்தபோது நடந்தவற்றை குறிப்பிட்டு
உங்களுக்கு கருணாநிதி எச்சரிக்கை தந்துள்ளாரே? </p><p>
</p><p>
பதில்:- இது மாதிரி எச்சரிக்கை தேவையில்லை. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p> </p>

<p>ருசி கண்ட பூனை - ஜெ.,
மீண்டும் கொள்ளை அடிக்க வந்து விட்டார், முன்னாள் அமைச்சர் க.சுந்தரம் கடும் தாக்கு</p>

<p> </p>

<p>பள்ளிப்பட்டு ,மார்ச் 28- ருசி கண்ட பூனை ஜெயலலிதா மீண்டும்
தமிழகத்தை கொள்ளை அடிக்க வந்து விட்டார் என்று முன்னாள் அமைச்சர் க.சுந்தரம் கூறினார். </p><p>
</p><p>
பள்ளிப்பட்டு ஒன்றிய தி.மு.க., மற்றும் இளைஞர் அணி சார்பில் சென்னை மேயர் மு.க.
ஸ்டாலின் 50 வது பிறந்த நாள் விழாவும் அ.தி.மு.க. அரசின் விலைவாசி உயர்வை கண்டித்தும்
கொளத்தூரில் மாபெரும் கூட்டம் நடந்தது. ஒன்றிய செயலாளர் பி.ரவீந்திர ரெட்டி தலைமை
தாங்கினார். ஏ.சோமு, பி.எம்.ரவி வரவேற்றனர். மாவட்ட இளைஞர் அணி அமைப்பாளர்
எஸ்.சந்திரன், ஒன்றிய இளைஞர் அணி அமைப்பா ளர் டி.சந்திரன், துணை அமைப்பாளர்
சீனிவாசன் முன்னிலை வகித்தனர். </p><p>
</p><p>
சுந்தரம் சிறப்புரை </p><p>
</p><p>
இதில் தி.மு.க. துணைப் பொதுச்செயலாளரும் முன்னாள் அமைச்சருமான க.சுந்தரம் பேசியதாவது:- </p><p>
</p><p>
தி.மு.கழகத்தை பொறுத்த வரை ஆட்சியில் இருந்தாலும் இல்லாவிட்டாலும் மக்கள் செய்ய வேண்டிய
பணிகளை தொடர்ந்து செய்யும். கலைஞர் ஆட்சியில் இருக்கும் போது வௌிமாநில முதல்வர்
எல்லாம் பாராட்டக்கூடிய வகையில் தமிழகத்தை முன்னோடி மாநிலமாக திகழச்செய்தார். </p><p>
</p><p>
கடந்த 91முதல் 96ம் ஆண்டு வரை ஜெயலலிதா ஆட்சி பொறுப்பில் இருந்த போது தமிழ்நாட்டை
சுடுகாடாக மாற்றி இருந்தார். பிறகு 96ல் கழக ஆட்சி அமைந்து தலை வர் கலைஞர் முதல்வர்
ஆனதும், அ.தி.மு.க.வினர் செய்த சீர்கேட்டை எல்லாம் ஒழுங்்கு படுத்தினார். இப்போது
மீண்டும் அ.தி.மு.க. அரசு பொறுப்பு ஏற்று உள்ளது. திரும்பவும் தமிழ்நாடு சீர்கெடும்.
மீண்டும் தி.மு.க. ஆட்சிக்கு வந்து தலைவர் கலைஞர் 5 வது முறையாக முதல்வர் பதவி ஏற்று
அந்த சீர்கேட்டை எல்லாம் சரி செய்வார். </p><p>
</p><p>
ருசி கண்ட பூனை </p><p>
</p><p>
ஜெயலலிதா ருசி கண்ட பூனை. அவர் மீண்டும் தமிழ்நாட்டை கொள்ளை அடிக்க வந்து விட்டார்.
பொது மக்களின் அத்தியாவசிய தேவைகளான மின்சார கட்டணம் உயர்வு, போக்குவரத்து கட்டண
உயர்வு போன்றவற்றை அமுல்படுத்தி மக்கள் வாழ முடியாத சூழ்நிலைக்கு தள்ளி விட்டது. நாட்டு
மக்களின் நன்மைக்காக போடப்பட்ட பட்ஜெட் அல்ல இது. மக்கள் பணத்தை கொள்ளை
அடிப்பதற்காக போடப்பட்ட பட்ஜெட். </p><p>
</p><p>
நாங்கள் தவறு செய்தோம் என்று நியாயமான முறையில் உங்களால் சுட்டிக்காட்ட முடியாது.
அ.தி.மு.க. அரசின் தவறுகளை நாங்கள் சுட்டிக்காட்ட தயங்க மாட்டோம். நீதிமன்றங்கள்
ஜெயலலிதாவை நிரபராதி என்று விடுவித்தாலும் தமிழ்நாட்டு மக்களை ஜெயலலிதா ஏமாற்றினாலும்
உங்களுக்கு எல்லாம் காலம் நிச்சயம் பதில் சொல்லும். </p><p>
</p><p>
இவ்வாறு அவர் பேசினார். </p><p>
</p><p>
இ.ஏ.பி.சிவாஜி </p><p>
</p><p>
கூட்டத்தில் மாவட்ட செயலாளர் இ.ஏ.பி.சிவாஜி, புலவர் ஆடலரசு, ஆவடி நாசர், பி.வடிவேல்,
பூ.கன்னியப்பன், ஜெ.ஜெ.ரவி, வி.எஸ்.ரமேஷ், கமலக்கண்ணன்,திருமலை லோகநாதன்,
ஏ.சுப்பிரமணி, ஏ.நாகலிங்கம், கோவர்த்தனம், எஸ்.கே.சுப்பிரமணி, சின்ன சாமி ராஜி,
கே.என். ஜெய ராமன், சி.ஜே.செந்தில்குமார், நசீர், சேகர், மாணிக்கம்,மோக னாமுத்து,
வெங்கட பெருமாள், எஸ். ஜெயச்சந்திரன், பி.கே. சீனிவாசன், கே.ஜி.ராஜே ந்திரன்,
கே.கே.சந்திர சேகர், எஸ்.எல். நடராசன், பி.எம்.கண்ணையா, பெங்களூர் வெங்கடேசன்,
எஸ்.ஜோதி,எம். ரவி மற்றும் பலர் கலந்து கொண்டனர். முடிவில் பி.ஏ.வெங்கடேசன் நன்றி
கூறினார். </p>

<p> </p>






</body></text></cesDoc>