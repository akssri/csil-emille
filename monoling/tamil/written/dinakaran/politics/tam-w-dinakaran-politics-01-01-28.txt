<cesDoc id="tam-w-dinakaran-politics-01-01-28" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-politics-01-01-28.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-01-28</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-01-28</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>மகளிர் இடஒதுக்கீடு
மசோதாவை நிறைவேற்றியே தீருவோம்: உலக பத்திரிகையாளர் கருத்தரங்கில் வாஜ்பாய் பேச்சு</p>

<p> </p>

<p>புதுடெல்லி, ஜன.28- சர்வதேச பத்திரிகையாளர் கழகம் டெல்லியில்
3 நாள் கருத்தரங்கம் நடத்தியது. 100 நாடுகளைச் சேர்ந்த பிரதிநிதிகள் அதில்
பங்கேற்றார்கள். பிரதமர் வாஜ்பாய் அதில் தொடக்க உரை ஆற்றினார். அவர் பேசியதாவது:- </p><p>
</p><p>
பத்திரிகைகளுக்கு மிகப் பெரிய சமூக பொறுப்பு இருக்கிறது. அரசியலை பொறுத்த வரை பத்திரிகைகள்
நடுநிலை தன்மையை கடைபிடிக்க வேண்டும். நானும் பத்திரிகை யாளராக வாழ்வை
தொடங்கியவன்தான். </p><p>
</p><p>
பத்திரிகையில் வௌியாகும் செய்திகள் சந்தைப்பொருட்கள் அல்ல. எப்படியாவது நல்ல விலைக்கு
விற்று லாபம் தேட வேண்டும் என்ற எண்ணத்தில் செய்திகளை வௌியிடக் கூடாது. </p><p>
</p><p>
மகளிர் மசோதா </p><p>
</p><p>
மக்களவையிலும், மாநில சட்டமன்றங்களிலும் பெண் களுக்கு இடஒதுக்கீடு வழங்கும் மசோதா
விரைவில் நிறை வேற்றப்படும். மிக விரைவில் இந்த மசோதாவை நாங்கள் நிறைவேற்றுவோம். </p><p>
</p><p>
தற்போது மனித நலனை மறந்து விட்டு பொருளாதார முன்னேற்ற போட்டா போட்டி நடக்கிறது.
அறிவியல் மற்றும் தொழில்நுட்ப முன்னேற்றத் தின் பயன் மனிதர்களுக்குத் தான் போய்ச் சேர
வேண்டும். அவர்களது நலனை மறந்து விட்டு போட்டா போட்டி போட்டு என்ன பயன்? </p><p>
</p><p>
கூட்டணி ஆட்சி </p><p>
</p><p>
உலகின் மிகச்சிறந்த ஜன நாயகம் இந்தியாவில் நிலவு கிறது. இந்திய ஜனநாயகத்தில் அனைத்து
சமுதாயப் பிரிவினருக்கும் பிரதிநிதித்துவம் உள்ளது. தற்போதுள்ள நிலை யில் கூட்டணி
அரசுகள்தான் நிலையான ஆட்சியைத் தருகின்றன. கூட்டணி அரசுகள் தான் வெற்றிகரமான அரசாக
விளங்குகின்றன. </p><p>
</p><p>
வேற்றுமையில் ஒற்றுமை </p><p>
</p><p>
எங்கள் நாடு பல்தேசிய பல மதம்பல மொழிகள் கொண்ட நாடு ஆகும். இங்கு வேற்றுமையில் ஒற்றுமை
காண்கின்றோம். சிறுபான்மையினருக்கு இங்கு போதிய பாது காப்பு இருக்கிறது. பூமி உலக
மயமாகி வரும் இந்த தருணத் தில் இந்தியாவின் வேற்றுமை யில் ஒற்றுமை சிந்தாந்தம் அனைத்து
நாடுகளுக்கும் ஒரு எடுத்துக்காட்டு ஆகும். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>தனி ஊர்வலம் -
ஆர்ப்பாட்டம் நடத்த தடை: அதிருப்தி தலைவர்களுக்கு காங். மேலிடம் எச்சரிக்கை: தலைமையை
தாக்கி அறிக்கை விட்டால் ஒழுங்கு நடவடிக்கை</p>

<p> </p>

<p>ரோதாக், ஜன. 28- காங்கிரஸ் கட்சியில் மாநி லத்துக்கு மாநிலம்
அதிருப்தி கோஷ்டி உள்ளது. கட்சியின் மாநிலத் தலைவரை எதிர்த்து இந்த கோஷ்டியினர்
அறிக்கை விடுவதும், பேட்டி அளிப்பதும் வாடிக்கையானது. தனித் தனியாக போராட்டமும் நடத்து
வார்கள். </p><p>
</p><p>
அரியானாவில்.. </p><p>
</p><p>
அரியானா மாநிலத்தில் மாநில கமிட்டி தலைவராக இருக்கும் பூபிந்தர் சிங் ஹுடா என்பவரை
எதிர்த்து அரி யானா முன்னாள் முதல்வர் பஜன்லால் போர்க் கொடி உயர்த்தி உள்ளார். இவர்
அரியானாவில் ஓம் பிரகாஷ் சவுதாலாவின் கொள்கை களை எதிர்த்து தனியாக போராட்டம்
நடத்தப் போவ தாக அறிவித்து இருந் தார். </p><p>
</p><p>
மேலிடத்துக்கு கடிதம் </p><p>
</p><p>
இதுகுறித்து மாநில கட்சித் தலைவர் ஹுடா கட்சியின் அகில இந்திய பொதுச் செயலாளர்
எம்.எல். வோரா வுக்கு கடிதம் எழுதியிருந்தார். இதற்கு கட்சி மேலிடம் பதில் அளித்து
உள்ளது. இது குறித்து மேலிட வட்டாரம் கூறியதாக யு.என்.ஐ. நேற்று வௌியிட்ட செய்தியில்
கூறியிருப்பது வரு மாறு:- </p><p>
</p><p>
தலைவர்களுக்கு தடை </p><p>
</p><p>
கட்சியில் ஒழுங்கை மீறும் தொண்டர்கள் மீது கடும் நடவடிக்கை எடுக்க வேண்டும் என்று மாநில
கமிட்டித் தலைவர்களுக்கு மேலிடம் உத்தரவிட்டு உள்ளது. தலை வர்களையும், கட்சிக் கொள்கை
களையும் எதிர்த்து அறிக்கை விடுவதற்கு தடை விதிக்கப் படும். மாநில அளவில் நடைபெறும்
எல்லா பேரணி மற்றும் ஆர்ப்பாட்டங்கள் அனைத்தும் மாநில கமிட்டி யின் பெயரில் தான்
நடைபெற வேண்டும். </p><p>
</p><p>
இவ்வாறு கட்சி மேலிடம் உத்தரவிட்டு இருப்பதாக அந்த செய்தியில் கூறப்பட்டு உள்ளது. </p>

<p> </p>

<p>சோனியா அவசர அழைப்பு:
தங்கபாலு மீது நடவடிக்கையா? டெல்லி சென்றார்</p>

<p> </p>

<p>சென்னை, ஜன. 28- தமிழ்நாடு காங்கிரஸ் தலைவர் ஈ.வி.கே.எஸ்.
இளங்கோவனை எதிர்த்து முன்னாள் தலைவர் தங்கபாலு தொடர்ந்து குரல் கொடுத்து வருகிறார்
அல்லவா? இது பற்றி இளங்கோவன் மேலிடத்தில் புகார் செய்தார். இதையடுத்து உடனடியாக
டெல்லி வரும்படி தங்கபாலுவுக்கு சோனியாவிடம் இருந்து அவசர தகவல் வந்தது. இதனால் மதுரையில்
இருந்து காரிலேயே சென்னை வந்த தங்கபாலு நேற்று காலை விமானத்தில் டெல்லி பிறப்பட்டு
சென்றார். </p><p>
</p><p>
அங்கு சோனியாவை சந்திக்கிறார். அப்போது மோதல் போக்கை கை விடும்படி அவரிடம் சோனியா
கூறுவார் என்றும், ஒழுங்கு நடவடிக்கை எடுப்பார் என்றும் கூறப்படுகிறது. </p>

<p> </p>

<p>அ.தி.மு.க.- த.மா.கா.
இடைவௌி குறைகிறது: ஜெ.- மூப்பனார் திரைமறைவு பேச்சுவார்த்தையின் பின்னணி: பரபரப்பான
தகவல்கள்</p>

<p> </p>

<p>சென்னை, ஜன.28- அ.தி.மு.க.வும் த.மா.கா.வும் மத சார்பற்ற
அணியில் ஒன்றாக இருக்கிறோம் என்று கூறி கொண்டிருந்தாலும் கூட்டணி விஷயத்தில் இரு கட்சி
களுக்குமிடையே மிகுந்த இடை வௌி இருந்து வந்தது. இரண்டு கட்சிகளின் சார்பிலும் மாறி மாறி
வௌியிடப்பட்டு வந்த கருத்துக்கள் கூட்டணியில் இந்த இரண்டு கட்சிகளும் ஒன்றாக இடம்பெறுமா
என்ற சந்தேகத்தை ஏற்படுத்தி இருந்தது. </p><p>
</p><p>
கடைசியாக இம்மாதம் இரண்டாம் வாரத்தில் கூடிய த.மா.கா. செயற்குழு கூட்டத்தில் அதன் அரசியல்
நிலைப்பாடு என்ன என்பதை மூப்பனார் அவரது உரையின் மூலம் வௌியிட்டிருந்தார். கூட்டணி
ஆட்சிதான் இனி ஏற்படும் என்றும் கூட்டணியில் இடம் பெறும் கட்சிகள் குறைந்தபட்ச செயல்
திட்டத்தின் அடிப்படை யில் தேர்தலை சந்திக்க வேண் டும் என்றும் அறிவித்தார். அவரது
இந்த அறிவிப்பு ஜெய லலிதாவுக்கு கோபத்தை ஏற் படுத்தியது. இது இம்மாதம் 17-ந் தேதி
ஜெயலலிதா அளித்த பேட்டியில் வௌிப்பட்டது. </p><p>
</p><p>
ஜெ. கோபம் </p><p>
</p><p>
அ.தி.மு.க.வை பொறுத்த வரையில் கூட்டணி ஆட்சி என்ற நிலையை ஏற்று கொள்ள மாட்டோம் என்று
அறிவித்த துடன் அ.தி.மு.க. கூட்டணியில் த.மா.கா. இடம்பெறுமா? இல்லையா? என்பதை
மூப்பனாரிடம் கேளுங்கள் என்று கூறி தனது கோபத்தை வௌிப்படுத்தினார். </p><p>
</p><p>
ஜெயலலிதாவின் இந்த கருத்து த.மா.கா. தரப்பில் பரபரப்பையும், அதிருப்தியை யும்
ஏற்படுத்தியது. ஏற்கனவே காங்கிரஸ் உள்ளிட்ட கட்சிகளை சேர்த்து கொண்டு அ.தி.மு.க. வுடன்
தொகுதி உடன்பாட்டு பேச்சுவார்த்தையை தொடங்க லாம் என்ற திட்டத்தை ஜெய லலிதா
தந்திரமாக முறியடித்து விட்டதால் திகைத்திருந்த த.மா.கா. இந்த அறிவிப்புக்கு பின்னர்
விழித்து கொண்டது. </p><p>
</p><p>
3-வது அணி </p><p>
</p><p>
த.மா.கா. தனியாக இல்லை என்பதை உணரவைப்பதற் கான முயற்சியில் இறங்கியது. அதன்
விளைவுதான் புதிய நீதிக் கட்சி தலைவர் ஏ.சி.சண்முகம் மூப்பனாரை சந்தித்தார்.
த.மா.கா. தலைமையில் மூன்றா வது அணி அமைக்கும் வேலை தொடங்கி விட்டதாக பத்திரிகைகளில்
பரபரப்பான செய்திகள் வௌிவந்தன. </p><p>
</p><p>
இதை கண்டு ஜெயலலிதா பரபரப்படைந்தார். அவரது புதிய ஆலோசகர் சோ அதை விட அதிகமாக
பரபரப்பு அடைந்தார். சோ இவ்வாறு பதட்டமடைந்ததற்கு ஒரு கார ணம் இருக்கிறது. அவரே அவரது
துக்ளக் பத்திரிகையில் (31-1-2001 இதழ்) சுட்டிக்காட்டி இருப் பதை போல இன்றைய நிலை
யில் தேர்தல் நடந்தால் அ.தி.மு.க.வை விட தி.மு.க. விற்கே கூடுதலாக வெற்றி
வாய்ப்பிருக்கிறது என்று சோ கருதுகிறார். இதனை ஜெய லலிதாவுக்கு எடுத்து சொல்லி த.மா.கா.வை
விட்டுவிட வேண் டாம் என்று அறிவுறுத்தியிருக்கிறார். </p><p>
</p><p>
சோ பேச்சு </p><p>
</p><p>
அப்படியானால் நீங்களே பேச்சு நடத்துங்கள் என்று ஜெயலலிதா பொறுப்பை அவரிடம்
ஒப்படைத்திருக்கிறார். அதன் பிறகு சோ கடந்த 22-ந்தேதி மூப்பனாரை அவரது வீட்டில்
சந்தித்து பேசியுள்ளார். இந்த பேச்சுவார்த்தையின் போது த.மா.கா.வுக்கு எத்தனை
தொகுதிகள் வரை ஒதுக்க ஜெயலலிதா முன் வந்திருக்கிறார் என்பது குறித்து சோ தெரிவித்த
தகவலை பின்னர் மூப்பனார் மறுநாள் 23-ந்தேதி த.மா.கா.வின் முன்னணி தலைவர்களுடன்
பகிர்ந்து கொண்டு ஆலோசனை நடத்தியிருக்கிறார். </p><p>
</p><p>
ப.சிதம்பரம் </p><p>
</p><p>
வௌிநாடு செல்ல டெல்லியில் தங்கியிருந்த ப.சிதம்பரம் தனது பயணத்தை இரண்டு நாட்கள்
தள்ளி வைத்து விட்டு இந்த ஆலோசனைக்காகவே அன்றைய தினம் சென்னை வந்துள்ளார். அந்த
கூட்டத்தில் த.மா.கா. என்ன நிலை எடுக்க லாம் என்பது குறித்து விவாதிக் கப்பட்டிருக்கிறது.
கூட்டத்தில் கலந்து கொண்டவர்கள் இரு வேறு விதமான கருத்துக்களை தெரிவித்துள்ளனர்.
ப.சிதம்பரம் வழக்கமான அவரது பாணி யில் எந்து முடிவானாலும் விரைவாக எடுங்கள் என்று
கூறிவிட்டு வௌிநாட்டு பயணம் சென்று விட்டார். </p><p>
</p><p>
இத்தகைய நிலையில் த.மா.கா. தரப்பில் பொங்கல் விருந்துக்கு ஏற்பாடு செய்யப் பட்டது.
ஜெயலலிதாவையும் விருந்துக்கு அழைக்க முடிவு செய்து அவருக்கு அழைப்பு கொடுக்க நேரம்
கேட்கப்பட்ட போது தலைமை கழகத்தில் கொடுக்குமாறு தெரிவிக்கப் பட்டிருக்கிறது. அதன்படி
தலைமை கழக நிர்வாகியிடம் த.மா.கா. நிர்வாகிகள் சந்தித்து கொடுத்திருக்கிறார்கள். </p><p>
</p><p>
ஜெயலலிதாவும் தான் பணிந்து சென்று விட கூடாது என்று கருதியே முதலில் அழைப்பிதழை தலைமை
கழகத்தில் பெறுவதற்கு ஏற்பாடு செய்திருக்கிறார். பொங்கல் விருந்து ஏற்பாடு செய்த நாளன்று
சென்னையில் இருக்க கூடாது என கருதி திருச்சி பயணத்திற்கும் ஆயத்தமாகி இருந்தார். </p><p>
</p><p>
இப்படி த.மா.கா. தரப்பிலும், அ.தி.மு.க. தரப்பிலும் பலப்பரிட்சைக்கும் யார் யார் வழிக்கு
பணிந்து வருகிறார்கள் பார்க்கலாம் போட்டிக்கும் பரஸ்பரம் ஆயத்தமாகி கொண்டிருந்த
நிலையில் இந்த இரு கட்சிகளையும் சேர்த்து வைத்து விட வேண்டும் என்ற முயற்சியில் சோ
இறங்கினார். திரை மறைவு பேச்சுவார்த்தைகளை முடுக்கி விட்டார். இதனை ஊர் ஜிதம் செய்து
ஜெயலலிதா தெரிவித்த கருத்துக்கள் அவர் சொல்வதையும், நினைப்பதையும் அப்படியே
வௌியிட்டு வரும் ஒரு ஆங்கில பத்திரிகையில் முக்கியத்துவம் கொடுத்து பிரசுரிக்கப்பட்டு
இருந்தது இங்கு குறிப்பிடத்தக்கது. </p><p>
</p><p>
அன்பழகன் அழைப்பு </p><p>
</p><p>
த.மா.கா. தனது புதிய யுக்திகளுக்கான முயற்சியில் ஒரு புறம் ஈடுபட்டிருந்தது. அதே
நேரத்தில் சட்டசபையில் கல்வி அமைச்சர் அன்பழகன் நாட்டையும், சமுதாயத்தையும்
சீரழிக்கக் கூடிய மிகப்பெரிய ஊழல் சக்தியை ஒழித்திட வாரீர் என்று த.மா.கா.வுக்கு
பகிரங்கமாக அழைப்பு விடுத்தார். சமரச தூதர் சோ த.மா.கா. இல்லாமல் தேர்தலில் வெற்றி
பெற முடியாது என்று ஜெயலலிதாவிற்கு அறிவுறுத்தினார். </p><p>
</p><p>
இவையெல்லாம் ஜெய லலிதாவை திடுக்கிட வைத்து விட்டன. உடனடியாக தனது நிலையை மாற்றி கொண்டு
மூப்பனாருக்கு அவசர அவசரமாக கடிதம் எழுதியிருக்கிறார். பொங்கல் விருந்திற்கு அ.தி.மு.க.
சார்பில் கட்சி பொருளாளரை அனுப்பிவைக்கிறேன் என்று கடிதத்தில் குறிப்பிட்டு விட்டு தனது
எண்ணங்களை பிரதி பலித்து வரும் ஆங்கில பத்திரிகையையும் கூப்பிட்டு தகவலை முன் கூட்டியே
தெரிவித்து விட்டார். </p><p>
</p><p>
உடன்பாடு என்ன? </p><p>
</p><p>
அ.தி.மு.க. கூட்டணியில் த.மா.கா. இருக்கிறதா இல்லையா? என்பதை மூப்பனா ரிடம் போய்
கேளுங்கள் என்று சொன்ன ஜெயலலிதா இரு கட்சி களுக்குமிடையே திரைமறைவில் பூர்வாங்க
பேச்சுவார்த்தைகள் நடந்து வருவதாகவும், பேச்சு வார்த்தை ஊக்கம் தருவதாக வும், இரு
கட்சிகளுக்குமிடையே பிரச்சினை எதுவும் இல்லை என்றும் சொல்லி பத்திரிகையி லும் வௌிவர
செய்து விட்டார். </p><p>
</p><p>
இந்த செய்தி வௌியிட்ட அந்த ஆங்கில பத்திரிகை த.மா.கா.- அ.தி.மு.க. இடையே
ஏற்பட்டுள்ள உடன்பாடு என்ன? என்பதையும் வௌியிட் டிருக்கிறது. த.மா.கா. தன்னு டைய
நிலையை மாற்றி கொண்டு மனம் மாறி கூட்டணி ஆட்சி அதிகாரத்தில் பங்கு என்ற கோரிக்கையை
இனி வலியுறுத்தாது என்றும் அதற்கு ஈடாக த.மா.கா.விற்கு தொகுதி பங்கீட்டில் சில
இடங்கள் அதிகமாக கொடுக்கப்படலாம் என்றும் அந்த பத்திரிகை மேலும் குறிப்பிட்டிருந்தது.
ஜெயலலிதா தெரிவித்துதான் இந்த செய்திகள் வௌியிடப் பட்டிருக்கிறது. </p><p>
</p><p>
திரைமறைவு பேச்சு </p><p>
</p><p>
இதிலிருந்து த.மா.கா.வும், அ.தி.மு.க.வும் தத்தம் பிடிவாத நிலையிலிருந்து இறங்கி
வந்திருப்பது தௌிவாக தெரி கிறது. அ.தி.மு.க. தனது நிலை யில் இருந்து இறங்கி த.மா.கா.
விருந்துக்கு பொருளாளரை அனுப்பி வைத்து இருக்கிறது. வந்தால் வரட்டும், வரவிட்டால்
போகட்டும் என்ற நிலையையும் மாற்றி கொண்டு த.மா.கா. வுடன் திரைமறைவு பேச்சு
வார்த்தைகளில் இறங்கி பல்டி அடித்துள்ளது. </p><p>
</p><p>
த.மா.கா.வும் அடைந்தால் கூட்டணி ஆட்சி இல்லையேல் மூன்றாவது அணி என்று அறிவித்ததை விட்டு
விட்டிருக்கிறது. </p><p>
</p><p>
ஜெயலலிதா சொல்லி அந்த ஆங்கில பத்திரிகை வௌியிட் டிருப்பதை போல இவர்களது இந்த
திரைமறைவு உடன்பாடு விரைவிலேயே சென்னை யிலோ அல்லது வேறு ஒரு முக்கிய நகரத்திலோ நடத்த
எண்ணியுள்ள கூட்டணி கட்சி களின் கூட்டுகூட்டத்தில் வௌிச்சத்திற்கு வரும். ஜெ. திருச்சி
போனார் அ.தி.மு.க. பொதுச்செயலாளர் ஜெயலலிதா நேற்று (சனிக்கிழமை) மாலை 6.10
மணியளவில் சென்னை யிலிருந்து சாலை மார்க்கமாக திருச்சிக்கு புறப்பட்டு சென்றார். வருகிற
1-ந்தேதிதான் அவர் சென்னை திரும்புவார். </p><p>
</p><p>
திருச்சியில் பிரபல ஓட்ட லில் தங்கியிருக்கும் அவர் திருமண நிகழ்ச்சி ஒன்றிலும், 29-ம்
தேதி தேசியலீக் மாநாட்டி லும் பங்கேற்கிறார். திருச்சி மற்றும் அதை சுற்றி யுள்ள
கோவில்களுக்கு சென்று வழிபடவும் திட்டமிட்டிருக் கிறார். அவர் பங்கேற்கும் திருமண
நிகழ்ச்சி சசிகலாவின் குடும்ப நிகழ்ச்சியாகும். 26-ம் தேதியே அதாவது வௌ்ளிக்கிழமையே
திருச்சிக்கு பயணமாக முதலில் திட்ட மிட்டிருந்தார். ஆனால் த.மா.கா. அ.தி.மு.க. இடையே
நடந்த திரைமறைவு பேச்சு வார்த்தைக்காக சென்னையில் இருக்கவேண்டிய அவசியம் ஏற்பட்டதால்
பயணத்தை ஒருநாள் ஒத்திப்போட்டார். இந்த இரு கட்சிகளுக்குமிடையே இருந்த இடைவௌி குறைந்து
இருக்கிறது என்பது விருந்துக்கு வராததன் காரணம் குறித்து மூப்பனாருக்கு ஜெயலலிதா எழுதிய கடிதமும்
அதற்கு நன்றி தெரிவித்து மூப்பனார் பின்னர் எழுதியிருப்பதாக கூறப்படும் கடிதமும் புலப்
படுத்துகின்றன. </p>

<p> </p>

<p>தமிழகத்தில் கூட்டணி
ஆட்சிக்கு வாய்ப்பு குறைவு: தி.மலையில் இந்திய கம்யூ. இணை செயலர் மகேந்திரன் பேட்டி</p>

<p> </p>

<p>திருவண்ணாமலை,ஜன.28- இந்திய கம்யூனிஸ்டு கட்சி யின் மாநில இணை
செயலர் மகேந்திரன் திருவண்ணாமலை யில் நிருபர்களுக்கு பேட்டியளித் தார்.அப்போது அவர்
கூறிய தாவது- </p><p>
</p><p>
மாநாடு </p><p>
</p><p>
வருகிற பிப்ரவரி மாதம் 13-ந்தேதி மாலை 5மணிக்கு திருவண்ணாமலை காந்திசிலை அருகில்
திருவண்ணாமலை வேலூர் விழுப்புரம் கடலூர் ஆகிய மாவட்டங்களை உள்ள டக்கிய மதசார்பற்ற
மண்டல அரசியல் மாநாடு நடைபெறஉள் ளது. இந்தமாநாட்டை இந்திய கம்யூனிஸ்டு கட்சிமற்றும்
மார்க் சிஸ்்டு கம்யூனிஸ்டு கட்சி இணைந்து நடத்துகின்றன. </p><p>
</p><p>
நல்்லக்கண்ணு </p><p>
</p><p>
இம்மாநாட்டிற்கு சி.பி.ஐ. மாநில செயலர் ஆர்.நல்லக்க ண்ணு தலைமை தாங்குகிறார். மத
சார்பற்ற கூட்டணியில் உள்ள கட்சியின் தலைவர்க ளான ஜெ.ஜெயலலிதா ஜி.கே. மூப்பனார்
என்.சங்கரய்யா கி.வீரமணி இ.வி.கே.எஸ்.இளங்் கோவன் எம்.ஏ.லத்தீப் ஜி.ஏ. வடிவேலு
திருமாவளவன் ஜெகவீரபாண்டியன் ஆகியோர் கலந்துகொண்டு சிறப்புரை யாற்றுகிறார்கள். </p><p>
</p><p>
சேலத்தில் </p><p>
</p><p>
இதேபோல்் பிப்.20-ந்தேதி சேலத்தில் மதசார்பற்ற மண்டல மாநாடு நடக்கிறது. நாளை
(29-ந்தேதி) திருச்சியில் எம்.ஏ. லத்தீப் மதசார்பற்ற மாநாட்டை நடத்துகிறார். இந்த
மதசார்பற்ற அரசியல் மாநாடு அரசியலில் பலகேள்விகளுக்கு பதில் தர உள்ளது. தமிழகத்தில்
3-வது அணி அமையஉள்ளதாக பேசப் படுகிறது. அந்த அணிஉருவாக யார் முயற்சிசெய்கிறார்கள்
என்பதுதான் தெரியவில்லை. ஆனால் 3-வது அணி உருவாக வேண்டும் என்பதில் தி.மு.க. ஆர்வம்
காட்டுகிறது. மதசார் புள்ள பா.ஜ.க. தி.மு.க. கூட்டணி ஒருஅணி,மத சார்பற்ற அ.தி.மு.க.
த.மா.கா. , கம்யூனிஸ்டு கூட்டணி கட்சிகள் ஓர்அணி இவைதான் தமிழகத்தில் இருக்கும். 3-வது
அணி என்பது குழப்பம் தான். </p><p>
</p><p>
சாதிகட்சிகள் </p><p>
</p><p>
தமிழகத்தில் சாதிகட்சிகள் இருக்கக்கூடாது. பிற்பட்டோர் பட்டியலில் மட்டும் 600சாதிகள்
உள்ளது. அவர்கள் தனித்தனியே கட்சி ஆரம்பித்துவிட்டால் தமி ழன் என்ற அடையாளம் போய்
விடும். எப்போதும் மனிதனை முதன்மைபடுத்தவேண்டும் எனவே சாதிகட்சிகள் ஏற்புடை யது அல்ல.
எங்கள்அணியும் இதைத்தான் விரும்புகிறது. </p><p>
</p><p>
சீரழிவு </p><p>
</p><p>
அண்மை காலத்தில் பெரும் பொருளாதார சீரழிவுஏற்பட் டுள்ளது. இதன் முழு பொறுப்பை தி.மு.க.
ஏற்கவேண் டும். பொருளாதார ரீதியில் கிராம பொருளாதாரம் குறிப் பாக விவசாய பொருளாதாரம்
மிகமோசமாக பாதிக்கப்பட் டுள்ளது. விவசாயிகள் பெற்ற கூட்டுறவு கடனை கட்டவில்லை.
என்றால் ஜப்தி செய்கிறது. தமிழகத்தில் அரசு மற்றும் தனியார் சர்க்கரை ஆலைகளில்
பலமாதங்களாக வருடக்க ணக்கில் கரும்பு பணம் தராமல் உள்ளனர். இதில் அரசுக்கும் பொறுப்பு
உள்ளது. </p><p>
</p><p>
டன்னுக்கு ரூ.1000 </p><p>
</p><p>
விவசாயிகள் கோபமுற்று எழுச்சி போராட்டமாக நடத்த வரும்போது முதல்வர் கலைஞர்
கரும்பு டன் ஒன்றுக்கு ரூ.1000 என அறிவிக்கிறார். 10பிழிதிறன் கொண்ட கரும்புக்கு ரூ.1000
வழங்கப்படும் என கூறுகின்ற னர். தமிழகத்தில் 10பிழிதிறன் கொண்ட கரும்புகளே இல்லை.
எனவே தமிழகத்தில் இல்லாத கரும்புக்கு கலைஞர் ரூ.1000 விலை என அறிவித்துள்ளார்.
தேயிலை தேங்காய் போன்ற விலைகள் வீழ்்ச்சி அடைந் துள்்்ளது. காரணம் 714 பொருட்க ளுக்கு
தாராள மயமாக்கல் கொள்கை உத்தரவை முர சொலிமாறன் பிறப்பித் துள்ளதுதான். </p><p>
</p><p>
முதலிடம் </p><p>
</p><p>
தொழில் துறையில் இந்தியா விலேயே தமிழகம் முதலிடம் என சொல்கிறார்கள் .இதைநாங் கள்
ஏற்கமுடியாது. காரணம் திட்டங்களை அறிவித்ததை வைத்துமுதலிடம்என்றுசொல்கி றார்கள்.
ஹூண்டாய் கார் கம்பெனி துவக்கப்பட்டுள்ளதாக கூறுகின்றனர். ஆனால் அதில் 500 பேருக்குக்கூட
வேலை வாய்ப்பு இல்லை. ஆனால் 10ஆயிரம்பேர் பணியாற்றிய ஸ்டேண்டர்டு மோட்டார் கம்
பெனி மூடப்பட்டுள்ளது. சிறு தொழில்கள் நசுங்கிவிட்டது. மலிவான விளம்பரம் மூலம் ஓட்டுவாங்க
தி.மு.க.நினைக் கிறது. தேர்தல் ஆதாயத்திற் காகத்தான் ஜெயலலிதா மீதுள்ள ஊழல் வழக்குகளை
முன்நிறுத்தி விளம்பரப்படுத்து கிறார்கள். எங்களின் மதசார் பற்ற அணி தொடர்ந்து
நீடிக்கும். ஆனால் தமிழகத்தில் கூட்டணி ஆட்சி என்பதற்கு வாய்ப்புகள் குறைவாகத்தான்
உள்ளது. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
பேட்டியின்போது சி.பி.ஐ. மாவட்ட செயலர் ஜோதி என். பாலசந்தர் கற்பகம் உடன்
இருந்தனர்.' </p>

<p> </p>

<p>பாலக்கோடு ம.தி.மு.க
பொதுக் கூட்டத்தில் வைகோவிடம், ரூ.12 லட்சம் தேர்தல் நிதி</p>

<p> </p>

<p>பாலக்கோடு, ஜன. 28- தர்மபுரி மாவட்ட ம.தி.மு.க தேர்தல்
நிதியளிப்பு விழா பொதுக் கூட்டம் பாலக்கோடு பஸ் நிலையத்தில் நடந்தது. மாவட்ட
செயலாளர் மாதை யன் தலைமை தாங்கினார். காரி மங்கலம் ஒன்றிய செயலாளர் சூடப்பட்டி
டி.சுப்பிரமணி, மாவட்ட தலைவர் சாமிக் கண்ணு, பொருளாளர் நாகரா ஜன், பச்சியப்பன்,
தேவராஜன், குணசேகரன் ஆகியோர் முன் னிலை வகித்தனர். கோவிந்தன் வரவேற்றார். </p><p>
</p><p>
ரூ.12 லட்சம் </p><p>
</p><p>
கூட்டத்தில் மாவட்ட செயலாளர் மாதையன் ரூ.12 லட்சத்தை தர்மபுரி மாவட்ட தேர்தல் நிதியாக
வைகோவிடம் வழங்கினார். நிதியை பெற்றுக் கொண்டு வைகோ பேசி னார். </p><p>
</p><p>
மற்றும் மத்திய அமைச்சர் கள் கண்ணப்பன், செஞ்சி ராமச்சந்திரன் ஆகியோரும்
பேசினார்கள். வக்கீல் நட ராஜன், சத்தியமூர்த்தி, மூர்த்தி, பத்மாவதி சுப்பிரமணி,
சம்பத், சேகர், ராஜா, கதிரவன், ஜெய சீலன், இளங்கோ, தர், ராதா கிருஷ்ணன் உள்பட
பலர் கலந்து கொண்டனர். கூட்டத் தில் இரண்டு குழந்தைகளுக்கு வைகோ பெயர் சூட்டினார். </p>

<p> </p>

<p>ஆணவம்-அலட்சியம் மிகுந்த
ஜெ.வுடன் தேர்தல் உறவை மூப்பனார் முறிக்க வேண்டும்: திருநாவுக்கரசு எம்.பி. பேச்சு</p>

<p> </p>

<p>சென்னை, ஜன.28- சென்னை பெரவள்ளூர் சதுக்கத்தில் எம்.ஜி.ஆர்.
அ.தி. மு.க. சார்பில் வீரவணக்க நாள் பொதுக்கூட்டம் வடசென்னை மாவட்ட செயலாளர் தேவ.ஜவ
ஹர் தலைமையில் நடை பெற்றது. இக்கூட்டத்தில் கலந்து கொண்டு திருநாவுக்கரசு எம்.பி.
பேசியதாவது:- </p><p>
</p><p>
இந்தி எதிர்ப்பு என்பது 70 ஆண்டுகாலமாக உள்ளது. இந்தி திணிப்பைத்தான் திரா விட இயக்கங்கள்
எதிர்க் கிறோமே தவிர இந்தியையே எதிர்க்கவில்லை. </p><p>
</p><p>
தமிழை காப்பதற்காக தங்கள் தேக்குமர தேகங்களை தீக்கும், விஷத்திற்கும் பலி கொடுத்த
சிதம்பரம் ராஜேந்திரன், மாயவரம் சாரங்க பாணி, உள்ளிட்ட அறுபதுக் கும் மேற்பட்ட தியாக
தீபங்களுக்கு அஞ்சலி செலுத்திட தமிழகம் முழுவதும் எம்.ஜி.ஆர். அண்ணா தி.மு.க. கூட்டம்
நடத்தி தன் அஞ்சலியை தெரி வித்துக்கொள்கிறது. </p><p>
</p><p>
இன்றைய அரசியலில் தூய் மையை விரும்பும் அனைத்து சக்திகளும் ஒன்று சேர்ந்து ஜெய லலிதாவை
வீழ்த்தவேண்டும். கலைஞர் தலைமையில் தமிழ கத்தில் நல்ல ஆட்சி நடைபெற்று வருகிறது.
ஜெயலலிதா வாலேயே கலைஞர் மீது குறிப் பிட்டு சொல்லும்படி எந்த ஊழலும் நடைபெறவில்லை.
</p><p>
</p><p>
ஜெ. ஆணவம் </p><p>
</p><p>
மூப்பனார் கடந்த முறை தி.மு.க. கூட்டணியில் நின்ற நாற்பது இடங்களுக்கும் அதிக மான
இடங்களில் இந்த முறை அ.தி.மு.க. நிற்பதற்கு வாய்ப்பு தராவிட்டால் அதே நாற்பது
இடங்களில் தி.மு.க. கூட்டணி யில் நிற்பதுதான் மூப்பனா ருக்கு கவுரவம். </p><p>
</p><p>
நிருபர்கள் ஜெயலலிதா விடம் உங்கள் கூட்டணியில் மூப்பனார் இருக்கிறாரா? என
கேட்டபோது, இதை நீங்கள் மூப்பனாரிடம்தான் கேட்க வேண்டும் என அலட்சியத்துட னும்,
ஆணவத்துடனும் பதில் கூறிய ஜெயலலிதாவுடன் தேர் தல் உறவை மூப்பனார் முறித் துக்கொள்ள
வேண்டும். </p><p>
</p><p>
பேசத்தயார் </p><p>
</p><p>
கூட்டணியின் ஒற்றுமைக் காக கலைஞரிடமும், ராம தாசிடமும் நான் நேரில் சென்று
பேசியதுபோல, மூப்பனாரிட மும், கலைஞரிடமும் நேரில் சென்று பேசவும் நான் தயார். </p><p>
</p><p>
இவ்வாறு அவர் பேசினார். </p><p>
</p><p>
இக்கூட்டத்தில் இலக்கிய அணி செயலாளர் கவிஞர் காசி.முத்துமாணிக்கம், இளை ஞர்
அணி செயலாளர் நாசே. ஜெ.ராமச்சந்திரன், தொண்டரணி செயலாளர் எஸ்.நாகப் பன்,
எம்.ஜி.ஆர். பேரவை செயலாளர் சொர்ணா சேதுராமன், தொழிற்சங்க தலைவர் ஏ.சி.
முத்துக்கண்ணன், எஸ். வெங்கடாசலம் உள்பட பலரும் பேசினார்கள். </p>

<p> </p>

<p>கன்னித் தமிழ் வாழ
தமிழ்நாட்டில் மீண்டும் கலைஞர் முதல்வராவார்: டத்தோ சாமிவேலு பேச்சு</p>

<p> </p>

<p>சென்னை,
ஜன.28- சென்னையில் நடந்த முர சொலி அறக்கட்டளை விருது வழங்கு விழாவில் அமைச்சர்
அன்பழகன் பேசியதாவது:- </p><p>
</p><p>
முரசொலி அறக்கட்டளை சார்பில் இயல், இசை, நாட கத்தை காப்பவர்களுக்கு விருது கள்
வழங்கப்படுகின்றன. இய லுக்கு அடையாளமாக டத்தோ சாமிவேலுக்கும், இசைத் தமி ழுக்கு
அடையாளமாக வைர முத்துக்கும், நாடகத் தமிழுக்கு அடையாளமாக பாலசந்தருக் கும் விருதுகள்
வழங்கப் பட்டுள்ளன. </p><p>
</p><p>
முரசொலி மாறன் </p><p>
</p><p>
மத்திய அமைச்சர் முர சொலி மாறன் பேசியதாவது:- </p><p>
</p><p>
இந்த விழாவில் நான் கலந்து கொள்ள முடியுமா என்ற கேள்விக் குறி சில வாரங் கள் முன்பு
வரை இருந்தது. அந்த நிலை மாறி உங்கள் முன் நான் நின்று கொண்டிருக்கி றேன். நான் நலம்
பெற வாழ்த் திய உங்கள் எல்லோருக்கும் என் மனமார்ந்த நன்றியை தெரி வித்துக்
கொள்கிறேன். </p><p>
</p><p>
தொண்டர்கள் </p><p>
</p><p>
சாதி, மதம், அரசியலுக்கு அப்பாற்பட்டு உருவாக்கப் பட்டது இந்த அறக்கட்டளை. இதை
உருவாக்கியவர்கள் தி.மு.க. மாவட்ட செயலாளர் கள். எனவே தி.மு.க. தொண் டர்கள்
ஒவ்வொருவருக்கும் நன்றி தெரிவிக்கிறேன். </p><p>
</p><p>
நாம் எதிர்்க்கட்சியாக இருந்த போது ஒவ்வொருவரும் நிதி தந்து இது ஏற்படுத்தப்பட்டது.
தமிழர்களை தமிழர்கள் சிறப் பிக்க வேண்டும் என்பதற்காக இங்கே விருதுகள் வழங்கப்படு
கின்றன. ஒவ்வொரு துறையி லும் சிறந்தவர்களுக்கு இந்த விருதுகள் வழங்கப்படுகின்றன. 20-ம்
நூற்றாண்டில் சிறந்த தமிழர்களுக்கு இன்று விருது கள் வழங்கப்படுகின்றன. </p><p>
</p><p>
டத்தோ சாமிவேலு </p><p>
</p><p>
மலேசிய அமைச்சர் டத்தோ சாமிவேலு விழாவில் பேசிய தாவது:- </p><p>
</p><p>
முதலமைச்சர் கலைஞர் என் தலைவர் ஆவார். அவரது எழுத்துக்கள் தான் என்னை
அரசியலுக்கும், பொது வாழ்வுக்கும் கொண்டு வந்தன. கலைஞரின் பொற்கரங்களால் விருது
பெற்றதில் நான் பெரு மகிழ்ச்சி அடைகிறேன். இந்த விருது மூலம் எனக்கு அங்கீகாரம்
அளித்துள்ளார். </p><p>
</p><p>
மலாய் மொழி பிரிவு </p><p>
</p><p>
இந்த விருது தமிழினத்திற்கு தூண்டுதலும், தமிழுக்கு சேவை செய்வோருக்கு தெம்பும் அளிக் கும்.
</p><p>
</p><p>
இந்த பரிசுத் தொகையுடன் 2 மடங்கு தொகை சேர்த்து சென்னை பல்கலைக்கழகத்தில் மலாய்
மொழிப் போதனை பிரிவு தொடங்க அளிக்கிறேன். தமிழகத்தில் தொடர்ந்து 5-வது முறையும்
கலைஞர் முதல மைச்சர் பொறுப்பு ஏற்பார். அதற்கு என் வாழ்த்துக்களை தெரிவித்துக்
கொள்கிறேன். கன்னித் தமிழ் வாழ கலைஞர் ஆட்சி என்றும் நிலைக்க வேண் டும். </p><p>
</p><p>
பாலசந்தர் </p><p>
</p><p>
டைரக்டர் பாலசந்தர் பேசியதாவது:- </p><p>
</p><p>
என் 40 ஆண்டு கால கலை வாழ்வில் எத்தனையோ விருது களை பெற்றாலும் இந்த விருதை
எல்லாவற்றிற்கும் மகுடமாக கருதுகிறேன். இன்னும் பல சிகரங்களை அடைய எனக்கு தரப்பட்ட
ஊக்கமாகும் இது. இந்த நாள் என் தலையில் வைரக்கிரீடம் சூட்டிய நாள். நான் வியக்கும்
தலைவர் கலை ஞர் கையால் விருது பெற்றதை பெருமையாக கருதுகிறேன். </p><p>
</p><p>
வைரமுத்து </p><p>
</p><p>
கவிஞர் வைரமுத்து பேசிய தாவது:- </p><p>
</p><p>
சில வாரங்களுக்கு முன் மத்திய அமைச்சர் முரசொலி மாறன் மருத்துவமனையில் இருந்தார். தவம்
இருந்து கலைஞர் அவரை மீட்டு வந்து இருக்கிறார். அவர் பல்லாண்டு வாழ வாழ்த்துகிறேன். </p><p>
</p><p>
கலைஞர் இன்று வழங்கிய விருதை பெரிய விருதாக கருது கிறேன். இந்த விருதால் உற் சாகம்
அடைகிறேன். 35 ஆண்டு கால முரசொலி வாசகனுக்கு தரப்பட்ட விருது இது. </p><p>
</p><p>
இவ்வாறு அவர்கள் பேசினர். </p>

<p> </p>

<p>தேர்தலில் நிற்பதை தடுக்க
சதி: ஜெ.மீது வழக்கு போட்டு அழிக்க நினைக்கிறார்கள்: தி.மலை பொதுகூட்டத்தில் மதுசூதனன்
பேச்சு</p>

<p> </p>

<p>திருவண்ணாமலை,ஜன.28- திருவண்ணாமலை வேட்ட வலம் சாலை சந்திப்பில்
தெற்கு மாவட்ட அ.தி.மு.க. மாணவர் அணி சார்பாக வீர வணக்க நாள் பொதுக் கூட்டம் நடந்தது. </p><p>
</p><p>
கூட்டத்துக்கு மாவட்ட மாணவர் அணி செயலாளர் பெருமாள் நகர் கே.ராஜன் தலைமை தாங்கினார்.
மாவட்ட மாணவர் அணி இணை செயலாளர் ராஜா என்ற போர். மன்னன், ஆர்.சுலைமான், பாஷா
கே.ராம மூர்த்தி ஆகியோர் முன்னிலை வகித்தனர். நகராட்சி உறுப்பினர் பி.சுனில்குமார்
வரவேற்று பேசினார். </p><p>
</p><p>
மதுசூதனன் </p><p>
</p><p>
கூட்டத்தில் முன்னாள் அமைச்சர் மதுசூதனன் கலந்து கொண்டு பேசினார். </p><p>
</p><p>
அவர் கூறிய தாவது:- </p><p>
</p><p>
இன்றைய தினம் தமிழகம் முழுவதும் இந்தி எதிர்ப்பு போரில் வீர மரணம் அடைந்த மொழிப்
போர் தியாகிகளுக்கு அஞ்சலி செலுத்திடும் வகையில் வீர வணக்க நாள் பொதுக்
கூட்டங்கள் நடந்து கொண்டிருக்கிறது. </p><p>
</p><p>
கருணாநிதி யாரை ஊழல் வாதி என்கிறார். தன்னைத்தானே சொல்லி கொள்கிறார். ஊழல்
இன்றல்ல நேற்றல்ல கருணாநிதி திரைக்கதை எழுத ஆரம்பித்த திலிருந்தே இருக்கிறது. </p><p>
</p><p>
கருணாநிதி குடும்ப அரசியல் நடத்தி வருகிறார். </p><p>
</p><p>
1949-ல் தி.மு.க.வை பேரறிஞர் அண்ணா தொடங்கினார். ஆனால் இன்று தி.மு.க.வினர்
அண்ணாவை மறந்து விட்டார்கள். </p><p>
</p><p>
சுருட்டல் </p><p>
</p><p>
பழி வாங்கும் எண்ணம் உள்ள கருணாநிதி தனது 5 ஆண்டு கால ஆட்சியில் செய்தது என்ன? தமிழக
மக்களுக்கு என்ன திட்டங் களை தீட்டி செயல் படுத்தி இருக்கிறார். அவர் ஆட்சியில்
விலைவாசி குறைந்திருக்கிறதா? கருணாநிதிக்கு சன் டி.வி. எப்படி வந்தது? கேபிள்கள்
எங்கிருந்து வந்தத? இன்று உலக அளவில் 10 பணக்காரர்களில் கலாநிதி ஒருவராக
திகழ்கிறார். இதன் மர்மம் என்ன? </p><p>
</p><p>
இன்று ஜெயலலிதாவை பார்த்து ஊழல்வாதி என்கிறார். யார் யாரை பார்த்து ஊழல் வாதி என
சொல்வது? திருட்டை கண்டு பிடித்தவரே கருணாநிதிதான். அவர் எழுதிய மந்திரி குமாரி
படத்தில் களவாடுவது, கொள்ளை யடிப்பது ஒரு கலை என்று அவரே சொல்லியிருக் கிறார். </p><p>
</p><p>
நான் கருணாநிதியை பார்த்து கேட்கிறேன். உங்கள் குடும்ப சொத்தை நாட்டு மக்களுக்கு சொல்ல
தயாரா. நீங்கள் முதல்வர் பொறுப்பு ஏற்ற போது கணக்கு காட்டினீர்களா? உங்கள் சொத்து
எவ்வளவு? </p><p>
</p><p>
அவர் ஆரம்பித்த உழவர் சந்தை ஊழல் சந்தையாகி விட்டது. அது போ சமத்துவ புரம் என்று ஒன்று
உருவாக்கினார். அங்கு சமத்துவமே இல்லை. ஒரே சண்டையும், சச்சரவுமாக இருக்கிறது. எங்கு
பார்த்தாலும் ஒரே ஊழல் தலை விரித்தாடு கிறது. </p><p>
</p><p>
உயிர் நாடி </p><p>
</p><p>
தொண்டர்கள் தான் என் உயிர் நாடி. அவர்கள் இல்லை என்றால் நான் இல்லை என்றார். ஆனால்
கருணாநிதி ஆட்சியில் தி.மு.க. தொண்டனுக்கு மதிப்பில்லை. பெண் என்றும் பாராமல்
ஜெயலலிதாவை 28 நாள் சிறையில் அடைத்தவர் கருணாநிதி. </p><p>
</p><p>
ஜெயலலிதா தேர்தலில் நிற்க என்னனென்ன தடைகள் செய்ய முடியுமோ அத்தனை சதி வேலை களையும்
அவர் செய்து வருகிறார். இந்த சதி திட்டம் எல்லாம் செல்லாது. நாங்கள் பணங்காட்டு நறி.
இந்த சலசலப்புக்கெல்லாம் அஞ்ச மாட்டோம். </p><p>
</p><p>
ஜெயலலிதா மீது வழக்கு போட்டு அவரை அழித்து விடலாம் என கருணாநிதி கனவு காண்கிறார். அது
பலிக்காது. தமிழகத்தில் அடுத்த முதல்வர் ஜெயலலிதாதான் மக்கள் முடிவு செய்து விட்டார்கள்.
ஜெயலலிதா வை ஒரு கருணாதி மட்டும் அல்ல. ஓராயிரம் கருணாநிதி வந்தாலும் அசைக்க முடியாது. </p><p>
</p><p>
இவ்வாறு அவர் பேசினார். </p><p>
</p><p>
பா.வளர்மதி </p><p>
</p><p>
மற்றும் தலைமை கழக பேச்சாளர் பா.வளர்மதி, மாணவர் அணி துணை செயலாளர் செந்தமிழன்,
மாவட்ட அ.தி.மு.க. செயலாளர் எஸ்.ராமச் சந்திரன், நகராட்சி தலைவர் வி.பவன்குமார்
மாவட்ட பேரவை செயலாளர் ஏ.ஜி.பஞ்சாட்சரம், எம்.ஜி.ஆர். இளைஞர் அணி மாவட்ட
செயலாளர் என்.பாலச்சந்தர், அக்ரி கிருஷ்ண மூர்த்தி, ஜி.அனந்த கிருஷ்ணன், நகராட்சி
உறுப்பினர் ஜெய பாலன், நகர பேரவை செயலாளர் ஆர்.பால சுப்பிர மணியன் உள்பட பலர்
பேசினர். </p><p>
</p><p>
முடிவில் மாவட்ட மாணவர் அணி துணை செயலாளர் ரேடியோ எஸ்.ஆறுமுகம் நன்றி கூறினார். கூட்ட
ஏற்பாடுகளை மாவட்ட மாணவர் அணி செயலாளர் பெருமாள் நகர் கே.ராஜன் செய்திருந்தார். </p>

<p> </p>






</body></text></cesDoc>