<cesDoc id="tam-w-dinakaran-news-00-08-13" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-news-00-08-13.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-08-13</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-08-13</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சந்தன வீரப்பன் 14
நிபந்தனை: நக்கீரன் கோபால் விரிவான விளக்கத்துடன் நாளை காட்டுக்குள் செல்வார்:
முதல்வர் எஸ்.எம்.கிருஷ்ணா பேட்டி</p>

<p> </p>

<p>பெங்களூர்,ஆக.13- கர்நாடக முதல்வர் எஸ்.எம். கிருஷ்ணா நிருபருக்கு
பேட்டி அளித்தார். அப்போது அவர் கூறியதாவது:- </p><p>
</p><p>
சந்தன வீரப்பனிடம் இருந்து நடிகர் ராஜ்குமாரை மீட்கும் முயற்சியில் 2 மாநில அரசுகளும்
தீவிரம் காட்டி வருகிறது. வீரப்பன் முதல் கட்டமாக விதித்த 10 நிபந்தனைகளுக்கும் 2 மாநில
அரசுகளும்பதில் அளித்து உள் ளது. அந்த பதிலை வீரப்பன் சரியாக புரிந்து கொள்ளவில்லை என
தெரிகிறது. </p><p>
</p><p>
நாளை காட்டுக்குள் </p><p>
</p><p>
இப்போது மேலும் 4 நிபந்த னைகளை சந்தன வீரப்பன் விதித்து உள்ளான். மொத்தம் 14
நிபந்தனைகளுக்கும் விரிவான விளக்கத்துடன் நக்கீரன் கோபால் மூலம் வீரப்பனுக்கு
அனுப்பப்படும். நாளை (திங்கள்) கோபால் காட்டுக்கு செல்கிறார். </p><p>
</p><p>
தடா கைதி விடுவிப்பு </p><p>
</p><p>
1.மைசூர் சிறையில் உள்ள தடா கைதிகளை விடுதலை செய்ய வேண்டும். </p><p>
</p><p>
2. வாச்சாந்தி, சின்னாம்பதி ஆகிய பகுதிகளில் பாதிக்கப்பட்ட மக்களுக்க நஷ்ட ஈடு
கொடுக்க வேண்டும் என்ற 2 நிபந்தனைகளையும் 2 மாநில அரசுகளும் ஏற்று ஆணை பிறப்பிக்கும்.
இதற்கான பதில் கோபால் மூலம் அனுப்பி வைக்கப்படும். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>அரசியல் சட்ட திருத்தம்
நிறைவேற்றியே தீருவேன்: சந்திரிகா சபதம்</p>

<p> </p>

<p>கொழும்பு, ஆக.13- இலங்கை அதிபர் சந்திரிகா ரூபவாகினி
தொலைக்காட்சிக்கு நேற்றுமுன்தினம் பேட்டி அளித்தார். அதில் அவர் கூறியதாவது:- </p><p>
</p><p>
இலங்கை அரசியல் சட்டத் திருத்தத்தை எப்படியாவது நிறைவேற்றியே தீருவேன். இந்த
முயற்சியில் தற்காலிக பின்னடைவு இப்போது ஏற்பட்டு இருக்கிறது. இருப்பினும் எனது முயற்சி
தொடரும். இந்த திட்டத்தை நான் கைவிடுவதாக இல்லை. யார் எதிர்த்தாலும் எனது முயற்சி
தொடரும். </p><p>
</p><p>
ஆகஸ்டு 24 </p><p>
</p><p>
தற்போதுள்ள பாராளு மன்றத்தின் ஆயுள் வரும் 24-ந் தேதி நள்ளிரவுடன் முடிகிறது. அதற்குள்
சட்டத்திருத்த மசோதாவை எப்படியாவது நிறைவேற்ற பார்ப்பேன். மூன்றில் 2 பங்கு
பெரும்பான்மை பெற முயற்சிப்பேன். இந்த முயற்சி யில் எதிர்க்கட்சியான ஐக்கிய தேசிய
கட்சி எங்களை ஆதரித் தாலும் சரி. எதிர்த்தாலும் சரி. அதைப்பற்றி கவலைப்பட
போவதில்லை. </p><p>
</p><p>
தனி மெஜாரிட்டி </p><p>
</p><p>
இப்போது இந்த சட்டத் திருத்தத்தை நிறைவேற்ற முடியாவிட்டால் அடுத்த பாராளுமன்ற தேர்தலில்
வெற்றி பெற்று நிறைவேற்று வேன். தனி மெஜாரிட்டி கிடைத்து விட்டால் மூன்றில் இருபங்கு
பெரும்பான்மை தேவையின்றி சட்டத்திருத் தத்தை நிறைவேற்றி விடுவேன். </p><p>
</p><p>
அரசியல் நிர்ணய சபை </p><p>
</p><p>
அடுத்த பொதுத்தேர் தலுக்கு பின் புதிதாக அமையும் பாராளுமன்றத்தை நான் அரசி யல் நிர்ணய
சபையாக மாற்றுவேன். இந்த மாற்றத்தை உறுதிப்படுத்துவேன். பாராளுமன்றத்தை அரசியல் நிர்ணய
சபையாக மாற்றுவதற்கு தேசிய மற்றும் சர்வதேச ஆதரவு எனக்கு கிடைத்துள்ளது. </p><p>
</p><p>
மக்கள் ஆதரவு </p><p>
</p><p>
அரசியல் சட்டத்தை திருத்தி இனப்பிரச்சினைக்கு நான் தீர்வுகாண முயல்கிறேன்.
பெரும்பான்மை மக்கள் இதை ஆதரிக்கிறார்கள். இனப்பிரச் சினையை ஒழிக்கும் எனது முயற்சியை
எதிர்த்து போராட்டம் நடக்கிறது. இனவெறியர் கள்தான் இதுபோன்ற போராட்டம்
நடத்துகிறார்கள். எதிர்க்கட்சியான ஐக்கிய தேசிய கட்சி, விடுதலைப்புலிகள், சிங்கள
இனவெறியர்கள் ஆகியோர் எனது முயற்சியை எதிர்க் கிறார்கள். இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
புத்த அமைப்பு கோரிக்கை </p><p>
</p><p>
இதற்கிடையே இலங்கை புத்த மதத்தலைவர்களில் ஒரு வரான விபாசி என்பவர் அனைத்து சிங்கள
அமைப்புகளுக்கும் அழைப்பு விடுத்துள்ளார். புதிய அரசியல் சட்டத் திருத்தத்தை எதிர்த்து
ஐக்கிய முன்னணி ஒன்றை அமைத்து போராட வேண்டும் என்று அவர் கோரிக்கை விடுத்துள்ளார். </p>

<p> </p>

<p>வாஜ்பாய் உறுதிமொழி
எதிரொலி: பொதுத்துறை ஊழியர்கள் ஸ்டிரைக் தள்ளிவைப்பு</p>

<p> </p>

<p>புதுடெல்லி, ஆக.13- மத்திய அரசின் தனியார் மயகொள்கையை
எதிர்த்து பல்வேறு தொழிற்சங்கங்கள் போராட்டம் அறிவித்துள்ளன. இதுதொடர்பாக
தொழிற்சங்க தலைவர்கள் நேற்று பிரதமர் வாஜ்பாயை சந்தித்து பேசினர். </p><p>
</p><p>
அப்போது அவர்களிடம் வாஜ்பாய் கூறியதாவது:- </p><p>
</p><p>
தொழிலாளர்களுடன் மத்திய அரசு மோதல் போக்கை வளர்க்க விரும்பவில்லை. பொதுத்துறை
நிறுவனங்களில் அரசின் பங்கை குறைப்பதால் தொழிலாளர்களுக்கு எந்த பாதிப்பும் வராது.
அவர்களின் நலனை அரசு பாதுகாக்கும். இது தொழிலாளர் விரோத அரசு அல்ல. இவ்வாறு அவர்
கூறினார். </p><p>
</p><p>
ஸ்டிரைக் வேண்டாம் </p><p>
</p><p>
இந்த கூட்டத்தில் மந்திரிகள் யஷ்வந்த்சின்கா, சத்ய நாராயண் ஜாட்டியா, அருண் ஷோரி
ஆகியோர் கலந்து கொண்டனர்.பொதுத்துறை நிறுவனங் களில் வேலைநிறுத்த நோட்டீசு கொடுத்து
இருப்பதை வாபஸ் பெற்றுக் கொள்ளுமாறு மந்திரி அருண்ஷோரி கேட்டுக் கொண்டார். </p><p>
</p><p>
இதையடுத்து பிரதமர் உறுதிமொழி ஏற்று பொதுத் துறை நிறுவன ஊழியர்களின் 3 நாள்
வேலைநிறுத்தம் தள்ளி வைக்கப்பட்டு உள்ளது. எனவே வருகிற 17-ந்தேதி முதல் ஸ்டிரைக் நடக்காது
என்று அறிவிக்கப்பட்டு இருக்கிறது. </p>

<p> </p>

<p>சி.எம்.சி நூற்றாண்டு
தபால் தலை வௌிட்டார்: வேலூர் மாவட்டத்தில் வரும் டிசம்பரில் டெலிபோன் துறை மூலம்
செல்போன் வசதி: மத்திய அமைச்சர் பஸ்வான் உறுதி</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>




 
  
  <p>ராம்விலாஸ் பஸ்வான் </p>
  
 




<p>வேலூர், ஆக.13- வேலூரில் கடந்த 1990-ம் ஆண்டு டாக்டர் ஐடா
ஸ்கட்டர் ஒரு படுக்கையுடன் சி.எம்.சி மருத்துவமனையை தொடங்கினார். அது பல்வேறு வசதிகளுடன்
விரிவடைந்து இந்தியாவிலேயே தலைசிறந்த மருத்துவ மனையாக திகழுகிறது. தற்போது இந்த
மருத்துவமனை நூற்்றாண்டு விழா கொண்டாடி கொண்டு உள்ளது. </p><p>
</p><p>
தபால் தலை வௌியீடு </p><p>
</p><p>
இதையொட்டி மருத்துவ மனையின் நூற்றாண்டு நினைவு தபால் தலை வௌியீட்டு விழா வேலூர்
பாகாயத்தில் சி.எம்.சி மருத்துவ கல்லூரியில் உள்ள ஸ்கட்டர் கலையரங்கில் நேற்று
பிற்பகல் 1.45 மணியளவில் நடைபெற்றது. மத்திய தபால் துறை சார்பில் இந்த விழா
நடத்தப்பட்டது. </p><p>
</p><p>
விழாவுக்கு, மத்திய சுகாதார சேவை தலைமை இயக்குனர் எஸ்.பி.அகர்வால் தலைமை வகித் தார்.
சி.எம.்.சி கவுன்சில் தலைவர் தாமஸ் ஆபிரகாம் வரவேற்று பேசினார். சி.எம்.சி மருத்துவ
மனை இயக்குனர் டாக்டர் ஜாய்ஸ் பொன்னையா அறிக்கை வாசித்தார் </p><p>
</p><p>
மத்திய அமைச்சர் பஸ்வான் </p><p>
</p><p>
விழாவில் மத்திய தகவல் தொடர்பு துறை அமைச்சர் ராம்விலாஸ் பஸ்வான் கலந்து கொண்டு
சி.எம்.சி நூற்றாண்டு நினைவு தபால் தலையை வௌியிட்டார். மத்திய சுகாதார சேவை தலைமை
இயக்குனர் எஸ்.பி. அகர்வால் முதல் தபால் தலையை பெற்று கொண்டார். தபால் தலையை
வௌியிட்டு அமைச்சர் பஸ்வான் பேசிய தாவது:- </p><p>
</p><p>
சி.எம்.சி நூற்றாண்டு தபால் தலை வௌியிடுவதில் நான் மிக்க மகிழ்ச்சி அடைகிறேன்.
டாக்டர் ஐடா ஸ்கட்டர் ஒரு படுக்கையுடன் தொடங்கிய இந்த மருத்துவமனை இன்றைய தினம்
பல்வேறு வசதிகளுடன் தலை சிறந்த மருத்துவமனையாக திகழுகிறது. </p><p>
</p><p>
இங்கு தான் உலகிலேயே முதன் முதலாக தொழுநோய் சீரமைப்பு சிகிச்சை, மற்றும் இருதய மாற்று
சிகிச்சை உள்பட பல்வேறு சிகிச்சைகள் அறிமுகப் படுத்தப்பட்டு உள்ளது. சி.எம்.சி
நிறுவனத்தினர் மருத்துவ பணியை மட்டுமின்றி கே.வி.குப் பம், அணைக்கட்டு போன்ற கிராம
பகுதிகளில் நலிந்த பிரிவினருக்கு ஆற்றி வருகிற பணி மகத்தானது. </p><p>
</p><p>
செல்போன் வசதி </p><p>
</p><p>
தொலை தொடர்பு துறை மூலமாக வருகிற அக்டோபரில் சென்னை, மதுரை, கோவை, புதுவை ஆகிய
இடங்களில் செல்போன் வசதி செய்யப்பட உள்ளது. வருகிற டிசம்பரில் வேலூரில் செல்போன்
வசதி செய்யப்பட உள்ளது. </p><p>
</p><p>
இந்தியாவில் அனைத்து மாவட்ட தலைநகரங்களிலும் இண்டர்நெட் வசதி செய்யப்படும் எள்று
ஏற்கனவே நாங்கள் கொடுத்த உறுதிமொழியை நிறைவேற்றி வருகிறோம். </p><p>
</p><p>
தொலை தொடர்பு மையம் </p><p>
</p><p>
இந்தியாவில் உள்ள 6 லட்சம் கிராமங்களில் 3 லட்சத்து 76 ஆயிரம் கிராமங்களில் டெலி
போன் வசதி செய்து உள்ளோம். வருகிற 2002 -ம் ஆண்டுக்குள் இந்தியாவில் உள்ள அனைத்து
கிராம பஞ்சாயத்திலும் தொலை தொடர்பு மையங்கள் தொடங்கப் படும். </p><p>
</p><p>
பாராட்டு </p><p>
</p><p>
மேலும் அகில இந்திய விஞ்ஞான மருத்துவ கழகம் நடத்திய ஆய்வில் இந்தியாவில்
உள்ள சிறந்த மருத்துவ கல்லூரிகளில் ஒன்றாக சி.எம்.சி மருத்துவ கல்லூரியை தேர்வு செய்து
உள்ளனர். அதற்கு எனது பாராட்டுக்கள். சி.எம்.சி மருத் துவமனையும் தனது சிறந்த சேவையை
தொடர வாழ்த்துக் களை தெரிவித்து கொள்கிறேன். இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
அமைச்சர் துரைமுருகன் </p><p>
</p><p>
விழாவில் தமிழக பொதுப் பணி துறை அமைச்சர் துரை முருகன் பேசியதாவது:- </p><p>
</p><p>
வேலூரில் மருத்துவமனை மற்றும் மருத்துவ கல்லூரி தொடங்கி டாக்டர் ஐடாஸ்கட்டர் ஆற்றிய
தொண்டுக்கு நீண்ட வரலாறு உண்டு. </p><p>
</p><p>
பெருமை </p><p>
</p><p>
வேலூருக்கு நீர் இல்லாத பாலாறு, மன்னன் இல்லாத கோட்டை, அதிகாரம் இல்லாத போலீஸ் என்று
பல சிறப்புகளை கூறுவார்கள்.இத்தனைக்கும் மேலாக வேலூருக்கு பெருமை சேர்க்க கூடியது சி.எம்.சி
மருத்துவமனை தான். உலகில் எந்த மூலைக்கு சென்று சி.எம்சி மருத்துவமனை என்றாலும் வேலூர்
என்றும் வேலூர் என்றால் சி.எம்.சி என்றும் கூற கூடிய நிலை தான் உள்ளது. </p><p>
</p><p>
சி.எம்.சி மருத்துவமனையில் இதுவரை உழைத்தவர்்்களுக்கும் உழைத்து கொண்டிருக்கிறவர் களுக்கும்
எனது பாராட்டு- நன்றியை தெரிவித்து கொள் கிறேன். </p><p>
</p><p>
கல்வி-மருத்துவ பணி </p><p>
</p><p>
வௌ்ளைக்காரன் இந்தியாவை ஆளாமல் இருந்திருந்தால் ஆங்கிலத்தை இங்கு அறிமுகப்படுத்தாமல்
விட்டு இருந்தால் நமது நாடு 200 ஆண்டு பின்நோக்கி போயிருக்கும். மதத்தை பரப்ப
வந்தவர்கள் அந்த பணியுடன் நிற்காது கல்வி, மருத்துவ பணிகளையும் ஆற்றினர். </p><p>
</p><p>
வௌிநாட்டில் இருந்து வீரமா முனிவர்,கால்டுவெல், டாக்டர் ஜி.யு. போப் போன்ற வர்கள்
இந்தியாவுக்கு வந்த வுடன் கிறிஸ்தவ மதத்தை பரப்பவில்லை. முதலில் நமது தமிழ் மொழியை
தான் படித்தனர். நமது மொழிக்கு மலபார் மொழி, நமது நாட்டுக்கு மலபார் நாடு என்று இருந்ததை
அவர்கள் தான் கண்டுபிடித்து நமக்கு கூறினா.் </p><p>
</p><p>
நன்றி </p><p>
</p><p>
மேலும் அவர்கள் தான் ஆங்கில மொழி, கல்வி, மருத்துவம் ஆகியவற்றை நமக்கு கொண்டு
வந்தனர்.அதனால் நாம் நல்ல நிலையை அடைந்து உள்ளோம். எனவே நாம் கிறிஸ்தவ மதத்துக்கு,
கிறிஸ்தவர் களுக்கு நன்றி கூற கடமைப்பட்டு உள்ளோம். இவ்வாறு அமைச்சர் பேசினார். </p><p>
</p><p>
கலெக்டர் </p><p>
</p><p>
மற்றும் மாவட்ட கலெக் டர்ஆர்.சிவகுமார் கலந்து கொண்டு பேசினார். </p><p>
</p><p>
இந்திய தபால் துறை தலைமை அதிகாரி மேனன், தபால் வாரிய உறுப்பினர் (ஊழியர் நலம்)
டத்தா, தமிழக வக்ப் வாரிய உறுப்பினர் தி.அ .முகமது சகி, சி.எம்.சி மருத்துவ மனை முன்னாள்
இயக்குனர்கள் எல்.பி.எம்.ஜோசப், பெஞ்சமின் புளிமூட், மாத்தன் மற்றும்
டாக்டர்கள், மருத்துவமனை ஊழியர்கள் திரளாக கலந்து கொண்டனர். </p><p>
</p><p>
முடிவில் சி.எம்.சி நூற்றாண்டு விழா குழு அமைப்பாளர் டாக்டர் லட்சுமி சேஷாத்திரி நன்றி
கூறினார்.</p>

<p> </p>

<p>வீரப்பன் கோரிக்கைபடி
தமிழை 2-வது ஆட்சி மொழியாக்க இயலாது</p>

<p> </p>

<p>பெங்களூர், ஆக. 13- கன்னட நடிகர் ராஜ்குமார் மற்றும் 3 பேர்
கடந்த 14 நாட்களாக காட்டில் சந்தன வீரப்பனிடம் பணயக் கைதிகளாக இருக்கிறார்கள்
அல்லவா? இவர்களை விடுவிப்பதற்காக வீரப்பன் ஏற்கனவே 10 கோரிக் கைகளை விடுத்து
கேசட் அனுப்பினான். இந்த கோரிக் கைகளுக்கு தமிழ்நாடு, கர்நாடக அரசுகள் ஏற்கனவே சாதகமான
பதில்களை தெரிவித்து விட்டன. இந்த நிலையில், ஏற்கனவே விடுத்த 10 நிபந்தனைகளுக்கு அரசு
தெரிவிக்கும் முறையான பதில் என்ன? என்பதைஅறியவும், புதிதாக 4 நிபந்தனைகளை
விதித்தும் வீரப்பன் நக்கீரன் கோபால் மூலம் புதிய கேசட்டுகளை அனுப்பி உள்ளார். . </p><p>
</p><p>
கிருஷ்ணா பேட்டி </p><p>
</p><p>
புதிய நிபந்தனைகள் பற்றிய வீடியோ கேசட்டுகளை ராஜ் குமார் குடும்பத்துக்கும், கர் நாடக
முதல்வர் கிருஷ்ணாவுக் கும் தனித் தனியாக வீரப்பன் அனுப்பி இருந்தான். அந்த வீடியோ
கேசட்டை கிருஷ்ணா நேற்று போட்டுப் பார்த்தார். </p><p>
</p><p>
இதன் பிறகு முதல்வர் எஸ்.எம். கிருஷ்ணா பெங்க ளூரில் நிருபர்களுக்கு பேட்டி அளித்தார்.
அப்போது அவர் கூறியதாவது:- </p><p>
</p><p>
ராஜ்குமார் மற்றும் 3 பேரை விடுவிப்பதற்காக தீவிரமான கோரிக்கைகளை வைக்க வேண் டாம்
என்று சந்தன வீரப்பனை கேட்டுக்கொள்கிறேன். வீரப்பன் வைத்து உள்ள கோரிக்கை களை
நிறைவேற்றுவது கடின மல்ல என்றாலும், சுமுகமான முன்னேற்றத்துக்கு ஒத்து வராத தீவிர
கோரிக்கையை எழுப்பு வது முறையானது அல்ல. </p><p>
</p><p>
நிராகரிப்பு </p><p>
</p><p>
உதாரணமாக தமிழை கர்நாடகத்தில் 2-வது ஆட்சி மொழியாக்க வேண்டும் என்ற கோரிக்கையும்,
கர்நாடகத்தில் தமிழர்களுக்கு கல்வி, வேலை வாய்ப்பில் சம வாய்ப்பு அளிக்க வேண்டும்
என்பதும் சாத்திய மானது அல்ல. இதுபோல் தமிழ்நாட்டில் கன்னடத்தை 2-வது ஆட்சி மொழியாக்கு
வதற்கும் வாய்ப்பு இல்லை. அதே நிலைதான் கர்நாடகத்திலும். </p><p>
</p><p>
மத்திய அரசின் சிக்கலான நியமன கொள்கை எல்லா மாநி லங்களுக்கும் பொருந்தும். இதில்
மொழி சிறுபான்மையினரின் நலன் கவனிக்கப்படுகிறது. ஆனால் இந்த கொள்கையின் படி
கன்னடர்களுக்கு அநீதி இழைக்கப்படுவதாக நாங்கள் உணருகிறோம். தனது கோரிக்கைகளை
நிறைவேற்ற வீரப்பன் ஆகஸ்டு 19-ஐ கெடு வாக நிர்ணயிக்கவில்லை. இந்த தேதிக்குள் தனது கோரிக்கைகளுக்கு
பதில் தர வேண்டும் என்று எதிர் பார்க்கிறார். அவர் கெடு விதித்து இருப்பதாக நான்
நினைக்கவில்லை. </p><p>
</p><p>
ஆவணங்கள் சேகரிப்பு </p><p>
</p><p>
வீரப்பன் கூட்டாளிகள் என்று சந்தேகிக்கப்படும் 51 பேர்கள் மீதான தடா வழக்கு களை வாபஸ்
பெறுவது,1991- 92ல் நடந்த காவிரி கலவரத்தில் பாதிக்கப்பட்டவர்களுக்கு இழப் பீடு வழங்குவது
உள்பட வீரப் பனின் பிற கோரிக்கைகள் மீது எடுக்கப்பட்டுள்ள நடவடிக்கை கள் தொடர்பான
தகவல்களை கர்நாடக அரசு சேகரித்து வரு கிறது. இது ஒரு சிக்கலான மற்றும் உணர்வுப்பூர்வமான
நடவ டிக்கை. நாங்கள் உலகில் உள்ள எல்லாவிதமான பொறு மைகளையும் கடைப்பிடிக்க வேண்டி உள்ளது.
வீரப்பனை சமாதானப்படுத்துவதற்காக தமிழ்நாடும், கர்நாடகமும், இந்த பாதையைத்தான் பின்
பற்றுகிறது. </p><p>
</p><p>
இந்த விஷயத்தில் புத்திசா லித்தனமாக முடிவை எடுக்க வேண்டும். எனவே சம்பந்தப்
பட்டவர்களுடன் ஆலோசித்து வருகிறோம். இதன் விவரங் களை இப்போது வௌியிட முடியாது. ஆனால்
ராஜ்குமார் விடுதலை பற்றி எந்த அச்சமும் தேவையில்லை. </p><p>
</p><p>
51 தடா கைதி நாளை விடுதலை </p><p>
</p><p>
தடா வழக்குகளை வாபஸ் பெறுவது தொடர்பாக மைசூர் கோர்ட்டில் அரசு வக்கீல் ஆகஸ்டு
10-ந்தேதி மனு தாக்கல் செய்து விட்டார். இதன் மீதான விசாரணை 14-ந்தேதிக்கு
தள்ளிவைக்கப்பட்டு உள்ளது. தடா வழக்குகள் வாபஸ் பெறப் பட்ட பிறகு, 7 ஆண்டுகள் ஜெயிலில்
இருந்த 51 பேரும் ஜாமீனில் வௌியே வரலாம். </p><p>
</p><p>
நாளை (14-ந்தேதி) இந்த வழக்கு மைசூர் கோர்ட்டில் விசாரணைக்கு வரும் போது 51 பேரும்
விடுதலை செய்யப்படு வார்கள் என்று எதிர்பார்க்கப் படுகிறது) காவிரி கலவரத்தில்
பாதிக்கப்பட்டவர்களுக்கு ஏற்கனவே ரூ.2 கோடி நஷ்டஈடு கொடுக்கப்பட்டு விட்டது.பிற
நிவாரணம் பற்றி பரிசீலிக்கப்படும். </p><p>
</p><p>
சிலை திறப்பு தேதி </p><p>
</p><p>
சென்னையில் திறக்கப்பட இருக்கும் கன்னடப் புலவர் சர்வஜனா சிலை செதுக்கும் பணி
முடிவடைந்தது பற்றி தெரிந்து கொண்ட பிறகு, பெங்களூரில் திருவள்ளுவர் சிலை திறப்புக்கான
தேதி குறிக்கப்படும். </p><p>
</p><p>
வீரப்பனின் கோரிக்கைளை நிறைவேற்றுவது தொடர்பாக எடுக்கப்பட்டுள்ள நடவடிக்கைகளுக்கான
ஆவணங்களை 14-ந்தேதிக்குள் சேகரித்து விடு வோம். அந்த ஆவணங்களுடன் கர்நாடக மூத்த
போலீஸ் அதிகாரி சீனிவாசன் சென்னைக்கு கொண்டு சென்று நக்கீரன் கோபாலிடம்
கொடுப்பார். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>போலீசில் சரண் அடைந்த
வீரப்பன் கூட்டாளி ஆனந்தன் யார்? பரபரப்பு தகவல்கள்</p>

<p> </p>

<p>பவானி, ஆக. 13- பவானி வர்ணாபுரம் 5-வது வீதியில் வசித்து
வருபவர் கந்தசாமி. இவர் பவானியில் உள்ள ஒரு தனியார் மில்லில் டிரைவராக பணி புரிந்து
வருகிறார். இவருக்கு ஆனந்தன் (வயது 20) என்ற ஒரு மகனும், பூவிழி (வயது 9) என்ற ஒரு மகளும்
உள்ளனர். இதில் ஆனந்தன் 7-ம் வகுப்பு வரை படித்து விட்டு ஈரோட்டில் மெக்கானிக்காக
பணி புரிந்து கொண்டு வந்துள்ளார். பூவிழி 3-ம் வகுப்பு படித்து வருகிறாள். கந்தசாமியின்
மனைவி டெய்லராக உள்ளார். </p><p>
</p><p>
வீட்டிலிருந்து ஓட்டம் </p><p>
</p><p>
ஆனந்தன் அதிகமாக சினிமா பார்க்கும் வழக்கம் உடையவன். அடிக்கடி சினிமா பார்ப்பதால்
ஆனந்தனை அவரது தந்தை கண்டித்து உள்ளார். அதனால் கோபித்துக் கொண்டு வீட்டை விட்டு
கொடைக்கானலுக்கு ஓட்டம் பிடித்துள்ளான். அங்கு ஒரு டீக்கடையில் வேலை செய்து வந்துள்ளான்.
பின்னர் தகவல் கிடைத்து பெற்றோர்கள் சென்று அழைத்து வந்து ஈரோட்டில் ஒரு தனியார்
மோட்டார் வாகன விற்பனை நிலையத்தில் மெக்கானிக்காக வேலையில் சேர்த்து உள்ளனர். </p><p>
</p><p>
தமிழ் ஆர்வம் </p><p>
</p><p>
பவானியிலிருந்து தினசரி காலையில் வேலைக்கு சென்று இரவு வீடு திரும்பி கொண்டி ருந்த
ஆனந்தனுக்கு சிறு வயது முதலே தமிழ் மொழி மீது ஆர்வம் இருந்து வந்தது. இதன் காரணமாக
ஈரோட்டில் சில தமிழ் தீவிரவாத அமைப்பை சேர்ந்தவர்களோடு தொடர்பு ஏற்பட்டதாக
தெரிகிறது. தமிழ் தீவிரவாத அமைப்பை சேர்ந்த வர்கள் தங்களது தகவல்களை ஆனந்தன் மூலமாக
பரிமாறி கொண்டதாகவும் தெரிய வருகிறது. </p><p>
</p><p>
காவல் நிலைய தாக்குதல் </p><p>
</p><p>
மேலும், வௌ்ளித்திருப்பூர் காவல் நிலைய தாக்குதலில் ஈடுபட்டதாக கூறப்படுபவனும்,
தற்போது வீரப்பனால் விடு தலை செய்யப்பட வேண்டும் என கோரிக்கை வைத்துள்ள தமிழர்
விடுதலை படையை சேர்ந்த சிலரை பவானி கோர்ட் டிற்கு போலீசார் கொண்டு வந்தபோது அவர்களை
ஆனந்தன் சந்தித்து பேசி சில தகவல் பரிமாற்றங்கள் நிகழ்வதற்கு தூதுவனாக இருந்ததும்
போலீசார் விசாரணையில் தெரிய வந்துள்ளது. </p><p>
</p><p>
பெற்றோருக்கு டிமிக்கி </p><p>
</p><p>
கொடைக்கானலில் இருந்து பெற்றோரால் அழைத்து வந்து ஈரோட்டில் மெக்கானிக்காக சேர்த்து
விடப்பட்ட பின்னர் இந்த மாதம் 4-ந்தேதி சம்பளம் வாங்கி கொண்டு வீட்டில் சம்பள
பணத்தை தர வில்லை. ஆனால் கடந்த செவ்வாய்க் கிழமை வரை எப்போதும் போலவே வேலைக்கு
செல்வதாக கூறிக்கொண்டு காலையில் சென்று விட்டு இரவு வீடு திரும்பி உள்ளான். பின்னர்
சென்றவன் இன்று வரை வீடு திரும்ப வில்லை. இதற்கிடையே கடந்த 10-ந்தேதி போலீசாரால் கைது
செய்யப்பட்டுள்ளான். ஆனந்தனை காணாமல் தவித்த பெற்றோர் அவன் வேலை செய்யும் அலுவலகத்தில்
தொடர்பு கொண்டபோது தான் பையன் வேலையை விட்டு சம்பளத்தை வாங்கி கொண்டு நின்ற
விஷயம் பெற்றோருக்கு தெரிய வந்தது. </p><p>
</p><p>
மேலும் தகவல்கள் </p><p>
</p><p>
புதிய நபரான ஆனந்தனை போலீசார் கைது செய்து விசாரணை நடத்தி வருவதால் தற்போது
கடத்தப்பட்டுள்ள ராஜ்குமாரை மீட்க உதவியாக பல தகவல்கள் போலீசாருக்கும், அரசுக்கும்
கிடைக்க வாய்ப்பு உள்ளதாக போலீஸ் வட்டாரங்களில் இருந்து நம்பத்தகுந்த தகவல்கள்
தெரிவிக்கிறது. </p>

<p> </p>

<p>மகேந்திர சவுத்ரி பேட்டி:
பிஜி தீவில் ஜனநாயகம் மலர இந்தியாவிடம் உதவி கேட்போம்</p>

<p> </p>

<p>புதுடெல்லி, ஆக.13- மகேந்திர சவுத்ரி வருகிற 17-ந்தேதி இந்தியா
வருகிறார். ஒரு வாரம் அவர் இந்தியாவில் தங்குகிறார். </p><p>
</p><p>
தொலைபேசியில் பேட்டி </p><p>
</p><p>
அவரிடம் தொலைபேசி மூலம் இந்திய தொலைக்காட்சி பேட்டி எடுத்தது. அது இன்று
ஒளிபரப்பாகிறது. அதில் அவர் கூறி இருப்பதாவது:- </p><p>
</p><p>
பிஜி தீவில் மீண்டும் ஜனநாயகம் மலர இந்திய அரசின் உதவியை கேட்போம் நான் டெல்லி
வரும்போது இந்திய தலைவர்களுடன் அது பற்றி பேசுவேன். மற்ற காமன்வெல்த் நாடுகளுக்கும் சென்று
முறையிடுவேன். பிஜி தீவின் இடைக்கால அதிபர் இடைக்கால அரசை கலைக்க வேண்டும். அரசியல்
சட்டத்தை மீண்டும் கொண்டு வரவேண்டும். </p><p>
</p><p>
அதிருப்தி </p><p>
</p><p>
பிஜிதீவில் மனித உரிமைகள் பறிக்கப்பட்டது குறிப்பாக இந்தியர்களுக்கு எதிராக சர்வ தேச
அமைப்புகள் செயல்பட்ட விதம் அதிருப்தி அளிக்கிறது. ஆஸ்திரேலியா மற்றும் நியூசிலாந்து
நாடுகளின் பிரதமர்கள் அளித்த பதில் திருப்தி அளிக்கிறது. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>ராஜ்குமாரை கடத்தினால்
கோரிக்கை ஏற்கப்படுகிறது: வீரப்பன் வழி பின்பற்றினால் தான் குடகு தனி மாநிலம்்
அமையுமா? மத்திய அரசுக்கு மிரட்டல்</p>

<p> </p>

<p>புதுடெல்லி, ஆக.13- ஜார்கண்ட், உத்தராஞ்சல், சட்டீஸ்கர்
ஆகிய 3 புதிய மாநி லங்கள் அமைக்க மசோதா நிறைவேறியது தெரிந்ததே. இந்த நிலையில்
அடுத்ததாக கர்நாட கத்தில் இருந்து புதிதாக குடகு மாநிலம் அமைக்க வேண்டும் என்ற கோரிக்கை
எழுந்து உள்ளது. </p><p>
</p><p>
குடகு மாநிலம் </p><p>
</p><p>
குடகு தனி மாநிலம் அமைப்ப தற்காக உருவாக்கப்பட்ட குடகு ராஜ்ய முக்தி மோர்சா என்ற
அமைப்பானது தற்போது கூர்க் தேசிய கவுன்சில் என்று பெயர் மாற்றம் செய்யப்பட்டு உள்ளது.
இதன் பொதுச் செயலாளர் என்.யூ.நாச்சப்பா கூறிய தாவது:- </p><p>
</p><p>
ஏற்கனவே குடகு தனி மாநிலமாக இருந்தது. இப்போது கர்நாடகத்தில் குடகு மாவட்டம் என்பது 3
சட்டசபை தொகுதியைக் கொண்டதாக உள்ளது. மக்களவைக்கு தனது உறுப்பினரை தேர்ந்து எடுக்க
முடியாமல் குடகு தவிக்கிறது. கடந்த 44 ஆண்டுகளாக குடகு பகுதி புறக்கணிக்கப்பட்டு வருகிறது. </p><p>
</p><p>
நாங்கள் குடகு மாநிலம் அமைக்க வேண்டும் என்று அமைதியான வழியில் போராடி வருகிறோம்.
எங்கள் கலாச்சாரம், தனித்தன்மையை காட்டுவதற்காக துப்பாக்கி வைத்து இருக்க அனுமதி அளித்து
உள்ளனர். </p><p>
</p><p>
கடத்தினால் நடக்குமா? </p><p>
</p><p>
ஆனால் வன்முறையை கையில் தூக்குவோரின் கோரிக்கைகளுக்கே அரசு செவி சாய்க்கிறது. சினிமா
நடிகர் ராஜ்குமாரை வீரப்பன் கடத்தி னான். அவனது கோரிக்கை களை இரு மாநில அரசுகளும்
ஏற்கின்றன. அதே போலத் தான் ஹிஸ்புல் முஜாகிதீன் இயக்கத் துடன் மத்திய அரசு பேச்சு
நடத்துகிறது. துப்பாக்கியை கையில் தூக்கியதால் தான் அவர்களுடன் அரசு பேச்சு நடத்துகிறது. </p><p>
</p><p>
ஒரு லட்சம் பேர் கொண்ட லேக் மற்றும் லடாக் மக்களுக்கு தனி மாநிலம் அமைக்க தேசிய
ஜனநாயக கூட்டணி அரசு செவி சாய்க்கிறது. ஆனால் 5 லட்சம் மக்கள் தொகையை கொண்ட குடகு
பகுதியை தனி மாநிலம் ஆக்க இதுவரை முன்வரவில்லை. எனவே குடகு தனி மாநிலம் அமைக்க வேண்டும்
என்று கோரி வருகிற நவம்பர் 21-ந்தேதி டெல்லியில் மாபெரும் பேரணி நடத்துவோம். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>தமிழர் மீது பற்றுள்ளதுபோல்
வீரப்பன் நடிக்கிறான், தேவாரம் பேட்டி</p>

<p> </p>

<p>நாகர்கோவில், ஆக. 13- மாநில அளவிலான கால்பந்து போட்டி
நிறைவுவிழா நேற்று நாகர்கோவிலில் நடந்தது. இதில் கலந்துகொள்ள வந்த முன்னாள் டி.ஜி.பி.
வால்டர் தேவாரம் நிருபர் களிடம் கூறியதாவது:- </p><p>
</p><p>
கடந்த 93-ம் ஆண்டு எனது தலைமையில் 200 சிறப்பு பயிற்சி பெற்ற போலீசார் வீரப்பனை
பிடிக்க காட்டுக்கு சென்றோம். அப்போது நடந்த மோதலில் இரு தரப்பிலும் உயிர்ச்சேதம்
ஏற்பட்டது. வீரப்பன் வசம் இருந்த பலரை சுட்டுக் கொன்றோம். 90 பேரை கைது செய்தோம்.
வீரப்பனுடன் கூட்டாளியாக யாரும் சேராத அளவில் போக்குவரத்துகளை தடை செய்தோம். </p><p>
</p><p>
வினோதம் </p><p>
</p><p>
அப்போது அவனிடம் 5 பேர் மட்டுமே இருந்தனர். வீரப்பன் வெடிகுண்டு வீசி பலரை கொலை
செய்தும், பலரை பிடித்துச் சென்றபோதும் வீரப்பன் ஆயிரம் கோடி பணம் வேண்டும் என்று
மட்டுமே கோரிக்கை விடுத்தான். ஆனால் தற்போது தமிழகத்துக்கு காவிரி நீர் வேண்டும், தடா
கைதிகளை விடுதலை செய்யவேண்டும் என்று கூறுவது வினோதமாக உள்ளது. </p><p>
</p><p>
வேடிக்கை </p><p>
</p><p>
தமிழர்கள் மீது ஏதோ பற்று உள்ளதுபோல் வீரப்பன் நடிக்கிறான். கடந்த ஆண்டுகளில் அவனால்
கொலை செய்யப்பட்ட 140 பேரில் 100 பேர் தமிழர்கள். வீரப்பன் சரணடையவேண்டும், பொது
மன்னிப்பு வழங்கவேண்டும் என்று கூறுவது வேடிக்கையாக உள்ளது. </p><p>
</p><p>
வீரப்பன் ஒருபோதும் சரணடைய வாய்ப்பு இல்லை. தவறி சரணடைந்தாலும் கோர்ட்டு விடுதலை
செய்தாலும் வீரப்பனால் பாதிக்கப் பட்ட பலர் கோர்ட்டில் வழக்கு தொடர தயாராக உள்ளனர்.
நடிகர் ராஜ்குமாரை விடுதலை செய்ய நடவடிக்கை எடுக்க வேண்டும். ராஜ்குமாருக்கு ஏதாவது
நேர்ந்தால் கர்நாடகத்தில் உள்ள தமிழர்களை நாம் இழக்க வேண்டிய சூழ்நிலை உண்டாகும். </p><p>
</p><p>
வீரப்பனை பிடிக்கவோ, சரணடைய செய்யவோ அரசு முயலவேண்டும். இப்போது உள்ள சூழ்நிலையில்
வீரப்பன் அதிக ஆள் பலத்துடன் உள்ளான். தைரியம் இருந்தால் போலீசார் வீரப்பனை
பிடிக்கவேண்டும். தைரியம் உள்ளவர்களிடம் அரசு பொறுப்பை ஒப்படைக்க வேண்டும். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>

<p>காஷ்மீர் குண்டு வெடிப்பில்
பலி: ராணுவ வீரர் உடல் கோவை வந்தது: கலெக்டர்-போலீஸ் கமிஷனர் மலர் அஞ்சலி</p>

<p> </p>

<p>கோவை,
ஆக. 13- காஷ்மீர் பிரச்சினைக்கு தீர்வு காண்பதற்காக ஹிஸ்புல் முஜாஹிதீன் இயக்கம்
மத்திய அரசுடன் பேச்சுவார்த்தை நடத் தியது. இது தோல்வி அடைந்தது. இதைதொடர்ந்து
ஹிஸ்புல் முஜாஹிதின் இயக்கம் தனது போர் நிறுத்தத்தை வாபஸ் பெற் றது. போர் நிறுத்தம்
வாபஸ் பெற்ற இரண்டே நாளில் ஜம்மு காஷ்மீர் மாநிலம்  நகரில் பயங்கரமான கார் குண்டு
வெடித்தது. இதில், ராணுவ வீரர் கள், போலீஸ் அதிகாரிகள், பத்திரிகை போட்டோகிராபர்
உள்பட 13 பேர் பலியானார்கள். ஏராளமானோர் படுகாயம் அடைந்தனர். </p><p>
</p><p>
கோவை ராணுவ வீரர் </p><p>
</p><p>
இந்த குண்டுவெடிப்பில் கோவையை சேர்ந்த ராணுவ வீரர் ஒருவரும் வீரமரணம் அடைந்து உள்ளார்.
அவரது பெயர் சேகர் (வயது 38). இவர் நகரில் பாதுகாப்பு பணியில் ஈடுபட்டிருந்தபோது
இந்த சோக சம்பவம் நடந்தது. இவரது சொந்த ஊர் சேலம் மாவட்டம் மேட்டூர் அருகே உள்ள
தங்கமா புரிபட்டினம் ஆகும். இவரது தந்தை பெயர் பழனியப்பன். இவ ருக்கு திருமணமாகி விட்டது.
மனைவி பெயர் காவேரி. இவர் களுக்கு 2 குழந்தைகள் உள்ள னர். மனைவி காவேரி தனது 2 குழந்தைகளுடன்
கோவை சித்தா புதூர் ஜவகர் நகரில் வசித்து வரு கிறார். வீர மரணம் அடைந்த சேகரின்
உடல் நேற்று (சனிக் கிழமை) மதியம் விமானம் மூலம் கோவை கொண்டு வரப்பட்டது. </p><p>
</p><p>
மலர் அஞ்சலி </p><p>
</p><p>
இவரது உடலுக்கு கோவை மாவட்ட கலெக்டர் ஜி.சந்தானம், மாநகர போலீஸ் கமிஷனர்
கே.ராதாகிருஷ்ணன், துணை கமிஷனர் சங்கர் மற்றும் கோவை ராணுவ அதிகாரிகள் மலர் வளையம்
வைத்து அஞ்சலி செலுத்தினர். பின்னர் சேகரின் உடல், அலங்கரிக்கப்பட்ட ராணுவ
வண்டியில் ஊர்வல மாக எடுத்து செல்லப்பட்டு, சித்தாபுதூரில் உள்ள அவரது மனைவி மற்றும்
உறவினர் களிடம் ஒப்படைக்கப்பட்டது. </p><p>
</p><p>
மனைவி கதறல் </p><p>
</p><p>
கணவனின் பிணத்தை பார்த் ததும் காவேரி கதவி அழுதார். நான் காஷ்மீர் சென்று வருகிறேன்
என்று கூறிவிட்டு சென்ற நீங்கள் இன்று பிணமாக வந்துள் ளீர்களே, என்னையும், உங்கள்
பிள்ளைகழையும் இனி யார் கவனிப்பார்? என்று கதறி அழு தது அருகில் நின்றவர்களின்
கண்களில் இருந்து கண்ணீரை வர வழைத்தது. கணவனை இழந்த துக்கத்தை அவரால் தாங்கிக் கொள்ள
முடியவில்லை. அவ ரது உறவினர்கள் அவருக்கு ஆறுதல் கூறினர். ஆனால் அந்த வார்த்தை அவரது
காதில் விழவே இல்லை. தரையில் புரண்டு அழுத காட்சி கல் நெஞ் சையும் கரைய
வைப்பதாக இருந்தது. தாய் அழுவதை பார்த்த அவரது 2 குழந்தைகளும் கதறி அழுதன. ஒரே சோக மயமாக
காட்சி அளித்தது. </p><p>
</p><p>
உடல் தகனம் </p><p>
</p><p>
வீர மரணம் அடைந்த சேகரின் உடல் நேற்று மாலை ஆவா ரம்பாளையத்தில் உள்ள மின் மயான
சுடுகாட்டில் ராணுவ மரியாதையுடன் தகனம் செய்யப்பட்டது. இதில் சேகரின் உறவினர் கள், ராணுவ
அதிகாரிகள், அரசு அதிகாரிகள், போலீஸ் அதிகாரி கள் உள்பட ஏராளமான பொது மக்கள் கலந்து
கொண்டனர். </p>

<p> </p>

<p>சிறப்பு பூஜைகளில்
பங்கேற்பு: சிறையில் இருந்து ஆசிரமத்துக்கு வந்தார் பிரேமானந்தா</p>

<p> </p>

<p>புதுக்கோட்டை, ஆக. 13- திருச்சி பாத்திமா நகர் பகுதியில்
ஆசிரமம் நடத்தி வரு பவர் பிரேமானந்தா சாமியார். இவரது ஆசிரமத்தில் நடந்த கொலை
மற்றும் கற்ப ழிப்பு சம்பவங்கள் மாநிலம் முழுவதும் பெரும் பரபரப்பை ஏற்படுத்தியது.
பிரேமானந்தா மீது தொடரப்பட்ட வழக்கில் அவருக்கு இரட்டை ஆயுள் தண்டனை விதிக்கப்பட்
்டது. அவர் சிறையில் அடைக்கப் பட்டார். </p><p>
</p><p>
ஆசிரம செயல்பாடு </p><p>
</p><p>
பிரேமானந்தா சிறையில் இருந்தாலும் ஆசிரம செயல்பாடுகள் தொடர்ந்து நடைபெற்று வருகின்றன. இந்த
நிலையில் கடந்த 26-7-2000 ந்தேதி முதல் கடந்த 15-ந்தேதி வரை ஆசிர மத்தில் பல்வேறு
சிறப்பு நிகழ்ச்சிகள் நடைபெற்று வருகிறது. </p><p>
</p><p>
இந்த நிகழ்ச்சிகளில் கலந்து கொள்வதற்காக பிரேமா னந்தா சாமியார் கடந்த 26-7-2000ல்
கடலூர் சிறையில் இருந்து பரோ லில் ஆசிரமத் துக்கு வந்தார். அவருடன் போலீசார் 4 பேரும்
வந்திருந் தனர். பிரேமானந்தா ஆசிரமத் தில் நேற்று முன்தினம் வரை அங்கு தங்கி இருந்தார்.
</p><p>
</p><p>
வௌிநாடுகளில் </p><p>
</p><p>
ஆசிரமத்தில் நடைபெற்ற பல்வேறு நிகழ்ச்சிகள், சிறப்பு பூஜைகளில் அவர் கலந்து
கொண்டார். இதில் வௌிநாடு களில் இருந்தும் பிரேமா னந்தா சாமியாரின் சிஷ்ய கோடிகள்
வருகை தந்தனர். நிகழ்ச்சிகளை முடித்து கொண்டு பிரேமானந்தா சாமியார் மீண்டும் சிறைக்கு
கொண்டு செல்லப்பட்டார். </p>

<p> </p>

<p>நாச வேலை காரணமா?
தஞ்சை-நாகூர் பாசஞ்சர் ரெயில் கவிழ்ந்தது- போக்குவரத்து பாதிப்பு</p>

<p> </p>

<p>திருச்சி,ஆக.13- தஞ்சையில் இருந்து நாகூருக்கு நேற்று (சனி)
காலை 10 மணி அளவில் ஒரு பாசஞ்சர் ரெயில் புறப்பட்டு சென்றது. இந்த ரெயில்
திருவாரூருக்கும் சிக்கல் ரெயில்் நிலையத்திற்கும் இடையில் அடியக்கமங்கலம் என்ற
கிராமத்தின் அருகில் சென்றபோது மதியம் 12.30 மணி அளவில் திடீர் என கவிழ்ந்தது. </p><p>
</p><p>
5 பெட்டிகள் </p><p>
</p><p>
இதில் அந்த ரெயிலில் மொத்தம் 6 பெட்டிகள் இருந்தன. 5 பெட்டிகளில் பயணிகளும் ஒரு
பெட்டியில் குடிநீரும் இருந்ததாம். பயணிகள் இருந்த 5 பெட்டிகளுமே கவிழ்ந்தது. தண்டவாளத்தை
விட்டு அவை கீழே இறங்கி கவிழ்ந்து கிடந்தன. </p><p>
</p><p>
பயணிகள் தப்பினர் </p><p>
</p><p>
ரெயில் தண்டவாளத்தை விட்டு கீழே இறங்கியதும் பயணிகள் அனைவரும் அலறியடித்துக்கொண்டு கீழே
குதித்தனர். அதிர்ஷ்டவசமாக யாரும் காயம் அடையவில்லை. </p><p>
</p><p>
நாச வேலை காரணமா? </p><p>
</p><p>
இந்த ரெயில் எப்படி கவிழ்ந்தது? ஏதாவது எந்திர கோளாறு காணமாக கவிழ்ந்ததா? அல்லது
நாச வேலை காரணமாக கவிழ்ந்ததா? என்று ரெயில்வே போலீசார் விசாரணை நடத்தி
வருகிறார்கள். </p><p>
</p><p>
கூடுதல் மேலாளர் </p><p>
</p><p>
இந்தவிபத்து பற்றி தகவல் அறிந்ததும் திருச்சியில் கோட்ட கூடுதல் ரெயில்வே மேலாளர்
குணாளன், கோட்ட பாதுகாப்பு அதிகாரி ராமன் மற்றும் ரெயில்வே பாதுகாப்பு படை
அதிகாரிகள், மீட்்பு குழுவினர் திருவாரூருக்கு விரைந்து உள்ளனர். </p><p>
</p><p>
போக்குவரத்து பாதிப்பு </p><p>
</p><p>
கவிழ்ந்து கிடக்கும் ரெயிலை மீட்பதற்கான பணிகள் துரிதமாக நடந்து வருகிறது. இந்த விபத்து
காரணமாக கொல்லம்-நாகூர் ரெயில் போக்குவரத்தும் , மற்றும் சில பாசஞ்சர்
ரெயில்களின் போக்குவரத்தும் பாதிக்கப்பட்டு உள்ளது. </p>

<p> </p>

<p>தனுஷ்கோடிக்கு படகில் 7
பேர் வந்தனர்: சிங்கள ராணுவத்தின் தொல்லை தாங்காமல் தப்பி வந்தோம்: இலங்கை அகதி
தகவல்</p>

<p> </p>

<p>ராமேசுவரம், ஆக.13- இலங்கையில் விடுதலைப் புலிகளுக்கும்,
ராணுவத்தினருக் கும் கடும் சண்டை நடந்து வரு கிறது. இதனால் அங்கு வாழும் தமிழர்கள் தங்கள்
உயிரை காப்பற்றிக் கொள்ள தமிழகம் நோக்கி அகதிகளாக வந்த வண்ணம் இருந்தனர். இதற்
கிடையே கடந்த 24-ந் தேதி அகதிகளாக தமிழகம் வந்த தமிழர்களின் படகு இலங்கை கடல்
பகுதியில் கவிந்தது. இதில் 21 அகதிகள் உயிர் இழந்ததாக கூறப்பட்டது. இதைத் தொடர்ந்து
தமிழகத்திற்கு அகதிகள் வருகை குறைந்து இருந்தது. </p><p>
</p><p>
மீண்டும் 7 பேர் </p><p>
</p><p>
இந்த நிலையில் இலங்கை யில் உள்ள பேச்சாலையில் இருந்து 2 குடும்பத்தைச் சேர்ந்த 4
ஆண்கள், 3 பெண்கள் உள் பட 7 பேர் நேற்று (சனி) அதி காலை தனுஷ்கோடி அருகே உள்ள
தவுக்காடு என்ற பகுதிக்கு வந்தனர். மன்னாரில் உள்ள நூறுகுடியிருப்பு காலனியை சேர்ந்த
இவர்கள் படகோட் டிக்கு தலா ரூ.10 ஆயிரம் கொடுத்து வந்துள்ளார்கள். தவுக்காடு வந்த
அகதிகள் அங்கி ருந்து தனுஷ்கோடி போலீஸ் நிலையத்தை அடைந்தனர். </p><p>
</p><p>
இதைத் தொடர்ந்து போலீ சார் மற்றும் புலனாய்வுத் துறையினர் அகதிகளிடம் தீவிர விசாரணை
நடத்தினார்கள். அதன் பின்னர் அவர்கள் மண்டபம் அகதிகள் முகாமிற்கு அனுப்பி
வைக்கப்பட்டார்கள். </p><p>
</p><p>
நேற்று அகதியாக வந்த சுகீந்தர் (வயது 21) கூறியதாவது:- </p><p>
</p><p>
இலங்கையில் உள்ள வன்னி யில் எனது தந்தை கனகலிங்கம் கால்நடை மருத்துவராக வேலை பார்த்து
வருகிறார். மன்னார் காலனியில் நான் எனது அம்மா, 2 தம்பி, தங்கையுடன் வசித்து
வருகின்றேன். இலங்கை புலனாய்வுத் துறையினர் எங்களுக்கும், போராளி இயக்கத்திற்கும்
தொடர்பு உள்ளது என கூறி, எங்களை பிடித்து செல்வதும், பின்னர் திருப்பி அனுப்பு வதுமாக
உள்ளனர். இதனால் என் தம்பி, தங்கையின் படிப்பு பாதியில் நின்று விட்டது. </p><p>
</p><p>
நிம்மதியாக வாழ வந்தோம் </p><p>
</p><p>
மேலும் வன்னிக்கு சென்று எனது தந்தையுடன் சேர்ந்து வாழவும் அரசு அனுமதிக்க வில்லை. வன்னி
பகுதிக்கு சென் றால் அப்பகுதியில் உள்ள போராளி இயக்கத்தினர் தங்கள் இயக்கத்தில் சேர
சொல்லி வற்பு றுத்துகின்றார்கள். </p><p>
</p><p>
இதனால் அவர்களிடம் இருந்து உயிர் தப்பி நிம்மதியாக வாழ இந்தியாவுக்கு அகதிகளாக வந்
தோம். </p><p>
</p><p>
இவ்வாறு அவர் கண்ணீர் மல்க கூறினார். </p>

<p> </p>

<p>90-வது பிறந்த நாள் விழா:
வேதாத்திரி மகரிஷியின் கோட்பாடு உலகில் அமைதியை ஏற்படுத்தும்: பேரூர் மருதாசலம் அடிகளார்
பேச்சு</p>

<p> </p>

<p>கோவை,ஆக.13- பொள்ளாச்சி அருகே உள்ள ஆழியாரில் அறிவு
திருக்கோவில் நடத்தி உலகம் முழுவதும் அருட்பணி ஆற்றி வரும் வேதாத்திரி மகரிஷியின் 90-வது
பிறந்த நாள் உலக அமைதி தின விழாவாக 3-நாள் கொண்டாடப்படுகிறது. முதல் நாள் ஆழியாரில்
உள்ள அறிவுத்திருக்கோவிலில் நேற்று (சனி) காலை தொடங்கியது. அறிவுத்திருக்கோவில்
கொடியை வேதாத்திரி மகரிஷி ஏற்றி வைத்தார். விழாக்குழு தலைவர் பொள்ளாச்சி
நா.மகாலிங்கம் குத்துவிளக்கு ஏற்றினார். </p><p>
</p><p>
பிறந்த நாள் விழா </p><p>
</p><p>
பிறந்தநாள் விழாவில் முதல் நிகழ்ச்சியாக வேதாத் திரிய இலக்கிய கருத்தரங்கு நடந்தது.
பேரூர் ஆதீதனம் இளையபட்டம் மருதாசல அடிகளார் தலைமை தாங் கினார். வேதாத்திரி மகரிஷி
முன்னிலை வகித்தார். எம்.கார்த்திகேயன் வரவேற்று பேசினார். </p><p>
</p><p>
பேரூர் மருதாலசல அடிகளார் பேசியது வருமாறு:- </p><p>
</p><p>
கடந்த 1984-ம் ஆண்டு இங்கு அறிவுத்திருக்கோவில் என்ற ஆலமரத்துக்கு சிறிய விதை
விதைக்கப்பட்ட போது, ராமாயணத்தில் அணில் தொண்டாற்றியதுபோல சிறிய தொண்டாற்றும்
வாய்ப்பு, அரும்பேறு பெற்றவன் நான். அருட்தந்தை வேதாத்திரி மகரிஷி காட்டிய வழியில்
ஈடேற்றம் வந்ததால் இந்த அளவு எனது நிலை உயர்ந்து உள்ளது. </p><p>
</p><p>
அருளாளர் </p><p>
</p><p>
உலகத்தில் எப்போது அமைதியின்மை தலை தூக்குகிறதோ அப்போது அமைதியை ஏற்படுத்தும்,
அருளாளர்கள் தோன்றுவார் கள். ஐக்கிய நாடுகள் சபை அமைக்கப்பட்டு 54 ஆண்டுகள் ஆன
பின்னரும் இனி உலகில் அமைதி தோன்றுமா? அல்லது கற்பனை கோட்டையாக போய்விடுமா?
என்ற அச்சம் உள்ளது. </p><p>
</p><p>
வேதாத்திரி மகரிஷி உலக அமைதி காண வழி காட்டும் உத்தமர் தனது 90-வது வயதிலும், உலக
அமைதிக்காக அயராது உழைத்து வரக்கூடிய வர். அவர் உலகப்பொது நெறிகள் கொண்ட ஒரு சமயத்தை
உருவாக்கி சமய பூசல்கள் கூடாது என்ற புது நெறி கண்டுள்ளார். </p><p>
</p><p>
புதிய கோட்பாடு </p><p>
</p><p>
உலகில் அமைதி எற்படுத்து வதற்காக வேதாத்திரி மகரிஷி முன்பு 7 நெறிகள் கொண்ட
கோட்பாட்டை உருவாக்கினார். ஆனால் தற்போது உலகில் அவலம் பெருகி வருவதால் உலக
அமைதிக்காக 14 நெறிகள் கொண்ட கோட்பாட்டை தந்து உள்ளார். இந்த கோட்பாடு உலகில்
நிச்சயம் அமைதியை உருவாக்கும். </p><p>
</p><p>
கலாச்சாரம் </p><p>
</p><p>
கலாச்சாரம் என்பது ஒரு வார்த்தை அல்ல. கலை, ஆச்சாரம் என்ற இரண்டு வார்த்்்்தைகள் என்று
வேதாத்திரி மகரிஷி அடிக்கடி கூறுவார். இந்த நூற்றாண்டில் கலை படிப்படியாக வளர்ந்து
தொழில்துறை, அறிவியல் துறை, தொழில் நுட்ப துறையில் மண்ணுலகம் பயன்படும் வகையில்
சாதனைகள் செய்து வருகிறது. ஆனால் கலைக்கு ஈடுகொடுக்கும் வகையில் ஆச்சாரம் வளரயில்லை
என்று வேதாத்திரி மகரிஷி வேதனையோடு சொல்வார். அதனால் தான் இந்த சமுதாயம் முழுமையான
சமுதாயமாக இல்லாமல் ஊனமுற்ற சமுதாயமாக உள்ளது. இந்த ஊனமுற்ற சமுதாயத்தை மீட்க வேதாத்திரி
மகரிஷி புதிய சமயத்தை பயன்டுத்தி இருக்கிறார். </p><p>
</p><p>
புதிய சமயம் </p><p>
</p><p>
இந்த சமயம் எல்லா சமய கருத்துக்களையும தன்னகத்தே உள்ளடக்கிய சமயம். எல்லா
சமயத்துக்கும் அறநெறி, இறை நெறி என்பவை இரண்டு இறக்கைகள். உலகில் அமைதி மலர்ந்து நல்ல
நிலை ஏற்பட புத்த, சமண , சைவ சித்தாந்த வேதாந்த நெறிகளை ஒன்றிணைத்து புதிய சமயத்தை
ஏற்படுத்தி வேதாத்திரி மகரிஷி ஏற்படுத்தி இருக்கிறார். </p><p>
</p><p>
இந்த சமயம் வறுமை நோய் போக்க ஒரு தீர்வாகும். வறுமை என்றால் பொருள் வறுமை அல்ல. அறிவு
வறுமை என்று வேதாத்திரி மகரிஷி அடிக்கடி சொல்வார். </p><p>
</p><p>
மந்திரம் </p><p>
</p><p>
மக்களின் மனதில் அமைதி என்கிற விதை வளர்்ந்தோங்கி உலகம் அமைதி காண மகரிஷி இரண்டு
மந்திரம் தந்து இருக்கிறார். வாழ்க வையகம். வாழ்க வையகம். வாழ்க வளமுடன் என்ற
மந்திரத்தின் துணை கொண்டு நாம் உலகத்தை பற்றியுள்ள அவலத்தை போக்க முடியும்.
சமுதாயத்தில் தான் என்ற சிந்தனை மாறி உலகம் என்று விரிந்து வளர்ந்த நிலை மீண்டும்
ஏற்படும். யாதும் ஊரே, யாவரும் கேளீர் என்ற பழைய உயர்ந்த நிலையை அடைவோம். </p><p>
</p><p>
உலக அமைதி </p><p>
</p><p>
அறிவியல் கண்டு பிடிப்புகள் உலகை சுருக்கி உள்ளது. இந்த நூற்றாண்டு மனித உள்ளத்தை சுருக்கி
உள்ளது. மனித உள்ளம் விரிந்து இருக்குமேயானால் சமுதாயத்தில் நல்ல மாற்றம் ஏற்படும்.
உலகில் அமைதி ஏற்படும் வேதாத்திரி மகிரிஷி யின் கோட்பாடுகளால் அவரது காலத்திலேயே
உலகில் அமைதி மலரும். அதை அவர் கண்டு மகிழும் நிலை ஏற்படும். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
கருத்தரங்கு </p><p>
</p><p>
தொடர்ந்து நடந்த வேதாத்திரிய இலக்கிய கருத்தரங்கில் பேராசியர்கள், வேதசுப்பையா,
எம்.கே.தாமோ தரன், கனக கே.ஜி.சாமி, உழவன் தங்கவேலு, கே.பெருமாள், மீனா
சத்தியமூர்த்தி, எஸ்.எம்.எம்.வி.கிருஷ்ணராஜ், டாக்டர் கே.ஷி.அனந்த நாராயணன் ஆகியோர்
பேசினார்கள். </p><p>
</p><p>
பிற்பகலில் நடந்த கருத்தரங்கில் மனோரமா சின்னசாமி, பேராசிரியர்கள் எஸ்.லட்சுமணன்,
ஜெயலால் மோகன், புலவர் க.தியாக ராஜன், ஏ.ஏ.எஸ்.ராதாகிருஷ் ணன், ஆர்.ராஜாராம்,
சுகந்தா சுவாமி, வி.எஸ்.இளமுருகன் ஆகியோர் பேசினார்கள். முடிவில் உலக சமுதாய சேவா சங்க
துணைசெயலாளர் பேராசிரியர் பி.பரமசிவன் நன்றி கூறினார். </p><p>
</p><p>
யார்,யார்? </p><p>
</p><p>
கருத்தரங்கில் ஆழியார், அறிவுத்திருக்கோவில் அறங் காவலர்கள் எஸ்.கே.எம்.மயி
லானந்தம் , எஸ்.வி.பாலசுப்பிர மணியம், எம்.சின்னசாமி, ஆர்.பச்சையப்பன், உலக சமுதாய
சேவா சங்க செயலாளர் டி.என்.அஜ்ஜய கவுடர், கோவை பாரதிய வித்யா பவன் தலைவர்
கிருஷ்ணராஜ் வாணவராயர் அன்னபூர்ணா தாமோதரசாமி நாயுடு உள்பட ஏராளமானவர் கள் கலந்து
கொண்டனர். </p>

<p> </p>

<p>ரூ.2 கோடி விமான பாகம்
திருடிய விமான அதிகாரி</p>

<p> </p>

<p>மும்பை, ஆக.13- பாட்னாவில் விமானம் மோதி நொறுங்கி 50 பேர்
பலியானார்கள். டெல்லியில் விமானப்படை விமானம் நொறுங்கியது. </p><p>
</p><p>
இப்படி விமானங்கள் விழுந்து நொறுங்குகின்றன. இந்த நிலையில் விமானங்களில் முக்கிய
பாகங்களை விமான கம்பெனி அதிகாரியே திருடி விற்றது கண்டுபிடிக்கப் பட்டு உள்ளது. </p><p>
</p><p>
தொடர்பு </p><p>
</p><p>
போயிங்-747 ரக விமானத்தில் இருந்து ஒலிபரப்பு கருவி, டேப் கட்டுப்பாட்டு கருவி, ஆம்னி
பர்க், டேட்டா ரெக்கார்டர் திருடப்பட்டது. இவை விமானத்தை ஓட்டும் பைலட், விமான நிலைய
கட்டுப்பாட்டு அறையுடன் தொடர்பு கொள்வதற்கு முக்கியமான கருவிகள் ஆகும். </p><p>
</p><p>
இந்த கருவிகளை திருடியவர் ஏர்-இந்தியா உதவி டெக்னீசியன் ஆவார். அவரது பெயர் அமிக்டன்
தக்சீரா (வயது44). </p><p>
</p><p>
கடைசி ஆளாக </p><p>
</p><p>
அமிக்டன் ரொம்ப சாதாரணமாக இந்த திருட்டு வேலையை செய்து உள்ளார். விமானம்
ஏர்போர்ட்டில் நிறுத்தப்பட்டு இருக்கும் நேரத்தில் இந்த தகவல் தொடர்பு கருவிகளை
அமிக்டன் கழற்றி விடுவார். விமானநிலைய பாதுகாப்பு சோதனையில் இருந்து தப்புவதற்காக
அவர் எல்லோரும் போன பிறகு போவார். ஊழியர்கள் வீட் டுக்கு திரும்பும் கம்பெனி பஸ்
கிளம்பிய உடனே ஓட்டம் ஓட்டமாக கடைசி ஆளாக வந்து ஏறுவார். கடைசி நேரத்தில் ஓடி வருவதால்
அவர் திருடிக் கொண்டு போகும் கருவிகள் சோதனையில் பிடிபடாமல் தப்பிவிடும். </p><p>
</p><p>
ஆனால் அமிக்டனின் இந்த திருட்டை துணை போலீஸ் கமிஷனர் பிரதீப் சவந்த் கண்டுபிடித்தார்.
அமிக்னிடம் இருந்து ரூ.2 கோடி மதிப்புள்ள விமான கருவிகள் கைப்பற்றப்பட்டன. அமிக்டனுக்கு
விமான பாதுகாப்பு அதிகாரிகளின் உதவி இருக்கும் என்று சவந்த் விசாரித்து வருகிறார். </p>

<p> </p>

<p>தீவிரவாதம் முறியடிக்க
வாஜ்பாய் அரசுக்கு தி.மு.க. துணை நிற்கும்: மாநிலங்களவையில் திருச்சி சிவா பேச்சு</p>

<p> </p>

<p>புதுடெல்லி,
ஆக. 13- பிரதமரின் அறிக்கை மீது மாநிலங்களவையில் நடந்த விவாதத்தில் திருச்சி சிவா
பேசியதாவது:- </p><p>
</p><p>
தி.மு.க. துணை நிற்கும் </p><p>
</p><p>
நான் சார்ந்திருக்கும் கட்சி யான தி.மு.க. வன்முறையும், தீவிரவாதமும் நாட்டின் எந்தப்
பகுதியில் தலையெடுத்தாலும் உடனடியாக கண்டிக்கின்ற இயக்கம். இந்த அவையில் உள்ள மற்ற
உறுப்பினர்கள் வௌிப் படுத்திய கருத்துக்களோடு என் உணர்வுகளையும் இணைத்து இதுவரை அரசு
எடுத்திருக்கும் முயற்சிகளுக்கும், இனி எடுக்க விருக்கும் முயற்சிகளுக்கும் நாங் கள் ஆதரவு
தந்து நிச்சயமாக துணை நிற்போம் என்பதை உறுதி கூற விரும்புகிறேன். </p><p>
</p><p>
நான் பிரதமரிடமிருந்து தெரிந்துகொள்ள விரும்புவது ஒன்றுதான். தங்களுடைய அறிக்கையில்
நடைபெற்ற இந்த கொடுமைக்கு வௌிநாட்டி லிருந்து ஊடுருவி வந்திருக் கக்கூடிய தீவிரவாதிகள்தான்
காரணம் என பாதுகாப்புத் துறையினர் தௌிவுபட தெரிவித்திருப்பதாக கூறியிருக்கிறீர்கள். </p><p>
</p><p>
உளவுத்துறை </p><p>
</p><p>
இரண்டு தினங்களுக்கு முன் னாள் இதே அவையில் உள் துறை அமைச்சர் எல்.கே.அத் வானி
உறுப்பினர்களின் சந் தேகங்களுக்கு விளக்கம் தரும் போது அமர்நாத் யாத்ரீகர் களின்
பாதுகாப்பிற்காக ராணு வம் நிறுத்தப்பட்டுள்ளதாக கூறி னார். இவ்வளவு பெரிய பலம்
பொருந்திய பாதுகாப்பிற்குப் பின்னும் இந்த துர்பாக்கிய நிகழ்ச்சி நடைபெற்றிருக்கிறது
என்றால் நம்முடைய நாட்டின் உளவுத்துறை பலப்படுத்தப்பட வேண்டியது அவசியம் என்று இதன்மூலம்
நமக்கு புலப்படு கிறது. </p><p>
</p><p>
தீர்வு </p><p>
</p><p>
தீவிரவாதத்தை எதிர்த்து நடத்தும் யுத்தம் தொய்வில்லா மல் தொடரும் என்றும் ஜம்மு
காஷ்மீரில் அமைதி திரும்ப எடுக்கும் முயற்சிகள் கைவிடப் படாது என்றும் பிரதமர் தெரி
வித்திருப்பது மகிழ்ச்சி தருவ தாக இருக்கிறது. இது அனைவரின் பாராட்டினையும் பெறக்கூடிய
ஒன்று. மத்திய அரசு திட்டமிட்டுள்ள மூன்று கட்ட நடவடிக்கை திட்டத்தை நான் முழுமையாக
ஆதரிக்கிறேன். 1. தீவிரவாதத்தை அறவே அகற்ற நடவடிக்கை. 2. பலம் பொருந்தியுள்ள
தீவிரவாதிகளுடன் பேச்சுவார்த்தை மூலம் தீர்வுகாணும் முயற்சி, 3. ஜம்மு காஷ்மீர் மக்களின்
பொருளா தாரத்தை மேம்படுத்த முழு முயற்சி, காரணம் இது மட்டுமே பிரச்சினைக்கு சரியான தீர்
வாக இருக்கும் என்பதுதான். </p><p>
</p><p>
வன்முறைக்கு எதிரான இந்த அரசின் தளர்வற்ற போர் தொடரட்டும். நம்பலம் அனைத்தையும்
ஒன்று திரட்டி தீவிர வாதத்தை முறியடிப்போம். எங்கள் ஒத்துழைப்பு இந்த அரசின் இதுபோன்ற
முயற்சி களுக்கு என்றும் எப்பொழுதும் உண்டு. பிரதமரின் மன உறுதியும், நாட்டு மக்களின்
ஒற்றுமையும் எங்களைப் போன்றோரின் ஒத்துழைப்பும் நிச்சயமாக வெற்றிபெறும். </p><p>
</p><p>
இவ்வாறு பேசினார். </p>

<p> </p>






</body></text></cesDoc>