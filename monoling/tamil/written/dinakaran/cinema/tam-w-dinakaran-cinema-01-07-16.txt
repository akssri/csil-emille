<cesDoc id="tam-w-dinakaran-cinema-01-07-16" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-01-07-16.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-07-16</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-07-16</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>டைரக்டர் மணிரத்னத்துக்கு
திடீர் நெஞ்சு வலி, மருத்துவமனையில் அனுமதி</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>




 
  
  <p>மணிரத்னம் </p>
  
 




<p>சென்னை, ஜூலை 16- டைரக்டர் மணிரத்னத்திற்கு நேற்று
நெஞ்சுவலி ஏற்பட்டதால் மருத்துவமனையில் அனுமதிக்கப்பட்டார். அவரது உடல்நிலை தேறி
வருகிறது. கமல், பாரதிராஜா நலம் விசாரித்தனர். </p><p>
</p><p>
மவுனராகம், உயிரே, பம்பாய், ரோஜா, அலைபாயுதே ஆகிய படங்களை இயக்கியவர் பிரபல
இயக்குனர் மணிரத்னம். நேற்று இவர் வீட்டில் படுத்து தூங்கிக்கொண்டிருந்தார். காலை 5.30
மணி அளவில் அவருக்கு திடீரென்று நெஞ்சு வலி ஏற்பட்டது. இதுபற்றி அருகில் இருந்த அவரது
மனைவி சுஹாசினிக்கு தெரிந் ததும் உடனடியாக மணி ரத்னத்தை சென்னை அப்பல்லோ மருத்துவமனைக்கு
அழைத்துச் சென்றார். </p><p>
</p><p>
அங்கு டாக்டர்கள் மணி ரத்னத்திற்கு ஈ.சி.ஜி, பி.பி. பரிசோதனைகள் நடத்தினார்கள்.
பின்னர் தகுந்த சிகிச்சை அளித்து தீவிர கண் காணிப்பு பிரிவில் அனுமதித்தனர். தற்போது
அவரது உடல்நிலை தேறி வருகிறது. இன்னும் இரண்டு நாட்களுக்கு அவர் மருத்துவமனையில் தங்கி
ஓய்வு எடுக்க வேண்டும் என்று டாக்டர்கள் கூறி உள்ளனர். </p><p>
</p><p>
கமல்-பாரதிராஜா </p><p>
</p><p>
மணிரத்னத்தை சுஹாசினி அருகில் இருந்து கவனித்து வருகிறார். மணிரத்னம் மருத்து வமனையில்
அனுமதிக்கப்பட்ட தகவல் அறிந்ததும் நடிகர்கள் கமல்ஹாசன், மாதவன், பிரகாஷ்ராஜ்,
டைரக்டர் பாரதி ராஜா, நடிகை சிம்ரன் உள்பட ஏராளமான திரையுலக பிரமுகர்கள் நேரில் வந்து
பார்த்து நலம் விசாரித்தனர். </p>

<p> </p>






</body></text></cesDoc>