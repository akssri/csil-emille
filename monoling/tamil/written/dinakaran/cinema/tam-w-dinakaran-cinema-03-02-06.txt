<cesDoc id="tam-w-dinakaran-cinema-03-02-06" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-03-02-06.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-02-06</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-02-06</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>கே.எஸ்.ரவிகுமார்
இயக்கத்தில் சரத்குமார் நடிக்கும் பாறை படம் தொடக்கம்: பட அதிபர்கள் - நடிகர் -
நடிகைகள் வாழ்த்து</p>

<p> </p>

<p>சென்னை, பிப். 6- கே.எஸ்.ரவிகுமார் இயக்க சரத்குமார் எம்.பி.
நடிக்கும் பாறை புதிய பட விழாவில் பட அதிபர்கள், நடிகர், நடிகைகள் கலந்துகொண்டு
வாழ்த்தினார்கள். </p><p>
</p><p>
வெற்றி கூட்்டணி </p><p>
</p><p>
டைரக்டர் கே.எஸ்.ரவிகுமார், சரத்குமார் இணைந்து சேரன் பாண்டியன், நாட்டாமை, நட்புக்காக
என வெற்றி படங்களை வழங்கிய வெற்றி கூட்டணி தற்போது பாறை படம் மூலம் மீண்டும்
இணைந்துள்ளது. இதில் ஜெயராம், ரம்யா கிருஷ்ணன் உள்பட பலர் நடிக்கின்றனர். மாஸ் மூவி
மேக்கர்ஸ் சார்பில் எஸ்.எஸ்.துரைராஜ் தயாரிக்கிறார். சமுத்திரம் சபேஷ்- முரளி இசை
அமைக்கின்றனர். </p><p>
</p><p>
தொடக்கம் </p><p>
</p><p>
இப்படத்தின் தொடக்கவிழா ஏவி.எம்.ஸ்டுடியோவில் உள்ள ஜி தியேட்டரில் நேற்று நடந்தது.
இதற்காக ஏவி.எம். ஸ்டுடியோவின் முகப்பு அலங்கரிக்கப்பட்டிருந்தது. நுழை வாயில் தொடங்கி
விழா நடந்த இடம் வரை இருபக்கமும் பாறை படத்தின் வண்ண பேனர்கள் வந்தவர்களை வரவேற்றது.
விழா நடந்த இடத்திற்கு முன்பாக சிறிய மலைபோன்று பாறை மினியேட்சர்கள்
உருவாக்கப்பட்டிருந்தன. திருமண விழா போல் கெட்டி மேளம் முழங்க முழங்க பூஜை நடைபெற்றது. </p><p>
</p><p>
பட அதிபர்கள் </p><p>
</p><p>
விழாவில் பட அதிபர்கள் கே.ஆர்.ஜி., தமிழ் திரைப்பட தயாரிப்பாளர்கள் சங்க தலைவர்
கே.முரளிதரன், வி.சுவாமிநாதன், ஜி.வேணுகோபால், சித்ராலட்சுமணன், இப்ராகிம்ராவுத்தர்,
ஏ.எல். அழகப்பன், கே.ராஜன், கே.கண்ணப்பன், கே.பார்த்திபன், எடிட்டர் மோகன்,
பி.எல்.தேனப்பன், தங்கராஜ், </p><p>
</p><p>
நடிகர்கள் முரளி, விஜயகுமார், ரமேஷ் கண்ணா, ஆனந்தராஜ், தியாகு, சார்லி,
வினுசக்ரவர்த்தி, ஆர். சுந்தர்ராஜன், கே.வீரமணி, பாண்டு, போண்டாமணி, டைரக்டர்கள்
எஸ்.ஏ.சந்திரசேகரன், வி.சேகர், ஈ.ராமதாஸ், ராஜகுமாரன், நடிகைகள் ராதிகா,
விந்தியா, தேவயானி, கோவை சரளா, விது உள்பட பலர் கலந்துகொண்டு பட குழுவினருக்கு
பூச்செண்டு கொடுத்து வாழ்த்து கூறினார்கள். அனைவரையும் தயாரிப்பாளர் எஸ்.எஸ்.துரைராஜ்,
பி.ஆர்.ஓ. நெல்லை சுந்தர்ராஜன், பாலன் ஆகியோர் வரவேற்றனர். </p>

<p> </p>

<p> </p>

<p>ரிமோட்டுடன் 48 அடி
உயரமுள்ளது: ஆங்கில படங்களில் பயன்படுத்தும் ராட்சத கிரேன் - காமிரா தமிழில் பயன்படுத்த
பிரசாத் ஸ்டுடியோ அறிமுகம்</p>

<p> </p>

<p>சென்னை, பிப். 6- ஆங்கில படங்களில் பயன்படுத்தப்படும் 48 அடி
உயர ரிமோட் கட்டுப்பாடுடன் கூடிய ராட்சத கிரேன், காமிராவை தமிழ் படங்களில் பயன்படுத்துவதற்காக
பிரசாத் ஸ்டுடியோவில் அறிமுகப்படுத்தப்பட்டது. </p><p>
</p><p>
தொழில் நுட்பம் </p><p>
</p><p>
தமிழ் படங்களில் தற்போது கம்ப்யூட்டர் தொழில் நுட்பம் அதிக அளவில்
பயன்படுத்தப்படுகிறது. ஆனால் உயரமான ஹெலிகாப்டர் கோணங்கள், ஜேம்ஸ் பாண்ட் படங்களில்
வேகமாக செல்லும் கார்களில் சாதனை புரிந்தபடி சண்டையிடும் காட்சிகளை போல் தமிழில்
எடுப்பதற்கு போதுமான தொழில்நுட்ப கருவி, காமிரா இல்லாமல் இருந்தது. அந்த குறை தற்போது
நிவர்த்தி செய்யப்பட்டு உள்ளது. </p><p>
</p><p>
சென்னையில் உள்ள பிரசாத் ஸ்டுடியோவில் கோடிக்கணக்கான ரூபாய் மதிப்புள்ள ராட்சத
கிரேன் மற்றும் காமிராக்களை தருவித்துள்ளது. அதன் செய்முறை விளக்கம் நேற்று
தயாரிப்பாளர்கள், நடிகர்கள், இயக்குனர்களுக்கு செய்து காட்டப்பட்டது. </p><p>
</p><p>
ராட்சத கிரேன் </p><p>
</p><p>
48 அடி உயரமுடன் ரிமோட் கட்டுப்பாடு வசதியுடன் கொண்ட கிரேன் இயக்கப்பட்டது. மிகவும்
கடுமையான சூழலில் கூட துல்லியமாக வேலை செய்யக் கூடியது. இவ்வளவு பெரிய கருவியை தயார்
நிலைக்கு கொண்டு வரச் செய்ய 30 நிமிடங்கள் போதும். இந்த கிரேனுக்கு கிராப் லாங்
ரேஞ்சர் என்று பெயர். </p><p>
</p><p>
ப்ளோகோ என்ற டிஜிட்டல் ப்ரோசண்ட் ஒளிக்கருவி நீலம் மற்றும் பச்சை நிற ஒளிகளை
படமெடுத்தலுக்கு மிகச்சிறந்தது. சமமான கலர் வெப்ப நிலை தரக்கூடியது. </p><p>
</p><p>
லெவல் 7 டைம் லாப்ஸ் கருவி என்பது குறிப்பிட்ட இடைவேளைகளில் ஒன்று அல்லது பல பிரேம்களை
படம் எடுக்கும் சக்தி கொண்டது. பைரஸ் காமிரா வுடன் சேர்ந்து பல பிரேம்கள் வினாடியில்
படம் எடுக்கும் வசதி கொண்டது. </p><p>
</p><p>
2000 பிரேம் </p><p>
</p><p>
போட்டோ சோனிக் காமிரா என்ற மற்றொரு கருவி ஒரு வினாடிக்கு இரண்டாயிரம் பிரேம்வரை
படம் எடுக்கும் திறன் கொண்டது. சண்டை காட்சிகள் மற்றும் விளம்பர படம் எடுத்தலுக்கு
உகந்தது. </p><p>
</p><p>
ரமேஷ்பிரசாத் </p><p>
</p><p>
திரைப்படக் கல்லூரியில் ஆற்றல் மிக்க மாணவர்களை தேர்ந்தெடுத்து அவர்களுக்கு பிரசாத்
ஸ்டுடியோ சார்பில் பரிசு மற்றும் சலுகைகள் அளிக்கப்பட உள்ளது. இதனை பிரசாத் ஸ்டுடியோ அதிபர்
ரமேஷ்பிரசாத் நிருபர்களிடம் தெரிவித்தார். கனடா நாட்டை சேர்ந்த தொழில் நுட்ப
வல்லுனர்கள் ஜேம்ஸ் போர் லாண்ட், டான் கில்ஹான், சுரேஷ்ரோகின் ஆகியோர்
செய்முறைவிளக்கம்அளித்தனர். </p><p>
</p><p>
தயாரிப்பாளர்-இயக்குனர் </p><p>
</p><p>
நடிகர்கள் கமல்ஹாசன், பார்த்திபன், சூர்யா, சித்ரா லட்சுமணன், டைரக்டர்கள்
பாலுமகேந்திரா, ஷங்கர், பாலா, ஆர்.வி.உதயகுமார், ஸ்டண்ட் மாஸ்டர்கள் சூப்பர்
சுப்பராயன், ராக்கிராஜேஷ், மற்றும் ஏராளமான ஒளிப்பதி வாளர்கள் கலந்து கொண் டனர்.
முன்னதாக எஸ்.சிவ ராமன், பி.ஆர்.ஓ. ஜான்சன் ஆகியோர் வரவேற்றனர். </p>

<p> </p>

<p> </p>

<p>சினிமா தொழிலாளர்
சங்கம்: பெப்சி புதிய தலைவராக விஜயன் தேர்வு; வி.சுந்தரம்-பொதுச் செயலாளர்</p>

<p> </p>

<p>சென்னை, பிப். 6- சினிமா தொழிலாளர் சம்மேளனம் பெப்சி புதிய
தலைவராக எஸ்.விஜயன் போட்டி யின்றி தேர்வு செய்யப்பட்டார். வி.சுந்தரம் பொது
செயலாளராக தேர்ந்தெடுக்கப் பட்டார். </p><p>
</p><p>
பெப்சி தேர்தல் </p><p>
</p><p>
தென்னிந்திய திரைப்பட தொழிலாளர்கள் சம்மேளன புதிய நிர்வாகிகள் தேர்தல் சம்மேளன
அலுவலகத்தில் நேற்று நடந்தது. சம்மேளனத்தில் இணைந்திருக்கும் 22 அமைப்புகளின்
செயலாளர்கள் வாக்கு அளித்தனர். காலை 10 மணிக்கு ஓட்டுப் பதிவு தொடங்கி 1 மணிக்கு
முடிவடைந்தது. பின்னர் ஓட்டு எண்ணிக்கை நடந்தது. மீனாட்சிசுந்தரம் தேர்தல் அதிகாரியாக
இருந்து தேர்தலை நடத்தினார். </p><p>
</p><p>
புதிய நிர்வாகிகள் </p><p>
</p><p>
தலைவர் பதவிக்கு எஸ்.விஜயன் போட்டியின்றி தேர்வு செய்யப்பட்டார். இவர் ஏற்கனவே
பெப்சியில் தலைமை பொறுப்பு வகித்தவர் ஆவார். பொதுச்செயலாள ராக வி.சுந்தரம்,
பொருளாளராக சி.ஆர்.மணி, துணை தலைவராக என்.ராமதுரை, எஸ்.ஆர்.சந்திரன், எம்.ஏ.ராமதுரை,
ராணிடேவிட், பூவை செங்குட்டுவன், இணைச் செயலாளர்களாக எம்.ராமமூர்த்தி,
மீனாட்சிசுந்தரம், எஸ்.ஆர்.சந்திரன், ஆர்.ரவி ஆகியோர் தேர்வு செய்யப்பட்டனர். </p><p>
</p><p>
இன்றுகாலை 9 மணிக்கு பெப்சி அலுவலகத்தில் புதிய நிர்வாகிகள் பதவி ஏற்கின் றனர். </p><p>
</p><p>
கே.பாலசந்தர் </p><p>
</p><p>
கடந்த வருடங்களில் பெப்சி தலைவராக பதவி வகித்த டைரக்டர் கே.பாலசந்தர் அந்த பதவியை
தனது பணி காரணமாக ராஜினாமா செய்தார். ஆனால் அதனை சம்மேளனம் ஏற்காததால் தொடர்ந்து
பதவி பொறுப்பு வகித்தார். இந்த முறை தேர்தலில் அவர் போட்டியிடவில்லை. </p>

<p> </p>

<p> </p>

<p>ஆபாசபடத்தில் நடித்த நடிகை
சிறையில் அடைப்பு</p>

<p> </p>

<p>சென்னை,பிப்.6 ஆபாசபத்டதில் நடித்த துணை நடிகை தேவி கைது
செய்யப்பட்டு சிறையில் அடைக்கப்பட்டார். </p><p>
</p><p>
ஆபாச படம்-கைது </p><p>
</p><p>
விருகம்பாக்கத்தைச் சேர்ந்த நவீன்குமார், ஸ்டில் போட்டோ கிராபர்கோபால் ஆகியோர்
டி.வி. நடிகை, துணை நடிகை, நடன அழகி உள்பட பலதரப்பட்ட பெண் களின் நிர்வாண படத்தை எடுத்து
இண்டர்நெட் மற்றும் ஆபாச பத்திரிகைக்கு விற் பனை செய்வதாக இணைக்கமிஷனர் திரிபாதிக்கு
ரகசிய தகவல் கிடைத்தது. இதனால் துணைக்கமிஷனர் சமுத்திர பாண்டி தலைமையில்இன்ஸ்பெக்டர்
ஸ்டீபன் மற்றும் போலீசார் ரகசியமாக கண்காணித்து நவீன்குமார், கோபால் ஆகியோரை கைது
செய்தனர். அவர்களிடம் இருந்து 110 ஆபாச புகைப்படங்கள் மற்றும் ஆபாச படம் எடுத்த
300-க்கும் மேற்பட்ட பிலிம் ரோல்களை கைப்பற்றினர். </p><p>
</p><p>
டி.வி. நடிகை </p><p>
</p><p>
ஆபாச புகைப்படங்களுக்கு போஸ் கொடுத்த பெண்களை அடையாளம் கண்டு விசாரணை செய்து வந்தனர்.
இதில் டி.வி. நடிகையை போலீசார் பிடித்து விசாரித்தனர். அவரது கே. கே.நகர்வீட்டில்சோதனை
போட்டனர்.பிறகுஅந்தடி.வி. நடிகையிடம் விசாரணை நடத்தினர். ஆனால் அந்த நடிகை செக்ஸ்சாக
போஸ் கொடுத்து இருக்கிறாரே தவிர ஆபாசபடத்தில் நடிக்கவில்லை. இதனால் அவரை விட்டு
விட்டனர். </p><p>
</p><p>
சிறையில் அடைப்பு </p><p>
</p><p>
ஆபாசமாக போஸ் கொடுத்த ஒரு நடிகை விரு கம்பாக்கத்தில் ஒருவீட்டில் தலைமறைவாக இருப்பதாக
துணை கமிஷனர் சமுத்திர பாண்டிக்கு தகவல் கிடைத் தது. இதையடுத்து அவரது உத்தரவின் பேரில்
பெண் போலீஸ் உள்பட போலீசார் அந்த பெண் தங்கி இருக்கும் வீட்டை ரகசியமாக கண்காணித்து
அந்த வீட்டில் இருந்த துணை நடிகையை கைது செய்தனர். கைது செய்யப்பட்ட தேவி சைதாப் பேட்டை
9-வது குற்றவியல் நீதிமன்றத்தில் ஆஜர்படுத்தப்பட்டு சிறையில் அடைக் கப்பட்டார். </p>

<p> </p>






</body></text></cesDoc>