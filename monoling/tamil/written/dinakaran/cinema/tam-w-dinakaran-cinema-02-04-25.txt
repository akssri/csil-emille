<cesDoc id="tam-w-dinakaran-cinema-02-04-25" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-02-04-25.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-04-25</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-04-25</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>நட்சத்திர கலைவிழாவுக்கு
வர அழைப்பு: ரஜினி - விஜயகாந்த் திடீர் சந்திப்பு, மோதல் செய்திகளுக்கு முற்றுப்புள்ளி
வைத்தனர்</p>

<p> </p>

<p>சென்னை, ஏப். 25- ரஜினிகாந்த்-விஜயகாந்த் நேற்று நேருக்கு நேர்
சந்தித்து பேசினார்கள். அப்போது நட்சத்திர கலைவிழாவில் பங்கேற்க ரஜினிக்கு விஜயகாந்த்
அழைப்பு விடுத்தார். </p><p>
</p><p>
ரஜினி அரசியலுக்கு வருவார் என்று அவரது ரசிகர்களும், விஜயகாந்த் அரசியலுக்கு வருவார் என்று
அவரது ரசிகர்களும் எதிர்பார்த்துக் கொண்டிருக்கிறார்கள். இது பற்றி அவர்கள் இருவரிடமும்
கேட்டபோது ''யார் தலையில் என்ன எழுதி இருக்கிறதோ அதுதான் நடக்கும்'' என்று கூறி
உள்ளனர். </p><p>
</p><p>
சவுக்கடி வசனம் </p><p>
</p><p>
ஆனால் இருவரது படங்களிலும் அரசியல் நெடி மிகுந்த சவுக்கடி வசனங்கள் இடம் பிடித்து
வருகின்றன. இப்படியொரு பரபரப்பான சூழ்நிலையில் ரஜினியும் விஜய காந்த்தும் மோதிக்
கொண்டதாக ஒரு வார இதழில் செய்தி வௌியானது. ஆனால் அதற்கு இருவருமே மறுப்பு தெரிவித்தனர்.
</p><p>
</p><p>
தென்னிந்திய நடிகர் சங்க தேர்தல் நடந்தபோது ரஜினியும் விஜயகாந்த்தும் நேரில்
சந்தித்தார்கள். பின்னர் ஒன்றிரண்டு முறை மட்டுமே இவர்களது சந்திப்பு நடந்திரு க்கிறது.
இருவருக்கும் மோதல் என்று வந்த வதந்திக்கு பிறகு இருவரும் நேரில் சந்திக்கவில்லை. ஆனால்
தொலைபேசியில் இருவரும் பேசிக் கொண்டனர். </p><p>
</p><p>
சந்திப்பு </p><p>
</p><p>
இந்தநிலையில் நேற்று ரஜினிகாந்த்தும் விஜயகாந்த்தும் நேருக்கு நேர் சந்தித்து மனம் விட்டு
பேசிக்கொண்டார்கள். ''பாபா'' படம் எப்படி வளர்கிறது என்று விஜயகாந்த் ரஜினியிடம்
கேட்டறிந்தார். பின்னர் இருவரும் நடிகர் சங்க வளர்ச்சிக்கான திட்டங்கள் பற்றி
பேசினார்கள். </p><p>
</p><p>
நடிகர் சங்கத்தில் உறுப் பினராக உள்ள நலிவடைந்த நடிகர், நடிகைகளுக்கு உதவு வதற்காக நடிகர்
சங்கம் நடத்த உள்ள நட்சத்திர கலை விழா பற்றி அப்போது ரஜினியிடம் விஜயகாந்த்
தெரிவித்ததுடன் அதில் பங்கேற்க வேண்டும் என்று அழைப்பு விடுத்தார். அந்த அழைப்பை ரஜினி
ஏற்றுக்கொண்டதாக தெரிகிறது. </p><p>
</p><p>
மனோரமா விழா </p><p>
</p><p>
மேலும் பத்ம விருது பெற்றுள்ள நடிகை மனோரமாவுக்கு நடிகர் சங்கம் சார்பில் பாராட்டு விழா
எடுப்பது குறித்தும் அவரிடம் தெரிவித்தார். அந்த விழாவிலும் கலந்து கொள்ள வேண்டும் என்று
அவர் அழைப்பு விடுத்தார் என்று கூறப்படுகிறது. </p><p>
</p><p>
ரஜினியும் விஜயகாந்த்தும் நேரில் சந்தித்து பேசிக்கொண்டதையடுத்து இருவருக்கும் மோதல்
உள்ளது என்று வந்த செய்திகளுக்கு முற்றுப்புள்ளி வைக்கப்பட்டுள்ளது. </p>

<p> </p>

<p> </p>

<p>நடிகை ரோஜா வழக்கில்
ஐகோர்ட்டு புது உத்தரவு</p>

<p> </p>

<p>சென்னை,ஏப்.25- செக் மோசடி வழக்கில் நடிகை ரோஜாவுக்கு சென்னை
ஐகோர்ட்டு புது உத்தரவை பிறப்பித்துள்ளது. </p><p>
</p><p>
செக் மோசடி வழக்கில் நடிகை ரோஜாவுக்கு சென்னை ஐகோர்ட்டு ரூ.4 லட்சம் அபராதம்
விதித்தது. இதை தொடர்ந்து நடிகை ரோஜா கோர்ட்டில் அபராதத்தை கட்டினார். இந்த விவரத்தை
நீதிபதி கற்பகவிநாயகம் முன்பு ஆஜராகி அவரது வக்கீல் ஏழுமலை கூறினார். இதை கேட்ட
நீதிபதி இந்த வழக்கில் அபராதம் தாமதமாக கட்டப்பட்டு உள்ளது, இதுதவிர நடிகை ரோஜா
கோர்ட்டு தீர்ப்புக்கு பிறகு தற்கொலை செய்து கொள்வதாக பேட்டி கொடுத்து உள்ளார், ஆகவே
இதற்காக மனநிலை பாதிக்கப்பட்ட குழந்தைகள் உள்ள இடத்திற்கு நடிகை ரோஜா சென்று அங்கு ஒரு
நாள் அவர்களுடன் இருந்து சாப்பிட வேண்டும், அதற்கு உரிய தொகை யை நஷ்ட ஈடாக தர
வேண்டும் என்று கூறினார். இதை ரோஜா தரப்பில் ஏற்றுக் கொண்டனர். எவ்வளவு பணம்
நன்கொடையாக தர போகிறார் என்பது குறித்து ஒரு மனு தாக்கல் செய்ய நடிகை ரோஜாவுக்கு
உத்தரவிட்டு வழக்கு விசாரணையை வருகிற 29-ந் தேதிக்கு நீதிபதி தள்ளிவைத்தார். </p>

<p> </p>






</body></text></cesDoc>