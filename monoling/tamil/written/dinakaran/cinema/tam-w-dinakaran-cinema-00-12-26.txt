<cesDoc id="tam-w-dinakaran-cinema-00-12-26" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-00-12-26.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-12-26</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-12-26</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>நான் தமிழன்தான்: என்
ரத்தத்தில் பாரதியின் கவிதைகள் கலந்துவிட்டன: பாரதியாக நடித்த சாயாஜி பெருமிதம்</p>

<p> </p>

<p>சென்னை, டிச. 26- மீசைக்கவி பாரதியின் வாழ்க்கை வரலாறை பாரதி என்ற
பெயரில் ஐ.ஏ.எஸ். அதிகாரி ஞானராஜசேகரன் டைரக்டு செய்தார். </p><p>
</p><p>
பாரதியாக மராட்டிய மாநிலத்தை சேர்ந்த சாயாஜி ஷிண்டே நடித்தார். மனைவி செல்லம்மாவாக
தேவயானி நடித்தார். இளையராஜா இசை அமைத்தார். மீடியா டிரீம்ஸ் பட நிறுவனம் தயாரித்தது. </p><p>
</p><p>
100 நாட்கள் </p><p>
</p><p>
பாரதி படம் 100 நாட்கள் ஓடி வெற்றிபெற்றது. அதன் வெற்றி விழா நேற்று சென்னை ராணி சீதை
அரங்கில் கொண்டாடப்பட்டது. </p><p>
</p><p>
விழாவில் சாயாஜி ஷிண்டே, நடிகர் பார்த்திபன், எழுத்தாளர் சுஜாதா, பெண்ட்டா மீடியா
அதிபர் சந்திரசேகரன், ராஜா வைத்திய நாதன் கலந்து கொண்டனர். </p><p>
</p><p>
முதல்வர் வாழ்த்து </p><p>
</p><p>
தமிழக முதலமைச்சர் கருணாநிதி சார்பில் சாயாஜி ஷிண்டேவுக்கும்,சந்திரசேகருக்கும் மாலைகள்
அணிவிக்கப்பட்டன. </p><p>
</p><p>
பேச்சு </p><p>
</p><p>
சாயாஜி ஷிண்டே மராட்டியராக இருந்தபோதும் தமிழ் மொழியில் தனது உரையை நிகழ்த்தினார்.
அவர் கூறியதாவது:- </p><p>
</p><p>
நான் தமிழன் இல்லையென்று நினைக்கிறீர்கள். நான் தமிழன்தான். ஏனென்றால் நான்
பாரதியாக நடித்து விட்டேன். என் ரத்தத்தில் பாரதி கவிதைகள் கலந்து விட்டன.
உலகத்திலேயே மிகப்பெரிய கவி பாரதியார் வேடத்தில் நடிக்க என் வாழ்க்கையில் நான் போன
ஜென்மத்தில் செய்த பாக்யம். </p><p>
</p><p>
பாரதி பாத்திரமாக நான் நடிக்கவில்லை. என்னுள் அவர் வாசம் செய்துவிட்டார். என் வாழ்வில்
நான் சந்தித்த மிகப்பெரிய இயக்குனராக ஞான ராஜசேகரை நினைக்கிறேன். இந்த
வாய்ப்பை அளித்த அனைவருக்கும் என் நன்றி. அனைத்துக்கும் மேலாக தமிழ் ரசிகர்களுக்கு
ரொம்ப ரொம்ப நன்றி. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
இளையராஜா </p><p>
</p><p>
ஞானராஜசேகரன் கூறியதாவது:- பாரதி படத்தின் வெற்றி தமிழ் மக்கள் பாரதிக்கு தந்த
மரியாதை. பாரதி வேடத்தில் நடித்து முடித்து சாயாஜி ஷிண்டே புறப்பட்டார். விமான
நிலையத்தில் அவரை நான் வழி அனுப்ப சென்றேன். அப்போது அவர் இனிமேல் நான் சாகலாம்
ஏனென்றால் பாரதி வேடத்தில் நடித்துவிட்டேன் என்று கூறினார். </p><p>
</p><p>
இப்படத்திற்கு இசை அமைத்த இளையராஜா படத்தின் வெற்றிக்கு முக்கிய காரணம் ஆவார். </p><p>
</p><p>
சுஜாதா எழுத்தாளர் சுஜாதா பேசும் போது, இப்படத்திற்கு வரி விலக்கு வழங்கிய தமிழக அரசுக்கு
நன்றி என்றார். </p><p>
</p><p>
நடிகர் பார்த்திபன் பேசும் போது, பாரதி படத்தை பார்த்து ஒரு மாதம் அதே நினை வாக
இருந்தேன். அதற்காக எனது பங்கு ஏதாவது இருக்க வேண்டும் என்பதற்காக பத்திரிகைகளில்
அப்படத்தை பார்க்க சொல்லி விளம்பரம் கொடுத்தேன். சாயாஜி ஷிண்டே பாரதியாக
நடித்திருக்கிறார். அந்த கதாபாத்திரத்துக்கு நான் பின்னணி குரலாவது கொடுக்கலாம் என்று
எண்ணினேன். </p><p>
</p><p>
இப்படத்தில் தேவயானி செல்லம்மாவாக வாழ்ந்திருக்கிறார். இனி அவர் நடிக்கவே தேவையில்லை.
ஒரு நல்ல பாத்திரத்தில் நடித்துவிட்ட தேவயானிக்கு அவரது பெற் றோர் திருமணம் செய்து
வைத்துவிடலாம் என்றார். </p><p>
</p><p>
கேடயம் </p><p>
</p><p>
படத்தில் நடித்த நடிகர் நடிகைகளுக்கு வெற்றி கேடயம் வழங்கப்பட்டது. </p><p>
</p><p>
நடிகர்கள் சாயாஜி ஷிண்டே, நிழல்கள் ரவி, டி.பி. கஜேந்திரன், ஜூனியர் பாலையா, காந்த்,
எடிட்டர் லெனின் விஜயன், ஒளிப்பதிவாளர் தங்கர்பச்சான், நடிகை தேவயானி சார்பில் அவரது
தாய் மற்றும் நடிகை இந்து, டைரக்டர் ஞானராஜசேகரன், பாடகர் ஹரிஷ் ராகவேந்தர்,
பி.ஆர்.ஓ. நிகில் முருகன், ஆகியோர் பெற்றார்கள். </p>

<p> </p>

<p>கன்னட நடிகர் மரணம்</p>

<p> </p>

<p>பெங்களூர், டிச. 26- கன்னட சினிமாவிலும், டி.வி. நாடகங்களிலும்
நடித்து வந்த திரேந்திர கோபால் நேற்று தாவங்கரே மாவட்டத்தில் ஹரி காரில் உள்ள தனது
இல்லத்தில் மாரடைப்பால் மரணம் அடைந்தார். 60 வயதான இவர் பெரும்பாலும் வில்லன்
வேடத்தி லும், துணை வேடங்களிலும் நடித்து உள்ளார். </p>

<p> </p>

<p> </p>






</body></text></cesDoc>