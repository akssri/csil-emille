<cesDoc id="tam-w-dinakaran-cinema-03-07-09" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-03-07-09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-07-09</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-07-09</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>வருமான வரி சோதனை
பெருமையாக நினைக்கிறேன் அஜீத்குமார் பேட்டி </p>

<p> </p>

<p>சென்னை,
ஜூலை. 9- நடிகர் அஜீத்குமார் வீட்டில் நேற்று வருமான வரி அதிகாரிகள் 2 பேர் விசாரணை
நடத்தினார்கள். இதுபற்றி நேற்று ஆஞ்சனேயா படப்பிடிப்பில் நடித்துக்கொண்டி ருந்த
நடிகர் அஜீத்குமாரிடம் கேட்டபோது அவர் கூறியதாவது:- </p><p>
</p><p>
கடந்த வருடம் என் வீட்டில் வருமான வரி அதிகாரிகள் சோதனை நடத்தி னார்கள். சோதனை
முடிந்து ஒரு வருடம் முடிந்துவிட்டது. இது எல்லோருக்கும் தெரிந்த விஷயம். அன்றுமுதல் என் சொத்துக்களை
மதிப்பீடு செய்வது, விசாரணை நடத்து வது ஆகியவற்றை வருமான வரி அதிகாரிகள் செய்து
வருகின்றனர். ஒரு நல்ல குடிமகனாக அவர்களுக்கு எப்படி ஒத்துழைப்பு கொடுக்க வேண்டுமோ அதை
நானும் எனது ஆடிட்டரும் செய்து வருகிறோம். </p><p>
</p><p>
நேற்று சொத்துக்களை மதிப்பீடு செய்வதற்காக 2 அதிகாரிகள் என் வீட்டுக்கு வந்திருந்தனர்.
தகவல் கிடைத்ததும் படப்பிடிப்பில் இருந்து வீட்டுக்கு சென்று அவர்களின் கேள்விகளுக்கும்
சந்தேகங்களுக்கும் சுமார் ஒரு மணி நேரம் பதில் அளித்தேன். பிறகு மீண்டும் படப்பிடிப்பில்
கலந்துகொண்டேன். நேற்று சோதனை எதுவும் நடக்க வில்லை. வருமான வரித்துறை அதிகாரிகள்
சோதனை நடத்து வதற்காகவோ, விசாரணை நடத்துவதற்காகவோ நான் வெட்கப்பட வேண்டியதில்லை.
ஏனென்றால் இது நான் உழைத்து சம்பாதித்த பணம். யாரிடமும் கொள்ளை அடித்தது அல்ல. முன்னணி
நடிகனாக இருப்பதால் சம்பாதிப்பதால்தான் என்னிடம் விசாரிக்கிறார்கள் இது நான்
பெருமைப்பட வேண்டிய விஷயம்தான். </p><p>
</p><p>
இவ்வாறு கூறினார். 

</p>

<p> </p>

<p> </p>

<p>டெல்லியில் மலர்ந்த
காதல்: சிம்ரன்-தீபக் திருமணம் ஜனவரியில் நடக்கிறது தந்தை பேட்டி </p>

<p> </p>

<p>சென்னை, ஜூலை.9- பிரபல நடிகை சிம்ரன், தீபக் காதல் திருமணம்
பற்றி கடந்த சில வாரங்களுக்கு முன் பரபரப்பாக செய்திகள் வௌியானது. ஆனால் இதை சிம்ரன்
மறுத்தார். இந்தநிலையில் சிம்ரன்-தீபக் திருமண நிச்சய தார்த்தம் டெல்லியில் நடந்த
தாக அவரது தந்தை கூறி உள்ளார். இதுபற்றி மும்பையில் உள்ள சிம்ரனின் தந்தை அசோக்
கூறியதாவது:- </p><p>
</p><p>
தொடக்கத்தில் சிம்ரனின் காதல் பரபரப்பாக வௌியே வந்தபோது, இவளால் நமது
குடும்பத்துக்கே அவமானம் என நாங்கள் ஒதுங்கி போய் விட்டோம். கமலுடன் சுற்று கிறாள்,
இதனால் கமல்-சரிகா பிரிந்துவிட்டார்கள் என்றெல்லாம் செய்தி வந்தபோது சிம்ரன் மீது
எங்களுக்கு கடும் கோபம் ஏற்பட்டது. அவளும் இங்கே வந்து போவது கிடையாது. நாங்களும் அவளுடன்
பேசுவது இல்லை. </p><p>
</p><p>
ஒருநாள் திடீரென சிம்ரன் வீட்டுக்கு வந்தார். ஒரு இளைஞரையும் கூட்டி கொண்டு வந்து
இருந்தார். அப்பா, பழையதை எல்லாம் மறந்து என்னை மன்னித்து விடுங்கள். என்னை பற்றிய
செய்திகளுக்கு முற்றுப்புள்ளி வைக்க விரும்புகிறேன். அதற்காக இவரைத்தான் மணந்துகொள்ள
போகிறேன் என கூறி தீபக்கை எங்களுக்கு அறிமுகப்படுத்தினார். </p><p>
</p><p>
பெற்ற மகள் ஆயிற்றே, அவருக்கு ஒரு நல்ல வாழ்க்்கையை அமைத்து கொடுக்க வேண்டுமே, எனவே
மன்னித்து ஏற்றுக்கொண்டோம். இருவருக்கும் நிச்சயதார்த்தம் நடத்தி, திருமணத்தை உறுதிப்படுத்தி
கொள்ள விரும்பினேன். சிம்ரன், தீபக் இருவரும் இதற்கு ஒப்புகொண்டார்கள். அதன்படி
டெல்லியில் தீபக் வீட்டில் கடந்த மாதம் திருமண நிச்சயதார்த்தம் நடந்தது. நாங்கள்
எல்லோரும் கலந்து கொண்டோம். சிம்ரனும் வந்திருந்தார். </p><p>
</p><p>
சிம்ரன் இப்போது ஓரிரு தமிழ் படங்களில் நடித்து கொண்டு இருக்கிறார். இந்தி படம்
ஒன்றிலும் நடிக்கிறார். சில விளம்பர படங்களிலும் நடிக்க ஏற்கனவே ஒப்பந்தம் இருக்கிறது.
புதிய படங்களை சிம்ரன் ஏற்று கொள்வது இல்லை. கையில் உள்ள படங்களை நடித்து முடித்து
கொடுக்கவே 6 மாதம் ஆகும். டிசம்பருக்குள் முடித்து விடுவார் என்று நினைக்கிறேன். ஜனவரியில்
நிச்சயம் திருமணம் இருக்கும். இதற்கு தீபக் வீட்டாரும் ஒப்பு கொண்டு விட்டார்கள்.
திருமணத்துக்கு பிறகு சிம்ரன் சினிமாவில் நடிக்க மாட்டார். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
சிம்ரன் கடந்த ஆண்டு வௌிநாட்டு தூதர்கள் முன்னிலையில் நடந்த நடன நிகழ்ச்சியில்
கலந்துகொள்ள டெல்லி சென்றார். இந்த நிகழ்ச்சிக்கு பிரதமர் வாஜ்பாய் தலைமை
தாங்கினார். இந்த தருணத்தில்தான் சிம்ரன் தீபக்கை சந்தித்தார். அப்போது இருவரும்
மனம்விட்டு பேசினார்கள். இருவரும் காதலர்கள் ஆனார்கள். திருமணம் செய்துகொள்ளவும் முடிவு
செய்தார்கள் என்பது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p> </p>

<p>திருட்டு வி.சி.டி. தடுக்க
குண்டர் சட்டம் முதல்வருக்கு கே.ராஜன் வேண்டுகோள் </p>

<p> </p>

<p>சென்னை, ஜூலை 9- தமிழ் திரைப்பட தயாரிப்பாளர்கள் சங்க திருட்டு
வி.சி.டி. தடுப்பு குழு தலைவர் கே.ராஜன் தமிழக முதல்வர் ஜெயலலிதாவுக்கு ஒரு கடிதம் அனுப்பி
உள்ளார் .அதில் கூறி இருப்பதாவது:- </p><p>
</p><p>
கடந்த 5 ஆண்டுகளில் தமிழ் திரை உலகம் திருட்டு வி.சி.டி.யால் மிகவும் பாதிக்
கப்பட்டுவிட்டது. திரை அரங்கு சென்று திரைப்படம் பார்க்க வேண்டிய மக்கள் திருட்டு
வி.சி.டி.தயாரிப்பவர்களின் கொடுஞ் செயலால் வீட்டுக்குள்ளேயே படம் பார்க்க
ஆரம்பித்துவிட்டனர். இதனால் திரை அரங்குகளில் வசூல் குறைந்துவிட்டது. கோடிக்கணக்கில்
முதலீடு செய்து திரைப்படம் எடுத்த தயாரிப்பாளர்கள் கடன் சுமை யால் பாதிக்கப்பட்டு
உள்ளனர். அரசுக்கு சேர வேண்டிய கேளிக்கை வரியும் கொள்ளை அடிக்கப்படுகிறது. இந்த
இக்கட்டான சூழ்நிலை யில் தமிழக முதல்வராக நீங்கள் பொறுப்பு ஏற்ற பின்னர்தான் தமிழ்
திரை உலகில் புதுமுகங்களை வைத்து படம் எடுக்க பல தயாரிப்பாளர்கள் முன் வந்துள்ளனர்.
கடந்த இரண்டு வாரங்களில் மட்டும் புது முகங்கள் நடித்த 5 படங்கள் வியாபாரம் ஆகாத
நிலையிலும் படத்தயாரிப்பாளர்களே கஷ்டப்பட்டு சொந்தப் பொறுப்பில் ரிலீஸ்
செய்துள்ளனர். படம் மக்களின் வரவேற்பை பெற்று வசூல் அதிகரிக்கும்நேரத்தில் ஜெயம்,
காதல் கொண்டேன், விசில், ஐஸ், கோவில்பட்டி வீரலட்சுமி ஆகிய படங்களின் திருட்டு
வி.சி.டி. தமிழகத்தில் ஒவ்வொரு மாவட்டத்தில் உள்ள பர்மா பஜார் கடைகளில்
விற்கப்படுவதாக தகவல் வந்தி ருக்கிறது. இதனால்திரை அரங்குகளில் ஓடிக்கொண்டிருக்கும் புதிய
படங்களின் வசூல் மிகவும் பாதிக்கப்படுகிறது. ஆகவே தமிழகத்தில் தொடர்ந்து திருட்டு
வி.சி.டி. தயாரிப்பவர்கள், விற்பவர்களை குண்டர் சட்டத்தில் கைது செய்தால்தான் தமிழ்
திரை தொழில் காப்பாற்றப்படும். சென்னையை பொறுத்த வரை காவல்துறை ஆணையாளர் மிகச் சிறப்பாக
செயல் பட்டு திருட்டு வி.சி.டி. தயாரிப் பவர்கள், விற்பவர்கள்்மீது கடும் நடவடிக்கை எடுத்து
வருகிறார். சமீபத்தில்கூட அண்ணாநகர் துணை ஆணையாளர் திருட்டு வி.சி.டி. தயாரிக்கும்
கூட்டத்தை கைது செய்திருக்கிறார் என்ற செய்தியை கேட்டு தமிழ்்திரைப்படத்
தயாரிப்பாளர்கள் சங்கத்தின் சார்பாக எங்கள் மனமார்ந்த பாராட்டுகளை தெரிவித்துக்
கொள்கிறோம். திரை உலகை அழிக்கும் திருட்டு வி.சி.டி. தயாரிக்கும் தீய சக்திகளை ஒடுக்க
சட்டப்பேரவையில் குண்டர் சட்டம் கொண்டு வந்து தமிழ் திரை உலகை காப்பாற்ற
வேண்டுகிறோம். வேலூர் பர்மா பஜாரில் உள்ள ஒருவர் தற்சமயம் புதுப்படங்களின் திருட்டு
வி.சி.டி. தயாரிப்பில் முன்னிலையில் உள்ளார். திருட்டு வி.சி.டி. தடுப்பு காவல் துறை
அதிகாரிகள் அவர்களை உடனே கைது செய்திட உத்தரவிட வேண்டுகிறேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறி உள்ளார். </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p>புது வீட்டுக்கு தண்ணீர்
ஊற்றியபோது மாடியிலிருந்து விழுந்த நடிகர் சேகர் சாவு மண்டை உடைந்து பரிதாபம் </p>

<p> </p>

<p>சென்னை, ஜூலை. 9- புது வீட்டுக்கு தண்ணீர் ஊற்றியபோது
மாடியிலிருந்து விழுந்த நடிகர் சேகர் சாவு மண்டை உடைந்து பரிதாபமாக செத்தார். </p><p>
</p><p>
மணிப்பயல், வா ராஜா வா, எங்கமாமா, அனாதை ஆனந்தன், ஓ மஞ்சு, கவரி மான்,மறுபக்கம்
போன்ற படங்களில் சிறுவனாகவும், குடியிருந்த கோவில் படத்தில் எம்.ஜி.ஆரின் சிறுவயது
வேடத்திலும் நடித்திருப்பவர் மாஸ்டர் சேகர். இவர் சென்னை கோடம்பாக்கத்தில் உள்ள
டைரக்டர்ஸ் காலனி அடுக்கு மாடி குடியிருப்பில் மனைவி, குழந்தைகளுடன் கடந்த 26 வருடங்களாக
வசித்து வந்தார். சினிமாவில் அதிக வாய்ப்பு இல்லாததால் டி.வி. தொடர்களில் நடித்து
வந்தார். தாம்பத்யம் என்ற தொலைக்காட்சி தொடரில் வயதானவர் வேடத்தில் நடித்தார்.
தனியார் தொலைக் காட்சி ஒன்றிலும் பணியாற்றி வந்தார். </p><p>
</p><p>
சேகர் குடியிருக்கும் வீடு பழுதடைந்திருந்ததால் அதனை புதுப்பித்து வந்தார். அதற்கான பணி
நடைபெற்று வருகிறது. மாடியின் முன்புற சுவர் ஒட்டி அமைக்கப்பட்்ட சன்ஷேடிற்கு நேற்று மதியம்
12.30 மணி அளவில் சேகர் தண்ணீர் ஊற்றிக்கொண்டிருந்தார். அப்போது அவர் அங்கிருந்து
தவறி விழுந்தார். 40 அடி உயரத்திலிருந்து விழுந்த வேகத்தில் சேகரின் தலை கீழ்வீட்டு
காம்பவுண்டு சுவற்றில் முன்மண்டை டமார் என்று மோதி உடைந்து ரத்தம் கொட்டியது. உயிருக்கு
போராடிக்கொண்டிருந்த அவரை உடனடியாக அருகில் உள்ள தனியார் மருத்துவ மனைக்கு அழைத்து
சென்றனர். ஆனால் மருத்துவமனைக்கு செல்லும் வழியிலேயே சேகர் பரிதாபமாக மரணம் அடைந்தார்.
</p><p>
</p><p>
இந்த சம்பவம் குறித்து கோடம்பாக்கம் போலீசார் வழக்குபதிவு செய்து விசாரித்து
வருகின்றனர். சேகரின் உடல் ராயப்பேட்டை அரசு மருத்துவமனைக்கு பிரேத பரிசோதனைக்காக
அனுப்பப்பட்டு உள்ளது. இன்றுகாலை பிரேத பரிசோதனை நடக்கிறது. பின்னர் அவரது உடல்
டைரக்டர்ஸ் காலனி வீட்டிற்கு கொண்டு செல்லப்பட்டு பொதுமக்கள் அஞ்சலிக்காக
வைக்கப்படுகிறது. </p><p>
</p><p>
நடிகர் சேகரின் தந்தை ஜே.ஜி.விஜயன் பழம்பெரும் சினிமா ஒளிப்பதிவாளர் ஆவார்.
மந்திரிகுமாரி, ஆனந்த ஜோதி மற்றும் மாடர்ன் தியேட்டர்ஸ் படங்களுக்கு ஒளிப்பதிவாளராக
பணியாற்றியவர். சேகரின் அண்ணன் சுரேஷும் சினிமா ஒளிப்பதி வாளராக இருக்கிறார்.
டைரக்டர் கே.எஸ்.கோபால கிருஷ்ணன் படங்களுக்கு இவர் ஒளிப்பதிவாளராக பணியாற்றி
உள்ளார். </p><p>
</p><p>
மரணம் அடைந்த சேகருக்கு 43 வயதாகிறது. இவருக்கு சுஜாதா என்ற மனைவியும், நாகார்ஜூன் என்ற
நான்கரை வயது மகனும், அஜய் என்ற 9 மாத குழந்தையும் உள்ளனர். </p><p>
</p><p>
சேகரின் சாவு குறித்து அவரது சகோதரர் சுரேஷ் கூறும்போது, சேகருக்கு நீரிழிவு நோய்
இருக்கிறது. அதற்கான பரிசோதனைக்காக மருத்துவமனை லேபில் இன்று (நேற்று)காலை ரத்தம்
கொடுத்துவிட்டு வந்தார். புதிதாக கட்டிய சுவற்றுக்கு தண்ணீர் ஊற்றியபோது அவருக்கு மயக்கம்
ஏற்பட்டு கீழே விழுந்திருக்கிறார் என்று கூறினார். </p>

<p> </p>






</body></text></cesDoc>