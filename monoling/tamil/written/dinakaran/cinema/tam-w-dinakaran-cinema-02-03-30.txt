<cesDoc id="tam-w-dinakaran-cinema-02-03-30" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-02-03-30.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-03-30</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-03-30</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>''படையப்பா''வுக்கு முருகன்
சிலை உருவானது போல், ரஜினியின் ''பாபா''விற்கு 5 அடி உயர அம்மன் சிலை தயாராகிறது</p>

<p> </p>

<p>சென்னை, மார்ச். 30- ரஜினியின் ''பாபா'' படத்திற்கு 5 அடி உயர
அம்மன் சிலை தயாராகிறது. </p><p>
</p><p>
இமயமலையில் பல்லாயிரம் ஆண்டாக வாழும் பாபாஜி என்ற ஞானியின் வாழ்க் கையை மையமாக
வைத்து ''பாபா'' என்ற படத்தை ரஜினி காந்த் தயாரித்து நடிக்கிறார் அல்லவா? </p><p>
</p><p>
இப்படம் சமூகம் கலந்த ஆன்மீக கதையாக உருவாகிறது. ரசிகர்கள் எதிர்பார்பபுக்கு ஏற்ப
தனக்கே உரித்தான ஸ்டைல்களை ரஜினிகாந்த் செய்து நடிக்கிறார். கிண்டி கேம்பா கோலாவில்
அமைக்கப்பட்டுள்ள அரங்கில் ''பாபா'' படத்தின் படப்பிடிப்பு இடை விடாமல் தொடர்ந்து
நடந்து வருகிறது. சமீபத்தில் முரடர்களுடன் ரஜினி மோதிய பயங்கர சண்டை காட்சி ஒன்று
படமாக்கப்பட்டது. </p><p>
</p><p>
அம்மன் சிலை </p><p>
</p><p>
இப்படத்திற்காக காளி காம்பாள் கோவில் அரங்கு போடப்பட்டிருக்கிறது அல்ல வா? அந்த
கோவிலில் வைப்பதற்காக அருள் தரும் பொலிவான முகத்தோற்றத்துடன் 5 அடி உயர அம்மன் சிலை
உருவாக்கப்படுகிறது. அம்மன் சிலை உருவாக்கும் பணி முடிந் ததும் முறைப்படி படப்பிடிப்புக்காக
போடப்பட்டுள்ள அரங்கில் அம்மன் சிலை வைக்கப்படுகிறது. அன்றைய தினம் சிறப்பு பூஜையும்,
அபிஷேகமும் நடக்கிறது. </p><p>
</p><p>
லாரி டிரைவராக நடிக்கிறார் ரஜினி </p><p>
</p><p>
''பாட்ஷா'' படத்தில் ஆட்டோக்காரன் வேடத்தில் நடித்த ரஜினிகாந்த் ''பாபா'' படத்தில்
லாரி டிரைவர் வேடத்தில் நடிக்கிறார் என்று கூறப்படுகிறது. சினிமாவிற்கு வருவதற்கு முன்
பெங்களூரில் தனது தொடக்க காலத்தில் பஸ்சில் கண்டக்டராக இருந்தபோது பஸ்சையும் ஓட்ட
பழகி இருக்கிறார். இதனால் அவருக்கு லாரி ஓட்டுவதில் எந்த சிரமமும் தெரியாது. படத்தில்
ரஜினி 9 விதமான தோற்றங்களில் வருகிறார். அதற்காக வித்தியாசமான மேக்கப் அணிகிறார். </p>

<p> </p>

<p> </p>

<p>ஏவி.எம். நிறுவனம்
தயாரிக்கும் ''ஜெமினி'' பாடல் கேசட் வாங்கிய ரசிகர்களுக்கு விக்ரம் ஆட்டோகிராப்,
நூற்றுக்கணக்கில் கேசட் விற்று தீர்ந்தன</p>

<p> </p>

<p>சென்னை, மார்ச். 30- ''ஜெமினி'' பட பாடல் கேசட் வாங்கிய
ரசிகர்களுக்கு நடிகர் விக்ரம் நேரில் கையெழுத்து (ஆட்டோகிராப்) போட்டுக் கொடுத்தார்.
சில மணி நேரத்தில் நூற்றுக்கணக்கான கேசட்டுகள் விற்பனை ஆகின. </p><p>
</p><p>
ஏவி.எம். படநிறுவனம் தயாரிக்கும் புதிய படம் ''ஜெமினி''. விக்ரம் கதாநாய கனாக
நடிக்கிறார். சரண் டைரக்டு செய்கிறார். வைரமுத்து பாடல்களுக்கு பரத்வாஜ் இசை
அமைத்திருக்கிறார். எம்.சரவணன், எம்.பாலசுப்பிரமணியன், எம்.எஸ்.குகன், பி.குருநாத்
இணைந்து தயாரிக்கின்றனர். </p><p>
</p><p>
இப்படத்தின் பாடல் கேசட் தமிழகம் முழுவதும் விறுவிறுப்பாக விற்பனை ஆகிக் கொண்டிருக்கிறது.
நடிகர் விக்ரம் படத்தில் வரும் ''ஓ போடு'' என்ற பாடலை சொந்த குரலில் பாடி இருக்கிறார்.
</p><p>
</p><p>
''ஜெமினி'' பாடல் கேசட் வாங்குபவர்களுக்கு விக்ரம் ஆட்டோகிராப் போட்டு தரும் விற்பனை
விழா சென்னை ஸ்பென்சர் பிளாஸாவில் உள்ள ''மியூசிக் வேர்ல்்டு'' வளாகத்தில் நேற்று
நடந்தது. விக்ரமை காண நூற்றுக்கணக்கான ரசிகர்கள் திரண்ட னர். அவர் வந்தவுடன் போட்டி
போட்டு கை குலுக்கியதுடன் ''ஓ போடு, ஓ போடு'' என்று பாட்டு பாடி ஆரவாரம் செய்தனர். </p><p>
</p><p>
கேசட் வாங்கிய ரசிகர்களுக்கு விக்ரம் ஆட்டோ கிராப் போட்டுக்கொடுத்தார். அவரி டம்
ஆட்டோகிராப் பெறுவதற்காக ரசிகர்கள் முண்டியடித்தனர். இந்த விழாவில் தயாரிப்பாளர்கள்
எம்.சரவணன், எம்.எஸ்.குகன், டைரக்டர் சரண், நடிகர் தாமு ஆகியோர் கலந்து கொண்டனர். </p><p>
</p><p>
விக்ரம் கடைக்கு வந்திருந்த சில மணி நேரத்தில் நூற்றுக்கணக்கான கேசட்டுகள் விற்பனை
ஆகின. </p><p>
</p><p>
நுங்கம்பாக்கத்தில் இன்று </p><p>
</p><p>
இன்று மாலை 6.30 மணி முதல் 8.30 மணி வரை நுங்கம்பாக்கத்தில் உள்ள லாண்ட் மார்க்
வளாகத்தில் ''ஜெமினி'' பாடல் கேசட் வாங்குபவர்களுக்கு விக்ரம் நேரில் ஆட்டோகிராப் போட்டு
தருகிறார். வருகிற ஏப்ரல் 12-ந் தேதி படம் திரைக்கு வருகிறது. </p>

<p> </p>






</body></text></cesDoc>