<cesDoc id="tam-w-dinakaran-cinema-01-11-03" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-01-11-03.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-11-03</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-11-03</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ஸ்டார் மியூசிக்
வௌியிடும் 70-வது பட பாடல்: விஜய்யின் 'ஷாஜகான்' பாடல் கேசட் விற்பனையில் சாதனை</p>

<p> </p>

<p>சென்னை, நவ. 3- ஸ்டார் மியூசிக் வௌியிடும் 70-வது பட பாடல்
கேசட்டான விஜய் நடிக்கும் ஷாஜகான் படத்தின் பாடல் கேசட் விற்ப னையில் சாதனை
படைத்துள்ளது. </p><p>
</p><p>
சூப்பர் குட் பிலிம்ஸ் சார்பில் ஆர்.பி.சவுத்ரி தயாரிக்கும் படம் ஷாஜகான். இதில்
விஜய் கதாநாயகனாக நடிக்கிறார். கவிஞர் வைரமுத்து பாடல்கள் எழுத மணிஷர்மா இசை
அமைக்கிறார். </p><p>
</p><p>
இப்படத்தின் பாடல் கேசட் வௌியான ஒரு வாரத்திற்குள் விற்பனையில் சாதனை படைத்துள்ளது.
இதுபற்றி ஸ்டார் மியூசிக் நிறுவன உரிமையாளர்கள் பூனம் சவுத்ரி, முகம்மது அலி ஆகியோர்
கூறியதாவது:- </p><p>
</p><p>
ஸ்டார் மியூசிக் 1997-ம் ஆண்டு சூர்யவம்சம் படத்தின் ஆடியோவை முதன்முறையாக
வௌியிட்டது. முதல் கேசட்டே விற்பனையில் சாதனை படைத்தது. தொடர்ந்து ரஜினி நடித்த
படையப்பா, கமல் நடித்த ஹேராம், தெனாலி, விஜயகாந்த் நடித்த கண்ணுபட போகுதய்யா, விஜய்
நடித்த காதலுக்கு மரியாதை, துள்ளாத மனமும் துள்ளும், அஜீத் நடித்த பூவெல்லாம் உன் வாசம்,
சரத்குமார் நடித்த சமுத்திரம் உள்பட இதுவரை 70 படங் களின் பாடல் கேசட்டுகளை வௌியிட்டு
உள்ளது. இதில் 50 படங்களுக்கு மேலான பாடல் கேசட்டுகள் சூப்பர் ஹிட் ஆகி உள்ளன. நல்ல
திரைக்கதை அம்சமுள்ள படங்களின் வசனம் அடங்கிய கேசட்டுகள் 25 வௌி யிடப்பட்டு உள்ளன. </p><p>
</p><p>
விரைவில் ஆஸ்கார் பிலிம்சின் ரோஜாக்கூட்டம், சூப்பர் குட் பிலிம்சின் புன்னகை
தேசம், அற்புதம், வருச மெல்லாம் வசந்தம் ஆகிய படங்களின் ஆடியோ உரிமை பெற்று
கேசட்டுகளை வௌியிட உள்ளது. இதைத்தொடர்ந்து விரைவில் 100-வது பட பாடல் கேசட்டும்
வௌிவரவுள்ளது. </p><p>
</p><p>
சாதனை விழா </p><p>
</p><p>
தற்போது வௌிவந்திருக்கும் ஷாஜகான் பாடல் கேசட் விற்பனையில் சாதனை படைத்துள்ளதை
தொடர்ந்து இரண்டொரு நாளில் அதற்கான பாராட்டு விழா நடத்த உள்ளோம். </p><p>
</p><p>
இவ்வாறு அவர்கள் கூறினர். </p>

<p> </p>

<p>இன்று நடக்கிறது: 'சித்தி'
தொடர் வெற்றி விழா: கருணாநிதி பங்கேற்பு</p>

<p> </p>

<p>சென்னை,அக்.3- சித்தி தொடரின் வெற்றி விழா இன்று நடக்கிறது.
இதில் தி.மு.க. தலைவர் கருணாநிதி கலந்து கொள்கிறார். </p><p>
</p><p>
சன் டி.வி.யில் திங்கள் முதல் வௌ்ளி வரை தினமும் இரவு 9.30 மணிக்கு சித்தி என்ற
தொடர் ஒளிபரப்பாகி வருகிறது. சிறுவர்கள் முதல் பெரியவர்கள் வரை அனைத்து தரப்பினரையும்
கவர்ந்த இந்த தொடரை ராதிகா சரத் தயாரித்து, நடிக்கிறார். அவருடன் சிவகுமார், யுவராணி,
அஜய் ரத்னம்,வாசு விக்ரம் என ஒரு நட்சத்திர பட்டாளமே நடிக்கின்றனர். </p><p>
</p><p>
வெற்றி விழா </p><p>
</p><p>
இந்த தொடர் வரும் 9-ந்தேதி (வௌ்ளி)யுடன் முடிவடைகிறது. இதையொட்டி இதன் வெற்றி
விழாவும், கலைஞர்களுக்கு பாராட்டு விழாவும் நடக்கிறது. சென்னை லீ ராயல் மெரீடியன்
ஓட்டலில் இன்று (சனி) மாலை 6.30 மணிக்கு நடக்கும் இந்த விழாவில் தி.மு.க. தலைவர்
கருணாநிதி சிறப்பு அழைப்பாளராக கலந்து கொண்டு வாழ்த்திப் பேசுகிறார். மற்றும்
இவ்விழாவில் சன் டி.வி.யின் நிர்வாக இயக்குனர் கலாநிதி மாறன், டைரக்டர் பாரதிராஜா,
சரத்குமார் எம்.பி., கவிஞர் வைரமுத்து, யு.டி.வி. நிறுவன தலைவர் ரோனி ஸ்குரூவாலா
உள்பட சித்தி நடிகர்- நடிகைகள், தொழில் நுட்ப கலைஞர்கள் கலந்து கொள்கின்றனர். </p><p>
</p><p>
விழாவுக்கான ஏற்பாடுகளை ராதிகா சரத் மற்றும் ராடன் டி.வி. ஊழியர்கள் செய்து
வருகின்றனர். </p>

<p> </p>

<p> </p>






</body></text></cesDoc>