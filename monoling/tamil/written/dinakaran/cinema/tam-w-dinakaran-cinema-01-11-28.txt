<cesDoc id="tam-w-dinakaran-cinema-01-11-28" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-01-11-28.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-11-28</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-11-28</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>'நண்பர்்கள்' பட
கதாநாயகி, நடிகை மம்தா குல்கர்னி ரகசிய திருமணம்: கணவருடன் வௌிநாட்டில் குடியேறினார்</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>




 
  
  <p>மம்தா குல்கர்னி </p>
  
 




<p>சென்னை, நவ. 28- நண்பர்கள் படத்தில் நடித்த நடிகை மம்தா ரகசிய
திருமணம் செய்து கொண்டு கணவருடன் வௌிநாட்டில் குடியேறிவிட்டார். </p><p>
</p><p>
தமிழில் வௌியான நண்பர்கள் படத்தில் கதாநாயகியாக அறிமுகமானவர் நடிகை மம்தா. பின்னர்
இவர் இந்தியில் மம்தா குல்கர்னி என்ற பெயருடன் முன்னணி நடிகைகள் பட்டியலில் இடம்
பிடித்தார். அதேசமயம் நிர்வாண போஸ் கொடுத்தார், லல்லுவுடன் சம்பந்தப்படுத்தி
பேசப்பட்டார். இவ்வாறு சர்ச்சைக்குரிய பல்வேறு செய்திகளில் இடம் பிடித்து பரபரப்பை
ஏற்படுத்தினார். </p><p>
</p><p>
திருமணம் </p><p>
</p><p>
தற்போது அவரது திருமண செய்தி வௌியாகி உள்ளது. தனது காதலனை மணந்து கொண்டு மம்தா
குல்கர்னி வௌிநாட்டில் குடியேறி விட்டார். அவரது கணவர் பெயர் என்ன? என்ன தொழில்
செய்கிறார் என்பதை மம்தா ரகசியமாக வைத்திருக்கிறார். </p>

<p> </p>

<p>'பார்த்தாலே பரவசம்' பட
விழா: நடிகர் மாதவனிடம் ரசிகைகள் கெஞ்சல், மீசை வைக்க வேண்டாம்</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>




 
  
  <p>ரசிகைகள் மத்தியில்
  மாதவன் </p>
  
 




<p>சென்னை, நவ. 28- மீசை வைத்துக்கொள்ள வேண்டாம் என்று நடிகர்
மாதவனிடம் ரசிகைகள் கெஞ்சினார்கள். ருசிகரமான இந்த சம்பவம் சென்னையில் நடந்தது.
</p><p>
</p><p>
டைரக்டர் கே.பாலசந்தர் இயக்கத்தில் வௌியாகி இருக்கும் படம் பார்த்தாலே பரவசம். </p><p>
</p><p>
இதில் மாதவன், சிம்ரன், லாரன்ஸ், சிநேகா உள்பட பலர் நடித்துள்ளனர். இப்படம் படப்
பிடிப்பில் இருந்தபோது வெப் சைட் (கம்ப்யூட்டர் இணைய தளம்) ஒன்று தொடங்கப்பட்டு அதில்
ரசிகர், ரசிகைகளுக்கு இடையே போட்டி வைக்கப்பட்டது. </p><p>
</p><p>
படத்தில் நடித்த நடிகர், நடிகைகளின் 5 விதமான போஸ்களில் புகைப்படங்கள்
போட்டியாளர்களுக்கு தரப்பட்டது. அதை வைத்து படத்தின் கதையை கூற வேண்டும். இந்த
போட்டியில் வெற்றி பெற்ற 5 ரசிகைகள் உள்பட 10 பேர்களுக்கு பரிசளிக்கும் விழா எளிமையாக
சென்னை ஸ்பென்சர் பிளாசாவில் உள்ள தி ஹெல் வெட்டிகா வாட்ச் வளாகத்தில் நடந்தது. </p><p>
</p><p>
பரிசு </p><p>
</p><p>
இதில் நடிகர் மாதவன் கலந்துகொண்டு வெற்றி பெற்ற வர்களுக்கு பரிசுகள் வழங் கினார்.
பின்னர் ரசிகர், ரசிகைகள் கேட்ட கேள்விக்கு மாதவன் பதில் அளித்தார். அது வருமாறு:- </p><p>
</p><p>
கேள்வி:- (ரசிகர்) நீங்கள் சிகரெட் பிடிக்கும் பழக்கம் உள்ளவரா? </p><p>
</p><p>
பதில்:- சிகரெட் பிடிப்பது, மது அருந்துவது கிடையாது. </p><p>
</p><p>
கேள்வி:- என்னவளே படப் பிடிப்பின்போது நீங்கள் சிகரெட்் பிடிப்பதை நான் நேரில்
பார்த்தேனே? </p><p>
</p><p>
பதில்:- அது படத்தில் வரும் காட்சிக்காக மட்டுமே நான் செய்தது. </p><p>
</p><p>
பெண்கள் துரத்தல் </p><p>
</p><p>
கேள்வி:- பார்த்தாலே பரவசம் படத்தில் உங்களை பெண்கள் துரத்துவதுபோல் காட்சி
உள்ளதே... நிஜத்தில் எப்படி? </p><p>
</p><p>
பதில்:- இதோ கடையை சுற்றி பாருங்கள் எவ்வளவு கேர்ள்ஸ் இருக்காங்க. </p><p>
</p><p>
கேள்வி:- நீங்கள் இரண்டு கதாநாயகன் படத்தில் நடிப்பீர்களா? </p><p>
</p><p>
பதில்:- 10-வது கதாநாயகனாக கூட நடிக்க தயார். ஆனால் படத்தில் கதையும் எனது வேடமும்
நன்றாக இருக்க வேண்டும். </p><p>
</p><p>
கேள்வி:- இந்தி படத்தில் நடித்தீர்களே அப்படம் எப்படி போய்க்கொண்டிருக்கிறது? </p><p>
</p><p>
பதில்:- படம் ரிலீஸ் தொடக்கத்தில் சுமாராக ஓடியது. தற்போது நன்றாக பிக்அப் ஆகி
இருக்கிறது. </p><p>
</p><p>
கேள்வி:- தொடர்ந்து இந்தியில் நடிப்பீர்களா? </p><p>
</p><p>
பதில்:- சரியான வாய்ப்பு வந்தால் நடிப்பேன். </p><p>
</p><p>
கேள்வி:- இந்தியில் நடிக்க விருப்பமா? தமிழில் நடிக்க விருப்பமா? </p><p>
</p><p>
பதில்:- இரண்டு மொழியிலும் நடிக்க ஆசை. அதிக கவனம் தமிழில் செலுத்துவேன். </p><p>
</p><p>
மீசை </p><p>
</p><p>
கேள்வி:-(ரசிகர்) நீங்கள் மீசை வைத்துக்கொள்வீர்களா? </p><p>
</p><p>
பதில்:- (மாதவன் பதில் சொல்வதற்குள் அருகில் இருந்த 5 ரசிகைகள் ஒரே குரலில்
வேண்டாம்...ப்ளீஸ் மீசை வைக்க வேண்டாம்....மீசை இல்லாமல் இருந்தால்தான் நன்றாக இருக்
கிறது. மீசை வைக்க கொஞ்ச நாள் போகட்டும்...) ரசிகைகளின் அன்பை மீற முடியாது
(மாதவன்). </p><p>
</p><p>
இவ்வாறு அவர் பதில் அளித்தார். </p><p>
</p><p>
முன்னதாக அகிலா, ஷோபனா, சரஸ்வதி, சரோஜா, சவுமியா, பி.சிவகுமார், அமர் நாத், மாதவா,
சிவபிரகாஷ், அனந்தகிருஷ்ணன் ஆகியோர் மாதவனிடமிருந்து பரிசுகள் பெற்றுக்கொண்டார்கள்.
கடை அதிபர் அசோக் தோஷி மாதவனுக்கு கோல்ப் பந்துடன் கூடிய புதுமையான வாட்ச் ஒன்றை
பரிசளித்தார். </p>

<p> </p>

<p>கன்னட நடிகை பிரேமாவுக்கு
கொலை மிரட்டல்: பட அதிபர் கைது</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>




 
  
  <p>பிரேமா </p>
  
 




<p>பெங்களூர், நவ.28- கன்னட நடிகை பிரேமாவுக்கு கொலை மிரட்டல்
விடுத்ததாக பட அதிபர் கைது செய்யப்பட்டார் </p><p>
</p><p>
கன்னட படத்தில் முன்னணி நடிகையாக திகழ்பவர் பிரேமா. இவர் நடித்த எஜமானா படம்
கர்நாடகத்தில் வெற்றிகரமாக ஓடிக்கொண்டு இருக்்கிறது. </p><p>
</p><p>
கால்ஷீட் </p><p>
</p><p>
பிரேமாவின் கால்ஷீட்டை தயாரிப்பாளர்கள் போட்டி போட்டு கொண்டு புக் செய்து
வருகிறார்கள். தயாரிப்பாளர் சில்பா சீனிவாஸ், நடிகை பிரேமாவை வைத்து பிரேம பர்வா என்ற
படம் தயாரித்தார். </p><p>
</p><p>
தாமதம் </p><p>
</p><p>
இப்படத்தில் நடிகை பிரேமா மேலும் 2 நாள் நடிக்க தயாரிப்பாளர் சில்பா சீனிவாஸ்
கேட்டார். ஆனால் பிரேமா கால்ஷீட் கொடுக்காமல் தாமதப்படுத்தி வந்தார் </p><p>
</p><p>
வீட்டுக்கு சென்று </p><p>
</p><p>
இந்த நிலையில் நேற்று முன்தினம் நடிகை பிரேமா வீட்டுக்கு தயாரிப்பாளர் சில்பா சீனிவாஸ்
செனறார். தனது படத்தில் நடித்து முடிக்க வேண்டும் என்று வற்புறுத்தினார் </p><p>
</p><p>
திராவகம் </p><p>
</p><p>
அங்கு ஆர்ப்பாட்டம் நடத்தினார். நடிகை மீது திராவகம் வீசி விடுவேன் என்று மிரட்டினாராம்.
</p><p>
</p><p>
கைது </p><p>
</p><p>
இதுகுறித்து நடிகை பிரேமாவின் தந்தை செங்கப்பா போலீசில் புகார் செய்தார் அதில் தனது
மகளின் உயிருக்கு ஆபத்து உள்ளது என்று கூறி உள்ளார். இதன் பேரில்். ஜே..பி நகர்
போலீசார் வழக்கு பதிநது படதயாரிப்பாளர் சில்பா சீனிவாசை கைது செய்தனர். </p>

<p> </p>

<p>சட்டத்தை கடுமையாக்கி,
மலேசியாவில் 3 மாதத்தில் திருட்டு வி.சி.டி. கும்பல் ஒழிப்பு, தமிழ் பட அதிபர்களிடம்
டத்தோ சாமிவேலு உறுதி</p>

<p> </p>

<p>சென்னை, நவ. 28- சட்டத்தை கடுமையாக்கி மலேசியாவில் 3
மாதத்திற்குள் திருட்டு வி.சி.டி தயாரிக்கும் கும்பல் அடியோடு ஒழிக்கப்படும் என்று தமிழ் பட
தயாரிப்பாளர்களிடம் டத்தோ சாமிவேலு உறுதி அளித்தார். </p><p>
</p><p>
தமிழ் திரைப்பட தயாரிப்பாளர்கள் சங்கம் சார்பில் நேற்று ஒரு அறிக்கை வௌி
யிடப்பட்டது. அதில் கூறி இருப்பதாவது:- </p><p>
</p><p>
கடந்த ஜனவரி மாதம் மலேசியாவில் உள்ள வீடியோ திருடர்களை உடனடியாக ஒடுக்கி திருட்டு
வி.சி.டிக்களை அடியோடு ஒழிக்க வேண்டும் என்று மலேசிய அமைச்சர் டத்தோ  சாமிவேலுவிடம்
தமிழ் திரைப்பட தயாரிப்பாளர்கள் சங்கம் சார்பில் மனு ஒன்று அளிக்கப்பட்டது. உடனடியாக
அது சம்பந்தப்பட்ட அமைச்சர்களிடம் பேசி தகுந்த நடவடிக்கை எடுப்பதாகச் சொன்ன டத்தோ
சாமி வேலுவின் தீவிர முயற்சி காரணமாக மலேசியாவில் திருட்டு வி.சி.டி.க்களை ஒழிக்க
முயற்சிகள் முழு மூச்சாக நடந்து வருகின்றன. இந்த நிலையில் நல்லெண்ணப் பயணமாக சென்னை
வந்துள்ள டத்தோ சாமிவேலுவுக்கு தயாரிப்பாளர்கள் சங்கம் சார்பில் வரவேற்பு
அளிக்கப்பட்டது. </p><p>
</p><p>
வரவேற்பு </p><p>
</p><p>
இந்த நிகழ்ச்சியில் தமிழ் பட தயாரிப்பாளர்கள் சங்க தலைவர் அ.செ.இப்ராகிம்
ராவுத்தர், துணை தலைவர் ஏ.எல்.அழகப்பன், செயலாளர்கள் சித்ரா லட்சுமணன், அன்பாலயா
பிரபாகரன், டைரக்டர் பாரதிராஜா, தயாரிப்பாளர்கள் கே.விஜயகுமார், பிரமிட் நடராஜன்,
ஹேம்நாக் பாபுஜி, சவன்த் சானல் நாராயணன், எச்.முரளி, நந்தகோ பால் செட்டியார்
அகியோர் கலந்து கொண்டனர். </p><p>
</p><p>
உறுதி </p><p>
</p><p>
திருட்டு வி.சி.டியை ஒழிக்க முழுமூச்சாக செயல்படும் மலேசிய அரசுக்கும் அதன் அமைச்சர்களுக்கும்
இந்த வரவேற்பின்போது நன்றி தெரிவிக்கப்பட்டது. இந்த சந்திப்பின் போது தமிழ்
திரைப்பட தயாரிப் பாளர்களிடம் இன்னும் இரண்டு அல்லது மூன்று மாதங்களில் மலேசியாவில்
திருட்டு வி.சி.டி. தயாரிக்கும் கும்பல் அடியோடு ஒழிக்கப்படும். அதேபோன்று திருட்டு
வி.சி.டி. தயாரிப்பு மற்றும் விநியோகத்தை அடியோடு ஒழிக்கும் வகையில் சட்டத்தை
தீவிரமாக்க விருக்கிறோம். அதையும் மீறி இதில் குறைபாடுகள் ஏதேனும் நேருமானால் எனக்கு
உடனுக்குடன் தெரிவியுங்கள் நான் உடனே நடவடிக்கை எடுக்கிறேன் என்று அமைச்சர் உறுதி
அளித்தார். </p><p>
</p><p>
இவ்வாறு அதில் கூறப்பட்டுள்ளது. </p>

<p> </p>






</body></text></cesDoc>