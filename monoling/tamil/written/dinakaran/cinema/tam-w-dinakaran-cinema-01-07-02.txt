<cesDoc id="tam-w-dinakaran-cinema-01-07-02" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-01-07-02.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-07-02</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-07-02</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>கருணாநிதி கைது- விஜயகாந்த்
கண்டனம்</p>

<p> </p>

<p>சென்னை, ஜூலை 2- கருணாநிதி கைது செய்யப்பட்டதற்கு நடிகர்
விஜயகாந்த் கண்டனம் தெரிவித்துள்ளார். </p><p>
</p><p>
இதுதொடர்பாக அவர் வௌியிட்டுள்ள அறிக்கையில் கூறி இருப்பதாவது:- </p><p>
</p><p>
எழுபத்து எட்டு வயதான பெரியவரை போலீசார் கைது செய்த முறை உலகத் தமிழர்களின் மனதையும்
என் மனதையும் நோகடிக்கிறது என்பதை வருத்தத்துடன் தெரிவித்துக் கொள்கி றேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறி உள்ளார். </p>

<p> </p>

<p>கலைஞர் கைது
அநாகரிகம்- கமல்ஹாசன் கண்டனம்</p>

<p> </p>

<p>சென்னை, ஜூலை 2- கருணாநிதி கைது செய்யப்பட்ட விதம் அநாகரிகமானது
என்று கமல்ஹாசன் கூறி உள்ளார். </p><p>
</p><p>
பத்ம கமல்ஹாசன் நேற்று வௌியிட்டுள்ள ஒரு அறிக்கையில் கூறி இருப்ப தாவது:- </p><p>
</p><p>
அரசியலும் சட்ட விவகாரங்களும் அதிகமாகத் தெரியாத, புரியாத கலைஞன் நான்.
இருப்பினும் டாக்டர் கலைஞரின் கைது ஒரு சாதாரண குடிமகனுக்கு கிடைக்க வேண்டிய
நியாயங்கள் கூட இல்லாமல் நடந்திருப்பது தெரிகிறது. </p><p>
</p><p>
அவர் கைது செய்யப்பட்ட நேரமும் விதமும் அநாகரிகமாகத் தெரிகிறது. மூத்த தலைவர்
என்றில்லாவிட்டாலும் மூத்த ஒரு தமிழன் இந்தியக் குடிமகன் என்ற முறையில் அவருக்கு கிடைக்க
வேண்டிய மரியாதைகள்கூட நிராகரிக்கப்பட்டதாய் தெரிகிறது. </p><p>
</p><p>
கட்சி சார்பற்ற என் இந்த கருத்து எந்த அரசாங்கம் ஆட்சியில் இருந்தாலும் இவ்வாறாகவே
இருக்கும். ஜனநாயகத்தில் இன்னமும் கொஞ்சம் நம்பிக்கை வைத்திருக்கும் என்
போன்றவர்களின் பெரும்பான்மைக் கருத்தும் இதுவாகவே இருக்கும் என்றே நம்புகிறேன். </p><p>
</p><p>
இவ்வாறு அவர் அதில் கூறி உள்ளார். </p>

<p> </p>






</body></text></cesDoc>