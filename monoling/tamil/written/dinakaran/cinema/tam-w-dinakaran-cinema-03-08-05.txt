<cesDoc id="tam-w-dinakaran-cinema-03-08-05" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-03-08-05.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-08-05</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-08-05</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>காதல் கிசு கிசு நகைச்சுவை
பார்த்து விவேக்கிற்கு ஜனாதிபதி அப்துல்கலாம் பாராட்டு உங்கள் பணி சிறக்கட்டும் </p>

<p> </p>

<p>சென்னை,
ஆக. 5- காதல் கிசுகிசு படத்தின் நகைச்சுவையை பார்த்து ரசித்த ஜனாதிபதி அப்துல் கலாம்
விவேக்கிற்கு பாராட்டு கடிதம் அனுப்பினார். </p><p>
</p><p>
விவேக் </p><p>
</p><p>
டைரக்டர் பி.வாசு இயக்கத்தில் உருவான படம் காதல் கிசு கிசு. இப்படத்தில் நகைச்சுவை
நாயகனாக விவேக் நடித்திருக்கிறார். மலேசியா நாட்டில் போலீ சிடம் சிக்கி திணறும்
விவேக் உணர்ச்சி மயமாக பேசும் ஒரு காட்சியில் உங்க நாட்டுக்கு ஒரு மகதீர் (மலேசியா
பிரதமர்) கிடைச்சதுபோல எங்க நாட்டுக்கு ஒரு அப்துல் கலாம் கிடைச்சிருக்காரு. எதிர்கால
இந்தியா பிரகாசமாக இருக்கப்போகிறது என்ப தற்கான அறிகுறி இப்போதே தெரிந்துவிட்டது என்று
பேசும் வசனம் இளைஞர் களுக்கு உற்சாகத்தையும் ஊக்கத்தையும் அளிப்பதாக உள்ளது. </p><p>
</p><p>
இந்த நகைச்சுவை பகுதி காட்சிகளை மட்டும் தயாரிப்பாளர் கேசவன் அனுமதியுடன் விவேக்
வி.சி.டியாக எடுத்து ஜனாதிபதி அப்துல் கலாமிற்கு அனுப்பினார். அதை அவர் பார்த்ததுடன்
ஜனாதிபதி மாளிகை ஊழியர்களையும் பார்க்கச் செய்தார். பின்னர் அவர் விவேக்கிற்கு ஒரு
கடிதம் அனுப்பினார். அதில் இந்தியாவை வள மான நாடாக்குவதில் ஒவ்வொரு இந்தியருக்கும் பங்கு
இருக்கிறது. உங்கள் பணி நற்பணியாகும். உங்கள் குரல் உணர்ச்சியான உயர்ந்த குரல்,
உங்களுக்கும் உங்கள் குடும்பத்தினருக்கும் வாழ்த்துக்கள் என்று கூறி உள்ளார். </p><p>
</p><p>
பேட்டி </p><p>
</p><p>
இதுபற்றி நடிகர் விவேக் கூறியதாவது:- </p><p>
</p><p>
காதல் கிசுகிசு படத்தை பார்த்த காந்திகண்ணதாசன் உங்கள் நகைச்சுவை பகுதி யை
ஜனாதிபதியின் பார்வைக்கு அனுப்பி வையுங்கள் என்று கூறினார். அதன்படி அனுமதிபெற்று அனுப்பி
வைத்தேன். தனது பெரும் பணிக்கு நடுவில் ஜனாதிபதி அதைப் பார்த்து எனக்கு பாராட்டு கடிதம் எழுதி
உள்ளார். இது எனக்கு ஜனாதிபதி விருது கிடைத்தது போல் மகிழ்ச்சியை ஏற்படுத்தி உள்ளது.
இவர் ஜனாதிபதி வேட்பாளராக பரிந்துரை செய்யப்படாமல் விஞ்ஞானியாக இருந்த
சமயத்திலேயே லவ்லி என்ற படத்தில் தாடி வைத்தவன் எல்லாமல் அப்துல் கலாம்போல்
விஞ்ஞானி ஆகிவிட முடியாது என்று நான் பேசி இருக்கிறேன். </p><p>
</p><p>
இத்தகைய பாராட்டுக்கு என்னை ஆளாக்கிய டைரக்டர் பி.வாசு, தயாரிப்பாளர் கேசவன், பிரசன்ன
குமார், முருகன், மானேஜர் சுதாகர் ஆகியோருக்கு என் நன்றி. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். 

</p>

<p> </p>

<p> </p>

<p>ஐஸ் பட போட்டி:
ரசிகர்களுக்கு நடிகர் அசோக் பரிசு </p>

<p> </p>

<p>சென்னை, ஆக. 5- புதுமுக கதாநாயகன் அசோக், பிரியங்கா திரிவேதி
ஜோடியாக நடித்த ஐஸ் படம் திரைக்கு வந்து வெற்றிகரமாக ஓடிக்கொண்டிருக்கிறது. ஜாய்
எண்டர்டெயின் மெண்ட் நிறுவனம் சார்பில் கே.ஷரிப்அகமது தயாரித்திருக்கிறார். </p><p>
</p><p>
இப்படத்தின் பாடல் போட்டி அகில இந்திய வானொலியில் திரைமாலை நிகழ்ச்சியில்
நடத்தப்பட்டது. இந்த நிகழ்ச்சியின்போது நடிகர் அசோக் நேரில் பங் கேற்று ரசிகர்களின்
கேள்விகளுக்கு பதில் அளித்தார். </p><p>
</p><p>
இதே நிகழ்ச்சியில் ரசிகர்களுக்கு போட்டி நடத்தப்பட்டது. கேட்கப்பட்ட கேள்வி களுக்கு
சரியான பதில் அளித்து அத்துடன் ஐஸ் படம் பார்த்ததற்கான டிக்கெட் இணைத்து அளித்த வர்கள்
தேர்வு செய்யப்பட்டனர். அவர்களுக்கு பரிசளிக்கும் விழா சென்னையில் உள்ள பட நிறுவன
அலுவலகத்தில் எளிமையாக நடந்தது. வெற்றி பெற்ற ரசிகர்களுக்கு தயாரிப்பாளர் கே.ஷரிப்
அகமது, அசோக் பரிசளித்தனர். அப்போது வானொலி நிகழ்ச்சி அதிகாரி சி.கமலக்கண்ணன்,
பி.ஆர்.ஓ. சந்துரு உடனிருந்தனர். </p><p>
</p><p>
பேட்டி </p><p>
</p><p>
பின்னர் இதுபற்றி அசோக் கூறும்போது, வானொலியில் என்னிடம் பேசிய ரசிகர்கள் யாரும்
என்னை குறை சொல்லவில்லை. கவிதை மூலமாக ஒருவர் என்னை பாராட்டி னார். ரசிகர்களின்
வரவேற்பு எனக்கு மகிழ்ச்சி தந்துள்ளது. விரைவில் குமாரராஜா இயக்கும் துளிதுளியாய் என்ற
படத்தில் நடிக்க உள்ளேன் என்றார். </p><p>
</p><p>
தயாரிப்பாளர் கே.ஷரிப் அகமது கூறும்போது, தமிழ் ரசிகர்கள் எங்களுக்கு தந் திருக்கும்
வரவேற்பு உற்சாகத்தையும் நெகிழ்ச்சியையும் அளித்து உள்ளது. விரைவில் சரத்குமார்
நடிக்கும் புதிய படமொன்றை தயாரிப்பது பற்றி அவரிடம் பேசி வருகி றேன். அதில் அசோக்
அவரது தம்பியாக நடிக்கிறார் என்றார். </p>






</body></text></cesDoc>