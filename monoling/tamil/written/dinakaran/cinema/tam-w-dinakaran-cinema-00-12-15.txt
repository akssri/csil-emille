<cesDoc id="tam-w-dinakaran-cinema-00-12-15" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-00-12-15.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-12-15</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-12-15</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>டைட்டானிக் நடிகர் மீது
ரூ.20 கோடி நஷ்ட</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>




 
  
  <p>லியானர்டோ
  டிகாப்ரியோ </p>
  
 




<p>டைட்டானிக் சினிமா பட கதாநாயகன் லியானர்டோ டிகாப்ரியோ. இவர்
1998-ம் ஆண்டில் நடிகை எலிசபெத் பெர்க்லியின் காதலர் ரோஜர் வில்சனை தாக்கினார்.
ரோஜர் வில்சன் சினிமா கதை எழுதுபவர் ஆவார். அவரை நியூயார்க்கில் உள்ள ஓட்டலில்
வைத்து நடிகர் லியானர்டோ அடித்து உதைத்தார். </p><p>
</p><p>
இந்த சம்பவத்துக்கு லியானர்டோ மீது ரோஜர் வில்சன் வழக்கு போட்டு உள்ளார். ரூ.20 கோடி
நஷ்டஈடு கேட்டு உள்ளார். டைட்டானிக் நடிகர் மீது சினிமா கதாசிரியர் போட்டுள்ள இந்த
வழக்கு பரபரப்பை ஏற்படுத்தி உள்ளது. </p>

<p> </p>

<p>புதுப்பட படப்பிடிப்பு உடனே
தொடங்க வற்புறுத்தி கே.ராஜன் உண்ணாவிரதம்: பாலசந்தர்-கேயார்-பாக்யராஜ் நேரில்
வாழ்த்து</p>

<p> </p>

<p>சென்னை, டிச. 15- நடிகர் தங்கள் சம்பளத்தை குறைத்துக்கொள்ள
வேண்டும், லேப் லெட்டர் அடிப்படையில் சம்பளம் பெற்றுக்கொள்ள வேண்டும் என்ற பிரச்சினை
யால் புதிய படம் எதுவும் தொடங்கப்படவில்லை. </p><p>
</p><p>
இதனால் சினிமாவில் தேக்க நிலை ஏற்பட்டிருப்ப துடன், திரைப்பட தொழிலா ளர்கள் வேலை
இல்லாமல் கஷ்டப் படுகின்றனர். எனவே இப்பிரச் சினைக்கு உடனடியாக தீர்வு காண வேண்டும்
என்று கோரி திரைப்பட விநியோகஸ்தர்கள் சங்க தலைவர் கே.ராஜன் நேற்று சென்னையில் உள்ள
பிலிம்சேம்பர் வளாகத்தில் உண்ணாவிரதம் இருந்தார். </p><p>
</p><p>
பாலசந்தர் </p><p>
</p><p>
தென்னிந்திய திரைப்பட தொழிலாளர் சம்மேளன தலைவர் கே.பாலசந்தர் நேரில் வந்து
வாழ்த்தினார். </p><p>
</p><p>
அவர் பேசியதாவது, </p><p>
</p><p>
னாதிரைத்துறை நன்றாக இருக்க வேண்டும், தொழி லாளர்களுக்கு தொடர்ந்து வேலை கிடைக்க உடனடியாக
படப் பிடிப்பு தொடங்க வேண் டும் என்று இங்கு கே.ராஜன் உண்ணாவிரதம் இருக்கிறார். அவரது
முயற்சிக்கு 24 ஆயிரம் பெப்சி தொழிலாளர்களின் ஆதரவும் இருக்கிறது என்பதை தெரிவிக்கவே
இங்கு நான் வந்தேன். </p><p>
</p><p>
ைகேயார் </p><p>
</p><p>
பிலிம்சேம்பர் தலைவர் கேயார் பேசியதாவது, </p><p>
</p><p>
னாநடிகர்களிடம் லேப் லெட்டர் வற்புறுத்துவதால் தயாரிப்பாளருக்கு பணம் தட்டுப்பாடு ஏற்படும்
நேரத்தில் நடிகர்களிடம் கருணையை எதிர்பார்க்க முடியாது. இந்த பிரச்சினை தொடர்பாக அனு
பவம் வாய்ந்த தயாரிப்பாளர் களிடமும் இதற்கு ஆலோசனை கேட்டால் சொல்ல தயாராக
இருக்கிறார்கள் </p><p>
</p><p>
பைாக்யராஜ் </p><p>
</p><p>
டைரக்டர் கே.பாக்யராஜ் பேசியதாவது, னாசினிமா தொழில் நன்றாக இருக்க வேண்டும்
என்பதற்காக இங்கு ராஜன் உண்ணாவிரதம் இருக் கிறார். கூடிய விரைவில் தயாரிப்பாளர்கள்-
நடிகர் களுக்கு இடையே சுமூக முடிவு ஏற்படும் என்று நம்புகிறேன். மேலும் கலைஞர் வரை
இப்பிரச்சினை போய் இருப்ப தால் கூடிய விரைவில் பிரச் சினை தீர்ந்துவிடும். </p><p>
</p><p>
ைகே.ராஜன் பேசியதாவது:- </p><p>
</p><p>
இங்கு வாழ்த்தியவர்களுக்கு எனது நன்றி. கே.பாக்யராஜ் கேட்டுக்கொண்டதற்கிணங்க எனது
உண்ணாவிரத்தை இன்று நான் முடித்துக் கொள்கி றேன். ஒரு வாரத்திற்குள் புது படப்பிடிப்பு
தொடங்கா விட்டால் மீண்டும் உண்ணா விரதம் தொடங்கி விடுவேன். என்னுடன் சேர்ந்து பெப்சி
தொழிலாளர்களும் தொடர் உண்ணாவிரதம் இருப்பதாக அறிவித்திருக்கிறார்கள் என் றைார். </p><p>
</p><p>
தீர்மானம் </p><p>
</p><p>
உண்ணாவிரத கூட்டத்தில் நிறைவேற்றப்பட்ட தீர்மா னங்கள் வருமாறு:- </p><p>
</p><p>
* பட அதிபர்கள்- நடிகர் கள் பிரச்சினைக்கு தீர்வு கண்டு புதிய படங்களின் படப் பிடிப்பை
உடனே தொடங்க வேண்டும். </p><p>
</p><p>
* முதல் பிரதி தயாரான நிலையில் 141 படங்கள் வௌி வராமல் முடங்கி இருக்கிறது. அதனை
திரையிட ஏற்பாடு செய்யவேண்டும். </p><p>
</p><p>
* தமிழக முதல்வர் சிறு முதலீட்டு படங்களுக்கு மட்டும் கேளிக்கை வரியை முழுமை யாக ரத்து செய்ய
வேண்டும். </p><p>
</p><p>
* தரமான படத்திற்கு மானிய தொகையினை ரூபாய் 10 லட்சமாக உயர்த்திட வேண்டு கிறோம். </p><p>
</p><p>
250 தொழிலாளர் </p><p>
</p><p>
கே.ராஜனுடன் நடிகர் பாபு கணேஷ், பட அதிபர் ஜோதி பிரசாத், விநியோகஸ்தர்கள்
எஸ்.ஆர்.எம்.சண்முகம், பங் களா சீனிவாசன் உள்பட பலர் உண்ணாவிரதம் இருந்தனர். </p><p>
</p><p>
டைரக்டர் வி.சி.குகநாதன், விநியோகஸ்தர்கள் சங்க செய லாளர் ஜே.வி.ருக்மாங்கதன், பட
அதிபர் கிருஷ்ணா ரெட்டி, நடிகர் ஐசரி கணேஷ், டைரக்டர் எம்.கே.அருந்தவ ராஜா மற்றும்
பெப்சி மகளிர் பிரிவு தொழிலாளர் 250 பேர்கள் உள்பட பலர் கே.ராஜ னுக்கு வாழ்த்து
தெரிவித்தனர். </p>

<p> </p>






</body></text></cesDoc>