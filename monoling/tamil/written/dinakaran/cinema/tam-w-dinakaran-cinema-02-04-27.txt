<cesDoc id="tam-w-dinakaran-cinema-02-04-27" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-02-04-27.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-04-27</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-04-27</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>''பாபா'' படப்பிடிப்பில்
பங்கேற்கும் மனீஷாவுக்கு போலீஸ் பாதுகாப்பு</p>

<p> </p>

<p>சென்னை, ஏப். 27- ரஜினியுடன் பாபா படத்தில் ஜோடியாக நடிக்கும்
மனீஷாவுக்கு பயங்கர போலீஸ் பாதுகாப்பு தரப்பட்டுள்ளது. </p><p>
</p><p>
ரஜினியின் ''பாபா'' படப்பிடிப்பில் நடிகை மனீஷா கலந்து கொண்டு நடித்து வருகிறார். அவருக்கு
மும்பை தாதாக்கள் கொலை மிரட்டல் விடுத்திருந்ததால் துப்பாக்கி ஏந்திய போலீஸ்
பாதுகாப்பு போடப்பட்டு உள்ளது. </p><p>
</p><p>
சென்னையில் மனீஷா கொய்ராலா தங்கி இருக்கும் ஓட்டல் அறைக்கும் போலீஸ் பாதுகாப்பு
போடப்பட்டு உள்ளது. அவரை முன் அனுமதி இன்றி யாரும் பார்க்க முடியாது. </p>

<p> </p>

<p> </p>

<p>சூப்பர் ஹிட் இந்தி
படங்கள் டி.வி.யில் ஒளிபரப்பு</p>

<p> </p>

<p>மும்பை, ஏப்.27- பிரபல இந்தி டைரக்டர் சுபாஷ் கய் இயக்கிய
சூப்பர் ஹிட் படங்கள் சோனி எண்டர்டெய்ன்ட் மெண்ட் டிவியில் ஒளிபரப்பப்படுகிறது. </p><p>
</p><p>
பிரபல இந்தி சினிமா டைரக்டர் சுபாஷ்கய். இவர், தனது 11 சூப்பர் ஹிட் படங்களை
தொலைக்காட்சியில் ஒளிபரப்பும் உரிமையை ''சோனி எண்டர்டெய்ன் மென்ட் டி.வி.''
நிறுவனத்திற்கு வழங்கி உள்ளார். அந்த படங்களை 5 ஆண்டுகளுக்கு ரூ. 16.10 கோடிக்கு அவர்
வழங்கி உள்ளார். அதற்கான ஒப்பந்தம் கையெழுத்தாகி உள்ளது. அதன்படி, கர்ஸ், ஹீரோ,
கர்மா, திருமூர்த்தி, ராம்லக்கன், சவுதாகர், கல் நாயக், பர்தேஸ், தால், ராகுல் மற்றும்
யாதீன் ஆகிய படங்கள் சோனி டி.வி.யில் அடுத்த மாதம் முதல் ஒளிபரப்பப்பட உள்ளன. </p>

<p> </p>






</body></text></cesDoc>