<cesDoc id="tam-w-dinakaran-cinema-01-01-10" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-01-01-10.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-01-10</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-01-10</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>நேபாள விஷயத்தில் இந்தியா
'பெரியண்ணன்'போல நடக்கக்கூடாது, நடிகை மனிஷா கொய்ராலா பேட்டி</p>

<p> </p>

<p>மும்பை, ஜன.10- நடிகர் ரித்திக் ரோஷன் நேபாளம் பற்றி கருத்து
தெரி வித்தாக கூறி அந்தநாட்டில் பெரும் கலவரம் வெடித்தது அல்லவா? ரித்திக் ரோஷன்
நடித்த திரைப்படங்களுக்கு நேபாளத்தில் தடை விதிக்கப்பட்டது. இந்த நிலையில் நடிகை மனிஷா
கொய்ராலா, நேபாளத்தைச் சேர்ந்தவர் என்பதால் மனிஷாவின் படங்களுக்கு தடை விதிக்க
வேண்டும் என்று சிலர் குரல் எழுப்பினார்கள். </p><p>
</p><p>
மனிஷா பதில் </p><p>
</p><p>
இந்தநிலையில் நடிகை மனிஷா கொய்ராலா இண் டியா டைம்ஸ் என்ற இண்டர்நெட் வெப்சைட்டில்
வாசகர்களின் கேள்விகளுக்கு பதில் அளித்தார். அவர் கூறியிருப்ப தாவது:- </p><p>
</p><p>
நேபாளத்தை பொறுத்த வரை இந்தியா எப்போதும் ஆதிக்கப்போக்குடன் (பெரிய அண்ணன்
பாணியில்) நடந்து கொள்வதாக ஒரு பத்திரிகையில் படித்தேன். சூழ்நிலையை சரியாக புரிந்து
கொண்ட ஒருவர்தான் அந்த கட்டுரையை எழுதியிருக்கிறார். நேபாள விஷயத்தில் இந்தியா ஆதிக்க
போக்கை கடைபிடிக்காமல் இருப்பதுதான் சரியானது. </p><p>
</p><p>
மோசமான சினிமா உலகம் </p><p>
</p><p>
நான் நேபாளத்தை பூர்வீகமாக கொண்டவள் என்பதால் எனக்கு எந்த பிரச்சினையும் வரவில்லை.
என் சினிமா உலக நுழைவுக்கும் அது தடையாக இருக்கவில்லை. நான் மிகச் சிறிய நகரத்தில்
இருந்து வந்தவள். அங்கிருந்து பிரம்மாண்டமான மோசமான சினிமா உலகத்துக்குள் புகுந்தவள்.
அதுதான் எனக்கு பிரச்சினையாக இருந்தது. </p><p>
</p><p>
அரசியல் தெரியும் </p><p>
</p><p>
நான் அரசியலில் நுழைவதாக இருந்தால் இந்திய அரசியலில் நுழைவேனா? அல்லது நேபாள
அரசியலில் நுழைவேனா? என்று பலர் கேட்கிறார்கள். என்குடும்பத்துக்கும் அரசியலுக்கும்
இடையில் நெருங்கிய தொடர்புண்டு. உலக அரசியல் பற்றி எனக்கு நன்றாக தெரியும்.
இருந்தாலும் எனக்கு அரசியலை விட சினிமாவைத்தான் மிகவும் பிடித்திருக்கிறது. </p><p>
</p><p>
கோடீஸ்வர நிகழ்ச்சி </p><p>
</p><p>
ஜீ. டி.வி. நடத்தி வந்த சவால் தஸ் குரோர் கா கோடீஸ்வர நிகழ்ச்சியில் இருந்து என்னை
நீக்கிவிட்டார்கள். அது வருத்தம் அளித்தது. அது கடந்துபோன கதை. எனவே அதுபற்றி நான் பேச
விரும்பவில்லை. அந்த நிகழ்ச்சியை நடத்துவதில் நடிகர் அனுபம் கேருக்கும் எனக்கும் இடையில்
எந்த பிரச் சினையும் ஏற்படவில்லை. நாங்கள் இருவருமே தொழில்முறை நடிகர்கள். எனவே
பிரச்சினை வர வாய்ப்பில்லை. </p><p>
</p><p>
நேபாளத்தில் நான் பல ஓட்டல்களை கட்ட திட்டமிட்டிருப்பது உண்மைதான். ஆனால் இப்போது
நான் நடிப்பில்தான் அதிக கவனம் செலுத்தி வருகிறேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறியுள்ளார். </p>

<p> </p>






</body></text></cesDoc>