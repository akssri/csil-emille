<cesDoc id="tam-w-dinakaran-cinema-02-07-24" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-02-07-24.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-07-24</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-07-24</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>மலேசியா-சிங்கப்பூரில்
நட்சத்திர இரவு: நடிகர்-நடிகைகளுக்கு விஜயகாந்த் விருந்து-நிபந்தனை விமானத்தில்
பேசக்கூடாது- வௌியாருடன் போஸ் தரக்கூடாது</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>விஜயகாந்த் </p>

<p> </p>

<p>சென்னை, ஜூலை 24- வௌிநாட்டு கலை நிகழ்ச்சிக்கு செல்லும் நடிகர்,
நடிகைகளுக்கு விஜயகாந்த் நேற்று சென்னையில் விருந்து அளித்தார். பிரச்சினைகளை தவிர்க்க
விமானத்தில் வரும் போதுயாரும் அரட்டை அடிக்ககூடாது என்று நிபந்தனையும் விதித்தார். </p><p>
</p><p>
நட்சத்திர இரவு </p><p>
</p><p>
நடிகர் சங்க கடன் மற்றும் நலிந்த கலைஞர்களுக்கு உதவுவதற்காக மலேசியா சிங்கப்
பூரில் வருகிற 27, 28 ஆகிய தேதிகளில் நட்சத்திர கலை நிகழ்ச்சி நடக்கிறது அல்லவா?
அதில் ரஜினிகாந்த், கமல ஹாசன், விஜயகாந்த், சரத்குமார் எம்.பி., எம்.எல்.ஏ.க்கள்
ராதாரவி, நெப்போலியன் மற்றும் சத்யராஜ், பிரபு, கார்த்திக், விஜய், பிரசாந்த்,
அர்ஜூன், நடிகைகள் குஷ்பு, மீனா, சிம்ரன், மும்தாஜ் உள்பட அனைத்து முன்னணி
நட்சத்திரங்களும் பங்கேற்கிறார்கள். </p><p>
</p><p>
3 அணிகள் </p><p>
</p><p>
3 அணியாக நடிகர்- நடிகைகள் மலேசியா போகிறார்கள். அவர்களில் நடிகர் எஸ்.வி. சேகர்
தலைமையில் ஒரு அணியினர் நேற்று மலேசியா புறப்பட்டு சென்றனர். முன்னதாக நெப்போலியனும்
மலேசியா புறப்பட்டு சென்றார். இரண்டாவது கட்டமாக இன்று இரவு ஒரு குழுவினர் மலேசியா
செல்கின்றனர். முன்னணி நட்சத்திரங்கள் 25-ந் தேதி சென்னையிலிருந்து புறப்பட்டு
செல்கிறார்கள். மலேசியா சென்றடையும் அனைத்து நடிகர், நடிகைகளையும் மலேசியா மந்திரிடத்தோ
சாமுவேல் வரவேற்று விருந்தளித்து கவுரவிக்கிறார். 26-ந் தேதி நட்சத்திரங்கள் ஒத்திகை
செய்கின்றனர். </p><p>
</p><p>
விஜயகாந்த் விருந்து </p><p>
</p><p>
தென்னிந்திய நடிகர் சங்க தலைவர் விஜயகாந்த் நேற்று நட்சத்திர இரவில் பங்கேற்கும்
அனைத்து நட்சத்திரங்களுக்கும் சென்னையில் உள்ள லீ மெரிடியன் ஓட்டலில் விருந்தளித்தார்.
அதில் முன்னணி நடிகர், நடிகைகள் உள்பட அனைத்து கலைஞர்களும் கலந்துகொண்டனர்.
அப்போது அவர்களிடம் விஜயகாந்த் சில நிபந்தனைகள் விதித்தார். </p><p>
</p><p>
விமானத்தில் செல்லும்போது யாரும் அநாவசியமாக மற்றவருடன் பேசக்கூடாது. சத்தமாகவோ,
கிசுகிசுத்தோ ஒருவருக்கொருவர் பேசிக்கொள்ளக்கூடாது, அங்கே, இங்கே என்று கை நீட்டி
பேசக்கூடாது, விமான பயண விதிமுறைகளுக்கு கட்டுப்பட்டு நடந்துகொள்ள வேண்டும் என்று அவர்
அறிவுரைகள் வழங்கினார். </p><p>
</p><p>
நடிகர்கள் தாமு, சின்னி ஜெயந்த், மயில்சாமி போன்றவர்கள் மிமிகிரி செய்யத் தெரிந்த
கலைஞர்கள் அதனால் அவர்களிடம் தனிப்பட்ட முறையில் இத்தகைய நிபந்தனைகளை
விஜயகாந்த் தெரிவித்தார். மேலும் முகம் தெரிந்தவர்கள் தவிர மற்ற யாருடனும் நடிகர்,
நடிகைகள் போட்டோவுக்கு போஸ் கொடுக்கக்கூடாது என்று கேட்டுக் கொள்ளப்பட்டுள்ளனர். </p><p>
</p><p>
காரணம் என்ன? </p><p>
</p><p>
சமீபத்தில் லண்டன் சென்ற நடிகர் கமல்ஹாசனை போலீசார் சந்தேகத்தின் பேரில்
விசாரணைக்கு உட்படுத்தியது, நடிகை சம்யுக்தவர்மா அமெரிக்க போலீசாரால் தீவிரவாதி என்று
சந்தேகத்தின் பேரில் கைது செய்தது போன்ற சம்பவங்களே நடிகர், நடிகைகளிடம் விஜயகாந்த்
விதித்த நிபந்தனைகளுக்கு காரணம் ஆகும். </p>

<p> </p>

<p> </p>

<p>நடிகருடன் கஜாலா காதலுக்கு
பெற்றோர் எதிர்ப்பு தற்கொலை முயற்சியில் புதிய தகவல்கள்</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>கஜாலா </p>

<p> </p>

<p>ஜதராபாத், ஜூலை 24- தெலுங்கு நடிகர் உதய்கிரணை நடிகை கஜாலா
காதலித்தார். அதற்கு பெற்றோர் எதிர்ப்பு தெரிவித்ததால் அவர் தூக்க மாத்திரை தின்று
தற்கொலைக்கு முயன்றார் என்ற புதிய தகவல் வௌியாகி உள்ளது. </p><p>
</p><p>
கஜாலா </p><p>
</p><p>
ஏழுமலை படத்தில் அர்ஜூன் ஜோடியாக நடித்த நடிகை கஜாலா நேற்று முன் தினம் ஐதராபாத்தில்
தூக்க மாத்திரை தின்று தற்கொலைக்கு முயன்றார் அல்லவா? அவர் தெலுங்கு நடிகர் உதய்
கிரணை காதலித்தார். அதற்கு பெற்றோர் எதிர்ப்பு தெரிவித்ததால் தற்கொலைக்கு முயன்றார்
என்று திடுக்கிடும் தகவல் வௌியாகி உள்ளது. அதுபற்றி விவரம் வருமாறு:- </p><p>
</p><p>
நடிகை கஜாலாவின் நிஜப் பெயர் ராஜி. இவர் தெலுங்கு படத்தில்தான் முதன்முதலில் நடிகையாக
அறிமுகமானார். 3 வருடங்களுக்கு முன்பு மும்பை யில் உள்ள பவன்ஸ் கல்லூரியில் கஜாலா பிளஸ்
2 படித்துக்கொண்டிருந்தார். அப்போது ஐதராபாத்தில் உள்ள உறவினர் வீட்டு விழாவில்
கலந்துகொள்ள வந்திருந்தார். அப்போது தெலுங்கு நடிகர் ஜெகபதிபாபுவின் கண்ணில் கஜாலா
தென்பட்டார். உடனே அவர் சினிமாவில் நடிக்க உனக்கு விருப்பம் இருக்கிறதா என்றார்.
அதுபற்றி தனது தோழிகளிடமும் உறவினர்களிடம் கலந்தாலோசித்தார். தேடி வரும் வாய்ப்பை நழுவ
விடவேண்டாம் என்று அவர்கள் கூறினார்கள். இதையடுத்து கஜாலா சினிமாவில் நடிக்க சம்மதித்தார்.
நேனே நின்னை பிரமிஸ்தானு என்ற படத்தில் கதாநாயகியாக நடிக்க ஒப்பந்தம்
செய்யப்பட்டார். இந்த படத்தில் நடித்துக்கொண்டிருந்தபோதே என்.டி.ராமராவின் பேரன்
ஜூனியர் என்.டி. ஆருடன் நடிக்க வாய்ப்பு வந்தது. அதில் கஜாலா நடித்தார். ஸ்டண்ட் நம்பர்
1 என்ற பெயரில் அப்படம் வௌியாகி சூப்பர் ஹிட் ஆனது இதைய டுத்து கஜாலா முன்னணி நடிகைகள்
பட்டியலில் இடம் பிடித்தார். அதைத்தொடர்ந்தே ஏழுமலை தமிழ் படத்தில் அர்ஜூன் ஜோடியாக
நடிக்க ஒப்பந்தம் ஆனார். இப்படம் தமிழில் வெற்றிகரமாக ஓடிக் கொண்டிருக்கிறது. பல தமிழ்
படங்களில் நடிக்கும் வாய்ப்பும் கஜாலாவுக்கு வந்துக்கொண்டிருந்தது. </p><p>
</p><p>
வௌிநாட்டில் காதல் </p><p>
</p><p>
இதற்கிடையில் கலுசுகோ வாலனி என்ற படத்தில் தெலுங்கு பட இளம் முன்னணி நடிகர் உதய்கிரணுடன்
ஜோடியாக நடித்தார். வௌிநாடுகளில் இவர்கள் நடித்த நெருக்கமான காதல் பாடல் காட்சிகள்
படமாக்கப்பட்டன. அப்போது இவர்களுக்குள் நிஜமாகவே காதல் மலர்ந்தது. </p><p>
</p><p>
மஸ்கட்டில் பெற்றோர் </p><p>
</p><p>
கஜாலா அரேபிய ஷேக் குடும்பத்தை சேர்ந்தவர். இவரது தந்தை அமீர்ஷேக், தாயார் ரஷியாஷேக்
இருவரும் மஸ்கட்டில் வியாபாரம் செய்து வருகிறார்கள். இவர்களுக்கு மூன்று பிள்ளைகள். கஜாலா
வின் மூத்த அண்ணன் தந்தைக்கு துணையாக மஸ்கட்டிலேயே இருக்கிறார். மற்றொரு அண்ணன் மும்பையில்
வேலை பார்த்து வருகிறார். மஸ்கட்டைவிட படிப்பதற்கு ஏற்ற இடம் மும்பை என்பதால் கஜாலா
மும்பை வந்தார். அதைத்தொடர்ந்துதான் அவர் திரையுலகுக்கு வந்தார். </p><p>
</p><p>
தங்களது ஒரே மகள் கஜாலா சினிமாவில் நடிப்பதை பெற்றோர் விரும்பவில்லை. அவரை தொலை
பேசியில் அடிக்கடி அழைத்து மஸ்கட்டுக்கு தங்களுடன் வந்து தங்கும்படியும் தாங்கள் பார்க்கும்
மாப்பிள்ளையை கல்யாணம் செய்துகொள்ள வேண்டும் என்றும் கூறி வந்தனர். ஆனால் கஜாலாவுக்கு
சினிமா ஆசை விடவில்லை. மேலும் தனது காதலன் உதய்கிரணை மறந்து விடவும் தயாராக இல்லை.
வீட்டில் பெற்றோர் மாப்பிள்ளை பார்க்கும் விஷயத்தை உதய்கிரணிடம் தெரிவித்து தன்னை
திருமணம் செய்து கொள்ளும்படி கஜாலா கேட்டார். ஆனால் உதய்கிரண் தான் இப்போதுதான்
சினிமாவில் வளர்ந்து வருவதாகவும் உடனடியாக திருமணம் செய்து கொள்ளாமல் சில வருடம்
காதலர்களாகவே இருப்போம் என்று கூறினாராம். </p><p>
</p><p>
மனம் உடைந்தார் </p><p>
</p><p>
கஜாலாவின் காதல் விவகாரம் அவரது அண்ணன் மூலமாக பெற்றோருக்கு தெரிய வந்தது. கோபம்
அடைந்த அவர்கள் கஜாலாவை தொலை பேசியில் அழைத்து திட்டினார்கள். இதனால் மனம்
உடைந்தார். சம்பவதினத் தன்று தெலுங்கு படத்தின் படப் பிடிப்பை முடித்துவிட்டு கஜாலா
தோழியுடன் ஓட்ட லுக்கு திரும்பினார். அங்கு அவர் அவில் மாத்திரைகளை (தூக்க மாத்திரை)
அளவுக்கு அதிகமாக தின்றார். கஜாலாவை அவரது தோழி எழுப்பியபோது மயங்கிய நிலையில் அவர்
முணுமுணுத்துக் கொண்டிருந்ததை பார்த்து பயந்தார். கஜாலாவின் அண்ணனுக்கு தொலைபேசியில் தகவல்
தெரிவித்தார். உடனே கஜாலாவின் அண்ணன் ஐதராபாத்தில் சர்வாதிகாரி படப்பிடிப்பில்
இருந்த நடிகர் அர்ஜூனுக்கு தகவல் சொன்னார். அடுத்த சில நிமிடத்தில் கஜாலா தங்கி இருந்த
ஓட்டலுக்கு வந்த அர்ஜூன் அவரை மருத்துவமனைக்கு கொண்டு சென்று சேர்த்தார். அங்கு
டாக்டர்கள் அளித்த தீவிர சிகிச்சையில் உயிர் பிழைத்தார். சிகிச்சைக்கு பிறகு வீடு
திரும்பிய கஜாலா இன்றுமுதல் படப்பிடிப்பில் கலந்துகொண்டு நடிப்பார் என்று தெரிகிறது. </p><p>
</p><p>
காதல் பற்றி </p><p>
</p><p>
நடிகர் உதய்கிரண் </p><p>
</p><p>
நடிகை கஜாலாவின் காதலன் நடிகர் உதய்கிரண் தெலுங்கு பத்திரிகை ஒன்றுக்கு பேட்டி அளித்தார்.
அப்போது அவரிடம் நீங்கள் யாரையாவது காதலிக்கிறீர்களா என்று கேட்ட போது, எனது
பெற்றோரை நான் காதலிக்கிறேன். என்னை காதலிக்கும் எல்லோரையும் நான் காதலிக்கிறேன்
என்று பதில் அளித்தார். </p>

<p> </p>






</body></text></cesDoc>