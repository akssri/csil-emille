<cesDoc id="tam-w-dinakaran-cinema-02-09-29" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-02-09-29.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-09-29</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-09-29</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>தாறுமாறாக கார் ஓட்டி ஒருவரை
கொன்றார், நடிகர் சல்மான்கான் போலீசில் சரண் அடைந்து ஜாமீனில் விடுதலை</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>நடிகர் சல்மான்கானை
போலீசார் கைது செய்து கொண்டு வந்த காட்சி </p>

<p> </p>

<p>மும்பை, செப்.29- பிரபல இந்தி நடிகர் சல்மான்கான் மும்பையில்
தாறுமாறாக கார் ஓட்டியதில் கார் பேக்கரி ஒன்றுக்குள் புகுந்தது. அங்கே
உறங்கிக்கொண்டிருந்த ஒருவர் நசுங்கிப் பலியானார். 4 பேர் படுகாயமடைந்தார்கள். </p><p>
</p><p>
சல்மான்கான் </p><p>
</p><p>
மும்பை படவுலகத்தின் பிரபல நடிகர்களில் ஒருவர் சல்மான்கான். நேற்று நள்ளிரவு 2.40
மணியளவில் அவர் மும்பை நகரின் வடமேற்கே உள்ள பந்த்ரா புற நகர்ப்பகுதியில் கார் ஓட்டி
வந்தார். காரில் சல்மான்கானின் நண்பர் ஜமால்கான் மற்றும் சல்மானின் பாதுகாப்புக்காக
நியமிக்கப்பட்ட போலீஸ்காரர் ஆகியோர் இருந்தார்கள். </p><p>
</p><p>
மோதினார் </p><p>
</p><p>
சல்மான்கான் ஓட்டிவந்த கார் அங்குள்ள ஹில்ரோடு பகுதியில் இருக்கும் நீல்சாகர் ஓட்டல்
அருகே வந்தது. சல்மான்கான் அப்போது அலட்சியமாக காரையோட்டியதால் கார் நிலைதடுமாறி
சாலையோரம் இருந்த பேக்கரி ஒன்றுக்குள் முட்டி மோதிக் கொண்டு புகுந்தது. அந்த
பேக்கரிக்குள் தூங்கிக் கொண்டிருந்த தொழிலாளர்கள் 5 பேர் மீது கார் ஏறியது. </p><p>
</p><p>
ஒருவர் பலி </p><p>
</p><p>
அதில் நூர்உல்கான் (வயது 42) என்ற தொழிலாளி அதே இடத்தில் நசுங்கி பலியானார். மற்ற 4
பேரும் காயமடைந்தார்கள். நள்ளிரவு 2.50 மணியளவில் இந்த பயங்கரம் நடந்தது. </p><p>
</p><p>
விபத்து நடந்ததும் சல்மான்கான் அந்த இடத்தை விட்டு தப்பியோடி விட்டார். அவருடன் வந்த
போலீஸ்காரர், காயமடைந்த 4 பேரையும் பாபா மருத்துவமனைக்கு அனுப்பி விட்டு இதுபற்றி
போலீசுக்கு தகவல் கொடுத்தார். </p><p>
</p><p>
சரணடைந்தார் </p><p>
</p><p>
விபத்து நடந்த 8 மணி நேரம் கழித்து நேற்று பகல் 11 மணியளவில் சல்மான்கான் பந்த்ரா
போலீஸ் நிலையத்தில் சரணடைந்தார். புதிதாக இறக்குமதி செய்யப்பட்ட தனது டயோட்டா
லாண்ட் குரூசர் கார் திடீரென கட்டுப்பாட்டை இழந்து பிளாட்பாரத்தில்
தூங்கிக்கொண்டிருந்தவர்கள் மீது மோதி விட்டதாக சல்மான்கான் கூறினார். </p><p>
</p><p>
வழக்கு </p><p>
</p><p>
போலீசார் அவர் மீது, கவனக்குறைவாக கார்ஓட்டி ஆளைக் கொன்றதற்காக 304 ஏ பிரிவின் கீழ்
வழக்குப் பதிவு செய்தனர். காரை படுவேகமாக ஓட்டியதாக 279 பிரிவின் கீழும் காயம்
ஏற்படுத்தியதற்காக 338 பிரிவின்கீழும் வழக்கு பதிந்தனர். மோட்டார் வாகனசட்டப் படியும்
அவர் மீது மற்றொரு பிரிவில் வழக்கு பதிவு செய்யப்பட்டது. </p><p>
</p><p>
குடி போதையா? </p><p>
</p><p>
பின்னர் குடித்து விட்டு சல்மான்கான் மதுபோதையில் கார் ஓட்டினாரா என்பதை கண்டுபிடிக்க
அவரை ஜே.ஜே. அரசு மருத்துவமனைக்கு அழைத்துச் சென்றனர். அங்கே சல்மான்கான் உடலில் ரத்தம்
எடுத்து அதில் மது கலந்திருக்கிறதா? என்று சோதனை நடத்தப்பட்டது. அதன் பிறகு மீண்டும்
அவரை பந்த்ரா காவல் நிலையத்துக்கு கொண்டு வந்தனர். அப்போது சல்மான்கானின் வக்கீல்
அங்கு வந்தார். சல்மான்கான் மீது ஜாமீனில் விடக்கூடிய குற்றப்பிரிவில் வழக்கு பதிவாகி
இருப்பதால் ரூ.950 கட்டி சல்மான்கானை அவர் ஜாமீனில் விடுவித்துச் சென்றார். </p><p>
</p><p>
உயிர் ஊசல் </p><p>
</p><p>
இதனிடையே சல்மான்கானின் கார் ஏறி படுகாய மடைந்த 4 பேரும் பந்த்ராவில் உள்ள பாபா
மருத்துவமனையில் சிகிச்சை பெற்றுவருகிறார்கள். அதில் ஒருவரது உயிர் ஊசலாடுகிறது.
டாக்டர்கள் அவருக்கு தீவிர சிகிச்சை அளித்து வருகிறார்கள். </p><p>
</p><p>
சர்ச்சைகளுக்கு பேர் போன சல்மான்கான் </p><p>
</p><p>
சர்ச்சைகளில் சிக்குவது என்றால் சல்மான்கானுக்கு சர்க்கரைப் பொங்கல் சாப்பிடுவது போல.
வம்பு வழக்குகளில் அடிக்கடி இவரது பெயர் அடிபடும். 98-ம் ஆண்டு ராஜஸ்தான் மாநிலத்தில் ஹம்
சாத் சாத் எயின் சினிமா படப்பிடிப்பு நடந்தது. அப்போது சக நடிகைகளான சோனாலி
பிந்த்ரே, நீலம் ஆகியோருடன் மான்வேட்டையாட சென்றார் சல்மான்கான். அபூர்வ இனமான
கறுப்பு கலைமானை சுட்டுக்கொன்றார். அது தொடர்பாக ஜோத்பூர் கோர்ட்டில் வழக்கு நடந்து
வருகிறது. </p><p>
</p><p>
அந்நிய செலாவணி விதிமீறல் வழக்கு தொடர்பாக அமுலாக்கப்பிரிவு அலுவலகம் சல்மான்கானை
விசாரணைக்கு அழைத்தது. அங்கு நிருபர்கள் புகைப்படக்காரர்களுடன் சல்மான்கான் கைகலப்பில்
ஈடுபட்டு பரபரப்பை ஏற்படுத்தினார். </p><p>
</p><p>
உலக அழகி ஐஸ்வர்யாராயை காதலிப்பதாக கூறி சல்மான்கான் நடத்திய அமளி துமளி மும்பையில்
மிகவும் பிரபலம். ஒருநாள் இரவு ஐஸ்வர்யாராய் வீட்டுக்குள் அத்துமீறி நுழைய முயன்ற
சல்மான்கான் கடும் ரகளை செய்ததால் ஐஸ்வர்யாவின் தந்தை போலீசில் புகார் செய்தார்.
பந்த்ரா போலீசார் வந்து சல்மான்கானை எச்சரித்து அனுப்பிவைத்தனர். ஐஸ்வர்யா நடிக்கும்
திரைப்ப சூட்டிங்குகளில் வந்து கலாட்டா செய்வதை சல்மான் ஒரு கடமையாகவே வைத்திருந்தார்.
செட்டில் அவர் அடிக்கடி வந்து தகராறு செய்வதும் படப்பிடிப்பு நின்று போவதும் வழக்கமான
விஷயம். ஒருமுறை ஐஸ்வர்யா ராய் சூட்டிங் முடிந்து தன்னுடன் வர மறுத்ததற்காக வௌியில்
நிறுத்தி வைக்கப்பட்டிருந்த ஐஸ்வர்யாராயின் கார் மீது காரை மோதி சல்மான்கான் அதை
சின்னாபின்னப்படுத்தியது குறிப்பிடத்தக்கது. அண்மையில் ஐஸ்வர்யாராய் பத்திரிகையாளர்களை
கூட்டி சல்மானுக்கும் எனக்கும் இடையில் காதல் எதுவும் இல்லை என்று அதிரடியாக அறிவித்தார்.
அதன்பின்னும் சல்மான்கானின் திருவிளையாடல்கள் நிற்கவில்லை. சில நாட்களுக்கு முன்
பூனேயில் ஷாருக்கானுடன் நடித்துக் கொண்டிருந்த ஐஸ்வர்யாராயுடன் சல்மான் கலாட்டா செய்து
படப்பிடிப்பு நின்று போனது. பிறகு அந்த படத்தில் ஐஸ்வர்யா ராய் நீக்கப்பட்டு அவருக்கு
பதில ராணி முகர்ஜி சேர்க்கப்பட்டார். மும்பை தாதாக்களும் பாலிவுட் படவுலகத்துக்கும்
ரகசிய தொடர்பு இருப்பதாக தகவல்கள் வௌிவந்தன அல்லவா? அப்படி ஒரு தகவல் வௌியாக
முழுமுதல் காரணமாக இருந்தவர் சல்மான்கான்தான். அவர் நடித்த சோரி சோரி சுப்கே சுப்கே
படத்தில்தான் முதன்முறையாக இந்த பிரச்சினை கிளம்பியது. அந்த படத்தில் நடித்த நடிகை
பிரீத்தி ஜிந்தாவை மிரட்டி மும்பை தாதா சோட்டா சக்கீல் ஏராளமான பணம் கறந்ததாக
குற்றச்சாட்டு இருப்பது குறிப்பிடத்தக்கது. மும்பையின் பிரபல ஆடியோ அதிபர் குல்சன் குமார்
கொலைவழக்கு தொடர்பாகவும் சல்மான்கான் விசாரிக்கப்பட்டு வருகிறார். </p><p>
</p><p>
கார் மோதி காயமடைந்த நபர் கண்ணீர் பேட்டி </p><p>
</p><p>
சல்மானின் கார் மோதிய பந்த்ரா பகுதி நேற்று போர்க்களம் போல காணப்பட்டது. கார்
மோதியதால் உடைந்து நொறுங்கிய பேக்கரி கடை எங்கும் ரத்தக்கறை திட்டுதிட்டாக இருந்தது.
கார் டயர்களில் இருந்த ரத்தம் தரையில் சிவப்புக்கோடு ஒன்றை வரைந்திருந்தது. கடையின்
அஸ்பெஸ்டாஸ் ஷட்டரை உடைத்துக்கொண்டு கார் உள்ளே நுழைந்ததால் கடைசின்னா பின்னமாகி
அலங்கோலமாக காட்சியளித்தது. எங்கு பார்த்தாலும் ரத்தக்கறை, கண்ணாடி சிதறல்களாக
கிடந்தது. சல்மான்கானின் கார் அந்த இடத்தில் காட்சி பொருளாக நின்றது.
எம்.எச்.01-டி.ஏ.-32 எண்ணுள்ள அந்த காரை போலீசார் பிறகு வேறு வாகனம் மூலம்
காவல்நிலையத்துக்கு இழுத்து வந்தனர். </p><p>
</p><p>
இதனிடையே சல்மானின் கார் ஏறி கால் நொறுங்கிய எம்.ஷேக் என்ற 22 வயது நபர் அது பற்றி
கண்ணீர் பேட்டி அளித்துள்ளார். நள்ளிரவில் திடீரென ஒரு சத்தம். திடுக்கிட்டு
விழித்துப்பார்த்தால் எனது கால்மீது கறுப்புநிற கார் டயர் ஏறி நிற்கிறது. என் அருகில்
படுத்திருந்த நூர் உடல்சிதறி சின்னா பின்னமாக கிடக்கிறான். அதை பார்த்ததும் நான்
மயங்கி நினை விழந்து விட்டேன். நான் கடையோரம் படுத்திருந்த இடம் சாலையை விட்டு 6 அடி
தூரத்தில் உள்ளது. கார் அவ்வளவு தூரம் பிளாட்பாரத்தில் ஏறிவந்து எங்கள் மீது மோதியது
ஆச்சரியமாக உள்ளது. அறுவை சிகிச்சை செய்துதான் என் காலை சரிசெய்ய முடியும் என்று
டாக்டர்கள் கூறிவிட்டனர். </p><p>
</p><p>
இவ்வாறு அவர் கண்ணீர் மல்க கூறியுள்ளார். </p><p>
</p><p>
போலீஸ் பாதுகாப்பு கேட்டுப் பெற்ற சல்மான்கான் </p><p>
</p><p>
நடிகர் சல்மான் கானின் செல்லப் பெயர் சாலு. சல்மான்கானின் (வயது 37) இவரது தந்தை
சலீம்கான் இந்தி சினிமாவின் பிரபல வசனகர்த்தா ஆவார். சல்மான் நடித்த மெய்னி பியார்
கியா, சோரி சோரி சுப்கே சுப்கே குச்குச் ஹோத்தா ஹை ஹம் தில் தீ சுக்கே சனம் தீவானா
மஸ்தானா என்ற படங்கள் சக்கைபோடு போட்டன. இதில் சோரி சோரி சுப்கே சுப்கே படத்தை
தயாரித்தவர் பிரபல வைர வியாபாரி பரத்ஷா ஆவார். மும்பை தாதா சோட்டா சக்கீலுடன்
தொடர்பு இருப்பதாக கருதி தற்போது அவர் மீது வழக்கு நடப்பது குறிப்பிடத்தக்கது.
இந்தநிலையில் சில குண்டர்களிம் இருந்து மிரட்டல் வருவதால் தனக்கு போலீஸ் பாதுகாப்பு
வேண்டும் என்று சல்மான் கேட்டதால் அவருக்கு துணையாக ஒரு போலீஸ் பாதுகாவலர்
நியமிக்கபட்டார். ஆனால் உண்மையில் சல்மான்கானிடம் இருந்து தான் மற்றவர்களுக்கு
பாதுகாப்பு தேவைப்பட்டு வருகிறது. </p><p>
</p><p>
விபத்துக்கு முன் விருந்தில் ஆட்டம் போட்ட சல்மான் </p><p>
</p><p>
பிளாட்பாரத்தில் தூங்கும் ஏழை எளிய மக்கள் மீது நடிகர்கள் காரை கொண்டு வந்து ஏற்றுவது
மும்பையில் புதிதல்ல. சில ஆண்டுகளுக்கு முன், மறைந்த ராஜ்குமாரின் மகன் நடிகர் புரு
இதேப்போல தாறுமாறாக கார் ஓட்டி வந்து சிலரை பலியாக்கினார். இந்தநிலையில் தற்போது
சல்மான்கான் இதே காரியத்தைச் செய்திருக்கிறார். விபத்து நடப்பதற்கு முன் சல்மான் ஒரு
இரவுவிருந்தில் கலந்து கொண்டு ஆட்டம்போட்டதாக கூறப்படுகிறது. அதன்பிறகு ஜாலிமூடில் கார்
ஓட்டி வந்த போதுதான் இந்த விபத்து நடந்திருக்கிறது. </p>

<p> </p>

<p> </p>

<p>சீடர்களிடம் சொன்னதுபோல்
50 வருடம் கழித்து பாபாஜி பெருமை விளக்கியது பாக்கியம் பாபா படம் பற்றி ரஜினி கருத்து</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>ரஜினிகாந்த் </p>

<p> </p>

<p>சென்னை, செப். 29- பாபா வெற்றி தோல்வி பற்றி வருத்தம் இல்லை.
சீடர்களிடம் சொன்னதுபோல் 50 வருடம் கழித்து பாபாஜி பெருமையை விளக்கியதே பாக்கியம்
அதுவே எனக்க போதும் என்று ரஜினி காந்த் கூறினார். </p><p>
</p><p>
ரஜினி </p><p>
</p><p>
பாபா படம் திரையிட்ட 116 தியேட்டர் உரிமையாளர்கள் மற்றும் விநியோகஸ்தர்களுக்கு பல
கோடி ரூபாயை ரஜினிகாந்த் நேற்று முன்தினம் திருப்பி அளித்தார் அல்லவா? அப்போது பாபா
படம் பற்றி ரஜினிகாந்த் சொன்ன கருத் தினை அவரது நெருங்கிய நண்பர் நாகராஜன்ராஜா
வௌியிட்டார். அவர் கூறிய தாவது:- </p><p>
</p><p>
பாபாஜி திடீரென்று வருவார், திடீரென்று மறைவார். 1952-ம் ஆண்டு நடந்த கும்ப மேளாவில்
திடீரென்று பாபாஜி தோன்றினார். அப்போது அவரது சீடர்கள் உங்கள் புகழ்வௌி உலகுக்கு
தெரியவில்லையே நீங்கள் ஏன் உங்களைப்பற்றி சொல்வ தில்லை என்று கேட்டனர். அதற்கு
பாபாஜி இன்னும் 50 வருடம் கழித்து என்னைப்பற்றி வௌி உலகுக்கு தெரியவரும் என்று பதில்
அளித்தார். தற்போது 2002-ம் ஆண்டில் பாபாஜியை பற்றி ரஜினிகாந்த் பாபா படத்தின் மூலம்
வௌி உலகுக்கு தெரியவைத்திருக்கிறார். </p><p>
</p><p>
பாபா படம் எதிர்பார்த்த வெற்றி பெறவில்லை என்று கூறப்பட்டாலும் பாபாஜியை பற்றி
இப்படம் மூலம் நான் சொன்னதே எனக்கு பாக்கியம். அதுவே எனக்கு போதும் என்று ரஜினிகாந்த்
என்னிடம் தெரிவித்தார். பாபா படம் பற்றி பத்திரிகைகள், டி.வி.க்கள் என பரபரப்பாக
விளம்பரங்கள் வந்தபோதே அனைவரையும் அழைத்து படத்தை பற்றி சொல்லிவிடுங்கள் என்று
ரஜினிசாரிடம் கேட்டபோது ஆண்டவன் எனக்கு ஒரு சோதனை வைத்திருக்கிறான். அதை அவனே முடித்து
வைப்பான் என்று மட்டும் கூறினார். அதனால் பாபா படத்தைபற்றி வந்த எந்த செய்திக்கும்,
தாக்குதல்களுக்கும் அவர் பதில் சொல்லவில்லை. சோதனை களை எல்லாம் அவர் சாதனைகளாக்கி
காட்டி இருக்கிறார். உப்பிட்ட தமிழ்மண்ணை நான் மறக்க மாட்டேன் உயிர் போனால்
இங்கேதான் ஓடிவிடமாட்டேன் என்று அவர் படத் தில் கூறியதுபோல்தான் நிஜத்திலும் செய்வார்.
இது உறுதி. </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
இமயமலை செல்கிறார் </p><p>
</p><p>
ரஜினிகாந்த் வருகிற 1-ந் தேதி சென்னையில் நடக்க உள்ள சிவாஜி எழுதிய எனது சுயசரிதை
புத்தக வௌியீட்டு விழாவில் கலந்துகொள்கிறார். அதைத்தொடர்ந்து இரண் டொரு நாளில்
அவர் அமெரிக்கா புறப்பட்டு செல்கிறார். மறைந்த தனது குருஜி ஆசிரமத்தில் சில நாட்கள்
தங்கி இருந்துவிட்டு பின்னர் அவர் இமயமலை செல்ல திட்டமிட்டிருக்கிறார். </p>

<p> </p>

<p> </p>

<p>அக்டோபர் 1-ந்தேதி
சிவாஜி 75-வது பிறந்தநாள் நடிகர் தின விழாவாக கொண்டாட்டம் விஜயகாந்த் அறிக்கை</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>விஜயகாந்த் </p>

<p> </p>

<p>சென்னை, செப். 29- அக்டோபர் 1-ந்தேதி சிவாஜியின் 75-வது
பிறந்தநாள் நடிகர்தின விழாவாக கொண்டாடப்படும் என்று விஜயகாந்த் அறிவித்து உள்ளார். </p><p>
</p><p>
நடிகர் தினம் </p><p>
</p><p>
இதுகுறித்து தென்னிந்திய நடிகர் சங்க தலைவர் விஜயகாந்த் வௌியிட்டுள்ள அறிக்கை வருமாறு:- </p><p>
</p><p>
தென்னிந்திய நடிகர் சங்கத்தின் முன்னாள் தலைவரும், நடிகர் சங்கத்தை வளர்த்த மூத்த
கலைஞருமான பத்ம, செவாலியே, நடிகர் திலகம் சிவாஜி கணேசன் பிறந்த நாள் அக்டோபர்
முதல் தேதியாகும். அன்றைய தினத்தை நடிகர் தினமாக வெகு சிறப்புடன் கொண்டாடுவது என்று
தென்னிந்திய நடிகர் சங்கத்தால் ஏற்கெனவே தீர்மானிக்கப்பட்டுள்ளது. </p><p>
</p><p>
எனவே, அக்டோபர் 1-ம் தேதியன்று காலை 10 மணியளவில் தென்னிந்திய நடிகர் சங்கம் சுவாமி
சங்கரதாஸ் கலையரங்கத்தில் நடிகர் தினம் விழா நடைபெறுகிறது. விழாவிற்கு நடிகர்
சங்கத்தலைவர் விஜயகாந்த் தலைமை ஏற்கிறார். இந்த விழாவில் திரை உலகின் முன்னணிக்
கலைஞர்கள், மூத்த நடிகர், நடிகைகள் மற்றும் சங்கத்தின் அனைத்து உறுப்பினர்களும்
கலந்து கொள்கிறார்கள். </p><p>
</p><p>
இலவச வேட்டி - சேலை </p><p>
</p><p>
நடிகர் திலகத்தின் 75-வது பிறந்த நாளையொட்டிய விழாவில் 75 நலிந்த நடிகர், நடிகைகளுக்கு
வேட்டி, சேலை வழங்கப்படுகின்றன. இந்த வேட்டி, சேலைகளை சிவாஜி பிலிம்ஸ் சார்பில்
ராம்குமார், நடிகர் பிரபு ஆகியோர் விழாவில் கலந்துகொண்டு வழங்குகிறார்கள். விழாவில்
பங்கேற்கும் அனைத்து உறுப்பினர்களுக்கும் நடிகர் சங்கம் சார்பில் மதிய உணவு வழங்கப்படுகிறது.
</p><p>
</p><p>
விழா ஏற்பாடுகளை நடிகர் சங்க பொதுச்செயலாளர் ஆர்.சரத்குமார், சங்கத் துணைத்தலைவர்கள்
எஸ்.எஸ். சந்திரன், நெப்போலியன், பொருளாளர் கே.என்.காளை மற்றும் செயற்குழு
உறுப்பினர்கள் முன்னின்று கவனித்து வருகின்றனர். விழாவில் நடிகர் சங்கத்தின் அனைத்து
உறுப்பினர்களும் தவறாமல் கலந்துகொண்டு விழாவை சிறப்பிக்குமாறு கேட்டுக்கொள்கிறேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறி உள்ளார். </p>

<p> </p>

<p> </p>

<p>உயிர் உள்ளவரை தி.மு.க.
தான்- கலைஞரை அரியணையில் ஏற்றுவேன் காதலிக்கும் போதே பெற்றோரிடம் கூறிவிடுங்கள்
- நடிகர் சரத்குமார்</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>சரத்குமார் </p>

<p> </p>

<p>மதுரை, செப், 29- உயிர் உள்ளவரை தி.மு.க. தான்- கலைஞரை
அரியணையில் ஏற்றுவேன் காதலிக்கும் போதே பெற்றோரிடம் கூறிவிடுங்கள் என்று நடிகர் சரத்குமார்
கூறியுள்ளார். </p><p>
</p><p>
சரத்-ராதிகா பதில் </p><p>
</p><p>
காரைக்குடியில் நடந்த தமிழர் தந்தை சி.பா.ஆதித்த னார் பிறந்த நாள்விழாவில் அழகப்பா
பல்கலைக்கழக மாணவ-மாணவிகளின் கேள்விக்கு நடிகர் சரத்குமார் எம்.பி., ராதிகா சரத்குமார்
பதில் அளித்தனர். அதன் விபரம் வருமாறு:- </p><p>
</p><p>
மாணவிகள்:- நாங்கள் கேள்வி கேட்க ரெடி மேடம் நீங்கள் ரெடியா? </p><p>
</p><p>
ராதிகா (புண் சிரிப்புடன்):- ரசிகர்களே நீங்கள் ரெடியா? </p><p>
</p><p>
ரத்திடம் கேள்வி:- ஹலோ சார் சினிமாவில் உள்ள நீங்கள் எம்.பி.யாக இருக்கிறீர்கள்.
அரசியலுக்கு வந்தது எப்படி? </p><p>
</p><p>
பதில்:- நான் என்று ஓட்டுப் போடும் தகுதியைப் பெற்றேனோ அன்றே அரசியலுக்கு
வந்துவிட்டேன். அதாவது ஒரு கட்சிக்குத்தானே ஓட்டுப் போடுகிறோம். </p><p>
</p><p>
ரஜினி </p><p>
</p><p>
கேள்வி:- ரஜினி அரசியலுக்கு வருவாரா? </p><p>
</p><p>
சரத் பதில்:- அரசியலுக்கு வருவாரா? வரமாட்டாரா? என்பது ஒருவரின் தனிப்பட்ட
விருப்பம். ரஜினி, கமல், விஜய் அரசியலுக்கு வருவார்களா, ஆன்மீகத்திற்கு வருவார்களா
என்பதை எப்படி சொல்ல முடியும். </p><p>
</p><p>
கேள்வி:- ரஜினி நடித்த படம் எது பிடிக்கும்? </p><p>
</p><p>
பதில்:- ஆறிலிருந்து அறுபது வரை எனக்கு பிடித்த படம். நல்லா நடித்திருந்தார். </p><p>
</p><p>
கேள்வி:- ஆன்மீகத்தில் ஈடுபடுவீர்களா? </p><p>
</p><p>
பதில்: நான் தி.மு.க.வில் இருக்கிறேன். ஆன்மீகம் முக்கிய மானதாக நினைத்ததற்கு காரணம்
சிறுவயதில் இருந்து குளித்தவுடன் சாமிகும்பிட வேண்டும் என்பது எனது தாய் சொல்லிக்
கொடுத்தது. நீங்கள் (மாணவர்கள்) எந்த நடிகரை கேட்கிறீர்கள் என்று தெரியாது (சிரிப்பு)
நிச்சயம் எனக்கு தயக்கம் கிடையாது. என்னைப் போல் 80, 90 சதவீதம் பேர்
வாக்களித்திருந்தால் நிச்சயம் நல்ல அரசு அமைந்திருக்கும். </p><p>
</p><p>
கேள்வி:- சினிமாவில் ஒதுங்கும் நேரம் வந்தால் வாலண்டரி ரிட்டையர்மெண்ட்
கொடுப்பீர்களா? </p><p>
</p><p>
பதில்: நான் தமிழன். தமிழன் என்ற உணர்வுடன் நடந்து கொள்வேன். சாதி மதக் கலவரம்
நாட்டை பிரிப்பதை எதிர்ப்பேன். சினிமா துறையில் நடிப்பதற்கு வயது வரம்பு இல்லை.
நீங்கள் ஏற்றுக்கொள்ளும் வரை நடிப்பேன். அரசியலில் இருந்து கொண்டு தான் இருக்கிறேன்.
சந்தேகமென்ன தலைவர் கலைஞர் எங்களை சாமி கும்பிடக்கூடாது என்று கூறியதில்லை.
நெற்றியில் இருந்து கரைந்த குங்குமத்தை பார்த்து குங்குமம் கரைகிறது என்று சொல்லியதும்
அழித்ததை பத்திரிகையில் கலைஞர் சாமி கும்பிடக்கூடாது எனக் கூறியதாக பத்திரிகையில்
திரித்துப் போட்டிருந்தார்கள். </p><p>
</p><p>
செட்டிநாட்டு மருமகள் </p><p>
</p><p>
கேள்வி: (ராதிகாவிடம்): செட்டிநாட்டுச் சீமைக்கு நீங்கள் அடிக்கடி வருவது செட்டி
நாட்டுச்சீமை மருமகள் என்றா? தொழில் ரீதியாகவா? </p><p>
</p><p>
ராதிகா பதில்:- நான் ஏற்கனவே இந்தப்பகுதியில் படத்தில் நடிக்க வந்துள்ளேன். உங்கள்
கணவர் ஆண் ஆதிக்கம் உள்ளவரா? ஆண் ஆதிக்கம் என்றோ பெண் ஆதிக்கம் என்றோ
நினைப்பவர் இல்லை. அப்படிப்பட்ட மனிதர் இல்லை எனது கணவர். </p><p>
</p><p>
கேள்வி:- சித்தி வேடத்தில் நடித்த பின் சார்ப்பாக இருக்கிறீர்களே? </p><p>
</p><p>
ராதிகா பதில்: இந்த விழா குடும்பவிழா. இதில் கலந்து கொள்வதில் பெருமைப்படுகிறேன்.
சித்தி என்றால் கொடுமைக்காரி என்று பரவலாக நினைக்கிறார்கள். அது இருக்கக் கூடாது.
இந்தச்சித்தி கொடுமைக்காரி இல்லை (சிரிப்பு) </p><p>
</p><p>
கேள்வி: உங்களுடன் சேர்ந்து நடித்த நடிகைகள் பற்றி? </p><p>
</p><p>
சரத் பதில்:- ராதிகா நம்ம அண்ணாச்சி படத்தில் கன்னத்தில் அரையும் சீன். அந்தச்
சீனில் ஒரு நாளாவது நீங்கள் அறைய மாட்டீர்களா என்று காத்திருந்தேன் என்ற வாசகத்தைப் பேச
வேண்டும். அதை ராதிகா மறுத்துவிட்டார். காரணம் சும்மா இருக்கிற ஆண்களை தூண்டிவிடுவது போல்
ஆகிவிடும் என்பதால் எனக்கு அது மிகவும் பிடித்திருந்தது. </p><p>
</p><p>
கேள்வி:- சமீப காலமாக கதாநாயகிகளை மையமாக வைத்து படம் எடுத்து புது முகமாக இறக்குமதி
செய்கிறார்களே? </p><p>
</p><p>
பதில்:- சமீப காலமாக மட்டுமல்ல. எம்.ஜி.ஆர். காலத்தில் இருந்தே புதிய கதாநாயகிகளை
அறிமுகப்படுத்தினார்கள். அது கமர்சியல் பிரச்சினை. </p><p>
</p><p>
கமல் உடல் தானம் </p><p>
</p><p>
கேள்வி: பத்ம கமல் உடல் தானம் செய்து உள்ளது பற்றி தங்கள் கருத்து </p><p>
</p><p>
சரத் பதில்:- உடல் தானம் அனைவரும் செய்தால் அரசு மருத்துவமனையில் இடம் கிடைக்குமா?
உடல் தானம் என்பது நினைத்துப் பார்க்க முடியாதது. நான் 6ஆண்டு களுக்கு முன்பே உடலில்
உறுப்புகளை எழுதிக்கொடுத்துவிட்டேன். உடல் தானத்திற்கு தகுந்த இடம் கிடையாது. ஒருவர்
இறந்த பின் அவரது உடலை உறவினர்கள் ஒப்படைப்பார்களா? ஏற்று கொள்வார்களா?
என்பதை சொல்ல முடியாது. </p><p>
</p><p>
கேள்வி (ராதிகாவிடம்): டி.வி. தொடர் செட்டிநாட்டுப் பகுதியில் அதிகமாக எடுப்பதேன்
ராசியா? மாமனார் ஊர் பகுதி என்பதாலா? </p><p>
</p><p>
பதில்: நான் இப்பகுதியில் ஏற்கனவே படப்பிடிப்பில் கலந்து கொண்டு ஆச்சி சீனில் படித்துள்ளேன்.
கோவில், குளம், குடும்பம் எல்லாம் சூட்ட புல்லாக இருந்ததால் அண்ணா மலை தொடர் இங்கு
எடுத்தோம். </p><p>
</p><p>
கேள்வி (சரத்திடம்): பச்சை குத்திக்கொள்வது பற்றி உங்கள் கருத்து? </p><p>
</p><p>
பதில்: நான் தேசியக்கொடியை பச்சை குத்திக்கொண்டிருக்கிறேன். அதுகுறித்து தி.மு.க.வில்
இருக்கும் யாரும் ஆட்சேபனை தெரிவிக்கவில்லை. பச்சை குத்திக்கொள்ளலாம். அதில் கொடி
சின்னத்தை பச்சை குத்திக்கொள்ளக்கூடாது. ஏன் என்றால் நிரந்தரமாக அந்த மாசு அற்ற
கட்சியில் இருக்க முடியாது. இதுகுறித்து மேடை எதிரே 2-வது வரிசையில் உட்கார்ந்திருக்கும்
முன்னாள் அமைச்சர் தென்னவன் உள்ளார். (சிரிப்பு) </p><p>
</p><p>
கலைஞர் </p><p>
</p><p>
என்னைப்பொறுத்தவரை உறுதியாக இறுதி மூச்சு உள்ள வரை தி.மு.க.வில் தான் இருப்பேன். மீண்டும்
கலைஞரை அரியணையில் ஏற்றுவேன். மொழிக்காக, தமிழுக்காக போராடியவர் கலைஞர்.
சிலர் தமிழில் பேசுவார்கள். வௌியில் சென்ற இடத்தில் ஆங்கிலத்தில் பேசுவார்கள்.
தமிழ்மொழி முழுமையாக நாம் பயன்படுத்தாவிட்டால் 2050ல் அல்ல 2010லேயே தமிழ் மொழி
அழிந்துவிடும். </p><p>
</p><p>
கேள்வி (ராதிகாவிற்கு): திரைப்பட இசையில் எம்.எஸ். விஸ்வநாதன் இசை பிடிக்குமா?
இளையராஜா இசை பிடிக்குமா? </p><p>
</p><p>
பதில்: பழைய பாடல்களில் உள்ள இசை இன்றைய இளைஞர்கள் கூட ரசிக்கும் நிலையில் தான்
உள்ளது. தமிழகத்தில் மட்டுமல்ல. வௌியிலேயும் பழைய பாடலுக்கு மரியாதை தனிதான். எனது
மகனே பழைய பாடலில் உள்ள தத்துவத்தை ரசிப்பான். காலத்தில் சில மாற்றம் வரலாம். அது
போலதான் இசையும். அதனால் புதிய இசையை மதிக்கவில்லை என்று கூற முடியாது. </p><p>
</p><p>
கேள்வி: (சரத்திடம்) கோவில்களில் தாய் மொழிப் பிரச்சினை ஏன் வந்தது. உங்களது
உணர்வுப்பூர்வமான பதில் தேவை? </p><p>
</p><p>
பதில்: தாய் மொழிப்பற்று தமிழர்களிடம் இல்லை. அதனால் தான் கேள்வியில் கூட
தமிழ்மொழி பயன்படுத்த மறுக்கிறார்கள். </p><p>
</p><p>
கேள்வி:- திரைப்பட கலைஞர்கள் கலை நிகழ்ச்சிகளை வௌிநாட்டில் நடத்தியது ஏன்? </p><p>
</p><p>
பதில்: நான் திரைப்படம் சூட்டிங் எடுக்க ஊட்டி, பொள்ளாச்சி காரைக்குடி பகுதிக்கு அடிக்கடி
வந்தேன். ஊட்டியில் முதலில் சென்றபோது 10 ஆயிரம் பேர் கூடினார்கள். பிறகு படிப்படியாக
குறைந்தது. காரணம் அடிக்கடி வருவார் பார்க்கலாம் என்று நினைக்கிறார்கள். சமஸ்தானம் படம்
டைரக்ஷன் செய்த காஜா மைதீன் தான் எல்லா கலைஞர்களும் ஒரே இடத்தில் கூடினால் நிதி
கிடைக்கும் என்ற கருத்தை கூறினார். அதன் பேரில் வௌி நாட்டில் கலைநிகழ்ச்சி
நடத்தினோம். அழகப்பா பல்கலைக் கழக மாணவ-மாணவிகள் நிகழ்ச்சி நடத்தி ரூ.5கோடி நிதி
திரட்டித் தருகிறீர்கள் என்றால் இங்கும் காரைக்குடியில் நட்சத் திர விழா நடத்த வருகிறோம்.
(சிரிப்பு) </p><p>
</p><p>
கேள்வி: சினிமா உலகம் திருட்டு வீடியோவால் அழிந்து கொண்டிருக்கிறதா? நெஞ்சை
தொட்டுச் சொல்லுங்கள்? </p><p>
</p><p>
பதில்: திருட்டு வீடியோவினால் மட்டும் அழிவல்ல. தொழில் நுட்பம் தான் காரணம்.
வாரத்தில் 84 சீரியல் வௌிவருகிறது. தியேட்டரில் மக்கள் படம் பார்க்கவருவது இல்லை.
ரிலீஸ் படம் கூட திருட்டு வீடியோவில் பார்க்க முடிகிறதே. படம் கஷ்டப்பட்டு
எடுக்கிறார்கள். குறைந்த பட்சம் ஒரு படம் எடுக்க 11,2 கோடி ரூபாய் வரை செலவாகிறது.
வீட்டை அடமானம் வைத்து கூட எடுக்கிறார்கள். சூதாட்டம் போல் சினிமா தொழில் </p><p>
</p><p>
தேர்தலில் நிற்பீர்களா? </p><p>
</p><p>
கேள்வி- (ராதிகாவிற்கு) சோனியாகாந்தியை வௌி நாட்டுக்காரி என்று தனிப்பட்ட விமர்சனம்
செய்யலாமா? </p><p>
</p><p>
பதில்: சோனியாகாந்திக்கு ஆதரவு கொடுப்பதா? வேண்டாமா என்பது மக்கள் கையில் உள்ளது.
அதே நேரத்தில் சோனியாகாந்தி என்று ராஜீவ்காந்தியை திருமணம் செய்து கொண்டாரோ அன்றே
நமது நாட்டு பெண்ணாகிவிட்டார். நான் உங்கள் வீட்டுப் பெண் என்னை தேர்தலில் நிற்க
முடியாது என்று சொல்வீர்களா? (நான் தேர்தல் நிற்கப் போவதில்லை) (சிரிப்பு) </p><p>
</p><p>
இதுகுறித்து இப்ப பெரிய சர்ச்சை தேவையில்லை. மக்கள் முடிவு தானே முக்கியம். ஒரு சமயம் ஒரு
கட்சிக்கு ஓட்டுப் போடுகிறார்கள். மீண்டும் அடுத்த மறுபக்கம் ஓட்டு போடுகிறார்கள்.
அவர்கள் தானே முடிவு எடுக்கனும். </p><p>
</p><p>
காதல் </p><p>
</p><p>
கேள்வி: (சரத்திடம்) கல்லூரி மாணவ மாணவிகள் காதல் பற்றி தங்களது கருத்து? </p><p>
</p><p>
பதில்: காதல் என்பது கணவன் மனைவியிடம் உள்ளது. தாய், தந்தையரிடம் உள்ளது. காதல் தான்
அது எப்படி வரும், எப்போது வரும் என்று தெரியாது. உங்களுக்கு காதல் இருந்தால் பெற்றோரிடம்
நேரில் சொல்லிவிடுங்கள். தகப்பனார் ஒரு முடிவை எடுத்திருப்பார். முடிவு எடுத்தபின்
சொல்வதால் தான் பிரச்சினை வரும். தாய், தந்தையிடம் மனதில் உள்ளதை ஓப்பனாக
சொல்லுங்கள். </p><p>
</p><p>
போட்டி-பொறாமை </p><p>
</p><p>
கேள்வி:- திரைப்படத்துறையில் போட்டி, பொறாமை உள்ளதா? </p><p>
</p><p>
பதில்:- சினிமா துறையில் ரஜினிகாந்திடம் பேசிக்கொண்டிருந்த போது கூறினார். ஒரு தூணில்
எண்ணை தடவி அதில் ஒருவன் மேலே ஏறும் போது வழுக்கி வழுக்கி கீழே வந்து பிறகு முயற்சி செய்து
மேலே செல்வான். கீழே உள்ளவன் ஏற முயற்சி செய்யும் போது மேலே தன்னை வந்து தள்ளி
விடுவானோ என்று நினைப்பான் என்றார். அது போல் திரைப்படத்துறையில் போட்டி, பொறாமை
இருந்தால் தான் முன்னேற முடியும். நான் 90 படங்களில் நடித்துவிட்டேன். எம்.ஜி.ஆர். போல்
நடிக்க வேண்டும் என நினைத்தவன். நடிகர் விக்ரம் பல போட்டிகளுக்கு இடையே வந்துள்ளார்.
போட்டியிருக்கத்தான் வேண்டும். </p><p>
</p><p>
கேள்வி:- சினிமாபார்த்து சிகரெட் பிடிக்கிறார்கள் கெட்டு விட்டார்கள் என்று கமல்
கூறியிருக்கிறாரே? </p><p>
</p><p>
பதில்:- நான் ஒரு மாதம் ஊரில் இல்லை. அப்போது சொல்லியிருப்பாரோ? (சிரிப்பு)
சினிம பார்த்து கெட்டு விட்டார்கள் என்று கூறுவதை நம்ப முடியாது. 10பேர் சேர்ந்து சிகரெட்
குடிக்க சொன்னாலும் அந்த புத்தி அவனுக்கு ஏன் வர வேண்டும், யாரையும், யாரும் கெடுக்க
முடியாது. சினிமாப் பார்த்து கெட்டு விட்டார்கள் என்ற சொல்லை தவிர்க்க வேண்டும். </p><p>
</p><p>
கேள்வி: (ராதிகாவிடம்) சரத் உங்கள் கணவர் தி.மு.க.வில் உள்ளார். உங்கள் சகோதரர்.
அ.தி.மு.க.வில் உள்ளார். உறவு ரீதியாக யாரை ஆதரிப்பீர்கள். </p><p>
</p><p>
பதில்: நான் எனது கணவரை தான் ஆதரிப்பேன். தி.மு.க. உணர்வு ரீதியாகவும், அரசியல்
ரீதியாகவும் ஈடுபாடு இருக்கிறது. தலைவர் கலைஞர் மீது மிகப்பெரிய பற்றுதலைக்
கொண்டவர். ஒரு தந்தையாக தமிழனாக இருப்பவர். எனது கணவர் தி.மு.க.வில் இருப்பதில்
பெருமைப்படுகிறேன். </p><p>
</p><p>
கேள்வி- (ராதிகாவிடம்) 25 ஆண்டுகளாக திரைப்படத் துறையில் நடித்துள்ளீர்கள். சித்தி
தொடரில் உங்கள் பெயர் பிரபலமாகி விட்டதே? </p><p>
</p><p>
பதில்: 25 ஆண்டுகளாக எத்தனையோ படம் நடித்துள்ளேன். பல படங்கள் நல்ல வரவேற்பை பெற்றது.
சித்தி தொடரில் இரவு 9.30-10 மணி வரை என்னை திணமும் டி.வி. யில் பார்க்கிறீர்கள்.
அதனால் என் மீது அதிகமான பற்று ஏற்பட்டிருக்கும். சி.பா.ஆதித்தனார் அவர்கள் உழைப்பால்
உயர்ந்தவர். அவரது வழியில் உழைத்திட முயற்சிக்க வேண்டும். </p><p>
</p><p>
கேள்வி (சரத்திடம்): ஒன்றே குலம் ஒருவனே தேவன் என்று சொன்ன நாம் ஏன் காவேரி
பிரச்சினையில் இப்படி இருக்கிறோம். </p><p>
</p><p>
பதில்: தமிழ் உணர்வு வேண்டும். தமிழன் ஆதிக்கம் செலுத்த வேண்டும். தாய்த்திரு நாடு
தனித்திருநாடு என்ற உணர்வு நம்மிடம் இல்லை. தமிழர் யார் என்று தெரியப்படுத்த வேண்டும்.
திரை உலகில் நான் தமிழன் என்று சிலபேர் சொல்கிறார்கள். கன்னட நாட்டில் கன்னடம்
படிக்க வேண்டும். தமிழகத்தில் சொல்லை திணிக்கப்படுவதாக ஏன் நினைக்கிறீர்கள். தலைவர்
கலைஞர் உத்தரவின் பேரில் கொதித்தெழுவோம். </p><p>
</p><p>
இவ்வாறு மாணவ-மாணவியர் கேள்விகளுக்கு சரத்குமார் எம்.பி., ராதிகா சரத்குமார்
பதிலளித்தனர். </p><p>
</p><p>
அழகப்பா பல்கலைக்கழக மாணவ-மாணவியர் சார்பில் அருணாசலம், தனபாலன் ஆகிய நன்றியினை
தெரிவித்து கொண்டனர். </p>

<p> </p>

<p> </p>

<p>சிவாஜிக்கு மணி மண்டபம்
பிலிம்சேம்பர் அறிக்கை</p>

<p> </p>

<p>சென்னை, செப். 29- தென்னிந்திய திரைப்பட வர்த்தக சபை தலைவர்
கே.சி.என். சந்திரசேகர் நேற்று ஒரு அறிக்கை வௌியிட்டார். அதில் அவர் கூறி
இருப்பதாவது:- </p><p>
</p><p>
சிவாஜிக்கு மணிமண்டபம் அமைக்க தமிழக முதலமைச்சர் ஜெயலலிதா அடையாறு பகுதியில் ஆந்திர
மகிள சபா அருகில் அரசுக்கு சொந்தமான 12 கிரவுண்ட் இடம் ஒதுக்கி அறிவித்திருக்கிறார்.
தென்னிந்திய நடிகர் சங்கம் சிவாஜிக்கு மணி மண்டபம் கட்டுவதற்கு வழங்கியதற்காக
முதல்வருக்கு தென்னிந்திய திரைப்பட வர்த்தக சபை சார்பிலும் எனது சார்பிலும் இதயப்பூர்வ
நன்றியை தெரிவித்துக்கொள்கிறேன். </p><p>
</p><p>
தென்னிந்திய திரைப்பட வர்த்தக சபை அரசுக்கு முழு ஒத்துழைப்பும் ஆதரவும் தொடர்ந்து
வழங்கும் என்பதையும் தெரிவித்துக்கொள்கிறேன். </p><p>
</p><p>
இவ்வாறு அவர் அதில் கூறி உள்ளார். </p>

<p> </p>






</body></text></cesDoc>