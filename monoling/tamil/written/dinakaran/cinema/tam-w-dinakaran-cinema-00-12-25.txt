<cesDoc id="tam-w-dinakaran-cinema-00-12-25" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-00-12-25.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-12-25</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-12-25</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>லண்டன்
அருங்காட்சியகத்தில் இடம் பிடித்த, அமிதாப்பச்சனுக்கு சென்னையில் பாராட்டு, டைரக்டர்
ராமநாதன்-பிரகாஷ்ராஜ் பூச்செண்டு அளித்தனர்</p>

<p> </p>

<p>சென்னை, டிச. 25- பிரபல இந்தி திரையுலக சூப்பர் ஸ்டார்
அமிதாப்பச்சனுக்கு லண்டனில் உள்ள அருங்காட்சியகத்தில் மெழுகு பொம்மை அமைக்கப்பட்டது
அல்லவா? இந்தியாவிலிருந்து ஒரு நடிகருக்கு முதன்முறையாக அங்கு சிலை வைக்கப் பட்டதால்
அந்த விழா அனைவரின் கவனத்தையும் ஈர்த்தது. அமிதாப்பச்சனே நேரில் சென்று மெழுகு
சிலையை திறந்துவைத்தார். </p><p>
</p><p>
சென்னையில் அமிதாப் </p><p>
</p><p>
எஸ்.ராமநாதன் இயக்கும் ஜமானக் என்ற இந்தி படத்தில் தற்போது அமிதாப்பச்சன் இரட்டை
வேடத்தில் நடிக்கிறார். அதற்கான படப்பிடிப்பு சென்னை வடபழனியில் உள்ள விஜயா வாகினி
ஸ்டூடி யோவில் நேற்று நடந்தது. அதில் அமிதாப்பச்சன் கலந்து கொண்டு நடித்தார். அவருடன்
நடிகர் பிரகாஷ்ராஜூம் முக்கிய வேடத்தில் இணைந்து நடித்தார். விஜயசாந்தி கதா நாயகியாக
நடிக்கிறார். </p><p>
</p><p>
படப்பிடிப்புக்கு இடையே அமிதாப்பச்சனுக்கு டைரக்டர் ராமநாதன், பிரகாஷ்ராஜ் இரு வரும்
பூச்செண்டு கொடுத்து பாராட்டு தெரிவித்தார்கள். </p><p>
</p><p>
ஆர்ட் டைரக்டர்சலம், ஒளிப்பதிவாளர் முரளி, நெல்லை சுந்தர்ராஜன் ஆகியோரும் கைகுலுக்கி
வாழ்த்து கூறினார்கள். </p><p>
</p><p>
பெருமை </p><p>
</p><p>
பின்னர் டைரக்டர் எஸ். ராமநாதன் பாராட்டி பேசினார், லண்டன் மியூசியத்தில் நடந்த
விழாவில் கலந்து கொண்டு அமிதாப்பச்சன் தனது மகிழ்ச்சியை வௌிப் படுத்தியபோது மெழுகு
பொம்மை அமைத்து தனக்கு வழங்கப்பட்ட கவுரவம் இந்திய திரையுலகுக்கு கிடைத்த கவுரவமாக
கருதுகிறேன் என்று கூறினார். அவரது அந்த பேச்சின் பெருந்தன் மைக்கு விழா எடுத்துக்கொண்
டாடலாம் என்று குறிப்பிட்டார். பாராட்டை ஏற்றுக் கொண்ட அமிதாப்பச்சன் அனைவருக்கும் தனது
நன்றியை தெரிவித்தார். </p>

<p> </p>






</body></text></cesDoc>