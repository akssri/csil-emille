<cesDoc id="tam-w-dinakaran-cinema-02-10-11" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-02-10-11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-10-11</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-10-11</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>விஜயகாந்த் கோரிக்கை
ஏற்றார்: ரஜினி உண்ணாவிரதம் 13-ந்தேதிக்கு மாற்றம்</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>ரஜினிகாந்த் </p>

<p> </p>

<p>சென்னை, அக். 11- விஜயகாந்த் கோரிக்கையை ஏற்று ரஜினிகாந்த்
தனது உண்ணாவிரத போராட்டத்தை 13-ந் தேதிக்கு தள்ளி வைத்தார். </p><p>
</p><p>
உண்ணாவிரதம் </p><p>
</p><p>
காவிரி நீர் தராத கர்நாடக அரசுக்கு மின்சாரம் தரக்கூடாது என்று வலியுறுத்தி 12-ந் தேதி
தமிழ் திரையுலகினர் திரண்டு பேரணி, ஆர்ப்பாட்டம் நடத்துகிறார்கள் அல்லவா?
இந்தநிலையில் ரஜினிகாந்த் காவிரி நீரை தமிழகத்துக்கு திறந்து விட வேண்டும் என்று கோரி
சென்னையில் 12-ந் தேதி உண்ணாவிரதம் இருக்கப்போவதாக அறிவித்தார். </p><p>
</p><p>
விஜயகாந்த் பேட்டி </p><p>
</p><p>
இந்தநிலையில் நடிகர் சங்க தலைவர் விஜயகாந்த் நேற்று இரவு நிருபர்களுக்கு சென்னையில்
பேட்டி அளித்தார். அப்போது ரஜினிகாந்த் 12-ந் தேதி அறிவித்திருக்கும் உண்ணாவிரதத்தை
13-ந் தேதி தள்ளி வைத்துக் கொண்டால் அன்றையதினம் அவரது போராட்டத்தில் நடிகர்
சங்கமும் கலந்துகொள்ளும் என்று கோரிக்கை விடுத்திருந்தார். இந்த கோரிக்கையை ஏற்று
ரஜினிகாந்த் தனது உண்ணா விரதத்தை 13-ந்தேதிக்கு தள்ளி வைத்துக் கொண்டார். </p><p>
</p><p>
இதுகுறித்து விஜயகாந்திடம் தொடர்பு கொண்டு கேட்டபோது, உண்ணாவிரதத்தை 12-ந் தேதிக்கு
பதிலாக 13-ந்தேதி தள்ளி வைத்துக்கொள்ள வேண்டும் என்று ரஜினியுடன் தொலைபேசியில்
தொடர்பு கொண்டு பேசினேன். அதனை அவர் ஏற்றுக்கொண்டார். 13-ந்தேதி நடிகர் சங்கமும்
அவரது போராட்டத்தில் பங்கேற்கும் என்று கூறினார். </p><p>
</p><p>
கமல்ஹாசன் </p><p>
</p><p>
நிருபர்:- கமல்ஹாசன் நெய்வேலி பேரணியில் கலந்துகொள்கிறாரா? </p><p>
</p><p>
விஜயகாந்த்:- கலந்துகொள்வதாக அவர் ஏற்கனவே அறிவித்திருக்கிறார். </p>

<p> </p>

<p> </p>

<p>சினிமா
தயாரிப்பாளர்-இயக்குனர்கள் உள்பட 6110 கலைஞர்கள் பங்கேற்பு: 175 பஸ்களில்
நடிகர்-நடிகை பயணம் பலத்த பாதுகாப்புடன் நெய்வேலி போகிறார்கள் போராட்ட ஏற்பாடு
குறித்து விஜயகாந்த் பேட்டி</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>காவிர் நீர் கேட்டு
நெய்வேலியில் நடக்கும் பேரணியில் பங்கேற்பது மற்றும் யார் யார் எப்படி பேரணியில்
வரவேண்டும் என்பது பற்றி நேற்று இரவு நடந்த ஆலோசனை கூட்டத்தில் நடிகர் சங்க தலைவர்
விஜயகாந்த் பேசியபோது எடுத்த படம். அருகில் டைரக்டர் பாரதிராஜா, தயாரிப்பாளர்கள்
சங்க செயலாளர் சித்ரா லட்சுமணன், நடிகர் நாசர் உள்ளனர். </p>

<p> </p>

<p>சென்னை, அக். 11- காவிரி பிரச்சினைக்காக தமிழ் திரையுலகினர்
நடத்தும் போராட்டத்துக்கு தீவிர ஏற்பாடுகள் நடந்து வருகின்றன. 175 பஸ்களில் நடிகர்-நடிகைகள்
நாளை நெய்வேலி போகிறார்கள். அவர்களுக்கு பலத்த போலீஸ் பாதுகாப்பு தரப்படுகிறது.
ஏறத்தாழ 6 ஆயிரம் சினிமா கலைஞர்கள் இதில் கலந்து கொள்கிறார்கள். இந்த தகவலை
நடிகர் சங்க தலைவர் விஜயகாந்த் நேற்று கூறினார். </p><p>
</p><p>
சுப்ரீம் கோர்ட்டு உத்தரவு, மத்திய கண்காணிப்பு குழு உத்தரவிட்ட பிறகும் தமிழகத் துக்கு
கர்நாடக அரசு காவிரி நீர் திறந்துவிடாமல் சண்டித்தனம் செய்து வருகிறது அல்லவா?
கர்நாடக அரசின் இந்த போக்கை கண்டித்தும், உடனடியாக காவிரி நீரை திறந்துவிடக்கோரியும்
தமிழ் திரையுலகே திரண்டு நாளை நெய்வேலியில் பேரணி நடத்து கிறது அல்லவா? அதற்கான ஏற்பாடுகள்
வேகமாக நடை பெற்று வருகின்றன. </p><p>
</p><p>
விஜயகாந்த் பேட்டி </p><p>
</p><p>
நடிகர் சங்க தலைவர் விஜய காந்த் இதுகுறித்து நேற்று சென்னையில் உள்ள நடிகர் சங்கத்தில்
நிருபர்களுக்கு பேட்டி அளித்தார். அதன் விவரம் வருமாறு:- </p><p>
</p><p>
நடிகர் சங்கத்தில் இருந்து தான் நடிகர், நடிகைகள் நெய் வேலி பேரணிக்கு புறப்படு கிறார்கள்.
அதிகாலை 5 மணியிலிருந்து புறப்பட்டு மீனம் பாக்கம் ஸ்டேடியம் அடை கிறார்கள். அங்கு
அனைவரும் காலை டிபன் சாப்பிட்டுவிட்டு பஸ்களில் புறப்படுகிறார்கள். கடலூரில் மதிய
சாப்பாடு வழங்கப்படுகிறது. அதன்பிறகு நேராக நெய்வேலி சென்றடைகிறோம். </p><p>
</p><p>
175 பஸ்கள் </p><p>
</p><p>
கேள்வி:- மொத்தம் எத்தனை பஸ்களில் கலைஞர்கள் செல்கிறார்கள். </p><p>
</p><p>
பதில்:- மொத்தம் 175 பஸ்களில் புறப்படுகிறார்கள். </p><p>
</p><p>
கேள்வி:- கன்னட நடிகர் ராஜ்குமார் 12-ந் தேதி மீண்டும் கர்நாடகாவில் போராட்டம்
நடத்தப்போவதாக அறிவித்திருக்கிறாரே.. </p><p>
</p><p>
பதில்:- அவர் இன்னொரு முறை போராட்டம் நடத்தினால் நடிகர் சங்கமும் அடுத்த கட்ட
போராட்டம் நிச்சயம் அறிவிக்கும். கர்நாடகாவில் 40 லட்சம் தமிழர்கள் இருக்கிறார்கள்.
அவர்களுக்கு பாதுகாப்பு நிச்சயம் தரப்பட வேண்டும். தமிழகத்திலும் கன்னடர்கள்
வசிக்கிறார்கள். இவர்களைப்பற்றியும் கர்நாடக மக்கள் சிந்தித்து செயல்படவேண்டும்.
இங்கு நாங்கள் நினைத்தால் தமிழகத்தை ஸ்தம்பிக்க வைக்க முடியும். நான் கன்னட மக்களை
கேட்டுக் கொள்வதெல்லாம் இதுதான். அங்குள்ள அனைவரும் அண்ணன் தம்பிகளாக வாழ வேண்டும்.
இரண்டு மாநிலங்களிலும் இதுபோன்ற பிரச்சினைகள் நீடித்துக் கொண்டிருப்பதால்தான்
போராட்டம் தொடர்ந்து கொண்டே போகிறது. இந்த பிரச்சினையை 2 முதல் அமைச்சர்களாலும்
தீர்க்க முடிய வில்லை என்று கூறுகிறார்கள். அரசியல் விளையாடுவதால் தான் இதற்கு தீர்வு காண
முடியவில்லை. </p><p>
</p><p>
கேள்வி:-ரஜினிகாந்த் உண்ணாவிரதம் அறிவித்திருக்கிறாரே? </p><p>
</p><p>
பதில்:- அது அவரது இஷ்டம். </p><p>
</p><p>
மடத்தனம் </p><p>
</p><p>
கேள்வி:- நெய்வேலி பேரணியில் அனைத்து நடிகர்களும் பங்கேற்கிறார்களா? </p><p>
</p><p>
பதில்:- காவிரி போராட்டம் என்பது பொது நோக்கு பிரச்சினை. இந்த பிரச்சினைப்பற்றி
நான் ஊரில் இல்லாதபோது தமிழக முதல்வரை போய் நடிகர், நடிகைகள், கலைக்குழுவினர் பார்த்து
பேசிவிட்டு வந்தார்கள். அதிலிருந்து பின்வாங்குவது மடத்தனம் என்று நினைக்கிறேன். முதல்வர்
பேரணிக்கு அனுமதி வழங்கி இருக்கிறார். அதற்கு நான் தலைவணங்குவேன். </p><p>
</p><p>
கேள்வி:- முதல்வரை சந்தித்து பேசினீர்களா? </p><p>
</p><p>
பதில்:- நேரில் சந்தித்து பேச நேரம் கேட்டிருந்தேன். கிடைக்கவில்லை. தற்போது பேரணி
ஏற்பாடுகள் நிறைவடைந்துவிட்டது. இனி முதல்வரை பார்க்க தேவையில்லை என்று நினைக்கிறேன். </p><p>
</p><p>
கேள்வி:- சினிமா கலைஞர்கள் மொத்தம் எவ்வளவு பேர் பேரணியில் பங்கேற்
கிறார்கள்? </p><p>
</p><p>
பதில்:- 6110 சினிமா கலைஞர்கள் இந்த பேரணியில் பங்கேற்கிறார்கள். </p><p>
</p><p>
கேள்வி:- தி.மு.க.தலைவர் கருணாநிதியை சந்தித்தீர்களா? </p><p>
</p><p>
பதில்:- கலைஞரை நான் இன்று (நேற்று)பார்த்தேன். தி.மு.க. நடிகர்களையும் பேர ணிக்கு
அழைக்க வேண்டும் என்று கேட்டேன். அவர் டைரக்டர் பாரதிராஜா தடையை மீறி செல்வோம் என்று
கூறிய கருத்தை மறுபடியும் சொல்கிறார். பாரதி ராஜா தடை மீறி போராடப்போவதில்லை என்று
நேற்றே அறிவித்துவிட்டார். அது பத்திரிகை, டி.வி.யில் வந்ததுபோல் தெரியவில்லை. </p><p>
</p><p>
கேள்வி:- நடிகர் சங்க குழுவினர் கர்நாடக முதல்வர் எஸ்.எம்.கிருஷ்ணா, நடிகர் ராஜ்குமார்
ஆகியோரை நேரில் சந்தித்து பேசும் திட்டம் உள்ளதா? </p><p>
</p><p>
பதில்:-நல்ல கேள்விதான். கர்நாடக முதல்வர் எஸ்.எம். கிருஷ்ணா, நடிகர் ராஜ் குமாரை
பார்க்க தயாராக இருக்கிறோம். ஆனால் அதற்கு அவர்கள் பாதுகாப்பு ஏற்பாடு செய்ய வேண்டும்.
</p><p>
</p><p>
கேள்வி:- மொத்தம் எத்தனை அமைப்புகள் கலந்து கொள்கின்றன? </p><p>
</p><p>
பதில்:-பெப்சியின் 24 அமைப்புகள், நடிகர் சங்கம், சின்னத்திரை சங்கத்தினர்,
தயாரிப்பாளர்கள் சங்கம், இயக்குனர்கள் சங்கம், விநியோகஸ்தர்கள் சங்கம், தியேட்டர்
அதிபர்கள் சங்கம் என 31 அமைப்புகள் கலந்துகொள்கின்றன. நான் மீண்டும் நடிகர் சங்க
தலைவர் என்ற முறையில் வேண்டுகோள்விடுகிறேன். நடிகர் சங்கத்தின் அனைத்து உறுப்பினர்களும்
இந்த பேரணியில் பங்கேற்க வேண்டும். </p><p>
</p><p>
ரசிகர்கள் உண்ணாவிரதம் </p><p>
</p><p>
கேள்வி:- உங்களது ரசிகர் மன்றத்தினர் பேரணியில் பங்கேற்கிறார்களா? </p><p>
</p><p>
பதில்:- கடலூர், விழுப்புரம், திருவண்ணாமலை, பாண்டிச்சேரி ஆகிய இடங்களில் உள்ள எனது
மன்ற ரசிகர்களை மட்டும் நான் அழைத்திருக்கிறேன். மற்ற வர்கள் எல்லாம் அந்தந்த ஊரில்
12-ந்தேதி அல்லது 13-ந் தேதி உண்ணாவிரதம் இருக்கக் கேட்டுக்கொள்கிறேன். </p><p>
</p><p>
கேள்வி:-உங்களது அடுத்த கட்ட போராட்டம் என்னவாக இருக்கும்? </p><p>
</p><p>
பதில்:- பிரதமர் வாஜ்பாய், துணை பிரதமர் எல்.கே.அத்வானி, காங்கிரஸ் தலைவர்
சோனியாகாந்தி ஆகியோரை சந்தித்து மனு கொடுப்போம். முறைப்படி அவர்களிடம் அதற்கான தேதி
பெற்ற பிறகு சந்திப்போம். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
</p><p>
</p><p>
13-ந்தேதி ரஜினி உண்ணாவிரதம் இருந்தால் பங்கேற்க தயார் </p><p>
</p><p>
ரஜினிகாந்த் 13-ந் தேதி உண்ணாவிரதம் இருந்தால் நடிகர் சங்கம் கலந்து கொள்ளும் என்று
விஜயகாந்த் கூறினார். </p><p>
</p><p>
உண்ணாவிரதம் </p><p>
</p><p>
நடிகர் சங்க தலைவர் விஜயகாந்த் நேற்று பேட்டி அளித்தார். அது வருமாறு:- </p><p>
</p><p>
கேள்வி:- ரஜினிகாந்த் உண்ணாவிரதம் இருக்கிறாரே அதில் நடிகர் சங்கத்தினர்
பங்கேற்பார்களா? </p><p>
</p><p>
பதில்:-ரஜினிகாந்த் 13-ந் தேதி உண்ணாவிரதம் இருந்தால் நடிகர் சங்கம் அவரது
போராட்டத்தில் கலந்து கொள்ளும். </p><p>
</p><p>
கேள்வி:- கமல்ஹாசன் நெய்வேலிக்கு வருகிறாரா? </p><p>
</p><p>
பதில்:- வருவதாக ஏற்கனவே அவர் கூறிவிட்டார். </p>

<p> </p>

<p> </p>

<p>கேட்ட இடம்
கொடுக்கவில்லை: சேப்பாக்கம் விருந்தினர் விடுதியில் ரஜினி உண்ணாவிரதம் காலை 8 மணி
முதல் மாலை 5 வரை நடக்கிறது பலத்த பாதுகாப்பு: கமிஷனர் பேட்டி</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>விஜயகுமார் </p>

<p> </p>

<p>சென்னை, அக். 11- சேப்பாக்கம் விருந்தினர் விடுதியில் காலை 8
மணி முதல் மாலை 5 மணி வரை ரஜினி உண்ணாவிரதம் இருக்க போலீஸ் அனுமதி அளித்துள்ளது. ஆனால்
அவர் கேட்ட இடம் எதுவும் கொடுக்கப்பட வில்லை. </p><p>
</p><p>
ரஜினி உண்ணாவிரதம் </p><p>
</p><p>
காவிரியில் தண்ணீர் திறந்து விடாத கர்நாடக அரசை கண்டித்து சூப்பர் ஸ்டார் ரஜினி
12-ந்தேதி உண்ணாவிரதம் இருப்பேன் என்று அறிவித்தார். இதனால் போலீஸ் கமிஷனர்
விஜயகுமாரை சந்தித்து உண்ணாவிரதத்துக்கு அனுமதி கேட்க நேற்று காலை 11.30 மணிக்கு அகில
இந்திய ரசிகர் மன்ற தலைவர் சத்திய நாராயணன் வந்தார். அப்போது கமிஷனர் அங்கு இல்லை.
இதனால் அவரை உளவு பிரிவு துணைக்கமிஷனர் சடகோப ராமானுஜம் அறையில் இருக்க வைத்தனர். </p><p>
</p><p>
கமிஷனரிடம் மனு </p><p>
</p><p>
அதன்பிறகு கமிஷனர் மதியம் 12.15 மணிக்கு வந்தார். உடனே சத்திய நாராயணனை துணைக்கமிஷனர்
ராமானுஜம் அழைத்து கொண்டு கமிஷனர் அறைக்குள் சென்றார். அங்கு சென்றதும், கமிஷனரிடம்
போலீஸ் அனுமதி கேட்டு ரஜினி கொடுத்து அனுப்பிய மனுவை சத்திய நாராயணன் கொடுத்தார். அந்த
மனுவை வாங்கி படித்த கமிஷனர் 20 நிமிட நேரம் சத்திய நாராயணனிடம் பேசினார். அப்போது
அவர் உண்ணாவிரதம் இருக்கும் இடங்கள் பற்றி விரிவாக விளக்கி கூறினார். </p><p>
</p><p>
சத்திய நாராயணன் பேட்டி </p><p>
</p><p>
அதன்பிறகு 12.40 மணிக்கு வௌியே வந்த சத்திய நாராயணனிடம் நிருபர்கள் கேட்ட
கேள்வியும், அதற்கு அவர் அளித்த பதிலும் வருமாறு:- </p><p>
</p><p>
கேள்வி- ரஜினி எந்த இடத்தில் உண்ணாவிரதம் இருக்க அனுமதி கேட்டுள்ளீர்கள்? </p><p>
</p><p>
3 இடங்கள் </p><p>
</p><p>
பதில்:- அவர் உண்ணா விரதம் இருக்க கடற்கரையில் உள்ள காந்தி சிலை, சீரணி அரங்கம்,
வள்ளுவர் கோட்டம் ஆகிய 3 இடங்களில் ஏதாவது ஒரு இடத்தில் உண்ணாவிரதம் இருக்க அனுமதி
வேண்டும் என்று கேட்டிருக்கிறோம். </p><p>
</p><p>
கேள்வி:- அதற்கு கமிஷனர் என்ன சொன்னார்? </p><p>
</p><p>
பதில்:- கண்டிப்பாக ஏதாவது ஒரு இடத்தில் உண்ணா விரதம் இருக்க அனுமதி தருவதாக கூறினார்.
அவர் அனுமதி தருவார் என்ற நம்பிக்கை உள்ளது. </p><p>
</p><p>
கேள்வி:- எப்போது அனுமதி தருவதாக கூறியுள்ளார்? </p><p>
</p><p>
பதில்:-இன்னும் அரை மணி நேரத்தில் முடிவு செய்வதாக சொல்லி இருக்கிறார். </p><p>
</p><p>
தடையை மீறுவீர்களா? </p><p>
</p><p>
கேள்வி:- போலீஸ் அனுமதி கொடுக்காவிட்டால் தடையை மீறி உண்ணாவிரதம் இருப்பீர்களா? </p><p>
</p><p>
பதில்:-அதற்கு அவசியம் ஏற்படாது. </p><p>
</p><p>
அரசியலுக்கு வருவாரா? </p><p>
</p><p>
கேள்வி:- ரஜினி அரசியலுக்கு வருவதற்கு இது ஒரு முன்னேற்பாடாக எடுத்து கொள்ளலாமா? </p><p>
</p><p>
பதில்:- இந்த கேள்வி இப்போது தேவை இல்லை. அந்த கேள்வியே எழவில்லை. </p><p>
</p><p>
கேள்வி:- உங்களிடம் கமிஷனர் எப்படி நடந்து கொண்டார்? </p><p>
</p><p>
பதில்:- நல்ல முறையில் பேசினார். நல்ல ஒத்துழைப்பு கொடுப்பதாகவும் கூறினார். </p><p>
</p><p>
நடிகர்-நடிகை வருகையா? </p><p>
</p><p>
கேள்வி:-ரஜினி உண்ணா விரதத்தில் பங்கேற்க எந்தெந்த நடிகர், நடிகை வருகிறார்கள்? </p><p>
</p><p>
பதில்:- அதை இப்போது சொல்ல முடியாது. </p><p>
</p><p>
கேள்வி:- ரசிகர்கள் எவ்வளவு பேர் வருகிறார்கள்? </p><p>
</p><p>
பதில்:- இதையெல்லாம் இப்போது சொல்ல முடியாது என்று சிரித்து கொண்டே கூறினார். </p><p>
</p><p>
எங்களுக்கு போலீஸ் அனுமதி கிடைத்ததும் ரஜினியுடன் கலந்து பேசி முடிவு செய்வோம். </p><p>
</p><p>
இவ்வாறு கூறினார். </p><p>
</p><p>
ரசிகர்கள் எல்லாம் புறப்பட்டு வந்து கொண்டு இருக்கிறார்களாமே? என்று சத்திய
நாராயணனிடம் கேட்ட போது அவர் அதற்கு பதில் எதுவும் சொல்லாமல் சிரித்துக் கொண்டே
சென்றுவிட்டார். </p><p>
</p><p>
போலீஸ் அனுமதி </p><p>
</p><p>
ரஜினி மனுவை பரிசீலனை செய்த கமிஷனர் விஜயகுமார் நேற்று மாலையில் சேப்பாக்கம்
விருந்தினர் விடுதி முன்பு காலை 8 மணி முதல் மாலை 5 மணி வரை உண்ணாவிரதம் இருக்க போலீஸ்
அனுமதி வழங்கினார். ஆனால் ரஜினி கேட்ட காந்தி சிலை, சீரணி அரங்கம், வள்ளுவர்
கோட்டம் ஆகிய இடங்கள் எதுவும் ஒதுக் கப்படவில்லை என்பது குறிப்பிடத்தக்கது. </p><p>
</p><p>
கமிஷனர் ஆபீசில் ரஜினி பரபரப்பு </p><p>
</p><p>
நேற்று காலையில் கமிஷனரிடம் மனு கொடுக்க ரஜினி வந்துள்ளார் என்ற செய்தி பரவியது.
இதனால் கமிஷனர் அலுவலக ஊழியர்கள் தங்களது வேலையை அப்படியே போட்டு விட்டு வந்தனர். கமிஷனர்
அலுவல கத்துக்கு வௌியே நடந்து சென்றவர்களும் உள்ளே வந்து ரஜினி எங்கே? ரஜினி எங்கே
என்று கேட்டனர். அதற்கு அங்கிருந்த போலீசார், ரஜினி வரவில்லை. சத்திய நாராயணன் தான்
வந்துள்ளார் என்று கூறினர். இந்த சம்பவம் கமிஷனர் ஆபீசில் பெரிய பரபரப்பை
ஏற்படுத்தியது. </p><p>
</p><p>
ரஜினி உண்ணாவிரதத்துக்கு பலத்த போலீஸ் பாதுகாப்பு ஏற்பாடுகள் செய்யப்பட்டுள்ளன என்று
கமிஷனர் விஜயகுமார் கூறினார். </p><p>
</p><p>
விஜயகுமார் பேட்டி </p><p>
</p><p>
அகில இந்திய ரஜினி ரசிகர் மன்ற தலைவர் சத்திய நாராயணன் நேற்று காலையில் கமிஷனர்
விஜயகுமாரை சந்தித்து ரஜினி உண்ணாவிரதம் இருக்க போலீஸ் அனுமதி கேட்டு மனு கொடுத்தார்.
இந்த மனுவை இணைக்கமிஷனர்கள் திரிபாதி, சைலேந்திரபாபு மற்றும் துணைக்கமிஷனர் களுடன்
கமிஷனர் பரிசீலனை செய்தார். அதன்பிறகு மாலையில் சேப்பாக்கம் விருந்தினர் விடுதி முன்பு
உண்ணாவிரதம் இருக்க அனுமதி கொடுத்தார். </p><p>
</p><p>
அவர் நிருபர்களிடம் கூறியதாவது:- </p><p>
</p><p>
ரஜினி உண்ணாவிரதம் இருக்க நான்கு அல்லது 5 இடங்களில் அனுமதி கேட்டு மனு கொடுத்தனர். இந்த
மனுவை பரிசீலனை செய்து சேப்பாக்கம் விருந்தினர் விடுதி முன்பு காலை 8 மணி முதல் மாலை 5
மணி வரை உண்ணாவிரதம் இருக்க அனுமதி கொடுக்கப்பட்டுள்ளது. </p><p>
</p><p>
பலத்த பாதுகாப்பு </p><p>
</p><p>
உண்ணாவிரதத்தையொட்டி பலத்த போலீஸ் பாதுகாப்பு ஏற்பாடுகள் செய்யப்பட்டுள்ளன. பெண்
போலீசாரும் பாதுகாப்பு பணியில் ஈடுபடுவார்கள். உண்ணாவிரதத்தில் எந்த அசம் பாவித
சம்பவமும் நடக்காத அளவுக்கு போலீசார் குவிக்கப் படுவார்கள். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
ஆட்டோவில் வந்த சத்திய நாராயணன் </p><p>
</p><p>
போலீஸ் கமிஷனரிடம் மனு கொடுக்க சத்தியநாராயணன் நேற்று காலை எழும்பூரில் உள்ள கமிஷனர்
அலுவலகத்துக்கு வாடகை ஆட்டோவில் வந்தார். பிறகு மனு கொடுத்து விட்டு மீண்டும் ஆட்டோவில்
ஏறி சென்றார். அவரை நிருபர்கள், புகைப் படக்காரர்கள், டி.வி. காமிராக் காரர்கள்
பின்தொடர்ந்து விரட்டி சென்று படம் எடுத்தனர். </p><p>
</p><p>
வள்ளுவர் கோட்டம் கிடைக்க விடாமல் அ.தி.மு.க. தடுத்தது </p><p>
</p><p>
ரஜினி நேற்று முன்தினம் பேட்டி அளிக்கும் போது, பிலிம் சேம்பரிலோ, நடிகர் சங்கத்திலோ,
வள்ளுவர் கோட்டத்திலோ உண்ணாவிரதம் இருப்பேன் என்றார். </p><p>
</p><p>
இந்தநிலையில் அ.தி.மு.க. பத்திரிகையாளர்கள் சங்கம் காவிரி பிரச்சினைக்காக வள்ளுவர்
கோட்டத்தில் நாளை உண்ணாவிரதம் இருக்க அனுமதி கேட்டு கடிதம் கொடுத்து இருப்பதாக அரசு
வட்டாரம் கூறுகிறது. சங்கத் தலைவர் சத்யாலயா ராம கிருஷ்ணன் இதற்காக கடிதம் கொடுத்து
இருக்கிறார். </p><p>
</p><p>
ரஜினி வள்ளுவர் கோட்டத்தில் உண்ணாவிரதம் இருக்க அனுமதி கொடுத்துவிடக் கூடாது
என்பதற்காகவே அ.தி.மு.க. சார்பில் வள்ளுவர் கோட்டத்தில் உண்ணாவிரதம் இருக்க அனுமதி
கேட்டு போலீஸ் கமிஷனரிடம் கடிதம் கொடுத்ததாக கூறப்படுகிறது. இதனாலேயே வள்ளுவர்
கோட்டத்தில் உண்ணா விரதம் இருக்க ரஜினிக்கு அனுமதி தரப்படவில்லை என்றும் கூறப்படுகிறது. </p>

<p> </p>

<p> </p>

<p>ரஜினி உண்ணாவிரதத்திற்கு
இயற்கைத்தாய் பதிலளித்து விட்டாள் தமிழகத்தில் பெய்த மழையை குறிப்பிட்டு கிருஷ்ணா
பதில்</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>எஸ்.எம்.கிருஷ்ணா </p>

<p> </p>

<p>பெங்களூர், அக்.11- தமிழகத்தில் நல்ல மழை பெய்து உள்ளது. ரஜினி
உண்ணா விரதத்திற்கு இயற்கைத்தாயே பதிலளித்து விட்டாள் என்று எஸ்.எம்.கிருஷ்ணா கூறினார்.
</p><p>
</p><p>
சொந்த தொகுதியில்... </p><p>
</p><p>
கர்நாடக முதல்வர் எஸ்.எம். கிருஷ்ணா காவிரி பாசனப்பகுதியில் நடைபயணம் மேற்
கொண்டுள்ளார் அல்லவா? 4-வது நாளான நேற்று அவர் மண்டியா மாவட்டத்தில் உள்ள அவரது
சொந்த தொகுதியான மத்தூருக்கு சென்றார். அங்கு நிருபர்களிடம் கூறியதாவது:- </p><p>
</p><p>
தமிழ்நாடு, கர்நாடகாவில் குறிப்பாக காவிரி டெல்டா பகுதியில் நேற்று மழை பெய்து உள்ளது.
மழை அடுத்த 7 நாட்களுக்கு தொடரும் என வானிலை தகவல்கள் கூறு கின்றன. இது மகிழ்ச்சிக்குரிய
முன்னேற்றம். தமிழகத்தில் பெய்யும் பலத்த மழை தமிழ்நாடு- கர்நாடகா இடையே உள்ள
பதட்டத்தை குறைக்கும் என்று நம்புகிறேன். </p><p>
</p><p>
கேள்வி:- காவிரி பிரச்சினையில் தமிழ் சூப்பர்ஸ்டார் ரஜினி உண்ணாவிரதம் இருக்க முடிவு
செய்துள்ளாரே? </p><p>
</p><p>
பதில்:- தமிழர்கள் மற்றும் கன்னடர்கள் இடையே ஒரு ஒப்பந்தம் ஏற்பட வேண்டும் என்று அவர்
நினைக்கிறார். இந்த எண்ணம் கொண்ட யாருடைய முயற்சியும் வரவேற்கத்தக்கதுதான். </p><p>
</p><p>
ரஜினிக்கு இயற்கைத்தாய் பதில் </p><p>
</p><p>
கேள்வி:- தமிழகத்திற்கு கர்நாடகம் தண்ணீர் திறந்துவிட வேண்டும் என்று கேட்டு
உள்ளாரே? </p><p>
</p><p>
பதில்:- தமிழகத்தில் தஞ்சாவூர், சேலம், சென்னை மற்றும் பல பகுதிகளில் மழை
பெய்துள்ளது. அதுவே ரஜினியின் உண்ணாவிரதத்திற்கு இயற்கைத்தாய் தந்த பதிலாக இருக்கலாம். </p><p>
</p><p>
கேள்வி:- காவிரி விவகாரத்தில் மத்திய அரசு தலையிடுமா? </p><p>
</p><p>
பதில்:- மத்திய அரசால் என்ன செய்ய முடியும்? 2 மாநிலங்களிலும் போதிய தண்ணீர்
இல்லை என்று காவிரி கண்காணிப்புக்குழுவே தெரிவித்துவிட்டது. தண் ணீரை தயாரிக்கவோ
இறக்குமதி செய்யவோ முடியாது. அது அரிசியா, கோதுமையா? குடோனில் இருந்து பெறுவதற்கு?
தண்ணீர் பிரச்சினைக்கு இயற்கை ஒத்துழைத்தால் தான் திருப்தியான தீர்வு காண முடியும். </p><p>
</p><p>
இவ்வாறு கூறினார். </p>

<p> </p>

<p> </p>

<p>கலைஞர் சொல்லும்
வழியில் போராடுவேன் சரத்குமார் அறிவிப்பு</p>

<p> </p>

<p>சென்னை, அக். 10- காவிரி நீர் பிரச்சினைக்கு பல்வேறு முறை
தீர்வு கண்ட கலைஞர் சொல்லும் வழியில் போராடுவேன் என்று சரத்குமார் எம்.பி.
தெரிவித்து உள்ளார். </p><p>
</p><p>
சரத் அறிக்கை </p><p>
</p><p>
நடிகர் சங்க பொதுச் செயலாளர் சரத்குமார் எம்.பி. வௌியிட்டுள்ள அறிக்கையில் கூறி
இருப்பதாவது:- </p><p>
</p><p>
காவிரி நீர் பிரச்சினை என்பது மிகவும் உணர்ச்சிகரமான பிரச்சினை. அதில் நடிகர் சங்கம்
சிந்தித்துத்தான் முடிவு எடுக்கவேண்டும் என்று சங்கத்தில் நடந்த முதல் ஆலோசனை கூட்டத்தில்
பேசப்பட்டது. தமிழக மக்களும் தமிழக சினிமா கலைஞர்களும் ஒன்றாக இருக்க வேண்டும். ஒரு
அரசியல் கட்சி போராட்டம் அறிவிப்பது என்பது அந்த கட்சியின் முடிவுக்கு உட்பட்டது. ஆனால்
ஒரு பிரச் சினைப் பற்றி நடிகர்கள் சொல்லும் போது மக்களிடையே மிகப்பெரிய வேகத்தை
உருவாக்குவதுடன் அவர்கள் தட்டி எழுப்பப்படுகிறார்கள். நடிகர் சங்கம் வௌியிட்ட முதல்
அறிக்கையிலேயே சுப்ரீம் கோர்ட்டு உத்தரவை மதித்து கர்நாடக அரசு காவிரி நீர் திறந்து
விட வேண்டும் என்று வலியுறுத்தப்பட்டிருந்தது. </p><p>
</p><p>
காந்தி சொன்ன வழி </p><p>
</p><p>
ூூலி ்ூ் ூூலி பி நீயூநீ ்ூ் பி நீயூநீ டுஜிநீகி லிநீ சீூ்ளிணீ லுளிலீபிணீ என்று
மகாத்மா காந்திஅடிகள் கூறினார். அதாவது பழிக்கு பழி உணர்ச்சி கூடாது என்பதை அவர்
வலியுறுத்தினார். எதற்காக அப்படி கூறினார் என்றால் எந்த முடிவும் எந்த உயிருக்கும்
பாதிப்பு இல்லாத முடிவாக இருக்கவேண்டும். தமிழ், தெலுங்கு, கன்னடம், மலையாளம் என அனைத்து
மாநில மொழியின மக்களில் ஒருவரைக்கூட அது பாதிக் காமல் அமைய வேண்டும். </p><p>
</p><p>
பள்ளியில் படிக்கும்போதே இந்தியா எனது தாய்நாடு. இந்திய மக்கள் யாவரும் எனது உடன்பிறந்த
சகோதர, சகோதரிகள் என்ற உறுதி மொழி எடுத்துக்கொண்டுதான் படிக்கி றோம். உணர்ச்சி
பிரவாகத்தில் எடுக்கப்படும் எந்த போராட்ட முடிவும் வேறுவிதமாக அமைந்துவிடக்கூடாது. அது
அமைதியாக, அறவழியில் நடக்க வேண்டும். கலைக்குடும்பத்தினர் பாதுகாப்பாக சென்று பேரணி
பங்கேற்று விட்டு திரும்பி வரவேண்டும் என்று கேட்டுக்கொள்கிறேன். </p><p>
</p><p>
என்ன செய்யபோகிறோம் </p><p>
</p><p>
ஆனால் இந்த போராட்டத்துக்கு பிறகு நாம் என்ன செய்யப்போகிறோம். ஒருநாள்
போராட்டத்துடன் காவிரிப் பிரச்சினையைவிட்டு விடப்போகிறோமா? அல்லது அது வொரு
தொடர் போராட்டமாக அமைய வேண்டுமா? என்பதையும் இது தொடர்பாக கர்நாடக முதல்வர்
எஸ்.எம்.கிருஷ்ணா, ராஜ்குமார் மற்றும் கர்நாடகாவில் உள்ள அனைத்து அமைப்புகளையும் கலைக்
குழுவினர் நேரில் சந்தித்து நல்லதொரு தீர்வு காணப்போகிறோமா? காவிரிப்
பிரச்சினையில் ஒன்றிணைந்து போராட வேண்டும் என்பதுசரிதான். தமிழ் உணர்வுள்ள எவரும்
தங்களது எண்ணத்தை அமைதியாக அறவழியில் வௌிப்படுத்தலாம்். 12-ந் தேதி நடக்கும்
போராட்டத்தில்தான் அனைவரும் கலந்துகொள்ள வேண்டும்என்று யாரேனும் கூறினால் தமிழக
அரசினால் கூட்டப்பட்டு, அனைத்து கட்சியினரால் நடத்தப்பட்ட முழு அடைப்பு (பந்த்)
போராட்டம் மட்டும் ஏன் 9-ந்தேதி நடந்தது. அதையும் 12-ந் தேதியே நடத்தி இருக்கலாமே.
அதுவும் தமிழின உணர்வின் வௌிப்பாடுதான். உணர்வுபூர்வமாக ஒரே கருத்தை வௌிப்படுத்த
அறவழியில் நடக்கும் அனைத்து போராட்டமும் வரவேற்கத்தக்கதுதான். எந்த வொரு
போராட்டத்தின் விளைவும் இந்தியாவின் ஒற்றுமையை, ஒருமைப்பாட்டை சிதைத்துவிடக்கூடாது.
அதை மனத்தில் வைத்துக் கொண்டு தான் செயல்படவேண்டும். ரஜினிகாந்த், கமல்ஹாசன்,
விஜயகாந்த், சத்யராஜ், பிரபு, நெப்போலியன், டி.ராஜேந்தர், பிரபு, முரளி,
சந்திரசேகர் மற்றும் கலைக்குடும்பத்தை அனைத்து நடிகர், நடிகைகளும் எனது சகோதர சகோதரிகளே.
</p><p>
</p><p>
ரசிகர்கள் போராட்டம் </p><p>
</p><p>
இப்படிப்பட்ட சூழலில் நானும் எனது ரசிகர் மன்றத்தை சார்ந்தவர்களும் அமைதி யான வழியில்
போராடுவோம். அறவழியில் எந்தவொரு போராட்டத்தையும் ரசிகர் மன்ற தோழர்கள்
செய்யலாம். என்னைப் பொறுத்தவரை நான் எந்த வழியில் போராட வேண்டும் என்பதை நான்
சார்ந்திருக்கும் மாபெரும் இயக்கமான திராவிட முன்னேற்ற கழகத்தின் தலைவரும், காவிரி நீர்
பிரச்சினையில் பல்வேறு முறை சுமுக தீர்வு கண்டு தமிழக விவசாய பெருமக்களுக்கு எந்த
பிரச்சினையும் இல்லாமல் காவிரி நீர் பெற்றுத் தந்த தானைத்தலைவர் கலைஞர்
சொல்லும் வழியில் போராடுவேன். காவிரி நீருக்காக தலைவர் கலைஞர் எந்த ஒரு
போராட்டத்தில்களம் இறங்கச் சொன்னாலும் என்னை வாழ வைத்துக்கொண்டிருக்கும் தமிழக
மக்களுக்கும், ரசிகர்களுக்கும், தாய்மார்களுக்கும், வறட்சியால் வாடும் விவசாய
தொழிலாளருக்காகவும் எனது உடல், பொருள், உயிர் அனைத்தையும் அர்ப்பணிக்க தயாராக
இருக்கிறேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறி உள்ளார். </p>

<p> </p>

<p> </p>

<p>வௌியூர் ரசிகர்கள்
சென்னை வரவேண்டாம் சத்தியநாராயணன் வேண்டுகோள்</p>

<p> </p>

<p>சென்னை, அக்.11- வௌியூர் ரசிகர்கள் சென்னைக்கு வரவேண்டாம்
என்று சத்திய நாராயணன் வேண்டு கோள் விடுத்திருக்கிறார். </p><p>
</p><p>
இது குறித்து ரஜினிகாந்த் ரசிகர் நற்பணி மன்ற பொறுப்பாளர் க.சத்திய நாராயணன்
வௌியிட்ட அறிக்கையில் கூறி உள்ளதாவது: </p><p>
</p><p>
க.சத்திய நாராயணன் </p><p>
</p><p>
தமிழகமெங்கும் உள்ள ரஜினிகாந்த் ரசிகர் நற்பணி மன்ற சகோதரர்களுக்கு அகில இந்திய
ரஜினிகாந்த் ரசிகர் நற்பணி மன்ற பொறுப்பாளர் க.சத்தியநாராயணன் தெரிவித்துக்கொள்வது:
</p><p>
</p><p>
உச்ச நீதிமன்ற ஆணையின் படி காவிரியில் தண்ணீர் திறந்துவிடக் கோரி, கர்நாடக அரசை
வலியுறுத்தி, வருகின்ற 12-ந் தேதி நமது அன்புத் தலைவர் ரஜினிகாந்த் இருக்கப் போகிற
உண்ணாவிரதத்தில் எல்லா ஊர்களிலிருந்தும் ஆயிரக்கணக்கில் நீங்கள் அனைவரும் சென்னைக்கு
வந்து ஆர்வத்துடன் கலந்து கொள்ள ஏற்பாடு செய்வதாக அறிந்தேன். </p><p>
</p><p>
சென்னை வரவேண்டாம் </p><p>
</p><p>
மேற்படி உண்ணாவிரதப் போராட்டத்திற்கு பெருகி வரும் ஆதரவைப் பார்த்து, சீரணி
அரங்கத்தில் நடத்த அனுமதி கேட்டிருந்தோம். ஆனால் அங்கு வேறு ஒரு நிகழ்ச்சி
நடக்கவிருப்பதால் நமக்கு சேப்பாக்கம் அரசு விருந்தினர் விடுதி அருகில் அனுமதி கிடைத்து
உள்ளது. </p><p>
</p><p>
மேற்சொன்ன இடத்தில் போதுமான இடவசதி இல்லாத காரணத்தால் தயவு செய்து வௌியூரில்
இருக்கும் அன்பு ரசிகர்கள் யாரும் இங்கு வரவேண்டாமென அன்புடன் கேட்டுக் கொள்கிறேன்.
காவிரி பிரச்சினை சுமுகமாக தீர எல்லாம் வல்ல இறைவனை பிரார்த்திக்குமாறு கேட்டுக்
கொள்கிறேன். </p><p>
</p><p>
கருணாநிதிக்கு நன்றி </p><p>
</p><p>
காவிரி நதி நீர் பிரச்சினையில் சென்னையிலேயே போராடலாம் என்று ரஜினிகாந்த் எடுத்த
முடிவிற்கு முதலில் குரல் கொடுத்த முன்னாள் முதல்வர் டாக்டர் கலைஞர் அவர்களுக்கு
நன்றியை தெரி வித்துக் கொள்கிறேன். தலைவரது உண்ணாவிரத போராட்டத்திற்கு ஆதரவு
தெரிவித்திருக்கும் பாரதிய ஜனதா கட்சி, காங்கிரஸ் கட்சி, காங்கிரஸ் ஜனநாயக பேரவை,
இந்தி கம்யூனிஸ்டு, விடுதலை சிறுத்தைகள் அமைப்பு, புதிய தமிழகம், ஜனதா கட்சி, ஐக்கிய
கம்யூனிஸ்டு கட்சி, சிவசேனா, அகில இந்திய முஸ்லிம் லீக், அகில இந்திய மீனவர் சங்கம்
மற்றம் ஆதரவு தெரிவித்துள்ள பல்வேறு அமைப்புகளுக்கும் ரஜினிகாந்த் சார்பிலும், நமது
ரசிகர் மன்ற சார்பிலும் எனது மனமார்ந்த நன்றியை தெரிவித்துக் கொள்கிறேன். </p><p>
</p><p>
வேண்டுகோள் </p><p>
</p><p>
மேலும், ரஜினிகாந்த், 12-ந் தேதி காலை 8மணி முதல் மாலை 5மணிவரை உண்ணா விரதம்
இருக்குமிடம் பெரிய அளவிலான கூட்டத்தைத் தாங்காது என்பதாலும், அப்பகுதி பொதுமக்களுக்கு
இடையூறு ஏற்படாமல் இருக்கவும், இப்போராட்டத்திற்கு ஆதரவு தெரிவித்துள்ள அனைத்துக்
கட்சி தொண்டர்களும், சென்னை ரசிகர்களும் வௌியூர் ரசிகர்களைப் போன்று ஒத்துழைப்பு
அளிக்கும்படி கேட்டுக்கொள்கிறேன். </p><p>
</p><p>
கவர்னரிடம் மனு </p><p>
</p><p>
இந்த உண்ணாவிரதத்திற்கு இடமளித்த தமிழக அரசிற்கும், தகுந்த பாதுகாப்பு ஏற்பாடு களை
செய்து கொடுக்க உறுதி அளித்துள்ள சென்னை மாநகர காவல்துறை ஆணைய ருக்கும் நன்றியை
தெரிவித்துக் கொள்கிறேன். </p><p>
</p><p>
உண்ணாவிரதம் முடிந்த பிறகு ரஜினிகாந்த், நேராக ராஜ்பவனுக்குச் சென்று ஆளு நரை சந்தித்து மனு
கொடுக்கிறார். நல்லவர்களை ஆண்டவன் சோதிப்பான். கைவிட மாட்டான். </p><p>
</p><p>
இவ்வாறு அவர் கூறி உள்ளார். </p>

<p> </p>

<p> </p>

<p>நடிகர் சல்மான்கான் ஜாமீன்
மனு நிராகரிப்பு போலீஸ் காவல் நீட்டிப்பு</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>சல்மான்கான </p>

<p> </p>

<p>மும்பை, அக்.11- நடிகர் சல்மான்கான் நேற்று மும்பை கோர்ட்டில்
ஆஜர்படுத் தப்பட்டார். அவரது ஜாமீன் மனு நிராகரிக்கப்பட்டு போலீஸ் காவல்
நீட்டிக்கப்பட்டது. </p><p>
</p><p>
கோர்ட்டில் ஆஜர் </p><p>
</p><p>
பிளாட்பாரத்தில் தூங்கிக் கொண்டிருந்தவர்கள் மீது கார் ஓட்டிக்கொன்ற வழக்கு காரணமாக
நடிகர் சல்மான்கான் கைதானார் அல்லவா? தற் போது அவர் தெற்கு மும்பையில் உள்ள
கிராபோர்டு என்ற இடத்தில் போலீஸ் காவலில் வைக்கப்பட்டுள்ளார். </p><p>
</p><p>
நேற்று அவர் வடமேற்கு மும்பையில் உள்ள பாந்த்ரா மெட்ரோ பாலிடன் நீதிமன்றத் தில்
பிற்பகல் 2.40 மணியளவில் ஆஜர்படுத்தப்பட்டார். சல்மான்கான் அப்போது டெனிம்
பாண்ட்டும், டீ சர்ட்டும் அணிந்திருந்தார். </p><p>
</p><p>
உட்கார அனுமதி </p><p>
</p><p>
நீதிபதி எஸ்.ஒய்.சிசோடி முன்னிலையில் சல்மான் ஆஜர் படுத்தப்பட்டார். அவரை
நாற்காலியில் உட்கார நீதிபதி அனு மதித்தார். </p><p>
</p><p>
அப்போது சல்மானின் வக்கீல்கள் அபாத் போண்டா, சமீர்கான் ஆகியோர் ஆஜராகி
வாதாடினார்கள். சல்மானை ஜாமீனில் விடக்கோரினார்கள். ஆனால் நீதிபதி அதை ஏற்கவில்லை.
ஜாமீன் மனுவை அவர் நிராகரித்து விட்டார். சல்மானை தற்போது ஜாமீனில் விடுவிப்பது
விசாரணையை கடுமையாக பாதித்துவிடும் என்று அவர் கருத்து தெரிவித்தார். </p><p>
</p><p>
காவல் நீட்டிப்பு </p><p>
</p><p>
அப்போது அரசு வக்கீல், சல்மான்கானின் போலீஸ் காவலை மேலும் 10 நாட்களுக்கு நீட்டிக்க
அனுமதி கோரினார். நீதிபதி அதை ஏற்கவில்லை. சல்மான்கானின் போலீஸ் காவலை மேலும் 4
நாட்களுக்கு மட்டும் (அக்.14-ந் தேதி வரை) நீட்டிக்க அவர் உத்தர விட்டார். </p><p>
</p><p>
வீட்டுச்சாப்பாடு </p><p>
</p><p>
சல்மானுக்கு வீட்டில் இருந்து சாப்பாடு, தண்ணீர், மாற்று உடைகள், டாய்லட் பொருட்களை தர
நீதிபதி அனு மதித்தார். </p><p>
</p><p>
இரண்டே கேள்வி </p><p>
</p><p>
நேற்று மும்பை நீதிமன்றத்தில் ஆஜர்படுத்தப்பட்ட நடிகர் சல்மான்கான் நிருபர்களிடம்
கூறியதாவது:- </p><p>
</p><p>
போலீசார் காவலில் வைத்து விசாரிக்கும்போது என்னிடம் இரண்டே கேள் விகள்தான்
கேட்கிறார்கள். காரை நீ ஓட்டினாயா? கார் உனக்கு சொந்தமானதா? இதுதான் அந்த 2
கேள்வி. தினமும் 15 நிமிடம் தொடர்ந்து இதே கேள்விகளை கேட்கிறார்கள். இதற்காக எதற்கு
போலீஸ் காவலை நீட்டிக்க வேண்டும்? </p>

<p> </p>

<p> </p>

<p>இறக்குமதி ஆன வௌிநாட்டு
வாகனம்: சல்மானுக்கு கார் விற்றவர் கோர்ட்டில் ஆஜராக உத்தரவு</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>சல்மான்கான </p>

<p> </p>

<p> </p>

<p>மும்பை, அக்.11- நடிகர் சல்மான்கான் கார் ஏற்றி ஆளைக்கொன்றார்
அல்லவா? அவருக்கு வௌிநாட்டு கார் விற்ற ஏஜெண்டுக்கு மும்பை சுங்கத்துறை சம்மன்
அனுப்பியுள்ளது. </p><p>
</p><p>
சல்மான்கான் </p><p>
</p><p>
பிரபல நடிகர் சல்மான்கான் கடந்த செப்.28-ந்தேதி குடித்து விட்டு கார் ஓட்டினார்
அல்லவா? மும்பை பந்த்ரா பகுதியில் அவர் கண்மூடித்தனமாக காரை ஓட்டி பிளாட் பாரத்தில்
படுத்திருந்தவர்கள் மீது மோதினார். அதில் ஒருவர் பலியானார். 4 பேர் படுகாய மடைந்தனர்.
சல்மான்கான் தற்போது கைதாகி போலீஸ் காவலில் இருக்கிறார். </p><p>
</p><p>
வௌிநாட்டுக்கார் </p><p>
</p><p>
சல்மான் ஓட்டிவந்த கார் நவீன டயோட்டா லாண்ட் குரூசர் ரக கார் ஆகும். இது
வௌிநாட்டுக்கார் என்பதால் இந்த காரை சல்மான்கானுக்கு விற்றவருக்கு மும்பை சுங்கத் துறை
சம்மன் அனுப்பியுள்ளது. அந்த நபரின் பெயர் அப்துல் ரகிம். அவர் கேரளாவில் உள்ள
காசர்கோடு பகுதியைச் சேர்ந்தவர் ஆவார். காசர்கோடு முகவரிக்கு சம்மன் அனுப்பப்பட்டுள்ளது.
</p><p>
</p><p>
வேறு நடவடிக்கை </p><p>
</p><p>
இதுபற்றி மும்பை சுங்கத் துறை (இறக்குமதிப்பிரிவு) கமிஷனர் அகமது உசேன் கூறி யிருப்பதாவது:-
</p><p>
</p><p>
சல்மான்கானுக்கு கார் விற்பனை செய்த அப்துல் ரகிம் காசர்கோடு முகவரியில் இல்லை என்றால்
அவர் மீது வேறுமாதிரியான நடவடிக்கை எடுக்கப்படும். சல்மான்கான் கார்வாங்கிய விதம், பணம்
செலுத்திய விதம், கார் பற்றிய தகவல்களை திரட்டும்படி மத்திய புலனாய்வுத்துறையை நாங்கள்
கேட்டுக்கொண்டிருக்கிறோம். தேவைப்பட்டால் சல்மான்கானுக்கும் சம்மன் அனுப்புவோம்.
தேவைப்படும் பட்சத்தில் மும்பை போலீசின் உதவியை கேட்டுப்பெறுவோம். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
பல்வேறு இலாகா </p><p>
</p><p>
சல்மான்கானின் வௌி நாட்டுக்கார் தொடர்பாக தற்போது விற்பனை வரிப்பிரிவு,
வருமானவரிப்பிரிவு, கஸ்டம்சின் வேறுபல பிரிவுகள் விசாரணை தொடங்கியுள்ளன.
சல்மான்கானின் கார், இறக்குமதி செய்யப்பட்ட வௌிநாட்டுக் கார் என்பதால் பல்வேறு
கோணங்களில் அவர்கள் விசாரணை நடத்தி வருகிறார்கள். </p><p>
</p><p>
திருட்டுக் காரா? </p><p>
</p><p>
விபத்தில் சிக்கிய சல்மான்கானின் கார், திருட்டுக் கார் என்று ஆரம்பத்தில் வதந்தி
பரவியது. அதை சுங்கத்துறை அதிகாரிகள் மறுத்துள்ளனர். அது முறையாக இறக்குமதி செய்யப்பட்ட
கார் என்று அதிகாரிகள் தெரிவித்துள்ளனர். சல்மான்கானின் அண்ணன் ஒருவரும் கார்இறக்குமதி
செய்து வருகிறார். அவர்தான் சல்மானுக்கு கார் விற்றார் என்பதையும் அதிகாரிகள்
மறுத்துள்ளனர். </p>

<p> </p>

<p> </p>

<p>தஞ்சையில் அவரே
அளித்த பேட்டி: நடிகர்கள் அஜீத்-விஜய்க்கு சிலம்பரசன் போட்டியா?</p>

<p> </p>

<p>தஞ்சை,அக்.11- தஞ்சையில் நிருபர்களுக்கு பேட்டி அளித்த
நடிகர் சிலம்பரசன் நடிகர்கள் அஜீத், விஜய்க்கு தான் போட்டியா? என்ற கேள்விக்கு
பதில் அளித்தார். </p><p>
</p><p>
சிலம்பரசன் பேட்டி </p><p>
</p><p>
தஞ்சைக்கு வந்த நடிகர் சிலம்பரசன் நிருபர்களுக்கு பேட்டி அளித்தார். அப்போது அவர்
கூறியதாவது:- </p><p>
</p><p>
நடிகர்களை பொறுத்த வரையில் 4, 5 திரைப்படங்களில் நடித்த பிறகுதான் அவர் களுக்கு ரசிகர்
மன்றம் அமைப்பார்கள். ஆனால் நான் ஹீரோவாக நடிப்பதற்கு முன் பாகவே நாடு முழுவதும் எனக்கு
மன்றங்கள் தொடங்கியுள்ளனர். இதற்காக நான் மகிழ்ச்சி அடைகிறேன். காவிரி
பிரச்சினையை பொறுத்த வரையில் எல்லோருக்கும் உள்ள உணர்வுதான் எனக்கும் உள்ளது.
நடிகர்களுக்குள் எந்தவிதமான குழப்பமும் இல்லை. இரண்டு விதமாக போராட்டம் நடத்துகிறார்கள்.
இந்த போராட்டத்தில் நான் கலந்து கொள்வது குறித்து எனது தந்தையிடம் கேட்டுத்தான் முடிவு
செய்வேன். </p><p>
</p><p>
அஜீத், விஜய் போன்றவர்களை நான் போட்டியாக நினைக்கவில்லை. யார் யாருக் கும் போட்டி
இல்லை. எனக்கு இசையில் அதிக ஆர்வம் உண்டு. டைரக்ஷன் செய்வது குறித்து எனக்கு இப்போது
ஆர்வம் இல்லை. இப்போதைக்கு நான் ஒரு நடிகன். குறிப்பிட்ட பாத்திரத்தில் மட்டும்
நடிக்காமல் நான் எல்லா கதாபாத்திரத்திலும் நடிக்க விரும்புகிறேன். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p>

<p> </p>






</body></text></cesDoc>