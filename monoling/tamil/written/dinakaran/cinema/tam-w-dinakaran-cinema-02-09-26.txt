<cesDoc id="tam-w-dinakaran-cinema-02-09-26" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-02-09-26.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-09-26</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-09-26</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>பாபா வெற்றிப்படமா?
ரஜினி தனிக்கட்சி தொடங்க வேண்டும் லயோலா கல்லூரி மாணவர் கருத்து கணிப்பு முடிவு</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>ரஜினிகாந்த் </p>

<p> </p>

<p>சென்னை, செப். 26- ரஜினிகாந்த் தனி கட்சி தொடங்க வேண்டும்
என்று ரசிகர்களும், பொதுமக்களு மாக 60 சதவீதம் பேர் கருத்து தெரிவித்துள்ளதாக லயோலா
கல்லூரி மாணவர்கள் நடத்திய கருத்து கணிப்பில் கூறப்பட்டு உள்ளது. </p><p>
</p><p>
பாபா கருத்தரங்கு </p><p>
</p><p>
சென்னையில் உள்ள லயோலா கல்லூரியை சேர்ந்த விஷூவல் கம்யூனிகேசன் மாணவ-மாணவிகள்
பாபா படத்தை பற்றி ரஜினிகாந்த் அரசியலுக்கு வருவதா? ஆன் மீகத்துக்கு செல்வதா?
என்பது பற்றியும் பாபா படம் வெற்றி படமா என்பது பற்றியும் ரசிகர்கள், பொதுமக்களிடம்
கருத்து கணிப்பு நடத்தினார்கள். அதன் மீது நேற்று கருத்தரங்கு நடந்தது. </p><p>
</p><p>
பல்வேறு மாணவ- மாணவிகள் தங்களது கருத்துக்களை மனம் திறந்து கூறி னார்கள். ஒரு மாணவர்
கூறும்போது, பாபா படத்தை இரண்டு படமாக உருவாக்கி இருக்கலாம். முதல்பாதி படத்தை ஆக்ஷன்
படமாகவும், இரண்டாவது பாதி படத்தை பக்தி படமாகவும் வௌியிட்டிருந்தால் 2 படமும் பெரிய
வெற்றி பெற்றிருக்கும் என்றார். இன்னொரு மாணவர் கூறும் போது,ரஜினி படத்துக்கு போவதே
புதுசா ஏதாவது அவர் ஸ்டைல் செய்வார், முதல் காட்சியில் முதல் ஷோ ரஜினி படம்
பார்த்தேன் என்று பெருமைபட்டுக்கொள்ளலாம். ரஜினி படமென்றால் அரட்டை அடித்து, கலாட்டா
செய்தபடி காட்சிகளை ரசிக்கலாம். மற்ற நடிகர்களின் படங்களில் இதை செய்ய முடியாது
என்றார். </p><p>
</p><p>
பாபா படத்தை அம்மா, அப்பா, தங்கையுடன் ஒரு தரம், நண்பர்களுடன் ஒரு தரம், தனியாக ஒரு
தரம் என்று 3 முறை பார்த்தேன். இதுவொரு வெற்றி படம்தான் என்று ஒருவரும், பாபா படத்தில்
ரஜினியிடம் நிறைய எதிர்பார்த்து சென் றோம். ஆன்மீக பகுதி வந்த போது அது ஏமாற்றத்தை
தந்தது என்று இன்னொரு மாணவரும் கருத்து தெரிவித்தனர். மேலும் பல்வேறு மாணவர்கள் பாபா
படத்தில் தங்களுக்கு பிடித்தபகுதி, பிடிக்காத பகுதி எது என்றும் படத்திற்கு முதலில்
நிர்யிக்கப்பட்ட கட்டணம் மிகவும் அதிகம் என்றும் கூறினர். </p><p>
</p><p>
5 தியேட்டர் </p><p>
</p><p>
பாபா படம் திரைக்கு வந்த ஆகஸ்டு 15-ந் தேதி முதல் செப்டம்பர் மாதம் 17-ந் தேதி வரை 40
மாணவர்கள் அப்படத்தின் கருத்து கணிப்புகளை சேகரித்து வந்தனர். இதற்காக சென்னையில்
உள்ள குரோம் பேட்டை வெற்றி, பூந்தமல்லி சுந்தர், திருவான்மியூர் தியாகராஜா, பழைய
வண்ணாரப் பேட்டை மகாராணி, எழும்பூர் ஆல்பர்ட் ஆகிய 5 சினிமா தியேட்டர்கள் தேர்வு
செய்யப்பட்டு ரசிகர்களிடமும், பொது மக்களிடமும் கருத்து கணிப்பு நடத்தப்பட்டது. இதுபோல்
2500 பேர்களிடம் கருத்துகணிப்பு நடத்தப்பட்டது. இதுதவிர தியேட்டர் மானேஜர்கள், கேண்டீன்
வைத்திருப்பவர்கள் ஆகியோரிடம் கருத்து கேட்கப் பட்டது. பாபா படம் பார்க்க
தியேட்டருக்குள் செல்பவர்களிடமும் படம் முடிந்து வௌியில் வருபவர்களிடமாக நூறுபேர்களிடம்
இரண்டு கட்டமாக கருத்து கணிப்பு நடத்தப்பட்டது. </p><p>
</p><p>
60 சதவீதம் பேர் ஆதரவு </p><p>
</p><p>
இவ்வாறு நடத்தப்பட்ட கருத்துக்கணிப்பில் 60 சத வீதம் ரசிகர்கள் மற்றும் பொது மக்கள்
ரஜினிகாந்த் அரசியலுக்கு வரவேண்டும். அவர் தனிக்கட்சி தொடங்க வேண் டும் என்று கூறி
உள்ளனர். ரஜினி ஆன்மீகத்திற்கு செல்வதுபற்றிய கருத்து கணிப் பில் வெறும் 2 சதவீதம்
பேர்கள் மட்டுமே அவர் ஆன்மீகத்துக்கு செல்லலாம் என்று கருத்து தெரிவித்திருக்கின்றனர். </p>

<p> </p>

<p> </p>

<p>அபிராமி வளாகத்தில் 6
தியேட்டர்: 4 வது மாடியில் காரில் அமர்ந்து சினிமா பார்க்கலாம் ராமநாதன் பேட்டி</p>

<p> </p>

<p>சென்னை, செப். 26- சென்னையில் உள்ள அபிராமி தியேட்டரில் 4-வது
மாடியில் காரில் அமர்ந்து படம் பார்க்கும் திறந்த வௌி அரங்கு வசதி செய்யப்படுகிறது என்று
ராமநாதன் கூறினார். </p><p>
</p><p>
திறந்த வௌி தியேட்டர் </p><p>
</p><p>
புரசைவாக்கம் நெடுஞ் சாலையில் உள்ள அபிராமி தியேட்டர் வளாகம் 11 கோடி ரூபாய்கும்
அதிகமான செலவில் அபிராமி மா மஹால் என்ற பெயரில் முற்றிலுமாக சர்வதேச தரத்திற்கு
உயர்த்தப்படுகிறது. ஏற்கனவே உள்ள அபிராமி தியேட்டர் அபிராமி, பேபி அபிராமி என்று
இரண்டு தியேட்டராக பிரிக்கப்படுகிறது. நான்காவது மாடியில் (கூரைபகுதி) டிரைவ் இன்
தியேட்டர் அமைக்கப்படுகிறது. இத்துடன் மொத்தம் 6 தியேட்டர்கள் இதில் அமைகிறது.
வாகனத்தில் இருந்தபடியும், உணவு விடுதியில் அமர்ந்து கொண் டும் திரைப்படத்தை பார்க்க
முடியும். கார்களை நான்காவது தளத்திற்கு எடுத்துச் செல்ல இரண்டு எலிவேட்டர் வசதி
ஏற்படுத்தப்பட்டுள்ளது. டிரைவின் தியேட்டர் உள்ள மேல்தளத்தில் 32 கார்கள் நிறுத் தக்கூடிய
வகையில் இட வசதி உள்ளது. </p><p>
</p><p>
பாஸ்ட் புட்-காய்கறி </p><p>
</p><p>
முதல் தளத்தில் 18 பாஸ்ட் புட் கடைகள், ஒரு காய்கறி கடை, உணவு விடுதி, பல்பொருள்
அங்காடி, குளிர்பான கடை, ஐஸ்கிரீம் பார்லர், பழரசக்கடை மதுபான பார் இவைகளுடன்
பார்வையாளர்கள் சுதந்திரமாக புழங்குவதற்கு ஏற்ப 12 ஆயிரம் சதுர அடியில் திறந்த
வௌிப்பகுதி போன்றவை இருக்கும். இரண்டாவது தளத்தில் 18 துணிக் கடைகள், காலணிகள்,
ஜவுளி, பொம்மைகள், பைகள;/
Mguz';fs; nghd;wit th';Ftjw;fhd filfs;/ rpwe;j czt[ tpLjp ,Uf;Fk;. \d;whtJ
jsj;jpy; th;j;jfg; bghUl;fis tpw;gid bra;tjw;fhd 17 filfs;/ czt[tpLjp Mfpait
,Uf;Fk;. nkYk; tPonah nfk;!;/ tpisahl;L fhh;fs; vd ,is"h;fis
cw;rhfg;gLj;Jk; bghGJnghf;F mk;r';fs; bfhz:l tpis ahl;L mu';Fs; ,Uf;Fk;. </p><p>
</p><p>
,';Fs;s tzpf/ th;j;jf mikg;g[fs; Mz:L KGtJk; xt;bthU ehSk; ,ut[ 11 kzp tiu
bray;gLk;. efhpd; ,ju gFjpfspy; trpf;Fk; thof;ifahsh;fis Fiwe;j fl;lzj;jpy;
,';F miHj;J tut[k;/ jpUk;g bfhz:L brd;W tplt[k; fhh; trjp Vw;ghL bra;ag;gl;L
cs;sJ. epyeLf;fj;ij jh';ff;Toa fl;Lkhzk;/ o$pl;ly; bjhHpy;El;gj;Jld; Toa
o.vr;.vf;!; jpiu mu';Ffs;/ mgpuhkp kh kQhYf;F tUk; thof;ifahsh;fspd; thfd';fis
epWj;j nghJkhd ,ltrjp (170 fhh;fs;/ 200 ,uz:L rf;fu thfd';fis epWj;j yhk;)
nghd;wit ,e;j tshfj;jpy; ,Uf;Fk;. ,';F tUgth;fis j';fsJ FHe;ijfis ghJfhg;ghftpl;L
brd;W kPz:Lk; miHj;J bry;yt[k; trjp bra;ag;gl;L cs;sJ. mLj;j Mz:L khh;r; khjk;
Kjy; ,e;j tshfk; bray;gLk;. mgpuhkp th;j;jf tshfj;jpd; khh;f;bfl;o'; Kftuhf
gd;dhl;L epWtdkhd rpgP hpr;rh;L vy;yP!; bray;gLfpwJ. </p><p>
</p><p>
,t;thW mgpuhkp uhkehjd; Twpdhh;. </p><p>
</p><p>
kh$pf; n#h </p><p>
</p><p>
ey;yk;ik uhkehjd; TWk;nghJ/ mgpuhkp kh kQhypy; ,lk; bgw cs;s kpfg; bghpa
tuhz:lhtpy; ,ytrkhf ,ir epfH;r;rpfs;/ nk$pf; n#hf;fs;/ ehl;oa/ eld';fs;
cs;spl;l gy bghGJnghf;F epfH;r;rpfs; ,lk; bgWk;. cs;ehl;L kf;fSila uridf;F
Vw;gt[k;/ rh;tnjr juj;jpw;F ,izahft[k; ,J ,Uf;Fk; vd;whh;. </p><p>
</p><p>
jpdKk; 8 Mapuk; ngh; rpdpkh ghh;f;Fk; jpnal;lh; </p><p>
</p><p>
g[uir thf;fj;jpy; milahsk; brhy;yg;gLk; ,lkhf tps';Fk; mgpuhkp jpiu mu';Ffspy;
ehs;njhWk; 8 Mapuk; ngh; rpdpkh ghh;f;fpd;wdh;. fle;j 26 Mz:Lfshfr; bray;gLk;
mgpuhkp jpiu mu';Ffspy; ,e;jpahtpnyna Kjd;Kiwahf o.o.v!;. mwpKfg;gLj;jg;gl;lJ.
nkYk; khepy bkhHp jpiu mu';Ffspy; lhy;gp rt[z:l; rp!;lk; mikf;fg;gl;lJ vd;gJ
Fwpg;gplj; jf;fJ. glk; ghh;f;f tUgth; fs; bjhiyngrp \yk; of;bfl;Lfis Kd;gjpt[
bra;J bfhs;syhk;. fle;j 26 Mz:Lfspy; ,j;jpiu mu';fpy; 44 gl';fs; MW khj';fs;
btw;wpfukhf Xo cs;sd. 183 gl';fs; E}W ehl;fs; Xo cs;sd. </p>

<p> </p>

<p> </p>

<p>சினிமா தயாரிப்பாளர்
சங்கம் மீது ராஜன் தொடர்ந்த வழக்கு தள்ளுபடி ஐகோர்ட்டு தீர்ப்பு</p>

<p> </p>

<p>சென்னை,செப்.26- தமிழ்் திரைப்பட தயாரிப்பாளர் சங்க தேர்தல்
எதிர்த்து ராஜன் தாக்கல் செய்த வழக்கை சென்னை ஐகோர்ட்டு தள்ளுபடி செய்தது. </p><p>
</p><p>
வழக்கு </p><p>
</p><p>
தமிழ் திரைப்பட தயாரிப்பாளர் சங்க தேர்தல் வருகிற 28-ந்தேதி நடக்கிறது. இந்த
தேர்தலில் பட அதிபர் கே.ஆர்.ஜி.தலைமையில் ஒரு அணியும் லட்சுமி மூவி மேக்கர்ஸ் பட
நிறுவன அதிபர் கே.முரளி தரன் தலைமையில் மற்றொரு அணியும் போட்டியிடுகிறது. கே.ராஜன்
சங்க தேர்தலில் போட்டியிட வேட்பு மனு தாக்கல் செய்திருந்தார். இந்த மனுக்களை தேர்தல்
அதிகாரி ஏ.வி.எம்.முருகன் பரிசீலனை செய்து தேர்தலில் போட்டியிடுபவர்களின் வேட்பாளர்
பட்டியலை அறிவித்தார். இதற்கிடையில் கே.ராஜன் சென்னை ஐகோர்ட்டில் ஒரு வழக்கு தாக்கல்
செய்தார். </p><p>
</p><p>
தள்ளுபடி </p><p>
</p><p>
அந்த மனுவில் சங்கத்தில் ஓட்டு உரிமை பறிக்கப்பட்ட குறிப்பிட்ட உறுப்பினர்களுக்கும் ஓட்டு
அளிக்க அனுமதி தர வேண்டும் என்று கூறி இருந்தார். இந்த வழக்கை நீதிபதி ஆர்.பாலசுப்பிர
மணியம் விசாரித்து தள்ளுபடி செய்து தீர்ப்பு கூறினார். </p>

<p> </p>






</body></text></cesDoc>