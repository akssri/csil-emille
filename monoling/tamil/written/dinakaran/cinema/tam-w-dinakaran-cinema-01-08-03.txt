<cesDoc id="tam-w-dinakaran-cinema-01-08-03" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-01-08-03.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-08-03</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-08-03</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>தமிழ் திரைப்படங்களுக்கு
வரியை நீக்க வேண்டும், திரையரங்கு உரிமையாளர்கள் கோரிக்கை</p>

<p> </p>

<p>கோவை, ஆக.3- தமிழ் திரைபடங்களுக்கு ஒட்டு மொத்த வரியை நீக்க
வேண்டும் என்று திரையரங்கு உரிமை யாளர்கள் கோரிக்கை வைத்து உள்ளனர். </p><p>
</p><p>
சங்க கூட்டம் </p><p>
</p><p>
தமிழ்நாடு திரையரங்கு உரிமை யாளர்கள் சங்கத்தின் ஒட்டு மொத்த வரி செலுத்தும் உறுப்பி
னர் கூட்டம் கோவை ஆர்.எச். ஆர்.ஒட்டலில் நேற்று (புதன் கிழமை) மாலை 4 மணியளவில்
நடந்தது. </p><p>
</p><p>
கூட்டத்திற்கு மாவட்ட பொது செயலாளர் நாராயணசாமி நாயுடு தலைமை தாங்கினார். மாநில உதவி
தலைவர் (டி.எம். கே.) பழனிச்சாமி கவுண்டர் முன்னிலை வகித்தார். </p><p>
</p><p>
தீர்மானம் </p><p>
</p><p>
கூட்டத்தில் கீழ்க்கண்ட தீர்மானங்கள் நிறைவேற்றப்பட்டது. </p><p>
</p><p>
டி.வி.,கேபிள்டி.வி, வி.சி.டி. போன்ற கருவிகளின் மூலம் அரசுக்கு பெரிய வருவாய் எதுவும்
இல்லை. பொதுமக்கள் தினமும் இதன் மூலம் ஏராள மான திரைபடங்கள் பார்க்க வாய்ப்பு
இருப்பதால் திரைய ரங்குகளுக்கு வருவது 60 முதல் 70 சதவீதம் வரை குறைந்து விட்டது. எனவே திரை
அரங்கு கள் நடத்த முடியாத சூழ்நிலை ஏற்பட்டு உள்ளது. இதை கருத்தில் கொண்டு தமிழ்
திரைப்படங்களுக்கு ஒட்டு மொத்த வரியை முழுவதுமாக நீக்க வேண்டும் என தமிழக அரசை
கேட்டுக்கொள்கிறோம். </p><p>
</p><p>
கடைசி வகுப்பு கட்டணம் உள்பட அனைத்து வகுப்பு அனுமதி கட்டணத்தை மாற்றி அமைத்து கொள்ளும்
உரிமை வேண்டும் </p><p>
</p><p>
உயர்த்துக </p><p>
</p><p>
பராமரிப்பு கட்டணத்தை ஒரு நுழைவு சீட்டிற்கு ஒரு ரூபாயாக உயர்த்தி தர வேண் டும்.மேற்கண்ட
தீர்மானங் களை வரும் ஆகஸ்ட் 15-ந் தேதிக்குள் நிறைவேற்ற படாவிட்டால் திரையரங்கள்
மூடும் அபாயம் உள்ளது என்றும் நிர்வாகி கள் தெரிவித்தனர். </p><p>
</p><p>
பங்கேற்றோர் </p><p>
</p><p>
கூட்டத்தில் பெரியநாயக்கன் பாளையம் லட்சுமி தியேட்டர் ரங்கசாமி கவுண்டர், நரசிம்ம
நாயக்கன் பாளையம் பத்மா லயா ஆர்.தாமோதரன், சேவூர் மணி,( ராம் தியேட்டர்)
டி.சந்திரசேகர் (கருணா, அன் னூர்), வௌ்ளிங்கிரி (அரசூர் சத்தி), நடராஜன் (இந்திரா
கோபி), மகாலட்சுமி தியேட்டர் மேலாளர் கே.எம்.சேகர், தேவி சித்ரா தியேட்டர் கந்தசாமி,
ராஜாத்தி தியேட்டர் ஆனை மலை எல்.எம்.முகமது சாதிக், முகமது முஷதீக், பவானி ராயல் சுரேஷ்,
அந்தியூர் மோகன், பிரபு ( சந்தியாஅண்ட் வசந்தா), விசுவநாதன்(சித்ரா),
அங்குசாமி(சென்னியப்பா), உதயக்குமார்(வௌ்ளக்கோயில்), சாலிமுகம்மது (கொடுமுடி)
வி.எஸ்.முத்துசாமி(ராஜராஜேஸ்வரி டாக்கீஸ்), சிவகிரி மணியகாரர் (வேல் தியேட்டர்),
பா.சு.சண் முகம் (ராக்கேட், பாசூர்) கணி யூர் ஏ.ஆர்.சி.தியேட்டர் அப்துல் சமது,
மலையாண்டி பட்டணம் மயில்சாமி, ராமமூர்த்தி (சங் கீதா), எஸ்.பாலசுப்பிரமணி யம்
(எஸ்.ஏ.பி.எஸ்.) மற்றும் மேட்டுப்பாளையம், நீலகிரி மாவட்டம் கோத்தகிரி ஆகிய
பகுதிகளில் இருந்து திரையரங்கு உரிமையாளர்கள் கலந்து கொண்டனர். </p>

<p> </p>






</body></text></cesDoc>