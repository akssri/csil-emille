<cesDoc id="tam-w-dinakaran-cinema-02-01-16" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-02-01-16.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-01-16</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-01-16</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சென்னையில் இன்று
நட்சத்திர இரவு: நடிகர் சங்கம் புதிய இணையதளம் தொடக்க விழா, படப்பிடிப்பு ரத்து</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>




 
  
  <p>சென்னையில் இன்று நடக்கும்
  நட்சத்திர இரவு கலை நிகழ்ச்சியில் நடிகர் சங்கம் சார்பில் புதிய இணையதளம்
  தொடங்கப்படுகிறது. அதற்காக சங்கம் சார்பில் அச்சிடப்பட்ட அழைப்பிதழின் எழில்்
  தோற்றத்தை காணலாம். </p>
  
 




<p> </p>

<p>சென்னை, ஜன. 16- சென்னையில் இன்று நடக்கும் நட்சத்திர இரவு
நிகழ்ச்சி விழாவில் நடிகர் சங்கம் புதிய இணைய தளத்தை தொடங்குகிறது. </p>

<p>சினிமா ஒளிப்பதிவாளர்கள் சங்கத்தின் அறக்கட்டளைக்கு நிதி திரட்ட இன்று மாலை
5.30 மணிக்கு சென்னையில் உள்ள நேரு உள்விளையாட்டு அரங்கில் நட்சத்திர இரவு நிகழ்ச்சி
நடக்கிறது. இதில் நடிகர் சங்க தலைவர் விஜயகாந்த் தலைமையில் நடிகர், நடிகைகள்
கலந்துகொள்கின்றனர். </p>

<p>ரித்திக்-ஷாருக் </p>

<p>இந்தி நடிகர்கள் ரித்திக் ரோஷன், ஷாருக்கான், நடிகை இந்தி நடிகை பூஜா பட்ரா,
நடன அமைப்பாளர்கள் பராகான், தெலுங்கு நடிகர்கள் ஜெகபதி பாபு, மோகன்பாபு, மலையாள
நடிகர்கள் சுரேஷ்கோபி, ஜெய ராம், திலீப் ஆகியோரும் கலந்து கொள்கிறார். </p>

<p>இந்த கலை நிகழ்ச்சியை யொட்டி சென்னையில் இன்று அனைத்து படப்பிடிப்புகளும்
ரத்து செய்யப்பட்டுள்ளன. டப்பிங், எடிட்டிங் வேலைகளும் நடக்காது. </p>

<p>நடிகர்-நடிகைகள் </p>

<p>இதேவிழாவில் தென்னிந்திய நடிகர் சங்கத்தின் இணையதளத்தின் தொடக்க விழாவும்
நடக்கிறது. இதில் ரஜினிகாந்த்,கமல்ஹாசன், விஜயகாந்த், சரத்குமார் நெப்போலியன்,
விஜய், அஜீத், பிராசந்த்...... </p>

<p>நடிகைகள் ராதிகா, குஷ்பு, ரேவதி, ரம்பா, சிம்ரன், லைலா... என அனைத்து நடிகர்,
நடிகைகள் பற்றிய முழுவிவரம் அடங்கி இருக்கும். www.tamilfilmstars.com என்ற முகவரியில் இதனை காணலாம். இதற்கான ஏற்பாடுகளை நடிகர் சங்க
உப தலைவர் நெப்போலியன், பி.ஆர்.ஓ. டைமண்ட் பாபு செய்துள்ளனர். </p>

<p> </p>

<p>தி.மலை தியேட்டரில்
கோஷ்டி மோதல்: ரசிகர்கள் பீதியில் ஓட்டம்</p>

<p> </p>

<p>திருவண்ணாமலை,ஜன.16- திருவண்ணாமலை தியேட்டரில் அஜீத் நடித்த
ரெட் படம் பார்க்க வந்த போது 2 கோஷ்டியினர் வீச்சு அரிவாளுடன் கலாட்டா செய்தனர்.
இதனால் பீதியில் ரசிகர்கள் ஓட்டம் பிடித்தனர். </p><p>
</p><p>
திருவண்ணாமலையில் உள்ள ஒருதியேட்டரில் அஜீத் நடித்த ரெட் படம் பொங்கல் விழாவை யொட்டி
நேற்று முன்தினம் திரையிடப்பட்டது. </p><p>
</p><p>
காலை 11 மணி காட்சியில் தியேட்டடரில் 2 கோஷ்டிகளை சேர்ந்தவர்கள் கலாட்டா செய்தனர்.
இதனால் ரசிகர்கள் பீதியில் ஆங்காங்கு ஓடினர். </p><p>
</p><p>
இது பற்றி தியேட்டர் உதவி மேலாளர் திருவண்ணாமலை டவுண் போலீசாருக்கு தகவல் கொடுத்தார்.
</p><p>
</p><p>
வீச்சு அரிவாள் </p><p>
</p><p>
இதைத்தொடர்ந்து போலீசார் தியேட்டருக்கு வந்து சமாதானப்படுத்தினர். </p><p>
</p><p>
இது பற்றி போலீஸ் தரப்பில் கூறப்படுவதாவது:- </p><p>
</p><p>
திருவண்ணாமலை தியாகி அண்ணாமலை தெருவைச் சேர்ந்த பாக்யராஜ், சுமான் மற்றும்
திருவண்ணாமலையை சேர்ந்த ஜீவா, முத்து ஆகியோருக்கும் இடையே முன் விரோதம் காரணமாக வீச்சு
அரிவாளுடன் ஒருவருக் கொருவர் வெட்ட முயற்சி செய்தனர். இது தொடர்பாக பாக்யராஜ் (வயது
23), சுமான் (வயது 20), மற்றொரு கோஷ்டியைச் சேர்ந்த ஜீவா (வயது 20), முத்து (வயது 20)
ஆகியோரை கைது உள்ளதாக கூறப்பட்டுள்ளது. </p><p>
</p><p>
இந்த சம்பவத்தால் தியேட்டரில் பெரும் பரபரப்பு ஏற்பட்டது. </p>

<p> </p>






</body></text></cesDoc>