<cesDoc id="tam-w-dinakaran-cinema-01-12-22" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-01-12-22.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-12-22</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-12-22</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>நீதிபதி விடுமுறையில்
சென்றார்: ரஜினி வழக்கில் 2-ந் தேதி தீர்ப்பு</p>

<p> </p>

<p>சென்னை,டிச.22- நடிகர் ரஜினிகாந்த் திருமண மண்டப அப்பீல்
வழக்கில் சென்னை கோர்ட்டில் அடுத்த மாதம் 2-ந் தேதி தீர்ப்பு கூறப்படுகிறது. நீதிபதி
விடுமுறையில் இருப்பதால் தீர்ப்பு தள்ளிவைக்கப்பட்டு உள்ளது. </p><p>
</p><p>
சென்னை கோடம்பாக்கத் தில் நடிகர் ரஜினிகாந்தின் ராகவேந்திரா திருமண மண்டபம் உள்ளது.
இந்த திருமண மண்டபத்தின் நிலம் தனக்கு சொந்தமானது என்று கூறி நடிகர் ரஜினிகாந்த் மீது
மனோகரன் என்பவர் சென்னை சிட்டி சிவில் கோர்ட்டில் வழக்கு தாக்கல் செய்தார். இந்த
வழக்கை சிட்டி சிவில் கோர்ட்டு விசாரித்து தள்ளுபடி செய்து தீர்ப்பு கூறியது. </p><p>
</p><p>
அப்பீல் </p><p>
</p><p>
இந்த தீர்ப்பை எதிர்த்து மனோகர் சென்னை சிட்டி சிவில் கோர்ட்டில் அப்பீல் செய்தார்.
இந்த அப்பீல் வழக்கில் மனோகரன் சார்பாக வக்கீல் நசுருதீன் , ரஜினிகாந்த் சார்பாக
வக்கீல் வரதாச்சாரியார் ஆகியோர் ஆஜராகி வாதாடினார்கள். </p><p>
</p><p>
தள்ளிவைப்பு </p><p>
</p><p>
இரு தரப்பு வாதத்தையும் கேட்ட 6-வது கூடுதல் சிட்டி சிவில் கோர்ட்டு நீதிபதி மாணிக்கம்
வழக்கின் தீர்ப்பை நேற்று 21-ந் தேதிக்கு தள்ளிவைத்திருந்தார். அதன்படி இந்த வழக்கில்
நேற்று தீர்ப்பு கூறப்படும் என்று கருதி இரு தரப்பு வக்கீல்களும் நேற்று கோர்ட்டில்
ஆஜரானார்கள். அப்போது கோர்ட்டுக்கு நீதிபதி மாணிக்கம் வராததால் இந்த வழக்கின்
தீர்ப்பு 2-ந் தேதிக்கு தள்ளிவைக்கப்படுகிறது என்று அறிவிக்கப்பட்டது. இந்த வழக்கின்
தீர்ப்பு பற்றி மனோகரன் தரப்பு வக்கீல் நசுருதீன் பதில் கூறும் போது நீதிபதி
விடுமுறையில் இருப்பதால் வழக்கின் தீர்ப்பு 2-ந் தேதி தள்ளிவைக்கப்படு வதாக கோர்ட்டு
அலுவலர் தெரிவித்தார் என்று குறிப்பிட்டார். </p>

<p> </p>

<p> </p>

<p> </p>

<p>புதுவையில் நடிகை ரோஜா
கொடும்பாவி எரிப்பு: கமல் ரசிகர்கள் 6 பேர் கைது தியேட்டர் முன் பரபரப்பு</p>

<p> </p>

<p>புதுச்சேரி, டிச.22- புதுவையில் தியேட்டர் முன்பு நடிகை ரோஜாவின்
கொடும் பாவியை எரித்த கமல் ரசிகர்கள் 6 பேரை போலீசார் கைது செய்தனர். </p><p>
</p><p>
நடிகர் கமல் அவருடைய படங்களில் நடிக்கும் நடிகைகளை கேவலமாக சித்தரிப்ப தாக நடிகை ரோஜா
கூறியிருந்தார் அல்லவா? நடிகை ரோஜாவின் இந்த பகீர் குற்றச் சாட்டு கமல்
ரசிகர்களிடையே கடும் கொந்தளிப்பை ஏற்படுத்தியது. இதையடுத்து ரோஜா படங்கள்
திரையிடப்படும் தியேட்டர்களுக்கு முன் போராட்டம் நடத்துவோம் என்று தமிழகம்- புதுவையில்
உள்ள கமல் ரசிகர்கள் எச்சரிக்கை விடுத்துள்ளனர். </p><p>
</p><p>
கொடும்பாவி எரிப்பு </p><p>
</p><p>
இந்த நிலையில் புதுவை முதலியார் பேட்டையில் உள்ள ஒரு தியேட்டரில் நடிகை ரோஜா
கதாநாயகியாக நடித்த வீட்டோட மாப்பிள்ளை என்ற படம் திரையிடப்ப ட்டது. இதையறிந்த கமல்
ரசிகர்கள் முதலியார்பேட்டை கமல் நற்பணி மன்ற தலைவர் அரிகிருஷ்ணன் மற்றும் நற்பணி மன்ற
பொறுப்பாளர்கள் ராமராஜ், பாலா, பாக்கிய ராஜ், செல்வக்குமார், மோகன் ஆகிய 6 பேரும்
தியேட்டர் முன் நடிகை ரோஜாவின் கொடும்பாவியை எரித்து ஆர்ப்பாட்டம் நடத்தினர்.
அப்போது ரோஜாவை எதிர்த்து கோஷ்ங்கள் எழுப்பினர். </p><p>
</p><p>
6 பேர் கைது </p><p>
</p><p>
இது குறித்து தகவல் அறிந்த முதலியார் பேட்டை போலீஸ் சப்- இன்ஸ்பெக்டர் பாஸ்கரதாஸ்,
ஏட்டு சேகர் வழக்குபதிவு செய்து ரோஜா கொடும் பாவியை எரித்த 6 பேரையும் கைது செய்தனர்.
மேலும் அவர்களிடம் தீவிர விசாரணை நடத்தி வருகிறார்கள். </p>

<p> </p>






</body></text></cesDoc>