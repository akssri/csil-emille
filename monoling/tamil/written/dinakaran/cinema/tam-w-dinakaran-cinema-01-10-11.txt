<cesDoc id="tam-w-dinakaran-cinema-01-10-11" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-cinema-01-10-11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-10-11</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-10-11</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>2 வருட இடைவௌிக்கு பிறகு,
ரஜினி புதிய படம் ஷங்கர் இயக்குகிறார், விஜய தசமியில் பூஜை போட திட்டம்</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>




 
  
  <p>ரஜினிகாந்த் </p>
  
 




<p>சென்னை, அக். 11- ரஜினிகாந்த் நடிக்கும் புதிய படத்தை ஷங்கர்
டைரக்டு செய்கிறார். வருகிற விஜயதசமி நாளன்று பட தொடக்க விழா நடத்த திட்டமிடப்பட்டுள்ளது.
</p><p>
</p><p>
படையப்பா படத்தை அடுத்து சூப்பர் ஸ்டார் ரஜினி காந்த் புதிய படம் எதுவும் நடிக்காமல்
இருந்தார். அப்படம் திரைக்கு வந்து இரண்டு வருடங்கள் முடிந்துவிட்டது. </p><p>
</p><p>
ரஜினி புதிய படத்தில் நடிக்க வேண்டும் என்று அவரது ரசிகர்கள் கோரி வந்தனர். அதை
வலியுறுத்தி மதுரை, திருச்சி போன்ற நகரங்களில் ரசிகர்கள் உண்ணாவிரத போராட்டத்திலும்
ஈடுபட்டார்கள். </p><p>
</p><p>
இந்தநிலையில் ரஜினிகாந்த் துணைவியார் சாரல் பாடல் கேசட் விழாவிற்காக வௌியூர் களுக்கு
சென்றபோது அவரிடமும் ரஜினியை புதிய படத்தில் நடிக்கச் சொல்லுங்கள் என்று ரசிகர்கள்
கேட்டுக்கொண்டனர். அதற்கு லதா உங்களது உணர்வுகளை அவரிடம் நான் தெரிவிக்கிறேன் என்று
பதில் அளித்தார். </p><p>
</p><p>
வலுவான கதை </p><p>
</p><p>
இதற்கிடையில் ரஜினிகாந்த் தனது புதிய படத்திற்கான கதைக்கரு மிகவும் வலுவுள்ள தாக
இருக்க வேண்டும் என்று நினைத்தார். அதற்காக பல்வேறு கதைகள் பரிசீலிக்கப் பட்டன. ஆனால்
அதில் எந்த கதையை தேர்வு செய்வது என்பது பற்றி முடிவு செய் யாமல் இருந்தார். </p><p>
</p><p>
கடந்த மாதம் அமெரிக்கா சென்ற ரஜினிகாந்த் அங்கிருந்த படி தொலைபேசியில் இயக்குனர்கள்
ஒரு சிலரிடம் விவாதித்தார். பின்னர் அவர் அங்கிருந்து சென்னை திரும்பி வந்தார். </p><p>
</p><p>
ஷங்கர் </p><p>
</p><p>
தற்போது தனது புதிய படத்தை இயக்கும் இயக்குனர் யார் என்பதை ரஜினி முடிவு
செய்திருக்கிறார். இந்தியன், முதல்வன், ஜீன்ஸ் போன்ற படங்களை இயக்கிய ஷங்கர்
ரஜினியின் புதிய படத்தை டைரக்டு செய்ய உள்ளார். </p><p>
</p><p>
முதல்வன் படத்தின் கதையை ரஜினிக்காகவே ஷங்கர் உருவாக்கினார். ஆனால் அப்போது
ரஜினியால் அந்த படத்தில் நடிக்க முடியாமல் போனது. இதனால் அப்படத்தில் அர்ஜுன்
நடித்தார். திரைக்கு வந்து படம் வெற்றி கரமாக ஓடியது. இதே படத்தை ரத்னம் தயாரிக்க
இந்தியில் நாயக் என்ற பெயரில் ஷங்கர் இயக்கினார். இதில் அனில்கபூர் கதாநாயகனாக
நடித்தார். அப்படம் இந்தியா முழுவதும் வௌியிடப் பட்டது. </p><p>
</p><p>
சந்திப்பு </p><p>
</p><p>
இந்தநிலையில் ரஜினிகாந்த், ஷங்கர் சந்திப்பு சமீபத்தில் சென்னையில் நடந்தது.
அப்போது ரஜினியின் புதிய படத்தை ஷங்கர் இயக்குவது என்று முடிவானது. இப்படத்திற்கான கதையை
ஷங்கர் தயாரித்து விட்டார். அது ரஜினியையும் கவர்ந்து விட்டது. நான்கெழுத்து பிரபல
தயாரிப்பாளர் இப்படத்தை பலகோடி செலவில் தயாரிக்கிறார். </p><p>
</p><p>
விஜயதசமி </p><p>
</p><p>
இப்படத்தின் தொடக்க விழா வருகிற விஜயதசமி நாளன்று சென்னை ஏவி.எம். ஸ்டுடியோவில்
நடத்த திட்டமிடப்பட்டுள்ளது. தொடர்ந்து படப்பிடிப்பு வேலைகளும் நடக்க உள்ளன. அடுத்த
ஆண்டு தமிழ் புத்தாண்டு தினத்தில் ரசிகர்களுக்கு விருந்தாக படம் திரைக்கு வர உள்ளது. </p><p>
</p><p>
ஐஸ்வர்யராய் </p><p>
</p><p>
இதில் ரஜினிகாந்தின் ஜோடியாக ஐஸ்வர்யராய் நடிக்க உள்ளார். ஜீன்ஸ் படத்தில் ஷங்கரின்
இயக்கத்தில் ஏற்கனவே ஐஸ்வர்யராய் நடித்துள்ளார் என்பது குறிப் பிடத்தக்கது. </p><p>
</p><p>
ரஜினியின் புதிய படத்திற்கு பெயர் ஓரிருநாளில் முடிவாகிறது. அடுத்த சில தினங்களில்
படத்தின் அறிவிப்பு வௌிவரும் என்று எதிர்பார்க்கப்படுகிறது. </p>

<p> </p>

<p>100 ஹங்கேரி
கலைஞர்கள் பங்கேற்பு: இளையராஜா சிம்பொனி இசையில் 'திருவாசகம்'
தமிழ்-ஆங்கிலத்தில் தயாரிக்கிறார்</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>




 
  
  <p>இளையராஜா </p>
  
 




<p>சென்னை, அக். 11- சிவபெருமான் புகழ்பாடும் திருவாசகம்
முதன்முறையாக தமிழ், ஆங்கிலத்தில் இசை அமைப்பாளர் இளையராஜா சிம்பொனி இசையில் உருவாக்
குகிறார். அவருடன் 100 ஹங் கேரி நாட்டு கலைஞர்கள் பங்கேற்கின்றனர். </p><p>
</p><p>
சிவபெருமான் புகழ்பாடும் திருவாசகத்தை மாணிக்க வாசகர் எழுதினார். திருவாசகத் துக்கு
உருகார் ஒரு வாசகத்துக்கும் உருகார் என்று கூறுவார்கள். பிரிட்டிஷ் நாட்டை சேர்ந்த
ஜி.யு.போப் என்பவர் தமிழ் மொழி மீது பற்றுகொண்டவர். அவர் திருவாசகத்தை தமிழ்,
ஆங்கிலத்தில் எழுதினார். தான் இறந்த பிறகு தனது கல்லறையில் இங்கே ஒரு தமிழன்
உறங்குகிறான் என்ற வாசகத்தை பொறிக்கும்படி அவர் கூறி இருந்தார். அதன்படி அவரது
கல்லறையில் அந்த வாசகம் பொறிக்கப்பட்டு உள்ளது. இன்றும் அதனை அவரது கல்லறையில் காண
முடியும். </p><p>
</p><p>
சிம்பொனி </p><p>
</p><p>
அவ்வளவு புகழ் வாய்ந்த திருவாசகத்தை இசை அமைப்பாளர் இளையராஜா முதன் முறையாக தமிழ்,
ஆங்கிலத்தில் ஓம் நமச்சிவாய என்ற பெயருடன் சிம்பொனி இசையில் உருவாக்குகிறார்.
இப்பணியில் அவருடன் ஹங்கேரி நாட்டை சேர்ந்த சிம்பொனி இசை கலைஞர்கள் 100
பேர்கள் இணைந்து ஈடுபடுகின்றனர். </p><p>
</p><p>
60 நிமிட நேரம் ஓடும் சி.டி. இசைத் தட்டாக உருவாக்கும் பணிகள் விரைவில் தொடங்க உள்ளன.
இதன் சிறப்பு என்ன வென்றால் முதலில் இந்த இசைத்தட்டில் திருவாசகம் தமிழில் பாடப்படும்.
அதன் பின்னணியில் தமிழ் அர்த்தம் சற்றும் மாறாமல் தமிழில் எப்படி பாடப்படுகிறதோ அதே
ராகத்தில் ஆங்கிலத்தில் பாடல் ஒலிக்கும். இதன்மூலம் திருவாசகத்தின் பெருமையை மேலை
நாட்டாரும் உணர்ந்து கொள்ள முடியும். </p><p>
</p><p>
ஆசி </p><p>
</p><p>
காஞ்சி பெரியவர் ஒருமுறை திருவாசகத்தை இளையராஜா சிம்பொனி வடிவில் தருவார் என்று
ஆசிர்வதித்தார். அது தற்போது நிறைவேறுகிறது என்று இளையராஜாவின் நண்பர் டாக்டர்
அமுதகுமார் தெரிவித்தார். </p><p>
</p><p>
அறக்கட்டளை </p><p>
</p><p>
பாரம்பரிய இசை, கலை மற்றும் கலாச்சார அறக்கட்டளையை என்ற பெயரில் புதிய அறக்கட்டளை
ஒன்றை இளையராஜா நிறுவி இருக்கிறார். அந்த அறக்கட்டளையின் சார்பாக திருவாசக இசைத்தட்டு
உருவா கிறது. </p>

<p> </p>

<p> </p>






</body></text></cesDoc>