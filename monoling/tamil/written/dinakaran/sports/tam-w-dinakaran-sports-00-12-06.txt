<cesDoc id="tam-w-dinakaran-sports-00-12-06" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-00-12-06.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 00-12-06</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>00-12-06</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ஜிம்பாப்வேயுடன் ஒருநாள் கிரிக்கெட்:
கங்குலி அதிரடி சதத்தில் இந்தியாவுக்கு 2-வது வெற்றி</p>

<p> </p>

<p>அகமதாபாத், டிச.6- ஜிம்பாப்வே அணிக்கு எதி ரான 2-வது ஒருநாள்
கிரிக் கெட் போட்டியில் இந்தியா நேற்று மோதியது. முதல் போட் டியில் வெற்றிபெற்றாலும்
இந் திய வீரர்கள் பட்டியலில் ஒரு மாற்றம் செய்யப்பட்டது. வேகப் பந்து வீச்சாளர்
அகர்கர் நீக்கப் பட்டு அவருக்குப்பதிலாக தமி ழக வீரர் ராம் அணியில்
சேர்க்கப்பட்டார். எதிர்அணி யில் எந்த மாற்றமும் செய்யப் படவில்லை. </p><p>
</p><p>
இந்தியா ரன்குவிப்பு </p><p>
</p><p>
டாஸ் வென்ற இந்திய அணி முதலில் பேட்டிங் செய்தது. நம் பிக்கை வீரர் சச்சின் டெண்டுல்
கர் 8 ரன்னில் ஏமாற்றம் அடைந்தாலும், காப்டன் கங் குலி மற்றும் திராவிட் ஆகி யோர்
ரசிகர்களின் நம்பிக் கையை பொய்யாக்க வில்லை. இவர்களின் விளாசலில் இந்திய அணி
நிர்ணயிக்கப்பட்ட 50 ஓவர் முடிவில் 5 விக்கெட் இழந்து 306 ரன் குவித்தது. 152 பந்துகளை
எதிர்கொண்ட கங் குலி 8 பவுண்டரி மற்றும் 6 சிக் சர்களுடன் 144 ரன் எடுத்தார்.
திராவிட் 62 ரன் பெற்றார். </p><p>
</p><p>
ஜிம்பாப்வே தோற்றது </p><p>
</p><p>
இதையடுத்து ஓவருக்கு 6 ரன் னுக்கு மேல் எடுக்க வேண்டிய கட்டாயத்தில் ஜிம்பாப்வே களம்
இறங்கியது. ஆனால் ஆட்டத் தின் தொடக்கம் முதல் ஜிம் பாப்வே வீரர்கள் ஆட்டம் இழந்
தனர். இதனால் நிர்ணயிக்கப் பட்ட ஓவர் முடிவில் ஜிம் பாப்வே 8 விக்கெட் இழப்புக்கு 245
ரன்பெற்றது. எனவே இந் தியா இந்த ஆட்டத்தில் 61 ரன் வித்தியாசத்தில் வெற்றிபெற் றது.
இதன்மூலம் 5 போட்டிகள் கொண்ட ஒருநாள் தொடரில் இந்தியா தற்போது 2-0 என்ற
ஆட்டக்கணக்கில் முன்னணி பெற்றது. அடுத்த ஆட்டம் வரு கிற 8-ந்தேதி ஜோத்பூரில் நடக்க
உள்ளது. அதிரடியாக சதம் அடித்த இந்திய காப்டன் கங்குலிக்கு ஆட்டநாயகன் விருது
வழங்கப்பட்டது. </p><p>
</p><p>
ஸ்கோர் போர்டு </p><p>
</p><p>
இந்தியா-306/5 </p><p>
</p><p>
டெண்டுல்கர் (கா) ஏ.பிளவர் (ப) பிரண்ட்-08 </p><p>
</p><p>
கங்குலி (கா) மெரிலீர் (ப) வில்ஜான்-144 </p><p>
</p><p>
திராவிட் ரன்அவுட் (ஸ்டிரீக்)-62 </p><p>
</p><p>
யுவ்ராஜ்சிங் (கா) ஸ்டிரீக் (ப) மர்பி-17 </p><p>
</p><p>
ஜோஷி (கா) கேம்பல் (ப) ஓலங்கா-22 </p><p>
</p><p>
பதானி அவுட் இல்லை-17 </p><p>
</p><p>
சோதி அவுட் இல்லை-04 </p><p>
</p><p>
உதிரிகள்-32 </p><p>
</p><p>
விக்கெட் சரிவு: 1/22 2/197 3/242 4/255 5/295 </p><p>
</p><p>
பந்துவீச்சு: ஓலங்கா 10-0-59-1 </p><p>
</p><p>
பிரண்ட் 10-1-37-1ஸ்டிரீக் 10-0-63-0மர்பி 8-1-51-1வில்ஜான் 7-0-38-1 </p><p>
</p><p>
மரிலியர் 5-0-44-0 </p><p>
</p><p>
ஜிம்பாப்வே-245/8 </p><p>
</p><p>
கேம்பல் (கா) திராவிட் (ப) டெண்டுல்கர்-32 </p><p>
</p><p>
மரிலியர் (ப) பிரசாத்-02 </p><p>
</p><p>
கேர்லிசில் (ப) ஜாகீர்கான் -01 </p><p>
</p><p>
ஏ.பிளவர் (கா) (ப) ராம்-51 </p><p>
</p><p>
ஜி.பிளவர் (கா) யுவ்ராஜ்சிங் (ப) ஜோஷி-08 </p><p>
</p><p>
விட்டல் (கா) பிரசாத் (ப) ராம்-26 </p><p>
</p><p>
வில்ஜான் (கா) தாஸ் (ப) ஜோஷி-26 </p><p>
</p><p>
ஸ்டிரீக் அவுட் இல்லை-51 </p><p>
</p><p>
பிரண்ட் (கா) ஜாகீர்கான் (ப) ராம்-08 </p><p>
</p><p>
மர்பி அவுட் இல்லை-04 </p><p>
</p><p>
உதிரிகள்-36 </p><p>
</p><p>
விக்கெட் சரிவு: 1/21 2/35 3/55 4/96 5/141 6/147 7/203 8/220 </p><p>
</p><p>
பந்துவீச்சு: ஜாகீர்கான் 6-0-24-1 </p><p>
</p><p>
பிரசாத் 6-0-15-1டெண்டுல்கர் 10-0-53-1 </p><p>
</p><p>
சோதி 8-1-31-0ஜோஷி 8-0-30-2 </p><p>
</p><p>
ராம் 8-0-47-3பதானி 2-0-15-0 </p><p>
</p><p>
யுவ்ராஜ்சிங் 2-0-14-0 </p>

<p> </p>

<p>பிரபாகர் திடீர் பல்டி:
போர்டு நிர்வாகிகளால்தான் சூதாட்டத்தில் ஈடுழுடு்சூடுனு்</p>

<p> </p>

<p>புதுடெல்லி, டிச.6- கிரிக்கெட் சூதாட்டத்தில் சிக் கிய
இந்தியவீரர்கள் மீதான நட வடிக்கை குறித்து கிரிக்கெட் போர்டு நேற்று அறிவிப்பு
வௌியிட்டது. அசாருதீன் மற் றும் அஜய் சர்மா ஆகியோர் களுக்கு ஆயுள்தடை விதிக்கப்
பட்டது. பிரபாகர் மற்றும் அஜய் ஜடேஜா ஆகியோர் 5 ஆண்டு ஆடத்தடை பெற்றனர். பிர பாகர்
ஏற்கனவே சர்வதேச போட்டிகளில் இருந்து ஓய்வு பெற்று விட்டதால் அவருக்கு தற்போது
வழங்கப்பட்ட தடை யால் பெரிய பிரச்சினை எது வும் இல்லை. ஆனால் ஜடேஜா வின் எதிர்கால
கிரிக்கெட் வாழ்க்கை இத்தோடு முடிந்து விட்டதாகவே தோன்றுகிறது. </p><p>
</p><p>
பிரபாகர் பல்டி </p><p>
</p><p>
தண்டனை குறித்த அறி விப்பு வௌியான சில மணி நேரத்தில் புதுடெல்லியில் பிர பாகர்
அதிரடியாக ஒரு பேட்டி அளித்தார். நடவடிக்கை எடுக் கும் வரை சூதாட்டத்தில் ஈடு பட்டதை
மறுத்த பிரபாகர் தற் போது, கிரிக்கெட் போர்டு நிர் வாகி ஒருவருக்காகத்தான் சூதாட்டம்
செய்தேன் என்று அறிவித்தார். போர்டு நிர்வாகி ஒருவர்தான் சூதாட்ட புரோக் கர் பலரை
தனக்கு அறிமுகம் செய்து வைத்ததாகவும் பிரபா கர் திடீர் பல்டி அடித்துள்ளார். </p><p>
</p><p>
பிரபாகரின் இந்த பேட்டி பெரிய அதிர்ச்சியாக இருக் கிறது. இதுதொடர்பாக மேலும் அவர்கூறுகையில்,
னானாசம்பந்தப் பட்ட போர்டு நிர்வாகி பற்றி நான் குறிப்பிட்டு சொல்ல வேண்டும் என்பது
இல்லை. அவருக்கே இது தற்போது தெரிந்திருக்கும். முதலில் கிரிக் கெட் போர்டு அவரை தண்
டிக்க வேண்டும். அதன்பிறகு தான் சூதாட்ட குற்றச்சாட்டை இந்திய அணியை விட்டு அடி யோடு
ஒழிக்க முடியும்ை என் றைார். பிரபாகர்தான் சூதாட்டப் புயலை முதலில் கிளப்பினார்.
கபில்தேவ் மீது பிரபாகர் குற் றம் சாட்டினாலும் கடைசியில் சி.பி.ஐ. அறிக்கையில்
கபில்தேவ் விடுவிக்கப்பட்டார். குற்றம் சாட்டிய பிரபாகர் தற்போது வசமாக தண்டனையில்
மாட்டிக்கொண்டுள்ளது குறிப் பிடத்தக்கது. </p>

<p> </p>

<p>சூதாட்ட நடவடிக்கை
அறிக்கை: அறிவிக்கும் முன்பே அம்பலம்</p>

<p> </p>

<p>சென்னை,
டிச.6- இந்திய கிரிக்கெட் வீரர்கள் மீதான சூதாட்ட குற்றச்சாட் டுக்கு இறுதி நடவடிக்கை
சென்னை கிண்டி ஸ்பிக் வளா கத்தில் நேற்று அறிவிக்கப்பட் டது. மாலை 6 மணிக்கு வீரர் கள்
மீதான நடவடிக்கை அறி விக்கப்படும் என்று முதலில் தெரிவிக்கப்பட்டது. ஆனால் இரவு 7.10
மணிக்குதான் கிரிக் கெட் போர்டு தலைவர் ஏ.சி.முத் தையா நிருபர்களை சந்தித்தார். </p><p>
</p><p>
நிருபர்கள் சந்திப்பு தொடங் குவதற்கு முன்பாகவே கிரிக் கெட் போர்டு எடுத்த நட வடிக்கை
குறித்த விவரங்கள் தௌிவாக சில செய்தி நிறு வனங்களில் வௌி வந்து விட் டது. அதுமட்டுமின்றி,
தொலைக்காட்சிகளும் சிறப்பு செய்திகள் வௌியிடத் தொடங்கி விட்டன. இந்த செய் திகள்
அனைத்தும் ஏ.சி.முத் தையா கூறியதாக செய்திகள் வௌியிட்டன. இந்தத் தகவல் கள்
கிடைத்ததும் அதிகாரபூர்வ செய்திக்காக காத்திருந்த நிரு பர்கள் பெரிய அதிர்ச்சி அடைந்
தனர். </p><p>
</p><p>
புரியாத புதிர் </p><p>
</p><p>
இதுபற்றி பின்னர் ஏ.சி.முத்தையாவிடமும் கேட்கப் பட்டது. ஆனால் அவர் இது பற்றி தனக்கு
எதுவுமே தெரி யாது என்பதுபோல் மழுப்ப லாக பதில் அளித்தார். என்ன தான் வீரர்கள் மீது நட
வடிக்கை எடுக்கப்பட்டாலும், கிரிக்கெட் போர்டின் நட வடிக்கை விவரங்கள் அவ் வளவு
தௌிவாக முன்பே வௌிவந்தது எப்படி என்பது இன்னும் புரியாத புதிராக உள் ளது. </p>

<p>விளையாட்டுத் துளிகள்</p>

<p> </p>

<p>கிரிக்கெட்: கர்நாடகத்திடம் தமிழகம் தோல்வி </p><p>
</p><p>
சென்னை, டிச.6- தென்மண்டல ஒருநாள் கிரிக் கெட் போட்டி சென்னையில் நடந்தது.
தமிழகம்-கர்நாடகம் அணிகள் இந்தப்போட்டியில் மோதின. ஆடுகளம் ஈரப்பத மாக
இருந்ததால் தலா 32 ஓவர் கள் கொண்டதாக மோதல் நடத்தப்பட்டது. இதன்படி முத லில் களம்
இறங்கிய கர்நாடகம் 9 விக்கெட் இழப்புக்கு 171 ரன் குவித்தது. குமரன் 3 விக்கெட்
கைப்பற்றினார். இதன்பின்னர் களம் இறங்கிய தமிழ்நாடு அணி நிர்ணயிக்கப்பட்ட ஓவ ரில் 9
விக்கெட் இழப்புக்கு 138 ரன் எடுத்து தோற்றது. </p><p>
</p><p>
ஸ்கோர்: கர்நாடகம்-171/9 (மிதுன் பீரலா-53, விஜய் பரத் வாஜ்-47, குமரன்-3/24, ஆசிஸ்
கபூர்-2/38, பலாஜிராவ்-2/35), தமிழ்நாடு-138/9 (சரத்-32). </p><p>
</p><p>
</p><p>
</p><p>
காயமடைந்த சந்தர்பால் நீக்கம்: சாமுவேல் தேர்வு </p><p>
</p><p>
லண்டன், டிச.6- ஆஸ்திரேலியாவில் சுற்றுப்பய ணம் செய்துள்ள மேற்கு இந்திய தீவு
கிரிக்கெட் அணி தற்போது வரை நடந்து முடிந்த இரு டெஸ்ட் போட்டிகளிலும் பரி தாபத்தோல்வி
கண்டுள்ளது. முதல் டெஸ்ட் போட்டியில் இடம் பெற்று சிறப்பாக ஆடிய சந்தர்பால் அந்த
ஆட்டத்தின் போது காயமடைந்தார். இத னால் 2-வது மோதலுக்கு தேர்வு பெறவில்லை. இந்தநிலையில்
சந்தர்பாலுக்குப்பதிலாக இளம் வீரர் சாமுவேல் அணியில் சேர்க்கப்பட்டுள்ளார். தொடர்ந்து
6 வாரங்கள் ஓய்வு எடுக்க வேண்டிய கட்டாயம் ஏற் பட்டபோதும் சந்தர்பால் தொடர்ந்து
ஆஸ்திரேலியாவில் இருப்பார் என்று மேற்கு இந்திய தீவு போர்டு அறிவித்துள்ளது. ஒருவேளை
இடைப்பட்ட காலத் தில் குணம் அடைந்தால் அவர் உடனடியாக அணியில் சேர்க் கப்படுவார். </p>

<p> </p>






</body></text></cesDoc>