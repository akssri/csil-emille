<cesDoc id="tam-w-dinakaran-sports-01-09-07" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-01-09-07.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-09-07</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-09-07</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>இந்திய கிரிக்கெட் அணிக்கு
மீண்டும் கங்குலியே காப்டன்</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>




 
  
  <p>கங்குலி </p>
  
 




<p>மும்பை, செப். 7- இந்திய கிரிக்கெட் அணிக்கு கங்குலியே மீண்டும்
காப்டனாக அறிவிக்கப்பட்டு உள்ளார். </p><p>
</p><p>
இந்திய கிரிக்கெட் அணி வருகிற 24-ந்தேதி தென் ஆப்பிரிக்காவுக்கு புறப்படுகிறது. அங்கு
தென்்ஆப்பிரிக்கா அணியுடன் 3 டெஸ்ட் போட்டிகளில் விளையாடுகிறது. </p><p>
</p><p>
பிறகு அங்கே நடைபெற உள்ள 3 நாடுகள் போட்டியில் (டிரையாங்குலர்) இந்திய அணி
பங்கேற்கிறது. இதில் இந்தியா, தென் ஆப்பிரிக்கா, கென்யா ஆகிய அணிகள் மோதுகின்றன.
இப்படி இந்திய அணி 66 நாட்கள் தென் ஆப்பிரிக்காவில் சுற்றுப்பயணம் மேற்கொள்கிறது. </p><p>
</p><p>
காப்டன் யார்? </p><p>
</p><p>
இந்த தென் ஆப்பிரிக்க பயணத்துக்கு இந்திய கிரிக்கெட் அணியின் காப்டன் யார் என்ற
விஷயம் சில நாட்களாக பரபரப்புடன் விவாதிக்கப்பட்டு வந்தது. தற்போது காப்டனாக உள்ள
கங்குலி ஜிம்பாப்வே, இலங்கை சுற்றுப்பயணத்தில் நன்றாக ஆடவில்லை. இந்திய அணியும்
தோற்றது. </p><p>
</p><p>
எனவே கங்குலிக்கு காப்டன் பதவி நிலைக்காது என பேசப்பட்டது. துணை காப்டனாக உள்ள ராகுல்
டிராவிட் காப்டனாக அறிவிக்கப்படலாம் என எதிர்பார்க்கப்பட்டது. </p><p>
</p><p>
கங்குலி அறிவிப்பு </p><p>
</p><p>
இந்த சூழ்நிலையில் இந்திய கிரிக்கெட் தேர்வுக்குழு கூட்டம் நேற்று காலை 11 மணிக்கு
மும்பையில் நடைபெற்றது. தேர்வுக்குழு தலைவர் சந்து போர்டே தலைமை வகித்தார். முடிவு என்ன
என்பதை அறிய நூற்றுக்கணக்கான பத்திரிகை யாளர்களும், டி.வி. காமிராமேன்களும் காத்திருந்தார்கள்.
</p><p>
</p><p>
பகல் 12.50 மணிக்கு வௌியே வந்த சந்து போர்டே, தென் ஆப்பிரிக்க சுற்றுப் பயணம்
மேற்கொள்கிற இந்திய அணிக்கு காப்டனாக சவ்ரவ் கங்குலி தேர்ந்து எடுக்கப்பட்டுள்ளார்
என்று அறிவித்தார். </p><p>
</p><p>
5 நிமிட முடிவு </p><p>
</p><p>
மேலும் சந்து போர்டே கூறியதாவது:- </p><p>
</p><p>
கூட்டம் தொடங்கி 5 நிமிடத்திலேயே கங்குலிதான் மீண்டும் காப்டன் என்பதை முடிவு செய்து
விட்டோம். அதன்பிறகு மற்ற வீரர்கள் இலங்கையில் விளையாடிய தன்மை பற்றி விவாதித்து
கொண்டிருந்தோம். கங்குலியின் லீடர்ஷிப் தகுதி பற்றியும் ஆலோசித்தோம். </p><p>
</p><p>
கங்குலி இதுவரை நன்றாக பணியாற்றி இருக்கிறார். அவர் காப்டனாக இருந்து 11 டெஸ்ட் ஆடியதில்
6 போட்டிகளில் வென்றுள்ளார். இது ஒரு சிறந்த சாதனை. ஒரு நாள் போட்டியில் 4 இறுதி
ஆட்டங்களுக்கு இந்தியாவை இட்டு வந்திருக்கிறார். காப்டன் பதவியை சிறப்பாகவே
செய்துள்ளார். </p><p>
</p><p>
இவ்வாறு அவர் கூறினார். </p><p>
</p><p>
வீரர்கள் </p><p>
</p><p>
தென் ஆப்பிரிக்காவுக்கு செல்லும் இந்திய அணி வீரர்கள் யார், யார் என்ற பட்டியலை
தேர்வுக்குழு இன்று அறிவிக்கிறது. காயம் அடைந்த தெண் டுல்கர், கும்ப்ளே இப்போது முழு தகுதி
பெற்று விட்டனர். எனவே அவர்கள் அணியில் இடம் பெறுவது உறுதியாகிவிட்டது.
வி.வி.எஸ்.லட்சுமண் பூரண குணம் அடைய இன்னும் 3 வாரம் ஆகும். நாத் இன்னும் 1 வாரத்தில் முழு
தகுதி பெறுவார். நெக்ரா தகுதி சர்டிபிகேட் சமர்பித்து விட்டார் என்றும் சந்துபோர்டே
நிருபர்களிடம் தெரிவித்தார். </p>

<p> </p>

<p> </p>

<p>அமெரிக்க ஓப்பன்
டென்னிஸ்: இந்திய வீரர் லியாண்டர் பயஸ் இறுதிப் போட்டிக்கு தகுதி, மிக்சட் டபுள்ஸ்
ஆட்டத்தில்</p>

<p> </p>

<p>நியூயார்க், செப். 7- அமெரிக்க ஓப்பன் டென்னிஸ் மிக்சட்
டபுள்ஸ் ஆட்டத்தின் இறுதிப்போட்டிக்கு இந்திய வீரர் லியாண்டர் பயஸ் ஜோடி தகுதி
பெற்றுள்ளது. </p><p>
</p><p>
லியாண்டர் ஜோடி வெற்றி </p><p>
</p><p>
உலகப்புகழ் பெற்ற அமெரிக்க ஓப்பன் டென்னிஸ் போட்டி நியூயார்க் நகரில் நடை பெற்று
வருகிறது. நேற்று மிக்சட் டபுள்ஸ் பிரிவில் அரைஇறுதிப் போட்டி ஒன்று நடந்தது. </p><p>
</p><p>
அதில் ஒரு ஜோடி இந்திய வீரர் லியாண்டர் பயஸ், அமெரிக்க வீராங்கனை லிசா ரேமண்ட்.
அவர்களை எதிர்த்து மோதிய ஜோடி கிம்பர்லி, டான்்் ஜான்சன். இவர்கள் இருவருமே
அமெரிக்காவை சேர்ந்தவர்கள். </p><p>
</p><p>
இந்த போட்டியில் லியாண்டர்-லிசா ஜோடி அபாரமாக ஆடி 7-6 (7-3), 6-4 செட் கணக்கில்
வென்று இறுதிப்போட்டிக்குள் நுழைந்தது. </p><p>
</p><p>
இறுதிப்போட்டி </p><p>
</p><p>
நேற்று இன்னொரு அரை இறுதிப்போட்டியில் ஆஸ்திரேலிய ஜோடி ஸ்டப்ஸ்-உட் பிரிட்ஜ் வென்றது.
அவர்கள் 6-3, 6-4 செட் கணக்கில் சுஜியாமா (ஜப்பான்)-எலிஸ் வெரைரா (தென்
ஆப்பிரிக்கா) ஜோடியை வீழ்த்தினர். </p><p>
</p><p>
எனவே இறுதிப்போட்டியில் லியாண்டர்-லிசா ஜோடியும், ஸ்டப்ஸ்-உட்பிரிட்ஜ் ஜோடியும்
மோதுகிறார்கள். </p><p>
</p><p>
காட்டமான மோதல் </p><p>
</p><p>
நேற்று நடந்த ஆண்கள் கால் இறுதி போட்டியில் அமெரிக்க வீரர்களும், மிகப்பெரிய ஜாம்ப
வான்களுமான பீட்சாம்ரஸ்- ஆண்ட்ரி அகாசி இருவரும் மோதினார்கள். போட்டி மிக கடுமையாக
இருந்தது. அகாசிக்கு 31 வயது. சாம்ராசுக்கு 30 வயது. 23,000 ரசிகர்கள் திரண்டு இப்போட்டியை
கண்டுகளித்தனர். </p><p>
</p><p>
ஒவ்வொரு கேமும் நீயா, நானா என்று மாறி, மாறி வந்தது. கடைசியாக பீட்சாம் ரஸ் 6-7, 7-6,
7-6, 7-6 செட் கணக்கில் வெற்றி பெற்றார். அவர் அரை இறுதி போட்டிக் குள் நுழைந்தார்.
இவர்களுடைய போராட்டம் 3 மணி 32 நிமிட நேரம் நீடித்தது என்பதிலிருந்தே மோதலின்
வேகத்தை தெரிந்து கொள்ளலாம். இதை டென்னிஸ் வரலாற்றில் ஒரு அற்புதமான போட்டி என்று
வர்ணனை யாளர் தெரிவித்தார். </p><p>
</p><p>
காப்ரியாட்டி </p><p>
</p><p>
பெண்கள் பிரிவில் நேற்று நடந்த கால் இறுதி போட்டி ஒன்றில் அமெரிக்காவின் ஜெனிபர்
காப்ரியாட்டியும், பிரான்சின் அமேலிமவுரேஸ்மா வும் மோதினார்கள். </p><p>
</p><p>
அதில் காப்ரியாட்டி 6-3, 6-4 நேர் செட் கணக்கில் வென்று அரை இறுதிப் போட்டிக்கு தகுதி
பெற்றார். மற்றொரு கால் இறுதிப்போட்டியில் அமெரிக்கா வின் வீனஸ் வில்லியம்ஸ் 6-3,
6-1 செட் கணக்கில் பெல்ஜியத்தின் கிம்கிலிஸ்டர்ரை ஜெயித் தார். </p><p>
</p><p>
சாம்பியன் யார்? </p><p>
</p><p>
இனி நடைபெற இருக்கும் பெண்கள் அரை இறுதிப்போட்டி ஒன்றில் ஜெனிபர் காப்ரியாட்டி-வீனஸ்
வில்லி யம்ஸ் மோதுகிறார்கள். மற் றொரு அரை இறுதிப்போட்டி யில் சுவிஸ் வீராங்கனை
மார்ட்டினா ஹிங்கிஸ்-அமெரிக்காவின் செரினா வில்லியம்ஸ் மோதுவார்கள். </p><p>
</p><p>
இதில் வெற்றி பெறுகிறவர்கள் இறுதிப் போட்டியில் சந்திப்பார்கள். கடந்த ஆண்டின்
அமெரிக்க ஓப்பன் டென்னிஸ் சாம்பியன் வீனஸ் வில்லியம்ஸ் என்பது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p>ஆயுதக்கலைப் போட்டி:
சென்னை கராத்தே வீரருக்கு பரிசு</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>




 
  
  <p>பாலச்சந்தர் </p>
  
 




<p>சென்னை, செப். 7- அமெரிக்காவில் உள்ள அட்லாண்டா நகரில்
ஆயுதக்கலை போட்டி (கொபுடா) நடந்தது. ஜப்பான் கிராண்ட் மாஸ்டர் ஒகினாவா இஷின்ரியே
முன்னிலை வகித்தார். </p><p>
</p><p>
அதில் சென்னை கராத்தே வீரர் சென்சாய் எச்.பாலசந்தர் கலந்து கொண்டார். அவர் 4-வது
பரிசு பெற்றார். அதற்காக அவருக்கு சான்றிதழ் வழங்கப்பட்டது. சர்வதேச அளவில் நடக்கும்
ஆயுதக்கலை போட்டியில் கலந்து கொண்டு பரிசு பெற்ற முதல் இந்தியர் பாலசந்தர்தான். </p>

<p> </p>

<p>90 ரன்னில் பங்களாதேஷ்
சுருண்டது, முத்தையா முரளிதரன் அபார பந்துவீச்சு</p>

<p> </p>

<p>கொழும்பு, செப். 7- இலங்கையில் உள்ள கொழும்பு நகரில் நேற்று
அந்நாட்டு அணிக்கும், பங்களாதேஷ் அணிக்கும் இடையே ஆசிய டெஸ்ட் சாம்பியன்ஷிப் போட்டி
தொடங்கியது. </p><p>
</p><p>
பங்களாதேஷ் முதல் இன்னிங்சை தொடங்கியது. 90 ரன்னில் சுருண்டு ஆல் அவுட் ஆனது. முத்தையா
முரளிதரன் 13 ரன் கொடுத்து 5 விக்கெட் வீழ்த்தினார். </p><p>
</p><p>
பிறகு இலங்கை அணி பந்தடித்தது. நேற்று ஆட்ட நேர இறுதியில் 1 விக்கெட் இழப்புக்கு 246 ரன்
எடுத்துள்ளது. ஜெய சூர்யா 89 ரன்னில் ஆட்டமிழந்தார். அட்டப்பட்டு 99 ரன்னுடனும்,
சங்கக்்்்காரா 49 ரன்னுடனும் அவுட் ஆகாமல் உள்ளனர். </p>

<p> </p>






</body></text></cesDoc>