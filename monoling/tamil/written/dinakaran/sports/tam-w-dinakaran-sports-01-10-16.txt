<cesDoc id="tam-w-dinakaran-sports-01-10-16" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-01-10-16.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-10-16</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-10-16</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>விரைவில் உடல்தகுதி
சோதனை: குணம் அடைந்தார் ஜாகீர்கான்</p>

<p> </p>

<p>மும்பை, அக். 16- காயத்தால் அவதிப்பட்ட வேகப் பந்து வீச்சாளர்
ஜாகீர்கான் குணம் அடைந்து விட்டார். எனவே விரைவில் அவருக்கு உடல்தகுதி சோதனை நடத்தப்பட
உள்ளது. </p><p>
</p><p>
கங்குலி தலைமையிலான இந்திய கிரிக்கெட் அணி தென் ஆப்பிரிக்காவில் சுற்றுப்பயணம் செய்து
ஒருநாள் போட்டியில் மோதிவருகிறது. இந்த பந் தயம் முடிந்ததும் தென்ஆப்பிரிக்காவுடன்
டெஸ்ட் போட்டியில் இந்தியா மோதஉள்ளது. இதற்கான இந்திய அணி வருகிற 21 அல்லது 22-ந்தேதி
தேர்வு செய்யப்படும் என்று இந்திய கிரிக்கெட் போர்டு ஏற்கனவே அறிவித்தது. ஆனால்
தற்போது வீரர்கள் தேர்வு தேதி மாற்றப் படும் என்று எதிர்பார்க்கப்படுகிறது. </p><p>
</p><p>
தேறினார் ஜாகீர்கான் </p><p>
</p><p>
இந்தியாவின் முன்னணி இளம் சுழல்பந்து வீச்சாளர்கள் ஜாகீர்கான் மற்றும் ஆசிஷ் நெக்ரா
ஆகியோர் காயம் காரணமாக அணியில் இருந்து நீக்கப்பட்டனர். தற்போது இந்த வீரர்கள்
இருவரும் காயம் சரியாகி விட்டதாக மருத்துவ சான்றிதழ் கொடுத்துள்ளனர். இதற் காக இன்னும்
சில நாட்களில் ஜாகீர்கான் மற்றும் நெக்ரா ஆகியோர்களிடம் உடல்தகுதி சோதனை நடக்க
உள்ளது. இந்த சோதனை திருப்தி அளிக்கும் பட்சத்தில் வீரர்கள் இருவரும் அணியில்
இடம்பிடிப்பது குறித்து முடிவு செய்யப்படும். </p><p>
</p><p>
உடல்தகுதி சோதனை </p><p>
</p><p>
தென்ஆப்பிரிக்காவுடன் முதல் ஆட்டத்தில் தோல்வி கண்டதும் ஜாகீர்கானை அணி யில்
சேர்க்கும்படி காப்டன் கங் குலி இந்திய கிரிக்கெட் போர்டுக்கு கோரிக்கை விடுத்தார்.
ஆனால் இதுபோன்ற கோரிக்கை எதையும் கங்குலி தெரிவிக்க வில்லை என்று போர்டு கூறிவிட்டது.
கங்குலியின் அபிமானத்தை பெற்ற ஜாகீர்கானுக்கு நிச்சயம் டெஸ்ட் போட்டியில் வாய்ப்பு
கொடுக்கப்படும். அதேசமயம் சமீபத்தில் நடந்த போட்டிகளில் சிறப்பாக பந்துவீசியதால்
நெக்ராவுக்கும் வாய்ப்பு கிடைக்கும் என்று எதிர்பார்க்கப்படுகிறது. ஆனால் இந்த வீரர்கள்
இருவரும் உடல் தகுதி சோதனையில் தேறுவார்களா? என்பதே தற்போது பெரிய கேள்விக்குறியாக
உள்ளது. </p>

<p> </p>

<p>அமெரிக்காவுடன் டென்னிஸ்:
இந்திய அணி பரிதாப தோல்வி</p>

<p> </p>

<p>வின்ஸ்டன் சலேம், அக். 16- டேவிஸ் கோப்பை டென்னிஸ்
போட்டியில் அமெரிக்கா 4-1 என்ற புள்ளியில் இந்தியாவை தோற்கடித்தது. </p><p>
</p><p>
அடுத்த ஆண்டு நடக்க உள்ள டேவிஸ்கோப்பை டென்னிஸ் போட்டிக்கான உலக தகுதி சுற்று பந்தயம்
வடக்கு கலிபோர்னியாவில் நடந்தது. இந்தியா-அமெரிக்கா அணிகள் இந்தப்போட்டியில்
மோதின. முதலில் நடந்த இரு ஒற்றையர் ஆட்டங்களிலும் அமெரிக்கா எளிதாக வெற்றிபெற்றது.
ஆனால் பின்னர் நடந்த இரட்டையர் ஆட்டத்தில் லியாண்டர்- மகேஷ்பூபதி ஜோடி வெற்றி
பெற்றது. இதனால் 1-2 என்ற புள்ளியை எட்டிய இந்திய அணிக்கு லேசான நம்பிக்கை ஏற்பட்டது. </p><p>
</p><p>
லியாண்டர் தோல்வி </p><p>
</p><p>
இதன்படி நேற்று நடந்த மாற்று ஒற்றையர் ஆட்டத்தில் இந்தியாவின் நம்பர்ஒன் வீரர்
லியாண்டர் பயஸ்-ஆன்டி ரோடிக் ஆகியோர் மோதினார்கள். மாற்று ஒற்றையர் ஆட்டங்கள்
இரண்டிலும் வெற்றிபெற்றால் அமெரிக்காவை தோற்கடிக்க முடியும் என்ற நிலை இந்தியாவுக்கு
ஏற்பட்டதால் லியாண்டர் ஆட்டம் ரசிகர்கள் மத்தியில் அதிக எதிர்பார்ப்பை
உண்டாக்கியது. இதன்படி நடந்த மோதலில் லியாண்டர் முதல் செட்டை 6-4 என்ற புள் ளியில்
வென்றார். ஆனால் சுதாரித்து ஆடிய இளம் வீரர் ரோடிக் பந்தய முடிவில் 4-6 6-3 6-2 7-5 என்ற
செட் கணக்கில் லியாண்டரை தோற் கடித்து 3-1 என்ற புள்ளியில் அமெரிக்காவை முன்னணி
பெறச்செய்தார். </p><p>
</p><p>
இந்த வெற்றியால் அமெரிக்கா உலக சுற்றுக்கு தகுதி பெற்றது. இதையடுத்து இறுதி யாக நடந்த
மற்றொரு ஒற்றையர் ஆட்டத்திலும் அமெரிக்கா வெற்றிபெற்றது. இந்தியாவின் இளம் வீரர்
ஹர்ஷ் மன்காட் இந்த மோதலில் களம் இறங்கினார். முடிவில் அமெரிக்கா 4-1 என்ற
புள்ளியில் இந்தியாவை தோற்கடித்தது குறிப் பிடத்தக்கது. </p>

<p> </p>

<p>ஹிங்கிசை முந்தினார்
ஜெனீபர்</p>

<p> </p>

<p>மொனாக்கோ, அக்.16- பெண்கள் டென்னிஸ் அரங்கில் கடந்த சில
ஆண்டுகளாக ஆதிக் கம் செய்த இளம் வீராங்கனை மார்ட்டினா ஹிங்கிஸ். தொடர்ந்து 73
வாரங்கள் நம்பர்ஒன் தகுதியை தக்க வைத்திருந்த ஹிங்கிஸ் கடந்த 2 ஆண்டுகளில் எந்த பந்
தயத்திலும் சாம்பியன் கோப்பை கைப்பற்ற வில்லை. இதனால் அவ ரது நம்பர்ஒன் தகுதிக்கு
ஆபத்து ஏற்பட்டு விட்டது. முதலிடத்தில் இருந்த ஹிங்கிஸ் தற்போது தர வரிசை பட்டியலில்
2-வது இடத்துக்குத் தள்ளப்பட்டார். நடப்பு பிரெஞ்சு ஓபன் சாம்பியன் ஜெனீபர்
கேபிரியாட்டி 4 ஆயிரத்து 867 புள்ளிகளுடன் முதலிடத்தை எட்டியுள்ளார். ஹிங்கிஸ் இவரை விட
25 புள்ளிகள் குறைவாக பெற்று 2-வது இடத்தில் இருக்கிறார். அமெரிக்காவின் தேவன்போர்டு
தொடர்ந்து 3-வது இடத்தை தக்க வைத்துள்ளார். வீனஸ் வில்லியம்ஸ், கிம் கிளிஸ் ஜெர்ஸ்,
மவுரெஸ்மோவா, ஜஸ்டின் ஹெனின், மோனிகா செலஸ், செரீனா வில்லியம்ஸ் மற்றும் ஜெலீனா
டோகிக் ஆகியோர்கள் அடுத்த இடங்களை தக்க வைத்துள்ளனர். முன்னணி வீராங்கனை யான
அரன்ட்சா சாஞ்செஸ் 19-வது இடத்தில் இருந்து ஒருபடி முன்னேறி 18-வது இடத்தை
எட்டியுள்ளது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p>உலக கால்பந்து: 21 நாடுகள்
தகுதி</p>

<p> </p>

<p>டோக்கியோ, அக். 16- உலக கோப்பைக்கான கால்பந்து போட்டி
அடுத்த ஆண்டு மே 31-ந்தேதி முதல் ஜூன் 30-ந் தேதி வரை ஜப்பான் மற்றும் கொரியா
நாடுகளில் நடக்க உள்ளது. உலக கோப்பையில் மொத்தம் 32 அணிகள் மோத உள்ளன. தற்போதுவரை
21 அணிகள் தகுதி செய்யப்பட்டு விட்டன. நடப்பு சாம்பியன் பிரான்ஸ் மற்றும் போட்டியை
நடத்தும் ஜப்பான், கொரியா அணிகள் நேரடியாகத் தேர்வு செய்யப்பட்டன. மற்ற அணிகள் தகுதி
சுற்று அடிப்படையில் தேர்வு செய்யப்பட்டுள்ளன. மற்ற 11 அணிகள் அடுத்தமாத இறுதிக்குள் தகுதி
செய்யப்படும் என்று தெரிகிறது. உலக கோப்பைக்கு முன்னேறிய 21 நாடுகள் பெயர் விவரம்
வருமாறு:- </p><p>
</p><p>
பிரான்ஸ், ஜப்பான், கொரியா, ரஷியா, போர்ச்சுக்கல், டென்மார்க், ஸ்வீடன், போலந்து,
குரேஷியா, ஸ்பெயின், இத்தாலி, இங்கிலாந்து, அர்ஜென்டினா, கோஸ்டாரிகா, அமெ ரிக்கா,
கேமரூன், தென்ஆப்பிரிக்கா, செனகல், துனிஷியா, நைஜீரியா, சீனா. </p>

<p> </p>






</body></text></cesDoc>