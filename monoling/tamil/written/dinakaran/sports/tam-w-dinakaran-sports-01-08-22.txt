<cesDoc id="tam-w-dinakaran-sports-01-08-22" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-01-08-22.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-08-22</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-08-22</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>இலங்கையுடன் இன்று 2-வது
டெஸ்ட்: இந்தியாவுக்கு ஆறுதல் கிடைக்குமா?</p>

<p> </p>

<p></p><p>
</p><p>
கண்டி, ஆக. 22- இலங்கைக்கு எதிரான 2-வது டெஸ்ட் கிரிக்கெட் போட்டியில் இந்திய அணிக்கு
ஆறுதல் கிடைக்குமா? என்ற எதிர்பார்ப்பு ஏற்பட்டுள்ளது. </p><p>
</p><p>
இலங்கை அணிக்கு எதிரான 2-வது டெஸ்ட் கிரிக்கெட் போட்டியில் இன்றுமுதல் இந்தியா
மோதஉள்ளது. கேல் நகரில் நடந்த முதல் டெஸ்ட் போட்டியில் மோசமாக தோல்வி கண்ட
நிலையில் இந்த ஆட்டத்தில் வெற்றிபெற்றால்தான் தொடரை இழக்காமல் மானத்தை
காத்துக்கொள்ள முடியும் என்ற கட்டாயத்துக்கு இந்திய அணி தள்ளப்பட்டுள்ளது. டெண்டுல்கர்,
லட்சுமண் மற்றும் நாத் ஆகிய முன் னணி ஆட்டக்காரர்கள் காயம் காரணமாக அணியில் இடம் பெற
வில்லை என்பதால் மன தளவில் இந்திய வீரர்கள் சோர்வடைந்து காணப்படுகிறார்கள். </p><p>
</p><p>
இளம் வீரர் தடுமாற்றம் </p><p>
</p><p>
கங்குலி, திராவிட் ஆகிய வீரர்கள் மட்டுமே ஓரளவு அனுபவம் பெற்றவர்கள். அதிலும் குறிப்பாக
கங்குலி சமீபகால மாக நடந்த எந்தப்போட்டிகளி லும் சிறப்பாக ஆடவில்லை என்பதால்
அவர்மீதான எதிர் பார்ப்பு வெகுவாக குறைந்து விட்டது. திராவிட் மட்டுமே இந்தியாவின்
மானத்தை காப்பாற்றும் வகையில் 2-வது இன்னிங் சில் அரைசதம் எடுத்தார். மற்ற படி தாஸ்,
ரமேஷ், பதானி உள் பட பல இளம் வீரர்கள் எதிர் பார்ப்பை பொய்யாக்கும் வகையில் ஆடி
வருகிறார்கள். எனவே இந்த வீரர்கள் அத் தனை பேர்மீதும் அதிக எதிர் பார்ப்பு
ஏற்பட்டுள்ளது. </p><p>
</p><p>
பலத்துடன் இலங்கை அணி </p><p>
</p><p>
முதல் டெஸ்ட் வெற்றியால் புதுதெம்புடன் காணப்படும் இலங்கை அணியில் அனைத்து வீரர்கள்
சிறப்பாக ஆடிவருகி றார்கள். யாரையும் குறை சொல்லும் அளவுக்கு இல்லை. பந்துவீச்சை
பொறுத்தவரை வாஸ், முரளிதரன், தில்காரா ஆகியோர் இந்திய பேட்ஸ்மென் களுக்கு பெரிய
சவாலாக இருக் கிறார்கள். பந்துவீச்சில் இந்திய வீரர்கள் யாரையும் குறிப்பிட்டு
சொல்வதற்கு இல்லை. முதல் டெஸ்ட்டில் இந்திய வீரர்கள் செய்த தவறை நிவர்த்தி செய்து
பொறுப்பாக ஆடினால், நிச்ச யம் இலங்கை அணிக்கு பெரிய சவால் கொடுக்க முடியும். </p>

<p> </p>

<p>பா.ஜ. வலியுறுத்தல்:
விளையாட்டுக்கென தனி பல்கலை. அமையுங்கள்</p>

<p> </p>

<p>சென்னை, ஆக. 22- பா.ஜ.க எம்.எல்.ஏ. எச்.ராஜா சட்டசபையில்
பேசியதாவது:- தமிழக அரசு இந்து அறநிலையத்துறையை ஏற் படுத்தி, கோவில்களை தனது
கட்டுப்பாட்டுக்குள் வைத்துள் ளது. இந்து அற நிலையத் துறையை சிறப்பாக பரா மரிப்பதற்காக
இந்து சமய சான்றோர்களை கொண்ட குழு ஒன்று தமிழக அரசு அமைக்க வேண்டும். </p><p>
</p><p>
சென்னையில் உள்ள ஒய்.எம்.சி.ஏ., காரைக்குடியில் உள்ள அழகப்பா உடற்கல்வி கல்லூரி,
கோவை பெரிய நாயக்கன்பாளையத்தில் உள்ள உடற்பயிற்சிக் கல்லூரி ஆகியவை செயல்பட்டுக்
கொண்டிருக்கின்றன. இந்த அரசுக்கு விளையாட்டுத் துறைக்கு அதிக முக்கியத்துவம் கொடுத்து
வருகிறது. விளை யாட்டுக்கென்று தனி பல் கலைக்கழகம் தமிழ்நாட்டில் உருவாக்கினால் மற்ற
மாநிலங்களுக்கு ஒரு முன் மாதிரியாக அமையும். எனவே காரைக்குடியில் உள்ள அழகப்பா உடற்கல்வி
கல்லூரியை தலைமை யிடமாகக் கொண்டு விளை யாட்டுக்கென ஒரு பல்கலைக்கழகத்தை அமைக்க
வேண்டும் என்று கேட்டுக் கொள்கிறேன். </p><p>
</p><p>
இவ்வாறு அவர் பேசினார். </p>

<p> </p>

<p>பெடரேஷன் கால்பந்து:
கால்இறுதிக்கு சர்ச்சில் தகுதி</p>

<p> </p>

<p>சென்னை, ஆக. 22- பெடரேஷன் கால்பந்து போட்டிக்கான கால்இறுதிக்கு
சர்ச்சில் அணி தகுதிபெற்றது. </p><p>
</p><p>
பெடரேஷன் கோப்பைக்கான கால்பந்து போட்டி சென்னையில் நடந்து வருகிறது. இதற்கான நேற்றைய
ஆட்டத்தில் சர்ச்சில்-எஸ்.பி.டி. (திருவனந்த புரம்) அணிகள் மோதின. இரு அணிகளும்
சமபலத்துடன் இருந்ததால் ஆட்டம் தொடக்கம் முதல் விறுவிறுப்பாக இருந்தது. இதனால் முதல்பாதி
ஆட்டம் 0-0 என்ற நிலையில் இருந்தது. இதையடுத்து நடந்த 2-வது பாதி ஆட்டத்தில் இரு அணிகளும்
புதுவியூகம் வகுத்தனர். </p><p>
</p><p>
பிரான்சில் வெற்றிகோல் </p><p>
</p><p>
ஆனால் சர்ச்சில் அணிக்கு அதிர்ஷ்டம் அடித்தது. சர்ச்சில் அணி வீரர் பிரான்சிஸ்
கோயல்கோ சிறப்பாக அடித்த ஒருகோல் மூலம் அந்த அணி வெற்றிபெற்று கால்இறுதிக்கு
முன்னேறியது. அடுத்து நடக் கும் கால்இறுதியில் சர்ச்சில்- மகேந்திரா யுனைடெட் அணி கள்
பலப்பரீட்சை நடத்த உள் ளன. நேற்றைய ஆட்டத்தில் தோல்வி கண்ட எஸ்.பி.டி. அணிக்கு 4
கார்னர் வாய்ப்பு கள் கிடைத்தன. ஆனால் இந்த வாய்ப்புகள் அத்தனையையும் அந்த அணி
வீரர்கள் கோட்டை விட்டது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p>லட்சுமண் அறுவை சிகிச்சை
சக்சஸ்</p>

<p> </p>

<p>சிட்னி, ஆக.22- இந்திய கிரிக்கெட் அணியின் நம்பிக்கை
ஆட்டக்காரர் லட்சுமணனுக்கு கால் மூட்டில் காயம் ஏற்பட்டிருந்தது. இதற்காக ஆஸ்திரேலியா
சென்று அறுவைச்சிகிச்சை செய்து சொண்டார். </p><p>
</p><p>
இந்திய கிரிக்கெட் அணியின் நம்பிக்கை ஆட்டக்காரர் லட்சுமண். ஆஸ்திரேலியாவுக்கு
எதிரான டெஸ்ட் கிரிக்கெட் போட்டியில் இந்தியா தோல்வி நிலையில் இருந்தது. ஆனால் எந்த
நெருக்கடிக்கும் இடம்கொடுக்காமல் சிறப்பாக ஆடி இரட்டை சதம் அடித்த லட்சுமண் இந்திய
அணியையும் வெற்றிபெற வைத்தார். இதனால் லட்சுமண் புகழ் கிரிக்கெட் ரசிகர்கள்
மத்தியில் விரைவாக பரவியது. இந்தநிலையில் லட்சுமணுக்கு கால் மூட்டில் காயம் ஏற்பட்டது.
இதன் காரணமாக இலங்கை அணிக்கு எதிரான டெஸ்ட் போட்டியில் லட்சுமண் ஆட வில்லை. பின்னர்
ஆஸ்திரேலியாவில் அவருக்கு அறுவை சிகிச்சை நடத்தப்பட்டது. இந்தநிலை யில் லட்சுமணுக்கு
நடத்தப்பட்ட அறுவை சிகிச்சை வெற்றிகர மாக முடிந்துள்ளது. ஆனால் இப்போதைக்கு
எந்தப்போட்டியிலும் கலந்து கொள்ளப்போவதில்லை என்று லட்சுமண் அறிவித்துள்ளார். இந்திய
அணி அடுத்து தென்ஆப்பிரிக்கா சுற்றுப் பயணம் செய்ய உள்ளது. இந்த பயணத்தில் லட்சுமண் இடம்
பெறுவாரா? என்பது பெரிய கேள்விக்குறியை எழுப்பி உள்ளது. </p>

<p> </p>

<p>தேசிய ஹாக்கிப்போட்டிக்கான
கால்இறுதியில் இந்தியன் ரெயில்வே-கர்நாடக அணி வீரர்கள் மோதிய காட்சி.</p>

<p> </p>

<p>சென்னை, ஆக. 22- மாநில அளவிலான பள்ளிக் கூட கூடைப்பந்து
போட்டியில் சிவசாமி எம்.சிடி.எம். அணிகள் சாம்பியன் பட்டம் வென்றது. </p><p>
</p><p>
மாநில அளவிலான பள்ளிக் கூட கூடைப்பந்து போட்டி சென்னை புரசைவாக்கத்தில் உள்ள
எம்.சிடி.எம். பள்ளியில் நடந்தது. இதற்கான பெண்கள் இறுதிப்போட்டியில் மயிலாப் பூர்
லேடி சிவசாமி-மேரிஸ் (செங்கல்பட்டு) அணிகள் மோதின. பரபரப்பான ஆட்டத் தின் முடிவில்
சிவசாமி பள்ளி 57-38 என்ற புள்ளிக்கணக்கில் வெற்றிபெற்று சாம்பியன் கோப்பையை தக்க
வைத்தது. வெற்றிபெற்ற அணியில் தீபா 20 புள்ளிகள் குவித்தார். ஆண் கள் பிரிவில்
எம்.சிடி.எம். பள்ளி கடும் போராட்டத்துக்குப் பின்னர் 67-63 புள்ளி வித்தி யாசத்தில்
செங்கல்பட்டு ஜோசப் பள்ளியை தோற்கடித் தது. கடலூர் ஜோசப் பள்ளி 3-வது இடம் பெற்றது. </p>

<p> </p>

<p>தேசிய ஹாக்கி: அரைஇறுதிக்கு
ஏர்இந்தியா தகுதி, டை-பிரேக்கரில் தமிழக அணி தோல்வி</p>

<p> </p>

<p>சென்னை, ஆக. 22- தேசிய ஹாக்கிப்போட்டிக்கான கால்இறுதியில்
தமிழக அணி அதிர்ச்சி தோல்வி அடைந்தது. வெற்றிபெற்ற ஏர்இந்தியா அரைஇறுதிக்கு
முன்னேறியது. </p><p>
</p><p>
முருகப்பா கோப்பைக்கான தேசிய ஹாக்கிப்போட்டி சென்னை எழும்பூர் மேயர் ராதாகிருஷ்ணன்
மைதானத் தில் நடந்து வருகிறது. நேற்று முதல் கால்இறுதி மோதல்கள் நடந்தன. மாலை நடந்த
2-வது கால்இறுதி ஆட்டத்தில் தமிழ் நாடு-ஏர்இந்தியா அணிகள் மோதின. கடந்தமுறை இறுதிப்
போட்டிக்கு முன்னேறிய தமிழ் நாடு இந்தமுறையும் நிச்சயம் வெற்றிபெறும் என்று எதிர்
பார்க்கப்பட்டது. </p><p>
</p><p>
சமநிலை </p><p>
</p><p>
ஆனால் ஆட்டம் தொடங்கிய சிறிது நேரத்திலேயே தமிழக வீரர்கள் ஏர்இந்தியாவிடம்
தடுமாறியது பெரிய வேதனை உண்டாக்கியது. கெவின் பெரைரா, அனில் ஆல்டிரின், ராஜேஷ் சவுகான்
உள்பட பல இந்திய வீரர்களை கொண்ட ஏர்இந்தியா ஆட்டத்தின் 13-வது நிமிடத்தில் முதல்
கோல் போட் டது. ஏர்இந்தியா வீரர் தேவிந்தர் குமார் கடத்தி வந்த பந்தை ராஜேஷ்
சவுகான் சிறப்பாக கோலாக்கினார். இதனால் அந்த அணி 1-0 என்ற கோல் கணக்கில் முன்னணி
பெற்றது. இதற்கு பதில் கோல் போட தமி ழக ஆட்டக்காரர்கள் பலமுறை முயன்றனர். ஆனால் முதல்
பாதி ஆட்டத்தின் கடைசி நிமிடத்தில் தமிழக வீரர் வினோத் போட்ட கோல்மூலம் 1-1 என்ற
சமநிலை கிடைத்தது. </p><p>
</p><p>
ஏர்இந்தியா வெற்றி </p><p>
</p><p>
இதையடுத்து நடந்த 2-வது பாதி ஆட்டத்தின் முடிவில் இரு அணிகளும் கோல் போட வில்லை. எனவே
நிர்ணயிக்கப் பட்ட ஆட்டம் 1-1 என்ற சம நிலை அடைந்தது. இதையடுத்து நடந்த 15 நிமிட
அதிகப்படியான ஆட்டத்திலும் ஆட்டம் டிரா வில் இருந்தது. எனவே வெற் றியை நிர்ணயிக்கும்
டை-பிரேக்கர் ஆட்டம் கடைபிடிக்கப்பட்டது. டை-பிரேக்கர் ஆட்டத்தில் ஏர்இந்தியா 5
கோல்களையும் தவறாமல் போட்டது. ஆனால் தமிழக அணியில் கவுதம் தனக்கு கிடைத்த
வாய்ப்பில் கோல் போட தவறிவிட்டார். இதனால் ஏர்இந்தியா அணி 6-5 என்ற கோல்
கணக்கில் வெற்றி பெற்று அரைஇறுதிக்கு முன்னேறியது. </p><p>
</p><p>
கால்இறுதியில் ரெயில்வே </p><p>
</p><p>
முன்னதாக நடந்த மற்றொரு கால்இறுதியில் இந்தியன் ரெயில்வே அணி 6-2 என்ற கோல்
கணக்கில் கர்நாடகத்தை தோற்கடித்து அரைஇறுதிக்கு தகுதி பெற்றது. ரெயில்வே அணியில் சந்தர்பால்
அடுத் தடுத்து 3 கோல்கள் போட்டு ஹாட்ரிக் சாதனை செய்தார். முக்தியால் சிங்,
ஜம்சீர்கான் மற்றும் இக்பால்சிங் ஆகியோர் தலா ஒருகோல் போட்டனர். கர் நாடக அணியில்
அனுப் அந்தோணி 2 கோல்கள் அடித்தார். காலையில் நடந்த கடைசி லீக் மோதலில் தமிழ்நாடு
கோல்ட்ஸ் அணி 2-3 என்ற கோல் கணக்கில் இந்தியன் ஏர் லைன்ஸ் அணியிடம் தோல்வி கண்டு
கால்இறுதி வாய்ப்பை இழந்தது. ஐ.எச்.எப்.-இந்தியன் ஏர்லைன்ஸ் மற்றும் பஞ்சாப்
சிந்து வங்கி-பாரத்பெட்ரோலிய அணிகள் இன்று நடக்கும் கால் இறுதியில் மோதஉள்ளன. </p>

<p> </p>






</body></text></cesDoc>