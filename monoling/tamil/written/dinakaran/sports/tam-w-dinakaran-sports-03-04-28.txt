<cesDoc id="tam-w-dinakaran-sports-03-04-28" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-03-04-28.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-04-28</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-04-28</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>பாகிஸ்தான் போர்டு
அதிகாரி நம்பிக்கை: இந்தியாவுடன் மீண்டும் கிரிக்கெட் உறவு </p>

<p> </p>

<p>கராச்சி, ஏப். 28- இந்தியாவுடன் மீண்டும் கிரிக்கெட் உறவு
ஏற்படும் என்று பாகிஸ்தான் போர்டு அதிகாரி நம்பிக்கை தெரிவித்துள்ளார். </p><p>
</p><p>
இந்தியா-பாகிஸ்தான் நாடுகளிடையேயான அரசில் உறவில் பெரிய ஓட்டை விழுந்துள்ளது. கார்கில்
சண்டைக்குப் பின்னர் இந்த விரிசலின் தூரம் அதிகமாகி விட்டது. அரசியல் ரீதியான இந்த
பிரச் சினை கிரிக்கெட் உறவையும் பாதித்தது. கார்கில் சண்டைக்குப்பின்னர் பாகிஸ்தான்
அணிக்கு எதிரான அனைத்து கிரிக்கெட் போட்டிகளையும் இந்தியா ரத்து செய்தது. நல்ல வேளையாக
திட்டமிட்டபடி உலக கோப்பையில் இந்தியா- பாகிஸ்தான் அணிகள் மோதின. </p><p>
</p><p>
பாகிஸ்தான் சுற்றுப்பயணம் மேற்கொள்வது தொடர்பாக இந்தியா யோசித்து வந்த போது மேலும்
ஒரு அசம்பாவிதம் நடந்தது. இந்தியா பாராளுமன்றத்தில் மீது தீவிர வாதிகள் தாக்குதல்
நடத்தினர். இந்த தாக்குதலுக்கு பாகிஸ்தான் ஆதரவு தீவிரவாதிகள் தான் காரணம் என்று
மத்திய அரசு குற்றம் சாட்டியது. ஆனால் பாகிஸ்தான் இதை ஏற்க மறுத்து விட்டது. இதையடுத்து
நூல் இழையில் ஆடிக் கொண்டிருந்த கிரிக்கெட் உறவு முற்றிலுமாக அறுந்து விழுந்தது. </p><p>
</p><p>
இந்தநிலையில் ஆசிய கிரிக்கெட் கவுன்சில் கூட்டம் துபாயில் அடுத்தவாரம் நடக்க உள் ளது.
இந்திய கிரிக்கெட் போர்டு தலைவர் ஜக்மோகன்டால் மியா மற்றும் பாகிஸ்தான் கிரிக்கெட்
போர்டு அதிகாரி தாகீர்ஜியா ஆகியோர் இந்த கூட்டத்தில் கிரிக்கெட் உறவுகள் குறித்து
விவாதிக்க உள் ளனர். இந்த பேச்சுவார்த்தை சுமூகமாக நடக்கும் என்று பாகிஸ்தான் அதிகாரி
தாகீர் ஜியா கூறினார். இதுதொடர்பாக அவர் கூறும்போது, சமீபத்தில் இந்திய பிரதமர்
வாஜ்பாய் பாகிஸ்தானுடன் பேச்சு வார்த்தை நடத்தத்தயார் என்று கூறினார். அரசியல் ரீதியாக
முன்னேற்றம் ஏற்பட உள்ள இந்த சூழ்நிலையில் கிரிக்கெட் உறவை புதுப்பித்துக் கொள்வது
சரியாக இருக்கும். விளையாட்டையும், அரசிய லையும் ஒருபோதும் கலக்கக் கூடாது என்றார். </p><p>
</p><p>
டாக்காவில்நடக்க உள்ள ஆசிய கிரிக்கெட் தொடரில் இருந்து பாகிஸ்தான் அணி விலகியது.
இந்தப்போட்டியில் கலந்து கொள்வது தொடர்பாக தாகீர்ஜியாவிடம் கேட்டபோது, முதலில்
திட்டமிட்ட படி இந்திய அணி பாகிஸ்தான் சுற்றுப்பயணம் வரவேண்டும். இதற்கு உறுதி அளிக்காத
வரை ஆசிய கிரிக்கெட் போட்டியில் பாகிஸ்தான் கலந்து கொள்ளாது. நாங்கள் முடிந்த வரை
வளைந்து கொடுத்து விட் டோம். இதற்கு மேலும் வளைந்து கொடுக்க முடியாது என்றார். </p>

<p> </p>

<p> </p>

<p>வங்கதேசத்துடன் டெஸ்ட்
கிரிக்கெட்: தென்ஆப்பிரிக்கா எளிதான வெற்றி </p>

<p> </p>

<p>டாக்கா, ஏப். 28- வங்கதேசத்துடனான டெஸ்ட் கிரிக்கெட்
போட்டியில் தென்ஆப்பிரிக்கா எளிதான முறையில் வெற்றி கண்டது. </p><p>
</p><p>
டெஸ்ட் அந்தஸ்தை பெற்றிருக்கும் வங்கதேசம் வலுவான தென்ஆப்பிரிக்காவை எதிர்த்து
மோதியது. முதல் இன்னிங்சில் வங்கதேசம் 173 ரன்னில் சுருண்டது. இதை யடுத்து களம் இறங்கிய
தென் ஆப்பிரிக்கா எந்தவித பதட்டமும் இன்றி ரன்களை விளாசியது. ருடோல்ப் மற்றும்
டிப்பினர் ஆகியோர் நிலைத்து ஆடி தென்ஆப்பிரிக்காவின் ஸ்கோரை 470 ஆக உயர்த்தினர். 2
விக்கெட்டுகளை மட் டுமே இழந்த நிலையில் தென் ஆப்பிரிக்கா முதல் இன்னிங்சை டிக்ளேர்
செய்தது. </p><p>
</p><p>
297 ரன் முன்னணி பெற்ற நிலையில் தென்ஆப்பிரிக்கா அடுத்து பந்துவீச்சில் கவனம் செய்தது.
விக்கெட்டுகளை காப் பாற்றிக்கொள்ள முயன்ற வங்கதேச அணிக்கு சுழல்பந்து வீச்சாளர்
பால்ஆடம்ஸ் அதிக சோதனை கொடுத்தார். 69 ரன் விட்டுக்கொடுத்து இவர் 5 விக்கெட்டுகள்கைப்பற்றினார்.
இதனால் வங்கதேசம் 237 ரன்னில் சுருண்டது. இதையடுத்து தென் ஆப்பிரிக்கா இன்னிங்ஸ் மற்றும்
60 ரன்னில் வெற்றிபெற்றது. ஆடம்ஸ் முதல் இன்னிங்சில் 37 ரன் விட்டுக்கொடுத்து 5
விக்கெட்டுகள் சாய்த்தார். </p>

<p> </p>

<p> </p>

<p>தேசிய லீக் கால்பந்து:
இந்தியன் வங்கி - ஜே.சி.டி. சென்னையில் இன்று மோதல் </p>

<p> </p>

<p>சென்னை, ஏப். 28- சென்னையில் நடந்து வரும் தேசிய லீக் கால்பந்து
போட்டிக்கான இன்றைய மோதலில் இந்தியன் வங்கி - ஜே.சி.டி. அணிகள் மோதுகின்றன. </p><p>
</p><p>
தேசிய லீக் கால்பந்து போட்டிக்கான ஒரு ஆட்டம் சென்னையில் இன்று நடக்கிறது.
இந்தியன்வங்கி-ஜே.சி.டி. அணிகள் இன்றைய போட்டியில் பலப்பரீட்சை நடத்த
காத்திருக்கின்றன. முன்னாள் சாம்பியன் அணியான ஜே.சி.டி. இன்றைய போட்டியில்
வெற்றிபெற்றால் தரவரிசையில் 4-வது இடத்தை எட்டிப்பிடிக்கும். தற்போது வரை அந்த அணி 21
போட்டிகளில் 10 வெற்றிகளுடன் 35 புள்ளிகள் குவித்துள்ளது. ஆனால் இந்தியன்வங்கி அணி 6
வெற்றிகள் மட்டுமே பெற்று 20 புள்ளிகள் பெற்றிருக்கிறது. </p><p>
</p><p>
விஜயன், ஜோபால் அஞ்சாரி போன்ற முன்னணி வீரர்களுடன் களம் இறங்க உள்ள ஜே.சி.டி.
அணிக்கு இன்றைய போட்டியில் வெற்றிவாய்ப்பு அதிகமாக உள்ளது. இருந் தாலும்
தடுப்பாட்டத்தில் கவனம் செலுத்தப்போகும் இந்தியன் வங்கியிடம் எதிர்அணி ஜொலிக்குமா?
என்பதை பொறுத்திருந்துதான் பார்க்க வேண்டும். இன்றைய ஆட்டம் மாலை 4 மணிக்கு தொடங்க
உள்ளது. பெஞ்சமின் டிசில்வா, பாலு, ஜெயந்திர நக் வேங்கர், அனில்குமார் ஆகியோர்
நடுவர்களாக தேர்வு செய்யப்பட்டு இருக்கிறார்கள். </p>

<p> </p>

<p> </p>

<p>அகாசி புதிய சாதனை </p>

<p> </p>

<p>சர்வதேச டென்னிஸ் சரித்திரத்தில் அமெரிக்காவின் அகாசி புதிய
சாதனை படைத்தார். </p><p>
</p><p>
ஹவுஸ்டன் நகரில் நடந்துவரும் டென்னிஸ் போட்டியில் அகாசி இறுதி மோதலுக்கு முன்னேறினார்.
அரைஇறுதியில் ஆஸ்திரியாவின் மெல்சரை 6-4 6-1 என்ற செட்கணக்கில் அகாசி தோற்
கடித்தார். இந்த வெற்றியால் அகாசி மீண்டும் நம்பர்ஒன் தகுதியை எட்டினார். அகாசிக்கு
தற்போது 33 வயது ஆகும். அதிக வயதில் நம்பர்ஒன் தகுதியை எட்டிய முதல் வீரர் என்ற சாதனையை
அகாசி படைத் துள்ளார். முன்னதாக ஜிம்மி கானர் தனது 30-வது வயதில் நம்பர்ஒன் இடத்தை
எட்டி சாதனை படைத்திருந்தார். அகாசியின் இந்த சாதனையில் முதலிடத்தில் இருந்த
ஆஸ்திரேலிய வீரர் ஹெவிட் 2-வது இடத்துக்குத் தள்ளப்பட்டுள்ளது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p> </p>

<p>டெஸ்ட்: இலங்கை அணி
தப்பியது </p>

<p> </p>

<p>நியூசிலாந்துக்கு எதிரான டெஸ்ட் கிரிக்கெட் போட்டியில் இலங்கை
அணி தப்பியது. </p><p>
</p><p>
நியூசிலாந்து கிரிக்கெட் அணி இலங்கையில் சுற்றுப்பயணம் செய்து டெஸ்ட் போட்டியில்
ஆடிவருகிறது. முதல் இன்னிங்சில் நியூசிலாந்து 7 விக்கெட் இழந்து 515 ரன் குவித்தது.
பின்னர் இலங்கை அணியை பேட்டிங் செய்ய அழைத்த நியூசிலாந்து தொடக்கத்திலேயே ஒரு
விக்கெட்டை கைப்பற்றியது. சொந்த மண்ணில் இலங்கை அணிக்கு ஏற்பட்ட சோதனையை முன்னாள்
காப்டன் ஜெயசூர்யா, சங்கக்கரா ஆகியோர் தீர்த்து வைத்தனர். சார்ஜா கிரிக்கெட்
முடிந்ததும் காப்டன் பதவியில் இருந்து விலகிய ஜெயசூர்யா 50 ரன் எடுத்தார். 3-வது
விக்கெட்டுக்கு ஜெயசூர்யா மற்றும் சங்கக்கரா ஜோடி 103 ரன் விளாசியது. சங்கக்கரா 67 ரன்
எடுத்தார். 3-வது நாள்முடிவில் மழை குறுக்கிட்டு ஆட்டத்தை சீக்கிரமே முடித்து வைத்தது.
அப்போது இலங்கை 4 விக்கெட் இழந்து 267 ரன் எடுத்திருந்தது. காப்டன் திலகரத்னே (71 ரன்)
மற்றும் ஜெயவர்த்தனே (58 ரன்) ஆகியோர் ஆட்டம் இழக்காமல் களத்தில் இருந்தனர். </p>

<p> </p>

<p> </p>

<p>விளையாட்டு துளிகள் </p>

<p> </p>

<p>விளையாட்டு துளிகள் </p><p>
</p><p>
பில்லியார்ட்ஸ் வீரருக்கு ஜனாதிபதி கலாம் வாழ்த்து தெரிவித்தார். </p><p>
</p><p>
சர்வதேச அளவில் இந்திய பில்லியார்ட்சுக்கு பெருமை சேர்த்தவர் வில்சன் ஜோன்ஸ்.
சர்வதேச போட்டிகள் பலவற்றிலும் பட்டம் வென்றிருக்கும் ஜோன்ஸ் கடந்த சில நாட்களுக்கு
முன்பு உடல் நலக்குறைவு காரணமாக மருத்துவமனையில் அனுமதிக்கப்பட்டார். விரைவில் குணம்
அடைய வேண்டும் என்று வாழ்த்தி ஜனா திபதி அப்துல்கலாம் நேற்று வில்சனுக்கு செய்தி
அனுப்பினார். தனது மருத்துவ நிதியில் இருந்து ரூ.10 ஆயிரம் வழங்கவும் கலாம் உத்தரவு
பிறப்பித்தார். இந்தியாவின் முன்னணி வீரர்களான அசோக் சாண்டில்யா, தேவேந்திரஜோஷி
ஆகியோர்களை வில்சன் உருவாக்கினார் என்பது குறிப்பிடத்தக்கது. </p><p>
</p><p>
விறுவிறுப்பான நிலையில் தமிழ்நாடு-டெல்லி கிரிக்கெட் </p><p>
</p><p>
ரஞ்சி கிரிக்கெட் போட்டிக்கான அரைஇறுதியில் தமிழ்நாடு- டெல்லி அணிகள் மோதி
வருகின்றன. முதலில் பேட்டிங் செய்த தமிழ்நாடு அணி 327 ரன் எடுத்தது. பின்னர் களம்
இறங்கிய டெல்லி அணிக்கு தொடக்கத்திலேயே சோதனை ஏற்பட்டது. நம்பிக்கை நாயகன் ஷேவாக்
ரன் கணக்கை தொடங்குவதற்குள் ஆட்டம் இழந்தனர். இந்த பாதிப்பை டெல்லி அணி நன்றாக
உணர்ந்தது. ஆனாலும் தட்டுத்தடுமாறி டெல்லிஅணி முதல் இன்னிங்சில் 256 ரன் எடுத்தது. இதனால்
தமிழ்நாடு அணிக்கு முதல் இன்னிங்சில் 71 ரன் முன்னணி கிடைத்தது. புத்துணர்ச்சியுடன் 2-வது
இன்னிங்சை ஆடவந்த தமிழ்நாடு அணிக்கு உச்சக்கட்ட சோதனை ஏற்பட்டது. முதல் 3
விக்கெட்டுகளை தமிழ்நாடு 43 ரன்னுக்குள் கோட்டைவிட்டது. டெல்லி வீரர்கள் அபாரமாக பந்து
வீசினர். 3-வது நாள் முடிவில்தமிழ்நாடு 4 விக்கெட் இழந்து 88 ரன் பெற்றது. இன்னும் 2 நாட்கள்
எஞ்சி உள்ள நிலையில் தமிழ்நாடு 159 ரன் முன்னணி பெற்றிருக்கிறது. ரமேஷ் மற்றும்
சரத் ஆகியோர் ஆட்டம் இழக்காமல் களத்தில் உள்ளனர். இவர்கள் கைகொடுத்தால் மட்டுமே
தமிழகத்தின் சோதனை விலகும். இல்லையென்றால் டெல்லி அணி 2-வது இன்னிங்சில் சாதிக்க
எளிதான வாய்ப்பு ஏற்பட்டு விடும். ரமேஷ் 20 ரன்களும், சரத் 5 ரன்களும் எடுத்து ஆட்டம்
இழக்காமல் இருக்கிறார்கள். இன்னொரு போட்டியில் மும்பை-பரோடா அணிகள் மோதின. இதில்
மும்பை அணி எளிதாக வெற்றிபெற்று இறுதிவாய்ப்பை எட்டியது. மும்பை அணியை இறுதிப்போட்டியில்
எதிர்கொள்ளப் போவது தமிழ்நாடா? இல்லை டெல்லியா? என்பது அதிக பரபரப்பை
உண்டாக்கி இருக்கிறது. </p>

<p> </p>






</body></text></cesDoc>