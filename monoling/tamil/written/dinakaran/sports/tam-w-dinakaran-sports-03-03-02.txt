<cesDoc id="tam-w-dinakaran-sports-03-03-02" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-03-03-02.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-03-02</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-03-02</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>டெண்டுல்கரின் அபார ஆட்டம்
மூலம், பாகிஸ்தானை விரட்டி அடித்தது இந்தியா </p>

<p> </p>

<p>செஞ்சுரியன், மார்ச் 2- டெண்டுல்கரின் அபார ஆட்டத்தின்
மூலம் பாகிஸ்தான் அணியை இந்தியா விரட்டி அடித்தது. </p><p>
</p><p>
1000 வால்ட் மின்சாரத்தையும் மிஞ்சும் உச்சக்கட்ட டென்ஷனுடன் இந்தியா-பாகிஸ்
தான் கிரிக்கெட் மோதல் செஞ்சுரியன் மைதானத்தில் தொடங்கியது. கிரிக்கெட்
போட்டியில் மட்டும் இருக்கும் தேசிய உணர்வுக்கு நேற்றைய போட்டியில் கொஞ்சமும்
பஞ்சம் இல்லை. கார்கில் சண்டைக்குப் பின்னர் பாகிஸ்தான் அணிக்கு எதிரான எந்தப்போட்டியிலும்
இந்தியா ஆடவில்லை. இதனால் கடந்த 3 ஆண்டுகளாக இரு அணிகளும் நேருக்கு நேர் சந்திக்கும்
வாய்ப்பு கிடைக்க வில்லை. உலக கோப்பை போட்டி அந்த வாய்ப்பை பெற்றுக்கொடுத்தது. </p><p>
</p><p>
பதட்டமான இந்தப்போட்டிக்காக செஞ்சுரியன் ஆடு களத்தில் கூடுதல் போலீஸ் பாதுகாப்பு
போடப்பட்டு இருந்தது. இரு நாடுகளில் இருந்தும் ஏராளமான ரசிகர்கள் போட்டியை நேரில் காண
வந்திருந்தனர். டாஸ் வென்ற பாகிஸ்தான் காப்டன் வாக்கர்யூனுஸ் தனது அணியை முதலில்
பேட்டிங் செய்ய வைத்தார். பாகிஸ் தான் அணியில் சுழல்பந்து வீச்சாளர் சாகுலைன்
நீக்கப்பட்டார். அதுபோல் இந்திய அணியில் ஹர்பஜனுக்குப்பதி லாக கும்ளே வாய்ப்பு
பெற்றார். </p><p>
</p><p>
இதுவரை நடந்த 4 போட்டிகளிலும் சொற்ப ரன்களில் பெவிலியன் திரும்பிய அன்வர் பாகிஸ்தான்
அணிக்கு சிறப்பான தொடக்கத்தை ஏற்படுத்தி கொடுத்தார். 126 பந்துகளில் அன்வர் 101 ரன்
எடுத்தார். அதிர்ச்சி அளிப்பார் என்று எதிர்பார்க்கப்பட்ட ஜாகீர்கான் இந்திய ரசிகர்
களை ஏமாற்றத்தில் ஆழ்த்தினார். முதல் ஓவரில் 7 உதிரி ரன்களை விட்டுக்கொடுத்த
ஜாகீர்கான் ரசிகர்களைவிட அதிக பதட்டத்தில் இருந்ததை அது தௌிவாக காட்டியது. இதனால்
காப்டன் கங்குலி தொடக்கத்திலேயே பந்து வீச்சை மாற்ற வேண்டிய கட்டாயத்துக்குத்
தள்ளப்பட்டார். </p><p>
</p><p>
இங்கிலாந்துடன் 6 விக்கெட்டுகள் கைப்பற்றிய நெக்ரா நேற்றைய ஆட்டத்தில் 73 ரன்களை
விட்டுக்கொடுத்து 2 விக்கெட்டுகள் சாய்த்தார். கடைசி நேரத்தில் வந்த ஜாகீர்கான்
சிறப்பாக பந்துவீசி பாகஸ்தான் அணிக்கு நெருக்கடி கொடுத்தார். ஆனாலும் கடைசி 5 ஓவர்களில்
பாகிஸ்தான் வீரர்கள் 47 ரன்கள் விளாசினர். இதனால் பாகிஸ்தான் நிர்ணயிக்கப்பட்ட 50
ஓவரில் 7 விக்கெட் இழந்து 273 ரன் பெற்றது. விக்கெட் கீப்பர் லத்தீப் பேட்டிங் செய்த
தலையில் காயம் ஏற்பட்டது. இத னால் அவர் போட்டிக்குப் பின்னர் மருத்துவமனையில் சிகிச்சை
பெற்றார். </p><p>
</p><p>
கடினமான இலக்குடன் களம் இறங்கிய இந்தியா வெற்றி இலக்கை எட்டுமா? என்ற குழப்பம்
தொடக்கத்தில் அதிகமாக இருந்தது. டெண்டுல்கர்-ஷேவாக் ஜோடி எந்த வித பதட்டமும் இன்றி
பாகிஸ் தான் பந்துவீச்சில் ரன் குவித்தனர். 5 ஓவரில் 53 ரன் குவித்த நிலையில் ஷேவாக்
தனது விக் கெட்டை பறிகொடுத்தார். அடுத்து ஆடவந்த காப்டன் கங்குலி கோல்டன் டக் ஆனார்.
அதாவது சந்தித்த முதல் பந் திலேயே கங்குலி பெவிலியன் திரும்பினார். கங்குலி இது பெரிய
விஷயம் இல்லை என்றாலும், இந்தியஅணிக்கு நெருக்கடி அதிகமானது. </p><p>
</p><p>
திருவிழா என்பது குறிப்பிட்ட சில காலங்களில்தான் வரும். இந்தஉலக கோப்பையில்
டெண்டுல்கர் ரன்குவிப்பு திருவிழா நடத்தி வருகிறார். 4 போட்டிகளில் 371 ரன் விளாசிய
டெண்டுல்கர்தான் நேற்றைய போட்டியிலும் இந்திய அணியை காப்பாற்றினார். கிரிக்கெட்டில்
கரைகண்டவர் என்பதை டெண்டுல்கர் நேற்றைய போட்டியில் தௌ்ளத் தௌிவாக நிரூபித்தார். ஒரு
பக்கம் விக்கெட் சரிந்தாலும், அதைப்பற்றி கவலைப்படாமல் வழக்கம்போல் தனது விளா சலை
சரிவர செய்த டெண்டுல்கருக்கு கண்டிப்பாக ஓ போட்டுத்தான் ஆக வேண்டும். </p><p>
</p><p>
சர்வதேச கிரிக்கெட்டில் 35-வது சதம் எடுப்பார் என்று எதிர்பார்த்த நிலையில் தசை
பிடிப்பு காரணமாக டெண்டுல்கர் அவதிப்பட்டார். 2 முறை சிகிச்சை பெற்றபோதும் அவரால்
சரியாக ஆடமுடிய வில்லை. குழப்பமான மன நிலையில் இருந்த டெண்டுல்கர் 98 ரன்னில் தனது
விக்கெட்டை இழந்தார். நேற்றைய ஆட்டத்தில் டெண்டுல்கர் 75 பந்தில் 12 பவுண்டரி
மற்றும் ஒரு சிக்சர் விளாசினார். போட்டிக்கு முன்னதாக சவால் விட்ட வேகப்பந்து வீச்சாளர்
அக்தரின் முதல் ஓவரில் டெண் டுல்கர் 18 ரன்கள் விளாசியது இந்திய ரசிகர்களை
உற்சாகத்தின் உச்சத்துக்கே கொண்டு சென்றது. இதே பாணியில் வாக்கர்யூனுஸ் ஓவரில் ஷேவாக்
ரன் எடுத்தார். </p><p>
</p><p>
மேலும் 100 ரன்கள் எடுத் தால்தான் வெற்றி என்ற நிலையில் டெண்டுல்கரும் பெவிலியன் சென்று
விட்டார். முதல் பாதியில் இந்திய வெற்றிக்கு டெண்டுல்கர் புள்ளி வைத்தார். 2-வது பாதியில்
திராவிட் மற்றும் யுவ்ராஜ் ஆகியோர் சரியான முறையில் கோலத்தை பூர்த்தி செய்தனர்.
எந்தவித பதட்டமும் இன்றி ஒற்றை ரன் களை எடுத்த இந்த ஜோடி 45.4 ஓவரில் இந்தியாவை
வெற்றி பெற வைத்தது. </p><p>
</p><p>
உலக கோப்பையில் 2-வது முறையாக இவர்கள் இருவரும் இந்தியாவின் மானத்தை காப் பாற்றி இருக்கிறார்கள்.
யுவ்ராஜ் இறுதிவரை ஆட்டம் இழக்காமல் 50 ரன்களும், திராவிட் 44 ரன்களும் எடுத்தனர்.
3-வது வீரராக ஆடும் வாய்ப்பு பெற்ற காய்ப் நேற்றைய போட்டியில் டெண்டுல்கருடன் ஜோடி
சேர்ந்து 102 ரன்கள் எடுத்தார். பிரகாசமான எதிர்காலத்தை நோக்கி இருக்கும் காய்ப்
இந்திய அணியின் அசையா சொத்து என்பதை நிரூபித்து விட்டார். 5 வெற்றிகள் பெற்ற இந்தியா
சூப்பர்சிக்ஸ் சுற்றுக்கு முன்னேறி உள்ளது. டெண்டுல்கர் ஆட்டநாயகனாக தேர்வு செய்
யப்பட்டார். </p><p>
</p><p>
நேற்றைய தோல்வியால் பாகிஸ்தான் அணிக்கு சிக்கல் உண்டாகி இருக்கிறது. ஜிம்
பாப்வேயுடன் அடுத்தப்போட்டியில் வெற்றிபெற்றால் சூப்பர் சிக்ஸ் வாய்ப்புக்கு
இங்கிலாந்து, ஜிம்பாப்வே அணியுடன் பாகிஸ்தான் பலப்பரீட்சை நடத்த முடியும்.
இல்லையென்றால் அடுத்த உலக கோப்பையில் பார்த்துக்கொள்வோம் என்று மனதை தேற்றிக்
கொண்டு பாகிஸ்தான் வௌியேற வேண்டியதுதான். கிட்டத் தட்ட கார்கில் போரைப் போலவே கிரிக்கெட்டிலும்
இந்தியா தன்னுடைய பலத்தை நிரூபித்து விட்டது. </p><p>
</p><p>
ஸ்கோர் போர்டு </p><p>
</p><p>
பாகிஸ்தான் (50 ஓவர்)-273/7 </p><p>
</p><p>
சயீத்அன்வர் (ப) நெக்ரா-101 </p><p>
</p><p>
தாபீக்உமர் (ப) ஜாகீர்கான்-22 </p><p>
</p><p>
அப்துர் ரசாக் (கா) திராவிட் (ப) நெக்ரா-12 </p><p>
</p><p>
இன்சமாம் உல்-ஹக் ரன்அவுட்-06 </p><p>
</p><p>
யுகானா (கா) ஜாகீர்கான் (ப) நாத்-25 </p><p>
</p><p>
யூனிஸ்கான் (கா) தினேஷ் (ப) ஜாகீர்கான்-32 </p><p>
</p><p>
அபிரிடி (கா) கும்ளே (ப) தினேஷ் -09 </p><p>
</p><p>
லத்தீப் அவுட் இல்லை-29 </p><p>
</p><p>
வாசிம்அக்ரம் அவுட் இல்லை-10 </p><p>
</p><p>
உதிரிகள்-27 </p><p>
</p><p>
விக்கெட் சரிவு: 1/58 2/90 3/98 4/171 5/195 6/208 7/256 </p><p>
</p><p>
பந்துவீச்சு: ஜாகீர்கான் 10-0-46-2நாத் 10-0-41-1 </p><p>
</p><p>
நெக்ரா 10-0-74-2கும்ளே 10-0-51-0 </p><p>
</p><p>
கங்குலி 3-0-14-0ஷேவாக் 4-0-19-0 </p><p>
</p><p>
தினேஷ் 3-0-19-1 </p><p>
</p><p>
இந்தியா (45.4 ஓவர்)-276/4 </p><p>
</p><p>
டெண்டுல்கர் (கா) யூனிஸ்கான் (ப) அக்தர்-98 </p><p>
</p><p>
ஷேவாக் (கா) அபிரிடி (ப) வாக்கர்யூனுஸ்-21 </p><p>
</p><p>
கங்குலி எல்.பி.டபிள்யூ. (ப) வாக்கர்யூனுஸ்-00 </p><p>
</p><p>
காய்ப் (ப) அபிரிடி-35 </p><p>
</p><p>
திராவிட் அவுட் இல்லை-44 </p><p>
</p><p>
யுவ்ராஜ்சிங் அவுட் இல்லை -50 </p><p>
</p><p>
உதிரிகள்-28 </p><p>
</p><p>
விக்கெட் சரிவு: 1/53 2/53 3/155 4/177 </p><p>
</p><p>
பந்துவீச்சு: அக்ரம் 10-0-48-0அக்தர் 10-0-72-1 </p><p>
</p><p>
வாக்கர்யூனுஸ் 8.4-0-71-2அபிரிடி 9-0-45-1 </p><p>
</p><p>
ரசாக் 8-0-36-0 </p><p>
</p><p>
ஆட்டநாயகன்-சச்சின் டெண்டுல்கர் </p><p>
</p><p>
வாஜ்பாய் -சோனியா வாழ்த்து </p><p>
</p><p>
இந்தியா-பாகிஸ்தான் கிரிக்கெட் போட்டியை பிரதமர் வாஜ்பாய் தன் குடும்பத்தினருடன்
அமர்ந்து டி.வி.யில் பார்த்து ரசித்தார். இந்தியா அபார வெற்றி பெற்றதையடுத்து இந்திய
அணிக்கு வாஜ்பாய் வாழ்த்து தெரிவித்ததாக பிரதமர் அலுவலக வட்டாரங்கள் தெரிவித்தன.
இந்திய வீரர்களுக்கு எதிராக கடந்த சில வாரங்களுக்கு முன்பு ரசிகர்கள் கலாட்டா செய்த
போதும் பிரதமர் வாஜ் பாய் வீரர்கள் சாதிப்பார்கள். எனவே அமைதியாக இருங்கள் என்று
ரசிகர்களுக்கு கோரிக்கை விடுத்தது குறிப்பிடத்தக்கது. </p><p>
</p><p>
குணம் அடைந்து விடுவேன் -சச்சின் </p><p>
</p><p>
இந்திய கிரிக்கெட்டின் கதாநாயகனான சச்சின் டெண்டுல்கர் பாகிஸ்தானுக்கு எதிரான
போட்டியின்போது தசைபிடிப்பால் அவதிப்பட்டார். இதனால் அவருக்கு 2 முறை மைதானத்தில்
வைத்து சிகிச்சை அளிக்கப்பட்டது. இருந்தாலும் 98 ரன் குவித்து இந்தியாவின்வெற்றிக்கு
டெண்டுல்கர் வழிவகுத்தார். போட்டிக்குப்பின்னர் அவர் கூறுகையில், ரசிகர்கள் யாரும்
கவலைப்பட வேண்டாம். சூப்பர்சிக்ஸ் மோதலுக்குள் நான் குணம் அடைந்து விடுவேன் என்றார்.
இதுவரை நடந்த போட்டிகளில் டெண்டுல்கர் 469 ரன்கள் குவித்து முதலிடத்தில் உள்ளார்.
நேற்றைய போட்டிக்குப்பின்னர் தொடர்நாயகன் பட்டியலில் டெண்டுல்கர் முதலிடம் பிடித்தது
குறிப்பிடத்தக்கது. </p>

<p> </p>

<p> </p>

<p>வ.தேசத்துடன் வெற்றி:
சூப்பர்சிக்சில் கென்யா </p>

<p> </p>

<p>ஜோகனஸ்பெர்க், மார்ச் 1- வங்கதேசத்தை வென்று கென்யா சூப்பர்
சிக்சில் நுழைந்தது. </p><p>
</p><p>
உலக கோப்பை கிரிக்கெட்டிலும் கால்பந்து போட்டிபோல் அதிர்ச்சியான சம்பவங்கள்
நடக்கத்தான் செய்கின்றது. சமீபத்தில் கொரியாவில் நடந்த உலக கால்பந்தில் கத்துக்குட்டியான
செனகல் நடப்பு சாம்பியன் பிரான்சை தோற்கடித்தது. அதுபோல் டெஸ்ட் அந்தஸ்து பெறாத
கென்யா அணி லீக் போட்டியில் இலங்கை அணியை தோற்கடித்தது. நேற்றைய போட்டியில் கென்யா
32 ரன் வித்தியாசத்தில் வங்க தேசத்தை வீழ்த் தியது. இதன்மூலம் கென்யா அணி 16
புள்ளிகளுடன் சூப்பர்சிக்ஸ் வாய்ப்பை பெற்றது. டெஸ்ட் அந்தஸ்து பெறாத கிரிக்கெட் அணி
உலக கோப்பையின் அடுத்தசுற்றுக்கு முன்னேறியது இதுவே முதல்முறையாகும். தற்போதுள்ள
சூழ்நிலைப்படி பார்த்தால் தென்ஆப்பிரிக்கா அணி தனது லீக் போட்டியில் இலங்கையை
தோற்கடித்தால் சூப்பர்சிக்ஸ் வாய்ப்பை எட்டும். இல்லையென்றால் தென்ஆப்பிரிக்கா
வௌியேற வேண்டியதுதான். தென்ஆப் பிரிக்கா வெற்றிபெற்றால் நியூசிலாந்து போட்டியில்
இருந்து வௌியேறும் பரிதாப சூழ்நிலை ஏற்பட்டுள்ளது. </p><p>
</p><p>
னாஏ பைிரிவில் இன்று நடக்கும் முக்கியமான ஆட்டத்தில் நடப்பு சாம்பியன்
ஆஸ்திரேலியா-இங்கிலாந்து அணிகள் பலப்பரீட்சை நடத்துகின்றன. இன்றைய போட்டி இங்கிலாந்து
அணிக்கு வாழ்வா-சாவா? போராட்டம் ஆகும். 12 புள்ளிகள் பெற்றிருக்கும் இங்கிலாந்து
அணி ஒருவேளை ஆஸ்திரேலியாவை தோற்கடித் தால் ஜிம்பாப்வே மற்றும் பாகிஸ்தான்
அணிகள்போட்டியில் இருந்து வௌியேறும். இல்லையென்றால் ஜிம்பாப்வே அணிக்கு சூப்பர்சிக்சுக்குள்
நுழையும் வாய்ப்பு கிடைக்கும். முக்கியமான இன்றைய ஆட்டத்தில் இங்கிலாந்து வீரர்கள்
சாதிப்பார்களா? இல்லை வழக்கமான சோதனைதானா? என்பதை பார்ப்போம் </p>

<p> </p>

<p> </p>

<p>இன்று ஆஸ்திரேலியாவுடன்
மோதல்: இங்கிலாந்துக்கு வாழ்வா-சாவா போராட்டம் </p>

<p> </p>

<p>போர்ட்எலிசபெத், மார்ச் 2- இன்று ஆஸ்திரேலியா - இங்கிலாந்து
அணிகள் மோதுகின்றன. இங்கிலாந்துக்கு இது வாழ்வா சாவா என்பதை நிர்ணயிக்கும் போர்
களமாகும். </p><p>
</p><p>
உலக கோப்பை கிரிக்கெட் போட்டி வழக்கமான தனது விறுவிறுப்பை கொஞ்சம் தாமதமாக
எட்டியிருக்கிறது. ஏ பிரிவில் ஆஸ்திரேலியா அனைத்து மோதல்களிலும் வெற்றிபெற்று சூப்பர்
சிக்ஸ் வாய்ப்பை பெற்று விட்டது. ஆனால் இங்கிலாந்து அணி இதுவரை நடந்த போட்டிகளில் 3
வெற்றி மற்றும் 2 தோல்விகளை கண்டுள்ளது. இந்தநிலையில் இங்கிலாந்து தனது கடைசி லீக்
போட்டியில் நடப்பு சாம்பியன் ஆஸ்திரேலியாவை எதிர் கொள்கிறது. இன்றைய போட்டி
இங்கிலாந்துக்கு வாழ்வா-சாவா? போராட்டத்தை உண்டாக்கி இருக்கிறது. </p><p>
</p><p>
இன்றைய போட்டியில் வெற்றிபெற்றால் மட்டுமே இங்கிலாந்து அணிக்கு சூப்பர்சிக்ஸ்
வாய்ப்பு கிடைக்கும் என்ற நிலை உருவாகி இருக்கிறது. இதனால் ஒட்டுமொத்த பதட்டமும்
இங்கிலாந்து அணிக்கு உள்ளது. இன்றைய போட்டிக்கான ஆஸ்திரேலிய அணியில் நம்பிக்கை
பந்துவீச்சாளர் கிலெஸ்பீ இடம்பிடிக்க மாட்டார் என்று காப்டன் ரிக்கி பாண்டிங்
அறிவித்துள்ளார். கடந்த சில நாட்களாக காயம் காரணமாக அவதிப்பட்ட கிலெஸ்பீக்கு ஓய்வு
கொடுக்கப்பட்டுள்ளது. தொடர்ந்து 11 போட்டிகளில் வெற்றி பெற்றிருக்கும் ஆஸ்திரேலியா
இன்று தனது 12-வது வெற்றியை எட்டும் நோக்கில் காத்திருக்கிறது. ஆனால் எப்போதுமே ஆஸ்தி
ரேலியாவுக்கு எதிரான போட்டியில் இங்கிலாந்து சிறப்பாக ஆடும் என்பதால் மோதல்
விறுவிறுப்பாக இருக்கும் என்று நம்பப்படுகிறது. </p><p>
</p><p>
கிலெஸ்பீ இடம்பிடிக்காத காரணத்தால் மெக்ராத்-பிரெட்லீ ஆகியோர் பந்துவீச்சை தொடங்க
காத்திருக்கிறார்கள். இங்கிலாந்து அணியின் தொடக்க பேட்ஸ்மென் டிரெஸ்கோதிக்
இந்தப்போட்டி யில் சொல்லிக்கொள்ளும்படியான ரன்களை எடுக்க வில்லை. இன்றைய போட்டி
தனிப்பட்ட முறையில் அவருக்கு மிகப்பெரிய சவாலை உண்டாக்கி உள்ளது. முந்தைய ஆட்டத்தில்
இங்கிலாந்து 82 ரன் வித்தியாசத்தில் இந்தியாவிடம் தோல்வி கண்டது. கடைசி கட்டத்தில்
களம் இறங்கிய பிளின்டாப் மட்டுமே அரை சதம் கடந்தார். மற்றபடி அனைத்து நம்பிக்கை
வீரர்களும் ஏமாற் றம் அடைந்தனர். ஆனால் இதற்கு நேர்மாறாக ஆஸ்திரேலிய அணியில் அனைத்து
வீரர்களும் நல்ல நிலையில் இருக்கிறார்கள். நீக்கப்பட்ட கிலெஸ்பீ இடத்தை பிக்கெல்
பிடிப்பார் என்று எதிர்பார்க்கப்படுகிறது. </p>

<p> </p>

<p> </p>

<p>டாக்டர்கள் அறிவுரை
தட்டிக்கழித்த சர்வண் </p>

<p> </p>

<p>கேப்டவுன், மார்ச் 2- இலங்கை - மேற்குஇந்திய தீவு
மோதலின்போது டாக்டர்கள் அறிவுரையை தட்டிக்கழித்து அணியின் வெற்றிக்காக போராடினார்
சர்வண். </p><p>
</p><p>
சூப்பர்சிக்ஸ் வாய்ப்பை எட்ட வேண்டிய கட்டாய சூழ்நிலையில் மேற்கு இந்தியதீவு அணி தனது
லீக் போட்டியில் இலங்கையுடன் மோதியது. 10 புள்ளிகள் பெற்றிருந்த மேற்கு இந்தியதீவு
அணிக்கு இலங்கையுடன் வெற்றி முக்கியமாக பட்டது. ஆனால் கடும் போராட்டத்துக்குப்பின்னர்
மேற்கு இந்தியதீவு அணி 6 ரன் வித்தியாசத்தில் தோல்வி அடைந்தது. மேற்கு இந்தியதீவு
அணிக்கு தோல்விதான் என்றாலும் குறிப்பிடத்தக்க விஷயம் ஒன்று இந்தப்போட்டியின் போது
அரங்கேறியது. </p><p>
</p><p>
மேற்கு இந்தியதீவு அணியின் துடிப்பான இளம் ஆட்டக்காரர் ராம்நரேஷ் சர்வண். சிக்கலான
நிலையில் அணியை காப்பாற்ற வேண்டிய நெருக்கடியில் சர்வண் காயம் அடைந்தார். இலங்கை
அணியின் வேகப்பந்து வீச்சாளர் தில்காரா வீசிய பந்து நேராக சர்வண் தலையை தாக்கியது.
பந்து பட்டதும் அந்த இடத்தி லேயே சர்வண் சுருண்டு விழுந்தார். பின்னர் மருத்து வக்குழு
சர்வணை பரிசோதித்தது. காயம் அடைந்த முதல் 10 நிமிடம் சர்வண் உடலில் எந்த அசைவும்
இல்லை. </p><p>
</p><p>
இதனால் அவர் அவசரமாக அருகில் இருந்து மருத்துவமனையில் சேர்க்கப்பட்டார். முதல்கட்ட
சிகிச்சை முடிந்த பின்னர் சர்வண் பழைய நிலைக்குத் திரும்பினார். ஆனாலும் தலையில் அவருக்கு
பலத்த காயம் ஏற்பட்டதால் ஓய்வு எடுக்க வேண்டும் என்று மருத்துவர்கள் ஆலோசனை
வழங்கினார்கள். மேற்கு இந்தியதீவு காப்டன் கூப்பரும் உடல்நிலை முக்கியம் என்று
கூறிவிட்டார். ஆனால் மேற்கு இந்தியதீவு அணி 169 ரன்னுக்கு 7 விக்கெட்டுகளை இழந்து
தடுமாறியதை கண்ட சர்வண் மீண்டும் பேட்டிங் செய்ய விருப்பம் தெரிவித்தார். </p><p>
</p><p>
மறுப்பு தெரிவித்த மருத்துவர்கள் பின்னர் சர்வணை ஆட அனுமதித்தனர். துடிப்பு டன் களம்
இறங்கிய சர்வண் பின்னர் 2 சிக்சர் மற்றும் 4 பவுண்டரிகள் விளாசினார். ஆனாலும் கடைசி
ஓவருக்கு 14 ரன் தேவை என்ற நிலையில் சர்வண் ஏமாற்றம் கண்டார். மேற்கு இந்தியதீவு அணி
தோற்றபோதும் சர்வணின் இந்த ஆட்டத்தை உலக கிரிக்கெட் வல்லுனர்கள் அனைவரும் பாராட்டி உள்ளனர்.
மேற்கு இந்தியதீவு பயணத்தின்போது இந்திய வீரர் கும்ளே தனக்கு ஏற்பட்ட காயத்தையும்
பொருட்படுத்தாமல் களம் இறங்கி, லாரா விக்கெட்டை சாய்த்ததை நாமும் பெருமை யாக நினைவு
கூர்ந்து கொள்வோம். </p>

<p> </p>






</body></text></cesDoc>