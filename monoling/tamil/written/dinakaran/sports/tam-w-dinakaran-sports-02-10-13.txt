<cesDoc id="tam-w-dinakaran-sports-02-10-13" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-02-10-13.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-10-13</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-10-13</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>முதல் டெஸ்ட்டில் மேற்கு
இந்தியதீவு பரிதாபம்: இந்திய அணிக்கு இன்னிங்ஸ் வெற்றி</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>மேற்கு இந்தியதீவு வீரர்
சர்வண் அடித்த பந்தை காட்ச் செய்த டெண்டுல்கருக்கு இந்திய விக்கெட் கீப்பர் பார்த்தீவ்
பாராட்டு தெரிவித்த காட்சி. </p>

<p> </p>

<p>மும்பை, அக். 13- மேற்கு இந்தியதீவு அணிக்கு எதிரான முதல் டெஸ்ட்
கிரிக்கெட் போட்டியில் இந்தியா இன்னிங்ஸ் வித்தியாசத்தில் வெற்றி பெற்றது. </p><p>
</p><p>
டெஸ்ட் கிரிக்கெட் </p><p>
</p><p>
கிட்டத்தட்ட 76 டெஸ்ட் போட்டிகளுக்குப்பிறகு மேற்கு இந்தியதீவு அணிக்கு எதிராக இந்தியா
இன்னிங்ஸ் வித்தியாசத்தில் முதல் வெற்றி கண்டுள்ளது. நம்பிக்கை ஆட்டக்காரர் லாரா
அணியில் இடம்பிடிக்க முடியாமல் போனது மேற்கு இந் தியதீவு அணிக்கு மிகப்பெரிய சோதனையாக
அமைந்தது. லாரா அணியில் இடம்பிடிக்க வில்லை என்ற சோகம் அணியில் உள்ள அனைத்து
வீரர்களையும் அதிகமாகவே தாக்கி இருந்தது. </p><p>
</p><p>
இந்தியா 457 ரன் </p><p>
</p><p>
டாஸ் வென்ற இந்தியா முதல் இன்னிங்சில் 457 ரன் குவித்தது. சோகமான முறை யில் பேட்டிங்
செய்ய வந்த மேற்கு இந்தியதீவு அணி முதல் இன்னிங்சில் 157 ரன்னில் சுருண்டது.
இந்தியாவைவிட 300 ரன்கள் பின்தங்கிய நிலையில் மேற்கு இந்தியதீவு மீண்டும் 2-வது
இன்னிங்ஸ் ஆடவேண்டிய கட்டாயம் ஏற்பட்டது. இந்த முறை மேற்கு இந்தியதீவு வீரர்களிடம்
சோகத்தைவிட தோல்வி பயம் அதிகமாக காணப்பட்டது. பாதுகாப்புடன் ஆடிய ஒருசில மேற்கு
இந்திய தீவு வீரர்கள் மட்டுமே இரட்டை இலக்க ரன் எடுத்தனர். </p><p>
</p><p>
மேற்கு இந்தியதீவு அணியின் 4-வது நாள் ஆட்டம் கிட்டத்தட்ட கிரிக்கெட் மறு ஒளிபரப்பை
பார்ப்பது போல் விறுவிறுப்பாக இருந்தது. சுழல்பந்து வீச்சு நாயகர்களான ஹர்பஜன் மற்றும்
கும்ளே இந்தப்போட்டியில் ஒன்றாக இணைந்து சாதித்துவிட்டனர். 14 டெஸ்ட் போட்டிகளில்
தொடர்ந்து வெற்றிபெற்று சாதனை படைத்த ஆஸ்திரேலியாவை மண்கவ்வச் செய்த ஹர்பஜன் மேற்கு
இந்தியதீவு அணிக்கு எதிராகவும் சாதிக்க தவறவில்லை. </p><p>
</p><p>
ஹர்பஜன் 7 விக்கெட் </p><p>
</p><p>
முதல் இன்னிங்சில் விக்கெட் கைப்பற்றும் முயற்சியில் தோல்வி அடைந்த ஹர்பஜன் நேற்றைய
ஆட்டத்தில் ஒட்டு மொத்தமாக 7 விக்கெட்டுகள் சாய்த்தார். டெஸ்ட் கிரிக்கெட்டில் 10-வது
முறையாக 5 விக்கெட்டுகளுக்கு மேல் சாய்த்து ஹர்பஜன் சாதனை படைத்தார். இந்த ஆட்டத்தில்
கும்ளேவின் பந்துவீச்சும் மெச்சும்படியாக இருந்தது. முதல் இன்னிங்சில் 4 விக்கெட்டுகள்
சாய்த்த கும்ளே நேற்றைய ஆட்டத்தில் 3 விக்கெட்டுகள் கைப்பற்றி மீண்டும் தனது பெயரை
நிலைநாட்டிக் கொண்டார். </p><p>
</p><p>
ஷேவாக் ஆட்டநாயகன் </p><p>
</p><p>
முடிவில் மேற்கு இந்தியதீவு அணி 188 ரன்னில் அனைத்து விக்கெட்டுகளையும் பறி கொடுத்தது.
இதனால் இந்தியா இன்னிங்ஸ் மற்றும் 112 ரன் வித்தியாசத்தில் வெற்றிபெற்றது. இன்னும் 2
மோதல்கள் எஞ்சி உள்ள நிலையில் இந்தியா 1-0 என்ற புள்ளியில் முன்னணி பெற்றது.
இந்த இரு அணிகளும் மோதும் 2-வது டெஸ்ட் கிரிக்கெட் போட்டி வருகிற 17-ந் தேதி முதல்
சென்னை சேப்பாக்கம் மைதானத்தில் நடக்க உள்ளது. முதல் இன்னிங்சில் சதம் அடித்த ஷேவாக்
ஆட்டநாயகனாக தேர்வு செய்யப்பட்டது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p> </p>

<p>ஹாக்கி: கொரியாவுடன்
தோல்வியால் இந்தியாவுக்கு வௌ்ளி</p>

<p> </p>

<p>புசான், அக். 13- ஆசிய விளையாட்டுப் போட்டிக்கான ஹாக்கியில்
கொரியாவிடம் தோற்றதால் இந்தியாவுக்கு வௌ்ளிப்பதக்கம் கிடைத்தது. </p><p>
</p><p>
14-வது ஆசிய விளையாட்டுப்போட்டி கொரியாவில் நடந்து வருகிறது. இந்தப்போட்டியில் இந்தியா
10 தங்கப்பதக்கம் வென்று 5-வது இடத்தை எட்டியுள்ளது. தங்கப்பதக்க எண்ணிக்கையில்
இந்தியா மேலும் ஒரு பதக்கம் பெறும் என்ற எதிர்பார்ப்போடு ஹாக்கி இறுதிமோதல் நேற்று
நடந்தது. அளவுக்கு அதிகமான எதிர்பார்ப்புடன் நடந்த இந்தப்போட்டியில் இந்தியா-கொரியா
அணிகள் பலப் பரீட்சை நடத்தின. நடப்பு சாம்பியன் தகுதியோடு இந்தியா களம் இறங்கியது.
கடந்தமுறை பாங்காங் ஆசிய விளையாட்டு இறுதிப் போட்டியில் இந்திய அணியிடம் கொரியா
தோற்றது. </p><p>
</p><p>
இதற்கு பதிலடி கொடுக்கும் முடிவுடன் கொரியா ஆடியது.வழக்கத்தைவிட வேகமாக ஆடிய கொரியா அணி
வீரர்களின் நுணுக்கத்தை இந்திய வீரர்களால் கணிக்க முடியவில்லை. இதனால் முதல் பாதியில்
கொரியா 2 கோல்போட்டு முன்னணி பெற்றது. 2-வது பாதி தொடக்கத்தில் கொரியா மேலும் ஒரு
கோல் அடித்தது. இதனால் இந்திய ரசிகர்கள் முகத்தில் சோகம் தொற்றிக்கொண்டது. ஆனால்
ஏதோ ஒரு உத்வேகத்துடன் ஆடிய இந்திய அணி அடுத்தடுத்து 3 கோல்கள் அடித்து ஆட்டத்தை சமன்
செய்தது. </p><p>
</p><p>
இந்திய அணியில் ஜுக்ராஜ் 2 கோல்களும், ககன்அஜித்சிங் ஒரு கோலும் போட்டனர். இதனால்
மோதல் அதிகப்படியான நேரத்தை நோக்கி நகர்ந்து கொண்டிருந்தது. அப்போது
எதிர்பாராதவிதமாக இந்திய வீரர் செய்த தவறால் கொரியா அணிக்கு பெனால்டி வாய்ப்பு ஒன்று
கிடைத்தது. பதட்டத்தை உண்டாக்கிய இந்த பெனால்டி வாய்ப்பை கொரியா வீரர்கள் சரியாக
பயன்படுத்திக் கொண்டனர். இதனால் கொரியா 4-3 என்ற கோல் வித்தியாசத்தில் வெற்றி
பெற்று தங்கப்பதக்கம் கைப்பற்றியது. கொரியாவின் இந்தகோலுக்கு பதிலடி கொடுக்க இந்தியா
மேற்கொண்ட முயற்சிகள் எதுவும் பலிக்க வில்லை. </p><p>
</p><p>
சாம்பியன் கோப்பையை தக்கவைத்துக்கொள்ள தவறிய இந்திய அணிக்கு வௌ்ளிப்பதக்கம்
கொடுக்கப்பட்டது. முன்னதாக வெண் கலப்பதக்கத்துக்காக மலேசியா மற்றும் பாகிஸ்தான் அணிகள்
மோதின. அதிக எதிர்பார்ப்புடன் களம் இறங்கிய பாகிஸ்தான் அணிக்கு பரிதாபம்தான்
பரிசாக கிடைத்தது. நிர்ணயிக்கப்பட்ட ஆட்டத்தில் இரு அணிகளும் தலா ஒரு கோல் அடித்தன.
பின்னர் நடந்த அதிகப்படியான நேரத்தில் மலேசியா மேலும் ஒரு கோல் போட்டு
வெண்கலப்பதக்கம் கைப்பற்றியது. </p><p>
</p><p>
பாகிஸ்தான் அணி அரைஇறுதிப்போட்டியில் இந்தியாவிடம் பரிதாபமாக தோற்றது
குறிப்பிடத்தக்கது. நேற்றைய இறுதிப்போட்டியில் இந்திய ஆட்டக்காரர் தன்ராஜ் பிள்ளை
சிறப்பாக ஆடினார். ஆனாலும் அவரால் கோல்ஏதுவும் அடிக்க முடியவில்லை. இவர் பாஸ் செய்த
பந்தை ஜுக்ராஜ் சரியாக பயன்படுத்தி ஒரேஒருமுறை கோல் அடித்தார். மற்றபடி தன்ராஜ்
பிள்ளையை சுற்றி கொரியா வீரர்கள் சரியான பாதுகாப்பு வளையம் அமைத்து இந்தியாவின்
ஆட்டத்தை தடுத்தது குறிப்பிடத்தக்கது. முன்னதாக லீக் ஆட்டத்தில் இந்தியா மற்றும் கொரியா
அணிகள் தலா ஒரு கோல் போட்டு டிரா செய்தது நினைவு கூறத்தக்கது. </p>

<p> </p>

<p> </p>

<p>கல்லூரிகளுக்கான
தடகளப்போட்டி: எம்.ஓ.பி. வைஷ்ணவா அணி சாம்பியன் ஆண்கள் பிரிவில் லயோலா வெற்றி</p>

<p> </p>

<p>சென்னை, அக். 13- கல்லூரிகளுக்கான தடகளப் போட்டியில்
எம்.ஓ.பி. வைஷ்ணவா-லயோலா அணிகள் சாம்பியன் கோப்பை வென்றன. </p><p>
</p><p>
சென்னை பல்கலைக்கழக கல்லூரி அணிகளுக்கான தடகளப்போட்டி கடந்த 3 நாட் களாக சென்னை நேரு
மைதானத்தில் நடந்தது. முன்னணி அணிகள் பலவும் இந்தப்போட்டியில் கலந்து கொண்டன.
மதிப்புமிக்க இந்தப்போட்டியின் தொடக்க நாளில் இருந்தே சாம்பியன் கோப்பை வெல்வதில்
கடும் பலப்பரீட்சை இருந்தது. ஆண்கள் பிரிவில் எம்.சி.சி. கல்லூரி எதிர்பார்த்தபடி
பிரகாசிக்க வில்லை. இதனால் லயோலா அணியின் வெற்றி எளிதானது. </p><p>
</p><p>
இதற்கு நேர்மாறாக பெண்கள் பிரிவில் சாம்பியன் கோப்பைக்கு எத்திராஜ் மற்றும்
எம்.ஓ.பி. வைஷ்ணவா கல்லூரிகளிடையே கடும் போட்டி நிலவியது. தொடக்க நாளில் இருந்தே
அதிகமான புள்ளிகள் பெற்று முன்னணி பெற்ற எம்.ஓ.பி.வைஷ்ணவா நேற்றைய நாளில் கூடுதல்
புள்ளிகள் எட்டியது. முடிவில் வைஷ்ணவா கல்லூரி 85 புள்ளிகள் கைப்பற்றி ஒட்டுமொத்த
சாம்பியன் கோப்பையை வென்றது. எத்திராஜ் அணிக்கு 54 புள்ளிகள் மட்டுமே கிடைத்தது. </p><p>
</p><p>
எத்திராஜ் ஏமாற்றம் </p><p>
</p><p>
இந்தப்போட்டியில் 15 முறை சாம்பியன் கோப்பை கைப்பற்றிய எத்திராஜ் அணிக்கு இந்த முறை
ஏமாற்றம்தான் மிஞ்சியது. பெண்களுக்கான தனி நபர் சாம்பியன் பட்டத்தை எம்.ஓ.பி.
வைஷ்ணவா வீராங்கனை கீர்த்தனா தட்டிச்சென்றார். 100மீ., 200மீ., 400மீ. மற்றும்
400மீ. தடைஓட்டம் ஆகிய போட்டிகளில் கீர்த்தனா தங்கப் பதக்கம் வென்று சாதனை
படைத்தார். ஆண்கள் பிரிவில் எம்.சி.சி. வீரர் ரகுநாத்துக்கு தனிநபர் சாம்பியன் பட்டம்
வழங்கப்பட்டது. </p><p>
</p><p>
போட்டி முடிவில் சென்னை பல்கலைக்கழக துணை வேந்தர் இன்னாசி முத்து சிறப்பு விருந்தினராக
கலந்து கொண்டு பரிசுகள் வழங்கினார். சென்னை பல்கலைக்கழக ஆலோசகக்குழுத் தலைவர் டாக்டர்
பா.சிவந்தி ஆதித்தன் நிறைவு விழாவுக்கு தலைமை தாங்கினார். </p><p>
</p><p>
ஆண்கள்: அரைமாரத்தான்: ரகுநாத் (எம்.சி.சி.), மாரிமுத்து (எம்.சி.சி.), சுரேஷ்குமார்
(ஒய்.எம்.சி.ஏ.); 110மீ. தடைஓட்டம்: முத்துச்சாமி (லயோலா), சரவணன் (எம்.சி.சி.),
சிவராமகிருஷ்ணன் (லயோலா); உயரம் தாண்டுதல்: விக்டர் ஜெபசிங் (லயோலா),
தமிழ்ச்செல்வன் (எம்.சி.சி.), அருள்குமார் (எம்.சி.சி.), அமர்நாத் (ஒய்.எம்.சி.ஏ.),
சிவராமன் (பச்சையப்பா-3 பேர் 3-வது இடம்); 200மீ. ஓட்டம்: ராஜேஷ்அப்பு (லயோலா),
பிரகாஷ் (எம்.சி.சி.), வெங்கடேஷ்வரன் (எஸ்.ஆர்.எம்.); வட்டுஎறிதல்: டெரிக் ஹட்சன்
(லயோலா), எட்வின் புஸ்பராஜ் (லயோலா), ஜாகீர்உசைன் (எம்.சி.சி.); 1500மீ. ஓட்டம்:
நிவாசன் (லயோலா), சீனிவாசன் (லயோலா), லட்சுமண் (பச்சை யப்பா); டெக்காத்லான்:
விக்டர் ஜெபசிங் (லயோலா), ரஜ்சித் (லயோலா), ஸ்டாலி (எம்.சி.சி.); 400மீ. தொடர்
ஓட்டம்: லயோலா, எம்.சி.சி., ஒய்.எம்.சி.ஏ.; 100மீ. தொடர் ஓட்டம்: லயோலா,
ஒய்.எம். சி.ஏ., எம்.சி.சி.; </p><p>
</p><p>
பெண்கள்: அரைமாரத்தான்: சுதா தேவி (எத்திராஜ்), பாஞ்சாலி (எம்.ஓ.பி. வைஷ்ணவா),
மரிய தெரசா (எத்திராஜ்); 200மீ. ஓட்டம்: கீர்த்தனா (எம்.ஓ.பி. வைஷ்ணவா), ஸ்வேதா
(எத்திராஜ்), அஞ்சுகுமாரி (ராணிமேரி); நீளம் தாண்டுதல்: அமுதா (எம்.ஓ.பி.),
நூஷீன் ராஜேந்திரன் (அண்ணா), கிறிஸ்டி ஜெயா (எத்திராஜ்); 1500மீ. ஓட்டம்: மலையழகு
(ஒய்.எம்.சி.ஏ.), ருக்மணி (எம்.ஓ.பி.), வாணி (எம்.ஓ.பி.); ஹெப்டாத்லான்: அமுதா
(எம்.ஓ.பி.), பிரமீனா (எத்திராஜ்), சுரேகா (எம்.ஓ.பி.); 100மீ. தொடர் ஓட்டம்:
எம்.ஓ.பி., ஒய்.எம்.சி.ஏ., எத் திராஜ்; 400மீ. தொடர்ஓட்டம்: எம்.ஓ.பி.,
எத்திராஜ், ஒய்.எம்.சி.ஏ.. </p>

<p> </p>






</body></text></cesDoc>