<cesDoc id="tam-w-dinakaran-sports-01-02-04" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-01-02-04.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-02-04</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-02-04</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சிறந்த விளையாட்டு
வீரர்களை உருவாக்க, மீனவ இளைஞர்களுக்கு நீச்சல் -படகு ஓட்டும் பயிற்சி, மத்திய
அமைச்சர் உமாபாரதி பேட்டி</p>

<p> </p>

<p>ராமநாதபுரம், பிப்.4- ராமநாதபுரம் அரண் மனை சார்பில் மத்திய
மந்திரி உமாபாரதிக்கு சிறப்பான வரவேற்பு அளிக்கப்பட்டது. ராமநாத சுவாமி திருக்கோவில்
அறங்காவலர் குழு தலைவரும் இளைய மன்னருமான குமரன் சேதுபதி அவரது துணைவி யார் லெட்சுமி
நாச்சியார் ஆகியோர் வரவேற்றனர். </p><p>
</p><p>
பேட்டி </p><p>
</p><p>
மத்திய அமைச்சர் உமா பாரதி நிருபர்களுக்கு பேட்டி அளித்தார். அப்போது அவர்
தெரிவித்ததாவது:- </p><p>
</p><p>
தேசிய அளவில் தமிழகம் கலை கலாச்சாரம் பண்பாடு தெய்வீக பற்று போன்றவற்றில் சிறப்பாக
உள்ளது. ராமேசுவரம் திருக்கோவிலின் சிலைகள் அதன் சிற்பங்களை காணும்போது இந்த நாடு
எவ்வளவோ நவீனமாகி விட்டாலும் கூட தமிழகம் பழைய பண்பாட்டில் இன்றும் நிலைத்து நிற்பது
நிறைவு தருகிறது. </p><p>
</p><p>
இளைஞர்களுக்கு பயிற்சி </p><p>
</p><p>
தமிழகம் நீண்ட நெடிய கடற்கரையை கொண்டது. இந்த மாவட்டத்தில் (ராமநாதபுரம்) மீனவர்
அதிகம் பேர் உள்ளனர். இவர்களில் இளைஞர்களை தேர்ந்தெடுத்து அவர்களுக்கு நீச்சல்
பயிற்சி, படகு ஓட்டும் பயிற்சி போன்ற விளையாட்டு துறையில் ஈடு படுத்தி அவர்களுக்கு
ஊக்கம் அளித்து சிறந்த விளையாட்டு வீரர்களை உருவாக்க முடியும். இதற்காக மத்திய
விளையாட்டு துறை தமிழக அரசுக்கு தேவை யான எல்லாவிதமான நிதி உதவிகளையும் வழங்க தயாராக
உள்ளது. </p><p>
</p><p>
விளையாட்டு அகாடமி </p><p>
</p><p>
ஒவ்வொரு மாநிலத்திலும் ஒரு விளையாட்டை ஊக்கு விக்கும் வகையில் மத்திய அரசு விளையாட்டு
அகாடமி (ஸ்போர்ட்ஸ் அகாடமி) அமைக்க முடிவு செய்துள்ளது. இதற்காக பலகோடி ரூபாய்
செலவிடப்படும். </p><p>
</p><p>
நடப்பு ஆண்டில் தமிழகம் உள்பட 5 மாநிலங்களில் 5 விளையாட்டு அகாடமி அமைக் கப்படும்.
தமிழகத்தில் சென் னையில் பல கோடி ரூபாய் செலவில் அகாடமி அமைக்கப் படும். இதன் மூலம்
சிறந்த விளையாட்டு வீரர்களை உருவாக்க முடியும். இதன் மூலம் தகுதியானவர்களை உலக
சவால்களுக்கு ஏற்ப உருவாக்க முடியும். மிகச் சிறந்த விளை யாட்டு வீரர்களை உருவாக்கும்
நடைமுறை திட்டம் அடியோடு அழிந்து விட்டது. புதிய திட்டம் தொடங்கப்பட்டு உள்ளது. இதன்
மூலம் இளைஞர்களுக்கு நல்ல பயிற்சி, தேசப்பற்று மக்களிடையே விழிப்புணர்வு ஏற்படுத்தல்
போன்ற பல்வேறு நல்லப் பணிகளில் தற்போ துள்ள இளைஞர் சமுதாயத்தை ஈடுபடுத்தும்
வகையில் நேரு யுவகேந்திரா தேசிய இளைஞர் புணரமைப்பு திட்டம் உருவாக்கப்பட்டு
உள்ளது. இந்த திட்டம் இந்திய முழுவதும் உள்ள 80 மாவட்டத்தில் செயல் படுத்தப்படும். </p><p>
</p><p>
விழிப்புணர்வு </p><p>
</p><p>
இதில் பங்கேற்கும் இளைஞர்கள் அரசு நலத் திட்டப் பணிகள் செயல்பாடு மற்றும்
சாலைகள், பாலம் அமைக்கும் பணிகளில் மக்களை ஈடுபடுத்துவது அவர்களுக்கு விழிப்பு ணர்வு
ஏற்படுத்தும் வகையில் செயல்படுவார்கள். </p><p>
</p><p>
கருணாநிதிக்கு பாராட்டு </p><p>
</p><p>
வடமாநிலங்களோடு ஒப்பிடும் போது தமிழகத்தில் சிறப்பான ஆட்சி நடக்கிறது. தமிழக முதல்வர்
கருணாநிதி நிர்வாக திறன்மிக்கவர் என்பதனை எடுத்துக்காட்டும் வகையில் தமிழகம்
திகழ்கிறது. நல்லாட்சி நடக்க தமிழகத்தின் பாரம்பரியம் உதவுகிறது. மதுரை ராமேசுவரம் அகல
ரெயில் பாதை திட்டம் மிக முக்கியமானது. இந்த திட்டம் நிறைவேற்றுவது குறித்து சம்பந்தப்
பட்ட அமைச்சரிடம் வலியுறுத்துவேன். </p><p>
</p><p>
சேது சமுத்திர திட்டம் நிறைவேற்றப்படும் போது ராமேசுவரம் தனுஷ்கோடி அருகே உள்ள வரலாற்று
சிறப்பு மிக்க கோதண்டராமர் கோவில் இடிக்கப்படமாட்டாது. </p><p>
</p><p>
காஷ்மீர் பிரச்சனை விரைவில் தீர்ந்து விடும் இன்னொரு நாடு தலையிடுவதை மத்திய அரசு
விரும்பவில்லை. வாஜ்பாய் பிரச்சனைகளை தீர்த்து விடுவார் மேற்கண்டவாறு அவர்
தெரிவித்தார். </p><p>
</p><p>
பேட்டியின் போது மன்னர் குமரன் சேதுபதி, அவரது துணைவியார் லெட்சுமி நாச்சியார் ஆகியோர்
உடன் இருந்தனர். </p>

<p> </p>

<p>நியூசி.யுடன் கிரிக்கெட்:
அர்னால்டு விளாசலில் இலங்கை வெற்றி</p>

<p> </p>

<p>வெலிங்டன், பிப்.4- நியூசிலாந்தில் சுற்றுப்பயணம்
மேற்கொண்டுள்ள இலங்கை கிரிக்கெட் அணி 2-வது ஒரு நாள் போட்டியில் நேற்று ஆடியது. முதலில்
பேட்டிங் செய்த நியூசிலாந்து 50 ஓவரில் 8 விக்கெட் இழந்து 205 ரன் பெற்றது. ஹாரிஸ்
அதிகபட்சமாக 56 ரன் எடுத்தார். சொய்சா 4 விக்கெட் கைப்பற்றினார். இதையடுத்து பேட்டிங்
செய்த இலங்கை அணி 3 பந்து வீசவேண்டிய நிலையில் 3 விக்கெட் வித்தியாசத்தில்
வெற்றிபெற்றது. ஏற் கனவே நடந்த முதல் ஆட்டத்திலும் இலங்கை அணி வெற்றி பெற்றது
குறிப்பிடத்தக்கது. நேற் றைய ஆட்டத்தில் இலங்கை வீரர் அர்னால்டு 78 ரன்னுடன் இறுதிவரை
ஆட்டம் இழக்காமல் வெற்றிக்கு வழிவகுத்தார். </p><p>
</p><p>
ஸ்கோர்: நியூசிலாந்து-205/8 (ஹாரிஸ்-56, சொய்சா-4/28), இலங்கை-206/7 (அர்னால்டு-
78, ஓரம்-2/20). </p>

<p> </p>

<p>விளையாட்டு மந்திரி
உமாபாரதி அறிவிப்பு: சார்ஜா கிரிக்கெட்டில் இந்தியா பங்கேற்காது</p>

<p> </p>

<p>சென்னை, பிப்.4- குஜராத்தை தாக்கிய பூகம்பம் இந்திய நாட்டையே
கண்ணீர் வடிக்க வைத்துவிட்டது. ஒரு லட்சத்துக்கும் மேற்பட்டவர்கள் கட்டிட இடிபாடுகளில்
சிக்கி இருப்பதாக அஞ்சப்படும் குஜராத் இழப்புக்கு ஈடுகட்டும் வகையில் கண்காட்சி
கிரிக்கெட் போட்டி நடத்த இந்திய போர்டு முடிவு செய்தது. இதன்முதல் கட்டமாக ஆஸ்திரேலிய
அணியை இந்திய போர்டு தொடர்பு கொண்டது. ஆனால் வேறுவிதமாக இந்தியாவுக்கு உதவி
செய்வதாக ஆஸ்திரே லிய போர்டு கூறிவிட்டது. இந்த நிலையில் இந்தியா, பாகிஸ்தான் மற்றும்
வங்கதேச அணிகள் பங்கேற்கும் ஒருநாள் போட் டியை சார்ஜாவில் நடத்தலாம் என்று ஆசிய
கிரிக்கெட் கவுன்சில் கூறியது. </p><p>
</p><p>
இந்திய அணிக்கு தடை </p><p>
</p><p>
இதன்மூலம் மத்திய அர சுக்கு ரூ.20 கோடிவரை கொடுக்கவும் சார்ஜா கிரிக்கெட் கவுன் சில்
உறுதி அளித்தது. இந்தப் போட்டியில் பங்கேற்க மத்திய அரசும் அனுமதி கொடுத்து விட்டதாக
இந்திய கிரிக்கெட் போர்டு அறிவித்தது. இதற்கான இந்திய வீரர்கள் நாளை தேர்வு
செய்யப்படுவார்கள் என்றும் கிரிக்கெட் போர்டு செயலாளர் லீலே நிருபர்களிடம் தெரிவித்
தார். தற்போது எந்தவித அவ சரத்துக்கும் அவசியம் இல்லா மல் போய்விட்டது. சார்ஜா
கிரிக்கெட் போட்டியில் பங்கேற்க இந்திய அணிக்கு மத்திய அரசு தடைவிதித்தது. பாகிஸ்
தான் அணியுடன் எந்தப்போட் டியிலும் பங்கேற்கக்கூடாது என்ற கட்டுப்பாடு இருக்கும்
பட்சத்தில் நிவாரணத்தொகை திரட்டுவதற்காக இந்த முடிவை மாற்ற அரசு தயாராக இல்லை என்று
மத்திய விளையாட்டுத் துறை அறிவித்து விட்டது. </p><p>
</p><p>
மத்திய விளையாட்டுத்துறை மந்திரி உமாபாரதி நேற்று ராமேஷ்வரம் சென்றார். அப்போது அவர்
நிருபர்களுக்கு அளித்த பேட்டி விவரம் வருமாறு:- </p><p>
</p><p>
குஜராத் நிவாரணத்துக் காக ரூ.20 கோடிவரை கொடுக்க முடியும் என்று நம்பிக்கை கொடுத்த
இந்திய கிரிக் கெட் போர்டின் எண்ணத்தை முதலில் நான் பாராட்டு கிறேன். ஆனால் உள்ளூரில்
உள்ள சினிமா நட்சத்திரங்களுடன் கிரிக்கெட் போட்டி நடத்தி னால் ரூ.200 கோடிவரை திரட்ட
முடியும். உண்மையி லேயே நிதி திரட்டும் நல்ல எண்ணத்தில் இந்திய கிரிக்கெட் போர்டு
இந்த முடிவு எடுத்திருந்தால் அதை உள்ளூரில் இருந்து கொண்டே செய்து காட்ட முடியும். இதற்கான
உதவிகளை மத் திய விளையாட்டுத்துறையும் செய்யத்தயாராக உள்ளது. டெண்டுல்கர், கங்குலி
மற்றும் ஹிருத்திக் ரோஷன் ஆகியோர்களை வைத்து கிரிக்கெட் போட்டி நடத்தினால் நிச்சயம்
ரசிகர்கள் மத்தியில் நல்ல வரவேற்பு கிடைக்கும். </p><p>
</p><p>
தவறான செய்தி </p><p>
</p><p>
சோகத்தில் தவிக்கும் குஜராத் மக்களுக்கு உதவி செய்ய இந்தியர்கள் எப்போதும் தயாராக
இருக்கிறார்கள். பாகிஸ்தான் அணியுடன் எந்த உறவும் வைத்துக் கொள்ளக்கூடாது என்று இந்திய
கிரிக்கெட் போர்டுக்கு ஏற்கனவே மத்திய அரசு உத்தரவு போட்டுள்ளது. இதற்கிடையில்
பத்திரிகைகள் வௌியிட்ட தவறான செய்தியால் பெரிய பிரச்சினையே ஏற்பட்டுவிட்டது.
பாகிஸ்தானுக்கு எதி ரான போட்டிகளில் கலந்து கொள்ள வௌியுறவுத்துறையிடமும் அனுமதி
பெறவேண்டிய கட்டாயம் உள்ளது. இந்த விவகாரத்தில் விளையாட்டுத் துறைக்கு தனிப்பட்ட
முறையில் முடிவு எடுக்கும் அதிகாரம் இல்லை. </p><p>
</p><p>
இவ்வாறு அவர் பேட்டி அளித்தார். மத்திய மந்திரி உமாபாரதியின் இந்த அதிரடி பேட்டியால்
இந்திய கிரிக்கெட் போர்டின் அடுத்த முயற்சியும் தவிடு பொடியாகி விட்டது. சூதாட்ட
விவகாரத்துக்குப்பிறகு இந்திய கிரிக்கெட் போர்டுக்கு சோதனை மேல் சோதனை ஏற்பட்டு வருவது
குறிப்பிடத்தக்கது. சென்னை விமான நிலை யத்திலும் இதே கருத்தை வலி யுறுத்தி உமாபாரதி
பேட்டி அளித்தார். </p><p>
</p><p>
</p><p>
</p><p>
உமாபாரதி பேட்டி: சூதாட்ட வீரர்களின் விருதுகள் பறிப்பு </p><p>
</p><p>
சென்னை, பிப்.4- மத்திய விளையாட்டுத்துறை மந்திரி நேற்று மதுரை வந்தார். பின்னர்
டெல்லி திரும்பு வதற்காக சென்னை வந்த போது விமானநிலையத்தில் நிருபர்களை சந்தித்தார்.
அப்போது, சூதாட்டத்தில் சம்பந் தப்பட்ட கிரிக்கெட் வீரர்களின் அர்ஜுனா விருதுகள்
விரைவில் பறிக்கப்படும். இதுகுறித்து மத்திய சட்டத்துறையிடம் தொடர்ந்து ஆலோசனை நடத்தி
வருகிறோம். இது தொடர்பாக புதிய சட்டம் இயற்றவும் பரிந்துரை செய்துள்ளோம் என்று உமா
பாரதி பேட்டி அளித்தார். </p>

<p> </p>

<p>சென்னையில் சர்வதேச
கராத்தே பயிற்சி முகாம்</p>

<p> </p>

<p>சென்னை, பிப்.4- சர்வதேச ஒக்கினாவா கராத்தே மற்றும் கோபுடோ
பயிற்சி முகாம் சென்னை நந்தனம் ஒய்.எம்.சி.ஏ.வில் 3 நாட்கள் நடந்தன. ஒக்கினாவா
கராத்தே பிரிவை சேர்ந்த ஷோரின்ரியு, கோஜுரியு, யுச்சிரியு, இஷின் ரியு, கென்ஷிகாய்,
ஷோகாய் ரியு, ஷோட்டாரியு, ஷோட்டா கான் மற்றும் ரியுகியு ஆகிய கலைகளில் தேர்ச்சி
பெற்ற வல்லுனர்கள் இந்த முகாமில் கலந்து கொண்டனர். டெல்லி, ஆந்திரா, மும்பை உள்பட பல
வௌிமாநிலத்தை சேர்ந்தவர்களும் இந்த முகாமில் பங்கேற்றது குறிப்பிடத்தக்க அம்சம் ஆகும்.
</p><p>
</p><p>
சர்வதேச கராத்தே பயிற்சி முகாம் தென்ஆசியாவில் முதல் முறையாக தற்போதுதான் சென்னையில்
நடத்தப்பட்டது. ஒக்கினாவா கராத்தேயின் உலகத்தலைவர் ஹிரோஷி அக் காமினே இந்த முகாமில்
நேரடியாக கலந்து கொண்டு பங்கேற் பாளர்களுக்கு சிறப்பு பயிற்சி கொடுத்தார். இதன்
இந்தியத் தலைவர் ஏ.எஸ். கிருஷ்ணமூர்த்தியும் பங்கேற்பாளர்களுக்கு பயிற்சி அளித்தார்.
ஆயுதப்படை ஐ.ஜி. முத்துக்கருப்பன் தலைமையில் ஜப்பான் தூதரக உயர் அதிகாரி டெட்சுயோ
யுன்னோ இந்த முகாமை தொடங்கி வைத்தார். முகாமில் மோசஸ் திலக், லயன். பாக்யநாதன்
ஆகியோர்களும் கலந்து கொண்டனர். </p>

<p> </p>

<p>மாநில கல்லூரி தடகளம்:
சுஜாதா-சங்கீதா வெற்றி</p>

<p> </p>

<p>சென்னை, பிப்.4- மாநில பொறியியல் கல்லூரிகளுக்கான தடகளப்
போட்டி சென்னையில் நடந் தது. இதற்கான பெண்கள் ஈட்டி எறியும் போட்டியில் சென்னை
டெக்னாலஜி கல்லூரி வீராங் கனை சுஜாதா 31.68மீ. தொலைவு வீசி தங்கப்பதக்கம்
வென்றார். யூபா (சென்னை) மற்றும் புஸ்பா (சென்னை) ஆகி யோர் அடுத்த இரு இடங்களை
வென்றனர். உயரம் தாண்டு தலில் சங்கீதா 1.59மீ. தாண்டி தங்கப்பதக்கம் கைப்பற்றினார்.
சீத்தல் மற்றும் புனிதா ஆகி யோர் முறையே வௌ்ளி மற்றும் வெண்கலம் பெற்றனர். </p><p>
</p><p>
பி.எஸ்.ஜி. சாம்பியன் </p><p>
</p><p>
பெண்களுக்கான நீளம் தாண்டுதலில் நூசன் ராஜேந்திரன் தங்கமும், மகேஷ்வரி மற்றும் சங்கீதா
வௌ்ளி மற்றும் வெண்கலப்பதக்கமும் வென்றனர். ஆண்கள் பிரிவில் பெலிக்ஸ் டேனியல் (குண்டு
எறிதல்), சதீஸ்குமார் (10ஆயிரம் மீ. ஓட்டம், 5ஆயிரம் மீ. ஓட் டம்), விஜயசந்தர்
(800மீ. ஓட் டம்), சக்தி முருகன் (வட்டு எறிதல்) ஆகியோர் தங்கப்பதக்கம் வென்றனர்.
ஒட்டுமொத்த சாம் பியன் பட்டத்தை பி.எஸ்.ஜி. (கோவை) கல்லூரி 62 புள்ளி களுடன்
பெற்றது. காருண்யா மற்றும் கிண்டி பொறியியல் கல்லூரி அணிகள் 2-வது இடத்தை பகிர்ந்து
கொண்டன. </p>

<p> </p>

<p>பசிபிக் டென்னிஸ்:
இறுதியில் ஹிங்கிஸ்- தேவன் பலப்பரீட்சை</p>

<p> </p>

<p>டோக்கியோ, பிப்.4- பசிபிக் ஓபன் டென்னிஸ் போட்டி
டோக்கியோவில் நடந்து வருகிறது. இதற்கான இறுதிப் போட்டிக்கு உலகத் தர வரிசையில் முதல்
இரு இடங்களை பெற்றுள்ள ஹிங்கிஸ்- தேவன்போர்டு ஆகியோர் மோதஉள்ளனர். நேற்று நடந்த
அரைஇறுதியில் ஹிங்கிஸ் 6-3 1-6 6-0 என்ற செட்கணக்கில் பல்கேரியாவின் மக்டோலனாவை
வீழ்த்தினார். அது போல் தேவன்போர்டு 6-1 5-7 (5-7) 6-0 என்ற புள்ளியில் ரஷிய
வீராங்கனை கவுர்னி கோவாவை தோற்கடித்தார். 16-வது முறையாக ஹிங்கிஸ்- தேவன்போர்டு
ஆகியோர் இறுதிப்போட்டியில் மோதஉள்ளனர். இவர்களிடையே நடந்த ஆட்டத்தில் தேவன்போர்டு
9 முறை வெற்றிபெற்றுள்ளது குறிப்பிடத்தக்கது. 7 முறை ஹிங்கிஸ் வெற்றிபெற்றுள்ளார். </p>

<p> </p>






</body></text></cesDoc>