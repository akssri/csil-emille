<cesDoc id="tam-w-dinakaran-sports-01-10-03" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-01-10-03.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-10-03</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-10-03</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>புதிய தலைவர்
டால்மியாவுக்கு நெருக்கடி: பீகார் சங்க அனுமதி ரத்து? வழக்கு தொடுக்க லல்லு முடிவு</p>

<p> </p>

<p>பாட்னா, அக். 3- பீகார் கிரிக்கெட் சங்க அனுமதி ரத்து
செய்யப்பட்டால் கோர்ட்டில் வழக்குத் தொடருவேன் என்று லல்லு பிரசாத் யாதவ் புதிய
தலைவர் டால்மியாவுக்கு மிரட்டல் விடுத்துள்ளார். </p><p>
</p><p>
இந்திய கிரிக்கெட் போர்டு தலைவராக ஜக்மோகன் டால்மியா கடந்த சில நாட்களுக்கு முன்பு
சென்னையில் தேர்வு செய்யப்பட்டார். புதியதாக தேர்வு செய்யப்பட்ட டால்மியா
கிரிக்கெட் போர்டில் பல அதிரடி மாற்றங்கள் செய்து வருகிறார். முன்னாள் தலைவர்
ஏ.சி.முத்தையா ஆதரவாளர்களாக இருந்த ராஜ்சிங் துங்கர்பூர் மற்றும் சேகர் ஆகியோர்களை
இந்திய கிரிக்கெட்டின் முக்கிய பொறுப்பில் இருந்து நீக்கிவிட்டு தனது ஆதரவாளர்களை அந்த
பதவிகளுக்கு டால்மியா நியமித்து விட்டார். இதனால் மூத்த நிர்வாகிகள் மத்தியில் லேசான
சலசலப்பு ஏற்பட்டது. </p><p>
</p><p>
லல்லு கோபம் </p><p>
</p><p>
இதற்கிடையில் பீகார் கிரிக்கெட் சங்கத்துக்கு உரிமை வழங்கியது குறித்து மறுபரிசீலனை
செய்யப்படும் என்று அதிரடியாக டால்மியா அறிவித்தார். இதனால் பீகார் கிரிக்கெட் சங்கத்
தலைவரும், பீகார் முன்னாள் முதல்வருமான லல்லு பிரசாத் யாதவ் கடும் கோபம் கொண்டார்.
ஏ.சி.முத்தையா தலைவராக இருந்தபோது பீகார் கிரிக்கெட் சங்கத்துக்கு அனுமதி வழங்கப்பட்டது.
தற்போது டால்மியா தலைமை ஏற்றதும் இந்த அனுமதியை மறுபரிசீலனை செய்யப் போவதாக
கூறியுள்ளதை நான் ஏற்க மாட்டேன் என்று லல்லு கூறியுள்ளார். </p><p>
</p><p>
வழக்கு தொடருவேன் </p><p>
</p><p>
இதுதொடர்பாக அவர் கூறு கையில், டால்மியாவின் இந்த நடவடிக்கையால் பழிவாங்கும் போக்கு
நன்றாக தெரிகிறது. சென்னையில் நடந்த தேர்தலின் போது பீகார் கிரிக்கெட் சங்கம்
ஏ.சி.முத்தையாவுக்கு ஆதரவு தெரிவித்தது. இந்த கோபத்தில் தான் டால்மியா இதுபோன்ற
அநாகரீகமான செயலில் ஈடுபட்டுள்ளார். இதுதொடர்பாக நான் கோர்ட்டில் வழக்கு தொடுக்க
உள்ளேன். பீகார் கிரிக்கெட் சங்கத்தின் அனுமதியை ரத்து செய்தால் சமீபத்தில்
பிரிக்கப்பட்ட உத்தரபிரதேசம் மற்றும் மத்திய பிரதேசம் ஆகிய மாநிலங்களின் கிரிக்கெட்
சங்கங்களின் அனு மதியையும் ரத்து செய்ய வேண்டும் என்றார். </p><p>
</p><p>
லல்லுவின் இந்த மிரட்டலுக்கு தற்போதுவரையில் டால்மியா பதில் எதுவும் கொடுக்க வில்லை.
இந்த பிரச்சினை விரைவில் விஸ்வரூபம் எடுக்கும் என்று கிரிக்கெட் வட்டாரங்களில் பேச்சு
நிலவுகிறது. </p>

<p> </p>

<p>போதைமருந்து
உபயோகித்தார்: இந்திய வீராங்கனை ஷீமா வென்ற தங்கம் பறிப்பு</p>

<p> </p>

<p>புதுடெல்லி, அக். 3- போதைமருந்து பயன்படுத்தி இருப்பது கண்டு
பிடிக்கப்பட்டதால் உலக கோப்பை தடகளத்தில் இந்திய வீராங்கனை ஷீமா வென்ற
தங்கப்பதக்கம் பறிக்கப்பட்டது. </p><p>
</p><p>
இந்திய தடகள வட்டாரத்தில் போதைமருந்து உபயோகம் பரவலாக இருப்பதாக சமீபத்தில் குற்றம்
சாட்டப்பட்டது. இது தொடர்பாக மத்திய விளையாட்டு அமைச்சகம் அதிர்ச்சி தரும் வகையில் பல
தகவல்களை வௌியிட்டது. இந்தநிலையில் இந்திய வீராங்கனை ஷீமா அன்டில் போதைமருந்து
பயன்படுத்தியதாக குற்றம் சாட்டப்பட்டுள்ளார். உலக தடகள போட்டி கடந்த அக்டோபர் மாதம்
சிலியில் நடந்தது. இந்தப்போட்டியில் இந்திய வட்டு எறிதல் வீராங் கனை ஷீமா அன்டில்
தங்கப் பதக்கம் வென்று இந்தியாவுக்கு பெருமை சேர்த்தார். </p><p>
</p><p>
இந்தநிலையில் அவர் தடை செய்யப்பட்ட போதைமருந்து பயன்படுத்தி இருப்பது தற்போது கண்டு
பிடிக்கப்பட்டுள்ளது. இதனால் அவர் வென்ற தங்கப்பதக்கம் பறிக்கப்பட்டுள்ளது. ஆனால்
ஷீமாவுக்கு சர்வதேச தடகள சம்மேளனம் தடை எதுவும் அறி விக்க வில்லை. இதுதொடர்பாக இந்த
ஆண்டின் தொடக்கத்தில் இந்திய தடகள சம்மேளனத்துக்கு தகவல் கொடுக்கப்பட்டது. சம்
பந்தப்பட்ட வீராங்கனையிடம் ரகசிய விசாரணை நடத்தி விட்டு தற்போது இந்த தகவல்
அதிகாரபூர்வமாக அறிவிக்கப் பட்டுள்ளது. இதுதொடர்பாக வீராங்கனை ஷீமா கூறுகையில்,
டெல்லியில் இருந்து சிலிக்கு போகும் வழியில் நான் ஜல தோசத்துக்காக மாத்திரை
சாப்பிட்டேன். அந்த மாத்திரையில் தடை செய்யப்பட்ட போதை பொருள் இருந்துள்ளது எனக்கு
தெரியாமல் போய் விட்டது என்றார். </p>

<p> </p>

<p>பயிற்சி கிரிக்கெட்:
தெ.ஆ. லெவனுடன் இந்தியா இன்று மோதல்</p>

<p> </p>

<p>பெனோனி, அக். 3- தென்ஆப்பிரிக்க லெவன் அணிக்கு எதிரான
பயிற்சி கிரிக்கெட் போட்டியில் இந்தியா இன்று மோத உள்ளது. </p><p>
</p><p>
சர்ச்சைகளில் சிக்கித் தவிக்கும் இந்திய காப்டன் கங்குலிக்கு சமீபத்தில் நடந்த
போட்டிகள் அனைத்திலும் தோல்வி மட்டுமே கிடைத்தது. அதுமட்டு மின்றி கங்குலியின்
தனிப்பட்ட பேட்டிங் திறனும் பாதிக்கப்பட்டுள்ளது. இவை அனைத்துக்கும் முற்றுப்புள்ளி
வைக்கும் விதமாக இந்திய அணி தென்ஆப் பிரிக்காவில் சுற்றுப்பயணம் செய்துள்ளது. ஏற்கனவே
தோல்வியில் தவிக்கும் இந்தியா வலுவான தென்ஆப்பிரிக்காவின் தாக்குதலை எப்படி
சமாளிக்கப் போகிறது என்பதே அனைவரின் கவலையாக உள்ளது. </p><p>
</p><p>
3 நாடு கிரிக்கெட் </p><p>
</p><p>
வருகிற 5-ந்தேதி முதல் 3 நாடு கள் மோதும் ஒருநாள் கிரிக்கெட் போட்டியில் இந்தியா
பங்கேற்க உள்ளது. இந்தியா தவிர, கென்யா மற்றும் தென்ஆப்பிரிக்கா அணிகளும் மோதல் களத்
தில் உள்ளன. இந்த மோதலுக்கு முன்பாக பயிற்சி எடுக்கும் வகை யில் தென்ஆப்பிரிக்கா லெவன்
அணியுடன் இந்தியா இன்று மோத உள்ளது. முன்னதாக கண் காட்சி போட்டி ஒன்றில் மோதிய
இந்தியா எந்தவித போராட்ட மும் இன்றி எளிதாக வெற்றிபெற்றது. </p><p>
</p><p>
இன்று பயிற்சி மோதல் </p><p>
</p><p>
ஆனால் இன்றைய மோதல் அவ்வளவு எளிதாக இருக்கும் என்று கூறமுடியாது. ஒருநாள் கிரிக்கெட்
அணியில் இடம் கிடைக்காத இளம் வேகப்பந்து வீச்சாளர்கள் பலரும் இன்றைய மோதலில்
இந்தியாவுக்கு சவாலாக உள்ளனர். எனவே இன் றைய ஆட்டம் இந்தியாவுக்கு நல்ல பயிற்சி
மோதலாக இருக்கும் என்று எதிர்பார்க்கப்படு கிறது. முக்கிய மோதலுக்கு முன் பாக கிடைத்த
இந்த வாய்ப்பை இந்திய வீரர்கள் சரியாக பயன்படுத்திக் கொள்ள வேண்டும். அநேகமாக
இன்றைய ஆட்டத்தில் முன்னணி வீரர்கள் அத்தனை பேரும் இந்திய அணியில் இடம்பிடிப்பார்கள்
என்று நம் பலாம். முன்னதாக நடந்த கண்காட்சி மோதலில் டெண்டுல்கர், திராவிட், தாஸ்
மற்றும் கங்குலி ஆகியோர் சிறப்பாக ரன் குவித்தது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p>கேரம் பந்தயம்:
குபேரன்-பொன்னரசி சாம்பியன்</p>

<p> </p>

<p>சென்னை, அக். 3- சென்னையில் நடந்த கேரம் போட்டியில்
குபேரன்-பொன்னரசி சாம்பியன் பட்டம் வென்றனர். </p><p>
</p><p>
டாக்டர் அம்பேத்கர் பயிற்சி மையத்தின் சார்பில் கேரம் போட்டி சென்னை நேரு
மைதானத்தில் நடந்தது. முன்னணி வீரர்-வீராங்கனைகள் பலரும் கலந்து கொண்ட
இந்தப்போட்டிக்கான ஆண்கள் இறுதி ஆட் டத்தில் சர்வதேச வீரர் மரிய இரு தயம்-குபேரன்
ஆகியோர் மோதினர். முதல் செட்டில் 25-20 என்ற புள்ளியில் குபேரன் வெற்றிபெற்றார்.
அடுத்த செட்டை 25-20 என்ற புள்ளியில் மரிய இருதயம் கைப்பற்றினார். </p><p>
</p><p>
மரிய இருதயம் தோல்வி </p><p>
</p><p>
இதனால் கடைசியாக நடந்த 3-வது செட் பெரிய பரபரப்பை உண்டாக்கியது. முடிவில் குபே ரன்
25-13 என்ற புள்ளியில் 3-வது செட்டில் வெற்றிபெற்றார். இதனால் குபேரன் 25-20 20-25
25-13 என்ற புள்ளியில் மரிய இருதயத்தை தோற்கடித் தார். பெண்கள் ஒற்றையர் ஆட்
டத்தில் பொன்னரசி 17-25 25-7 25-10 என்ற புள்ளியில் ஷர் மிளாவை தோற்கடித்து சாம்
பியன் ஆனார். போட்டி முடி வில் இந்தியன் ஏர்லைன்ஸ் தென் மண்டல இயக்குனர் சந்திரசேகர்
மற்றும் லலிதா சந்திரசேகர் ஆகி யோர் சிறப்பு விருந்தினர்களாக கலந்து கொண்டு பரிசுகள்
வழங் கினர். </p><p>
</p><p>
மற்ற போட்டியின் முடிவுகள் வருமாறு:- </p><p>
</p><p>
சப்-ஜூனியர்: தரணிகுமார்- மனோ ரஞ்சன்: 25-21 25-20 </p><p>
</p><p>
பதக்கம் இல்லாததோர்: ஆனந்தராஜ்-செந்தில்குமார்: 25-10 24-25 25-13 </p><p>
</p><p>
இரட்டையர் மோதல்: மகேஷ் தேவராஜ்:பிரேம் குமார்- டோமினிக் ராஜ்:வெங்கட்ராமன்:
25-21 11-25 25-16. </p>

<p> </p>

<p>குலாம் அகமது டிராபி:
தமிழகம்-கேரளா மோதல் ரத்து</p>

<p> </p>

<p>சென்னை, அக். 3- 19 வயதினோர்களுக்கான குலாம் அகமது டிராபி
கிரிக்கெட் போட்டி சென்னையில் நடந்தது. இதற்கான ஒருநாள் ஆட்டத்தில் தமிழகம்-கேரளா
அணிகள் மோதின. நேற்று முன்தினம் பெய்த மழை காரணமாக ஆடு களத்தில் அதிகமான தண்ணீர்
தேங்கி இருந்தது. இதனால் ஒரு பந்துகூட வீசப்படாத நிலையில் இந்த மோதல் ரத்து
செய்யப்பட்டது. இதன் காரணமாக தமிழகம் மற்றும் கேரளா அணிகளுக்கு தலா 2 புள்ளிகள்
வழங்கப்பட்டன. முன்னதாக இந்த இரு அணிகளும் மோதிய 3 நாள் ஆட்டத்தில் தமிழ்நாடு அணி
இன்னிங்ஸ் மற்றும் 40 ரன் வித்தியாசத்தில் வெற்றிபெற்றது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p> </p>






</body></text></cesDoc>