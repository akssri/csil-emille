<cesDoc id="tam-w-dinakaran-sports-03-01-09" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-03-01-09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-01-09</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-01-09</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>தோல்விக்கு முடிவு கட்டியது
இந்தியா</p>

<p> </p>

<p>வெலிங்டன், ஜன. 9- </p><p>
</p><p>
நியூசிலாந்துக்கு எதிராக இந்தியா முதல்முறையாக வெற்றி பெற்றது. </p><p>
</p><p>
உலக கோப்பை கிரிக்கெட்டுக்கு முன்னதாக இந்தியாவுக்கு ஆறுதல் வெற்றியாவது கிடைக்குமா?
என்று ஏங்கிய ரசிகர்கள் கொஞ்சம் நிம்மதி அடைந்துள்ளனர். நம்பிக்கையுடன்
நியூசிலாந்து பயணத்தை தொடங்கிய இந்திய அணி இதுவரை தோல்வியைத் தவிர பெரிதாக எதையும்
சாதிக்கவில்லை. ஒருசில போட்டிகளில் ஒட்டுமொத்த வீரர்களும் சேர்ந்து 100 ரன்கூட எடுக்க
முடியாமல் தவித்தது இந்திய ரசிகர்களை கவலையில் ஆழ்த்தியது. </p><p>
</p><p>
ஏற்கனவே ஒருநாள் தொடரை பறிகொடுத்த இந்தியா ஆறுதல் வெற்றிக்காக 5-வது போட்டியில்
நியூசிலாந்துடன் ஆடியது. வழக்கம்போல் எதிர்அணி காப்டனுக்கு டாஸ் வெற்றி கிடைத்தது.
பேட்டிங்கை தேர்வு செய்த நியூசிலாந்து காப்டனுக்கு முதல் முறையாக இந்திய அணி நடுக்கம்
கொடுத்தது. ஆக்ரோஷமாக பந்துவீசிய ஜாகீர்கான் அடுத்தடுத்து விக்கெட்டுகளை சாய்த்தார்.
இதனால் 3 ரன் எடுப்பதற்குள் நியூசிலாந்து 3 விக்கெட்டுகளை பறிகொடுத்தது. இந்தியாவுக்கு
ஏற்பட்ட பரிதாபம் நியூசிலாந்து அணியையும் தொற்றிக்கொண்டதில் ஒரு வகையில்
சந்தோஷம்தான். </p><p>
</p><p>
ஆனால் கடைசியில் களம் இறங்கிய கெய்ர்ன்ஸ், ஆடம்ஸ், மெக்குலம் ஆகியோர் இந்திய
பந்துவீச்சாளர்களுக்கு லேசாக நெருக்கடி கொடுத்தனர். இதனால் நியூசிலாந்து தட்டுத் தடுமாறி
168 ரன் பெற்றது. ஜாகீர்கான் மற்றும் நாத் தலா 3 விக்கெட்டுகள் கைப்பற்றினர்.
நெக்ரா, கும்ளே தலா 2 விக்கெட்டுகள் சாய்த்தார். </p><p>
</p><p>
ரன் எடுக்காவிட்டாலும் டெண்டுல்கர் ஆடினால்தான் இந்தியாவுக்கு வெற்றி கிடைக் கும் என்ற
எழுதப்படாத விதி நேற்றைய ஆட்டத்தில் நிரூபிக்கப்பட்டது. முதல் 4 மோதல்களிலும் காயம்
காரணமாக ஓய்வு எடுத்த டெண்டுல்கர் நேற்றைய மோதலில் தெம்புடன் களம் இறங்கினார்.
வித்தியாசமான செய்கையால் ரசிகர்களை உற்சாகம் கொள்ள வைத்த நியூசிலாந்து நடுவர்
போடவுன், எதிர்பாராத விதமாக டெண்டுல்கருக்கு கொடுத்த அவுட் அனைவரையும் அதிர்ச்சியில்
உறைய வைத்தது. மட்டையில் பட்ட பந்து பின்னர் டெண்டுல்கரின் கால் தடுப்பில் பட்டது.
ஆனால் உணர்ச்சிவயத்தின் உச்சத்தில் இருந்த நடுவர் டெண்டுல்கருக்கு தவறான தீர்ப்பு
கொடுத்து விட்டார். இதனால் டெண்டுல்கர் ரன் எடுக்காமல் பெவிலியன் திரும்பினார். </p><p>
</p><p>
ஷேவாக், யுவ்ராஜ் ஆகியோர் நிலைமையை புரிந்து கொண்டு சிறப்பாக ஆடிய நிலையில்
ஜாகீர்கான் கடைசி கட்டத்தில் 34 ரன் குவித்து இந்தியாவின் வெற்றியை உறுதி செய்தார். இதன்
காரணமாக அவர் ஆட்டநாயகனாகவும் தேர்வு செய்யப்பட்டார். இதன் மூலம் இந்தியா தனது
தோல்வி பயணத்துக்கு தற்காலிக முடிவு கட்டியது. வருகிற 11-ந்தேதி அடுத்த ஆட்டம் நடக்க
உள்ளது. </p><p>
</p><p>
காப்டன் கங்குலி சொல்கிறார்: பேட்டிங்கில் இன்னும் சாதிக்க வேண்டும் </p><p>
</p><p>
நியூசிலாந்துக்கு எதிரான ஒருநாள் கிரிக்கெட்டில் முதல்முறையாக இந்தியா வெற்றிபெற்றது.
சோகத்தில் உச்சத்தில் தவித்த இந்திய அணிக்கு இந்த வெற்றி லேசான ஆறுதல் அளித்துள் ளது.
வழக்கம்போல் காப்டன் கங்குலி ரன் கணக்கை தொடங்கும் முன்பே பெவிலியன் திரும்பினார்.
ஆட்டத்தின் முதல் பந்தை எதிர்கொண்ட கங்குலி விக்கெட் கீப்பரிடம் எளிதாக காட்ச்
கொடுத்து ஆட்டம் இழந்தார். இதனால் கங்குலி 5 இன் னிங்சில் மொத்தம் 20 ரன்கள் எடுத்து
விமர்சனங்களை அள்ளிக்குவித்துள்ளார். இந்தநிலையில் கங்குலி நிருபர்களிடம் கூறுகையில்,
இந்த வெற்றி திருப்தி அளிக்கும் வகையில் உள்ளபோதும், இந்திய பேட்ஸ்மென்கள் இன்னும்
சாதிக்க வேண்டும். முதலில் பந்துவீசியது ஒருவகையில் இந்தியாவுக்கு நல்லதாக அமைந்தது.
ஆட்டத்தின் முதல் ஒருமணிநேரம் வேகப்பந்து வீச்சாளர்களுக்கு சாதகமாக இருந்தது. யுவ்ராஜ்
சிங் இந்தியாவுக்கு கைகொடுத்தது குறிப்பிடத்தக்கது என்றார். </p>

<p> </p>

<p> </p>

<p> </p>

<p>இந்திய வீரர்கள் சம்பளம்
பாதியாக குறைப்பு</p>

<p> </p>

<p>கொல்கத்தா, ஜன. 9- இந்திய வீரர்களின் மோசமான ஆட்டத்தை
தொடர்ந்து அவர்களின் சம்பளம் பாதியாக குறைக்கப்பட்டுள்ளது. </p><p>
</p><p>
நியூசிலாந்தில் சுற்றுப்பயணம் செய்துள்ள இந்திய கிரிக்கெட் அணி டெஸ்ட் மற்றும் ஒருநாள்
தொடரை இழந்தது. இந்திய வீரர்களின் மோசமான ஆட்டம் பலதரப்பட்ட விமர்சனங்களை
உண்டாக்கியது. இந்தநிலையில் மோசமான ஆட்டம் காரணமாக இந்திய வீரர்களின் சம்பளம்
பாதியாக குறைக்கப்பட்டுள்ளது. இந்திய கிரிக்கெட் போர்டு பொருளாளர் ரங்தா கூறுகையில்,
இந்திய கிரிக்கெட் போர்டு சமீபத்தில் புதிய விதிஒன்றை அறிமுகம் செய்தது. அதில்
வீரர்களின் ஆட்டத்துக்கு ஏற்றாற் போல் சம்பளம் வழங்கப்படும் என்ற விதி உள்ளது.
நியூசிலாந்தில் படுமோசமாக ஆடி தோல்வியைத்தழுவி வருவதால் வீரர்களுக்கு சம்பளம் பாதியாக
குறைக்கப்பட்டுள்ளது என்றார். இந்த முடிவுக்கு இந்திய வீரர்கள் தரப்பில் எந்த
எதிர்்ப்பும் தெரிவிக்கப்படவில்லை. காரணம் இங்கிலாந்து பயணத்தில் ஒரு நாள் தொடரை
வென்ற இந்திய அணிக்கு ரூ.1 கோடி போனஸ் வழங்கப்பட்டது குறிப்பிடத்தக்கது. தற்போது
டெஸ்ட் போட்டிக்கு தலா ரூ.2.5 லட்சமும், ஒருநாள் போட்டிக்கு ரூ.2.25 லட்சமும் வழங்கப்பட்டு
வருவது நினைவு கூறத்தக்கது. </p>

<p> </p>

<p> </p>

<p>ஆஸி. காப்டன் பாண்டிங்
சொல்கிறார்: முரளிதரன் சிறந்த பந்துவீச்சாளர்</p>

<p> </p>

<p>சிட்னி, ஜன. 9- முரளிதரன் சிறந்த பந்துவீச்சாளர் என்று
ஆஸ்திரேலிய காப்டன் பாண்டிங் கூறினார். </p><p>
</p><p>
3 நாடுகள் ஒருநாள் கிரிக்கெட் போட்டிக்கான இன்றைய ஆட்டத்தில் ஆஸ்திரேலியா-இலங்கை
அணிகள் மோத உள்ளன. ஆஸ்திரேலிய அணியில் 3 வேகப்பந்து வீச்சாளர்கள் காயம் காரணமாக
அவதிப்பட்டு வருகிறார்கள். இந்தநிலையில் பாண்டிங் நிருபர்களிடம் கூறுகையில், இலங்கை
அணியின் முரளி தரன் உலகின் தரம் வாய்ந்த சுழல்பந்து வீச்சாளர். அவரை சமாளிப்பது சாதாரண
விஷயம் இல்லை என்றார். காயம் காரணமாக ஓய்வில் இருந்த முரளிதரன் முதல்முறையாக இன் றைய
ஆட்டத்தில் களம் இறங்க காத்திருக்கிறார். கடந்த சில வாரங்களுக்கு முன்பு முரளிதரனை
கண்டபடி பேசிய ஆஸ்தி ரேலிய வீரர்களுக்கு தற்போது பயம் அதிகமாகி உள்ளது. தோல்விக்கு
பதிலடி கொடுக்க இலங்கை காத்திருக்கிறது. </p>

<p> </p>

<p> </p>

<p>தேசிய கைப்பந்து: தமிழக
அணி தேர்வு</p>

<p> </p>

<p>சென்னை, ஜன. 9- தேசிய ஜூனியர் சாம்பியன் கோப்பைக்கான
கைப்பந்து போட்டிக்கு தமிழக அணி அறிவிக்கப்பட்டுள்ளது. </p><p>
</p><p>
தேசிய ஜூனியர் சாம்பியன் கோப்பைக்கான கைப்பந்து போட்டி ஆந்திராவில் நாளை முதல்
நடக்க உள்ளது. இந்தப் போட்டியில் கலந்து கொள்ளும் தமிழக அணி தேர்வு செய்யப்பட்டது.
ஆண்கள் பிரிவில், பிரபு, தவச்செல்வன், செல்டன்மோசஸ், சதீஷ்குமார், மணிகண்டன்,
சஞ்சய், கலைச்செல்வன், கார்த்திகேயன், ஜெயக்குமார், சர வணன், அப்துல்லா,
கலைவாணன் ஆகியோர் இடம் பெற்றுள்ளனர். பெண்கள் பிரி வில், கீதா, பிரமிளா, ஷர்மிளா,
மணிமேகலை, ஜெரிதா, அருணா, சபர்மதி, கார்த்திகா, சுஜாதா, ஆர்த்தி, ஷிமிலால் சிங்,
ஜெய ஆகியோர்களுக்கு வாய்ப்பு கொடுக்கப்பட்டுள்ளது. </p>

<p> </p>

<p> </p>

<p>சென்னையில் தேசிய செஸ்</p>

<p> </p>

<p>சென்னை, ஜன. 9- தேசிய அளவிலான செஸ் பந்தயம் சென்னையில் வருகிற
11 மற்றும் 12 ஆகிய 2 தேதிகளில் நடக்க உள்ளது. </p><p>
</p><p>
தேசிய அளவிலான செஸ் பந்தயம் சென்னை தாம்பரத்தில் உள்ள சாய்ராம் பொறியியல்
கல்லூரியில் வருகிற 11 மற்றும் 12 ஆகிய 2 தேதிகளில் நடக்க உள்ளது. தமிழ்நாடு, ஆந்திரா,
கர்நாடகம், கேரளா, புதுவை ஆகிய மாநிலங்களில் இருந்து வீரர்-வீராங்கனைகள் இந்தப்
போட்டியில் கலந்து கொள்கிறார்கள். வருகிற 11-ந்தேதி நடக்கும் தொடக்க விழாவில்
முன்னாள் இந்திய கிரிக்கெட் வீரர் ராபின்சிங் சிறப்பு விருந்தினராக கலந்து
கொள்கிறார். செஸ் கிராண்ட்மாஸ்டர் பட் டம் வென்றுள்ள இளம் வீராங்கனை ஆர்த்தியும்
விழாவில் பங்கேற்க உள்ளார். நிறைவு நாளில் முன்னாள் இந்திய வீரர் சந்திரசேகர்
பரிசுகள் வழங்குகிறார். போட்டிக்கான ஏற்பாடுகளை சாய்ராம் பொறியியல் கல்லூரி தாளாளர்
லியோமுத்து செய்துள்ளார். கல்லூரி முதல்வர் சீதாராமன் ஒரு செய்திக்குறிப்பில் இந்தத்
தகவல்களை தெரிவித்துள்ளார். </p>

<p> </p>

<p> </p>

<p>கல்லூரி கால்பந்து</p>

<p> </p>

<p>சென்னை, ஜன. 9- கல்லூரி அணிகளுக்கான அகில இந்திய கால்பந்து
போட்டி சென்னையில் வருகிற 26-ந்தேதி முதல் நடக்க உள்ளது. </p><p>
</p><p>
கல்லூரி அணிகளுக்கான அகில இந்திய கால்பந்து போட்டி சென்னை லயோலா கல்லூரி சார்பில்
வருகிற 26-ந்தேதி முதல் நடக்க உள்ளது. தமிழ்நாடு, கேரளா, ஆந்திரா, கர்நாடகம் ஆகிய
மாநிலங்களை சேர்ந்த கல்லூரி அணிகள் இந்தப்போட்டியில் கலந்து கொள்கின்றன.
போட்டியில் சேர நாளை கடைசிநாள் ஆகும். மேலும் விவரங்களுக்கு, உடற்கல்வி இயக்குனர்,
லயோலா கல்லூரி, சென்னை- 34 என்ற முகவரியில் தொடர்பு கொள்ள வேண்டும். </p>

<p> </p>






</body></text></cesDoc>