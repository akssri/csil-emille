<cesDoc id="tam-w-dinakaran-sports-03-04-03" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-03-04-03.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-04-03</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-04-03</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>இந்திய கிரிக்கெட்:
கங்குலி காப்டன்-ஷேவாக் துணை காப்டன் 4 புதிய வீரர்களுக்கு அணியில் வாய்ப்பு </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>கங்குலி </p>

<p> </p>

<p>மும்பை, ஏப். 3- வங்கதேசத்தில் நடக்கும் 3 நாடு கிரிக்கெட்
போட்டிக்கான இந்திய அணி நேற்று அறிவிக்கப்பட்டது. ஓய்வு கேட்ட டெண்டுல்கர், திராவிட்,
நெக்ரா, நாத், கும்ளே ஆகியோர் நீக்கப்பட்டனர். இவர்களுக்குப் பதிலாக இளம்
ஆட்டக்காரர்கள் 5 பேர் புதியதாக சேர்க்கப்பட்டுள்ளனர். இவர்களில் 4 பேர் முதல்முறையாக
இந்திய அணியில் வாய்ப்பு பெற்றிருக்கிறார்கள். நீண்ட இடை வௌிக்குப்பின்னர் சரண்
தீப் சிங் அணியில் சேர்க்கப்பட்டுள்ளார். </p><p>
</p><p>
லட்சுமண் தலைமையி லான இந்திய ஏ அணி மேற்கு இந்தியதீவில் சுற்றுப்பயணம் செய்துள்ளது. அங்கு
நடக்கும் போட்டியில் சிறப்பாக ஆடிவரும் கவுதம் கேம்பீர், அவிஸ்கர் சால்வி, அமித்
மிஸ்ரா, அபிஜித் கேல் ஆகியோர் அணியில் வாய்ப்பு பெற்றனர். அதிரடி வீரரான
கவுதம்கேம்பீர் மேற்கு இந்தியதீவில் 617 ரன்கள் விளாசினார். இதனால் அவரின் தேர்வு
சந்தேகத்துக்கு இடமின்றி உறுதியானது. டாக்கா கிரிக்கெட் போட்டியில் அவர் தொடக்க வீரராக
களம் இறங்க வாய்ப்பு உள்ளது. </p><p>
</p><p>
டெண்டுல்கர் அணியில் இடம்பிடிக்காத காரணத்தால் ஷேவாக்-கேம்பீர் ஜோடி ஆட் டத்தை
தொடங்கும் என்று நம்பலாம். ஆனால் காப்டன் கங்குலி மீண்டும் தொடக்க வீரர் வாய்ப்பின்
மீது ஒரு கண் வைத்துள்ளதால், கேம்பீர் 3-வது இடத்துக்குத் தள்ளப்படவும் வாய்ப்பு உள்ளது.
மேற்கு இந்தியதீவு அணியுடன் அசாத்திய சாதனைகள் செய்திருக்கும் வேகப்பந்து வீச்சாளர்
அவிஸ்கர் சால்வி இந்திய அணிக்கு கிடைத்துள்ள சொத்து என்று ஏ அணி பயிற்சியாளர் அசோக்
மல்கோத்ரா தெரிவித்தார். </p><p>
</p><p>
சமீபத்தில் நடந்து முடிந்த உலக கோப்பையில் ஒரு போட்டியில்கூட ஆடாத அகர்கர், பார்த்தீவ்
பட்டேல், சஞ்சய்பாங்கர் ஆகியோர்கள் மீண்டும் தேர்வு செய்யபட்டுள்ளனர்.
இந்தியாவின் வெற்றிக்கு பெரிய உதவிகள் எதையும் செய்யாத போதும் வெற்றிகள் குவித்த
இந்திய அணியில் இடம்பெற்றிருந்த தினேஷ் மொங்கியாவுக்கு தொடர்ந்து வாய்ப்பு
கொடுக்கப்பட்டுள்ளது. காயம் காரணமாக அவதிப்பட்டு வருவதால் லட்சுமண் மற்றும்
முரளிகார்த்திக் ஆகியோர்களின் பெயர்கள் பரிசீலனை செய்யப்படவில்லை. </p><p>
</p><p>
கடைசி நேரத்தில் தனது முடிவை மாற்றிக்கொண்டதால் இந்தப்போட்டிக்கு கங்குலி காப்டனாக
தேர்வு செய்யப்பட்டுள்ளார். அதிரடி ஆட்டக்காரர் ஷேவாக் துணை காப்டனாக நியமிக்கப்பட்டு
இருக்கிறார். இந்திய வீரர்கள் தேர்வு குறித்து கிரிக்கெட் போர்டு செயலாளர் எஸ்.கே.
நாயர் கூறும்போது, மேற்கு இந்தியதீவில் இளம் வீரர்களின் திறமை அடிப்படையில் வாய்ப்பு
கொடுக்கப்பட்டுள்ளது. டெண்டுல்கரின் கையில் லேசான காயம் ஏற்பட்டுள்ளது. இதற்காக அவர்
குறைந்த பட்சம் 6 வாரங்கள் ஓய்வு எடுத்தாக வேண்டிய நிர்ப்பந்தம் ஏற்பட்டுள்ளது. நாத்
ஓய்வு கேட்டார். சொந்த காரணங்களுக்காக கும்ளே விலகி விட்டார். நெக்ராவும் காயத்தால்
விலகிக்கொண்டார் என்றார். </p><p>
</p><p>
வருகிற 11-ந்தேதி முதல் நடக்கும் இந்தப்போட்டியில் இந்தியா, வங்கதேசம் மற்றும்
தென்ஆப்பிரிக்கா ஆகிய 3 அணிகள் கலந்து கொள்கின்றன. இந்தப்போட்டிக்கான இந்திய அணி
வருகிற 9-ந்தேதி கொல்கத்தாவில் இருந்து கிளம்பி டாக்கா செல்கிறது. டாக்கா ஆடுகளங்களை
கருத்தில் கொண்டு சுழல்பந்து வீச்சாளர்கள் அதிகமாக தேர்வு செய்யப்பட்டுள்ளதாக எஸ்.கே.
நாயர் தெரிவித்தார். தற்போது இந்திய அணியில் ஹர்பஜன், சரண்தீப்சிங் ஆகியோர் தவிர
ஷேவாக், தினேஷ், யுவ்ராஜ் ஆகியோர்களும் சுழல்பந்து வீசும் தகுதி பெற்றவர்கள் என்பது
குறிப்பிடத்தக்கது. பார்த்தீவ் மட்டுமே விக்கெட் கீப்பர் என்பதால் அவருக்கு நிச்சயம்
இந்த முறை அணியில் களம் இறங்கும் வாய்ப்பு கிடைக்கும். ஜாகீர்கான், அகர்கர், சால்வி
ஆகிய 3 வேகப்பந்து வீச்சாளர்கள் அணியில் இடம்பெற்றிருக்கிறார்கள். உலக கோப்பையில்
வாய்ப்பு பெறாத அகர்கர் இந்த முறை களம் இறங்கும் வாய்ப்பு பிரகாசமாகி இருக்கிறது. </p><p>
</p><p>
இந்திய அணி </p><p>
</p><p>
சவுரவ் கங்குலி (காப்டன்) </p><p>
</p><p>
ஷேவாக் (து.காப்டன்) </p><p>
</p><p>
முகமது காய்ப் </p><p>
</p><p>
யுவ்ராஜ்சிங் </p><p>
</p><p>
தினேஷ் மொங்கியா </p><p>
</p><p>
கவுதம் கேம்பீர் </p><p>
</p><p>
பார்த்தீப் பட்டேல் </p><p>
</p><p>
ஹர்பஜன்சிங் </p><p>
</p><p>
சரண்தீப்சிங் </p><p>
</p><p>
அஜித் அகர்கர் </p><p>
</p><p>
அவிஸ்கர் சால்வி </p><p>
</p><p>
அமித் மிஸ்ரா </p><p>
</p><p>
சஞ்சய் பாங்கர் </p><p>
</p><p>
அபிஜித் கேல் </p><p>
</p><p>
ஜாகீர்கான் </p>

<p> </p>

<p> </p>

<p>இந்திய கிரிக்கெட் அணிக்கு
ஜனாதிபதி விருந்தளிக்கிறார் </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>அப்துல் கலாம் </p>

<p> </p>

<p>இந்திய கிரிக்கெட் அணிக்கு ஜனாதிபதி அப்துல் கலாம் தனது
மாளிகையில் வருகிற 7-ந்தேதி விருந்தளிக்க உள்ளார். </p><p>
</p><p>
சமீபத்தில் நடந்து முடிந்த உலக கோப்பை கிரிக்கெட்டில் இந்தியாவுக்கு சாம்பியன் பட்டம்
கிடைக்காவிட்டாலும், கணிசமான வெற்றிகளை குவித்தது. அடுத்தடுத்து 8 போட்டிகளில் வெற்றி பெற்று
சாதனை படைத்த இந்தியா இறுதிப்போட்டியில் 125 ரன் வித்தியாசத்தில் ஆஸ்திரேலியாவிடம்
தோற்றது. 20 ஆண்டு இடைவௌிக்குப்பின்னர் இறுதிப்போட்டிக்கு முன்னேறிய இந்திய கிரிக்கெட்
அணிக்கு ஜனாதிபதி அப்துல் கலாம் தனது மாளிகையில் வருகிற 7-ந்தேதி விருந்தளிக்க உள்ளார்.
உலக கோப்பையில் ஆடிய 15 வீரர்களும் இந்த விருந்தில் கலந்துகொள்ள உள்ளதாக இந்திய
கிரிக்கெட் போர்டு செயலாளர் நாயர் தெரிவித்தார். இந்திய கிரிக்கெட் வீரர்கள் அனை
வருக்கும் பல லட்சத்துக்கு கிரிக்கெட் போர்டு சம்பள பாக்கி வைத்துள்ளது. டாக்கா
கிரிக்கெட் போட்டிக்கு இந்திய அணி கிளம்புவதற்கு முன்பாக அனைத்து பாக்கியும்
அடைக்கப்படும் என்று நாயர் தெரிவித்தார். உலக கோப்பை மற்றும் பல சர்வதேச போட்டிகளில்
ஆடியதற்காக ஷேவாக், திராவிட் ஆகியோர்களுக்கு ரூ.1 கோடிக்கும் அதிகமாக இந்திய
கிரிக்கெட் போர்டு சம்பள பாக்கி வைத்துள்ளது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p> </p>

<p>கபில்தேவ் குற்றச்சாட்டு:
அடுத்தடுத்து போட்டியில் கிரிக்கெட் வீரர்கள் காயம் </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>கபில்தேவ் </p>

<p> </p>

<p>அடுத்தடுத்து போட்டியில் கிரிக்கெட் வீரர்கள் பங்கேற்பதால்
காயம் ஏற்படுகிறது என்று இந்திய முன்னாள் காப்டன் கபில்தேவ் குற்றஞ்சாட்டியுள்ளார்.
</p><p>
</p><p>
உலக கோப்பை கிரிக்கெட் முடிந்த கையோடு இந்திய அணி டாக்கா சுற்றுப்பயணம் மேற்கொள்ள
உள்ளது. இந்தப்போட்டிக்கான இந்திய அணியில் சீனியர் வீரர்கள் பலரும் காயம் காரணமாக
விலகிவிட்டனர். தொடர்ந்து பல போட்டிகளில் ஆடுவதால்தான் வீரர்கள் காயத்தால்
அவதிப்படுவதாக முன்னாள் காப்டன் கபில் தேவ் குற்றம் சாட்டி உள்ளார். இது தொடர்பாக அவர்
கூறும்போது, இந்திய கிரிக்கெட் வீரர்கள் தொடர்ந்து ஒரு ஆண்டு முழுவதும் இடைவௌி இன்றி
ஆடுகிறார்கள். வீரர்களின் காயத்துக்கு இதுதான் முக்கிய காரணம். உலக கோப்பை முடிந்ததும்
வீரர்கள் அனைவருக்கும் கண்டிப்பாக ஓய்வு கொடுத்திருக்க வேண்டும். ஆனால் டாக்கா
போட்டிக்கு பாதி வீரர்கள் தயாராகி விட்டனர். வீரர்களை சில காலம் தங்களின்
குடும்பத்தினருடன் செலவிட அனுமதிக்க வேண்டும். இந்திய கிரிக்கெட் போர்டு இதைப்பற்றி
யோசிக்க வேண்டும் என்றார். </p>

<p> </p>

<p> </p>

<p>தெ.ஆ வீரர் காலிஸ் திடீர்
விலகல் </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>காலிஸ் </p>

<p> </p>

<p>இந்தியா, தென்ஆப்பிரிக்கா மற்றும் வங்கதேசம் ஆகிய 3 அணிகள்
மோதும் ஒருநாள் கிரிக்கெட் போட்டியில் இருந்து தென்ஆப்பிரிக்காவின் முன்னணி
ஆட்டக்காரர் காலிஸ் அணியில் இருந்து விலகினார். </p><p>
</p><p>
இந்தியா, தென்ஆப்பிரிக்கா மற்றும் வங்கதேசம் ஆகிய 3 அணிகள் மோதும் ஒருநாள்
கிரிக்கெட் போட்டி வருகிற 11-ந்தேதி முதல் டாக்காவில் நடக்க இருக்கிறது.
இந்தப்போட்டியில் கலந்து கொள்ளும் வீரர்கள் பட்டியல் அறிவிக்கப்பட்டு விட்டது.
இந்தநிலையில் தென்ஆப்பிரிக்காவின் முன்னணி ஆட்டக்காரர் காலிஸ் அணியில் இருந்து
விலகினார். தனது பெற்றோர் உடல்நலக்குறைவால் பாதிக்கப்பட்டுள்ளதால் அணியில் ஆட
முடியவில்லை என்று காலிஸ் தரப்பில் விளக்கம் கொடுக்கப் பட்டுள்ளது. ஆனால் அவர் நேரடியாக
எதுவும் தெரிவிக்க வில்லை. உலககோப்பை போட்டியில் காலிஸ் பெரிய அளவில்
சாதிக்கவில்லை. இதனால் அவர் மீது பல்வேறு வகையான விமர்சனங்கள் எழுந்தன. காலிசின்
தற்போதைய வில கலுக்கு அதுவும் ஒரு காரணமாக இருக்கலாம் என்று தெரிகிறது. தென்ஆப்பிரிக்கா
கிரிக்கெட் போர்டு நிர்வாகி மஜோலா கூறும்போது, எப்போது விருப்பப்பட்டாலும் காலிஸ்
அணியில் சேர்ந்து கொள்ளலாம் என்றார். </p>

<p> </p>

<p> </p>

<p>தாயகம் திரும்பினார்
மெக்ராத் </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>மெக்ராத் </p>

<p> </p>

<p>ஆஸ்திரேலிய அணியின் வேகப்பந்து வீச்சாளர் மெக்ராத் சொந்த
காரணங்களுக்காக தாயகம் திரும்பி உள் ளார். </p><p>
</p><p>
ஸ்டீவ்வாக் தலைமையிலான ஆஸ்திரேலிய கிரிக்கெட் அணி 4 டெஸ்ட் போட்டியில் ஆடுவதற்காக
மேற்கு இந்தியதீவு வந்துள்ளது. தொடர்ந்து 2-வது முறையாக உலக கோப்பை வென்று சாதனை படைத்த
ஆஸ்திரேலியா டெஸ்ட் தொடரை கைப்பற்றி சர்வதேச தரவரிசையில் முதலிடத்தை எட்டும்
நோக்கில் காத்திருக்கிறது. ஆஸ்திரேலியாவின் நினைப்பில் தற்போது பின்னடைவு உண்டாகி
இருக்கிறது. வேகப்பந்து வீச்சாளர் மெக்ராத் சொந்த காரணங்களுக்காக தாயகம் திரும்பி உள்
ளார். மெக்ராத்தின் மனைவி உடல்நலக்குறைவு காரணமாக சிகிச்சை பெற்று வருகிறார். இதற்காக
மெக்ராத் தாயகம் திரும்ப கோரிக்கை வைத்தார். இந்த கோரிக்கையை ஏற்று அவர் தாயகம்
திரும்ப அனுமதி வழங்கப்பட்டது. இவருக்குப்பதிலாக மாற்று வீரர் அறிவிக்கப்படுவாரா?
என்பது குறித்து விரைவில் தகவல் வௌியிடப்படும் என்றும் ஆஸ்திரேலிய கிரிக்கெட் போர்டு
தகவல் தெரிவித்துள்ளது. </p>

<p>  </p>

<p> </p>

<p>இலங்கை பயிற்சியாளர் பதவி
ஏற்க ஊல்மர் மறுப்பு </p>

<p> </p>

<p>இலங்கை அணிக்கு பயிற்சியாளர் பதவி ஏற்க பாப் ஊல்மர் மறுப்பு
தெரிவித்துள்ளார். </p><p>
</p><p>
உலக கோப்பை கிரிக்கெட் போட்டியின் அரைஇறுதி வரை முன்னேறிய இலங்கை அணி புதிய
பயிற்சியாளரை நியமிக்க முடிவு செய்துள்ளது. இதற்காக உலக கோப்பைக்கு பயிற்சியாளராக
இருந்த வாட்மோர் நீக்கப்பட்டார். இலங்கை அணி பயிற்சியாளர் பதவியை ஏற்கும்படி பாப்
ஊல்மரிடம் கேட்கப்பட்டது. ஆனால் இலங்கை கிரிக்கெட் போர்டின் கோரிக்கையை ஊல்மர்
மறுத்தார். ஊல்மர் தென்ஆப்பிரிக்கா அணிக்கு பயிற்சியாளராக இருந்தவர் என்பது
குறிப்பிடத்தக்கது. இதையடுத்து ஆஸ்திரேலியாவின் ஸ்டீவ் ரிக்சன் மற்றும் நியூசிலாந்தின்
ஜான் பிரேஸ்வெல் ஆகியோர்களிடம் பயிற்சியாளர் பதவி தொடர்பாக பேச்சுவார்த்தை நடந்து
வருகிறது. </p>

<p> </p>

<p> </p>

<p>சார்ஜா: பாக்.-ஜிம்ப்.
இன்று மோதல் </p>

<p> </p>

<p>சார்ஜா கிரிக்கெட் போட்டியின் இன்றைய முதல்ஆட்டத்தில்
பாகிஸ்தான்-ஜிம்பாப்வே அணிகள் பலப்பரீட்சை நடத்த உள்ளன. </p><p>
</p><p>
4 நாடுகள் மோதும் ஒருநாள் கிரிக்கெட் போட்டி இன்று முதல் சார்ஜாவில் நடக்க இருக்கிறது.
இந்தப்போட்டிக்கான இன்றைய முதல்ஆட்டத்தில் பாகிஸ்தான்-ஜிம்பாப்வே அணிகள்
பலப்பரீட்சை நடத்த உள்ளன. உலக கோப்பையின் தோல்வி காரணமாக பாகிஸ்தான் அணியின்
சீனியர் வீரர்கள் அனைவரும் நீக்கப்பட்டு விட்டனர். இளம் வீரர்களுடன் களம் இறங்கி
இருக்கும் பாகிஸ்தான் அணி இந்தப்போட்டியில் சாதிக்க வேண்டிய கட்டாயத்தில்
காத்திருக்கிறது. முன்னணி வீரர் ஆன்டி பிளவர் விலகிவிட்டதால் சார்ஜா கிரிக்கெட்டில்
ஜிம்பாப்வே கடும் போராட்டத்தை சந்திக்கும் என்பதில் சந்தேகம் இல்லை. இந்த ஆட்டம்
டென்ஸ்போர்ட்ஸ் அலைவரிசையில் மாலை 4 மணி முதல் நேரடியாக ஒளிபரப்பு செய்யப்படுகிறது.
கென்யா, இலங்கை அணிகளும் இந்தப்போட்டியில் கலந்து கொள்கின்றன. </p>

<p> </p>

<p> </p>

<p>நியூசிலாந்து வீரர்
மெக்மில்லன் நீக்கம் </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>மெக்மில்லன் </p>

<p> </p>

<p>ஆக்லாந்து, ஏப். 3- இலங்கையில் சுற்றுப்பயணம் செய்து டெஸ்ட்
போட்டிகளில் மோத உள்ள நியூசிலாந்து அணியில் நம்பிக்கை ஆட்டக்காரர் மெக்மில்லன்
நீக்கப்பட்டுள்ளார். </p><p>
</p><p>
நியூசிலாந்து கிரிக்கெட் அணி இலங்கையில் சுற்றுப்பயணம் செய்து டெஸ்ட் போட்டிகளில் மோத
உள்ளது. இந்தப்போட்டிக்கான நியூசிலாந்து அணியில் நம்பிக்கை ஆட்டக்காரர்
மெக்மில்லனுக்கு வாய்ப்பு கொடுக்கப்படவில்லை. உலக கோப்பை போட்டியின்போது தனது
கிடைத்த வாய்ப்பை சரியாக பயன்படுத்திக்கொள்ளாத காரணத்தால் மெக்மில்லன்
நீக்கப்பட்டதாக நியூசிலாந்து தேர்வுக்குழுத் தலைவர் ஹாட்லி விளக்கம் அளித்தார்.
இவருக்குப் பதிலாக மேத்யூ ஹார்ன் அணியில் சேர்க்கப்பட்டுள்ளார். சுழல்பந்து வீச்சாளர்
வைஸ்மென்னுக்கும் வாய்ப்பு கொடுக்கப்பட்டுள்ளது. காயம் காரணமாக தொடக்க வீரர் நாதன்
ஆஸ்லே அணியில் இருந்து விலகியது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p> </p>

<p>மலேசிய ரேசுக்கு அர்மான்
தேர்வு </p>

<p> </p>

<p>சென்னை, ஏப். 3- சர்வதேச வீரர்கள் கலந்துகொள்ளும் கோ-கார்ட்டிங்
ரேஸ் போட்டியில் சென்னை வீரரான அர்மான் ஜூனியர் பிரிவில் தேர்வு செய்யப்பட்டு
இருக்கிறார். </p><p>
</p><p>
சர்வதேச வீரர்கள் கலந்துகொள்ளும் கோ-கார்ட்டிங் ரேஸ் பந்தயம் மலேசியாவில் நடக்க
உள்ளது. 5 சுற்றுகள் கொண்ட இந்தப்போட்டியில் கலந்து கொள்ள அர்மான் இப்ராகீம் தேர்வு
செய்யப்பட்டுள்ளார். சென்னை வீரரான அர்மான் ஜூனியர் பிரிவில் தேர்வு செய்யப்பட்டு
இருக்கிறார். முன்னாள் சர்வதேச கார் பந்தய வீரர் அக்பர் இப்ராகீமின் மகன் அர்மான்
என்பது குறிப்பிடத்தக்கது. சென் னையை சேர்ந்த அர்மான் லேடி ஆன்டாள் பள்ளியில் படித்து
வருகிறார். ஏப்ரல் 6-ந் தேதி தொடங்கும் ரேஸ் இந்த ஆண்டு செப்டம்பர் 7-ந்தேதி வரை
நடக்க இருக்கிறது. </p>

<p> </p>






</body></text></cesDoc>