<cesDoc id="tam-w-dinakaran-sports-01-06-19" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-01-06-19.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 01-06-19</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>01-06-19</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>டெஸ்ட் கிரிக்கெட் தொடர்
சமநிலை பெற்றது: இந்தியாவை வீழ்த்தியது ஜிம்பாப்வே</p>

<p> </p>

<p>ஹராரே, ஜூன் 19- இந்திய கிரிக்கெட் அணியை தோற்கடித்து டெஸ்ட்
தொடரை ஜிம்பாப்வே சமன் செய்தது. </p><p>
</p><p>
முதல் டெஸ்ட் கிரிக்கெட் போட்டியில் வெற்றிபெற்ற மிதப்பில் இருந்த இந்திய அணிக்கு
ஸ்டிரீக் தலைமையிலான ஜிம்பாப்வே அணி அடுத்த ஆட்டத்திலேயே வேதனை கொடுத்து விட்டது.
ஹராரே ஆடுகளத்தில் மோதிய 6 டெஸ்ட் போட்டிகளில் ஜிம்பாப்வே ஏற்கனவே 5 வெற்றி பெற்று
சாதனை படைத்திருந்தது. தற்போது ஜிம்பாப்வே அணிக்கு ஹராரே ஆடுகளம் மீண்டும் ராசியான ஒன்று
என்பது நிரூபணம் ஆகிவிட்டது. </p><p>
</p><p>
கங்குலி சொதப்பல் </p><p>
</p><p>
முதல் இன்னிங்சில் 237 ரன் குவித்த இந்திய வீரர்கள் 2-வது இன்னிங்ஸ் தொடக்கம் முதல்
தடுமாறினர். தொடக்க ஆட்டக்காரர் தாஸ் (70 ரன்) மற்றும் டெண்டுல்கர் (69 ரன்)
ஆகியோர் மட்டுமே நம்பிக்கை கொடுக்கும் வகையில் ரன் குவித்தனர். மற்ற வீரர்கள் வருவதும்
போவதுமாகத்தான் ஆடினர். குறிப்பாக காப்டன் கங்குலி மிகவும் கேவலமாக ஆடி ஆட்டம்
இழந்தார். ஆஸ்திரேலியாவுக்கு எதிரான டெஸ்ட் போட்டியில் தொடங்கி சொதப்பி வரும்
கங்குலி ஜிம் பாப்வே அணிக்கு எதிராகவும் மோசமாக ஆடிவருகிறார். </p><p>
</p><p>
இந்தியாவை விட்டு கிளம்பும் போது ஜிம்பாப்வே அணிக்கு எதிராக சாதிப்பேன் என்று பேட்டி
அளித்த கங்குலி இந்தியாவுக்கு சோதனை மட்டுமே கொடுத்துள்ளார். முதல் இன்னிங்சில் 9 ரன்
எடுத்த கங்குலி 2-வது இன்னிங்சில் ரன் கணக்கை தொடங்கும் முன்பே விக்கெட்டை
பறிகொடுத்தார். தமிழக வீரர் பதானி இறுதி வரை ஆட்டம் இழக்காமல் 16 ரன் பெற்றார்.
முடிவில் இந்தியா தனது 2-வது இன்னிங்சில் 234 ரன் பெற்றது. </p><p>
</p><p>
டெஸ்ட் தொடர் சமன் </p><p>
</p><p>
முதல் இன்னிங்சில் 315 ரன் குவித்த ஜிம்பாப்வே அணிக்கு மேலும் 157 ரன் எடுத்தால் வெற்றி
என்ற இலக்கு நிர்ணயிக்கப்பட்டது. தொடக்கத்தில் ஒருசில விக்கெட்டுகளை பறி கொடுத்த
ஜிம்பாப்வே அணி முடிவில் 6 விக்கெட்டுகள் மட்டும் இழந்து வெற்றி இலக்கை எட்டியது.
இதன்மூலம் ஹராரே டெஸ்ட் போட்டியில் ஜிம்பாப்வே அணிக்கு 4 விக்கெட் வித்தியாசத்தில்
வெற்றி கிடைத்தது. இதன்மூலம் 2 டெஸ்ட் போட்டிகள் கொண்ட தொடர் 1-1 என்ற புள்ளியில்
சமநிலை பெற்றது. அடுத்து வருகிற 23-ந் தேதி முதல் 3 நாடுகள் பங்கேற்கும் ஒருநாள்
கிரிக்கெட் போட்டி நடக்க உள்ளது. மேற்கு இந்தியதீவு அணி இந்தப்பந்தயத்தில்
பங்கேற்றுள்ள 3-வது அணியாகும். </p>

<p> </p>

<p>இங்கி. போர்டு
எச்சரிக்கை: ஆடுகளத்துக்குள் ஓடிவரும் ரசிகர்களுக்கு தண்டனை</p>

<p> </p>

<p>லண்டன், ஜூன் 19- ஆடுகளத்துக்குள் ஓடிவரும் ரசிகர்கள்
தண்டிக்கப்படுவார்கள் என்று இங்கிலாந்து போர்டு எச்சரிக்கை விடுத்துள்ளது. </p><p>
</p><p>
இங்கிலாந்தில் நடக்கும் கிரிக்கெட் போட்டி என்றாலே வீரர்களுக்கு பீதி உண்டாகி விடுகிறது.
பாகிஸ்தான், ஆஸ்திரேலியா மற்றும் இங்கிலாந்து ஆகிய 3 அணிகள் பங்கேற்றுள்ள ஒருநாள்
கிரிக்கெட் போட்டி தற்போது நடந்து வருகிறது. இதற்கான லீக் ஆட்டத்தில்
இங்கிலாந்து-பாகிஸ்தான் அணிகள் மோதின. சொற்ப ரன்கள் எடுத்த இங்கிலாந்து அணி
பாகிஸ்தானிடம் பரிதாபமாக தோல்வி கண்டது. ஆட்டம் முடிந்த அடுத்த நிமிடமே வீரர்கள்
அத்தனை பேரும் பெவிலியன் ஓடினர். ஆனால் வீரர் களைவிட ரசிகர்கள் கொஞ்சம்
கெட்டிக்காரர்களாகவே இருக்கிறார்கள். </p><p>
</p><p>
ரசிகர்களுக்கு தண்டனை </p><p>
</p><p>
கைகுலுக்க ஓடி வரும் ரசிகர்களின் பிடியில் இருந்து தப்பிக்க வீரர்கள் படாதபாடு
படுகிறார்கள். நேற்றுமுன்தினம் நடந்த ஆட்டத்தின் போது இப்படித்தான் வீரர்கள் பெவிலியன்
நோக்கி ஓடினார்கள். அதற் குள் ஆடுகளம் வந்துவிட்ட ரசிகர்கள் பிடியில் இங்கிலாந்து
காப்டன் ஸ்டூவர்ட் சிக்கினார். இதில் ஸ்டூவர்ட் முகம் மற்றும் முதுகு ஆகிய பகுதிகளில்
காயம் ஏற்பட்டது. ஆஸ்திரேலிய காப்டன் ஸ்டீவ்வாக் ரசிகர்களை கட்டுப்படுத்தும்படி ஏற்கனவே
இங்கிலாந்து போர்டிடம் கோரிக்கை வைத்தார். தற்போது இந்த பிரச்சினை விஸ்வரூபம்
எடுத்துள்ளது. எனவே ஆடுகளத்துக்குள் ஓடிவரும் ரசிகர்களை கட்டுப்படுத்த புதிய நடவடிக்கைகள்
குறித்து இங்கிலாந்து போர்டு யோசித்து வருகிறது. முதல்கட்டமாக விதி முறைகளை மீறும் ரசிகர்களுக்கு
அபராதம் விதிக்க புதிய சட்டம் இயற்றும்படி அரசுக்கு இங்கிலாந்து கிரிக்கெட் போர்டு
கோரிக்கை வைத்துள்ளது. இங்கிலாந்து அரசு இந்த கோரிக்கையை ஏற்குமா? என்பது
தெரியவில்லை. </p>

<p> </p>

<p>லியாண்டர்-மகேஷ் ஜோடி
தோற்றது</p>

<p> </p>

<p>லண்டன், ஜூன் 19- ஸ்டெல்லா ஏர்டோய்ஸ் சாம்பியன்
கோப்பைக்கான டென்னிஸ் போட்டி லண்டன் நகரில் நடந்து வருகிறது. இதற்கான அரைஇறுதியில்
இந்தியாவின் லியாண்டர் பயஸ்-மகேஷ் பூபதி ஜோடி ஆடியது. முந்தைய ஆட்டங்களில் இந்திய
வீரர்களிடம் காணப்பட்ட விறுவிறுப்பான ஆட்டம் நேற்றைய மோதலில் காணாமல் போய்விட்டது.
முடிவில் அமெரிக்காவின் பாப்பிரையான்-மைக் பிரையான் ஜோடி 7-6 6-4 என்ற செட் கணக்கில்
இந்திய ஜோடியை தோற்கடித்தது. பிரெஞ்சு ஓபன் பட்டத்தை 2-வது முறையாக கைப்பற்றிய
இந்திய ஜோடிக்கு இந்த பந்தயம் கடைசியில் ஏமாற்றத்தை கொடுத்து விட்டது. இருந்தாலும்
இரட்டையர் தரவரிசையில் இந்திய ஜோடிக்கு 2-வது இடம் கிடைத்துள்ளது. பிஜோர்க்மென்
(ஸ்வீடன்)- உட்பிரிட்ஜ் (ஆஸ்திரேலியா) ஜோடி தொடர்ந்து முத லிடத்தை தக்கவைத்துள்ளது. </p>

<p> </p>

<p>போத்தம் சாதனை:
முந்தினார் காஃப்</p>

<p> </p>

<p>லண்டன், ஜூன் 19- பாகிஸ்தான் அணிக்கு எதிரான ஒருநாள்
கிரிக்கெட் போட்டியில் இங்கிலாந்து அணி பரிதாபமாக தோற்றது. இந்த ஆட்டத்தின் போது
இங்கிலாந்து வேகப்பந்து வீச்சாளர் காஃப் 2 விக்கெட்டுகள் சாய்த்தார். பாகிஸ்தான்
அதிரடி ஆட்டக்காரர் அபிரிடியை ஆட்டம் இழக்கச்செய்ததின் மூலம் காஃப் புதிய சாதனைக்கு
சொந்தக்காரராகி விட் டார். 94 ஒருநாள் போட்டிகளில் ஆடியுள்ள காஃப் 146-வது
விக்கெட்டாக அபிரிடியை ஆட்டம் இழக்கச்செய்தார். முன்னதாக இயன் போத்தம் 116 ஒருநாள்
போட்டிகளில் 145 விக்கெட்டுகள் சாய்த்து அதிக விக்கெட் கைப்பற்றிய இங்கிலாந்து வீரர்
என்ற சாதனையை தக்க வைத்திருந்தார். இந்த சாதனைக்கு காஃப் தற்போது முற்றுப்புள்ளி
வைத்தார். டெஸ்ட் கிரிக்கெட் போட்டியில் காஃப் 200 விக்கெட்டுகள்
கைப்பற்றியுள்ளார். போத்தம் 383 விக்கெட்டுகள் கைப்பற்றி முதல் இடத்தை
பிடித்துள்ளார். </p>

<p> </p>

<p>கிரிக்கெட் சூதாட்டம்
செய்த 13 பேர் கைது: பணம் பறிமுதல்</p>

<p> </p>

<p>பெங்களூர், ஜூன் 19- கிரிக்கெட் சூதாட்ட பிரச்சினை நாட்டையே
உலுக்கி எடுத்து விட்டு தற்போது லேசாக ஓய்வு எடுத்து வருகிறது. ஆனாலும் இப்போதும் சூதாட்டம்
நடந்து கொண்டுதான் இருக்கிறது. பெங்களூரில் நேற்று நடத்தப்பட்ட அதிரடி சோதனையில் பங்களா
ஒன்றில் சூதாட்டம் செய்த 13 பேர் கைது செய்யப்பட்டனர். போலீசாருக்கு சூதாட்டம் பற்றி
ரகசியத்தகவல்கள் கிடைத்தன. உடனடியாக உயர் அதிகாரிகள் குழு சம்பந்தப்பட்ட பங்களாவை
சுற்றி வளைத்தது. அப்போது அங்கு சூதாட்டம் நடந்தது உறுதியாக தெரிந்தது. சூதாட்டத்துக்கு
பயன்படுத்தப்பட்ட 23 தொலைபேசிகள், 8 செல்போன்கள், 7 டேப் ரிக்கார்டர்கள், கேசட்,
கால் குலேட்டர் ஆகிய பொருட்களையும் போலீசார் பறிமுதல் செய்தனர். சூதாட்டம் செய்த
அவர்களிடம் இருந்து ரூ.1 லட்சத்துக்கு 64 ஆயிரத்து 910 பணமும் கைப்பற்றப்பட்டது. இந்த
சூதாட் டத்தில் ரூ.15 லட்சத்துக்கும் மேல் பேரம் நடந்துள்ளதை போலீசார் கண்டு
பிடித்துள்ளனர். கைது செய்யப்பட்டவர்கள் இதை போலீசிடம் ஒத்துக்கொண்டுள்ளனர். இதில்
முக்கிய குற்றவாளியாக செயல்பட்ட ஒருவர் போலீஸ் பிடியில் சிக்காமல் தலைமறைவாக இருந்து
வருகிறார். அவரை கைது செய்ய தற்போது போலீசார் முழுவீச்சில் செயல்பட்டு வருகிறார்கள். </p>

<p> </p>

<p> </p>






</body></text></cesDoc>