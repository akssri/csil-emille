<cesDoc id="tam-w-dinakaran-sports-02-10-16" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-02-10-16.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-10-16</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-10-16</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>சென்னையில் நாளை 2-வது
டெஸ்ட் கிரிக்கெட்: இந்தியா-மேஇ.தீவு அணிகள் தீவிர பயிற்சி</p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>இந்தியா-மேற்கு
இந்தியதீவு அணிகள் மோதும் டெஸ்ட் கிரிக்கெட் நாளை சென்னையில் நடக்க உள்ளது.
இந்தப்போட்டிக்காக இந்திய அணி காப்டன் கங்குலி, டெண்டுல்கர், ஷேவாக் ஆகியோர்
பேட்டிங் பயிற்சி செய்ய சேப்பாக்கம் மைதானம் வந்த காட்சி. </p>

<p> </p>

<p>சென்னை, அக். 16- டெஸ்ட் கிரிக்கெட் போட்டிக்காக இந்தியா
மற்றும் மேற்கு இந்தியதீவு அணி வீரர்கள் சேப்பாக்கம் மைதானத்தில் தீவிர பயிற்சி
மேற்கொண்டனர். </p><p>
</p><p>
சென்னை டெஸ்ட் </p><p>
</p><p>
இந்தியா வந்துள்ள மேற்கு இந்தியதீவு கிரிக்கெட் அணி சென்னை சேப்பாக்கம் மைதானத்தில்
நாளை முதல் 2-வது டெஸ்ட் போட்டியில் மோத உள்ளது. மும்பையில் நடந்த முதல் டெஸ்ட்
போட்டியில் மேற்கு இந்தியதீவு பரிதாபமாக தோற்றது. தோல்வி காயத்துக்கு மருந்து போடும்
கனவுடன் மேற்கு இந்தியதீவு அணி வீரர்கள் காத்திருக்கிறார்கள். இதற்காக இந்திய
வீரர்களைவிட ஒருநாள் முன்பாக மேற்கு இந்தியதீவு அணி சென்னை வந்து விட்டது. </p><p>
</p><p>
நேற்று காலை 8.30 மணி முதல் மேற்கு இந்தியதீவு வீரர்கள் சென்னை சேப்பாக்கத்தில்
தீவிரபயிற்சி செய்தனர். இந்த பயிற்சியைக்காண ரசிகர்கள் யாரும் உள்ளே
அனுமதிக்கப்படவில்லை. பந்துவீச்சு, பீல்டிங், காட்ச், பேட்டிங் என மேற்கு இந்தியதீவு
வீரர்கள் அத்தனை பேரும் இடைவிடாது பயிற்சி மேற்கொண்டனர். சென்னை வெயிலை
தாங்கிக்கொள்ள முடியாத மேற்கு இந்தியதீவு வீரர்கள் சிலர் மேல் சட்டையை
கழற்றிவிட்டனர். </p><p>
</p><p>
இந்திய வீரர்கள் பயிற்சி </p><p>
</p><p>
காலை விமானநிலையம் வந்த இந்திய கிரிக்கெட் வீரர்கள் மதியம் வரை ஓட்டலில் ஓய்வு
எடுத்தனர். பின்னர் மாலை 2.30 மணிக்கு இந்திய வீரர்கள் அத்தனை பேரும் சேப்பாக்கத்தில்
பயிற்சி மேற்கொண்டனர். வெற்றிக்களிப்பில் இருந்த இந்திய வீரர்களும் நேற்று தீவிர
பயிற்சி செய்தனர். சமீபத்தில் நடந்த போட்டிகளில் இந்தியாவின் பேட்டிங் அதிக வலுவுடன்
உள்ளது. உள்ளூர் போட்டி என்பதால் பந்து வீச்சிலும் இந்திய வீரர்கள் சாதித்து
வருகிறார்கள். </p><p>
</p><p>
ஆவேத்துடன் மே.இ.தீவு </p><p>
</p><p>
எனவே சென்னை போட்டியில் இந்திய வீரர்களின் ஆதிக்கம் அதிகமாக இருக்கும் என்று
நம்பலாம். அதேசமயம் அடிப்பட்ட புலியாக மேற்கு இந்திய தீவு வீரர்கள் சென்னை
மோதலுக்காக ஆவேத்துடன் காத்திருக்கிறார்கள். இவர்களின் ஆவேசம் எந்த அளவுக்கு இந்திய
வீரர்களுக்கு நெருக்கடி கொடுக்கிறதோ? அதை பொறுத்தே இந்தப்போட்டியின் முடிவு இருக்கும்.
இவை எல்லாவற்றுக்கும் மேலே, இயற்கையின் ஆதிக்கமும் சென்னை போட்டியில் அதிகமாக இருக்கும்
என்று கூறப்படுகிறது. </p><p>
</p><p>
மழை அபாயம் </p><p>
</p><p>
சென்னையை பொறுத்தவரை கடந்த சில நாட்களாக மழை பெய்து வருகிறது. இரவு நேரங்களில்
அதிகமான மழை பெய்து போட்டி நடக்க வாய்ப்பு இல்லாமல் போனால் ஆட்டத்தின் முடிவு
எதிர்பார்த்தபடி இருக்காது என்பதில் எந்த சந்தேகமும் இல்லை. வெற்றி நினைப்புடன்
காத்திருக்கும் மேற்கு இந்தியதீவு அணிக்கு மழை பயம் அதிகமாகவே உள்ளது. எது எப்படி இருந்தாலும்
நம்பிக்கை வீரர் பிரைன் லாரா அணியில் இடம்பிடிக்காமல் போனது சென்னை ரசிகர்களை
சோர்வடையச் செய்துள் ளது குறிப்பிடத்தக்கது. </p><p>
</p><p>
மெகா-ஸ்கிரீனில் கிரிக்கெட் </p><p>
</p><p>
சென்னை சேப்பாக்கத்தில் இந்தியா-மேற்கு இந்தியதீவு அணிகள் நாளை முதல் டெஸ்ட்
கிரிக்கெட்டில் மோத உள்ளன. இந்தப்போட்டி சேப்பாக்கம் மைதானத்தில் உள்ள
மெகா-ஸ்கிரீனில் நேரடியாக ஒளிபரப்பு செய்யப்படுகிறது. கடந்த ஆண்டு ஆஸ்திரேலியாவில்
இருந்து இந்த ஸ்கிரீன் வாங்கப்பட்டது. இதன் காரணமாக கேலரியில் வைக்கப்பட்டு இருந்த
தொலைக்காட்சிகள் அனைத்தும் அகற்றப்பட்டு விட்டன. இந்த ஸ்கிரீனில் ஸ்கோர் போர்டு,
மறுஒளிபரப்பு ஆகிய அனைத்தையும் ரசிகர்கள் கண்டுகளிக்கலாம். </p><p>
</p><p>
போலீஸ் கெடுபிடி </p><p>
</p><p>
இந்தியா-மேற்கு இந்தியதீவு அணிகள் மோதும் 2-வது டெஸ்ட் கிரிக்கெட் போட்டி நாளை முதல்
சென்னை சேப்பாக்கம் மைதானத்தில் நடக்க உள்ளது. இதற்காக 2 ஆயிரத்துக்கும் அதிகமான
போலீசார் பாதுகாப்பு பணியில் ஈடுபடுத்தப்பட உள்ளனர். தற்போது சேப்பாக்கம்
மைதானத்தில் கணிசமான போலீஸ் குவிக்கப்பட்டுள்ளனர். நேற்றைய நாளில் இருந்தே
போலீசாரின் கெடுபிடிதொடங்கிவிட்டது. இரு அணிகளும் மைதானத்தில் நேற்று பயிற்சி செய்தன.
இதைக்காண பத்திரிகையாளர்கள் சென்றனர். சில நிருபர்கள் இன்னும் தங்கள் அனுமதி அட்டையை
பெறவில்லை. வாசலில் நிருபர்களை தடுக்கும் போலீசார் பல்வேறு கேள்விகளை மாறிமாறி
கேட்கிறார்கள். கடுமையான கெடுபிடிகளுக்கு மத்தியில்தான் நிருபர்களே மைதானம் செல்ல
அனுமதிக்கப்படுகிறார்கள். கிரிக்கெட்டை நேரில் ரசிக்கத்துடிக்கும் ரசி கர்களுக்கு இன்னும்
என்னென்ன கெடுபிடிகள் காத்திருக்கிறதோ? தெரியவில்லை. </p>

<p> </p>

<p> </p>

<p> </p>

<p> </p>

<p>தன்ராஜ் பிள்ளை பேட்டி:
ஓய்வு எண்ணம் இல்லை</p>

<p> </p>

<p>புதுடெல்லி, அக். 16- சர்வதேச ஹாக்கியில் இருந்து இப்போதைக்கு
ஓய்வு பெற மாட்டேன் என்று இந்திய வீரர் தன்ராஜ் பிள்ளை பேட்டி அளித்தார். </p><p>
</p><p>
தன்ராஜ் பிள்ளை </p><p>
</p><p>
ஆடுகளத்தில் சாதனை நாயகனாக இருக்கும் தன்ராஜ் பிள்ளை அடிக்கடி சர்ச்சை களையும்
கிளப்புவதற்கு தவறுவது இல்லை. இந்திய ஹாக்கி அணியின் முன்னாள் காப்டனான தன்ராஜ் பிள்ளை
நடந்து முடிந்த ஆசிய விளையாட்டில் தனது திறமையை மீண்டும் நிரூபித்தார். 33 ஆண்டு
இடைவௌிக்குப்பிறகு பாங்காங் ஆசிய விளையாட்டில் இந்தியா சாம்பியன் கோப்பை கைப்பற்ற
முக்கிய காரணமாக இருந்த தன்ராஜ் இன்றைக்கும் ரசிகர்கள் மத்தியில் நீங்காத
இடம்பிடித்துள்ளதை யாரும் மறுக்க முடியாது. </p><p>
</p><p>
புசான் ஆசிய விளையாட் டுக்கான இறுதிப்போட்டியில் கொரியாவிடம் சாம்பியன் கோப்பையை
பறிகொடுத்த இந்தியஅணி தாயகம் திரும்பியது. தாயகம் வந்த தன்ராஜ் பிள்ளையிடம்
நிருபர்கள் பேட்டி கேட்டனர். அப்போது அவர் கூறுகை யில், தங்க கோப்பை கையில் இருந்து
நழுவிவிட்டது என்றே சொல்லவேண்டும். என்னிடம் சிலர் எப்போது ஓய்வு பெறப்போகிறீர்கள்
என்று கேட்கிறார்கள். ஆசிய விளையாட்டுப் போட்டியில் இதற்கு சரியான பதில் அளித்து
விட்டேன். சர்வதேச ஹாக்கியில் இருந்து ஓய்வு பெறும் எண்ணம் இப்போதைக்கு என்னிடம்
இல்லை. இன்னும் சில ஆண்டுகள் கழித்து இதுபற்றி முடிவு செய்வேன் என்றார். </p><p>
</p><p>
ஆசியவிளையாட்டுக்கான இறுதி ஆட்டம் பற்றி இந்திய பயிற்சியாளர் ரஞ்சிந்தர்சிங்
கூறுகையில், 3 கோல் வாங்கி பரிதாப நிலையில் இருந்த இந்தியா பின்னர் சுதாரிப்பாக ஆடி
அடுத்தடுத்து 3 கோல்கள் போட்டது. இந்த வேகம் கொஞ்சம் குறையாமல் இருந்திருந்தால்
நிச்சயம் இந்தியா சாம்பியன் கோப்பையை வென் றிருக்கும். எல்லாவற்றுக்கும் மேலே
ராசியும் இருக்கவேண்டும் என்றார். </p>

<p> </p>

<p> </p>

<p>இந்திய ஓபன் டென்னிஸ்:
பிலிப்போசிஸ் பங்கேற்கிறார்</p>

<p> </p>

<p>சென்னை, அக். 16- இந்திய ஓபன் டென்னிஸ் போட்டியில்
ஆஸ்திரேலியாவின் முன்னணி ஆட்டக்காரர் மார்க் பிலிப்போசிஸ் கலந்து கொள்கிறார். </p><p>
</p><p>
சர்வதேச வீரர்கள் பங்கேற்கும் இந்திய ஓபன் டென்னிஸ் போட்டி சென்னையில் வருகிற
டிசம்பர் 30-ந்தேதி முதல் ஜனவரி 5-ந்தேதி வரை நடக்க உள்ளது. இந்தியாவில் நடக்கும்
ஒரேஒரு சர்வதேச பந்தயம் இது என்பதால் ரசிகர்கள் அதிக ஆர்வத்துடன் இந்தப்போட்டியை
எதிர்நோக்கி உள்ளனர். இந்தமுறை நடக்க உள்ள இந்தப்பந்தயத்தில் ஆஸ்திரேலியாவின்
முன்னணி ஆட்டக்காரர் மார்க் பிலிப்போசிஸ் கலந்து கொள்கிறார். சென்னையில் நடக்க
உள்ள போட்டியை எதிர் நோக்கி காத்திருப்பதாக பிலிப்போசிஸ் பந்தய அமைப்பாளர்களிடம்
தெரிவித்தார். ஜெர்மனியின் நம்பிக்கை நாயகன் போரீஸ்பெக்கர், ராப்டர், மோயா,
கபல்நிகோவ் போன்ற சர்வதேச நட்சத்திர ஆட்டக்காரர்கள் இந்தப்போட்டியில் பங்கேற்று
சென்னை ரசிகர்களை உற்சாகப்படுத்தி உள்ளனர். கனாஸ் போட்டியின் நடப்பு சாம்பியன் ஆவார்.
</p>

<p> </p>

<p> </p>

<p>குத்துச்சண்டை: கேரளா
சாம்பியன்</p>

<p> </p>

<p>சென்னை, அக். 16- தென்மண்டல சாம்பியனுக்கான குத்துச்சண்டை
போட்டியில் கேரளா அணி ஒட்டுமொத்த சாம்பியன் கோப்பை கைப்பற்றியது. </p><p>
</p><p>
தென்மண்டல சாம்பியனுக்கான குத்துச்சண்டை போட்டி சென்னையில் நடந்தது. தமிழ் நாடு, கேரளா,
கோவா, ஆந்திரா, கர்நாடகம் ஆகிய மாநிலங்களில் இருந்து வீரர்-வீராங் கனைகள்
இந்தப்போட்டியில் கலந்து கொண்டனர். பெண்கள் பிரிவில் கேரளா அணி ஒட்டுமொத்த
சாம்பியன் கோப்பை கைப்பற்றியது. இந்த அணிக்கு 31 புள்ளிகள் கிடைத்தது. ஆந்திரா
அணிக்கு 2-வது இடம் கிடைத்தது. ஆண்கள் பிரிவில் ஆந்திரா முதலிடத்தையும், தமிழ்நாடு 2-வது
இடத்தையும் பிடித்தன. அஸ்வதி பிரபா மற்றும் ரஞ்சித் ஆகியோர் சிறந்த
குத்துச்சண்டை சாம்பியன்களாக தேர்வு செய்யப்பட்டனர். போட்டி முடிவில் சென்டிரல் எக்சைஸ்
மற்றும் கஸ்டம்ஸ் துணை கமிஷனர் சந்தோஷ் குமார் சிறப்பு விருந்தினராக கலந்துகொண்டு
பரிசுகள் வழங்கினார். தமிழ்நாடு குத்துச்சண்டை சங்க செயலாளர் கருணாகரன், துணைத்தலை வர்
பலராமன், அம்பத்தூர் குளோத்திங் நிறுவனத்தின் இயக்குனர் சுப்பிரமணியன் ஆகியோர் இந்த
விழாவில் கலந்து கொண்டனர் </p>

<p> </p>

<p> </p>

<p>பள்ளிகளுக்கான மாநில
கைப்பந்து</p>

<p> </p>

<p>சென்னை, அக். 16- பள்ளி அணிகளுக்கான மாநில கைப்பந்து போட்டி
அடுத்த மாதம் 7-ந்தேதி முதல் தொடர்ந்து 3 நாட்கள் வேல்ஸ் கல்லூரியில் நடக்க உள்ளது. </p><p>
</p><p>
பள்ளி அணிகளுக்கான மாநில கைப்பந்து போட்டி அடுத்த மாதம் 7-ந்தேதி முதல் தொடர்ந்து 3
நாட்கள் பல்லாவரம் வேல்ஸ் கல்லூரியில் நடக்க உள்ளது. ஆண்கள் மற்றும் பெண்கள் அணிகள்
இந்தப் போட்டியில் கலந்து கொள்கின்றன. போட்டியில் சேர வருகிற 25-ந்தேதி கடைசிநாள்
ஆகும். மேலும் விவரங்களுக்கு, பந்தய அமைப்பு செயலாளர், வேல்ஸ் விளையாட்டு, அகாடமி,
வேலன் நகர், பி.வி.வைத்தியலிங்கம் சாலை, பல்லாவரம், சென்னை-117 என்ற முகவரியில்
தொடர்பு கொள்ளவேண்டும். </p>

<p> </p>






</body></text></cesDoc>