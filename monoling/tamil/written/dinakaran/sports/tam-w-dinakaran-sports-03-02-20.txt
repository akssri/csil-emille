<cesDoc id="tam-w-dinakaran-sports-03-02-20" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-03-02-20.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 03-02-20</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>03-02-20</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>எங்களால் முடியும்
நிரூபித்தது இந்தியா ஜிம்பாப்வேயை வீழ்த்திய டெண்டுல்கர்-கங்குலி </p>

<p> </p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>கடும் விமர்சனங்களுக்குப்
பிறகு இந்திய அணி சிறப்பாக ஆடியது. ஜிம்பாப்வே அணிக்கு எதிராக இந்தியஅணிக்கு வெற்றித்
தேடி கொடுத்த டெண்டுல்கரை படத்தில் காணலாம். </p>

<p> </p>

<p>ஹராரே, பிப்-20- அவமானங்களை சந்தித்த எவரும் வாழ்க்கையில்
தோற்றதாக சரித்திரமே கிடையாது. இந்திய கிரிக்கெட் அணி விஷயத்தில் இது நூற்றுக்கு நூறு
உண்மை. ஹாலந்துடன் பரிதாபம், ஆஸ்திரேலியாவுடன் அவமானம். இப்படி ஜீரணிக்க முடியாத
விஷயங்களை மட்டுமே எதிர்கொண்ட இந்தியா மீண்டுவருமா? என்ற கேள்வி பலரின் மனதிலும்
தோன்றியது. உணர்ச்சிவயப்பட்ட ரசிகர்கள் கொஞ்சம் கூடுதலா கவே தங்கள் எதிர்ப்பை
காட்டிவிட்டனர். காய்ப், கங்குலி, திராவிட் ஆகியோர் வீடுகளில் தாக்குதல் நடத்திய
ரசிகர்கள் ஜிம்பாப்வேயுடன் மோதலை அவ்வளவாக எதிர்பார்க்க வில்லை. </p><p>
</p><p>
எதிர்பார்க்கும் ஒவ்வொரு முறையும் சோதனைதான் நடக்கிறது. இதனால் எதிர்பார்க்கா மல்
இருந்து விடலாம் என நினைத்த ரசிகர்களை ஒரு கனம் நினைத்து சந்தோஷப் படுவோம்.
ஜிம்பாப்வேயுடன் தோற்றால், சூப்பர்சிக்ஸ் வாய்ப்பை மட்டுமின்றி, தாய் நாட்டில்கூட
இடம்பிடிக்க முடியாது என்ற நெருக்கடியான நிலையில் இந்திய வீரர்கள் இருந்தனர். நீண்ட...
இடைவௌிக்குப் பின்னர் முதல் முறையாக இந்திய வீரர்கள் அணி உணர்வுடன் ஆடியதற்காக நன்றி
சொல்லிக் கொள்வோம். </p><p>
</p><p>
ஜிம்பாப்வே என்ன நினைத்ததோ தெரியவில்லை. டாஸ் வென்றதும் முதலில் பந்து வீச்சை தேர்வு
செய்தது. ஜிம்பாப்வே காப்டனின் இந்த முடிவுக்கு முந்தைய மோதல்களில் இந்தியாவின்
பரிதாபமான பேட்டிங் மட்டுமே காரணமாக இருக்க முடியும். அடுத்து காப்டன் கங்குலி எடுத்த சில
புத்திசாலித்தனமான முடிவுகள் இந் திய அணியை உத்வேகம் கொள்ளச்செய்தது. முந்தைய
போட்டியில் ஆடிய கும்ளே வுக்குப்பதிலாக வேகப்பந்து வீச்சாளர் நெக்ரா சேர்க்கப்பட்டார்.
</p><p>
</p><p>
தொடக்க வீரர் இடத்தை யாருக்கும் விட்டுக்கொடுக்க மாட்டேன் என்று அடம்பிடித்த கங்குலி
நல்லவேளையாக 4-வது வீரராக களம் வந்தார். இதனால் அதிரடியாக ஆடக் கூடிய ஷேவாக்குக்கு
மீண்டும் தொடக்க வீரர் வாய்ப்பு கிடைத்தது. தினேஷ் மொங் கியாவை தொடக்க
ஆட்டக்காரராக களம் இறக்க நினைத்த பயிற்சியாளர் ஜான் ரைட் கடைசி நேரத்தில் தனது முடிவை
மாற்றிக்கொண்டார். </p><p>
</p><p>
டெண்டுல்கர்-ஷேவாக் ஜோடி உலக கோப்பையில் தனி முத்திரைபதித்து விட்டது. சோர்ந்து
கிடந்த ஒவ்வொரு ரசிகனும் துடித்து எழும் வகையில் இவர்கள் ஆடியது மறக்க முடியாத திருவிழா.
வாழ்க்கையில் எப்போதாவது மட்டுமே நமக்கு அதிர்ஷ்டம் வாய்க்கும். அந்த வாய்ப்பு
கிடைத்தவர்கள் மட்டுமே இந்தியாவின் பேட் டிங்கை நேற்று கண்டிருப்பார்கள். முதல்
விக்கெட்டுக்கு இந்த ஜோடி 99 ரன் குவித்த நிலையில் ஷேவாக் ஆட்டம் இழந்தார். </p><p>
</p><p>
வழக்கமாக சுழல்பந்து வீச்சை தவிடுபொடியாக்கும் இந்திய ஆட்டக்காரர்கள் ஜிம் பாப்வே
அணியின் கிரான்ட் பிளவர், மர்பி பந்துவீச்சில் கொஞ்சம் தடுமாறினர். கிடைத்த
வாய்ப்பை தினேஷ் மொங்கியாவும் சரியாக பயன்படுத்திக்கொள்ள வில்லை. இவருக்குப்பின்னர்
வந்த கங்குலி அடிக்கும் மனநிலையுடன் களம் இறங்கினார். உள்ளூர் ரசிகர்களை
திருப்திப்படுத்த வேண்டிய கட்டாயம் ஒருபக்கம், அணிக்கு வெற்றித் தேடி கொடுக்க வேண்டிய
நெருக்கடி மறுபக்கம். குழப்பமான நிலையிலும் ஆத்ம திருப்திக்காக சிக்சர் ஒன்றை அடித்தார்
கங்குலி. </p><p>
</p><p>
சதம் அடிப்பார் என்று எதிர்பார்த்த நிலையில் டெண்டுல்கர் போல்டானார். அரை சதம் அடித்த
பின்னர் டெண்டுல்கர் லெக் ஸ்டெம்புக்கு வௌியே விலகி நின்று ஆடியது கொஞ்சம்
பயத்தை உண்டாக்கியது. ஆனாலும் தொடர்ந்து நன்றாக ஆடிய டெண்டுல்கர் கடைசியில் ஜி.பிளவர்
பந்தில் ஏமாந்தார். டெண்டுல்கர் 81 ரன் எடுத் தார். 43 ஓவரில் 200 ரன் எடுத்த இந்தியா
கடைசி 7 ஓவரில் 55 ரன் குவித்தது. கடைசி கட்டத்தில் காய்ப், திராவிட் ஆகியோர்
இந்தியாவின் ஸ்கோர் உயர்வுக்கு காரணமாக அமைந்தனர். இதனால் இந்தியா
நிர்ணயிக்கப்பட்ட ஓவரில் 255 ரன் பெற்றது. </p><p>
</p><p>
பேட்டிங்கில் முன்னேற்றத்தை காண்பித்த இந்தியா பந்துவீச்சிலும் சாதித்தது. அனுபவத்தை
மட்டுமே நம்பி களம் இறங்கிய நாத் எதிர் அணி வீரர்களுக்கு தொடர்ந்து நெருக்கடி
கொடுத்தார். முதல் ஓவரிலேயே வெர்முலனை வீட்டுக்கு அனுப்பிய நாத், ஜிம்பாப்வே அணியின்
முன்னணி ஆட்டக்காரர் ஆன்டி பிள வரை சோதித்தார். 4 முறை ஆட்டம் இழக்கும் அபாயத்தில்
இருந்து தப்பிய ஆன்டி கடைசியில் கங்குலியிடம் ஏமாந்தார். </p><p>
</p><p>
பேட்டிங், பந்துவீச்சு, பீல்டிங் ஆகியவற்றில் சர்வதேச தரத்தை காண்பித்த இந்தியா
கடைசியில் ஜிம்பாப்வே அணியை 172 ரன்னில் சுருட்டி வீசியது. கடைசியில் வந்த விக்கெட்
கீப்பர் தாய்பு மட்டும் கொஞ்சநேரம் போராடினார். ஆனாலும் அவரின் போராட்டம்
ஜிம்பாப்வே வெற்றிக்கு போதுமானதாக இல்லை. பேட்டிங்கில் டெண்டுல்கர் கதாநாயகன்
என்றால், பந்துவீச்சில் கங்குலி சபாஷ் வாங்கினார். அடுத்தடுத்து 3 விக்கெட்டுகளை
கைப்பற்றி ரசிகர்களுக்கு புன்னகை கொடுத்த கங்குலி தன்னால் எப்படியும் பங்களிக்க முடியும்
என்பதை நிரூபித்தார். எப்படியோ, சாதிக்க வேண்டும் என்ற போட்டி உணர்வு இந்திய
வீரர்களிடம் வந்ததே பெரிய விஷயம். டெண்டுல்கர் ஆட்டநாயகனாக தேர்வு செய்யப்பட்டார். </p><p>
</p><p>
இத்தனை நாட்கள் இந்த திறமையை நமது வீரர்கள் எங்கே ஒளித்து வைத்திருந்தார் கள்
என்பதுதான் தெரியவில்லை. சோதனைதான் சில சாதனைகளுக்கு வழிவகுக்கும். இந்திய
அணிக்கும்அப்படித்தான். ஆஸ்திரேலியாவுடன் கிடைத்த சோதனையே, நேற் றைய ஆட்டத்தில்
இந்திய வீரர்களை ஒன்றுபடுத்தி உள்ளது. நம்மை தோற்கடித்திருந்தாலும்கூட, இந்திய
வீரர்களின் உணர்வை தட்டி எழுப்பியதற்காக ஆஸ்திரேலிய அணிக்கு நன்றி
தெரிவித்துக்கொள்வோம். ஒருவேளை வாய்ப்பு கிடைத்தால், அரைஇறுதியில் ஆஸ்திரேலியாவுக்கு
இதே பதிலடியை திருப்பி கொடுப்போம். இது ஆரம்பம்தான். இனி நடப்பது எல்லாம் நன்மையை
மட்டுமே கொடுக்கும். நம்பிக்கையுடன் காத்திருப்போம், கலாமைப்போல </p><p>
</p><p>
கங்குலிக்கு அசாருதீன் அறிவுரை </p><p>
</p><p>
இன்றைய வரைக்கும் இந்திய கிரிக்கெட்டின் சாதனை காப்டன் என்ற தகுதியை அசாருதீன் தக்க
வைத்திருக்கிறார். 83-ம் ஆண்டு உலக கோப்பையை வென்றாலும் கபில்தேவுக்கு கிடைக்காத
மரியாதை அசாருதீனுக்கு கிடைத்திருக்கிறது. சூதாட்டத்தில் சிக்கியதால் அசாருதீன் அணியில்
இருந்து தூக்கி எறியப்பட்டார். இந்தநிலையில் அசாருதீன் இந்திய காப்டன் கங்குலிக்கு
அறிவுரை வழங்கி உள்ளார். ஆஸ்திரேலியாவுடன் மோசமாக தோற்றதை அடுத்து இந்திய அணி மீது
கடும் விமர் சனங்கள் பாய்ந்துள்ளன. இதற்கு கங்குலி பதில் எதுவும் கொடுக்க வில்லை.
ரசிகர்களின் கோபத்தை தணிக்க திராவிட் பேட்டி அளித்தார். இந்த அணுகுமுறையை அசாருதீன்
பாராட்டியுள்ளார். மேலும் அவர் கூறுகையில், இப்போது இருப்பது போல் கங்குலி அமைதியாக
இருந்தால் நல்லது. தேவையில்லாமல் பேசிக்கொண்டு இருக்கும் நேரம் இது இல்லை. அவசியம்
ஏற்பட்டால் மட்டுமே கங்குலி பேச வேண்டும். மற்ற சமயங்களில் பொறுப்பான மற்றொருவரை
கொண்டு விஷயங்களை முடித்துக் கொள்ள வேண்டும். காய்ப் வீட்டை தாக்கியது கண்டிக்கத்தக்கது.
கிரிக்கெட்டில் தோற்று விட்டால் வீரர்களுக்கு எப்பேற்பட்ட டென்ஷன் இருக்கும் என்பதை
ரசிகர்களை நிச்சயம் அறிய மாட்டார்கள் என்றார். </p><p>
</p><p>
ரசிகர்கள் தொடுத்த வழக்கில் கங்குலி-டால்மியாவுக்கு நோட்டீசு </p><p>
</p><p>
உலக கோப்பையில் கலந்து கொண்டுள்ள இந்திய கிரிக்கெட் அணி ஆஸ்திரேலியாவுடன் மோசமாக
தோற்றது. கோடிக்கணக்கான ரசிகர்களின் உணர்வை பாதித்த இந்தவிஷயம்தொடர்பாக பாவ்நகர்
கோர்ட்டில் வழக்குதாக்கல் செய்தனர். வீரர்களின் மோசமான ஆட்டத்துக்கு சரியான விளக்கம்
அளிக்கும் வரை அவர்களுக்கு சேரவேண்டிய பணத்தை கொடுக்கக்கூடாது என்று வழக்கில் ரசிகர்கள்
குறிப்பிட்டு இருந்தனர். இந்த வழக்கை ஏற்றுக்கொண்ட நீதிபதி இந்திய கிரிக்கெட் காப்டன்
கங்குலி, கிரிக்கெட் போர்டு தலைவர் டால்மியா, மத்திய விளை யாட்டுத்துறை செயலர்
ஆகியோர்களிடம் விளக்கம் கேட்டு நோட்டீசு அனுப்பினார். வருகிற 24-ந்தேதிக்குள் விளக்கம்
கொடுக்க வேண்டும் என்றும் நீதிபதி குறிப்பிட்டார். </p><p>
</p><p>
இந்திய கிரிக்கெட் ஸ்கோர் அறிய பாராளுமன்ற எம்.பி.க்கள் ஆர்வம் </p><p>
</p><p>
இந்திய கிரிக்கெட் அணி கண்டிப்பாக வெற்றிபெற்றாக வேண்டிய சூழ்நிலையில் ஜிம்பாப்வே
அணியுடன் நேற்று மோதியது. இந்திய கிரிக்கெட் அணியின் ஸ்கோரை தெரிந்துகொள்ளும்
ஆர்வம் ரசிகர்களிடம் மட்டுமின்றி, லோக்சபா எம்.பி.க்களிடமும் பரவலாக நிலவியது.
முன்னாள் கிரிக்கெட் வீரரும், எம்.பி.யுமான கீர்த்தி ஆசாத் சக உறுப்பினர்களிடம்
இந்தியா முதலில் பேட்டிங் செய்வதாக கூறினார். இதையடுத்து மற்ற உறுப்பினர்கள் இந்தியாவின்
ஸ்கோரை அறிந்து கொள்ள அதிகமாக துடித் தனர். பாராளுமன்ற கூட்டத்தின் நடுவே உறுப்பினர்கள்
கூட்டம் கூட்டமாக வௌியே சென்று கிரிக்கெட்டை பற்றி விவாதம் நடத்தினர். </p><p>
</p><p>
இந்திய அணிக்கு வெங்கையா ஆறுதல் </p><p>
</p><p>
உலக கோப்பையில் கிடைத்த தோல்விகளால் இந்திய ரசிகர்கள் கடும் அதிருப்தி அடைந்துள்ளர்
வீரர்களுக்கு எதிராக ரசிகர்கள் போராட்டம் நடத்தி வருகிறார்கள். இந்தநிலையில் பாரதிய
ஜனதா தலைவர் வெங்கையா நாயுடு சென்னையில் கூறுகை யில், இந்திய ரசிகர்களின் கோபத்தில்
நியாயம் கிடையாது. இந்தமாதிரியான நேரத்தில்தான் நாம் வீரர்களுக்கு ஆதரவு தெரிவிக்க
வேண்டும். வெற்றியும் தோல்வியும் விளையாட்டில் சகஜம். எனவே இந்த விஷயத்தை
விட்டுவிடுவோம் என்றார். </p><p>
</p><p>
ஆஸ்திரேலியாவுக்கு எதிரான கிரிக்கெட் போட்டியில் இந்தியா மோசமாக தோற்ற தால்
ரசிகர்கள் கொதிப்படைந்தனர். கிரிக்கெட் வீரர்களின் வீடுகளில் கலாட்டா செய்து
வருகிறார்கள். இந்தநிலையில் திராவிட் வீட்டின் முன்பு நிறுத்தி வைக்கப்பட்டு இருந்த கார்
கண்ணாடி உடைக்கப்பட்டது. இதுகுறித்து திராவிட்டின் தந்தை சரத் திராவிட் போலீசில் புகார்
கொடுத்துள்ளார். விசாரணை நடந்து வரு கிறது. </p><p>
</p><p>
சாதனைகள் </p><p>
</p><p>
உலக கோப்பையில் ஜிம்பாப்வேயுடன் பெற்ற அதிகபட்ச ரன் வித்தியாச வெற்றி இதுவாகும்.
முன்னதாக இந்தியா 55 ரன் வித்தியாசத்தில் ஜிம்பாப்வே அணியை தோற்கடித்துள்ளது. </p><p>
</p><p>
உலக கோப்பையில் டெண்டுல்கர் தனது 6-வது ஆட்டநாயகன் விருதை நேற்றைய போட்டியில்
பெற்றார். உலககோப்பை வரலாற்றில் வேறு எந்த வீரரும் இந்த சாதனை செய்தது இல்லை. </p><p>
</p><p>
டெண்டுல்கர்-ஷேவாக் ஜோடி குவித்த 99 ரன் உலக கோப்பையில் ஜிம்பாப்வேயுடன் பெற்ற
அதிகபட்ச ஸ்கோர் ஆகும். முன்னதாக கவாஸ்கர்-காந்த் ஜோடி முதல் விக்கெட்டுக்கு 76 ரன்
குவித்து சாதனை படைத்திருந்தது. </p><p>
</p><p>
சவுரவ் கங்குலி உலக கோப்பையில் தனது சிறந்த பந்துவீச்சு சாதனையை எட்டினார். முன்னதாக 27
ரன்னுக்கு 3 விக்கெட் கைப்பற்றியது உலக கோப்பை சாதனையாக இருந்தது. </p><p>
</p><p>
ஸ்கோர் போர்டு </p><p>
</p><p>
இந்தியா (50 ஓவர்)-255/7 </p><p>
</p><p>
ஷேவாக் (கா) தாய்பு (ப) விட்டல்-36 </p><p>
</p><p>
டெண்டுல்கர் (ப) ஜி.பிளவர்-81 </p><p>
</p><p>
தினேஷ்மொங்கியா (கா) ஹோண்டா (ப) ஜி.பிளவர்-12 </p><p>
</p><p>
கங்குலி (கா) ஸ்டிரீக் (ப) பிலிக்னாட்-24 </p><p>
</p><p>
திராவிட் அவுட் இல்லை-43 </p><p>
</p><p>
யுவ்ராஜ் (கா) தாய்பு (ப) மர்பி-01 </p><p>
</p><p>
காய்ப் எல்.பி.டபிள்யூ. (ப) ஹோண்டா-25 </p><p>
</p><p>
ஹர்பஜன் (கா) மர்பி (ப) ஸ்டிரீக்-03 </p><p>
</p><p>
ஜாகீர்கான் அவுட் இல்லை-13 </p><p>
</p><p>
உதிரிகள்-17 </p><p>
</p><p>
விக்கெட்சரிவு:1/99 2/142 3/142 4/182 5/184 6/227 7/234 </p><p>
</p><p>
பந்துவீச்சு: ஸ்டிரீக் 9-0-46-1பிலிக்னாட் 10-0-54-1 </p><p>
</p><p>
ஹோண்டா 9-1-56-1விட்டல் 6-0-37-1 </p><p>
</p><p>
ஜி.பிளவர் 6-0-14-2மர்பி 10-0-40-1 </p><p>
</p><p>
ஜிம்பாப்வே (44.4 ஓவர்)-172 </p><p>
</p><p>
விஷார்ட் (ப) நாத்-12 </p><p>
</p><p>
வெர்முலேன் (கா) திராவிட் (ப) நாத்-00 </p><p>
</p><p>
ஆன்டிபிளவர் (ப) ஹர்பஜன்சிங்-22 </p><p>
</p><p>
கிரான்ட்பிளவர் (கா) ஹர்பஜன்சிங் (ப) கங்குலி-23 </p><p>
</p><p>
இப்ராகீம் (கா) (சப்) அகர்கர் (ப) கங்குலி-19 </p><p>
</p><p>
பிலிக்னாட் (கா) ஜாகீர்கான் (ப) கங்குலி-02 </p><p>
</p><p>
தாய்பு அவுட் இல்லை-29 </p><p>
</p><p>
விட்டல் (கா) ஜாகீர்கான் (ப) ஷேவாக்-28 </p><p>
</p><p>
ஸ்டிரீக் (கா) காய்ப் (ப) ஹர்பஜன்சிங்-20 </p><p>
</p><p>
மர்பி (ப) ஜாகீர்கான்-02 </p><p>
</p><p>
ஹோண்டா (ப) ஜாகீர்கான் -02 </p><p>
</p><p>
உதிரிகள்-13 </p><p>
</p><p>
விக்கெட் சரிவு: 1/1 2/23 3/48 4/83 5/83 6/87 7/124 8/160 9/165 </p><p>
</p><p>
பந்துவீச்சு: நாத் 8-1-14-2ஜாகீர்கான் 7.4-0-23-2 </p><p>
</p><p>
நெக்ரா 7-0-35-0ஹர்பஜன்சிங் 10-0-42-2 </p><p>
</p><p>
கங்குலி 5-1-22-3ஷேவாக் 3-0-14-1 </p><p>
</p><p>
தினேஷ்மொங்கியா 4-0-16-0 </p><p>
</p><p>
ஆட்டநாயகன்-சச்சின் டெண்டுல்கர் </p>

<p> </p>

<p> </p>

<p>டெண்டுல்கர் ருசிகர தகவல்:
ஷேவாக் அதிரடியை ரசித்தேன் </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>சச்சின் டெண்டுல்கர் </p>

<p> </p>

<p>ஹராரே, பிப். 20- இந்திய கிரிக்கெட்அணி அனைவரும் எதிர்பார்த்த
வெற்றியை ஜிம்பாப்வே அணியுடன் பெற்றது. தொடக்க வீரர்களாக களம் இறங்கிய டெண்டுல்கர்-
ஷேவாக் ஜோடி சிறப்பான முறையில் ரன் குவித்தது. ஷேவாக் 36 ரன் மட்டுமே எடுத்து ஆட்டம்
இழந்தார். இருந்தாலும் நட்சத்திர வீரர் டெண்டுல்கரின் புகழை ஷேவாக்
அள்ளிக்குவித்துள்ளார். </p><p>
</p><p>
ஷேவாக்கின் ஆட்டம் பற்றி டெண்டுல்கர் கூறுகையில், இந்திய அணிக்கு ஷேவாக் சிறப்பான
முறையில் ரன் குவிக்கவில்லை என்ற போதும் நேற்றைய போட்டியில் அவரை அதிகமாக ரசித்தேன்.
வீரு (வீரேந்தர்) மிகச்சிறந்த வீரர். அவரின் ஆட் டத்தை எதிர்முனையில் இருந்து பார்க்க
கொடுத்து வைத்திருக்கவேண்டும். கிட்டத் தட்ட அது ஒரு திருவிழா போன்ற மகிழ்ச்சியை
கொடுக்கும். நான் அந்த மகிழ்ச்சியை நேற்றைய போட்டியில் பெற்றேன். 9 ஆண்டுகளாக
தொடக்க வீரராக களம் இறங்கி சாதித்தேன். ஆனால் கடந்த சில போட்டிகளில் எனக்கு
மிகப்பெரிய ஏமாற்றம் கிடைத்தது. இப்போது மீண்டும் எனக்கு பிடித்தமான தொடக்க வீரர்
வாய்ப்பை பெற்றிருக்கிறேன். முன்பைப்போல் நிச்சயம் சாதிப்பேன் என்றார். நியூசிலாந்து
போட்டியின்போது தொடக்க வீரர் வாய்ப்புக்கு டெண்டுல்கர் கடுமையாக போராடியும் பலன்
கிடைக்கவில்லை. நல்லவேளையாக உலக கோப்பையில் டெண்டுல்கரின் கனவு நனவானது. </p>

<p> </p>

<p> </p>

<p>கனடாவை பந்தாடியது இலங்கை
36 ரன்னில் கனடா அவுட் </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>வாஸ் </p>

<p> </p>

<p>பார்ல், பிப். 20- கனடா அணி தனது அசுர வேகப்பந்து வீச்சால்
இலங்கை அணியை ஊதித்தள்ளியது. </p><p>
</p><p>
96-ம் ஆண்டு உலக கோப்பையை பிரதிபலிக்கும் வகையில் தென்ஆப்பிரிக்காவில் சாதனையுடன்
நகர்ந்து கொண்டிருக்கிறது இலங்கை அணி. உலக கோப்பைக்கு முன் னதாக இலங்கை அணி மோசமான
தோல்விகளை சந்தித்தது. ஆனால் தென்ஆப்பிரிக்காவில் எல்லாமே தலைகீழாக மாறி விட்டது.
சவால் கொடுக்கும் என்று நினைத்த நியூசிலாந்தை முதல் போட்டியில் தோற்கடித்த இலங்கை
பின்னர் வங்கதேசத்தை சுருட்டிவீசியது. இந்தப்போட்டியில் வேகப்பந்து வீச்சாளர் சமிந்தா
வாஸ் ஹாட்ரிக் சாதனை படைத்தார். </p><p>
</p><p>
இந்தநிலையில் கனடா அணியுடன் இலங்கை நேற்று மோதியது. டாஸ் வென்ற இலங்கை அணி
எதிர்அணியை முதலில் பேட்டிங் செய்ய வைத்தது. பந்துவீச்சு பலத்தை நம்பிய ஜெயசூர்யாவை சக
ஆட்டக்காரர்கள் கொஞ்சமும் ஏமாற்ற வில்லை. 21 ரன்னுக்கு 7 விக்கெட்டுகளை
பறிகொடுத்த கனடா பின்னர் 36 ரன்னில் அனைத்து விக்கெட்டுகளையும் கோட்டைவிட்டது.
முந்தைய போட்டியில் தனி ஆளாக எதிர்அணியை சிதறடித்த வாஸ் கனடாவுடன் 3 விக்கெட்டுகள்
மட்டும்தான் கைப்பற்றினார். நிசாங்கா 12 ரன்கள் விட்டுக்கொடுத்து 4 விக்கெட்டுகள்
சாய்த்தார். </p><p>
</p><p>
பின்னர் களம் இறங்கிய இலங்கை அணி 4.4 ஓவரில் ஒரு விக்கெட் இழந்து வெற்றி இலக்கை
கடந்தது. முதல்முறையாக ஜெயசூர்யா சொற்ப ரன்களில் பெவிலியன் திரும்பினார். 9 ரன்கள்
எடுத்த நிலையில் ஜெயசூர்யா கனடாவின் துரைசிங்கம் பந்துவீச்சில் ஆட்டம் இழந்தார்.
துரைசிங்கம் ஒரு இந்தியர் என்பது குறிப் பிடத்தக்கது. இந்த வெற்றியால் பி பிரிவில்
இலங்கை அணி 3 வெற்றிகளுடன் முதலிடத்தை பிடித்துள்ளது. இன்னும் ஒரு போட்டியில்
வெற்றிபெற்றால் சூப்பர்சிக்ஸ் வாய்ப்பை எட்டமுடியும் என்ற வலுவான நிலையில் இலங்கை
காத்திருக்கிறது. இலங்கை அணி இனிவரும் போட்டிகளில் தென்ஆப்பிரிக்கா, மேற்கு இந்தியதீவு
மற்றும் கென்யா அணிகளை எதிர்கொள்ள காத்திருக்கிறது. </p><p>
</p><p>
உலக கோப்பை கிரிக்கெட்டில் மிகக்குறைந்த ஸ்கோரில் ஆட்டம் இழந்த அணி என்ற சாதனையை
கனடா பெற்றது. கடந்த 79-ம் ஆண்டு உலக கோப்பையில் கனடா 45 ரன்னில் சுருண்டது. இப்போது
முந்தைய சாதனையையும் கனடா முறியடித்து விட்டது. நேற்றைய ஆட்டத்தில் இரு அணிகளும்
ஒட்டுமொத்தமாக 20 ஓவர்கள்தான் ஆடின என்பதும் புதிய சாதனைதான். சர்வதேச கிரிக்கெட்டில்
இவ்வளவு குறைவான ஓவர்களில் முடிவு கிடைப்பது இதுவே முதல்முறை ஆகும். நேற்றைய ஆட்டத்தில்
கனடா வீரர்கள் 6 பேர் ரன்கணக்கை தொடங்காமல் ஆட்டம் இழந்தனர். நேற்றைய ஆட்டத்தில்
4 விக்கெட் கைப்பற்றிய நிஷாங்கா ஆட்டநாயகனாக தேர்வு செய் யப்பட்டார். </p><p>
</p><p>
கனடாவுக்கே முதலிடம் </p><p>
</p><p>
சர்வதேச கிரிக்கெட்டில் மிகக்குறைந்த ஸ்கோர் எடுத்த அணி வரிசையில் கனடா முதலிடத்தை
எட்டியிருக்கிறது. உலக கோப்பை கிரிக்கெட்டை பொறுத்தவரையிலும் கனடாவுக்கே முதலிடம்
கொடுக்கப்பட்டுள்ளது. இந்த வித்தியாசமான சாதனை படைத்துள்ள விவரங்களை கொஞ்சம்
பார்ப்போம். </p><p>
</p><p>
ஒருநாள் கிரிக்கெட் </p><p>
</p><p>
அணிஸ்கோர்எதிர்அணிஆண்டு </p><p>
</p><p>
கனடா36இலங்கை2003 </p><p>
</p><p>
ஜிம்பாப்வே38இலங்கை2000 </p><p>
</p><p>
பாகிஸ்தான்43மேற்கு இந்தியதீவு1993 </p><p>
</p><p>
கனடா45இங்கிலாந்து1979 </p><p>
</p><p>
இந்தியா54இலங்கை2000 </p><p>
</p><p>
உலக கோப்பை கிரிக்கெட் </p><p>
</p><p>
கனடா36இலங்கை2003 </p><p>
</p><p>
கனடா45இங்கிலாந்து1979 </p><p>
</p><p>
ஸ்காட்லாந்து68மேற்கு இந்தியதீவு1999 </p><p>
</p><p>
பாகிஸ்தான்74இங்கிலாந்து1992 </p><p>
</p><p>
நமீபியா84பாகிஸ்தான்2003 </p>

<p> </p>

<p> </p>

<p>நமீபியாவை வீழ்த்தியது
இங்கிலாந்து </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>அலெக் ஸ்டூவர்ட் </p>

<p> </p>

<p>போர்ட்எலிசபெத், பிப். 20- நமீபியா அணிக்கு எதிரான
ஆட்டத்தில் இங்கிலாந்து போராடி வெற்றிபெற்றது. </p><p>
</p><p>
ஜிம்பாப்வேயுடன் ஆட மறுத்த இங்கிலாந்து கிரிக்கெட் அணி நேற்றைய ஆட்டத்தில் நமீபியாவை
எதிர்கொண்டது. இங்கிலாந்து காப்டன் நாசர் உசைன் நேற்றைய ஆட்டத்தில் களம்இறங்க
வில்லை. முதலில் பேட்டிங் செய்த இங்கிலாந்து அணி நிர்ணயிக்கப்பட்ட 50 ஓவரில் 272
ரன்னுக்கு அனைத்து விக்கெட்டுகளையும் கோட்டைவிட்டது. மூத்தவீரர் அலெக் ஸ்டூவர்ட்
அதிகபட்சமாக 60 ரன் எடுத்தார். தொடக்க வீரர் டிரெஸ்கோதிக் 58 ரன் பெற்றார்.
எதிர்பார்ப்பை உண்டாக்கிய வாகன் மற்றும் பிளின்டாப் ஆகியோர் சொற்ப ரன்களில்
பெவிலியன் திரும்பினர். </p><p>
</p><p>
நமீபியா அணியின் வுரென் 5 விக்கெட்டுகள் கைப்பற்றினார். பின்னர் களம் இறங்கிய
நமீபியா அணி ஆட்டத்தின் கடைசிவரை இங்கிலாந்து அணிக்கு நெருக்கடி கொடுத்தது. 50 ஓவர்களை
முழுதாக ஆடிய நமீபியா அணி 9 விக்கெட் இழந்து 217 ரன் பெற்றது. அந்த அணியில் பர்கர்
அதிகபட்சமாக 85 ரன்கள் விளாசினார். முன்னதாக ஹாலந்துக்கு எதிரான போட்டியில்
இங்கிலாந்து எளிதாக வெற்றி பெற்றது. இதனால் ஏ பிரிவில் இங்கிலாந்து 8 புள்ளிகளுடன்
4-வது இடத்தில் உள்ளது. இன்னும் இந்த அணிக்கு 3 மோதல்கள் எஞ்சி இருக்கின்றன. சூப்
பர்சிக்ஸ் வாய்ப்பை எட்ட இங்கிலாந்து கடும் போராட்டம் இருக்கும் என்று
எதிர்பார்க்கப்படுகிறது. வலுவான ஆஸ்திரேலியா, இந்தியா, பாகிஸ்தான் ஆகிய அணிகளுடன்
இங்கிலாந்து மோத உள்ளது. </p><p>
</p><p>
அணிகள் பெற்ற புள்ளிகள் </p><p>
</p><p>
உலக கோப்பை கிரிக்கெட்டில் இதுவரை நடந்த மோதல்களின் அடிப்படையில் அணிகள் பெற்ற புள்ளிகள்
மற்றும் ரன்விகித விவரங்கள் வருமாறு:- </p><p>
</p><p>
ஏ பிரிவு </p><p>
</p><p>
அணிமோதல்வெற்றிதோல்விபுள்ளிவிகிதம் </p><p>
</p><p>
ஆஸ்திரேலியா22-8+2.49 </p><p>
</p><p>
இந்தியா3218+0.32 </p><p>
</p><p>
ஜிம்பாப்வே3218+0.04 </p><p>
</p><p>
இங்கிலாந்து3218+2.08 </p><p>
</p><p>
பாகிஸ்தான்2114+0.93 </p><p>
</p><p>
ஹாலந்து2020-1.965 </p><p>
</p><p>
நமீபியா3030-2.49 </p><p>
</p><p>
பி பிரிவு </p><p>
</p><p>
இலங்கை33-12+3.17 </p><p>
</p><p>
நியூசிலாந்து3218-0.08 </p><p>
</p><p>
மேற்கு இந்தியதீவு3116-0.22 </p><p>
</p><p>
தென்ஆப்பிரிக்கா3124+1.14 </p><p>
</p><p>
கென்யா2114-1.32 </p><p>
</p><p>
கனடா3124-0.68 </p><p>
</p><p>
வங்கதேசம்3022-1.86 </p><p>
</p><p>
மேற்கு இந்தியதீவு-வங்கதேச ஆட்டம் மழையால் ரத்து. </p><p>
</p><p>
தலா 2 புள்ளிகள் பகிர்ந்துகொடுக்கப்பட்டன. </p><p>
</p><p>
இன்றைய மோதல்கள் </p><p>
</p><p>
ஆஸ்திரேலியா-ஹாலந்து </p><p>
</p><p>
மதியம் 1.30 மணி </p>

<p> </p>

<p> </p>

<p> </p>

<p>இந்திய கிரிக்கெட் வீரர்
திராவிட் வீட்டில் ரசிகர்கள் கல்வீசிதாக்குதல்: கார் கண்ணாடி உடைப்பு </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>திராவிட் </p>

<p> </p>

<p>பெங்களூர்,பிப்.20- இந்திய கிரிக்கெட் வீரர் திராவிட் வீட்டில்
ரசிகர்கள் கல்வீசி தாக்கினர். இதில் கார் கண்ணாடி உடைந்தது. </p><p>
</p><p>
பாதுகாப்பு </p><p>
</p><p>
உலக கோப்பை கிரிக்கெட் போட்டில் ஆஸ்திரேலியாவுக்கு எதிரான லீக் போட்டியில் இந்தியா
மோசமாக ஆடி தோற்றது அல்லவா? இதனால் வெறுப்பு அடைந்த ரசிகர்கள் இந்திய கிரிக்கெட்
வீரர்களின் வீட்டுக்கு முன் கலாட்டா செய்து வருகின்றனர். </p><p>
</p><p>
திராவிட் கார் உடைப்பு </p><p>
</p><p>
இதனை தொடர்ந்து பெங்களூரில் உள்ள இந்திய கிரிக்கெட் வீரர்கள் கும்ளே, நாத்,
திராவிட் ஆகியோரின் வீடுகளுக்கு பலத்த போலீஸ் பாதுகாப்பு போடப்பட்டு உள்ளது.
இந்நிலையில் பெங்களூர் இந்திரா நகரில் உள்ள ராகுல் திராவிட் வீட்டில் ரசிகர்கள்
கல்வீசி தாக்கினர். இதில் அவரது வீட்டின் முன் நிறுத்தி இருந்த காரின் கண்ணாடி உடைந்தது. </p><p>
</p><p>
செருப்பு மாலை </p><p>
</p><p>
மேலும் பெங்களூர் டிக்கன் சன்ரோடு- எம்.ஜி. ரோடு சந்திப்பில் வைக்கப்பட்டு இருந்த
திராவிட்டின் விளம்பர படத்தின் மீது ரசிகர்கள் சாணியால் அடித்து, செருப்பு மாலை அணிந்து
உள்ளனர். </p><p>
</p><p>
கார் கண்ணாடியை ரசிகர்கள் உடைத்தது பற்றி திராவிட்டின் குடும்பத்தினர் இந்திரா நகர்
போலீசில் புகார் செய்தார். போலீசார் வழக்கு பதிந்து மேலும் விசாரணை நடத்தி
வருகின்றனர். </p>

<p> </p>






</body></text></cesDoc>