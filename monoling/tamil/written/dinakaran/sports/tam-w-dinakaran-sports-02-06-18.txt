<cesDoc id="tam-w-dinakaran-sports-02-06-18" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-dinakaran-sports-02-06-18.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker and Celia Worth</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Dinakaran" internet news (www.dinakaran.com), news stories collected on 02-06-18</h.title>
<h.author>Dinakaran</h.author>
<imprint>
<pubPlace>Tamil Nadu, India</pubPlace>
<publisher>Dinakaran</publisher>
<pubDate>02-06-18</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>நட்சத்திர வீரர்கள்
ரொனால்டோ-ரிவால்டோ அபாரம்: 2 கோல் போட்டு பிரேசில் அணி வெற்றி</p>

<p> </p>

<p>கோப், ஜூன் 18- 2 கோல் அடித்து பிரேசில் அணி கால்இறுதிக்கு
தகுதிபெற்றது. </p><p>
</p><p>
முன்னணி வீரர்கள் </p><p>
</p><p>
நட்சத்திர அந்தஸ்து பெற்றுள்ள பிரேசில் கால்பந்து அணி நேற்றைய 2-வது சுற்றில் பெல்ஜியம்
அணியுடன் மோதியது. பிரான்ஸ் அணி வௌியேறியதை அடுத்து சாம்பியன் வாய்ப்பு பிரேசில்
அணிக்கு அதிகமானது. இதை நிரூபிக்கும் வகையில் தொடக்க ஆட்டத்தில் இருந்து பிரேசில்
வீரர்கள் சிறப்பாக ஆடிவருகிறார்கள். முன் னணி வீரர்கள் ரொனால்டோ, ரிவால்டோ,
கார்லோஸ், ரொனால்டின்கோ ஆகியோர் நேற்றைய ஆட்டத்தில் ஜோடி சேர்ந்தனர். </p><p>
</p><p>
ரிவால்டோ கோல் </p><p>
</p><p>
முதல்பாதியில் இரு அணி வீரர்களும் கோல்போட முயன் றும் பலன் கிடைக்க வில்லை. 2-வது
பாதியின் தொடக்கத்தில் பிரேசில் வீரர்கள் கொஞ்சம் மந்தமாக ஆடினர். ஆனாலும்
பிரேசில் நட்சத்திர ஆட்டக்காரர்களின் வேகத்துக்கு பெல் ஜியம் வீரர்களால்
ஈடுகொடுக்க முடியவில்லை. ஆட்டத்தின் 67-வது நிமிடத்தில் ரொனால் டின்கோ பாஸ் செய்த
பந்தை ரிவால்டோ சிறப்பாக நெஞ்சில் வாங்கி, நிதானமாக கோல் அடித்தார். </p><p>
</p><p>
பிரேசில் வெற்றி </p><p>
</p><p>
முதல் கோல் போட்ட விறுவிறுப்பில் பிரேசில் வீரர்கள் வழக்கம்போல் ஆதிக்கம் செய்யத்
தொடங்கி விட்டனர். ஆட்டத்தின் 87-வது நிமிடத்தில் ரொனால்டோ எதிர்அணியின்
கோல்கீப்பர் கால்களுக்கு நடுவில் பந்தை பாஸ் செய்த கோல் போட்டது ரசிகர்களுக்கு புதிய
உற்சாகத்தை கொடுத்தது. இதன் மூலம் பிரேசில் அணி 2-0 என்ற கோல் வித்தியாசத்தில்
வெற்றிபெற்று கால்இறுதிக்கு முன்னேறியது. கால்இறுதியில் பிரேசில்-இங்கிலாந்து அணிகள்
பலப்பரீட்சை நடத்துகின்றன. </p><p>
</p><p>
ரொனால்டோ முன்னணி </p><p>
</p><p>
இந்த உலக கோப்பை கால்பந்து போட்டியில் சிறந்த வீரர் பட்டத்தை வெல்லும் வாய்ப்பை
ரொனால்டோ அதிகப்படுத்திக் கொண்டார். 4 ஆட்டங்களில் முடிவில் ரொனால்டோ 5
கோல்களுடன் முதலிடத்தை எட்டினார். ஜெர்மனி வீரர் மிரோஸ்லவ் குளோஸ் 5 கோல்கள்
அடித்துள்ளது குறிப்பிடத் தக்கது. பிரேசில் அணியின் ரிவால்டோ 4கோல்கள் அடித்துள்ளார். 4
முறை சாம்பியன் கோப்பை கைப்பற்றி உள்ள பிரேசில் அணி கடந்த உலக கோப்பை
இறுதிப்போட்டியில் 3 கோல் வாங்கி பிரான்சிடம் தோற்றது. இதற்கு இந்தமுறை நிச்சயம்
பிரேசில் அணி ஆறுதல் தேடிக்கொள்ளும் என்று எதிர்பார்க்கப்படுகிறது. </p>

<p> </p>

<p> </p>

<p>கால்இறுதிக்கு அமெரிக்கா
தகுதி</p>

<p> </p>

<p>சாஞ்சு, ஜூன் 18- உலக கோப்பை கால்பந்து போட்டிக்கான
கால்இறுதிக்கு அமெரிக்கா அணி தகுதிபெற் றது. </p><p>
</p><p>
ஜப்பான் மற்றும் தென்கொரியா நாடுகளில் நடந்து வரும் உலக கோப்பை கால் பந்து போட்டியில்
அடுத்தடுத்து பல அதிர்ச்சியான நிகழ்வுகள் நடந்து வருகின்றன. கத்துக் குட்டி அணியான செனகல்,
நடப்பு சாம்பியன் பிரான்சை மண்கவ்வச் செய்தது. டென் மார்க் அணியும் பிரான்சை
தோற்கடித்தது. அத்தோடு நிற்காமல் கால்பந்தில் பிரபலம் அடையாத அமெரிக்கா அணி வலுவான
போர்ச்சுக்கல் அணிக்கு நெருக்கடி கொடுத்து தோற்கடித்தது. </p><p>
</p><p>
இதுபோன்ற சில அதிர்ச்சிகளை சந்தித்த கால்பந்து ரசிகர் களுக்கு அமெரிக்கா-மெக் ஸிகோ
அணிகளின் மோதலும் அதிர்ச்சியை கொடுத்தது. அடுத்த சுற்று வாய்ப்பை அதிர்ஷ்டவசமாக பெற்ற
அமெரிக்கா அணி கடைசி லீக் போட்டியில் போலந்து அணியிடம் தோல்வி கண்டது. இந்த தோல்
வியின் பிரதிபலிப்பு நேற்றைய போட்டியில் கொஞ்சமும் அமெரிக்க வீரர்களிடம்
தென்படவில்லை. ஆட்டத்தின் 8-வது நிமிடத்தில் அமெரிக்காவின் பிரைன் மெக்பிரைட் முதல்
கோல் போட்டார். </p><p>
</p><p>
2-வது பாதி ஆட்டத்தில் மெக்ஸிகோ வீரர்கள் அதிக பதட்டத்துடன் ஆடினர். கிடைத்த சில
கோல் வாய்ப்பு களையும் அந்த அணி வீரர்கள் கோட்டைவிட்டனர். ஒட்டுமொத் தமாக 10
மஞ்சள் அட்டை காண்பிக்கப்பட்ட இந்த ஆட்டத் தின் 65-வது நிமிடத்தில் அமெ
ரிக்காவின் டோனோவன் லேன் டன் 2-வது கோல் போட்டார். ஆட்டத்தின் முடிவுவரை
மெக்ஸிகோ வீரர்கள் கோல் ஏதும் போடவில்லை. இதனால் இந்த போட்டியில் அமெரிக்கா 2
கோல் போட்டு வெற்றிபெற்றது. கால்இறுதிப்போட்டியில் அமெரிக்கா-ஜெர்மனி அணிகள்
பலப்பரீட்சை நடத்துகின்றன. </p><p>
</p><p>
புஸ் வாழ்த்து </p><p>
</p><p>
அமெரிக்க கால்பந்து உலக கோப்பை கால்இறுதிக்கு தகுதி பெற்றது. மெக்ஸிகோவுக்கு எதிரான
ஆட்டத்தில் அமெரிக்கா 2-0 என்ற கோல்கணக்கில் வெற்றிபெற்றது. கால்பந்தில் அவ்வளவாக
சாதிக்காத அமெரிக்க அணிக்கு இந்த வெற்றி புதிய உற்சாகம் கொடுத்துள்ளது.
போட்டித்தொடங்குவதற்கு முந்தையநாள் அதிகாலை அமெரிக்க அதிபர் புஸ் தொலைபேசி மூலம்
தொடர்பு கொண்டு பயிற்சியாளருடன் பேசி உள்ளார். அப்போது அணியின் வெற்றிக்காக
வாழ்த்துக்கள் தெரிவித்த புஸ், கால்பந்து பற்றி அதிக விழிப்புணர்வு இல்லாத அமெரிக்க
மக்களுக்கு நீங்கள் பெறும் வெற்றி புதிய வழிகாட்டுதலாக இருக்க வேண்டும் என்று
கூறியுள்ளார். புஸ் நினைத்ததுபோலவே அமெரிக்க அணிக்கு வெற்றி கிடைத்துள்ளது குறிப்பிடத்தக்கது.
</p>

<p> </p>

<p>  </p>

<p>கொரியா சவாலை சமாளிக்குமா
இத்தாலி?</p>

<p> </p>

<p>டேஜியோன், ஜூன் 18- கொரியா அணியின் சவாலை இத்தாலி
சமாளிக்குமா? என்ற கேள்விக்குறி அனைவரின் மனதிலும் உண்டாகி உள்ளது. </p><p>
</p><p>
உலக கோப்பை கால்பந்து போட்டி முதல்முறையாக ஆசிய கண்டத்தில் நடக்கிறது. அதுவும்
தென்கொரியா மற்றும் ஜப்பான் நாடுகள் போட்டியை இணைந்து நடத்துகின்றன. உலக கால்பந்து
வழக்கப்படி இரு அணிகளும் 2-வது சுற்றுக்கு முன்னேறின. இன்று இந்த இரு அணிகளுக்கும் சவால்
காத்திருக்கிறது என்பது மறுக்க முடியாத உண்மை. தென்கொரியா அணி இன்று நடக்கும் போட்டி யில்
இத்தாலியுடன் பலப்பரீட்சை நடத்துகிறது. 3 முறை உலக கோப்பை கைப்பற்றி உள்ள இத்தாலி
அணி ஒருவித பீதியில் இன்றைய போட்டியில் களம் இறங்க உள்ளது. </p><p>
</p><p>
உள்ளூரில் போட்டி நடப்பதால் கொரியா அணிக்கு பலத்த ஆதரவு உள்ளது. ரசிகர்களின் இந்த
ஆதரவுடன் துடிப்பாக ஆடிவரும் கொரியா வீரர்களின் சவாலை சமாளிப்பது என்பது இத்தாலி அணிக்கு
பெரிய தலைவலி. இந்த தலைவலியையும் சமாளித்து கொரியா அணியை தோற்கடிப் பது என்பது பெரிய
சவால் தான். முன்னணி வீரர்கள் இருக்கும் நம்பிக்கையில் இத்தாலி அணி லேசான தெம்புடன்
இருக்கிறது. ஆனால் முன்னணி வீரர்கள் எதிர்பார்ப்பை நிறைவேற்றுவார்களா? என் பதை
பொறுத்திருந்துதான் பார்க்க வேண்டும். </p><p>
</p><p>
இன்று நடக்கும் மற்றொரு போட்டியில் ஜப்பான்-துருக்கி அணிகள் பலப்பரீட்சை நடத்த உள்ளன.
போட்டியை நடத்தும் நாடு என்பதால் ஜப்பான் அணி மீதும் அதிகமான எதிர்பார்ப்பு
ஏற்பட்டுள்ளது. உலக கோப்பை 2-வது சுற்றுக்கு ஜப்பான் மற் றும் துருக்கி அணிகள் முதல் முறையாக
தகுதிபெற்றுள்ளன. இரு அணிகளும் சமநிலையில் இருப்பதால் மோதல் நிச்சயம் விறுவிறுப்பாக
இருக்கும் என்று நம்பலாம். ஜப்பான் அணியில் நட்சத்திர வீரர் நக்காட்டா சாதிக்கும்
நோக்கில் உள்ளார். அதுபோல், துருக்கி அணியில் ஹசன்சாஸ் இந்த உலக கோப் பையின்
அனைத்து மோதல் களிலும் சிறப்பாக ஆடிவருகிறார். எனவே அவர்மீதும் எதிர் பார்ப்பு
ஏற்பட்டுள்ளது. இன் றைய போட்டிகள் இரண்டையும் காண டிக்கெட்டுகள் அனைத்தும் ஏற்கனவே
விற்று தீர்ந்து விட்டநிலையில், மைதானத்தில் கூடுதல் இடம் ஒதுக்க வேண்டும் என்று
ரசிகர்கள் கோரிக்கை விடுத்துள்ளனர். </p><p>
</p><p>
இன்றைய மோதல்கள் </p><p>
</p><p>
உலக கோப்பை கால்பந்து போட்டிக்கான 2-வது சுற்றில் நடக்கும் இன்றைய மோதல்கள் பற்றிய
விவரங்கள் வருமாறு:- </p><p>
</p><p>
மோதும் அணிகள்இந்திய நேரம் </p><p>
</p><p>
ஜப்பான்-துருக்கிமதியம் 12.00 மணி </p><p>
</p><p>
இத்தாலி-கொரியாமாலை 5.00 மணி </p><p>
</p><p>
உலக கோப்பையில் அதிக கோல் </p><p>
</p><p>
உலக கோப்பை கால்பந்து போட்டியில் அதிக கோல் அடித்து முன்னணி இடத்தை பிடித்துள்ள வீரர்
மற்றும் அணிகள் பற்றிய விவரம் வருமாறு:- </p><p>
</p><p>
குளோஸ் (ஜெர்மனி)-5 </p><p>
</p><p>
ரொனால்டோ (பிரேசில்)-5 </p><p>
</p><p>
ரிவால்டோ (பிரேசில்)-4 </p><p>
</p><p>
தோமாசன் (டென்மார்க்)-4 </p><p>
</p><p>
வியரி (இத்தாலி)-3 </p><p>
</p><p>
லார்சன் (ஸ்வீடன்)-3 </p><p>
</p><p>
மோரியன்டஸ் (ஸ்பெயின்)-3 </p><p>
</p><p>
வில்மோட் (பெல்ஜியம்)-3 </p><p>
</p><p>
பவுபா (செனகல்)-3 </p><p>
</p><p>
பாலெட்டா (போர்ச்சுக்கல்)-3 </p>

<p> </p>

<p> </p>

<p>நியூசிலாந்துடன் ஒருநாள்
கிரிக்கெட்: கடைசி பந்தில் மேற்கு இந்தியதீவு வெற்றி</p>

<p> </p>

<p>வின்சென்ட், ஜூன் 18- நியூசிலாந்துடனான ஒருநாள் கிரிக்கெட்
போட்டியில் கடைசி பந்தில் மேற்கு இந்திய தீவு அணி வெற்றி பெற்றது. </p><p>
</p><p>
நியூசிலாந்து கிரிக்கெட் அணி மேற்கு இந்தியதீவில் சுற்றுப்பயணம் செய்து ஒருநாள்
போட்டிகளில் ஆடிவருகிறது. இந்த இரு அணிகளும் மோதிய கடைசி ஒருநாள் போட்டி விறுவிறுப்புடன்
தொடங்கியது. முதலில் பேட்டிங் செய்த நியூசிலாந்து அணி நிர்ணயிக்கப்பட்ட 50 ஓவரில் 8
விக்கெட் இழந்து 291 ரன் எடுத்தது. கடினமான வெற்றி இலக்குடன் களம் இறங்கிய மேற்கு
இந்தியதீவு அணியில் முன்னணி வீரர்கள் அனைவரும் சிறப்பாக ரன் எடுத்தனர். ஆனாலும் கடைசி
ஓவரில் 15 ரன் எடுத்தால் வெற்றி என்ற சிக்கல் மேற்கு இந்தியதீவு அணிக்கு ஏற்பட்டது.
முதல் 2 பந்தில் ஒரு ரன் எடுத்த மேற்கு இந்தியதீவு அணியில், நம்பிக்கை வீரர்
சந்தர்பால் அடுத்த 3 பந்துகளையும் பவுண்டரிக்கு விளாசினார். கடைசி பந்தில்
வெற்றிக்குத் தேவையான ஒரு ரன்னை எடுத்து மேற்கு இந்தியதீவு வெற்றிபெற்றது. இதனால் 5
போட்டிகள் கொண்ட ஒருநாள் தொடரை மேற்கு இந்தியதீவு அணி 3-1 என்ற புள்ளிக்கணக்கில்
கைப்பற்றியது. மழை காரணமாக ஒரு ஆட்டம் நடக்க வில்லை என்பது குறிப்பிடத்தக்கது. </p>

<p> </p>

<p> </p>

<p>கிரிக்கெட் வீரர்
டிசில்வாவிடம் திருட்டு</p>

<p> </p>

<p>லண்டன், ஜூன் 18- கிரிக்கெட் வீரர் டிசில்வாவிடம் இருந்து
ரூ.2.25 பணம் திருடப்பட்டுள்ளது. </p><p>
</p><p>
இலங்கை கிரிக்கெட் அணியில் அதிகமான ரன்கள் குவித்து சாதனை படைத்துள்ளவர் அரவிந்த
டிசில்வா. இங்கிலாந்துக்கு எதிரான டெஸ்ட் போட்டியில் ஆட இலங்கை அணியில் டிசில்வா
வாய்ப்பு பெற்றார். ஒருநாள் போட்டிக்கான அணியில் அவருக்கு வாய்ப்பு கிடைக்க வில்லை.
லண்டனில் தங்கி இருந்த போது டிசில்வாவிடம் இருந்து ரூ.2.25 லட்சம் திருட்டு போய் உள்ளது.
விளம்பரம் ஒன்றில் நடித்ததிற்காக டிசில்வாவுக்கு ரூ.2.25 லட்சத்துக்கான காசோலை
கொடுக்கப்பட்டது. இந்த காசோலையை மாற்றி ஓட்டல் அறையில் டிசில்வா பணமாக
வைத்திருந்தார். வௌியில் போய்விட்டு திரும்பிய போது தனது அறையில் வைத்திருந்த பணம்
காணாமல் போனது தெரியவந்தது. உடனடியாக ஓட்டல் நிர்வாகிகளிடம் இதுபற்றி டிசில்வா புகார்
செய்தார். ஆனால் திருட்டு போன பணம் கிடைக்க வில்லை. </p>

<p> </p>






</body></text></cesDoc>