<cesDoc id="tam-w-legislation-6nadu6" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-legislation-6nadu6.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-04</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>தமிழ்நாடு சட்டமன்றப் பேரவை நடவடிக்கைகள் செயலக வெளியீடு</h.title>
<h.author>****</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1990</pubDate>
</imprint>
<idno type="CIIL code">6nadu6</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 5.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-04</date></creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>		    தமிழ்நாடு
	சட்டமன்றப் பேரவை நடவடிக்கைகள்
		செயலக வெளியீடு</p>

<p>வெளியீடு : 24.11.1990
பதிப்பகம் : சட்டமன்றப் பேரவைச் செயலகம்,
	    சென்னை - 600 009.</p>

<p>-618-</p>

<p>		2. தகவல் கோரல்</p>

<p>(அ)	டெல்லியில் நடைபெற்ற இந்தியத் தொழிலாளர்கள் மாநாட்டில்
	கலந்து கொண்ட தொழிலாளர்கள் நலத்துறை அமைச்சர்
	அவைக்கு விளக்கம் அளிக்க வேண்டியது குறித்து.</p>

<p>10-05 மணி</p>

<p>	திரு. சி. கோவிந்தராஜன் : மாண்புமிகு பேரவைத்
தலைவர் அவர்களே, சென்ற மாதம் 20, 21 மற்றும் 22 ஆகிய
தேதிகளில் டெல்லியில் இந்தியத் தொழிலாளர் மாநாடு நடை
பெற்றது. அது தொழிலாளர்களுடைய பிரதிநிதிகள் முதலாளி~
களுடைய பிரதிநித஖ள் மற்றும் அரசு பிரதிநிதிகள் எல்லாம் கலந்து
கொண்ட ஒரு மாநாடு. 5 ஆண்டுகளுக்குப் பிறகு இப்போதுதான்
அது டில்லியில் நடைபெற்றது. அதில் நம்முடைய மாண்புமிகு
தொழிலாளர் நல அமைச்சர் அவர்களும் பங்குகொண்டு விட்டு
வந்திருக்கிறார்கள். அந்த மாநாட்டுக்கு முன்னதாக ட்ரிப்பார்ட்
டைட் ஸ்டாண்டிங் கமிட்டியும் நடைபெற்றிருக்கிறது.
அதிலும் அவர்கள் பங்கு கொண்டிருக்கிறார்கள். மாநில தொழி~
லாளர் நல அமைச்சர்களுடைய மாநாட்டிலும் அவர்கள் பங்கு
பெற்றிருக்கிறார்கள். அவர்கள் திரும்பி வந்து 10 நாட்களுக்கு
மேல் ஆகிவிட்டது. நான் அவ்வப்போது அதைப்பற்றி இங்கே
எழுப்பியபோதிலும் கூட, இதைவிட முக்கியமான பிரச்சினைகள்
அந்த மன்றத்தில் வந்த காரணத்தாலே இது எடுத்துக்கொள்ளப்
படவில்லை. அவர் போய் வந்து பல நாட்கள் ஆகியுள்ள
நிலையில் பல செய்திகள் பத்திரிகைகளில் வந்து கொண்டிருக்~
கின்றன. குறிப்பாக, தொழிற்சாலை நிர்வாகத்தில் தொழி~
லாளர்களுடைய பங்கு, போனஸ் பிரச்சினையில் மத்திய அரசின்
புதிய நிலை, தொழிலாளர் சங்கங்களை அங்கீகரித்தலில் மத்திய
அரசின் புதிய நிலை, இவைகளைப் பற்றி எல்லாம் பத்திரிகை~
களில் செய்திகள் வெளி வந்திருக்கின்றன. அதுமாத்திர~
மல்லாமல், பல ஆண்டுகளுக்கு முன்பு தயார் செய்யப்பட்டு,
மசோதாவாக ஆக்கப்பட்டு, பார்லிமென்டிலே பல தடவை
கொண்டுவரப்பட்டு, தொழிலாளர்களுடைய எதிர்ப்பு காரணமாக
கைவிடப்பட்டிருந்த அந்த தொழில் உறவு மசோதா கூட பின்
வாங்கப்பட்டதாகவும், புதிய மசோதா தயாரிக்கப்படுவதாகவும்
கூட செய்தி வந்திருக்கிறது. இப்படிப்பட்ட விவாதங்களில்
எல்லாம் நம்முடைய மாண்புமிகு தொழிலாளர் நல அமைச்சர்
அவர்கள் பங்குக் கொண்டு வந்திருக்கிறார்கள். அதைவிட
முக்கியமாக தமிழகத்தில் மூடிக் கிடக்கின்ற பஞ்சாலைகளைத்
திறப்பதற்கான ஒரு முயற்சியையும் மாண்புமிகு தொழிலாளர்
நல அமைச்சர் அவர்கள் அங்கு பேசி, ஏதோ தகவல் கொண்டு
வந்திருப்பார் என்று கருத வேண்டிய சூழ்நிலை இருக்கிறது.
இவைகளை எல்லாம் விளக்கமாக இந்த மன்றத்திற்கு
மாண்புமிகு தொழிலாளர் நல அமைச்சர் அவர்கள் தருவார்~
களா என்பதை அறிய விரும்புகிறேன்.
-619-
	மாண்புமிகு திரு. கே. என். நேரு : மாண்புமிகு பேரவைத்
தலைவர் அவர்களே, 19-4-1990 அன்று காலை முதல் நாள் ஆல்
இந்தியா இன்பர்மேசன் மினிஸ்டர்ஸ் கான்பரன்ஸ் முடிந்த
பிறகு 19 ஆம் தேதி காலை மாண்புமிகு தமிழக கைத்தறித்
துறை அமைச்சர் அண்ணன் திரு. தங்கவேலு அவர்களுடன்
நமது மத்திய அமைச்சர் மாண்புமிகு திரு. மாறன் அவர்களைச்
சந்தித்து, மூடிய ஆலைகளைத் திறப்பதற்காக அரசு ஒரு
முடிவு செய்து, தமிழகத்தினுடைய முதல்வர் அவர்கள் மத்திய
கைத்த஼றித் துறை அமைச்சருக்கு நேரடியாக கடிதம் எழுதி~
யிருக்கின்றார்கள். நாங்கள் இப்போது மத்திய கைத்தறித்
துறை அமைச்சரைச் சந்திக்கச் சென்றோம். அவர்களிடம்
மூடிய மில்களைத் திறப்பதற்கு அனுமதி தரச் சொல்லுங்கள்
என்று சொல்லி, காலை 9 மணி அளவிலே நமது மத்திய
அமைச்சர் அவர்களைச் சந்தித்தோம். அங்கு மத்திய
அமைச்சரும், இந்தத் துறையினுடைய செயலாளரையும், தலை~
வரையும், லேபர் கமிஷனர் அவர்களையும் தீர விசாரித்து, இந்த
அரசின் சார்பாக இதுவரை என்னென்ன நடவடிக்கை எடுக்கப்~
பட்டிருக்கிறது, நீங்கள் ஏன் மத்திய அரசிலிருந்து இதற்கு
அனுமதி தர மறுக்கிறீர்கள் என்பதை எல்லாம் விளக்கமாக
கேட்டுக்கொண்டு பிறகு மத்திய கைத்தறித் துறை அமைச்சர்
மாண்புமிகு திரு. யாதவ் அவர்களுடன் நேரடியாகத் தொலை~
பேசியில் பேசி, எங்களுடைய அரசு, எங்களுடைய மாண்புமிகு
முதல்வர் அவர்கள் அரசியல் முடிவு எடுத்திருக்கிறார்கள்,
இப்போது மூடிய மில்களை எல்லாம் திறக்க வேண்டு~
மென்றும், அதற்காக மத்திய அரசிலிருந்து எந்தப் பணமும்
நீங்கள் தர வேண்டியதில்லை. மத்திய அரசின் நிர்வாக
அனுமதி மட்டும் தர வேண்டுமென்று மத்திய அமைச்சரி~
டத்தில் சொல்லி, மத்திய கைத்தறித் துறை அமைச்சர் அவர்~
களும் அதை ஏற்றுக்கொண்டார்கள். இது நடைபெற்றது
காலை 9 மணி அளவிலே, மீண்டும் நாங்கள் அவரைச் சந்திக்க
12 மணிக்கு நேரம் ஒதுக்கி தந்தார்கள். பகல் 12 மணியளவில்
மாண்புமிகு அமைச்஼ச஼ர் திரு. தங்கவேலு அவர்களும், இயக்குநர்
அவர்களும், தொழிலாளர் நல ஆணையாளர் அத்தனை
பேரோடு சென்று மத்திய கைத்தறித் துறை அமைச்சர்
அவர்களைச் சந்தித்தோம். நாங்கள் மத்திய அமைச்சரை
 சந்தித்து பேசிய அதே நாளில், அதே நேரத்தில் காலையிலே
மகாராஷ்டிரா மாநிலத்திலிருந்து தமிழ்நாட்டில் நாம் திறப்பது
போல அவர்களும் மூடிய மில்களை திறப்போம் என்று ஒரு
அறிக்கையை அந்த மாநில அரசின் சார்பின் வெளி௟ிட்டி~
ருந்தார்கள். மகாராஷ்டிராவில் மட்டுமல்லாமல், குஜ~
ராத்திலும் மூடிய ஆலைகளைத் திறப்போம் என்று அந்த
அரசு சார்பிலும் சொல்லியிருக்கிறார்கள். இப்படி எல்லாம்
அன்று காலையில் செய்தி வந்ததிலிருந்து இவைகளைப்
பற்றி எல்லாம் இந்தத் துறையினுடைய செயலாளர் மத்திய
-620-
அமைச்சரிடம் எடுத்துச் சொன்னார். தமிழ்நாட்டிற்கு நாம்
கேட்பதுபோல அனுமதி வழங்கினால் மற்ற மாநிலங்களும்
அதுபோல கேட்கும்போது அனுமதி கொடுக்க வேண்டியிருக்கும்.
1985 ஆம் ஆண்டில் இதுகுறித்து மத்திய அரசின் சார்பாக
ஒரு முடிவு செய்யப்பட்டது. மாநில அரசோ, மத்திய அரசோ
ஒரு மூடிய மில்லைத் திறக்க வேண்டுமென்று, ஒரு அர~
சாங்கத்தின் சார்பாக முடிவு செய்தால்கூட பி.ஜ.எஃப்.ஆர்.
அவர்களிடமிருந்து கிளீயரன்ஸ் - அது வையபிளா அலல்து
நான் - வையபிளா என்று சர்டிபிகேட் வாங்கித்தான்,
அவர்களுக்கு அனுமதி தர வேண்டுமென்று சட்டம்
தெரிவித்தார்கள். நாங்களும் நமது மாண்புமிகு முதலமைச்~
சரவர்கள் எழுதிய கடிதத்தையும், அதைப்போல மத்திய
அமைச்சரவர்கள் காலையிலே சொன்ன விஷயங்களை எல்லாம்
மத்திய கைத்தறித்துறை அமைச்சரிடம் தெரிவித்தோம். அவர்கள்
மிகவும் உன்னிப்பாகக் கவனித்து மத்திய அரசின் சார்பாக
உங்களுக்கு அனுமதி வழங்கலாமென்று சொன்னார்கள். ஆனால்
அன்றைக்கு வராதிருந்த ஒரு செயலாளர் மறுநாள் வந்து,
தமிழ்நாட்டுக்கு மட்டும் நாம் தர முடியுமானால், நிச்சயமாக
மற்ற மாநிலங்களுக்கும் நாம் பதில் சொல்ல வேண்டிவரும்.
இன்றைக்கு இதுபோல மாண்புமிகு முதலமைச்சரவர்கள்
கடிதம் எழுதி இருக்கிறார்கள். மாண்புமிகு கைத்தறித் துறை
அமைச்சரவர்களும் நானும் வந்து நேரடியாகச் சந்தித்த~
தையும், அவர்கள் என்ன பதில் சொன்னார்கள் என்பதையும்
மாத்திரம் உங்களுக்கு நான் தெரிவிக்கிறேன், ஒரு மினிட்
போல தயாரித்து, தெரிவிப்பதாகத் தெரிவித்தார்கள்.
(மணி அடிக்கப்பெற்றது.)</p>

<p>10-10 மணி</p>

<p>	மாண்புமிகு பேரவைத் தலைவர் : அவர் கேட்ட கேள்விகளுக்கு
மட்டும் பதில் சொல்லி முடிக்கலாம்.</p>

<p>	மாண்புமிகு திரு. கே. என். நேரு : கேட்ட கேள்விக்குத்தான்
பதில் கேட்ட கேள்விக்குப் பதில், மில்கள் திறக்கப்படும்.
உட்கார்ந்து கொள்ளட்டுமா ?</p>

<p>	மாண்புமிகு டாக்டர் க. அன்பழகன் : மாண்புமிகு
பேரவைத் தலைவரவர்களே, இந்தச் சட்டமன்றத்தில் கேள்வி
நேரத்தை அடுத்து எழுப்பப்படுகிற கேள்வி ஏதாவது ஒரு
பிரச்சினையில் அரசாங்கம் ஏதாவது ஒன்று செய்திருந்தால்,
அது பற்றிக் கேட்கலாமே தவிர, நடைமுறையில்
அன்றாடம் பிரச்சினையாக இருந்தால், அதைப் பற்றி எழுப்புவது
அவ்வளவு முறையல்ல. கொள்கைப் பிரச்சினையாக இருந்~
தாலும், அது முடிவுக்கு வருகிற வரையில் எழுப்புவது
-621-
முறையல்ல. இருந்தாலும் ஆர்வத்தால் ஒரு உறுப்பினர்
அவர்கள் எழுப்பினாலும்கூட, இந்த நேரத்தில் அமைச்சர்கள்
நடைபெற்ற நிகழ்ச்சிகளை எல்லாம் சொல்லி, ஒரு அறிக்கை
தர வேண்டிய அவசியமில்லை. எனவே இந்த அவை~
யினுடைய நேரத்தை மாண்புமிகு பேரவைத் தலைவர்
அவர்கள் குறிப்பிட்ட நோக்கத்திற்காக, நியாயமான
நோக்கத்திற்காக பயன்படுத்திக்கொள்஼ள கவனித்துக்கொள்ள
வேண்டுமென்று கேட்டுக்கொள்கிறேன்.</p>

<p>	திரு. கே. ரமணி : தலைவரவர்களே, ஒரு முறையான
அறிக்கையை சமர்ப்பிக்க வேண்டிய பொறுப்பு அமைச்சர்
அவர்களுக்கு இருக்கிறது. ஒரு முக்கியமான நிகழ்ச்சி
நடந்திருக்கிறது. நாடு முழுவதற்கும் தெரிந்த ஒரு முக்கிய~
மான நிகழ்ச்சி. டெல்லியிலே நடந்திருக்கக்கூடிய கான்~
பெரன்ஸ், இந்தியன் லேபர் கான்பரன்ஸ் 5 வருடங்களுக்குப்
பிறகு நடந்திருக்கிறது. அதைவிட பெரிய சமாச்சாரம்
என்ன இந்தச் சபையிலே சொல்வதற்கு இருக்கிறது? ஆகவே
நீங்கள் சொல்கிற கருத்து எங்களுக்கும் புரிகிறது. அதனால்
தான் சொல்கிறேன். நீங்கள் சொல்வதால் அவர் மேற்கொண்டு
சொல்வதை சொல்ல முடியாத நிலைமை வரக் கூடாது. அவர்
சொல்லும்போது, அது நீண்டு போகாமல், அதைச் சுருக்கி
முறையாகத் தயாரித்துக் கொடுக்க வேண்டுமென்று சொல்வதை
ஒத்துக்கொள்ளத் தயார். அதில் பிரச்சினையில்லை. ஆனால்
அவர் கேட்கிற சில குறிப்பான கேள்விகள் அங்கே விவாதிக்~
கப்பட்டன. It is a Public Property அது பிரைவேட் அல்ல.
பத்திரிகையில் வந்திருக்கிறது. அது சம்பந்தமாக சுருக்கமாகச்
சொல்லும்படி நீங்களே டைரக்ஷன் கொடுத்து, செய்ய
வைத்தால் ஂநல்லது. ஆனால் கேட்பதை பறிமுதல் செய்யக்கூடாது
என்று தெரிவித்துக் கொள்கிறேன்.</p>

<p>	மாண்புமிகு டாக்டர் க. அன்பழகன் : அதைக்கூட சட்ட~
மன்றத்தில் முறையாக எழுப்புவதை நான் ஏற்றுக்கொள்~
கிறேன். ஜீரோ ஹவர் அதற்காகப் பயன்படுத்துவதை நாம்
ஏற்றுக்கொள்வது அவ்வளவு நியாயமல்ல. ஏனென்றால்
கொள்கை விளக்கங்கள், விவாதங்கள், மத்திய அமைச்சரோடு
பேசிய பேச்சு வார்த்தைகள் - இந்த விளக்கங்கள் எல்லாம் ஜீரோ
ஹவரில் எழுப்பப்பட்டு பதில் சொல்வதென்றால் அது
அவ்வளவு இயல்வது அல்ல. சென்றேன், வந்தேன், கண்டேன்
என்றுதான் சொல்ல முடியும். எனவே அப்படிப்பட்ட சூழ்~
நிலையில் அறிக்கைகள் தேவையானால், மாண்புமிகு உறுப்~
பினர்கள் அறிக்கைகள் பெறுவதற்கு கவன ஈர்ப்புத்
தீர்மானம் கொண்டு வரலாம் அல்லது அமைச்சர் விரும்~
பினால் ஓர் அறிக்கை வைக்கலாம். அதற்குரிய முறையைப்
பயன்படுத்த வேண்டுமென்று கேட்டுக்கொள்கிறேன்.
-622-
	திரு. கே. ரமணி : தலைவரவர்களே, அரசினுடைய
கொள்கைப் பிரகடனமாக அறிக்கை வேண்டுமென்று நான்
கேட்கவில்லை.</p>

<p>	மாண்புமிகு திரு. கே. என். நேரு : மாண்புமிகு பேரவைத்
தலைவரவர்களே, அங்கே இது குறித்து....</p>

<p>	திரு. கே. ரமணி : அதை நான் கேட்கவில்லை. நடந்~
திருக்கிறது பகிரங்கமாகச் சொல்லக்கூடிய செய்தியாகத்தான்
இருக்கிறது. அந்த செய்தி தருவது, It is harmless and it is useful
அப்படி ஒரு காரியத்தைச் செய்ய வேண்டுமென்று சொல்கிறோம்.
அதை அவர்கள் செய்ய அனுமதித்தால் நல்லது.</p>

<p>
",0
               0 
</p></body></text></cesDoc>