<cesDoc id="tam-w-socsci-journo-tamil2" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-socsci-journo-tamil2.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-09</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>தமிழ் இதழியல் வரலாறு</h.title>
<h.author>மா.சு. சம்பந்தன்</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1987</pubDate>
</imprint>
<idno type="CIIL code">tamil2</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 7.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-09</date></creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>-1-</p>

<p>                      நுழைவாயில்..</p>

<p>     மாந்தன் தன் கருத்தை முதலில் தோற்றங்கள் மூல~
மாகவும், சைகைகள் மூலமாகவும், தீ மூட்டி புகையைக்
கிளப்புவதன் மூலமாகவும், தீ அம்புகளை வானத்தில் விடு~
வதன் மூலமாகவும், சூரிய வெளிச்சத்தைக் கண்ணாடியில்
பிரதிபலிக்கச் செய்வதன் மூலமாகவும் பிறர்க்குப் புலப்படுத்~
தினான். அவன் நாகரிகம் அடைந்தவுடன் புறாக்களின் மூல~
மாகவும், மனிதர் மூலமாகவும் தான் அனுப்பும் செய்தி~
களைத் தன்னைச் சேர்ந்தவர்களுக்கு எட்டச் செய்தான். தற்~
போது சாரணர் கொடிகளை அசைத்தும், கப்பற்படையினர்
விளக்கு வெளிச்சத்தைக் காட்டியும் செய்திகளை அனுப்பு~
வதை நாம் பார்க்கிறோம்.</p>

<p>     மிகப் பழைய காலத்தில் அரண்மனையிலிருந்து அரசாங்~
கத்தின் அறிவிப்புகளும் கட்டளைகளும் பொதுமக்களுக்குப்
பறையறிவிப்பதன் மூலம் தெரிவிக்கப்பட்டு வந்துள்ளன.
தமிழகத்தில் அரசர்கள் முரசு அறைந்து அரண்மனைச்
செய்திகளைக் குடிமக்களுக்கு அறிவிக்கச் செய்துள்ளனர்.
முற்காலத் தமிழகத்தில் அரசச் செய்திகளைத் தெரிவிப்பதற்~
காகவே `வள்ளுவ஼ன்' என்னும் ஓர் அதிகாரியை ஏற்பாடு
செய்திருந்தனர். ஆக உலகெங்கும் அரண்மனையே
செய்தியை வெளிப்படுத்தும் மூலவாயிலாக அமைந்திருந்~
தது!</p>

<p>     சீன நாட்டில் மிக நெடுங்காலமாகச் செய்திகளைக்
கவிதைகளாக்கித் தெருவில் பாடிச் செல்லும் வழக்கம் இருந்~
துள்ளது! பின்னர் அங்கு மர அச்சமைப்பில் அ஛்சடித்து
இ஥ழை வெளியிடும் வழக்கம் சுமார் 2500 ஆண்டுகளாகத்
தொடர்ந்து நடைபெற்று வந்துள்ளது. இவ்வாறு இ஥ழ்கள்
வெளிவந்தவற்றுள் குறிப்பிடத்தக்கவை ""கிங்போ"", ""பீகிங்</p>

<p>-2-</p>

<p>நியூஸ்"", ""பீகிங் கெஜட்""௃என்பனவாம். கி.பி. ஆறாம்
நூற்றாண்டில் சீன அரசாங்கம் ""இத்சிங் பவே""
என்னும் இ஥ழைத் தொடங்கியது. `பீகிங் நியூஸ்' கி.பி.
363இல் மரப்பலகையில் சீன எழுத்துக்களைச் செதுக்கிய
பின்னர் அ஛்சிட்டு இவ்விதழ் வெளியாகியது. இது 1935
வரை இடையறாது நடைபெற்றுள்ளது. இ஥ன் 1500 ஆம்
ஆண்டு விழா கி.பி. 1863 இல் கொண்டாடப்பட்டது. இ஥ு
தான் உலகத்தின் மிகப் பழமையான இ஥ழாகக் கருதப்படு~
கிறது. `கிங்போ' கி.பி. 750இல் துவக்கப்பட்டது.</p>

<p>     மேலை நாட்டிலுள்ள உரோமிலும் 2062 ஆண்டுகளுக்கு
முன்பிருந்தே அரண்மனையிலிருந்து பல திக்குகளிலிருந்த
இராணுவ அ஥ிகாரிகளுக்கு நாள் தோறும் இராணுவ தகவல்~
கள் அனுப்பப்பட்டுவந்துள்ளன. இ஥ற்கு `ஆ஖்டோ டயூர்னா'
என்று பெயர். இத்தாலிய நாட்டில் தாள்~
களில் செய்திகளை எழுதித் தெருவில் முக்கிய இ஠ங்களில்
ஒட்டிவைப்பார்கள். இதை அருகில் சென்று படிப்பதற்கு
`கெஜட்டா' என்னும் சிறு இத்தாலிய நாணயம் தர
வேண்டும்! இதிலிருந்து தான் `கெஜட்' என்ற
பெயர் ஏற்பட்டது. பழங்கால இ஥ழ்கள் எல்லாம் `கெஜட்'
என்ற பின் அடைமொழியைப் பெரும்பாலும் பெற்றிருக்கும்.
நாளாவட்டத்தில் அரசாங்க அலுவலரின் பெயர்களும் அவர்~
களது பதவி உ௟ர்வுகளும், பதவி மாற்றங்களும் அதிகப்படி~
யாக இ஥ழ்களில் இடம் பெறத் தொடங்கின! ஆகவே,
அரசாங்க அலுவலர்களுக்கு `கெஜடட் ஆபீசர்'
அதாவது பதிவுப் பெற்ற அரசாங்க அலுவலர் என்ற
பெயர் பின்னர் ஏற்படலாயிற்று!</p>

<p>-3-</p>

<p>     அ஛்சும் அச்சுப் பொறியும் மேனாட்டில் பெருவழக்கிற்கு
வரத்தொடங்கிய பின்னர்தான் - அதாவது 500 ஆண்டு~
களாகத்தான் செய்தி இ஥ழ்கள் அ஛்சடித்து விரைவாக வெளி~
யிடப்படுகின்றன. இ஥ற்கு முன்பெல்லாம் மரக்கருவிகளைப்
பயன்படுத்தியே இ஥ழ்கள் அச்சடிக்கப்பட்டன.</p>

<p>     இங்கிலாந்து நாட்டில் எலிசபெத் அரசியின் காலத்தில்
இ஥ழியல் தொழில் தொடங்கியது! இங்கு 1588இல்
தோன்றிய முதல் செய்தித்தாள், சாதாரண விளம்பரம்
போல் வெளியாகியது! அது விக்டோரியா பேரரசிக் காலத்~
திற்குள் தக்க முறையில் வளர்ந்து நவீன செய்தி இ஥ழ் வடிவ~
மைப்புடன் வெளியாயிற்று! அங்கு ""வீக்லி - நியூஸ்""
என்னும் பெயரில் முதல் கிழமை இதழ்
1622இல் தோன்றியது. இங்கு திரு இஸ்டேட்
என்பவர் இ஥ழியல் தொழிலின் முன்னோடியாகத் திகழ்ந்~
துள்ளார். இலண்டனின் முதல் நாளிதழ் ""போஸ்ட் பாய்"".
இ஥ற்கு அடுத்தபடியாக வெளியான஥ு
""மார்னிங் போஸ்டு"". அ஠ுத்தது
""இலண்டன் டைம்ஸ்"". இது மிகப்
புகழ் பெற்ற நாளிதழாகத் திகழ்ந்தது; செய்திகளைத் தெளி~
வாக அமைத்து வெளியிடும் முறையில் உலகிற்கு வழிகாட்டி~
யாக விளங்கியது. 1785இல் ஜான் வால்டர் என்பவர்
`தினசரி உலகப் பதிவேடு'
என்ற நாளிதழைத் தொடங்கினார். இதுதான் பின்னர்
""இலண்டன் டைம்ஸ்"" என்று பெயர் மாறிப் புகழ் அடைந்~
தது. இங்கிலாந்தில் முதல் நாளிதழைத் தொடங்கிய
பெருமை எலிசபெத் மாலெத் என்னும்
பெண்மணிக்குரியதாகும்! இவர் `தினசரிச் செய்திகள்'
என்ற நாளிதழை 11-3-1702இல் தொடங்கி
நடத்தினார்.</p>

<p>     ஜெர்மனியில் 1609இல் இஸ்டராஸ்பெர்க் நகரில்
தோன்றிய ""உறவு"" என்ற நாளிதழ்தான்,</p>

<p>-4-</p>

<p>தனிப்பட்ட ஒருவரால் நவீன முறையில் அச்சிட்டு வெளியிட்ட
முதல் செய்தித் தாளாகக் கூறப்படுகிறது.</p>

<p>     அமெரிக்காவில் பாஸ்டன் நகரில் 29-9-1690இல்
பெஞ்சமின் ஆரிஸ் என்பவர் ""பொது
நிகழ்ச்சிகள்"" என்ற பெயரில்
நாளிதழ் ஒன்றை வெளியிட்டார். இவர் அரசாங்கத்தை
மிகக் கடுமையாகத் தாக்கி எழுதியதால், அது வெளியான
முதல் நாளே அச்செய்தித் தாளை அரசாங்கம் தடை செய்து
விட்டது! 1833இல் நியூயார்க் நகரில் ""சன்""
என்ற நாளிதழைப் பெஞ்சமின் எச்டே
என்பவர் ஒரு `பென்னி' (காலணா) விலையில் மலிவாக
வெளியிட்டு வழிகாட்டினார். அ஥ற்கு முன்பெல்லாம் ஒரு
நாளிதழின் விலை ஒரு ரூபாய், இரண்டு ரூபாய் மதிப்புடை~
யதாக இருந்தது. 1619இல் இதே நகரில் ""தினச் செய்தி""
என்னும் நாளிதழ்தான், திடுக்கிடும்
பரபரப்பான செய்திகளைப் படத்துடன் வெளியிட்டு வழி
காட்டிய முதல் இதழாகும்!</p>

<p>     ஐரோப்பாவில் ஏற்பட்ட பிரஞ்சுப் புரட்சிக்குப் பிறகு
தான் உரிமை உணர்ச்சியும் சமுதாய விழிப்பும் பெருக்கெடுக்~
கத் தொடங்கின. இவை செய்தி இ஥ழ்களின் உரிமை வேட்கை~
யைக் கொழுந்துவிடச் செய்தன. ஐரோப்பாவில் கி.பி. 1702
முதல் இ஥ழ்கள் தற்போது போல் தொழில் அமைப்புடன்
நடைபெற்று வருகின்றன.</p>

<p>     பதினெட்டாம் நூற்றாண்டில் ஏற்பட்ட அச்செழுத்து
அடுக்கலும் அமைத்தலும்
அச்சுப் பதிப்புமே மக்கள் கூட்டத்தின் கருத்துக்களைப் பல
திக்குகளில் பரவுவதற்கு அடிப்படையாக இருந்தன. மேலும்,
சுருக்கெழுத்து மூலம் விரைவாகக் குறிப்புகள்
எ஠ுத்துச் செய்திகளைத் திரட்டி அனுப்ப ஆங்காங்கே
நிருபர்கள் ஏற்படுத்தும் முறை ஏற்படலா~
யிற்று. செய்திகளை உ஠னுக்குடன் அனுப்ப தந்தி முறை
மிகுதியும் உதவியாக இருந்தது. புகை</p>

<p>-5-</p>

<p>வண்டி, கப்பல், கம்பியில்லாத் தந்தி வானவூர்தி
போன்ற போக்குவரத்து வசதிகள் சாதனமாக
மேற்கொள்ளப்பட்டன. இ஥ன் காரணமாகச் செய்தி நிறுவன
அமைப்புகள் ஏற்பட்டன. ஒளிப்~
படம் பிடித்து வெளியிடுதல், பேட்டி காணல்,
அன்றாட முக்கிய நிக஼ழ்ச்சிகளைக் கேலிச் சித்திரமாக
வரைதல் போன்ற பல வழிகளால் இ஥ழ்கள் விரைவாகவும்
விளக்கமாகவும் செய்திகளைத் தாங்கி மக்களை மகிழ்வித்த~
தால், இதழியல் தொழில் நாளுக்கு நாள் உலகெங்கும் பல்கிப்
படரத் தொடங்கியது!</p>

<p>     இ஥ழியல் இப்படி வளர்ச்சியுற்றாலும், ஒவ்வொரு
நாட்டினருக்கு என்று இ஥ழியல் துறையில் தனிச் சிறப்பு,
தட்டாமல் இருக்கவே செய்கிறது!</p>

<p>     ""இலண்டன் டைம்ஸ்"" உலகத்திலுள்ள எல்லா
இ஥ழாசிரியர்களாலும் ஒரு முன்மாதிரியாகக் கொள்ளப்~
பட்டு, பின்பற்றத்தக்க பெருமை வாய்ந்ததாக மிளிர்கிறது!</p>

<p>     அமெரிக்கச் செய்தி இ஥ழ்களில் - பல்வேறு ஒளியுருவப்
படங்களும், குற்றச் செய்திகளும், பல சரக்கு விளம்பரங்~
களும், பொறி புலன்களை ஈர்க்கக்கூடிய நிகழ்ச்சிகளும்,
அழகான பெரிய தலைப்புகளும் முக்கிய அமைப்புகளாகக்
கொண்டு விளங்குகின்றன. பிற்காலத்தில் பிரிட்டிஶ்
இ஥ழியல் தொழிலின் வளர்ச்சி பெரும்பாலும் அமெரிக்க
இ஥ழியல் முறையைப் பின்பற்றியதாகவே உள்ளது என்கிறார்
திரு. மாஶிங்காம்.</p>

<p>     சிறுகதை, தொடர்கதை முறையால் மக்களால் விரும்பி
வரவேற்கப்படுவது பிரஞ்சு இ஥ழியலின் இயல்பாகும்.</p>

<p>     பண்டைய ஜெர்மானிய இ஥ழ்களின் முக்கிய அமைப்பு
அ஥ன் உள்நாட்டுத் தன்மையாகும். மற்றொன்று, அ஥ன்
பொதுப்படையான கவர்ச்சியற்ற தோற்றமாகும்! மிக்க
வியப்பும் விருப்பும் உண்டாகுமாறு அமைக்கப் பெறும்</p>

<p>-6-</p>

<p>விளம்பர முறையில் ஜெர்மானிய இ஥ழ்கள் பெயர் பெற்று
விளங்குகின்றன. இங்கு இலக்கிய இ஥ழ்களும் விஞ்ஞான
இதழ்களும் நன்கு தேர்ந்தவர்களால் எழுதப்படுகின்றன.</p>

<p>     சீன நாட்டின் இ஥ழ்கள் பல திறத்தனவாய் அமைந்து
காணப்படுகின்றன. சிறப்புச் செய்திப் பகுதிகளுக்கும்
சாதாரணப் பகுதிகளுக்கும் இடையில் அவற்றை வேறு
பிரித்துக் காட்ட ஒரு கோடு அமைக்கப்பட்டிருக்கும்!</p>

<p>     ஜப்பானிய நாட்டில் இ஥ழ்களின் தொடக்கம் 1845
ஆகும். 1861இல் டச்சு நாட்டு இ஥ழின் செய்திகளைச் சீன
மொழியிலே மொழி பெயர்க்கப்பட்டதை, ஜப்பானிய
இதழ்கள் தம் மொழியில் அப்படியே எ஠ுத்து வெளியிட்~
டுள்ளன. 1878இல் ஜப்பானின் முதல் நாளிதழ், ""காலை~
தோறும் செய்திகள்"" என்ப~
தாகும்; அது யோகோயாமா நகரில் வெளியிடப் பெற்றது.
ஜப்பானிய இ஥ழ்கள் உள்நாட்டு நடவடிக்கைகளிலும், வெளி~
நாட்டு நடவடிக்கைகளிலும் வேற்றுமை உடையனவாக
இருந்தன! ஆனால், தேசியப் பெருமைக் குறைவு அல்லது
அழிவு நேரும் எச்சமயத்திலும் ஜப்பானியர் ஒற்றுமைப்பட்ட
கட்டுப்பாடும் நாட்டுப்பற்றும் பூண்டு, வீறு கொண்டெழு~
கின்றனர். அறிஞர் நோமா என்பவர், தம்
ஒன்பது இ஥ழ்களையும் ஒன்றைவிட மற்றொன்று சிறக்குமாறு
நன்கு நடத்தி வந்துள்ளார்! ""ஸ்யூபென் எலெக்வென்ஸ்""
என்னும் சொற் பெருக்காற்றல் துறைக்கு
என்று தனி இதழையும் நடத்தியுள்ளார்.</p>

<p>     இந்தியாவில் பண்டைக் காலத்தில் இருந்த அரசர்களால்
இ஥ழ்கள் நடத்தி வரப்பட்டதாகத் தெரியவில்லை! இங்கு
முகலாயர் ஆட்சிக் காலத்தில்தான் அரண்மனைச் செய்தித்~
தாள் வெளியிடும் வழக்கம் கொண்டு வரப்பட்டது. இ஥ற்கு
வகியா அக்நாவிஸ் என்று பெயர். இ஥ில்
அரசாங்க நிர்வாகச் செய்திகள் மட்டுமே இடம் பெற்றிருந்~
தது! கையால் எழுதப்பட்டு முகலாய அரண்மனையிலிருந்து
வெளியிடப்பட்டுள்ள இத்தகைய செய்திகளுள், நூற்றுக்</p>

<p>-7-</p>

<p>கணக்கில் கலோனல் ஜேம்ஸ் பிட் என்பவர் `இராயல்
ஏசியாடிக் சொசைட்டி'க்கு அனுப்பியுள்ளார். இவை
ஒவ்வொன்றும் சராசரி 8""X4.5"" அங்குலம் கொண்ட
அளவில் வெவ்வேறு கையெழுத்தால், எழுதப்பட்~
டிருந்தனவாம்!</p>

<p>     இவர்களுக்குப் பின்னர் ஆங்கிலேயர் ஆட்சி செய்த
போது இந்தியத் தலைநகராயிருந்த கல்கத்தாவில் 1750
சனவரி 29ஆம் நாளன்று ஜேம்ஸ் அகஸ்டஸ் இக்கே
எனும் ஆங்கிலேயரால், ""வங்காள
கெஜட்"" என்ற முதல் ஆங்கில வார
இதழ் தொடங்கப்பட்டது. இதுதான் இந்தியாவின் முதல்
இதழாகக் குறிக்கப்படுகிறது. திரு. இக்கே, தமது முதல்
இ஥ழில், ""எனது அறிவின் - ஆன்மாவின் உரிமையைப் பெறுவ~
தற்கு விலையாக என் உ஠லை அடிமைப்படுத்துவதில்
மகிழ்ச்சியுறுவேன்"" என்று எழுத்துரிமை வேட்கையுடனும்
துணிவுடனும் குறிப்பிட்டுள்ளார். இவர் அந்நாளில்,
ஆங்கிலக் கிழக்கிந்திய குழும்பை (கம்பெனி)ச் சேர்ந்த
அதிகாரிகளிடம் காணப்பட்ட ஊழலை வெளியிட்டார்;
அவர்தம் தனிப்பட்ட வாழ்க்கையைப் பற்றியும் வெளிப்~
படுத்தவும் செய்தார். இ஥னால், 1782இல் இவரது
அச்சகத்தை ஆங்கில அரசாங்கம் கைப்பற்றிக் கொண்டது!
இவரையும் கைது செய்து சிறையில் அடைத்தது! சிறையி~
லிருந்து கொண்டே இ஥ழை வெளிவரச் செய்த பெருமை
இவருக்கு உண்டாயிற்று!</p>

<p>     இக்கியின் `பெங்கால் கெஜட்' இரண்டு தாள்கள்
கொண்டது; 12X28 அங்குலம் அளவுடையது. அ஥ிகாரிகளின்
ஊழல்களையும், தனிப்பட்ட வாழ்க்கையில் ஏற்பட்ட
சம்பவங்களையும் குறித்து அவதூறுகள் எழுதியதனால்,
இக்கிக்கு ஓராண்டு சிறைத் தண்டனையும், ரூ 2000
அபராதமும் விதிக்கப்பட்டது. உறுதியுள்ளவராக இருந்தும்
இக்கியின் வாழ்க்கை வறுமையிலே கழிந்தது!
    </p>

<p>               0 
</p></body></text></cesDoc>