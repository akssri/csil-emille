<cesDoc id="tam-w-socsci-myths-lit" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-socsci-myths-lit.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-09</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>வில்லி பாரதம் (நான்காம் பாகம்)</h.title>
<h.author>தி. வெங்கட கிருஶ்ணய்யங்஖ார்</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1986</pubDate>
</imprint>
<idno type="CIIL code">lit</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 22.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-09</date></creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>-1-
		வில்லிபுத்தூராரின்
		மகாபாரதம்</p>

<p>		7. துரோண பருவம்</p>

<p>	 		1
	பதினோராம் போர்ச் சருக்கம்</p>

<p>	பதினோராம் நாட்காலை இரு திறத்துப் படைகளும்
போரிடுவதற்காகப் போர்க்களம் புகுந்தன. துரியோதனனு~
டைய படைகளுக்குத் தலைமை தாங்கி அவற்றைத்
துரோணன் நடத்தினான். ஆயினும் வீடுமன் இல்லாத
சேனை, சந்திரன் இல்லாத வானம் போன்றும், மண௉மில்லாத
மலர் போன்றும், நதியில்லாத நாடு போன்஼றும், நரம்பு~
களின் அமைதியில்லாத யாழ்போன்றும், செல்வமில்லாத
வறியவர்களின் வாழ்க்கை போன்றும், தகுந்த
நல்லெண்ணங்஖ளில்லாத மனம் போன்றும் வேதங்களில்
-2-
கூறியுள்ள முறைப்படி நடக்காத ௟ாகத்தைப் போன்றும்
அழகற்றிருந்தது.</p>

<p>	துரோணன் தன்னுடைய படைகளைச் சகட வியூகமாக
வகுத்தான். பாண்டவர்களுடைய படை஖ளைத் திட்டத்~
துய்ம்நன் கிரௌஞ்ச வியூகமாக (கிரௌஞ்சப் பறவையின்
வடிவில்) வகுத்தான். இருதரப்பு வீரர்களும் ஒருவர் மீது
ஒருவர் அன்பு மாரி பொழிந்தனர். அவ்வாறு சண்டை
செய்தவர்களுள் சகதேவன் சகுனியை எதிர்த்துக் கடுமை~
யாக஼ப் போர் செய்தான். சகுனியும் மிகவும் திறமையுடன்
பொருத்தால், நெடுநேரம் வரை அவர்கள் வெற்றி தோல்வி~
யின்றிப் போர் செய்தனர். இறுதியில் சகதேவன் ஓரம்பினால்
சகுனியின் தேர்ப்பாகனைக் கொன்று, இருகணையால்
அவனுடைய கொடியை வீழ்த்தி, நான்கு அம்புகள் எய்து
அவனுடைய தேர்க் குதிரைகளைக் கொன்று, சகுனியின்
மார்பில் ஆறு அம்புகளைச் செலுத்தி அவனை வென்றான்.
அப்பொழுது சகுனி கையில் கதையை எடுத்துக் கொண்டு
தேரினின்று கீழே குதிக்கச் சகதேவனும் கீழே இறங்கித்
தன்னுடைய கதாயுதத்தால் சகுனியின் தோளில் தாக்க,
அவன் புறங்காட்டியோடித் தப்பினான்.</p>

<p>	ஒரு புறத்தில் துரியோதனன் தனது படைகளுடனும்,
துணை அரசர்களுடனும் வந்து வீமனை எதிர்த்தான். ஆனால்
வலிமை மிக்க வீமன், அவனுடைய கொடியையும் வில்லை~
யும் அறுத்துத் தள்ளி, அவனுடன் வந்த மன்னர்களும்,
அவர்களுடைய குதிரை, யானை முதலியனவும் அழியுமாறு
அம்புகளை எய்தான். அதனால் துரியோதனன், எதிர் நிற்க஼மாட்டாமல்
-3-
முதுகு காட்டி ஓடினான். அதைக் கண்ட
சல்லியன், அவனுக்குத் துணையாக வீமனுடன் போர் செய்ய
வந்தான்.</p>

<p>	அவனை இடைமறித்துத் தன்னுடன் போர் செய்யும்படி
நகுலன் அறை கூவினான். பின்பு நகுலன் பல அம்புகளை
ஏவிச் சல்லியனுடைய கொடியையும், வில்லியும், தேர்ப்~
பாகனையும், குதிரைகளையும் அழித்து, அவனுடைய
உடம்பில் பல விடங்஖ளில் புண்படுத்தினான். இதனால் அவன்
தோற்றோடினான்.</p>

<p>	மற்றொரு பகுதியில் கர்ணனுக்கும் விராடனுக்கும் கடும்
போர் நடந்தது. மலையோடு மலை மோதியது போன்று இரு~
வரும் சண்டையிட்டு எண்ணற்ற அம்பு஖ளை எய்தார்கள்.</p>

<p>	வேறொரு பகுதியில் பாஞ்சால தேசத்து மன்னனான
துருபதனுக்கும், பகத்தனுக்கும் யானைப் போர் நடந்தது.
இருவரும் மிகுந்த ஊக்கமுடன் தங்களுடைய முழுத்திறமை~
யையுங் காட்டிப் பகற்பொழுது முழுவதும் போர் செய்தனர்.</p>

<p>	இன்னுமோரிடத்தில் சிகண்டி சோமதத்தனையும்,
அவனுடன் வந்த படைகளையும் தாக்கிப் புறங்காட்டி~
யோடச் செய்தான்.</p>

<p>	பிறிதோரிடத்தில் துரியோதனனுடைய மகனான
இலக்கண குமாரனுக்கும், அருச்சுனனின் புதல்வனான
அபிமந்யுவிற்கும் பெரும் போர் நிகழ்ந்தது. இலக்கணன்
தனது அம்புகளால் அபிமந்யுவின் வில்லை இரு துண்டு~
களாகத் துண்டித்தான். அதனால் கோபம் கொண்ட
அபிமந்யு முறிந்஼த வில்லால் இலக்கண குமாரனின் தேர்ப்~
பாகனையும், குதிரைகளையும் தாக்கிக் கொன்று, தன்னுடைய
தேரைக் கொண்டு அவனுடைய தேரின் மீது மோதி அதை
அழித்து, இலக்கண குமாரனை நெருங்கி, அவனை உயிருடன்
பிடித்துக் கொண்டான். பின்னர் அவனுடைய தலைமயிரைப்
-4-
பற்றிக் கொண்டு அவனைத் தன்னுடைய தேர்~
மேற் கொண்டு செல்லலுற்றான்.</p>

<p>	அதைக் கண்ட சிந்து தேசத்தரசனும், துரியோதன~
னின் சகோதரியான துச்சனையின் கணவனுமான சயத்திர~
஦஼ன் அவனை எதிர்த்துத் தடுத்துப் போர் செய்யலுற்றான்.
ஆனால் ஆண் சிங்கம் போன்ற அபிமந்யு சயத்திரதனை
எளிதில் வென்று விட்டான். அப்பொழுது கர்ணன் முதலிய
மகாரதர்கள் ஒருங்கே வந்து, தனியனான அபிமந்யுவிடம்
போர் செய்தனர். அபிமந்யு அவர்கள் அனைவரையும்
துன்புறுத்தி ஓடுமாறு செய்தான். இவ்வாறு ஒரு தேரில்
நின்று கொண்டு பல தேர் மன்னர்களையும் வென்஼று,
இலக்கண குமாரனையும் கொண்டு செல்வதைக் கண்ட
சல்லியன் அங்கு வந்து அவனைச் சூழ்ந்து கொண்டான்.</p>

<p>	அபிமந்யு சல்லியனுடைய நெற்றியின் மீது எட்டு அம்பு~
களைச் செலுத்தி அவனை வருத்தி, மேலும் ஂஇரு அம்புகளால்
அவனுடைய தோள்஖ளில் துளை செய்தான். அவனுடைய
தேர்ப்பாகன் மேல் கொடிய வேலாயு஥த்தை வீசி, அவனைக்
கொன்றான். தேரோட்டியற்ற சல்லியன், தன்னுடைய
கதையைக் கையிலெடுத்துக் கொண்டு தேரிலிருந்து கீழே
இறங்கினான்.</p>

<p>	சல்லியன் அபிமந்யுவைத் தாக்கச் செல்வதைக் கண்ட
வீமன் விரைவாக ஓடி வந்து, சத்்ருகாதினி என்னும்
கதாயுதத்தால், அவனுடைய எலும்புகளும் தோளும்
நொறுங்கிய கிரீடம் சரியும்படி அடித்தான். அதனால் அவன்
கீழே விழுந்தான்.</p>

<p>	அப்பொழுது அபிமந்யு வீமனை நோக்கி, "நீங்கள் இவ்~
வாறு இடையில் புகுந்து போர் செய்தால் என்னுடைய
ஆண்மை என்னாகும்?" என்று கூறிச் சிறிது வருந்தித்
தளர்ந்து நின்றான். அச்சமயம் பார்த்து இலக்கணகுமாரன்
-5-
அவன் பிடியிலிருந்து நழுவியோடித் தன் தேரில் ஏறிக்
கொண்டான்.</p>

<p>	அப்பொழுது அங்கு வந்த யாதவ சேனையின் தலைவனான
கிருதவன்மா, கீழே விழுந்த சல்லியனையும், இலக்கண
குமாரனையும் தன்னுடைய தேரில் ஏற்றிக் கொண்டு
சென்று விட்டான்.</p>

<p>	அன்று பல விடங்களிலும் நடந்த போர்களில் தருமனு~
டைய வீரர்களே வென்றதனால், துரியோதனன் மிகவும்
வாட்டமுற்றுத் தன் படைகளுடன் பாசறைக்குத் திரும்பி~
னான். சூரியனும் மேற்கு மலையில் மறைந்தான். பாண்ட~
வர்கள் மகிழ்ச்சியோடு தங்கள் படை வீட்டில் கண் துயின்~
றார்கள்.</p>

<p>	துரியோதனனும் அவனுடைய நண்பர்களும், உற்றார்
உறவினர்களும், தாங்கள் அனைவரும் இருந்த போதே
அன்றையப் போரில், இலக்கண குமாரனை அபிமந்யு கைப்~
பற்றி விட்டானே என்று கூறி அவமானத்தால் வருந்தி
நள்ளிரவு வரை உறங்காதிருந்தனர்.</p>

<p>	அப்பொழுது துரியோதனன் துரோணனிடம் வந்து,
"எங்கள் குருவே, நான் இனி விரும்பிச் சொல்லப் போகும்
வேலையை உம்மையன்றி வேறு யாரே செய்யவல்லவர்?
நாளை நடக்கவிருக்கும் போரில் நீர் தருமனை உயிருடன்
பிடித்துக் கொணர்வீர் என்றால் அது இன்று இலக்கண
குமாரன் பிடிபட்ட இகழ்ச்சியைச் சிறிது குறைக்கும்"
என்றான்.</p>

<p>	அதற்குத் துரோணன்," நீ சொல்வது அவ்வளவு எளி~
தன்று. வாயு குமாரனான வீமன் பின்னால் நிற்கவும்,
முன்னால் வெற்றியையுடைய விசயன் நிற்கவும், இவர்~
களால் நன்கு காக்கப்படும் தருமனை உதவியின்றித் தனி௟ே
நிற்பவனைப் போன்று அகப்படுத்துதல் ஒருவேளை மறுபிறப்஼பில்
-6-
வேண்டுமானால் முடியலாம். இப்பிறப்பில் முடியாது.
ஆயினும், பாஞ்ச சன்னியத்தை ஊதிப் படைகளை நடுங்க
வைக்கும் பார்த்த சாரதியையும், பராக்கிரமம் மிகுந்த
பாத்தனையும் நாளை சிறிது நேரமாவது தருமனை அணுகா~
திருக்கும்படி செய்ய முடியுமானால் நான் தருமனைக் கைப்~
பற்றலாம்" என்றான்</p>

<p>	இதைக் கேட்ட திரிகர்த்த அரசனும், எடுத்த சபதத்~
தைத் தப்பாமல் முடிக்கும் வல்லமையுடைய சில சம்சபதகர்~
களும், "அருச்சுனனைத் தருமனுக்கு உதவ முடியாதபடி மக
நட்சத்திரத்துச் சனி போலச் சூழ்ந்து கொள்வோம்.
நாங்கள் நாளை முழுவதும், அருச்சுனனை, அறத்தின்
மைந்தனை அணுகாமல் தடுக்கா விட்டால், மனுதர்மத்தைப்
புறக்கணித்துச் செல்வதையே விரும்பு஖ிற கொடுங்கோன்
மன்னன் புகும் நரகத்தில் புகுவோமாக. அது போலத்
தருமனுக்குப் பின்னால் நின்று அவனைப் பாதுகாக்கும்
வீமனை, அவனுக்கு உதவி செய்ய முடியாதபடி நாளை
நாங்கள் தடுக்காவிட்டால், தம்முடைய கைப்பிடித்த
மனைவியைக் கதற விட்டுப் பிறர் மனைவியைப் பேணிச்
செல்வோர் மறுமையில் அடையும் பெருநகரத்தை அடை~
வோமாக. இனி, நாளை நாங்கள் எவ்வளவு கொடிய போரிட்~
டாலும் அருச்சுனனையும் வீமனையும் தருமன் அருகில் உதவி
செய்யப் போக முடியாமல் தடுக்கா விட்டால், செய்ந்நன்றி
கொண்றவர்கள் ஆவோம்" என்று கொடிய சபதங்கள் செய்~
தார்஖ள்.</p>

<p>	துரியோதனன், தன் பொருட்டுக் கொடிய வஞ்சினங்~
களைக் கூறிய திரிகர்த்தன் முதலியவர்களை மிகவும் பாராட்டி
அவர்களுக்கு மிகுந்த உபசாரங்கள் செய்து அனுப்பிவிட்டுத்
துரோணனுடைய பாதங்களைப் பணிந்து, அவனைப் போற்றி
அவனிடமும் விடை பெற்றுக் கொண்டு தன்னுடைய இருப்~
பிடத்துக்குச் சென்றான்.
-7-</p>

<p>			2</p>

<p>	பன்னிரண்டாம் போர்ச் சருக்கம்</p>

<p>	பதினோராம் நாளிரவில் துரியோதனன் தன்னுடைய
ஆசானான துரோணனுடன் கலந்து செய்த ஆலோசனைகளை~
யெல்லாம், ஒற்றர் மூலம் உடனே அறிந்த தருமன், மறு~
நாள் கடுமையாகப் போர் புரிவது என்று தீர்மானித்தான்.
அன்று கண்ணன் பாண்டவ சேனையை மண்டல வியூகமாக
வகுக்குமாறு செய்தான். தன்னுடைய பின்புறத்தில் பாது~
காப்பாக வீமனை நிறுத்திப் பக்கங்களில் நகுல சகதேவர்~
களை நிறுத்தி, ஆங்காங்கே பல மன்னர்களை நிறுத்தி
முன்னால் அருச்சுனனையும் அபிமந்யுவையும் நிற்கச் செய்து
தருமன் இவர்களுக்கு நடுவில் பாதுகாப்புடன் நின்றான்.</p>

<p>	அப்பொழுது கௌரவ    சேனை இரவிற் செய்த
ஆலோசனைக் கேற்பத் திட்டத்துய்ம்஼நனும் ஸம்ஸப்தகர்~
களும், துரோணனும் பிற மன்னர்களுமாகச் சேர்ந்து கருட~
வியூகமாக அணிவகுத்து நின்றனர். போர் தொடங்கியதும்
திரிகர்த்த தேசத்து மன்னனான சுசர்மாவும், நாராயண
கோபாலர்கள் என்று கூறப்படும் மன்னர்களும்,
அருச்சுனனை அறை கூவிப் போருக்கு அழைத்தனர்.
அதனால் அருச்சுனன் தருமனின் அருகிலிருந்த மன்னர்~
களிடம் அவனை நன்஖ு பாதுகாத்து நிற்குமாறு கூறிவிட்டுத்
தருமனிடம் விடை பெற்றுக் கொண்டு அறை கூவியவர்~
களுடன் போரிடச் சென்று கடும்போர் புரிந்தான்.
-8-
	துரோணன், சகுனியும், சயித்திரதனும் பிற மன்னர்~
஖ளும் புடைசூழப் போர்க் களத்துக்கு வந்தான். அவன்
தருமனுடன் போர் செய்யச் சென்ற பொழுது, அவனை
அங்குச் செல்ல வொட்டாமல் தடுத்துப் பாண்டவ சேனாபதி~
யான திட்டத்துய்ம்நன், பல்லாயிர வீரர்களுடன் வந்து
எதிர்த்தான். இருவரும் கடும்போர் செய்து பல மன்னர்~
களை மாள்வித்தார்கள். அப்பொழுது திட்டத்துய்ம்நனுக்கு
எதிர் நிற்கமாட்டாமல் தோற்றோடிய துர்முகன், துர்மர்ஶ~
ணன், காந்தார தேசத்து வீரர்கள், கலிங்கநாட்டு வீரர்கள்,
கோசல வீரர்கள், நிடத நாட்டோர் ஆகியோர் முதுகு
காட்டியோடித் துரோணனிடம் வந்து சேர்ந்தனர்.</p>

<p>	அதனால் வெகுண்ட துரோணன், கடுமையாக அம்பு~
களை விட்டுத் திட்டத்துய்ம்நனின் வில்லை அறுத்தான்்.
பின்பு அவன் பாஞ்சால மன்னனான சத்திய கேதுவின்
வில்லையும் சிகண்டியின் வில்லையும் அறுத்து, உத்தமோசா
போன்றவலிய மன்னர்களும் அஞ்சியோடும்படி சண்டை
செய்து பாண்டவ சேனையை மிகவும் பாழ்படுத்தினான். அவ~
னுடைய வில்லாண்மைக்கு எதிர் நிற்க மாட்டாமல் நகுல~
சகதேவர், பாண்டிய மன்னன், ஆகிய அனைவரும் ஓடினார்~
கள். பின்னர் அவன் தருமனை நெருங்கிப் போர் செய்யலுற்~
றான். தருமனும் மிகுந்த கோபங் கொண்டு கணைகளைத்
தொடுத்தான். அப்பொழுது அப்போர்க் களத்தில் பல இ஠ங்~
஖ளில் வீரர்கள் ஒருவரோடொருவர் துவந்தயுத்தம் செய்~
தனர். அவர்கள் சிங்கம் போல் கர்ச்சித்துத் தங்கள்
உயிரைத் துரும்பென மதித்துப் போர் செய்தனர்.</p>

<p>	தருமனும் துரோணனும் தும்பைப் பூமாலைச் சூடிக்~
கொண்டு மிகவும் உக்கிரமாகப் போர் செய்தனர். அப்~
பொழுது தருமன் ஐந்து அம்புகளை வேகமாகச் செலுத்தித்
துரோணனுடைய கொடி, குதிரைகள், தேர் முதலியன
அழியும் படியும், தேர்ப்பாகன் மடியும் படியும் செய்தான்.
பின்பு அவன் துரோணனின் வில்லையும் அறுத்தான்.
-9-
அதனால் துரோணன் செய்வதறியாது திகைத்தான். தருமன்
அவனை நோக்கி, "நீர் பிராம்மண குலத்தவராகலாலும்
என்னுடைய ஆசிரியராதலாலும், இனி உம்மீது அம்புகளைத்
தொடுக்க஼மாட்டேன். பயப்படாதீர். களைப்பு நீங்கச் செல்வீ~
ராக" என்று கூறினான்.</p>

<p>	தருமனைப் பிடித்துத் தருவதாக வீரவாதங் கூறிவந்து,
அவனால் தோற்கடிக்கப்பட்ட துரோணன் மிகவும் நாணி,
வருந்தித் திரும்பிச் சென்று வேறொரு தேரில் ஏறிக் கொண்~
டான்.</p>

<p>	தங்஖ளுடைய மன்னனான தருமன் துவந்தப் போரில்
துரோணனை வென்றதையும், தோற்ற துரோணன் வேறொரு
தேரில் ஏறிக் கொண்டதையும் கண்ட பாண்டவசேனை
மன்னர்஖ளான விராடன், திட்டத்துய்ம்நன், குந்திபோச
மன்னனான புருசித் ஆகியோர் கோபங் கொண்டு, சிங்கங்~
கள் போன்று முழங்கித் துரோணனைச் சூழ்ந்து கொண்டு,
போர் செய்யலுற்றனர். அவர்கள் வீரத்தோடும், ஊக்க~
மோடும் செய்த தாக்குதலினால் துரோணனுடைய சேனைக்கு
மிகுந்த சேதம் ஏற்பட்டது. எண்ணற்ற யானைகளும், தேர்~
களும், குதிரைகளும் அழிந்தன. காலாட்கள் காலன் வாய்ப்~
பட்டனர். தருமனுடைய படையினர் எதிரிகளை நோக்கி,
"உங்கள் சேனை தோற்றது?" என்று இகழ்ந்தனர். எதிரி~
களும் அதை ஏற்றுக் கொண்டு, "உங்கள் அரசனான தரும~
னின் வில்வலிமையால் எங்கள் சேனை தோற்றது" என்று
கூறித் தளர்ச்சியுற்ற துரோணனுக்கு இரங்கினார்கள்.
ஆயினும் வீரர்கள் துவந்தப்போர் செய்வதை நிறுத்தாமல்
பொருது வந்தனர்.</p>

<p>	அந்த நேரத்தில் வீமனுடைய மகனான கடோற்கசன்
பாண்டவர்களுக்கு உதவியாக அங்கு வந்தான். அவன்
தன்னுடைய படைகளுடனும் அபிமந்யுவுடனும், மற்றவர்~
களுடனும் சேர்ந்து துரோணனை எதிர்த்துப் போரிடச் சென்~
றான். அதனால் கௌரவப்படைகள் பகலவன் முன் ஓடும்
-10-
பனியென ஒழிந்தன; அவ்வாறு தப்பியோடிய அரசர்களும்,
கர்ணனும் துரியோதனனை அடைந்தார்கள். தருமன்
வென்றதை அறிந்த துரியோதனன் கலக்க முற்று, மறுபடி~
யும் சேனைகளை அழைத்துக் கொண்டு காற்றெனக஼் கடிதில்
போர் செய்யவந்தார்கள்.</p>

<p>	இவ்வாறு வேகமாக வந்த துரியோதனனையும் அவனு~
டைய வீரர்களையும் அபிமந்யு தாக்கி, அம்பு மழை
பொழிந்து, எண்ணற்ற யானைகளையும் குதிரைகளையும்
அழித்துக் களமெல்லாம் செங்குருதி ஓடச் செய்தான்.</p>

<p>	மறுமுறையும் தருமனை எதிர்க்க வந்த துரோணன்,
வில்லறுப்பட்டுத் தேரொடிந்து போய்ப் பல தேர்
வீரர்கள் பாதுகாக்கக் களத்தை விட்டு விலகினான்.</p>

<p>	அபிமந்யுவிடம் தோற்றுத் துரியோதனன் முதலானோர்
அல்லற்படுவதைப் பார்த்த பகதத்தன், அவர்களுக்குத்
துணையாக வந்தான். அவன் ஐராவதம் போன்று தன்னு~
டைய சுப்ரதீகம் என்னும் யானையிலமர்ந்து, துதிபாடுவோர்~
க஼ள் மெய்க்கீர்த்திபாட, இந்திரன் போன்று பொர வந்தான்.</p>

<p>	அவன் ஊக்கமுடன் வருவதைக் கண்ட துரியோதன~
னும், அவனுடைய சேனை வீரர்களும் புறங்காட்டியோடு~
வதை நிறுத்திவிட்டு அவனுடன் சேர்ந்து கொண்டு மறுபடி~
யும் சண்டை செய்ய வந்தனர். வெற்றி வாகை சூடிய
தருமனும், அபிமந்யுவும் அதைக் கண்டு அஞ்சாமல்
ஆரவாரித்து ஊக்கத்துடன் போர்செய்ய ஆயத்தமானார்கள்.</p>

<p>	பகதத்தன், "வீமன் எங்கே? அவனுடைய தலையைத்
துணித்துத் திரும்புவேன்" என்று வீரவாதங் கூறிக்
கொண்டு வந்தான். அதைக் கேள்வியுற்ற வீமன், அவன்
மீது பாய்ந்து, அவன் கைகளை மடக்கி, அவனுடைய மார்~
பில் குத்தி, அவனுடைய யானையின் முகத்தில் கதை~
யாலடித்து, விரைவில் தன்னுடைய தேரில் ஏறிக் கொண்~
டான்.
-11-
	கோபங் கொண்ட பகதத்தன் வீமன் மீது அம்புமழை
பொழிந்தான். வீமனும் அவனுக்கு நிகராகச் சரமழை
பொழிய இருவரும் வெற்றி தோல்வியின்றிப் பொருதனர்.</p>

<p>	பகதத்தனின் யானையான சுப்ரதீகம், தன்னுடைய
கையால் பகைவர்களுடைய யானையைப் பற்஼றி வீசியது;
அதனுடைய கொம்புகளால் தேர்களைத் தாக்கிச் சேதப்படுத்~
தியது; வலிமையுடைய வாலால் குதிரைகளை அடித்து
வீழ்த்தியது; அதன் கால்களால் காலாள் வீரர்களைத்
துவைத்துக் கொன்றது. இவ்வாறு போர் செய்யும் யானை~
யாலும், பகதத்தனாலும் தன்னுடைய சேனை அழிவதைக்
கண்ட தருமன், ஸம்ஸப்தகர்களுடன் போர் செய்யும்
தம்பிக்குத் துணையாகத் தேரோட்டும் கண்ணபெருமானைத்
தியானம் செய்தான்.</p>

<p>	அதை உடனே உணர்ந்த கண்ணன் அருச்சுனனை
நோக்கி, "அருச்சுனா, போரில் வெற்றிக் கொண்ட தருமனு~
டன் பகைவர்கள் கடும் போர் செய்கிறார்கள். உன்னு~
டைய வில்லின் வலிமையால், இங்கு உன்னை எதிர்த்த
எண்பதினாயிரம் ஸ்ம்ஸப்தகர்களையும் கொன்று உன் பகை~
யைத் தொலைத்து விட்டாய். இனி, உன்னுயிர்க்கினிய
தருமனுக்கு உதவ உடனே சென்று பகதத்தனுடைய
உயிரைக் கவர்வாயாக." என்று கூறித் தேரைப்
பகதத்தனை நோக்கிச் செலுத்தினான்.</p>

<p>	அருச்சுனன் தன்னை நோக்கி வருவதைக் கண்ட
பகதத்தன், வீமனுடன் செய்த போரை நிறுத்து விட்டு,
அருச்சுனன் மீது எண்ணற்ற அம்புகளை எய்தான். அவ்வம்பு~
கள் அருச்சுனனின் அனுமக் கொடியை நடுவிலே அறுத்து,
அவனுடைய குதிரைகளைக் குத்தி, அருச்சுனனுடைய
கவசங்களிலும் நுழைந்து, கண்ணனையும் துன்புறுத்தி
அருச்சுனனைத் தளர்வுறச் செய்தன.
-12-
	அருச்சுனன் பல அம்பு஖ளைப் பொழிந்து, அவனுடைய
அம்புகளைத் தடுத்துப் பல அர்த்த சந்திர பாணங்களால்
அவனுடைய வில்லை அறுத்து, அவனுடைய கவசத்தையும்
துணித்தான். உடனே அவன் கையில் வேலா~
யுதத்தை எடுத்துக் கண்ணன் மீது வீசினான். ஆனால்
அருச்சுனன் அதைப் பல கணைகளால் துண்டித்து, மேலும்
பல அம்புகளை ஏவிப் பகதத்தனுடைய யானைகள் கால் அறு~
பட்டும், காது அறுபட்டும், கையறுபட்டும், குடலறுபட்டும்,
வயிறறுபட்டும் வீழ்ந்தன.</p>

<p>	பகதத்தன், முன்பு திருமாலை வழிபட்டுப் பெற்றிருந்த
ஒரு திவ்யமான வேலாயுதத்தை அருச்சுனன் மீது வீசினான்.
ஆனால் அது அருச்சுனன் மீது படாதபடி, கண்ணன்
எழுந்து நின்று அதைத் தன்னுடைய மார்பில் ஏற்றுக்
கொண்டான். அக்கொடிய வேல் அவன்     எண்ணியபடியே
புதிய மணிகள் நிறைந்த நவரத்தின மாலையாகி அவன்
மார்பில் பொருந்திப் பொலிவுற்றது. இதைக் கண்ட
தேவர்கள் கண்ணனைப் புகழ்ந்து பாடினார்கள்.</p>

<p>	பின்பு கண்ணன் பகதத்தனைக் கொல்ல அதுவே சமயம்
என்று கருதி, அருச்சுனனுக்கு ஒரு அம்஼பை (நாராயணாத்~
திரத்தை) எடுத்துக் கொடுத்தருளினான். அருச்சுனன் அதை
இரு கைகளாலும் மரியாதையுடன் ஏற்றுத் தன்னை நோக்கி
விரைவாக யானையைச் செலுத்தி வந்த பகதத்தன் மேல்
ஏவினான். அது பகதத்தனுடைய உடல் முழுவதையும்
தாக்கி இரத்தம் சொரிய, அவனை கிரீடத்தோடு தலை சிதறிக்
கீழே விழச் செய்தது. அது அவனுடைய வலிமை மிக்க
யானையான சுப்ரதீகத்தையும் கொன்றது. இவ்வாறு
பகதத்தனும் அவனுடைய சுப்஼ரதீகமும், பிற யானைகளும்
இறந்ததைக் கண்டு யாவரும் அருச்சுனனையும் கண்ணனை~
யும் புகழ்ந்தனர்.
-13-
	அப்பொழுது காந்தார நாட்டு மன்னர்கள் பல 
வாத்தியங்களை முழக்கிக் கொண்டும் வீர வாதங்கள் பேசிக்
கொண்டும், "அருச்சுனனைக் கொல்லாமற் போக மாட்~
டோம்" என்று வஞ்சினங் கூறிக் கொண்டும் போர் செய்ய
வந்தனர். அவர்களுள் சகுனியின் மைந்தர்களான
விருஶஜயன், ஜயன் என்ற இரு வீரர்கள் மிகவும் சினந்து
அருச்சுனனுக்கெதிரில் வந்து கடுமையாகப் பொருது, இறுதி
யில் அருச்சுனனுடைய அம்புகளால் உயிர் துறந்தார்கள்.</p>

<p>	இங்கு அருச்சுனன் இவ்வாறு பகைவர்களை அழித்துக்
கொண்டிருந்த போது, சகுனி மற்ற மன்னர்கள் பலருடன்
சேர்்ந்துகொண்டு தருமனைத் தாக்கச் சென்றான். தருமன்
அவனைப் பார்த்து, "நீ வஞ்஛னையோடு விளையாடி வென்ற
சூதுக் காய்கள் போலவல்ல இவை. இவை உடம்பைத்
துளைத்து உருவும் வலிமை மிக்க அம்புகள். உன்னை இவற்~
றால் நான் வெல்வேன்" என்஼று வீதவாதம் கூறிப் பல அம்பு~
களைத் தொடுத்தான். சகுனி அவை தன்மீது படா வண்ணம்
பின்னால் தள்ளித் தள்ளிச் சென்று, பின்னிட்டவர்களுக்கெல்~
லாம் முன்னால் ஓடினான். அவனுடன் வந்த பொருத மன்னர்~
கள் பலர் தருமனின் அம்புகளால் மாண்டனர். எஞ்சியவர்
தப்புவதே நல்லதென எண்ணி முதுகு காட்டி ஓட்டிவிட்டனர்.</p>

<p>	மற்றொரு பக்கத்தில் துரோணன், அசுவத்தாமன்,
கர்ணன், துரியோதனன் அவனுடைய தம்பியர் ஆகியோர்
ஒன்று க஼ூடி வீமனைத் தாக்கி அம்புமாரி பொழிந்தனர்.
ஆனால் அவை வீமன் உடம்பில் பட்டுக் கல்லின் மேல்
விழுமம் மழை போலச் சிதறின. அவன் வில்லை வளைத்து,
ஓரோர் முறையும் பல அம்புகளைத் தொடுத்து, எதிர்த்துச்
சுழலுமாறு செய்து, பல மன்னர்களைக் கொன்று, துரியோ~
தனனுடைய மனம் கொதிக்குமாறு செய்தான். துரியோதன~
னுடைய தம்பிகள் வில் வளைக்கவே, வீமன் அவர்களுடைய
விற்களையும் அவற்றின் நாணையும் அம்புகளால் அறுத்தெறிந்஼தான்.
-14-
அவர்கள், `பிர஼ம்மனே வந்தாலும் வீமனுடன் போர்
செய்ய முடியாது' என எண்ணித் திகைத்து நின்றனர். அப்~
பொழுது வீமன் தன்னுடைய கொடிய அம்பு஖ளால்,
துரோணன், அசுவத்தாமன், கர்ணன்், துரியோதனன்,
அவனுடைய தம்பிகள் ஆகிய அனைவரையும் மிகவும் துன்~
புறுத்திக் கொல்லாமற் கொன்றான்.</p>

<p>	இறந்தவரொழிந்த, எஞ்஼சிய மன்னர்கள், "நம் சேனை
களைத்துவிட்டது. இனி இங்கு வீமனும், அங்குக்் கண்ண~
னுடன் அருச்சுனனும், துரோணனைத் தோற்கச் செய்த
தருமனும் இருக்கும் போது, நம்மால் ஒன்஼றும் செய்ய முடி~
யாது" என்஼று எண்ணிப் பாசறையை நோக்க஼ிச் சென்றனர்.</p>

<p>	அப்பொழுது சூரியனும் மேற்கில் மறைந்தான்.
துரியோதனனின் பாசறையில் அன்று தருமனுக்கும், தனஞ்~
சயனுக்கும், அபிமந்யுவிற்கும், வீமனுக்கு தோற்றுத் தப்பி
வந்த வீரர்கள் துரியோதனனின் புறத்தே வந்து, இறந்த
வீரர்களுக்கு இரங்கி, அழுது புலம்பி மாவீரனான பகதத்த~
னின் போர்த்திறமையையும் வலிமையையும் பற்றிப்
புகழ்ந்து பேசினார்கள்.</p>

<p>	அப்பொழுது துரியோதனனுக்கு எதிராக வீற்றிருந்த
துரோணனைப் பார்த்துக் கர்ணன்,"நேற்று நீர் ஆலோசனை
செய்யாமல் தர்மனைப் பிடித்துக் கொடுப்பதாக உறுதி கூறி~
னீரே, அதை நடத்திவிட்டீரே! வேதம் ஓதும் பிராமண~
ருக்கு, உறுதியான வீரம் உண்டோ?" என்஼று எள்ளி நகை~
யாடினான்.</p>

<p>	அதனாற் சினங் கொண்ட துரோணன், "இங்குத் தரும~
னுக்கு எதிராகப் போரிட யாருளர்? துரியோதனா, இன்~
றென்ன? நாளையும் அவனைப் பிடிக்க முடியாது. நாளை
அவனைப் பிடித்துத் தருவதாகக் கூறி அவனுடன் போர்
செய்து, அவனை வெல்லாமல் திரும்பி வர மாட்டேன் என்று
கூறும் வீரருக்கு நீ அவ்வாறு செய்ய விடை கொடுப்பாயாக.
-15-
வீரத்தில் ஒருதலைப் பக்கமாக இகழ்ந்து கூறுவது சரி~
யில்லை இனிச் சபதம் கூறாவிடினும் நாளை முதல் தரும~
னுடன் போரிட்டு யாராவது அவனைப் பிடித்து வருவா~
ராகில், அவர் இவ்வுலகில் மட்டுமல்லாமல் எவ்வுலகிலும்
சிறந்த வீரராவார். `என்னைப் போன்று விற்றிறமை பெற்ற~
வர் வேறு யாருளர்?' என்று செருக்குறும் வீரனும், தருமன்
முன்னால் கணப் பொழுது கூட நிற்கமாட்டாமல் அழிவான்.
அவனுடைய அம்புகள் தசரத ராமனுடைய அம்புகள்
போன்று கூர்மையும் வலிமையும் உடையவனவாக உள்ளன.
எனவே வீண் சொற்கள் பேசி என்ன ப௟ன்? அவன்
என்னுடன் பொருத பொழுது, நான் விட்ட வாளி஖ளுக்கு
ஏற்றதான பல அம்புகளை எய்து, தருமன் என்னுடைய
அம்புகளைப் பயனற்றனவாகச் செய்தான். உலகில்
வலிமைக்கு வீமனும், வில்லுக்கு விசயனும் அன்றி வேறு
எவருமிலர் என்று கூறுவர். இவ்விரண்டிலும் தனித்தனியே
இவர்களையொத்த மன்னர்கள் பலரைக் காணலாம். ஆனால்
வில் வன்மை, வலிமை ஆகிய இரண்டிலும் உறுதி பெற்~
றுள்ள தருமனுக்கு நிகராக எவருமிலர். வீண் ஆரவாரஞ்
செய்பவர்கள் அவ்வாறு செய்யட்டும். தருமனை எதிர்க்க
வல்லவர் உண்டாகில், நாளை முதல் அவர்களைக் காணலாம்.
வீணாக அதிகம் பேசிப் பயனென்ன?" என்று கூறிவிட்~
டெழுந்து துரோணன் தன்னிடத்துக்குச் சென்று விட்டான்.
துரியோதனனும் கர்ணன் முதலான மன்னர்களுக்கு விடை
கொடுத்து அனுப்பி விட்டுத் தூங்கப் போனான்.</p>

<p>	அன்று துரோணனையும் பகதத்தனையும், காந்தார
மன்னனையும் வென்ற பாண்டவர்கள் வெற்றிப் பெருமித~
முடன் கண் துயின்றார்கள்.</p>

<p>	அன்று இர஼வு பாண்டவர்களின் வெற்றி போன்று
சந்திரன் பாலொளி வீசிக் கொண்டிருந்தது.</p>

<p>	மறுநாட் காலை சூரியன் உதித்தான்.</p>

<p>-16-
		பதின்மூன்றாம் போர்ச் சருக்கம்</p>

<p>	இரு திறத்துப் படைகளும் போருக்கு ஆயத்தமாயின.
கௌரவப் படையில் பல மன்னர்கள் துரியோதனனின்
ஏவற்படி அருச்சுனனுடன் போர் செய்யச் சென்றனர்.
துரோணன் முந்திய நாளினும் மிகுந்த வேகத்துடனும்
ஊக்கத்துடனும் தருமனுடன் போர் செய்௟ச் சென்றான்.
இலக்கண குமாரன், துரியோதனனின் தம்பிகள், கலிங்க
மன்னர்கள் சிந்து நாட்டுச் சயத்திரதன் ஆகிய அனைவரும்
படை வகுப்பில் கலந்து நின்றனர். துரோணன் தன்~
னுடைய படைகளைச் சக்கர வியூகமாக அமைத்தான்.</p>

<p>	தருமன் ஒற்றர்க஼ள் மூவர் துரியோதனாதியர்கள்
எண்ணியிருந்த எண்ணத்தை அறிந்து, அதைத் தன்~
னுடைய தம்பி அருச்சுனனிடங் கூறி, வலிமை மிகுந்த
சேனைகள் தன்னை புடைசூழப் போர்க் களத்திற்கு வந்தான்.
அவனுடைய சேனை அளவிறந்ததாக இருந்தது. அச்சேனை~
யைத் திட்டத்்துய்ம்நன் மகர வியூகமாக வகுத்தான்.</p>

<p>	இவ்வாறு இரு படைகளும் அணிவகுத்துச் சண்டை
செய்யத் தயாராக இருந்த பொழுது, துரியோதனனுடைய
-17-
ஆணையின் படி முந்திய நாளைப் போன்று அன்றும்
ஸம்ஸப்தகர்கள் தங்கள் நால்வகைச் சேனைகளுடன் வ஼ந்து
அருச்சுனனைப் போருக்கு அறைகூவி அழைக்க, அவன்
அவர்களுடன் போர் செய்யலுற்றான். காற்றினும் கடிதாக
அவன் எய்த அம்புகளால், எதிரிகளின் தேர், யானை,
குதிரை யாவும் அழிபட்டு, வீரர்கள் பெருங் காற்றினுக்கு
எதிர்ப்பட்ட காய்ந்த சருகுகள் போன்று கதிகலங்கி ஓட~
லானார்கள். எதிர்த்து நின்ற எண்ணற்ற வீரர்கள் அவ஼~
னுடைய அம்புகளால் துளைக்கப்பட்டு இரத்தம் சொரிய
இறந்தனர். இதனால் போர்க்களம் இரத்த வெள்ளத்தில்
நிரம்பியது.</p>

<p>	போர்க் களத்தின் மற்றொரு பகுதியில் பாண்டவர்~
களின் படைத் தலைவனான திட்டத்துய்ம்நன், துரோணனை
எதிர்த்துப் போர் செய்தான். துரோணன், திட்டத்துய்ம்நன்
எய்த அம்பு஖ளைத் தன்னுடைய அம்புகளால் துண்டித்து,
அவனுடைய கொடியையும் வில்லின் நாணையும் அறுத்து,
அவனுடைய கவசத்தையும் அழித்தான். துரோணனுடைய
எதிரில் நின்று பொர முடியாமல் திட்டத்துய்ம்நன் தன்~
னுடைய தேரைத் திருப்பிக் கொண்டு முதுகு காட்டியோடி~
னான். இதைக் கண்ட அனைவரும் துரோணனுடைய
ஆண்மையையும், வீரத்தையும், வில்லின் திறத்தையும்
புகழ்ந்தார்கள்.</p>

<p>	புறங்காட்டி வந்த திட்டத்துய்ம்நனைத் தருமன்
மார்புறத் தழுவி முகமன் கூறி, "மன்னர்களுக்கெல்லாம்
முடிமணி போன்றவனே, நீயே, துரோணனை எதிர்க்க முடி~
யாமல் திரும்பினாயென்றால், வேறு யார்தான் அவனுடன்
போர் செய்ய முடியும்? நீ போரிட்டு மிகவும் களைத்திருக்~
கிறாய். எனவே ஓய்வெடுத்துக் கொள்வாயாக" என்று
அன்பு மொழிகளால் அவனுக்கு ஆறுதலளித்தான்.
-18-
	பின்னர் அவன் அபிமந்யுவை அழைத்து, "உனக்கு
எதிராகப் போர் செய்ய வல்லவர் எவருமில்லை. நீ சண்டை
செய்தால் தான் பகைவர்களுடைய இந்தச் சக்கர வியூக~
மும் சிதைவுறும். நேற்று என்னைப் போருக்கு அழைத்து
என்னுடன் போரிட்டுத் தோற்றுப் போன ஆசிரியர்
துரோணர் அந்தக் கோபத்தில் இன்று படையை
வெல்வதற்கரிய சக்கர வியூகமாக அமைத்து எதிரே நிற்~
கிறார். உன்னையும் உன் தந்தையாகிய அருச்சுனனையும்
தவிர இந்த வியூகத்தை பிளக்க வல்லார் வேறு யாருளர்?"
எனவே விரைவிற் சென்று, வெற்றி வாகை சூடிப் பகை~
வரை அழிப்பாயாக?" என்று கூறி அவனை மார்புறத்தழுவி,
ஆசி கூறி அனுப்பினார்.</p>

<p>	அபிமன்யு தருமனின் பாதங்களைப் பணிந்து திட்டத்~
துய்ம்நனும் பிற பலவரசர்களும் சூழ்ந்து வரப் பல்லியங்஖ள்
முழங்கப் பகைவர் படையுடன் பொரச் சென்றான். சக்கர
வியூகத்தில் அணிவகுத்து நின்ற பகைவர்களின் மார்பு~
களில் பல அம்புகளைச் செலுத்தி, இராமபிரானையும்
அருச்சுனனையும் நினைத்து வணங்கினான். தன்னுடைய
மாமாவான கண்ணபிரானிடமிருந்து உபதேசம் பெற்ற
வைஶ்ணவாஸ்திரத்தைப் பக்தியுடன் உச்சரித்துவிட்டான்
அபிமந்யு. இதனால் இந்திரனிடமிருந்து உபதேச பரம்பரை~
யாக வந்து இயற்றப்பட்ட, சக்கரவியூகம் சிதைந்தது.
அங்கிருந்த வீரர்கள் அனைவருடைய தலைகளும், கைகளும்
துண்டிக்கப்பட்டன.</p>

<p>	இதனால் சினங்கொண்ட துரோணன் அபிமந்யு விடுத்த
அம்புகளைத் தன்னுடைய அம்புகளால் தடுத்து அவனுடன்
-19-
போர் புரிய வந்தான். ஆனால் அபிமந்யு தொடுத்த அம்பு~
களால் அவனுடைய தேரும், வில்லும் அழிந்து புகழும்
ஒடுங்கிற்று. துரோணன் இவ்வாறு தாழ்வுற்றதை அறிந்து
அவன் புறங்காட்டு முன் அவனுடைய மகனான அசுவத்தா~
மன் அங்கு வந்து அபிமந்யுவை எதிர்த்தான். அபிமந்யு
எண்ணற்ற அம்புகளைத் தொடுத்து அவனையும் அம்பு மழை~
யில் மூழ்கடித்தான்.</p>

<p>	அப்பொழுது கர்ணனும் அங்கு வந்து பல கணைகளை
அபிமந்யு மீது தொடுத்தான். அபிமந்யு அவற்றையெல்லாம்
தவிர்த்து, எண்ணற்ற அம்புகளை கர்ணன் மீது ஏவி
அவனைத் திணறடித்தான். கர்ணன், "இவனுடன் நம்மால்
பொர முடியாது?" என அறிந்து தேரோடு திரும்பிச்
சென்று விட்டான்.</p>

<p>	கிருபாசாரியனும் யாத மன்னனான கிருதவர்மனும்
அபிமந்யுவை எதிர்த்துப் பல அம்புகளை எய்தனர். ஆனால்
ஒப்பற்ற வீரனான அபிமந்யு, தன் ஒரு வில்லிலிருந்து
தொடுத்த பலகணைகளால் அவ்விருவருடைய விற்களையும்
அறுத்து அவர்களை அஞ்சியோடுமாறு செய்தான்.</p>

<p>	சகுனியும், அவனுடைய மகனும் பிற மன்னர்களும்,
ஒன்று சேர்ந்து அபிமந்யுவை வெல்஼லலாம் என்று எண்ணி~
வந்த பொருதனர். ஆனால் அபிமந்யுவின் வாளியால் சகுனி~
யின் மகன் இறந்தான். வேறுபல மன்னர்களும்
மாண்டனர். எஞ்சியோர் இடருற்று ஏகினார். துரியோதனு~
டைய தம்பிகளான விகர்ணன், துன்முகன் முதலியோர்
அபிமந்யுவை எதிர்க்க வந்தனர். அபிமந்யு அவர்களை
இகழ்ந்து, "நீங்கள் என்னுடன் போர் புரியத் தகுதியற்ற~
வர்கள். பின்னிட்டுப் பிழைத்துப் போங்கள்" என்று கூறி
அவர்களைத் தாக்காமல் விட்டான். இவ்வாறே அவனை
-20-
எதிர்க்க வந்தவர்கள் அனைவரும் சிதைவுற்று நலிவுற்றனர்.
நால்வகைப்பட்ட படைகளும் சிதைவுண்டு சிதறின.</p>

<p>	அபிமந்யு தன்னந்தனியாக இவ்வளவு வீரமாகவும்,
விரைவாகவும் போர் செய்து பகைவர் பலரையும் அழித்துக்
கொண்டிருந்ததைக் கண்டு, அவன் களைத்து விடக்கூடாதே
என்ற கவலையால் வீமன் தருமனிடம் வந்து அவன் கழல்~
களை வணங்கி, "எண்ணற்ற யானைப் படைகளும் தேர்ப்~
படைகளும் குதிரைப் படைகளும், காலாள் வீரர்களும்
உள்ள பகைவர் சேனையில் சிறுவனாகிய அபிமந்யுவைப்
போர் செய்ய அனுப்புவது கொடிய பாவமாகும். ஆகை~
யால் நான் அவனுக்கு உதவியாகச் செல்கிறேன்" என்று
கூறி அவனிடம் விடை பெற்றுக் கொண்டு பல அரசர்களும்
புடைசூழ அபிமந்யுவிற்கு உதவியாகப் புறப்பட்டான்.
அவனும் அவனுடன் வந்த அரசர்களும் எதிரிகளின் சக்கர
வியூகத்தில் இருந்த அரசர்களும் பலரையும் அமரர் உலகிற்கு
அனுப்பினார்கள்.</p>

<p>	வீமன் தன்னுடைய கதாயுதத்தைக் கொண்டு சக்கர
வியூகத்தைச் சிதைத்ததைக் கண்ட துரியோதனன், முன்பு
இராமபிரானுடன் சேர்ந்து கொண்டு மலைகளினால் அரக்கர்~
களை அழித்த அனுமான் தான் வீமன் உருவில் வந்து
விட்டானோ என எண்ணினான். "அபிமந்யு ஒருவனுடைய
அம்புகளைக் கண்டே நமது படைகள் யாவும் இடியுண்ட
நாகம் போலவும், சிங்கத்தைக் கண்ட யானைக் கூட்டங்கள்
போலவும் நடுங்கி ஒடுங்குகின்றனவே, அவனுடன் வீமனும்
சேர்ந்து விட்டால் நெருப்பும், காற்றும் சேர்ந்து விட்டது~
போல் ஆகும்?" என்று எண்ணித் தன்னுடைய சேனை வீரர்~
களை நோக்கி, "விந்துபூரி முதலான மன்னர்கள் சகுனி~
யுடன் சென்று வீமனை, அபிமந்யுவிடன் சேர்ந்து விடாதபடி
வளைத்துத் தடுத்துப் போர் செய்வார்களாக. அதே நேரத்~
தில் விகர்ணனும், பிற இளைஞரான மன்னர்களும், தம்பி~
களும், அசுவத்தாமனும் அபிமந்யுவுடன் போர் செய்வார்~
களாக. இப்படிச் செய்தால் விரைவிலேயே வீமனையும்,
-21-
அபிமந்யுவையும் வெல்வதும், கொல்வதும் முடியும்"
என்றான். அதன் படியே அவர்கள் அபிமந்யுவுடனும்
வீமனுடன் போர் செய்ய வந்தனர்.</p>

<p>	வீமன் மிகுந்த கோபங் கொண்டு பல அர்த்த சந்திர
அம்புகளை ஏவி எதிரிகளின் நால்வகைச் சேனைகளையும்
நாசஞ் செய்தான். அபிமந்யுவும் தன்னை எதிர்த்த வீரர்~
களின் எலும்பு, தசை, மூளை ஆகியவை சிதறும்படி அம்பு~
களை எய்து அவர்களை எமனுக்கு விருந்தாக்கினான். தப்பிய~
வர்கள் இரவியைக் கண்ட இருளென மறைந்தார்கள்.</p>

<p>	தன்னுடைய வீரர்கள் அபிமந்யுவிற்கும் வீமனுக்கும்
ஂ஁எதிரில் நிற்க மாட்டாமல் தோற்றுப் பின்னிடுவதைக் கண்ட
துரியோதனன், மிகவும் கோபமுற்று, அவர்களைப் பலவாறு
இகழ்ந்து, தன்னருகிலிருந்த சிந்து நாட்டரசனான சயத்திரத~
னிடம், "வலிமையும் அழகும் நற்குணமும் உடையவனே,
வலிமை, மிக்க வீமனும் அபிமந்யுவும் ஒன்று சேர்ந்து
விட்டால் அவர்களுடன் போர் செய்வது மிகவும் அரிது.
அவர்களை அவ்வாறு ஒன்று சேர முடியாமற் பிரித்து
விட்டால், நாம், அவர்களிருவரையும் நமது சேனா
சமுத்திரத்தால் தாக்கி வென்று விடலாம். அதற்கு ஒரு வழி~
யுள்ளது. நீ சிவபெருமானிடத்திலிருந்து பெற்றுள்஼ள
கொன்றை மாலையை அவர்களுக்கிடையில் போட்டு
-22-
விட்டால், அவர்களிருவரும், அது சிவபெருமானுடைய
மாலை என்பதனால் அதைத் தாண்டமாட்டார்கள். அப்~
பொழுது நீ, சிவபெருமான் உனக்களித்துள்ள கதாயுதத்~
தால் அபிமந்யுவைத் தாக்கினால் அவனைக் கொன்று
விடலாம்" என்று கூறினான்.</p>

<p>	அதைக் கேட்ட சயத்திரதன் மகிழ்ச்சியுற்றுத்
துரியோதனின் பாதங்களைப் பணிந்து, "நன்றே
சொன்னீர்! வேறென்ன வழி இருக்கிறது? இதுவே
சிறந்தது. நான் என்னுடைய கதையால் அபிமந்யுவைக்
கொல்வேன்" என்று கூறிச் சிவபெருமான் அளித்த
கொன்றை மாலையை எடுத்து, மன஼த்தால் சிவபெருமானை
வழிபட்டு உரிய மந்திரத்தை ஓதி, அது அபிமந்யுவின்
தேரைச் சூழ்ந்து விழுமாறு எண்ணி வீச, அதுவும் அவ்~
வாறே போய் விழுந்தது.</p>

<p>               0 
</p></body></text></cesDoc>