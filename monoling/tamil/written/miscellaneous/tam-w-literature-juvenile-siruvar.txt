<cesDoc id="tam-w-literature-juvenile-siruvar" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-literature-juvenile-siruvar.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-09</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>சிறுவர்களுக்கான வேடிக்கைக் கதைகள்</h.title>
<h.author>முல்லை முத்தையா</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1986</pubDate>
</imprint>
<idno type="CIIL code">siruvar</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 40.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-09</date></creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>-5-</p>

<p>           வேடிக்கைக் கதைகள்</p>

<p>            பாசமுள்ள குடும்பம்</p>

<p>     சந்தைக்குப் போன தாய் திராட்சைக்குலை
வாங்கி வந்தாள். அதை தன் மகளிடம் கொடுத்~
தாள். அவ஼ள் ஒரு பழத்தை எ஠ுத்து வாயில்
போட்டதும் ஥ன் தம்பியின் நினைவு வந்தது.
உ஠னே அதைத் தம்பியிடம் கொடுத்தாள்.</p>

<p>     அவன் ஒரு பழத்தைச் சாப்பிட்டதும்,
தோட்டத்தில் வேலை செய்து கொண்டிருந்த
தந்தைக்குக் கொண்டு போய்க் கொடுத்தான்.</p>

<p>     அவர் ஒரு பழத்தை எ஠ுத்துச் சாப்பிட்ட~
தும், சமையல் அறையில் வேலை செய்து
கொண்டிருந்த மனைவியின் நினைவு வந்தது,
அவளிடம் கொண்டு போய்க் கொடுத்தார்.</p>

<p>     அதை வாங்கிக் கொண்ட அவர் மனைவி,
எங்கிருந்து இந்தத் திராட்சைக் கொத்துப்
புறப்பட்டதோ, அந்த இ஠த்துக்கே திரும்பி
விட்டது!" என்றாள்.</p>

<p>-6-</p>

<p>          மறுப்புக் கூறும் மனைவி</p>

<p>     ஒரு ஊரில் கணவனும் மனைவியும்
வாழ்ந்து வந்தனர்.</p>

<p>     மனைவி மூர்க்கமான குணம் உள்ளவள்.
கணவன் எதைச் சொன்னாலும், அ஥ற்கு
மறுப்புக் கூறி, மாறாகவே நடந்து கொள்வாள்.
கணவன் பொறுமையோடு இருந்தான்.</p>

<p>     அவனுடைய தாய் இறந்து போய் ஒரு
வருடம் ஆகப்போகிறது. அந்த நினைவு நாளில்
நெருங்கிய உறவினர்களை அழைத்து! தன்
வீட்டில் உணவு அளிக்க வேண்டும். அது சமூக
வழக்கம்.</p>

<p>     ஆனால், மனைவியிடம் சொன்னால்,
அவள் எ஥ிர்ப்புக் கூறி, அதைச் செய்ய மாட்~
டாளே. உறவினர் ஏளனமாகப் பேசுவார்~
களே என்று வருத்தத்தோடு தோட்டத்தில்
உட்கார்ந்து கொண்டிருந்தான்.</p>

<p>     அவனைக் கண்ட ஫ழைய வாத்தியார்,
"ஏன் வருத்தத்தோடு இருக்கிறாய்?" என்று
கேட்டார்.</p>

<p>     அவன் விவரத்தைக் கூறினான்.</p>

<p>-7-</p>

<p>     "இ஥ற்காக ஏன் வருந்துகிறாய்? நான்
சொல்கிறபடி செய்!" என்று ஒரு யோசனை
சொல்லிக் கொடுத்தார் வாத்தியார்.</p>

<p>     ஆறுதலோடு வீட்டுக்குப் போனான்.
மனைவியைக் கூப்பிட்டு, "நாளை, என் தாய்
இறந்து ஒரு வருடம் ஆகிறது. சமூக வழக்க~
படி உறவினர்களை அழைத்து, சாப்பாடு
போட வேண்டாம். நாம் இருவரும் சாப்பிட்டு
உற்சாகமாக இருப்போம்" என்றான்.</p>

<p>     அ஥ற்கு அவள், "உங்களுக்கு ஏன் புத்தி
இப்படி போகிறது? உறவினர்களை அழைத்து,
நன்றாக விருந்து வைத்து விடுவோம், அதுவே
நமக்குக் கௌரவம்" என்று கூறி, உறவினர்~
களை அழைத்து வரச் சொல்லி, தடபுடலாக
விருந்து வைத்து விட்டாள்.</p>

<p>-8-</p>

<p>           எவருக்கு உரியது?</p>

<p>     ஒரு கிராமத்தில் இருந்த விவசாயி,
தன்னுடைய வீட்டை, மற்றொரு விவசாயிக்கு
விற்றான்.</p>

<p>     வீட்டை வாங்கிய விவசாயி, வீட்டிற்குள்
போய் சுற்றிப் பார்த்தான். அங்கே ஒரு அறை~
யில் மூன்று செப்புத் தவலைகள் இருந்தன.</p>

<p>     அவற்றை எ஠ுத்துக் கொள்ளுமாறு
வீட்டை விற்ற விவசாயிடம் சொன்னான்
வீட்டை வாங்கியவன்.</p>

<p>     "நான் வீட்டை விற்றுவிட்டதனால், அ஥ில்
உள்ள பொருள் உனக்குத்தான் உரியது. ஆகை~
யால், அதை நான் எடுத்துக்கொள்வது நியாயம்
இல்லை" என்றான் வீட்டை விற்றவன்.</p>

<p>     "நான் வீட்டைத்தான் விலைக்கு வாங்கி~
னேன். அதில் உள்ள பொருள்களை நான்
எடுத்துக் கொள்வது நியாயம் ஆகாது" என்~
றான் வீட்டை வாங்கியவன்.</p>

<p>     இப்படியாக, மூன்று மாதங்கள் கடந்தன.</p>

<p>     ஒருநாள், வீட்டில் இருந்த தவலைகளை
எடுத்துக் கொள்வதற்காக வந்தான், வீட்டை</p>

<p>-9-</p>

<p>விற்றவன் "நீ வீட்டை விற்றபின், அதில் உள்ள
பொருள் எனக்கே உரியது. அதை நீ எடுத்துக்
கொள்வது நியாயம் ஆகாது" என்றான்
வாங்கியவன்.</p>

<p>     "நான் வீட்டைத்தான் உனக்கு விற்றேன்.
அ஥ில் உள்ள பொருள் எனக்கே உரியது. அதை
நான் எடுத்துக் கொள்வதே நியாயம்" என்றான்
வீட்டை விற்றவன்.</p>

<p>     தகராறு முற்றியது. இருவரும் பஞ்சாயத்~
தாரிடம் சென்றனர். "வீட்டிற்குள் இருக்கும்
பொருள், வீட்டை வாங்கியவனுக்கே உரியது"
என்று பஞ்சாயத்தார் கூறினர்.</p>

<p>-10-</p>

<p>      அண்ணனுக்கு உண்டான கர்வம்</p>

<p>     ஒரு ஊரில் அண்ணனும் தம்பியும் அடுத்த
அ஠ுத்த வீட்டில் வசித்து வந்தனர்.</p>

<p>     அண்ணன்் பணக்காரன்; தம்பியோ ஏழை.</p>

<p>     தான் பெரிய பணக்காரன் என்ற மமதை
அண்ணனுக்கு எப்பொழுதும் உண்டு.</p>

<p>     அண்ணனுடைய மமதையைக் கவனித்து
விட்டு தம்பி, ஒரு நாள் அண்ணனைப் பார்த்து,
"அகம்பாவத்தோடு இருக்க வேண்டுமானால்,
நான்தான் அவ்வாறு இருக்க வேண்டும்.
ஏனெனில், என் பக்கத்து வீட்டுக்காரர் என்
அண்ணன்; அவர் பெரிய பணக்காரர் என்று
நான் பெருமையாகக் கூறிக் கொள்ள முடியும்.
நான் அவ்வாறு பெருமை பேச முடியாதபோது,
நீ ஏன் அகம்பாவத்தோடு விளங்க வேண்டும்.
ஏனெனில், உன் பக்கத்து வீட்டுக்காரனாகிய
உன் தம்பி பரம ஏழையாகிய நான்தானே!"
என்றான்.</p>

<p>-11-</p>

<p>           உணவு தயாராகிறதா?</p>

<p>     ஒரு கோயிலைச் சேர்ந்த ஆயிரம் கால்
மண்டபத்தின் வாயிலில் அரசு அ஥ிகாரி ஒருவன்
நின்று கொண்டு, உரத்த குரலில், பலருக்கு
ஏதேதோ கூறி, உத்தரவிட்டுக் கொண்டிருந்~
தான்.</p>

<p>     அவனுடைய பேச்சைப் பார்க்கும் போது,
மண்டபத்தின் உள்ளே, ஊர் மக்களுக்கு உணவு
தயாராகிக் கொண்டிருப்பதைப் போலத்
தோன்றும்.</p>

<p>     மண்டபத்தில் நுழைந்து பார்த்தால்,
அங்கே ஒன்றுமே இருக்காது. [எதுவும் தயா~
ராக வில்லை!]</p>

<p>     அந்த அதிகாரியைப் பார்த்து, "எ஥ற்காக
இவ்வாறு செய்தாய்?" என்று கேட்டார் ஒருவர்.</p>

<p>     "மக்கள் பட்டினியால் மடிந்து கொண்~
டிருக்கிறார்கள். அரசனோ நாடு செழிப்பாக
இருக்கிறது என்று கூறிக்கொண்டிருக்கிறான்.
அதை விளக்கவே இவ்வாறு செய்தேன்"
என்றான் அந்த அ஥ிகாரி.</p>

<p>-12-</p>

<p>            தொப்புளில் புனுகு</p>

<p>     மூன்று தோழர்கள் வேலை தேடி அடுத்த
ஊருக்கு நடந்து போய்க் கொண்டிருந்தனர்.</p>

<p>     வழியில், பெரிய பிள்ளையார் சிலை ஒன்று
காணப்பட்டது!</p>

<p>     பிள்ளையாருடைய தொப்புள் பெரிதாக
இருந்தது. அ஥னுள் ஏதாவது இருக்குமா
என்று பார்ப்பதற்காக ஒரு விரலை அ஥ில்
நுழைத்தான். உள்ளே ஒரு தேள் இருந்து
அவனுடைய விரலில் கொட்டியது.</p>

<p>     "ஆகா தொப்புளுக்குள் புனுகு இருக்~
கிறது!" என்றான் தொப்புளுக்குள் விரலை
வைத்தவன்.</p>

<p>     உ஠னே இரண்டாமவனும் தொப்புளில்
விரலை நுழைத்தான். அவன் விரலிலும் தேள்
கொட்டியது.</p>

<p>     அவனும், ஆ஖ா! தொப்புளுக்குள் புனுகு
இருக்கிறது என்றான்.</p>

<p>     அவர்கள் இருவரும் தொப்புளில் புனுகைத்
தொட்டது போல், தானும் தொப்புளில் விரலை
நுழைத்தான் மூன்றாமவன் அவனையும் தேள்
கொட்டியது.</p>

<p>-13-</p>

<p>              பெயரைச் சொல்லக்
                   கூடாதா?</p>

<p>     ஒரு ஊரில், ஒரு வீட்டில். சிலர் காசு வைத்து
சீட்டு விளையாடிக் கொண்டிருந்தனர். போலீ~
சுக்குப் பயந்து, கதவைத் தாழ் போட்டுவைத்~
திருந்தனர்.</p>

<p>     உள்ளூர்க்காரன் ஒருவன் வந்து கதவைத்
தட்டினான்.</p>

<p>     உள்ளே இருந்தவர்களில் ஒருவன் "யார்
அது?" என்று கேட்டான்</p>

<p>     பதில் இல்லை!</p>

<p>     "கோவிந்தனா?" என்றான் உள்ளே இருந்~
தவன்.</p>

<p>     "ஹூ உம்' என வெளியில் இருந்தபடியே
தலையை அசைத்தான்.</p>

<p>     "குப்பனா?" என்று மறுபடியும் கேட்டான்</p>

<p>     அ஥ற்கும் வெளியிலிருந்தே த஼லை அசைத்~
தான் அவன்.</p>

<p>     "குருசாமியா? என்று மறுபடியும் கேட்டான்.</p>

<p>     அ஥ற்கும் வெளியிலிருந்தே அதே தலை
அசைப்புத்தான்.</p>

<p>-14-</p>

<p>     "கிருஶ்ணன் நாயரா?" என்றான் உள்ளே
இருந்தவன்.</p>

<p>     `அதே அதே' என்றான் வெளியில் இருந்~
தவன். உ஠னே கதவைத் திறந்து, "அட
முண்டமே. முதலிலேயே உன் பெயரைச்
சொல்லித் தொலைத்திருக்கக் கூடாதா? நாங்கள்
பயந்து போய்த்தானே கதவைச் சாத்தி, தாழ்
போட்டுக் கொண்டிருக்கிறோம் ஒவ்வொரு பெய~
ராகச் சொல்லும் வரை தூண் போல் நின்று
கொண்டிருந்தாயே!" என்றனர்.</p>

<p>-15-</p>

<p>        சும்மாவா கொண்டு வந்தோம்</p>

<p>     செட்டிநாட்டு வட்டாரத்தில் ஒரு இளைஞன்
அவனுக்குத் தாய் தந்தை இல்லை. அந்த
இளைஞன் தனக்காக ஒரு வீட்டை அழகாகக்
கட்டிக் கொண்டான்.</p>

<p>     உறவினர்கள் அவனுக்குத் திருமணம்
செய்து வைத்தார்கள். பெண் கறுப்பு நிறம்;
குள்ளமானவள்; தடிமனாக இருப்பாள். அ஥்~
தகையவள் தன்னை மிக உயர்வாக எண்ணிக்
கொண்டாள்.</p>

<p>     முதல் இரவில் கணவனிடம், "வீட்டையும்
கட்டி ஂவெள்ளையும் அடித்து எலக்ட்டோரியான்
விளக்கையும் போட்டு, எல்லோரும் கூடி
என்னை மயக்கிக் கொண்டு வந்து விட்டீங்களே
என்று சொன்னாள்.</p>

<p>     ஐம்பது ஆண்டுகளுக்கு முன் ஂபெண்கள்
கிராக்கியாக இருந்ததால், பெண்ணுக்கு நிறையப்
பணம் கொடுக்க வேண்டியதாயிருந்஼தது</p>

<p>     அ஥ற்குக் கணவன், உன்னை சும்மாவா
கொண்டு வந்தோம் [இரண்டு கையும் காட்டி]
இவ்வளவு பணம் என்றான்.</p>

<p>-16-</p>

<p>       மோதிரம் அணிந்த பெருமை</p>

<p>     கிராமத்திலிருந்து ஒருவன் வேலை தேடி
பட்டணத்துக்குச் ஂசென்றான். அங்கே அலைந்து
திரிந்து வேலை தேடினான்.</p>

<p>     ஒரு மாதம், இரண்டு மாதம் என்று ஒவ்~
வொரு இ஠த்திலும் எடுபிடி வேலை பார்த்தான்.
கிராமத்துக்கே திரும்பிச் சென்றான்.</p>

<p>     கிராமத்து இளைஞர்கள், அவனுடைய
பட்டண வாழ்க்கையைப் பற்றி ஆவலோடு
விசாரித்தார்கள்.</p>

<p>     அவனும் ஆளுக்கு ஒரு மாதிரியாக
அளந்து விட்டான்.</p>

<p>     "அடுத்த தடவை பட்டணத்துக்குப்
போகும் போது என்னைக் கூட்டிக் கொண்டு
போ" என ஒவ்வொருவனும் நச்சரித்தான்.</p>

<p>     அவர்களில் ஒருவன், தன்னுடைய நினை~
வாக அவனுக்குத் தங்க மோதிரம் ஒன்றைப்
பரிசாக அளித்தான்.</p>

<p>     "நான் பட்டணம் போய் கடிதம் போடு~
கிறேன்" என்று எல்லோரிடமும் கூறிவிட்டு
மறுபடியும் பட்டணத்துக்குப் புறப்பட்டான்.</p>

<p>     பட்டணத்தில் முன்பு பழக்கமானவர்களைப்
பார்த்தான். அவர்களில் சிலர், "மழை உண்டா?
வேளாண்மை எப்படி?" என்று விசாரித்தனர்.</p>

<p>     எல்லோரிடமும், "எங்க ஊர் ஆற்றில்
மார்பு அளவுக்கு தண்ணீர் போகிறது," என்று
கூறி மோதிரம் அணிந்த விரலை மார்பில்
வைத்துக் காட்டிக் கொண்டிருந்தான்.</p>

<p>-17-</p>

<p>        தண்ணீர் நிற்பது எ஥னால்?</p>

<p>     ஒரு சிறிய ஜமீன்தார் தம்முடைய
ஆ஠்களோடு நிலங்களைப் பார்வையிடச்
சென்று கொண்டிருந்தார்.</p>

<p>     அப்போது அருகில் இருந்த ஏரியைப்
பார்த்து, "இந்த ஏரித் ௃஼தண்ணீர் எப்படி இருக்~
கிறது?" என்று கேட்டார் ஜமீன்தார்</p>

<p>     "பால் போல் இருக்கிற஥ு" என்றான்
ஒருவன்.</p>

<p>     "முத்துப் போல் இருக்கிறது" என்றான்
மற்றொருவன்</p>

<p>     "தெளிவாய் இருக்கிறது" என்றான் இன்~
னொருவன்.</p>

<p>     மூவர் சொன்ன பதிலும் ஜமீன்தாருக்குத்
திருப்தி அளிக்காமல் அருகில் இருந்த பிரதான
அதிகாரியைப் பார்த்தார்.</p>

<p>     "ஜமீன்தார் அவர்களே! தண்ணீர் கரை~
யாலே இருக்கிற஥ு" என்றார் அ஥ிகாரி</p>

<p>     [கரை இல்லாவிடில் தண்ணீர் தேங்கி
நிற்குமா?]</p>

<p>-18-</p>

<p>      இருட்டில் ஏற்பட்ட தடுமாற்றம்</p>

<p>     கணவன் தொழிற்சாலையின் இரவு நேர
வேலைக்குச் சென்று விட்டான்.</p>

<p>     மனைவியோ ஆசை நாயகனைத் தேடிச்
சென்றாள்.</p>

<p>     நல்ல இருட்டு நேரம்!</p>

<p>     வேலைக்குச் சென்ற கணவனோ ஆசை
நாயகியைத் தேடிச் சென்று கொண்டிருந்தான்.</p>

<p>     வழியில் எ஥ிர்ப்பட்ட கணவனை, ஆசை
நாயகன் என்று எண்ணி, அவன் கையைப்
பற்றினாள்! மனைவி.</p>

<p>     அப்பொழுது மனைவியை, ஆசை நாயகி
என்று எண்ணி அவள் கையைப் பற்றினான்
கணவன்.</p>

<p>-19-</p>

<p>           அவனுடைய சுயநலம்</p>

<p>     அரசனிடம் ஒரு பிராமணன், ஒரு பசுவும்
கன்றும் தானமாக வாங்கி ஓட்டிக் கொண்டு,
பிராமணர்் வீதியிலே சென்றான்.</p>

<p>     அங்கே ஒரு பிராமணன், "ராஜா எத்தனை
பசுக்கள் தானம் கொடுத்தார்?" என்று கேட்~
டான்.</p>

<p>     "ஒவ்வொருத்தனுக்கும் ஒரு பசு வீதம்,
ஐநூறு பசுக்கள் தானம் செய்தார். ஆனால்,
எனக்கு மாத்திரம் இரண்டு பசுக்கள் தானம்
கொடுத்தார்" என்றான்.</p>

<p>     "ஒன்று தானே ஓட்டிக் கொண்டு போகி~
றாய்?" என்று கேட்டான் அவன்.</p>

<p>     "ராஜா, என் அடுத்த வீட்டு ரா஗வனுக்குக்
கொடுக்கவில்லை. அ஥னால், எனக்கு இரண்டு
கொடுத்ததாயிற்று" என்று பெருமையோடு
கூறினான் அந்தப் பிராமணன்.</p>

<p>-20-</p>

<p>           கடனை அடைக்க வழி</p>

<p>     ஆலையில் வேலை செய்யும் தொழிலாளி
ஒருவன் கடன் வாங்கிச் செலவு செய்து விட்டுக்
கடனைக் கொடுக்க முடியாமல் தவித்தான்.</p>

<p>     கடன் கொடுத்தவனோ அவனை நெருக்கி~
னான்.</p>

<p>     தொழிலாளி மற்றொருவனிடம் போய்க்
கடன் கேட்டான். அவன் "என்ன தேவைக்குக்
கடன் கேட்கிறாய்?" என்று கேட்டான்.</p>

<p>     "ஏற்கனவே ஒருவனிடம் கடன் வாங்கி~
னேன். அதைக் கொடுக்க முடியாததால், அவன்
வந்து நிர்ப்பந்திக்கிறான்" என்று சொன்னான்
தொழிலாளி.</p>

<p>     "சரி, இப்பொழுது நான் கடன் தந்தால்,
அதை எப்படி எனக்குத் திரும்பித் தருவாய்?"
என்று கேட்டான்.</p>

<p>     "மாதா மாதம் சிறு தொகை சேர்த்து,
அதைக் கொடுத்து விடுகிறேன்" என்றான்
தொழிலாளி.</p>

<p>     என்னிடம் கடன் வாங்கி, அவனிடம்
கொடுத்துத் தீர்த்து விடுவாய். பிறகு, எனக்கும்</p>

<p>-21-</p>

<p>உனக்கும் தகராறு உண்டாகும். முதலிலேயே
அதை மாதா மாதம் சிறுகச் சிறுகக் கொடுத்துத்
தீர்த்திருக்கலாமே. இ஫்பொழுதும் ஒன்றும்
கெட்டுப் போகவில்லை. புதிதாகக்கடன் வாங்கு~
வதற்கு அலையாமல், பழைய கடன்காரனுக்கு
மாதா மாதம் சிறுதொகை தருவதாகச்சொல்லி
அப்படியே கொடுத்து விடு. க஠ன் தீர்ந்து
நிம்மதியாக இருக்கலாம்" என்று புத்தி கூறி~
னான்.</p>

<p>-22-</p>

<p>            லாபம் யாருக்கு?</p>

<p>     ஒரு செட்டியார் வட்஼டிக்குக் கடன்
கொடுத்து வந்தார்.</p>

<p>     சந்தையில் காய்கறி வியாபாரம் செய்ப~
வனும், பழ வியாபாரம் செய்பவனும் செட்டி~
யாரிடம் வந்து நூறு ரூபாய் கடன் கேட்டார்~
கள்.</p>

<p>     "கடன் யாருக்கு வேண்டும்?" என்று
கேட்டார். செட்டியார்.</p>

<p>     "காய்஖றி வியாபாரி, எனக்குத்தான்
வேண்டும். பழவியாபாரி அ஥ற்கு ஜாமீன்"
என்றான்.</p>

<p>     ஒரு மாதத்திற்கு நூற்றுக்குப் பத்து ரூபாய்
வட்டி. மூன்று மாதத்துக்கான வட்டி முப்பது
ரூபாய் பிடித்துக் கொண்டு எழுபது ரூபாய்
கொடுப்பேன் சம்மதமா? என்று கேட்டார்
செட்டியார்.</p>

<p>     சரி என்று சொல்லி கடன் சீட்டில்
கையொப்பம் போட்டு விட்டு, எழுபது ரூபாயை
வாங்கிக் கொண்டு புறப்பட்டனர்.</p>

<p>     காய்கறி வியாபாரியிடம் "ஏண்டா, இப்படி
அநியாய வட்டி கொடுத்து வாங்கினால் கட்டு~
படி யாகுமா?" என்று கேட்டான் பழ வியா~
பாரி.</p>

<p>     "செட்டியாருக்கு முப்பது ரூபாய் லாபம்;
எனக்கோ எழுபது ரூபாய் லாபம். கட்டுபடி
ஆகாமல் என்ன?" என்றான் காய்கறி வியா~
பாரி.</p>

<p>-23-</p>

<p>         மூவருக்கும் இ஠ம் உண்டு</p>

<p>     ஒரு ஊரில் இருந்த கோயிலுக்குப் போய்~
விட்டுத் திரும்பினார் ஒருவர். அங்கே காலியாக
இருந்த சிறு குடிசையில் சென்று இரவில்
உறங்கத் தொடங்கினார்.</p>

<p>     சிறிது நேரத்தில், யாரோ ஒருவர் கதவைத்
தட்டி, "இரவு தங்க இடம் கிடைக்குமா?"
என்று கேட்டார்.</p>

<p>     "கட்டாயம் இடம் கிடைக்கும். இங்கே
ஒருவர் படுக்கலாம்; இருவர் உ஠்காரலாம்
ஆகையால், உள்ளே வாரும். இருவரும்
உ஠்கார்ந்திருப்போம்" என்று கூறி, கதவைத்
திறந்தார். இருவரும் உ஠்கார்ந்து உரையாடிக்
கொண்டிருந்தனர்.</p>

<p>     அப்பொழுது மற்றொருவர் வந்து கதவைத்
தட்டி, ஒருவர் இன்று இரவு தங்க இடம்
கிடைக்குமோ?" என்றார்.</p>

<p>     "கட்டாயம் இடம் கிடைக்கும்; இங்கே
ஒருவர் படுக்கலாம்; இருவர் உட்காரலாம்;
மூவர் நிற்கலாம்; நீங்கள் உள்ளே வாருங்கள்"
என்று கூறி, கதவைத் திறந்தார்.</p>

<p>     மூவரும் நின்று கொண்டே இரவு முழுதும்
உரையாடிக் கொண்டிருந்தனர்.</p>

<p>-24-</p>

<p>                நிலவே சாட்சி</p>

<p>     இரவு, உணவுக்குப் பிறகு, நிலவு ஒளியில்
ஒருவன் வீட்டு முற்றத்தில் பாயை விரித்துப்
படுத்தான். அப்போது வானத்திலிருந்த
நிலவைப் பார்த்து ஏளனமாகச் சிரித்தான்.</p>

<p>     மனைவி வேலையை முடித்து விட்டு,
வெற்றிலை பாக்குடன் அவன் அருகில் அமர்ந்~
தாள்.</p>

<p>     கணவனிடம், "நான் வந்து கொண்~
டிருக்கும் போது, குறும்பாகச் சிரித்தீர்களே,
ஏன், என்னைப் பற்றியா?" என்று கேட்டாள்
மனைவி.</p>

<p>     "அது ஒரு ரகசியம்!" என்றான்.</p>

<p>     "அதைச் சொல்லியே தீரவேண்டும்" என்று
சிணுங்கினாள்; கெஞ்சினாள். சாகசங்கள் செய்~
தாள்.</p>

<p>     அவன் உறுதி தளர்ந்தது. சொல்லத்
தொடங்கினான்!</p>

<p>     "பன்னிரண்டு ஆண்டுகளுக்கு முன் உன்
அண்ணன் வயலில் இறந்து கிடந்தானே!
நினைவு இருக்கிறதா? என்னுடைய நிலத்துக்கு
அருகில் சிறு துண்டு நிலம் இருந்தது. அதை</p>

<p>-25-</p>

<p>நான் வாங்க முயன்றேன். அவன் அதை வாங்க
எண்ணினான். எவ்வளவோ கூறி எனக்கு
விட்டு விடச் சொல்லி அவனிடம் மன்றாடி~
னேன். அவன் பிடிவாதமாக மறுத்து, அதிக
விலை கொடுத்து, எனக்குப் போட்டியாக
வாங்கி விட்டான். அதை நான் பொருட்படுத்~
தாதவன் போல் இருந்துவிட்டேன். ஒரு நாள்,
சாராயத்தில் விஶத்தைக் கலந்து, அவனைக்
குடிக்கச் செய்தேன். அவன் குடித்ததும், தலை
சுற்றியது "எனக்கு நெஞ்சு வலிக்கிறது; விஶத்~
தைக் கலந்து தந்து விட்டாய் நான் சாகப்
போகிறேன்." நிலவைப் பார்த்து, "நிலவே ௃நீ
தான் சாட்சி! நீ இருப்பது உண்மையானால்,
அவனுக்குத் தண்டனை கொடு" என்று
சொல்லி விட்டு, மயங்கி விழுந்து செத்து விட்~
டான். இப்பொழுது நினைவுக்கு வந்தது.
அதைப் பார்த்து, எனக்குச் சிரிப்பு வந்தது'.
என்று கூறினான்.</p>

<p>     உ஠னே அவள் வேகமாக ஓடிப்போய்
கிராமத்து தலையாரிடம் க஼ூறி விட்டு, தாய்
வீட்டிற்குப் போய் விட்டாள்.</p>

<p>     பிறகு அவன் கைது செய்யப்பட்டான்!</p>

<p>-26-</p>

<p>      எளிதில் மாற்றி விட முடியுமா?</p>

<p>     ஒரு அரசன், தன்னுடைய நாட்டு மக்கள்
கடைப்பிடித்து வரும் பழக்க வழக்கங்கள் மீது
வெறுப்புற்று, அவற்றை மாற்றி அமைத்துப்
புதுமையை ஏற்படுத்த விரும்பினான். அ஥ற்~
காக, ஒரு குழுவை நியமித்து, அக் குழுவினர்,
முன்னேற்றம் அடைந்துள்ள நாடுகளுக்குச்
சென்று, அங்குள்ள பழக்க வழக்கங்களைக்
கண்டு வந்தால், தன்னுடைய திட்டம் எளிதாக
நிறை வேறும் என்று எண்ணினான்.</p>

<p>     அரசனுடைய திட்டத்தை அமைச்சர்களும்,
அதிகாரி஖ளும் பாராட்டினர்.</p>

<p>     பாராட்டாமல், பதில் சொல்லாமல் இருந்த
ஒரே ஒரு அமைச்சரைப் பார்த்து, "இந்தத்
திட்டம் உமக்கு விருப்பம் இல்லையோ?" என்று
கேட்டான் அரசன்.</p>

<p>     "அரசே! இது ப௟ன் தராது. எந்த மாற்ற~
மும் நாட்டில் ஏற்படாது என்பது உறுதி!"
என்று துணிவாகக் கூறிவிட்டு,</p>

<p>     ஒரு தாளை எடுத்து, இரண்டாக மடித்~
தான். அதைப் பல முறை விரல்களால் அழுத்தி,
பிறகு நகத்தால் தேய்த்து, அதை அரசனிடம்
நீட்டி.</p>

<p>-27-</p>

<p>     "அரசே! நீங்கள் எதையும் சாதிக்கக்கூடிய
வல்லமை உள்ளவர். உங்கள் எ஥ிரில் எந்த
எதிர்ப்பும் நிற்காமல் மறைந்து விடும் என்பது
எங்கள் அனைவருக்கும் தெரியும். ஆகையால்,
தயை கூர்ந்து, இந்தத் தாளில் உள்ள மடிப்பை,
அ஥ன் அ஠ையாளமே தெரியாதபடி, செய்ய
முடியுமா என்று பாருங்கள்" என்றான் அந்த
அமைச்சன்.</p>

<p>     அர஛ன் சிந்தித்தான்; இரத்தத்தில் ஊறிப்
போன மனத்தின் ஆழப்பதிந்த எதையும்
எளிதில் மாற்ற முடியாது என்பதை உணர்ந்~
தான்.</p>

<p>-28-</p>

<p>           கூனன் செய்த உதவி</p>

<p>     ஆற்றில் ஒருவன் தூண்டில் போட்டு மீன்
பிடித்துக் கொண்டிருந்தான். அவன் முதுகு
சற்று வளைந்திருந்ததால், அந்தப் பக்கமாகப்
போகும் சிறுவர்கள் அவனைக் கூனன் என்று
சொல்லிக் கேலி செய்தனர். அவனோ அவர்~
களைப் பார்க்காமலும், அவர்கள் சொற்களைக்
காதில் வாங்காமலும் காரியத்திலேயே ஖ண்ணா~
யிருப்பான்:</p>

<p>     ஒருநாள் சிறுவர்கள் கரையில் விளையாடிக்
௃கொண்டிருந்தனர். அவர்களில் சிலர் ஆற்றில்
குளித்தனர். ஒருவன் சுழலில் சிக்கித் தத்தவித்~
தான். அவர்களுக்கு நீச்சல் தெரியாது. எதுவும்
செய்ய இயலாமல் பயந்து கூச்சலிட்டனர்.</p>

<p>     சிறுவர்கள் போட்ட கூச்சலை அறிந்த
கூனன், உ஠னே ஆற்றில் குதித்து, மூழ்கிக்
கொண்டிருந்த சிறுவனைப் பிடித்து இழுத்துக்
கரைக்குக் கொண்டு வந்தான். அவன் குடித்~
திருந்த நீரை வெளியேற்றினான் சிறுவன்
பிழைத்துக் கொண்டான்.</p>

<p>     சிறுவர்கள் தங்கள் செயலுக்குக் கூனனிடம்
மன்னிப்புக் கேட்டனர்.</p>

<p>-29-</p>

<p>          மரத்தொட்டி விற்பனை</p>

<p>     ஆலைத் தொழிலாளி ஒருவன். தினமும்
இரண்டு மைல் தொலைவு நடந்து போய்
வேலை பார்த்து மாலையில் திரும்பி வருவான்.</p>

<p>     அவன் மனைவி அழகாக இருப்பாள். தன்
மனைவியைப் பற்றி பெருமையாக எண்ணிக்
கொள்வான்.</p>

<p>     ஆலையில் வேலை நிறுத்தம் தொடங்கி
விட்டது. அவனுக்குக் கிடைத்து வந்த
வருமானம் தடைப்பட்டது.</p>

<p>     ஒரு நாள், "இப்படி வருமானம் இல்லாமல்
எத்தனை நாட்களைக் கழிப்பது? சாமான்களை~
யும் ஒவ்வொன்றாக விற்றுக் கொண்டிருக்~
கிறோமே. மரத்தொட்டி ஒன்றுதான் பாக்கி
இருக்கிறது!" என்று கணவனும் மனைவியும்
பேசிக் கொண்டிருந்தனர்.</p>

<p>     தொழிலாளி வேலை தேடி வீட்டை விட்டுப்
புறப்பட்டான். இனி மாலையில் தான் அவன்
வருவான் என்று நினைத்த அவன் மனைவி,
தன்னுடைய ஆசைநாயகனை வரவழைத்து,
அவனோடு மகிழ்ந்து கொண்டிருந்தாள்.</p>

<p>     திடீரென கதவு தட்டும் சத்தம் கேட்டது.
கணவனே திரும்பி வந்திருக்கிறான் என்பதை</p>

<p>-30-</p>

<p>அறிந்த மனைவி, ஆசைநாயகனை கொல்லைப்
புறத்தில் இருக்கும் மரத்தொட்டிக்கு அருகில்
ஒளிந்து கொள்ளும்படி சொல்லிவிட்டு, வந்து
கதவைத் திறந்தாள்.</p>

<p>     உள்ளே வந்தவன் மனைவியிடம்,</p>

<p>     "வேலை ஒன்றும் கிடைக்கவில்லை. அடுத்த
மூன்றாம் நாள் ஆலையைத் திறந்து விடுவார்~
கள் என்று சொல்கிறார்கள். அந்த மரத்~
தொட்டியை விற்பதற்கு ஆள் கூட்டி வந்திருக்~
கிறேன்" என்றான் தொழிலாளி.</p>

<p>     "நீங்கள் அந்தப் பக்கம் போனதும், நான்
ஒரு ஆளைக்கூப்பிட்டு, அந்த மரத்தொட்டியை,
இருபத்தைந்துக்குப் பேசியிருக்கிறேன். அந்த
ஆள் தொட்டியைப் பார்த்துக் கொண்டிருக்~
கிறார்" என்று ஒளிந்து இருப்பவனுக்கு
கேட்கும்படியாகச் சொன்னாள்.</p>

<p>     தான் கூட்டி வந்த ஆளை அனுப்பி விட்டு,
கொல்லைப்புறத்துக்குப் போய், "என்ன
தோழரே! தொட்டி பிடித்திருக்கிற஥ா?" என்று
கேட்டான் தொழிலாளி.</p>

<p>     அவன் ஒன்றும் தெரியாதவனைப் போல்
"நீ யார்? அந்த அம்மா எங்கே? அம்மா!" என்று
குரல் கொடுத்தான் தொட்டிக்கு அருகில் இருந்த
ஆசைநாயகன்.</p>

<p>     இருபத்தைந்து ரூபாயை அவளிடம்
கொடுத்துவிட்டு, `தொட்டியை மறுநாள் எ஠ுத்~
துப் போவதாகச் சொல்லி' அவன் புறப்பட்டு
விட்டான்.</p>

<p>-31-</p>

<p>          தந்தையை விடுவித்த மகன்</p>

<p>     ஒரு கிராமத்தில் புரோகிதர் ஒருவர் வசித்து
வந்தார். அவருக்குக் கொஞ்சம் நிலம் இருந்தது.
அவர் நிலத்தீர்வை கட்டாததால், அந்தநாள்
வழக்கப்படி அவரைச் சிறையில் அடைத்தனர்.</p>

<p>     புரோகிதரின் தந்தை இறந்து இரண்டு
ஆண்டுகள் ஆயின. அந்த நினைவுக்காக
சடங்குகள் செய்வது வழக்கம். புரோகிதர்
சிறையில் இருக்கிறாரே. எவ்வாறு செய்ய
முடியும். ஊரில் இருக்கும் மற்றவர்களுக்கு எல்~
லாம் நினைவுபடுத்தி சடங்குகளைச் செய்து வரு~
வார். அவருடைய சொந்தக்காரியத்தை அவர்
செய்ய முடியாமல் சிறையில் இருக்கும்படி
ஆயிற்றே என்று அவர் மனைவி மிகவும் வருந்~
தினார்.</p>

<p>     தாயின் வருத்தத்தையும், தந்தையின் நிலை~
யையும் அறிந்தான் மகன். அவனுக்கு வயது
பதின்மூன்று. பள்ளியில் படிக்கிறான்.</p>

<p>     காலையில் எழுந்து, நடந்து, இருபது மைல்
தொலைவில் இருந்த அதிகாரியிடம் போய்,
நிலைமையைக் கூறினான்.</p>

<p>-32-</p>

<p>     "உன் தந்தையை மூன்று நாட்கள் விடுவிக்~
கிறேன். ஆனால், அ஥ற்கு ஜாமீன் வேண்டும்"
என்றார் அ஥ிகாரி.</p>

<p>     "ஜாமீன் கொடுக்க எவருமே இல்லை.
ஆனால், ஒரே ஒரு வழிதான் உண்டு. அவர்
திரும்பி வரும் வரையில், மூன்று நாட்களுக்கு
நான் சிறையில் இருக்கிறேன்" என்றான்
சிறுவன்.</p>

<p>     பதின்மூன்று வயதுப் பையனிடமிருந்து
எ஥ிர்பாராத கிடைத்த இந்தப் பதில் அ஥ிகாரி~
யின் உள்ளத்தை நெகிழச் செய்து விட்டது.
உ஠னே, ஜாமீன் இல்லாமலேயே மூன்று
நாட்கள் அவரை விடுதலை செய்தார்.</p>

<p>-33-</p>

<p>          எங்கே இருந்தால் என்ன?</p>

<p>     ஒரு சமயம் ஒரு சிற்றூரில் பிளேக் வியாதி
பரவிக் கொண்டிருந்தது. அங்கே இருந்த
பண்ணையார் பயந்து பட்டணத்துக்குப் போய்
விடத் தீர்மானித்தார். அ஥ற்காக வண்டியைத்
தயார் செய்யும்படி கூறினார்.</p>

<p>     வண்டியோட்டி சமையல்காரனைப்
பார்த்து, "நீயும் வருகிறாயா?" என்று கேட்~
டான்.</p>

<p>     அ஥ற்கு, "பண்ணையார் பிளேக்கிலிருந்து
தப்புவதற்காக, இந்தக் கிராமத்தை விட்டுப்
புறப்படுவதைப் பார்த்தால், அவருடைய
கடவுள் கிராமத்தில் வசிக்காமல், பட்டணத்தில்
வசிப்பது போல் தோன்றுகிறது. என்னுடைய
கடவுள் இங்கேயேதான் இருக்கிறார்" என்றான்
சமையல்காரன்.</p>

<p>     ஆமாம். உண்மைதான்! கடவுள் இருப்பா~
ரானால் எல்லா இ஠ங்களிலும்தானே இருக்~
கிறார். கிராமத்தில் இருந்தாலும், காப்பாற்றத்~
தானே செய்வார்" என்றான் வண்டியோட்டி.</p>

<p>-34-</p>

<p>           திணற வைத்த கேள்வி</p>

<p>     ஒரு ஊரில் ஒரு தாத்தா இருந்தார். ஒரு நாள்
அவர் தம் பேரனை வைத்துக் கொஞ்சிக்
குலாவிக் கொண்டிருந்தார்.</p>

<p>     பேரன் அவரிடம் "தாத்தா! என்னிடம்
உங்களுக்கு அ஥ிகப் பிரியம் உண்டு அல்லவா?
என்று கேட்டான்:</p>

<p>     "ஆம். உன்னிடம் எனக்கு உள்ள பிரியத்~
துக்கு அளவே இல்லை" என்றார் தாத்தா.</p>

<p>     பேரன், "தாத்தா! அப்படியானால்,
கடவுளிடமும் பிரியம் வைத்திருக்கிறீர்கள்
அல்லவா?" என்று கேட்டான்.</p>

<p>     அவர், "ஆம் பேரா, கடவுளிடமும் எனக்குப்
பிரியம் உண்டு" என்றார்.</p>

<p>     உ஠னே பேரன், "அது எப்படி தாத்தா
முடியும்? உங்களுக்கு உள்ளம் ஒன்று தானே?"
என்று கேட்டான்.</p>

<p>     தாத்தா திகைத்து விட்டார். `உலகப்பற்று
நிறைந்துள்ள மனத்தில், கடவுள் பற்று எவ்~
வாறு இருக்கும்?'</p>

<p>-35-</p>

<p>       குழந்தை அறிந்த நாய் - பேய்</p>

<p>     ஒரு ஊரில் கணவனும் மனைவியும் வாழ்ந்து
வந்தனர். அவர்களுக்கு ஒரு பெண் குழந்தை
இருந்தது.</p>

<p>     கணவன் ஒரு வீட்டில் வேலை பார்த்து
வந்தான். குறைவான சம்பளம். மனைவி தன்
தாய் வீட்டுக்குப் போய், நெல், தானியங்கள்
முதலானவற்றைக் கொண்டு வருவாள். அவளு~
டைய தாய் வீடு கொஞ்சம் வசதியானது.</p>

<p>     கணவனின் வருமானம் போதவில்லை.
மேலும், தான் கொண்டுவந்து போடுகிறேன்
என்பதால் மனைவிக்குக் கர்வம் அதிகம். அ஥~
னால், கணவனை ஏளனமாக `நாய், பேய்'
என்று தினமும் கூறி கேவலப் படுத்துவாள்
மனைவி. குழந்தை அதைக் கவனித்துக் கொண்~
டிருக்கும்.</p>

<p>     கணவன் பொறுமை இழந்து, ஒரு நாள்
வீட்டை விட்டுப் புறப்பட்டு விட்டான். எங்கே
போனான், என்ன ஆனான், என்று தெரியாது.
அவனைப் பற்றி அவள் கவலைப் படவும்
இல்லை.</p>

<p>-36-</p>

<p>     இரண்டு மாதங்கள் எங்கெங்கோ சுற்றி
விட்டு, பிழைக்க வழியில்லாமல் திரும்பி
ஊருக்கு வந்து வீட்டின்முன் நின்றான்.</p>

<p>     அவனைக் கண்டதும் மூன்று வயதுக்
குழந்தை, "அம்மா, அம்மா! `நாய்' பேய்' வந்து
நிற்கிற஥ு" என்று கூறியது. குழந்தை கூறிய~
தைக் கேட்டதும் வீட்டினுள் இருந்த மனைவி
ஓடி வந்து பார்த்தாள். கணவன் வாசற்படியில்
நின்று கொண்டிருந்தான்!</p>

<p>-37-</p>

<p>       சொன்னபடியே செய்தான்</p>

<p>     தன்னுடைய கிராமத்திலிருந்து ஒரு
வேலையாளை பம்பாய்க்குக் கூட்டிச் சென்றார்
ஒரு பணக்காரர்.</p>

<p>     அந்த வேலைக்காரனுக்கு தன்னுடைய
கிராமத்தைத் தவிர வேறு ஊர் எ஥ுவும் தெரி~
யாது. படிக்காதவனும் கூட.</p>

<p>     பம்பாய் சென்றதும் அந்த வேலைக்கார~
னிடம், "அடே இது பெரிய நகரம். கார்களும்,
வண்டி஖ளும், ஸ்கூட்டர்களும் போய்க் கொண்~
டும், வந்து கொண்டும் இருக்கும். கடை~
களுக்குப் போகும்போது. மிகவும் ஜாக்கிரதை~
யாகப் போக வேண்டும். இ஠து பக்கம் கார்
வந்தால், வலது பக்கம் போக வேண்டும். வலது
பக்கம் கார் வந்தால், இ஠து பக்கம் போக
வேண்டும்" என்று சொன்னார் பணக்காரர்.</p>

<p>     இரண்டு மூன்று நாட்களுக்குப் பிறகு,
பணக்காரர் வீட்டுக்கு ஒரு காவலர் வந்து,
"உங்கள் வேலைக்காரன் கார் விபத்துக்கு உள்~
ளாகி, மருத்துவமனையில் சேர்க்கப்பட்டிருக்~
கிறான்" என்று கூறினார்.</p>

<p>-38-</p>

<p>     பணக்காரர் மருத்துவ மனைக்குச் சென்று
அவனைப் பார்த்தார். "நான் சொன்னபடி நீ
நடந்திருந்தால், இப்படி விபத்துக்கு ஆளாகி~
யிருக்கமாட்டாயே?" என்றார்.</p>

<p>     படுக்கையில் கிடந்த வேலைக்காரன், "முத~
லாளி நீங்கள் சொன்னபடி செய்ததால்தான்,
இப்படி விபத்து நேரிட்டது. இ஠து பக்கமும்
கார் வந்தது; வலது பக்கமும் கார் வந்தது;
நான் நடுவில் சென்றேன்" என்றான் அவன்.</p>

<p>-39-</p>

<p>         தந்தை சொன்ன சிக்கனம்</p>

<p>     தன் மகனுக்குத் திருமணம் செய்து
வைத்து, அவனைத் தனிக்குடித்தனம் நடத்து~
மாறு சொல்லி, வருமானத்துக்கான வழியை~
யும் அவனுக்கு ஏற்பாடு செய்து கொடுத்தார்
தந்தை.</p>

<p>     வாரம் ஒரு முறை மகனை தந்தை வந்து
பார்த்துப் போவது வழக்கம்.</p>

<p>     ஒரு முறை தந்தை வந்திருந்தார். இரவு
நேரம். தந்தையும் மகனும் பேசிக் கொண்டிருந்~
தனர்.</p>

<p>     "அப்பா! நீங்கள் எவ்வாறு பணக்காரராக
ஆனீர்கள்? என்னுடைய வருமானத்தோடு
உங்களுடைய உதவி இருந்தும், எனக்கு
பற்றாக் குறையாகவே இருக்கிறதே!" என்று
கேட்டான் மகன்.</p>

<p>     "மகனே, எ஥ுவும் சுலபம் அல்ல! வாழ்க்~
கையில் மிகவும் கவனம் தேவை. தேவையற்ற
செலவைச் செய்யக் கூடாது. வீட்டில் வெளிச்~
சத்துக்கு, ஒரு விளக்கே போதும் என்றால்,
மற்றொரு விளக்கை எ஥ற்காக எரியவிட
வேண்டும்?" என்று கேட்டவாறு எழுந்தார்
தந்தை.</p>

<p>     உ஠னே மகன் எழுந்து தேவையில்லாமல்
எரிந்து கொண்டிருந்த மற்றொரு விளக்கை
அணைத்தான்.</p>

<p>-40-</p>

<p>           தங்கையின் பகிர்வு</p>

<p>     அக்காளுக்கும் தங்கைக்கும் அடிக்கடி
சண்டை ஏற்படும். தங்கைக்குத் தொந்தரவு
கொடுப்பதில் அக்காளுக்கு உற்சாகம்.</p>

<p>     ஒருநாள் இருவருக்கும் சச்சரவு அ஥ிகமாகி,
தங்கையின் மயிரைப் பிடித்து இழுத்து அடி
அடி என்று அடித்து விட்டாள் அக்காள்.</p>

<p>     அதை அறிந்த தந்தையும் தாயும் அவளைத்
திட்டி ஒரு அறையில் தள்ளி பூட்டி வைத்தனர்.
மேலும், அவளுக்கு பகல் உணவு அளிக்கக்
கூடாது என்று தீர்மானித்தனர்.</p>

<p>     அன்று ஞாயிற்றுக்கிழமை அப்பளம்,
வடை, பாயசத்தோடு எல்லோரும் மகிழ்ச்சி~
யோடு உண்டு உறங்கினர்.</p>

<p>     அறையில் தள்ளப்பட்டிருந்த அக்காள்
பசியோடு வேதனைப்படுவாளே என்ற இரக்கத்~
தோடு, பெற்றோர்க்குத் தெரியாமல், அறை~
யைத் திறந்து, கொண்டு போய் உணவை
அளித்துச் சாப்பிடும்படி வற்புறுத்தினாள்
தங்கை.</p>

<p>     "இனி உன்னோடு ஒருபோதும் சண்டை~
யிட மாட்டேன் இது உறுதி" என்றாள்
அக்காள்.</p>

<p>-41-</p>

<p>                பரோபகாரம்</p>

<p>     பள்ளிக்கூடத்தில் மணி அடித்ததும் மாண~
வர்கள் வேகமாக வெளியேறிக் கொண்டிருந்~
தனர்.</p>

<p>     அப்பொழுது ஒரு இளைஞன் நொண்டி
நொண்டி நடந்து போய்க் கொண்டிருந்தான்.
அவனைப் பார்த்து, சில மாணவர்கள்
அவனைப் போல், நொண்டி நொண்டி நடந்து
கேலி செய்தனர்.</p>

<p>     அதைப் பார்த்த பெரியவர், "அடே பசங்~
களா! அவனைக் கேலி செய்யாதீர்கள். அவன்
நொண்டியாகப் பிறக்கவில்லை. முன்பு ஒரு
மாடி வீட்டில் தீப்பற்றிக் கொண்டது. அங்கே
சிக்கித் தவித்த குழந்தையைக் காப்பாற்றும்
போது ஏணி முறிந்து, கீழே விழுந்து முட~
மானான். அவனுடைய பரோபகாரச் செயலைப்
போற்ற வேண்டும்" என்று கூறினார்.</p>

<p>-42-</p>

<p>            பண்பான பையன்</p>

<p>     தெருவில் ஒரு சிறுவன், அவனைக் காட்~
டிலும் பெரியவனை அடிக்க முற்பட்டான்.
அடிகள் தன்மீது விழாமல் தடுத்துக் கொண்~
டிருந்தான் பெரிய பையன். ஆனால், சிறுவனை
அவன் ஒரு அடிகூட அடிக்கவில்லை.</p>

<p>     அதைக் கவனித்த ஒருவன், பெரிய பைய~
னைப் பார்த்து, "அவன் யார்? உன் தம்பியா?"
என்று கேட்டான்.</p>

<p>     "அடுத்த வீட்டுப் பையன்!" என்றான்
பெரிய பையன்.</p>

<p>     "சிறுவன் முரடனாக இருக்கிறானே. நீ
ஏன் அவனை அடித்து விரட்டவில்லை?"
என்றான் வழிப்போக்கன்.</p>

<p>     "அவன் என்னைக் காட்டிலும் சிறியவனா~
யிருப்஼பதால், அவனை அடிக்க மனம் வர~
வில்லை" என்றான் பெரிய பையன்.</p>

<p>-43-</p>

<p>       ஒன்று கூடி இருப்பதே பெருமை</p>

<p>     ஒருமுறை கையில் உள்ள ஐந்து விரல்களுக்~
கிடையே யார் முக்கியம் என்஼ற பிரச்னை
எழுந்தது.</p>

<p>     ஒரு விரல், "நான்தான் பெரியவன்" என்று
இறுமாப்பு அடைந்தது.</p>

<p>     அடுத்தவிரல், "சுட்டிக் காட்டுவதுதான்
முக்கியம். என்னைக் கொண்டே சுட்டிக்
காட்டுவதால், நானே பெருமைக்கு உரியவன்"
என்று கூறியது.</p>

<p>     நடுவிரலுக்கு மிகவும் கோபம், "எல்லோரும்
எழுந்து நில்லுங்கள். பார்ப்போம். யார்
பெரியவன்? நானே அல்லவா?" என்றது.</p>

<p>     நான்காம் விரல் மிக அமைதியாக, "உங்~
களில் யாருக்கும் இல்லாத பெருமை எனக்கே
உண்டு. மோதிர விரல் என்று என்னை
அழைக்கவில்லையா?" என்று கூறியது.</p>

<p>     அடுத்து சுண்டுவிரல், "வணக்கம் என்று
கூறி எழுந்து ஒருவரை, அல்லது கடவுளையே
வணங்கும்போது நான்தான் முதலில் நிற்கி~
றேன். எனக்குப் பின்னே அல்லவா நீங்கள்
எல்லோரும் நிற்கிறீர்கள். ஆகவே பெருமைக்கு
உரியவன் நான் அல்லவா?" என்றது.</p>

<p>     சர்ச்சை தீரவில்லை. அப்பொழுது ஒருவர்
லட்டு லட்டு என்றுகூறி விற்றுக் கொண்டிருந்~
தான். கையிலுள்ள விரல்கள் அனைத்தும்
ஒன்று சேர்ந்து, அவனிடம் லட்டை வாங்கிக்
கொண்டன.</p>

<p>-44-</p>

<p>         ஒரு மொட்டை இலவசம்</p>

<p>     திருப்பதிக்குப் போய், இரண்டு பிள்ளை~
களுக்கும் மொட்டை போட்டு வருமாறு,
கணவனையும் பிள்ளைகளையும் அனுப்பி
வைத்தாள் மனைவி. வேறு செலவு எதையும்
வீணாகச் செய்ய வேண்டாம் என்றும் எச்சரித்~
தாள் மனைவி.</p>

<p>     திருப்பதி சென்றான் அவன். இரண்டு
பிள்ளைகளையும் சவரத் தொழிலாளியிடம்
உ஠்கார வைத்தான். மொட்டை அடித்து
விட்டான் அவன். "நீயும் வந்து உட்கார்!
உனக்கும் மொட்டை போட்டு விடுகிறேன்"
என்றான் சவரத் தொழிலாளி.</p>

<p>     "ஐயோ! எனக்கு வேண்டாம். என்னிடம்
அ஥ற்குக் காசு இல்லை." என்றான் அவன்.</p>

<p>     "இரண்டு மொட்டை போட்டால், ஒரு
மொட்டை இலவசம்!" என்றான் சவரத்
தொழிலாளி.</p>

<p>     "இலவசம்" என்றவுடன், அவன்
உட்கார்ந்து மொட்டை போட்டுக்கொண்டான்.</p>

<p>     வீட்டுக்குத் திரும்பினர். மூன்று மொட்டை~
களைக் கண்டாள் மனைவி கோபம் அடைந்த
மனைவி, "நீங்கள் ஏன் மொட்டை போட்டுக்
கொண்டீர்கள்? ஒரு ரூபாய் நஶ்டம் ஆயிற்றே!"
என்று கத்தினாள்.</p>

<p>     அ஥ற்கு, "இரண்டு மொட்டைக்கு, ஒரு
மொட்டை இலவசம்!" என்றான் ஖ணவன்.
    </p>

<p>               0 
</p></body></text></cesDoc>