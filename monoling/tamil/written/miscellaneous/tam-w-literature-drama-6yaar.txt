<cesDoc id="tam-w-literature-drama-6yaar" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-literature-drama-6yaar.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-04</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>பாரதியார் இல்லற நாடகம்</h.title>
<h.author>ஆ. ஜி. ரங்கநாயகி</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1981</pubDate>
</imprint>
<idno type="CIIL code">6yaar</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 17.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-04</date></creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>-1-</p>

<p>		பாரதியார் இல்லற நாடகம்</p>

<p>		1. ஜாதி மதங்களைப் பாரோம்</p>

<p>பாத்திரங்கள் : பாரதியார், சி. ஐ. டி. போலீஸ்காரர்கள்,
		ரங்கா, (ரங்கநாயகி), ஆண்டாள் (ஶ்ரீ. ஶ்ரீ.
		ஆச்சாரியாவின் இரண்டாவது புதல்வி;
		ரங்காவின் தமக்கை), செல்லம்மாள், (பாரதியார்
		மனைவி); சகுந்தலா (பாரதியாரின் இரண்டா~
		வது பெண்). அப்பாஸாமி (ஸி. ஐ. டி.)</p>

<p>	இடம் : பாரதியாரது வீடு.</p>

<p>		[புதுவையில் இந்திய தேச பக்தர்களான
	`சுதேசி'களின் வீடுகளின் வாசலில் எப்போதும் வேவு~
	காரர்கள் - சி. ஐ. டி. போலீஸார்-காவல் காத்திருப்பது
	வழக்கம்.</p>

<p>		பாரதியாரும் அவரை நிழல் போலத் தொடரும்
	வேவுகாரனும் வெளியில் சுற்றிவிட்டு வீட்டுக்கு வருகிறார்~
	கள். அப்பொழுது, ரங்கா, ஆண்டாள், சகுந்தலா
	முதலியோர் பொம்மை கலியாணம் விளையாட்டு
	விளையாடிக் கொண்டிருக்கிறார்கள். செல்லம்மா
	வேடிக்கை பார்த்துக் கொண்டிருக்கிறார். சி. ஐ. டி.
	வாசலில் நிற்கிறான். பாரதி உள்ளவே வருகிறார்.]
-2-</p>

<p>	செல்லம்மா (சி. ஐ. டி யைப் பார்த்து): இது எ஁ன்ன,
எப்பொழுது பார்த்தாலும் நீங்கள் ஜெயில் கைதி மாதிரி
அவன் உங்கள் பின்னாலேயே சுற்றிக் கொண்டு வருகிறானே?</p>

<p>	பாரதி: மடியில் கனமிருந்தால்தானே வழியில் ப௟ம்?
நான் ஒன்றும் திருடிவிடவில்லை. எல்லாருக்கும் உண்டானது
நமக்கும் வந்திருக்கிறது. அதில் என்ன அவமானம்? பாவம்
அவன் ரொம்ப நல்லவன். சர்க்கார் சம்பளம் கொடுத்து
வேலைக்கு வைத்திருக்கிறபடியால், அவன் தன் வேலையைச்
சரிவரச் செய்கிறான். அதில் என்ன தப்பு?</p>

<p>	செல்: சரி, உங்களோடு பேச எனக்குத் தெரியாது.
தண்ணி எடுத்துக் கொட்ட அந்தப் பர்வதமும் வரவில்லை.
நானே போய் தண்ணீர் எடுத்து வருகிறேன்.</p>

<p>	பாரதி: (அன்புடன்) செல்லம்மா, நீயேன் சிரமப்பட
வேண்டும்? வாசலில் வெட்டிக்கு கிடக்கிறானே, அவனைக்
கூப்பிட்டு தண்ணியைக் கொட்டும்படி சொல்கிறேன். நீ
அவனுக்கு கொஞ்சம் சோறு போட்டு விடு. நீ காலால்
இட்ட வேலையைத் தலையால் செய்வான் அவன். என்
பின்னாலேயே சுற்றிக் கொண்டிருப்பதால் வேளா வேளைக்குச்
சாப்பாடு கிடைப்பதில்லை. பாவம், ரொம்ப திண்டாடுகிறான்.
(வெளியே போய்) டேய், அப்பாஸாமீ! அம்மாவுக்குத்
தண்ணி கொணாந்து அண்டா நிரப்ப வேணுமாம், கிணற்றி~
லிருந்து மொண்டு எடுத்து வா!</p>

<p>	அப்பாஸாமி (வினையத்துடன்) : ஆகட்டும் சாமீ! [உள்ளே
போய்க் குடத்தை எடுத்஼துக் கொண்டு போகிறான்.]</p>

<p>	செல்லம்மா : அவன் என்ன ஜாதியோ என்னமோ!
அதைக் கேட்காமலே தண்ணி எடுத்துக் கொண்டு வரச்~
சொல்லி விட்டீர்களே!</p>

<p>	பாரதி : நான் சொல்வதைக் கேள். அவ஼ன் எந்த
ஜாதியானால் என்ன? நான் அவன் கூட உட்கார்ந்துதான்
-3-
சாப்பிடப் போகிறேன். அப்பளம் ஏதாவது சுட்டு வைத்திருக்~
கிறாயா? சோதனை போட்ட தினம் எல்லா அப்பளத்தையும்
அந்த அய்யங்காரே சாப்பிட்டு விட்டார். அதனால் இன்னிக்~
காவது செய்தால் சாப்பிடலாம்.</p>

<p>	செல்லம்மா : சரி, இது வேறு கூத்தா! ரங்கா,
ஆண்டாள், கேட்டால் சிரிக்க மாட்டார்களா? அவாத்து
சமையலறைக்கு நம்மையே சேர்க்க மாட்டார்களே! நான்
அவா அம்மாவிடம் கூட வேடிக்கையாகச் சொல்வதுண்டு,
"பரமாத்மாவையாவது பார்க்கலாம் போல் இருக்கு,
உங்காத்துச் சமையலறையைப் பார்க்க முடியாது போலி~
ருக்கே!" என்பேன். அவாளும் கூட சிரித்துக் கொண்டே
சமயலறைக் கதவைச் சாத்துவார்கள்.</p>

<p>	பாரதி : ஏன் சிரிக்கிறார்கள்? அது அவர்கள் வீட்டு
வழக்கம். பெரிய பெரிய ஜமீன்தார்களுக்கு, மெய்காப்பாளனாக
ஒருவனுக்கு நிறைய சம்பளம் கொடுத்து வேலைக்கு வைத்துக்
கொள்வது வழக்கம். சர்க்காரில் பெரியி஼஁஁ உத்தியோகத்தில்
இருப்பவர்கள் சிலருக்கு இரண்டு ஆள் வீட்டு வேலைக்காக~
வும் வேறு இரண்டு ஆட்கள் ஆபீஸ் வேலைக்காகவும்
கொடுப்பது வழக்கம். அதைப்போல நாமும் நினைத்து
தானாக வந்ததை நல்லதாக ஏற்று நம்ம காரியத்திற்கு ஏன்
உபயோகப்படுத்திக் கொள்ளக் கூடாது?</p>

<p>	செல்லம்மா : என்னமோ நீங்கள் சொல்வது உங்களுக்குத்
தான் சரியே தவிர, எனக்கு சற்று சரிபடவில்லை. அவன்
எந்த ஜாதி என்பதைக் கேட்டிருந்தால் நன்றாய் இருக்கும்.
-4-</p>

<p>	செ : சிவ, சிவ, என் காதால் கேட்கவே படவில்லை
எங்கம்மா யாராவது வந்தால் என்ன நினைத்துக் கொள்~
வார்கள்! [அப்பாஸாமி வருகிறான்]</p>

<p>	பாரதி : இவனுக்கு இன்னும் ஏதாவது வேலை இருக்கிற~
தாச் செல்லம்மா? வீடு எல்லாம் நன்றாய்ப்஼ பெருக்கிவிடு,
அப்பாஸாமி, பிற்பாடு, நாம் சாப்பிடலாம். [அப்பாஸாமி
வீடு எல்லாம் பெருக்கிவிட்டு நிற்கிறான்]</p>

<p>	செ (மாடியிலிருந்து சில்லறை கொடுத்து) : கடைக்குப்
போய் வெற்றிலை பாக்கு வாழைப் பழம் எல்லாம் வாங்கிக்
கொண்டுவா.</p>

<p>	அப்: ஆகட்டும் அம்மா. [சில்லறையைக் கைநீட்டி
வாங்கிக் கொண்டு, வெளியேறுகிறான்]</p>

<p>	சகுந்தலா, ரங்கா (செல்லம்மாவைப் பார்த்து) : நாங்களும்
அவனுடன் போய் வருகிறோம்.</p>

<p>	செல்: இருவரும் போய் வாருங்கள், ஆண்டாள்
இங்கேயே இருக்கட்டும்.</p>

<p>	[சிறுமிகள் இருவரும் அப்பாஸாமியோடு போகிறார்~
கள்.]</p>

<p>	சகுந்தலா: அப்பாஸாமீ, நீ அப்பாவோடு எங்கே
போயிருந்தாய்?</p>

<p>	அப்: எங்கோ கொசவர்பாளயம் என்கிற இடத்திற்கு
உன் அப்பாவோடு போனோம். அங்கே பொம்மைகள் செய்~
வதை எல்லாம் பார்த்துக் கொண்டு வந்தோம். அதற்குள்
விளக்கேற்றும் வேளையாய் விட்டது. அது ரொம்பத்தொலை~
விலே இருக்குதம்மா, பாப்பா!</p>

<p>	சகு: எங்களையும் ஒருநாள் அங்கே அழைத்துப்
போகிறாயா?</p>

<p>	அப்: நான் எப்படியம்மா, அழைத்துப் போக முடியும்?
நான் சர்க்கார் வேலை செய்யும் அடிமை தானே?
		[வ஼ழியில் இன்னொரு ஸி. ஐ. டி. வருகிறான்.]
-5-</p>

<p>	ஸி. ஐ. டி: ஏண்டா! எங்கே போறே, இந்த குழந்தை~
களை கூட்டிக் கொண்டு.</p>

<p>	அப்: கடைக்குப் போய் வெத்திலைப் பாக்கு வாங்கி வரப்
போகிறேன். மூணு நாளாக வெத்திலைப் பாக்கு போடவே~
யில்லை. சும்மா இவர்களையும் கூட்டிக் கொண்டு வந்தேன்.</p>

<p>	ஸி. ஐ. டி: இப்போ இவங்க அப்பன் மார்கள் என்ன
செய்து கொண்டிருக்கிறார்கள்? வீட்டிலா, இல்லை வெளியே
கிளியே போய் இருக்கிறாங்களா?</p>

<p>	அப்: நான் இந்தப் பாப்பா வீட்டில் தான் காவலாக
இருக்கிறேன். இவங்க அப்பா எப்போது பார்த்தாலும் நட~
மாடிக஼் கொண்டு. `சக்தி, சக்தி,' என்று சொல்லிக் கொண்டே
இருக்கிறாரே தவிர, வேறு ஒரு துப்பும் அவரிடம் கிடைக்க~
வில்லை. இப்போ அவர், வீட்டில் தான் இருக்கிறார்.</p>

<p>	ஸி. ஐ. டி: ஆமாம் நடேச௟ன், சின்னன், இருக்கிற வீடு~
களுக்குப் போய் ஏதாவது பேசிக் கொள்ளுகிறார்களா?</p>

<p>	அப்: அவங்க இரண்டு பேர் இருக்கிற வீட்டுக்குத்
தப்பாமே நான் இருக்கிற வீட்டு ஐயா போய் வராங்க. அங்கே~
யும் ஏதோ கலகலப்பான சிரிப்பும், பாட்டுமாய் பேசிக் கொள்ளு~
கிறார்கள். இல்லா விட்டால் ஏதோ செய்யுள் தேவாரங்கள்
முதலியதுகளைப் பேசி எழுத உட்கார்ந்து விடுகிறார்கள்.
நேரம் போவதே அவர்களுக்குத் தெரியவில்லை. நம்
சர்க்காரைப் பற்றி ஒரு பேச்சையும் காணோம்!</p>

<p>	ஸி. ஐ. டி: அன்னிக்கு சோதனை போட்ட தினம் எல்லாம்
சரியாய்ப் பார்த்தீர்களா, இல்லையா?</p>

<p>	அப்: நான் இருக்கிற வீட்டில் ஒன்றும் கிடைக்கவில்லை.
எங்கே பார்த்தாலும் புஸ்தகங்கள் தான். சின்னன் இருக்கிற
வீட்டிலும் அவ்வளவு தானாம். ஒரு இருட்டு உள் நிறைய
பெரிய அஞ்சு அடுக்கு ஷெல்புகளில் ஒரு முழம் அளவில்
-6-
பெரிய பெரிய புஸ்தகங்கள் ஒரு மாதிரி இருந்தபடியால் அந்த
உள்ளுக்கே கீழே விழுந்து நமஸ்காரம் செய்து விட்டு
ஐயன் மார்களுக்கு கும்பிடு போட்டு விட்டு வெளியே வந்து
விட்டானாம். நடேசன் இருக்கிற வீட்டில் சோதனை போட்ட
போது கிணத்தில் ஏதோ இரண்டு ஆயுதங்கள் கிடைத்த~
தாய்ச் சொல்லிக் கொண்டார்கள். எது நிஜமோ. எது
பொய்யோ, நமக்குத் தெரியாதுங்க?</p>

<p>	சகு: அப்பாஸாமி, போகலாமா, போதாய் விட்டதோ?</p>

<p>	ரங்கா: ஆமாம், நாங்களும் வீட்டிற்குப்஼போக வேணும்.
அம்மா தனியாக இருப்பாள்.</p>

<p>		[எல்லாரும் பாரதி வீட்டிற்கு திரும்பி வருகிறார்கள்.
	உள்ளே போகிறார்கள், அப்பாஸாமி செல்லம்மாவிடம்
	வந்து பழம் வெற்றிலை பாக்குகளை கொடுக்கிறான்.]</p>

<p>	பாரதி: (அதைப்பார்த்து) எதற்காகச் செல்லம்மா
இதுகளை வாங்கி வரச்சொன்னாய்?</p>

<p>	செல்: நம்ம பாப்பா பொம்மைக் கல்யாணம்
பண்ணினாள். அதற்குப் பெண் வீட்டாருக்கு வெற்றிலை
பாக்கு மரியாதை செய்ய வேணும் என்று சொன்னாள்.
அதனால் வாங்கி வரச்சொன்னேன்.</p>

<p>	சகு: அப்பா! ரங்காவின்் பெண்ணை அழைத்துக்
கொண்டு போவதாய்ச் சொல்கிறான். நான் அதை
இங்கேயே இருக்க வேணும் என்று சொல்லுகிறேன்.
அவளுக்கு இஷ்டமில்லை. நீயே இதற்கு சமாதானம்
சொல்லப்பா!</p>

<p>	பாரதி: நீ பிள்ளை வீட்டுக்காரியா? ஆனால் நான்
சொல்வதைக் கேள். தங்கக் கிளியின் (ரங்காவுக்கு பாரதி
-7-
இட்ட பெயர்) இஷ்டப்படி கொஞ்சம் நாள் பெண்
அவளிடமே இருக்கட்டும். பெண்களுக்கும் சுதந்திரமுண்டு.
கல்யாணம் செய்஼து விட்டால் கணவன் வீட்டிலேயே
எப்போதும் இருக்க வேணும் என்கிற கட்டுப்பாட்டை
நிறுத்தி விடவேணும். அவர்களுக்குத் தானாகவே உத்~
சாகம் வந்ததும் வரலாம் புருஷர்களுக்கும் பெண்களுக்~
கும் சரி சமனாக நடக்கும் நாள் வந்தால் தான் நம் இந்தியா
நல்ல நிலைக்கு வரும்.</p>

<p>		[சகுந்தலா, பொம்மையை ரங்காவிடம் கொடுத்து
	விடுகிறாள். ரங்கா அதை வெகு ஸந்தோஷமாக வாங்கிக்
	கொண்டு பாரதியாரை நன்றியறிதலோடு பார்த்து]</p>

<p>	ரங்கா: நீர் சொன்னபடி பாப்பா, பொம்மையைக்
கொடுத்து விட்டாள், பாரும்</p>

<p>	பாரதி (சகுந்தலாவைப் பார்த்து) : கல்யாணம் செய்~
தால் எல்லோரும் சந்தோஷமாய் இருக்க வேணும்
அல்லவா? இப்போது பார், ரங்காவை எவ்வளவு சந்~
தோஷப் படுகிறாள். (ரங்கா ஆண்டாளைப் பார்த்து)
இங்கேயே நீங்களும் சாப்பிடுங்களம்மா?</p>

<p>	ஆண்டாள்: இல்லை, நாங்கள் போக வேணும். அம்மா
வீட்டில் தனியாகவே இருப்பாள். ஐயா அரவிந்த் கோஷ்
வீட்டிலிருப்பார். இன்னொரு நாள் வருகிறோம்.</p>

<p>		(சகுந்தலா இருவருக்கும் மஞ்சள் குங்குமம் பழம்
	வெற்றிலை பாக்கு எல்லாம் கொடுக்கிறாள். அதை
	வாங்கிக் கொண்டு வேலைக்காரப்பையனுடன் அவர்கள்
	வெளியே போகிறார்கள்.]</p>

<p>			திரை.
-8-</p>

<p>		2. இந்த ஜன்மத்தில் விடுதலை</p>

<p>பாத்திரங்கள் : ஶ்ரீ.ஶ்ரீ. ஆச்சாரியார், அவருடைய தமையனார்
		ஶ்ரீ திருமலாச்சாரியார் அவர்களுடைய தம்பி
		பார்த்தசாரதி, வ. வே. சு. ஐயர், பாரதியார்,
		ரங்கா, ஆண்டாள், சகுந்தலா.</p>

<p>
	இடம்:	ஶ்ரீ.ஶ்ரீ. ஆச்சாரியாருடைய வீடு. மாடிவெளி
		முற்றத்தில் உலாவியபடியே திருமலாச்சாரி~
		யும் பாரதியாரும் பேசுகிறார்கள். குழந்தைகள்
		விளையாடிக் கொண்டிருக்கின்றன.</p>

<p>	திருமலாச்சாரி: என்ன பாரதியார்வாள், அந்த
சுப்பிரமணிய சிவாவை ஜெயிலில் வைத்து கடின ஸஜா
செய்துவிட்டார்களே! என்ன அநியாயம், பாருங்கள்.</p>

<p>	பார஼தி: பாவம் ரொம்ப கஷ்டம்! நான் அப்போதே
சொன்னேன் புதுவைக்கு வ஼ந்துவிடும்படி. கேட்காமல்,
ஏதேதோ விஷயங்களிளெல்லாம் தலையிட்டு ஜெயிலில்
போய்ச் சேர்ந்து விட்டார்.</p>

<p>	திரு: நீங்கள் பேசுவது வெகு நன்றாய் இருக்கிறதே!
இங்கேயும் சிறைவாசம் போலத்தானே இருக்கிறது! அங்கே
சிறையில் இருந்தால் சாப்பாடாவது தள்ளும், இங்கு
வந்தால் யார் கொடுப்பார்கள்? ஏதாவது சொந்தப் பணம்
இருந்தால் செலவு செய்து கொண்டு இருக்கலாமாய்
இருந்தது. மற்றும் ஜெயிலிலாவது இவ்வளவு வருஷம்
`வஜா' என்று கண஗்குடன் சேர்ந்து பிறகு விடுதலை செய்~
கிறார்கள்; இங்கே இந்த ஜன்மத்தில் விடுதலை உண்டோ
இல்லையோ, யாருக்குத் தெரியும் ? சொல்லும் பார்ப்போம்.
-9-</p>

<p>	பார்த்த ஸாரதி: நம்ம பாரதியார் நல்லது சொன்னால்
நிஜமாய் விடுமே. அவரும் சுப்பிரமணிய சிவாவுக்கு
எவ்வளவோ புத்தி புகட்டினார். இவரிடம் பணமிருந்தால்
ஜாமீன் மீதாவது விடுதலை செய்யலாம். அதுவும் இல்லை.
ஒன்றுமே பிரயோஜனம் இல்லாதபடி ஆகிவிட்டது.</p>

<p>	திரு: இது என்ன பேச்சு? இங்கே இவர்களே
வயிற்றுக்குத் திண்டாடும் போது சிவாவுக்கு எப்படி ஜாமீன்
கொடுத்து உதவ முடியும்? (பாரதியைப் பார்த்து) நீங்கள்
பணத்திற்க஼ு இப்போது என்ன செய்கிறீர்கள் ?</p>

<p>	பாரதி: எல்லாம் பராசக்தியின் அருள். இப்படியே
நடந்து கொண்டு வருகிறது. பாரும், அண்ணா!</p>

<p>	திரு: அந்த அருள் இருந்தால் இது வேளைக்கு நாம்
இவ்வளவு கஷ்டப் படத் தேவையில்லையே! அதனால் தானே
கேட்டேன். நம்முடைய பக்கம் எப்போது ஜயம் வரும்?
எப்போது விடுதலை கிடைக்கும் என்று மனதில் பயப்பட
வேண்டி இருக்கிற தல்லவா?</p>

<p>	பாரதி: (பாடுகிறார்);
-10-</p>

<p>	திரு: அது என்னமோ, சரிதான், ஆங்கிலேயர்
சண்டைக்கு வந்தால் நம்மிடம் என்ன ஆயுதம் இருக்கிறது?
ஒன்றுமே இல்லையே! அதனால்தான் நாம் அவர்களுக்கு
அடிமையாக இருக்க வேண்டியிருக்கிறது.</p>

<p>	பாரதி: (பாடுகிறார், தன் தோள்களைத் தட்டிக்கொண்டு)</p>

<p>	திரு: உம்முடைய தோள் பலம் இருக்கும் வரை
சண்டை போடலாம். பிற்பாடு? உம்மை நம்பி இருப்பவர்~
களெல்லாரும் துன்பக் கடலில் முழுகிப்போய் விடுவார்களே,
அப்போது யார் காப்பாற்஼றுவார்கள்?</p>

<p>	பாரதி: (கைகளை மேலே தூக்கினபடி பாடுகிறார்):</p>

<p>		[அப்படியே கீழே சாய்ந்து விடுகிறார். பார்த்தஸாரதி~
	யும் ஐயரும் வேகமாய் ஓடிப்போய் அவரைத் தாங்கிக்
	கொள்ளுகிறார்கள். சிறுமிகளும் பெண்களும் கூடி
	நிறைந்து விடுகிறார்கள்.]</p>

<p>	ஶ்ரீ.ஶ்ரீ: ரங்கா! சொம்பிலிருக்கும் ஜலத்தைக் கொண்டு
வா!</p>

<p>		[ரங்கா சொம்பு ஜலத்தோடு வருகிறான். பாரதிக்குத்
	தலையில் கொஞ்சம் ஜலத்தை தட்டி, வாயிலும் விடுகிறார்
	ஶ்ரீ.ஶ்ரீ. ஆசாரியர். பாரதி மெள்ளக் கண் திறக்கிறார்]
-11-</p>

<p>	பாரதி: எனக்கு என்ன வந்து விட்டது? இவ்வளவு
பேர் என்னை சுற்றிக் கொண்டிருக்கிறீர்களே?</p>

<p>	ஐயர்: ஒன்றுமில்லை. பேசாமல் சற்று நேரம் சும்மா
இரும். உடம்பை அலட்டிக் கொள்ள வேண்டாம்.</p>

<p>	பார்த்த: கொஞ்சம் பால் சாப்பிட்டால் களைப்பு நீங்கும்
ஆண்டாள், கீழே போய் ஒரு லோட்டாவில் பால் கேட்டு
வாங்கி வாம்மா?</p>

<p>		[ஆண்டாள் சென்று பால் வாங்கி வருகிறாள்.
	அதை வாங்கி பாரதிக்கு கொடுக்கிறார்.]</p>

<p>	திரு: இந்தப் பாட்டு பாடியதற்கே களைத்து விட்டீரே
சண்டைக்கு எப்படி நிற்பீர்?</p>

<p>	ஐயர்: திருமலாச் சாரியாரே, சற்று நேரம் பேசாமல்
இருக்க மாட்டீர்களா?</p>

<p>	பாரதி (திருமலாச்சாரியைப் பார்த்து): நீர் சொல்லியது
வாஸ்தவம்தான். ஆனால் பராசக்திக்கு சண்டை பிடிக்க~
வில்லையாம். ஆங்கிலேயர்களுடன் சிநேக பாவத்துடனேயே
நம் இந்தியாவை விடுவித்துக் கொள்ள வேணும் என்று
என்று இச்சையாக இருக்கிறது போலத் தோன்றுகிறது.</p>

<p>	திரு: அது எப்படி முடியும்? நமக்கு வேண்டியவர்களை
எல்லாம் ஹிம்சை செய்து கொண்டு வருகிறானே!</p>

<p>	பாரதி: அவர்களைப் போல நாம் நயவஞ்சகம் செய்யாமல்
அந்தரங்கப் பிரியத்துடன் நமக்குச் சேரவேண்டியதைக்
கேட்டு வாங்கிக் கொண்டு இருபக்கத்தாரும் ஒத்துழைக்~
கலாமே (பாடுகிறார்)
-12-</p>

<p>	பார்த்த: நீர் சொன்னபடியே நடந்தால் வெகு
ஸந்தோஷமானதே. ஆனால் தயவுசெய்து இப்போது பாட
வேண்டாம். சற்று விச்ராந்தியாகப் படுத்துக் கொள்ளும்.
(திருமலாச்சாரியைப் பார்த்து) அண்ணா, இப்போது
அவருடன் சர்ச்சை செய்யாதிரும். நாளைக்கு உம்முடைய
சந்தேகத்தை, நிவர்த்தி செய்து கொள்ளுவீர்களாம்.</p>

<p>		[பாரதி பெஞ்சில் படுத்துக் கொண்டிருக்கிறார்.
	சத்தம் செய்யாமல் சிறுமியர்கள் மூவரும் பெண்க஼ளும்
	கீழேபோய் விடுகிறார்கள். புருஷர்கள் எல்லாரும் அவர்
	அருகிலேயே இருக்கிறார்கள்.]</p>

<p>			[திரை]</p>

<p>
		3. எல்லா உயிரிலும் நானே</p>

<p>பாத்திரங்கள்:	பாரதியார், ரங்கா, சகுந்தலா, செல்லம்மா,
		புஷ் வண்டிக்காரன், கோவில் வேலைக்~
		காரன், பிச்சைக்காரன் கிழ஼வன் காலைவேளை.</p>

<p>		[பாரதி, ரங்கா, சகுந்தலா மூவரும் தெருவில்
	நடந்துகொண்டு வருகிறார்கள். கடற்கரையில் சூர்~
	யோதய தரிசனம் செய்துவிட்டு வீடு திரும்புகிறார்கள்.
	அப்போது புஷ்வண்டிக்காரன் பாரதியைப் பார்த்து
	ஓடிவருகிறான்.]</p>

<p>	புஷ் வண்டிக்காரன்: சாமீ, அய்யரே! நம்ம வண்டியில்
ஏறுங்கோ! குழந்தைகள் கால் கடுக்க நடக்கிறார்களே!
நீங்களும் எவ்வளவு தொல்லை நடந்து வரிங்க, சாமீ!</p>

<p>	பாரதி - வேண்டாமடா, உனக்கு கூலி தரவேண்டாமா!
வேறு ஏதாவது நல்ல கிராக்கி பார்த்துக் கொள், போடா!
-13-</p>

<p>	புஷ் வண் - இல்லை, சாமீ! நீங்கள் பணம் கொடுக்காததும்
ஒன்றே, இன்னொருவர் பணம் கொடுப்பதும் ஒன்று தான்
சாமி, நான் ஏழை, ஆனாலும் அவ்வளவு பேராசை கிடையா~
துங்க. என் தம்பி சர்க்கார் வேலை செய்து வருகிறான்.
ஆனாலும் தனக்கு வரும் பணம் போதவில்லை என்று
என்னிடம் இருப்பதை சுரண்டிக் கொண்டு போகிறான். அது
கிடக்கிறது, ஜல்தி ஏறுங்க, சாமீ.</p>

<p>	பாரதி - போகட்டும், எங்கள் வீட்டுக்குப் போக உனக்கு
என்ன வட்டிச் சத்தம் கொடுக்க வேணும்?</p>

<p>	புஷ்: நாலு அணா, கொடுங்கள், ஐயரே போதும், காலை
நாஸ்தாவுக்குக் காணும்.</p>

<p>	[பாரதி, ரங்கா, சகுந்தலா, மூவரும் வண்டியில் உட்காரு~
கிறார்கள். வண்டிக்காரன் வண்டியை இழுத்துகொண்டு
போகிறான்]</p>

<p>	சகுந்: அப்பா சர்க்காரில் வேலை என்றால் பணம்
நிறையக்கிடைக்குமா, என்ன?</p>

<p>	பாரதி: ஆமாம்: மாதா மாதம் தப்பாமல் ஒரே நிலையாக
வரும் பணம். ஆனால் .......</p>

<p>	சகுந்: அப்படியானால் எல்லோருமே அந்த வேலைக்குச்
சேரலாமே?</p>

<p>	பாரதி: பணம் வரும்! ஆனால் அதிலும் எவ்வளவோ
கஷ்டம் இருக்கிற஥ு. நான் எழுதிய "பகதூர்" என்னும்
நாயின் கதையை படித்தாயானால் எல்லாம் விளக்கிக் காட்டும்.
சர்க்கார் வேலையிலிருந்தால் நம் சுதந்திர வாழ்வு இல்லாமல்
எப்போதும் கருடனைக் கண்ட பாம்பு போல் நடுங்கிச்
சாகவேண்டும். இப்போது நம் இஷ்டப்படி நடக்கலாம்
-14-</p>

<p>		[பாரதியார் வீட்டு வாசலில் வண்டி நிற்கிறது.
	எல்லாரும் கீழே இறங்குகிறார்கள். செல்லம்மா வாசலில்
	கோலம் போட்டு விட்டு ஈசுவரன் கோவில் வேலை~
	யாளிடம் பேசிக்கொண்டிருக்கிறாள்.]</p>

<p>	செல்: என்னப்பா, இன்றைக்குப் பச்சை மாவிலைத்
தோரணங்கள் எல்லாம் கோவிலுக்குக் கட்டியிருக்கிறதே!
என்ன விஷயம்?</p>

<p>	வேலையாள்: இன்றைக்குத் "திருவாதிரை" உங்~
களுக்குத் தெரியாதா?</p>

<p>	செல் : நாளைக்கு என்றல்லவா பஞ்சாங்கத்தில் போட்~
டிருக்கு! அதுவும் இல்லாமல், எங்க ஊர்களிலும் நாளைக்கு
என்று கடிதம் வந்திருக்கே!</p>

<p>	வேலை : நாளைக்கு நக்ஷத்திரம் கொஞ்சமே மிச்சமிருந்~
திருக்கும். இன்றைக்குத்தான் பூர்த்தி இருக்கிறபடியால்
இன்றே உத்ஸவம் நடக்கிறது.</p>

<p>	பாரதி: என்ன செல்லம்மா, என்னமோ அவனிடம்
கேட்டுக் கொண்டிருக்கிறாயே? எதைப்பற்றி?</p>

<p>	செல்: ஒன்றுமில்லை; எங்க ஊரில் "திருவாதிரை"
பண்டிகை நாளை என்று கடிதம் எழுதி இருக்கிறாள் அம்மா.
நானும் நாளைக்குப் பண்டிகை கொண்டாடலாம் என்று
இருக்கிறேன். இந்த ஊர் கோவிலில் இன்றைக்கு
உத்ஸவம் நடப்பதாய் கோவில் ஆள் சொல்லுகிறான்.
எப்படிச் செய்யலாம் என்று நினைத்தேன். அதற்குள் நீங்கள்
எல்லாரும் வந்துவிட்டீர்கள். நாம் நாளைக்கே கொண்டாட~
லாமா, பண்டிகையை?</p>

<p>	பாரதி: இங்கே கோவிலில் எப்போது கொண்டாடு~
கிறார்களோ, அப்போதே செய்தால் போச்சு. நமக்கு என்று
ஒரு நாள் வைத்துக்கொள்வார்களா? எல்லாம் 
-15-
தெய்வத்துக்குத்தானே செய்வது? ஔவையார் பாடியபடி "ஊருடன்
ஒத்துவாழ்" -என்று இன்றே செய்து விடு. (வண்டிக்~
காரனைப் பார்த்து) இந்தா ஒரு ரூபாய் எடுத்துக்கொண்டு
சந்தோஷமாய்ப் போய்வா?</p>

<p>	வண்டி (பல்லை இளித்துக் கொண்டே) : சில்லரை
செய்து வரட்டுமா சாமீ? கூலியைவிட ஜாஸ்தி கொடுத்~
திருக்கிறீர்களே?</p>

<p>	பாரதி: ஒன்றும் வேண்டாமடா. இன்னும் எப்போதா~
வது ஒரு நாள் ஒரு நடை எங்களை வண்டியில் அழைத்துக்
கொண்டு வந்து விட்டுவிடு. இப்போ அதைப்பற்றி ஒன்றும்
பேசாதே ஓடிப்போ!</p>

<p>		[வண்டிக்காரன் வெகு சந்தோஷத்துடன் அவருக்கு
	ஒரு கும்பிடி போட்டு விட்டுப் போகிறான்.]</p>

<p>	செல்: (உள்ளே வந்து பாரதியாரைப் பார்த்து) : அந்த
வண்டிக்காரனுக்கு முழுசாக ஒரு ரூபாய் தூக்கிக் கொடுத்து
விட்டீர்களே! வீட்டுச் செலவுக்கு, பண்டிகைக்கு எல்லாம்
வேண்டாமா? திருவாதிரைக் களி வேறு செய்யவேணுமே!</p>

<p>	பாரதி: கவலைப்பட வேண்டாம் செல்லம்மா? இந்தா,
ஐந்து ரூபாய். யாரோ ஒரு மஹானு பாவர் கொடுத்தார்.
அதில் வண்டிச்சத்தம் போக மீதியை உன்னிடம் கொடுத்து
விடுகிறேன். என்ன வேணுமோ செய்துகொள்.</p>

<p>		[செல்லம்மா அதை வாங்கிக்கொண்டு வீட்டிற்கு
வேண்டிய சாமான்களை வாங்கி சமையல் செய்துகொண்டி~
ருக்கிறாள். பாரதி முன் அறையில் உட்கார்ந்து ஏதோ
எழுதிக்கொண்டு இருக்கிறார். எதிரே கோயிலில் நடக்கும்
வைபவங்களை ரங்காவும் சகுந்தலாவும் பார்த்துக் கொண்டி~
ருக்கிறார்஖ள். பகல் 11 மணி வேளைக்கு யாரோ ஒரு
-16-
தொண்டு கிழமான பிச்சைக்காரன் பாரதியின் வீட்டு
வாசலில் வந்து பாடுகிறான்.]</p>

<p>	கிழவன்:
	சாமீ! வயிறு பசிக்குது! ஏதாவது போடுங்கள், ஐயன்~
மார்களே! அம்மணீ, தாயே!</p>

<p>	பாரதி (வெளியே வந்து பார்க்கிறார்) : சற்று இரு
உள்ளே போய்ப் பார்க்கிறேன். (உள்ளே சமயலறைக்குச்
சென்று) செல்லம்மா! சமையல் எல்லாம் ஆய் இருக்கிறதா?</p>

<p>	செல்: எல்லாம் ஆகிவிட்டது. நீங்கள் ஸ்னானம் செய்து
வந்து சுவாமிக்கு நைவேத்தியம் செய்து விட்டால்,
பேஷாய்ச் சாப்பிடலாமே. துணி எல்லாம் மடியாகவே வெந்~
நீர் அறையில் இருக்கு.</p>

<p>	பாரதி: செல்லம்! சுவாமிக்கு பிற்பாடு நைவேத்தியம்
செய்யலாம், வீதியில் யாரோ ஒரு தொண்டு கிழவன்
வந்து பிச்சை கேட்கிறான். மூன்று நாள் பட்டினிபோல்
துடிக்கிறான். ரொம்ப பசியாம். அதனால் நீ அவனுக்கு
வயிறு நிறைய சாப்பாடு போடு முதலில்.</p>

<p>	செல்: நன்றாய் இருக்கு! இவ்வளவு கஷ்டப்பட்டு
நாளும் கிழமையுமா, சுவாமிக்கு நைவேத்தியம் கூடச்
செய்யாமல் முதலில் பிச்சைக்காரனுக்கு எடுத்துச் சோற்றைப்
போடுவாளா?</p>

<p>	பாரதி: போட்டால் என்ன தப்பு, செல்லம்மா? ஒரு
சக்கிலி வீதியில் வருபவர்களை ஏசுகிறிஸ்துவாக பாவித்து
வழிப்பட்டானாம். பைபிளில் இந்த மாதிரி விஷயங்களைப்
படித்துப் பார்த்தால் தெரியும். நான் அரவிந்த கோஷ்
வீட்டில் படித்தேன்.
-17-</p>

<p>	செல்: அய்யோ! இன்னும் கிருஸ்துவமத வழக்கம் வேறு
செய்ய வேணுமா?</p>

<p>	பாரதி: இல்லை நம்முடைய வழக்கத்திலும் இந்த மாதிரி
உண்டு (பாடுகிறார்்)</p>

<p>	செல்: ஈச்வரா! இதுவே உனக்கு பிரியம் என்றால்,
இப்படியே செய்கிறேன். கூப்பிடுங்க கிழவனாரை,</p>

<p>	பாரதி: (வெளியே போய்) தாத்தா வா, கூடத்தில்
தண்ணி வைத்திருக்கு. கை கழுவிச் சாப்பிட உட்காரு.</p>

<p>		[செல்லம்மா கூடத்தில் இலை போட்டுக் கிழவனுக்கும்
	பரிமாறுகிறார். பாரதி அதைப் பார்த்து ஸந்தோஷிக்~
	கிறார்.]</p>

<p></p></body></text></cesDoc>