<cesDoc id="tam-w-science-film-kumar3" lang="tam">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>tam-w-science-film-kumar3.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-09</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>சினிமா கோட்பாடு</h.title>
<h.author>பேல பெலாஸ்</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1985</pubDate>
</imprint>
<idno type="CIIL code">kumar3</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 19.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-09</date></creation>
<langUsage>Tamil</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>பக்கம்:89
குரல்கள் மீது நவீன இ௟க்குநர்களுக்கு ஒருவித
வெறுப்பு ஏற்பட்டது. ஒரு தொழில் ரீதியான பாடகன்
அல்லது அது போன்ற கதாபாத்திரங்களுக்கு மட்டுமே அது
போன்ற குரல் தேவைப்பட்டது. மிகச் சிறந்த குரல் என்பது
இயற்கையாக தோன்றவில்லை. ஏதோ கலை நிகழ்ச்சி
போன்று இருந்ததே தவிர உண்஼மையான வாழ்க்கையை
வெளிப்படுத்துவதாக இல்லை. சாதாரண (பயிற்றுவிக்கப்~
படாத) குரலால் பாடப்பட்ட பாட்டானது அதிகமான
நெருக்க உணர்வையும் மனித உணர்வையும் தந்தது.</p>

<p>இயற்கை செயற்கை போல் தோன்றுகிறது</p>

<p>எதுவும் இயற்கையாக இருக்க வேண்டும் என்கின்ற போக்கு
அதிகரித்த உடன் இயக்குநர்கள் பெரும்பாலும் நடிப்பையே
தொழிலாகக் கொண்டவர்களை விட்டுவிட்டு தங்கள் கதாபாத்~
திரங்களுக்கு சாதாரண மனிதர்களைத் தெருவிலிருந்து தேர்ந்~
தெடுத்தார்கள். சாதாரண சிறிய கதாபாத்திரங்களாக
இருந்தால், அதற்கான நடிகர்களைச் சாலையில் உள்ள கும்பலி~
ருந்து தேர்ந்தெடுப்பது உண்மையிலே வசதியாக இருந்தது.
ஏனெனில், அவர்கள் ஒன்றும் பெரிதாக நடிக்க வேண்஼டிய~
தில்லை; வெறும் முகத்தை மட்டும் காட்டினாலே போது~
மானது. ஆனால் குறிப்பிட்ட கதாபாத்திரத்தைச் சரியானபடி
நடித்து, குறிப்பாக பேசவும் வேண்டுமெனில், எத்தனை மோச~
மான, பயிற்சியற்ற நடிப்பாக இருந்தாலும் சரி, சரியான பாவ~
யியலை உபயோகப்படுத்துவதன் மூலம் அந்த மோசமான
நடிப்பு மறைந்து விடுகிறது.</p>

<p>இ௟ற்கை கலையாக மாறியது</p>

<p>சினிமாவில் இயற்கையை அப்படியே உள்ளது உள்ளபடியாக
காண்பிக்க வேண்டும் என்று முயற்சி செய்தால் அது மிகவும்
ஆபத்தான வேலையாகும், இயற்கை வாதத்தின் தீவிரவாதி~
களாய் இருந்தவர்கள் (ஐஸன்ஸ்டைனும்), புரோங்கினும் ஒரு
காலத்தில் அப்படி இருந்தார்கள். தெருவிலிருந்து சாதாரண
மக்களை படபிடிப்பு தளத்திற்கு கொண்டு வந்து, அவர்கள்
இ௟ற்கையாக நடிக்க வேண்டும் என்று எதிர்பார்த்தார்கள்.
ஏனெனில், அவர்கள் நடிகர்களில்லை. ஆனால் அவர்கள் நடிகர்~
களாக இல்லாத ஒரே காரணத்தினாலேயே அந்த படபிடிப்புத்~
தளம் அவர்களுக்கு அந்நியமாய் பட்டது. அதனால் அவர்கள்
மிகவும் செயற்கையாக உணர்ந்தார்கள். இயற்கைவாதத்தின்
மீது கொண்ட அவர்கள் காதல் இத்தோடு நிற்கவில்லை. அவர்஼கள்</p>

<p>பக்கம்:90
தெருவில் சாதாரண மக்களின் இயற்கையான முகபாவங்~
களையும், அசைவுகளையும் காமிராவில் பதிவு செய்து அதை
தங்கள் படத்துக்கான கலாரீதியான காட்சியாக மாற்ற பார்த்~
தார்கள். இயற்கையாய் அவர்கள் எடுத்த பல்வேறு ஷாட்டு~
களை மோன்ட்டாஜ் மூலம் ஒன்றிணைத்து ஒரு கலையாக உரு~
வாக்கப் பார்த்தார்கள். தன் முன் நீட்டிய துப்பாக்கியைக்
கண்டு ஒரு பெண் அலற வேண்டுமெனில் அப்பெண்ணின்
நடிப்போ இயற்கையானதாக அவர்களின் தேவைக்கேற்ப
இல்லை. எனவே, உண்மையான ஒருத்தியைத் தேடிக்கொண்டு
அவர்கள் சாலையெங்கும் சென்றார்கள். ஒரு பெண் தன்
குழந்தையை வைத்து தள்ளிக்கொண்டு சென்ற தள்ளுவண்டி
திடீரென்று கவிழ்ந்து விட்டது. அதைக் கண்டு அவள் அலறு~
கிறாள். அவளுக்கு தெரியாமலேயே இதை      அவர்கள் படம்
பிடிக்கின்றனர். பின்னர் இயற்கையாக சுய உணர்வின்றி
பயந்து போன இந்த முகத்தை துப்பாக்கியைக் கண்டு
அலறுவதுபோல் அமைத்தார்கள்.</p>

<p>ஒரு குறிப்பிட்ட வழிமுறைக்கான உதாரணமாகத்தான்
மேற்கண்ட நிகழ்ச்சியை சொன்னேன். இந்த வழிமுறை
எப்போதுமே ஏமாற்று முறையாகும். நமது உ஠ல்பாவயியல்
கலாச்சாரம் என்று இன்னும் பல்வேறு பயங்களுக்கான பல்~
வேறு காரணங்களையும் பல்வேறு சிரிப்புக்கான பல்வேறு கார~
ணங்களையும் துல்லியமாக அறிந்து கொள்ளும் அளவுக்கு
வளரவில்லை. இதன் காரணமாகத்தான் மேற்சொன்ன ஂநிகழ்ச்சி
சாத்தியப்படக் கூடியது. க்ளோசப்பின் மூலம் முகபாவத்தின்
இயற்கை தன்மையை நம்மால் நன்கு அறிந்து கொள்ளமுடிந்~
தது. இந்த அறிந்துக் கொள்ளும் தன்மையானது இன்னும்
பின்னால் கூர்மையாக வளரக்கூடியது. அப்போது வெறும்
முகபாவத்தை மட்டும் அறிந்து கொள்ளாமல், அதற்கான
காரணங்களையும் அந்த முகபாவங்களின் தன்மையையும்,
நம்மால் அறிந்துகொள்ளமுடியும். இப்போதும் கூட நாம் அடிக்~
கடி முரண்பட்ட சூழ்நிலைகளைச் சந்திக்கிறோம். ஒரு நல்ல
நடிகனின் நல்லநடிப்பு என்பது மிகவும் இயற்கையாக தோன்~
றும். ஆனால் உண்மையாக நிகழ்ந்த ஒன்றை படம் பிடித்~
தாலே அது செயற்கைபோல தோன்றும். உதாரணத்திற்கு
திடீரென்று எடுக்கப்படும் ஓடும் குதிரை அல்லது மனிதனின்
ஷாட்.</p>

<p>சிலநேரங்களில், இயக்குநர்கள் கதைகளுக்கு அப்பாற்பட்ட
வழிமுறைகளை உபயோகித்து முகபாவங்களை வரவழைக்க
வேண்டியுள்ளது. குறிப்பாக குழந்தைகள் மற்றும் புராதன</p>

<p>பக்கம்:91
வினோத கதாபாத்திரங்களை இம்முறையில் கையாண்டார்கள்.
முகபாவங்கள் எந்த அளவுக்கு அந்நியப்பட்டதாகவும் வினோ~
தமானதாகவும் இருந்ததோ அந்த அளவுக்கு நம்பும்படியும்
இருந்தது. பாவங்கள் எப்போது போலி என்பதை அறிந்து
கொள்ளக்கூடிய அளவுக்கு நாம் சாலூஸ் அல்லது
சென்ஸ் ஆகியோரை அறிந்திருக்கவில்லை.</p>

<p>உடல் பாவயியல் பற்றிய கல்வி</p>

<p>திரைப்பட தயாரிப்புப் பற்றிய தன்னுடைய புத்தகத்தில்,
புரோங்கின் குறிப்பிட்ட கலாரீதியான அம்சத்தை அடைவதற்~
காக எப்படி இயற்கையான, தன்னிச்சையான உணர்வுகளை
எழுப்ப வேண்டியிருந்தது என்பது பற்றியும், அ஥ற்காகத்தான்
கையாண்ட வழிமுறைகள் பற்றியும் விவரமாக விவரித்துள்~
ளார். தன்னுடைய இதே புத்தகத்தில்஼தான் அவர் மேலும்
பின்வருமாறு கூறுகிறார். தன்னிச்சையான இயற்கையான
உணர்வை ஒரு கலைப்படைப்பாக மாற்ற வேண்டுமெனில்
அதற்கு நடி஼ன் அந்த அளவுக்கு திறமையானவனாகவும் சுய
உணர்வோடும் இருக்க வேண்டும். இருவருக்கிடையே நிகழும்
உரையாடலில் ஒருவரையொருவர் பேசுகின்ற வாக்கியங்களின்
மூலம் புரிந்து கொள்கின்றனர். அதே போன்று பல்வேறு சூழ்~
நிலை மற்றும் காட்சிகளின் மூலமாக ஁஁எழும் முகபாவங்களைப்
புரிந்து கொண்டு பிரதிபலித்திட உடல் பாவயியல் பற்றிய
பரந்த கூர்மையான அறிவு தேவைப்படுகிறது. அதன்மூலம்
மட்டுமே சுய உணர்வற்ற முகபாவங்களைச் சரியான முறையில்
புரிந்து கொள்ளவும், உபயோகப்படுத்தவும் முடியும்.</p>

<p>மௌனப் படங்களை இயக்கும் சிறந்த இயக்குநரால் தெருவில்
கண்டெடுக்கும் சாதாரண முகத்தில் காட்சிக்குத் தேவைப்~
படும் சரியான பாவம் இருக்கிறதா என்பதை துல்லியமாக
கண்டுபிடிக்க முடியும். ஆனால் ஒலிப்படத்திலோ, இ஥்தகைய
வழிமுறையை அமைதியான பெருங்கூட்டத்தின் தனித்தனி~
யான க்ளோசப்பில் மட்டும்தான் உபயோகப்படுத்த முடியும்.</p>

<p>குழந்தைகளும் நாகரீகமற்ற காட்டுவாசிகளும்</p>

<p>குழந்தைகளுக்கு நடிப்பு என்பது எப்போதுமே இயற்கையாய்
இருக்கும். நம்பும்படி நடிப்பது என்பது அவர்களுக்கு இயற்~
கையாய் அமைந்த ஒன்று, நடிகர்களைப்஼போல அவர்கள் குறிப்~
பிட்ட பாவங்களை முகத்தில் காட்டவேண்டிய அவசியமில்லை.
அவர்கள் உண்மையில் எப்படி இருக்கிறார்களோ, அப்படி</p>

<p>பக்கம்:92
இல்லை, மாறாக வேறு மாதிரி என்பது போல சாதாரண~
மாக நடிப்பார்கள். தாங்கள் எந்த சூழ்நிலையில் இருக்கிறார்~
களோ அந்த சூழ்நிலையல்லாத வேறொன்றில் இருப்பதுபோல
மிகச் சாதாரணமாக பாவிப்பார்கள். இதை நடிப்பு என்று
சொல்ல முடியாது. இளம் உள்ளத்தின் இயற்கையான
வெளிப்பாடு இது. மனிதர்களுக்கு மட்டும் பொருந்துவது
அல்ல இது. மற்ற உயிரினங்களுக்கும் இது பொருந்துவதாகும்
கனவு அல்லது மயக்கத்தில் ஏற்படும் மாற்றத்தைப் போன்றது
இது. குழந்தைகளை இயக்கக் கூடாது மாறாக அவர்களோடு
விளையாட வேண்டும் என்பதை, சினிமாவிலும் நாடகத்தி~
லும் குழந்தைகளோடு பழகியவர்கள் நன்கு அறிவர். அவர்~
களின் நடிப்பு இயற்கையாக தோன்றவில்லை. மாறாக நடிப்பு
விளையாட்டு என்பது அவர்களின் இயற்கைதன்மையாகும்.</p>

<p>இதே விஶயங்களைப் புராதன மனிதர்களிடத்தும், காட்டுவாசி~
களிடமும் காணலாம். அவர்களுடைய க்ளோசப்புகள் பெரும்~
பாலும் வழக்கத்துக்கு மாறான அசைவையும் பாவங்களையும்
காட்டும். அதாவது வெள்ளை மனிதனின் கண்ணோட்டத்தில்
இது வழக்கத்துக்கு மாறாக இருக்கும். அவைகளை நாம்
புரிந்துக் கொண்டோமெனில் அவைகள் நமக்கு புதிய விஶய~
மாகவும், உடனடியான பாதிப்பை ஏற்படுத்துவதாகவும்
இருக்கும். ஆனால் பெரும்பாலான நேரங்களில் அவைகளை
நாம் புரிந்துக் கொள்வதில்லை.</p>

<p>சிவப்பிந்தியர்கள் மற்றும் சீனர்கள் ஆழ்ந்த மன வருத்தத்~
தையோ அல்லது வலியையோ வெளிப்படுத்துவார்களெனில்
அது நம் கண்களுக்கு அவர்கள் சிரிப்பதுபோல் தோன்றும்.
அவர்களின் பாவங்கள் வெளிப்படுத்துவது என்ன என்பதை
நாம் எப்போதுமோ அல்லது உடனடியாகவோ புரிந்துக்
கொள்வதில்லை. ஆனால் அவ்வாறு புரிந்து கொண்டோமெனில்
அவர்களின் வலியை வெளிப்படுத்தும் உணர்வு நம்மை வெகு~
வாக நெகிழ செய்யும் அது அவ்வாறு நெகிழச் செய்வதற்கு
காரணம் அது வழக்கத்துக்கு மாறாக இருப்பதுதான்.</p>

<p>குழு உடல் பாவயியல்</p>

<p>வளர்ச்சியடைந்த வெள்ளையர் அல்லாத (Colonued) இனங்க~
ளுக்கே உரிய பொதுவான அம்சங்களை காண்பிப்பதற்கு க்ளோ~
சப் தேவையில்லை. நீக்ரோக்கள், சீனர்கள், எஸ்கிமோக்கள்
போன்றோரைக் குழுவாகப்பார்ப்பதன் மூலம் அறிந்துக் கொள்~
ளலாம். இதற்கு நேர்மாறாக இந்த வினோத முகங்கள் எல்஼லாமே</p>

<p>பக்கம்:93
நமக்கு ஒரே மாதிரியாக தோன்றும். ஏனெனில் அவர்~
களைப்பற்றி நாம் மிகவும் மேம்போக்காகத்தான் அறிந்திருக்~
கிறோம். ஒரு சீனனுக்கும் இன்னொரு சீனனுக்கும் இடையே
உள்ள வேறுபாட்டையோ அல்லது ஒரு நீக்ரோவுக்கும் இன்~
னொரு நீக்ரோவுக்கும் இடையே உள்ள வேறு பாட்டை காண்~
பிக்கவோதான் க்ளோசப் இங்கு தேவைப்படுகிறது.</p>

<p>ஆங்கிலேயன், பிரெஞ்சுக்காரன் இத்தாலியன் அல்லது ஜெர்~
மானியனுடைய குணச்சித்திரத்தை நாம் சினிமாவில் காண்பிப்~
போமெனில் அது ஒரு கண்டு பிடிப்பாக இருக்காது ஏனெனில்
அவர்களைப் பற்றி நமக்கு முன்னமேயே நன்கு தெரியும். அதிக
பட்சம் அவர்களைப்஼பற்றி அந்த சினிமா ஏ஥ாவது புதிய விஶ~
யங்களை சொல்லலாம். அவர்களைப் பற்றி நமது அறிவு
மேலும் விருத்தியடையும்.</p>

<p>எது எப்படியாயிருந்த போதிலும், இது மாதிரியான முகம்
இந்த நாட்டைச் சேர்ந்தது அல்லது இந்த இனத்தைச் சேர்ந்தது
என்று சொல்வது மிகவும் கடினம். மறுக்க முடியாத எல்லோ~
ராலும் ஒத்துக்கொள்ளப்பட்ட ஆங்கில முகம் என்று ஏதாவது
உள்ளதா என்ன? அப்படி ஒரு வேளை இருந்தால் அது எப்படி
இருக்கும்? அந்த குறிப்பிட்ட முகம் ஏன் உன்மையானதாக~
வும் சரியானதாகவும் இருக்க வேண்டும்? வேறு ஒரு முகமாக
அது ஏன் இருக்கக் கூடாது? மொழிக்களுக்கான ஒப்பியல்
விஞ்ஞானம், இருக்கும்போது அதே போல் ஏன் பாவங்களுக்~
கும், அசைவுகளுக்கும் ஒப்பியல் விஞ்ஞானம் இருக்கக் கூடாது?
அப்படி ஒரு விஞ்ஞானம் இருக்குமாயின் அதில் மேற்கொள்~
ளப்படும் ஆராய்ச்சிகள் வெளிப்பட்டு பாவங்களின் பொது~
வான அடிப்படை வடிவங்கள் என்ன என்பதை கண்டு பிடிக்க
உதவும். சினிமாவானது அப்படிப்பட்ட ஒரு விஞ்ஞானத்தை
நிறுவுவதற்கு உதவிகரமாய் இருக்கும்.</p>

<p>முகங்களின் வர்க்கத் தன்மை</p>

<p>     சினிமா அறிமுகப்படுத்திய இன்னொரு புதுமை என்ன~
வெனில், முகங்களையும், முகபாவங்களையும் `வகையின
படுத்திய'தாகும். வலிமை வாய்ந்த தொழிலாளியின் முரட்~
முகத்தையும் அதற்கு நேர்மாறாக சீர்கெட்ட பிரபுவின் மிருது~
வான முகத்தையும் இங்கே குறிப்பிடவில்லை. இவைகள் மேம்~
போக்கான வகைபடுத்துதல் என்பதும் க்ளோசப்புகள்
அத்தகைய புராதன முகத்திரையைக் ஂகிழித்தெறிந்தது என்பதும்</p>

<p>பக்கம்:94
நாம் அறிந்த ஒன்றே. வெளிப்படையாகவும், சாதாரணமாக~
வும் தெரியக்கூடிய குணச்சித்திரங்களை மட்டும் க்ளோசப்
வெளிக்காட்டவில்லை. மறைந்திருந்த மற்றும் வர்க்க குணாம்~
சங்களையும் அது எடுத்துக் காட்டியது. இந்த குணாம்சங்கள்
பல நேரங்களில் தேசிய மற்ற இன ஂகுணாம்சங்களைக் ஂகாட்டிலும்
வெளிப்படையாக தெரிந்தது. உதாரணமாக, பிரெஞ்சு சுரங்க
தொழிலாளியின் முகம், பிரெஞ்சு பிரபுவின் முகத்தோடு
ஒத்துப் போவதைக் காட்டிலும், ஜெர்மானிய அல்லது ஆங்கி~
லேயே சுரங்க தொழிலாளியின் முகத்தோடு அதிகமாக ஒத்து
போயிற்று. தேசிய, இன மற்றும் வர்க்க கலப்பான஥ு பல்வேறு~
பட்ட சுவையான கூட்டை உருவாக்கியது. ஆனால் அவைகள்
அனைத்தும் காண்பித்தது என்னவெனில் மனிதர்களையும்
மனித வகையினங்களையும்தான்.</p>

<p>மிகச் சிறந்த முறையில் வர்க்க அடிப்படையிலான உடல்பாவ~
யியலைத் தந்தது சோவியத் சினிமாதான். இது தற்செயலாக
நிகழ்ந்த நிகழ்ச்சியல்ல. புரட்சிகரமான வர்க்கப் போராட்டத்~
தால் கூர்மை படுத்தப்படுத்த கண்கள், அங்கே `பணக்கார'னுக்~
கும் `ஏழை'க்கும் இடையே உள்ள வித்தியாசத்தை மிக நுணுக்க~
மாக பார்த்தது. சில சோவியத் சினிமாக்கள் காண்்பித்த
`வகையினங்களைப் போல, எந்த கொள்கை அடிப்படை~
யிலான ஆய்வாலும் அத்தனை முழுமையாக சமூகப் பாகுபாடு~
களை காண்பிக்க முடியாது. ஐஸன்ஸ்டினுடைய அக்டோபர்
திரைப்படத்தை யாரால்தான் மறக்க முடியும்? அதில் வெறும்
தொழிலாளிகள் மற்றும் பிரபுக்கள் முகத்தை மட்டும் ஒன்றுக்~
கொன்று எதிராக மோதும்படி இணைத்திருப்பதாக மட்டும்
அது இருக்கவில்லை. முதலாளித்துவ தாராள வாதிகள், மித~
வாத சமூக அறிவாளிகள் இவர்களுக்கிடையே வித்தியாசம்
காணக் கூடிய வகையில் கூட அவர்கள் முகங்கள் இருந்தன.
மாலுமி ஒருவன் அவர்களின் பாதையைப் பாலத்திலே தடுக்கும்
போது முகத்தோடு முகம் மோதுகிறது. உலகின் இரண்டு
வெவ்வேறான கருத்துகளின் மோதலை நாம் அங்கே மிகச்
சரியான இரு வேறு விதமான உடல்பாவயியல்களின் மோது~
தலிலே பார்க்கிறோம்.</p>

<p>இதற்கு மிதச் சிறந்த உதாரணமாக, தாவ்ஶங்கோ~
வின் உள்நாட்டு போரைப் பற்றிய படமான ஆர்சனல்
படத்தில் வரும் மிகப் பிரம்மாண்டமான காட்சியைச்
சொல்லலாம். அந்தக் காட்சி கிவ் நகரத்தில் எழப்போகும்
எழுச்சியைக் காட்டுகிறது. புயலுக்கு முன்னே ஓர் ஆழ்ந்த</p>

<p>பக்கம்:95
அமைதி நிலவுகிறது. `முதல் தாக்குதலுக்காக காத்திருக்கிறார்'
என்று டைட்டில் கூறுகிறது. நகரம் முழுவதுமே அந்த இரவில்
அசையாமல், மூச்சை அடக்கிக் கொண்டு காத்திருக்கிறது.
யாருமே தூங்கவில்லை. ஒவ்வொரு வரும் காத்திருக்கிறார்கள்.
முதல் தாக்குதலுக்காக இவர்கள் காத்திருக்கிறார்கள்.</p>

<p>காத்துக் கொண்டிருப்பது யார்? ஏன் காத்துக் கொண்டிருக்~
கிறார்கள்? தொடர்ந்து வரும் சிறு சிறு காட்சிகள் அந்த
நகரத்தில் இருக்கும் பல்வேறு சமூகப்பிரிவினரைக்காட்டுகிறது.
தொழிலாளிகள் உன்னிப்பாக நோக்கிக் கொண்டிருக்கிறார்~
கள். படை வீரர்கள் பார்த்துக் கொண்டார்கள். ஆலை தொழி~
லாளிகள் உற்றுக் கேட்கிறார்கள். வியாபாரிகள் தங்கள் காது~
களைக் கூர்மையாக்கிக் கொள்கிறார்கள். தொழிற்சாலையின்
சொந்தக்காரன் சுறுசுறுப்படைகிறான். ஆசிரியர், குமஸ்தா
நிலச்சொந்தக்காரன் மற்றும் கலைஞன் எல்லோரும் வர்க்கங்~
களை மறந்து வெளியே காத்துக் கொண்டிருக்கிறார்கள். எல்~
லோரும் எதையோ எதிர்பார்த்து இருட்டையே நோக்கிக்
கொண்டிருக்கின்றனர். அவர்கள் யார் யார் என்று நமக்கு
எப்படி தெரிகிறது? எந்த ஂடைட்டிலும் அவர்கள் யார் என்பதை
நமக்குச் சொல்லவில்லை. அவர்கள் யார் யார் என்பதும் அவர்~
கள் எந்த வர்க்கத்தைச் சேர்ந்தவர்கள் என்பதும் அவர்கள்
முகத்திலேயே எழுதியிருக்கிறது. அவர்களுடைய தனிப்பட்ட
உடல்பாவயியலில் அது தெளிவாகத் தெரிகிறது. மனிதனுடைய
வர்க்கத்தைக் ஂகாட்டவில்லை. மாறாக வர்க்கங்களை மனிதர்கள்
மூலம் காட்டுகிறார்கள். இந்தக் காட்சிக்குப் பிறகு நடுத்~
தெருக்களிலேயே சண்டை வெடிக்கிறது. அப்போது அங்கே
மோதுவது வெறும் இயந்திர துப்பாக்கிகளும், குண்டுகளும்
மட்டும் அல்ல. கூடவே ஂஉயிருள்ள மனித முகங்களும் ஆகும்.</p>

<p>நாம் அறியாத நம் முகங்கள்</p>

<p>மனித முகம் இன்னும் முழுமையாக அறியப்படவில்லை.
இன்னும் அதில் கண்டுபிடிக்கப்படாத பகுதிகள் எவ்வளவோ
உள்ளன. இது குறித்து சினிமாவில் ஒரு முக்கியமான பணி
என்னவெனில், தன்னுடை `நுணுக்கமான உடல்பாவயியலைக்'
கொண்டு, தம்முடைய முகத்தில் எந்த அளவு நமக்குச் சொந்த~
மானது, எந்த அளவு நம்முடைய குடும்பம், தேசியம், வர்க்கம்
ஆகியவற்றுக்கு சொந்தமானது என்பதை எடுத்துக் காட்டுவ~
தாகும். நம்முடைய தனிப்பட்ட அம்சங்கள் பொதுவான
அம்சங்களோடு எவ்வாறு இணைகிறது என்பதை அது காட்டுகிறது.</p>

<p>பக்கம்:96
அவ்வாறு அவைகள் ஒன்றோடொன்று பிரிக்கமுடியாத~
படி இணைந்த பின், ஒன்று இன்னொன்றின் அம்சமாக விளங்~
குவதைக் காட்டுகிறது. இதுவரை எழுதப்பட்ட உளவியலா~
னது தன்னுடைய ஆய்வின் மூலம் சமூகத்தையும் தனிமனிதனை~
யும் பிரிப்பது எது என்று அறிய முயற்சிக்கிறது. இந்தப்
பணியை சினிமாவின் `மிக நுணுக்கமான உடல்பாவயி~
யல்' வார்த்தைகளைக் காட்டிலும் மிக சிறப்பாகவும் நுட்~
பமாகவும் செய்கிறது. இவ்வாறு செய்வதன் மூலம்
சினிமா கலாரீதியான முக்கியத்துவத்தோடு விஞ்ஞான
ரீதியான முக்கியத்துவத்தையும் அடைகிறது. ஏனெனில் இது
மானிட சாஸ்திரவியல் மற்றும் உளவியலுக்கு பல அறிய
விஶயங்களைத் தருகிறது.</p>

<p>இரண்டாவது முகம்</p>

<p>தனிப்பட்ட மற்றும் இனத்துக்கான குணாம்சங்கள் ஒன்றோ~
டொன்று கலப்பதால், இரண்டு விதமான பாவங்கள் ஒன்றன்
மீது ஒன்றாக, ஒன்றினூடே ஒன்று தெரிவதாக அமைகிறது.
உதாரணமாக நன்கு நாகரிகமடைந்த, பழைய, உன்னத
குலத்தைச் சேர்ந்த சீர்கெட்ட ஒருவனை நாம் அடிக்கடிப்
பார்க்கிறோம். ஆங்கில பிரபுவின் உடலமைப்புபடி அவன்
முகபாவம் அழகானதாகவும், உன்னதமாகவும் இருக்கலாம்.
அது அந்த பழைய இனக்கலாச்சாரத்துக்கே உரிய ஒரு உடற்
பாவயியலாகும். ஆனால் அதே முகத்தை க்ளோசப்பில்
பார்க்கும்போது, முரட்டுத்தனமான, இழிந்த பாவத்தைக்
காணலாம். அது அந்த தனிப்பட்ட மனிதனுக்குரிய ஒன்று.
அல்லது சினிமா இதற்கு எதிர்மாறாகவும் காட்டலாம்.
அதாவது நாகரிகமடையாத இனத்தின் அழகற்ற அம்சங்களை
மறைத்தவனாய், அழகிய உளப்பூர்வமான உடற்பாவயியலை
ஒருவன் கொண்டிருக்க முடியும்.</p>

<p>`நுணுக்கமான உடற்பாவயியல்' எதைக்காட்டுகிறது என்றால்
பின்னாலிருக்கக்கூடிய, அதாவது மறைந்திருக்கக்கூடிய
இரண்டாவது முகத்தை நம்மால் கட்டுபடுத்த முடியும்.
இன்னொரு முகத்தை நம்மால் கட்டுபடுத்த முடியாது.
காரணம், இது ஏற்கனவே உ஠லமைப்போடு ஒன்றிணைந்து
இறுகிப்போய்விட்டது.</p>

<p>நுட்பமான நாடகம்</p>

<p>`நுட்பமான பாவயியலால்' கீழே கூறப்படும் விளைவும்
தேவையின் பொருட்டு ஏற்பட்டது. க்ளோசப்பில் காட்ட஼ப்பட்ட</p>

<p>பக்கம்:97
படங்களின் விரிவான உளவியல் படத்தில் ஏராளமான
இடத்தை (பல அடிகளை) எடுத்துக் கொண்டது. இதனால்
கதைக்கான பகுதி நாளுக்கு நாள் குறைந்துக் கொண்டே
வந்தது. எந்த அளவுக்கு நிகழ்ச்சிகள் உள்ளார்ந்த உள்ளடக்~
கத்தில் சிறந்தனவாக இருந்தனவோ அந்த அளவுக்கு
நிகழ்ச்சிகளும் சினிமாவில் குறைவாக இருந்தன. இந்நிகழ்ச்சி~
களின் நீளமும் சோனட்டா இசை வடிவத்தைப் போல ஒரு
குறிப்பிட்ட நீளத்தையே கொண்டிருந்தன.</p>

<p>ஒன்றுக்கு மேல் ஒன்றாக பல்வேறு நிகழ்ச்சிகளின் மொத்தமாக
இருந்த தீரச்செயல்களின் தொகுப்பு என்பது அதற்கு மேல்
தேவையற்ற ஒன்றாக இருந்தது. ஆரம்பகாலத்தில் இருந்த
பரந்துபட்ட நிகழ்ச்சிகளின் குவியல் பாணி மாறி நிகழ்ச்சி~
களின் ஆழத்தன்மைக்கு முக்கியத்துவம் கொடுக்கப்பட்டது.
கதைகளின் போக்கு உள்ளார்ந்ததாகவும், ஆழமானதாகவும்
மாறியது. அவைகள் இதயத்தை நெகிழவைப்பதாக இருந்தன.
சினிமாவில் க்ளோசப்புகளின் வளர்ச்சியானது கதை மற்றும்
திரைக்கதைகளின் பாணியை முழுமையாக மாற்றியது.
உள்ளத்தின் இதுவரை தெரியாமலிருந்த நுணுக்கமான விஶயங்~
களைப் பற்றி கதைகள் விவரிக்கத் தொடங்கின.</p>

<p>பல வளமான நிகழ்ச்சிகளைக் கொண்டிருந்த மிகச் சிறந்த
நாவல்கள் சினிமாவுக்கேற்றதாக இல்லாமல் போனது.
எளிமையான, சாதாரண கதைகள்தான் தேவையாயிருந்தன.
சிக்கல்களும், தீரச்செயல்களும் நிறைந்த கதைகளுக்கு அவசிய~
மில்லாமல் போய்விட்டது. திரைப்பட இயக்குநர்கள் தங்கள்
கற்பனையையும் புதுமைகளையும் பல்வேறு வேகாவேசமான
காட்சிகள் மூலம் காட்டாமல், காட்சிகளை வடிவமைப்~
பதிலும் அதன் விளக்கங்களைக் காட்டுவதிலும் வெளிப்படுத்~
தினார்கள். வார்த்தைகளால் விவரிக்க முடியாத, பார்ப்பதால்
மட்டுமே புரிந்துக் கொள்ளக்கூடிய காட்சிகளை அமைப்பதில்~
தான் அதிக ஆர்வம் காட்டினார்கள். இவ்வாறாக மௌனப்
படங்கள் படிப்படியாக இலக்கிய சார்பை இழந்தன. அப்~
போது ஓவியத்திலும் அத்தகையப் போக்குதான் நிலவியது.</p>

<p>நாடகப் போக்கு</p>

<p>க்ளோசப்பின் காரணமாக சினிமாவில் கதை என்பது எளிமைப்~
படுத்தப்பட்டது. வாழ்க்கையின் நாடக அம்சத்தை ஆழமாக~
வும், விளக்கங்களோடும் காண்பித்தது. வெளி நிகழ்ச்சி</p>

<p>பக்கம்:98
ஏதும் இல்லாமல் நாடக சூழ்நிலையின் அழுத்தத்தை வெறும்
நிலைப்பாடாக மாற்றுவதில் வெற்றிக்கண்டது. வெறும்
மிகச்சிறிய அசைவுகளின் மூலமும், ஒரு மயிரிழையின் அசைவின்
மூலமும், மேலோட்டமான அமைதியின் கீழ் கொப்புளித்துக்
கொண்டிருக்கும் இறுக்கத்தை நம் மெய்சிலிர்க்கும்படியும்,
ஆழத்திலுள்ள கடும் புயலை நாம் தொட்டுணர்வது போலவும்
வெளிப்படுத்தியது. இத்தகைய படங்கள் மிகக் குறுகிய
இடத்தில் மனிதர்களது மிகக் கடுமையான அமைதியில்
ஸ்டிரின்ட்பர்கிய பாவங்களை தன்னிகரற்ற
அளவில் காண்பித்தது. சாதாரண குடும்பங்களில் உள்ள
அமைதியில் நிலவும் மிக நுணுக்கமான சோகங்களை ஏதோ
பயங்கர போரை காட்டுவது போல காட்டினார்கள். ஒரு
துளி நீரில் உள்ள நுண் கிருமிகளின் போராட்டத்தை
மைக்ராஸ்கோப் காட்டுவது போல, இந்தப் போராட்டத்தை
சினிமா காட்டியது.</p>

<p>காமிராவின் சீர்மை</p>

<p>காமிராவின் அசைவு மற்றும் வேகமான படத்தொகுப்பு
(மோன்டாஜ்) மூலம் நிலைத்த சூழ்நிலைகளின் அசையாத்
தன்மையைக்கூட அசைய வைக்கவும், விறுவிறுப்பாக்கவும்
முடியும். இது சினிமாவுக்கு மட்டுமே உரிய வெளிப்பாட்டுச்
சாதனமாகும். நாடகத்திலோ அல்லது ஸ்டியோவிலோ
ஒரு காட்சியின் போது ஏதோ ஒரு காரணத்தால் அசைவு
என்பது நின்று போகுமாயின் அதன் பின் வேகம், சீர்மை
என்ற பேச்சுக்கே இ஠மில்லை. ஆனால் சினிமாவிலோ காட்சி~
களானது ஒரு துளியும் அசைவின்றி, உறைந்து போயிருந்~
தாலும், திரையில் காண்பிக்கும் போது பல்வேறுபட்ட
வேகமான அசைவுகளை கொண்டிருக்கும். இ஥ு சினிமாவுக்கே~
யுரிய குறிப்பிட்ட தன்மையாகும். கதாபாத்திரங்கள் அசை~
யாமல் இருக்கலாம். ஆனால் காமிரா அசைவின் காரணமாக
நமது கண்கள் ஒரு கதாபாத்திரத்திலிருந்து இன்னொரு
கதாபாத்திரத்திற்கு தாவும். மனிதர்களும் பொருள்களும்
அசையாமல் இருக்கலாம். ஆனால் காமிராவோ ஒன்றிலிருந்து
இன்னொன்றிற்கு வேகமாக மாறுகிறது. இவ்வாறு எடுக்கப்~
பட்ட அசைவற்ற படங்களோ பின்னர் வேகமான படத்~
தொகுப்பின் மூலம் இணைக்கப்படுவதால் அவைகள் வேகமான
அசைவையும், மிகுந்த சீர்மையையும் கொண்டிருக்கும்.
இதன் மூலம் அந்தக் காட்சி வெளிப்புற அளவில் அசையாவிட்டாலும்</p>

<p>பக்கம்:99
உள்ளார்ந்த அசைவை நம்மால் உணர முடியும்.
ஒருகாட்சி அதன் மொத்தத்தில் மௌனமாய் பெரிய அசைவற்ற
இயந்திரம் போல் இருக்கலாம். ஆனால் தொடர்ந்து வரும்
க்ளோசப்புகளோ கடிகாரத்தில் உள்ள சிறிய சக்கரத்தின்
துடிப்பைப் போல இருக்கும். உருவங்கள் அசையாமல்
இருக்கலாம். ஆனால் அவைகளின் கண்ணிமை துடிப்பதையோ
அல்லது உதடு முறுவலிப்பதையோ நாம் பார்க்கிறோம்.
அந்த அசையாத் தன்மையில் பொதிந்திருப்பதோ கடுமையான
அழுத்தமாகும்.</p>

<p>லுப்பு பிக் என்பவர் மௌனப் பட காலத்தில்
மிகச் சிறந்து விளங்கிய ஜெர்மானிய இயக்குநர் ஆவார். அவர்
குற்றங்களைப் பற்றிய படம் ஒன்றை எ஠ுத்தார். அப்படத்தில்
பாங்கில் கொள்ளையடிப்பதற்காக ரகசிய பண அறைக்குள்
செல்கிறார்கள். அந்த அறையில் அவர்கள் எதிர்பாராத
விதமாக சிக்கிக் கொள்கிறார்கள். அங்கிருந்து தப்புவதற்கு
அவர்களுக்கு வேறு அறை வழியும் இல்லை. இன்னும் பத்து
நிமிடத்தில் அந்த அறை வெடித்து சிதறப் போகிறது. அந்த
ஒன்பது கொள்ளைக்காரர்களும் வழி ஏதாவது கிடைக்கிறதா
என்று முன்னும் பின்னும் மோதிப் பார்க்கிறார்கள். இன்னும்
மூன்று நிமிடங்களே பாக்கியுள்ளன. அவர்கள் அப்படியே
அசையாமல் கடிகாரத்தின் முள்ளையே பார்த்துக் கொண்டு
நிற்கிறார்கள். அவர்கள் அப்படியே காத்துக் கொண்டிருக்கி~
றார்கள். அந்த ரகசிய அறையில் எதுவுமே அசையவில்லை.
ஆனால் கடிகாரத்தின் முட்கள் மட்டும் அசைகின்றன. இந்த
நடுக்க உணர்வை இத்தனை அழுத்தமாக வேறு எந்த முறை~
யிலும் காண்பித்திருக்க முடியாது. கதாபாத்திரங்கள்
எத்தனை வெறித்தனமாக முன்னும் பின்னும் ஓடினாலும்
இந்த உணர்வு நமக்கு கிடைத்திருக்காது. அவர்கள் மூச்சு
விடக்கூட மறந்து நிற்கிறார்கள். அத்தனை முழுமையாய்
இருந்தது அவர்களது அசைவற்றத்தன்மை.</p>

<p>நாடக மேடையிலோ இத்தகைய அசைவற்ற தன்மையே
ஒரு சிறிது நேரத்துக்குத்தான் காண்பிக்க முடியும். ஒரு சில
கணங்களுக்குப் பின் அது உயிரற்ற வெறுமையான காட்சி~
யாகிவிடும். ஏனெனில் எதுவுமே அக்காட்சியில் நிகழவில்லை.
ஆனால் சினிமாவிலோ கதாபாத்திரங்கள் அசையாவிட்டாலும்
மற்ற எத்தனையோ விஶயங்கள் நிகழ்ந்துக் கொண்டிருக்கும்.
ஏனெனில் கதாபாத்திரங்கள் அசையாமல் இருக்கும் போது
கூட காமிரா அசையமுடியும். வேகமான படத்தொகுப்பில்</p>

<p>பக்கம்:100
இணைக்கப்பட்ட க்ளோசப்புகள் அசையும்-பாங்கு கொள்ளை
பற்றி மேற் குறிப்பிட்ட காட்சியில், காமிராவோ, நடிகர்கள்
அசைவதைக் காட்டிலும் மிக வேகமாக முன்னும் பின்னும்
நகர்கிறது. இதனால் படமும் வேகமா஗ நகர்கிறது. நடிகன்
அசையாமலும் பேசாமலும் இருக்கலாம். ஆனால் வேகமாக
தொடர்ந்துவரும் க்ளோசப்புகளும், `மிக நுணுக்கமான உடற்~
பாவயியலும்' பேசுகிறது. அவைகள் பயத்தைப் பற்றிய
பொதுவான படத்தைக் காட்டவில்லை. மாறாக ஒன்஫து
இதயங்களின் பய வேதனையை, இசைவடிவத்தின் அடுத்~
தடுத்த கட்டத்தைக் காண்பிப்பது போல் காண்பிக்கிறது
படத் தொகுப்பின் வேகம் அதிகரிக்கவும், க்ளோசப்புகளின்
நீளம் குறையவும் நமது பயமோ அதிகமாகிறது.</p>

<p>ஒரு எறும்புக் கூட்டத்தை தூரத்திலிருந்து பார்த்தால் உயி~
ரற்றது போலத்தான் தோன்றும். ஆனால் நெருக்கத்தில்
பார்த்தால் சுறுசுறுப்பான வாழ்க்கையின் உயிரோட்டத்தைப்
பார்க்க முடியும். சோபையிழந்து சாதாரணமாக தெரியும்
நமது தினசரி வாழ்க்கையும் நாம் வெகு கவனமாக நெருக்~
கத்திலிருந்துப் பார்த்தால் அதன் மிக நுணுக்கமான நாடகத்
தன்மையில் பல ஆழமான உயிரோட்டமுள்ள விஶயங்களைப்
பார்க்கலாம்.</p>

<p>நாடக மயப்படுத்தப்பட்ட சாதாரண இடங்கள்</p>

<p>மௌனப் படங்கள் சிறந்து விளங்கிய காலகட்டத்தில்
யதார்த்தப் படங்கள் என்பது வழக்கத்திலிருந்தது. அப்படங்~
கள் சாதாரண இடங்களையும். பொதுவாக நாம் கவலைப்~
படாத சாதாரண விஶயங்களையும், பெரிய அளவில் நாடக
மயப்படுத்தியது. இந்த காலகட்டத்தில்தான் தெருவில்
இருக்கக்கூடிய ஒரு சாதாரண அமெரிக்கனின் தினசரி
வாழ்க்கையைச் சித்தரிக்கக் கூடிய மிகச்சிறந்த படம் ஒன்றை
கிங் விடோர் எடுத்தார். அது ஒரே மாதிரியான
நிகழ்ச்சியைக் காண்பித்தது. இருந்தபோதிலும் அதனினூடே
உள்ள பல விஶயங்கள் நம்மை நெகிழவும் நடுங்கவும்
செய்தன. பல சிறிய சிறிய மகிழ்ச்சிகரமான நிகழ்ச்சிகளும்
அ஥ில் இருந்தன. ஒரே தொனியில் பாடப்படும் ராகத்தில்
மறைந்திருக்கும் இனிமைகளைப் போன்றிருந்தது அது.</p>

<p>பரபரப்பாக உள்ள படத்தின் முக்கியமான தருணத்தை
இதுபோன்ற க்ளோசப்புகளை விளக்கமாக ஒரு குறிப்பிட்ட
முனையை நோக்கிக் காண்பிப்பதில் இந்த முறையைப் பயன்஼படுத்தினால்,</p>

<p>பக்கம்:101
அது மிகச்சிறந்த முறையில் காட்சியை அமைத்துக்~
கொடுக்கும். அந்த முக்கியமான தருணம் மட்டும் விறுவிறுப்~
பாக உடனடியான காலகட்டத்தில் நிகழ்வது போலவும்
மீதமுள்ளவை ஏதோ தராசு தட்டில் இருப்பது போலவும்
இருக்கும். தராசு தட்டின் முள்ளோ கடைசியாக எந்த
திசையில் நிற்கும் என்பது தெரியாதது போல் இருபுறமும்
ஆடிக் கொண்டிருக்கும்.</p>

<p>முக்கியமான தருணம்</p>

<p>ரீஸ்மானின் மிகச் சிறந்த படமான கடைசி இரவு
படத்தின் கடைசிக் காட்சியைப் பார்ப்~
போம். பெட்ரோகிராடிலுள்ள ரயில் நிலையம் ஒன்றில் ஒரு
ராணுவ ரயில்வண்டி நீராவியைக் கக்கிக் கொண்டு
நுழைகிறது. பெட்ரோகிராடோ புரட்சிக்காரர்களின் வசம்
இருக்கிறது. அந்த ரயில் முழுவதும் மூடப்பட்டு ஒரு ரகசிய
தோற்றத்தைக் கொண்டுள்ளது. அந்த ரயிலில் வந்திருப்பது
நண்பர்களா? அல்லது எதிரிகளா? ரயிலின் ஒவ்வொரு கதவிலும்,
சன்னலிலும் துப்பாக்கி முனைகள் மின்னுகின்றன. ஆனால்
முகங்கள் எதுவும் தெரியவில்லை. ரயில்வே நிலையக் கட்டிடத்~
தின் ஒவ்வொரு கதவிலும், சன்னலிலும் துப்பாக்கிகள் ரயிலை
நோக்கிக் குறிபார்த்துக் கொண்டிருக்கின்றன. உள்ளே
இருப்பது யார்? நண்பனா அல்லது எ஥ிரியா? யாருமற்ற அந்த
பிளாட்பாரத்தில் காலை வைப்பதற்கு யாரும் முன்வர வில்லை.
எ஥ிர்பார்ப்பும் அமைதியும் அங்கே நிலவுகிறது. இன்ஜி~
னிலிருந்து நீராவி வெளிவரும் சத்தம் மட்டும் கேட்கிறது.
எதுவுமே அசையவில்லை. அடுத்த சில நிமிட நேரங்களில்
நிகழப்போவதை வைத்துத்தான், அந்த ரயிலின் அந்த நக~
ரத்தின், ஏன் மொத்தப் புரட்சியின் எதிர்காலமே நிர்ணயிக்கப்~
படப் போகிறது. அந்த உறைந்து போன இறுக்க உணர்வு,
சந்தேகம், எதிர்பார்ப்பு ஆகியவைகளை உண்மையில் அசை~
வற்ற ஒன்று என்று சொல்ல முடியாது. நம்முடைய நரம்புகள்
துடிப்பதைப் போலவே அந்தச் சூழ்நிலையின் அம்சங்களும்
துடித்துக் கொண்டிருந்தன. மாறி மாறித் தொடர்ந்து வரும்
க்ளோசப்புகள், நமது திகைப்பை இன்னும் கூர்மையாக்குகிறது.
கடைசியாக ஒரு வயதான விவசாயப் பெண்மணி தனியாக
எந்த ஆயுதமும் இல்லாமல் ப்ளாட் பாரத்தில் நடந்து செல்~
கிறாள். அவள் அந்த ரயிலருகில் செல்கிறாள். அதில்
உள்ளவர்களோடு பேசுகிறாள். கேள்விகளைக் கேட்கிறாள்.
படம் முழுவதும் அதுபோன்ற அசையாத்தன்மையைக்</p>

<p>பக்கம்:102
கொண்டிருக்குமேயானால், மேற்சொன்ன அந்தக் காட்சி
அத்தனைச் சிறப்பாக அமைந்திருக்காது. பரபரப்பாக
ஒலித்துக் கொண்டிருக்கும் இசையின் நடுவே வரக்கூடிய
அமைதியைப் போன்றது இது.</p>

<p>உண்மைக்குப் பதிலாக யதார்த்தம்</p>

<p>க்ளோசப்புக்கேயுரிய நுணுக்கமான நாடகவியல் ஒரு குறிப்~
பிட்ட அளவு வளர்ச்சியடைந்த பிறகு, நாடகமயமான
காட்சிகள் இல்லாமல் மட்டும் படம் எடுக்கப்படவில்லை.
நாடகத்தன்மைக்கு மையமாக விளங்கும் கதாநாயகன்
இல்லாமல் கூடப் படம் எடுத்தார்கள். இது குறித்துப்
பின்னால் வரும் அத்தியாயங்களில் மற்ற வளர்ச்சிகள்
குறித்துப் பேசும்போது சொல்கிறேன். இந்த இடத்தில் ஒரு
முக்கியமான விஶயத்தைக் கவனிக்க வேண்டும். கதை மற்றும்
நிகழ்ச்சிகள் ஏதும் இல்லாமல் வெறும் ஒரு சூழ்நிலையை
மட்டும் வைத்து எடுக்கப்படும் படங்கள் இலக்கியச்
சார்புடையவையாய் இருந்தன என்ற குற்றச் சாட்஼டலிலிருந்து
தப்பின. ஆனால் அதனால் பயன்கள் மட்டுமல்லாமல் ஆபத்~
தான போக்குகளும் விளைந்தன. அதாவது சிறுசிறு சுவையான
விள஗்கங்களுக்கு முக்கியத்துவம் கொடுக்கும் படங்கள் அதிக~
மாயின. ஆனால் இப்படங்களின் மொத்தக் கதை என்பது
மிகச் சாதாரணமானதாகவும், முக்கியத்துவமில்லாததாகவும்
இருந்தது.</p>

<p>கலைகளில் ஏற்பட்ட பல்வேறு போக்குகளைப் போலவே இப்
போக்குக்கும் தத்துவார்த்த காரணம் ஒன்று இருந்தது.
தப்பித்தல் என்கின்ற வாதம் ஆரம்பத்திலிருந்தே
முதலாளித்துவ சினிமாவின் ஒரு பிராதனப் போக்காக
இருந்தது. அந்த அடிப்படையிலேயே இந்தப் புதிய போக்கும்
ஏற்பட்டது. மாயக் கதைகளிலும், கற்பனையான வினோத
தீரச்செயல்களிலும் ஆரம்பத்தில் சினிமா தப்பித்தது.
இப்போதோ யதார்த்தப் படங்கள் என்ற பெயரில் சிறு சிறு
விளக்கங்களில் தப்பிக்கிறது. முழு உண்மையைக் கண்டு
எப்போதுமே பயந்த இந்தச் சினிமா, நெருப்புக் கோழியைப்
போல் தன் தலையை, உண்மையின் சிறு துகள்கள் என்ற
மணலில் புதைத்துக் கொண்டது.</p>

<p>பக்கம்:103
                  மாறுகின்ற நிலைப்பாடு</p>

<p>                           9</p>

<p>மாறுகின்ற நிலைப்பாடு இது சினிமாக் கலையின் இரண்டா~
வது மாபெரும் படைப்பு ரீதியான வழிமுறையாகும்.
இந்த விஶயத்திலும் சினிமாவானது மற்றக் கலைகளின்
கோட்பாடு மற்றும் வழிமுறைகளிலிருந்து மாறுபடுகிறது.
நாடகத்திலோ நாம் எல்லாவற்றையும் ஒரு குறிப்பிட்ட
பார்வையோட்டத்திலிருந்தும், கோணத்திலிருந்தும் அதாவது
ஒரே குறிப்பிட்ட கண்ணோட்டத்திலிருந்தும் பார்த்தோம்.
படம் பிடிக்கப்பட்ட நாடகத்தாலும் இதை அவ்வளவாக
மாற்ற முடியவில்லை. ஒரு காட்சிக்கும் இன்னொரு காட்சிக்~
கும் இடையே கோணத்தையும், கண்ணோட்டத்தையும்
மாற்றியதே தவிர, ௃௃ஒரு காட்சிக்குள்ளேயே அவைகளை
மாற்ற முடியவில்லை. பல்வேறு ஓவியங்களை எடுத்துக்
கொண்டால் ஒவ்வொரு ஓவியத்திற்கும் வெவ்வேறு குறிப்~
பிட்ட கண்ணோட்டம் உண்டு. அது அதற்கே உரிய படைப்~
பியல் மற்றும் கலாரீதியான அம்சமாகும். ஆனால் ஒரு
தனிப்பட்ட ஓவியத்தின் கண்ணோட்டம் என்பது ஒன்று
தான். அது மாற்ற முடியாதது. இதன் காரணமாக அப்~
படத்தில் உள்ள உருவம் ஒரு குறிப்பிட்ட உறுதியான
வடிவத்தைக் கொண்டிருக்கும். அந்த உருவம் உணர்ச்சி
பூர்வமான பாவத்தைக் கொண்டிருக்கலாம். ஆனால் அந்த
பாவமானது அந்த உருவத்தின் வடிவத்தைப் போலவே
மாறாத ஒன்றாகும். ஆனால், சினிமாவிலோ மாறுகின்ற
நிலைப்பாடு (Changing Set-up) காரணமாக இவைகளும்
மாறும்.</p>

<p>இந்த மாறுகின்ற நிலைப்பாடு காரணமாகத்தான், சினிமா~
வானது ஒரு கலையாக மாறியது. பொருள்களின் அசைவிற்~
கேற்ப காமிராவும் அசைவதால்தான் இது சாத்தியமானது.</p>

<p>பக்கம்:104
வெறும் ஒளி அமைப்புகளால் மட்டும் இது சாத்தியமாயிருக்~
காது. இல்லையெனில், சினிமாவானது வெறும் இயந்திர
ரீதியான மறுபதிப்பாகவே இருந்திருக்கும்.</p>

<p>பட பிம்பங்களின் தொகுப்பு</p>

<p>நோக்கம் மற்றும் விஶயங்களை ஒன்றாகத் தொகுப்பது
தான் எல்லாக் கலைக்குமான அடிப்படைத் தன்மையாகும்.
இந்த அடிப்படைத் தன்மையை, திரையில் பார்க்கின்ற பிம்~
பங்களுக்கு தருவது, மேலே சொன்ன மாறுகின்ற நிலைப்~
பாட்டுக்கேயுரிய தனித்தன்மைகள்தான். எல்லா கலைப்~
படைப்புகளும் வெறும் புற உண்மையை மட்டும் காட்டுவ~
தில்லை, கூடவே அதைப் படைத்த கலைஞனது உள்ளத்தை~
யும் காட்டுகிறது. இந்தக் கலைஞனுடைய உள்ளம் என்பது,
அவன் உலகைப் பார்க்கின்ற விதம், அவனுடைய தத்துவம்,
அவனுடைய காலகட்டத்துக்கேயுரிய சாத்தியப்பாடு ஆகிய~
வற்றை உள்ளிட்டதாகும். இவைகள் எல்லாம் ஒரு படத்தில்
இயற்கையாகவே பதிந்திருக்கக்கூடிய விஶயங்களாகும். ஒவ்~
வொரு படமும் உண்மையின் ஒரு பகுதியை மட்டும் காட்டுவ~
தோடு அல்லாமல் ஒரு குறிப்பிட்ட கண்ணோட்டத்தையும்
காட்டுகிறது. காமிராவின் கோணம், காமிராவுக்குப் பின்~
னால் இருக்கக்கூடியவரின் உள்நோக்கத்தையே மாற்றக்
கூடியது.</p>

<p>ஒரு உருவத்தைப் பார்த்து ஒரு கலைஞன் அப்படியே ஒரு
சிறிதும் மாறாமல் ஒரு கலைப்படைப்பை வரைந்தால்கூட,
அந்தக் கலைப்படைப்பு அந்த உருவத்தை மட்டும் பிரதி~
பலிக்கவில்லை, கூடவே வரைந்த கலைஞனையும் பிரதிபலிக்~
கிறது. ஒரு ஓவியன் தன்னுடைய ஓவியங்களின் மூல஼ம்
தன்னையே பல வழிகளில் காண்பித்துக் கொள்கிறான்.
அவன் தேர்ந்தெடுக்கிற வண்ணம், வடிவமைப்பு, மற்றும்
தீட்டுகின்ற தன்மை எல்லாமே தான் சொல்ல விரும்புகிற
விஶயத்தை எந்த அளவுக்குச் சொல்கிறதோ, அதே அளவு
அவனைப் பற்றியும் சொல்கிறது. திரைப்படக் கலைஞனின்
தன்மையோ அவன் தேர்ந்தெடுக்கிற காமிராக் கோணங்~
களில்தான் வெளிப்படுகிறது. அந்தக் காமிராக் கோணங்கள்
தான் ஒரு ஶாட்டுக்கான வடிவமைப்பையும் தருகிறது.</p>

<p>பக்கம்:105
பொருளின் பல்வேறு கோணங்கள்</p>

<p>     எந்த ஒரு பொருளையும், அது மனிதனோ, மிருகமோ,
இயற்கைத் தோற்றமோ அல்லது கலைப்படைப்போ எதுவாக~
யிருந்தாலும் சரி, அதை நாம் எந்தக் கோணத்திலிருந்து
பார்க்கிறோமோ அந்த அடிப்படையில்஼தான் அதன் உருவ~
மும் அமையும். இந்த அடிப்படையில் ஒவ்வொரு பொருளுக்~
கும் ஆயிரம் உருவங்கள் உண்டு. இவ்வாறு ஆயிரம் முறை~
க஼ளில் பல்வேறு உருவங்களைக் கொண்டிருந்தாலும் நாம்
பார்க்கக்கூடிய உருவம் என்னவோ ஒன்றுதான். அவைகள்
ஒன்றுக்கொன்று ஒத்துப்போகக்கூடியதாக இல்லாத போதி~
லும், உருவம் என்னவோ ஒன்றுதான். ஆனால் அவைகள்
ஒவ்வொன்றும் வெவ்வேறு விதமான கண்ணோட்டங்களை~
யும், விளக்கங்களையும், பாவங்களையும் கொண்டிருப்ப~
தாகும். காட்சி ரீதியான ஒவ்வொரு கோணமும் ஒவ்வொரு
மனப்போக்கைக் காட்டுகிறது. இது அதிகப் புறத்தன்மை
உடையது அல்லது அகத்தன்மை உடையது என்பதெல்லாம்
அர்த்தமற்றது.</p>

<p>தெஜா ஊ</p>

<p>பார்வையாளன், ஒரு படத்தைப் பார்க்கும் போது, அதை
இனங்கண்டு கொள்ளுதலில் அவனுடைய சொந்தக் கண்~
ணோட்டமும் அடங்கியுள்ளது. இதன் காரணமாகச் சிலக்
குறிப்பிட்ட கோணங்கள் மீண்டும் வரும்போது, அந்த
சினிமாவில் உள்ள படத்தை அவன் இதற்கு முன் எப்போதோ
பார்த்திருப்பதாக உணர்வு ஏற்படுகிறது. ஒரு முகமோ அல்~
லது நிலப்பரப்போ நாம் எப்போதோ உண்மையில் பார்த்~
தது போன்று நம் மனதில் மீண்டும் தோன்றும். இல்லையெனில்
அதே மனஉணர்வை இப்போது நமக்குத் தராது. அதே
நேரத்தில், குறிப்பிட்ட கோணங்களை மீண்டும் பார்க்கும்
போது அது நம் மனதில் கடந்தகால உணர்வுகளைத்
தூண்டுவதாக இருக்கும். அதன் காரணமாக இதை `முன்பு
எப்போதோ நாம் பார்த்திருக்கிறோம்' என்ற உளவியல்
ரீதியான உணர்வு நமக்குக் கிடைக்கும்.</p>

<p>பக்கம்:106
நானும், ஆல்ஃபிரட் ஏபெலும் சேர்ந்து வெகு
நாட்களுக்கு முன்பு பெர்லினில் நார்காஸிஸ்
என்ற படத்தை எடுத்தோம். அந்தப் படத்தில், கதாநாயகன்
நீண்ட நாட்களுக்குப் பிறகு ஒரு பெண்ணை மீண்டும் பார்க்கி~
றான். அவளை இப்போது அவனால் நினைவுகூற முடிய~
வில்லை. அந்தப் பெண் அந்த அளவுக்குப் பெரிதும் மாறிவிட்டி~
ருக்கிறாள். அவனால் இப்போது அவளை ஒரு துளியும்
அடையாளங்கண்டு கொள்ள முடியவில்லை. ஆனால் அந்தப்
பெண்ணோ அவள் யாரென்பதை அவனுக்குச் சொல்ல
வில்லை. மாறாக, அவர்கள் இருவரும் சந்தித்த ஒரு முக்கிய~
மான சூழ்நிலையை மீண்டும் அவள் அதே மாதிரி உருவாக்கு~
கிறாள். முன்பு அவர்கள் இருவரும் சந்தித்த அதே இடத்தில்
அதே நாற்காலியில் அதே நெருப்புக்கு முன்னால் அதே
மாதிரி உ஠்காருகிறாள். அவனையும் அவன் முன்பு உ஠்~
கார்ந்த அதே நாற்காலியில் உட்கார வைக்கிறாள். அந்த
நாட்களில் அந்த நாற்காலியில் அமர்ந்து கொண்டுதான்
அவன் அவளையே பார்த்துக்கொண்டிருப்பது உண்டு. முன்பு
இருவரும் முதன் முதலாக சந்தித்த அதே சூழ்நிலையை அப்~
படியே உருவாக்குகிறாள். இப்போது நாம் முன்பு பார்த்த
அதே காட்சியைச் சினிமாவில் பார்க்கிறோம். இந்தக் காட்சி
கதாநாயகனுக்கு மீண்டும் பழைய நினைவை உண்டாக்கும்
என்பதில் பார்ப்பவர்களுக்கு எந்தச் சந்தேகமும் எழவில்லை.</p>

<p>இனங் காணுதலைக் குறித்து
இன்னும் சில விஶயங்கள்</p>

<p>நாம் படத்தில் பார்க்கும் ஒவ்வொரு பொருளின் பாவயிய~
லும் உண்மையில் இரு பாவயியல்களின் தொகுப்பாகும்.
ஒன்று பொருளுக்கேயுரிய பாவயியலாகும். அதற்கும் பார்வை~
யாளனுக்கும் சம்பந்தமில்லை. இன்னொன்று, பார்வையாள~
னுடைய கண்ணோட்டத்தையும் படத்தினுடைய கண்~
ணோட்டத்தையும் பொருத்து அமைவது படத்தில் இந்த
இரண்டும் ஒன்றோடொன்று நன்கு பழக்கப்பட்ட கண்ணால்
மட்டுமே இரண்டு கூறுகளுக்கும் இடையேயான வித்தி~
யாசத்தைக் கண்டுபிடிக்க முடியும். காமிராமேன் பல்வேறு
குறிக்கோள்களுக்காகத் தன்னுடைய கோணத்தைத் தேர்ந்~
தெடுக்கலாம். படத்தில் காட்டப்படும் பொருளின் உண்஼மை~
யான முகத்தைக் காட்ட வேண்டும் என்பதற்கு அவர்</p>

<p>பக்கம்:107
அர்த்தம் கொடுக்கலாம். அது மாதிரியான நேரங்களில்,
பொருளின் சரியான தன்மையைக் காட்டவேண்டும் என்ற
குறிக்கோளோடு வடிவமைப்பைத் தேர்ந்தெடுக்கலாம் அல்லது
காட்டப்படும் பொருளைப் பற்றிய சரியான மன உணர்வைப்
பார்வையாளன் மனதில் எழுப்ப வேண்டும் என்பதில் அக்கறை
கொண்டிருக்கலாம். அது மாதிரியான நேரங்களில், பயந்து
போன மனிதனைக் காட்ட வேண்டும் எனில், அந்த மனிதனை
ஒரு மோசமான கோணத்தில் பயந்த உணர்வை வெளிப்~
படுத்தும் வகையில் காட்டலாம். அல்லது மகிழ்ச்சியான
மனிதன் ஒருவன் உலகத்தைப் பார்ப்பதைக் காட்ட வேண்டு~
மெனில், அவன் பார்க்கின்ற பொருள்களை அழகிய, ரசிக்கக்~
கூடிய கோணத்தில் காண்பிக்கலாம். இதன் மூலம் கதாபாத்~
திரங்களின் நிலைப்பாடோடு மட்டுமல்லாமல், கதாபாத்திரங்~
களின் மன நிலையோடும் பார்வையாளன் உணர்வு ஂரீதியாக
இனங்காண முடியும் காமிராவின் நிலைப்பாடும், கோணமும்
ஒரு பொருளை விரும்பக் கூடியதாகவும், வெறுக்கக் கூடிய~
தாகவும், பயன்படக்கூடியதாகவும், அல்லது சிரிக்கக் கூடிய~
தாகவும் மாற்றக் கூடியது.
         
               0 
</p></body></text></cesDoc>