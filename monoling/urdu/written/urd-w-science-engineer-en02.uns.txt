<cesDoc id="urd-w-science-engineer-en02.uns" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-science-engineer-en02.uns.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-01</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>K.K.T.</h.title>
<h.author>P.H.U.</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1963</pubDate>
</imprint>
<idno type="CIIL code">en02.uns</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 62\   .</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-01</date></creation>
<langUsage>Urdu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>
<p>
                        6(63) 1

                       6چوتھا باب 

            6خلا مےں سفر کے لےے ضروری شرائط

	آج جب ہم ےہ سنتے ہےں کہ روس ےا امرےکہ نے 
مصنوعی سےارے خلا مےں چھوڑے ہےں، تو ہمارے دل 
سےارے بنانے والوں کی عظمت کے قائل ہو جاتے ہےں
اور ہم ان کی دماغی اور علمی صلاحےتوں کا فراخ دلی 
سے اعتراف کرتے ہےں۔ ےہ اےک قدرتی امر ہے، لےکن 
ں اس شخص کو بھی فراموش نہےں کرنا۔ چاہےے
جس نے آج سے تےن سو سال پہلے خلا مےں سفر کے
امکانات اور اس کی شرائط کی وضاحت کر دی تھی۔ 
وہ شخص انگلستان کا مشہور سائنس دان نےوٹن تھا۔ اس
نے اپنی اےک تصنےف رےاضےات قوانےن مےں بڑی صفائی 
سے ان شرائط کا ذکر کےا تھا، جن کے مطابق کسی چےز 
کو اےک مقررہ مدار مےں قائم کےا جس کےا جا سکتا ہے۔ اس نے 
2                                                                        
1.Principia Mathematica.                         
 
                  6(63) 2

اس سلسلہ مےں راکٹ کے ذرےعہ دھکا لگانے کے امکان
اور اس کے اصول کا بھی ذکر کےا ہے۔ اس کے علاوہ
دوری حرکت پےدا کرنے کے لےے حسالی مقداروں کا 
بڑی صحت کے ساتھ اظہار کےا ہے۔ اس نے اس پر ہی 
اکتفا نہےں کےا ہے، بلکہ دوری حرکت کے سلسلہ مےں ہوا
کی مزاحمت کا بھی جائزہ لےا ہے اور ےہاں تک لکھ دےا 
ہے کہ اگر کوئی جسم زمےن کے گرد گردش کرتے
ہوئے کسی بڑے ابھار (پہاڑی سلسلہ وغےرہ) پر سے 
گزرے گا توکشش ثقل کی تبدےلی کے باعث اپنے مدار
سے ہٹ جائے گا۔ غرض مصنوعی سےاروں سے متعلق اس 
نے قرےب قرےب سب ضروری باتےں بےان کر دی تھےں۔
صرف اس قدر کام باقی تھا کہ عملاََ اےک سےارہ مدار مےں 
چھوڑ دےا جاتا۔ اب سے تےن سو سال پہلے ان تمام امور 
کی پےش بےنی کرنا ظاہر کرتا ہے کہ نےوٹن کو بھی 
قدرت نے نہاےت اعلٰی دماغی صلاحےتوں سے نوازا تھا۔

0نےوٹن کے کلےات 

	ہم مےں سے اکثر نے ےہ مشاہدہ کےا ہوگا کہ 
جب کسی گاڑی کو دھکا لگانا مقصود ہو، تو ہمےں 
ابتدائََ بڑی قوت صرف کرنی پڑتی ہے۔ لےکن جب وہ اےک
دفعہ حرکت مےں آجائے، تو پھر تھوڑی طاقت صرف
 
                    6(64) 3

کرکے بھی ہم اس کی حرکت کو جاری رکھ سکتے
ہےں۔ کچھ دور چلنے کے بعد اگر اس کو روکنا مقصود</p>

<p>               0 
</p></body></text></cesDoc>