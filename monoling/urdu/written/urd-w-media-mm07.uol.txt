<cesDoc id="urd-w-media-mm07.uol" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-media-mm07.uol.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-02</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Ishtahariyat</h.title>
<h.author>Dilshad</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1991</pubDate>
</imprint>
<idno type="CIIL code">mm07.uol</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 148/.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-02</date></creation>
<langUsage>Urdu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>								60(148) 1</p>

<p>کے لےے کام کر رہا ہے تو اس کا بھی اس ادارے کے ملازمےن پر بہت 
اچھا اثر ہوتا ہے۔ اس طرح ادارہ اپنے ملازموں کی ہمدردےاں اور بھرپور
تعاون حاصل کرنے مےں کامےاب ہوجاتا ہے جو کہ ادارے کے حال کا 
اثاثہ اور اچھے مستقبل کی ضمانت ہوتا ہے۔</p>

<p>0(4) اچھے کارکنوں کے اصول :</p>

<p>		 تشہےر کی وجہ سے اداروں کو اچھے کارکن اور اچھے منظم مل 
جاتے ہےں۔ کام کرنے والے اور کاروباری لوگ عموماََ مشہور اور جانے 
پہچانے اداروں کے ساتھ کام کرنا پسند کرتے ہےں۔ ےہ قدرتی چےز ہے کہ 
اےسے لوگ جب زےادہ تشہےر کرنے والے اداروں مےں کام کرےں گے تو 
انھےں اس کے اشتہار دےکھ کر فخر اور خوشی محسوس ہوگی۔ پھر انھےں 
ےہ بھی اطمےنان ہوگا کہ اےسے ادارے مےں جو تشہےر کرتا ہے، ان کی 
ملازمت نسبتاََ مستقبل اور ان کی ترقی کے امکانات زےادہ ہوتے ہےں۔</p>

<p>0جنگ کے دوران  تشہےر :</p>

<p>		 دوسری جنگ عظےم اور اس سے قبل کی عالمی چپقلش کی تارےخ مرتب
کرنے والے مورخےن کو مختلف دفاعی منصوبوں اور دوسری حب وطن 
سرگرمےوں کے سلسلے مےں تشہےر کے اہم کردار کو فراموش نہےں کرنا 
چاہےے۔ جنگ کے دوران امرےکی کاروباری اداروں کی تشہےر کے ذرےعے 
رےڈےو اور دوسرے اداروں کو دس کروڑ ڈالر سے زےادہ آمدنی ہوئی اس 
وقت امرےکہ مےں تقرےباََ اےک سو تشہےری مہمےں شروع ہوئےں جن سے 
27 سرکاری اداروں کے تعاون سے شروع کی گئےں۔ ےہ مہمےں جنگی بونڈ 
فروخت کرنے، عوام کو کم راشن استعمال کرنے پر تےار کرنے اور 
دھاتوں، کاغذ اور دوسری ضروری اشےا کی بچت کرنے اور دشمن تک 
اہم معلومات نہ پہنچانے کے متعلق تھےں۔</p>

<p>		 دوسری جنگ عظےم کے دوران بہت سی کمپنےوں نے معمول کے مطابق 
اپنی تشہےری مہمےں جاری رکھےں حالانکہ اس وقت ان کے پاس فروخت
کرنے کے لےے کچھ بھی نہےں تھا۔ انھوں نے اس وقت اپنے اشتہارات 
 
									60(154)7 </p>

<p> کو حقےقی تسکےن بخشے گی۔ سننے والوں مےں رےڈےو پر موسےقی کے اسباق 
کا سلسلہ شروع کرکے موسےقی کی کلاسوں کے ذرےعے موسےقی کی سوجھ
اور ذوق پےدا کےا جاتا ہے۔ اس طرح موسےقی کی خصوصےات کے مکمل 
علم کے بعد لوگوں کی موسےقی سے زےادہ تسکےن کی جاسکتی ہے۔ اسی 
طرح تشہےر کے ذرےعے اشےا مےں موجود خصوصےات کی نشاندہی کرکے ان 
کی تسکےن مےں اضافہ کےا جاسکتا ہے۔</p>

<p>		 اسی طرح اےک اےسی معےشت مےں جہاں نئی مصنوعات اور پوشےدہ 
خصوصےات والی مصنوعات کی کثرت ہے، وہاں ےہ مصنوعات عوام مےں 
اسی وقٹ مقبول ہوسکتی ہےں اگر انھےں ان کی خواہشات کو تسکےن 
بخشنے والی خصوصےات سے آگاہ کےا جائے۔ وہ مصنوعات جو عوام کو 
اس صورت مےں پےش کی جائےں کہ انھےں ان خواہشات کو تسکےن بخشنے 
والی خصوصےات کا علم نہ ہو تو عوام مےں مقبول نہےں ہوسکتےں تاہم 
تشہےر کے ذرےعے عوام کو اشےائے فروخت کی خصوصےات سے آگاہ کرکے 
اشےا کی افادےت مےں اضافہ کےا جاسکتا ہے۔</p>

<p>0صارفےن کا خسارہ :</p>

<p>		 تشہےر مےں بلا شبہ پہلے سے زےر استعمال اشےا سے حاصل ہونے والی
تسکےن مےں اضافہ کرنے کی قوت موجود ہے۔ ےہ بہت سے صارفےن مےں 
اےسی اشےا خرےدنے کی خواہش پےدا کرنے کی طاقت بھی رکھتی ہے جو ان 
کے پاس موجود نہےں، ےا جنھےں وہ ناکافی قوت خرےد کی بنا پر خرےدنے کی 
پوزےشن مےں نہےں ہےں۔ اس طرح ان مصنوعات کو خرےدنے کی ہمت نہ 
رکھنے والوں مےں بے اطمےنانی پےدا ہوسکتی ہے۔</p>

<p>		 تشہےر پر کےا جانے والا اےک قدرے سنجےدہ اور حقےقت پسند انہ 
اعتراض ےہ ہے کہ اس کے انسانی خواہشات مےں شدت پےدا کردےنے والے
اثر کی وجہ سے اشےا کی مادی افادےت ختم ہونے سے پہلے ہی ان کی افادےت 
ختم ہوجاتی ہے۔ رےاست ہائے متحدہ امرےکہ کے اےک صنعت کار نے اس 
خواہش پر بار بار زور دےا ہے کہ لوگوں کے پاس جو کچھ موجود ہے</p>

<p>							60۔۔۔۔۔۔۔۔۔۔۔۔۔</p>

<p></p>

<p></p>

<p>
               0 
</p></body></text></cesDoc>