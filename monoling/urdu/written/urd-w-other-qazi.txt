<cesDoc id="urd-w-other-qazi" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-other-qazi.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-01</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>****</h.title>
<h.author>****</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - ****</publisher>
<pubDate>****</pubDate>
</imprint>
<idno type="CIIL code">qazi</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page ****.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-01</date></creation>
<langUsage>Urdu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>
مرجع رقم : 786
                                               التارےخ :93/10/30 </p>

<p> 
     0                         شھادۃ زواج</p>

<p>      0                   (الی من ےھمہ الامر)</p>

<p>
انا، محمد اجمل، قاضی منطقۃ علی فارہ، اشھد بموجب ھذا ان السےد/عمران خان بن ملک 
افتخار خان، المقےم فی بنفلو رقم آر63/، غولےستان۔ئی۔ جوھار، طرےق الجامعہ الرئےسی،
کراتشی رقم 32، پاکستان، قدرنزوج منالانسہ/شےبا خان بنت اقابل خان، المقےمہ فی 206/3
اےہ انصار باغ کولونی شمشاد مارکےت علی قارہ و ذلاک بتارےخ 22 سبتمبر 1993 وذلک علمی
مھر ھو 000،50 ربےہ (مون الف ربےہ فقط)</p>

<p>0والشھود علی النکاح کماےلی :</p>

<p>1۔ السےد/فےصل خان بن السےد/اقابل خان المقےم فی 206/3، اےہ ، انصار باغ کولونی، 
   سمشاد مرکےت، علی قارہ</p>

<p>2۔ السےد/بابلو بن السےد/اےس احمد خان المقےم فی ناےی کی مندی 63/17، تشوراھا 
   حسےنی، اقرا</p>

<p>3۔ السےد/سراج الاھی بن السےد/ احسان الاھی المقےم فی مانےک تشوک علی قارہ </p>

<p></p>

<p></p>

<p>0                                             القاضی محمد اجمل</p>

<p> 0                                             قاضی المنطقہ
  0                                             علی قارہ</p>

<p>
               0 
</p></body></text></cesDoc>