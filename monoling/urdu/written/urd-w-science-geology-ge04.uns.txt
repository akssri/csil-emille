<cesDoc id="urd-w-science-geology-ge04.uns" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-science-geology-ge04.uns.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-01</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Tasaur</h.title>
<h.author>Husain</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1974</pubDate>
</imprint>
<idno type="CIIL code">ge04.uns</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 500/524.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-01</date></creation>
<langUsage>Urdu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>                                 (500) 1</p>

<p>سنگ رےگ (sandstone) اور سلےٹ مےں پاےئ جانے والے تانبے کے ذخائر
بناتے ہےں۔ لےگون اور جھےلوں مےں کثرت سے انجارات کے سبب نمک تہ نشےن ہو 
#جاتا 
ہے اےسے معدنی ذخائر  کو پرت دار (sedimentary) کہتے ہےں۔</p>

<p>	تحلےل ہونے والے معدنےات پانی مےںگھل کر زےر زمےن آب کی طرف 
مائل ہوتے ہےں اور موٹے رےزوں والی چٹانوں کے درمےان، کنکری اور بجری کی 
#شکل 
مےں تہ نشےن ہو جاتے ہےں۔ لوہے اور مےنگنےز دھات کی بجری اور تانبے کی 
#دھات 
ےسے معدنےات کو کنکری 
#دار
(concretionary) معدنےات کہتے ہےں۔</p>

<p>	برنموز خائر Exogenous deposits مےں وہ پرائمری ذخائر (صرف غےر 
دھاتوی) بھی شامل ہےں جو درختوں ےا جانورں کے ذرےعہ ہوا سے حاصل کردہ 
#مادے سے 
بناےئ جاتے ہےں۔ مثال کے طور پر کاربن جو کہ کاربن ڈایئ آکسائڈ کا اےک حصّہ 
#ہے۔ ان مےں 
خشکی کے دلدلی حصّوں، ساحلوں، جھےلوں اور سمندروں مےںتہ نشےن ہونے والے
پےٹ (peat)، لگنائٹ (lignite)، کوئلہ اور تےل اےسے معدنےات شامل ہےں
سورج کی گرمی سے درختوں کی جڑےں پتّے اور شاخےں مٹی ےں کل کرپےٹ 
#(peat)
اور کوئلہ مےں بدل جاتی ہےں اور سمندری خود رو پودوں اور جانوروں سے 
#معدنی 
تےل بنتا ہے۔</p>

<p>	فوسفورائٹ (Phosphorite) اور گوانوں (Guano) بھی اےسے پرائمری 
برنمو (exogenous) ذخائر ہےں جو زمےن کی سطح کے اوپر بنتے ہےں۔ ان ذخائر 
#کو بڑے
جناور بناتے ہےں۔ بڑے جانور اپنے کھانے مےں سے فوسفورس جمع کرلےتے ہےں اور 
#اسکو 
گوبرکی شکل مےں باہر نکلتے ہےں۔
 
                                  (501)</p>

<p>	معدنی چشمے بھی معدن کا مخرج کہے جا سکتے ہےں۔ در اصل معدنی 
#چشمے التہاب 
کی اےسی پےداوار ہےں جےسی دوسری کچ دھات، اس لےئ معدنی چشموں کو بھی 
#پرائمری 
ذخےرہ کہا جا سکتا ہے۔ اس کے بر عکس سطح زمےن کے پانی سے بنے چشموں 
(vadore springs) مےں معدنی رےزہ پرت دار چٹانوں سے ٹوٹ کر شامل ہےئ ہےں 
#ان کو ثانوی 
ذخےرہ کہا جاےئ گا مثلاََ نمک کے چشمے۔</p>

<p>	معدنےات کی قدرتی تقسےم (Natural Distribution fo Minerals) 
ہم جان گےئ ہےں کہ معدنی ذخائر ان تحرےکات (processes) کی دےن ہےں جو 
#قشر ارض 
کے داخلی اور خارجی حصّوں مےں کار فرما ہےں ےعنی معدنی ذخائر کی تشکےل 
#درنمودار اور 
برنمو ارضےاتی قوتوں کا تنےجہ ہےں۔ علاوہ ازےں ےہی ارضےاتی قوتےں قشر کی 
#ساخت، بناوٹ 
اور اس کے خط و خال کی تعمےر کرتی ہےں۔ اس کا مطلب ےہ ہوا کہ سطح زمےن 
#پر اہم اور 
کار آمد ذخائر کی تقسےم اےک خاص ضابطہ کی پابند ہے۔ کسی علاقے مےں کون 
#سے 
معدنےات ملےں گے، اس کو جاننے کے لےئ ضروری ہے کہ زمےن کی بناوٹ اور ساخت 
کے سلسلوں سے واقفےت پےدا کی جاےئ اور پتہ لگاےا جاےئ کہ کس طرےقے سے 
#معدنےات کی کھوج کی جاتی ہے۔</p>

<p>	ہم جانتے ہےں کہ سطح زمےن کے خط و خال مختلف قسم کے ہےں۔ بر اعظموں 
#پر 
الپائن پہاڑی سلسلے، پلےٹو، اوسط اونچایئ کے پہاڑ، پہاڑےاں اور مےان پھےلے
ہوےئ ہےں۔ ارضی خط و خال کی ےہ رنگارنگی اور نمو اور برنمو قوتوں نے بنایئ 
#ہے
	
	قشرارض کی تحرےکات سے پہاڑ، پلےٹو اور نشےب بنے۔ ان تحرےکات کے دوران 
زمےن کے اندرونی حصّے سے لاوا نکل کر پرت دار چٹانوں مےں پےوست ہو جاتا ہے 
جس سے مختلف جسامت کے تداخل اور لاوے کی رگوں (veins) کا اےک جال سا 
بن جاتا ہے بسا اوقات پرت دار چٹانوں کو توڑ کر لاوا باہر نکل آتا ہے جس سے
 
                                   (502) 3</p>

<p>اتش فشانی ہونے لگتی ہے۔ گوےا سطح زمےن پر لاوے کے دھارے بہہ نکلتے ہےں 
#اور 
ٹف (tuff) کی پرتےں تہ نشےن ہو جاتی ہےں۔</p>

<p>	زمےن کی داخلی قوتوں سے پےدا ہونے والے ےہ اونچے نےچے علاقے خارجی 
قوتوں (مثلاََ بہتا ہوا پانی، ہوا وغےرہ) کے عمل سے اےک دوسرے سے جدا ہو جاتے 
#ہےں۔
ارضی خط و خال کی شکلےں تبدےل ہونے لگتی ہےں اور تراش خراش سے  ہموار 
#ہو جاتی ہےں
ےہ خارجی قوتےں اےک طرف انتھک محنت کر کے زمےن کے خط و خال کو نےچا کرنے 
#مےں 
لگی رہتی ہےں اور دوسری طرف مختلف قسم کی دےرایئ مٹی، رےت کی پرتےں، 
#گل
سلٹ، کنکری اور چونے کے پتھروں کو جھےلوں اور سمندروں ےں تہ نشےن کر کے 
نےیئ  
چٹانوں کو جنم دےتی ہےں۔ ہم جانتے ہےں کہ خارجی اور داخلی قوتےں مل کر 
#مفےد ذخائر تہ نشےن 
کرتی ہےں۔ کسی ملک کے قشر کی بانوٹ سے اس مےں پائے جانے والے معدنےات کا 
پتہ لگاےا جا سکتا ہے اور ےہ بھی معلوم کےا جا سکتا ہے کہ ان معدنےات کی 
#تقسےم
کی ترتےب کےا ہے۔</p>

<p>	0قشر ارض کی پچی کاری (Mosaic of the Earth's Crust) :0 فولڈ پہاڑ
عام طور سے جےو سنکلائےن اےک اےسے لمبے چوڑے نشےب کو 
کہتے ہےں جو خشکی مےں ےا سمندری کناروں پر واقع ہوتا ہے۔ ان نشےبوں مےں 
#قرب و 
جوار کے درےاوں ےا سمندری لہروں کے عمل سے لسٹ اور مٹی آکر تہ نشےن ہوتی 
رہتی ہے۔ اےک خاص مدّت کے بعد زےادہ مٹی اور سلٹ کا وزن پڑنے سے نشےبوں 
کا فرش نےچے کی طرف دھنسنے لگتا ہے اور جب نشےب رسوبات (sediments) 
سے پر ہو جائے تو پہاڑوں کی تعمےر ہوتی ہے۔ پہاڑوں کی اس طرح تشکےل کے 
وجو ہات کا ابھی تک مکمل  علم نہےں ہو سکا ہے اور اس موضوع پر بحث مباحثہ
 
                                      (503) 4</p>

<p>کی بہت گنجائش ہے۔ تاہم اس مےں کویئ شک نہےں کہ پرت دار چٹانوں کی 
#پرتےں انہےں
جےوسنکلائےن مےں تہ نشےن ہوئےں اور پرتوں مےں پےچےدہ فولڈ پڑنے پر پہاڑوں 
#کی 
شکل اختےار کر گئےں۔ چونکہ جےو سنکائےن کا فرشی حصّہ لچکدار اور نےچے کی 
#طرف اتنی گہرایئ 
تک دھنس جاتا ہے کہ نےچے کی بہت اونچے درجہ حرارت والی چٹانوں سے جا ملتا 
ہے۔ ان گرم چٹانوں سے وصل ہونے پر نےچے کی پرت دار چٹانےں پگھل جاتی ہےں 
#اور
لاوا پرت دار چٹانوں مےں مختلف جسامتوں مےں داخل ہو جاتا ہے۔ اس عمل سے 
معدنی ذخےرہ، معدنی رگےں اور آتش فشاں وجود مےں آئے</p>

<p>	پہاڑی سلسلوں کے مطالعہ سے معلوم ہوتا ہے کہ فولڈوں کی تعمےر کے ساتھ 
ساتھ پرتوں مےں مےگما (magma) بھی پےوست ہوتا رہتا ہے۔ اور چونکہ مےگما 
دھاتوں کا پرئمری مخرج ہے اس لےئ تعمےر کوہ کے دوران مختلف قسم کی کچ 
#دھات 
(oresA) کے ذخائر وجود مےں آتے ہےں جن مےں مےگمایئ (magnatic) اور آبی 
#درجہ 
حرارتی (Hydrothermal) ذخائر شامل ہےں ےعنی پرائمری اور زےادہ گہرائی پر 
پےدا ہونے والے ذخائر (Primary and deep stav)۔</p>

<p>	دےوسنکلائےن سے بنے ہوےئ بلند پہاڑوں مےں کس قسم کے معدنےات پاےئ 
جاتے ہےں؟ اس سوال کا جواب علاقہ مخصوص کی عمر معلوم کرنے پر دےا جا سکتا 
#ہے۔
اگر پہاڑی سلسلے بہت بعد کے بنے ہوےئ ہےں اور حال ہی مےں اوپر اٹھے ہےں تو 
#ان مےں 
کم گہرے آبی درجہ حرارتی (hydrothermal) اور آتش فشانی (بشرطےکہ اس 
#علاقے 
مےں آتش فشانی ہوئی ہو) ذخائر دستےاب ہوں گے۔ اگر ےہ بلند علاقے کچھ 
#زےادہ 
پرانے ہےں تو ان پر خارجی قوتےں اپنی کارےکگری دکھا چکی ہوں گی اس لئے 
#ان مےں 
مزکورہ ذخائر کے علاوہ اوسط گہرایئ کے آبی درجہ حرارتی ذخائر ملےں گے۔ بہت 
پرانے پہاڑی سلسلے جن پر باہری قوتوں نے شدےد تراش خراش کی ہو ان مےں
 </p>

<p>                               (504) 5	</p>

<p>مسطح کے پاس ہی بہت گہرایئ پر جانے والے نہ صرف آبی درجہ حرارتی ذخائر 
#ملےں گے 
بلکہ اےسے پہاڑوں مےں نےومےٹولائٹی (Pneumatolitic) اتصالی اور مےگمایئ ذخائر 
#بھی 
ملےں گے۔ داخلی قوتوں سے تہ نشےن ہونے والے مزکورہ ذخائر کے علاوہ ان مختلف 
#عمر 
کے پہاڑی سلسلوں مےں کچھ برنمو معدنی ذخائر بھی پاےئ جاتے ہےں۔ مثال کے 
#طور پر 
پرت دار لوہے اور تانبے کی کچ دھات، کوئلہ اور فوسفورئٹ کی پرتےں بھی ساز 
#گار
حالات مےں تہ نشےن ہو جاتی ہےں۔</p>

<p>	زمےن کی تارےخ سے معلوم ہوتا ہے کہ قرےباََ سبھی بر اعظموں مےں اونچے 
#پہاڑوں
کی تعمےر مختلف ادوار مےں ہویئ ہے کےونکہ وقت کے ساتھ جےوسنکلائےن اپنا 
#مقام بدلتے 
رہے ہےں۔ اونچے پہاڑ عمر کے ساتھ خارجی قوتوں کے اثر سے کٹنے چھٹنے لگے اور 
#نسبتاََ
نےچے ہوگےئ۔ تراش خراش کے اثر سے اونچے پہاڑ اوسط اونچایئ کے پہاڑوں مےں 
اور اوسط اونچایئ سے نےچی پہاڑےوں کی شکل اختےار کر گئے۔ ےہ نےچی پہاڑےاں 
#بھی کٹ
چھنٹ کر پےنی مےدان (Peneplain) بن گئےں۔ ان پہاڑوں کے کسی نہ کسی پہلو 
#مےں 
نےا جےو سنکلائےن وجود مےں آےا جس کو قرب و جوار کے رسوبات (sediments) 
#نے
پر کردےا اور اس طرح پھر نےئ پہاڑی سلسلے پےدا ہوتے رہے جن کا وہی حشر ہوتا 
#رہا۔</p>

<p>	بّر اعظم ےورپ مےں قدےم ترےن آرکےن (Archaean) اور پروٹےروزواک 
(proterozoic) عہدوں کی بالٹک شےلڈ کٹ چھنٹ کر پےنی مےدان کی شکل اختےار 
#کر گیئ 
ہے۔ ےہ ےورپ کا سب سے پرانا علاقہ ہے اور اس کو آرک ےورپ (Arch Europe) 
کہتے ہےں۔ بالٹک شےلڈ کے پےنی مےدان کے مغربی حصّے مےں پےلوزواک دور 
کی کےلےڈونےن سائکل کے اونچے پہاڑ ان مےں شامل ہےں۔ بر اعظم ےورپ کے اس 
#حصّے کو 
پےلو ےورپ (Palaeo Europe) کہتے ہےں۔ اس علاقے کے جنوب مےں پولےنڈ سے
 
                               (505) 6</p>

<p>فرانس اور اسپےن تک ورسےن سائکل (Variscian Cycle)  پہاڑی سلسلے ہےں جو
پےلوزواک عہد کے بعد کے حصّے مےں بنے تھے۔ ےورپ کے اس حصّے کو مےسو ےورپ 
(Meso-Europe) کہتے ہےں۔ ےورپ کے سب سے جنوب کے حصّے بحر روم کے 
اس پاس اےشےا مائنر، کوہ قاف، بلکان، کار پےتھےن، آلپس، پےر نےز، (Pyrenees)
اپےنائےن (Apenine) اور اٹلس پہاڑ ہےں۔ ےہ پہاڑ مےسوزواک اور 
کےنوزواک عہدوں کی الپائےن سائکل کے دوران وجود مےں آئے تھے۔ اس کو نےو 
ےورپ (Neo Europe) کہتے ہےں۔</p>

<p>	اس طرح ےورپ کے موجودہ پہاڑوں کو جنم دےنے والی جےوسنکلائنی پےٹی 
#شمال 
سے جنوب کی طرف منتقل ہوتی رہی ہے۔ جےو سنکلائنوں کے انتقال کے ساتھ
ےگمایئ عمل (جس سے دھات کے ذخائر تہ نشےن ہوتے ہےں) بھی تبدےل ہوتا 
#رہا۔
 علاوہ ازےں گہرائی پر دبے ہوےئ معزنےات کو خارجی قوتےں تباہ کر کے دوسرے 
علاقوں مےں منتقل کر تہ نشےن کرتی رہےں۔ ان قوتوں کی تراش خراش اور تہ 
#نشےنی 
سے ثانوی ذخائر وجود مےں آئے۔</p>

<p>	لےکن پہاڑوں کی تشکےل صرف جےو سنکلانوں ہی تک محدود نہےں ہے۔ جےو 
سنکالئےن مےں تہ نشےن ہونے والے  ذخائر کی پرتوں سے قشر ارض پر دباوئ بڑھتا
جاتا ہے ےہ دباوئ دوسرے علاقوں کو بھی متاثر کرتا ہے۔ مثال کے طور پر جدےد 
سنکلائےن کی پرتوں کے دباوئ کا اثر قدےم جےو سنکلائےن پر بھی پڑتا ہے جو 
#پہاڑ کٹ 
جھنٹ کر نےچے ہو گےئ ہےں ےہ ابھرے ہوےئ پہاڑی حصّے کم لچکدار ہوتے ہےں 
#کےونکہ
ان مےں پہلے ہی فولڈ پڑ چکے ہوتے ہےں۔ اکثر اوقات ان فولڈوں کی پرتوں مےں 
#مےگما، 
آتشی چٹناوں کی صورت مےں پےوست ہو جات ہے۔ اس لےئ قدےم پہاڑوں 
(قدےم جےوسنکلائنوں) مے معمولی فولڈ پڑ تے ہےں اور بہت سی چگہوں پر انمےں
 
                                     (506) 7</p>

<p>شگاف اور دراڑےں پڑجاتی ہےں، بہت سی پرت دار چٹانےں اپنی جگہ سے منتقل 
#ہو جاتی 
ہےں۔ بہت سی پروتں کے سلسلے ٹوٹ کر اےک دوسرے کے اوپر چڑھ جاتے ہےں 
#علاوہ 
ازےں بہت سے حصّے ہارسٹ پلےٹو کی شکل مےں ابھرتے ہےں اور بہت سے علاقے 
نشےبوں مےں منتقل ہو جاتے ہےں۔ چٹانی پرتوں کے دبائ کی وجہ سے زمےن کے 
#اندرونی 
حصّوں سے مےگما اوپر کی طرف ابھر آتا ہے اور چٹانوں مےں پےوست ہو کر نئے 
#معدنی 
ذخائر، معدنی رگوں اور اوپری سطح پر پہنچ کر آتش فشانوں کو جنم دےتا ہے۔ 
#اس عمل کے
وران ابتدائی معدنےات کے ذخائر، معدنی رگوں اور اوپری سطح پر پہنچ کر آتش 
#فشانوں کو جنم دےتا ہے۔ اس عمل کے 
دوران ابتدائی معدنےات کے ذخائر وجود مےں آتے ہےں اور فرسودہ چٹانوں سے ان 
کے سخت حصّے جھےلوں اور سمندروں مےں منتقل ہو جاتے ہےں جن سے ثانوی 
#ذخائر 
تہ نشےن ہو جاتے ہےں۔</p>

<p>	قشرارض کے اےسے قدےم ترےن حصّے جن مےں پہاڑوں کی تعمےر پری 
#کےمبرےن دور مےں 
ہوئی تھی اور اس کے بعد ان مےں بھی وسےع پےمانے پر فولڈ نہےں پڑے قدےم 
پلےٹ فارم (PlatformA) کہلا تے ہےں۔ ان قدے، پلےٹ فارموں کے وہ حصّے جن پر 
#کبھی بھی سمندر کا 
پانی نہےں پھےلا ہے شےلڈ (Shield) کہلا تے ہےں۔ شےلڈ در حقےقت قدےم پلےٹ فار 
کے اےسے حصّوں کو کہتے ہےں جہاں پر فولڈ سازی کے آخری مرحلہ (stage) سے لے 
#کر 
آجتک پرتوں مےں استحکام رہا ہو اور ان پر کبھی بھی سمندر کا پانی کے نےچے 
#ڈوبتے اور ابھرتے 
رہے ہےں۔ اس لئے پلےٹ فارموں پر پرن دار چٹانوں کی اےک موٹی پرت تہ نشےن 
ہوگیئ ہے اس بالائی پرت مےں معمولی قسم کے فولڈ پڑ گئے ہےں اور زےادہ تر 
#چٹانےں 
افق کے متوازی حالت مےں پھےلی ہویئ ہےں۔ پلےٹ فارم کے اےسے حصوں کو پلےٹ 
Plates کتہے تہں۔</p>

<p>	پلےٹ فارم برّ اعظموں کے وسطی حصے مےں پاےئ جاتے ہےں۔ ےورےشےا
 
                                  (507) 8</p>

<p> (Eurasia) مےں روس کا پلےٹ فارم ہے۔ بالٹک اور ےوکرےن اسی پلےٹ فارم
کی شےلڈ ہےں۔ سائبرےا پلےٹ فارم انابار (Anabar) اور الدان (Aldan) 
شےلڈ ہےں۔ سائبرےا پلےٹ فارم کی سرحدوں پر ےنسی رج (yeniseri ridge)، 
#مشرقی
سےان اور بےکال کے وانچے حصّے ہےں۔ شمالی چےن مےں سنےان (Sinian) پلےٹ فارم 
ہے، اس پلےٹ فارم مےں بہت معمولی شےلڈ ہےں۔ اےشےا کے جنوب اور جنوب مغرب 
مےں ہندوستان اور عرب کی شےلڈ ہےں۔ امرےکہ مےں شمالی امرےکہ پلےٹ فارم ہے، 
کنڈا جس کی شےلڈ ہے۔ جنوبی امرےکہ مےں جنوبی امرےکہ نام کا پلےٹ فارم ہے
برازےل شےلڈ اسی پلےٹ فارم کا حصح ہے۔ افرےقہ بر اعظم اےک وسےع پلےٹ فار ہے 
بر ازےل شےلڈ اسی پلےٹ فارم کا حصہ ہے۔ اسی طرح اسٹرلےال بھی اےک پلےٹ فارم 
#ہے جس کے 
قرق مےں اےک عظےم شےلڈ ہے تقرےباََ پورا برّ اعظم انٹارکٹک اےک پلےٹ فارم ہے 
جس پر اےک قدےم وسےع شےلڈ موجود ہے۔ک</p>

<p>	جن شےلڈوں پر تراش خراش کا کام زےادہ ہوا ہے، ان کی اوپری سطح پر 
بہت زےادہ گہرایئ مےں پائے جانے خاص مےگما کے پرائمری ذخائر، صدور 
#(emanations)
اور آبی درجہ حرارتی (Hydrothermal ) ذخائر عےاں ہو جاتے ہےں۔ پری 
#کےمبرےن عہد 
مےں بننے والی متغےر چٹانےں اور ذخائر بسا اوقات سطح زمےں پر آشکار ہوےئ 
#ہےں۔ ےہ بھی 
ممکن ہے کہ اسی شےلڈ کے دوسرے بہت سے حصّے ثانوی اور پرت دار چٹانوں سے 
ڈھکے ہوےئ ہوں جو کہ پرائمری چٹانوں پر عمل فرسود گی اور تراش خراش 
#ہونے پر تہ نشےن 
ہوتی ہےں۔ ان پرت دار چٹانوں کے ساتھ، جھےلوں اور سمندورں مےں لوہے اور 
کوئلے کی پرتےں بھی تہ نشےن ہو جاتی ہےں۔ اگر شےلڈ پر آتش فشاں پھوٹ 
#پڑے
تو آتش فشانی معدنےات کے ذخےرے بھی شےلڈ کی سطح پر مجتع ہو جاتے ہےں۔</p>

<p>	اگر کسی پلےٹ فارم پر مختلف ادوار کی پرت دار چٹانےں پھےلی ہویئ ہےں
 
                                (508) 9</p>

<p>اور ان پرت دار چٹانوں مےں شدےد فولڈ نہےں پڑے ہےں تو اےسی چٹانوں مےں
پرائمری چٹانوں کے فرسودہ مادے سے بننے والے ثانوی ذخائر مل سکتے ہےں لےکن
عام طور پر پلےٹ فارم پر پرت دار چٹانوں والے ذخائر (Sedimentary deposits)
جےسے کوئلہ، لوہے کی کچ دھات، فوسفورائٹ، مختلف قسم کے نمک اور معدنی تےل 
جو کہ ان سمندروں، جھےلوں اور لےگونوں (lagoonsa) مےں تہ نشےن ہوےئ 
#جنہوں 
نے پلےٹ فارم کو مختلف ادوار مےں زےر آب کےا۔ پلےٹ فارم کے کچھ حصّوں مےں 
جہاں پر اس کی جڑ (root) مےں پایئ جانے والی قدےم چٹّانےں عرےاں ہوگیئ 
#ہوں 
وہاں شےلڈ مےں پاےئ جانے والے معدنےات بھی ملتے ہےں۔</p>

<p>اسی وجہ سے ہمارے برّا اعظم ارضےاتی تارےخ کے مختلف ادوار کی سچچی 
#کاری کا 
نتےہ ہےں اور ان کی الگ الگ بناوٹ ہے۔ بہت زےادہ کٹی پھٹی اور مستحکم شےلڈ 
#کے 
پاس ہی نسبتاََ کم مستحکم پلےٹ فارم جو مختلف ادوار کی اےسی پرت دار 
#چٹانوں سے 
ڈھکے ہوےئ ہےں جن کی پرتوں کے سلسلوں مےں ٹوٹ پھوٹ، خلل اور فرےکچر 
#بہت کم 
ہویئ ہےں اور ساتھ ہی بہت ہی ناپائدار جےوسنکلائےن، (geosynclines) جو 
اونچے پہاڑوں مےں تبدےل ہو چکے ہےں اور جن مےں مختلف گہرایئ تک تراش 
#خراش ہویئ 
ہے بھی ملتی ہےں۔ معدنےات کے ذخائر اس پچی کاری (mosaica) مےں چٹانوں 
#کی 
مختلف بناوٹ و تارےخ History کی بنا پر پائے جاتے ہےں۔ اس طرح کسی ملک 
کی ارضےاتی ساخت کو معلوم کر کے ہم ےہ پتہ لگا سکتے ہےں کہ اس مےں کون 
#کون سے 
معدنی ذخائر کن کن جگہوں پر ملےں گے۔</p>

<p>	0روس مےں معدنےات کی جغفرافےایئ تقسےم :0 روس دنےا کی خشکی کا
چھٹا حصّہ ہے۔ ےہ ےورپ اور اےشےا مےں پھےلا ہوا ہے۔ اس کی وسےع جسامت اور 
بڑے رقبہ کی ارضےاتی تارےخ رنگارنگ ہے اور ےہاں کی ارضےاتی ساخت بھی
 
                                (509) 10</p>

<p>طرح طرح کی ہے۔ اس رنگا رنگ ساخت مےں بہت سی قسم کے معدنےات پائے جاتے 
ہےں۔ ذےل مےں مختصر طور پر روس کے معدنےات کی جغرافےائی تقسےم بےان کی 
#گئی ہے۔</p>

<p>	ےورپےن روس مےں بالٹک شےلڈ کا شمال مشرقی حصّہ کر ےلےا اور کولا 
#(Karelia and Kola)
جزےرہ نما مےں پھےلا ہوا ہے۔ باقی ےورپےن روس کے زےادہ تر حصے پر روس 
کا پلےٹ فارم پھےلا ہوا ہے۔ اس پلےٹ فارم کے جنوب مےں پری کمےبرےن عہد کی 
#ےوکرےن 
شلےڈ پھےلی ہے۔ ےوکرےن در اصل اےک شےلڈ نہےں ہے بلکہ قدمے پلےٹ فارم کی 
#بنےاد کا 
ےک حصّہ ہے جو ابھر کر تراش خراش کے عمل سے عرےاں ہو گےا ہے۔ ےوکرےن 
#شےلڈ کے 
اوپر ٹرشری اور پےلوزواک ٔہد کی چٹانےں اور پہاڑی سلسلے پھےلے ہوےئ ہےں۔ 
#ےوکرےن کی 
اس شےلڈ کے ساتھ ساتھ ڈون بےسن مےں پےلوزواک جےو سنکلاےئن کی پرتےں 
#پھےلی ہویئ 
ہےں جس کے اےک حصّہ  مےں شدےد فولڑ سازی ہویئ ہے۔ اس کے سب سے جنوبی 
#حصّے 
مےں کرےمےا (Crimea) اور کوہ قاف پہاڑوں کے سلسلے ہےں۔ ےہ پہاڑی سلسلے 
#ٹرشری 
دور کے نےوسنکلاےئن سے بنے ہےں جن مےں الپائن قسم کے فولڈ پڑے ہےں۔</p>

<p>	پورپےن روس کے معدنی ذخائر بےشتر مذکورہ علاقوں کی مختلف ساختوں 
#مےں ذےل 
ترتےب مےں پھےلے ہوےئ  ہےں۔</p>

<p>	بالٹک شےلڈ کے زےادہ کے زےادہ ترشےدہ حصّے مےں مےگمایئ اور متغّےر معدنی 
#ذخائر پائے جاتے
ہےں۔ ےہاں کی متغےر پرت دار چٹانوں مےں کورٹزائٹ اور ہمےٹائٹ لوہا پاےا جاتا 
#ہے۔ گہری 
رگوں Venis مےں پائےرائٹ (تانبہ، نکل، جستہ) اور سطح کے قرےب کی رگوں مےں 
#ابرق
فےلسپار اور آرکےن سلےٹ مےں گارنٹ پاےا جاتا ہے۔ کےروسک (Kirovsk) 
شہر کے پاس اپے ٹائٹ اور قےمتی پتھر جدےد پےلوزواک کے تداخلوں (intrusions) 
#مےں 
پاےئ جاتے ہےں۔ تراش خراش کے بعد جھےلوں اور دلدلوں مےں بھی لوہے کے 
#ذخےرے،
ک 
                                (510) 11</p>

<p>تہ نشےن ہوگئے ہےں۔ کرےلےن شنگائٹ (Karelian Shungite) کوئلہ کی اےک قسم 
ہے جو قدےم ادوار مےں متغےر ہو گےا تھا۔</p>

<p>	روسی پلےٹ فارم کا بےش تر حصّہ پرت دار چٹانوں سے ڈھکا ہوا ہے۔ ےہ پرت 
#دار
چٹانےں مختلف ارضےاتی ادوار کی ہےں اور ان کی پرتوں کی ترتےب مےں معمولی 
#ٹوٹ پھوٹ، 
ہٹاوئ اور فولڈ پڑے ہےں۔ علاوہ ازےں ان مےں تداخل (instrusions) بھی کم 
#ہوےئ ہےں
اس لئے ان مےں گہرایئ پر پائی جانے والی کچ دھاتےں نہےں ملتےں۔ روسی پلےٹ 
#فارم مےں 
درجہ ذےل معدنےات پائے جاتے ہےں۔ ماسکو اور پےچورا بےسن مےں کوئلہ، کےرو 
#(Kirov)
تولا (Tula)، لی پےٹسک (Lipetsk) اور نوگو روڈ (Novgorod) کے آس پاس مےں 
لوہا، ےورال کے مختلف مغربی حصّوں مےں کاما (Kama) درےا سے والگا کی زےرےں 
#وادی 
(Lower volga) تک، کےسپےن ساگر کے اےمبا (Emba) ضلع مےں شمالی کوہ قاف 
#مےں 
گروژنی (Grozny) اور مےکوپ (Maikop) مےں تےل نکالا جاتا ہے۔ چٹانی اور 
متفرق نمک، سولکما سک (Salikmask)، باخمت (Bakhmat)، الےٹسکےا
(lletskaya) اور ژاشےتا (Zashchita) مقامات سے حاصل کئے جاتے ہےں۔
باکسائےٹ (المونےم کی دھات)، ٹی خون (Tikhun) سے نکالا جاتا ہے۔ زےرےں والگا
(Lower volga) اور سواش (Sivash) کے علاقے مےں جھےلوں سے نمک حاصل کےا 
جاتا ہے فوسفورائٹ کے مختلف ادوار کے ذخےرے ہےں۔ کرسک (Kursk) مےں
پرت دار چٹانوں کے نےچے پری کےمبرےن پلےٹ فارم مےں لوہے کی متغےر دھات ملی 
#ہے۔
اےسی ہی متغےر لوہے کی کانےں ےوکرےن کے گرئوروگ (krivoi Rog) کے پاس بھی 
ہےں۔ پری کےمبرےن کے اسی علاقہ مےں ابرک (mica) اور فےلسپار (Felspar)
کی مےگماٹائٹ، اور گرےفائٹ (graphite) کے ذخائر اور اس کے اوپر ٹرشری
(Tertiary) دورکی پرت دار چٹانوں مےں نےکوپول (Nikopol) کے قرےب بھورا کوئلہ
 
                                 (511) 12</p>

<p>اور مےنگنےز (manganese) کی پرت دار کچ دھات پائی جاتی ہےں۔</p>

<p>	ڈونٹزبےسن (Donetz Basin) کے ترشےدہ جےوسنکلائےن مےں کار بونےفےرس 
#دور
کی چٹانوں مےں کوئلے کے بھنڈار موجود ہےں۔ ان کوئلے کی پرتوں کے اوپر کری 
#ٹےشےس اور 
ٹرشری ادوار کی پرت  دار چٹانےں پھےلی ہوئی ہےں۔ ان پرت دار چٹانوں سے 
#سفےد کھرےا مٹی
(white chalk)، سےمنٹ مارل (Cement marl9) اور ٹری پولائٹ کے ذخےرہ 
نکالے جاتے ہےں ان پرت دار چٹانوں مےں کہےں کہےں مےگما داخل ہو گےا ہے ہس 
#کے سبب
وگولنسل رج (Nogolnvridge) مےں چاندی اور نےکےٹوکا (Nikitouka) سے پارہ 
نکالا جاتا ہے۔</p>

<p>	ےورال کے کٹے پھٹے جےوسنکلائےن مےں کئی مرتبہ اور کیئ مقامات پر پرت 
#دار 
چٹانوں مےں زےرےں مےگما پےوست ہوا ہے۔ اس لئے ےورال مےں بہت سے مےگمائی 
#اور
آبی درجہ حرارتی معدنےات کے ذخائر پائے جاتے ہےں۔ ان معدنےات مےں سونا، 
#پلےٹےنم،
مقناطےس، تائتےنک (Titanic) اور کرومی لوہا، ٹنگ سٹن (Tungsten) نکل 
#(Nickle) 
تانبہ Copper، تانبہ پائےرائٹ (copper pyrite)، ابرق، زمرو (		emeralds)،
مےگنے سائٹ اور الےس بےسٹس (asbestos) قابل ذکر ہےں۔ علاوہ ازےں ےورال پہاڑ 
#کی 
پرت دار چٹانوں مےں مےنگنےز اور کوئلہ کی کانےں بھی موجود ہےں۔</p>

<p>	کوہ قاف (جن کی تشکےل بعد مےں ہوئی ہے) مےں بہت سے مقامات پر پرت 
دار چٹانوں مےں مےگما داخل ہو گےا ہے اور گئے ہےں۔ ان وجوہات سے کوہ قاف 
#مےں کئی مقامات 
سے سونا، چاندی، سےسہ، جستہ، مولےبڈنم، ٹنگسٹن اور لوہے کی معدنےات نکالی 
جاتی ہےں ازربایئ جان رےاست کے باکو (Baku) مقام سے تےل حاصل کےا جاتا 
ہے اور مغربی جور جےا کی پرت دار چٹانوں سے مےنگےنز اور کوئلہ نکالا جاتا ہے۔
 
                                     (512) 13</p>

<p>	سائبےرےا مےں اوبے درےا اور ےورال کے بےچ مےں اےک بڑا نشےب موجد ہے اس 
نشےب مےں حال کی (recent) پرت دار چٹانےں پھےلی ہوئی ہےں۔ جن کے نےچے 
#پرانی
چٹانےں ہےں جن مےں شاےد مےگمایئ داخلہ ہےں (intrusionsa) جن کا پتہ صرف 
#گہرے سوراخوں 
(Bore-holes) کے ذرےعہ چلا ہے۔ اوپر پائی جانے والی پرت دار چٹانوں مےں پےٹ 
Peat رےت اور گل کے ذخےرے پھےلے ہوئے ہےں۔</p>

<p>	اوبے درےا کے جنوب مےں قزاکستان کی نےچی پہاڑےاں ہےں۔ ان پہاڑےوں کی 
ساخت بہت پےچےدہ ہے۔ پری کےمبرےن عہد کے بعد ےہ علاقہ پےلوزواک 
#(Palaeozic)
ےوسنکلائےن بنا رہا جو کےلےڈ ونےن (Caledonian) دور مےں پہاڑوں کی شکل 
#مےں ابھرا۔
بلند پہاڑ تراش خراش ہونے سے کٹ چھنٹ کر بہت نےچے ہوگےئ۔ اس کے بعد ےہاں
کی زمےن نےچے کی طرف دھنسی جس سے پےلوزواک عہد کے بالائی حصے مےں 
#ےہاں اےک
وسےع اور چھچھلا نشےب بن گےا۔ اس نشےب مےں بہت سے چھوٹے چھوٹے جچھےہ 
#جو 
کےلےڈونےن دور کے پہاڑوں کے باقی ماندہ حصّہ تھے پاےئ جاتے تھے۔ ورسےن 
(Varscian) دور مےں ےہ جےوسنکالئن پہاڑ کی شکل اختےار کر گےا تھا لےکن وقت 
گزرنے پر ےہ پہاڑ بھی تراش خراش کے عمل سے نےچے ہوگےئ۔ قزاکستان کے سا 
#حصّے
مےں مےسوزواک عہد مےں بہت سی جھےلےں تھےں اور ٹرشری دور مےں اسٹےپی 
#کے علاقے 
پر سمندر کا پانی پھےل گےا تھا۔ الپائنی سائےکل کے دوران اس علاقے مےں شگاف 
#پڑے 
اور چٹانوں کے سلسلوں مےں ٹوٹ پھوٹ ہوئی۔ فرےکچر  ہونے پرتےں اپنی 
#جگہوں
سے سرک کر اےک دوسرے کے اوپر چڑھ گئےں۔ تاہم اس حصّے مےں کوہ تعمےر 
قوتوں کے اثر کی شدت بہت معمولی رہی جس کی وجہ سے مشرقی حصے کے 
#علاوہ ےہ علاقہ اونچے
پہاڑوں کی صورت اختےچر نہ کرسکا۔ نےچے علاقے کے خط و خال کو عوامل ترش 
#خرش نے
جلد ہی کاٹ چھانٹ دےا۔ چونکہ قدےم جےو سنکلائےن مےں  مقامات پر
 
                                    (513) 14</p>

<p>مےگما داخل ہوگےا تھا اس لےئ قزاکستان کی پرت دار چٹانوں مےں سونا، ٹن، 
ٹنگسٹن
تانبہ، چاندی، جستہ اور سےسہ معدنی رگوں مےں ملتا ہے اور اتصالی متغےر 
چٹانوں
مےں لوہا، تانبہ  پاےا جاتا ہے علاوہ ازےں جو رےسک ٔہد کا کوئلہ، آبی نمک اور 
جسپم بھی 
ملتا ہے اور چٹانوں کے فرسودہ ذخائر تہ نشےن ہوتے سے کہےں کہےں پرتوں مےں 
المونےم کی 
کچ دھاتےں بھی ملی ہےں۔</p>

<p>	اسےٹپی کے مشرق مےں جنوبی سائبےراد سے لےکر بےکال جھےل تک الٹایئ سان 
ے بلاک 
فولڈ Block folded  پہاڑ پھےلے ہوئے ہےں۔ الٹایئ سان پہاڑوں کی تعمےر فولڈدار 
قدےم
کےلےڈونےن اور مغربی علاقہ مےں ورسےن (Variscian) پہاڑوں کے مقام پر ہوئی 
ہے۔
الٹایئ سان پہاڑوں مےں بھاری مقدار مےں درنمو endogenous معدنےات پائے 
جاتے ہےں۔ پلےسر (Placer)، سونا، ٹنگسٹن، مولےبڈنم Molybodenum,تانبہ، 
چاندی، سےسہ، جےستہ معدنی رگوں کی شکل مےں پاےا جاتا ہے۔ پری کےمبرےن 
عہد کا 
لوہا کوارٹزائٹ (iron quartxite)، اےس بےسٹےس (asbestos)، گرےفائٹ 
(graphite)، نےفرائٹ (Naphrite)، اتسالی لوہا (contact iron)، تانبہ، 
تانبہ پائےرائٹ، زمرد، فرسودہ المونےم کی کچ دھات اور کہےں کہےں ٹرشری دور 
کی کوئلہ کی 
پرتےں پھےلی ہویئ ہےں۔ ان پہاڑوں مےں موجود کزنےٹسک  (Kuxnetsk) اور 
مےنوسنسنک 
(Minusinsk) نشےبوں مےں کار بو نےفرس، پرمےن اور جور ےسک ادوار کا کوئلہ 
پاےا جاتا 
ہے۔ مےنوسنسک (Minusinsk) نشےب مےں کوئلے کے علاوہ جھےلوں مےں تہ نشےن 
ہونے والا نمک اور پےلوزواک عہد کی عرےاں چٹانوں مےں لوہا اور تانبہ بھی پاےا 
جاتا ہے۔</p>

<p>	سائبےرےا پلےٹ فارم روسی پلےٹ سے اس معنی مےں مختلف ہے کے اس کے 
پےلوزواک عہد کی مےگما پےوست چٹانوں کے اوپر مےسوزواک عہد اور ٹرشری دور 
کی 
 
                                     (514) 15</p>

<p>پرت دار چٹانےں نہےں پھےلی ہویئ ہےں۔ سائبےرےا پلےٹ فارم مےں لوہا، تانبہ، 
#نکل، پلٹنےم اور 
پےلوزواک عہد کے سمندری معدنی ذخائر موجود ہےں۔ نمکےن معدنی چشمے، 
#چٹانی نمک، تےل، 
کوئلے کی پرتےں، جورےسک دور کا جلنے والا شےل (Shale) ارکوٹسک (Irkutsk) اور 
ولوئی (Vilyui) بےسن مےں پاےا جاتا ہے۔ےنسی رِج اور انابار رِج مےں جہاں جہَاں
پری کےمبرےن عہد کی چٹانےں سطح پر عُرےاں ہو گئی ہےں اِن مےں سونے کے 
#ذخائر ملے ہےں۔</p>

<p>	0بےکال کا پہاڑی علاقہ :0 جھےل بےکال کے مغرب مےں بےکال کے مغرب مےں بےکال 
#کا اوُنچا پہاڑی 
لاقہ ہے۔ بےکال جھےل کے کنارے پاٹو مسک وِٹم  (patomsk Vitim) کی پہاڑی 
پری کےمبرےن عد کی ہے۔ ےہ چٹانےں تراش تراش کے سبب بہت گہرائی تک کٹ
کر عرےاں  ہو گئی ہےں۔ اِن عرےاں چٹانوں مےں بہت سی جگہ مےگمائی تداخل 
#دکھائی دےتے 
ہےں۔ جن شگافوں سے ہارٹ پلےٹےو اور گسل بنے ان مےں مےگما پےوست ہو گےا  
#ہے۔ ےہ
نشےب(پاٹو مسک وِٹم) جورےسک دور مےں بنے تھے جن مےں بعد مےں پانی بھر 
#گےا تھا
اور وہ جھےل  کی شکل اختےار کر گئے تھے۔ بےکال جھےل  کے مغربی حصے کے پہاڑی 
#علاقے کی 
قدےم چٹانوں مےں سونا، اتصالی اور متغےر قسم کا لوہا، تانبہ، ابرق اور فےلسپار (felspar)
پاےا جاتا ہے۔ جےورےسک اور ٹرشری ادوار کے ذخائر مےں کوئلہ کی پرتےں مےوجود ہےں
اس کا جنوبی حصہ پےلوزواک عہد کی تعمےر کردہ پہاڑےوں سے ملتا ہے۔ اس حصّے سے 
#ٹنگسٹن، 
ٹن، مولےبڈنم، سونا، چاندی اور سےسےہ بھی نکالا جاتا ہے۔</p>

<p>0	الڈان شےلڈ (Aldan Shield) :0 اونچے پہآری علاقے کے مشرق مےں 
اےک پےنی مےدان ہے۔ اس پےنی مےدان کی چٹانےں آرکےن عہد کی ہےں۔ ان 
#چٹانوں پر 
کےمبرےن عہد مےں پانی پھےل گےا تھا۔ باےں ہمہ جورےسک دور کی جھےلوں کے 
#ذخائر بھی 
ان قدےم چٹانوں پر پھےلے ہوےئ ہےں۔ بعد کے ادوار مےں ان چٹانوں مےں مےگما 
پےوست ہو گےا تھا جس کے سبب ان پر توں مےں سونے کی دھات ملتی ہے۔ الڈان 
 </p>

<p>                                      (515) 16</p>

<p>شےلڈ کی زےرےن آرکےن عہد کی چٹانوں مےں لوہا، ابرق، کورنڈم (Corundum) اور 
بالائی جورےسک دور کی چٹانوں مےں کوئلے کے ذخےرہ پاےئ جاتے ہےں۔</p>

<p>	0بےکال جےھل کے اس پار کے مشرقی حصّے (Eastern Transbaikal Area) :
 اس حصّے کی ساخت بہت پےچےدہ ہے۔ پری کےمبرےن عہد کی چٹانوں کے کٹاوئ 
کے بعد ےہاں پےلوزواک عہد کے جےوسنکلائن وجود مےں ائے۔ اس جےوسنکالئن مےں 
کےلےڈونےن اور سےن (Variscian) کی تحرےکات کے دوران فولڈ پڑے اور پہاڑ
ابھرے۔ ان پہاڑوں کی جگہ مےسوزواک عہد مےں اےک نشےب پےدا ہوگےا تھا اور 
#سمندر 
کا پانی اس نشےب مےں بھرا گےا تھا جس کے سبب ےہ علاقہ اےک لمبی کھاڑی کے 
مانند 
نظر آتا تھا۔ ےہ کھاڑھی جورےسک تحرےک کے دوران جےھلوں مےں منقسم ہوگیئ 
#تھی۔
بحرالکاہلی سائکل کے دوران ےہاں کے پری کےمبرےن، پےلوزواک عہدوں کے ذخائر 
اور ٹرےسک اور جورےسک ادوار مےں زبردست فولڈ سازی ہوئی۔کککککککککککککککک</p>

<p>	المختصر بےکال جھےل کے مشرقی حصے مےں کیئ مرتبہ پہاڑوں کی تعمےر 
#ہویئ اور ہر مرتبہ
پرت دار چٹانوں مےں مےگما پےوست ہوا۔ بعد کی ارضےاتی تحرےکات کے دوران اس
علاقے مےں زبردست آتش فشانی ہویئ اور ےہاں کے آتش فشاں ٹرشری اور 
#کواٹرنری
ادوار مےں ھی لاوا نکالتے رہے۔</p>

<p>	بےکال جھےل کے مشرقی حضّے کی پےدےدہ ارضےاتی تارےخ کے پےش نظر ےہاں 
#بہت قسم 
کے معدنی ذخائر مجتمع ہوگےئ ہےں۔ مثال کے طور پر ےہاں اتصالی لوہے 
#(contact iron)
کے بھنڈار، مےگمایئ رگوں مےں ابرق، بےش قےمتی پتھر، ہےرے جواہرات، ٹن، 
#ٹنگسٹن 
اور مولےبڈنم پاےئ جاتے ہےں۔ علاوہ ازےں آبی درجہ حرارتی (Hydrothermal)
درجہ حرارتی مےگمائی رگوں مےں سونا، تانبہ، چاندی، سےسہ، جستہ، اےنٹی اور 
#پارہ 
پاےا جات ہے۔ جورےسک اور کری ٹےشےس ادوار کی جھےلوں مےں تہ نشےن ہونے 
#والی
 
                                (516) 17</p>

<p>چٹانوں مےں کوئلہ اور چٹانوں کے فرسودہ و ترشےدہ ملبے سے تہ نشےن ہونے والی 
#چٹانوں 
مےں سونا، ٹنگسٹن اور ٹن بھی ملتا ہے۔</p>

<p>	درےائے امور کے علاقے اور شمالی مشرقی سائبرےا کی تارےخ بھی بہت پےچےدہ 
ہے ےہاں پری کےمبرےن  عہد کی چٹانوں کے اور پر بہت سی جگہ پےلوزواک اور 
#مےسوزواک 
عہدوں کی چٹانےں  پھےلی ہویئ ہےں۔ علاوہ ازےں مشرقی حصے مےں ٹرشری 
#دور کی 
چٹانےں بھی تہ نشےن ہےں۔ بعد کے عہدوں ( پےلوزواک اور مےسوزواک) مےں تہ 
#نشےن 
ونے والی پرتوں مےں زبردست فولڈ پڑے ہےں اور جگہ جگہ پرتوں مےں مےگما 
#داخل 
ہو گےا ہے۔ جس کے سبب اس علاقے مےں بھی مختلف ادوار کے معدنی ذخےرے
 
                               (517) 18</p>

<p>پھےلے ہوےئ ہےں۔ مثال کے طور پر پری کےمبرےن، پےلوزواک، مےسوزواک عہدوں 
#اور ٹرشری 
دور کے سونے کے ذخےرے ےہاں پائے جاتے ہےں۔ پری کےمبرےن عہدکا کوارئٹزائٹ
(Quartzite) لوہا 	Iron  اور گرےفائٹ (graphite)، ٹرشری، تانبہ، چاندی، جستہ، 
سےسہ اور اےنٹی منی اور براعظمی ذخائر مےں مےسوزواک عہد اور ٹرشری دور 
#کا کوئلہ اور تےل پاےا 
جاتا ہے۔</p>

<p>	وسطی سائبےرےا کے حصے کی معدن ارضےاتی تارےخ کو دو حصّوں مےں تقسےم 
#کےا جا سکتا 
ہے۔ امور درےا کے مغرب مےں ترک مےنےا (Turkmenia) کے مےدان ہےں۔ اس 
ےدان کے جنوب مےں کوپٹ داغ (Kopat Dagh) پہاڑ ہے۔ ےہ پہاڑ در اصل کوہ 
#قاف
پہاڑی سلسلے کا حصّہ ہےں۔ مےدان کے شمال مےں اُست اُرت (Ust-Urt) کا پلےٹو 
#ہے
ان علاقوں مےں مندرجہ ذےل معدنےات پایئ جاتی ہےں۔ نابٹ داغ (Nabit Dagh)
مےں تےل پاےا جاتا ہے کثرت تےل کے سبب اس پہاڑ کو تےل کا پہاڑ کو تےل کا پہاڑ 
#بھی کہتے ہےں۔ چےلےکن 
جزےرہ  نما (Cheleken) مےں اورزوکےرائٹ (zokeriate) نکالا جاتا ہے۔ کوپٹ داغ 
(Kopet Dagh ) کی وادی مےں سوڈےم سلفائٹ، کراکم (Karakum) کے رےت مےں 
گندھک، منگےش لاک (Mangvshlak) جزےرہ نما مےں فوسفورائٹ، تانبہ اور کوئلہ 
ملتا ہے۔ ےہاں، مےگمایئ گوں کے علاوہ ان سب ہی معدنےات کا آغاز پرت دار 
چٹانوں مےں ہوا ہے۔</p>

<p>	تان شان (Tien Shan) اور پامےرالائی (Pamis.Alai) پہاڑ آمو درےا 
کے مشرق مےں پھےلے ہوئے ہےں۔ ان پہاڑوں کی تشکےل کےلےڈونےن، ورسےن 
سائکل مےں ہویئ تھی اور ان پہاڑوں کی تجدےد (rejuvetion) مےسوزواک اور 
#کےنوزواک 
عہدوں مےں ہوئی تھی۔ پامےر کی گانٹھ مےں الپائنی سائکل کے دوران بھی 
#فولڈ پڑے تھے۔
 
                                  (518) 19</p>

<p>مختلف ارضےاتی ادوار مےں ےہاں کی زےرےں چٹانوں مےں مےگما داخل ہوتا رہا ہے، 
#جس کے 
سبب ےہاں پلوطونی اور پرت دار چٹانےں پہلو بہ پہلو ہےں۔ ان پہاڑی علاقوں 
مےں آبی درجہ 
حرارتی معدنےات مثلاََ سونا، ٹن ٹنگسٹن، تانبہ، چاندی، سےسہ اور جستہ کے 
#ذخائر ملتے 
ہےں۔ اور لوہے کی کچ دھات، کوارٹز ٹرملن (Quartz-Turmalin)، مےگنےسائٹ، 
سونے والی اےلووےم کاکٹاوئنکاتمی درےا کے کنارے کنارے، بودےبو ضلع نےچے۔ چھوٹی 
بٹّےوں اور سونے کی پرت، اوپر۔ گلےشےائی زمےنی مورےن کے ثےر سڈول ذخائر۔
ٹنگش، آرسےنو پائرائٹ (Arsenopyrite) جےسے اتصالی معدنےات (contact 
minerals) پائے جاتے ہےں۔ علاوہ ازےں ےہاں کی مےگمائی رگوں مےں ابرق اور 
 
                                 (519) 20</p>

<p>فےلسپار (felspar) بھی پاےا جاتا ہے۔ نچلے پہاڑی حصّوں مےں کوئلہ اور تےل بھی 
موجود ہے۔ فرغانہ والی مےں جورےسک دور کا کوئلہ اور تےل، اتنٹی منی اور پارہ 
#پاےا جاتا 
ہے۔ 17 اکتوبر کے انقلاب کے بعد اس علاقے سے جھےلوں مےں تہ نشےن ہونے والے 
#چٹانی 
نمک، گندھک، پھٹکری، فوسفورائٹ، اےس بےسٹس (asbestos) اور گرےفائٹ بھی 
کوج نکالا ہے۔</p>

<p>	0جےوکمےسٹری کے مقاصد:0 کسی ملک کی ارضےاتی ساخت اور بناوٹ کے مطالعہ 
سے ہم ملک مخصوص کی ارضےاتی تارےخ اور موجودہ خط و خال کے ارتقائ کا پتہ 
لگا سکتے ہےں۔ ارضےاتی
ارےخ اور ساخت سے ےہ معلوم کےا جا سکتا ہے کہ اس ملک مےں کون کون سے 
#معدنےات
موجدو ہو سکتے ہےں۔ تاہم زمےن کی ساخت اور بناوٹ سے اس ملک کے تمام 
#ذخائر کا 
صحےح اندازہ نہےں لگا ےا جا سکتا۔ کسی مخصوص علاقے کے معدنےات کے اقسام اور 
#ذخائر 
 
                              (520) 21</p>

<p>کے صحےح اندازے کی گھتھی کو سلجھانے کے لےئ نےز معدنی ذخائر کے بارے مےں 
#پےش گویئ کرنے
کے لئے ضروری ہے کہ ہم ان معرض تعمےر تحرےکات کی جانکاری حاصل کرےں 
#جہنوں نے 
سعدنےات کی تشکےل کی ہے۔ جےو کےمسٹری مےں ہم معدنی ذخائر کی تشکےل کے 
#قاعدوں اور 
ضوابط کا مطالعہ کرتے ہےں۔ ارضےات کی ےہ نیئ شاخ (جےوکےمسٹری) معدنی اور 
#کےمےایئ تحقےقی 
طرےقوں کو ارضےات سے وابستہ کرتی ہے۔</p>

<p>	معدنےات کی ٹشکےل کے طرےقے (جوقشرارض کی مختلف گہرائےوں اور مختلف 
التوں مےں عمل مےں آتے ہےں) بہت زےادہ پےچےداہ اور الجھے ہوئے ہےں اور 
#مشاہدہ گاہوں مےں 
صرف جزوی طور پر ہی ان کا مطالعہ کےا جا سکتا ہے۔ زمےن کے اندرونی حصے سے 
#نکلنے 
والے مےگما سے پرائمری اور ثانوی چٹانےں اور معدنی ذخائر وجود مےں آتے ہےں۔ 
#اس مےگما 
کے عناصر ممکن ہے کہ ابتدا مےں بہت مختلف قسم کے ہوں لےکن پرتوں مےں داخل 
ہونے کے بعد ان عناصر مےں بتدےلی ہو سکتی ہے۔ اندرونی ذخائر کی تشکےل کے 
#دوران 
مےگما اقشرارش مےں داخل ہو جاتا ہے۔ مےگما کے اثر سے بہت سی چٹانےں پگھل 
#جاتی ہےں 
(مثال کے طور پر چونے کے پتّھر، سلےٹ، سنگ رےگ اور مارل وغےرہ)۔ پگھل جانے 
کے بعد چٹانوں کی ساخت بدل جاتی ہے اور نتےجتاََ چٹانوں مےں موجود پانی 
#مےگما مےں 
شامل ہو جاتا ہے۔ علاوہ ازےں چٹانوں مےں پائے جانے والے عناصر مثلاََ سلےکا، 
#المونےم
چونا اور اتقلی مرکبات بھی مےگما مےں شامل ہو جاتے ہےں۔ ےعنی مےگما کے 
#ٹکڑوں کے ساخت
صدور بننے کے طرےقے (Process of emanation)، صدور کی ساخت اور کچ دھات 
کی ساخت کا انحصار پرائمری مےگما کی بناوٹ مےگما کی جسامت اور رقےق مےگما 
#کے ٹھنداے 
ہونے کی گہرائی اور ٹھنڈا ہونے کے طرےقہ پر ہوتا ہے ۔ کسی علاقے کی چٹانوں 
#کی خصوصےات 
ان کی بناوٹ اور مفصل Joints 			 بھی معدنی ذخےروں کی تہ نشےنی مےں بڑی 
#اہمےت رکھتے 
ہےں۔ مثال کے طور پر چونے کی چٹانےں دوسری چٹانوں کے مقابلے مےں تےزی سے 
#کچ دھات 
 
                                  (521) 22</p>

<p>(Ore) مےں تبدےل ہو جاتی ہے۔ کچھ چٹانوں مےں ان کی سختی. پرتوں کے 
سلسلے اور ان کے مفصل (jontsa) دھات کی تشکےل کرتے ہےں۔ اس کے برعکس 
#کچھ چٹانوں مےں ےہی 
چےزےں معدن کی تشکےل مےں روکاوٹ پےدا کرتی ہے۔ ان سب چےزوں کا اثر 
#معدنی ذخےروں 
کی شکل و سعت اور اوصرف پر پڑتا ہے۔</p>

<p>	اسی وجہ سے مختلف قسم کے مےگمایئ صدور (emanations) اور آبی درجہ 
حرارتی (Hydrothermal) کچ دھتوں کی تشکےل مےں ےکسانےت ہونے کے باوجود 
ان مےں رنگا رنگی ہوتی ہے اس رنگار نگی کا خاص صبب پر توں مےں مقامی 
#اختلاف
ے ےعنی کچ پرتوں مےں جو معدن موجود ہوتے ہےں دوسری پروتں مےں ان کی 
#کمی ہوتی 
ہے۔ مثال کے طور پر بہت سے معدنےات مےں مےگما نہےں ہوتا، کچھ معدنےات مےں 
اتصالی معدن پےگمےٹائٹ نہےں پائے جاتے۔ علاوہ ازےں پےگےمےٹائٹ (Pagmatites) 
#اےسڈ
(acid) چٹانوں سے بنتے ہےں وہ بےسک چٹانوں سے بننے والے پےگمےٹائٹ سے 
#مختلف 
ہوتے ہےں۔ اےسڈ اور بےسک چٹانوں سے بننے والے معدنےات کی معدن سازی جدا
جدا قسم سے ہوتی ہے۔ آتشی اونچے پہاڑوں کے گھٹے ہوئے سلسلے (massif) مےں 
ادنٰی ےا ےکساں قسم کی کچ دھات بنتی ہے جبکہ دوسرے حصّوں مےں رنگا رنگ 
#قسم کے 
معدنی ذخائر کثےر مقدار مےں مجمتع ہوتے ہےں۔</p>

<p>	قدرت مےں مختلف قسم کی حالوں کے سبب مےگمایئ چٹانوں کے عمےق مطالعہ 
کی ضرورت ہے۔ اس کے ساتھ کسی مخصوص علاقے مےں کچ دھات کی تشکےل 
کہ جملہ ضوابط کا مطالعہ کرنا بھی ضروی ہے تاکہ تمام حالات کی تحقےق کی جا 
#سکے 
اور ذخائر بنن ے پےچےدہ مسئلہ کا صحےح ہال تلاش کےا جا سکے۔ ذخرےوں کے 
#مسئلہ کا 
صحےح حل تلاش کرنے کے لےئ ےہ بھی ضروری ہے کہ زےر مطالعہ علاقے کی ارضےاتی 
#تارےخ
 
                                   (522) 23</p>

<p>سے واقضےت ہو۔ جےو کےمسڑی کا ےہی  اہم مقصد ہے۔</p>

<p>	مزکورہ بالا سرسری تبصرے مےں ہم نے روس کے مختلف معدنی وسائل کا 
#جائزہ 
لےا ہے۔ روس مےں وہ سب ہی دھات موجود ہےں جن کی کار خانوں اور صنعت و 
حرفت مےں ضرورت پڑتی ہے۔ نےز غےر دھات والے معدنےات مثلاََ کوئلہ، تےل، 
پےٹ (peat)، مختلف قسم کے نمک، اےس بےسٹس (asbestos)، گرےفائٹ، 
#مےگنےسائٹ
اور طرح طرہ کے تعمےری وسائل بھی کثےر مقدار مےں موجدو ہےں۔ روس مےں 
#طرح طرہ 
کے معدنےات موجود ہی نہےں بلکہ ان کے بھنڈار بہت زےادہ ہےں۔ معدنےات 
ے ان بھنڈاروں  سے روس کے کارخانے صدےوں تک چلائے جا سکتے ہےں۔ </p>

<p>	ہےاں ےہ کہنا بےجانہ ہوگا کہ خاص خاص معدنےات مےں روس کا شمار دنےا کے 
گنے چنے ممالک مےں ہے اور معدنی وسائل مےں صرف رےا ستہائے متحدہ امرےکہ 
#ہی 
روس کا مقابلہ کر سکتا ہے۔</p>

<p>	دنےا کے باقی تمام ملکوں مےں معدنےاتی وسائل بہت زےادہ غےرہ متوزن ٔ
ہےں گوےا کچھ معدن ملتے ہےں تو دوسروں کی قلت ہے۔ مثال کے طور پر جرمنی 
#مےں 
کوئلے اور نمک کے افراط ہے مگر وہاں لوہے، تانبے اور دوسری اہم دھاتوں کی
شدےد کمی ہے جرمنی مےں تےل برائے نام نکلتا ہے۔ جاپان اور اٹلی مےں کوئلے اور
لوہے کی کمی ہے۔ اسپےن مےں لوہے، تنبے اور پارہ کی بہتات ہے لےکن وہاں 
کوئلہ وار تےل نہےں۔ سوئٹزر لےنڈ اےک پہاڑی ملک ہونے کے بوجود معدنےات مےں 
بہت غرےب ہے اور اس ملک مےں صرف تعمری وسائل ہی موجد ہےں۔</p>

<p>	بہت سی بھےانک جنگےں اہم معدنی وسائل کو حاصل کرنے کے لئے دور دراز 
کے بر اعظموں  پر نو آبادےات قائم کی گئےں۔ اسپےن نے مےکسےکو اور جنوبی 
#امرےکہ کو 
 
                                 (523) 24</p>

<p>اس لئے فتح کےا تھا کےونکہ ان جگہوں پر چاندی کے بھنڈار تھے۔ انگلےنڈ نے سونا 
حاصل کرنے کی غرض سے جمہورےہ ٹرانسوال پر قبضہ کےا تھا۔ موجودہ سامراجےوں 
مےں بھی جنگ لڑنے کی اےک خاص وجہ لوہا، کوئلہ اور تےل حاصل کرنے کی غرض
رہی ہے۔ دورسری جنگ عظےم مےں جاپان اور جرمنی نے شروع مےں ان ملکوں 
#کو فتح 
کرنے کی کوشش کی تھی جن سے ےہ ممالک اےسے معدنےات منگاتے تھے جن کی ان 
#کے 
ےہاں قلّت تھی۔</p>

<p>
کمی            0 
</p></body></text></cesDoc>