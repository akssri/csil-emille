<cesDoc id="urd-w-socsci-cater-hs10.uss" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-socsci-cater-hs10.uss.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-01</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Lakhnaw</h.title>
<h.author>MHusain</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1984</pubDate>
</imprint>
<idno type="CIIL code">hs10.uss</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 49/.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-01</date></creation>
<langUsage>Urdu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>                                   (49) 1</p>

<p>6                                باب دوم</p>

<p>6                       گوشت کی متفرق غذائےں</p>

<p>61۔ بھےجا</p>

<p>	عہد قدےم مےں ےہ عقےدہ اب تک راسخ ہے کہ جانور کے اعضا مثلاََ بھےجا، جلےجی 
وغےرہ 
کے استعمال سے انسان کے انھےں مخصوص اعضائ کو تقوےت پہنچتی ہے اسی اصول کے تحت 
بھےجا اور 
کلےچی بہت مرغوب غذائےں تھےں۔ بھےجے کے متعلق ےہ کہا جاتا تھا اور اب بھی کہا 
جاتا ہے 
کہ اس کے کھانے سے دماغ کو تقوےت پہنچتی ہے اور حافظہ تےز ہوجاتا ہے کلےجی  کے 
کھانے
سے خون پےدا ہوتا ہے۔ ےہ نظےے اب تک مسلم ہےں لےکن حقےقت امر تو ےہ ہے کہ ےہ 
چےزےن
صرف لذت ہی کے لےے کھائ ی جاتی ہےں۔ ان مےں بھےجا زےادہ لذےز  غذا ہے لےکن ےہ 
بات
کہنے مےں آتی تھی کہ روساوعمائےدے کے دسترخوانوں پر ان غذاوں کی کوئی خاص 
قدردانی 
نہےں ہوتی ہے۔ تنوع کا مقصد پورا کرنے کے لےے بھےجا ےا کلےجی پک جاتے تھے، بعض 
روسا انکی 
طرف رخ بھی نہےں کرتے تھے۔</p>

<p>	بکرے کا بھےجا اپنے وزن اور قامت کے اعتبار سے مختصر اور چھوٹا ہوتا ہے اس لےے
اےک وقت مےں تےن چار بھےجوں سے کم نہےں پکتے تھے چونکہ بھےجے کے اوپر جھلی اور 
موٹی موٹی رکےں ہوتی ہےں جن کو پکان ےک قبل دور کر دےنا ضروری ہوتا ہے بھےجو 
کے پہلے لہسن
کے جووں اور زےرہ کے دانوں کے ساتھ ابال لےا جاتا ہے۔ ابل جانے کے بعد ناقابل
قبول اجزا آسانی کے ساتھ دور کر دےے جاتے ہےں اب بھےجا پکانے کے قابل ہوجاتا 
ہے فی زمانا بہت سے لوگ بھےجا تل کے کھاتے ہےں اور اسی طرح پکی ہوئی اس غذا کو 
پسند بھی کرتے ہےں۔ تلے ہوئے بھےجے مےں نمک اور گول مرچ کے علاوہ اور کوئی مسالہ
 
                                      (50) 2</p>

<p>استعمال نہےں ہوتا۔ پرانے زمانہ مےں بھےجا تل کر کھانا نفاست و لطافت کے سنافی تھی
اس لےے اس کا صرف سالن پکتا تھا۔ لونگ الاےچی گھی  مےں داغ کر کے ہلدی کا مسلہ 
جس کا ذرک 
اوپر آچکا ہے بھون لےا جاتا پھر بھےجا شامل کر کے سالن تےار کر لےا جاتا تھا آخر مےں 
دہی 
دے کر بھون لےتے اور لعاب  پر کھان تےار ہوجاتا تھا ہمارے خاندان مےں ےہ رواج تھا کہ
آٹے کی اےک چھوٹی ٹکےا کے دونوں طرف  مےتھی کے دانے چپکا کر اس کو گھی مےں تلا 
جاتا تھا، 
جب مےتھی کے دانے  جل کر سےاہ ہوجاتے تو ٹےکاں کو پتےلی سے نکال کر ہلدی کا مسالہ 
دم کر دےا جاتا، 
ےعنی ڈھکنی ڈھاک دی جاتی تھی، نمک بھی مسالے کے ساتھ شامل کر دےا جاتا تھا 
چند منٹ
کے بعد مسالہ کا پانی خشک ہوجاتا تو پانی کے چھےنٹے دے کر ہلدی کی بو نکالی 
جاتی۔ مسالہ 
مسالہ اچھی طرح بھن جانے کے بعد بھےجا اور اس کے ہم وزن دہی اےک ساتھ پتلےی 
مےں داخل 
کر دےا جاتا تھا۔ بھےجا تےار ہوجاتا تھا۔ آخر مےں  ہرا دھنےا ہری ادرک اور ہی مرچ 
بارک 
بارکے کترے کے چھڑک دی جاتی تھی۔ ہرے دھنےا کا موسم نہ ہوتا تو دو تولہ عرق 
گلاب کےوڑہ 
چھڑک دےا جاتا تھا۔ اس طرز پر پکانے مےں  بہت زےادہ دھےان اس بات پر رکھا جاتا 
تھا کہ آٹے کی ٹےکا پر چپکی ہوئی  مےتھی کا کوئی دانہ گھی مےں گرنے نہ پائے گر 
جاتا تو اس کو دور 
کردےنا ضروری ہوتا تھا ورنہ غذا کے کڑواہٹ ہوجانے کا اندےشہ  رہتا تھا۔</p>

<p>62۔ کلےجی</p>

<p>	کلےچی ےقےناََ بھجے سے زےادہ فائدہ  مند غذا ہے لےکن روسا عمائےدےن کے 
دسترخوانوں پر بہت کم حاضر کی جاتی تھی، کبھی کبھی پکائی  جاتی تب بھی بعض 
روسا کھنا ےس ے
اجتناب کرتے تھے جس کی وجہ ےہ تھی  کہ وہ کلےجی کو بہت ثقےل اور دےر ہضم غذا  
سمجھتے تھے کلےجی کے 
ہمراہ پھےپھڑا نرخراہ اور دل بھی فروخت ہوتا تھا۔ ےہ اجزا ےقےناََ نہ لزےز ہےں اور نہ 
زور 
ہضم اس لےے رئےسوں ک اشک بڑی حد کت حق بہ.جانب تھا بےسےوں صدی کی غالباََ  
دوسری 
دہائی مےں صرف کلےجی علاحدہ کر کے فورخت کرنے کا چلن رائج  ہوگےا تھا اسوقت سے 
کلےجی کی خوراک مےں بہتات ہوگی ہے بعض گھروں مےں بچے اور بھعض جوان بوڑھے 
بھی 
کلےجی کے ٹکڑوں کو گرم انگاروں پر بھون  کر کھالےتے ہےں۔ بازار مےں کلےجی کے کباب 
بہتات
کے ساتھ فروخت ہوتے ہےں لےکن ےہ کباب بڑے گوشت والے جانور کی کلےجی کے ہوتے
 
                                      (51) 2</p>

<p>مےں۔ اس لےے بڑے گھرانوں مےں استعمال نہےں ہوتے ہےں۔ وہاں کلےجی کے کباب
گھر ہی کے پاکئے ہوئے استعمال ہوتے ہےں کلےجی کے کباب بھی پکتے ہےں اور سالن بھی 
پکاےا جاتا ہے۔</p>

<p>	کلےجی کا سالن دو طرح سے پکاےا جاتا تھا۔ ان مےں سے اےک طرےقہ وہ تھا جو 
قےمہ 
دو پےاز ےعنی قےمہ خاصگی کے سلسلے مےں  اوپر بےان کےا جاچکا ہے بجنسہ اسی طرح
کلےجی کا سالن بھی پکاےا جاتا اور ےہی طرےقہ روسا کے ےہاں کلےجی پکانے کا رائج تھا۔
شرفا اور منوسط درجے کے لوگوں کے ےہاں سادہ سالن کے طرز پر کلےجی پکائی جاتی 
تھی۔ پہلے گھی مےں پےاز داغ کر کے نکال لی جاتی تھی اور اسی گھی مےں لونگ 
الائچی 
داغ کر کے ہلدی کا مسالہ بھون  لےا جاتا تھا۔ اس مسالہ کی تفصےل قےمہ کے ذےل مےں 
پےش کی جا چکی ہے۔ مسالہ بھن جانے کے بعد اسی مےں کلےجی بھون لی جاتی تھی۔ 
آکر مےں 
تلی ہوئی پےاز دہی مےں پےس کر کلےجی مےں شامل کر کے دوسری  بار بھونا جاتا تھا۔ 
آخر
مےں تھوڑی کتری ہوئی ادرک ہری مرچ، ہرادھنےا سوےا چھڑک کر ہانڈی تےار کلی 
جاتی تھی ےہی طرےقہ اب تک رائج ہے۔ بعض لوگوں کے ےہاں بہ.لحاظ کفاےات پےاز کر 
لچھے دہی 
مےں پےش کر شامل کرن ےکے بجائے صرف دہی پر اکتفا کرلی جاتی ہے۔</p>

<p>	کلےجی کے کباب بھی دو طرح سے پکائے جاتے ہےں۔ اےک طرز ماہی توے مےں
اور دوسری سےخ پر پکانے کےی ہے۔ دونوں  طرےقوں ے پکانا سہل ہے۔ ماہی توے 
مےں پکانے کے لےے پہلے کبابوں کے مسالے جن کا بےان گول کباب کے سلسلہ مےں آچکا 
ہے دہی مےں اچھی طرح ملا لےنا چاہےے پھر اسی مےں کلےجی کے ٹکڑے  لت کر دےے 
جائےں
ماہی تولے مےں تھوڑی پےاز گھی مےں داغ کی جائے اور اسی پر مسالے لگے ہوئے 
ٹکڑے بچھا کر بھون لےے جائےں، کباب تےار ہوجائےں گے۔ ماہی توے مےں کباب زےادہ 
مزے
 کے پکتے ہےں مگر سوندھا پن سےخ کے کبابوں مےں زےادہ ہوتا ہے سےخ پر پکانے کے لےے 
مسالوں کو 
دہی مےں پےسان چاہےے پھر اسی کو کلےجی مےں لت کر کے ٹکڑوں کو سےخ مےں کونچ 
دےنا چاہےے سےخ 
کے کباب پکانے کے لےے کلےجی کے ٹکرَے مقابلتاََ بڑے رہنا ضروری ہے تاکہ سےخ مےں آسانی 
کے ساتھ کو نچے جاسکےں۔ پھر  ان سےخوں کو اےنٹوں کا سہارا دے کر کوئلہ کے انگاوں 
پر لگادےان چاہےے ےہ طرےقہ سےخ کے کباب کے ذےل مےں بےان کےا جا چکا ہے۔ بالکل
 
                                   (52) 3</p>

<p>اسی طرح سےخ پر کباب بھون لےے جائےں بہت جلد کباب تےار ہوجائےں گے۔</p>

<p>63۔ کھےری گردے
	
	کلےجی کے بعد ذائقہ مےں کھےری گردوں کا شمار کےا جاتا ہے اور ےہ غذا خوش 
ذائقہ ہوتی
ہے لابتہ بہت دےر ہضم ہے۔ لکےجی سے نرخرہ، پھےپھڑے اور دل علاحدہ کردے جائںے
تو خالص کلےجی جلد ہضم ہوجاتی ہے لےکن گردے بجائے خود بھی ثقےل ہوتے ہےں اور 
کھےری کے الگ کردےنے کے بعد بھی بہت دےر مےں ہضم ہوتے ہےں۔</p>

<p>	کھےری گردوں کا سالن پکتا ہے اور کباب و قورمہ بھی تےار کےا جاتا ہے۔ ان کے 
سب 
پکانے کے طرےقے سہل ہےں۔ کباب ماہی تولے مےں اور سےخ پر بھی پکائے جاتے ہےںا ور 
ان کے پکانے کے بالکل وہی طرےقے ہےں جو کلےجی  کے کبابوں کے سلسلہ مےں اوپر بےان 
ہوئے۔
ہر سالن ہلدی کے مسالے مےں پکتا ہے، چنانچہ کھےری گردوں کا سالن بھی ہلدی کے سالے 
مسالے 
مےں بالکل اسی طرح پکانا چاہےے، جو کلےجی کا سالن پکانے کے سلسلہ مےں دوسرے طرےقہ 
کے تحت بےشن کےا گےا ہے۔ سالن اور کبابوں سے کہےں زےادہ لزےز قورمہ پکتا ہے لےکن 
قورمہ پکانے مےں کھےری  کو ےک بخت علاحدہ کر کے صرف گردوں کا قورمہ پکاےا جاتا 
ہے۔ قورمہ 
پاکنے کی بالکل دہی ترکےب ہے جو سادہ قورمہ پاکنے کے سلسلہ مےں بےان کردی گئی
ہےں۔ البتہ گردوں کے قرومہ مےں آخری بار بھونتے وقت دہی کی مقدار مےں اضافہ کر 
دےا 
ضروری ہوگا۔</p>

<p>	روسائد عمائےدن کے سترخوان پر خالص گردوں کا قورمہ حاضر کےا جاتا تھا ا
ور وہ لوگ اس کو شوق سے کھاتے تھے پھر بھی ےہ غذا گاہ بگاہ پکتی تھی اور باوجود 
خاصگی
طرز پر پکائے جاے کے زےادہ مقدار مےں نوش فرمانے  سے پرہےز کرتے تھے رئےسوں
کے ےہاں کھےری کسی طرح بھی کھان گوارا نہےں کےا جاتا تھا۔ اس لےے کھےری گردوں 
کی 
 ملی جلی کوئی غذا بھی رئےسوں کے دسترخوانوں پر نہےں آتی تھی۔ کہا جاتا تھا کہ 
کھےری کھانے 
سے دودھ پلانے ولای عورت کا دودھ  بڑھ جاتا ہے اس لےے اس کی مخصوص ہانڈی 
انوں کو 
پکا کر کھلائی جاتی تھی اور وہ بہت مزے لے لے کر کھالےتی تھےں۔</p>

<p>64۔ پسندے</p>

<p>	پسندے گوشت کے بڑے بڑے چپٹے ٹکڑوں کو گود کر تےار کےے جاتے ہےں۔
 
                                     (53) 4</p>

<p>گزشتہ زمانہ مےں پسندوں کو بنوانے کے لےے آدمی مامور کےا جاتا تھا جو گوشت والے 
کے ےہاں بےٹھ کر اپنے سامنے پسندے  بنواےا کرتا تھا۔ پسندے بنوانے مےں گوندنے کی 
اصطلاح رائج تھی  اور اب بھی ہے لےکن ےہ گود پہلے زمانہ مےں اےسا ہوتا تھا کہ 
گوشت کے ٹکڑوں پر چارخانہ بن جاتا تھا۔ پسندوں کے کباب بھی پکتے ہےںا ور قورمہ 
بھی کبابوں. کا ذکر اوپر آچکا ہے۔ اس مقامپ پر پسندوں کے قورمہ کا بےان پےش 
ہے۔ پسندوں کا سادہ قورمہ پکتا تھا لےکنا س مےں بھی کبابوں کا مسالہ دےا جاتا تھا 
اس کے پکانے کے لےے آدھ سےر پسندے مےں دو تولہ خشخش، دوتولہ نارےل، دو 
تولہ ادرک  اےک ماشہ.دار چےنی اےک ماشہ جوز جوتری اےک ماشاہ گرم مسالہ تےن 
تولہ بھنا ہوا دھنےا، دو تولہ سرخ مرچ، اےک چھٹانک پےاز اور اےک گٹھی لہسن کی 
ضرورت ہوتی تھی کہ مسالے پےسےن کے بعد آدھ پاو دہی مےں ملا دےے جاتے تھے 
سادہ قرومہ کی طرح پکاےا جاتا  اور پانی دے کر پسندے گلا لےے جاتے پسدے 
تل جانے پر تلی ہوئی پےاز دہی مےں پےس کر شامل کر کے آخری بار بھون لےا جاتا 
تھا۔
واضہ رہے کہ مسالوں کی مقدار پسندوں  کے وزن کے مقابل زےادہ ہے لےکن پسندے 
جتنے زےادہ چٹ پٹے ہوتے اتنے ہی زےادہ پسند کےے جاتے تھے۔</p>

<p>65۔ لبےرےاں</p>

<p>	لبرےاں گوشت کے لمبے لمبے، چپٹے چپٹے اور پتلے پتلے ٹکڑے کاٹ کاٹ کر بنائی 
جاتی تھےں ان کو کوچناےا گودنا نہےں پڑتا تھا۔ لبرےاں بھی قورمہ اور کبابوں کے مسالے </p>

<p>مےں پکتی تھےں۔ کبابوں کے طرز پر پکانے کا بجنہ وہی اسلوب تھا جواد پر بےان کےا 
گےا فرق صرف اتنا تھا کہ پسندوں کے مقابل بسےرےوں مےں  مسالوں کی مقدار بہت کم 
شامل کی جاتی تھی، لبےرےوں کا قورمہ بالکل سادہ قرومہ کی طرح پکتا تھا دونوں طرز 
کی 
لبےرےاں پتےلی ہی مےں پکائی جاتی تھےں۔ ےہ غذا بےگمات کو زےادہ مرغوب تھی اس 
لےے 
زنانہ باودرچی خانہ ہی مےں پکتی تھی۔ بہار کے مطبح مےں غالباََ لبےرےاں کبھی نہےں 
پکائی 
گئںے۔ ےہ بات کہنے مےں آتی ہے کہ گھر کی مستورات اس غذا کو پےشہ در پکانے والوےں </p>

<p>سے بدر جہا ببہتر پکالےتی  تھےں جس کی کوئی خاص وجہ کبھی سمجھ مےں نہےں 
آئی صرف ےہی 
کہا جاسکتا ہے کہ غذا جس کو بہت پسند ہوتی ہے اس کو دہی خوب پکاتا ہے۔
 
                                   (54) 5</p>

<p>62۔ گلّے پائے</p>

<p>	اس ذےل مےں سب سے پہل بات ےہ عرض کر دےنا ہے کہ  مےرا کام و دہن اس 
غذا کے ذائقہ سے کبھی واقف نہےں ہوا لہذا  مےں کوئی ذاتی رائے پےش کرنے سے مغدور 
ہوں۔ دوسرے ےہ کہ ہمارے خاندان مےں بےگمات  کو پائے بہت مرغوب تھے۔ لہذا 
صرف پائے پکتے تھے۔ لےکن دوسرے خاندانوں مےں پاےوں کے ساتھ کلاّ بھی شامل کر لےا 
جاتا تھا اور اس مرکب کو لکّے  پائے کہتے تھے صرف پائے پکائے جائےں ےا 
مسلے اور پائے دونوں شامل ہوں ہر حال مےں پکانے کا طرےقہ اےک ہی تھا کلّے پائے
کھانے دوالے اس غذا کے بہت معترف تھے اور آج بھی پسندےدہ غذاوں مےں شمار 
ہے۔ پسندےد گی کا ےہ عالم ہے کہ شہر لکنھوں کی پرانی بازاروں مےں کثرت سے کلّے پئے 
فروخت ہوتے ہےں۔ بےنچے والے سروں پر جھوسے کے اندر پتےلا رکھے ہوئے دروازہ 
دروازہ آواز لگاتے اور اپنا سودا جلد ہی فروخت کر لےتے ہں۔ اس غذا کو پکانے 
کے لےے پتےلا درکار ہوتا ہے اور وہی پتےلا سروں  پر لے کر بےچنے والے آوازےں 
لگاتے ہےں۔ خالص پائے صرف گھروں مےں پکتے ہےں۔ بازار مےں ےا سودے 
ولاے نہےں فروخت کرتے۔ عوام  کی رائے مےں خالص پائے زےادہ خوش ذائقہ نہےں ہوتے۔</p>

<p>	ہمارے خاندان مےں بےگمات کا ےہ قول تھا کہ پائے بہت جلد ہضم ہونےولای 
غذا ہے اور انسان کی آنتوں کو تقوےت پہنچاتی ہے۔ اسی لےے کمزور اور صعف مورہ 
کے مرےضوں کو پائے پلاو  دکھالےا جاتا تھا۔ ےہ کھان بھی بےگمات کے ذوق کی چےز تھاور 
زنانہ باورچی خانہ ہی مےں پکتا تھا۔ ہماری مخدرات جب خصوصےت کے ساتھ اس 
کھانے کا طر مراد نہ دسترخوان پر بھےجتی تھںے۔ تب ہمارے اسلاف بھی ذائقہ 
حاصل کر لےتے تھے۔ بےرونی مطبح مےں پائے کھبی نہےں پکتے تھے۔ ان کو پکانے کا طرےقہ 
تھا کہ پہےل پاےوں کو کھال اور بالوں سے صاف کراےا جاتا تھا پھر نمک اور لونگ 
الائچی کے ساتھ ابال لےا جاتا تھا، جب گل جاتے تھے تو گوشت اور ہڈےوں کے اندر
کا گودا اعلاحدہ کر کے ہڈےوں پھےنک دی جاتی تھےں۔ اس گوشت اور گودے کو 
ہلدی ولاے مسالے مےں سالن کی طرح پکالےا جاتا تھا۔ مسالے کے ہمراہ آدھ پاو
 
                                    (55) 6</p>

<p>وہی  بھی شامل کر دےا جاتا تھا۔ جب تےاری پر آجاتے تو تلی ہوئی پےاز اور دہی 
مےں پےس کر مالدی جاتی تھی۔ آخر مےں گول مرچ اور لونگ بارےک پےس کر اوپر 
سے چھڑک دی جاتی تھی۔ سالن کے مسالوں اور پکانے کا طرےقہ قےمہ کے ذےل 
مےں پےش کےا جا چکا ہے۔</p>

<p>	آخر مےں ےہ عرض کر دےنا بھی ضروری ہے کہ عہد قدمے مےں ہر غذا کا اےک 
مزاج معےن تھا اور اسی مزاج کے لحاظ سے غذائےں  موسم گرماےا موسم سرماے مےں
استعمال ہوتی تھےں۔ قورمہ کباب اور پلاو مستثنےات مےں تھے۔ پاوں کے 
بارے مےں کہنا جاتا تھا کہ ان کا مزاج بہت گرم ہوتا ہے اس لےے ےہ کھانا صرف 
موسم سرما مےں پکتا تھا۔ ےہی رواج آج تک برقرار ہے۔
 
                                    (56) 7</p>

<p>6                                باب سوم </p>

<p>6                              (مچھلی)</p>

<p>6مچھلی  کا قورمہ </p>

<p>	مچھلی بہترےن غذا ہے، بہت جلد ہضم ہوتی ہے اور ہماری صحت مندی کے 
لےے گوشت کے مقابلے مےں زےاد سود مند ہے۔ اس کا قورمہ قےمہ کباب سلان 
پلاو ہر طرح کے کھانے پکائے جاتے تھے اور پاکئے جاسکتے ہےں اور سب ہی لزےز
اور فائدہ مند ہوتے ہےں۔ مچھلی کے قسمےں بھی بہت ہوتی ہےں جن کو خواص و عاوم 
برابر  کھاتے ہےں لےکن ہمارے خاندان مےں صرف تےن قسموں کی مچھلےاں پسدےدہ 
تھےں  اور ان سے متعلق غذاوں کا بھی تعےن تھا ان مےں سے  ثور مچھلی کا قورمہ 
اور قےمہ مہاشےر مچھلی کا قےمہ کباب اور پلاو روہوں مچھلی کا سالن اپنا جواب 
نہےں رکھتا تھا۔ مہاشےر مچھلی شہر لنھوں مےں آسانی سے فراہم نہےں ہوتی تھی 
اس 
لےے تلاش کر کے بڑے قد کارد ہو اس کا بدل حاصل کےا جاتا تھا روہو مچھلی بہت 
عام تھی اور اب بھی ہے اس کے علاوہ اس کا ذائقہ نمکےن ہوتا ہے اس لےے 
سالن کے علاوہ دوسری غذائےں بھی اس سے تےار کر لی جاتی تھےں۔</p>

<p>
	قورمہ پکانے کے لےے مچھلی کے قتلوں کو لہسن کی پانی اور بےسن سے پہلے دو دو 
بار دھوےا جاتا تھا۔ سےر بھر مچھلی کے لےے لہنس دوگھٹی بنا ہوا دھنےا چار تولہ 
مرچ سرخ سولہ عدد پےاز آدھ پاو، گرم مسلہ چار ماشے بہت بارےک پےسے جاتے 
تھے پر سوا پاو گھی مےں پوا بھر پےاز کے لچھے تل کر نکال لےے جاتے اور اسی گھی مےں 
ککککسفےد زےرے کا دھنگار دے کر مچھلی کے قتلے اور مسالہ اےک ساتھ شامل کر کے 
پکاےا جاتا تھا۔ چند منٹ کے بعد ہی مچھلی کے ٹکڑوں کو الٹ پلٹ کر کے گھی اور 
گرمی ہر طرف متواز کر دی جاتی تھی، مچھلی امور سالے کا پانی خشک ہوجانے 
 
                                  (57) 7</p>

<p>کے بعد تلے ہوئے پےاز کے لچھے پاو بھر دہی مےں پےس کر شامل کر دے جاتے تھے
اس کے بعد نرم آنچ پر قورمہ تےار کر لےا جاتا تھا پکانے مےں کفگےر ہر بار اس 
طرح چلائی جاتی تھی کہ قتلے مسلم رہےں۔</p>

<p>62۔ مچھلی کا قےمہ </p>

<p>	مچھلی کے قتلے متزکرہ بالا طرےقے پر دھونے کے بعد  لونگ الائچی اور تےزپات 
کے ساتھ تھوڑے پانی ےمں جوش دے جاتے تھے۔ آدھےر سےر مچھلی کے لےے دو تےز پات 
چھ لونگوں اور سات الائچےوں کی ضرورت ہوتی تھی۔ جب مچلی گل جاتی اور پانی 
خشک ہوجاتا تو پتےلی  سے نکال کر کانٹے چن چن کے علاحدہ کر دےے جاتے تھے،
اور گلی ہوئی مچھلی سے قےمے کا کام لےا جاتا تھا۔ اس کو پکانے کے لےے آدھ پاو پےاز 
کے 
لچھے تےن چھٹانک گی مےں سرخ کر کے پہلے نکال لےے جاتے پھر اسی گھی مےں 
ہری الائچی داغ کر کے مچھلی کے قےمہ کو ہلکی آنچ پر بھونا جاتا تھا بھوننے کے قبل 
نمک دے دےا جاتا تھا۔ جب قےمہ سرخ ہوجاتا تو لہسن اےک گٹھی، ادرک دو تولے 
پےاز دو گٹھی، گرم مسالہ تےن ماشے سرخ مرچ دو تولے، بھنا ہوا دھنےا تےن توےل 
پےس کر مال دےا جاتا اور دو بار قےمے کو بھونا جاتا، جب مسالہ بھی ھن جاتا تو تلی 
ہوئی پےاز آدھ پاو دہی مےں پےس کر تےسری مرتبہ قےمے کو بھون کر تےار کر لےتے 
تھے۔</p>

<p>63۔ مچھلی کا سالن </p>

<p>	مچھلی کا سالن ہمےشہ سے پکاےا جا رہا ہے  اور اےک ہی رائج طرےقہ 
پر ہر ہر گھر مےں پکتا ہے۔ زنگےمی، لطافت اور خوشبو مےں اضافہ کرنے والے 
تدابےر مجبوراََ ترک کر دےئے گئے ہےں، اس مقامپ پر صرف وہ دو طرےقے مچسلی کا 
سلان 
پکانے کے لےے پےش لے جاتے ہےں جو مےرے دادھےال اور نانہےال مےں رائج تھے۔
انو دنوں طرےقےوں پر پکائی ہوئی مچھلی  کے ذائقے مختلف ہوتے ہےں ان۔۔۔ مےں 
کون ذائقہ بہتر ہوتا ہے اس کا فےصلہ ارباب ذوق تجربہ کر کے خود ہی فرمالےں 
مچھلی بہ.ہرحآل روہو ہوتی تھی۔</p>

<p>	دادھےالی طرےقہ ےہ تھا کہ مچھلی کے بڑے بڑے قتےل جو وزن  مےں سےر بھر 
ہوتے حسب 
مزکور دو دو بار لہسن اور بےسےن سے دھولےے جاتے تھے، پھر سےر بھر دہی مےں لہدی
 
                                  (58) 8 </p>

<p>کا سالن والا مسالہ جتنا سےر بھر گوشت کے لےے ضروری ہوتا پےس کر ملا دےاجاتا اور 
مہےن کپڑے مےں چھان لےا جاتا تھا، مسالے کا بہت بارےک پسنا بھی بہت ضروری 
تھا اسی دہی مسالے مےں مچھلی کے قتلے تخنےناََ تےن گھنٹے تک بھےگے رہتے تھے۔ دو 
گھنٹَے سے 
کم بھگونا قطعاََ جائز نہےں تھا پھر پاو بھر گی مےں آٹے کی تکےاں دنوں طرف مےتھی 
چپکا کے 
تل لی جاتی تھی۔ اگر کوئی دانہ مےتھی کا جل کر گر جاتا تو گھی سے فوراََ خارج کر 
دےا جاتا
تھا۔ سب مےتھی کالی پڑجاتی تو ٹکےاں نکلا کے مچلی مع دہی مسالے کے پتےلی مےں 
داخل 
کر کے فی.الفور ڈھکنی بند کردی جاتی تاکہ دھنگار کا اثر مچھلی اور دہی مسالے 
مےں آجئے قرے توقف کے بعد حسب ضورت نمک شامل کر دےا جاتا تھا اور مچھلی
نرم آنچ پر پکائی  جاتی تھی۔ ڈھکنی کے اوپر بھی چند انگارے رکھ دےے جاتے تھے
کم سے کم دو مرتبہ کف گےر سے آہستہ آہستہ قتلے الٹ دےے جاتے تھے لکن اس احتےاط 
کے ساتھ کہ ٹوٹنے نہ پائےں، مچلھی اور دہی کا پانی سوکھ جانے کے بعد کترےی ہوئی 
ادرک، ہری مرچ اور ہرادھنےا چھڑک کر مچھلی تےار کرلی جاتی تھی۔</p>

<p>	نانےھالی طرےقہ ےہ تھا کہ سوا پوائ گھی مےں سےر بھر پےاز کے لچھے تل کر نکال 
لےے 
جاتے تھے پھر اسی گھی مےں ہلدی کے سالن کا مسالہ جتنا سےر بھر گوشت کے لےے 
ضروری 
تھا بہت بارےک پےس کر نمک شامل کر کے بھون لےا جاتا تھا بوننے مےں پانی دےتے 
رہان ضروری تھا تا کہ ہلدی کو بو نکل جائے جب مسالہ سرخ ہوجاتا رو ہلدی 
دھنےے کی بو نکل جاتی  تو تلے ہوئے پےاز کے لچھوں کو آدھ سےر دہی مےں پےس کر 
مع مچھلی کے سےر بھر قتلوں کے پتےلی مےں ڈاخل کر دےا جاتا تھا۔ اس کے بعد پکانے 
کا وہی طرےقہ تھا جو اوپر بےان کےا گےا۔</p>

<p>	ےہ واضح کر دےنا ضروری ہے کہ متزکرہ بالا دونوں طرےقوں سے مچھلی کپکانے 
مےں پانی پےاز استعمال ہوتی تھی، نئی پےاز کے استعمال سے سالن کا خراب ہو 
جان ےقےنی تھا ےہ عرض کر دےنا بھی ضروری ہے کہ مچھلی کو دو بار لہسن اور بےسن 
سے 
دھونا ہمارے ہی خاندان مےں ضروری سمجھا جاتا تھا مےرے تجرےبے مےں ےہی بہترےن 
طرےقہ کار تھا اس لےے قرومے اور قےمے کے سلسلہ مےں بھی اسی طرز کو پےش کر دےا 
ہے۔
 
                                    (59) 9</p>

<p>
64۔ مچھلی کے شامی کباب</p>

<p>	سےر بھر مچھلی کے قتلوں کو حسب تےکب متذکرہ بالادھو کر قےمے ے تخت بےان 
کےے ہوچے طرز پر اہال کے  کانٹے چن لےے جاتے تھے تےار شدہ قےمے مں زےادہ تری 
بافی  نہےں رہتی تھی۔ ساتھ ہی ساتھ آدھ پاو پےاز کے لچھے گھی مےں تل کر آدھ 
پاو
دہی مےں پےس لےے جاتے تھے۔ پھر قےمہ مسالوں کے ہمراہ پےسا جاتا تھا۔ ماسالوں مےں 
سونف اور زےرہ تےن تےن ماشے۔ لہسن اےک گھٹی۔ چنے کی بھنی دال چھ تولہ،
لونگ، الائچی، کالی مرچ دوےد ماشے، دارچےنی اےک ماشہ، خشخش اور ادرک 
دو دو تولہ اور بقدر ضرورت نمک شامل ہوتے تھے۔ پھر اس مسالہ ملے قےمہ مےں لچھے 
دہی پےوست کے کر کے کبابوں کے سولہ ٹےکےاں بنا کر تھوڑی دےر کے لےے چھوڑدی 
جاتی تھےں تاکہ اچھی طرح خشک ہوجائےں پھر کبابوں کے دونوں طرف چاول کا 
پسا ہوا آٹالت کر کے ماہی تولے مےں تل لےا جاتا تھا اور ےہ کباب بھی گرم گرم کھائے 
جاتے تھے انھےں کبابوں کو خاضگی بنانے ک لےے مسالوں مےں اےک بڑے کاغذی لمےوں 
کا عرق دو انڈوں کی پھٹی ہوئی سفےدی اور قدرے زعفران شامل کردی جاتی تھی۔</p>

<p>65۔ مچھلی کے سےخ کباب</p>

<p>	ان کبابوں کے لےے بڑے قد کی مچھلی درکار ہوتی تھی۔ ےہ ضرورت عموماََ 
مہاشےر مچلھی
سے پوری ہوتی تھی ورنہ بہت بڑی روہو تلاش کی جاتی تھی۔ مچھلی  کے قتلے بھی 
کافی 
بڑے کٹوائے جاتے تھے ان قتلوں کو سونف، زےرے اور بےس سے کئی بار 
دھلوا کر کام مےں لاےا جاتا تھا۔ سےخ کباب کے مسالے حتی.الامکان پانی مےں نہےں 
پےسے جاتے تھے بلکہ آدھ پاو پےاز کے تلے ہوئے لچھوں، پاو بھر چھنی ہوئی بالای 
ادرک اےک بڑے کاغذی  لےموں کے عرق مےں پےسے جاتے تھے۔ مسالوں مےں ادرک 
تےن گرہ، بھنا ہوا دھنےا چا5ر مشے، لونگ اےک ماشہ، الائچی دو ماشے، گرم 
مسالہ تےن ماشہ اور ضرورت بھر نکم استعمال ہوتے تھے ےہ تمام اجزا سےر بھر
مچھکی کے بڑے بڑے قتلوں مںے گود کر آہستہ آہستہ پےوست کر دےے جاتے 
تھے۔ پھر ان قتلوں کو بارےک سےخ مےں کونچ کر سےخ کے کبابوں کی طرح پکاےا جاتا 
تھ، جو مسالہ بچ رہتا تھا وہ اوپر سےلےپ کر دےا جاتا تھا کوئلوں کی نرم
 
                                       (60) 10</p>

<p>آنچ پر پانی سوکھا جات اتو گھی کے پوچارے دے دےے جاتے تھے کباب سرخ 
ہوجاتے تو قتلوں کو سےخ سے نکال کر کسی ماہی توے مےں خفےف گھی لگا کر بچھا 
دےا جاتا اور تھوڑی دےر کے لےے بھوبھل پر رکھ کر کباب تےار کے لےے جاتے تھے۔</p>

<p>	مچھلی کے سلسلے مےں دوباتےں اور عرض کر دےنا ضروری  ہے فی زمانہ بےشتر 
گھارنوں مےں مچھلی کو کھال سمےت پکاتے ہےں ہمارے پرانے شہرےوں کی نفاست 
اس طرز کو گوارا نہےں کرتی تھی۔ مچھی ہمےشہ کھال سے صاف کرا کے پکائی جاتی 
تھی۔</p>

<p>	دوسری دل چسپ بات ےہ ہے کہ بعض روسا مچھی کے کانتوں سے بہت خائف 
رہتے تھے اور باورچےوں کو ہداےت رہتی تھی کہ قورمہ اور سالن گلا کے پکاےا 
جائے، کہا جاتےا تھا کہ بگلے کے ہڈی ےا سہاگہ شامل کردےنے سے کانٹے گل جاتے
ہےں لےن اس ترکےب پر عمل کرن ےسے مچھلی  مےں بکٹھا پن  آجاتا تھا اس لےے 
دوسری 
ترکےب بہ نکلی گئی تھی کہ مچھلی کے ہم وزن دہی دے کر پتےلی گل حکمت کردی 
جاتی 
اور کم سے کم بارہ گھنٹہ تک دھمےی آنچ پر پکائی جاتی تھی پھر بھی ذائفہ مےں. 
کچھ
نہ کچھ فرق ضرور آجاتا تھا اس لےے راقم کے نزدےک ےہ تمام تدابےر صرف دل کے بہلانے 
کا وسےلہ ہوتی ہےں اور ان کی طرف توجہ کران بے.کار ہے۔
 
                                     (61) 11</p>

<p>6                                  باب چہارم</p>

<p>6                           (مرغ اور دوسرے پرندے)</p>

<p>01۔ کباب پرندوں کے</p>

<p>	سب سے پہلے ےہ عرض کر دےنا ضروری ہے کہ پرندوں مےں مرغ اور بےٹر
کے علاوہ ہرےل، تےتر چھوٹی بط چہے  اور کہلگ استعمال ہوتے تھے بط چہے
اور کہلگ کا شامر درےائی جانوورں مےں تھا جن کے گوشت  مےں دوسرے  غےر
درےای پرندوں کے مقابل زےادہ لبارہندی ہوتی ہے اس خرابی کو دور کرن ے
کے لےے مسالوں کے وزن مےں لہسن دونا اور دہی ڈےوڑھا کر دےا جاتا تھا۔
ان سب پرندوں کے کباب پکتے تھے البتہ کبھی کبھی قورمہ بھی پک جاتا تھا ان 
دنوں غذاوں  کے پرند اےک ہی طرح پکاے جاتے تھے عےنی ےہ کہ ہر پرند کا 
قورمہ  ےا کباب اےک ہی طرز پر پکتے تھے اس لےے ان کے پکانے کے سلسےل مےں ہر 
پرند کا ذکر علاحدہ علاحدہ نہےں کےا جائے گا اس مقام پر کبابوں کا مسالہ 
پےش کر دےنا ضروری ہے، نےز ےہ اےک بات مزےد عرض کرنا ہے کہ مرغ اور دوسرے 
غےر درےائی پرندوں کی کھال  اور آلائش دور کرنے کے بعد اندر اور باہر 
لہسن کے پانی  اور پھر بےسن سے دو دو بار دھولےان کافی ہوتا تھا اور اسکے 
بعد بےسن مل کر بھی دھوتے تھے۔ کبابوں کے لےے سب پرندوں کے مشترک مسالوں 
مےں پرندوں کے اےک سےر گوشت کے واسطے لہسن اےک  گٹھی، پےاز دو بڑی گھٹی 
ادرک 3 تولہ، بنا ہوا دھنےا چار تولے، مرچ سرخ تےن تولے جوز جوتری 
دوماشے، تےز پات دو عدد لونگ الاچی چھ عدد، نارےل ڈھائی تولے، خشخش ڈےڑھ 
تولہ، بنھے چنے چھ تولے، زےرہ سےاہ بھنا ہوا تنے ماشے اور نمک ڈھایٰ تولے
 
                                   (62) 12</p>

<p>کی ضورت ہوتی تھی، ہر پرندے کے کباب پکانے کے لےے ان مسالوں کو پہلے 
بہت بارےک پےس لےنا ضروری تھا اسی مقام پر ےہ بےان کر دےنا بھی ضوری 
ہے کہ بےٹروں کے کبابوں کے واسطے گھاگر گربےٹر فراہم ہوتے تھے۔ چھوٹ، ےعنی 
چنک ہےٹر، نےز چہے  قورمہ کے مصرف مےں آتے تھے۔</p>

<p>	ہر پند کے کباب پکانے کے لےے اس کو صاف کرن کے بعد مسلم رکھن ا
ضروری تھا۔ اس کے جسم کو پہلے، ٹھوڑا پپےتہ نمک پےس کر گودا جاتا تھا، گودنے 
مےں ےہ پسا ہوا نمک اور پےستہ جسم مےں پےوست ہوجاتا تھا۔ پھر  اس کو تقرےباََ 
اےک گھنٹہ کے لےے چھوڑ دےا جاتا تھا۔ بعد اذاں آدھ پاوں پےاز کو گھی مےں داغ 
کر کے پاو بھر  دہی مےں پےس کر تےار شہد مسالوں مےں ملا کر پرندے کے جسم پر اندر </p>

<p>اور باہر مل دےا جاتا تھا۔ آدھ مسالہ جسم کے اندر دے کر اس کا پےٹ 
ڈورے سے باندھ دےا جاتا تھا۔ باقی  آدھا باہر کی طرف  لےپ ہوجتا 
تھا مسلے لےب کرنے کے قبل پرند کے جسم کو پپےتے  سے ےک لخت کسی کپڑے ےا 
ہاتھ سے پونچ کر صاف کر دےنا ضروی  تھا۔ کباب اےسے گہرے ماہی تولے 
 مےں پکائے جاتے تھےکہ پرندا اس  مےں سمائے اور اوپر سے طباق آسانی کے 
ساتھ ڈھکا جا سکے۔ ماہی تولے مےں پاو بھر گیھ کے ساتھ پےاز داغ کی جاتی 
اس کے سرخ ہوتے ہی اسی پےاز اور گھی پر مسالے لگے پرند کو بچھا دےا جاتا 
تھا  اور ماہی توے کے پےچے نےز طباق کے اوپر کوئلوں کی آگ دی جاتی تھی 
نےچے زےادہ اور اوپر کم انگارے  ہوتے تھے۔ ےہ کباب کوئلوں ہی پر پکتے تھے
ہر پندرہ منٹ کے بعد کف گےر سے پرندے کے پہلو بدلنا اور جو مسالہ 
گرجائے اس کو اوپر چڑھا دےنا ضروری تھا۔ پرند کے گل جانے  کے بعد دو ماشے 
زعفران تےن تولے عرق کےوڑہ مےں حل کر کے چھڑک دی جاتی تھی۔ کبھی کبھی ےہ
ہوتا تھا کہ پرندا اور مسالے کا پانی تےزی کے ساتھ سوکنے لگتا تھا اور گوشت 
 مےں گلنے کے آثار نہےں ہوتے تھے اےسی صورت مےں گرم پانی مےں لہسن پےس کر 
اسی پانی کے چھےنٹ دے جاتے تھے۔ کبابوں  کے لےے ہر پند فربہ ہونا چاہےے تھا۔
 
                                 (63) 13</p>

<p>62۔ قورمہ</p>

<p>	قورمہ ہو ےا کباب بوڑھے مرغ کی کوئی غذا ٹھےک نہےں پک سکتی اور 
نہ خوش ذائقہ ہوتی ہے اسی طرح گھاگر بےٹر بھی قورمہ کے لےے مناسب نہےں 
ہوتی۔ البتہ فربہ  مرغ دونوں غذاوں  کے لےے مناسب ہوتا ہے اور اس کے 
علاوہ ہر پرند کا قورمہ اچھا ہوتا ہے۔ قورمہ پکانے کے لےے پرند کو کھال اور 
آلائش سے صاف کر کے اس کے جسم کے ٹکڑے ٹکڑے کرنا پڑتے ہےں۔ اس کے 
بعد متزکرہ بالاطرےقہ پر اس کو دھوےا جاتا ہے غےر درےائی اور درےائی پرندوں 
کے دھونے کے طرےقے اوپر بےان کےے جاچکے ہےں ان پر بہ دستور عمل ہونا 
چاہےے پکانے کے لےے پتےلی مےں پہلے آدھ پاو پےاز سوا پوا گھی مےں داغ کےجائے
پھر تلی ہوئی پےاز نکال کر اسی گھی مےں لونگ  الاےچی داداغ کر کے پرند کو نمک دے 
کر بھونا جائے۔ بھوننے مےں بار بار لہسن کے پانی سے چھےنٹے دےتے رہنا ضروری 
ہے اور اس وقت تک بھونا جائے کہ گوشت سرخ ہوجائے۔ گوشت سرخ 
ہوجانے کے بعد سادہ قورمے کا مسالہ دے کر پھر بھونا جائے۔ درےائی 
جانوروں کے لےے اس مسالہ مےں لہسن کی مقدار دونی کرنا ہوگی۔ مسالہ بھن 
جائے تو پانی دے کر گوشت گلالےنا چاہےے اس کے بعد تلی ہوئی پےاز آدھ 
پاو دہی مےں پےس کر شامل کر کے تےسری مرتبہ بھونا جائے کہ گوشت سرخ ہوجائے۔ 
گوشت سرخ 
ہوجانے کے بعد سادہ قورمے کا مسالہ دے کر پھر بھونا جائے۔ درےائی 
جانوروں کے لےے اس مسالہ مےں لہسن کی مقدار دونی کرنا ہوگی۔ مسالہ بھن 
جائے تو پانی دے کر گوشت گلا لےنا چاہےے اس کے بعد تلی ہوئی پےاز آڈھ 
پاو دہی مےں پےس کر شامل کر کے تےسی مرتبہ بھونا جائے  اب گھی
اور لعاب پر قرومہ تےار ہوجائے گا۔ درےائی جانوروں کا گوشت تےسری مرتبہ 
بھوننے کے قبل تلی ہوئی پےرز کو تےن چھٹانک دہی  مےں پےسنا چاہےے قورمہ 
تےار ہوجانے پر روسا کے ےہاں خوش بو اور نفاست کا لحاظ کرتے ہوئے 
دو ماشے زعفران تےن  تولے عرق کےوڑہ مےں حل کر کے چھڑک دی جاتی تھی۔
عام گھروں مےں بھی عرق کےوڑہ ضرور چھڑک دےا جاتا تھا کےوں کہ باوجود تمام 
اہتمام کے درےائی  جانوروں  سے تےار کردہ غذا مےں لطےف مزاج ولاے کچھ نہ 
کچھ بسا ہند ضرور محسوس کر لےتے تھے۔</p>

<p>63۔ مرغ مسلم</p>

<p>	مرغ مسلم ہر عہد مےں مرغوب اور پسندےدہ غذا رہا ہے۔ اگر محنت اور 
 
                                    (64) 14</p>

<p>توجہ سے پکاےا جائے تو بہت خوش ذائقہ اور خوش رئح ہوتا ہے انسان کے 
جسم کے لےے دوسری غذاوں کے مقابلے مےں توانائی اور فرحت بخش ہے اس کو 
اچھا پکانے کے لےے فربہ لےکن جوان رغ کی ضرورت ہوتی ہے۔ معمولی طور پر پکانے 
کا بجنسہ وہی طرےقہ ہے جو کباب کے سلسلے مےں بےان کےا گےا ہے اسی کو ہم مرغ
کباب بھی کہتے ہےں اور  مرغ مسلم بھی لےکن روسا کے ےہاں اس کے پکانے 
مےں متعدد اےجادات و اختراعات کےے گےے تھے جن مےں دو طرح کے مرغ مسلم 
قابل ذکر ہےں، ان کا پکانا سہل ہے اور آج بھی آسانی سے پکائے جاسکتے ہےں 
اےک طرز توے ےہ تھی کہ مرغ کے جسم کے اندر مسالہ لےپ دےنے کے ساتھ دو باتےں ابلے 
ہوئے مسلم انڈے بھر دےے جاتے تھے۔ تےاری کے بعد اس کے پےٹ پر بندھے 
ہوئے ڈورے کو کاٹتے تو ےہ انڈے اےک اےک کر کے اس طرح باہر آجاتے گوےا کہ 
جانور انڈے دے رہا ہے ےہ انڈے بھی مسالہ مےں مخلوط کر کے کھا لےے جاتے تھے 
دوسرا طرےقہ ےہ تھا کہ مرغ کے پےٹ مےں مسالے کے ساتھ مےوے بھردے جاتے 
تھے۔ بادام کا اندرونی چلھکا دور کر کے اس کی گرمی کتر لی جاتی تھی پستے کی 
ہوائےاں تراش لی جاتی تھےں اور کش.مش کے دانے اچھی طرح  دھو کر شفاف
اور چمک دار کر دےے جاتے تھے، پھر ان کترے اور بنائے ہوئے  مےووں کو اےک 
دوسرے مےں اچھی طرح ملا کر مرغ کے پےٹ مےں بھر دےتے تھے۔ پےٹ کا ڈورا جدا 
کرن ےکے بعد ےہ  مےوہ مسالے اور گوشت مےں ل کر بہت لطف دےتا تھا۔ ان 
دونوں طرےقوں سے مرغ مسلم پکالےان آج بھی ممکن ہے البتہ پہلے طرےقے والا 
مقابلتاََ سستا ہوگا۔
 </p>

<p>6                         ----------------------</p>

<p>               0 
</p></body></text></cesDoc>