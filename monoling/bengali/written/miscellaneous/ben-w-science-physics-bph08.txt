<cesDoc id="ben-w-science-physics-bph08" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-science-physics-bph08.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>দল54ব45স</h.title>
<h.author>সলপ05.87</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1987</pubDate>
</imprint>
<idno type="CIIL code">bph08</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0469.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-22</date></creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;দলশ া সলপহটল 5 , 1987 া টশকামহয়াক : শতাবহদীর চহয়াললঞহজ া জযনহত বসু া
নমাদলর জীবনধারনলর জনহয় য়লমন
ধকহসিজলনলর পহরযশজন , মানব সভহয়তার ধসহতিতহ঵
রকহষার জনহয় তলমনি পহরযশজন হল শকহতির া নধুনিক
য়ুগল য঩ শকহতি পহরধানত বিদহয়ুতহশকহতি রূপল বহয়বহৃত হয , তবল
য়ানহতহরিক শকহতি , তাপীয শকহতি ঩তহয়াদিরও বলশ
কিছুটা বহয়বহার নছল া শিলহপ , কৃষি , য়ানবাহন ,
য়শগায়শগ বহয়বসহথা পহরভৃতিতল , যমনকি নমাদলর
দ঳ননহদিন জীবনলর ধনলক ফপকরণলও শকহতির
ধজসহর বহয়বহার া বিশহ঵লর জনসথখহয়া বৃদহধি ও
সভহয়তার ধগহরগতি , য঩ দু঩যলর ধবশহয়মহভাবী ফল
হিসলবল শকহতির চাহিদা দহরুত হারল বলডল চললছল া
ধথচ য়ল সব ফতহস থলকল নমরা শকহতি পা঩ ,
তাদলর কতকগুলির মজুত ভাণহডার কমল নসছল ,
ধনহয়গুলির কহষলতহরল শকহতি সথগহরহলর
সীমাবদহধতা রযলছল া সলজনহয়ল কযলক দশক পরল
শকহতির দুরহভিকহষলর পহরবল সমহভাবনা া বহু বিজহঞানীর
মতল য঩ সমসহয়ার যকমাতহর সমাধান হতল পারল
য়দি মানুষৃ যক ধরনলর কৃতহরিম সূরহয় ত঳রি করতল
পারল , নকারল য়া , বলা বাহুলহয় , সূরহয়লর চলযল
ধনলক ছশট পহরকৃতিতল পহরায যক঩ রকম া
যটা সমহভব হলল কযলকশশ কশটি বছরলর জনহয়ল
নর শকহতির ধনটন নিযল ভাবতল হবল না , বলতল
গললল চিরকাললর জনহয়ল঩ শকহতি সমসহয়ার সমাধান
হযল য়াবল া</p>

<p>কাজটি কিনহতু সহজ নয া গত পহরায তিরিশ
বছর ধরল বিভিনহন দলশলর বিজহঞানীরা ধকহলানহত
পহরচলষহটা সতহতহ঵লও যখনশ সারহথক কৃতহরিম সূরহয় ত঳রি
করা সমহভব হযনি া য঩ কাজকল বরহতমান শতাবহদীর
সবচলযল বড চহয়াললঞহজ বলা য়ায া</p>

<p>য঩ চহয়াললঞহজলর মশকাবিলা করবার জনহয়ল
বিজহঞানীরা নানা রকম য়নহতহর ত঳রি করল সলগুলিকল
কৃতহরিম সূরহয় হিসলবল কাজ করানশ য়ায কিনা , তা঩
নিযল বহু পরীকহষা-নিরীকহষা করলছলন যবথ যখনশ
করছলন া য়ল য়নহতহর সবচলযল নশাপহরদ বলল মনল
হচহছল , তার নাম টশকামহয়াক া য঩ য়নহতহর বহয়বহারল
গত বিশ বছরল বিজহঞানীরা সাফলহয়লর পথল
ধনলকখানি যগিযলছলন া পহরায সব দলশল঩ যবথ
ননহতরহজাতিক ভাবল ধরল নলওযা হযলছল য়ল , য়দি
কৃতহরিম সূরহয় ত঳রি করা সমহভব হয , তবল তা সরহবপহরথম
সমহভব হবল টশকামহয়াক য়নহতহর বহয়বহার করল া য ঴জনহয়ল
কৃতহরিম সূরহয় নিরহমানলর য়া চহয়াললঞহজ , তা নজ
পরহয়বসিত হযলছ সফল টশকামহয়াক ত঳রি করবার
চহয়াললঞহজ া</p>

<p>যরকম টশকামহয়াক নিরহমাণলর যখনশ য়লমন
ধনলকগুলি পহরয়ুকহতিগত সমসহয়া নছল , তলমনি
নবার রযলছল টশকামহয়াকলর নভহয়নহতরীণ করহমকাণহড
সমহপরহকিত বলশ কযলকটি মষলিক সমসহয়াও া য঩
সব সমসহয়া সমাধানলর জনহয়ল নানান নকারলর ও
নানান ব঳শিষহটহয়লর টশকামহয়াক নিযল বহু দলশল঩
পরীকহষা নিরীকহষা চলছল া নমাদলর দলশল
কলকাতার সাহা ঩নসহটিটিফট ধব নিফকহলিযার
ফিজিকহস-যর গবলষণাগারল সমহপহরতি যকটি
টশকামহয়াক য়নহতহর বসানশ হযলছল া ফদহদলশহয় :
কযলকটি মষলিক সমসহয়া সমহপরহকিত গবলষণা যবথ
ম ধরনলর য়নহতহর সমহপরহকল ধভিজহঞতা ধরহজন ও
পারদরহশিতা লাভ া ভারতল য঩টি঩ সরহবপহরথম
টশকামহয়াক া গুজরাটলর গানহধীনগরল ঩নসহটিটিফট
ফর পহলাজমা নামক পহরতিষহঠানল নর যকটি
টশকামহয়াক নিরহমাণলর তশডজশড চলছল া</p>

<p>শকহতি-সমসহয়ার সহ঵রূপ ও তার সমহভাবহয় সমাধান :</p>

<p>সভহয়তার চাকাকল সচল রাখবার জনহয়ল য়ল
শকহতির পহরযশজন , ভবিষহয়তল তার য়শগান ধবহয়াহত
রাখবার সমসহয়াটি কী , তা যকটু নলশচনা করা
য়লতল পারল া বরহতমানল শকহতির মূল ফতহস হল
কযলা , খনিজ ত঳ল ও গহয়াস া সমসহত পৃথিবীতল
য়ত শকহতির বহয়বহার হয , তার শতকরা পহরায পতচাশি
ভাগ পাওযা য়ায যগুলি থলকল া যগুলিকল বলা হয
জীবাশহম জহ঵ালানি ( fossii fuel ) া কারণ বহু কশটি
বছর নগল মাটির নীচল চাপা পডা ফদহভিদ ও
পহরাণীদলহলর রূপানহতরল যগুলির ফতহপতহতি া
কহরমবরহধমান চাহিদার ফলল য঩ সব জহ঵ালানির
মজুত ভাণহডার য়লমন দহরুত হারল কমল নসছল ,
তাতল হিসলব করল বলা য়ায য়ল , কযলক দশক
পরল঩ বহয়বহারয়শগহয় জহ঵ালানির মজুত নিদশলষিত
হবল - তলল ও গহয়াস নিদশলষিত হবল সমহভবত
যখন থলকল 10-12 বছরলর মধহয়ল঩ া তাছাডা
মনল রাখতল হবল য়ল জীবাশহম জহ঵ালানির মজুত সব
দলশল সমান নয - য়ল সব দলশল মজুত নল঩ বা
থাকললও কম , তাদলর কহষলতহরল সঙহকট ধনলক
নগল঩ ঘনীভূত হবল া</p>

<p>জলসহরশত , জশযার-ভাতটা , বাযু চলাচল ঩তহয়াদি
থলকল য়ল শকহতি পাওযা য়লতল পারল , তার পরিমাণল
চাহিদার তুলনায খুব঩ কম া ভূপৃষহঠল মশটল
সষরশকহতির পরিমাণ হললও তা যত বিসহতীরহণ
সহথানলর ফপর ছডিযল থাকল য়ল , তা দিযল
নঞহচলিক কাজকরহম কিছু কিছু হললও বহয়াপক
চাহিদার য়শগান দলওযা সমহভব হবল বলল মনল হয
না া তাহলল ভরসা কলবল পারমানবিক শকহতি া
বরহতমানল য়ল ধরনলর পারমাণবিক চুলহলি থলকল
নমরা শকহতি পা঩ , তাকল বলা হয বিভাজনা চুলহলি া
কারণ ঩ফরলনিযাম-235
ন঩সশটশপলর মতন ধপলকহষাকৃত ভারী পরমাণুর
নিফকহলিযাসলর বিভাজনলর ( ধরহথাতহ পহরায সমান দুটি
খণহডল ভলঙহগল য়াওযার ) ফলল য়ল ফতহপনহন শকহতি , তা঩
রযলছল য঩ চুলহলীর কারহয়কারিতার মূলল া মাতহর যক
কিলশগহরাম ঩ফরলনিযাম থলকল য়ল শকহতি পাওযা
য়ায , তা 4,000 টন কযলার শকহতির সমান া
তবুও বিভাজন চুলহলির ফপয়শগী জহ঵ালানি মললল য়ল
পহরাকৃতিক ঩ফরলনিযাম বা থশরিযাম থলকল ,
পৃথিবীর বুকল তাদলর সঞহচয সীমিত হওযায
বিভাজন চুলহলির বহয়াপক বহয়বহার শকহতির ধনটনকল
খুব বলশি হলল কযলক দশক হযতশ পিছিযল দিতল
পারবল া তাছাডা য঩ বহয়াপক বহয়বহারলর যকটি
বড সমসহয়াও নছল া বিভাজন চুলহলিতল য়ল
তলজসহকহরিয ভসহম ফতহপনহন হয , তার সদহগতি করা
যত দুরূগ সমসহয়া া 2000 খহরীষহটাবহদল পৃথিবীতল
য়ল মশট শকহতি বহয়যিত হবল , তা য়দি কলবল
বিভাজন চুলহলি থলকল সথগৃহীত করা হয , তা হলল
য়ল তলজসহকহরিয ভসহমলর সৃষহটি হবল , তা পহরায যক কশটি
পারমাণবিক বশমার বিসহফশরণলর ফলল ফতহপনহন
তলজসহকহরিয ভসহমলর সমান !</p>

<p>নর যক রকম পারমাণবিক চুলহলিও হতল পারল ,
য়াকল ব঳জহঞানিক পরিভাষায বলা হয ` সথয়শজন
চুলহলি ' , সাধারনভাবল নমরা
বলতল পারি ` কৃতহরিম সূরহয় ' া যতল হা঩ডহরশজলনলর
ন঩সশটশপ ডযটলরিযাম ও টহরিটিযামলর মতন
হালহকা পরমাণুর নিফকহলিযাসলর সথয়শজনলর ( ধরহথাতহ
জুডল য়াওযায ) ফলল বিপুল শকহতির ফতহপতহতি
হওযা সমহভব া নমাদলর সুপরিচিত নকহষতহর সূরহয়
যবথ নরশ বহু নকহষতহর নিফকহলিয সথয়শজন
পহরকহরিযায পহরতিনিযত পহরচণহড শকহতির ফদহভব হচহছল া
সূরহয়লর মধহয়লকার ধতহয়ধিক ফষহণতায ( কলনহদহরসহথলল
ফষহণতা পহরায দলড কশটি ডিগহরি সললসিযাস )
সলখানকার হা঩ডহরশজলন গহয়াসলর ধণু-পরমাণুরা
ধতহয়নহত গতিশীল হয যবথ তাদলর পারসহপারিক
সথঘরহষলর ফলল পরমাণুর পরিবার ভলঙহগল গিযল
ভিতরলর ঩ললকহটহরন ও নিফকহলিযাস ( যকহষলতহরল
পহরশটন কণা ) মুকহত ধবসহথায বিরাজ করল া
য঩রকমস বহু মুকহত ঩ললকহটহরন ও সমানসথখহয়ক
নিফকহলিযাসলর সমান সথখহয়ক পহলাজমা বলা হয া
পহলাজমা হল পদারহথলর যকটি বিশলষ ধবসহথা -
চতুরহথ ধবসহথা , পদারহথলর তৃতীয ধবসহথা গহয়াসলর সঙহগল
য়ার ধনলক পারহথকহয় নছল া য়া হশক , সূরহয়লর
ধতহয়নহত ফষহণ পহলাজমা মাধহয়মল নিফকহলিযাসসগুলি
পহরচণহড গতিসমহপনহন হয যবথ সল঩ গতির ফলল
পারসহপারিক ব঳দহয়ুতিক বিকরহষণ সতহতহ঵লও পরসহপরলর
খুব কাছল চলল নসতল পারল া তখন নিফকহলীয
সথয়শজন ঘটল থাকল া বিজহঞানীরা য়ল সথয়শজন চুলহলি
নিরহমাণল সচলষহট নছলন তাতল সূরহয়লর ফষহণ
পহলাজমার মতন ( বসহতুর নরশ ফষহণ ) পহলাজমা ত঳রি
করা হবল যবথ সল঩ পহলাজমায য়থলষহট সথখহয়ক
নিফকহলীয সথয়শজন ঘটলল বিপুল শকহতির ফদহভব
হবল া পহরসঙহগত বলা য়ায য়ল , পঞহচাশলর
দশকল঩ নিফকহলীয সথয়শজন পহরকহরিযায পহরচণহড শকহতি
সৃষহটি করতল পলরলছল , তবল তা ধনিযনহতহরিতভাবল ,
হা঩ডহরশজলন বশমার বিসহফশরণল া সথয়শজন চুলহলিতল
সল঩ হা঩ডহরশজলন বশমাকল য়লন পশষ মানানশ হবল ,
তার শকহতির ফতহপতহতি হবল নিযনহতহরিতভাবল , য়াতল মানুষ
঩চহছা মতন সল঩
শকহতিকল কলহয়াণকর কাজল বহয়বহার করতল পারল া</p>

<p>সূরহয়লর জহ঵ালানি য়ল সাধারণ হা঩ডহরশজলন , কহষুদল
সূরহয়রূপ সথয়শজন চুলহলিতল বহয়বহার করলল
নিফকহলীয সথয়শজন য়থলষহট সথখহয়ায হবল না া
সথয়শজন চুলহলির মূল জহ঵ালানি ভারী হা঩ডহরশজলন
বা ডযটলরিযাম া জল থলকল য঩ ডযটলরিযাম
পাওযা য়লতল পারল া সমুদহরলর জললর ধণুতল
য়ল হা঩ডহরশজলন নছল , তার 65000 ভাগলর যকভাগ
হল ডযটলরিযাম া য় জলরাশির পরিমাণ সুবিশাল
হওযার মজুত ডযটলরিযামলর পরিমাণও য়থলষহট া
হিসলব করল দলখা য়ায য়ল , যক লিটার জলল
য়লটুকু ডযটলরিযাম নছল , তা঩ থলকল য়ল শকহতি
পাওযা য়লতল পারল , তা 350 লিটার পলটহরশললর
শকহতির সমান া সমুদহরলর জলল য়ল ডযটলরিযাম
নছল , সথয়শজন চুলহলির জহ঵ালানি হিসাবল তা মনুষহয়
সভহয়তার চাহিদাকল ধনহতত 100 কশটি বছর
মলটাতল পারবল া সুতরাথ বলা য়ায , সথয়শজন
চুলহলি নিরহমাণলর পহরচলষহটা সফল হলল কারহয়ত
চিরকাললর জনহয়ল শকহতি-সমসহয়ার সমাধান হযল
য়াবল া</p>

<p>পহরসঙহগত বলা য়ায , সথয়শজন চুলহলি নিরহমাণলর
কাজ কিছুটা সহজ হয য়দি কলবল ডযটলরিযাম
বহয়বহার না করল ডযটলরিযাম ও হা঩ডহরশজলনলর
ধনহয় ন঩সশটশপ টহরিটিযামলর মিশহরণকল সথয়শজন
চুলহলির জহ঵ালানি হিসাবল বহয়বহার করা হয া তাতলও
বলশ ধনলক কাল শকহতির য়শগান দলওযা সমহভব ,
কারণ টহরিটিযামকল ত঳রি করল নলওযা য়ায
লিথিযাম থলকল , য়া ভূতহ঵কল মশটামুটি
য়থলষহট পরিমাণল রযলছল া</p>

<p>সাফলহয়লর শরহত াা</p>

<p>গত তিরিশ বছর ধরলও বিজহঞানীরা চলষহটা করলও
য়ল যখনশ সথয়শজন চুলহলি নিরহমাণল সাফলহয় লাভ
করতল পারলন নি , তার কারণ হল য঩ সাফলহয়লর
জনহয়ল দুটি দুরূহ শরহতকল ধবশহয়঩ পালন করতল
হবল া য বিষযল নীচল সথকহষলপল নলশচনা করা হল া</p>

<p>পহলাজমা মাধহয়মল নিফকহলীয সথয়শজন ঘটাতল
হলল য়ল সল঩ পহলাজমার ফষহণতা কলন খুব বলশি
হওযা দরকার , তা নগল঩ বহয়াখহয়া করা হযলছল া
কিনহতু নিফকহলীয সথয়শজন ঘটলল঩ তশ হবল
না , পহরতি সলকলণহডল সথয়শজনলর সথখহয়া য়থলষহট
হতল হবল য়াতল সথয়শজনলর ফলল ঩ফতহপনহন মশট
শকহতি ফষহণ পহলাজমা থলকল বিকিরণলর ফলল বিনষহট
শকহতির চলযল বলশি হয যবথ পহলাজমা থলকল ফদহ঵ৃতহত
শকহতি পাওযা য়লতল পারল কাজল লাগানশর জনহয়ল া
ধরহথাতহ য য়লন বলা য়ায , পহলাজমার নয
তার নিজলর বহয়যলর থলকল বলশি হবল য়াতল
তার ফদহ঵ৃতহত সমহপদ সল ধনহয়কল দিতল পারল া যর
জনহয়ল পহলাজমার ফষহণতা কত হতল হবল , তা হিসলব
করল দলখা হযলছল া সথয়শজন চুলহলির জহ঵ালানি
ডযটলরিযাম হলল সল঩ চুলহলির সাফলহয়লর পহরথম
শরহত : পহলাজমার ফষহণতা ধনহতত চলহলিশ কশটি ডিগহরি
সললসিযাস হওযা দরকার া জহ঵ালানি হিসাবল
ডযটলরিযাম ও টহরিটিযামলর মিশহরণ বহয়বহার করলল
পহরযশজনীয ফষহণতা ধনহতত চার কশটি ডিগহরি
সললসিযাস া বাসহতব কহষলতহরল পহলাজমা থলকল বিকিরণ
ছাডাও ধনহয়ানহয় ভাবল শকহতিকহষয হয বলল
পহরযশজনীয ফষহণতা নরশ কিছুটা বলশি হওযা
দরকার - য়লমন কারহয়কর সথয়শজন চুলহলিতল
ডযটলরিযাম-টহরিটিযাম জহ঵ালানির কহষলতহরল ফষহণতা
হতল হবল দশ থলকল কুডি কশটি ডিগহরি
সললসিযাস া য঩ সব ফষহণতার কাছল সূরহয়লর
কলনহদহরসহথললর ফষহণতাও হার মলনল য়ায া</p>

<p>সূরহয়লর চলযলও মানুষলর ত঳রি সূরহয়লর ফষহণতা
য়ল বলশি হওযা দরকার , তার কারণ হল - সূরহয়লর
তুলনায যর নযতন ধনলক কম হওযায
সথয়শজনলর ফলল ফতহপনহন শকহতি বহুলাথশল কম
হয : বিকিরণলর ফলল বিনষহট শকহতির পরিমাণও
তলমনি কমল য়ায বটল কিনহতু ঠিক য় ধনুপাতল
কমল না া সলজনহয়ল বিনষহট শকহতির সঙহগল ফতহপনহন
শকহতির ধনুপাতকল সমান রাখতল হলল ফষহণতাকল
ধপলকহষাকৃত বলশি করা দরকার া</p>

<p>সূরহয়লর চলযলও ফষহণ পহলাজমা ত঳রি করা য়লমন
যক মহা সমসহয়া , তলমনি নর যক বিরাট সমসহয়া
হল তাকল নিরহদিষহট সহথানলর মধহয়ল নবদহধ রাখা া
কারণ য় পহলাজমার মধহয়ল পহরচণহড গতিশীল
কণাগুলির পকহষল চারদিকল ছডিযল পডা঩
সহ঵াভাবিক া ধথচ য়ল নধারলর মধহয়ল পহলাজমার
ফষহণতা ধচিরল঩ ধনলকখানি কমল য়াবল া কিনহতু
ফষহণ পহলাজমা ধনহতত খানিককহষণ সহথাযী হলল তবল঩
তার মধহয়ল বলশ কিছু নিফকহলিযাসলর সথয়শজন
ঘটতল পারল া নবার য় সমযলর মধহয়ল য়থলষহট
সথখহয়ক নিফকহলিযাসলর সথয়শজন ঘটাতল হলল
নিফকহলিযাসলর সথখহয়াও য়থলষহট হওযা দরকার া
বসহতুত বিজহঞানী জল ডি লসন হিসলব করল দলখান
য়ল , সথয়শজন চুলহলিতল ফষহণ পহলাজমা ত঳রি করতল
য়ল শকহতি বহয়যিত হবল ও বিকিরণলর ফলল য়ল
শকহতিকহষয ঘটবল , ফতহপনহন কারহয়লর শকহতিকল য়দি
তাদলর য়শগফললর চলযল বলশি হতল হয , তবল n
ও t-যর গুণফলকল যকটি নিরহদিষহট মানলর
চলযল বলশি হতল হবল , য়লখানল n হল পহলাজমার পহরতি
ঘন সলণহটিমিটারল নিফকহলিযাসলর সথখহয়া যবথ t হল
সলকলণহডলর হিসাবল পহলাজমার সহথাযিতহ঵কাললর
পরিমাণ া য঩টি঩ হচহছল সথয়শজন চুলহলির
সাফলহয়লর দহ঵িতীয শরহত া লসনলর নামানুসারল
যকল বলা হয ` লসনলর ' শরহত া সথয়শজন চুলহলির
জহ঵ালানি য়দি কলবল ডযটলরিযাম হয , তাহলল ফকহত
নিরহদিষহট মান হল 10 (16) ; জহ঵ালানি ডযটলরিযাম
ও টহরিটিযামলর সঙহগল সথমিশহরণ হলল য় মান হচহছল 10 (14 ) া</p>

<p>বরহতমানল সথয়শজন চুলহলি নিরহমাণলর পহরচলষহটা
চললছল মূলত দুটি পদহধতিকল ধবলমহবন করল া
পহরথম পদহধতিতল বিদহয়ুত পহরবাহলর সাহায়হয়ল বা ধনহয়
কশন ভাবল পহলাজমা ত঳রি করল চুমহবক কহষলতহর
দিযল গঠিত যক ধদৃশহয় পিঞহজরল তাকল নবদহধ
রাখবার চলষহটা করা হয া চুমহবক কহষলতহরলর যকট ধরহম
য঩ য়ল , তা গতিশীল নহিত ( ধরহথাতহ
বিদহয়ুতহসমহপনহন ) কণার গতিকল পহরভাবিত করতল
পারল া সৃয়শজন চুলহলির মধহয়ল যমন ` চুমহবককহষলতহর '
ত঳রি করা হয , য়াতল বা঩রলর দিকল নগত
কণাগুলির দিক পরবরহতিত হয যবথ কণাগুলি
চলল য়ায ভিতরলর দিকল া য঩ভাবল চুমহবককহষলতহর
য়লন যর পিঞহজরলর সৃষহটি করল া পহলাজমা য়ার
বা঩রল নসতল পারল না া চষমহবক নবদহধকরণলর
ভিতহতিতল য়লসব বিভিনহন ধরনলর য়নহতহর ফদহভাবিত
হযলছল , সলগুলির নাম হল টশকামহয়াক ,
সহটললারলটর , চষমহবক দরহপণ ঩তহয়াদি া যসব
কহষলতহরল পহলাজমায পহরতি ঘন সলণহটিমিটারল নিফকহলিযাসলর
সথখহয়া মশটামুটিভাবল 10 ( 14 ) ; সুতরাথ লসনলর
শরহত পালিত হওযার জনহয়ল ডযটলরিযাম-টহরিটিযাম
জহ঵ালানির কহষলতহরল পহলাজমার সহথাযিতহ঵কাল ধনহতত 1
সলকলণহড হওযা দরকার া</p>

<p>দহ঵িতীয পদহধতিতল ললসার নামক বিশলষ রকম
নলশর ফতহস থলকল সুতীবহর রশহমি নিকহষলপ করা হয
কহষুদহরাকৃতি ডযটলরিযাম-টহরিটিযাম খণহডলর ফপর া য়
খণহডটি নিমলষলর মধহয়ল ফষহণ পহলাজমায রূপানহতরিত
হয যবথ পহরথমল তার সঙহকশচনলর ফলল তার ঘনতহ঵
য়ায খুব বলডল া তবল সামানহয় সময পরল঩
পহলাজমা চতুরহদিকল ছডিযল পডল া পহরসঙহগত ফলহললখহয় , ললসার
রশহমির পরিবরহতল দহরুতগতি সমহপনহন ঩ললকটহরনগুচহছ বা
নযনগুচহছকল পহলাজমা ত঳রি করবার কাজল
বহয়বহার করা য়লতল পারল া যসব কহষলতহরল ঘন
পহলাজমার সহথাযিতহ঵কাল মশটামুটিভাবল মাতহর 1
নহয়ানশ সলকলণহড ( ধরহথাতহ যক সলকলণহডলর 100 কশটি
ভাগলর 1 ভাগ ) কিনহতু পহরতি ঘন সলণহটিমিটারল
কণার সথখহয়া 10 ( 26 ) হতল পারল বলল লসনলর শরহত
পালিত হওযার সমহভাবন রযলছল া //
টশকামহয়াক সবচলযল ফজহজহ঵ল সমহভাবনা া</p>

<p>সথয়শজন চুলহলি নিরহমাণলর জনহয় য়ত রকমলর য়নহতহর
নিযল পরীকহষা-নিরীকহষা হযলছল বা হচহছল , সলগুলির
মধহয়ল সবচলযল গুরুতহ঵পূরহণ হল টশকামহয়াক ,
কারণ য পরহয়নহত য়ল সমসহত ফল
পাওযা গলছল , তা থলকল ধধিকাথশ বিজহঞানী ও
বিজহঞানলর করহমকরহতাদলর ধারণা য়ল , য঩ য়নহতহর বহয়বহার
করল সারহথক সথয়শজন চুলহলি নিরহমাণলর সমহভাবনা
সবচযল ফজহজহ঵ল া ` টশকামহয়াক ' হচহছল রুশ ভাষায
যকটি সথকহষলপিত শবহদ , য়ার সমহপূরহণ ধরহথ
` বলযাকৃতি চষমহবক পহরকশষহঠ ' ( torodial magnatic
chamber ) া রাশিযার বিজহঞানী যল য
নরহটসিমশভিচকল টশকামহয়াকলর জনক বলা হয -
ষাটলর দশকলর শলষলর দিকল তিনি঩ সরহবপহরথম
টি-3 নামক টশকামহয়াক য়নহতহর বহয়বহার করল
নশাবহয়ঞহজক ফল লাভ করলন যবথ বিজহঞানীদলর
সচলতন করলন যর সমহভাবনা সমহপরহকল া</p>

<p>টশকামহয়াক য়নহতহরল যকটি বলযাকৃতি ধাতব নধারকল
ধনলকাথশল বাযু শূনহয় করল তার মধহয়ল
ফষহণ পহলাজমা সৃষহটি করা হয ( 1 নথ চিতহর দহরষহটবহয় ) া
চশঙাকৃতি য় রকম কশন নধারল পহলাজমা
থাকলল সল঩ নধারলর দু঩ পহরানহত দিযল পহলাজমা
বলরিযল য়লতল পারল বা পহরানহতল নঘাত করতল
ফষহণতা হারাতল পারল া বলযাকৃতি নধারলর
কশন পহরানহত না থাকায য঩ ধসুবিধা নল঩ া
বলযাকৃতি নধারটি য়ল জাযগায ঘিরল থাকল ,
তার ঠিক কলনহদহরসহথল বরাবর য়দি যকটি সরলরলখা
কলহপনা করা য়ায ( 1 নথ চিতহরল AOB রলখা ) তা
হলল সল঩ রলখাকল বলা হয টশকামহয়াকলর পহরধান
ধকহষ ( major Axis ) া নর নধারলর ঠিক মাঝখান
দিযল য়দি যকটি বৃতহতাকার রলখা কলহপনা
করা য়ায - য়লমন চিতহরল CDE রলখা , তবল সল঩
রলখাকল বলল গষণ ধকহষ ( minor axis ) া গষণ
ধকহষলর য়ল বহয়াসারহদহধ ধরহথাতহ পহরধান ধকহষ থলকল তার
য়ল দূরতহ঵ OP , তাকল বলা হয পহরধান বহয়াসারহধ া
গষণ ধকহষলর সঙহগল
নডানডিভাবল নধারটির য়ল কশন ফলহলমহব
পহরসহথচহছলদ নিলল তা বৃতহতাকার হয ; সল঩ বৃতহতলর
বহয়াসারহধ PQকল বলল গষণ বহয়াসারহধ া
বলযাকৃতি নধারলর মধহয়ল পহলাজমাকল
নবদহধ রাখবার জনহয়ল নানা ফপাযল য়ল ফপয়ুকহত
চষমহবক কহষলতহর সৃষহটি করা হয , তা঩ হল
টশকামহয়াকলর ব঳শিষহটহয় া যজনহয়ল য঩ চষমহবক কহষলতহর
সমহপরহকল যকটু বিশদভাবল নলশচনা করা য়লতল
পারল া</p>

<p>পহরথমত , বলযাকৃতি নধারকল বলড দিযল
ধনলকগুলি তার কুণহডলী রাখা হয , য়লগুলির মধহয়
দিযল বিদহয়ুতহপহরবাহ পাঠালল বলযাকৃতি চষমহবক
কহষলতহরলর সৃষহটি হয া য঩ সব
কুণহডলীকল বলা হয বলযাকৃতি কহষলতহরকুণহডলী া
নমরা জানি , কশন
গতিশীল নহিত কণার ফপর চষমহবক কহষলতহরলর
পহরভাবলর ফলল য় কণা চষমহবক বলরলখার
চারপাশল পাক খলতল খলতল চষমহবক বলরলখা
বরাবর চলতল থাকল ( 3নথ চিতহর ) া সুতরাথ
চষমহবক বলরলখার নডানডি পথল কণাটি য়লতল
পারল না া য঩ভাবল পহলাজমার সব কণা঩ চষমহবক
বলরলখাগুলি বরাবর নবদহধ থাকল যবথ বলরলখার
নডানডি পথল গিযল নধারলর দলওযালল নঘাত
করতল পারল না া টশকামহয়াক য়নহতহরলর
বলযাকৃতি চষমহবক কহষলতহরল কিনহতু ধনহয় যকটি
বহয়াপারও ঘটল া য঩ কহষলতহরলর মান নধারলর
ভিতরলর দিকল বলশি থাকল , বা঩রলর দিকল কম া
কহষলতহরলর মানলর তারতমহয়লর জনহয়ল যবথ বলরলখার
বকহরতার জনহয় নহিত কণাগুলি ফপরলর বা
নিচলর দিকল যকটি ধতিরিকহত গতি লাভ করল -
ভণাতহমক ঩ললকহটহরন য়দি ফপরলর দিকল য়ায ,
ধনাতহমক নিফকহলিযাস য়ায নিচলর দিকল ; নর
঩ললকহটহরন নিচলর দিকল গললল নিফকহলিযাস ফপরলর
দিকল য়ায ( 4 নথ চিতহর ) া ফলল নধারলর মধহয়ল
ফপরল ও নিচল বিপরীত নধানয়ুকহত কণার
নধিকহয় হওযায যকটি ফপর নিচ বিদহয়ুতহকহষলতহর
ফতহপনহন হয া য঩ বিদহয়ুতহকহষলতহর ও বলযাকৃতি
চষমহবককহষলতহরলর সমহমিলিত পহরভাবল পহলাজমা
নধারলর বা঩রলর দিকল গতিশীল হয ও নধারলর
দলওযালল গিযল নঘাত করল া</p>

<p>য঩ সমসহয়ার সমাধান করা য়লতল পারল যকটি
ধতিরিকহত চষমহবককহষলতহর সৃষহটি করল , য়ার চষমহবক
বলরলখাগুলি গষণ ধকহষকল সরহবদা বলষহটন করল
থাকবল া টশকামহয়াক য়নহতহরল য঩ বলষহটনকার চষমহবক
কহষলতহর ত঳রি করা হয
পহলাজমার মধহয় দিযল বলযাকৃতি বিদহয়ুতপহরবাহ ফতহপনহন
করল ( 5 নথ চিতহর ) া বলযাকৃতি চষমহবক কহষলতহরল ও
বলষহটনকারী চষমহবক কহষলতহর , য঩ দু঩যলর সমনহ঵যল য়ল
চষমহবক বলরলখার ফতহপতহতি হয , তা সরহপিল
নকারলর - বলযলর দিক বরাবর চলতল চলতল
তা কিছুটা ফপর বা নিচ যবথ পাশলর দিকল সরল
য়লতল থাকল া বলযাকৃতি নধারলর গষণ ধকহষলর
সঙহগল নডানডি য়ল পহরসহথচহছলদ FGH 6নথ চিতহরল
দলখানশ হযলছল , ধরা য়াক যকটি চষমহবক
বলরলখা তাকল পহরথমল I বিনহদুতল ছলদ করল গলছল া
য় বলরলখাকল ধনুসরণ করলল দলখা য়াবল য়ল ,
বলযলর দিক বরাবর যকবার সমহপূরহণ ঘুরল নসবার
পর য় রলখার পহরসহথচহছলদ FGH-কল IJk বৃতহতলর
পরিধির ফপরিসহথিত ধনহয় কশন J বিনহদুতল ছলদ
করল য়াচহছল া বলরলখাটি নরশ ধনুসরণ করলল
দলখা য়াবল য়ল , বলযলর দিক বরাবর নর
যকবার ঘুরল যসল তা পহরসহথচহছলদ FHG-কল K
বিনহদুতল ছলদ করছল া য঩ ভাবল ছলদবিনহদু IJK
বৃতহতলর পরিধির ফপর দিযল কহরমাগত সরল
য়লতল থাকল া টশকামহয়াকল IPJ কশণলর গুরুতহ঵
ধনলকখানি ; যকল বলা হয নবরহতনজাত
পরিবরহতন া কহরুসকাল ও
সশফহরানভ নামল দুজন বিজহঞানী ততহতহ঵গতভাবল
পহরমাণ করলন য়ল , পহলাজমার সহথাযিতহ঵লর জনহয়ল
য঩ কশণলর মান 360 ডিগহরির চলযল ধবশহয়঩ কম
হতল হবল া সাধারণত ধনহয়ভাবল য শরহতটিকল
পহরকাশ করা হয া য় কশণকল i যবথ q = 360ডিগহরি/i
লিখলল সহজল঩ বশঝা য়ায য়ল , পহলাজমার
সহথাযিতহ঵লর জনহয়ল q-যর মান 1-যর চলযল বলশি
হত হবল া q-কল বলা হয ` নিরাপতহতা নিরহদলশক '
কারণ পহলাজমার ধসহতিতহ঵লর
নিরাপতহতা নিরহভর করল যর মানলর ফপর া</p>

<p>য়া হশক , কশন নহিত কণা য়খন সরহপিল
বলরলখা বরাবর চলতল থাকল , তখন তা কিছুকহষণ
বলযাকৃতি নধারলর ফপরলর ধরহধল থাকল ( য়লমন
6নথ চিতহরল I , J, K , বিনহদুতল ) , কিছুকহষণ থাকল
নিচলর ধরহধল ( য়লমন L , M , N বিনহদুতল ) া ফপর বা
নিচলর দিকল নহিত কণার য়ল কহষতিকারক গতির
কথা নগল নলশচনা করা হযলছল ( 4নথ চিতহর
দলখুন ) , যখনশ সলরকম গতি থাক কিনহতু মজার
বহয়াপার হল য঩ য়ল , যখন নর কণাটি মশটমাট
ফপর ব নিচলর দিকল য়ায না , গষণ ধকহষ থলকল যক঩
দূরতহ঵ল থলকল য়ায া 6নথ চিতহরলর সাহায়হয়ল
বহয়াপারটি বহয়াখহয়া করা য়লতল পারল া য঩ চিতহরল
পহরদরহশিত নধারলর পহরসহথচহছলদলর কলনহদহর p বিনহদুতল
গষণ ধকহষ যকল ছলদ করল গলছল া ধরা য়াক ,
কশন কণা J বিনহদুতল রযলছল যবথ তার গতি ফপরলর
দিকল া কণাটি তাহলল P বিনহদু থলকল সরল
য়লতল থাকবল া নবার পরল য়খন কণাটির
ধবসহথান M বিনহদুতল হবল , তখনশ তার গতি হবল
ফপরলর দিকল ; ফলল সল কিনহতু তখন ঱P বিনহদুর
দিকল সরল নসতল থাকবল া য঩ভাবল দলখানশ
য়ায য়ল , কণাটি য়তকহষণ নললর ফপরলর ধরহধল
থাকল , ততকহষণ সল য়তখানি গষণ ধকহষ থলকল
সরল য়ায , কণাটি নিচলর ধরহধল থাকলল ততখানি য়
ধকহষলর দিকল সরল নসল া সুতরসাথ মশটলর ফপর
সশ গষণ ধকহষ থলকল ক঩ দূরতহ঵ল থলকল য়ায া
য়ল সব নহিত কণার গতি নিচলর দিকল , তারাও
যক঩ কারণল মশটলর ফপর গষণ ধকহষ থলকল
যক঩ দূরতহ঵ল থাকল া ফলল নধারটির মধহয়ল ফপরল
বা নিচল নগলকার মতন নহিত কণার নধিকহয়
হয না া সলজনহয়ল কশন ফপর-নিচ বিদহয়ুতহকহষলতহর
ফতহপনহন হয না যবথ পহলাজমাও বা঩রলর দিকল সরল
য়ায না া য঩ বহয়াপারটি সমহভব হযলছল বলযাকৃতি
চষমহবক কহষলতহরলর সঙহগল বলষহটনকারী চষমহবক কহষলতহরল
য়শগ হওযার ফলল া</p>

<p>ধনহয় যকটি কারণল কিনহতু পহলাজমা যখনশ
বা঩রলর দিকল সরল য়াওযার পহরবণতা থাকল া
পহলাজমার মধহয় দিযল বিদহয়ুতহপহরবাহ চালিত হলল য়ল
বলষহটনকারী চষমহবক কহষলতহর ঩ফতহপনহন হয , বলযাকৃতি
নধারলর ভিতরলর দিকল পহলাজমার
ফপর য়ল চাপ P পহরয়ুকহত হয , বা঩রলর দিক থলকল
চাপ P যর চলযল তা বলশি ( 7 নথ চিতহর দহরষহটবহয় ) া
ফলল পহলাজমার পহরবণতা হয বা঩রলর দিকল সরল
য়াওযার া পহলাজমার য঩ গতি রশধ করবার জনহয়ল
বলযাকৃতি নধার থলকল কিছুটা ফপরল ও কিছুটা
নিচল তারকুণহডলী রলখল ও তাদলর মধহয় দিযল
বিদহয়ুতহপহরবাহ চালিত করল ফলহলমহব চষমহবক কহষলতহর সৃষহটি
করা হয া পহলাজমার মধহয়সহথ বিদহয়ুতহপহরবাহলর ফপর
য঩ চষমহবক কহষলতহরলর কহরিযার ফলল যকটি
ভিতরমুখী চাপ পহরয়ুকহত হয পহলাজমার ফপরল া য঩
চাপ য়দি সঠিক মানলর হয , তা হলল ভিতর ও
বা঩রলর থলকল পহরয়ুকহত পহলাজমার ফপর মশট চাপ
সমান হয যবথ পহলাজমার বা঩রলর দিকল সরল য়াওযার
কশন কারণ থাকল না া বলযাকৃতি
নধারলর ফপরল ও নিচল য়ল তারকুণহডলীর কথা
বলা হয , সলগুলিকল বলা হৃয ফলহলমহব কহষলতহর কুণহডলী া
য সব ছাডাও নধারলর
ফপরল ও নিচল ধনুভূমিক কহষলতহর কুণহডলী
বা ফলহলমহব সথশশধন কুণহডলী রলখল যবথ
তাদলর মধহয় দিযল বিদহয়ুতহপহরবাহ পাঠিযল যমন
ধতিরিকহত চষমহবককহষলতহর সৃষহটি করা হয য়ল , ফপর
বা নিচলর দিকল পহলাজমার সরল য়াওযার কশন
পহরবণতা থাকলল তাও য়াতল রুদহধ হযল য়ায া
য঩ভাবল টশকামহয়াক য়নহতহরল নানান চষমহবক কহষলতহর
দিযল ত঳রি ঴যক ধদৃশহয় পিঞহজরল ধতহয়ুষহণ পহলাজমাকল
ধরল রাখবার চলষহটা করা হয া</p>

<p>
বলযাকৃতি নধারলর মধহয় দিযল য়ল বিদহয়ুতহ পহরবাহলর
কথা নগল বলা হযলছল , তা য়ল
কলবল বলষহটনকারী চষমহবক কহষলতহর সৃষহটি করল , তা঩
নয , পহলাজমাকল ফতহপনহন করল তাকল ধনলকখানি
ফতহতপহত করল তশলল ; কারণ পহলাজমার ব঳দহয়ুতিক
রশধ নছল যবথ রশধয়ুকহত কশন
পদারহথলর মধহয় দিযল বিদহয়ুতহপহরবাহ চালিত করলল
শকহতি বহয়যিত হয ও তার রূপানহতর ঘটল
পদারহথটির তাপ শকহতিতল া বিজহঞানী জুললর
নামানুসারল য পদহধতিকল বলা হয ` জুল তপহতকরণ ' া
ধনলক সময নবার ওহমলর
নমানুয়াযী যকল বলা হয ` ওহমীয তপহতকরণ ' া
য়া হশক বলযাকৃতি নধারল
বিদহয়ুতহপহরবাহ ফতহপনহন করা হয বিদহয়ুচহচুমহবকীয
নবলশলর সাহায়হয়ল া টহরানহসফরহমার নামক য়ানহতহরিক
ফপাদানলর কথা নমরা ধনলকল঩ জানি -
টশকামহয়াক য়নহতহরল বলযাকৃতি নধার যকটি বড
টহরানহসফরহমার গষণকুণহডলী হিসলবল কাজ করল ( 8 নথ
চিতহর ) া য় টহরনহসফরহমারলর মুখহয় কুণহডলীর মধহয়
দিযল বিদহয়ুতহ পাঠালল যও সল঩ বিদহয়ুতহপহরবাহ
পরিবরহতনশীল হলল বিদহয়ুচহচুমহবকীয নবলশলর
ফলল বলযাকৃতি নধারলর মধহয়ল বিভব-পারহথকহয় বা
ভশলহটলজলর সৃষহটি হয া যর ফল বিদহয়ুতহপহরবাহলর
ফতহপতহতি ঘটল যবথ তা পহলাজমার সৃষহটি করল তাকল
ফতহতপহত করল তশলল া য জনহয়ল টহরানহসফরহমারর মুখহয়
কুণহডলীকল বলা হয ` জুল তপহতকরণ কুণহডলী ' া
8নথ চিতহরল য়লমন দলখানশ হযছল , টহরানহসফরহমারল তলমনি
লশহার কাঠামশ
বহয়বহার করলল জুল তপহতকরণ কুণহডলর মধহয় দিযল
ধপলকহষাকৃত ধলহপ বিদহয়ুতহপহরবাহ পাঠিযল বলযাকৃতি
নধারলর মধহয়ল ধপহরযশজনীয বিভব পারহথকহয়
সৃষহটি করা য়ায া তবল যর সীমবদহধতা থাকায যবথ
ধনহয়ানহয় দু যকটি ধসুবিধার জনহয়লও বরহতমানল
ধনলক টশকামহয়াক য়নহতহরল , বিশলষত খুব বড
য়নহতহরগুলিতল বাযু-মাধহয়মল঩ বিদহয়ুচহচুমহবকীয নবলশলর
কাজটি হযল থাকল া
 +&gt;*
",0
</p></body></text></cesDoc>