<cesDoc id="ben-w-literature-humour-bhu09" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-literature-humour-bhu09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>চীনলরঙহগ</h.title>
<h.author>঩নহদহরমি</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1990</pubDate>
</imprint>
<idno type="CIIL code">bhu09</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0636.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-22</date></creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;রহসহয়ালাপ া দলশ া 20 ধকহটশবর নবহব঩ া রমহয়রচনা া	
1893 খহরিষহটাবহদলর ধগষহটলর শলষদিকল সহ঵ামী
বিবলকাননহদ দিনকযলক ধধহয়াপক জল. য঩চ.
রা঩টলর যহয়ানিসহকুযাম গহরামলর বাডিতল থলকলছলন া
সলখানল যকদিন কযলকজন ঩থরলজ তার সঙহগল
দলখা করতল যললন া সহ঵ামী বিবলকাননহদ জগতল
঩থরলজদলর ধতহয়াচারলর কথা তুলললন - মানুষলর
পহরতি ভালশবাসার কথা কলবল তাদলর মুখল ,
ধনহতরল পাপ নর সরহবপহরকার হিথসা ছাডা নর
কিছু঩ নল঩ া তারা মুখল বলল "" ভা঩ , নমি
তশমাকল ভালশবাসি , "" কিনহতু সঙহগল সঙহগল গলায
ছুরি চালায া তাদলর হাত রকহতরাঙা া</p>

<p>সহ঵ামী বিবলকাননহদলর সুমিষহট গলার সহ঵র গমহভীর
হযল যল , তিনি নরও ধীরল ধীরল বলতল
লাগললন - কিনহতু পশহ঵রলর বিচার যকদিন তাদলর
ফপরলও নলমল নসবল া পহরভূ বললছলন ,
"" পহরতিশশধ নলব নমি , পহরতিফল দলব া ""
মহাধহ঵থস নসছল া য঩ পৃথিবীতল তশমাদলর
খহরিষহটানলরা সথখহয়ায কত ? সমগহর পৃথিবীর লশক
সথখহয়ার যক-তৃতীযাথশও নয া চলযল দহয়াখশ ,
লকহষ লকহষ চীনলদলর দিকল , পশহ঵রলর হাতিযার
হিসলবল তারা঩ নলবল যর পহরতিশশধ া তারা঩
তশমাদলর ফপর নকহরমণ চালাবল া নর যকবার
চলবল হুন-ধভিয়ান া</p>

<p>তারপর সহ঵ামী বিবলকাননহদ যকটু মুচকি হলসল
বলললন - তারা সমগহর ঩ফরশপকল ভাসিযল নিযল
য়াবল , কশন কিছুর঩ ধসহতিতহ঵ রাখবল না া নারী ,
পুরুষ , শিশু - সব ধহ঵থস হযল য়াবল া পৃথিবীতল
নলমল নসবল নবার যক ধনহধকার য়ুগ া যকথা
বলার সময সহ঵ামী বিবলকাননহদলর গলার সহ঵র ধবরহণনীয
ভাবল বিষণহণ হযল ফঠল া তারপর তিনি হঠাতহ বলল
ফঠললন - নমি - নমি কিছু঩ গহরাহহয় করি না া
য঩ ধহ঵থসসহতূপ থলকল পৃথিবী নরও ভালশভাবল গডল
ফঠবল া কিনহতু মহাধহ঵থস নসছল া পশহ঵রলর পহরতিশশধ
নর ধভিশাপ নলমল নসতল নর বলশী দলরী নল঩ া</p>

<p>সকলল পহরশহন করললন - শিগগির঩ কি সল঩
ধভিশাপ নলমল নসবল ?
সহ঵ামী বিবলকাননহদ ফতহতর দিললন - যক হাজার
বছরলর মধহয়ল য-ঘটনা ঘটবল !</p>

<p>সহ঵ামী বিবলকাননহদলর মতল বহু জাতি ভারতল
যসলছল কিনহতু ঩থরলজদলর চলযল খারাপ নর
কশনশ জাতি নসলনি া সলদিন সহ঵ামী বিবলকাননহদ
শলষকালল বললছিললন - ঩তিহাস ঩থরলজদলর
কাজলর পহরতিশশধ নলবল঩ া নমাদলর গহরামল-গহরামল
দলশল-দলশল য়খন মানুষ দুরহভিকহষল মরলছল তখন
঩থরলজরা নমাদলর গলা পা দিযল টিপল ধরলছল ,
নমাদলর শলষ রকহতটুকু তারা নিজলদলর তৃপহতির
জনহয় পান করল নিযলছল , নর নমাদলর দলশলর
কশটি-কশটি টাকা নিজলদলর দলশল চালান
দিযলছল া তাদলর ফপর ঝাতপিযল পডল চীনলরা঩
নজ তার পহরতিশশধ নলবল া নজ য়দি চীনলরা
জলগল ওঠল ও ঩থরলজকল সমুদহরল ঠললল ফললল
দলয - য়া তাদলর ফচিতহ পহরাপহয় - তাহলল সলটা
সুবিচার঩ হবল া</p>

<p>মিসলস বহরীডলর মুখল সহ঵ামী বিবলকাননহদ যকদা
যকজন চীনলমহয়ানলর গলহপ শুনলছলন -
শুযশরলর মাথস চুরি করল যকজন চীনলমহয়ান
ধরা পডলছল া তার বিচার হচহছল া বিচারক
বলললন - নমার ধারণা ছিল য়ল চীনলরা
শুযশরলর মাথস খায না া
ভাঙা-ভাঙা ঩থরলজীতল চীনলমহয়ান বলল ,
- সহয়র , নমি তশ যখন নমলরিকান -
নমি মললিকান , নমি বহলহয়াণহডি খা঩ , নমি
শুযশরলর মাথস খা঩ , নমি সব খা঩ া
গলহপটি শুনল সহ঵ামী বিবলকাননহদ খুব খুশি
হযলছলন , শশনার পর বহুবার তিনি ফিসফিস
করল বললছলন - নমি মললিকান !</p>

<p>1894-95 খহরিষহটাবহদল জাপান য়ুদহধ করলছল
চীনলর সঙহগল যবথ চীনকল পরাজিত করলছল া
1896 খহরিষহটাবহদল সহ঵ামী বিবলকাননহদ কিছুদিন
লণহডনল থলকলছলন - দকহষিণ বললগহরাডিযায যকটি
বাডিতল া যকদিন যকজন ভদহরলশক সলখানল
তাতর সঙহগল দলখা করতল যললন া কথায-কথায
তিনি জিজহঞলস করললন - পহরবল রণকুশল জাতি
না হযল কলফ কি কখনও বড হযলছল ?</p>

<p>বিনহদুমাতহর ঩তসহতত না করল সহ঵ামী বিবলকাননহদ
বলললন - হহয়াত , চীন হযলছল া ধনহয়ানহয় দলশলর
মধহয়ল নমি চীন ও জাপানল ভহরমণ করলছি া নজ
চীন যকটা ছতহরভঙহগ দললর মত হযল দাতডিযলছল ;
কিনহতু ফনহনতির দিনল ওর য়লমন সুশৃঙহখল
সমাজবহয়বসহথা ছিল , নর কশন জাতির য-পরহয়হয়নহত
তলমন হযনি া ধনলক বিষয - য়লগুলশকল
নমরা নজকাল ` নধুনিক ' বলল থাকি -
চীনল শতশত , যমনকি সহসহর-সহসহর বতহসর
ধরল - সলগুলশ পহরচলিত ছিল া দৃষহটানহতসহ঵রূপ
পহরতিয়শগিতা-পরীকহষার কথা ধরুন া
- চীন যমন ছতহরভঙহগ হযল গলল কলন ?
সহ঵ামী বিবলকাননহদ জবাব দিললন - কারণ চীন
সামাজিক পহরথা ধনুয়াযী মানুষ গডতল পারল
না া নপনাদলর যকটা চলতি কথা নছল য়ল
পারহলামলণহটলর ন঩নলর জশরল মানুষকল ধারহমিক
করতল পারা য়ায না া নপনাদলর ধনলক
নগল঩ চীনলরা সল কথা ঠলকল শিখলছিল া
ও঩ কারণল঩ রাজনীতির চলযল ধরহমলর নবশহয়কতা
গভীরতর া কারণ ধরহম বহয়বহারিক জীবনলর
মূলততহতহ঵ নিযল নলশচনা করল া //
1898 খহরিষহটাবহদল বহরিটলন , ফহরানহস ও জারহমানি
ধধিকার করল নিযলছল চীনলর কযলকটি বনহদর া
সহ঵ামী বিবলকাননহদলর মতল , চীনলরা হচহছল
সভহয়তার ধরহথাতহ ভশগবিলাসলর , সুখসহ঵াচহছনহদতার
নদিগুরু - ধনাদি কাল থলকল চীনল টলবিলল খায ,
চলযারল বসল , য়নহতহরতনহতহর করল খাওযার জনহয় , যবথ
কাটা পশশাক নানা রকম , ঩জার-জামা , টুপিটাপা
পরল ; পিতৃপুরুষদলর পূজাকরা চীনলর ধরহমলর
মূলভিতহতি া সহ঵ামী বিবলকাননহদ বললছলন : "" চীনল
মলযল-মদহদ সরহবদা নপাদমসহতক ঢাকা া চীনল
কনফুদলর চললা , বুদহধলর চললা , বড নীতিদুরসহত া
খারাপ কথা , চাল , চলন - ততহকহষণাতহ সাজা া
কহরিশহচান পাদহরী গিযল চীনল ভাষায বা঩বলল ছাপিযল
ফলললল া যখন বা঩বলল পুরাণ হচহছল হিতদুর
পুরাণলর চশদহদ পুরুষ - সল দলবতা মানুষলর ধদহভুত
কলললঙহকারি পডল চীনল তশ চটল ধসহথির া বললল ,
য঩ ব঩ কিছুতল঩ য দলশল চালানশ হবল না , য
তশ ধতি ধশহলীল কলতাব ; তার ফপর পাদহরিনী
বুকখশলা সানহধহয় পশশাক পরল , পরহদার বার হযল
চীনলদলর নিমনহতহরণল নহহ঵ান করললন া চীনল
মশটাবুদহধি , বললল - সরহবনাশ া য঩ খারাপ ব঩
পডিযল , নর য঩ মাগীদলর নদুড গা দলখিযল ,
নমাদলর ছশতডাদলর ব঩যল দিতল য ধরহম যসলছল া
য঩ হচহছল চীনলর কহরিশহচানলর ফপর মহাকহরশধ া নতুবা
চীনল কশনও ধরহমলর ফপর নঘাত করল না া
শুনছি য়ল পাদহরীরা যখন ধশহলীল ধথশ তহয়াগ করল
বা঩বলল ছাপিযলছল ; কিনহতু চীনল তাতল নরও
সনহদিহান া ""</p>

<p>সহ঵ামী বিবলকাননহদলর মুখল সিসহটার কহরিষহটান
যকদিন শুনলছলন - রাশিযা ধথবা চীনল
ধতদপর বিরাট ধভহয়ুথহথানলর ফলল নবয়ুগলর
সূতহরপাত হবল া ঠিক কশন দলশল বিরাট ধভহয়ুথহথান
হবল তা সহপষহট করল বলতল পারছি না - কিনহতু তা
রাশিযায কিথবা চীনল঩ হবল া</p>

<p>চীন দলশলর নভহয়নহতরীণ ধবসহথা ধতহয়নহত খারাপ ,
পাশহচাতহয়লর শকহতিমান দলশগুলশ চীনকল ভাগাভাগি
করল শশষণ করল নিচহছল , জাপানও তাদলর দলল
ভিডলছল - য বিষযল শিষহয় মনহমথনাথ গঙহগশপাধহয়ায
যকদিন জিজহঞলস করললন - যত পুরনশ সভহয়
যকটা দলশ - যবার কি শলষ হযল য়াবল ?
সহ঵ামী বিবলকাননহদ ধলহপকাল চুপ করল র঩ললন া
তারপর বলললন - নমি দলখলছি - যকটা পহরকাণহড
হাতির পলটল যকটা বাচহচা হযলছল া সল঩ বাচহচাটা
ভূমিষহঠ হল - কিনহতু সলটা যকটা সিথহশাবক া য঩
বাচহচাটা বড হবল া তখন নতুন চীন তশযলর
হবল া</p>

<p>চীন সমহপরহকল সহ঵ামী বিবলকাননহদলর নর যকটি
ভবিষহয়তহ বাণী বিশলষভাবল ফলহললখয়শগহয় া বিশহ঵সহতসূতহরল
মলরী লু঩ বারহক জলনলছলন য়ল সহ঵ামী বিবলকাননহদ
বললছলন - ঩থরলজরা য়খন ভারত ছলডল চলল
য়াবল - য়দি য়ায - তখন চীন করহতৃক ভারত
জযলর মসহত নশঙহকা নছল া
মাঞহচুবথশলর শলষ সমহরাট যকজন নাবালক া
নাবালক সমহরাট সিথহাসনল বসলছলন , রাজহয়
চালিযলছলন মুরুবহবিরা া</p>

<p>1907 খহরিষহটাবহদল চিযাথ কা঩-শলক জাপানল
মিলিটারি কললজল ভরহতি হযলছলন ; সলখানল
তিনি চীনলর নিরহবাসিত বিপহলবী সান ঩যাতহ-সলনলর
শিষহয়তহ঵ গহরহণ করলছলন া</p>

<p>1911 খহরিষহটাবহদল চীনল বিপহলব হযলছল া বিপহলবল
সান ঩যাতহ-সলনলর পহরধান সঙহগী ছিললন চিযাথ
কা঩-শলক া বিপহলবল ধবসহথা যমন হযল ফঠল য়ল
মাঞহচুবথশলর পকহষল নর রাজতহ঵ চালানশ সমহভব
হল না ; নাবালক সমহরাটকল ধগতহয়া সিথহাসন
ছলডল দিতল হল া</p>

<p>বিপহলবলর পর সান ঩যাতহ-সলন চীনল পহরজাতনহতহরলর
পহরথম পহরলসিডলণহট নিরহবাচিত হযলছলন যবথ ধলহপদিন
পর পদতহয়াগ করলছলন া ঩তিপূরহবল তিনি বিদলশল
গিযল পহরবাসী চীনলদলর সহাযতায জাতীয দল
গডলছলন া</p>

<p>সান ঩যাতহ-সলনলর পতহনীর কনিষহঠা সহদশরাকল
চিযাথ কা঩-শলক , 1927 খহরিষহটাবহদলর 1 ডিসলমহবর ,
বিবাহ করলছলন ; য঩ বিবাহলর কিছুকাল নগল ,
1925 খহরিষহটাবহদলর 12঩ মারহচ , সান ঩যাতহ-সলন মারা
গলছলন া চীনল সান ঩যাতহ-সলন পহরজাতনহতহরলর
জনক ; চীনলর ঩তিহাসল তাতর নাম ধবিসহমরণীয
হযল নছল া</p>

<p>মাঞহচুবথশলর পরল নর কশনশ রাজবথশ চীনল
রাজতহ঵ করলনি া পহরসঙহগত ফলহললখয়শগহয় , বিনযকুমার
সরকার লিখলছলন : "" সমগহর চীনল করহতৃতহ঵ করা
কশনশ বথশলর঩ সকল নৃপতির পকহষল সমহভবপর
হয না঩ া চীন বহুবার ভাঙহগিযাছল , চীনলর ভিতর
ধসথখহয় ঘরশযা লডা঩ , বিদহরশহ , দাঙহগাহাঙহগামা
ঘটিযাছল া ধধিকনহতু , ফতহতর যবথ পশহচিম হ঩তল
বহিদশতহরুর নশঙহকা চীনল সরহবদা঩ ছিল া য঩ কারণল
ধনলক সমযল চীনলর কিদযথশ পরহসহতগত হ঩যাছল ,
যবথ ধবশিষহটাথশ ভিনহন ভিনহন সহ঵াধীন রাষহটহরলর ধধীন
রহিযাছল া ফলত ধখণহড চীনলর সামহরাজহয়ভশগ
ধধিক সথখহয়ক নরপতির কপালল জুটল না঩ া
কযলকটি রাজবথশলর দু'যকজন মাতহর য়থারহথ
` রাজ চকহরবরহতী ' ছিললন া ""</p>

<p>মাঞহচুবথশলর ধবসানলর পর বছর দশলক বাদল
- 1921 খহরিষহটাবহদলর কথা - চীনল কমিফনিষহট
পারহটি পহরতিষহটিত হযলছল া মাও সল তুঙ
চীনল কমিফনিষহট পারহটির ধনহয়তম পহরতিষহঠাতা া
1923 খহরিষহটাবহদল চিযাথ কা঩-শলক কুওমিনটাথ
পারহটিতল পহরাধানহয় লাভ করলছলন া গশডার দিকল
তিনি কমিফনিষহটদলর সঙহগল সকহরিয ছিললন কিনহতু
1927 খহরিষহটাবহদ থলকল ধনহয়রকম া 1948
খহরিষহটাবহদল চিযাথ কা঩-শলক চীনলর পহরথম
পহরলসিডলণহট নিরহবাচিত হযলছলন া 1928 থলকল
1948 খহরিষহটাবহদ পরহয়হয়নহত জাতীযতাবাদী নলতা
হিসলবল চিযাথ কা঩-শলক বিপুল কহষমতার
ধধিকারী হযলছিললন া //
1931-45 খহরিষহটাবহদল দহ঵িতীয চীন-জাপান য়ুদহধ
হযলছল া শলষকালল দহ঵িতীয বিশহ঵য়ুদহধলর সঙহগল মিশল
গিযলছল চীন-জাপান য়ুদহধ া চীন মিতহরশকহতির
পকহষল , জাপান মিতহরশকহতির বিপকহষল ; মিতহরপকহষলর
সহাযতা সতহতহ঵লও 1945 খহরিষহটাবহদলর যপহরিল পরহয়হয়নহত
চীন য়ুদহধল সুবিধা করতল পারলনি া</p>

<p>1927 খহরিষহটাবহদল কমিফনিষহটদলর সঙহগল জাতীযতাবাদী
দললর বিবাদ নরমহভ হযলছল , বিবাদ থলকল
গৃহয়ুদহধ ; দহ঵িতীয চীন-জাপান য়ুদহধলর সমযল
দু-জন কিছুকাল গৃহয়ুদহধ বনহধ রলখল যকতহরল
জাপানীদলর সঙহগল লডা঩ করলছল া দহ঵িতীয
বিশহ঵য়ুদহধলর পর নবার চীনল দু-দলল গৃহয়ুদহধ
নরমহভ হযলছল া কমিফনিষহট নলতা মাওসল-তুঙ ,
জাতীযতাবাদীদলর নলতা চিযাথ কা঩-শলক া</p>

<p>রাজন঳তিক সমহমললন হযলছল - সমগহর দলশলর
জনসাধারণলর পহরতিনিধিরা সমহমললনল নতুন চীনলর
যকটি কলনহদহরীয গভরহণমলণহট গঠন করলছল া নতুন
গভরহণমলণহটলর সভাপতি নিরহবাচিত হযলছলন মাও
সল-তুঙ া 1949 সাললর 1লা ধকহটশবর - সলদিন
নতুন গভরহণমলণহটলর , নতুন চীনলর জনহমদিন ;
সলদিন পিকিথ থলকল জনসাধারণলর কাছল নুতুন
গভরহণমলণহটলর কথা ঘশষিত হযলছল া 1950
খহরিষহটাবহদলর জানুযারীতল সশভিযলত ঩ফনিযন , গহরলট
বহরিটলন যবথ ধনহয়ানহয় দলশ চীনলর নতুন গঠিত
গভরহণমলণহটকল সহ঵ীকৃতি দিযলছল া</p>

<p>পরাসহত হযল চিযাথ কা঩-শলক পালিযল
গিযলছলন তা঩ওযানল া 1950 খহরিষহটাবহদলর
যপহরিলল জাতীয সরকারলর পহরধান হিসলবল চিযাথ
কা঩-শলকলর দখলল র঩ল শুধু তা঩ওযান দহ঵ীপ
ও পহরধান ভুখণহডলর ধদূরল ধবসহথিত কযলকটি
ছশট-ছশট দহ঵ীপ া নর মূল ভূখণহডল সহথাযী
কমিফনিষহট সরকার ; সলখানল নবার বলি ,
নতুন গভরহণমলণহটলর সভাপতি মাও সল-তুঙ া</p>

<p>1949 সাললর 1লা ধকহটশবর থলকল , সহ঵চহছনহদল
বলা চলল চীনলর নতুন কিথবা বরহতমান য়ুগ
নরমহভ হযলছল া</p>

<p>জনসথখহয়ার হিসলবল চীন বিশহ঵লর পহরথম যবথ
নযতনলর হিসলবল বিশহ঵লর দহ঵িতীয বৃহতহতম দলশ া
বিশহ঵লর চিতহরকলার ঩তিহাসল চীনলর চিতহরকথা
বিপুলভাবল সমহমানিত া হরিণলর লশম থলকল
বিশলষভাবল পহরসহতুত তুলি চীনদলশল চিতহররচনার
জনহয় খহরিষহটপূরহব তৃতীয শতাবহদীতল ফদহভাবিত
হযলছল া চীনলর চিতহরকলার ঩তিহাসল চতুরহথ
শতাবহদীর বিশিষহট কবি ও চিতহরকর কু খা঩-চি
বিশলষভাবল সহমরণীয া তাওবথশলর সমহরাটদলর
রাজতহ঵কাল ( 618-906) চীনলর চিতহরকলার
সুবরহণয়ুগ হিসলবল চিহহনিত হযল নছল া ধনহতত দু
হাজার বছর নগল চীনল চিতহরাঙহকনলর বিধিসমহমত
পদহধতির সূতহরপাত যবথ সুপহরাচীন কাল থলকল঩
চীনলর চিতহরকলায নিরহদিষহট নিযমানুয়াযী
ধঙহকনপহরণালী বদহধমূল া চীনলর শিলহপধারা দলখল
বিশহ঵লর জহঞানীগুণীরা নিদসথশয হযলছলন য়ল চীনলর
শিলহপীদলর তীকহষহণ সষনহদরহয়হয়বশধ বিসহমযজনক া
শুধু চিতহরকলায নয , নরও বহু বিষযল চীন
সুপহরাচীন কাল থলকল তরহকাতীত সাফলহয়লর পহরমাণ
দিযলছল া</p>

<p>চীন বরাবর শিকহষায-দীকহষায নগহরহী া শুনলছি
শিকহষাদীকহষার সঙহগল রঙহগকষতুকলর ঘনিষহট সমহপরহক
নছল া কথাটা য়গি মিথহয়ল না হযল থাকল তাহলল
সুপহরাচীন কাল থলকল চীনলর রঙহগকষতুকচরহচা নিতানহত
সহ঵াভাবিক া</p>

<p>চীনল নতুন য়ুগ নরমহভ হওযার ধলহপ কিছুকাল
নগল চীনলরঙহগলর সূতহরল চীনলর যকজন পণহডিত
মনহতবহয় করলছলন - "" The fact is , the Chinese
are both a funny and humorous people.
The Chinese people do many fantastic and
contrary things. Intrinsically , these are
funny to Westerners , as Western things are
funny to the Chinese. And if you can make
the Chinese see that many of their own
things are funny , they are quick to laugh
at them , for have they not been laughing
at them all their lives ? ""</p>

<p>নিজলদলর নিযল য়ারা রঙহগকষতুক করতল
পারলন তাতরা রঙহগকষতুকলর সাধনায নিদসনহদলহল
সিদহধিলাভ করলছলন া</p>

<p>সুপহরাচীন কাল থলকল চীনলর ভাণহডারল রঙহগকষতুক
জমা হযল নছল া কিছু চিনলরঙহগলর নমুনা
যবার দাখিল করা য়লতল পারল া চীনরঙহগলর
নমুনার পালা নরমহভ করা য়াক া</p>

<p>নগল কখনশ নযনা দলখলননি যমন দুজন
মহিলাকল নিযল চীনলরঙহগলরর নমুনার পালা
নরমহভ করি া শাশুডি যসলছলন মলযলর
বাডিতল া দুজনলর কলফ কখনও নযনা
দলখলননি া
জামাতা বাজার থলকল ছশট যকখানা নযনা
কিনল নিযল যল া যনল বফযলর হাতল দিল া
নযনায বফ নিজলর মুখ দলখল া মাকল
বলল - মা , নমার সহ঵ামী বাডিতল নর
যকজন বফ নিযল যসলছল া
মলযলর হাত থলকল নযনাখানা নিযল শাশুডি
বলললন - দলখি া
নযনায নিজলর মুখ দলখল শাশুডি বলললন
- শুধু কি নরলকজন বফ নিযল যসলছল ,
নরলকজন শাশুডিও নিযল যসলছল া</p>

<p>তারপর ভুলশমন ভদহরলশক নিযল চীনলরঙহগ া
নিজলর ঘশডার গাডিতল চলপল ভদহরলশক বাডি
ফিরল নসছলন া হঠাতহ যকজন বনহধুর কথা মনল
পডল ; বনহধু নতুন বাডি করলছলন , নতুন বাডির
ঠিকানা দিযলছলন , বার বার য়লতল বললছলন ,
কিনহতু য়াওযা হযল ওঠলনি া
বাডি ফলরার পথল নজ঩ বনহধুর নতুন বাডি
হযল য়াওযা য়াক া গাডশযানকল সল঩ কথা঩
বলললন ভদহরলশক া কিনহতু কথাটা গাডশযান
শুনতল পলল না া
ঘশডার গাডি যসল ধতযব নিজলর বাডির
সামনল থামল া কিনহতু ভদহরলশক ভাবললন য়ল
তিনি বনহধুর নতুন বাডিতল যসলছলন া
বাডিতল ঢুকল ভদহরলশক ধবাক হযল
ভাবললন - নমার বনহধু ঠিক নমার বাডির
মতশ বাডি করলছলন া
তারপর দলওযালল যকখানা ছবি দলখল ভদহরলশক
ভাবললন - নমার বাডির দলওযাললর ছবি বনহধুর
বাডির দলওযালল কলন ?
ঠিক তখন঩ ঘরল ঢুকল নিজলর ভৃতহয় া
ভদহরলশক রাগ করল তাকল বলললন - তুমি
য বাডিতল যসলছ কলন ?
",0
</p></body></text></cesDoc>