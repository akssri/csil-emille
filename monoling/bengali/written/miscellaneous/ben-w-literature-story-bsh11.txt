<cesDoc id="ben-w-literature-story-bsh11" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-literature-story-bsh11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>সুন্দরবন</h.title>
<h.author>শিবমিত্র</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book.</publisher>
<pubDate>1982</pubDate>
</imprint>
<idno type="CIIL code">bsh11</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 1011.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-22</date></creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;আজ তিনদিন হলো ওরা এসেছে এই বক-খালে । এসেছে নোনা-
খালি আবাদ থেকে । কযরা নদীর উত্তরে নোনাখালি খুলনা জেলার
অন্য়তম দীর্ঘ আবাদ । উঁচু-ভেড়ি গোল হযে ঘিরে আছে এই
আবাদকে । ভেড়ির পাশে পাশে বসতি । মাঝ খানে ধুধু করছে ধানের
খেত । খেতের ঠিক মাঝে কিন্তু আজও জমি ওঠেনি । সারা বছরই
জল থৈ-থৈ করে সেখানে । এই আবাদেরই পূর্ব দিকে আযনা মিস্ত্রির
বাড়ি । বাড়ির পাশেই এক অশ্঵থ্থ গাছ । হিন্দুই হোক আর মুসলমানই
হোক , সকলেই দূর দূর গ্রাম থেকে এসে এই গাছের ঝুরি ও ডালে
ইট বেঁধে মানত করে বনদেবীর নামে । লোনা দেশে অশ্঵থ্থ গাছ
হওযাই এক আশ্চর্য়্য়ের বিষয । তাই ভাটি-দেশের মানুষ অবাক হযে
বিশ্঵াস করে - এই গাছের আশ্চর্য়্য় ক্ষমতা না থেকেই য়ায না ।
পাড়ার ছেলেমেযেদের কাছে রাত্রে এই অশ্঵থ্থ গাছ এক ভীতির
বস্তু । অন্ধকারে তারা কেই এর ধারে কাছে আসতে চায না । কিন্তু
দিনের আলোয এই গাছের তলায ছেলেমেযেদের হৈ-হল্লা লেগেই
থাকে । পাড়ার দশ বছরের মেযে মমতাজও ছেলেমেযেদের কোলাহলের
টানে রোজই আসে এই গাছের তলায । দু'একটা খেলাতেও য়োগ
দেয । তবু মমতাজ কেমন য়েন নির্জীব । বেশিরভাগ সময চুপচাপ
বসে থাকে । কারোর সাথে কথা পর্য়্য়ন্ত বলে না ।</p>

<p>অন্য়েরা তা সইবে কেন ? তারা কখনও অনুরোধ করে , কখনও বা
টেনে জোর করে খেলায নামাতে চায । নামেও মমতাজ । কিন্তু আযনা
মিস্ত্রিকে একবার দেখলে কেউ আর মমতাজকে টানাটানি করতে য়ায
না । আযনা মিস্ত্রি য়েন মমতাজের এক আশ্রয । মমতাজ ব্য়াকুল হযে
আযনা মিস্ত্রির অপেক্ষা করে - না , আযনা মিস্ত্রি মমতাজের টানে
আসে এই অশ্঵থ্থ গাছের তলায - তা বলা দুঃসাধ্য় । মিস্ত্রি এলে
মমতাজ তার কাছে গিযে বসবে , আর এটা-সেটা গল্প করেই চলবে ।
মিস্ত্রি মমতাজকে কাছে পেলেই বলবে , - জানিস , তোর আম্মা
আমাকে কেমন ঘটা করে সাবুদ খাইযেছিল ! কত রকম য়ে মাছ
খাইযেছিল , তা তুই বলতেই পারবি না !</p>

<p>মাযের কথা উঠতেই মমতাজ য়েন মোহগ্রস্ত হযে পড়ে । মাযের
খুঁটিনাটি কথা শুনবার আগ্রহ তার থামতে চায না । বলে , - নানা ,
আম্মা কেমন রাঁধতো ? আচ্ছা , আম্মা কোন পথে জল আনতে
য়েতো ? তার মাথায কি ঘোমটা থাকতো ? ... প্রশ্নের য়েন অন্ত
নেই । সে - প্রশ্নের কারণ অকারণও নেই ।</p>

<p>আযনা মিস্ত্রি এমনিতে সব সময বিরক্ত হযে থাকলে কি হবে
মমতাজকে একবার কাছে পেলে গল্প করবার , আজে-বাজে কথা বলবার ,
আমোদ-আহ্লাদ করবার নেশা তাকে পেযে বসে । সে আজ দশ
বছরের কথা - তার সবচেযে ছোট মেযে মারা য়াবার পর থেকে মিস্ত্রি
এমনিধারা খিটখিটে হযে গেছে । এখন মমতাজকে পেলেই বুড়ো অন্য়
রকম হযে য়ায । অশ্঵থ্থ গাছের ছাযায এসে ঘণ্টার পর ঘণ্টা তার
তামাক খাওযার আকর্ষণও সেই জন্য়েই ।</p>

<p>মমতাজের বা'জান রহিম বহুদিন ধরে বাওযালির কাজ করে ।
বাওযালির কাজ মানে , প্রাযই বনে য়েতে হয । বাঘ তাড়াবার কাজ ।
কাঠ , মধু বা গোলপাতা কাটবার দলের রক্ষক হযে বাওযালিদের
সুন্দরবনে য়াওযা মানে - এক এক সমযে একমাস দু'মাস কাটিযে
আসা । কাজেই বছরের বাকি অল্প সমযটুকু য়খন সে বাড়ি থাকে ,
মা-হারা মমতাজকে য়েন বুকের মধ্য়ে করে রাখে । আদরে য়েন সে
মাযের স্নেহকে পূরণ করে দিতে চায ।</p>

<p>এবার অবশ্য় মাত্র তিন-চার দিনের জন্য় বনে এসেছে । জ্঵ালানি
কাঠ কাটবে । নিজের সংসারের জন্য় । জ্঵ালানি কাঠ কাঠতে তেমন
হাঙ্গামা বা গাছ বাছাবাছির ব্য়াপার নেই । বনে আসবার সময মমতাজ
বা'জানকে আব্দার করে বলেছিল , - এবার তোমাকে আমার জন্য়
একটা পাখি এনে দিতেই হবে । কত বার বললে , এনে দেবে ।
একবারও তুমি দাও না !</p>

<p>আব্দারের উত্তরে বাওযালি য়েন অপরাধীর মতো বললো , - বরাবরই
তো পরের কাজে বনে য়াই । এবার তো নিজের কাজ , নিশ্চয সময
অনেক পাবো । এবার তোকে ঠিকই এনে দেবো । তুই কিন্তু ঘরদোর
সামলে রাখিস ।</p>

<p>আজ তালতলার বনে সাঁঝের বেলা পাখির কথা উঠতে বুড়ো
মিস্ত্রি তিতিযে উঠেছিল বটে , কিন্তু মমতাজের আব্দারের কথা
শুনতেই আর দ্঵িতীয কথাটি বলেনি । //
চাচার উত্তরের অপেক্ষা না করেই আছের কুড়ুল হাতে ডিঙি থেকে
নেমে চললো । শিষেতে তখন জল নেই বললেই চলে । শিষে ধরেই
এগিযে চলেছে । পরিখার মতো শিষেটি । বেশ গভীর । নিচুতে দাঁড়ালে
ওপর থেকে আছেরের মাথা দেখা য়ায কি য়ায না । তলদেশ পাঁচ-ছ'
হাত চওড়া । জল সামান্য় থাকলে কী হবে , খাদে ভীষণ কাদা ।
পলি মাটির কাদা । চোরাবালির মতো । এতে পা চেপে দিলে কোমর
পর্য়ন্ত সড়সড় করে দেবে য়াবে । এঁটেল মাটি । একবার পা বসে গেলে
য়েন কামড়ে ধরে রাখে ।</p>

<p>আছের এই কাদা এড়িযে পাড় ঘেঁষে ঘেঁষে শিষে ধরে এগিযে
চলেছে । কিছুদূর গিযে একবার পিছন ফিরে দেখে , চাচা কানে
আঙুল দিযে ঝুপঝুপ করে খালের জলে ডুব দিচ্ছে । কামটের ভযে
কযেকটা ডুব দিযেই নৌকায উঠে পড়লো ।</p>

<p>শিষের ভিতর দাঁড়িযে আছের দু'পাশের গাছে দেখবার চেষ্টা করে ।
না । ভালো দেখা য়ায না । সামনেই শিষে বাঁক নিযেছে । তাও
এবার পেরিযে গেলো । না , এমন করে হয না । উপরে উঠতেই
হবে । নিশ্চযই এখানে ভালো খুঁটির গাছ পাওযা য়ায ।</p>

<p>আছের পাড়ের খাড়াইতে বুক লাগিযে গাছের শিকড় ধরে হিঁচড়ে উপরে
উঠেছে । উঠে গাছ দেখবে কি , নজরে পড়ল দুটি বড় বড় চোখ গাছের
গুঁড়ির আড়ালে জ্঵লজ্঵ল করছে । ওর দিকেই তীক্ষ্ণ দৃষ্টি । এটা কী
জন্তু ? বাঘ ? না ! বাঘ হবে এই এতবড় জন্তু - চার পাযে দাঁড়িযে
ফুলতে থাকবে । হলদে কালো ডোরা । এ য়ে কালো-মতো জানোযার
লম্বা হযে মাটির সঙ্গে মিশে আছে । বাঘ হলে তো বীর বিক্রমে
গাঁ গাঁ করে হেঁকে উঠতো । কৈ , এতো নিঃশব্দে পড়ে আছে । না -
এ বাঘ না !</p>

<p>কিন্তু কী তীক্ষ্ণ দৃষ্টি - কী হিংস্র চাহনি । আছের বেশ অবশ হযে
আসছে । চিত্কার করে উঠলো , - চাচা ! এটা কী জন্ত ? চা- চা , এটা
কী ...</p>

<p>অবকাশ দেয না সুন্দরবনের রযেল বেঙ্গল টাইগার । ছোট হলে
কি হবে - হিংস্র গর্জনে ঝাঁপিযে পড়লো আছেরের উপর ।
আত্মরক্ষার জন্য় আছের কুড়ুল বাগিযেছিল । কুড়ুলের আঘাত
উপেক্ষা করেই বাঘ ঝাঁপিযে পড়লো । কুড়ুল ছিটকে পড়ে গেলো ।
থাবা মারলো বাঁ কাঁধ লক্ষ্য় করে । আছের কাঁধ সরাবার চেষ্টা করতেই
নকের আঁচড়ে বাঁ দিকের বাহুর কযেক পরদা মাংস উঠে এল ।
আছেরের ডাক শুনতে না শুনতেই বাঘের হুঙ্কারে চাচার ব্য়াপার
বুঝতে দেরি হযনি । কোনও উপায নেই । কি-ই বা সে করবে !
করবার কিছুই নেই তার । দ্রুত ডিঙির বাঁধন খুলে দিযে বড় নদীতে
পড়তে চাইলো । একবার শুধু বললো , - চেযেছিলি বাঘ দেখতে !
দেখলি তো বাঘ !</p>

<p>কিছুক্ষণ থেকে দাঁত কামড়িযে বললো , - সাধ মিটেছে । বাঘ দেখার
সাধ মিটেছে !
বাঘ দু'পাযে দাঁড়িযে থাবা মারতেই আছের পড়ে গেলো । ঠিক
শিষের কিনারায ছিল । পড়ে গেলো শিষের ভিতর । কোনমতে একটা
গাছের শিকড় ধরে টাল সামলে দাঁড়িযে গেছে শিষের দেওযাল ঘেঁষে ।
আছের পড়ে য়েতেই বাঘ দু'পাযে দাঁড়িযে টাল সামলাতে পারে
না । পড়ল শিষের ভিতর গড়িযে পলিমাটির কাদায । চটাং করে চার
পাযে লাফ দিযে উঠতে গিযে চার পা-ই দেবে গেল পলিমাটির চোরা
কাদায । য়ত জোর দেয ততই য়েন দেবে য়েতে থাকে ।</p>

<p>আছের এবার হিংস্র হযে ওঠে । না , তাকে উঠতে দেওযা হবে না ।
উঠলেই আমাকে ও শেষ করে ফেলবে । ঝাঁপিযে পড়ল বাঘের উপর ।
শরীরের সমস্ত ওজন ও শক্তি দিযে চেপে ঠেসে দিতে লাগলো কাদার
ভিতর ।</p>

<p>বাঘ তবু ঘাড় বাঁকিযে কামড়াতে চায । বেপোরোযা হযে আছেরই
ওর ঘাড়ের উপর হিংস্কভাবে কামড়ে ধরলো । এ য়েন আছেরের মরণ-
কামড় । বাঘের চামড়া ও মাংস ভেদ করে য়ায আছেরের হিংস্র দাঁত ।
শিষেতে ঝিরঝির করে লোনা জল বযে চলেছে । ঘাড় পর্য়্য়ন্ত
দেবে গেছে বাঘের । লোনা জল চোখে নাকে ও মুখে ঢুকে দম
আটকে আসতে থাকে । আছের পিঠের উপর চড়ে আছে । এবার
ওর মাথা চেপে দিতে থাকে জলে ও কাদায ।</p>

<p>আছের অনুভব করে , বাঘের পিঠে আর য়েন জোর নেই । পিঠ
দুমড়িযে শক্তি জড়ো করার আর চেষ্টা করে না । দম আটকে বাঘ
মৃত । তবু পিঠ ছেড়ে আছের উঠতে চায না । বিশ্঵াস নেই ওকে ।
লেজটা এখনও উঁচু হযে আছে কাদার উপর । লেজের কালো-হলদে
ডোরা দেখে আছেরের হাসি পেযে গেলো । খুব জোরে সে এরবার
হেসে উঠলো । চিত্কার করে বললো , - বাঘ ! //
এ-পাশ ও-পাশ মুখোমুখি দুই ঘর । বড় মিঞা রসুল ও ছোট
মিঞা ফজলের । এক সংসার বলা চলে , তাহলেও দুই হাঁড়ি । বড়
বউযের উস্কানিতেই এক উঠানে এরা দুই ঘর করেছে । সেদিন বড় বউ
উঠান পেরিযে ছোট মিঞার নিন্দাই শুনতে এসেছিল । কিন্তু ফরিদার
গলায ফজলের প্রতি দুর্বলতার ইঙ্গিত পেযেই তখনকার মতো উঠান
পেরিযে নিজের কাজে মন দিল ।</p>

<p>সে-বছর ফজলের সংসারে দুর্দিন এলো । এলো কিন্তু ফজলের
গান-পাগলামির জন্য় নয , এলো মিষ্টি ধানের দেশে নোনার দাপটে ।
ভরা ভাদরের গাঙ্গে লোনা পানি য়েন থৈথৈ করে উঠলো ভেড়ির
মাথা অবধি । ঘোর অমাবস্য়ার খরতর টান । রাতের অন্ধকারে চকের
মানুষ বেরিযে পড়লো আলো ও কোদাল হাতে । ভেড়ি কোথাও
ধ্঵সে পড়েছে , সবাইকে জড়ো করে বুক পেতে ঠেকাতে হবে । একবার
এই লোনা বিষ ঘেরে প্রবেশ করলে আর রক্ষা নেই । সে বিষে এ
চকের মানুষ জর্জরিত হবে সারা বছর ।</p>

<p>পুবে হাণওযা দিল । ঝরঝর ধারায বাদল নামছে । চাষিরা
জড়সড় । ভেড়ি কোথাও ধ্঵সে গেলে এরা হযতো কোদাল ও হাতের
ক্ষিপ্রতায প্রতিরোধ করতে পারত । কিন্তু পূবে হাওযা গাঙ থেকে-
থেকে আরও ফুলে উঠে । ভেড়ি ছাপিযে লোনা পানি সর্বত্র উপছে
পড়তে থাকে মাটির কৃত্রিম বাঁধনকে উপেক্ষা করে । এমন সর্বগ্রাসী
আক্রমণকে চাষিরা সেদিনের মতো ঠেকাতে পারে না । ভাদ্রমাসের
অমাবস্য়ার জোযারকে আমাদের মানুষ য়মের মতো ভয করে । ভয
নিরর্থকও নয । সে-বছর হাহাকার দেখা দিল শুধু ফরিদার সংসারে
নয , চকের প্রতি সংসারে । ধানের বদলে চিটের বোঝাই উঠেছিল
প্রতি খলেনে ।</p>

<p>ফাল্গুন পেরিযে চোত মাস পড়েছে । কিন্তু আর তো সংসার
চলে না । ফজল দাওযায বসে একতারা নিযে নাড়াচাড়া করছিল ।
ফরিদা মুখ-ঝাড়া দিযে উঠলো , "" ওতেই কি পেঠ ভরবে ? ""
ফজল অবাক হয , "" কেন ? খালুই ভরে তো মাছে এনে দিলাম
ভোর সকালে ! ""
"" মাছেই পেটের আগুন নিভবে ? ধানের মোড়াটা দেখেছো ?
একবার ডালা উল্টে দ্য়াখো ক'পালি আর ধান আছে ? ""
"" কি করতে বলো আমাকে ? ""
"" কি আর বলবো ? আমার মুণ্ডুশ্রাদ্ধ করো । য়াও-না বনে একবার ।
দ্য়াখো- কত লোক কত ভাবেই তো এটা-সেটা আয করছে । তুমি
য়েতে পারো না ? ""
"" বেশ , আমাকে তুমি বাঘের পেটে য়েতে বলছো ? ""
"" ছিঃ ! বাঘের পেটে য়েতে বলবো কেন ? কেন , য়ারা বনে উঠেছে
তারা সবাই বুঝি বাঘের পেটে য়াচ্ছে ? ধারে কাছে বুঝি বাওযালি-ফকির
নেই ! তার কাছে থেকে বুঝি কিছু মন্ত্র পড়ে নেওযা য়ায না ? ""
"" তা বেশ । ""
"" বেশ কি ? ঐ একতারা নিযেই মজে থাকো । তাতেই পেট
পুরবে । ""</p>

<p>বাঘ সম্পর্কে ফজলের য়তটা না ভীতি থাক , বনে ওঠা নিযে
অনিচ্ছাই ছিল বড় । তাহলেও সে না গিযে পারে না ।
তোড়জোড় চলে । গুইসাপ মারতে য়াবে । গুইসাপের চামড়ার
বেশ চড়া দাম । শৌখিন জিনিস তৈরী হযে এতে । তারই
সুয়োগে আবাদের লোকে অবাধে গুইসাপ মারতে শুরু করে ।
সুন্দরবনে আছেও অজস্র । কিন্তু মারতে মারতে এমন অবস্থা য়ে
বনে সাপের উপদ্রব হযে ওঠে ভীষণ । গুইসাপ সাপ-ভক্ষক । এদের
দাপটে বিষাক্ত সাপেরাও সংয়ত থাকে । অবশেষে গুইসাপ মারা সরকারি
ভাবে বে-আইনি ঘোষিত হলো ।</p>

<p>তারপর থেকে এই ব্য়বসা চলেছে তলে তলে । তাতে বিশেষ
অসুবিধা হযনি আবাদের মানুষের । ধরা পড়বার উপক্রম হলেই
অতি সহজে এরা গা ঢাকা দেয বনের অগুনতি নদী , নালা ও খালের
পথে ।</p>

<p>তোড়জোড়ের তেমন বিশেষ কিছু নেই । তিনজন লোক চাই ।
তিনজন য়ে হতেই হবে , এমন নয । তবে তিনজন না হলে কোনও
কাজে এরা বাদায সহসা ওঠে না । তিনজন হলে তবে য়েন একটা
ছোটখাটো দল হয । ফজলের সাথী হলো , দুর্লভ ও মাধো । দুর্লভের
কিন্তু মাত্র একটা চোখ । তাহলেও তীক্ষ্ণ দৃষ্টিতে সে কারও চেযে
কম নয । //
ফটিকের মনের ভাব বুঝে নিযে বক্স গাজি কোনও মতামতের
অপেক্ষা না রেখে বললেন , "" চল ফটিক , আমিও য়াচ্ছি । বাঘ য়া
পড়বে তা'তো জানি । বন্ধুকটা তো তাড়াতাড়ি ধুযে মুছে রাখতে হবে ।
সারারাত পানি ও নোনায পড়ে আছে । ""
"" তা য়া বলেছ , ঠিকই । চলো য়াই । ""
খাল পেরিযে ডিঙি চরে তুলে দু'জনে মিলে বনে উঠলো । থমথমে
বন । বাঘের আনাগোনা-পথে কোন জীবজন্তুরই পাত্তা পাওযা
য়ায না । এক কুমির আর শুযোর ছাড়া । সুন্দরবনের কুমিরকে
উভচর বলা চলে । চরে উঠে রোদ পোহানো এদের বাতিক । তাছাড়া
মাছের লোভে সামান্য় জলা জাযগার আশে পাশে ঘুরে বেড়ায ।
দেখা সাক্ষাত্ প্রাযই হলেও , বাঘ কিন্তু এদের বিশেষ ঘাঁটাতে য়ায
না । বাঘেরও তো প্রাণের ভয আছে । বনে প্রায সম-শক্তিশালী
জীবেরা অয়থা রেষারেষি করতে য়ায না । আর শুযোর তো একগুঁযে
ও নির্বোধ । হযতো ঠিক নির্বোধ নয , লুকোচুরি করে বেঁচে থাকার
স্পৃহা এদের কম । তাহলেও এ-য়াত্রা শ্঵শুর ও জামাই-এর সঙ্গে কোন
কুমি বা শুযোরের সঙ্গে দেখা হয না ।</p>

<p>ওরা সোজাসুজি বন্ধুকের ধারে এলো । বাঘ বা কোন জীবই
ধারে কাছে পড়ে নেই । ঠিকই চোট হযেছে । ফটিকের বউ মিথ্য়া
বলেনি । বন্ধুকটা ছিটকে কাত হযে পড়ে আছে ।</p>

<p>অন্য়দিকে দৃষ্টি না দিযে ফটিক তাড়াতাড়ি বন্ধুকটা হাতে
নিল । নতুন টোটা পুরে সাবধানির মতো নলটী ঝুলিযে দিল মাটির
দিকে ।</p>

<p>ফটিক বন্ধুক তদারকিতেই ব্য়স্ত ; কল পেতেছিল , কলের ফাঁদে
বাঘ পড়েনি । সুন্দরবনের বাঘ এক গুলিতে বহুসময ঘাযেল হয না ।
একবারে ঘাযেল না হলে , দ্঵িতীযবার তখন তখনই তাকে পাওযা
দুরূহ । কিন্তু সুন্দরবন সর্বত্র এক হলেও , সুন্দরবনের বাঘ সব
এক নয । রাযমঙ্গলের বাঘের কথা ফটিকের অজানা ।
ফটিকের অজানা থাকতে পারে , কিন্তু বক্স গাজি বাঘ শিকারী না
হলেও , রাযমঙ্গলের উপকূলবাসী । বাঘ না দেখে উদ্঵েগে চঞ্চল হযে
উঠেছে । সরে পড়তে চায । এবং তা য়ত শীঘ্র সম্ভব । বাঘ য়ে
এসেছিল তা তার পদচিহ্নে স্পষ্ট । বক্স নাক বড় করে গন্ধে হদিশ
পাবার দু'একবার চেষ্টা করলো বৃথাই ।</p>

<p>এমন অবস্থায বনে কথা বলার উপায থাকে না । মানাও আছে ।
বক্স আকার ইঙ্গিতে ফটিককে কাছে এনে সরে পড়বার জন্য় প্রায
হাত ধরে টেনে নিযে চললো । কোন দিকে বা কোন পথে য়াবে তাও
বিশেষ চিন্তা না করে এগিযে চললো । চিন্তার অবকাশই বা কোথায !
বেশ খানিকটা এগিযে এসেছে । সহসা দু'জনেই শিহরিত হযে
ওঠে । বাঘের পথ ধরেই ওরা এগিযে চলেছে । দুই জোড়া চোখ বাঘের
পদচিহ্ন ও রক্তচিহ্নের উপর । দাঁড়িযে পড়লো ওরা । এগুবে না পিছুবে ?
এগুলে বাঘের মুখোমুখি পড়বে । গুলিবিদ্ধ হিংস্রতম জীব এমন
স্পর্ধার প্রতিশোধ নিতে একটু দ্঵িধাগ্রস্ত হবে না । পিছু হটলেও
রক্ষা নেই । আর য়াই হোক , পলাতকের বাঘের হাত থেকে নিস্তার
নেই । এমন দোটানায পথ থেকে কেটে বেরুবার পন্থা সুন্দরবনের দক্ষ
শিকারীরা জানে । কিন্তু ফটিক সে দক্ষতা আজও অর্জন করেনি । না
করলেও বড়াই করতে সে ছাড়বে কেন ? কিন্তু এখন বড়াই তো মুখে
নয , কাজে দেখাতে হবে । শ্঵শুরের সামনে জামাই হযে কে-ই বা
পরাজয মানতে চায ; ফটিক এগিযে চললো রক্ত-রঞ্জিত পথ ধরে । বনে
বন্ধুক য়ার হাতে সেই নেতা । স্঵াভাবিকভাবেই বক্স গাজি ফটিকের অনুগামী
হলো ।</p>

<p>কিছুদূর য়েতে দেখা গেল বাঘের খোঁচ ঠিকই আছে , রক্তচিহ্ন
আর নেই । কিসের ইঙ্গিত , আর কী-ই বা এর মানে - তা ভাববার
অবস্থা এদের কি আর আছে ? সামনেই বড় খালের ফাঁকা আলো
- তা'তেই দু'জনেই মশগুল । আর কিছু না হোক , সরে পড়বার
অবকাশ হযতো মিলবে । //
এই সময খুলনা থেকে আবাদে য়াবার নৌকারও অভাব হয
না । আবাদে য়াবার নদী-পথগুলি শীতের মরশুমে য়েন নৌকাতে
নৌকাতে ধূলপরিমাণ হযে ওঠে । ফসলের সঙ্গে বুঝি পাখির দল
ঝাঁকে ঝাঁকে আসতে তাকে । এতো ফসল য়ে আবাদের লোকেরা তা
কেটে ঘরে তুলতে অপারগ । খুলনা , য়শোহর , ফরিদপুর , বরিশাল -
চারদিক থেকে দলে দলে ডিঙি-ডোঙায করে এসে ধান কাটার কাজে
য়োগ দেয । য়ে য়ে-ভাবেই আসুক , এক মাস গতর খাটালে ধানের
আঁটিতে তাদের ডিঙি বোঝাই করে দিতে আবাদের লোক কার্পণ্য়
করে না । কাজেই মাঝি-মাল্লা সবাই এ-সময আবাদের নামে বলতে
গেলে ডিঙির বাঁধন খুলেই থাকে ।</p>

<p>ভৈরব , রূপসা , পশর ও চুনকুড়ির পথ ধরে ওরা দাকোপ এলো ।
সেদিন দাকোপের হাট । আবশ্য়ক মতো কাঁচা সওদা কেনাকাটি করে
নেয ।</p>

<p>কম পক্ষে পনেরোদিন তো গুনোরিতে থাকতে হবে । দক্ষিণে
আরও ভাটিতে হাট নেই বললেই হয । দাকোপেই সে-রাতের মতো
বিশ্রাম করার কথা । কিন্তু শুক্লপক্ষের ষষ্ঠী , ভাটি শুরু হবে শেষ
রাতে । বিশ্রামের বদলে ভাটির অপেক্ষায ব্য়াগ্র মনে সময কাটাতে
হলো । এই ভাটি হাতছাড়া হলে , পরের ভাটিতে গুনোরি পৌঁছাতে
সন্ধ্য়া পেরিযে য়াবে । সন্ধ্য়ায বনের কাছে কেউই আনাগোনা
করতে চায না , বিদেশীরা তো কেউ সাহসই করে না ।
গুনোরি এসে গুছিযে-গাছিযে সড়গড় করে নিতে চার-পাঁচ দিন
কেটে গেলো । প্রতুল তো ধান কাটার কাজে ব্য়তিব্য়স্ত হযে ওঠে ।
ক'দিনের মধ্য়ে ধান খলেনে উঠে এলো প্রায । এর পর মাড়াইযের
কাজ । মাড়াইযের আগে অবশ্য় ধানের আঁটি রোদ খাইযে নিতে
হবে । ক'দিন ধরে কুযাশা পড়ছে । ফলে রোদ খাওযাতেও কিছুদিন
বেশি সময লাগবে । এবারই অবসর । মহেন্দ্র উশখুশ করতে থাকে ।
কথাও পাড়ে । বনে শিকারের কথা । নবীন মোল্লার সাথে কথা
পাড়লে সে বললো , "" কি শিকার ? ""
মহেন্দ্র থতমত খেযে বললো , "" হরিণ শিকার । ""
এই ক'দিনেই য়া দু'একটা বাঘের গল্প শুনেছে তাতে বাঘের কথা
মুখে আনা য়ে বেযাদবি , তা মহেন্দ্র বুঝে ফেলেছে ।
নবীন মোল্লা ঠাট্টার সুর নিযে বললো , "" তা হরিণ খুঁজতে খুঁজতে
য়দি বড় মিঞার সঙ্গে মোলাকাত হয , সে আপনার ভাগ্য়ি । ""
এ-কথা বলে আবার বললো , "" তাই বলে বাঘের কথা কিন্তু মুখে
আনতে য়াবেন না । ""</p>

<p>নবীন মোল্লার এসব কথা বলার খানিকটা অধিকার ছিল । নিজে
পুরো বাওযালি না হলেও অনেকবার বাওযালির সঙ্গে বাঘ সরিযে
দিতে বনে ঘুরেছে ; হরিণ শিকারও দু'চারটে করেছে ।
ঠিকঠাক হলো বনের ভিতরে গিযে শিকার করতে হবে ।
তবে ডাঙায নয , ডিঙি করে বড় নদীর চরে চরে । উত্তরের বাবু-
শিকারীকে নিযে বিপদে পড়তে চায না নবীন মোল্লা । কনকনে
শীতের পর মিষ্টি রোদ নদীর চরে উপচে পড়তেই দলে দলে
হরিণ আসে রোদ পোহাতে ।</p>

<p>এ-পন্থায সুন্দরবনের হরিণের দেখা পাওযা সহজ হলেও শিকার
করা সহজ নয । বড় নদীর মাঝ দরিযা থেকে বেশ দেখা য়াবে , ছোট
বড় হরিণের দল চপল পাযে ঘুরে ফিরে য়েন চরের ওপর মাছের মতো
সাঁতরে বেড়ায । বন্ধুকের পাল্লার বাইরে থাকলে ওরা নিশ্চিন্ত ও
নির্বিকার । কিন্তু ডিঙি একটু এগিযে বন্ধুকের পাল্লার মধ্য়ে আনতে
গেলেই চকিতে বনের আশ্রিত জীব বনের মাঝে কোথায য়েন উবে
য়াবে ।</p>

<p>সেখের টেক । দক্ষিণে কালীখাল । পুবে পশর , পশ্চিমে শিবসা
এবং উত্তরে সেখের খাল । চার বর্গ মাইল পরিমাণ স্থান । সুন্দরবন
দুর্গম । কিন্তু এই চত্঵রটুকুর দুর্গমতার তুলনা নেই । ঐতিহাসিকদের
অনুমান , এইখানেই রাজা প্রতাপাদিত্য় তাঁর শিবসা দুর্গ ও কালিকা
মন্দির প্রতিষ্ঠা করেছিলেন । তার চিহ্ন আজও বর্তমান । কোন কোন
মন্দির তো আজও বনের আড়ালে মাথা উঁচু করে দাঁড়িযে আছে ।
 +&gt;*",0
</p></body></text></cesDoc>