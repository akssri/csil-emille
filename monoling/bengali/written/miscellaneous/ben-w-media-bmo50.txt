<cesDoc id="ben-w-media-bmo50" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-media-bmo50.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>বঙ্গীযবি</h.title>
<h.author>জ্ঞানওবি</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1987</pubDate>
</imprint>
<idno type="CIIL code">bmo50</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0565.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-22</date></creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;আমাদের কথা ।
ঋতু পরিক্রমার পথ বেযে প্রতি বছর শরত্ আসে আমাদের সামনে বর্ষার বিদায লগনে ।
বর্ষপঞ্জীতে ভাদ্র-আশ্঵িন শরত্ ঋতু হলেও শরতের নিজস্঵ রূপ ফুটে ওঠে আশ্঵িন ও
কার্তিকে । অবশ্য় অনেক অভিধানে আশ্঵িন ও কার্তিক মাসই শরত্কাল বলে অভিহিত
হযেছে ।</p>

<p>শরত্ সৌন্দর্য়্য়ের ঋতু । শরত্ প্রবন্ধে রবীন্দ্রনাথ লিখেছেন - "" শরতের রঙটি প্রাণের রঙ ।
অর্থাত্ তাহা কাঁচা , বড়ো নরম । রৌদ্রটি কাঁচা সোনা , সবুজটি কচি , নীলটি তাজা । এজন্য়
শরতে নাড়া দেয আমাদের প্রাণকে , য়েমন বর্ষায নাড়া দেয আমাদের ভিতর মহলের
হৃদযকে , য়েমন বসন্তে নাড়া দেয আমাদের বাহির মহলের য়ৌবনকে । "" শরতের বাংলাই
য়েন জীবনানন্দের রূপসী বাংলা । মাঠে মাঠে সবুজের সমারোহ ; কদম্ব , কেতকী , শিউলির
ডালে ফুলের মেলা ; পূর্ণ সরোবরে শালুক-পদ্মের ফাঁকে ফাঁকে ডাহুক-ডাহুকী ,
চাকর-চকরীর কল-কাকলী সব মিলে প্রাচুর্য়্য়ের অনন্ত সম্ভবনা দিকে দিকে নবীনেরে জানায
আহ্঵ান । জননীর এ আহ্঵ান লিপি আমাদেরও মনে দেয দোলা । সুদিনের উদ্দীপনায ,
আগামী দিনের প্রাপ্তির উন্মাদনায পল্লীতে পল্লীতে চলে উত্সবের আযোজন , বেজে উঠে
বোধনের বাজনা । কদাচিত্ প্রকৃতির রুদ্ররোষে বাজনায পড়ে ছেদ , উত্সবে পড়ে ভাটা , কিন্তু
বঙ্গ ললনার অন্তর কাঁদে উমারূপী হৈমন্তী ফসলের আশায ।</p>

<p>কযেক দশক আগেও এ বোধনের বাজনা বাজত জমিদারের নাটমন্দিরে না হয ধনীর
বৃহদাযতন চত্঵রে । বিজ্ঞানের অগ্রগতির সাথে সাথে আর্থ-সমাজ ব্য়বস্থার পরিবর্তনে দেবী
আজ ধনীর প্রাসাদে বন্দী নন , তিনি আজ সার্বজনীন । রুচি ও পদ্ধতির পরিবর্তনের সঙ্গে
জ্ঞানের বিস্তৃতিতে ঘটে চলেছে নব নব উত্তরণ । মহিষমর্দিনীর স্তবগান আজ নগন্য় মাত্র ।
পোষাকে , মাইকে , আলোর জৌলসে শারদীয উত্সবের নব রূপাযণের সঙ্গে য়ুক্ত হযেছে
বাঙালীর এক সাংস্কৃতিক দিক । উত্সবকে উপলক্ষ্য় করে জন্ম নিযেছে শারদ-সাহিত্য় ।
বহু পত্র-পত্রিকা , পুস্তিকায জমে উঠে শারদীয সাহিত্য়ের বাজার । জ্ঞান ও বিজ্ঞান পুরোপুরি
বিজ্ঞান পত্রিকা , তথাকথিত লোকরঞ্জক পত্রিকা নয । আচার্য়্য় সত্য়েন্দ্রনাথ প্রতিষ্ঠিত বঙ্গীয
পরিষদ দীর্ঘ চল্লিশ বছর ধরে এ পত্রিকার মধ্য় দিযে কণ্টাকাকীর্ণ পথে প্রযাস চালাচ্ছে বিজ্ঞান
মানসিকতার উজ্জীবনে , জ্ঞানের সম্পূরণে , সত্য়ের উদ্ঘাটনে , পরিবেশ সংরক্ষণে , কুসংস্কার
মুক্ত আদর্শ সমাজ গঠনে । তাই আমাদের বিশ্঵াস জ্ঞান ও বিজ্ঞানের শারদীয সংখ্য়া আমার-
আপনার সবার শারদীয উত্সবের নানা উপাচারের মাঝে নিশ্চযই ঠাঁই পাবে আনন্দের সঙ্গে
নব সৃষ্টির আহ্঵ান জানাতে ।</p>

<p>সন্ধানী অন্য় পাঁচজন । সাধন দাশগুপ্ত ।
[ 1968 খৃষ্টাব্দের জুন মাসে ত্রিযেস্তে তাত্ত্঵িক
পদার্থবিদ্য়ার আন্তর্জাতিক কেন্দ্রে একটি সিমপো~
সিযাম হয - বড় বড় বিজ্ঞানীরা আবদস সালামের
আহ্঵ানে বক্তৃতা দিতে আমন্ত্রিত হযে আসেন ।
সেখানে , একটি ঘরোযা আলাপে , হানস বেথ একটি
প্রসঙ্গ তোলেন । আধুনিক বিজ্ঞান য়াঁদের হাতে
সৃষ্টি - তাঁরা দল বেঁধে জন্মেছেন । আর এদের
মধ্য়ে 1879 আর 1990 খৃষ্টাব্দটি চিহ্নিত ।
1878 থেকে 1880 খৃষ্টাব্দে জন্মান লীসে
মীটনার , আলবার্ট আইনস্টাইন , ফন লাউএ ,
অটোহান আর পল এরনফেস্ট - এই পাঁচজন ।
পাঁচজন জার্মান ভাষাভাষী বিজ্ঞানী । আর কিছু
পরে পাওযা য়ায পাঁচজন মাস্টারমশাযকে - ইংলণ্ডে
রাদার ফোর্ড , লেইডনে এরনফেস্ট , কোপেনহাগেনে
নীযেল বোর , মিউনিকে সমারফেল্ড আর গোটেনগেনে
মাক্মবোর্ন । এঁরাও পাঁচজন । এই পাঁচজন
মাস্টারমশায য়ে ছাত্রদের পেলেন তাঁদের মধ্য়ে
আছে অন্য় পাঁচজন - এঁরা উলফগাঙ্গ পাউলি ,
এনরিকে ফের্কি , ওযের্নের হাইসেনবার্গ , পল ডিরাক
এবং পল ইউজিন হিবগনার - এঁদের জন্ম 1990
থেকে 1902 খৃঃ । দুজন জার্মান ভাষাভাষী , একজন
ইংরেজ , একজন ইতালীয আর পঞ্চমজন হাঙ্গেরির ।
উত্সুক কৌতুহলী সমাগত বিজ্ঞানীরা ত্রিযেস্তে বললেন ,
এঁরা শুধু বিজ্ঞানে অনুসন্ধিত্সু বা অন্঵িষ্টু নন , এঁরা
সত্য়ের পথয়াত্রী । অমৃতের সন্ধানী । পঞ্চকন্য়ার
মতো এঁরা ভুলকে স্঵ীকার করে উত্তরণ ঘটিযে
বিজ্ঞানকে প্রতিষ্ঠা করে গেছেন । ত্রিযেস্তে সেদিন
এই অন্য়ান্য় পাঁচজনের মধ্য়ে তিনজন হাজির ছিলেন
- ডিরাক , হাইসেনবার্গ এবং হিবগনার । পাউলি
এবং ফের্মি পঞ্চাশের দশকে মারা গেছেন । এঁদের
কাছ থেকে সালাম শুনতে চাইলেন তাঁদের কাজ
ও চিন্তার নিজস্঵ বিশ্লেষণ । এঁরা বিশ্লেষণে নামলেন ।
পরে নানা প্রবন্ধে সেদিনের বক্তব্য়টি আরো
বিশদ করে বলে গেলেন । অমৃতের সন্ধানী
আসরে আগত ও অনাগত সেই অন্য় পাঁচজন স্পষ্ট
হযে প্রকাশ পায ।</p>

<p>এঁদের পরিচয জানাতে গিযে আবদস সালাম
বললেন , - "" 1748 খৃষ্টাব্দে পারস্য়ের শাহানশাহ্
নাদির শাহ ভারত আক্রমণ করে সৈন্য়সামন্ত সমেত
দিল্লী পৌঁছলেন । ভারতের মুগলশাহীর পরাজয
ঘটলো তাঁর হাতে । দিল্লীর আত্মসমর্পণ হলো
আর দুই রাজা শান্তিচুক্তি করতে বসলেন । চুক্তির
শেষে মযূর সিংহাসন হাতছাড়া হযে চলে গেল
পারস্য়ে - তখন পরাজিত ভারত সম্রাটের উজির ই
আজম - অসিফজাকে ডেকে পাঠালেন মোগল
বাদশাহ - উদ্দেশ্য় উজির স্঵যং শান্তির আর্বিভাব
দুই সম্রাটকে দ্রাক্ষারস পরিবেশন করুন ।
প্রোটোকল নিযে এদিকে উজির মহাসমস্য়ায
পড়লেন - কাকে তিনি প্রথম পেযালাটি দেবেন ?
য়দি নিজের মালিক মুগল বাদশাহকে প্রথম পেযালা
দেন , তবে ক্রদ্ধ অপমানিত পারস্য় সম্রাট বিনা বাক্য়
ব্য়যে তলোযার টেনে তার মাথাটি কেটে নিচে নামিযে
দেবেন । আর য়দি পারস্য় সম্রাটকে প্রথম
পেযালা দেন , তবে তাঁর মালিক অসন্তুষ্ট হবেন -
য়ার ফল দেখা দেবে পরে । এক লহমা চিন্তা
করে উজির ই আজম সোনার থালায দুটি সুরাপাত্র
এনে মোগল বাদশাহকে বললেন , খোদবন্দ , এই
পরিবেশন আমার হাতে শোভা পায না । সম্রাটই
শুধু সম্রাটকে পরিবেশন করতে পারেন । ""
গল্পটি বলে সালাম পল ডিরাককে অনুরোধ করলেন
হাইসেনবার্গকে পরিচিত করতে । - সেই পরিচিতির
পথ ধরে এই অন্য় পাঁচজনকে চেনার আসর বসে । ]</p>

<p>ওযেরনের হাইসেনবার্গ : অনিশ্চিতির দর্শক
ত্রিযেস্তের তাত্ত্঵িক পদার্থবিদ্য়ার আন্তর্জাতিক
কেন্দ্রের 1968 খৃষ্টাব্দের সেমিনারে প্রফেসার
আবদস সালামের অনুরোধ বিশ্঵বরেণ্য় বিজ্ঞানী
পল ডিরাক আরেকজন বিশ্঵বরণ্য় বিজ্ঞানী ওযের্নের
হাইসেনবার্গ সম্পর্কে সামান্য় কটি কথার মালা
গেঁথে তাঁকে বরণ করে নিলেন । ডিরাক বললেন ,
"" আমি য়ে ওযেরনের হাইসেনবার্গের একজন ভক্ত
তার অনেক কারণ । এক সঙ্গে , প্রায এক বযসে
আমরা দুজনে রিসার্চ স্টুডেণ্ট ছিলাম ; আর আমি
য়েখানে বিফল , সেখানে হাইসেনবার্গ সফল ।
বর্ণালী বিশ্লেষণের অনেক ডেটা-তথ্য় সেই সমযে
সংগৃহীত হযেছিল ; এদের ব্য়বহার করার কাজের
পথটি হাইসেনবার্গই একমাত্র খুঁজে পেলেন ; সেই
পথেই ফিজিক্মের স্঵র্ণয়ুগের সূচনা হলো , আর
শুরু হলো , তাঁরই হাতে । তারপর বেশ কযেক
বছর ধরে দ্঵িতীয শ্রেণীর ছাত্রদের পক্ষেও প্রথম
শ্রেণীর কাজ করার সম্ভবনা দেখা দিল । পরে তাঁর
সঙ্গে ভ্রমণয়াত্রী হবার সৌভাগ্য়ও আমার হলো । //
নিযন্ত্রণ হল তথ্য় সংগ্রহ , সংগৃহীত তথ্য় দিযে
কাজ করা , তথ্য়ের রূপান্তর ও অন্য়ের সঙ্গে
য়োগায়োগ প্রভৃতি ক্রিযার নামান্তর । জড় ও জীব
এসব কাজে ক্রিযাশীল হলে , তা হল সাইবোরনেটিক
য়ন্ত্র ।</p>

<p>এরকম য়ন্ত্র য়খন ক্রিযাশীল নয , তখন তার
এনট্রপি সবচেযে বেশী । য়ন্ত্র চললে সে তথ্য় সংগ্রহ
করে , অনিশ্চযতার অবসান ঘটায , বৈচিত্রের বিশৃঙ্খলা
হ্রাস পায ; য়ন্ত্রের ফলাফল তখন ভবিষ্য়দ্঵াণী করা
য়ায । তাই তখন এনট্রপিও কমে য়ায । বৈচিত্র
হ্রাস হল নিযন্ত্রণের একটি বড় উপাদান ।
সাইবারনেটিক্সে পছন্দসই এনট্রপি বলে একটি কথা
আছে । পছন্দসই এনট্রপি সাইবারনেটিক য়ন্ত্র
আমরা তাকেই বলি য়ার সবচেযে কম তথ্য়ের
সাহায়্য়ে একই উদ্দেশ্য়সাধনে বেশী কাজ করার
ক্ষমতা আছে । তাই সাইবারনেটিক্ম ও এনট্রপির
সম্পর্ক সুস্পষ্ট হযে পড়ে ।</p>

<p>এনট্রপি ও জীবজগত্
হ্঵িনার প্রথম এই ধারণা দিযেছিলেন য়ে
এনজাইমের গঠন বিন্য়াসে এনট্রপি হ্রাস পায ।
ব্রিলো য়াঁর মতে এনট্রপি ও জীবন এমন দুটি
উদাহরণ য়েখানে ঘড়ির সমযের কাঁটা উল্টে দেওযা
য়ায না । এখানেই দুটি সমস্য়ার কিছু মিল আছে ।
উদ্ভিদ , প্রাণী , মানুষ এমন রাসাযনিক কাঠামোতে
তৈরী য়ার সাম্য় খুব অস্থাযী । জড় পদার্থ থেকে
গঠনবিন্য়াসে এদের পার্থক্য় মৌলিক ও এসব
কাঠামোতে এনট্রপি কম । তাপগতি তত্ত্঵ের দ্঵িতীয
নিযম য়েন মৃত্য়ুর পরোযানা । মৃত্য়ুর পর সেই
নিযম জীবনের পক্ষেও অমোঘ সত্য় । অজৈব
পদার্থের তো আগেই মৃত্য়ু ঘটেছে । জীবন মৃত্য়ুর
পরোযানাকে ঠিক সেই সমযটুকুর জন্য় বিলম্বিত
করে রাখে , য়তক্ষণ তার মৃত্য়ু না ঘটছে ।</p>

<p>` পদার্থ বিজ্ঞানের চোখে জীবন ' বইতে স্রোডিংগার ,
থিওরেটিক্য়াল বাযোলজি বইতে বাওযের এনট্রপি
সমস্য়ার নানা দিক আলোচনা করেছেন । আধুনিক
কালে প্রিগজিন বলেছেন জীবন এমন এক মুক্ত
কাঠামো ( 	open system ) য়ার এনট্রপির মাত্রা
নির্দিষ্ট । জড় পদার্থের কঠামো বন্ধ ( closed ) ,
তাই তার এনট্রপি বেড়ে চলে ।</p>

<p>প্রকৃতির য়ে কোন পরিবেশে জড় ও জীবন
উভযেরই এনট্রপি বাড়ে । জীবনের এই এনট্রপি বৃদ্ধি
মৃত্য়ুতে শেষ সীমায পৌঁছয । জীবন তার
জীবত্কালে নিজস্঵ বিপাকক্রিযায তার পজিটিভ
এনট্রপি মুক্ত করে দিযে পরিবেশের নেগেটিভ
এনট্রপি সংগ্রহ করে নেয । তাই জীবজগতে
এনট্রপি হ্রাস পায ।</p>

<p>প্রাণীরা প্রাণিজ বা উদ্ভিজ্জ য়ে সব খাদ্য় খায ,
তাদের জৈব-অণুর শৃঙ্খলা থেকে তারা নেগেটিভ
এনট্রপি গ্রহণ করে । আবার ভুক্তাবশেষ য়া উদ্ভিদ
জগতকে ফিরিযে দেয তাতে শৃঙ্খলার অভাব থাকে
অর্থাত্ তা পজিটিভ এনট্রপি । উদ্ভিদ জগত্
সূর্য়্য়ালোক থেকে নেগেটিভ এনট্রপি আহরণ করে
তাদের ক্লোরোফিলে ঐ পজিটিভ এনট্রপি নেগেটিভ
করে নেয । সালোকসংশ্লেষ ক্রিযার এই পদ্ধতিই
পৃথিবীতে এনট্রপি হ্রাসের একমাত্র উদাহরণ । মুক্ত
সূর্য়্য়ালোকের প্রভাবে এই অঘটন ঘটে ।</p>

<p>ভজীবনের এই প্রক্রিযা তাপগতিতত্ত্঵ে দ্঵িতীয
নিযমের বিপরীতে এক নূতন বিজ্ঞানের ইঙ্গিত দেয ।
সেই বিজ্ঞানের নিযম কী হবে তার খোঁজে পদার্থবিদ
ও জীববিজ্ঞানী এখন একই মঞ্চে এসে দাঁড়িযেছেন ।
তৈরি হযেছে বিজ্ঞানের নতুন সব শাখা - জীব
পদার্থবিদ্য়া , জীবরসাযন বা জীবপ্রয়ুক্তি । এনট্রপি
হ্রাসবৃদ্ধির সমস্য়ায জড় ও জীবনের পার্থক্য়ের
ব্য়াখ্য়ায বিজ্ঞানে এক মহত্ আবিষ্কারের সম্ভাবনা
আছে । আপাতত এক টুকরো রুটি বা একটুকরো
মাংসের খাদ্য় হিসেবে য়োগ্য়তা য়াচাই করতে শুধু
শক্তির কথা ভাবলে চলবে না , সেই সঙ্গে নেগেটিভ
এনট্রপির কথাও ভাবতে হবে ।</p>

<p>এনট্রপির হাত এড়িযে শক্তির রূপান্তর কি
কখনও সম্ভব ? সালোকসংশ্লেষ ক্রিযার বিকল্প
কোন পদ্ধতিতে পৃথিবীতে শক্তি সমস্য়া কি কখনও
সমাধান হতে পারে ? অথবা পৃথিবীতে বা
মহাবিশ্঵ে এমন কোন অজানা উত্স কি আছে
য়েখানে নেগেটিভ এনট্রপি নিযে শক্তির রূপান্তর
ঘটছে ? আগামীকালের বিজ্ঞানে হযত এর উত্তর
খুঁজে পাওযা য়াবে ।</p>

<p>য্য়ালার্জেনিক ও বিষাক্ত উদ্ভিদ । তারকমোহন দাস ।
[ আমাদের পরিবেশের মধ্য়ে বেশ কিছু
বিষাক্ত গাছপালা আছে , - য়েমন ধুতরা ,
কলকে , খেসারি , বিচুটি , ব্য়াঙের ছাতা ,
পার্থেনিযাম , ভেলফিনিযাম , ল্য়ানটানা
ক্য়ামেরা ইত্য়াদি ; সেগুলি পরিবেশের মধ্য়ে
কতটা দূষণ সৃষ্টি করছে , বা আদৌ কোন
দূষণ সৃষ্টি করছে কিনা , এ বিষযে সাধারণের
মধ্য়ে তো বটেই , এমন কি বিজ্ঞানীদের
মধ্য়েও য়থেষ্ট বিভ্রান্তি আছে । এ প্রবন্ধে
বিভ্রান্তির কিছুটা অবসান ঘটবে । ]</p>

<p>কিছুদিন আগে শিকাগোতে বাযুদূষণ ওযার্কসপে
এবং লখনৌ-এর ইণ়্ডাস্ট্রিযাল টক্মিকোলজি রিসার্চ
সেণ্টারে আন্তর্জাতিক কনফারেন্সে য়োগদান করে
এ সম্পর্কে কিছু আলোচনা শুনেছিলাম , তা বিশেষ
ভাবে প্রণিধানয়োগ্য় ।</p>

<p>প্রথমতঃ আমরা অনেক সমযই প্রকৃত তথ্য়
না সংগ্রহ করে বা য়থেষ্ট প্রমাণ হাতে না পেযেই
কোন একটি গাছের বিরুদ্ধে ব্য়াপকভাবে
বিষোদ্গার করতে শুরু করে দিই ,
তারপর য়খন দেখি ঐ গাছের বিরুদ্ধে য়ে
সব অভিয়োগ খাড়া করা হযেছিল , অর্থাত্ য়ে
ব্য়াপত ক্ষযক্ষতির চিত্র আঁকা হযেছিল , - তা
বাস্তবের সঙ্গে মিলছে না তখন আমরা নিজে থেকেই
চুপ করে য়াই । অনেক সময দেখা য়ায কোন
একটি গাছের বিরুদ্ধে একদল বিজ্ঞানী খুব হৈ-চৈ
করতে শুরু করে দিযেছেন , তাকে সবংশে ধ্঵ংস
করবার জন্য় পরামর্শ দিচ্ছেন । দেশের সরকারও
এগিযে এসেছে ঐ কাজে সাহায়্য় করবার জন্য় ।
আবার তাকে বাঁচাবার জন্য় আর একদল বিজ্ঞানী
সকলকে বোঝাবার চেষ্টা করছেন । অস্ট্রেলিযায
ম্য়াক্রোজেমিযা মুরী নামক নগ্নজীবী উদ্ভিদের
বেলায এটা ঘটেছিল । এই ধরণের ঘটনায সব
থেকে বেশী বিভ্রান্ত হয জনসাধারণ , তাঁরা
বিজ্ঞানীদের ওপর আস্থা হারান , পরে সত্য়ই য়খন
কোন সঙ্কটের সূচনা হয তখন তাঁদের কাছ থেকে
আর কোন সহয়োগিতা পাওযা য়ায না ।</p>

<p>দ্঵িতীযতঃ এটাও সত্য় য়ে নিছক প্রচারের
অভাবে , প্রকৃত তথ্য় না জানার জন্য় এক বিরাট
সংখ্য়ক মানুষ সব দেশে , সব সমযই ক্ষতিগ্রস্থ হযে
আসছেন এই বিষাক্ত উদ্ভিদগুলির জন্য় । উপয়ুক্ত
সাবধানতা অবলম্বন না করে এই উদ্ভিদগুলি
ব্য়বহারের ফলে এই ক্ষতি ঘটছে । পৃথিবীতে খুব
কম করেও প্রতি বছর 15,000 লোক মারা
য়ায এই অজ্ঞতার জন্য় এবং অসুস্থ হয দশ লক্ষেরও
বেশী ।</p>

<p>তৃতীযতঃ পৃথিবীতে বেশ কিছু মানুষ আছেন
য়াঁরা সব জেনেশুনেই গাছ-গাছড়া থেকে নিষ্কাশিত
বিষাক্ত মাদক দ্রব্য় নিযমিত সেবন করে থাকেন ।
তারা জেনেশুনেই সেই বিষ পান করে য়াচ্ছেন , আর
তাদের সংখ্য়া ক্রমশই বাড়ছে ।</p>

<p>এই তিন ধরণের বিপত্তি সম্পর্কে আরো কিছু
বিস্তারিত ব্য়াখ্য়ার প্রযোজন । ম্য়াক্রোজেমিযা
মুরী নগ্নবীজী শ্রেণীর একটি বিরল বৃক্ষ , অস্ট্রেলিযাতে
পাওযা য়ায । দুর্ভাগ্য়ের বিষয এদের দেহে
বিচিত্র বিপাক ক্রিযার ফলে এক রকম বিষ
সঞ্চিত হয এবং ঐ গাছের পাতা অধিক পরিমাণে
খেলে বা খাওযালে গবাদিপশুর একরকম পক্ষাঘাত
রোগ হয । অস্ট্রেলিযায পশুপালকদের বিরাট
ক্ষমতা , তাঁরা সরকারকে বুঝিযে সরকারী কর্মীদের
সাহায়্য়েই এই বিরল প্রজাতির বৃক্ষটিকে আর্সেনিক
বিষ দিযে সবংশে ধ্঵ংস করবার ব্য়বস্থা পাকা
করে ফেলেছিলেন । সৌভাগ্য়ের বিষয দেশের
ভেতর ও বাইরে থেকে এই নীতির কঠোর সমালোচনা
হতে থাকায এই প্রচেষ্টা শেষ পর্য়্য়ন্ত বন্ধ হয ।
য়াঁরা এই বিষে বিষক্ষয নীতির কঠোর সমালোচনা
করেছিলেন তাঁদের মধ্য়ে আমেরিকার বিখ্য়াত
উদ্ভিদতত্ত্঵বিদ জে. জে চেম্বারলেন ছিলেন । তিনি
বাইবেলের নোযার প্রজাতি সংরক্ষণতত্ত্঵ অনুসরণ
করে এই গাছের কযেকটি চারা আমেরিকায নিযে
সয়ত্নে রোপণ করেছেন ।</p>

<p>আমাদের পরিবেশের মধ্য়েই এই রকম বহু
উদ্ভিদ আছে য়াদের পাতায , ফলে বা শেকড়ে বিষ
সঞ্চিত হয , য়া অধিক পরিমাণে সেবন করলে
নানা বিপত্তি ঘটে , মৃত্য়ু পর্য়্য়ন্ত ঘটতে পারে । এই
প্রসঙ্গে ইতিহাসপ্রসিদ্ধ গাছ হেমলকের কথা উল্লেখ
করা য়েতে পারে । সুতীব্র হেমলক-বিষ সঞ্চিত
হয এর পাতা , কাণ্ড ও শেকড়ে । বিখ্য়াত দার্শনিক
সক্রেটিসকে এই বিষপানে মৃত্য়ু বরণ করতে বাধ্য়
করা হযেছিল । পূর্বে গ্রীস দেশের বন্দীদের হত্য়া
করা হয এই বিষ পান করতে দিযে । এই বিষাক্ত
হেমলক আজও ইযোরোপের বহু অঞ্চলে বন্য়
আগাছা হিসাবে অস্তিত্঵ রক্ষা করে চলেছে ।
আমাদের চিরপরিচিত ধুতরা গাছের বীজে বিষাক্ত
য্য়ালকালযেড ট্রাইগোনেলিন সঞ্চিত হয । কলেকে
ফুলের ফল ও কুঁড়ির রস এবং শ্঵েত ও রক্ত করবীর
সমস্ত গাছটাই বিষাক্ত । করবীর পাতায , কাণ্ড ও
শেকড়ে বিষাক্ত ওলিযানড্রিন , ওলিযানড্রোসাইড ,
নেরিওসাইড প্রভৃতি হৃদয়ন্ত্রের ক্ষতিকারক গ্লাইকো~
সাইড সঞ্চিত থাকে । ধান , গম , ভুট্টা সমেত
অধিকাংশ ঘাস জাতীয উদ্ভিদের পরাগরেণু
য্য়ালার্জেনিক , পাইন গাছের পরাগরেণুও
য্য়ালার্জেনিক , অর্থাত্ বাতাসের সঙ্গে এদের পরাগ~
রেণু শ্঵াসয়ন্ত্রে প্রবেশ করলে বেশ কিছু লোকের দেহে
য্য়ালার্জির সৃষ্টি হয । কলা , আতা , কাঠাল ,
বেগুন , মুসুরডাল খেলে অনেকেরই য্য়ালার্জি হয ।
আমাদের পরিবেশের মধ্য়ে এই রকম অজস্র বিষ
ও য্য়ালার্জি সৃষ্টিকারী উদ্ভিদ আমাদের চোখের
সামনেই রযছে , তাদের সকলকার কথা না ভেবে ,
তাদের দ্঵ারা কি ভাবে কতটা ক্ষতি হচ্ছে সে কথা
য়াচাই না করে হঠাত্ একটি উদ্ভিদের পেছনে হৈ-চৈ
শুরু করা , - সেটা য়াই হোক সুস্থ মস্তিষ্কের লক্ষণ
নয বিজ্ঞান তো নযই ।</p>

<p>কিন্তু ঠিক এই রকম ঘটনা ঘটেছিল কিছুদিন
আগে আমাদের দেশে পার্থেনিযাম নিযে । কিছু
পত্র-পত্রিকায দাবী করা হযেছিল পার্থেনিযাম অতি
বিষাক্ত গাছ এর স্পর্শ ও গন্ধ মানুষ ও গবাদিপশুর
পক্ষে মারাত্মক ক্ষতিকর । পার্থেনিযাম গাছ স্পর্শ
করলেও এমন চর্মরোগ সৃষ্টি হয য়া কোনদিন সারে
না । কোথায একজন এই রোগ য়ন্ত্রণায আত্মহত্য়া
করেছেন । সঙ্গে সঙ্গে সরকার ও বিজ্ঞানীরা
একয়োগে এই গাছটির বিরুদ্ধে প্রচারে নেমে
পড়লেন । একবারও কেউ খোঁজ নিযে দেখলেন না
এই গাছটি স্পর্শ মাত্রই কোন চর্মরোগ সৃষ্টি হচ্ছে
কি না । গড়ের মাঠে দ্঵িতীয হুগলী সেতুর ধারে
কযেক বছর ধরে পার্থেনিযামে ভরে রযেছে ।
কাছের বস্তীর ছেলেরা ঐ জঙ্গলের মধ্য়ে খেলা করে ,
আমি একজনকার দেহেও কোন চর্মরোগ দেখি
নি , বরং ভেড়া ও ছাগলের পালকে গো-গ্রাসে গিলতে
দেখেছি পার্থেনিযামের পাতা , আমি তার ফটোও
তুলেছি ।</p>

<p>পার্থেনিযাম সম্পর্কে আমরা য়ে গেল-গেল রব
তুলেছিলাম তার অনেকটাই য়ে অতিরঞ্জিত তার
সবচেযে বড় প্রমাণ হল , - সেই হৈ-চৈ এখন আর
কেউ করছে না , টিভিতে ও রেডিওতে আর বক্তৃতা
হচ্ছে না । অথচ পার্থেনিযাম সারা দেশে অতি
দ্রুত ছড়িযে পড়ছে - তার পাশাপাশি দূরারোগ্য়
চর্মরোগের বিস্তারের কোন সাক্ষ্য় নেই । পার্থেনিযাম
একটি দ্রুত সম্প্রসারণশীল আগাছা , অন্য়ান্য় আগাছা
য়া ক্ষতি করে পার্থেনিযামও তা করে য়াচ্ছে , কারও
কারও শরীরে গাছ বিশেষ থেকে য়ে য্য়ালির্জি হয ,
পার্থেনিযাম থেকেও তা হতে পারে , কিন্তু মনে
রাখতে হবে পৃথিবীর প্রায সব প্রজাতির সৃষ্টি করে
থাকে । লুসিযা উডওযার্ড অতি সম্প্রতি বিষাক্ত
উদ্ভিদের ওপর একটি সচিত্র গ্রন্থ লিখেছেন , তাতে
পৃথিবীর সকল রকম বিষাক্ত উদ্ভিদ নিযেই
আলোচনা করেছেন , এই গ্রন্থের কোথাও পার্থে~
নিযামের উল্লেখ নেই । অর্থাত্ পার্থেনিযাম য়ে একটা
বিষাক্ত গাছ , - তার কোন আন্তর্জাতিক স্঵ীকৃতি
নেই ।
 +&gt;*",0
</p></body></text></cesDoc>