<cesDoc id="ben-w-media-bmo06" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-media-bmo06.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>পরিবর্তন</h.title>
<h.author>অশোক.চৌধ</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1981</pubDate>
</imprint>
<idno type="CIIL code">bmo06</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 1229.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-22</date></creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;মেযেরা রাজনীতিতে আসেন কিভাবে , কিসের টানে ?
পরিবর্তন নিউজ ব্য়ুরো ।
1944 । আকাশে মহায়ুদ্ধের রাক্ষসী
মেঘমালা । পরাধীনতার বাঁধন ছেঁড়ার
উচ্ছ্঵সিত বাসনায স্পন্দিত ভারতাত্মা ।
গান্ধীজীর অহিংস অসহয়োগ আন্দোলনের
পাশাপাশি বিপ্লবীদের সহিংস আঘাতে
আঘাতে ব্রিটিশ সাম্রাজ্য়বাদের চুন-বালি-~
পলেস্তারা খসছে ঝুর-ঝুর ...
ছোট্ট দেশীয রাজ্য় কোচবিহার । ওপর
ওপর দেখলে শান্ত , নিঝুম , য়েন বাকি
ভারতের ছোঁযাচ লাগেনি রাজ্য়ে । তবু
পাশের জেলা রংপুর থেকে কারা য়েন
প্রতিরাতেই ঢুকে পড়ে কোচবিহার শহরে ।
কারা তারা ? কেন আসে ? কোথায য়ায ?
এমনি এক রাত্রে গোরা পুলিশ আর
রাজতন্ত্রের সেপাইরা ঘিরে ফেলল একটি
বাড়ি । খোদ স্টেটের য্য়াকাউনট্য়ানট অবিনাশ
চন্দ্র লাহিড়ীর বাড়ি । ওদের সন্দেহ , একটি
পিস্তল লুকোনো আছে এ বাড়িতে । রংপুরের
বিপ্লবীরা পাচার করে গেছে সেটি ।
পুলিশ কিন্তু বোকা বনে ফিরে গিযে~
ছিল সেদিন । কারণ তারা জানতো না -
সেদিনই বিকেলে স্কুলের উঁচু ক্লাসের
ছাত্রী দিদি তার ছোট্ট বোনের কোমরে বেঁধে
দিযেছিল কাগজে মোড়ানো কি য়েন । ফ্রকে
ঢেকে পুলিশের সামনে অবাক চোখে ঘুর
ঘুর করেছিল ফুটফুটে মেযেটি । কেউ টেরও
পাযনি ।
সেদিনের সেই ছোট্ট মেযেটি আজও
ভোলেনি সে ঘটনার কথা । প্রেরণা বলতে
য়া বোঝায ঐ ঘটনাটি তার কাছে তা-ই ।
ঐ ঘটনাই তাকে দীক্ষা দিযেছিল রাজনীতির ।
ছোট্ট মেযেটি আজ বামপন্থী দলনেত্রী । নাম ?
সে কথায পরে আসছি । আগে এ লেখার
জানালাগুলি খুলে দিই ।
অনেক জানালা । অনেকগুলি নারী
মুখ । বিক্ষোভ মিছিল ঐ য়ে তরুণীটি ,
নির্ভীক বক্তব্য়ে ঐ য়ে নারী লোকসভা বা
বিধানসভায ক্ষতবিক্ষত করছেন বিপক্ষ
য়ুক্তিজাল , কলেজের গেট মিটিংযে স্পষ্টবাক
ছাত্রীটি , নির্বাচনী সভায হাজারপ্রাণে জোযার
আনছেন য়ে কল্লোলিনী , নিষিদ্ধ ইস্তেহার
নিযে বনপথে গ্রাম থেকে গ্রামান্তরে চলেছেন
ঐ য়ে তরুণীটি , য়ে বন্দিনী লোহার গরাদে
হাতমুঠো তাকিযে রযেছেন ভবিষ্য়ত্ ভারতে ,
সন্ন্য়াসিনীর স্পর্ধায য়িনি বেছে নিযেছেন
আরেক সন্ন্য়াস - এরা সকলেই এ লেখার
চরিত্র । //
পারটি হোলটাইমারদের দিন চলে কিভাবে ?
য়ারা সব সময পারটি করেন , তাদের পেট
ভরে কি করে ? অবশ্য় এমন নয য়ে পারটি
করলে আর চাকরি করা য়াবে না । পারটি
য়ারা করেন তাদের অধিকাংশই চাকরি করেন
কিংবা ব্য়বসা করেন অথবা জোতজমি থেকে
সংসার চালান ।
কিন্তু অনেকে আছেন য়াদের সুখ-দুঃখ
সংসার পরিজন পারটিকে কেন্দ্র করেই ।
তাদের জীবন পারটির জন্য়ই উত্সর্গ করা ।
তাদের রুটি কে জোগায । হোলটাইমারদের
পেট চালাবার দায পারটিরই ।
একটা শিল্পসংস্থায য়েমন নির্দিষ্ট
তারিখে বেতন দেওযা হয ঠিক সে রকম না
হলেও মোটামুটিভাবে পারটি হোলটাই~
মারদের ক্ষেত্রেও সময মত টাকা পযসা দিতে
হয । শিল্পসংস্থার সঙ্গে পারটির তফাত্
অবশ্য় আসমান-জমিন । সময মত টাকা
পযসা না পৌঁছ৉লে হোলটাইমারদের খুবই
অসুবিধা হয , হযত দিন কযেক উপোস দিতে
হয ; কিন্তু কেউ বিক্ষোভ দেখায না ।
নেতাদেরও ঘেরাও করেন না । পারটির দুর্দিনে ,
আর্থিক অনটনে অনেক সময পারটির হোল~
টাইমারদের দিনের পর দিন অর্ধাহার অনা~
হারেই দিন কাটাতে হয ।
অবিভক্ত সি পি আইতে কোন সময
হোলটাইমার ব্য়বস্থা চালু হয তার কোন
সঠিক সন তারিখ নেই । মোটামুটিভাবে
চল্লিশের দশকে সব জেলাতেই দু-একজন করে
হোলটাইমার ছিলেন । তবে তারা পারটি
থেকে নিযম করে কোন বেতন বা ভাতা
পেতেন না । পরে হোলটাইমারদের মাসিক
ভাতা ঠিক হল কারুর 30 টাকা , কারুর 35
টাকা । দলের দৈনিক মুখপত্র ` স্঵াধীনতা 'র
রিপোরটার এবং অন্য়ান্য় সাংবাদিকরা পেতেন
বেশি - 50 টাকা থেকে দেড়শ টাকা । বাড়ির
আর্থিক অবস্থা বুঝে । দলের সাংবাদিকরা
উপরি রোজগার করতেন টিউশনি করে কিংবা
কোথাও দু একটা লেখা ছাপিযে ।
তবে হোলটাইমাররা পারটির কাজে য়াতা~
যাত করার জন্য় আলাদা রাহা খরচ পেতেন ।
অনেক ক্ষেত্রে হোলটাইমাররা নিজেরা
কোন অবস্থাপন্ন কমরেড অথবা অবস্থাপন্ন
পারটি সমর্থকদের বাড়িতে বিনা খরচে পেযিং
গেসট হতেন । একজন বড় শিল্পপতি
আলাদা বাড়িভাড়া করে কযেকজন হোলটাই~
মারের থাকা খাওযার ব্য়বস্থা করেছিলেন ।
অনেক ছোট-খাটো ব্য়বসাযী এবং বড়
অফিসারও হোলটাইমারদের দাযিত্঵ নিতেন ।
হোলটাইমার ট্রেড ইউনিযন নেতারা খাওযার
খরচ জোগাড় করে নিতেন ইউনিযন থেকেই ।
অবিভক্ত সি পি আই-এর হোলটাই~
মারদের দুঃসমযে গিযেছে পারটি বেআইনী
থাকায সময । রাজ্য় বা জেলা নেতৃত্঵ের সঙ্গে
য়োগায়োগ বিচ্ছিন্ন । মাসিক ভাতা সংগ্রহ
করতে গিযে অনেক হোলটাইমারকে পুলিশের
হাতে ধরা পড়তে হযেছে ।
সি পি আই-এর সবচেযে দুঃসমযে গিযেছে
1962 সালে - চীনের ভারত আক্রমণের
সময । ভিতরে বাইরে বিপদ । অবশ্য়
বিপদটা বেশি হযেছিল পরবর্তীকালে য়ারা
সি পি আই ( এম )-এর সঙ্গে য়োগ দিযে~
ছিলেন তাদের । পারটির বহু কর্মীকে হামলার
মুখে পড়তে হযেছে । পারটি ভাঙ্গার
প্রাক্কালে সি পি আই ( এম ) অনুগামীদের
পারটিতে মাসোহারা বন্ধ । নতুন পারটি
সংগঠিত করার কাজও চলছে । ওই সময
য়ারা মধ্য়পন্থী ছিলেন তাদের অবস্থা আরও
খারাপ । //
কলকাতার প্রথম হাসপাতাল । সুভাষ সমাজদার ।
15 নভেমবর , 1780 ।
সন্ধ্য়ার অন্ধকার ঝাঁপিযে নেমে আসছিল ইংরেজদের নযা উপনিবেশ
কলকাতায । গ্রেট ট্য়াংক বা লালদিঘী এলাকায । সাহেব টাউনের
পাশেই কাঁচা রাস্তায ধুলো উড়িযে ইতস্তত য়াওযা-আসা করছে দুঘুড়ি ,
চৌঘুড়ি , বগি - হরেক রকমের ঘোড়ার গাড়ি । ` খবরদার , খবরদার ' -
সহিসদের উচ্চকিত চিত্কারে আর গাড়ির পাশে পাশে জ্঵লন্ত মশাল
হাতে দুই মশালচির হিস হিস শব্দে চারিদিকের নিস্তব্ধতা শিউরে
শিউরে উঠছে ।
ব্ল্য়াকটাউনের চৌহদ্দিতে জনাকীর্ণ জনপদ চিত্পুর থেকে কশাইটোলা
( বেনটিংক স্ট্রিট ) হযে কালীঘাট পর্য়্য়ন্ত প্রসারিত ব্রড স্ট্রিট বা পিলগ্রিম-~
রোডের মোড়ে লালাবাজারের সম্মুখে গারদখানা অন্ধকারে একটা ভুতুড়ে
বাড়ির মত দাঁড়িযে আছে । সেই ` কনট্রি জেলে 'র গেটে ঝুলছে
রেড়ির তেলের একটা টিমটিমে আলো । ঠিক তারপরেই ব্রড স্ট্রিটির
ওপরে হাজার ঝাড়বাতির আলোয ঝলমল করছে সাহেবদের পানশালা
হারমনিক ট্য়াভারন !
বড় হলঘরের দীর্ঘ টেবিলে শ্য়ামপেন , ব্রানডি আর হুইস্কির ফোযারা
ছুটছে । বেলজিযাম কাঁচের গ্লাসে গ্লাসে শব্দ উঠছে ঠুন ঠুন । টেবিলের
দুই পাশে সারি সারি চেযারে বসে রযেছে ইংরেজ রাজপুরুষরা - হেস্টিংস ,
ক্লেবারিং , ফ্রানসিস , জাসটিস হাইড বারওযেল প্রমুখ । তাদের প্রত্য়েকের
মুখে এক কথা , এক প্রসঙ্গ : -
- ওঃ ক্রাযেসট ! এ য়াত্রা খুব বেঁচে গিযেছি !
- কলকাতার বর্ষাটা পেরিযে য়ে সুস্থ শরীরে বেঁচে থাকবো ভাবতেই
পারিনি -
- এই রেনি সিজনে আমাদের হোযাইট টাউনের মট্য়ালিটি কত ?
পাঠক ! দুশো কি আড়াইশো বছর আগের এই দৃশ্য়টি য়ে
কাল্পনিক নয , বরং নিষ্ঠুর সত্য় তার আভাস পরিস্ফূট হযে উঠবে
পুরনো কলকাতার ইতিহাস বিশেষজ্ঞের এই উক্তিতে : ` সেকালের
( কলকাতার ) জলবাযু অবস্থা এত খারাপ ছিল , ল৉বণ হ্রদ হইতে এমন
দূষিত বাযু উত্পন্ন হইত য়ে বর্ষাকালটা কাটাইযা বাঁচিযা থাকা য়েন
একটা সৌভাগ্য়ের কথা ছিল । প্রতি বত্সর 15 নভেমবর সাহেবদের
একটা মিলনোত্সব হইত । ইহা বহুদিন পর্য়্য়ন্ত ছিল । '
এরই প্রতিধ্঵নি শোনা য়ায প্রাচীন কলকাতার প্রামাণিক গ্রন্থ ক্য়ালকাটা
ওলডে য্য়ানড নিউ-এর বিদগ্ধ লেখক হেনরী কটনের বক্তব্য়ে - //
ইরানের চলচ্চিত্র : ধর্ম , সেনসর , বিপ্লব ও নবীন সম্ভবনা । নির্মল ধর ।
ইরানের ইসলামী বিপ্লব এখনও থিতু
হযনি । প্রান্তিক প্রদেশ ইরাক ও নিজের
দেশের মধ্য়ে নানা চরিত্রের খুচরো ও রাজনৈতিক
দল ও আযাতুল্লাহ খোমেইনির সৈন্য়ের সঙ্গে
প্রায নিযমিতই বারুদ বিনিমযের খবর পাওযা
য়ায । কিছু কম্য়ুনিসট , কিছু প্রতিবিপ্লবীর
দল এখনও গাঁ-গঞ্জের ধুলো-জমানো কোণে
রাশিযা বা আমেরিকার ছাপ মারা আগ্নেযাস্ত্র
নিযে ঘাপটি মেরে বসে থাকে ।
ইরানের রাজনৈতিক আবহাওযা য়েমন
অস্থির , তেমনি অশান্ত ওদেশের সামাজিক
পরিবেশও । মোল্লাতন্ত্রের সাঁড়াশি এখনও
শক্ত হযে ইরানের ঘাড়ে চাপেনি বটে , কিন্তু
আচার-বিচার , সামাজিক নানান আইন-কানুনে
সাঁড়াশির দাঁত দুটো বেশ স্পষ্ট ।
গোঁড়া মোল্লারাও এতদিনে বুঝতে
পেরেছেন ফিলম নামক বস্তুটি কী ধারালো ! '
তাই দেশের সার্বিক অবস্থা স্থিতিশীল করার
সঙ্গে সঙ্গে ইরানের ফিলম জগত্কে জাগিযে
তুলতে চাইছেন তাঁরা ।
শাহ আমলের মত তাঁদের হাতে নেই
ধারালো কাঁচি - আশার বাণী শুনিযেছেন
ওদেশের ফিলম বিভাগের প্রধান মহম্মদ আলি
নাজাফি । তিনি বলেছেন - ` এখন সেনসর
বোরডে কাজ জানা য়োগ্য় লোকদের রাখা
হযেছে । সেকস ও বিবমিষা উদ্রেককারী
ভাযোলেনট দৃশ্য়কে ছাড়পত্র দেওযা হচ্ছে না ।
বিপ্লবী আদর্শকে বাণিজ্য় করলে অবশ্য় অন্য়
কথা । সেক্ষেত্রে শুধু দৃশ্য় বাদ দেওযা হবে না ,
সমগ্র ছবিটাই থাকবে অন্ধকারে । ' এই নির্দেশ -
বাণীতেই অনেকে ভীত ।
ভাল বা মন্দের কথা নয , শাহের আমলে
কিন্তু ইরানে ছবির ব্য়বসা ছিল চটকদার ,
চমত্কার । আমেরিকান ছবিতে বাজার ছেযে
থাকত । একমাত্র রাজধানী তেহরানেই ছিল
একশ তিরিশটি ছবিঘর । পারস্য় উপসাগরের
পারে পুরীর মত ছোট্ট শহর শিরাজে ছবিঘরের
সংখ্য়া ছিল পনেরটি । সারাদেশ জুড়ে
টি-ভি ছড়িযে পড়লেও সিনেমা দেখা ছিল
ওখানকার তরুণদের নেশা । সপ্তাহে চারখানা
ছবি দেখা চাই-ই । প্রদর্শনীর ফি ছিল মাত্র
তিনটাকা করে । গড়ে বছরে আশিটি ছবি
তৈরি হত ইরানে । পঁচাত্তর সালে রেকরড
সংখ্য়ক ছবি হযেছিল - একশটি ।
সংখ্য়া দিযে তো গুণের বিচার হয না ।
ব্য়বসাযীরা ছবি উত্পাদন করেছে । টাকা
আয করার জন্য় । আরট , সামাজিক উদ্দেশ্য় ,
বক্তব্য়ে , এসব জোলা আদর্শে ছাইচাপা দিযে
বেহিসাবী উত্পাদন হতো শুধু তখন । নিজের
দেশের মাটির সঙ্গে য়োগ থাকত না সেসব
ছবির । নগরকেন্দ্রিক য্য়াফলুযেনট সোসাইটির
বুর্জোযা মূল্য়বোধ ও শহুরে জীবনকে ঘিরে
লেখা হত চিত্রনাট্য় ।
কিছুদিন আগে কলকাতায দুটো ছবি
দেখেছি ইরানের । মাসুদ কিমিযাভির
` আরথ ' ও নসির তকভির ` টোপলি । '
` আরথ ' ছবিতে অতীতের বন্ধন , ঐতিহ্য়ের
প্রতি মমত্঵বোধকে এমন এক ভঙ্গীতে
চিত্রিত করা হযেছিল , য়াতে মনে হয
পুরনো ব্য়বস্থার কোন পরিবর্তন কাম্য় নয ,
উচিতও না । //
আঙুরবালা ও ছড়ির একটি আঘাত ।
` সারা জীবন গান গেযে বেড়িযেছি - দেশ
বিদেশে । '
কত স্মৃতির টুকরো জমা হযে
রযেছে আঙুরবালা দেবীর ভাঁড়ারে । ছোটো-~
বেলায ছাত্রী হিসাবে ভালোই ছিলেন ।
একবারেই অবশ্য় ` জ্য়োত্স্না ' বানান নিযে বেশ
লজ্জায পড়েছিলেন । অবশ্য় একটিবারই ।
বাবা-মার কাছে কোনোদিন বকুনিও খাননি ।
অথচ সংগীত শিক্ষাকালে ঘটল এমন এক
ঘটনা - আঙুরবালাদেরী স্মৃতির পটে আজও
য়া অম্লান ।
বযস তখন আঠারো-উনিশ । গান
শিখছেন জমিরুদ্দিন খাঁর কাছে । ঠুংরি-~
গজল-দাদরা । সহজেই গ্রহণ করার ক্ষমতা
ছিল । সুর-তাল-লযের বিষযে কোনোদিন
কোনো অসুবিধা হযনি । সেদিন কি হল ।
ঠুংরি শিখছেন , লম্বা তান । বার বার চেষ্টা
করেও সঠিকভাবে করতে পারছেন না । ক্রমে
ক্রমে বিরক্তি হচ্ছেন খাঁ সাহেব । খাঁ-সাহেব
বেশ কযেকবার বললেন , ` বোল্্ , চুপ কাহে । '
নিশ্চুপ আঙুরবালা । হাতের ছড়িটা দিযে
একবারই মারলেন ছাত্রীর গাযে । অভিমানে
আঙুরবালার চোখে জল । খাঁ সাহেবের
রাগ পড়ে আসতে একটি তামার পযসা বক~
সিস দিলেন । ধীরে ধীরে স্঵াভাবিক হলেন
আঙুরবালা ।
উত্তর কলকাতায বাবা-মার সঙ্গে বাস
করত ছোট্ট মেযেটি । এক মাথা ঘন কালো
চুল - ডাক নাম ছিল নেড়ী । পোশাকী নাম
প্রভাবতী । বাবা বিজন বন্দ্য়োপাধ্য়ায ছিলেন
কাসটমস অফিসের বড়বাবু ।
গান-বাজনা পছন্দ করতেন না বিজন-~
বাবু । অথচ ছোট্ট মেযেটির কান অসম্ভব
তৈরি । রাস্তা দিযে ভিখারি চলেছে গান
গাইতে গাইতে । পযসা দিযে ভিখারিকে
দাঁড় করিযে গান শুনত মেযেটি । শোনা গান
তুলত কণ্ঠে । বাড়িতে বাবার অমত , কিন্তু
মাযের প্রেরণা ছিল । বাবার বন্ধু অমূল্য়
হালদারই গান শেখাবার ব্য়বস্থা করলেন ।
বাবা য়খন বাড়িতে নেই তখনই চলত সংগীত
শিক্ষা । ইতিমধ্য়ে মেসোপটেমিরায য়ুদ্ধে
গেলেন বাবা । অবরুদ্ধ বাসনা এবার পরিণত
হল পূজায । গান চলতে লাগল । এবার
আর লুকিযে নয । তানপুরাও কেনা হল ।
খেযাল আর ঠুংরি । এবারে শিক্ষক রাম-~
প্রসাদ মিশ্র ।
বৃত্তি পাওযা মেযেটিকে বেশি আকর্ষণ
করল সংগীত । ধীরে ধীরে সংগীতের মধ্য়েই
পুরোপুরি নিজেকে নিবিষ্ট করলেন ।
পরবর্তীকালে শিক্ষা জমিরুদ্দিন খাঁ সাহেবের
কাছে ।
রেকরড করায ভীতি ছিল আঙুরবালার ।
সমবযসী বোন বিজু বলেছিলেন , ` কলের গান
য়াঁদের গান শোনা য়ায তাদরকে ছোটে করে
চোঙের মধ্য়ে ঢুকিযে দেওযা হয । সেই
বিশ্঵াসই কাজ করেছে পনেরো বছর বযসেও ।
ইতিমধ্য়ে তাঁর গানের সুখ্য়াতি ছড়িযেছে ।
গানের ব্য়াপারে য়াবার আর সেই আপত্তি
নেই । গল্পের মতো মনে হতে পারে , কিন্তু
সত্য়িই একদিন তাঁর গান শুনে বাড়িতে এসে
উপস্থিত গ্রামোফোন কোমপানির লোকেরা ।
একজন ছিলেন ` মান্তাবাবু ' । নামটি মনে
রেখেছেন আঙুরবালা । আঙুরবালার
রেকরড করায তীব্র আপত্তি । মনের মধ্য়ে
ভয । অবশেষে বাবার সঙ্গে একদিন
বেলেঘাটার কাছে স্টুডিওতে গিযে রেকরড
করলেন । স্মৃতির পটে অস্পষ্ট ছবি :
ফাযার ব্রিগ্রেডের চোঙের মতো দু-তিনটি
চোঙ । গান গাইলেন । পরে সেই রেকরড
করা গান শোনানো হল তাঁকে । ভয ভাঙল ।
প্রথম রেকরড ` বাঁধো না তরীখানি ' ও
ভাটিযালী সুরে প্রচলিত গান ` কালা তোর
তরে কদমতলায ' প্রভূত জনপ্রিযতা অর্জন
করলো । এরপরেই আরো ছটি গান -
তিনটি রেকরডে । তার মধ্য়ে ছিল ` তোমারি
গেহে পালিছ স্নেহে ' । বহুদিন পরে পুরাতনী
বাংলা গানের বিখ্য়াত শিল্পী চণ্ডীদাস মালের
কাছে শুনলেন গানটি রবীন্দ্রসংগীত । রবীন্দ্র-~
নাথের গান তখনো রবীন্দ্রসংগীত হযে
ওঠেনি ।
নজরুলের গানের জগতে কিভাবে প্রবেশ
ঘটল তাঁর ? একদিন গ্রামোফোন কোমপানির
ভগবতী ভট্টাচার্য়্য় জানালেন কাজী নজরুল
ইসলাম আসছেন ট্রেনার হযে । নজরুলের কথায
সুরে প্রথম রেকরড করলেন : ` ভুলি কেমনে
আজ ' ও ` এত জল ও কাজল চোখে ' । বলা
বাহুল্য় য়ে গানও জনপ্রিয হল । সেদিনের
স্মৃতিচারণায আঙুরবালা বলেন - শিল্পীদের
য়থায়থ সম্মান দিতেন নজরুল । কোনো
গানের বাণী বা সু	র সম্বন্ধে সংশ্লিষ্ট শিল্পীর
মতামত জিজ্ঞেস করতেন ।
কলকাতার বেতারকেন্দ্রের সঙ্গে প্রথম
দিনটি থেকেই য়ুক্ত আঙুরবালাদেবী ।
শিল্পীকে 1977-এ সম্মানিত করেছেন
আকাশবাণী কর্তৃপক্ষ , শুধু সংগীত নয -
অভিনেত্রীরূপেও তাঁকে দেখা গিযেছে । ইন্দ্রু-~
বালাদেবী ও কমলা ঝরিযার সঙ্গে ` য়মুনা
পুলিনে ' 1933-এ । ` ধ্রুব '-তে আঙুরবালা
সুনীতি , কাজী সাহেবও অভিনয করেছিলেন
নারদ চরিত্রে । ছবিটি মুক্তি পেযেছিল 1934-~
এ । হিন্দী ছবিতেও কাজ করেছেন : মা কী
মমতা , ` নসীব কা চককর ' । ` চার দরবেশ '
এ সহশিল্পী ছিলেন কানন দেবী ।
সন-তারিখ সঠিকভাবে স্মৃতির মণিকোঠা
থেকে উদ্ধার করতে পারেন না আঙুরবালা ।
তাঁর হিসাবে বযস এখন ছিযাত্তর । একদা
সারা ভারতকে গান শুনিযেছেন ঢ। কোনো
বিশেষ গানের গণ্ডিতে আবদ্ধও থাকেননি ।
আজকে নজরুলগীতির ক্ষেত্রে অন্য়তম
পুরোধা শিল্পী-শিক্ষক হিসাবেও তাঁর খ্য়াতি
সমাধিক । ছাত্রছাত্রীদের মধ্য়ে কেউ কেউ
এখন প্রথিতয়শা ।
মাযের হাত ধরে ছোট্ট মেযেটি গঙ্গাস্নানে
য়েত । অনেকেই ছোট্ট মেযেটিকে গান
গাইতে বলত । গান গাইবার ব্য়াপারে
মেযেটিরও কোনো দ্঵িধা ছিল না , সুরেলা কণ্ঠে
শোনাত ; ` হৃদয রাসমন্দিরে দাঁড়াও মা ' ।
সেই কণ্ঠ শুনেই পরিবারের এক বন্ধু এই
মিষ্টি কণ্ঠের অধিকারিণীর নাম দিযেছিলেন
` আঙুরবালা ' ।
আজকের আঙুরবালা সংগীত জগতের
এক অনন্য় ব্য়ক্তিত্঵ । সম্বর্ধনা পেযেছেন
অসংখ্য়বার , পেযেছেন পুরস্কার । আজ শুধু
তাঁর বক্তব্য় : সম্মান পেযেছেন অনেক , শ্রদ্ধা-~
ভালোবাসাও । কোনো ক্ষোভ নেই । শুধু
ইচ্ছা , ` মরবার দিন য়েন ডাযাসে গান গাইতে
গাইতে মরতে পারি ' ।
দুশো বছরের বাংলা গান ।
য়থায়থ প্রস্তুতি নিযেই ` অরূপ
শিল্প গোষ্ঠী ' 29 জুন রবীন্দ্রসদনে
আযোজন করেছিল দুশো বছরের বাংলা
গানের অনুষ্ঠান । মূল বিষযটির একক
শিল্পী ছিলেন হিমঘ্ন রাযচৌধুরী ।
সঙ্গে গ্রন্থনায ছিলেন রাধামোহন
ভট্টাচার্য়্য় ।
প্রথমে হিমঘ্নবাবুর পাঁচজন ছাত্র-~
ছাত্রী , য়াঁরা আসরে নতুন , রবীন্দ্রনাথ ,
নজরুল , রজনীকান্ত , অতুলপ্রসাদ ও
সৌমেন্দ্রনাথের গান শোনালেন । সুপর্ণা
ভট্টাচার্য়্য়ের রবীন্দ্রসংগীতে গলা ভাল
কিন্তু সানুনাসিক উচ্চারণ মাঝে মাঝে
পীড়া দেয । মানস চট্টোপাধ্য়ায য়থেষ্ট
তৈরি নন মনে হয ।
 +&gt;*"",0",0
</p></body></text></cesDoc>