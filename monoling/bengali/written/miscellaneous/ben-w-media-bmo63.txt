<cesDoc id="ben-w-media-bmo63" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-media-bmo63.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>খেলা....</h.title>
<h.author>অশোক.দাশ</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Magaz</publisher>
<pubDate>1988</pubDate>
</imprint>
<idno type="CIIL code">bmo63</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0022.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-22</date></creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;ইউরোপীযান চ্য়াম্পিযনশীপ প্রতিয়োগিতায
জাঁকজমক , উত্তেজনা বাড়ছে ,
পাল্লা দিযে বাড়ছে দাঙ্গা ,
নিরাপত্তারক্ষীও ।
পশ্চিম জার্মানী থেকে জি সি দাস
এই প্রথম ইউরোপীযান চ্য়াম্পি~
নশিপ দেখছি না । পশ্চিম
জার্মানিতে এর আগে বহুবার
এসেছি । ফলে , বিদেশ ভ্রমণের চমক
বা চোখ ধাঁধিযে য়াওযার কারণ নেই ।
কিন্তু , প্রতিটি আন্তর্জাতিক প্রতি~
য়োগিতাতেই চোখ শুধু নয মনও
ধাঁধিযে য়ায । কারণ , ফুটবল বোধহয
অন্তহীন । প্রতি বছরই নতুন নতুন	
উদ্ভাবনী ক্ষমতা নিযে হাজির হয
দেশগুলো । প্রতি বছরই মুগ্ধ হযে
কিছুটা ভ্য়াবা গঙ্গারামের মত
তাকিযে থাকতে হয প্রতিয়োগিতার
দিকে । শুধু ফুটবলের নয , সমর্থকদেরও
আচরণ পরিবর্তন হচ্ছে প্রতিদিন ।
প্রতিয়োগিতার জাঁকজমক , উন্মাদনা ,
সবই বাড়ছে । এবং পাল্লা দিযে বাড়ছে
দাঙ্গা এবং নিরাপত্তারক্ষী । অস্঵স্তি
একটাই , আমার মত নির্বিরোধী
ফুটবলপ্রেমীদের বড্ড সমস্য়ায পড়তে
হয । ফুটবল মাঠেও ভাবতে হয ,
সুস্থভাবে ফিরতে পারব তো । অবশ্য়
প্রতিবারের মত এবারের প্রতি~
য়োগিতা সম্পর্কেও সুখস্মৃতিই বেশি ।
একমাত্র প্রতি রাতে পশ্চিম জার্মানির
রাস্তায রাস্তায দাঙ্গাবাজ ফুটবল
সমর্থকদের উপদ্রবের জন্য় রাতে
বিদেশী শহরের রাস্তায ভ্রমণের
আনন্দ চেপে রাখার বিরক্তিকর
অভিজ্ঞতাটাই অস্঵স্তিকর । কিছুই
নিখুঁত হয না , তা সে য়ত অসাধারণই
হোক না কেন ।</p>

<p>ইংরেজ দর্শকরা বরাবরই
বর্বর । দীর্ঘদিন আন্তর্জাতিক প্রতি~
য়োগিতায উপস্থিত থাকার জন্য়
ইংরেজ দর্শকরা য়ে কী তা ভালভাবেই
জানি । প্রতিপক্ষের ভাল খেলা বা
সুখেলোযাড়ী মনোভাব কিছুই ওদের
ভাল লাগে না । তবে , এবার দেখলাম
জার্মান সমর্থকরাও কম য়াচ্ছে না ।
ইংরেজরা নানান সময বিভিন্ন দেশে
গিযে গুণ্ডামি করেছে । এবার
জার্মানির তরুণরা বোধহয ঠিকই করে
ফেলেছিল ইংরেজ দর্শকদের বিনা
বাধায গুণ্ডামি করতে দেবে না । ফলে ,
ইংরেজরাও ভালরকমই মার খেযেছে ।
য়াইহোক , বহুল প্রচারিত দাঙ্গা~
গুলোর বর্ণনা দেশে ফিরে গুছিযে
লেখা য়াবে । বেশ কিছু ছবিও য়োগাড়
করেছি । ` খেলা '-র প্রিয পাঠক-
পাঠিকাদের ভালই লাগবে মনে হয ।</p>

<p>আজ , পশ্চিম জার্মানি-হল্য়াণ্ড
সেমিফাইনাল । কিছু পরেই আমার
দুই প্রিয জার্মান সাংবাদিক বন্ধু ব্রুনো
এবং রলফের সঙ্গে দেখা হবে ।
এখানে 5500 হাজার সাংবাদিক
এসেছেন । এরমধ্য়ে কযেকজনের
সঙ্গে সম্পর্কটা এতই ঘনিষ্ঠ হযে
পড়েছে য়ে চটুল রসিকতাও করতে
পারছি । ব্রুনো ও রলফ চমত্্কার
লোক । শুধু ফুটবল নয , রাজনীতি
এবং সাহিত্য় সম্পর্কেও ওদের ধারণা
বেশ পরিষ্কার । প্রেসবক্সে এক
অপরিচিত	 সাংবাদিক কিছুটা অবজ্ঞার
সুরে আমায বলেছিলেন , ` ভারত
দেশটা কোনদিকে ? ওখানে তো প্রচুর
লোক , তারা কি করে ? ' ব্রুনো তাঁকে
বলেছিলেন ` শুধু ভুগোল নয ,
ইতিহাসটাও আপনার পড়া উচিত ।
ভারত য়ে ফুটবলের দুনিযায পৃথিবীর
প্রাচীন দেশগুলোর একটা তো বোধহয
জানেন না । আর , আপনি তো এই
প্রথম চ্য়াম্পিযনশিপে এলেন । মিঃ
দাস চারটে বিশ্঵কাপ কভার
করেছে । '</p>

<p>সাংবাদিকদের মধ্য়ে আমি বেশ
পরিচিত এবং জনপ্রিয হযে পড়েছি ।
হযত একমাত্র ভারতীয সাংবাদিক
বলেই তাড়াতাড়ি নজরে পড়েছি ।
তবে , য্য়াডিডাস প্রাইজ পাওযাটাও
অপর কারণ । ইতালি-পশ্চিম
জার্মানি ম্য়াচে কি হবে ? 5500
সাংবাদিকদের মধ্য়ে 112 জন সঠিক
উত্তর দিযেছিল । অর্থাত্্ 1-1 বলতে
পেরেছিলেন । এরমধ্য়ে মাত্র 10 জন
হাফ টাইমের আগের ফলও ( 0-0 )	
মেলাতে পেরেছিলেন । সৌভাগ্য়
বশত , আমি ছিলাম তার মধ্য়ে । ব্রুনো
এবং রলফও ছিল । 10 জনের মধ্য়ে
প্রথম পুরস্কারের জন্য় লটারী হয ।
এক্ষেত্রে ভাগ্য়লক্ষ্মী ফ্রান্সের আঁদ্রে
লেকোর দিকে তাকায । লেকো পায
অত্য়াধুনিক রঙিন টেলিভিশন ।
আমাদের ভাগ্য়ে জোটে একটা করে
সুদৃশ্য় সাইকো ঘড়ি এবং য্য়াডিডাসের
টি শার্ট । আপাতত আমার হাতে
ঘড়িটি । বহুদিনের মলিন হযে আসা
প্রিয ঘড়িটা খুলে রেখেছি । অবশ্য় দম
দিতে ভুলছি না । আপাতত প্রেস~
বক্সে আমি গেলেই নানান ভাষার
সাংবাদিকরা তাদের বিচিত্র উচ্চারণে
` ব্রাভো ইণ্ডিযা ' বলছে । আমি অতি
বিনযে সেগুলো শুনলেও ভেতরে
ভেতরে গর্বে বুকটা ফুলে উঠছে ।</p>

<p>একটা আন্তর্জাতিক প্রতি~
য়োগিতা মানে তো শুধু খেলা দেখা
নয । সুদূর অতীতের , সদ্য় গত
অতীতের তারকাদের , য়াঁদের কথা
বইযে পড়েছি , য়াঁরা শুধু ছবির বা
স্঵প্নের মানুষ , তাঁদের সংস্পর্শে আসা
য়ায । ফ্রাঞ্জ বেকেনবাউযার , লেভ
ইযাসিন , মিশেল প্লাতিনি , পল
ব্রাইটনারদের দেখছি । তাঁদের
সাক্ষাত্্কার নিচ্ছি । এই ডুসেলডর্ফের
রাজপথে হাঁটছি আলতোবেল্লীর
সঙ্গে । ফ্য়ানদের অত্য়াচারের মধ্য়েও
আলতোবেল্লী আমাকে গুরুত্঵ দিচ্ছেন ।
কযেকঘণ্টা পরেই হযত রাইন নদীর
বুকে প্রায ফাইভ স্টার হোটেলের মত
সুসজ্জিত লঞ্চের পার্টিতে পল
ব্রাইটনারের সঙ্গে একান্তে কথা
বলছি । পুরোটাই স্঵প্নের মত
লাগছে । গতকালই ফ্রাঞ্জ বেকেন~
বাউযার বললেন ` সেমিফাইনালটা
1974-এর বিশ্঵কাপ ফাইনালের
মতই হবে । ' এই অবিশ্঵াস্য় পৃথিবীর
মধ্য়ে আমি এতটাই ঘোরে আছি য়ে
গুছিযে কিছু লেখা মুশকিল । এবং
এতই ব্য়স্ত য়ে সময খুঁজে বড় সড়
লেখা খাড়া করাও সমস্য়ার । আসলে ,
লেখার বিষয এত য়ে , এই স্঵ল্প সমযে
তা লেখা অসম্ভব । দেশে ফিরে
গুছিযে লিখব । সেমিফাইনালটাই
মাথায ঘুরছে । ফুটবল-জ্঵রটা ফাই~
নালের আগে কাটবে না । এখানের
কযেকটা পত্রিকা লেখার অফার
দিযেছে । সেগুলোও অসম্পূর্ণ । তবে ,
মাঝেমাঝে , মনে হচ্ছে ভাগ্য়িস
জার্মানটা শিখেছিলাম ।</p>

<p>স্টীমারে আড্ডার মেজাজে পল ব্রাইটনার
` শক্তির মোকাবিলা
শক্তি দিযেই সম্ভব , শিল্প দিযে নয '
ডুসেলডর্ফ থেকে : জি সি দাস
9 জুন সন্ধ্য়ায ইউরোপীযান
ফুটবল চ্য়াম্পিযনশিপের
সংগঠকরা আযোজন করেছিল
` স্টীমার পার্টি 'র । জমজমাট
ফুটবল-আড্ডা । ছিলেন অনেক
নক্ষত্র । আমার পাশেই ছিলেন
পশ্চিম জার্মানির অতীতের
উজ্জ্঵ল ফুটবল-নক্ষত্র ও এখনকার
বিখ্য়াত ফুটবল সমালোচক পল
ব্রাইটনার । ফুটবল আলোচনারশ
শুরুতেই তাঁকে প্রশ্ন করে
ফেললাম , ইউরোপীযান
চ্য়াম্পিযনশীপে এবার ফেবারিট
কোন দল ?</p>

<p>পল ব্রাইটনার : আমার মতে ,
ফেবারিটের ক্রমতালিকা
এইরকম : ইংল্য়াণ্ড , পশ্চিম
জার্মানি ও হল্য়াণ্ড ।</p>

<p>কেন ? একটু বিস্তারিত ব্য়াখ্য়া
করবেন ?
পল ব্রাইটনার : এটা তো সবারই
জানা , 1974-এ পশ্চিম জার্মানি
বিশ্঵কাপ জিতেছিল । ঐ দলে
মিউনিখের 7।8 জন ফুটবলার
ছিল । তারা সেইসময শুধু পশ্চিম
জার্মানি নয , বিশ্঵ চ্য়াম্পিযন
ক্লাবের ফুটবলার । দুনিযা জুড়ে
খ্য়াতি । এবছর ইংল্য়াণ্ড দলে
চ্য়াম্পিযন লিভারপুলের
খেলোযাড়দের প্রাধান্য় । তারা
সবাই ফর্মেও রযেছে । এক ক্লাবের
খেলোযাড়দের বেশি সংখ্য়ায
জাতীয দলে থাকায আলাদা
গুরুত্঵ রযেছে । বোঝাপড়াটা
ভাল থাকে । পশ্চিম জার্মানিকে
ফেবারিটের তালিকায রাখতে
পারছি না এই কারণেই য়ে
এখনকার পশ্চিম জার্মানির মত
খেলতে পারছে না । পশ্চিম
জার্মানির এখানকার খেলার
ট্য়াকটিসই ভুলে ভরা ।</p>

<p>এটা কি আপনার একসমযের
সহখেলোযাড় ও বন্ধু বেকেন
বাওযারকে সমালোচনা করা হচ্ছে
না ?</p>

<p>পল ব্রাইটনার : তাতো হচ্ছেই ।
কিন্তু কিছু করার নেই । আপনার
তো অজানা থাকার কথা নয , বিশ্঵
চ্য়াম্পিযন হওযার আগে ছেলেরা
দারুণ-পরিশ্রম করত 8।10 ঘণ্টা
প্রাকটিস হত । কখনও কখনও
12 ঘণ্টাও । তাতেই আমরা অর্জন
করেছিলাম শক্তি , গতি ,
স্ট্য়ামিনা । করা গিযেছিল
বিশ্঵জযও ।</p>

<p>আপনাদের কিন্তু স্কিলে
ঘাটতি ছিল ?
পল ব্রাইটনার : তা ছিল । কিন্তু
এটা তো সত্য়ি , স্কিল বা আর্ট
সাফল্য়ের জন্য় একান্ত জরুরী
নয । না হলেও চলে । শুধু স্কিল
বা আর্ট-এর ওপর নির্ভর করে
কোনও ফুটবলার কখনও বড়
সাফল্য় পাযনি । আমি পাওযার বা
শক্তিতে বিশ্঵াসী ।</p>

<p>এবার ইউরো-সফরে প্লাতিনিয
সাফল্য় এসেছে কিন্তু শুধু
শিল্পের ওপর নির্ভর করেই ।
পল ব্রাইটনার : ঐ সাফল্য়ে
কখনও ধারাবাহিকতা থাকে না ।
1982 ও 1986-র বিশ্঵কাপের
দিকে তাকান , প্লাতিনি কিন্তু
আটকে গিযেছিল । শিল্প থেমে
য়েতে বাধ্য় হযেছিল শক্তির
কাছে । সমর্থকরা ট্রফি চায । সেটা
দিতে পারে পাওযার ফুটবলই ।
আর্টিস্টিক ফুটবল চলতে পারে
প্রদর্শনী ম্য়াচে । প্রতিয়োগিতা~
মূলক আসরে দর্শকরা প্রিয দলের
জয দেখতেই মাঠে আসেন ।
সেখানে শুধু শিল্প প্রাধান্য় পেতে
পারে না । এখন পাওযার
ফুটবলের য়ুগ । আর শক্তির
মোকাবিলা সম্ভব শক্তি দিযেই ,
শিল্প দিযে নয । আর শক্তি বা
স্ট্য়ামিনা বাড়ানোর জন্য় য়ে
ধরনের প্য়্রাকটিস দরকার তা এখন
পশ্চিম জার্মানির ফুটবলারদের
করানো হচ্ছে না ।</p>

<p>উইম্বলডন '88
ম্য়াকেনরোর ইচ্ছেটাই
সবচেযে বড় কথা ।
অরূপ বসু ।
য়দি এমন হয , য়দি গ্রীন
কার্পেটেই হয , নির্ঝরের স্঵প্নভঙ্গ , য়ে
ঝড় ওঠার কথা ছিল টেনিস টার্নাডো
ম্য়াকেনরোর ঱্য়াকেট থেকে , তা
নাভিমূলেই শেষ হযে য়ায , য়দি
রাজার য়ুদ্ধ জযের হারানো রাজমুকুট
পুনরুদ্ধারের শেষ স্঵প্ন সবুজ ঘাসের
বিছানায পরাহত নিষাদের মত শুযে
থাকে , তাহলেও হযত প্রকৃতি এতটুকু
ভারী হবে না । গ্রীন কার্পেটে পড়ে
থাকবে কিছু শ্঵েতবিন্দু , বিযার-পাব-
এ কোন কোন আসর একটু গম্ভীর
হবে । ব্য়স ঐ পর্য়্য়ন্তই । উইম্বলডনের
গ্রীন কার্পেট বড় নিষ্ঠুর , এখানকার
বাতাসে এখনও টেনিসের বিকল্প সুর
বাজে । উইম্বলডন য়াকে প্রত্য়াখান
করে তাকে আর সহজে গ্রহণ করে
না । রোজওযালকে করেনি । লেণ্ডলকে
এখনও করেনি ।</p>

<p>সেই গ্রীন কার্পেটে হারানো
রাজ্য় পুনরুদ্ধারের জন্য় য়ুদ্ধে নামছেন
জন ম্য়াকেনরো । এ লেখা য়খন
পাঠকদের কাছে পৌঁছবে ততদিনে
ম্য়াকেনরো কতটা বিক্ষত হলেন ,
পাঠকদের তা জানা হযে য়াবে । দুই
নারী ,	 সোনিযা মূল হত্য়াকাণ্ড ও ট্য়াটাম
ওনিল । একজন নামী মডেল ও
একজন নামী অভিনেত্রী ।শ য়খন মানুষ
সভ্য়তার গহীন অন্ধকার থেকে হঠাত্্
আলোর বিন্দু দেখেছিল , তখন থেকেই
মানুষের সাধনার সিদ্ধি নারীতে ।
সোনিযার প্রেরণায উইল্য়াণ্ডার
অস্ট্রেলিযান ও ফ্রেঞ্চ ওপেন	 জিতে
সাফল্য়ের পথে ফিরেছে । আর
ট্য়াটামের আবেগের স্রোতে ম্য়াকেন~
রো টেনিস কোর্ট থেকে দূরে অনেক
দূরে ভেসে গেছিলেন । গত দুবছর
উইম্বলডনের আসরে ম্য়াকেনরোর
রাজকীয উপস্থিতির স্পর্শ কেউ
পাযনি । আসলে ট্য়াটাম ও ম্য়াকেনরো
দুজনেই , দুজগতের তারকা ছিলেন -
বড় বেশি উজ্জ্঵ল । এগার বছর বযসে
পেপার-মুন ফিল্মে অভিনযের জন্য়
অস্কার পেযেছেন ট্য়াটাম । আর 11
বছর আগে বিশ্঵ের সর্বকনিষ্ঠ
খেলোযাড় হিসেবে উইম্বলডনে
সেমিফাইনালিস্ট হযে টেনিসে বিশ্঵
আলোড়ন তুলেছিলেন । তারপর শুধু
সাফল্য় , সাফল্য় ধরে রাখার জন্য়
লড়াই করে য়খন দুজনে ক্লান্ত , য়খন
জীবন থেকে অনেকদূর এক অদ্ভূত বিচ্ছিন্ন
জীবনের চাকা দুজনকে ছুটিযে চলেছে ,
তখনই একটি পার্টিতে দুজনের দেখা ,
আঠাশ বছরের ম্য়াকেনরো ও তেইশ
বছরের ট্য়াটাম । য়ে ম্য়াকেনরো
অন্য়দের থেকে আলাদা , বান্ধবীকে
কোর্টের পাশে বসিযে কোর্টের মধ্য়ে
ছোটাছুটি করতে য়ে ভালবাসে না , য়ে
কখনই অনুরাগিনীর সহজে ধরা
দেযনি , একবার কি দুবার ছাড়া , সেই
ম্য়াকেনরো , ট্য়াটামকে ধরা দিযেছিল
প্রেমের বন্ধনে । দুজনেই তাই পেতে
চেযেছিল জীবনের অন্য় স্঵াদ , গভীর
ও নিবিড় করে । তাই সোনিযার মত
ট্য়াটাম ম্য়াকেনরোকে আরও বেশি
করে টেনিস কোর্টের দিকে ঠেলে	 দিতে
পারেনি ।</p>

<p>ছিযাশির গোটা মরসুমটাই
ম্য়াকেনরো ছিলেন টেনিস কোর্ট থেকে
অনেকদূরে ফ্লোরিডার এক ইন্দ্রপুরীতে ,
ট্য়াটাম ওনিলের ও নবজাতকের
কাছাকাছি । 1981-1984 য়িনি
ছিলেন বিশ্঵ের একনম্বর , সেই
ম্য়াকেনরোর কাছে টেনিস দুনিযার
এমন কিছু অবশিষ্ট ছিল না , য়া
ট্য়াটামের আকর্ষণকে উপেক্ষা করতে
পারে । সময এগিযেছে , সবকিছুর মত
এই অন্য় জীবনের স্঵াদও পুরনো হযে	
এসেছে , ম্য়াকেনরো দ্঵িতীয পুত্রের
জন৉ক হযেছেন , ধীরে ধীরে আবার
টেনিসের অঙ্গনে তাঁর বর্ণময
পদসঞ্চার ঘটেছে , কিন্তু সেই জেদ
নেই , সেই ভেদশক্তি নেই , তাঁর প্রথম
সার্ভ আশির দশকের গোড়ার মত
প্রতিপক্ষে দেহ-মনে আতঙ্ক বাড়ায
না , দ্঵িতীয সার্ভ টেনিস প্রেমীদের
রোমাঞ্চিত করে না , তাঁর গ্রাউণ্ড-পেস
আর প্রতিপক্ষের সব আচরণকে
শিশুর পর্য়্য়াযে নামিযে আনে না , বরং
তাঁর ড্রেসিংরুমে ট্য়াটাম ওনিল ও
নবজাতকের উপস্থিতি সমালোচক~
দের তীরগুলোকে শানিত করে । এই
অবস্থায সাতাশিতে চারটি প্রথম
শ্রেণীর টুর্নামেণ্ট অনেকদূর এগিযেও
য়খন শেষধাপে পা রাখতে পারলেন
না , তখনই হযত তাঁর হতাশামন্দ্রিত
কণ্ঠ থেকে সেই কথাটা বেরিযে
এসেছিল য়া অবসর গ্রহণের আগে শুধু
কোন খেলোযাড়ের কণ্ঠ থেকে বেরিযে
আসে , সাতবছর ধরে একটানা টেনিস
খেলে ক্লান্ত । সারা বছর নিজেকে
শান্তি দেওযার কথা ভাবলে অস্঵স্তি
হয । তবে ফিরে আসার ইচ্ছে
পুরোপুরিই আছে । কিন্তু তখনই
ফিরব য়খন ইচ্ছে হবে । ' হ্য়াঁ শেষের
কথাটাই সবচেযে জরুরী , সবচেযে
গুরুত্঵পূর্ণ । ম্য়াকেনরোর ইচ্ছেটাই	
সবচেযে বড় কথা । এই প্রতিবেদকের
ম্য়াকেনরোর মুখোমুখি হওযার
সৌভাগ্য় কখনই হযনি । কিন্তু এমন
দু-চারজন টেনিস তারকার মুখোমুখি
হওযার সৌভাগ্য় হযেছে , য়ারা
ম্য়াকেনরোর বিরুদ্ধে খেলেছেন
কিংবা ম্য়াকেনরোকে খুব কাছ থেকে
দীর্ঘদিন দেখেছেন । প্রাক্তন উইম্বল~
ডন চ্য়াম্পিযন ইযান কোদেস
কযেকবছর আগে চেক দলের সঙ্গে
কলকাতায এসেছিলেন । একান্ত
সাক্ষাত্্কারে কোদেস স্঵ীকার
করেছিলেন , লেভারের পর ম্য়াকেন~
রোই সেরা টেনিস প্রতিভা । ' অথচ
রড লেভার বলেছিলেন , ` ম্য়াকেনরো
আমার চেযেও বড় টেনিস প্রতিভা ।
কোদেস বলেছিলেন , ` ম্য়াকেনরোর
প্রত্য়াবর্তন নির্ভর করছে ওর ইচ্ছের
ওপর । ' আর বিজয বলেছিলেন ,
শৃঙ্খলিত জীবনয়াপন করলে
ম্য়াকেনরো য়খন ইচ্ছে তখনই বিশ্঵ের
এক নম্বর হতে পারবে । ' রমেশ
কৃষ্ণাণের মুখেও একই কথার প্রতিধ্঵নি
যশুনেছি । সেই ম্য়াকেনরো উইম্বল~
ডনের কিছুদিন আগে বলেছেন ,
` আমার দৃঢ়-বিশ্঵াস আমি আবার
বিশ্঵ের একনম্বর জাযগায পৌঁছতে
পারব ।
 +&gt;*",0
</p></body></text></cesDoc>