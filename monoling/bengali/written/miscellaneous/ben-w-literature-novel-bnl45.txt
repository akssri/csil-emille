<cesDoc id="ben-w-literature-novel-bnl45" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-literature-novel-bnl45.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>ঘরলরমধহয়</h.title>
<h.author>শথকরমুখশ</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book.</publisher>
<pubDate>1990</pubDate>
</imprint>
<idno type="CIIL code">bnl45</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 08470.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-22</date></creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;গতরাতহরল পমার বহয়াপারল ভাবনানি মহয়ানসনলর যক নতুন রূপ ধরা
পডলশ া ওদলর দারশযান নিরহদহধিধায নমাকল বললশ যখানল পমা বা কলফ
নসলনি া ধথচ শলষ পরহয়হয়নহত ওখান থলকল঩ গণলশ সরকার পমা ও
বিপুলভূষণ বারিককল ফদহধার করল নিযল গলললন া দারশযান কি বহয়াপারটা
সতহয়ি঩ জানতশ না ? না , মালিকলর নিরহদলশ মতশ নমাকল ঩চহছল করল঩
বিপথল চালালশ দারশযানজী ?</p>

<p>শকুনহতলা-চাওলার সিলভার ডহরাগনলর ভুমিকা঩ বা কী ? যরা কি ধনলক
বহয়াপারল঩ নগাম কিছু জলনল বসল নছলন ?
কিনহতু সব থলকল য়া নশহচরহয়হয়লর , গণপতিবাবু কি করল পমা-সনহধানল য঩
থহয়াকারল মহয়ানসনল যসল হাজির হললন ? যবথ নজ কলন তিনি পহরশহন
করললন পমার বহয়াপারল নমি নগল থলকল কিছু জানতাম কিনা া</p>

<p>যসব বহয়াপারল ধকারণল ফদহ঵িগহন হওযার কশনশ মানল হয না া কিনহতু
পহরসঙহগটা সতহয়ি঩ গুরুতর , বিশলষ করল য়খন বিলাসিনী দলবীর যকমাতহর
সনহতান য঩ রহসহয়জালল জডিযল রযলছলন া
নকাশ-পাতাল ভাবছি া যমন সময বলযারা যসল বললশ , "" হুজুর ,
নপনার ফশন া ""</p>

<p>নপিস ঘরল যসল ফশন ধরতল঩ ওদিক থলকল য়ল হাসির ঝড ফঠলশ
তাতল঩ বুঝতল বাকি র঩লশ না কল য঩ ফশন করছলন া
"" হহয়ালশ , হহয়ালশ , মিষহটার শথকর ? নরল কশথায লুকিযল-লুকিযল
বলডাচহছলন ? সকাল থলকল ফশন করল করল পাতহতা঩ পাচহছি না া শকুনহতলা
চাওলা কি নজও নপনাকল ডলকল সকাল বললায গপহপশ জুডলছলন ? ""</p>

<p>মিসলস পপি বিশশযাস ধকারণল঩ টললিফশনলর ওধারল হাসিতল ভলঙল পডছলন া
মিসলস পপি বিশশযাসশর ননহদাজটা য়ল মিথহয়ল তা নমি জানিযল দিলাম া
মিসলস বিশশযাস বলললন , "" ফদ নপনাকল ধরা য়ল কী শকহত হযল ফঠলছল া
ধনহতত চারবার ফশন করলছি সকাল থলকল া য঩ নপনার গা ছুতযল বলছি া ""
সরহবনাশ ! য঩ মহিলারা টললিফশনলও কীভাবল লশকলর গা ছুতযল ফলললন
ভগবান জানলন া</p>

<p>পপি বিশশযাস ধভিয়শগ করললন , "" নপনি নমাকল কিছু঩ বলছলন না া
কিনহতু নমি সব জানি া ""
"" কী বহয়াপারল ? "" পমার বহয়াপারটা নিজলর থলকল পহরচার করবার কশনশ
঩চহছা঩ নমার ছিল না া</p>

<p>নবার খিলখিল করল হলসল ফঠললন পপি বিশশযাস া "" নমার কাছল
বশকা সলজল কী লাভ , মিষহটার শথকর ? য়া঩ হশক নপনি নপিস
ঘরল঩ বসল থাকুন া নমি যখনি঩ যকবার ঘুরল য়াচহছি া ""</p>

<p>কিছুকহষণলর মধহয়ল঩ পপি বিশশযাস সশরীরল যখানল ফপসহথিত হললন া নজ
যকলবারল টাঙহগা঩ল সাডি পরল খাতটি বাঙালী সলজলছলন পপি বিশশযাস া
পপি বলললন , "" মিষহটার জলঠমালানির রিকশযলসহটল নজ যকলবারল দলশী
সাজ করলছি া দিলহলীর পারহটির কাছল সলণহট পারহসলণহট বলঙহগলী লুকলর ভীষণ
কদর কিনা া বিশলষ করল য঩ ধল কটন টাঙহগা঩ল শাডির া কাপড দলখল
নপনি বুঝতল঩ পারবলন না য়ল পষনল তিন শ' টাকা দাম া ""</p>

<p>পপি বিশশযাস নজকল নর সিগারলট ধরাললন না া বলললন , "" গলাটা
ঠাণহডা ললগল যকলবারল বুজল নছল া যকটু পরল঩ নবার বকর-বকর করতল
হবল যক গলসহটলর সঙহগল া পুরানশ পারহটি , নগলও লুক-নফটার করলছি ,
ভদহরলশক যকলবারল কথার জাহাজ া সারাকহষণ নবশলতাবশল বকল না য়লতল
পারলল ভাববলন নমি যহয়াটলনশন দিচহছি না া "" //
মিসলস বিশশযাস বলললন , "" নরও যকদিন ভাবতল সময নিলাম া কিনহতু
঩তিমধহয়ল ধভাব নরও বলডলছল া কিছু কাতচা টাকার জনহয় মনটা ছটপট করছল া
তখন মিসলস ঘশষালকল ফশন করলাম া ভদহরমহিলা সশজা নমাকল ওর ফহলহয়াটল
চলল নসতল বলললন , য঩ থহয়াকারল মহয়ানসনল া তখন তশ নপনি য পাডায
নসলন নি া মিসলস ঘশষাল঩ নমাকল হিণহট দিললন পহরথম - বলললন ,
"" তশমার তশ সব঩ নছল , তবু চিনহতা কলন ? নমি঩ সব বহয়বসহথা করল দলবশ া ""
"" কলন ধকৃতজহঞ হবশ , মিষহটার শথকর , ফনি঩ নমাকল পহরথমল য঩ লা঩নল
সাহস করল পা ফললবার পথ দলখিযল দিললন া ফনি঩ বলললন , মিসলস
বিশহ঵াস , তশমার নামটা যকটু ধলটার করল নাও া পপি বিশহ঵াস রাতারাতি
য঩ ধবিশহ঵াসলর লা঩নল যসল পপি বিশশযাস হযল গললশ া ""</p>

<p>"" নর মিষহটার বিশহ঵াস ? "" নমি জিজহঞলস করি া
"" সব বাজল কথা া কশথায মিষহটার বিশহ঵াস ? মিষহটার বিশহ঵াস কশনশদিন঩
ছিললন না া নসলল যটা য লা঩নলর রলওযাজ া য়লমন মিসলস সাবিতহরী
ঘশষাল া নসলল সাবিতহরী দাস না কি ! যকবার ছশটবললায কী যকটা বিযলও
হযলছিল া কিনহতু বালহয়বিধবা হযল মামার বাডিতল ফিরল যসলছিললন সাবিতহরী া
তারপর কশন যকটা ধপিসল চাকরীও জুটিযলছিললন া কিনহতু তাতল কী
নর হবল ? পাকলচকহরল কাজলর সুবিধার জনহয় সাবিতহরী হললন মিসলস সাবিতহরী
ঘশষাল া ""</p>

<p>পপি বলললন , "" নপনি যসব শুনল রাখুন , মিষহটার শথকর া নিজলর
গশপন কথা তশ ধনহয় কাফকল বলল য়লতল পারলাম না া মিসলস ঘশষাললর
মুখল঩ পহরথম শুনলছিলাম , য঩ লা঩নল য঩ মিসলস টা঩টললটা যকটা যহয়াসলট া
মিস হলল঩ হাজার হাঙহগামা া বুদহধিমান লশকলরা কপালল সিতদুর না দলখলল
যগশতল঩ চায না া নসলল নমাদলর য লা঩নল রলড সিগনহয়াল঩ হচহছল গহরীণ
সিগনহয়াল া ""</p>

<p>নমি হতবাক হযল মিসলস পপি বিশশযাসলর মুখলর দিকল তাকিযল
নছি া কলমন সহজল নিজলর জীবনলর ধনহধকার ধধহয়াযগুলি তিনি নমার
সামনল যকলর পর যক তুলল ধরছলন া পাকলচকহরল য঩ থহয়াকারল মহয়ানসনল
নসতল না পারলল মানুষলর ত঳রী য঩ বিচিতহর সমাজ সমহপরহকল নমার জহঞান
ধসমহপূরহণ থলকল য়লতশ া</p>

<p>মিসলস পপি বিশশযাস বলললন , "" মিসলস সাবিতহরী ঘশষাললর কাছল
নমি য়থলষহট ভণী া ওর সাহায়হয় না পললল যতদিনল কশথায ভলষল চলল
য়লতাম , তার ঠিক নল঩ া ফনি঩ নমাকল মনল করিযল দিতলন , "" পপি
সব সময মনল রাখবল , সময ভাঙিযল খলযল চললছি নমরা া সহরশতলর
বিরুদহধল সাততার কাটছি নমরা া নমাদলর নপনজন বলল য঩ পৃথিবীতল
কলফ নল঩ কলবল য঩ নিজলর দলহটুকু ছাডা া ""</p>

<p>"" নমি ওর পহরতিটি কথা ধকহষরল ধকহষরল মনল রলখল চলবার চলষহটা করলছি ,
মিষহটার শথকর া "" মিসলস পিপ বিশশযাস যবার দীরহঘশহ঵াস ছলডল বলললন ,
"" কিনহতু য়িনি নমাকল যত জহঞান দিললন তিনি নিজল঩ শলষ পরহয়হয়নহত বলপরশযা
হযল ফঠললন া ""</p>

<p>নমি পপি বিশশযাসলর মুখলর দিকল নবার তাকালাম া সিগারলটল নর
যকটা লমহবা টান দিললন তিনি া তারপর বলললন , "" য়তদিন সময ছিল
ততদিন বলপরশযাভাবল কাটিযল দিললন মিসলস ঘশষাল া কশনশ চিনহতা
করললন না , সময য়ল চিরকাল থাকবল না তাও ভাবললন না া ""
"" খুব খরচ করতলন বুঝি , মিসলস ঘশষাল ? "" নমি জিজহঞলস করি া
"" খরচ তশ করতলন঩ - দুহাতল া থহয়াকারল মহয়ানসলনর ফহলহয়াটখানা ঩নহদহরপুরী
করল রলখলছিললন - গাতটলর টাকায কলনা নিজলর ফহলহয়াটলও লশকল ধত টাকা
ঢালল না া গরীবদুদখী য়ল-য়া চা঩তশ তা঩ হাত ফজাড করল দিতলন া তারপর
পদসহখলন হলশ া ""</p>

<p>পদসহখলন কথাটা শুনল঩ নমি যকটু সজাগ হযল ফঠলাম া মিসলস
বিশশযাসলর কথাবারহতা যবার মন দিযল শশনা বিশলষ পহরযশজন া
পপি বিশশযাস নিজলর মনল঩ বলললন , "" সব লা঩নল঩ ভুললর কহষমা
নছল , কিনহতু নমাদলর য লা঩নল পদসহখললন পহরাযশহচিতহত নল঩ া মিসলস
সাবিতহরী ঘশষাল যত বুঝলও য঩ সামানহয় কযলকটা কথা য়থা সমযল বুঝতল
পারললন না া কশথাকার যকটা লশকলর সঙহগল বডহড বলশী ভাব জমিযল
ফললললন া তখন঩ নমার সনহদলহ হযলছিল , প঩ প঩ করল নমি তাকল
সাবধানও করল দিযলছিলাম া কিনহতু কশন ফল হলশ না া সাবিতহরী ঘশষাললর
পহরধান যহয়াডভা঩জার তখন নপনাদলর য঩ শকুনহতলা চাওলা া দুজনলর খুব
ভাব া ওর঩ কাছল য঩ থহয়াকারল মহয়ানসনলর ফহলহয়াটখানা জমা রলখল সাবিতহরী
ঘশষাল কলকাতা শহর থলকল বিদায নিললন সতী-সাবিতহরীর রশলল চিরকাল
পারহট করবার লশভল া ""</p>

<p>যকটু থামললন মিসলস পপি বিশশযাস া তারপর বলললন , "" কিনহতু কপাল
ভাঙতল দলরী হলশ না মিসলস ঘশষাললর া যক বছর য়লতল না য়লতল নবার
ফিরল নসতল হলশ য঩ শহরল া হবল঩ তশ ! পশডা কপাল না হলল মলযলরা
য঩ লা঩নল নসবল কলন বলুন ? ""</p>

<p>"" সাবিতহরী ঘশষালকল দলখল তখন সতহয়ি দুদখ হয , মিষহটার শথকর া পশডা
কাঠলর মতশ চলহারা হযলছল া টাকা কডি গযনাগাটি ধনলক করলছিললন
মিসলস ঘশষাল , সল সব ওখানল খু঩যল , গশটা কযলক কাপড নিযল চলল
যসলছলন া থুডি , ফনি চলল হযতশ নসতলন না - ওকল তাডিযল দিযলছল া ""
মিসলস পপি বিশশযাস বলললন , "" যখানল঩ দুদখলর শলষ নয া নপনার
ফহরলণহড শকুনহতলা চাওলা া ওর খুরল খুরল নমসহকার া ওকল তখনি঩ তশ চিনতল
পারলাম নমি া ""</p>

<p>মিসলস ঘশষাললর কাছ থলকল য঩ থহয়াকারল মহয়ানসনলর ফহলহয়াটখানা জমা নলবার
নগল মিসলস চাওলা বললছিললন , "" তশমার কশনশ চিনহতা নল঩ , সাবিতহরীদি া
পিছনলর দিকল না তাকিযল য়লখানল য়াচহছশ সলখানল চলল য়াও া তশমার ফহলহয়াটটা
পললল নমার খুব সুবিধল হয া নমি দলখাশুনা করবশ , ভাডা দলবশ -
তুমি যকটা কাগজল লিখল দিযল য়াও , নমার ফহলহয়াটল নমার ধনুপসহথিতিতল
নমার বনহধু মিসলস শকুনহতলা চাওলাকল কলযার-টলকারলর দহ঵াযিতহ঵ দিযল
গললাম া ""</p>

<p>মিসলস সাবিতহরী ঘশষাল সরল বিশহ঵াসল সঙহগল সঙহগল ও঩ কথাগুলশ যকটা সাদা
কাগজল লিখল মিসলস শকুনহতলা চাওলার হাতল দিযলছিললন া শকুনহতলা
সল঩ কাগজখানা বহয়াগল পুরতল পুরতল বললছিল , "" কশনশ ভাবনা নল঩
তশমার া তশমার ফহলহয়াট তশমার঩ র঩লশ - ফিরল যসল য঩ ফহরলণহডলর সঙহগল
দলখা করলল঩ সঙহগল সঙহগল চাবি পলযল য়াবল া ""</p>

<p>"" কিনহতু মিষহটার শথকর , মানুষ চিনল রাখুন া "" সাবধানবাণী ফচহচারণ
করললন মিসলস পপি বিশশযাস া
"" য঩ শকুনহতলা চাওলা , নপনার ফহরলণহড , নপনাকল ডিনার খাওযায া
কিনহতু লশক মশটল঩ সুবিধার নয া বলচারা মিসলস সাবিতহরী ঘশষালকল ও঩
মহিলা ধসমযল঩ চিনতল পারললন না া ফহলহয়াটলর কথায য়লন নকাশ থলকল
পডললন া নর ধনহয় নপনাদলর কযলকজন করহমচারী া মিসলস চাওলার
সঙহগল য়শগসাজস করল , মিসলস সাবিতহরী ঘশষাললর নাম ভাডাটলদলর লিষহট
থলকল কবল ফঠিযল দিযলছল া সল঩ জাযগায কার নাম ফঠলছল তাও বুঝতল
পারছলন নিশহচয঩ া "" //
কলকালির সঙহগল দলখা করবার জনহয় ছাদল ফঠতল য়াচহছি , যমন সময
বলযারা যসল হনহতদনহত হযল বললশ , "" নপনি কশথায ছিললন সহয়ার ?
নপনাকল খুব খুতজছি া ""
যত বহয়সহত হযল খুতজবার কারণ কী জানতল চা঩লল বলযারা বলচারা
কশনশ কহরমল বললশ , "" টহয়াকহসি - ললডিজ , সহয়ার া ""</p>

<p>টহয়াকহসি ধবশহয়঩ নসতল পারল া থহয়াকারল মহয়ানসনলর য঩ দু-মুখশ গলট
দিযল কত টহয়াকহসি঩ তশ পহরতিদিন পহরবলশ করছল া ধনলক টহয়াকহসিওযালা তশ
মলন রাসহতার হাঙহগামা যডিযল শরহটকাটলর সুবিধা ভশগ করার জনহয় থহয়াকারল
মহয়ানসনল কশন কাজ না থাকললও যক গলট দিযল ঢুকল খশসমলজাজল ধনহয়
গলট দিযল বলরিযল য়ায া থহয়াকারল মহয়ানসনলর মা঩নল-করা দারশযানরা য঩ সব
বহয়াপার নিযল মাথা ঘামায না া রাসহতার টহয়াকহসিওযালাদলর ফপর নজর রাখবার
মতশ সসহতা সময রামসিথহাসন চষরাসিযার সহকারীদলর নল঩ - তারা সল঩
সময রিকহসাওযালা যবথ ধনহয়ানহয় পারহটিদলর কাছল নিজলদলর পাওনা-গণহডা
নদাযল বহয়াসহত া</p>

<p>য-বিষযল রামসিথহাসনলর সাথল নলাপ-নলশচনা করার কশনশ তাগিদ
ধনুভব করি না া কারণ রামসিথহাসন চষরাসিযা বিচিতহর যক ফপাদানল
ত঳রী া ফপরওযালার মুখলর ফপর পহরতিবাদ করার বা না বলার কথা
সল সহ঵পহনলও ভাবতল পারল না া য়ল কশনশ সমালশচনা ধথবা নদলশ সল
নীরবল মাথা নীচু করল মলনল নলয , দু-যকদিন সল঩ নিরহদলশ ধনুয়াযী
কাজকরহমও করল া তারপর সল঩ নবার পুরানশ ধবসহথা , ধথবা তার
থলকলও খারাপ ধবসহথার সৃষহটি হয া</p>

<p>থহয়াকারল মহয়ানসনলর কমহপাফণহডলর মধহয়ল পাযল চলা পথলর মধহয়ল য়লখানল
সলখানল ফললল রাখা রিকহসা সমহপরহকল যকবার রামসিথহাসনলর দৃষহটি নকরহষণ
করলছিলাম া রামসিথহাসন সঙহগল সঙহগল কিছু কিছু গরীব রিকহসাওযালাকল
থহয়াকারল মহয়ানসন থলকল বহিষহকারলর নদলশ দিযলছিল া কিনহতু সল মাতহর যক
দিনলর জনহয় া তার পরল঩ সল঩ পুরানশ বহয়বসহথা - মাঝখান থলকল পহরতহয়লক
রিকশার ফপর রামসিথহানলর মাসিক পহরা঩ভলট টহয়াকহসলর পরিমাণ দলডা
হযল গিযলছিল া</p>

<p>রিকশাওযালারা সব জলনল শুনলও পহরতিবাদ করতল সাহস পায নি া
কারণ য-পাডায নর কশথাও যত ভালশ য়াতহরী পাওযা য়ায না , যবথ
ভাডার জনহয় ধপলকহষার সময , বাস লরি কিথবা পুলিসলর কথা ভলবল
ধয়থা বহয়সহত হতল হয না , রিকশার ফপরল বসল঩ নিশহচিনহতল বিশহরাম
নলওযা য়ায া</p>

<p>দূরদরহশী রামসিথহাসনজী য়ল রশজগার বৃদহধির নরশ সব ধভিনব পনহথা
নবিসহকার করলছলন সল খবরও মদনার কাছ থলকল পলযলছি া
মদনা বললছল , "" মা-কালীর দিবহয়ি , সহয়ার , চুকলি খাওযার লা঩ন
নমি ছলডল দিযলছি া নপনি য়দি দারশযানজীর ফপর রাগ না করলন
তবল঩ খবরটা দলবশ , সহয়ার া ""</p>

<p>শুধু রাগ নয , মদনা সুয়শগ বুঝল নমার কাছ থলকল নরও পহরতিশহরূতি
নদায করলছিল া মদনার কথা শশনা মাতহর঩ তডিঘডি কশনশ বহয়বসহথা
নলওযা য়াবল না , কারণ তাতল সথশহলিষহট ধনলকলর঩ খুব কহষতি হবল া
মদনার কশনশ পহরসহতাবল঩ নমি না বলতল পারি না া ওর কথাবারহতা যবথ
হাবভাবলর মধহয়ল যমন যক ধরনলর ছলললমানুষী নছল য়া নমাকল নকরহষণ
করল া</p>

<p>মদনা বললছিল , "" গলায প঳তল জডানশ থাকলল কী হবল , সহয়ার ?
দারশযানজীর নজর ফতচু থলকল নীচু সব দিকল঩ রযলছল া পযসা কামানশর
কশনশ যসহকশপ঩ তিনি হাতছাডা করলন না া ""</p>

<p>নমি ফতহসুকভাবল মদনার মুখলর দিকল তাকালাম া মদনা বললশ ,
"" নদীর ঢলফ গুণতল দিললও দারশযানজী তার থলকল টাকা কামানশর
পথ বলর করল ফললবলন া ""
"" তুমি য়া বলতল চাও তা঩ বলশ , মদনা া "" নমি যবার মনহতবহয় করি া
মদনা গমহভীর হযল বললশ , "" য঩ কলকাতা শহরল পযসা কামানশর কথা
হচহছল , সহয়ার া পযসা ছাডা মানুষলর যখানল কশনশ দাম নল঩ , সুতরাথ
পযসা কামা঩ করবলন না কলন দারশযানজী ? ""</p>

<p>বুঝলাম মদনা নসল পহরসঙহগল নসতল গিযল যকটু দহধিধা বশধ করছল া
নরও যকবার মাথা চুলকল মদনা পহরযশজনীয সাহস সঞহচয করলশ া
তারপর বললশ , "" ঠিক হহয়ায , পযসা তশমার য়ত খুশী পকলটল পশরশ া
কিনহতু সঙহগল সঙহগল মনল রলখশ ধনহয় লশকলরও পকলট যবথ পলট নছল া
দারশযানজীর ধারণা , ধনহয় সব লশকলর শুধু যকজশডা করল হাত নছল ,
বিনা পযসায ওর হুকুম তামিল করবার জনহয় া ""</p>

<p>"" বহয়াপারটা কি ? যত ফতহতলজিত হযল ফঠছশ কলন , মদনা ? "" নমি
যবার মদনাকল শানহত করার চলষহঠা করি া
মদনা যবার বললশ , "" সু঩পারলর লা঩ন নমি ননলক দিন হলশ ছলডল
দিযলছি , তা঩ া না হলল সহয়ার যক হাত নিযল নিতুম া ""</p>

<p>যবার মদনা জানলশ , তার রাগলর কারণ , দারশযানজী শুধু নানা সূতহর
থলকল঩ ধরহথশপারহজন করলন তা নয , নিচলর যকটি ছশটহট পাযখানাকলও
রশজগার বাডাবার কাজল বহয়বহার করছলন , ধথচ দরিদহর সু঩পারদলর
তিনি যকটি পযসাও দলন না া য঩ টযললট বহয়বহার করবার জনহয়
রিকশাওযালাদলর সঙহগল মাসিক টাকার বহয়বসহথা নছল া যবথ পুরশ টাকাটি
রামসিথহাসনজী নিজল 	পকলটসহথ করলন া</p>

<p>"" য তশ গলল থারহড কহলাস পহয়াসলঞহজারদলর কথা া "" বললশ মদনা া
"" ধনলক ফারহট কহলাস পহয়াসলঞহজারও নছল , সহয়ার া ""</p>

<p>সামানহয় বহয়াপারল যকাধিক শহরলণীর ফপসহথিতির ফলহললখ সহ঵ভাবত঩ নমার
কষতূহল বাডিযল তুললশ া টযললটলর ধনুসনহধানল দুরহভাগা তৃতীয শহরলণীর
রিকশাওযালাদলর দুরহভশগলর কথা না-হয বশঝা গললশ , কিনহতু য঩ ফারহসহট
কহলাশ য়াতহরী কারা ?</p>

<p>সতহয়ি কথা বলতল কী শাজাহান হশটলল ও থহয়াকারল মহয়ানসনলর য়া
ধভিজহঞতা ঩তিমধহয়ল঩ সঞহচয করলছি তাতল তৃতীয শহরলণীর মানুষদলর সমহবনহধল
নমার তলমন কশন সনহদলহ হয না , নমার য়ত চিনহতা য঩ ফারহসহট কহলাশ
লশকদলর নিযল া যরা নমার জীবনকল ঩তিমধহয়ল঩ কিছুটা ধসহনীয করল
তুললছলন , য়ত সমসহয়া তার শুরুতল঩ থাকলন য঩ ফারহসহট কহলাশলর লশকলরা া
সুতরাথ নমাকল মদনার ফলহললখিত ফারহসহট কহলাশ পহয়াসলঞহজার সমহবনহধল সজাগ হযল
ফঠতল হলশ া</p>

<p>মদনা নমার ফদহ঵লগ লকহষহয় করল হলসল ফঠলশ া সল বললশ , "" কিছু
ভাববলন না , সহয়ার া কলকাতার সব জিনিসলর মধহয়ল঩ থারহড কহলাশ , সলকলণহড
কহলাশ , ফারহসহট কহলাশ নছল া ঘশডার গাডিতলও য়দি ফারহসহট , সলকলণহড থারহড
কহলাশ থাকল , তাহলল নমাদলর য঩ থহয়াকারল মহয়নাসনলর কল-পাযখানায
কলন কহলাশ থাকবল না , সহয়ার ? "" //
সীমা বললশ , "" সব জলনল-শুনলও বৃদহধমানুষকল , বিশলষ করল নিজলর
বাবাকল ঠকাতল খুব কষহট হয , শথকর বাবু া কিনহতু সথসারল নমাদলর
মতশ মলযলদলর যছাডা ধনহয় কী ফপায নছল বলুন ? ""</p>

<p>সীমা যবার বশধহয হাসি দিযল কানহনা ঢাকার চলষহটা করছল া সল নমার
দিকল তাকিযল নিজলর চশখ নামিযল নিলশ া তারপর বললশ , "" বাবা
নমার সমহবনহধল য়া-য়া পরিকলহপনা করলছিললন নমি তার কশনশটা঩ পহরতিবাদ
করি নি া ফনি য়া-য়া বললছলন , নমি তাতল঩ হহয়াত বললছি া যবথ যকটা
বহয়াপারল নপনাকলও জডিযল যসলছি া ""</p>

<p>নমাকল সীমা তাহলল সতহয়ি঩ নিজলর দলশল ফিরল গিযলও সমহপূরহণ ভুলল
য়ায নি া নমাকল কী নর জডাতল পারল সীমা ?
"" বিনা ধনুমতিতল য঩ য়ল নপনাকল জডিযল ফলললছি তার জনহয়঩ কহষমা
চা঩তল যসলছি নজ , শথকরবাবু া "" সীমা কলমন করুণভাবল হঠাতহ কথা
বলতল শুরু করলছল া
"" নমাকল কী নর জডানশ সমহভব ? "" নমি বিমরহষভাবল঩ ফতহতর দিলাম া
"" নমার মতশ মানুষলর কহষমতা কতখানি তা সথসারল কারশ জানতল বাকি
নল঩ া ""</p>

<p>সীমা য঩ ধরনলর ফতহতর নমার কাছল থলকল পহরতহয়াশা করলছিল কিনা তা
নজও নমি নিদসনহদলহ ন঩ া সল নমার কাছল নরও যকটু ভরসা পলতল
চলযলছিল কিনা তাও নমি জানি না া</p>

<p>সীমাকল বললাম , "" নমি কিভাবল জডিযল নছি তা জানতল পারি কি ? ""
সীমা যবার বলশ লজহজা পলযল গললশ া য঩ লজহজাকলও হাসি দিযল ঢলকল
রাখার বহয়রহথ চলষহটা করলশ সীমা া যবথ কশনশ রকমল বললশ , "" বাবার
কাছ থলকল নমার বিযলর টললিগহরাম যলল , নপনার ঠিকানাতল঩ নসবল া
সীমা , কলযার ধফ শথকর , থহয়াকারল মহয়ানসন , কহয়ালকাটা সিকহসটীন া ""</p>

<p>সীমা যবার য়লন কলমন হযল য়াচহছল া হাসির মাতহরা যকটু বাডিযল দিযল সল
বললশ , "" কশন রকম দহধিধা করবলন না , শথকরবাবু া নমার নামল চিঠি ,
টললিগহরাম , পশষহটকারহড , ঩নলহয়াণহড ললটার , খাম য়া঩ নসুক নপনি কশনশ
সথকশচ না করল সঙহগল সঙহগল খুলল ফললবলন যবথ পডল ফললবলন া বুঝললন ?
কশনশরকম দহধিধা করবলন না া "" য঩ বলল঩ সীমা হলসল঩ চললশ া তার
হাসির বহরলকটা য়লন য঩ মুহূরহতল তার নিজলর঩ নযতহ঵লর বা঩রল চলল গিযলছল া</p>

<p>সীমার সময য়লন শলষ হতল চললছল - নবার শুরু সুললখার া নসনহন
সনহধহয়ার সহতিমিত নলশকল নমার হঠাতহ ভয হলশ , সীমাকল দূরল সরিযল
দিযল সুললখা঩ য়লন কহরমশ সহপষহট হযল ফঠছল া ধথচ সুললখাকল নমি চা঩
না - তাকল চিরকাললর নিরহবাসনল পাঠিযল সীমার মুকহতি হশক া কিনহতু সথসারল
নমার মতশ মানুষলর ঩চহছা-ধনিচহছার কী মূলহয় নছল ?</p>

<p>যবার য়ল নমাকল পহরশহন করছল সল সীমা ? না সুললখা ? সল নমাকল জিজহঞলস
করছল , "" মিষহটার জগদীশ জলঠমালানির কী খবর ? ভদহরলশক কল	কাতায
নল঩ নাকি ? ""
 +&gt;*
",0
</p></body></text></cesDoc>