<cesDoc id="ben-w-literature-novel-bnl10" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-literature-novel-bnl10.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>জলনানাফা</h.title>
<h.author>রানীচনহদ</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book.</publisher>
<pubDate>1983</pubDate>
</imprint>
<idno type="CIIL code">bnl10</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 0687.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-22</date></creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text><body>

<p>&lt;মলঘল ছাওযা নকাশ া ঘরলর ভিতর নবছা নলশ া জানালার ধারল বসল গলহপ
টুকছি বকাসুরলর া হঠাতহ য়লন ঝকঝকল নলশতল ভরল গলল ঘর-বারানহদা া মলঘলর
ফাতকল সুরহয়হয় দলখা দিল কহষণিকলর জনহয় া কী ফজহজহ঵ল নলশ , কী সুনহদর নর কী
ধদহভুত সহনিগহধ তীবহর া বারানহদায-পডা নলশটুকুর দিকল তাকিযল র঩লাম - তুলনা করলাম
ধনহয় দিনকার য঩ সমযকার রশদহদুরলর নলশর সঙহগল া হযতশ বা যমনি঩ া শুধু
নজকলর নলশকল সুনহদর করল তুললছল - তার মহিমা বাডিযলছল নকাশ-ছাওযা ধূসর
মলঘ া কহষণিকলর জনহয় ধরা দিল বলল঩ য নলশটুকু যত দুরহলভ হযল ফঠলছল !
বিকললল বাগানল পাযচারি করছি া মলটহরন ডলকল ডলকল কযলদিদলর বলললন , "" ন঩জ
তশদলর যকটা কাজ কমল রল া বাগানল জল দিতল হ঩ব না , বৃষহটি঩ দিল ন঩জ সল কাজ
ক঩রা া য঩ লগল নিম গাছলর পাতাগুলিও য়দি সব নিত ন঩জকার হাওযাতল ঝরা঩যা ,
বাতচতাম নমি া "" ন঩. জি. নসবলন ; বাগানল কশথাও শুকনশ পাতা যকটি পডল
থাকবার হুকুম না঩ া কযলকদিন ধরল ধনবরত নিমপাতা ঝরছল - যবারকার মতশ
তাদলর পালা শলষ কর নতুনকল জাযগা ছলডল দিচহছল া কিনহতু জললখানায শীত-বসনহত
ভতুর ধরহম মানল঩ বা কল নর বশঝল঩ বা কল ? জমাদারনি যকটা ঝাতটা দিযল পাতা-~
ঝরল-য়াওযা নিমপাতার সরু সরু কাঠিগুলি জডশ করল হাতল বলছল বলছল তুলতল লাগল া
জিজহঞলস করলাম , "" কী হবল য দিযল ? ""</p>

<p>সল বললল , "" বাবা নমার রশজ ভাত খলযল য঩ কাঠি দিযল রশজ দাতত খশতটলন া ""
জললারকল জমাদারনি ` বাবা ' বলল ডাকল া
ননহদিতা বললল , "" যত তশ ` বাবা বাবা ' কর , তা বাবার ছলললর বিযল গলল
কদিন নগল া যকদিন তশ ডলকল খাওযালশ না কযলদিদলর া ""</p>

<p>দারুণ মশা া সনহধহয়লর পরল বসল কিছু করবার জশ না঩ া ভন ভন করল নাকল কানল
ঢুকল য়ায া সুরাতন মশারির ভিতরল মাথা গলিযল যলার সঙহগল গলহপ করছল া বডশ
যকটা মশারির নীচল নমার ও যলার খাট পাশাপাশি া বললাম , "" পা-দুটশও ঢুকিযল
নাও সুরাতন , ন঩লল মশা ঢুকবল া কাল রাতল ঘুমাতল পারিনি মশার জহ঵ালায া ""
সুরাতন বললল , "" দু-যকটা মশাতল঩ য঩ কথা কন নর নমরা য়ল মশারি ছাডা
শু঩ ; সারারাত কত মশা কামডায - হাতল মুখল নর জাযগা না঩ , মশার কামডল ফু঩লহয়া
গলছল সরহবদলহ া "" বডশ লজহজা পললাম সতহয়ি঩ া</p>

<p>বিছানায ঢুকবার নগল রশজ঩ য঩ নমহবরটা যকবার ঘুরল য়া঩ - কল কী করল দলখি া
নজও গললাম া ঘুমনহত জলিলকল বুকল নিযল তার মা ডানহাতল মাথা ভর করল ফিস~
ফিস করল গলহপ করছল মিছিরনলর সঙহগল া স঳যদা চুল বলতধল দিচহছল সুরাতনলর া দিনল
নজ সময পাযনি া ধনহধকারল সিতথি কাটতল গিযল বারবার বলতকল য়াচহছল নর
সুরাতন ধমকল ফঠছল - "" সর সর , য়ত জহ঵ালা , নিজলর চুল নিজল঩ বাতধুম , তর
ধত সশহাগ দলখা঩তল লাগব না া "" শুনল স঳যদা মুখ কাতচুমাচু করল া চুলবাতধার কাযদা
ননাডি হাতল যখনশ ঠিক ধরতল পারলনি া কাছল গিযল হাতলর লণহঠনটা তুলল ধরলাম া
স঳যদা পরিপাটি করল নতচডল দলখলশুনল চিকণ সিতথল কাটল া রূপজান নমাজ পডছল া</p>

<p>যক-যকদিন পহরহরল পহরহরল সল নমাজ পডল া রসুন বসল বসল দু-হাতটুতল মুখ গুতজল
ঝিমশচহছল া তার পাহারা যখন া সারাদিন খাটুনির পর নর পারল না জলগল থাকতল া
বললাম , "" শুযল পড , তশমার বদলি নজ নমি পাহারা দি঩ া "" রসুন ঘাড নলডল
বললল , "" সল কি হয ? নিযম - নমাকল঩ ডলকল পাহারা জমা দিতল হবল া না হলল
ধপিসল কলস করল দলবল া ""</p>

<p>নমহবরলর ওদিকটায সকলল঩ পহরায ঘুমিযল পডলছল া শুধু সরলা বিছানায ফঠল
বসল গাযলর কাপড খুলল পাতজরা-বলর-করা বুকল হাত বুলিযল বুলিযল নপন মনল
মাখা নিচু করল কী য়লন দলখছল া কী ভাবছল সল঩ সঙহগল সল঩ জানল া যত রাতল য঩
নিরালায কশন বহয়থা য়ল ওকল নিরালা করল তুলল া</p>

<p>মহাতহমাজীর খবর : "" নতহমিক শকহতি বলল ধগহনিপরীকহষায ফতহতীরহণ হ঩বার শকহতি ধরহজন া ""
ভাবী বলললন , "" য রানীদি , দূরলর য঩ গাছটার মাথায কলমন ছাযা প঩ডাছল ?
নিচল জল নছল বশধ হয া ছাযাটি নডছল দলখশ া ""</p>

<p>বারানহদায বসল মলটহরন কাজলর হিসাব খাতায তুলছলন া ভাবীর কথা শুনল বলল
ফঠললন , "" নছল঩ গশ , ও঩ বটগাছটার নীচল যকটা পুসহকরিণী নছল া ""
রূপজান বললল , "" দলখি তশমার চশমাটা ? নদ - য়লন শীতল লাগছল চারদিক া
য়াবার দিন নমায দিযল য়লযশ যটা া ""</p>

<p>জামরিন তাডা করল , "" কিলা কদমমণি , তর য়ল দলওযালল চুনলর গশলা লাগানশ
শলষ঩ হয না া পুতলর বিযা লাগলশ না কি ? ""
কদমমণি দীরহঘনিশহ঵াস ছলডল বলল , "" নর বিযা - জলল-খাটা-মরা ছল঩লার কি নর
বিযা হ঩ব রল ? ""</p>

<p>বিমলা - ছশট জমাদারনি - নতুন যক জশডা জুতশ পলযলছল ধপিস থলকল া ফতচু
হিল-তশলা জুতশ া দলখল দলখল বিমলা বডস মাপলর জুতশ঩ নিযল যল া বলল কিনা ,
"" দাম য়খন যক঩ তখন বড দল঩খহয়া ননা঩ তশ ভালশ া ধনলক বলশি দিন চলব া
ছশট কিনহয়া লাভ কি ? ""</p>

<p>সারাদিন নজ বিমলা সল঩ জুতশ পাযল দিযল চলা ধভহয়াস করছল , নর থলকল থলকল
হুমডি খলযল পডছল া বিমলার মনল নানা ধশানহতি া জশযান ছললল কাজকরহম করল না ,
ঘরল বসল দু-বললা সমানল ভাত খায ; তাও কি কম-সম ? বিমলা বলল , "" য঩
যতটি না হ঩লল পশডারমুখশর পলট ভরল না া "" তার ফপরল মলযল পহরথম পশযাতি ,
জামা঩ পাঠিযল দিযলছল মলযলকল মার কাছল া বিমলার বিরকহতির শলষ না঩ া বলল , -
"" য়ত নপদ বালা঩ জশটল ন঩সহয়া নমার া মা঩যা বিযা দিযা নিশহচিনহত হ঩লাম ; নবার
কহয়ান ন঩সহয়া ঘাডল পডা া রাগল নমার শরীর ফা঩টহয়া পডল া কন কি গশ ? জামা঩
ন঩ছিল - যকটা কথাও ক঩না঩ মুখ ঘুরা঩যা া ""</p>

<p>সরলা বসলছল হাসপাতাললর ডাযলট সঙহগল নিযল া ভাতলর কিনারল যকটু নলু
বাতধাকপির ঝশল , যকটু ডাললর জল ও যকটু দুধ া করকরল ভাতগুলি নাডতল
নাডতল সরলা বলল , "" যকটু নুন নছল নি নপনাগশ কাছল ? দিবলন ? ""</p>

<p>পাশল জামিনা দাতডিযল ছিল া তাকল বললাম , "" ওকল যকটু নুন যনল দাও তশ
রানহনাঘর থলকল া "" সরলা বাত হাত নলডল বলল ফঠল , "" না , না , থাফক া ""
"" থাক কলন ? ""
"" না , কাফরল ক঩যা দরকার না঩ া কল নবার কালা মুখ ক঩রা ফঠব - সল঩
ফদহদিশল঩ বলল নমি ম঩঱হয়া য়া঩ া ""
জামিনা সরলাকল ভলথচি কলটল ছুটল রানহনাঘরল নুন ননতল া</p>

<p>সরলা কপাল চাপডল নাকল কাতদতল লাগল , "" হা ভগমান , য঩ খাওযা঩যা বলল শরীরল
শকহতি করা঩বা া য঩ খা঩যহয়ানি শ঱ীরল শকহতি হয ? মুখল দিতল পারি না দু঩ মুঠা ভাত ;
তরকারিতল না দলয যকটু হলদি , না দলয যকটু নুন মরিচ - মুখল সশযাদ লাগল না ;
পাতচ তরকারি দিযা পাচনসিদহধ ক঩঱হয়া রাখছল ! বানহদা কপির ডালনা কত সুনহদর হয মাছলর
মুডা দিযা া রশগল ভু঩গহয়া ভু঩গহয়া কিছু খা঩বার মন য়ায া য঩ দুধটুকু থাকল ব঩লহয়া
দু঩টহয়া মুখল দিতল পারি া তাও নজি বিকাল থল঩কহয়া বলল নমার হাসপাতাললর খাওযা
কাটা য়া঩ব া হ঩যা গলল শরীরল নমার শকহতি , দহয়াহলন না - "" বলল সরলা
হাত পা ঘুরিযল ঘুরিযল দলখায া পরল জশরল যকটা নিশহ঵াস নিযল কুত঩ কুত঩ করল ,
"" ভগমান রল ; তু঩ নমার কপালল যত দুদখও লল঩খহয়া রাখছিলি রল ! "" //
"" শুনললন - শুনললন ? ধদলর কি - তার পর ধরা মুরগি জবা঩ করবল , খানাপিনা
করবল - হযল য়াবল ; ঘরল ঢুকবল া "" তারপর কী কথা মনল হতল কদমমণি শিফরল
ফঠল ; বলল , "" নজ য়া রূপজান মুরগি জবা঩ করল কী নর বলব ? কাটতল হয
যক কশপল কাট - নমরাও তশ তা঩ করি ; ধরা তা না া জামাটা শুকশতল দি঩ছিলাম
বারানহদায , বাতাসলর দাপটল পডল গিযা নরহদমায া ভাবলাম - বললা তশ নছল যখনশ
খানিকটা ; তাডাতাডি ধুযল দিযল শুকশললও শুকশতল পারল া তা঩ না ভলবল জামাটা নিযল
কলতলায গলছি - দলখি - রূপজান দাতডিযল নছল সামনল ; নর স঳যদাকল বলছল মুরগি
জবা঩ করতল া স঳যদা দাতডিযল বাত-হাতল মুরগির মাথাটা ধরল ডানহাতলর বতটি দা দিযল
যক পশতচ দিযল দিযলছল ছলডল - নর মুরগিটা কতকহ কতকহ করল দুটশ পাখা ঝাপটাতল
ঝাপটাতল খানিকটা ফপরল ফডল গিযল ধপাস করল য঩ভাবল মাটিতল পডল গলল - "" বলল
কদমমণি মুরগি মুখ থুবডল পডল য়াওযার ভঙহগিতল নিজল মলঝলতল মুখ গুতজল লুটিযল
পডল া</p>

<p>ভুরু কুতচকল রূপজান বললশ , "" মশটল শশন া ""
রুপজানলর যকটা ধভহয়লস - গুরুতর কিছু বিষয নলাপ করতল হলল঩ "" মশটল
শশন "" বলল সল সামনল যসল কথাটা পাডল া রূপজান হাতটুর ফপর কাপড টলনল তুলল দু঩
ফরুর মাঝল গুতজল পা মললল বসল ; বললল , "" তা নমাদলর দসহতুর মতশ কাজ করব তশ ?
নমি হাজির বলটি া নজ না হয জললখানায যসল নমার কিছু঩ নল঩ ; তা বলল ধরহম
তশ নষহট হয না঩ া স঳যদা , সুরাতন যরা সব চহয়াথডা ছুকরি - তারা কী জানল ? তা঩
নজ সামনল গিযা দাতডানু ; বলনু , দলখ , য঩ভাবল কর া দাতডিযল মনহতহর পডনু -
"" লা-যলা-হা-঩লহলালহ-লা-হশ , মশহামহদূর-রসু-ললহলা-হশ া "" স঳যদা মুরগিটার গলায
পশতচ লাগিযল ছলডল দিল া মুরগিটা মাটিতল পডল ধডফড করছল - তখন যকটা বাটিতল
জল নিযল মনহতহর পডল ফুত দিনু - "" কুলহ-হশ-ওযালহলা-হশ-নহাদ , নলহলা-হশ-সামদহ-লাম-
঩যাললদ - ওযালাম ঩ওলাদ ওযালাম঩যা-কুললা-হশ , ফশফশওযান নহাদ া "" শলষল সল঩
জলটা মুগির ফপর ছিটিযল দিবা মাতহর তার পরানটা সুসহতির মতশ বাহির হ঩যা গলল ;
মুরগিটাও তখন ` পাক ' হ঩যা গলল , ` ধপাক ' র঩ল না া তবল গিযা নমরা সল঩
মুরগিটা খা঩তল পারি া নয তশ তশমাদলর মতশ কাটনু নর খা঩নু - য নমাদলর
ধরহমল না঩ া ""</p>

<p>"" - য়াচহছ কশথায ? যকটু মুখল দলবল না য ? "" বলল রাধা যগিযল যল - সুজি ,
কলা , শাতকালুর ভাগ নিযল া হাত পলতল তা নিযল মুখল পুরলাম া নটার মতশ
দাততলর গশডায ললগল র঩ল সুজিটুকু া</p>

<p>বানু নজ কলবল঩ হাসছল - খিকহ খিকহ - ফঠতল বসতল , কথায গলহপল , কারণল
ধকারণল া চালান য়াবল ধনহয় জললল শিগহগির঩ া খবর যসলছল া জলল হযল ধবধি
যখানল নছল মাযল-ঝিযল - নট বছরলর ফপর া য঩বার যকটু বা঩রলর মুখ দলখবল-
রললগাডিতল চডবল - হযতশ রাসহতায যটা-ওটা কিনল খলতলও পাবল া নননহদ হয ব঩কি
যকটা া যক জলল থলকল ধনহয় জললল চালান হবার পথটুকু য়লন বিসহমযল , সষনহদরহয়হয়ল,
মাধুরীতল ভরল থাকল ; নর নতঙহক থাকল য঩ বুঝি পথলর শলষ হয বা া য঩
কথাটা঩ যকদিন ফচহছাসভরল বললছিলাম মিলনী-মাকল া "" বলুন ঠিক যমনি হয
নাকি মনল ধবসহথা ? "" মিলনী-মা নানা রঙলর ধগশছালশ রঙীন সুতশ যকটি যকটি
করল গুছিযল দু-পা মললল বসল পাযল জডাচহছিললন া জললখানার খাটুনি া মুখ নিচু
করল করুণ হলসল বলললন , "" মনলর ধবসহথা কী হয তা নমরা঩ জানি ; তশমাদলর
কথা নলাদা া নমরা য়খন চালান হ঩ - সঙহগলর জমাদারনি নসলপাশলর সবা঩কল
শশনাতল শশনাতল নসল - য যত বছরলর নসামি ধমুক কাজ করলছল , ধমুক কলস
তার হযলছল - কত কী ! নর সবা঩ শুনল মুখলর দিকল তাকায া লজহজায তখন মরল
য়া঩ া টহরলনল ফঠল যকপাশল কমহবল বিছিযল তাডাতাডি নাক-মুখ ঢাকা দিযল শুযল
পডি া</p>

<p>কযলদিদলর খাওযা শলষ হযল গলছল া য়ল-য়ার থালা বাটি কলতলায মলজল ধুযল পরিসহকার
করছল া নমরা সনহধলর পর খা঩ া রসুন নমাদলর খাবার ` নমহবরল ' নিযল ঢাকা দিযল
রলখল খলতল বসল নিজল খাটুনি-ঘরল া নমরা য়ল-য়ার বলডাচহছি যদিক ওদিক সরু সরু
রাসহতাগুলি ধরল া হটাতহ শুনি খাটুনি-ঘরল দারুণ চলতচামলচি া মলটহরন পহরাণপণ শকহতিতল
মুখল য়া নসছল তা঩ বলল রসুনলর মাথার ফপরল বারল বারল ঝুতকল পডল গালাগালি
করছলন া ভাব দলখল মনল হয য়দি দু-ঘা বসাতল পারতলন তবল বশধ হয শানহত হতল
পারতলন া কী বহয়াপার ? রাগল ও বকাবকিতল মলটহরনলর ঠশতটলর দু-পাশল ফলনা জমল গলছল া
মলটহরন কাগজলর পলতটলাতল কযলকটা শুকনশ লাল লঙহকা দলখিযল বলললন , "" দহয়াখলন
ন঩সহয়া , য঩ মরিচগুলি য঩ চশরনি কযলার ছালার নীচল লুকা঩যা রখছিল া ন-লশ ,
য঩গুলি তু঩ তর ধমুকলর বাডি ল঩যা য়া঩বি - না-লশ ? "" বলল নবার গলললন রসুনলর
ফপর রুখল া</p>

<p>নমাদলর রানহনা ঘরল য়ারা কাজ করল রসুন তাদলর঩ যকজন া ধনহয়রা বলল রসুন
নাকি পহরায লুকিযল লুকিযল লঙহকাটা পলতযাজটা জমাদারনিকল দলয া
জমাদার নসবল যখুনি া তার নগল হাত পা ধুযল নসি কলতলা থলকল া
খাটুনি-ঘরলর কশনায দাতডিযল দলখি া জমাদারনি শুকনশ হাসি হলসল রূপজানকল
হাত নলডল নলডল পরিমাণ দলখিযল বলছল - "" নমার ঘরল যত লঙহকা , যত রাসুন -
সাডল তিন সলর ডা঩ল - ছয মাসলর মতশ মজুত নছল সব া দু঩টা লঙহকা নিতল য়ামু
নমি - ফুদ া ""</p>

<p>দাজুর জহ঵র া বশধ হয মহয়াললরিযা া মাথায জলপটি দিযল গঙহগা পাখার হাওযা করছল
পাশল বসল া দাজু ঘুমশচহছল বলহুতস হযল া কী মশা , কালশ হযল চারদিক থলকল
মশা ঘিরল ধরলছল গঙহগাকল া গঙহগা পাখা দিযল সলগুলিকল তাডাতল য়ায , তারা যসল ঘিরল
ধরল দাজুকল া</p>

<p>ব঩ নিযল বিছানায ঢুকলাম া যক পাতা পডতল না পডসতল মাথার কাছলর লণহঠনটা
কহষীণ হযল যল া তলল ফুরিযল যসলছল া সল঩ কহষীণ নলশ কহষীণ হতল হতল যক সমযল
তা নিভল গলল া ব঩ বনহধ করল বালিশলর নিচল রলখল পাশ ফিরল চশখ বুজলাম া</p>

<p>ললবুতলা া সহনান করল যসল চুল নতচডাচহছিলাম নর ভাবছিলাম য঩ ধলহপ
পরিসরল ধতি ধলহপ জিনিস নিযল য঩ য়ল জীবনয়াতহরা চললছল ; দু'খানি রুমাল , যক~
খানি ধুযল রশজ জানলার ফপর টাঙানশ সরু দডিটায ঝলডলঝুডল মললল দি঩ , ধনহয়টি
ভাতজ করল কশমরল গুতজি ; যকটা শাডি পরি , নর যকটি শুকিযল পাট করল বালিশলর
তলায রাখি - কাল পরতল হবল ; জামা যকটা ছিতডলল঩ তখন঩ বসি রিপু করতল ;
জশরল সাবান ঘসি না পলটিকশটল - ছিতডল য়ায পাছল ; ডুরল-কাটা লাল গামছাটা হঠাতহ
যকদিন বডশ মনল হল , কলটল দুটশ গামছা বানালাম া তুলশর পহয়াকলট জডানশ কাগজ~
টুকু , চাযলর পহয়াকলটলর রাথতাটুকু , টুথ-বহরাসলর কাগজলর বাকহসটা , পাযরার পালকটা
হাতলর কাছল য়া পা঩ য়তহনল তুলল রাখি - কী জানি য়দি কখনশ কাজল লাগল া ভাবছিলাম
তা঩ য়ল মন সথকীরহণ হযল য়াবল না তশ শলষল ? //
সল঩ সু঩ট-পিতল ফুল যসলছল , কুতডি ধরলছল া যখানল যরা সু঩ট-পিকল বলল মটর
ফুল , পিটশনিযাকল বলল কলমি ফুল া মিছরিন বলল , "" কত঩ দলখলাম জললখানায
ন঩সহয়া , না হ঩লল যও নি নবার শখ ক঩঱হয়া বাগানল ন঩নহয়া লাগায মানষল া নমাগশ
খলতল খামরল কত রাশি রাশি ফু঩টহয়া থাকল য঩ ফুল া ""</p>

<p>গশবর দিযল নিকানশ বললফুল গাছলর গশডায জল ঢললল রলখলছল দুপুর থলকল঩ ,
য়ার পালি ছিল জল দলবার া সল঩ জলল সহনান করছল দুটি শালিক , পাখার জল চার~
দিকল ছিটিযল দিযল া যকবার করল মাথাটা জলল ডশবাচহছল , বুকল ভর দিযল পাখার
নধখানা ভিজিযল নিচহছল , নবার ফঠল দাতডিযল গা থলকল জল ঝাডা দিযল ফললছল ;
ঠশতট দিযল বুক খুতটছল , পালক খুতটছল , কিচিমিচি ডলকল ফঠছল নর চুলবুল করল চার
দিকল তাকাচহছল া তাডাহুডশর ধনহত নল঩ যদলর া</p>

<p>সহনান সারা হলল তারা ফডল যসল বসল নিমগাছলর ডালল া ডাললর ডগায ডগায
লাল লাল কচি পাতা া ভাবী যসল বসললন পাশল া নিমগাছলর দিকল মাথা তুলল
বলললন , "" হারল রানীদি , য঩রকম লাল লাল নিমপাতা নমাদলর দলশল ডাললর
বলসন দিযল যকটু তলল মিশিযল ভাজল া খলতল খুব সশযাদ লাগল া করব যকদিন ;
খাবা ? ""</p>

<p>কাল রাতল সহ঵পহন দলখলুম , দলল দলল শকুন ফডল য়াচহছল মাথার ফপর দিযল পহরকাণহড
দু঩ ডানা দু-দিকল মললল দিযল া বুকল , ঠশতটল , পালকলর তলায পডলছল তাদলর
সুরহয়হয়াসহতলর নভা ; নীচল দাতডিযল নমি বলছি , কী সুনহদর , কী সুনহদর !</p>

<p>মাযা শুনল ফতহসাহ দিযল বলল , "" লিখল রাখুন সব সহ঵পহনগুলশ তারিখ দিযল দিযল া
জানলন , পাগল য়ারা হয তাদলর মানসিক চিকিতহসার খুব সুবিধল হয সহ঵পহনগুলি জানা
থাকলল া ""</p>

<p>যলা বললল ,"" কলন মিছল মন খারাপ করছ া শকুন সহ঵পহন দলখা মশটলও খারাপ নয া
চিল দলখ রশজ ফডছল , সহ঵পহনল না হয শকুন দলখলছ ; কি হযলছল তাতল ? ""
শ঳লদি বলললন , "" ভালশ঩ দলখলছলন , সহ঵পহনল য়ল কশন পাখি দলখা-঩ ভালশ া ""
সুষমাদি বলললন , "" সহ঵পহন সহ঵পহন঩ া যর জনহয় নবার ভাবছলন কী ? ও সতহয়িও না ,
মিথহয়াও না া ""</p>

<p>তবুও মন মানল না া ফষাদি নাকি সহ঵পহনবৃতহতানহত জানলন ? তাতকল গিযল জিজহঞলস
করলাম , "" সব ছলডল শলষল শকুন কলন দলখলাম বলুন তশ ? ""
ফষাদি খানিককহষণ গমহভীর হযল ভাবললন , পরল বলললন , "" তার মানল , নপনার
কলফ মারা য়াবল শিগহগির঩ া ""</p>

<p>দুটশ বড বড দাতডকাক সামনলর বললফুল গাছ থলকল কচি কচি পাতা সুদহধ
সরু সরু ডাল তাদলর শকহত ঠশতট দিযল মটমট করল ভলঙল নিযল ফডল গলল া
মুণহডাদলর ছলললটা বসতল পারল না মশটল঩ া কশমর থলকল নিচলর ধথশগুলি
লিকলিক করল ঝুলছল য়লন শরীরল া মাথাটা টাল খায সারাকহষণ ডা঩নল বাতযল , কশন
মতল ধরল বসিযল দিলল পলটটা ফঠল নসল গলার কাছল , মাথাটা ভলঙল পডল পলটলর
ফপরল , তারপর টলল পডল য়ায সামনলর দিকল মুখ থুবডল া তিন ছললল মারা য়াবার
পর য঩ ছললল া ধতি নদরলর ; মা-দিদিমার কশলল কশলল঩ থাকল া মলটহরন রশজ঩
চলতচামলচি করলন , ছলললটার জনহয় দুধ বারহলি দলওযা হয , তা মা দিদিমা঩ খলযল নলয ;
বলল , ছললল নাকি দুধ খলতল চায না যকলবারল া মার বুকলর দুধ শুকিযল গলছল
কশন কালল া সল঩ ধবধি ছলললর গশটা গশটা ভাত খলযল঩ ধভহয়লস া ভাতলর জনহয়
কাতদল সারাকহষণ - মহয়াত-য-য-মহয়াত-য-য া নলছা বলল , "" ছল঩লাটার কাত঩দবারও শকহতি
না঩ গশ া ""</p>

<p>মলটহরন নজ জশর করল কযলক চামচ দুধবারহলি ঢললল দিযলছিললন ছলললটার গলায া
কাতঠালতলায দিদিমার কশলল বসল সল নিশহ঵াসলর সঙহগল ফগরল ফলললছল দুধবারহলি ; নর
ছলললর মা দিদিমা বিড বিড করল বকল চললছল সমানল মিটিন রূপজানকল া ছলললটার
মাথার পিছন দিকল ঘাড ধবধি থকহথকল ঘা া মিটিন ডাকহতারলর দলওযা শাদা মলম
লাগিযল দিযলছল তাতল নিজলর মা দিদিমার নপতহতি থাকা সতহতহ঵লও া সল঩ শাদা
মলম঩ ঘাডলর দিক থলকল মাথার ভিতরল ঢুকল যখন মুখ দিযল বলর হচহছল া দিদিমার
রাগ নর থামল না ; কাতঠাল পাতা দিযল চলতছল চলতছল ঘাযলর মলম তুলল ফললল তবল
নিশহচিনহত া</p>

<p>যলা যসল বললল , "" দলখলছ বফদি , পুবকশণার ললবুগাছল টুনটুনীতল কী সুনহদর
বাসা বলতধলছল া কিনহতু কী জানশ , টুনটুনীর বাসা যমন হয না ; দুটি পাতা বলশ
সললা঩ করার মতশ জশডা দিযল ফপরল নর যকটি পাতা ঢাকনার মতশ দলয া য য়লন
বাবু঩ পাখির বাসার মতশ ধনলকটা া "" বলতল বলতল দু'জনল঩ যগিযল গললাম ললবু
গাছটার দিকল া যকটি ছশটহট পাখি তার ছশটহট ঠশতটটি বলর করল বসল নছল বাসার ভিতরল া
নমাদলর দলখল ফ঱হ঱হ করল ফডল গলল বাসা থলকল া যবারল নরশ কাছল যগিযল
গললাম া কী সুনহদর বাসাটি া নমগাছলর গাযল লাল পিতপডলর বাসাগুলি ঝুলছিল
লমহবা লমহবা ` শিবলর জটা ' ফুললর মতশ , তা঩ দিযল ঝুলিযলছল নিজলর বাসাটি ললবুগাছলর
সঙহগল া চরকা-কাটা ছলতডা সুতশ যনল জডিযলছল তার গাযল গাযল চারদিক ঘিরল া
শুকনশ ঘাস পাতাও নছল দু-চারটি া বাসার দরজার মুখল খানিকটা চাল যগিযল
দিযলছল , বৃষহটির জল য়াতল না য়লতল পারল ভলতরল া চালটা ঠিক য়লন মুদির দশকানলর
সামনলকার ঝাতপ যকটি , হুবহু তার঩ নকল া বাসার ভিতরল পাতা নলটলর মতশ শুকনশ
ধশহ঵থহথ পাতা া কী নিখুতত পরিপাটি কাজ ঢ! ছশটহট পাখির ছশটহট ঠশতটল যমন কাজও
সমহভব ? তনহময হযল দলখছি নর ভাবছি া লকহষহয় করিনি মাথার ফপরল বাসার মালিকলর
ধসহথির গতিবিধি , ধধ঳রহয়হয়লর ছটফটানি া সরল যলাম তাডাতাডি সলখান থলকল - মনলর
মাঝল বাসা বাতধার কারুকাজটুকু গলতথল নিযল া</p>

<p>সাবিতহরীদি বলললন , "" ও তশ মষ-টুসকি , নমাদলর দলশল নছল ধনলক া ""
য়মুনা ছাডবল না কিছুতল঩ ; তাস খললতল হবল঩ া বসল গললাম খললায ; যকদিকল
নমি নর মাযা , ধনহয় দিকল ননহদিতা নর য়মুনা া</p>

<p>ধশপা যসলছল কাপড নিতল , মাযা঩ ফি-বার হিসলব মিলিযল কাপড দলয নলয া
খললা জমল ফঠলছল , মাযাকল য সময ছাডা য়ায না া ভবানী঩ মাযার হযল হিসলব
লিখল কাপড বুঝিযল দিল ধশপাকল া মাযার সশযাসহতি নল঩ মনল , য়ল কাজলর ভার সল
নিজল নিযলছল , সল কাজ কি নিশহচিনহত মনল ছাডা য়ায ধনহয়লর ফপরল , কলবল সল ফসখুসহ
করছল , "" কাজটা কি ভালশ হল ? দশ মিনিটলর বহয়াপার ব঩ তশ নয া নপনাদলর
য়লন য়ত সব ঩যল - "" ভবানী ঘরল ঢুকতল মাযা বললল , "" ঠিক মতশ মিলল তশ সব ? ""
ভবানী বললল , "" সব঩ ঠিক হল , তবল নপনার নধখানা বডিস নিতল কিছুতল঩ রাজী
হচহছিল না ধশপা া য়দি বা নিল , বললল , "" ঠিকমতশ ফিরল পাবলন কিনা জানি
না া ""
 +&gt;*
",0
</p></body></text></cesDoc>