<cesDoc id="ben-w-bengalnet-news-01-06-22" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-06-22.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-06-22</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-06-22</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>পাঁচ বছর
প্রেসিডন্ট থাকার ইঙ্গিত মুশারফর</p>

<p>২০০২ সাল সাধারণ
নির্বাচনর প্রতিশ্রুতি দিলও টানা পাঁচ বছর পাকিস্তানর প্রসিডন্ট থাকত চান মুশারফ।
সরাসরি এ ব্যাপার কিছু জানান না হলও নিজর ঘনিষ্ঠদর মাধ্যম এই বার্তা পৗঁছ দিত
চাইছন নতুন প্রসিডন্ট। এরই পাশাপাশি, পশ্চিমী দশগুলির সমালাচনাক নস্যাত্ কর দিয়
পাকিস্তানর অর্থমন্ত্রী শওকত আজিজ বলছন, যা হয়ছ তা পাকিস্তানর স্বার্থই। বর্তমান য
সংস্কার প্রক্রিয়া চলছ তা অব্যাহত রাখতই এর প্রয়াজন ছিল। সংস্কার প্রক্রিয়া য ২০০২ সালর
পরও চলব তাও দ্ব্যর্থহীন ভাষায় জানান আজিজ। মুশারফর ব্যক্তিগত মুখপাত্র রশিদ কুরশির
বক্তব্য আরও প্রাঞ্জল। তিনি বলছন, পাকিস্তান প্রসিডন্ট পদর ময়াদ পাঁচ বছর। মুশারফর
ক্ষত্রও তার ব্যতিক্রম হব না। </p>

<p>রাজনৈতিক মহলর
ধারণা, প্রতিশ্রুতিমত আগামী বছরর অক্টাবরর মধ্যই মুশারফ নির্বাচন করবন। তব সই ভাট
হব তাঁর প্রত্যক্ষ নিয়ন্ত্রণ। ভাটর পর য অ-সামরিক, গণতান্ত্রিক সরকার প্রতিষ্ঠিত হব তার
চাবিকাঠি নিজর হাতই রাখত চাইছন পাকিস্তানি প্রসিডন্ট। 

</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p> </p>

<p>মুশারফক
আগাম 'প্রেসিডন্ট' বল বিতর্ক বাজপয়ী</p>

<p>
 
 
<gap desc="illustration"></gap>জার কর রাষ্ট্র ক্ষমতা দখল করায় গাটা বিশ্ব যখন পরভজ মুশারফর সমালাচনায়
মুখর তখন প্রধানমন্ত্রী বাজপয়ী তাঁক প্রসিডন্টর স্বীকৃতি দিয়ছন। ঘটনাটি জানাজানি হয়
যাওয়ায় কূটনৈতিক স্তর যথষ্ট অস্বস্তিত পড় গছন অটল। বিদশমন্ত্রক গাটা ব্যাপারটা
ধামাচাপা দিত চাইছ। এক সরকারি মুখপাত্র বলছন, এর মধ্য অন্য অর্থ খাঁজা অনুচিত।
প্রধানমন্ত্রী মুশারফক আগাম অভিনন্দন জানিয়ছিলন মাত্র। </p>

<p>সবচয় মজার কথা
হল, বাজপয়ী যখন মুশারফক 'প্রসিডন্ট' বল সম্বাধন করন তখনও শপথ গ্রহণ হয়নি।
২০ জুন সকাল পৗন এগারাটায় দিল্লির বাসভবন থক বাজপয়ী হটলাইন ইসলামাবাদর সঙ্গ যাগাযাগ
করন। তাঁর প্রথম কথাই ছিল : 'সুপ্রভাত, মিস্টার প্রসিডন্ট।' কিছুটা অবাক হয়
মুশারফ বলন, 'আমি তা এখনও প্রসিডন্ট হইনি।' পাকিস্তানি সংবাদমাধ্যম ঘটনাটি
ফাঁস কর দিয়ছ। </p>

<p>শুধু প্রধানমন্ত্রী নন,
রাষ্ট্রপতি কে আর নারায়ণনও বৃহস্পতিবার পাকিস্তানর প্রসিডন্টক শুভচ্ছাবার্তা পাঠিয়ছন।
এই বার্তায় রাষ্ট্রপতি বলছন, 'আমাদর আশা, আগামী মাস আপনার সফর পাক-ভারত
সম্পর্কক আরও দৃঢ় ও ইতিবাচক করব। আপনি আমার শুভচ্ছা নবন।' </p>

<p>বিশ্বর প্রায় সব দশ
যখন মুশারফর সমালাচনায় মুখর, তখন প্রধানমন্ত্রী কন তাঁক আগবাড়িয় 'প্রসিডন্ট'
বললন, তা নিয় যথষ্ট বিতর্ক দখা দিয়ছ। সরকারিভাব বিদশমন্ত্রক কী প্রতিক্রিয়া জানাব
সটা বুঝ উঠত পারছ না। আসল কারণ হল : শীর্ষ সম্মলনর দিক তাকিয় দ্বিপাক্ষিক সম্পর্ক
নষ্ট হয়- এরকম কিছু করত ভারত নারাজ। তাই মুশারফর সিদ্ধান্ত পাক গণতন্ত্রর পক্ষ বিপজ্জনক
এটা বুঝও নয়াদিল্লিক হাত-পা গুটিয় বস থাকত হচ্ছ। 

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>যুদ্ধর সময়
নেই, কাজ করত হব : মুখ্যমন্ত্রী</p>

<p>রাইটার্স সরকার আর
মাঠ-ময়দান সংগ্রাম- দুই-ই একসঙ্গ চালাবন বল ঘাষণা করলন মুখ্যমন্ত্রী বুদ্ধদব ভট্টাচার্য।
বৃহস্পতিবার সন্ধ্যায় নতাজি ইন্ডার স্টডিয়াম 'বামফ্রন্ট সরকারর ২৫ বছর পদার্পণ'
উপলক্ষ আয়াজিত এক সমাবশ মুখ্যমন্ত্রী বলন, বিরাধীদর সঙ্গ যুদ্ধ করার মত সময়ও আমাদর
হাত নই। আমাদর লড়াই দারিদ্র ও বকারত্বর বিরুদ্ধ। প্রাক্তন মুখ্যমন্ত্রী জ্যাতি বসু বলন,
মানুষর জন্য কাজ করত পরছ বল একটানা ছয়টি নির্বাচন বামফ্রন্ট জয়ী হয়ছ। কাজর এই
ধারা অব্যাহত রাখত পারল আরও বহু বছর বামফ্রন্ট সরকার থাকব। অনিল বিশ্বাস, দেবব্রত বন্দ্যাপাধ্যায়,
মঞ্জুকুমার মজুমদার, অশাক ঘাষ প্রমুখ ভাষণ দন। পৗরাহিত্য করন বিমান বসু। </p>

<p>বুদ্ধদব ভট্টাচার্য য
কতটা জনপ্রিয় হয় উঠছন তার প্রমাণ মল ভাষণর সময় টানা হাততালির মধ্য। বুদ্ধদব বলন,
মানুষর সুস্পষ্ট রায়র পর আমাদর দায়িত্ব আরও বড় গছ। কবল মহাকরণ সরকার চালালই
হব না, দাবি আদায়র জন্য সংগ্রাম করত হব, পথ নামত হব। বিরাধীরা যুদ্ধ চাইলও
ওঁদর সঙ্গ লড়ার সময় আমাদর নই। আমাদর সংগ্রাম দারিদ্র ও বকারত্বর বিরুদ্ধ। মানুষ য
দায়িত্ব দিয়ছন, তা পূরণ করা খুব কঠিন। তা সত্ত্বও আমাদর এটা পারতই হব। </p>

<p>মুখ্যমন্ত্রীর প্রশংসায়
পঞ্চমুখ ছিলন জ্যাতি বসু-ও। তিনি বলন, 'মানুষ বুদ্ধক দারুণভাব গ্রহণ করছন।
বামফ্রন্টর শরিক দলর নতা থক আমলা পর্যন্ত সকলই ওক পছন্দ করন। এসব দখ আমার
খুব আনন্দ হচ্ছ।' 

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>ব্যর্থতার
জন্য দায়ী অতিরিক্ত একদিনর ম্যাচ 
চালু
হোক রোটশন পদ্ধতি : সৌরভ</p>

<p>
 
 
<gap desc="illustration"></gap>আমরা বুঝি একটু দরিত। সৗরভ গঙ্গাপাধ্যায়ও বুঝলন। ঘনঘন একদিনর
ম্যাচ খলাতই নাকি যত বিপত্তি। এ জন্যই তিনি রান পাচ্ছন না। আবার স্বীকার করলন,
'একদিনর ম্যাচ থক প্রচুর অর্থ আস। এটা না খল উপায় নই।' তিনি ঘনঘন একদিনর
ম্যাচ থক রহাই পাওয়ার পথ বাতলছন। বলছন, অস্ট্রলিয়ার মতা রাটশন পদ্ধতি চালু
করল খলায়াড়রা বিশ্রাম পাবন। 'একদিনর ম্যাচ খল টস্ট মানিয় নওয়া কঠিন'-
এও বলছন ভারত-অধিনায়ক সৗরভ। জানান, দুটা খলা একবারই আলাদা। </p>

<p>সৗরভর সমস্যার
সমাধানর জন্য এগিয় এসছন কাচ জন রাইট। তিনি অধিনায়কক ধৈর্য ধরার উপদশ  দিয়ছন।
এত সৗরভর আত্মবিশ্বাস বড়ছ। এবং এখনও বলছন, মন্দ অধ্যায়টা দ্রুত কট যাব। তিনি
আরও ১০-১২ বছর খলত চান। 'তব তার জন্য একদিনর ম্যাচ নিয় অন্য চিন্তা করত হব',
মন্তব্য সৗরভর। রাটশনর ব্যাপারটা তিনি সকলর জন্য প্রয়াগ করত বলছন। কারণ একদিনর
ম্যাচ প্রধানত জনা চারক ব্যাট করন। বাকিরা কয়ক ওভার। তিনি ২০০৩-এর বিশ্বকাপ ক্রিকটর
কথা মাথায় রখই রাটশন পদ্ধতি চালু করার কথা বলছন। এখন থকই ১৬-১৭ জন ক্রিকটারক
বছ নিয় রাটশন পদ্ধতি শুরু করত চান সৗরভ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>শ্রমিক
মৃতু্যর বিচারবিভাগীয় তদন্ত চাইলন মমতা</p>

<p>গ্যাঞ্জস জুটমিল
পুলিসর গুলিত শ্রমিক মৃতু্যর ঘটনার বিচারবিভাগীয় তদন্ত চাইবন তৃণমূল নত্রী মমতা বন্দ্যাপাধ্যায়।
বৃহস্পতিবার তিনি বাঁশবড়িয়ায় গিয় মৃত শ্রমিকর পরিবারক সমবদনা জানান। গুলিত আহত
শ্রমিকদর দখত চুঁচুড়ার ইমামবাড়া হাসপাতালও যান তৃণমূল নত্রী। মমতার সঙ্গ ছিলন
আই এন টি ইউ সি নতা সুব্রত মুখাপাধ্যায়। মমতা বলন, সিপিএম এখন আর শ্রমিক-দরদী নই।
তাই পুলিসর গুলিত শ্রমিক মারা যাচ্ছন। বিচারবিভাগীয় তদন্তর পাশাপাশি রাজ্য সরকারর
কাছ ক্ষতিপূরণর দাবিও জানিয়ছন মমতা। </p>

<p>বিকল সাড় তিনট
নাগাদ মৃত শ্রমিক সামশ্বরর বাড়িত পৗঁছন মমতা। নিউ জুটমিল কলানির সঙ্কীর্ণ রাস্তায়
তখন তিলধারণর জায়গা নই। সরু গলির মধ্য দিয় ভিড় ঠল কানওক্রম এগাত থাক
তৃণমূল নত্রীর ১৫ গাড়ির কনভয়। মমতাক দখ মৃতর মা ভাগীরথী দবী এগিয় যান তাঁর দিক।
কাঁদত কাঁদত মমতার হাত ধর বলন, 'পুলিস যখন আমার ছলটাক মর ফলল, তখন
আমাকও গুলি করুক।' তাঁক সান্ত্বনা দিয় মমতা বলন, পুলিসর গুলি চালানার প্রশিক্ষণ নই।
তাই ওদর গুলি শ্রমিকর বুক লাগ। </p>

<p>চটকল চত্বর মমতার
সভার জন্য মঞ্চ বাঁধা ছিল। কিন্তু এলাকায় ১৪৪ ধারা জারি থাকায় সভা করত রাজি হননি
মমতা। এদর স্হানীয় তৃণমূল নতারা হতাশ হন। ভিড়র মধ্য থক দুই-একটি কটূক্তিও ভস
আস। 

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p> </p>

<p>পূর্বাভাস
: আকাশ থাকব
মেঘ ঢাকা। সঙ্গ দু-এক পশলা হাল্কা বৃষ্টির সম্ভাবনা। দিনর সর্বাচ্চ তাপমাত্রা ৩২ ডিগ্রি
সেলসিয়াসর কাছাকাছি থাকব। </p>

<p>তাপমাত্রা : বৃহস্পতিবারর সর্বাচ্চ ৩৩.৬ (স্বাভাবিক) এবং সর্বনিম্ন ২৬.৬ (স্বাভাবিক)ডিগ্রি সেলসিয়াস। </p>

<p>আপক্ষিক আর্দ্রতা
: সর্বাধিক
৯৭ শতাংশ এবং নূ্যনতম ৭০ শতাংশ। </p>

<p>বৃষ্টি : ৫.৯ মিলিমিটার।</p>

<p> </p>

<p>পাকিস্তানই
সমালাচনার মুখ মুশারফ</p>

<p>পাকিস্তানর সনাশাসক
জনারল পরভজ মুশারফ নাটকীয়ভাব রাষ্ট্রপতি পদ দখল করার চব্বিশ ঘন্টার মধ্যই দশর বিরাধী
দলগুলির তীব্র ক্ষাভর মুখ পড়ছন। ষালটি রাজনৈতিক দলর সম্মিলিত জাট 'এ্যালায়ন্স
ফর রস্টারশন অব ডমাক্র্যাসি' (এ আর ডি)-এর মুখপাত্র, নবাবজাদা নাসারুল্লা খান এক
বিবৃতিত জানিয়ছন, জনারল মুশারফ সংবিধান ল্ঙঘন করছন। সংবিধান স্পষ্ট বলা আছ কানা
সামরিক শাসক ময়াদ চলাকলীন কখনওই দশর প্রসিডন্ট হত পারবন না। এক্ষত্র অবসর নওয়ার
পর কমপক্ষ দুই বছর অপক্ষা করত হব। </p>

<p>পাকিস্তানর অপসারিত
প্রধানমন্ত্রী নওয়াজ শরিফ জানিয়ছন, মুশারফ তাঁর পূর্বসূরিদর মতাই, নিজর শাসনক্ষমতা
টিকিয় রাখত 'ভারত-কার্ড' খললন। শরিকদর দল, পাকিস্তান মুসলিম লিগর
কার্যনির্বাহী প্রসিডন্ট জাভদ হাসমি মুশারফর প্রসিডন্ট পদ আসীন হওয়াক 'দশর
পক্ষ চরম ক্ষতিকারক ঘটনা' বল বর্ণনা করছন। </p>

<p>এদিক অপসারিত প্রসিডন্ট
রফিক তারার এই ঘটনায় অত্যন্ত ক্ষুব্ধ। নির্দিষ্ট পাঁচ বছরর ময়াদ শষ হওয়ার আঠারা মাস আগই
তাঁক সরিয় দওয়া হল। সনাপ্রধান মুশারফ 'প্রাভিশনাল কনস্টিটিউশনার অর্ডার'-এ
দুটি সংশাধনী এন য পদ্ধতিত তাঁক বরখাস্ত করছন তাকও চ্যালঞ্জ জানিয়ছন তারার।
সিন্ধ হাইকার্টর বার অ্যাসাসিয়শন-সভাপতি, রশিদ রিজভি, মুশারফর আচরণক সম্পূর্ণ
'অসাংবিধানিক' এবং 'অনৈতিক' বলছন। মুশারফ সুপ্রিমকার্টর নির্দশনামাকও
পরায়া করননি বল অভিযাগ রশিদর। </p>

<p>বৃহস্পতিবার লাহার
বনজির ভুট্টার সমর্থকরা মুশারফর বিরুদ্ধ তীব্র ক্ষাভ প্রকাশ করছন। ক্ষুব্ধ মহিলারা পাঞ্জাব
আইনসভা ভবনর সামন মুশারফর কুশপুতুল দাহ করন। দশর প্রধান ইসলামি সংগঠন
জামাত-ই-ইসলামির বর্ষীয়ান নতা কায়াজি হাসন দশ নতুন কর নির্বাচনর জন্য
অন্তর্বর্তী পার্লামন্ট ডাকার দাবি জানিয়ছন। </p>

<p>মুশারফর কাছ
একমাত্র আশার কথা, লাহারর বণিকসভা এবং শিল্পপতিরা তাঁর পাশ রয়ছন। বণিকসভাগুলি
জানিয়ছ, পাকিস্তানর এই অভ্যন্তরীণ বিশৃ্ঙখল পরিস্হিতিত মুশারফই একমাত্র ব্যক্তি যিনি
স্হিতিশীলতা আনত সক্ষম। সাধারণ মানুষও ঘটনাটি মন নিত পারছন না। দ্য নশন পত্রিকা
ইন্টারনট জনমত যাচাই করছ। ফলাফল দখা যাচ্ছ, ৬৩ শতাংশ মানুষ এভাব ক্ষমতা দখলর
বিরুদ্ধ। ২৮ শতাংশ ভাটদাতা মুশারফক সমর্থন করছন এবং ৯ শতাংশ কানও মতামত
জানাননি। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>কাশ্মীরক
ফের 'বিতর্কিত অঞ্চল' বলল পাকিস্তান</p>

<p>বুধবারই টলিফান
কথা হয়ছিল প্রধানমন্ত্রী অটলবিহারী বাজপয়ী ও পাকিস্তানর প্রসিডন্ট পরভজ মুশারফর। সখান
স্হির হয়ছিল, শীর্ষ বৈঠকর আগ দুই দশই পারস্পরিক সম্পর্কর ক্ষত্র উত্তজনা কমানার চষ্টা
করব। উত্তজক ও বিতর্কিত বিবৃতিও বন্ধ করা হব। ২৪ ঘন্টার মধ্যই প্রতিশ্রুতি ভঙ্গ করল
পাকিস্তান। বৃহস্পতিবার ইসলামাবাদর পক্ষ থক ফর কাশ্মীরক বিতর্কিত এলাকা বল বর্ণনা
করা হয়ছ। </p>

<p>অধিকৃত কাশ্মীর নিয়
বাজপয়ীর মন্তব্যর তীব্র সমালাচনা কর জনৈক সরকারি মুখপাত্র বলছন, কাশ্মীর সমস্যার
সমাধান হত পার একমাত্র রাষ্ট্রসংঘর সনদ বর্ণিত প্রস্তাব মনই। ভারত কাশ্মীরক অবিচ্ছদ্য
অংশ বল দাবি করলও বাস্তব পরিস্হিতির সঙ্গ তার কানও সম্পর্ক নই। কারণ, কাশ্মীরর
মানুষ এখনও আত্মনিয়ন্ত্রণর অধিকার পাননি। পাকিস্তানর এই মন্তব্য শীর্ষ বৈঠকর আগ
নিশ্চিতভাবই টনশন বাড়িয় দব। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>নির্বাচনাত্তর
সংঘর্ষ মৃত ৭, আহত ৭৪ : মুখ্যমন্ত্রী</p>

<p>পশ্চিমবঙ্গ নির্বাচনাত্তর
সংঘর্ষ ৭ জনর মৃতু্য হয়ছ এবং ৭৪ জন আহত হয়ছন। এই হিসাব ১১-২৪ ম-র।
বৃহস্পতিবার বিধানসভায় তৃণমূল কংগ্রসর বিধায়ক সৗগত রায়র এক প্রশ্নর জবাব
মুখ্যমন্ত্রী বুদ্ধদব ভট্টাচার্য এই কথা জানিয়ছন। তিনি বলন, বিধানসভা ভাটর পর ৫০টি ছাট
বড়া সংঘর্ষর ঘটনা ঘটছ। এর মধ্য মদিনীপুরই রাজনৈতিক সংঘর্ষ সবচয় বশি।
</p>

<p>হিংসাত্মক ঘটনায়
প্রাথমিকভাব ৭৩১ জন ঘর ছাড়ত বাধ্য হলও এর মধ্য ৫০২ জন ফির এসছন। বুদ্ধদববাবু
স্বীকার কর নন, য সব ঘটনা থানা বা প্রশাসনর গাচর এসছ তার বাইরও সংঘর্ষ হত পার।
মুখ্যমন্ত্রী আরও বলছন, প্রশাসনর তরফ থক কঠার ব্যবস্হা নওয়ায় পরিস্হিতি দ্রুত
স্বাভাবিক হয় আসছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বীরভূমর
জিপ ট্রাক মুখামুখি ধাক্কা, হত ৬</p>

<p>বৃহস্পতিবার ভার
বীরভূমর পানাগর-মার গ্রাম জীপর সঙ্গ ট্রাকর সংঘর্ষ ৬ জনর মৃতু্য হয়ছ। গুরুতর
আহত অবস্হায় ৪ জনক শিউরি সদর হাসপাতাল ভর্তি করা হয়ছ। মৃত ও আহতরা সকলই জিপর
যাত্রী ছিলন। তারা দওঘর থক তারাপীঠ যাচ্ছিলন। পথ পানাগড়র কাছ বিপরীত দিক থক
আসা ট্রাকর মুখামুখি ধাক্কা লাগ। ট্রাকর চালকক ধরা যায়নি। খুব ভার এই ঘটনা
ঘটায় আহতদর হাসপাতাল নিয় যত কিছুটা বিলম্ব হয়ছ। কী কর দুর্ঘটনা ঘটল জানা
যায়নি। পুলিসর অনুমান জিপর চালক মত্ত অবস্হায় নিয়ন্ত্রণ হারিয় ট্রাকর সামন চল আসন।
তব, ময়না তদন্তর আগ পুলিস অফিসাররা মুখ খুলত নারাজ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>সম্পাদকীয় :
দিশা, দিশারী</p>

<p>শুভদিন মিষ্টিমুখ করানাই
ভারতীয় সহবত। বামফ্রন্ট সরকার পঁচিশ পা দিচ্ছ। মিষ্টি মিষ্টি কথা দিয় সুখী গৃহকাণ গড়ার
বাসনা স্বাভাবিক। তবু একটু ঘুরপথ হাঁটা যাক। ষষ্ঠ বামফ্রন্ট সরকার উপলব্ধি করছ মিষ্টি কথায়
আর কাজ হব না। তাই বলা হচ্ছ কর্মসংস্কৃতির কথা। মহাকরণর করণিকদর বলা হচ্ছ, কাজ ফল
রাখবন না। হাতর কাজ সর ফলুন। শিক্ষকদর বল দওয়া হচ্ছ ঠিকমত কাজ না করল শাস্তি
হব। স্বাস্হ্যকর্মীদর বল দওয়া হচ্ছ, হাসপাতাল স্লাগান ধর্মঘট চলব না। ট্রড ইউনিয়ন নতাদর
বলা হয়ছ, সরকার জঙ্গি আন্দালন বরদাস্ত করব না। অর্থাত্ প্রকারান্তর গত ২৪ বছরর
ব্যর্থতার কারণগুলি চিহ্ণিত করা হয়ছ। সত্যি বলত কী, যাঁদর কর্মসংস্কৃতির ততা পাঁচন গলাবার
চষ্টা হচ্ছ এতদিন তাঁরাই ছিলন বামফ্রন্টর জয়র মূল স্হপতি, কারিগর। এখন প্রশ্ন, এঁদর
ঘুঘুর বাসা ভাঙল বামফ্রন্ট ভাট আর জিতত পারব কি? সাধারণ মানুষর উপলব্ধি বলছ,
পারব। এই কাজগুলা ঠিকঠাক করত পারল সাধারণ মানুষ দু-হাত তুল ধন্য ধন্য করব।
বামপন্হী ভাটযন্ত্রক অতি সক্রিয় কর তালার হয়ত প্রয়াজনই হব না। </p>

<p>আসল কথা, ষষ্ঠ
বামফ্রন্ট মন্ত্রিসভার কাছ মানুষর প্রত্যাশা পাহাড় প্রমাণ। আশঙ্কা এখানই। প্রত্যাশার চাপ
সব কিছু ভঙ না পড়। প্রথম দশ বছর বামফ্রন্টর মধ্য আত্মবিশ্বাস ছিল। পরর দশ বছর এল
আত্মসন্তুষ্টি। বন্ধ কলকারখানা, মালিকদর জাচ্চুরি, প্রভিডন্ট ফাণ্ডর টাকা মর দওয়া, ট্রড
ইউনিয়ন নতাদর ধান্দাবাজি, ভড়ি মালিকদর অত্যাচার, জমি নিয় প্রমাটরদর ফরববাজি-
গালকধাঁধায় হারিয় গল বামফ্রন্ট সরকার। শিল্পায়ন নিয়ও দ্বিধা, সঙ্কাচ। প্রাচীন তত্ত্ব আর
অভ্যাসর ঘামটা সরিয় দওয়ার লজ্জা। শাস্ত্রগ্রন্হ খুল বাস্তবক মাপার ব্যর্থ চষ্টা দখা গল।
সত্যি বলত কী, বামফ্রন্টর ফির আসার মূল আছ সদর্থক বিরাধী শক্তির অভাব। আর ব্যক্তি
বুদ্ধদব ভট্টাচার্যক ঘির, তাঁর পরিচ্ছন্ন ভাবমূর্তিক ঘির মানুষর প্রত্যাশা। সই বুদ্ধদব
ভট্টাচার্যই পারন হয়ত বাংলার ঘুর দাঁড়াবার স্বপ্নক সফল করত। তিনি ব্যর্থ হল? অনক দরি
হয় যাব হয়ত। তখন হয়ত দাঁড়াবারও জায়গা থাকব না।</p>

<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>মণিপুর
গুলি চালনার তদন্ত চায় অ্যামনস্টি</p>

<p>মণিপুর উত্তজিত
জনতার ওপর পুলিসর গুলি চালানার তদন্ত দাবি করছ অ্যামনস্টি ইন্টারন্যাশনাল।
বৃহস্পতিবার অ্যামনস্টির পক্ষ থক প্রকাশিত এক বিবৃতিত ঘটনার তীব্র নিন্দা কর বলা হয়ছ,
কার্ফু ল্ঙঘন করল দখামাত্র গুলি করার য নির্দশ প্রশাসন দিয়ছ তা অত্যন্ত উদ্বগজনক। আন্দালন
দমন করার ক্ষত্র রাষ্ট্রসংঘর য নির্দশিকা আছ, মণিপুর সরকারক তা মানতই হব।
</p>

<p>ম্যাজিস্ট্রট পর্যায়র
তদন্তর য নির্দশ দওয়া হয়ছ তাতও খুশি নয় অ্যামনস্টি। মানবাধিকার রক্ষার কাজ ব্রতী
এই আন্তর্জাতিক সংস্হাটি চায় সরকার উচ্চপর্যায়র বিচারবিভাগীয় তদন্তর নির্দশ দিক। নিরপক্ষ
তদন্ত না হল প্রকৃত ঘটনা জানা যাব না। প্রশাসনর সমালাচনার পাশাপাশি অ্যামনস্টি
হিংসার পথ থক বিরত থাকার জন্য আন্দালনকারীদর কাছ আবদন জানিয়ছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>মুশারফক
স্বাগত জানাত প্রস্তুতি নিচ্ছ আগ্রা</p>

<p>আগামী মাস ভারত ও
পাকিস্তানর শীর্ষ বৈঠকর স্হান হিসব বছ নওয়ায় সারা আগ্রা শহর উত্তজনায় ভরপুর হয়
রয়ছ। সমস্ত জলাস্তরর সংগঠনগুলিও এই ঐতিহাসিক ঘটনার সাক্ষী হত নিজদর গুছিয় নিচ্ছ।
বৃহস্পতিবার পুলিস বিভাগর উচ্চপদস্হ কর্তারা নিরাপত্তা ব্যবস্হা মজবুত কর গড় তুলত
আগ্রায় এস পৗঁছছন। </p>

<p>আগ্রা ভারতর প্রম-ভালবাসার
প্রতীক-শহর। সজন্যই হয়ত শান্তি আলাচনা বৈঠকর স্হান হিসব আগ্রাকই বছ নওয়া হয়ছ।
সই চতুর্দশ শতক থক নানা ধরনর ইতিহাস প্রসিদ্ধ সৗধভবনর সঙ্গই বহু সংস্হাও প্রতিষ্ঠিত হয়ছিল
এই আগ্রায়, এই সমস্ত সংস্হায় সরাসরি যাগাযাগ ছিল ভারতর মুঘল সম্রাটদরও। তাজমহল
শান্তি এবং ভালবাসার শ্রষ্ঠতম নিদর্শন। আগ্রার পরিবশ যন শান্তিসমৃদ্ধ গঠনমূলক য কানও
বিষয় আলাচনার আদর্শ স্হান, বলছন আগ্রার সাধারণ মানুষজন। এই প্রসঙ্গই চল আসছ
প্রাক্তন মার্কিন প্রসিডন্ট বিল ক্লিন্টনর আগ্রা সফরর কথা। তব শহর সকলই য খুশি- সটা
বলা ঠিক হব না। কউ কউ বলছন, 'মুশারফক তাজমহলর পাদদশ বসত দওয়াই উচিত
নয়। কারণ 'তাজ' আমাদর ভারতীয় সংস্কৃতির ঐতিহ্যস্বরূপ। একজন পাকিস্তানিক সখান
আমন্ত্রণ জানানা মাটই উচিত হব না।' </p>

<p>তব কউই তমন ভাল
কর জানন না ভারতর প্রধানমন্ত্রী অটলবিহারী বাজপয়ী এবং পাকিস্তানর মুশারফ হাসন কাথায়
থাকবন এবং কাথায় বৈঠক মিলিত হবন। তব বিখ্যাত কানও ব্যক্তিত্ব এল সারা শহরটাই
একটু পরিষ্কার পরিচ্ছন্ন করা হয়- এটাই সাধারণ মানুষর অভিজ্ঞতা। আর তাতই তাঁরা
খুশি।  
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>২৫-শে ক্রিকট
বোর্ডর দপ্তর তল্লাশি চালাব সি বি আই</p>

<p>সি বি আই-এর এক বিশষ
তদন্তকারী দল আগামী ২৫ জুন মুম্বইয় বি সি সি আই-এর অফিস হানা দিত চলছ। বুকি মুকশ
গুপ্তা সম্পর্ক আরও তথ্য পাওয়ার আশায় সি বি আই মুম্বইয় বি সি সি আই-এর অফিস আসছ।
মার্চ মাস আই সি সি-র কাছ বটিং বিতর্ক নিয় তদন্ত জবাবদিহির পর ভারতর বটিং কলঙ্কারির
অন্যতম এই বুকির টিকিটিও খুঁজ পাচ্ছ না সি বি আই। গুপ্তার সাক্ষ্যর উপরই নির্ভর করছ
অ্যালক স্টুয়ার্ট ও ব্রায়ান লারার ক্রিকট ভবিষ্যত। </p>

<p>সি বি আই জানিয়ছ,
হঠাত্ উধাও হয় যাওয়া গুপ্তা সম্পর্ক যতটা সম্ভব বশি তথ্য জাগাড় করার উদ্দশ্য তাঁরা মুম্বইয়
এসছন। সামবার লন্ডন আই সি সি-র এক মিটিংয় ম্যাচ গড়াপটা তদন্তর অন্যতম চিফ
এক্সিকিউটিভ পল কনডন মুকশ গুপ্তাক আগামী ১ জুলাইয়র মধ্য তাঁদর সামন এস হাজির হত
বলছন। কারণ মুকশ গুপ্তাই স্টুয়ার্ট ও লারাক ম্যাচ গড়াপটা করার জন্য টাকা দিয়ছিলন বল
অভিযাগ। এখন গুপ্তাই যদি তদন্ত কমিটির সামন এস সমস্ত তথ্য না দন তব স্বাভাবিকভাবই
লারা ও স্টুয়ার্টদর বিরুদ্ধ আনা অভিযাগগুলি খারিজ হয় যাব। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>অর্থর অভাব
রবার বুলট কিনত পারছ না পুলিস</p>

<p>বুধবার সকাল হুগলি জলার
বাঁশবড়িয়ায় গ্যাঞ্জস জুটমিল হামলার সময় পুলিসর গুলিত এক শ্রমিকর মৃতু্যর ঘটনা
আবার রাজ্য সরকার ও পুলিস প্রশাসনক এক পুরনা বিতর্কর সামন দাঁড় করিয়দিল। পশ্চিমবঙ্গও
পুলিসর হাত কন জলকামান ও রবার বুলট নই? এটাই এখন সর্বস্তরর মানুষর প্রশ্ন।
</p>

<p>যকানও ধরনর
হামলা, বিক্ষাভ, অবরাধক ছত্রভঙ্গ করত দিল্লি, মুম্বইয়র পুলিস যখান আজকাল রবার বুলটর
ব্যবহার করছ সখান ভারতর প্রাক্তন রাজধানী শহর নাকি রবার বুলট কনার মত পয়সা
পুলিস বা অর্থদপ্তরর নই। সই মান্ধাতার আমলর সিসার তৈরি বুলট দিয় উত্তজিত জনতাক
আটকাত গিয় তাই মানুষ মারছ পশ্চিমবঙ্গর পুলিস। </p>

<p>বুধবার কলকাতা পুলিসর
হড কায়ার্টার থক জানান হল য, পশ্চিমবঙ্গর গত বিধানসভা নির্বাচনর কিছুদিন আগই
এই ধরনর বুলট কনার ব্যাপার প্রস্তাব রাখা হয়ছিল অর্থমন্ত্রীর কাছ। কিন্তু অর্থদপ্তরর
ঢিলমির জন্য দু মাস বাদও সই প্রস্তাব রূপায়ণর কানওরকম উদ্যাগ দখা যাচ্ছ না।
</p>

<p>পুলিস অফিসাররা
জানালন, মুখ্যমন্ত্রী বুদ্ধদব ভট্টাচার্য তাঁদরক সাধারণ মানুষর সাথ মানবিক ব্যবহার
করার জন্য এবং উত্তজিত জনতাক নিয়ন্ত্রণ করার সময় মানুষর যাত আঘাত না লাগ সই বিষয়
অনুরাধ করায় তাঁরা এই রাবার বুলট কিনত চয়ছন। </p>

<p>পুলিস প্রশাসন এবার
নড়চড় বসত চায়। কলকাতা পুলিস এবার তাদর বিভিন্ন ট্রনিং সন্টার বিদশ থক বিভিন্ন
আধুনিক সরঞ্জাম এন পুলিসদর ট্রনিং দিত চায়। ভারতীয় মনাবিজ্ঞান ও ইতিহাসর ওপরই
এখান জার দওয়া হব। চারট ট্রনিং সন্টার খুলত চায় তারা। যার একটিত বিশষভাব
কমাণ্ডা ট্রনিং দওয়া হব এবং অন্যগুলিত কনস্টবলদর আধুনিক হাতিয়ার প্রশিক্ষণ দওয়া হব।
পশ্চিমবঙ্গ পুলিস এবার শুধুমাত্র আধুনিক সাজসরঞ্জামই নয়, দুষ্কৃতি মাকাবিলায় এক-৪৭এর মত
স্বয়ংক্রিয় বন্দুকও কিনত চলছ। এছাড়াও রাত দখার যন্ত্র, চিঠিত বা পার্সল বামা খাঁজার
যন্ত্র, বিস্ফারণ সৃষ্টিকারী পাউডার জাতীয় জিনিস খুঁজ পাওয়ার যন্ত্র, স্পাই ক্যামরা ও স্পাই
রকর্ডারও কিনত চায় পুলিস। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বিজয়নর
কলকাতায় খেলা নিয় জল ঘোলা হচ্ছ</p>

<p>এফ সি কাচির
স্ট্রাইকার জা পল আনচরি এবার লাল-হলুদ জার্সি পর কলকাতার মাঠ নামবন। কিন্তু আই
এম বিজয়নর কলকাতায় খলা অনিশ্চিত। তাঁর আন্তঃরাজ্য ছাড়পত্র নিয় জটিলতা কাটনি।
বুধবার অল ইন্ডিয়া ফুটবল ফডারশন আন্তঃরাজ্য ছাড়পত্রর য পাঁচটি তালিকা প্রকাশ করছ
তাত বিজয়নর নাম নই। এবার মাট ২৩৮ জন অন্য রাজ্য খলার আবদন করন, তাত বিজয়ন
ছিলন। </p>

<p>ফডারশন সচিব
আলবার্তা কালাসাক এ সম্পর্ক জিজ্ঞাসা করা হল বলন, মাহনবাগান ও টালিগঞ্জ অগ্রগামী
বিজয়ন সম্পর্ক য অভিযাগ করছ তার জন্যই ওর ছাড়পত্র আটক আছ। স বাংলাদশর মুক্তিযাদ্ধা
দল খল আন্তর্জাতিক ছাড়পত্র না নিয়ই এফ সি কাচিত খলছ। মাহনবাগান ও টালিগঞ্জ ওর
বৈধতা চ্যালঞ্জ করছিল জাতীয় লিগর সময়ই। বিজয়ন বিষয়টি আই পি এফ-এর সভায় উঠছিল।
টালিগঞ্জর মন্টু ঘাষ তখন বলন, এটি অল ইন্ডিয়া ফুটবল ফডারশনর ব্যাপার। তিনি এ নিয় কানও
সমঝাতায় যাবন না। ছাড়পত্র তালিকায় বিজয়নর নাম না থাকায় ইস্টবঙ্গল ক্লাব কর্তৃপক্ষ
চিন্তিত। ক্লাব সচিব অবিলম্ব ফুটবল-কর্তাদর এই ব্যাপার হস্তক্ষপ করত বলছন। 

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>উইম্বলডন
মূল পর্বর দিক লিয়ন্ডার</p>

<p>উইম্বলডন টনিসর
মূলপর্ব শুরু হব ২৫ জুন। তার চারদিন আগ কলকাতার লিয়ন্ডার পজ ঘাসর কার্ট বশ খললন।
পৗঁছ গলন সিঙ্গলসর যাগ্যতা অর্জনর শষপর্ব- ফাইনাল। ফাইনাল জিতলই খলত পারবন
মূলপর্ব। </p>

<p>বুধবার লিয়ন্ডার
৭-৫, ৬-১ সট হারান ইতালির মাজ নাভারা-ক। তব শুরুত তিনি প্রায় হারার মুখ চল
যান। প্রথম সট ফল দাঁড়ায় ৫-৫। এর পরই লি দুরন্ত খল ৭-৫ গম সটটি জিত নন।
দ্বিতীয় সট অবশ্য দাঁড়াতই পারননি ইতালির খলায়াড়টি। লিয়ন্ডার জিতলন ৬-১ গম।
</p>

<p>ফাইনাল রাউণ্ড পজর
সামন থাকবন আমরিকার আলক্স ও' ব্রায়ন। লি এর আগ আলক্সর মুখামুখি হয়ছিলন
১৯৯৮-এ ইউ এস ওপন। সবার আলক্স ৭-৫, ৬-৩, ৭-৬এ হারিয়ছিলন লি-ক। এবার অবশ্য লি
ওঁক ভয় পাচ্ছন না। বলছন, তিন বছর আমাদর দুজনরই খলার অনক বদল ঘটছ।

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>প্রাকৃতিক
সম্পদ কাজ লাগিয় আয় করছ বিদ্যাসাগর বিশ্ববিদ্যালয়</p>

<p>বিদ্যাসাগর
বিশ্ববিদ্যালয় বনজ সম্পদ ব্যবহার কর আর্থিকভাব স্বনির্ভর হত পরছ। পরিবশর ভারসাম্য
বজায় রখ এই বনসৃজন প্রকল্প থক বিশ্ববিদ্যালয় ২.৪
লক্ষ টাকা লাভ করছ। স্হানীয় মানুষও বনসৃজন প্রকল্পর গুরুত্ব বুঝ লাভবান হয়ছন।
</p>

<p>নিজস্ব ১৩৯ বর্গ একর
জমিত ইউক্যালিপটাস, শাল, সগুন ও কাজু গাছর চাষ শুরু কর কর্তৃপক্ষ। প্রাকৃতিক সম্পদ থক
স্হানীয় মানুষক লাভবান করার জন্য বহু নতুন পাঠক্রমও চালু করছ বিশ্ববিদ্যালয় কর্তৃপক্ষ।
৪০ একর জমিত মধু, মাম ইত্যাদি প্রাকৃতিক সম্পদ ব্যবহারর জন্য চাষবাসর পাশাপাশি ফ্লারিকালচার,
এনভায়রনমন্ট ম্যানজমন্ট কার্স শুরু করা হয়ছ। সম্প্রতি বিশ্ববিদ্যালয় রুরাল অ্যাডমিনিস্ট্রশন
অ্যান্ড ডভলপমন্ট ও অন্যান্য রিফ্রশার কার্স খালার অনুমতি চয়ছ মঞ্জুরি কমিশনর কাছ।
</p>

<p>প্রাকৃতিক দিক দিয় মদিনীপুর
জলা বনজ, কৃষি ও জল সম্পদ ভরপুর। এই দিকটি মাথায় রখই ছাত্রছাত্রীদর এই সম্পদ সংরক্ষণ
ও ব্যবহারর শিক্ষা দিচ্ছ বিশ্ববিদ্যালয়। </p>

<p>বিশ্ববিদ্যালয়র সবুজ
বচ লাভর মুখ দখার পর, আর্থিক সহায়তার জন্য আর মঞ্জুরি কমিশনর দ্বারস্হ হত হচ্ছ
না। সবুজর সদ্ব্যবহারর জন্য টন্ডার ডক জমি ইজারা দওয়া হয়। বিশ্ববিদ্যালয়র ক্যাম্পাসর
চারধার এখন তাই নয়নাভিরাম সবুজর সমারাহ। প্রযুক্তি পাঠর সঙ্গ সঙ্গ প্রকৃতিকও চিনছ
ও ভালবাসছ বিদ্যাসাগর বিশ্ববিদ্যালয়র ছাত্রছাত্রীরা। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p>বিদশ রসগাল্লার
কারখানা?</p>

<p>অনাবাসী ভারতীয়দর
সাহায্য নিয় বিদশ রসগাল্লার কারখানা খুলত চায় কে সি দাস। অস্ট্রলিয়ার অ্যাডিলড
আর কানাডার মন্ট্রিয়াল কারখানা খালার কথাবার্তা এগাচ্ছ। মার্কিন যুক্তরাষ্ট্র, পশ্চিম
এশিয়া আর দক্ষিণ-পূর্ব এশিয়াতও কারখানা খুলত চান বল জানালন কে সি দাস-এর অন্যতম
কর্ণধার ধীরন দাস। টিন বন্ধ স্পঞ্জ রসগাল্লা ইতিমধ্যই শাভা পাচ্ছ বিখ্যাত সুপারমার্কট
ওয়ালমার্ট এবং হ্যারডস-এ। ভারতর কর্ণাটক আর অন্ধ্রপ্রদশর বাজার ধরত কে সি দাস বিশষ
আগ্রহী। </p>

<p>১৮৬০ সাল দাস
পরিবারর উদ্ভাবন ওই বিশষ রসগাল্লা। ১৯৩০ সাল টিন ভর বাজার ছাড়া হয়। এসপ্ল্যানডর
বিপণী থক দিন ৩০০ টিন রসগাল্লা বিক্রি হয়। সই সঙ্গ চলছ গবষণাও। স্নহ পদার্থ কম,
শর্করাবিহীন, ভিটামিন সমৃদ্ধ মিষ্টিও তৈরি করছ কে সি দাস। পটন্ট নওয়ার চষ্টা চলছ রসগাল্লা,
পান্তুয়া, ল্যাংচা আর মিষ্টি দইয়র। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p>প্রাক্তন
রন্জি ক্রিকটার পুলিন মিত্রক সি এ বি-র সাহায্য</p>

<p>দুঃস্হ প্রাক্তন রন্জি
ক্রিকটার পুলিন মিত্রক অর্থ সাহায্য করব সি এ বি। গত এপ্রিল সি এ বি-র কাছ অর্থ সাহায্যর
আবদন জানান মাহনবাগান ও বাংলা দলর এই প্রাক্তন ক্রিকটার। চল্লিশর দশক তিনি ওপনিং
ব্যাটসম্যান ছিলন। বাংলার হয় রন্জি ম্যাচও খলন। সি এ বি-র পক্ষ থক বলা হয়ছ অর্থ
সাহায্য ছাড়াও তাঁর পরিবারর জন্য মডিক্লম কর দওয়া হব। </p>

<p>সাহায্য
কাশমর পরিবারকও </p>

<p>কয়ক বছর আগ মহমডান
স্পার্টিং-এর মাঠ গ্যালারি ভঙ পড়ায় আহত হয়ছিলন ফুটবলপ্রমী কাশম আলি। সম্প্রতি
তিনি মারা গলন যথাযথ চিকিত্সার অভাব। তাঁর দুর্দশাগ্রস্ত পরিবারক ইস্টবঙ্গল ক্লাব ৩০
হাজার টাকা সাহায্য দব জানিয়ছ। কাশমর পরিবারর নাম ডাকঘর মাসিক আয় জমা প্রকল্প
এই টাকা জমা দওয়া হব। প্রতি মাস কাশমর পরিবার এখান থক টাকা তুলত পারব।
রাজ্য সরকারও যাত সাহায্য করত পার সজন্য বিধায়ক মানস মুখার্জিও চষ্টা করছন। তিনি
বুধবার কাশমর স্ত্রী ও মা-র সঙ্গ কথাও বলছন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বর্ষসরা
ক্রিকটার রোহন, পঙ্কজ ট্রফি দীপ দাশগুপ্ত-কে</p>

<p>টস্ট বিশ্বরকর্ডর
অধিকারী ক্রিকটার প্রয়াত পঙ্কজ রায়র নাম ক্রিকট অ্যাসাসিয়শন অব বঙ্গল একটি বর্ষসরা
ট্রফি চালু করছ। ট্রফিটির নাম 'বস্ট জন্টলম্যান ক্রিকটার অব দ্য ইয়ার'। প্রথম বছর
এই পুরস্কার পাবন উইকটরক্ষক দীপ দাশগুপ্ত। ভাল খলার পাশাপাশি খলায়াড়দর ব্যক্তিত্বও দখা
হয়ছ। এবং দুই বিষয়র বিচার দীপ সরা হয়ছন। </p>

<p>সিনিয়র
নির্বাচকমণ্ডলীর চয়ারম্যান অশাক মালহাত্রার উপস্হিতিত রাহন গাভাসকরক বাংলার সিনিয়র
বর্ষসরা ক্রিকটার নির্বাচিত করা হয়ছ। সুনীল গাভাসকরর পুত্র রাহন গত মরসুম রন্জি ট্রফিত
তিনটি শতরান-সহ ৪৫৯ রান করন। </p>

<p> </p>






</body></text></cesDoc>