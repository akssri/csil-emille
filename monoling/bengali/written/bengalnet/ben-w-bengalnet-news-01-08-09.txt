<cesDoc id="ben-w-bengalnet-news-01-08-09" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-08-09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-08-09</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-08-09</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>গোটা জম্মুক
উপদ্রুত ঘোষণা করল কেন্দ্র</p>

<p>কন্দ্রীয় সরকারর
সিদ্ধান্ত অনুযায়ী গাটা জম্মুই উপদ্রুত এলাকার আওতায় চল এল। জম্মু ও কাশ্মীরর চারটি
নতুন জলাক উপদ্রুত এলাকা হিসব ঘাষণা করা হয়। এগুলি হল জম্মু, ডাডা, উধমপুর এবং
কাঠুয়া। এছাড়া পুঞ্চ ও রাজৗরি- এই দুই জলা আগই উপদ্রুত এলাকা হিসব চিহ্ণিত হয়ছ।
অর্থাত্ গাটা জম্মু-সহ রাজ্যর ছয়টি জলা বিশষ উপদ্রুত এলাকা আইনর আওতায় চল এল। এই
আইনটি ছাড়াও ওই ছয় জলার ক্ষত্র ১৯৯৯ সালর সশস্ত্র বাহিনীর বিশষ ক্ষমতা আইনটিও প্রযাজ্য
হব। এই আইন অনুসার নিরাপত্তা বাহিনীর হাত আরও ক্ষমতা চল আসব। কানও ওয়ারন্ট
ছাড়াই পুলিস বা নিরাপত্তা বাহিনী য কানও জঙ্গিক গ্রপ্তার করত পারব। </p>

<p>কন্দ্রীয় স্বরাষ্ট্রমন্ত্রী
লালকৃষ্ণ আডবাণীর পৗরাহিত্য নর্থব্লক এক উচ্চপর্যায়র বৈঠকই এই সিদ্ধান্ত নওয়া হয়।
এই বৈঠক প্রতিরক্ষামন্ত্রী তথা বিদশমন্ত্রী যশাবন্ত সিং কাশ্মীরর মুখ্যমন্ত্রী ফারুক আবদুল্লা,
রাজ্যর রাজ্যপাল গিরিশ সাক্সনা, সনাপ্রধান, মিলিটারি অপারশনর ডিরক্টর জনারল প্রমুখ
হাজির ছিলন। এই বৈঠক ওই সিদ্ধান্ত গ্রহণর আগই মুখ্যমন্ত্রী ফারুক আবদুল্লা সংশ্লিষ্ট জলাগুলিক
উপদ্রুত এলাকা ঘাষণা করার দাবি জানান। এর আগ পুঞ্চ ও রাজৗরিত উপদ্রুত এলাকা আইন
কার্যকর হয়ছ। </p>

<p>বৈঠকর পর ফারুক
আবদুল্লা বলন, জম্মুত সাধারণ নিরীহ মানুষর বাস। এখান নিরাপত্তাবাহিনীর পক্ষ অপারশন
চালানা খুবই কঠিন। প্রস্তাব ছিল, হলিকপ্টার ব্যবহার কর ওপর থক জঙ্গিদর খতম করা হব।
কিন্তু তাত নিরীহ মানুষর নিহত হওয়ার সম্ভাবনাও বাড়ত। তাই আজ সন্ধ্যায় দীর্ঘ আলাচনার
পর ঠিক হয় য, উপদ্রুত এলাকা আইন কার্যকর কর সন্ত্রাসক সম্পূর্ণ খতম করত হব। উপদ্রুত
এলাকা আইন কার্যকর হওয়ার ফল গাটা জম্মুই কার্যত চল গল সনার অধীন। রাজ্য পুলিসর
ক্ষমতা খর্ব হল পুরাপুরি।  
</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p> </p>

<p>কুখ্যাত দাউদর
দোসর কলকাতায় গ্রেপ্তার</p>

<p>কুখ্যাত মাফিয়া দাউদ
ইব্রাহিমর দাসর আবদুল গফ্ফর গনি শখ (৩৩) ধরা পড়ল কলকাতায়। দাউদর দলর এই ভাড়াট
খুনি দড় বছর কলকাতায় লুকিয়ছিল। মুম্বইয় স একাই সাতটি খুন করছ। খুন, অপহরণ ও
অন্যান্য অভিযাগ তার বিরুদ্ধ ২১টি মামলা রয়ছ। তার কাছ থক পাঁচটি মাবাইল ফান
বাজয়াপ্ত করা হয়ছ। কলকাতার খ্যাতনামা এক ভুজিয়াওয়ালাক অপহরণর ফন্দি এঁটছিল স।
দুবাই থক দাউদর ভাই আনিস ইব্রাহিম আবদুল গফ্ফরক নির্দশ দিত। নিউ আলিপুরর
বিখ্যাত ব্যবসায়ী গণশ মহালিঙ্গমক অপহরণর চষ্টা করছিল আনিসরই নির্দশ। তিলজলার
সমাজবিরাধী গুড্ডুক ব্যবহার করছিল। দুষ্কৃতীরা সি বি আইয়র অফিসার সজ মহালিঙ্গমর
বাড়িত হানা দিলও সফল হয়নি। তারপর মহালিঙ্গমর অফিস ভাড়াট গুণ্ডারা ভয় দখাত গুলি চালায়।
লাখখানক টাকাও হাতিয় নয় আবদুলরা। </p>

<p>আবদুল গফ্ফর পূর্ব
বান্দ্রার খড়ওয়াড়ি রাডর রজ্জাক বস্তির বাসিন্দা। তার বাবা গনি শখ বিষয় সম্পত্তির বিবাদ
১৯৮৭ সাল খুন হয় যান। সই খুনর বদলায় সাত্তার নাম একজনক খুন কর মাত্র ১৯ বছর বয়স
ফরার হয় যায় আবদুল গফ্ফর। এরপর মজিদ খান বিল্ডার্স নাম একটি সংস্হায় পশাদার গুণ্ডার
দায়িত্ব পায়। মজিদ খানর ভাই কুখ্যাত হাজি ইয়াকুবর সঙ্গ তার ঘনিষ্ঠতা হয়। মুম্বই পুলিসর
তাড়ায় ২০০০ সালর গাড়ার দিক আবদুল কলকাতায় আশ্রয় নয়। চিত্পুর এলাকায় ১৪এ লকগট রাড
তার মামা লাড্ডু ভাইয়র কাছ থাকত শুরু কর। জুনর প্রথম সপ্তাহ দমদম ইজাজ আহমদ খান
নাম থাকত শুরু কর। ওই ঘর ছড় জয়পুর দিদির কাছ চল যায়। ফির এস আবার দমদম
ঘর ভাড়া নয়। মুম্বই থক স্ত্রীক নিয় আসার জন্য চিত্পুর মামার বাড়িত অপক্ষা করছিল।
</p>

<p>পার্থসারথি অপহরণর
সঙ্গ আবদুলর কানও যাগসূত্র এখনও পায়নি পুলিস। তব কানও সম্ভাবনাই উড়িয় দওয়া হচ্ছ
না। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>



<p>
মমতার
ডাক কাঁথির জনসভায় জর্জ</p>

<p>য জর্জ ফার্নাণ্ডজর
ইস্তফা দাবি কর মন্ত্রিত্ব এবং এনডিএ ছড়ছিলন মমতা সই জর্জ কাঁথিত মমতার ডাকা
জনসভায় ভাষণ দিত যাচ্ছন। বুধবার মমতার আমন্ত্রণ কলকাতায় এস জর্জ জানিয় দন,
'তৃণমূল এনডিএ জাটই রয়ছ। একথা ঘাষণার কিছু নই।' তৃণমূলর বিদ্রাহী সাংসদ
অজিত পাঁজার ব্যাপার তিনি কানও মন্তব্য করননি। জুনর গাড়ায় দিল্লিত গিয় মমতা জর্জর
বাড়িত যান। দুঃখপ্রকাশ করন। পুরনা কথা ভুল যত অনুরাধ করন। মমতা ফিরল
তহলকা-কাণ্ড অভিযুক্ত জর্জরও মন্ত্রিত্ব ফির পাওয়ার পথ সুগম হয়। জর্জ তাই মিটমাট কর নন।
</p>

<p>বুধবার রাত কলকাতা
বিমানবন্দর জর্জক অভ্যর্থনা জানান তৃণমূলর দুই কর্তা- মুকুল রায় ও সুব্রত বক্সি। 
তৃণমূলর দাবি, প্রধানমন্ত্রীর পরামর্শই জর্জ ফার্নাণ্ডজ কাঁথি যাচ্ছন। রাত জর্জ আসার আগই
মমতা কাঁথির দিক রওনা হয় যান। দীঘায় রাত কাটান মমতা। কদিন আগই জর্জ বলছন,
'শিগগিরই এনডিএ-ত আসছ তৃণমূল।' খুব সম্ভবত কাঁথির জনসভাতই জর্জ সব খালসা কর
জানিয় দবন। </p>

<p>২৪ ঘণ্টার মধ্য
ধরতই হব সোনারপুরর খুনীদর : মমতা</p>

<p>সানারপুর দুই
তৃণমূল নতাক হত্যায় জড়িতদর ২৪ ঘণ্টার মধ্য গ্রপ্তার করত না পারল ব্যাপক আন্দালনর
কথা ঘাষণা করলন মমতা বন্দ্যাপাধ্যায়। বুধবার সকাল দিল্লি থক কলকাতায় ফিরই সরাসরি
সানারপুর চল যান মমতা। ঘাসিয়ারা পার্টি অফিস ছাড়াও নিহত দুই কর্মীর বাড়িত গিয়
তাঁদর পরিবারক অর্থ সাহায্য ও সান্ত্বনা দন। মমতা বলন- সারা রাজ্য যা চলছ তা এবার
সানারপুরও শুরু হল। অথচ, এতদিন এখান কানও গালমাল ছিল না। একদিনর মধ্য দাষীদর
গ্রপ্তার না করল প্রশাসন অচল কর দওয়ার হুমকিও দিয়ছন তৃণমূল নত্রী। </p>

<p>এদিক, হত্যাকাণ্ডর
প্রতিবাদ তৃণমূল কংগ্রসর ডাকা বন্ধ বুধবার সানারপুরর জীবনযাত্রা বিপর্যস্ত হয় পড়।
সকাল দফায় দফায় ট্রন অবরাধ দক্ষিণ শাখার ট্রন কার্যত স্তব্ধ হয় যায়। হাজার হাজার
নিত্যযাত্রী প্রচণ্ড অসুবিধায় পড়ন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>আগুনর আতঙ্ক
পালাত গিয় ঢাকায় পদপিষ্ট হয় মৃত ১৬</p>

<p>বাংলাদশর রাজধানী
ঢাকায় বুধবার একটি পাষাকর কারখানায় আগুন লগছ এই আশঙ্কায় আতঙ্কগ্রস্ত মানুষ দ্রুত
পালাত গিয় দুর্ঘটনার মধ্য পড়, মৃত ১৬ জন। আহতর সংখ্যা নূ্যনতম পক্ষ ৫০ জন।
</p>

<p>ঢাকায় একটি বহুতল
কারখানায় আগুন লাগার সঙ্কতধ্বনি বজ ওঠ। ওই বাড়িটির বিভিন্ন তলায় আরও চারটি পাষাক
তৈরির কারখানা ছিল। মাট কর্মীসংখ্যা প্রায় ৩০০০-এর মত। সঙ্কতধ্বনি শুন আতঙ্কগ্রস্ত কর্মীরা
হুড়াহুড়ি কর সরু সিঁড়িপথ বয় নিচ নম পালানার চষ্টা কর। ফল সিঁড়িত আটক
পদপিষ্ট হয় ১৬ জন মারা যায়। মৃতদর অধিকাংশই মহিলা। আহত হয়ছন ৫০ জন কর্মী। ঢাকার মট্রাপলিটান
পুলিসর ডপুটি কমিশনার ইব্রাহিম ফতমি জানান, শর্টসার্কিটর দরুণ আচমকাই ওই সঙ্কত-ঘণ্টাটি
বজ ওঠ। বিদু্যতর ফুলকি উঠলও কানও কারখানায় আগুন লাগার ঘটনা ঘটনি। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>খাদিম-কর্তার
চিকিত্সা করা ডাক্তারক খুঁজছ পুলিস</p>

<p>পার্থ রায়বর্মনর
অপহরণ মামলার কানও অগ্রগতি হয়নি। মুখ্যমন্ত্রী বা পুলিসকর্তাদর কউই এই ব্যাপার মুখ
খুলত চাইছন না। বশ কিছু সূত্র হাত এলও তদন্তর স্বার্থ কিছু জানাত রাজি নয় পুলিস।
পাশাপাশি, বিভিন্ন রাজ্য তদন্ত ও অভিযান চালিয়ও একমাত্র চুনুয়া ছাড়া অন্য কাউক ধরত না
পারায় পুলিসর একাংশ কিছুটা হতাশও। তব একশ্রণীর পুলিস অফিসার বলন- অপহৃত ব্যক্তিক
জীবিত উদ্ধারর আগ পর্যন্ত এসব মামলায় পুলিসক হাত গুটিয়ই বস থাকত হয়। বশি তত্পরতা
দখাল অপহৃতর জীবন সংশয় হত পার। ফল ২ আগস্ট পর্যন্ত সভাব তদন্ত শুরু করা যায়নি।
</p>

<p>এদিক, গুলিবিদ্ধ পার্থ
রায়বর্মনর চিকিত্সার জন্য দুবাইয় ওষুধ চয় পাঠানা হয়ছিল বল গায়ন্দা পুলিস জানত
পরছ। এই ব্যাপার প্রথম মুম্বইয় ও পর দুবাইয় টলিফান করা হয়। য টলিফান করছিল
তার স্বরর সঙ্গ পার্থবাবুর বাড়ির টলিফান থক রকর্ড করা কণ্ঠস্বর মিল যাচ্ছ। অনুমান
করা হচ্ছ, পার্থবাবুর শারীরিক অবস্হার অবনতি হওয়ায় আতঙ্কিত হয়ই অপহরণকারীরা ওষুধ চয়
পাঠিয়ছিল। </p>

<p>অপহরণর এক ঘণ্টার মধ্যই
খাদিম-কর্তার চিকিত্সার ব্যবস্হা হলও ঠিক কাথায় এই চিকিত্সা হয়, তা পুলিস জানত পারনি।
সংশ্লিষ্ট চিকিত্সকদরও চিহ্ণিত করা যায়নি। চিকিত্সকক পাওয়া গল তদন্ত বড় অগ্রগতি হব বল
পুলিস মন করছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>কাশ্মীর
উপদ্রুত এলাকা আইন জারি করুক কেন্দ্র : ফারুক</p>

<p>জম্মু কাশ্মীরর
হিংসা কবলিত অঞ্চলগুলিত উপদ্রুত এলাকা আইন জারি করার জন্য বুধবার কন্দ্রর কাছ আর্জি
জানিয়ছন ফারুক আবদুল্লা। এই আইন জারি হল কার্গিল ও লে বাদ রাজ্যর ১২টি জলা তার
আওতায় আসব। এই আইন সনাবাহিনী ও নিরাপত্তারক্ষীদর হাত অনিয়ন্ত্রিত ক্ষমতা দওয়া আছ।
ফারুক বলন, কাশ্মীরর বর্তমান সন্ত্রাসবাদর পিছন রয়ছ পাকিস্তান। তব, তাদর
পরিকল্পনা কানওদিনই সফল হব না। বুধবার ফারুক গভর্নমন্ট মডিক্যাল কলজ হাসপাতাল, সনা
হাসপাতাল ও কে এইচ এন হাসপাতাল গিয় জঙ্গিদর আক্রমণ আহত জওয়ানদর সঙ্গ দখা করন।

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>পুলিসর যা
কাজ</p>

<p>পার্থপ্রতিম রায়বর্মনক
কলকাতার উপকণ্ঠই টানা ৮দিন লুকিয় রাখা হয়ছিল। স্হানীয় কম্পাউণ্ডার, চিকিত্সকদর দিয়
তাঁর চিকিত্সাও করানা হয়ছ। তবু পুলিসর গায়ন্দারা কিছুই টর পায়নি। দূরভাষ বহু
দূর বস থাকা দুর্বৃত্তরা মুক্তিপণ নিয় দরকষাকষি করলও অপহরণ কর পার্থবাবুক আটক রখছিল
এই শহররই একটি সংগঠিত অপরাধচক্র। তাদর পশাদারি দক্ষতা পুলিসক তাক লাগিয় দিয়ছ।
পুলিস তাদর ধরত ব্যর্থ হল কলকাতার ব্যবসায়ী শিল্পপতিরা সমান্তরাল নিরাপত্তাবাহিনী গড় তালার
কথা ভাবত বাধ্য হবন হয়ত। </p>

<p>কিছুদিন আগও
অপরাধ দমন কলকাতা পুলিসর দক্ষতার সুনাম ছিল দশজাড়া। সই পুলিস এখন নিছক শাসকদলর
ধামাধরা। বছরর পর বছর রাজনীতির সঙ্গ জড়িয় থক আসল জায়গাটায় ফাঁকি থক গছ।
অপরাধীদর মধ্য পুলিসর কানও 'সূত্র' নই। মারাত্মক কিছু ঘট গল পুলিস অকূল
পাথার। রাজ্যর সর্বত্র তৈরি হয় গছ চুন্নু মিয়াঁরা। শাসক বা বিরাধী দলর আশ্রয় থক
তারা লক্ষ লক্ষ টাকা কামিয়ছ। অসত্ পথ অর্জিত সই অর্র্থর কিছুটা এলাকায় ছড়িয় তারা
সমাজসবী সজ বসছ। পুলিসর অফিসাররাও তাদর সলাম ঠাক। যুবভারতী-কাণ্ডর
অভিযুক্ত পিনাকী মিত্রও প্রামাটার সজ বসছিল। তার আসল ব্যবসা সমাজবিরাধীদর
নিরাপদ আশ্রয় দওয়া। ভাট জতার জন্য এইসব দুষ্কৃতীদর অবাধ ব্যবহার কর গছন
ডান-বাম সব দলর নতারাই। এই 'ফ্রাঙ্কনস্টাইন'দর এখন লাভ বড় গছ। এদর কউ কউ
এখন রাজনৈতিক দাদাদর পাত্তা না দিয় নানাবিধ অপরাধ হাত পাকাচ্ছ। পুলিসর দৗড় এদর
জানা। কলকাতাতও ব্যবসায়ী অপহরণর ঘটনা ঘটার একটি কারণ হল দিল্লি ও মুম্বইয় এখন প্রচণ্ড
কড়াকড়ি। পুলিসর টাস্ক ফার্স ভীষণ সক্রিয়। পাকিস্তানর ইন্টার সার্ভিসস ইনটলিজন্স-এর
চর বা আন্তঃরাজ্য দুষ্কৃতী- সকলরই নিরাপদ থাকার জায়গা এই কলকাতা। সল্টলকর বুক বস
এক জার্মান নাগরিক বআইনি ভি স্যাট বসিয় চরবৃত্তি চালিয় গছ নির্বিবাদ। প্রতিটি
খবরই যথষ্ট উদ্বগজনক। এখনও নড়চড় না বসল বিপদ। ফুটবল খল বা পাড়ার সাংস্কৃতিক
অনুষ্ঠান হাজিরা না দিয় পুলিস পুলিসর কাজ মন দিলই মঙ্গল- আশাকরি মুখ্যমন্ত্রী বুদ্ধদব
ভট্টাচার্য এতদিন তা উপলব্ধি করছন।</p>

<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>এরওয়াডিত
সব মানসিক চিকিত্সাকন্দ্র বন্ধর নির্দশ</p>

<p>এরওয়াডি মানসিক
চিকিত্সা কন্দ্র ভয়াবহ অগ্নিকাণ্ডর পরিপ্রক্ষিত রমানাথপুরম জলার ১৬টি চিকিত্সা কন্দ্র বন্ধ
করার নির্দশ দওয়া হয়ছ। এক সপ্তাহর মধ্য এই নির্দশ পালন করত হব বল বুধবার জলাশাসক
জানিয়ছন। </p>

<p>অবশ্য শুধু য
তামিলনাড়ুর মানসিক হাসপাতালই রাগীদর বঁধ রাখা হয় তা না। মাদুরাইয়র আলিমুদ্দিন
দরগা সংলগ্ন চিকিত্সা কন্দ্রগুলিতও রাগীদর সঙ্গ এইরকম নৃশংস ব্যবহারই করা হয় থাক।
অবশ্য নামই এগুলি হাসপাতাল বা চিকিত্সা কন্দ্র। কার্যত, খড়র ছাউনি দওয়া এক একটি ঘর
৮-১০ জনক গাদাগাদি কর আটক রাখা হয়। পশুর মত তাদর পায়ও পরিয় দওয়া হয় শিকল।
</p>

<p>এরওয়াডির ঘটনায়
তীব্র ক্ষাভপ্রকাশ করছ জাতীয় মানবাধিকার কমিশন। তামিলনাড়ু সরকারর কাছ থক সুপ্রিমকার্টও
পুরা ঘটনার রিপার্ট চয়ছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>জাত না মেন
বিয়- স্বামী-স্ত্রীর ফাঁসি</p>

<p>তথ্যপ্রযুক্তির চরম
উত্কর্ষতার পাশাপাশি ভারত অন্ধ কুসংস্কারর ভিত্তি কতদূর প্রাথিত তার প্রমাণ পাওয়া গল
আরও একবার। অন্য জাত বিয় করায় উত্তরপ্রদশর আলিপুর গ্রাম বুধবার ভার এক দম্পতিক
ফাঁসি দিয় মারা হয়ছ। ছল ও ময়র পরিবারর উদ্যাগই এই নৃশংস হত্যাকাণ্ড হয়। তা
প্রত্যক্ষ কর কয়ক শত মানুষ। কারুর মুখ থক প্রতিবাদর টুঁ শব্দও শানা যায়নি। </p>

<p>মুজফফরনগর থক
পাওয়া খবর অনুসার, ২০ বছরর বিশাল ও ১৮ বছরর সানু পরস্পরর প্রম পড়। জাত
আলাদা হওয়ায় দুজনর বাড়িতই এই সম্পর্ক নিয় আপত্তি ছিল। বিশাল ও সানু ভবছিল, বিয়
কর নিল দুজনর পরিবারই এটা মন নব। কয়কদিন আগ সইমত তারা গাপন বিয় কর।
জানাজানি হয় যাওয়ার পর আলিপুর গ্রাম তীব্র চাঞ্চল্য দখা দয়। সমাজপতিরা বিশাল ও সানুর
পরিবারক একঘর কর রাখার হুমকি দন। </p>

<p>বুধবার ভার বিশাল
ও সানুক জার কর একটি পরিত্যক্ত বাড়িত নিয় গিয় পরপর ছাদ থক ঝুলিয় ফাঁসি দওয়া
হয়। সানুর মা-বাবা ও বিশালর দাদা ঘটনাস্হল হাজির ছিল। পুলিস তাদর গ্রপ্তার করছ।
পাশাপাশি, সরকারর ভূমিকা কতটা নিন্দনীয় তার প্রমাণ মিলছ স্বরাষ্ট্রমন্ত্রী রামনাথ শর্মার
মন্তব্য। তিনি বলন, 'ফাঁসির পরও আলিপুরর পরিস্হিতি স্বাভাবিক। কাথাও কানও উত্তজনা
নই। যদি একটি পরিবার এই ঘটনা ঘটাত তাহল ঝামলা হত পারত।' 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>আবার বন্ধ হয়
যেত পার কৃষ্ণা গ্লাস</p>

<p>ফর বন্ধ হয় যত পার
কৃষ্ণা গ্লাস ফ্যাক্টরি। বর্তমান বারুইপুরর কারখানাটিত দৈনিক ১ লক্ষ টাকা লাকসান হচ্ছ।
এই বিপুল লাকসানর বাঝা টন কতদিন কারখানাটি চালু রাখা যাব তা নিয় সন্দহ দখা
দিয়ছ। দশ বছর বন্ধ থাকার পর গত ডিসম্বর মাস খুলছিল কৃষ্ণা গ্লাস। ভাটর মুখ এই
ঘটনাক 'দৃষ্টান্তমূলক ঘটনা' বল চিহ্ণিত করছিল রাজ্য সরকার। কিন্তু এক বছরর আগই
আবার সখান সঙ্কটর ছায়া। </p>

<p>১৯৮৯ সাল কৃষ্ণা
গ্লাসর যাদবপুর ও বারুইপুর- দুই কারখানাতই তালা পড়। এক সময় পশ্চিমবঙ্গর গর্বর বিষয়
ছিল কৃষ্ণা গ্লাস। এই সংস্হার তৈরি কাচর জিনিসর বিপুল কদর ছিল দশ জুড়। বন্ধ হয়
যাওয়ার পর রাজ্য সরকার সটি অধিগ্রহণ কর চালানার চষ্টা কর। ২০০০ সালর ডিসম্বর
বারুইপুর কারখানা চালু হলও যাদবপুরর ফ্যাক্টরি চালু করা যায়নি। </p>

<p>উত্পাদন শুরু হলও
পুরনা মর্যাদা আর ফির পায়নি কৃষ্ণা গ্লাস। বরং পরিকাঠামার অভাব ও শ্রমিকদর দাবি
দাওয়া নিয় পরিস্হিতি ঘারালা হত শুরু কর। ইতামধ্য কাচ শিল্প প্রায় বিপ্লব ঘট গছ।
অত্যাধুনিক প্রযুক্তি কাজ লাগিয় তৈরি হচ্ছ নানা সামগ্রী। দশ বছরর ব্যবধান মিটিয়
প্রতিযাগিতায় টিঁক থাকত গিয়ও সমস্যায় পড় কৃষ্ণা গ্লাস। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>কলকাতা
টোকিও-র মতই ভূগর্ভ ডুব যাচ্ছ</p>

<p>কলকাতা ক্রমশ ভূগর্ভ
বস যাচ্ছ। জিওলজিক্যাল সার্ভ অফ ইন্ডিয়া (জি এস আই)-র অধিকর্তা রবিশঙ্কর গত মঙ্গলবার
একথা জানিয় বলন, মাটির মধ্য বস যাওয়ার দরুণ কলকাতা শহরর সমূহ ক্ষতি হচ্ছ।
</p>

<p>সমুদ্রাপকূল অবস্হিত
অন্যান্য শহরর মতই কলকাতা ক্রমশ নিচ নম যাচ্ছ। এর প্রধান কারণ, বহুতল বাড়ি নির্মাণর
সংখ্যাবৃদ্ধিত যথচ্ছাচার। অতিরিক্ত পরিমাণ ভূগর্ভস্হ জল তুল নওয়ার জন্যও এমন ক্ষতিকর
অবস্হা তৈরি হচ্ছ। জাপানর টাকিও শহরটিরও ঠিক এমনই হাল। তবুও দৈনিক কী পরিমাণ
কলকাতা ভূগর্ভ ডুব যাচ্ছ তার পরিমাপ করা যাচ্ছ না এখনও। একটি অ-সরকারি সংগঠন এর
প্রতিকার কাজ করলও জানিয়ছ, একমাত্র জি এস আই এই পরিমাপ নির্ণয় করত সক্ষম। রবিশঙ্কর বলন,
প্রচুর পরিমাণ ভূগর্ভস্হ জল তুল নওয়ায় অবশিষ্ট জল অতিরিক্ত লবণাক্ত হয় পড়ছ, তাক
আর ব্যবহার করা যাচ্ছ না। তমনই সুউচ্চ বাড়ির ভঙ পড়ার যথষ্ট সন্দহ বা আশঙ্কা থক
যাচ্ছ। তিনি বলন, কলকাতার কানও অঞ্চলই গৃহনির্মাণর নিয়মাবলী মানা হয় না। জাপান
বাড়ি নির্মাণর সময় প্রতি তলার গঠন ব্যবহৃত হয় উপযুক্ত ধরনর বল বিয়ারিং- যাত
বাড়িগুলি যকানও ধরনর ভূ-আলাড়ন সামাল দিত পার। দশর এই ধরনর নানা ক্ষতি রুখত
সারা দশর পরিবশ সংরক্ষণর নিয়ম তিনি ব্যাপক রদবদল দাবি কর বলছন,
'প্রাকৃতিক বিপর্যয়ক বন্ধ করা যাব না। কিন্তু সগুলিক আগ থক আন্দাজ করার
বিদ্যা আয়ত্ত করতই হব।' সজন্য পরিবশ সচতনতা বৃদ্ধিতই তিনি জার দিয়ছন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p>পাক দাবাড়ুক
হাজিরা দিত স্পেশাল ব্রাঞ্চ</p>

<p>কলকাতার অ্যালখাইন চস
ক্লাবর আয়াজন গার্কি সদন ৯ থক ১৭ আগস্ট তারিখ অনুষ্ঠিতব্য তৃতীয় এশিয় দাবা
প্রতিযাগিতায় বিভিন্ন দশর ৭০ জন প্রতিযাগীর সঙ্গ অংশগ্রহণ করত আসছন পাকিস্তানর
মহম্মদ লাধিও। কিন্তু শুধু পাকিস্তানর নাগরিক হওয়ার জন্যই তাঁর ওপর এক অদ্ভুত নির্দশ
জারি হয়ছ। খলার আগ বা পর প্রতিদিন তাঁক একবার কর কলকাতা পুলিসর স্পশাল ব্রাঞ্চর
অফিস হাজিরা দিয় আসত হব। আয়াজক সংস্হার সম্পাদক সৗমন মজুমদার এই বিষয় তাঁর
অস্বস্তি এবং উষ্মা প্রকাশ করছন। যদিও লাধি এদশ নতুন নন। তিনি এর আগ গুডরিক
আন্তর্জাতিক দাবা প্রতিযাগিতায় দুবার অংশগ্রহণ কর গছন। সৗমনবাবু প্রশ্ন তুলছন,
পাক ক্রিকট দল তা এতবার ভারত বা কলকাতায় খলত এসছ। কান বছর, কান দলর, কান
খলায়াড়ক এইভাব স্পশাল ব্রাঞ্চ হাজিরা দিত হয়ছ! এই নির্দশ মানবিকতার দিক থক
বিচার করল শুধু সমস্যাজনকই নয়, রীতিমত অবমাননাকরও। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p>আজলান শাহ
হকিত আবার হার 
কমনওয়লথ
গেমস হকিত ভারতর যোগ্যতা পাওয়া অনিশ্চিত</p>

<p>কুয়ালালামপুর আজলান
শাহ হকিত ভারত আবার হরছ। মঙ্গলবার অস্ট্রলিয়া ৩-২ গাল হারায় ভারতক। সমাপ্তির ৩০
সকণ্ড আগ অস্ট্রলিয়ার ট্রয় এলডারর পনাল্টি কর্নার অস্ট্রলিয়া জিতছ। </p>

<p>অথচ ভারত ১৩ মিনিটর
সময় দীপক ঠাকুরর গাল ১-০ এগিয়ছিল। ১৬ মিনিট অস্ট্রলিয়ার ১-১ হয় জমি ডুইয়ারর গাল।
ম্যাথু স্মিথ ৩৭ মিনিট ২-১ করন। মুকশ কুমার তারপর ২-২ কর সমতা আনন। কিন্তু ৭০
মিনিট ট্রয় ৩-২ কর এগিয় দন অস্ট্রলিয়াক। </p>

<p>আজলান শাহ-এর এই
পরাজয় ভারতক আট দশর কলমনওয়লথ গমস হকিত যাগ্যতা থক অনকটা দূর সরিয় দিল।
ইন্টারন্যাশনাল হকি ফডারশনর নিয়ম : ৬টি অঞ্চল থক ৬টি দশ কমনওয়লথ গমস যাগ্যতা
পাব। সংগঠক দশ সরাসরি যাগ্যতা পয় থাক। কিন্তু প্রতিযাগী থাক আটটি দশ। ভারত যহতু
গত এশিয়ান গমস সানা জয়ী, তাই তাদর ডাকা হত পার। কিন্তু পাকিস্তান এশিয়া অঞ্চলর সরা
দশরূপ কমনওয়লথ গমস যাগ্যতা পয়ছ। ভারতর অংশগ্রহণ নির্ভর করব আন্তর্জাতিক ফডারশনর
ওপর। </p>

<p>আজলান শাহ-প্রতিযাগিতায়
ভারত এপর্যন্ত হরছ পাকিস্তানর কাছ ৩-৪ গাল, জার্মানির কাছ ০-২ গাল ও অস্ট্রলিয়ার
কাছ ২-৩ গাল। কবল ২-২ কর মালয়শিয়ার সঙ্গ। ভারতর খলা বাকি দক্ষিণ কারিয়া ও
ইংল্যান্ডর সঙ্গ। অন্তত একটিত জিতল সাত দশর এই প্রতিযাগিতায় পঞ্চম বা ষষ্ঠ স্হান পত
পারব। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বিশ্বনাথন
আনন্দ ৩ পয়ন্ট পেলই পৌঁছ যাবন 'ম্যাজিক ২৮০০'-এ</p>

<p>ভারতর বিশ্ব
চ্যাম্পিয়ন গ্র্যান্ডমাস্টার দাবাড়ু বিশ্বনাথন আনন্দ এলা রটিং-এ 'ম্যাজিক ২৮০০' থক
মাত্র ৩ পয়ন্ট দূর রয়ছন। গত জুলাইয় আন্তর্জাতিক দাবা সংস্হা ফিড য এলা রটিং
তালিকা প্রকাশ করছ, তাত আনন্দর পয়ন্ট ২৭৯৭। তব এত মক্সিকার মরিডা প্রতিযাগিতার
ফল ধরা হয়নি। আনন্দ ওত চ্যাম্পিয়ন হয়ছিলন। ওটা ধরল তিনি ২৮০০-ত পৗঁছ যাবন।
</p>

<p>পরবর্তী এলা তালিকা
প্রকাশ করা হব অক্টাবর। তখন আনন্দর অবস্হান কী হব বাঝা যাচ্ছ না। কারণ জার্মানির
ডর্টমুণ্ড প্রতিযাগিতায় আনন্দর স্হান হয়ছিল সর্বনিম্ন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p>বড় ম্যাচ নিয়
আশাবাদী দুই প্রধানই</p>

<p>শনিবার যুবভারতী
ক্রীড়াঙ্গন ইস্টবঙ্গল ও মাহনবাগানর লিগ ফুটবল দুই শিবিরই সাফল্য নিয় আশাবাদী। বড়
ম্যাচর আগ শষ খলায় মঙ্গলবার মাহনবাগান ২-০ গাল পার্ট ট্রাস্টক হারিয় বশ
উদ্দীপ্ত। সরিকি ও আর সি প্রকাশর গাল খুশি কাচ সুব্রত ভট্টাচার্য। ম্যাচ শষ তিনি বলন,
দল ছন্দ আছ। সম্মিলিতভাব ওরা খলছ। 'ব্যারটা ও বাসুদব না থাকা সত্ত্বও দল
ঠিক আছ'- মন্তব্য কাচর। শনিবারর আগ তিনি দলক আরও চাঙ্গা করার দিক লক্ষ্য রাখবন।
</p>

<p>ওদিক আই এম বিজয়ন
ইস্টবঙ্গল সই করছন মঙ্গলবার। ওর সইয়র পর কাচ মনারঞ্জন ভট্টাচার্য বলন, বড় খলায়াড়দর
সুবিধা ওঁরা দ্রুত পরিস্হিতির সঙ্গ মানিয় নিত পারন। বিজয়ন নিজও শনিবারর ম্যাচ সম্পর্ক
বলন, 'তিনদিনর অনুশীলন সব ঠিক হয় যাব। আর লন্ডন তা সব ম্যাচই খলছি।'
</p>

<p>বড় ম্যাচর আগ শষ
ম্যাচ জর্জ টলিগ্রাফর সঙ্গ ইস্টবঙ্গল একটুও ভাল খলনি। মনারঞ্জন এনিয় চিন্তিত থাকলও
বিজয়ন এস যাওয়ায় কিছুটা স্বস্তিত আছন। বিজন সিং-ও ক্ষমা চয় চিঠি দিয়ছন। ক্লাবর
কর্মসমিতি অনুমতি দিল তিনিও শনিবার খলবন। </p>

<p>বড় ম্যাচ ভিড় হব ভব
আই এফ এ ৭০ হাজার টিকিট বিক্রির সিদ্ধান্ত নিয়ছ। এছাড়া বিনামূল্যর কয়ক হাজার টিকিট
দব জানিয়ছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বৃহস্পতিবার
এশিয় দাবার উদ্বাধন 
কলকাতায়
খেলবন ৩০ জন গ্র্যান্ডমাস্টার</p>

<p>বৃহস্পতিবার কলকাতায়
গার্কি সদন উদ্বাধন হব তৃতীয় এশিয়ান দাবা চ্যাম্পিয়নশিপ। প্রতিযাগিতা শুরু শুক্রবার।
কলকাতায় দাবার এত বড় প্রতিযাগিতা এই প্রথম। কারণ ৮৪ জনর এই প্রতিযাগিতায় খলবন ৩০
জন গ্র্যান্ডমাস্টার ও দুজন প্রাক্তন জুনিয়র বিশ্বচ্যাম্পিয়ন। এত ভারত থক সর্বাধিক সংখ্যক
দাবাড়ু ১২ জন খলবন। তারপর ১০ জন কর চীন ও ফিলিপিন্স থক। কাজাকস্তানর খলবন ৭
জন। বাংলাদশর ৪ জন। </p>

<p>গার্কি সদন এলা রটিং-এর
সর্বাধিক পয়ন্ট পাওয়াদর মধ্য থাকবন চীনর গ্র্যান্ডমাস্টার ঝং ঝাং। ওদর আরও ৪ জন খলবন
যাঁরা ২৬০০-এর  ওপর আছন। বিশ্ব ক্রম তালিকায় ঝাং আছন ২৬ নম্বর। তাঁর এলা পয়ন্ট
২৬৬৭। </p>

<p>কলকাতায় খলবন ২৫
জন ইন্টারন্যাশনাল মাস্টারও। এঁদর কয়কজনর ডাবল জি এম নর্ম আছ। যথা- ভারতর ভি ভি
প্রসাদ ও সন্দীপন চন্দ এবং ইরানর এশান ঘায়ম মাঘামি। আরও ১৮ দশর দাবাড়ুরা ১১ রাউণ্ডর
সুইস লিগ ফরম্যাটর এই প্রতিযাগিতায় অংশ নবন। </p>

<p>৬৪ হাজার ডলার
পুরস্কার অর্থর এই প্রতিযাগিতায় চ্যাম্পিয়ন পাবন ১২ হাজার ডলার। ওখান থক সরা ১০
জন বিশ্বদাবার জন্য যাগ্যতা পাবন। </p>

<p>ভারতীয় গ্র্যান্ডমাস্টারদর
মধ্য অংশ নবন কৃষ্ণণ শশীকিরণ, অভিজিত্ কুন্ত, দিব্যন্দু বড়ুয়া ও প্রভীন থিপস। আর খলবন
পি হরিকৃষ্ণ, সূর্যশখর গঙ্গাপাধ্যায়, ভি ভি প্রসাদ, শ্রীরাম ঝা, লঙ্কা রবি, পি কঙ্গুভল,
নীরজ মিশ্র ও সন্দীপন চন্দ। </p>

<p> </p>






</body></text></cesDoc>