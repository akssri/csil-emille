<cesDoc id="ben-w-bengalnet-news-00-07-11" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-00-07-11.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 00-07-11</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>00-07-11</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>কশপুর ফর রণক্ষত্র,
দিনভর লড়াই, হত ২</p>

<p>ফর
উত্তপ্ত হয় উঠছ মদিনীপুরর কশপুর। গত ২৪ ঘন্টায় কয়ক হাজার সশস্ত্র সিপিএম কর্মী ও
সমর্থক তৃণমূলর প্রভাবাধীন ১০-১২টি গ্রাম উদ্ধার করত ব্যাপক সন্ত্রাস চালায়। তৃণমূল-বিজপি
সমর্থকরা সাধ্যমত বাধা দিত গল খণ্ডযুদ্ধর সৃষ্টি হয়। অজস্র গুলি ও বামা ছাঁড়া হয়।
সিপিএমর হামলায় দুজনর মৃতু্য হয়ছ। এদর মধ্য একজন স্কুলছাত্র ও অন্যজন তৃণমূলর
সমর্থক। গত চারদিন সিপিএম-তৃণমূলর সংঘর্ষ চারজন তৃণমূল সমর্থকর মৃতু্য হল। ঘটনাস্হল
পরিদর্শন শাভনদব চট্টাপাধ্যায়র নতৃত্ব তৃণমূল কংগ্রসর এক প্রতিনিধিদল সামবার রাতই
মদিনীপুর রওয়ানা হয় গছন। </p>

<p>গত
চারদিন য তৃণমূল কর্মীর মৃতু্য হয়ছ তাঁরা হলন অরুণ মণ্ডল (২৮), নৃপন দলুই (২৫) ও
তাজিজুল খাঁ (১৪)। এর মধ্য তাজিজুল স্কুলছাত্র। সামবার বলা এগারাটা নাগাদ স মাঠ
গরু চরাচ্ছিল। সিপিএম সমর্থকদর ছাঁড়া দূরপাল্লার গুলিত স প্রাণ হারায়। নজরুল ইসলাম
নাম আর এক সমর্থকর পট তীর লাগল প্রথম তাক পুলিশি প্রহরায় সদর হাসপাতাল পাঠানা
হয়। সখান শারীরিক অবস্হার অবনতি ঘটল নজরুলক কলকাতার পি জি হাসপাতল ভর্তি করা
হয়। </p>

<p>রবিবার
বিকল থকই তৃণমূল প্রভাবিত মহিষদা, সরুই প্রভৃতি গ্রামগুলিত হামলার প্রস্তুতি শুরু কর
সিপিএম। এলাকা ও বহিরাগত মিলিয় কয়ক হাজার সিপিএম সমর্থক জড়া হয়। কশপুর বাজার
থানার সামন একনলা-দানলা বন্দুক, পিস্তল, মাস্কট, তলায়ার, তীরধনুক, পাইপগান,
হাতকামান নিয় তারা স্লাগান দিত থাক। জলা সদর থক পাওয়া নির্দশ অনুসার পুলিশ
বা ই এফ আর জওয়ানরা কউ থানা থক বর হননি। আতঙ্কিত তৃণমূল নতারা বারবার মদিনীপুর
ফান কর এস পি বা জলাশাসকর সঙ্গ যাগাযাগর চষ্টা করও ব্যর্থ হন। </p>

<p>সরুই
গ্রাম সামবার দুই পক্ষর সংঘর্ষ যখন তুঙ্গ তখন অবশ্য ই এফ আর শূন্য দশ রাউণ্ড গুলি চালায়।
এত যুযুধান দুই পক্ষই ছত্রভঙ্গ হয় যায়। </p>

<p> </p>

<p>দলিত নির্যাতনর
দায় বিহার মন্ত্রী বরখাস্ত</p>

<p>দুই
দলিত ট্রাক ড্রাইভার ও খালাসিক ৩২ দিন বাড়িত আটক রখ অত্যাচার চালানার অভিযাগ
সামবার বিহারর সমবায় মন্ত্রী ললিত যাদবক বরখাস্ত করা হয়ছ। মুখ্যমন্ত্রী রাবড়ি দবীর
অনুরাধ রাজ্যপাল বিনাদচন্দ্র পাণ্ড ললিতক বরখাস্ত করন। অভিযুক্ত মন্ত্রীক ছয় বছরর জন্য
রাষ্ট্রীয় জনতা দল থক সাসপণ্ড করা হয়ছ। ভারতীয় দণ্ডবিধির একাধিক ধারায় তাঁর বিরুদ্ধ
মামলাও দায়র হয়ছ। </p>

<p>আটক
ড্রাইভার ও খালাসিক শনিবার গভীর রাত পুলিশ ললিত যাদবর বাড়ি থক উদ্ধার কর। তাদর
দুজনর শারীরিক পরিস্হিতি খুব খারাপ। হাত-পা বঁধ মারার ফল দীননাথ বাইথা নাম
ড্রাইভারর হাত-পা-এর হাড় ভঙ গছ। প্লাস দিয় টন তার পায়র নখ উপড় ফলা হয়ছ।
</p>

<p>ঘটনার
কথা ছড়িয় পড়ার সঙ্গ সঙ্গ রাজনৈতিক মহল তীব্র প্রতিক্রিয়ার সৃষ্টি হয়ছ। যহতু অত্যাচার
হয়ছ দুই দলিত ব্যক্তির ওপর, তাই পিছড় বর্গ-এর 'মিসা' লালুপ্রসাদ যাদবও অস্বস্তিত
পড়ছন। বিরাধী বিজপি এমনকী কংগ্রসর পক্ষ থকও লালুপ্রসাদর ওপর তীব্র চাপ সৃষ্টি
করা হয়ছ। </p>

<p>সামবার
রাত লালুপ্রসাদ বলন, স্বরাষ্ট্র সচিব ও পুলিশর ডিরক্টর জনারলর রিপার্ট পাওয়ার পরই
মুখ্যমন্ত্রী ললিত যাদবক বরখাস্তর সুপারিশ করছন। 'আইন কউই নিজর হাত তুল
নিত পারন না। এই ধরনর ঘটনা ঘটল তিনি যই হান তাঁক ক্ষমা করা হব না।' জাতীয়
মানবাধিকার কমিশন বিহার সরকার ও পুলিশর ডিরক্টর জনারলর কাছ থক ঘটনার পূর্ণাঙ্গ
রিপার্ট চয়ছ। ক্ষতিপূরণর দওয়ার নির্দশও দওয়া হয়ছ রাজ্য সরকারক। </p>

<p> </p>

<p>কন্দ্রর শাসক জাট
থাকা নিয় দ্বিমত ফারুকর দল</p>

<p>জম্মু
কাশ্মীরর স্বশাসনর প্রস্তাব কন্দ্রীয় মন্ত্রিসভায় খারিজ হয় যাওয়ার পর ফারুক আবদুল্লার
ন্যাশনাল কনফারন্স এন ডি এ জাট-এ থাকব কি না, তা নিয় সংশয় দখা দিয়ছিল। ফারুক
নিজ অবশ্য এন ডি এ-র সঙ্গ থাকার পক্ষই মত দিয়ছিলন। তব কন্দ্রর শাসক জাট থাকা না
থাকা নিয় ন্যাশনাল কনফারন্সর কার্যনির্বাহী সমিতির গতকাল য বৈঠক শ্রীনগর ডাকা হয়ছিল,
তাত কিন্তু দলীয় কর্মীরা এন ডি এ জাট থক যাওয়ার বিপক্ষই মত দিয়ছন। বশ কয়কজন
জানিয়ছন, স্বশাসনর প্রস্তাবটি জম্মু ও কাশ্মীর ন্যাশনাল কনফারন্সর রাজনৈতিক ভিত্তির সঙ্গ
জড়িত ছিল। কাজই কন্দ্র তাক খারিজ কর দওয়ার সিদ্ধান্ত যদি চুপচাপ মন নয় ন্যাশনাল
কনফারন্স, তা দলর বিশ্বাসযাগ্যতার ওপর একটি বড় প্রশ্নচিহ্ণ এঁক দব। কাজই দলর এন
ডি এ'র সিদ্ধান্তর বিরাধিতা কর জাট ত্যাগ করা উচিত। অন্যদিক, দলর কিছু প্রবীণ সদস্য
বলছন, মতর দিক থক বিরাধ থাকলও, এই মুহূর্ত দলর উচিত এন ডি এ জাট থক বরিয়
না আসা। কারণ কাশ্মীর উগ্রপন্হা দমন কন্দ্রর সাহায্য প্রতি পদ প্রয়াজন রাজ্যর।
কার্যনির্বাহী সমিতির ওই বৈঠক ৩১ সদস্যর প্রত্যকই নিজর মতামত এখনও জানাত পারন নি।
কাজই বৈঠক এখনও চলছ। </p>

<p> </p>

<p>'ধর্ষিতা'
বালিকাক ত্রিপুরা বিধানসভায় আনলন বিরাধীরা</p>

<p>গাটা
দশর সংসদীয় ইতিহাস প্রায় নজিরবিহীন ঘটনা সামবার ঘটল ত্রিপুরা বিধানসভায়। মাত্র সাত
বছরর এক বালিকাক ধর্ষণ করা হয়ছ, এই অভিযাগ কংগ্রসি বিধায়করা বিধানসভা তালপাড়
করন। বাচ্চা ময়টিক কাল কর সাংবাদিকদর জন্য নির্দিষ্ট আসন বসিয় ধর্ষণ অভিযুক্ত
ব্যক্তির শাস্তি দাবি ও পুলিশি নিষ্ক্রিয়তার অভিযাগ প্রচণ্ড চিত্কার-চঁচামচি করন
বিধায়করা। অবশষ মুখ্যমন্ত্রী মাণিক সরকার 'আজই খাঁজ নিয় আশ্বাস দব' বলার পর
সভা শান্ত হয়। </p>

<p>প্রশ্নাত্তর
পর্বর শষদিক ঘটনাটির কথা তালন বিরাধী দলনতা জহর সাহা। তিনি বলন, মাহনপুর
সাত বছরর একটি বালিকাক বকারির মধ্য ডক নিয় ধর্ষণ কর স্হানীয় এক যুবক। মুখ
গামছা বঁধ এই পাশবিক অত্যাচার চালানায় ময়টি অসুস্হ হয় পড়। তাক ইন্দিরা গান্ধী মমারিয়াল
হাসপাতাল পাঠানা হলও পুলিশ রিপার্ট ছাড়া সখানকার ডাক্তাররা চিকিত্সা করত রাজি
হননি। ফল ওই রাতই সিধাই থানায় ডায়রি করার পর ময়টির চিকিত্সা শুরু হয়। </p>

<p>স্পিকারর
উদ্দশ্য জহরবাবু বলন, অভিযুক্ত প্রবাধ দব এলাকায় ইচ্ছমত ঘুর বড়ালও পুলিশ তাক গ্রফতার
করছ না। জহরবাবুর কথা শষ না হতই কংগ্রস বিধায়ক রতনলাল নাথ সাত বছরর ময়টিক কাল
নিয় বিধানসভার ওয়ল ঢুক পড়ন। চারপাশর চিত্কার-হইচই'র মধ্য জড়াসড়া ময়টি
অবাক হয় চারপাশ দখত থাক। </p>

<p>মুখ্যমন্ত্রী
মাণিক সরকার সমস্তরকম ব্যবস্হা নওয়ার আশ্বাস দিলও কংগ্রস বিধায়করা সংশ্লিষ্ট পুলিশ
অফিসারক বরখাস্ত করার দাবি জানান। মাণিকবাবু কিছুটা ঘাবড় গিয় বলত থাকন-
'এভাব কি কাউক সাসপণ্ড করা যায়। আমাক স্বরাষ্ট্র দফতরর সঙ্গ কথা বলত হব। সম্ভব
হল বিকলর মধ্যই ব্যবস্হা নওয়া হব।' এরপর বিরাধীরা শান্ত হন। </p>

<p> </p>

<p>আসফা কংগ্রস
একমাত্র ভারতীয়</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>দক্ষিণ কারিয়ার
পুসান 'আসফা'র (এশিয়ান স্পার্টস ফর অল অ্যাসাসিয়শন) ষষ্ঠ কংগ্রস শুরু হচ্ছ
মঙ্গলবার ১১ জুলাই। চলব ১৪ জুলাই পর্যন্ত। এশিয়া ও ওসনিয়ার নানা দশর স্বাস্হ্য ও সমাজ
উন্নয়ন আসফা কাজ করছ। এরা ভারত বশি কাজ করত চায়। তব স্বাস্হ্য উন্নয়ন চিরাচরিত খলাধুলা
নয়। এরা অপ্রচলিত দশি খলাগুলির উন্নয়ন আগ্রহী। আর একারণই সম্ভবত কলকাতার যিনি কবাডিক
এশিয়ান গমসর অন্তর্ভুক্তির জন্য সবচয় প্রয়াসী ছিলন (ভারত কবাডিত ১৯৯০, ১৯৯৪ ও
১৯৯৮-এ চ্যাম্পিয়ন) সই অচিন্ত্য সাহাক আসফা ডক নিয় গল পুসান। আসফার ডিরক্টর মাট
১০ জন। এরা ১০ দশর। অচিন্ত্যবাবু একমাত্র ভারতীয়, যিনি এই সম্মান পলন। </p>

<p> </p>

<p>
  </p>

<p> </p>

<p>আজকাল</p>


 সভাপতি : দিল্লি ছুটলন রাজ্যর বি জ পি নতারা 
     
 মাহনবাগান পনাল্টি পল না 
     অনীতর দারুণ গাল
     দীপন্দু আরও ভাল  
 মমতা বললই ছাড়ব বিধায়ক পদ : সুব্রত
     
 কান্তিই হলন বিরাধী নতা 
 শ্যমাপ্রসাদ:
     বুদ্ধর পাশ দাঁড়ালন বসু 


<p>আনন্দবাজার 
পত্রিকা</p>


 ১২ গ্রাম তছনছ কর দিল সিপিএম : পুলিশ বলছ,
     দখা ছাড়া কিছু করার নির্দশ নই 
     গুলি-বামায় কশপুর
     রণক্ষত্র, স্কুলছাত্র নিহত  
 মমতার ডাক সারা দিয়ই প্রতিনিধিদল পাঠাচ্ছ
     দিল্লি 
 প্রকাশ্য সুদর্শনর নিন্দায় বি জ পি
     
 বসু যা-ই বলুন, দল না-ভাঙার আশ্বাস দননি
     বিক্ষুব্ধরা 
 ছুটির
     দিনই কাজ শুরু কর দিলন নতুন ময়র 


<p>গণশক্তি</p>


 দুই গ্রামর ১৫০ বাড়ি ভস্মীভূত 
     সবংয় তৃণমূলী
     হামলা, অপহৃত ২৫ পার্টিকর্মী 
 লন্ডন ব্রিটিশ শিল্পপতিদর সভায় বসু
     
     বিনিয়াগ সবচয়
     নিরাপদ রাজ্য পঃবঙ্গ 
 জাট থাকবন কিনা, আজ সিদ্ধান্ত নবন
     ফারুকরা 
 মগাসিটি
     প্রকল্প বরারদ্দর দাবিত আন্দালন অব্যাহত রাখব বামফ্রন্ট 


<p>প্রতিদিন</p>


 প্রতিনিধি দল পাঠালন মমতা 
     কশপুর জুড়
     তৃণমূল-সিপিএম যুদ্ধ, হত ২ 
 কশপুর কন্দ্র হস্তক্ষপ করুক মমতার দাবি
     
 নাগরিক পরিষবা বাড়াবন 
     
     বিধায়কপদ এখনই
     ছাড়ছন না সুব্রত  
 ময়রক চাপ রাখত রাস্তায় নামার সিদ্ধান্ত
     বামফ্রন্টর  
 কলকাতা পুরসভা কন হাতছাড়া? 
     
     পার্টির কর্মসূচি লাক
     নিচ্ছ না : সইফুদ্দিন 
 জঘন্য রফারিং,
     অনীতর গাল চ্যাম্পিয়ন ইস্টবঙ্গল  


<p>বর্তমান</p>


 কশপুর, সবং-এ সংঘর্ষ তুঙ্গ, লুট, আগুন,
     মৃতু্য বড় ৫ 
 কলকাতা পুরসভার কাজ তদারকি করত কমিটি
     গড়ছন মমতা 
 কংগ্রস বিধায়ক পদ থক ইস্তফা দবন না
     সুব্রত  
 কিছু কংগ্রস বিধায়ক যত পারন তৃণমূল, সানিয়ার
     আশঙ্কা 
 তড়িত্ তাপদারক বহিষ্কারর দাবি 
     
 অসীম
     বর্মনক সরিয় নিচ্ছ রাজ্য সরকার  


<p> </p>






</body></text></cesDoc>