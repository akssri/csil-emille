<cesDoc id="ben-w-bengalnet-news-01-08-18" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-08-18.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-08-18</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-08-18</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>বেতন ও
ভাতা নিয় সাংসদরা পেত চলছন মাস ৪৬ হাজার</p>

<p>সাংসদদর বতন ও
ভাতা এক ধাক্কায় মাস ২৫ হাজার টাকা বাড়ত চলছ। এই অঙ্ক সাংসদদর বর্তমান বতন ও
ভাতার মিলিত পরিমাণর চয়ও বশি। বর্তমান সব মিলিয় সাংসদরা মাস ২১,৫০০ টাকা
পান। এবার তা বড় হত চলছ ৪৬ হাজার টাকা। এই মর্ম একটি বিল মন্ত্রিসভায় অনুমাদিত
হয়ছ। আগামী সপ্তাহ সংসদ তা পশ হব। </p>

<p>বিলটিত মাসিক বতন
৪ হাজার টাকা থক বাড়িয় ১২ হাজার টাকা করার প্রস্তাব রয়ছ। বাড়ত চলছ সংসদ
হাজিরা ভাতা। অধিবশন চলাকালীন সংসদ এল প্রতিদিন এম পি-রা ৪০০ টাকা পান। সটা বড়
হব ৫০০ টাকা। এছাড়াও টলিফান নিঃশুল্ক কলর সংখ্যা বছর ১ লক্ষ থক বাড়িয় ১ লক্ষ ২০
হাজার করা হয়ছ। বিদু্যত্ ও জলর ইউনিটর পরিমাণও দ্বিগুণ বাড়ছ। শুধু তাই নয়, প্রত্যক
সাংসদক নিখরচায় মাবাইল ফানও দওয়া হব। তব বিলর খরচা তাঁদর নিজদর। বতন
বৃদ্ধি সংক্রান্ত বিল পাঁচ বছর অন্তর বতন ও ভাতা সংশাধনর প্রস্তাব রয়ছ। </p>

<p>বামপন্হীরা সংসদ বতন
বৃদ্ধির প্রতিবাদ করবন বল জানা গছ। অবশ্য বতনর পুরাটাই তাঁরা 'লভি' হিসব
দলক দিয় দন। ভাতা ও অন্য সূত্র পাওয়া অর্থ অবশ্য তাঁরা রাখত পারন। 
</p>

<p> </p>

<p> </p>

<p>পুরপ্রধান
হত্যার কিনারা দু'একদিনই, বলছ পুলিস</p>

<p>দমদমর পুরপ্রধান শৈলন
দাস হত্যাকাণ্ডর কিনারা রবিবারর মধ্যই হয় যাব বল পুলিস ও গায়ন্দারা দাবি করছন।
পুলিসর বিবৃতি অনুযায়ী, হত্যাকাণ্ডর প্রধান পাণ্ডা বিশ্বনাথ। বিশ্বনাথর বাড়ি উত্তর ২৪
পরগনার সন্দশখালি গ্রাম। শুক্রবার সখান তল্লাশি চালিয়ও পুলিস তাক ধরত পারনি। তব
গ্রপ্তার করা হয়ছ ইব্রাহিম ওরফ জামাই নাম এক দুষ্কৃতীক। স খালার বাসিন্দা।
</p>

<p>মাটর সাইকল ভাড়া
নিত প্রথম বিশ্বনাথ ইব্রাহিমর কাছই এসছিল। নিজর কাছ শক্তপাক্ত কানও গাড়ি না
থাকায় ইব্রাহিম বিশ্বনাথক আগরপাড়ার দিলীপ শীলর কাছ নিয় যায়। ঘটনার আগর দিন
ঘণ্টা পিছু ৫০ টাকা ভাড়ায় বিশ্বনাথ দিলীপর কাছ থক একটি মাটর সাইকল ভাড়া নয়। পরর
দিন তার গাড়ি ফরত দওয়ার কথা ছিল। বিশ্বনাথক ইব্রাহিম নিজর হলমটও দিয়ছিল।
ঘটনাস্হল থক পালানার সময় দুষ্কৃতীরা মাটরবাইকর সঙ্গ হলমটটিও ফল যায়। গাঢ়
সবুজ রঙর হলমটর মালিক য ইব্রাহিম-ই সটা তার মা-ও স্বীকার করছন। জিজ্ঞাসাবাদর
জন্য ইব্রাহিমর বাবা-মা, স্ত্রী এবং দুই ভাইক আটক করছ পুলিস। 
</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>ডব্লিউ
টি ও : কেন্দ্রর বিরুদ্ধ রাজ্যগুলি একযাগ মামলা করব</p>

<p>রাজ্য সরকারগুলিক
কিছু না জানিয় কন্দ্র কৃষি সংক্রান্ত ব্যাপার বিশ্ব বাণিজ্য সংস্হা বা ডব্লিউ টি ও-এর সঙ্গ
চুক্তি সই করায় পশ্চিমবঙ্গ-সহ বিভিন্ন  রাজ্য একযাগ কন্দ্রর বিরুদ্ধ সুপ্রিমকার্ট
মামলা করত চলছ। মুখ্যমন্ত্রী বুদ্ধদব ভট্টাচার্য একথা জানিয়ছন। </p>

<p>তিনি বলন, সংবিধান
কৃষিক যৗথ তালিকায় রাখা হয়ছ। অথচ রাজ্যগুলিক সম্পূর্ণ অন্ধকার রখ কন্দ্রীয় সরকার
ডব্লিউ টি ও-এর সঙ্গ চুক্তি সই করছ। এটা সংবিধান বিরাধী। বিভিন্ন রাজ্য এর বিরুদ্ধ
মিলিতভাব মামলা করত চলছ। ১ সপ্টম্বর জাতীয় উন্নয়ন পরিষদর বৈঠক বিষয়টি তুলবন
বুদ্ধদব। খালাখুলি আলাচনার পর আরও কয়কটি রাজ্য কন্দ্রর বিরাধিতায় নামব বল
তিনি আশাবাদী। </p>

<p>কৃষিক্ষত্র পশ্চিমবঙ্গর
উন্নতির কথা বর্ণনা কর বুদ্ধ  আরও বলন, অবাধ আমদানি নীতিত বিদশ থক চাল-সহ
বিভিন্ন খাদ্যশস্য আসছ। এত পশ্চিমবঙ্গর কৃষকরা মার খাবন। লাভ তা দূরস্হান, ফসল বিক্রি
কর চাষর খরচ তালাও তাঁদর পক্ষ মুশকিল হব। এই পরিস্হিতিত কৃষিজীবী মানুষক ডাল,
তৈলবীজ, ফুল, ফল ও সবজি চাষ গুরুত্ব দিত অনুরাধ করছন মুখ্যমন্ত্রী। </p>

<p>রাজ্য চালু
হল ই-গভর্ননন্স</p>

<p>শুক্রবার থক পশ্চিমবঙ্গ
তথ্যপ্রযুক্তি নির্ভর প্রশাসনিক ব্যবস্হা বা ই-গভর্নন্স চালু হল। মহাকরণ বস মুখ্যমন্ত্রী বুদ্ধদব
ভট্টাচার্য ১৭টি জলা সদরর সঙ্গ সরাসরি সংযাগ ব্যবস্হার উদ্বাধন করন। ২ এম ভি এস
ফাইবার অপটিক ব্যবস্হার মাধ্যম এই যাগাযাগ স্হাপিত হয়ছ। </p>

<p>মহাকরণ নিজর ঘর
বস বুদ্ধদব ভিডিও কনফারন্সর মাধ্যম বিভিন্ন জলা শাসকদর সঙ্গ কথা বলন। দার্জিলিঙ
এবং মুর্শিদাবাদর জলা শাসকর সঙ্গ বন্যা ও সামগ্রিক পরিস্হিতি নিয় তাঁদর কথাবর্তা
হয়। গতকালই মাইক্রাসফটর সঙ্গ রাজ্য সরকারর সাক্ষরিত চুক্তিত বিশষ গুরুত্ব দিয় ই-গভর্নন্সর
কথা বলা আছ। মাইক্রাসফট এই ব্যাপার রাজ্য সরকারক সবরকম সাহায্য করব। </p>

<p> </p>

<p>
মমতা
: রাজ্য বিজপির কথা শোনা হব না, বললন জনা</p>

<p>তৃণমূল কংগ্রসক
এনডিএ-ত নওয়ার ব্যাপার বিজপি-র রাজ্য নতাদর মতামত তমন গুরুত্ব পাব না। বিজপির
কন্দ্রীয় নতৃত্বই এ ব্যাপার সিদ্ধান্ত নবন। তারপর এনডিএ-ত সই সিদ্ধান্ত অনুমাদিত হব।
শুক্রবার প্রস ক্লাব এক সাংবাদিক সম্মলন একথা জানালন বিজপির সর্বভারতীয় সভাপতি জনা
কৃষ্ণমূর্তি। তপন শিকদার ও অসীম ঘাষরা, মমতা বন্দ্যাপাধ্যায়র অন্তর্ভুক্তিত যত আপত্তিই
করুন না কন, তাক খুব একটা পাত্তা দিত নারাজ কৃষ্ণমূর্তি। তাঁর মন্তব্যর পর এনডিএ-ত
মমতার ফরা শুধু সময়র অপক্ষা বল রাজনৈতিক মহলর অভিমত। </p>

<p>কৃষ্ণমূর্তি বলন,
এনডিএ-এর শরিকদর জন্য নির্দিষ্ট কিছু আচরণবিধি চালু করা হব। সব শরিকই এ ব্যাপার
একমত হয়ছ। আচরণবিধি চালু করা হব। সব শরিকই এ ব্যাপার একমত হয়ছ। এনডিএ-কে অনক
শরিক দল নিজদর স্বার্থপূরণর জন্য ব্যবহার করছ বলও জনার অভিযাগ। তাঁর কথায়: অনক
দলই এনডিএ থক বরিয় গছ। তাত আমাদর কানও ক্ষতি হয়নি। পর সইসব দলই আবার
ফিরত চয়ছ। </p>

<p> </p>

<p>দুটি ট্রেন
দুর্ঘটনায় হত ২৭, আহত ৫০</p>

<p>বৃহস্পতিবার ও
শুক্রবার দুটি ট্রন দুর্ঘটনায় মাট ২৭ জনর মৃতু্য হয়ছ। মহারাষ্ট্রর জলগাঁও-এ
প্রহরীবিহীন লভল ক্রসিং পার হত গিয় বিপরীত দিক থক আসা গীতাঞ্জলি এক্সপ্রসর ধাক্কায়
১৫ জন মারা যান। বৃহস্পতিবার সন্ধ্যায় এই ঘটনা ঘট। নিহতদর মধ্য ৮ জন মহিলা ও পাঁচটি
শিশু। এঁরা সকলই আকালা জলার বাসিন্দা। একটি দরগা পরিদর্শন কর ওয়ার্ধা ভুসওয়াল প্যাসঞ্জার
কর তাঁরা বাড়ি ফিরছিলন। কানও কারণ ট্রন থামল তাঁরা লাইন পার হত যান। সই সময়
গীতাঞ্জলি এক্সপ্রস তাঁদর ধাক্কা মার। খারাপ আবহাওয়ার জন্য তাঁরা বিপরীত দিক থক আসা
ট্রনটি দখত পাননি। </p>

<p>শুক্রবার ভার একটি
মালগাড়ির সঙ্গ সংঘর্ষ মুজাফফরপুর ফাস্ট প্যাসঞ্জারর ১০ যাত্রী মারা যান এবং ৪৮ জন আহত
হন। নিহতর তালিকায় রাজকুমার রাম বল কলকাতার এক বাসিন্দা রয়ছন। শিয়ালদহগামী
যাত্রীবাহী ট্রন ও মালগাড়িটি সিগন্যাল ব্যবহারর ত্রুটিত একই লাইন এস পড়ছিল। রলসূত্র
পাওয়া খবর অনুযায়ী মালগাড়িটি প্রচণ্ড গতিত প্যাসঞ্জার ট্রনটিক ধাক্কা মার। সংঘর্ষ
মালগাড়ির দুটি বগি প্যাসঞ্জর ট্রনর ওপর উঠ যায়। আহতদর মধ্য ১৫ জনর অবস্হা
আশঙ্কাজনক। তাঁদর দানাপুর রল হাসপাতাল ভর্তি করা হয়ছ। </p>

<p> </p>

<p>শৈলন দাস
হত্যাকাণ্ডর জাল গুটিয় আনছ পুলিস</p>

<p>দমদমর পুরপ্রধান
ডাঃ শৈলন দাশর হত্যাকাণ্ডর তদন্ত অনকটাই এগিয়ছন গায়ন্দারা। আপাতত যা পরিস্হিতি
তাত আর একজনক গ্রপ্তার করত পারলই এই হত্যাকাণ্ডর পিছন কারা আছ তা স্পষ্ট হয় যাব।
উত্তর ২৪ পরগণার এক কুখ্যাত দুষ্কৃতীই হত্যাকাণ্ডর পরিকল্পনা করছিল বল পুলিস জানিয়ছ।
তাক সাহায্য করছিল আগরপাড়ার দিলীপ শীল ও বিশ্বনাথ নাম দুই ব্যক্তি। দিলীপ শীলক গ্রপ্তার
করা হয়ছ। তাক জরা করই বিশ্বনাথ ও আর এক ব্যক্তির নাম জনছ পুলিস। এই ব্যক্তির
মদত নিম্নবিত্ত পরিবারর ছল দিলীপ রাতারাতি ধনী হয় উঠছিল। </p>

<p>হত্যার কারণ হিসাব
পুলিস এখনও পর্যন্ত জলাজমি বাজানা নিয় বিরাধকই দখাত চাইছ। দমদমর ১১ বিঘার
বিরাট জলাশয় পদ্মপুকুর ভরাট কর
সেখান একটি বসরকারি কলজ গড়ত চয়ছিলন শৈলনবাবু। দমদমরই একাধিক পুরপিতার
পরিকল্পনা ছিল পদ্মপুকুর বুজিয় প্লট হিসাব জমি প্রমাটারদর বিক্রি করা। এই নিয় এক
বছরর বশি সময় টানাপাড়ন চল। শষপর্যন্ত শৈলনবাবুক আটকাত তাঁর বিরাধীরা
স্হানীয় মানুষক সঙ্গ নিয় পুকুর ভরাটর বিরুদ্ধ আন্দালন গড় তালন। </p>

<p> </p>

<p> </p>

<p>সম্পাদকীয় :
সমাজবিরাধী ও সিপিএম </p>

<p>পরপর বিভিন্ন ঘটনায়
অপরাধ জগতর সঙ্গ সিপিএমর মাঝারি ও নিচুতলার নতাদর যাগাযাগ প্রকাশ্য চল আসায়
দলর ওপর মহল বিশষ উদ্বিগ্ন। ডাঃ শৈলন দাসর হত্যাকাণ্ডর পর যটুকু জানা গছ তাত
পরিষ্কার য, দলরই একশ্রণীর নতা শৈলনবাবুক সহ্য করত না পর কয়কজন সমাজবিরাধীর
সাহ্যয্য তাঁক খুন করিয়ছন। উত্তর ২৪ পরগনা জলায় সিপিএম ইতিমধ্যই গষ্ঠীদ্বন্দ্ব
বহুধাবিভক্ত। এরপর দলীয় চয়ারম্যানক হত্যার চক্রান্ত দলর নতাদর নাম জড়িয় গল
সিপিএমর মুখ দখানাই ভার হয় উঠব।  </p>

<p>ষষ্ঠবার ক্ষমতা দখলর
পর থকই বিভিন্ন ঘটনায় সিপিএমক তীব্র অস্বস্তিত পড়ত হয়ছ। অনিল বিশ্বাস ও বিমান বসু
কড়া হাত পরিস্হিতি মাকাবিলায় পুলিসক নির্দশ দিয়ছন। বিশষত, শৈলনবাবুর ঘটনায়
অপরাধীদর গ্রপ্তার করত গিয় রাজনৈতিক রঙ না দখার জন্য পুলিসকর্তাদর বলা হয়ছ। 
</p>

<p>পাশাপাশি জলাস্তরর
প্রত্যক নতাক ডক বিমান ও অনিল জানিয় দিয়ছন, অপরাধ জড়িত কাউক যন আশ্রয় দওয়া
না হয়। এই নির্দশ অমান্য করল দল য পাশ থাকব না এই ইঙ্গিতও পরিষ্কার। রিয়লএস্টট ও প্রমাটারি
ব্যবসার সঙ্গ নিচু তলার রাজনীতি এতটা জড়িয় পড়ায় আলিমুদ্দিন কর্তারা স্পস্টই অস্বস্তিত। জলা
ও রাজ্যস্তর একাধিকবার এই বিষয় নিয় আলাচনার পরও নিচুতলার নতাদর বাগ আনত না
পর এবার সিপিএম নতৃত্ব কিছুদিনর জন্য পুলিসর ওপর থক নিয়ন্ত্রণ তুল নিত চান।
পুলিসি হস্তক্ষপ সত্বও দল পাশ দাঁড়াচ্ছ না দখল লাকাল কমিটি স্তরর নতা ও
রাজনৈতিক আশ্রয় থাকা দুর্বৃত্ত ও সমাজবিরাধীরা থমক যাব বলই সিপিএম নতৃত্বর
বিশ্বাস। তব, এত একটা ঝুঁকিও আছ। ধরা পড়ার পর মরিয়া হয় মুখ খুলল জলাস্তরর নতারাও
বিপদ পড় যত পারন। দলীয় স্তর ক কী করন তা সবারই জানা। পারস্পারিক স্বার্থর
টানাপাড়ন ভারসাম্য বজায় রাখ। দীর্ঘ প্রায় ২০ বছর এই ব্যবস্হাই চল আসছ। রাতারাতি এর
পরিবর্তনর ধাক্কা কি বাংলার আগমার্কা মার্কসবাদী দল সামলাত পারব?</p>

<p> </p>

<p>সার্কিট বেঞ্চ
শেষ পর্যন্ত জলপাইগুড়িতই</p>

<p>দীর্ঘ টালবাহানার পর
জলপাইগুড়িত কলকাতা হাইকার্টর সার্কিট বঞ্চ তৈরির ব্যাপার সম্মতি দিল সুপ্রিম কার্ট।
সংসদর চলতি অধিবশনই এই মর্ম একটি বিল অনুমাদনর চষ্টা চলছ। সুপ্রিম কার্ট ও কন্দ্রীয়
সরকারর মনাভাব জানার পর কলকাতা হাইকার্ট ও রাজ্য সরকার এই ব্যাপার উদ্যাগী হয়ছ।
রাজ্যর আইনমন্ত্রী নিশীথরঞ্জন অধিকারী জানিয়ছ, যত দ্রুত সম্ভব সিদ্ধান্ত কার্যকর করা হব।
পরিকাঠামা দখত শীঘ্রই তিনি জলপাইগুড়ি যাবন। আপাতত সার্কিট বঞ্চর জন্য একটি বাড়ি
এবং দুজন বিচারপতির থাকার উপযুক্ত বাংলার ব্যবস্হা রাজ্য সরকারক করত হব। পরবর্তী
পর্যায় জলপাইগুড়ি হাইকার্ট-এর স্হায়ী বাড়ি নির্মাণ করা হব। 
  </p>

<p>উত্তরবঙ্গ কলকাতা হাইকার্টর
সার্কিট বঞ্চ তৈরি করা নিয় রাজ্য সরকার বা সখানকার মানুষর কানও দ্বিমত ছিল না।
কিন্তু সমস্যা দখা দয় শিলিগুড়ি না জলপাইগুড়ি- কাথায় এই বঞ্চ তৈরি হব তা নিয়।
বিষয়টি দুই শহরর মানুষর কাছ মর্যাদার প্রশ্ন হয় দাঁড়ায়। এই ব্যাপার মামলাও হয়।
বিতর্ক এড়াত রাজ্য সরকার স্হান নির্বাচনর ভার ছড় দয় কলকাতা হাইকার্টর ওপর। হাইকার্ট
নিযুক্ত কমিটিও সব দিক বিবচনা কর জলপাইগুড়ির পক্ষ রায় দয়। </p>

<p> </p>

<p>শ্রীলঙ্কায়
সিরিজ হারল নেতৃত্ব যাচ্ছ সৌরভর</p>

<p>সৗরভ গঙ্গাপাধ্যায়র
অধিনায়কত্ব ঘির আবার গুঞ্জন শুরু হয়ছ। শ্রীলঙ্কার বিরুদ্ধ টস্ট সিরিজ জয় বা সমতা না রখত
পারল দক্ষিণ আফ্রিকা সফর নতুন অধিনায়ক দখার সম্ভাবনা। সৗরভর বদল দায়িত্ব পত পারন
রাহুল দ্রাবিড়। ত্রিদশীয় সিরিজর মতা টস্ট ব্যর্থ হল সৗরভর ভবিষ্যত্ অনিশ্চিত হওয়ার
আরও বড় কারণ তাঁর নিজর ব্যর্থতা। </p>

<p>ক্রিকট-কর্তা রাজ
সিং দুঙ্গারপুর চিরকালই সৗরভর কট্টর সমালালাচক। শ্রীলঙ্কায় গিয় তিনি রাহুল দ্রাবিড়র সঙ্গ
এক প্রস্হ আলাচনাও করন। বলন- সৗরভ ব্যর্থ হল দক্ষিণ আফ্রিকা সফর তামাক দায়িত্ব নিত
হব। </p>

<p>আর একটি মহলর
ধারণা, সৗরভক এখনই সরানা হব না। তাঁর নতৃত্ব আটটির মধ্য পাঁচটি টস্ট ভারত জিতছ।
তব শ্রীলঙ্কায় ভারতর বাকি দুটি টস্ট হারল বি সি সি আই অন্য সিদ্ধান্ত নব। </p>

<p>তা ছাড়া জিতল
অধিনায়ক ও কাচকই বাহবা দওয়া হয়। হারল তার দায়ভারও তাদর নিত হব। ইতিমধ্য বিদশী
ফিজিও থরাপিস্ট অ্যাণ্ডু লিপাসক নিয়ও গুঞ্জন উঠছ। ওক এন লাভ কী হলা। জাভাগল
শ্রীনাথ অসুস্হ, ভঙ্কটশ প্রসাদও ফিট নন। শচীন সম্পর্কও লিপাস যথার্থ তথ্য দিত পারননি।
</p>

<p> </p>

<p>বিতর্কিত
বিষয় বাদ দিয় মহিলা বিল পাস হোক : প্রধানমন্ত্রী</p>

<p>বিতর্কিত বিষয়গুলি
বাদ দিয় ঐকমত্যর ভিত্তিত মহিলা আসন সংরক্ষণ বিল পাস করা যত পার বল শুক্রবার
প্রধানমন্ত্রী অটলবিহারী বাজপয়ী মন্তব্য করছন। মহিলাদর অধিকার বিষয়ক এক জাতীয় ফারাম
ভাষণ দিত গিয় প্রধানমন্ত্রী বলন, মহিলাদর জন্য কত শতাংশ আসন সংরক্ষণ করা উচিত তা নিয়
বিভিন্ন মহল মতভদ আছ। কউ কউ মন করন, আইনসভায় ৩৩ শতাংশ আসন মহিলাদর জন্য
সংরক্ষিত হলও প্রয়াজনীয় সংখ্যায় মহিলা পাওয়া যাব না। </p>

<p>প্রধানমন্ত্রী বলন,
দীর্ঘদিন ধর মহিলা আসন সংরক্ষণ বিল দীর্ঘদিন ধর পাস না হয় পড় আছ। অথচ এমন একটি
গুরুত্বপূর্ণ বিষয় বকয়া ফল রাখা যায় না। আপাতত বিষয়গুলি বাদ দিয় মহিলা বিল পাস
করানা উচিত। বিলটি পাস হয় গল সমস্ত রাজনৈতিক দলগুলি একসঙ্গ বস বিতর্কিত বিষয়গুলি
মিটিয় নওয়া যত পার। </p>

<p> </p>

<p>বিবাদী বাগ
'নো পার্কিং জোন' করার প্রস্তাব</p>

<p>পরিবষ দূষণ নিয়ন্ত্রণ
ও অফিসপাড়ায় যান চলাচল ব্যবস্হা গতিশীল করত বিবাদী বাগ এলাকাক 'নো পার্কিং জান'
হিসব ঘাষণা করত চায় পরিবহণ মন্ত্রক। বিবাদী বাগর বদল সমস্ত গাড়ি পার্ক করা হব
দ্বিতীয় হুগলি সতুর কাছাকাছি এলাকায়। সখান থক গাড়ির আরাহীদর ব্যাটারিচালিত বাস
ডালহৗসিত আনা হব। প্রস্তাবিত পার্কিং প্লাজা গড় উঠব ২৭ একর ওপর। গাড়ির আরাহীদর
আনা নওয়ার কাজ প্রাথমিকভাব ১২টি ব্যাটারিচালিত বাস কাজ লাগানা হব। </p>

<p>পরিবহণ মন্ত্রকর পক্ষ থক
হুগলি রিভার ব্রিজ কমিশনারর ভাইস চয়ারম্যান, ডি সি ট্রাফিক এবং পুরসভার অফিসার অন স্পশাল
ডিউটি (পরিকাঠামা)-কে প্রস্তাব পাঠানা হয়ছ। একটি সমীক্ষায় দখা গছ প্রতিদিন গড়
৮৫ হাজার গাড়ি ডালহৗসি এলাকায় চলাচল কর। এর মধ্য ১৫০০ গাড়ি নিয়মিত সখান পার্ক
করা থাক। এত একদিক যমন রাস্তা আটক থাক, অন্যদিক তমনি যানবাহনর গতিও কম
যায়। </p>

<p>দ্বিতীয় হুগলি সতুর
কাছ পার্কিং প্লাজা তৈরির পাশাপাশি অফিস পাড়ায় ভূগর্ভস্হ একটি পার্কিং প্লস তৈরির কথাও
ভাবা হচ্ছ। মন্ত্রী ও আমলাদর সঙ্গ যাঁরা দখা করত আসন তাঁদর গাড়ি এখান পার্ক করা
থাকব। </p>

<p> </p>

<p>নীলাঞ্জনক
নিয় সুব্রত'র 'স্বপ্নর ফেরিওয়ালা'</p>

<p>নতুন নতুন অভিনতা
অভিনত্রীদর কাছ সুব্রত সন কি 'স্বপ্নর ফরিওয়ালা'? এই প্রশ্নর উত্তর পত দরি হলও
সুব্রত সন পরর নতুন ছবি নির্মাণ দরি করছন না। তাঁর দ্বিতীয় ছবি 'স্বপ্নর ফরিওয়ালা'।
কঙ্কনার মতই আবার একটি নতুন মুখ দখা যাব এই ছবির নায়িকা চরিত্র। সুব্রত'র থিওরি
(না-ও হত পার) যন সলিব্রিটি নায়িকা-কন্যাদর পর্দায় নিয় এস বাংলা সিনমায় বাড়তি
একটা আকর্ষণ যাগ করা। সই সূত্রই, বাংলা সিনমার প্রখ্যাত জনপ্রিয় অভিনত্রী অঞ্জনা ভৗমিকর
ময় নীলাঞ্জনার দখা পাওয়া যাব 'স্বপ্নর ফরিওয়ালা'য়। সুব্রত'র বিশ্বাস, ভাল ছবির
সঙ্গ নতুন এবং প্রতিশ্রুতিসম্পন্ন নায়ক-নায়িকাদর ঘনিষ্ট অভিনয়ই বাংলা ছবিক দর্শকদর কাছ
গ্রহণযাগ্য করত পার। </p>

<p>উত্তমকুমারর সঙ্গ
'রাজদ্রাহী', 'নায়িকা সংবাদ', 'রৗদ্রছায়া'-র অভিনয় করা নায়িকা অঞ্জনা তমন
ডাকসাইট রূপবতী না হলও একটা আলগা চটক আর গ্ল্যামারর সংমিশ্রণ তিনি সকলর মন কড়ছিলন।
অভিনয়টাও চুটিয় করতন। জনপ্রিয় হয় উঠত উঠত হঠাত্ই বিয় কর চল গলন মুম্বই। ময়
নীলাঞ্জনা শর্মা এ্যাড-ফিল্ম করিয়ার পাকিয়ছন। জি-টিভির টলি সিরিয়াল অভিনয়রও
অভিজ্ঞতা আছ। ইচ্ছ ছিল মায়র মতই ভাল বাংলা ছবির নায়িকা হব। সুব্রত'র হাত ধর সই
স্বপ্নও পূরণ হত চলছ। </p>

<p> </p>

<p>'অর্জুন'
প্রত্যাখ্যান কর ঠিক করছন মিলখা</p>

<p>অপমানিত বাধ করই
'উড়ন্ত শিখ' মিলখা সিং 'অর্জুন' প্রত্যাখ্যান করলন। কন্দ্রীয় ক্রীড়ামন্ত্রী উমা
ভারতীক  জানিয় দিয়ছন ২৯ আগস্ট তিনি রাষ্ট্রপতি ভবন গিয় ওই পুরস্কার নবন
না। </p>

<p>সম্প্রতি ঘাষণা করা
হয় সারাজীবন খলায় (অ্যাথলটিক্স) অসাধারণ অবদানর জন্য তাঁক এই পুরস্কার দওয়া হব।
মিলখা ১৯৬০-এ রাম অলিম্পিক্স ১ সকন্ডর জন্য ব্রাঞ্জ পদক থক বঞ্চিত হন। তব এটা ছিল
১৯৫৬ অলিম্পিক্সর চাইত ভাল সময়। তার আগই ১৯৫৮-য় রাষ্ট্রপতি তাঁক 'পদ্মশ্রী' দন।
রাম অলিম্পিক্সর পর তিনি তা আরও মর্যাদাময় ওঠন। ১৯৬০ থক ২০০১, ৪১ বছর পর তাঁক
'অর্জুন' দওয়া  হচ্ছ। অথচ 'অর্জুন' চালু হয় ১৯৬২-তে। </p>

<p>তাঁর তুলনায় অতি
সাধারণ মানর খলায়াড়রা এখন 'অর্জুন' পাচ্ছন। মিলখা সঙ্গত কারণই ওই প্রস্তাব মন
নিত পারননি।  </p>

<p> </p>

<p>ভারত না খেললও পাকিস্তান এশিয়ান টেস্ট চ্যাম্পিয়নশিপ হবই : ডালমিয়া
</p>

<p>কন্দ্রীয় সরকার
অনুমতি না দিল ভারত আসন্ন এশিয়ান ক্রিকট চ্যাম্পিয়নশিপ খলব না। তা বল পাকিস্তান ওই
প্রতিযাগিতা হব না এমন কথা ভাবা ঠিক নয়। ভারত না খললও চ্যাম্পিয়নশিপ হবই।
কলকাতার এশিয়ান ক্রিকট ফাউণ্ডশনর তিনদিনর বৈঠক শষ ফাউণ্ডশনর সভাপতি জগমাহন
ডালমিয়া একথা বলন। </p>

<p>তব তাঁর আশা,
ভারত সরকার চতুর্দশীয় (ভারত, বাংলাদশ, পাকিস্তান ও শ্রীলঙ্কা) এই প্রতিযাগিতার জন্য
অনুমতি দব। </p>

<p>ডালমিয়া বলন, ভারত
খলব কি না- তা পাক সংগঠকদর ২৩ আগস্টর মধ্য জানিয় দিত হব। এ সি এফ-এর আমন্ত্রণ
বি সি সি আই সচিব জয়ন্ত লল কলকাতায় এসছিলন। তিনি ডালমিয়াক বলছন, আগামী সামবারর
মধ্যই কন্দ্রীয় সরকারর সিদ্ধান্ত জানা যাব। তিনিও সরকারর অনুমতি সম্পর্ক আশাবাদী।
</p>

<p>২৯ আগস্ট প্রতিযাগিতার
প্রথম ম্যাচ খলব পাকিস্তান ও বাংলাদশ। </p>

<p>বৃহস্পতিবার সুনীল
গাভাসকারর নতৃত্ব এশীয় চ্যাম্পিয়নশিপর টকনিকাল কমিটির সদস্যরা খলার শর্তাবলী, পয়ন্ট,
সিস্টম, ইত্যাদি ঠিক করন। বৈঠক হাজির ছিলন পাকিস্তানর প্রাক্তন তারকা ক্রিকটার
জাহির আব্বাসও। </p>

<p>ঠিক হয়ছ প্রতিটি
ম্যাচ একজন থাকবন এশীয় আম্পায়ার। তিনি হবন নিরপক্ষ। যমন পাকিস্তান বাংলাদশর খলায়
ভারত বা শ্রীলঙ্কার কউ। আর একজন হবন আই সি সি মনানীত। ম্যাচ রফারিও হবন আই সি
সি মনানীত। </p>

<p> </p>

<p>বিশ্বদাবায়
যোগ্যতার দিক সূর্যশখর</p>

<p>গার্কি সদনর তৃতীয়
এশীয় দাবা প্রতিযাতার প্রথম দশ জনর মধ্য যারা থাকছন, তাঁদর, বিশ্বদাবার জন্য
নির্বাচন করা হব। শুক্রবার এনিয় বাংলার দাবা মহল উচ্ছ্বাস দখা যায়। ১৮ বছর কিশার
সূর্যশখর গঙ্গাপাধ্যায়র ১০ রাউণ্ডর পর পয়ন্ট হয়ছ ৭। তাঁর বিশ্বদাবায় যাগ্যতা পাওয়া
প্রায় নিশ্চিত। সূর্য বৃহস্পতিবার হারিয় দন ভিয়তনামর গ্র্যান্ডমাস্টার ২৫৩৩ এলা রটিং-এর
নুয়ন আন দাঙক। সূর্যর রটিং ২৪৬৪। </p>

<p>ভারতর আর এক তরুণ
অন্ধ্রপ্রদশর পন্তিয়ালা হরিকৃষ্ণ বুধবার নবম রাউণ্ড শষ পয় যান তৃতীয় জি এম নর্ম।
বৃহস্পতিবার হরিকৃষ্ণ ড্র করন চীনর জ্যাং পংজিয়াং-এর সঙ্গ। চীনর শীর্ষ বাছাই শু-জুন ৮
পয়ন্ট শীর্ষ আছন। দশম রাউণ্ড গ্র্যান্ডমাস্টার দিব্যন্দু বড়ুয়া ৪১ চালর মাথায় কাজকস্তানর
গ্র্যান্ডমাস্টার মুরতাস কাঝালিয়ভক হারিয়ছন। কিন্তু  ৬.৫ পয়ন্ট তিনি চতুর্থ স্হান। বিশ্বদাবায় যাগ্যতা পাওয়া
কঠিন। বাংলার তিন দাবাড়ুর মধ্য সবচয় কম পয়ন্ট সন্দীপন চন্দর। দশম রাউণ্ড জিতলও হয়ছ
৪.৫। </p>

<p> </p>






</body></text></cesDoc>