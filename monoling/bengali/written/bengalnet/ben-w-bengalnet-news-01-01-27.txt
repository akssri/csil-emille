<cesDoc id="ben-w-bengalnet-news-01-01-27" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-01-27.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-01-27</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-01-27</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>ভূমিকম্প
মৃত কমপক্ষ ছয় হাজার</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>ভূমিকম্প কমপক্ষ ৬০০০ মানুষর
মৃতু্য হয়ছ বল আশঙ্কা করা হচ্ছ। আহতর সংখ্যা ৩৫০০-রও বশি। এঁদর অধিকাংশরই
আঘাত গুরুতর। ফল মৃতর সংখ্যা আরও বৃদ্ধি পাওয়ার আশঙ্কা রয়ছ। ভুজ শহরই ৫০০০ মানুষ
প্রাণ হারিয়ছন। এর মধ্য ১৫০০ দহ উদ্ধার করা হয়ছ। আমদাবাদই ৭০০ জনর মৃতু্য হয়ছ।
ভুজ শহরর এয়ারবস ধ্বংসস্তূপ চাপা পড় ১৫০ জন নিরাপত্তা কর্মীর মৃতু্য হয়ছ। গত ৫০
বছর এত ভয়াবহ ভূমিকম্প ভারত হয়নি। উদ্ধারকার্য যুদ্ধকালীন তত্পরতায় শুরু হয়ছ। শুক্রবার
রাত প্রধানমন্ত্রী ক্ষয়ক্ষতি পর্যালাচনার জন্য মন্ত্রিসভার জরুরি বৈঠক ডাকন। মার্কিন
যুক্তরাষ্ট্র, রাশিয়া, ব্রিটন-সহ বিশ্বর বিভিন্ন দশ ত্রাণ সাহায্যর আশ্বাস দিয়ছ। গভীর শাক
প্রকাশ করছন রাষ্ট্রসংঘর মহাসচিব কাফি আন্নান। </p>

<p>আমদাবাদর স্বামী
নারায়ণ বিদ্যালয়র কমপক্ষ ৪০ জন ছাত্র ধ্বংসস্তূপ চাপা পড় মারা গছ। ভূমিকম্পর সময়
ক্লাস চলছিল। উদ্ধারকারীরা কাজ শুরু করার পর শিশুদর আর্তনাদ শুনলও কাউকই জীবিত অবস্হায়
উদ্ধার করত পারননি।  
</p>

<p> </p>

<p>ভূমিকম্প
আহত হাজার মানুষ চিকিত্সা পাচ্ছন না</p>

<p>
 
 
<gap desc="illustration"></gap>গুজরাটর ভূমিকম্পর তীব্রতা কত
ছিল তা নিয় এখন ভারতীয় ও মার্কিন আবহাওয়াবিদদর মধ্য বিতর্ক দখা দিয়ছ। ভারতীয়
আবহাওয়া বিজ্ঞানীরা তীব্রতা রিখটার স্কল ৬.৯
বললও মার্কিন জিওলজিক্যাল সার্ভ তীব্রতা ৭.৯
বল জানিয়ছ। গুজরাটর কচ্ছ জলাই সবচয় ক্ষতিগ্রস্ত। ভুজ প্রায় ৫০০০ মানুষ প্রাণ হারিয়ছন।
এর মধ্য ১৫০০ দহ উদ্ধার করা হয়ছ। হাজার হাজার আহত মানুষর ভরসা একটিমাত্র সামরিক
হাসপাতাল। তাও সখান অস্ত্রাপচারর ভাল ব্যবস্হা নই। অস্হায়ী ভিত্তিত পুরা কাজ চালানা
হচ্ছ। সামরিক বাহিনীর চিকিত্সকরা দিনরাত কাজ করও অবস্হা আয়ত্ত আনত পারছন না।
</p>

<p>ভুজ শহরর প্রধান
হাসপাতালটি ভূমিকম্প পুরা ধ্বংস হয় গছ। সখানকার অধিকাংশ রাগীরও মৃতু্য হয়ছ।
পরিস্হিতি মাকাবিলায় ভুজ-এর এয়ারপার্ট বস-এ অস্হায়ী চিকিত্সাকন্দ্র খালা হয়ছ। শনিবার
দুপুর নাগাদ আন্তর্জাতিক রড় ক্রসর একটি দল আহমদাবাদ এস পৗঁছব। কয়কটি দশ থক
চিকিত্সক ও ওষুধপত্র আনা হচ্ছ।  
</p>

<p> </p>

<p>আন্নানর
শোক, সাহায্যর আশ্বাস আন্তর্জাতিক মহলর</p>

<p>
 
 
<gap desc="illustration"></gap>পশ্চিমভারতর মর্মান্তিক ভূকম্পনর
পর আন্তর্জাতিক মহল সাহায্য ও সহযাগিতার প্রস্তাব দিয়ছ। ক্ষয়ক্ষতির পরিমাণ খতিয় দখত
রাষ্ট্রসংঘর মহাসচিব কাফি আন্নান পাঁচ সদস্যর এক প্রতিনিধি দলক ভারত পাঠাচ্ছন। ভারত
সরকারক রাষ্ট্রসংঘ সবরকম সহায়তা করত প্রস্তুত বলও জানিয়ছন আন্নান। </p>

<p>মার্কিন সরকার ভূকম্পন
ক্ষতিগ্রস্তদর জন্য প্রাথমিকভাব ২৫ হাজার ডলার দওয়ার কথা ঘাষণা করছন। ভারত নিযুক্ত
মার্কিন রাষ্ট্রদূত রিচার্ড সলস্ট একথা জানিয়ছন। বুশ বলছন, 'মর্মান্তিক এই
ভূমিকম্পর খবর শুন আমি মর্মাহত। নিহতদর পরিবারবর্গক গভীর সমবদনা জানাচ্ছি।' বুশর
প্রস সক্রটারি আরি ফ্লশার বলছন, ভারতর প্রয়াজন অনুসার সবরকম সাহায্য করত
মার্কিন সরকার প্রস্তুত। </p>

<p>ইউরাপিয় ইউনিয়ন ও
রাশিয়ার পক্ষ থকও সাহায্যর প্রস্তাব দওয়া হয়ছ। ইউরাপিয় ইউনিয়ন ও রাশিয়ার ভূকম্পন
বিশষজ্ঞরা শনিবারই গুজরাট পৗঁছ যাচ্ছন। নিহতদর প্রতি গভীর সমবদনা জানিয়
প্রধানমন্ত্রী অটলবিহারী বাজপয়ীক বার্তা পাঠিয়ছন পাকিস্তানর সামরিক শাসক পারভজ মাশারফ।
ব্রিটনর পক্ষ থক ৩০ লক্ষ পাউণ্ড সহায়তার আশ্বাস দওয়া হয়ছ। এর মধ্য রড ক্রসক দওয়া
২ লক্ষ ৫০ পাউণ্ড অনুদানও অন্তর্ভুক্ত। 
</p>

<p> </p>

<p>আটটি ড্র-র
পর আনন্দর জয়, সন্দীপনও জিতলন</p>

<p>অবশষ জয় এল ফিড
বিশ্বকাপ দাবা চ্যাম্পিয়ন ভারতর বিশ্বনাথন আনন্দর। শুক্রবার নদারল্যাণ্ডসর উইক অ্যান জি-ত
কারাস আন্তর্জাতিক গ্র্যান্ড মাস্টার দাবায় তিনি হারালন একাদশ রাউণ্ড জবায়র পিকটক।
টানা ৮ গম ড্র-র পর আনন্দ জিতলন। তাঁর পয়ন্ট এখন ৬.৫। এই প্রতিযাগিতায় এটি আনন্দ-র দ্বিতীয় জয়। </p>

<p>জিতলন
সন্দীপনও</p>

<p>উবদা
আন্তর্জাতিক দাবায়ও জয় হল ভারতর সন্দীপন চন্দ্রর। পঞ্চম রাউণ্ড সন্দীপন হারান রুশ গ্র্যাণ্ড
মাস্টার কারনিভ-ক। সন্দীপনর পয়ন্ট এখন ৩.৫।</p>






</body></text></cesDoc>