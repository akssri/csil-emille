<cesDoc id="ben-w-bengalnet-news-01-02-28" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-02-28.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-02-28</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-02-28</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>বাজট বয়কট
করছন মমতা</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>অর্থমন্ত্রী যশাবন্ত সিংহর বাজট
বয়কট করছন রলমন্ত্রী মমতা বন্দ্যাপাধ্যায়। বুধবার সকাল এগারাটায় সাধারণ বাজট পশ
করবন অর্থমন্ত্রী। অন্যদিক মমতা মঙ্গলবার রাতই কলকাতা ফিরছন। ফল, বুধবার সকালর বাজট
চূড়ান্তকরণর জন্য কন্দ্রীয় কমিটির বৈঠক তিনি গরহাজির থাকছন। এই বাজট অধিবশন রলমন্ত্রীর
অনুপস্হিতির কারণ, বাজটর নানান কঠার সিদ্ধান্তর তিনিও শরিক। এই অবস্হায় মমতার
উপস্হিতিত বামদর প্রচার চালাত সুবিধাই হতা। তব কলকাতা রওনা হওয়ার আগ, রলমন্ত্রী
তাঁর দলর সাংসদদর এই নির্দশ দিয় যান য বাজটর য কানও কঠার প্রস্তাব তাঁরা যন
যথাচিত প্রতিবাদ করন। অন্যদিক রল বজট নিয় শিল্প মহল, আর্থিক বিশষজ্ঞ এবং কিছু
কিছু শরিক দলর তীব্র সমালাচনার মুখও মমতা মঙ্গলবার জানিয় দিলন, 'যা করছি বশ
করছি।' রলমন্ত্রীর বক্তব্য, সাধারণ মানুষর ইচ্ছাক মর্যাদা দিয়ই তিনি ভাড়া বাড়াননি।
নিরাপত্তার সঙ্গ তিনি আপস করছন, এই অভিযাগ খারিজ কর মমতা বলন, নিরাপত্তা খাত
আলাদা আর্থিক সহায়তার দাবি কন্দ্রর বিবচনাধীন। উল্লখ্য, সাধারণ বাজট বয়কট করলও
মমতা চাপর মুখ নতিস্বীকার কর এখনই পদত্যাগ করছন না। আগামী ১৪-১৫ মার্চ লাকসভায় রল
বাজট পাস করাত হব। ততদিন পর্যন্ত ইস্তফা দিচ্ছন না রলমন্ত্রী। পর কী হব তা এখন
অনিশ্চিত। রল বাজট পাস না হওয়া পর্যন্ত সংশ্লিষ্ট মন্ত্রকর কাজ চালাবন রাষ্ট্রমন্ত্রী দিগ্বিজয়
সিং। এই সময়টা মমতা আত্মনিয়াগ করবন পশ্চিমবঙ্গ নির্বাচনী প্রচারর কাজ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>




 
  
  <p>
   
  <gap desc="illustration"></gap></p>
  
  
  <p>সম্পাদকর দপ্তর আপনার
  মতামত লিখ পাঠান। 
   </p>
  
 




<p>মমতার
পদত্যাগ দাবি নবীনর</p>

<p>
 
 
<gap desc="illustration"></gap>রলমন্ত্রী মমতা বন্দ্যাপাধ্যায়র
পদত্যাগ দাবি করলন নবীন পট্টনায়ক। রল বাজট ওড়িশাক বঞ্চনার অভিযাগ ভুবনশ্বর এই
দাবি তুলছন ওড়িশার মুখ্যমন্ত্রী। মঙ্গলবার বিজু জনতাদলর বৈঠক সর্বসম্মত সিদ্ধান্ত হয়ছ: রল
বাজট ওড়িশাক উপক্ষার পর আর এই সরকারর শরিক হওয়ার কানও অর্থ হয় না। বিজু জনতা দলর
সংসদীয় বৈঠক দুই মন্ত্রীসহ ১০ জন সাংসদই ওড়িশাক বঞ্চনার প্রতিবাদ এন ডি এ জাট ছাড়ত
একমত। ওই দলর নতা প্রভাত সামন্ত রায়র মত, কন্দ্রীয় রলমন্ত্রীর আচরণ পুরসভার ওয়ার্ড
কাউন্সিলরর মতা। বি জ ডি নতাদর দাবি, 'হয় মমতা, নয় বিজু জনতা দল' বাছত হব
প্রধানমন্ত্রীক। অন্যদিক, এই অভিযাগর মুখ মমতার বক্তব্য, 'নবীন পট্টনায়ক য য
প্রকল্পর কথা লিখছন, তাতই অর্থ বরাদ্দ করছি।' এই বক্তব্যর জবাব ওড়িশা মুখ্যমন্ত্রীর
পাল্টা অভিযাগ, ' রলমন্ত্রীর এ দাবি অসত্য।' প্রভাত সামন্ত রায় হিসব কষ দখান,
গত সালর রল বাজট ওড়িশাক দওয়া হয়ছিল ১৭৪.২০
কাটি টাকা। কিন্তু এবার মাত্র ১৪০.৭০ কাটি
টাকা বরাদ্দ হয়ছ। বি জ ডি-র সঙ্গ গলা মিলিয় এই বঞ্চনার প্রতিবাদ ফুঁসছ সারা ওড়িশাই।

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p>
 
<gap desc="illustration"></gap></p>

<p>রেল বাজটর
সমালাচনায় সুভাষ</p>

<p>
 
 
<gap desc="illustration"></gap>এক বছর আগ য মমতা বন্দ্যাপাধ্যায়র
পশ করা রল বাজটর প্রশংসা করছিলন রাজ্য পরিবহনমন্ত্রী সুভাষ চক্রবর্তী, পরিবর্তিত
পরিস্হিতিত সই মমতার-ই এবারর রল বাজটক মঙ্গলবার তিনি 'নির্বাচনী চমক' বল
আখ্যা দিলন। সুভাষবাবু বলন, 'এবারর রল বাজট মমতার কানও বুদ্ধিমত্তার ছাপ নই।
রাজনৈতিক কৗশল বাংলার লাকক খুশি করার চষ্টা। গত বছর রল বাজট সমর্থন করার পছন
সুভাষবাবুর যুক্তি, পশ্চিমবঙ্গর জন্য রলর বরাদ্দ ৩ শতাংশ থক বাড়িয় ৮ শতাংশ করছিলন
মমতা। তাই প্রশংসা করছিলাম। কিন্তু এবার উনি অর্থনীতির নিয়মকানুন ল্ঙঘন করছন বাজট পশর
ক্ষত্র। এটা নিছক রাজনৈতিক চমক, যা মন নওয়া কঠিন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p>
 
<gap desc="illustration"></gap></p>

<p>নটবর-সুমন্ত
হীরাদর দল সুভাষ!</p>

<p>সুভাষ চক্রবর্তীক ঘির
ফর দল ছাড়ার গুঞ্জন শানা যাচ্ছ। পি ডি এস নয়, সুভাষ নাকি যাগ দিত চলছন পশ্চিমবঙ্গ
গণতান্ত্রিক মঞ্চ। এই তথ্য জানিয়ছন, পশ্চিমবঙ্গ গণতান্ত্রিক মঞ্চর সভাপতি নটবর বাগদি। তিনি বলন,
আমাদর আশা মার্চর প্রথম সপ্তাহই সুভাষ দল ছাড়ছন। ১৪ বা ১৫ মার্চ নতাজি ইনডার স্টডিয়াম
দলর রাজনৈতিক সম্মলন ডাকা হয়ছ। সদিনই সদলবল সুভাষ নতুন দল যাগ দবন।
</p>

<p>সম্প্রতি বিধাননগর
সুভাষর সঙ্গ গাপন বৈঠক করছন মন্টু সান্যাল, সুমন্ত হীরা, সুধীর ভট্টাচার্য প্রমুখ। ছিলন
সুভাষর ঘনিষ্ঠ হিসাব পরিচিত নকশাল নতা অসীম চ্যাটার্জি। নটবর বাগদি, সুমন্ত হীরাদর
বক্তব্য: সুভাষ গণতান্ত্রিক মঞ্চর সঙ্গ সৈফুদ্দিন-সমীরর দলর সমঝাতা চান। সজন্যই সবদিক দখশুন
তিনি 
ধীরসুস্হ এগাত চাইছন।
</p>

<p> </p>






</body></text></cesDoc>