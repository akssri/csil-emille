<cesDoc id="ben-w-bengalnet-news-01-07-19" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-07-19.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-07-19</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-07-19</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>আগ্রা :
পাক আচরণর কড়া প্রতিবাদ ভারতর</p>

<p>'এভাব চলল
একদিন হয়ত দু দশর শীর্ষ বৈঠকর আয়াজন করত হব অ্যাম্ফিথিয়টার-বাঝাই সাংবাদিকদর
সামন'- ঠিক এতটাই কড়া ভাষায় পাকিস্তানর নিন্দা করল ভারত। পাকিস্তানর বিদশমন্ত্রী আবদুস
সাত্তারর মন্তব্যর পর বিদশমন্ত্রকর মুখপাত্র নিরুপমা রাও জানিয় দন আগ্রা বৈঠকর আলাচনা
নিয় পাকিস্তান যভাব খালাখুলি মন্তব্য করছ তা আন্তর্জাতিক কূটনৈতিক শিষ্টাচারর বিরাধী।
পরবর্তী স্তর ভারত আগ্রার খসড়া ঘাষণাপত্র ভুল গিয় সিমলা ও লাহার চুক্তির সূত্র ধরই
আবার আলাচনা শুরু করব। </p>

<p>বুধবার প্রধানমন্ত্রী
বাজপয়ী এনডিএ-র শরিকদর আগ্রা বৈঠকর রিপার্ট দন। বৃহস্পতিবার সর্বদলীয় বৈঠক ডকছন
তিনি। তড়িঘড়ি ইসলামাবাদ যাওয়ার দিনক্ষণ চূড়ান্ত না করত বাজপয়ীক পরামর্শ দিয়ছন
আডবাণী। বাজপয়ী জানান, 'সুসংহত আলাচনা ও আস্হা অর্জনর প্রক্রিয়া এবং পরমাণু
অস্ত্র নিয় একটি মাটামুটি বাঝাপড়া হয়ছিল। মুশারফ জিদ ধরন কাশ্মীর নিয়।' পাকিস্তানর
তরফ থক বৈঠক ভস্ত যাওয়ার জন্য কানও 'অদৃশ্য হাত'-ক দায়ী করা হয়ছিল। ভারতর
তরফ থক বলা হচ্ছ, 'অদৃশ্য হাত আসল পাকিস্তানর জঙ্গি গাষ্ঠীগুলিই। মুশারফ দিল্লির
নহরওয়ালি হাভলি কিন থক যাওয়ার কথা বলায় বাঝা যায় কতটা চাপ ছিলন তিনি।'
</p>

<p>দিল্লির গতিপ্রকৃতি দখ
মন হচ্ছ ভারত-পাকিস্তানর ভবিষ্যত্ আলাচনার রশি থাকব স্বরাষ্ট্রমন্ত্রী লালকৃষ্ণ আডবাণীর
হাতই। তিনি বিভিন্ন মহল আলাচনা শুরু কর দিয়ছন। প্রাক্তন প্রধানমন্ত্রী ইন্দ্রকুমার গুজরালর
সঙ্গ কথা বলন তিনি। বৈঠকর বিষয়বস্তু না জানালও সাংবাদিকদর গুজরাল বলন, 'বাজপয়ী
সরকারর দক্ষতা আর প্রস্তুতি নিয় প্রশ্নর অবকাশ আস। বাতিল হওয়া যৗথ ঘাষণাপত্রর
খসড়াটি প্রকাশ করল ভাল হয়। মুশারফর উদ্দশ্যসিদ্ধি হয়ছ। বলত গল আমরা তাঁর কাজটা
সহজ কর দিয়ছি। তিনি হুরিয়তর সঙ্গ কথা বলছন, সংবাদমাধ্যমর সঙ্গও কথা বলছন।
পাকিস্তানর প্রচার অভিযানর কাছ ভারতক খুবই দুর্বল দখিয়ছ। একসময় আমরা বলছি
রাষ্ট্রনতা হিসব কার্গিলর খলনায়ক মুশারফ গ্রহণযাগ্য নন। আমরাই তাঁক পুনর্বাসনর সুযাগ
দিলাম।' 
</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p> </p>

<p>চোট
গোড়ালিত নয় বুড়া আঙুল 
শচীন
ত্রিদশীয় সিরিজ খেলবন না</p>

<p>
 
 
<gap desc="illustration"></gap>গাড়ালি থক বুড়া আঙুল- কানটা ঠিক? হারারত ওয়স্ট ইন্ডিজর
বিরুদ্ধ ফাইনালর দিন থক বুধবার ১৮ জুলাই সকাল পর্যন্ত তামাম ভারতর জানা ছিল শচীন তন্ডুলকরর
ডান গাড়ালিত চাট। পশী ও হাড়র সংঘর্ষ পশী মুচড় গিয়ছ। ওটা সারাত সপ্তাহ তিনক
লাগব। শ্রীলঙ্কায় ত্রিদশীয় সিরিজর শষ দুটি ম্যাচ তিনি খলবন। শ্রীলঙ্কার বিরুদ্ধ টস্ট তা
থাকবনই। </p>

<p>বুধবার অন্য কাহিনী
বললন শচীন নিজই সাংবাদিকদর ডক। হারারর পর এই প্রথম তিনি সাংবাদিকদর সঙ্গ পায়র
ব্যথার কথা জানালন। বললন, বুড়া আঙুলর সিজাময়ড হাড় সরু চালর মতা চিড় ধরছ।
মুম্বইয়র হিন্দুজা হাসপাতাল স্ক্যানর পর ওটা জনছন। </p>

<p>হারারত এক্স-রর পর
দলর ফিজিও (ডাক্তার নন) অ্যাণ্ড্রু লিপাস বলছিলন, চাটটা গাড়ালিতই। এতদিন সই
চিকিত্সাই চলছ। শচীন বুধবার জানান, হারারত ফাইনালর দিন উইকটর মধ্য দৗড়নার
সময় খুট কর একটি শব্দ পান। ব্যাপারটা তিনি নাকি নন স্ট্রাইকার রাহুল দ্রাবিড়কও জানান। ৪
জুলাই ওই অবস্হায় খল সঞ্চুরিও পান। চূড়ান্ত ম্যাচর আগ ব্যথা কমায় খলত নামন।
</p>

<p>শচীন আঙুলর ব্যথা
বা ফালার কথা আগ কখনও বলননি। বুধবার বলন, জিম্বাবায় থক ফরার পর আঙুলর ফালাটা
কমনি আইস প্যাক লাগিয়ও। যন্ত্রণাও ছিল। তাই স্ক্যান হল আঙুলর। খবর আরও- হারারত
ফাইনালর দিন তাঁর নাকি পট খারাপও হয়ছিল। বুধবার শচীন বলছন, 'এখনও তিন
সপ্তাহ বিশ্রাম থাকত হব। তাই হয়তা ত্রিদশীয় সিরিজ খলা হব না। টস্ট খলবাই।'
</p>

<p>অন্য সূত্রর খবর- ওঁর
যা 'চাট', তাত টস্টও খলত পারবন না। সন্দহ আরও : ড্রসিং রুমর গণ্ডগালর
ঘটনাটাই কী সত্যি? এ সব কারণই কী চাট গাড়ালি থক আঙুল চল গল? ফ্র্যাকচার হলা?

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>ওড়িশায়
মৃতু্য বেড় ৪৫, জলবন্দী ৫০ লক্ষ</p>

<p>
 
 
<gap desc="illustration"></gap>ওড়িশার বন্যা পরিস্হিতির আরও অবনতি হয়ছ। টানা বৃষ্টিত মহানদী,
ব্রাহ্মণী, দবী-সহ সমস্ত নদী কানায় কানায় পূর্ণ। এর উপর রয়ছ হীরাকুদ বাঁধ থক জল
ছাড়া। ছত্তিশগড়র একাধিক জলাধার জল ছাড়ায় পরিস্হিতি আরও সঙ্কটজনক হয় উঠছ। বন্যায় রাজ্যর
সাতটি জলা ক্ষতিগ্রস্ত। বানভাসি প্রায় ৫০ লক্ষ মানুষ। মৃতর সংখ্যা বড় দাঁড়িয়ছ ৪৫।
উপকূলবর্তী পশ্চিম ওড়িশার সঙ্গ রাজ্যর বাকি অংশর যাগাযাগ বিচ্ছিন্ন। ফল সখানকার
জলবন্দী মানুষর জন্য ত্রাণর ব্যবস্হা করা যাচ্ছ না। ভরসা একমাত্র আকাশ থক ফলা খাবারর
প্যাকট। পরিস্হিতির সঙ্গ যুঝত মধ্যপ্রদশ থক আরও চারটি হলিকপ্টার আনা হচ্ছ। কন্দ্রর
কাছ এককালীন সাহায্য বাবদ ২০০ কাটি টাকা চয়ছ ওড়িশা। এর মধ্য ১০০ কাটি
প্রধানমন্ত্রী মঞ্জুরও কর দিয়ছন। </p>

<p>৮২ সালর পর এই বছরর
মত বিধ্বংসী বন্যা আর হয়নি বল রাজ্যর মানুষর ধারণা। কউ কউ বলছন, ভয়াবহতায় শষ
পর্যন্ত এটা ৮২ সালকও ছাপিয় যাব। সবচয় ক্ষতিগ্রস্ত চারটি জলা হল- কারাপুট, ভদ্রক,
কালাহাণ্ডি ও জাজপুর। কয়ক মাস আগই কালাহাণ্ডি তীব্র খরার মুখামুখি হয়ছিল। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p>নতুন বাড়ি
ভাড়া আইন সংশাধিত হচ্ছ</p>

<p>শষ পর্যন্ত বাড়ি
ভাড়া আইন কার্যকর করা নিয় পিছু হঠত হল রাজ্য সরকারক। ভাড়াটিয়া সংগঠনগুলির দাবি মন
এই আইন বশ কিছু সংশাধন করত হচ্ছ। ভাড়াটিয়া পরিবারর উত্তরাধিকার ও ভাড়া নির্ধারণর
বিষয়টি পুনর্বিবচনা করা হব বল রাজ্য সরকার আশ্বাস দিয়ছ। শুক্রবার বাড়ি ভাড়া আইন
নিয় বিধানসভায় বক্তব্য পশ করবন সংশ্লিষ্ট বিভাগর মন্ত্রী আবদুর রজ্জাক মাল্লা। মহাকরণ
সূত্র খবর, নতুন বাড়ি ভাড়া আইনর ২ (জি), ৩ (ই), ৩ (এফ) ও ১৭ নম্বর ধারা সংশাধনর
সিদ্ধান্ত নিয় ফলছ রাজ্য সরকার। </p>

<p>নতুন আইনর এই
ধারাগুলিক যমন বাড়িওয়ালারা স্বাগত জানিয়ছিলন তমনি ভাড়াটিয়া সংগঠনগুলিও আপত্তি তুলছিল
ঠিক এই বিষয়গুলিতই। সরকারি সিদ্ধান্ত জানার পর ক্ষুব্ধ বাড়িওয়ালা সংগঠনগুলি পথ নামার
সিদ্ধান্ত নিয়ছন। ক্যালকাটা হাউস ওনার্স অ্যাসাসিয়শনর সাধারণ সম্পাদক অমর মিত্র বলন,
আইনটি সংশাধন কর ভাড়াটিয়াদর চিরস্হায়ী বন্দাবস্তর সুযাগ কর দিল বাড়িওয়ালারা মন
নবন না। </p>

<p>রাজ্যর
শিল্পায়ন এক বড় পদক্ষপ 
এস
এইচ ভি গ্রুপর সঙ্গ হলদিয়া পেট্রাকমর চুক্তি সই</p>

<p>বিশ্বর বৃহত্তম
লিকুইড পট্রালিয়াম গ্যাস বিপণনকারী সংস্হা এস এইচ ভি গ্রুপর ভারতীয় সাবসিডিয়ারি এস
এইচ ভি এনার্জি ইন্ডিয়া লিমিটড বুধবার হলদিয়া পট্রাকম-এর সঙ্গ একটি চুক্তি সই করছ।
এই স্ট্র্যাটজিক টাই আপর মাধ্যম হলদিয়া পট্রাকম দীর্ঘময়াদী ভিত্তিত এল পি জি
সরবরাহ করব। পক্ষান্তর, এল পি জি-র উত্পাদিত এল পি জি পূর্ব ভারত বন্টনর জন্য এস এইচ
ভি গ্রুপ প্রয়াজনীয় অর্থ বিনিয়াগ করব। </p>

<p>চুক্তির কথা ঘাষণা
কর এস এইচ ভি-র চিফ এক্সিকিউটিভ অফিসার ফ্রাঙ্ক ব্রুনউ বলছন, আন্তর্জাতিক বাজার এল পি
জি-র মূল্য আমরা দীর্ঘময়াদী ভিত্তিত হলদিয়া পট্রা রসায়ন প্রকল্প থক গ্যাস কিনত
চাই। এর ফল হলদিয়ার আর্থিক ভিত্তিও জারদার হব। </p>

<p>প্রসঙ্গত, গত এক বছর
এস এইচ ভি হলদিয়া পট্রাকম থক ৬০ কাটি টাকা মূল্যর এল পি জি কিনছ। সংস্হার
সিনিয়র ভাইস প্রসিডন্ট এবং এশিয়ায় মার্কটিংয়র দায়িত্বপ্রাপ্ত টন ফ্লারস বলছন,
'প্রথম আমরা হলদিয়ায় ১৫০ কাটি টাকা ব্যয় একটি এল পি জি টার্মিনাল তৈরির
পরিকল্পনা করছিলাম। এজন্য ২২ একর জমিও কনা হয়ছিল। কিন্তু এল পি জি-ত সরকারি ভরতুকি
অব্যাহত থাকায় আপাতত সই পরিকল্পনা বাতিল করা হয়ছ।' </p>

<p>এল পি জি অটা স্টশন
তৈরির অনুমতি চয় পশ্চিমবঙ্গ সরকারর কাছ আবদন জানিয়ছ এস এইচ ভি। সরকার তা বিবচনা
কর দখছ। এল পি জি অটা স্টশন হল কলকাতায় গ্যাস গাড়ি চালানার পরিকল্পনা সার্থক হব।

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বাজপয়ীর
পাক সফর পূর্ণ সমর্থন মন্ত্রিসভার</p>

<p>প্রধানমন্ত্রী
অটলবিহারী বাজপয়ীর প্রস্তাবিত পাক সফরক পূর্ণ সমর্থন জানাল কন্দ্রীয় মন্ত্রিসভা। বুধবার কন্দ্রীয়
মন্ত্রিসভার বৈঠকর পর প্রমাদ মহাজন এই কথা জানান। মহাজন বলন, আগ্রা বৈঠক ভস্ত
যাওয়ার পর প্রধানমন্ত্রীর পাক সফরর আমন্ত্রণ গ্রহণ নিয় মন্ত্রিসভা দ্বিধাবিভক্ত বল য সংবাদ
রটছ তা পুরাপুরি মিথ্যা ও ভিত্তিহীন। এমন কী, বৈঠক হাজির শিবসনার মন্ত্রীরাও কানও
আপত্তি তালননি বল দাবি মহাজনর। শিবসনা প্রধান বাল ঠাকর অবশ্য মঙ্গলবারই বাজপয়ীর
পাক সফরর তীব্র বিরাধিতা করছিলন। </p>

<p>এদিক, বুধবার রাত
ইসলামাবাদ ফডারল ক্যাবিনট এবং জাতীয় নিরাপত্তা পর্ষদর যৗথ বৈঠক ডকছন পাক প্রসিডন্ট
পরভজ মুশারফ। মন্ত্রিসভার বৈঠক অবশ্য পূর্ব নির্ধারিতই ছিল। যুগ্ম বৈঠক প্রধানত আগ্রা
বৈঠক নিয়ই আলাচনা হব। কন যৗথ ঘাষণাপত্র সই করা সম্ভব হল না, তা ব্যাখ্যা করবন
মুশারফ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>জলপাইগুড়িত
ম্যালরিয়ায় মৃত ৯</p>

<p>জলপাইগুড়ি জলার গত
কয়কদিন ম্যালরিয়া রাগ আক্রান্ত হয় ৯ জনর মৃতু্য হয়ছ। বসরকারি মত অবশ্য
মৃতু্যর সংখ্যা অনক বশি। ৫০ জনর বশি ম্যালরিয়া রাগীক জলার বিভিন্ন হাসপাতাল ও
স্বাস্হ্যকন্দ্র ভর্তি করা হয়ছ। রাগর প্রকাপ সবচয় বশি নাগরাকাটা ব্লক। মৃতু্যর
সংখ্যাও এখানই সবচয় বশি। স্বাস্হ্য দপ্তর সূত্র জানানা হয়ছ য, আপাতত ম্যালিগন্যান্ট
ম্যালরিয়া ছড়ায়নি। ম্যালরিয়া প্রতিরাধ হাসপাতাল ও স্বাস্হ্যকন্দ্রগুলিত ক্লারাকুইন,
প্রাইমাকুইন প্রভৃতি ট্যাবলট মজুত করা হচ্ছ। শাবার সময় প্রত্যকক মশারি ব্যবহার করার
অনুরাধ জানান হয়ছ। শহর জম থাকা জল পরিষ্কার করত বিশষ অভিযান নামছ পুরসভা।
প্রতিটি বাড়ির ট্যাঙ্ক ও চৗবাচ্চা নিয়মিত পরিষ্কার করত বলা হয়ছ। জলার স্বাস্হ্য
আধিকারিক বলছন, সমস্ত কর্মীর ছুটি বাতিল হয়ছ। ম্যালরিয়া যাত মহামারীর রূপ না নয়
সজন্য কাজ চলছ যুদ্ধকালীন তত্পরতায়। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>চালু হচ্ছ
তন্তুজ মিলনিয়াম কার্ড</p>

<p>বুধবার তন্তুজ মিলনিয়াম
কার্ড চালু করার কথা ঘাষণা করল রাজ্য সরকার। ক্ষুদ্র ও কুটির শিল্পমন্ত্রী বংশগাপাল চৗধুরি
মহাকরণ বলন, ১ আগস্ট থক এই বিশষ প্রকল্প চালু হব। মিলনিয়াম কার্ডর দাম হয়ছ
৪৮৫ টাকা। এই কার্ড কিন তন্তুজর যকানও শা-রুম থক ৫০০০ টাকার জিনিস কিনল ১২০০
টাকার পছন্দমত জিনিস বিনামূল্য পাওয়া যাব। </p>

<p>মিলনিয়াম কার্ডর
সময়সীমা হব ৫ বছর। অর্থাত্, এই সময়সীমার মধ্য যতবার খুশি এই কার্ড ব্যবহার কর কানও ক্রতা
বিশষ সুবিধা নিত পারবন। বিমার সুবিধাও থাকছ তন্তুজ মিলনিয়াম কার্ড। এর সর্বাচ্চ
পরিমাণ ৫০ হাজার টাকা। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বিভিন্ন প্রকল্প
জলর মতা অপব্যয়, অভিযাগ এ জি-র</p>

<p>বিশ্বব্যাঙ্কর আর্থিক
সহায়তায় স্বাস্হ্য প্রকল্পর কাজ বাজ খরচর অভিযাগ তুললন অ্যাকাউন্ট্যান্ট জনারলর
অডিটররাই। এর পাশাপশি আরও কিছু ক্ষত্র সরকারি টাকা গচ্চা যাওয়ার অভিযাগ তুলছন
তাঁরা। বিশ্বব্যাঙ্কর আর্থিক সহায়তায় পশ্চিমবঙ্গ হলথ সিস্টম প্রকল্পর কাজ শুরু হয় ১৯৯৬
সালর জুন। ওই প্রকল্প বিজয়গড় স্টট জনারল হাসপাতালর উন্নতির জন্য আনুমানিক খরচ
ধরা হয় ৮৮ লক্ষ ৯৪ হাজার ৪৬৬ টাকা। রাজ্য স্বাস্হ্য দপ্তর অনুমাদন কর ১৯৯৯ সালর ১৮
জানুয়ারি। একটি ঠিকাদারি সংস্হাক ৯৮ লক্ষ ৯৪ হাজার ৪৬৬ টাকায় টন্ডার দওয়া হয়। কাজ শষ
হওয়ার পর খরচ দাঁড়ায় ১ কাটি ১৩ লক্ষ ৪৭ হাজার ২১৭ টাকা। </p>

<p>হাসপাতালর পুরনা
বাড়িটির সংস্কার ছাড়াও খরচর মধ্য ধরা হয় নতুন বাড়ি নির্মাণ, মর্গ, রান্নাঘর নির্মাণ
প্রভৃতি। অ্যাকাউন্ট্যান্ট জনারলর (এ জি) অডিটররা ইন্সপকশন রিপার্ট অভিযাগ করছন,
কনসালট্যান্ট য ডিজাইন নির্মাণর পরামর্শ দিয়ছিলন, জনস্বাস্হ্য ইঞ্জিনিয়ারিং বিভাগর
এক্সিকিউটিভ ইঞ্জিনিয়ার তা পাল্ট দন। রাজ্য সরকারর এর জন্য বাড়তি ১৪ লক্ষ ৬৫ হাজার টাকা খসারত
দিত হয়ছ। </p>

<p>অডিট ইন্সপকশন রিপার্ট
ক্ষাভ প্রকাশ কর বলা হয়ছ, বাজট টাকার অভাব গ্রাম জল সরবরাহ প্রকল্পর কাজ শষ
করা হয়নি। অথচ অফিসারদর জন্য অতিরিক্ত গাড়ি এবং লঞ্চর ভাড়া বাবদ লক্ষ লক্ষ টাকা খরচ করা
হয়ছ। অফিসারদর জন্য দপ্তরর তিনখানি সরকারি গাড়ি আছ। মাট ছ'খানি গাড়ির দরকার হত
পার। সখান ১৮ খানি গাড়ি এবং এক খানি লঞ্চ ভাড়া করা হয়ছ। লঞ্চর জন্য ভাড়া বাবদ খরচ
হয়ছ চার লক্ষ ৭৯ হাজার ৮৪৩ টাকা এবং ১৮ খানি গাড়ির জন্য ১৪ লক্ষ ৭১ হাজার ৪০২ টাকা।
</p>

<p>এর পাশাপাশি অডিট
ইন্সপকশন রিপার্ট বলা হয়ছ, গাড়ি ভাড়া করার জন্য অর্থ দপ্তরর অনুমাদন নওয়া হয়নি। কান
কান অফিসার ভাড়া করা গাড়ি ব্যবহার করছন, তাঁদর নামও ডিভিশনাল অফিসার জানাত পারননি।
লঞ্চ ভাড়ার ব্যাপারও অডিটররা সন্দহ প্রকাশ করছন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p> </p>

<p>কেন বাজপয়ী?</p>

<p>আগ্রার শীর্ষ বৈঠক কর
পরভজ মুশারফক 'হিরা' কর দিয়ছন বাজপয়ী। তিনি নিজ 'জিরা' হননি
হয়ত, তব তাঁর কর্তৃত্ব অনকটাই কমল। এ হন একটা শীর্ষ বৈঠক তাঁর জনপ্রিয়তা বাড়াত কানও
সাহায্যই করত পারব না। আন্তর্জাতিক বা অভ্যন্তরীণ- কানও মহল থকই পাকিস্তানর সঙ্গ
বৈঠক বসার জন্য তমন কানও চাপ ছিল না। মুশারফক ডক কী কামাল দখাত চয়ছিলন
তা বাজপয়ীই জানন। পাকিস্তানর সঙ্গ যতবার ভদ্রতা দখাত গিয়ছন, ততবারই বাজপয়ী গাল
চড় খয়ছন। এবার খলন সধ। লাহার বাসযাত্রার পর কার্গিল দখত হয়ছিল। আগ্রা বৈঠকর
পর য তিক্ততা সৃষ্টি হয়ছ তা কূটনৈতিক মলম দিয় সারানা যাব না। কাশ্মীর সন্ত্রাস তা
বাড়বই, ভারতর সর্বত্র তা ছড়িয় পড়ত পার। মুশারফ ক্ষমতা দখল কর ভারতর সঙ্গ বৈঠক বসত
ব্যাকুল হয় উঠছিলন এমনও নয়। তাঁর কথাবার্তায় তমন কানও ইঙ্গিত পাওয়া যায়নি। তাঁক
আমন্ত্রণ জানানা, প্রসিডন্ট হয় বসার আগই প্রসিডন্ট বল সম্বাধন, দ্রুত স্বীকৃতি দওয়া-
বাজপয়ীর আচরণই কমন একটা অস্বাভাবিকতা দখা গছ। শীর্ষ বৈঠকর আমন্ত্রণ স্বীকার করলও
মুশারফ কানও আগাম আলাচনায় যত চাননি। ভারত প্রতিনিধি পাঠাত চাইলও তিনি রাজি
হননি। বরং শীর্ষ বৈঠকর আগই কাশ্মীর নিয় গরম গরম কথাবার্তা শুরু কর দন। মুশারফর
এই মনাভাব দখও বাজপয়ীর উত্সাহ কানও ভাঁটা পড়নি। ভারত এসই মুশারফ কাশ্মীর
প্রসঙ্গ পাড়ন পাকিস্তানি সাংবাদিকদর সঙ্গ আলাচনায়। রাষ্ট্রপতির ভাজসভাতও একই কথা।
পরদিন ভারতীয় সম্পাদকদর সঙ্গ কথাবার্তা পাকিস্তান টিভি মারফত সম্প্রচার কর কার্যত কাশ্মীর
ইসু্যটাক আন্তর্জাতিক স্তর নিয় যত তিনি সফল। এরপরও ভারত অতিথির প্রতি সৗজন্য দখিয়
গছ। পাকিস্তানর তরফ কানরকম কূটনৈতিক শিষ্টতা দখা যায়নি। বরং লালকৃষ্ণ আডবাণীর
বিরুদ্ধ বৈঠক ভস্ত দওয়ার অভিযাগ তুল অশাভন ইঙ্গিতও করা হয়ছ। </p>

<p>মুশারফ এই সুযাগ
পাকিস্তান নিজর জনপ্রিয়তা বাড়াবার একটা ইসু্য পয় গছন। দম দিয় দিয়ছন
সন্ত্রাসবাদীদর স্বাধীনতা সংগ্রামী বল। মুশারফই কার্গিল যুদ্ধ বাধিয়ছিলন। তাঁক জামাই
আদর কর কানও লাভ হয়নি। বাজপয়ী কন তাঁক ডাকলন সটাই একটা রহস্য। শীর্ষ বৈঠক
কী আলাচনা হয়ছিল, মুশারফর মনাভাব কী ছিল- তা দশবাসীক যতটা সম্ভব জানিয় দওয়াই
বাজপয়ীর কর্তব্য।</p>

<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>আলাচনা
চলত থাকুক, চায় রাষ্ট্রসংঘ, আমরিকা, রাশিয়া</p>

<p>আগ্রা শীর্ষ বৈঠক কানও
ফল না হলও ভারত ও পাকিস্তানর মধ্য আলাচনা শুরু হওয়াকই গুরুত্বপূর্ণ বল মন করন
রাষ্ট্রসংঘর মহাসচিব কাফি আন্নান। তিনি বলন, দুই দশকই আলাচনা প্রক্রিয়া চালিয় যত
হব। আমরিকাও এই ব্যাপার হতাশ নয়। ভারত ও পাকিস্তানর মধ্য সর্বাচ্চ স্তর কথাবার্তা
চালিয় যাওয়া উচিত বল জানিয়ছন হায়াইট হাউসর প্রস সক্রটারি অঁরি ফ্লশার। একই
সুর বিদশ দপ্তরর মুখপাত্র রিচার্ড বাউচার বলছন, এশিয়ার উত্তজনা কমাত দুটি দশ পরস্পরর
সঙ্গ কথা বলছ, এটাই যথষ্ট আশার কথা। কাশ্মীর প্রসঙ্গ বাউচার বলন, মার্কিন যুক্তরাষ্ট্র এই
আলাচনার শরিক নয়। তাই আলাচনা কান পথ চলা উচিত তা বল দওয়া আমাদর পক্ষ সম্ভব
নয়। </p>

<p>ভারত-পাকিস্তান শীর্ষ
বৈঠকক স্বাগত জানিয় কাফি আন্নান বলছন- দুই দশর বিরাধ মটানার পথ আগ্রা শীর্ষ
বৈঠক একটি পদক্ষপ মাত্র। একটি বৈঠকই সব সমস্যা মিট যাব এটা আশা করা ভুল। তাই দরকার
ধারাবাহিকভাব এই প্রক্রিয়াক চালিয় যাওয়া। শীর্ষ বৈঠকক ব্যর্থ বল মানত নারাজ
রাশিয়াও। রুশ পররাষ্ট্র দপ্তরর মুখপাত্র বলছন, উপমহাদশ টনশন ও হিংসা কমাত দুই
রাষ্ট্রপ্রধানই য আগ্রহী, বৈঠক সটাই প্রমাণিত হয়ছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বাজপয়ীর
পাক সফরর বিরাধী আর এস এস</p>

<p>আগ্রা শীর্ষ বৈঠক
ব্যর্থ হওয়ার পর প্রধানমন্ত্রী অটলবিহারী বাজপয়ী এখনই পাকিস্তান যান তা চায় না রাষ্ট্রীয়
স্বয়ংসবক সংঘ। আর এস এস-এর মত, আগামী তিন-চার মাসর মধ্য পাকিস্তান পালাবদল ঘটত
পার। মুশারফ ক্ষমতায় থাকবন কিনা, সটাও নিশ্চিত নয়। ফল এখনই বাজপয়ীর পাকিস্তান
যাওয়া উচিত হব না। </p>

<p>আর এস এস মুখপাত্র
আর এস বৈদ্য বলন, কাশ্মীর মুসলিম প্রধান বলই পাকিস্তান সখানকার দায়িত্ব নিত পার
না। ইসলামাবাদ কি পশ্চিমবঙ্গ, বিহার বা করলর মুসলিমদর সমস্যা মটাত সক্ষম? তাঁর মত,
উপত্যকার প্রতিনিধি কারা সটা ঠিক করার দায়িত্ব সখানকার মানুষদর ওপরই ছড় দওয়া যত
পার। আগামী এক বছরর মধ্যই কাশ্মীর বিধানসভা ভাট। হুরিয়ত যদি নিজদর কাশ্মীরিদর
প্রতিনিধি বল দাবি কর তাহল ওরা বরং নির্বাচন প্রতিদ্বন্দ্বিতা করুক। </p>

<p>শীর্ষ বৈঠক সম্পর্কও
সংঘর দৃষ্টিভঙ্গি বশ আকর্ষণীয়। সরকারিভাব এক স্বাগত জানিয়ছিল আর এস এস। প্রকাশ্য এক
ব্যর্থ বলতও তারা রাজি নয়। তব বৈদ্য পরিষ্কার জানিয়ছন, বর্তমান পরিস্হিতিত বাজপয়ীর
পাকিস্তান সফর সফল হওয়ার কানও সম্ভাবনা নই।  
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p> </p>

<p>মসজিদ ধ্বংসর
কথা ভাবিনি : যোশি</p>

<p>বাবরি মসজিদ ধ্বংসর
কথা বিজপি নতারা কখনও ভবও দখননি বল বুধবার লিবারহান কমিশন জানালন কন্দ্রীয়
মানব সম্পদ উন্নয়ন মন্ত্রী মুরলিমনাহর যাশি। তাঁর কথায়, বিজপি আলাপ আলাচনার মাধ্যম
রামমন্দির গড়ার পক্ষপাতী। মসজিদ ভঙ মন্দির তৈরির কথা তাঁরা স্বপ্নও ভাবননি। বজরঙ দল
মসজিদ ধ্বংসর জন্য স্বচ্ছাসবকদর প্রশিক্ষণ দিয়ছিল- একথাও অস্বীকার করন যাশি। আডবাণীর
রথযাত্রার সময় মুসলিমদর মধ্যও ব্যাপক উত্সাহ ছিল। তাঁদর অনক অযাধ্যা পর্যন্তও গিয়ছিলন,
বিচারপতি লিবারহানর প্রশ্নর উত্তর জানিয়ছন যাশি। তিনি বলন, রাজনৈতিক নতারাই
পরবর্তীকাল বিষয়টি সাম্প্রদায়িক ইসু্য কর তুলছিলন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>রায়গঞ্জ
গরিষ্ঠতা পেল না কংগ্রস</p>

<p>প্রিয়রঞ্জন দাশমুন্সির
খাসতালুক রায়গঞ্জ পুরসভার ভাট গরিষ্ঠতা পল না কংগ্রস। ২৬টি আসনর মধ্য কংগ্রস ১০,
সিপিএম ৯, সিপিআই ও বিজপি ১টি কর আসন পয়ছ। বাকি পাঁচটি আসন পয়ছন নির্দল
প্রার্থীরা। চয়ারম্যান ও ভাইস চয়ারম্যান দুজনই পরাজিত হয়ছন। </p>

<p>বার্ড গড়ত কমপক্ষ
১৪ জন কাউন্সিলারর সমর্থন দরকার। ফল কংগ্রস বা সিপিএম- দুই পক্ষকই নির্ভর করত হব
নির্দলদর ওপর। কংগ্রস ভঙ বরিয় এস নির্দল হয় জতা শঙ্কর চক্রবর্তী জানিয় দিয়ছন-
চয়ারম্যান করা না হল কানও পক্ষকই তিনি সমর্থন করবন না। বাকি ৪ নির্দল প্রার্থীও তাঁর
সঙ্গ রয়ছন বল দাবি শঙ্করবাবুর। প্রসঙ্গত, প্রিয়-শিবিরর সঙ্গ বিবাদর জরই শঙ্কর চক্রবর্তী
দল ছড়ছন। </p>

<p>উল্লখযাগ্য ভাল ফল করছ
সিপিএম। গতবার তারা মাত্র ৫ আসন জিতছিল। জলা সম্পাদক বীরশ্বর লাহিড়ি বলছন, কংগ্রসক
জার ধাক্কা দিত পরছি- এটাই বড় কথা হয়ছ। কংগ্রসর স্হানীয় নতা পবিত্র চন্দ বলছন,
প্রিয়দার সঙ্গ কথা হয়ছ। উনি এখান এলই সব ঠিক হয় যাব। বার্ড গড়ব কংগ্রসই।

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>সাসপণ্ড কর
ঠেকাত পারব না : পাঁজা</p>

<p>অজিত পাঁজাক সাসপণ্ড
করার কথা প্রধানমন্ত্রী বাজপয়ী এবং এনডিএ-র আহ্বায়ক জর্জ ফার্নাণ্ডজক জানিয় দিল
তৃণমূল। স্বভাবতই পাঁজা নিজক তৃণমূলর প্রতিনিধি বল দাবি করত পারবন না। এদিক
পাঁজা বলছন, 'সাসপণ্ড করার আগ আমাক নাটিস দওয়া হয়নি। ২০ নম্বর ধারায় সাসপণ্ড
করার কথা বলা হচ্ছ, তৃণমূলর গঠনতন্ত্র ২০ নম্বর ধারাটাই নই।' কাণঠাসা পাঁজার দাবি,
'মমতার ওই সভায় উপস্হিতদর অনকই আমার সঙ্গ আছন।' পাঁজার বক্তব্য, মন্ত্রী হওয়া ঠকাতই
মমতা আমাক বহিষ্কার করল না। মমতা ভীরু। এর থক অনক কম অপরাধ বাণী সিংহরায়, তপন
দাশগুপ্তদর বহিষ্কার করা হয়ছ। প্রধানমন্ত্রী চাইল আমি মন্ত্রী হব। কারণ সখান দল থাকা
বা সাসপণ্ড হওয়ার ওপর কিছু নির্ভর কর না। লাকসভার আসন্ন অধিবশন সরকারপক্ষর আসন
বসত চয় স্পিকারক চিঠি দিয়ছন পাঁজা। </p>

<p>এদিক, জর্জ ফার্নাণ্ডজ
বিদশ থক দিল্লিত ফিরছন বুধবার। তাঁক ঘির তৃণমূল জার তত্পরতা। পাঁজাক মন্ত্রী
না করার জন্য তদ্বির তা হবই, এনডিএ-ত ঢাকার রাস্তা নিয়ও কথাবার্তা হব। এমনিতই ২১
জুলাইয়র কর্মসূচি নিয় তৃণমূলর দুটি শিবির তুমুল উত্তাপ। মমতা ইতিমধ্যই এনডিএ-ত
ফির যাওয়ার সিদ্ধান্ত নিয় ফলছন। তব সরকার যাগ না দিয় বাইর থক সমর্থনর কথা
ভাবছন। শর্ত নিয়ই এখন আলাচনা চলছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>নন্দনর আদল
অডিটারিয়াম শ্রীরামপুর</p>

<p>সুস্হ সংস্কৃতি আর
বিনাদনর লক্ষ্য 'নন্দনর' আদল শ্রীরামপুর গড় উঠছ এক অত্যাধুনিক অডিটারিয়াম।
আর এরই পাশাপাশি চালু হচ্ছ একটি আধুনিক ফিল্ম ইনস্টিটিউটও। মহানায়ক উত্তমকুমারর নাম
উত্সর্গ করা এই অডিটারিয়াম ও ফিল্ম ইনস্টিটিউট তৈরির ব্যয়ভার বহন করছ একটি বসরকারি
প্রতিষ্ঠান। আগামী ১১ আগস্ট উদ্বাধন করা হব এই অডিটারিয়াম ও ফিল্ম ইনস্টিটিউটর।
</p>

<p>অত্যাধুনিক এই অডিটারিয়াম
পূর্ণাঙ্গ য ফিল্ম ইনস্টিটিউট তৈরি হচ্ছ তাত থাকব চলচ্চিত্র শিল্পর নানা শিক্ষাক্রমর
পঠনপাঠন ব্যবস্হা। থাকব চলচ্চিত্র সংক্রান্ত আর্ট গ্যালারি আর আর্কাইভ। দু কাটি টাকারও বশি
অর্থব্যয় নির্মীয়মান মহানায়কর নাম এই ধরনর অডিটারিয়াম ও ফিল্ম ইনস্টিটিউট রাজ্য এই
প্রথম বল জানিয়ছন প্রতিষ্ঠানর কর্ণধার বিধায়ক অধ্যাপক স্বরাজ মুখাপাধ্যায়। তৃণমূল কংগ্রস
বিধায়ক স্বরাজবাবু জানিয়ছন, দলনত্রী মমতা বন্দ্যাপাধ্যায়র ইচ্ছানুসারই এই বিনাদন কন্দ্রর
নাম মহানায়ক উত্তমকুমারর স্মৃতিত উত্সর্গ করা হচ্ছ। এই কন্দ্রর আর্ট গ্যালারিত উত্তমকুমারর
অভিনয় জীবনর নানা তথ্যসমৃদ্ধ সঙ্কলনও থাকব। এখান বসানা হব মহানায়কর এক মর্মর
মূর্তি। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p> </p>

<p>তিনজন শেফ
দ্য মিশন নিয় ফুটবল দল ইংল্যাণ্ড যাচ্ছ বৃহস্পতিবার</p>

<p>ভারতীয় ফুটবলর কথা
বাধহয় অমৃত সমান। এর তুঘলকী কাণ্ডর কথা শুনল ঢাকার কুট্টিরা হয়তা বলবন : আর কইয়ন
না ছার। আমার ঘাড়ায় হাইসবা। </p>

<p>গতবার ইংল্যান্ড য
ভারতীয় ফুটবল দল গিয়ছিল তাঁর সঙ্গ অতিরিক্ত কয়কজন কর্মকর্তা যাওয়ায় কঠার সমালাচনা
হয়ছিল। আবার যাত অমন কিছু না হয়, তাই তিনজন শফ দ্য মিশন যাচ্ছন। হঁ্যা তিনজন শফ
দ্য মিশন। অদ্ভুত কাণ্ড। ফুটবলর বিশ্বরকর্ড, গিনস বুক অব ওয়ার্ল্ড রকর্ড প্রভৃতিত এটি
স্হান পাবই। </p>

<p>অলিম্পিক, কমনওয়লথ গমস,
এশিয়ান গমস প্রভৃতি বহু খলার প্রতিযাগিতারও প্রতিটি দশর একজন শফ দ্য মিশন থাকন।
কিন্তু অল ইন্ডিয়া ফুটবল ফডারশন নজির গড়ত চলছ একই খলায় তিনজন শফ দ্য মিশন পাঠিয়।
খবরটি জন কউ কউ বলছন, এই না হল ভারতীয় ফুটবল? য দশর ৭০ মিনিটর একটি লিগ
ম্যাচ ১১৯ গাল হয়, স দশর ফুটবল দল তিনজন শফ দ্য মিশন থাকবন- এত আশ্চর্যর
কিছু নই। </p>

<p>তিন শফ দ্য মিশন হলন
গুজরাটর জাদজা এবং গায়ার অ্যাগনাস ও নাথু সিং। এ নিয় এ আই এফ এফ-এ কথা উঠছ সুযাগ
যখন আছ অতিরিক্ত কর্মকর্তা না পাঠিয় বাড়তি খলায়াড় পাঠাল তাঁরা সামান্য হলও
অভিজ্ঞতা অর্জন করত পারবন। </p>

<p>এই সফর চারটি
প্রদর্শনী ম্যাচ খলব ভারতীয় দল। প্রথম ম্যাচ ২৪ জুলাই গ্রিফিন পার্ক দ্বিতীয় ডিভিশনর ব্রন্টফার্ড
এফ সি-র বিরুদ্ধ, ২৮ জুলাই দ্বিতীয় ম্যাচ প্রথম ডিভিশনর নটিংহ্যাম ফরস্টর বিরুদ্ধ। ৩১
জুলাই তৃতীয় ডিভিশনর লটন ওরিয়ন্টর বিরুদ্ধ। চতুর্থ ও শষ ম্যাচটি বশ গুরুত্বপূর্ণ।
ওটি প্রথম ডিভিশনর ওয়ালশ এফ সি-র বিরুদ্ধ। সবই প্রদর্শনী ম্যাচ হলও ভারতীয় দলর কাচ
সুখবিন্দর সিং প্রতিটি ম্যাচক গুরুত্ব দিচ্ছন। দল রওনা হব ১৯ জুলাই। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>আমরা
অধিনায়ক নির্বাচন করি একটি সিরিজর জন্যই : চাঁদু বোরদ</p>

<p>নিজর বিবৃতির
প্রতিবাদ করলন ভারতর ক্রিকট নির্বাচক কমিটির চয়ারম্যান চাঁদু বারদ। বলছন,
আমরা অধিনায়ক নির্বাচন করি একটি সিরিজর জন্যই। বর্তমান নির্বাচকমণ্ডলীর আয়ু যতদিন,
ততদিন সৗরভ গঙ্গাপাধ্যায় অধিনায়ক থাকবন, এমন কথা বলিনি। সৗরভক অমন কানও আশ্বাস
বা প্রতিশ্রুতি দওয়া হয়নি, জানান বারদ। </p>

<p>শচীন তন্ডুলকর
অধিনায়ক হচ্ছন কিনা এ প্রসঙ্গ বারদ বলন, শচীন যদি অধিনায়ক হত চান তা হল
নিয়মকানুন মন আমাদর কাছ অনুরাধ পাঠাত পারন। </p>

<p>এর আগ শচীন য নতৃত্ব
থক সর যান, সই প্রসঙ্গ বারদ বলন, শচীনক সরানা হয়নি। তিনিই নতৃত্ব থাকত
অনীহা প্রকাশ করছিলন। বারদ আরও বলন, এই ধরনর নানা বক্তব্য ভারতীয় ক্রিকটর
উন্নতি হব না। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>রোহন
গাভাসকরক বাংলা দলর দায়িত্ব দেওয়া হত পার</p>

<p>সুনীল তনয় রাহন
গাভাসকরর বাংলা ক্রিকট দলর অধিনায়ক হওয়ার সম্ভাবনা যথষ্ট। গত বছর তার ভাল খলা তাক
সি এ বি-র 'সরা খলায়াড়' নির্বাচিত কর। তিনটি সঞ্চুরি-সহ গতবার রন্জিত বশ খলন
রাহন। এ জন্যই সি এ বি-র নির্বাচকমণ্ডলী তাঁক নতৃত্ব দওয়ার কথা বিবচনা করছন।
</p>

<p>আগস্টর শুরুতই
বাংলার নব নির্বাচিত কাচ কারসন ঘাউড়ি কলকাতায় এস অনুশীলন শুরু করাবন। রন্জি ট্রফির
প্রস্তুতির জন্য বাংলা খলব চন্নাইয় বুচিবাবু  ট্রফিত। খলব লখনউ-এ মইন-উ-দ্দৗলা
গাল্ড কাপ। এবং বাঙ্গালারর কে সি সি ট্রফিতও। এই তিনটি প্রতিযাগিতায় ট্রায়াল হিসব
রাহনক নতৃত্বর দায়িত্ব দওয়া হব। সফল হল রন্জি দলর দায়িত্বও দওয়া হত পার।
এই তিনটি প্রতিযাগিতার জন্য দল নির্বাচন হব ২৪ জুলাই। </p>

<p> </p>

<p> </p>






</body></text></cesDoc>