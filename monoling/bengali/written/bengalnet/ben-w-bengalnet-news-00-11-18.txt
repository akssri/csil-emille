<cesDoc id="ben-w-bengalnet-news-00-11-18" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-00-11-18.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 00-11-18</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>00-11-18</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>শচীনক
ফাঁসাত চাইছন আজহার</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>সি বি আই প্রাক্তন ডিরক্টর এবং
ক্রিকট বার্ড নিযুক্ত স্পশাল কমিশনার ক মাধবনর কাছ শচীন তণ্ডুলকরর বিরুদ্ধ মনর
ঝাল মিটিয়ছন প্রাক্তন অধিনায়ক মহম্মদ আজহারউদ্দিন। বৃহস্পতিবার ৬ ঘন্টা ধর জিজ্ঞাসাবাদ
পর্ব ঠিক কী কথা হয়ছ স ব্যাপার কানও পক্ষই মুখ খুলত রজি না হলও, বিশ্বস্ত সূত্রর
খবর শচীনর বিরুদ্ধ মাধবনর কাছ একগুচ্ছ অভিযাগ করছন আজহার। এর মধ্য প্রধান হল
অধিনায়ক থাকাকালীন শচীন নাকি আজহারক দলর বাইর রাখার চষ্টা করতন। এ কাজ কয়কজন
সতীর্থও নাকি শচীনক সাহায্য করতন। আজহার যদি সত্যিই একথা বল থাকন, তার একটা
গুরুত্ব থাকব। কারণ, একসময় শচীন আজহারর সম্পর্ক য তিক্ততার চরম পর্যায় পৗঁছছিল তা
সংবাদমাধ্যমর অন্তত অজানা নয়। ঘনিষ্ট মহল শচীন একাধিকবার বলছন আজহার দল থাকল
তিনি খলত চান না। </p>

<p>৬ ঘন্টা ধর মাধবনর
সঙ্গ আজহারর য কথা হয়ছ তার আয়তন প্রায় ১২ পাতা। বিবৃতিত মাধবন আজহারক দিয়
সইও করিয় রখছন। শচীন ও কপিলর সঙ্গও কথা বলত চান মাধবন। </p>

<p>শিল্পায়ন
সবার সাহায্য চান মুখ্যমন্ত্রী</p>

<p>
 
 
<gap desc="illustration"></gap>পশ্চিমবঙ্গর শিল্পায়ন ত্বরান্বিত
করত সমস্ত রাজনৈতিক দলর সহযাগিতা চাইলন বুদ্ধদব ভট্টাচার্য। তিনি বলন, শিল্পায়ন শুধু
একটা দলর ব্যাপার নয়। রাজ্যর সামগ্রিক উন্নয়নর কথা মন রখ রাজনৈতিক মত পার্থক্য সরিয়
না রাখত অগ্রগতি বাধা পত পার। মমতা ব্যানার্জি ও বি জ পি-র সাহায্যও য তিনি চান
তাও বলন মুখ্যমন্ত্রী। পাশাপাশি তাঁর ধারণা, রাজ্য সরকার শিল্পায়নর লক্ষ্য য প্রয়াস চালাচ্ছ
তাত বাধা দওয়ার ক্ষমতা মমতারও নই। শুক্রবার মহাকরণ মুখ্যমন্ত্রী সাংবাদিকদর আরও বলন,
শিল্পায়নর সহায়ক পরিবশ রাজ্য রয়ছ। বহু শিল্পপতি এখান বিনিয়াগ করত চাইছন।
প্রসঙ্গত, বুধবার একটি সাক্ষাত্কার বুদ্ধদব শিল্পায়নকই তাঁর প্রধান চ্যালঞ্জ বল বর্ণনা করছন।
</p>

<p>শিল্পান্নয়ন নিগামর চয়ারম্যান
সামনাথ চ্যাটার্জিও নতুন মুখ্যমন্ত্রীর ভূমিকায় খুশি। সামনাথবাবু বলন, শিল্পর ব্যাপার
বুদ্ধদব সদর্থক দৃষ্টিভঙ্গি নিয় চলছন। এদিন সামনাথবাবু মুখ্যমন্ত্রীর সঙ্গ একান্ত কিছুক্ষণ
কথা বলন। এখনই তিনি শিল্পান্নয়ন নিগমর চয়ারম্যানর পদ ছাড়ছন না বল জানা গছ। 
</p>

<p>সিপিএম-এর
'দাদাগিরি' মানত নারাজ সি পি আই</p>

<p>পশ্চিমবঙ্গ ও করল সি
পি এমর 'দাদাগিরি' বামফ্রন্টর পক্ষ ক্রমশ ক্ষতিকর হয় উঠছ বল সি পি আই অভিযাগ
করছ। দিল্লিত সি পি আই-এর প্লনাম প্রধানত এই দুই রাজ্যর নতারা সি পি এমর তীব্র
সমালাচনা করছন। পাশাপাশি কন্দ্রীয় নতাদর কাছ দলর কর্মসূচি ঢল সাজা এবং কংগ্রসর
প্রতি মনাভাব কী হব তা পরিষ্কার কর ঘাষণার আর্জি জানান পশ্চিমবঙ্গর সি পি আই নতারা।
প্লনাম গৃহীত রাজনৈতিক প্রস্তাব করল ও পশ্চিমবঙ্গ বামফ্রন্টর সাফল্য তুল ধরার
পাশাপাশি সি পি এমক তীব্র কটাক্ষ করা হয়ছ। তব এই সমালাচনা অনকটা পরাক্ষ হওয়ার
প্রতিনিধিদর বড় অংশই খুশি নন। তাঁরা চয়ছিলন দল আরও কড়া ভাষায় সি পি এমর সমালাচনা
করুক। কংগ্রসর প্রতি মনাভাব বদলর দাবি করছন প্রতিনিধিদর একাংশ। তাঁদর মত,
অর্থনীতির ক্ষত্র কংগ্রসর দৃষ্টিভঙ্গি মানা সম্ভব না হলও সাম্প্রদায়িক বিজপিক রুখত কংগ্রসর
সাহায্য নওয়া ছাড়া উপায় নই।  </p>

<p>পাক সফর
বাতিল করায় 
ভারতক
কড়া শাস্তি দিত পার আই সি সি</p>

<p>পাকিস্তান ক্রিকট
সফর বাতিল করায় ভারতক কবল আর্থিক জরিমানা নয়, আই সি সি অন্য শাস্তিও দিত পার।
আই সি সি-র প্রসিডন্ট ম্যালকম গ্র এখন লাহার। তিনি হুমকি দিয়ছন, তারা ভারতর
সদস্যপদ বাতিল করও দিত পার। ফল ভারত কানও দশর সঙ্গ টস্ট খলত পারব না। তব শষর
ব্যাপারটি নিছক হুমকিই হত পার। কারণ ম্যালকম এখন পাকিস্তানই আছন। ভারত কন সফর
বাতিল করছ তা তিনি ভারতর ক্রিকট বার্ডর কাছ থক কিছুই শানননি। ব্যাখ্যাও
চাননি ভারতর কাছ। শুনল বক্তব্য বদল করবন। ম্যালকম গ্র অস্ট্রলিয়ান ক্রিকট বার্ডর
প্রাক্তন প্রসিডন্ট। সফর বাতিল করল সাধারণত আর্থিক জরিমানাই হয়। আই সি সি-র অর্থ
কমিটির সভায় যাগ দিত তিনি এখন করাচিত। ভারত থক অংশ নবন বার্ডর প্রাক্তন প্রসিডন্ট
রাজ সিং দুঙ্গারপুর। দুঙ্গারপুর পাকিস্তান রওনা হওয়ার আগ বল নিয়ছন, তিনি ওখান গিয়
সফর বাতিল নিয়  প্রশ্নর মুখামুখি হবন। তার যথাযথ ব্যাখ্যাও দবন। </p>

<p>ম্যালকম স্বীকার করছন,
কানও দশর সরকার সফর বাতিলর সিদ্ধান্ত নিল ক্রিকট বার্ড কী করব। এটা য কানও দশর
ক্রিকট বার্ডর ক্ষত্রই ঘটত পার। </p>

<p> </p>






</body></text></cesDoc>