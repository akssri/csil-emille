<cesDoc id="ben-w-bengalnet-news-01-12-12" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-12-12.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-12-12</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-12-12</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>কফিন কেলঙ্কারি
: তীব্র সঙ্কট বাজপয়ী সরকার</p>

<p>কারগিল সংঘর্ষর সময়
সনাবাহিনীর জন্য অস্ত্র, পাশাক, জুতা কনার ক্ষত্র ক্যাগর রিপার্ট ব্যাপক আর্থিক
অনিয়ম ধরা পড়ায় তীব্র সঙ্কট পড়ত চলছ বাজপয়ী সরকার। কন্দ্র সবচয় অস্বস্তিত, মৃত
সৈনিকদর জন্য কনা কফিন নিয় অনিয়ম ধরা পড়ায়। বিষয়টির স্পর্শকাতরতা বুঝ বিরাধীরা
এই ইসু্যত প্রতিরক্ষামন্ত্রী জর্জ ফার্নাণ্ডজর ইস্তফা দাবি করছন। কিছুদিন আগ তহলকা ইসু্যত
বিরাধীদর চাপ জর্জক সরাত বাধ্য হয়ছিলন বাজপয়ী। বঙ্কটস্বামী কমিশন ফার্নাণ্ডজক নির্দাষ
ঘাষণা করার আগই তাঁক মন্ত্রিসভায় ফরানায় তীব্র সমালাচনার মুখ পড়ছন বাজপয়ী।
সংসদ বিরাধীরা জর্জক কার্যত বয়কট করছন। কউ তাঁক কানও প্রশ্নও করছন না বা তাঁর
জবাবও শুনছন না। </p>

<p>এই অবস্হায় নতুন কর
কলঙ্কারিত জড়িয় পড়লন ফার্নাণ্ডজ। তহলকা ডট কম ছিল একটি বসরকারি প্রতিষ্ঠান। তাদর
ভিডিও টপক সরকার খুব বশি গুরুত্ব না দিলও এবার তা হব না। কারণ, যারা কফিন কলঙ্কারি
ধরছ, সই কম্পট্রালার অ্যান্ড অডিটর জনারল (ক্যাগ) কন্দ্রীয় সরকাররই সংস্হা। মন রাখত
হব, বাফর্স কলঙ্কারিও প্রথম ধরা পড় ক্যাগর রিপার্টই। </p>

<p>জর্জ
নিজ ইস্তফার সম্ভাবনা উড়িয় দিলও সরকার য যথষ্টই উদ্বিগ্ন তার প্রমাণ- আডবাণী নিজ জর্জ
ফার্নাণ্ডজক ডক পাঠানায়। জাপান থক দশ ফির বাজপয়ীও কথা বলছন আডবাণী-সহ দলর
অন্য শীর্ষ নতৃত্বর সঙ্গ। উত্তরপ্রদশ বিধানসভা নির্বাচনর আগ কফিন-কলঙ্কারি য বিজপি-ক
বড় ধরনর সমস্যায় ফলব, তা বলার অপক্ষা রাখ না। </p>

<p> </p>

<p>অর্থর অভাব
কেনা যায়নি ন্যাপথা 
হলদিয়া
পেট্রাকম উত্পাদন বন্ধ</p>

<p>টাকার অভাব
কাঁচামাল কিনত না পারায় হলদিয়া পট্রাকম উত্পাদন বন্ধ হয় গছ। কর্তৃপক্ষ অবশ্য বলছ,
যান্ত্রিক ত্রুটির কারণই বন্ধ কর দিত হয়ছ কারখানা। বশ কিছুদিন ধরই কাঁচামাল কিনত
হিমশিম খাচ্ছিল পট্রাকম। এমনকী, অর্থর অভাব দৈনিক ভিত্তিত কাঁচামাল নিয় ক্ষমতার অর্ধক
উত্পাদন করা হচ্ছিল। সামবার থক উত্পাদন পুরাপুরি বন্ধ হয় গছ। প্রতিশ্রুতিমত অর্থ না
দওয়ায় চ্যাটার্জি গাষ্ঠীর ওপর ক্ষুব্ধ অপর দুই অংশীদার রাজ্য সরকার ও টাটা শিল্পগাষ্ঠী।
বারবার সময় চয় নিয়ও টাকা মটাচ্ছন না পূর্ণন্দু চ্যাটার্জি। বর্ধিত সময়সীমার ময়াদ
বুধবার শষ হচ্ছ। </p>

<p>পেট্রাকমর
এক কর্তা জানিয়ছন- কাঁচামাল ন্যাপথা কনার মত অর্থ য নই, এটা সত্যি। পাশাপাশি এটাও
ঠিক, প্ল্যান্টর সার্কিট ব্রকারও গণ্ডগাল আছ। এই সময় উত্পাদন বন্ধ রাখায় নগদ অর্থ যাগাড়র
সময় যমন পাওয়া যাব, তমনি গুদাম সঞ্চিত পলিমার বিক্রিরও সময় মিলব। তব, চ্যাটার্জি গাষ্ঠী
অর্থ না দিল হলদিয়া চালু করা অনিশ্চিত। কারণ, ইন্ডিয়ান অয়ল সাফ জানিয় দিয়ছ- নগদ
দাম মটানা না হল তারা ন্যাপথা সরবরাহ করব না। </p>

<p> </p>

<p>১০
জানুয়ারি এস ইউ সি-র বাংলা বন্ধ</p>

<p>বিদু্যতর শুল্ক বৃদ্ধি,
হাসপাতালর চার্জ বাড়ানা এবং স্কুল ফি বাড়ানার প্রতিবাদ জানাত ১০ জানুয়ারি বাংলা
বন্ধ ডাকল এস ইউ সি আই। ২৪ ঘণ্টার বন্ধর সিদ্ধান্ত ঘাষণা কর রাজ্য সম্পাদক প্রভাস ঘাষ
বলন, বামফ্রন্ট সরকার য সিদ্ধান্ত নিয়ছ তা গরিব ও মধ্যবিত্ত মানুষক সঙ্কটর মুখ ঠল দব।
বিদু্যতর দাম বাড়িয় বিদু্যত্ শিল্পক পুরাপুরি ব্যবসায়ীদর হাত তুল দওয়া হয়ছ। ইতামধ্যই
বিদু্যত্ বিল বয়কটর ডাক দিয়ছ এস ইউ সি আই। প্রভাসবাবু বলন, গরিবর চিকিত্সার সুযাগ
ছিল সরকারি হাসপাতালই। কিন্তু চিকিত্সার খরচ ও পথ্যর দাম বহুগুণ বাড়িয় দওয়ার ফল
তাঁরা চিকিত্সার সুযাগ নিত পারবন না। </p>

<p>এস
ইউ সি-এর ডাকা বন্ধ সমর্থন করত রাজি নন তৃণমূল নত্রী মমতা ব্যানার্জি। তিনি বলন- এই
ইসু্যগুলি নিয় আমরাও পৃথকভাব আন্দালন করছি। ২ জানুয়ারি রাজ্য কমিটির বর্ধিত বৈঠক
বসছ। চূড়ান্ত কর্মসূচি ঘাষিত হব সখানই। </p>

<p> </p>

<p>আবহাওয়া      পূর্বাভাস : বুধবার প্রধানত পরিষ্কার আকাশ। দিনর সর্বাচ্চ তাপমাত্রা
২৯ ডিগ্রি ও দিনর সর্বানিম্ন তাপমাত্রা ১৬ ডিগ্রি সেলসিয়াসর কাছাকাছি থাকব। </p>

<p>তাপমাত্রা : মঙ্গলবারর সর্বাচ্চ ২৮.৮ (+২) এবং সর্বনিম্ন ১৭.১ (+২) ডিগ্রি সেলসিয়াস। </p>

<p>আপক্ষিক আর্দ্রতা
: সর্বাধিক
৯৬ শতাংশ এবং নূ্যনতম ৪৯ শতাংশ। </p>

<p>বৃষ্টি : হয়নি। </p>

<p>সোনার দাম 
(১০
গ্রাম)   স্ট্যাণ্ডার্ড ৪৬৯৫ টাকা  
২২ ক্যারট ৪৩৪০টাকা </p>

<p>রূপার দাম 
(প্রতি
কেজি)   বাট ৭২২৫ টাকা                                                    
খুচরা ৭৩২৫ টাকা </p>

<p>বিদশি মুদ্রা  ক্রয়মূল্য                         
বিক্রয়মূল্য </p>

<p>ডলার           ৪৬.৯৫                                 
৪৮.৩৫</p>

<p>পাউন্ড          ৬৭.১৫                                 
৬৯.৩৫</p>

<p>ইউরা           ৪২.৩৯                                 
৪২.৮৬</p>

<p> </p>

<p> </p>

<p>আজকর
দিনটি কেমন যাব 
বুধবার
১২ ডিসম্বর, ২০০১, ২৬ অগ্রহায়ণ, ১৪০৮</p>

<p>মেষ                    পীড়া।</p>

<p>বৃষ                      অনুকূল।</p>

<p>মিথুন                 কষ্ট।</p>

<p>কর্কট                 শুভাশুভ।</p>

<p>সিংহ                  অসুখ।</p>

<p>কন্যা                  পরিবর্তন।</p>

<p>তুলা                   ভীতি।</p>

<p>বৃশ্চিক               আশাপ্রদ।</p>

<p>ধনু                      প্রতিকূল।</p>

<p>মকর                  শুভাশুভ।</p>

<p>কুম্ভ                    স্বল্প প্রাপ্তি।</p>

<p>মীন                    সন্তাপ।</p>

<p> </p>

<p> </p>

<p>কারগিল
শহিদদর কফিন ক্রয় : মুলতবি সংসদ</p>

<p>কারগিল শহিদদর
কফিন কনা সংক্রান্ত আর্থিক অনিয়মর অভিযাগ মঙ্গলবার দুই দফায় লাকসভার অধিবশন
মুলতবি কর দিত হয়। একটি সংবাদপত্র প্রকাশিত রিপার্টক ইসু্য কর সভার শুরুতই বিরাধীরা
হট্টগাল শুরু করাত স্পিকার বালাযাগী দুপুর দুটা অবধি সভা মুলতবি কর দন। সভা
মুলতবি ঘাষিত হলও বিরাধীরা সভাকক্ষর ওয়ল নম স্লাগান দিত থাকন ও কাগজপত্র
ছিঁড়ত থাকন। সংসদ কক্ষর বাইর তাঁরা ধরনাতও বসন। </p>

<p>এদিনই সভায় বিতর্কিত
পাটা অধ্যাদশটি পশ করার কথা ছিল। সজন্য স্পিকার এক সর্বদল বৈঠকও ডাকন। সখান কংগ্রস,
বামপন্হী ও সমাজবাদী পার্টির সাংসদরা এর প্রতিবাদ জানান। </p>

<p>বলা
দুটায় অধিবশন ফর কফিন-ইসু্যত গণ্ডগাল চলত থাক। কানওভাবই বিরাধীদর শান্ত করত
না পর স্পিকার অধিবশন মুলতবি ঘাষণা কর দন। ফল, এদিন সন্ত্রাসবাদ বিরাধী অধ্যাদশ
পাটা পশ করা সম্ভব হয়নি। </p>

<p> </p>

<p>ভারতক ফের
আলাচনার প্রস্তাব মুশারফর</p>

<p>কাশ্মীর বিতর্ক মটাত
নিরাপত্তা পরিষদর প্রস্তাবর আওতায় ভারতর সঙ্গ 'দীর্ঘময়াদী, অর্থবহ ও ফলদায়ী' আলাচনার
প্রস্তাব দিয়ছন পাকিস্তানর প্রসিডন্ট পরভজ মুশারফ। মঙ্গলবার মুশারফ বলন, ভারত ও
পাকিস্তান দুই দশই রাষ্ট্রসংঘর গৃহীত প্রস্তাব অনুসার কাশ্মীর সমস্যা সমাধান দায়বদ্ধ।
মুশারফর প্রস্তাব য নয়াদিল্লি মন নব না, তা বলাই বাহুল্য। কারণ, ভারত কাশ্মীরক
দ্বিপাক্ষিক সমস্যা বলই মন কর। তৃতীয় কানও পক্ষর হস্তক্ষপ মানত দিল্লি রাজি নয়। </p>

<p>তাছাড়া, এদিনই
জাপান থক দশ ফরার আগ বাজপয়ী টাকিওত সাংবাদিকদর বলন, এখনই পাকিস্তানর সঙ্গ
দ্বিপাক্ষিক আলাচনা শুরুর পরিবশ নই। গত দুই মাস একাধিকবার মুশারফ আলাচনার প্রস্তাব
দিলও বাজপয়ী তাত কান দননি। রাষ্ট্রসংঘর অধিবশন যাগ দিত গিয়ও মুশারফর সঙ্গ
বৈঠক সতর্কতার সঙ্গ এড়িয় গছন অটল। আপাতত ঠিক আছ, জানুয়ারি মাস সার্ক শীর্ষ বৈঠক
যাগ দিত কাঠমাণ্ডু গল সখান দুই রাষ্ট্রপ্রধানর একান্ত বৈঠক হত পার। </p>

<p> </p>

<p>কাশ্মীরর
ভোট নিয় পাকিস্তানক অটলর হুঁশিয়ারি</p>

<p>জম্মু কাশ্মীরর
বিধানসভা ভাট গণ্ডগাল পাকানার চষ্টা করল ভাল হব না বল মঙ্গলবার পাকিস্তানক ঠারঠার
সতর্ক কর দিয়ছন প্রধানমন্ত্রী অটলবিহারী বাজপয়ী। পাঁচদিনর জাপান সফর শষ কর দশ ফরার
প্রাক্কাল বাজপয়ী বলন, কাশ্মীর অবাধ নির্বাচন বাধা দওয়া পাকিস্তানর উচিত হব না। সখানকার
মানুষক নির্ভয় ভাট দওয়ার সুযাগ দিত হব। </p>

<p>জাপানি
প্রধানমন্ত্রী কৈজুমির অনুরাধ সত্ত্বও এখনই পাকিস্তানর সঙ্গ আলাচনা শুরু করা সম্ভব নয় বল
স্পষ্ট জানিয় দন প্রধানমন্ত্রী। বাজপয়ীর কথায়: ভারতর সব মিত্র দশগুলিই চায়, অবিলম্ব
পাকিস্তানর সঙ্গ আলাচনা শুরু করুক দিল্লি। কিন্তু তাদর বুঝত হব, পরিস্হিতি এখনও
অনুকূল নয়। কাশ্মীরর নির্বাচন ইসলামাবাদ কী ভূমিকা নয় তা থকই বাঝা যাব ওদর মনাভাব
পরিবর্তন হয়ছ কিনা। কার্যত হুমকির সুরই বাজপয়ী বলন, কাশ্মীর অবাধ ও শান্তিত ভাট
করাটা ভারতর চ্যালঞ্জ। পাকিস্তান তাত বাধা দিত না চাইলই ভাল করব। </p>

<p> </p>

<p>শিলিগুড়িত
মত্ত হাতির হামলা, নিহত ২</p>

<p>সবুজ অরণ্যানির
আয়তন কমত থাকায় এবং খাদ্যদ্রব্যর অভাব হাতির পাল দু'একদিন অন্তরই হামলা চালাচ্ছ গ্রাম-লাকালয়।
সামবার শিলিগুড়ির কাছ কয়কটি গ্রাম ৮০-৯০টি হাতির এক বিরাট দল তাণ্ডব চালায়। ক্ষতর
শস্যাদি খয়, নষ্ট করও শান্ত হয়নি। বাড়িঘর, আসবাবপত্র, দৈনন্দিন গৃহস্হালির জিনিসপত্রও
ভাঙচুর কর ভার সাড় তিনটর সময় চল যায়। মত্ত হাতিরা মর ফল দুজন গ্রামবাসীকও।
সম্পত্তি ক্ষতির পরিমাণও অনক টাকার বল জানায় গ্রামবাসীরা। </p>

<p>নপালর
মাওবাদীদর সন্ত্রাসবাদী কার্যকলাপ সম্প্রতি ইন্দা-নপাল সীমান্ত 'সিল' কর দওয়া হয়ছ।
একদিক অবাধ বিচরণ বাধা, অন্যদিক খাদ্যদ্রব্যর অভাব। তাই, হাতিরা গিয় ঢুকছ গ্রামর
শস্যক্ষত, লাকালয়। শিলিগুড়ির ডিভিশনাল ফরস্ট অফিসার পিয়ার চাঁদ জানান, 'হাতির
পাল মাঝমধ্য গ্রাম ঢাক। কিন্তু, এত বশি সংখ্যায় কানওদিন আসনি। সংরক্ষিত বনাঞ্চল
কমত থাকায় তারা যখান কানওদিন যত না, সখানও হানা দিচ্ছ। গ্রামর অধিবাসীদর
কাছ এই ক্ষুধার্ত হাতির দলর হামলা এখন প্রতি রাতর আতঙ্ক। </p>

<p> </p>

<p>চলচ্চিত্রর
নক্ষত্র জগত ধ্রুবতারা দাদামণি</p>

<p>'জীবন নাইয়া' বয়
মুম্বইয়র টিনসল টাউন প্রবশ করছিলন য ২৫ বছরর যুবক তিনিই পরবর্তীকাল জনপ্রিয়
এবং শ্রদ্ধয় 'দাদামণি' হয় দীর্ঘ ৬০ বছর বিরাজ করছন রূপালি পর্দার নক্ষত্রমণ্ডলীত।
২০০১-এর ১০ ডিসম্বর নক্ষত্রপতন ঘটলও সিনমা জগত্ তাঁক ভুলব না কানওদিন। অথচ কী
আশ্চর্য, তত্কালীন বলিউডর স্ক্রিন টস্ট নায়ক হত চাওয়া আইনর সই ছাত্র কুমুদলাল
গাঙ্গুলি প্রথমই বাতিল হয়ই ফির এসছিলন। জার্মান পরিচালক ফ্রান অস্টন নবাগত নায়কর
বড় চায়াল নিয় বিদ্র©প কর তাঁর নিকট-ভবিষ্যত ব্যর্থতা নিয় ভবিষ্যত্বাণী করছিলন। সব
কিছু মিথ্য কর দিয় ২৫০টি ছবিত অভিনয় কর কুমুদলাল হয় উঠছিলন এক এবং অদ্বিতীয় অশাককুমার।
চিরস্মরণীয় হয়ই রয় যাবন নক্ষত্রজগতর ধ্রুবতারা হিসব। </p>

<p>কুঞ্জলাল
গাঙ্গুলি এবং গৗরীদবীর কাল ১৯১১-য় বিহারর ভাগলপুরর রাজবাটিত তাঁর জন্ম। কিন্তু
বড় হয় ওঠা মধ্যপ্রদশর খান্ডায়াতই। তব ভাগলপুরর দুটি স্কুল দুর্গাচরণ এবং সি এম এস
হাইস্কুল তিনি পড়াশানা করছন। বাবা ছিলন প্রখ্যাত আইনজীবী। সই পথই এগিয়ছিলন
কুমুদলাল। আসন কলকাতায় আইন পড়ত। কিন্তু পরীক্ষার ফি ৩৫ টাকায় কিন ফলন মুম্বই
যাওয়ার টিকিট এবং পৗঁছ যান ভগ্নিপতি শশধর মুখাপাধ্যায়র ফিল্মালয় স্টুডিওয়। ল্যাবরটরি
অ্যাসিস্ট্যান্ট হয়ই তাঁর ফিল্মি দুনিয়ায় প্রবশ। তারপর বম্ব টকিজ-এ ২৫০ টাকা মাস মাইনত
ক্যামরা-অ্যাসিস্ট্যান্ট-এর কাজ পান। কিন্তু 'জীবননাইয়া'র নায়ক নাজমল হুসন হঠাত্
অসুস্হ হয় পড়ায় বাম্ব টকিজ-এর কর্ণধার হিমাংশু রায় ডাক দন তাঁক। বিপরীত ছিলন
তখনকার 'টপ হিরাইন' দবিকা রানী। তারপর দবিকা রানীর সঙ্গ আরও একটি ছবি
'অচ্ছুত্ কন্যা' তাঁর গ্রহণযাগ্যতা বাড়িয় তাল। তারপর থকই তিনি স্টার অশাককুমার।
সঙ্গীতও পারদর্শী ছিলন দাদামণি। দবিকা রানীর সঙ্গই অশাককুমার ডুয়ট গান
'ম্যায় বনক চিড়িয়া' বিখ্যাত হয় ওঠ। বশ ক'টি ছবিতও কণ্ঠ দন তিনি। এরপর আবার
হিট হয় তিনটি ছবি। 'কঙ্গন' (১৯৩৯), 'বন্ধন' (১৯৪০), 'ঝুলা' (১৯৪১)।
তারপর শুধুই এগিয় চলা। ছবির অ্যান্টিলিডর নায়ক হয়ও 'কিসমত' (১৯৪৩) সুপারহিট।
'মহল', 'বন্দিনী'ও মনকাড়া ছবি। 'জুয়ল থিফ' (১৯৬৭)-এর ভিলন,
'মিলি'(১৯৭৫)-র স্নহার্দ্র পিতা, 'খুবসুরত'-এর পরিবারর কর্তা- সব চরিত্রই
তিনি সমান দক্ষতার পরিচয় দিয়ছন। ১৯৮৮-ত পান দাদাসাহব ফালক পুরস্কার। ৯০-এর
মাঝামাঝি 'দ্য রিটার্ন অফ জুয়ল থিফ'ই তাঁর শষ বড়পর্দার ছবি। এর পর 'গ্র্যান্ড
ওল্ড ম্যান' 'বাহাদুর জাফর শাহ' সিরিয়াল অভিনয় করছন। প্রচণ্ড ধূমপায়ী অশাককুমার
হাঁপানি রাগ '৯৮ থকই শয্যাশায়ী ছিলন। মাত্র তিনটি বাংলা ছবি ('চন্দ্রশখর',
'হসপিটাল', 'হাটবাজার') করলও বাঙালি দর্শকদরও চাখর মণি হয়ই ছিলন
দাদামণি অশাককুমার। </p>

<p> </p>

<p>অটলর
ভুঁড়ি</p>

<p>জাপানর
প্রধানমন্ত্রীর দওয়া নৈশভাজ তাঁরই জন্য তৈরি বিশষ জ্যাকট পরত পারননি বাজপয়ী।
ভুঁড়ি বড়ছ। লজ্জিত প্রধানমন্ত্রী সাংবাদিকদর সামন বলন, 'আমারই দাষ। ওজন বড়ছ।
কথা দিচ্ছি, এবার কমিয় ফলব।' মুশকিল হচ্ছ, সমস্যাটা বাজপয়ীর ভুঁড়ির ওজন নয়,
মন্ত্রিসভার ওজন নিয়। শানা যাচ্ছ, আরও একটু ওজন বাড়াত চান বাজপয়ী। ডিসম্বরর শষ
মন্ত্রিসভার আবার সম্প্রসারণ করত চান তিনি। মমতা ব্যানার্জিক নবন, নবন তামিলনাড়ুর
পি এম কে দলকও। নিত চাইছন চন্দ্রবাবু নাইডুর তলুগু দশমক। চন্দ্রবাবু হয়ত রাজি হবন
না। কিন্তু ওজন বাড়াত মরিয়া বাজপয়ী নবন ওমপ্রকাশ চৗটালার হরিয়ানা লাকদলক।
উত্তরপ্রদশর নির্বাচন বিজপি হারল সরকার নড়বড় হয় যত পার, এমন আশঙ্কা থকই দল
ভারি করার ব্যবস্হা। যদিন শপথ নিলন সদিন থক আজ পর্যন্ত বাজপয়ী শুধু ওজন বাড়াতই
ব্যস্ত। যাক পারছন মন্ত্রী কর দিচ্ছন। জয়ললিতা ছড় গল করুণানিধির সঙ্গ বন্ধুত্ব করছন।
আবার করুণানিধি দুর্বল হয় পড়ল জয়ললিতার দিক বন্ধুত্বর হাত বাড়াচ্ছন। এইরকম একটা
পাঁচমিশলি সরকারর ওজনই বড়ছ শুধু। আর কাজ না করল ওজন তা বড়ই যায়। বাজপয়ীর
সরকার কাজ আর করল কই। কত মন্ত্রী আছন যাঁরা জাননই না তাঁদর কাজ কী। কাজ-হীন
মন্ত্রীরা একবার জাটও বঁধছিলন। তব অবশ্য কাজর কাজ কিছুই হয়নি। সুতরাং বাজপয়ীর
ওজন কমাবার প্রতিশ্রুতিত আমরা আদৗ আশ্বস্ত নই। সত্যিকারর কাজ কর দখাত হব তাঁক।
তবই ওজন কমব। তাঁর মন্ত্রিসভার ওজন দশবাসী হাঁসফাঁস করছ। মন্ত্রী ছঁট ওজন কমাত
রাজনৈতিকভাব তিনি অপারগ হতই পারন, কিন্তু এমন মন্ত্রী রাখুন যাঁরা কাজ করবন। তবই
ওজন কমব। লজ্জিত হত হব না প্রধানমন্ত্রীক। প্রধানমন্ত্রীর আসল ওজন তাঁর ভুঁড়িত নয়,
তাঁর ব্যক্তিত্ব। কাজ করত না পারল ইতিহাস অটলক ব্যর্থ প্রধানমন্ত্রীর তালিকাতই তুল
রাখব। </p>

<p> </p>

<p>পঞ্চায়ত
উপনির্বাচন বিপুল জয় বামফ্রন্টর</p>

<p>রাজ্য ত্রিস্তর পঞ্চায়তর
উপনির্বাচন শাসক বামফ্রন্ট দুই বিরাধী দল তৃণমূল কংগ্রস ও কংগ্রসর চয় অনক বশি
আসন পয়ছ। ফলাফলর প্রবণতা হিসব করল দখা যায়- উত্তর ও দক্ষিণবঙ্গর জলাগুলির মধ্য
একমাত্র মালদহ ও মুর্শিদাবাদই বামফ্রন্টর সঙ্গ বিরাধীদর তুল্যমূল্য লড়াই হয়ছ। এবং
লক্ষণীয় য, দুই জলাতই তৃণমূলক অনক পিছন ফল দিয়ছ কংগ্রস। এই দুই জলাতই কংগ্রসর
সংগঠন ভাল। কিন্তু দক্ষিণবঙ্গর জলাগুলিত প্রভাব অপ্রতিহত রয়ছ ফ্রন্টর। শুধু তাই নয়, এখান
বহু ক্ষত্র তারা আসনও বাড়িয় নিয়ছ। </p>

<p>কানও জলাতই নজর কাড়ত
পারনি বিজপি। অনুপ্রবশ ইসু্য কাজ লাগিয় সীমান্ত জলাগুলিত বিজপি ভাল ফল করত পার
বল বিশষজ্ঞ মহল আশা করছিলন। বাস্তব তা হয়নি। প্রায় সব জায়গাতই বিজপির স্হান
তৃতীয় বা চতুর্থ। তৃণমূল কংগ্রস নির্বাচন অনিয়মর অভিযাগ করছ। তৃণমূল নতারা বলছন-
বিধানসভার উপনির্বাচনর মতা এখানও ব্যাপক রিগিং, ছাপ্পাভাট হয়ছ। তাই এই ফলাফলর কানও
মূল্য নই। </p>

<p> </p>

<p>পার্কাম্যাট
: সুব্রত-প্রদীপর তুমুল ঝগড়ায় জটিলতা</p>

<p>তৃণমূলর দলীয় কান্দল
৯ কাটি টাকায় তৈরি রডন স্ট্রিটর বহুতল পার্কাম্যাটর ভবিষ্যত্ অথৈ জল। ২৫ নভম্বর উদ্বাধন
হয়ছ। ২১৯টি গাড়ি পার্কিংয়র ব্যবস্হা আছ। ১৫ দিন মাত্র ১টি গাড়ি রাখা হয়ছ। মাথায়
হাত পড়ছ নির্মাণকারী সিমপ্লক্স প্রজক্ট লিমিটডর। ধর্মতলায় দ্বিতীয় পার্কাম্যাট তৈরিত
আর কানও সংস্হা আগ্রহ দখাব কিনা সন্দহ। ময়র সুব্রত মুখার্জি অবশ্য আরও ৬-৭টি পার্কাম্যাট
তৈরি করত উঠপড় লগছন। </p>

<p>পার্কাম্যাট
নিয় বআইনি লনদনর অভিযাগ তুলছিল বামফ্রন্ট। সমর্থন করছিলন ময়র পারিষদ প্রদীপ ঘাষ।
তদন্তও চয়ছিলন। প্রকল্প চূড়ান্ত হওয়ার পর পুরসভার একাধিক অফিসারর বিদশযাত্রা নিয়
প্রশ্নও তুলছিলন। ময়র সুব্রত মুখার্জি এসব কান তুলত চাননি। পার্কাম্যাটর চারপাশ
৫০০ বর্গমিটার এলাকায় যসব গাড়ি রাখা হয় সসব নতুন বহুতল রাখার নিয়ম হয়। ঘণ্টায় ১০
টাকা ভাড়া ঠিক হয়। কিন্তু ওই এলাকাতই প্রদীপ ঘাষর পার্কিং দপ্তর কয়কটি সমবায়ক লাইসন্স
দিয় রখছ। প্রদীপবাবু ওই সমবায়র কর্মীদর রুটি-রুজি নিয় প্রশ্ন তুলছন। তাঁর
বক্তব্য, পার্কাম্যাটর জন্য বছর ১৩ লক্ষ টাকা ক্ষতি হব পুরসভার। আশপাশর ৮টি রাস্তায়
'না পার্কিং' কর পরাক্ষ তালা আদায়র সুযাগ কর দওয়া হল কিছু পুলিসকর্মী ও
স্হানীয় মস্তানদর। ময়র অবশ্য জানিয় দিয়ছন, '২০ বছর পর শহর গাড়ির সংখ্যা হব
৩০ লক্ষ। রাস্তা মাত্র ৬ শতাংশ। সুতরাং পার্কাম্যাট করতই হব। নিউ মার্কটক বাঁচাতই পার্কাম্যাট
দরকার। গাড়ি রাখার জায়গা না থাকল কউ আর নিউ মার্কট আসবন না। </p>

<p> </p>

<p>দাদামণি</p>

<p>ভারতীয়
চলচ্চিত্রর প্রবাদপুরুষ অশাককুমারর প্রয়াণ শাকস্তব্ধ শিল্পীরা। তাঁদরই কিছু
স্মৃতিচারণ। </p>

<p>যতবার দখা হয়ছ,
বলছি একশা বছর বাঁচত হব। তব বঁচ না থাকলও তাঁর অভিনয় বহু বছর বঁচ থাকব।
দাদামণিক বাদ দিয় ভারতীয় চলচ্চিত্রর ইতিহাস লখা যাব না। </p>

<p>সুনীল দত্ত </p>

<p>
স্ত্রীর সূত্র ওঁর সঙ্গ
ব্যক্তিগত একটা আত্মীয়তা ছিল। খুব আকস্মিকভাবই এস পড়ছিলন অভিনয়জগত। টকনিশিয়ান
হিসব শুরু কর কীভাব এত বড় অভিনতা হয় উঠলন সটাই বিস্ময়কর। </p>

<p> </p>

<p>সৌমিত্র চট্টাপাধ্যায় </p>

<p>
বম্ব টকিজ দাদামণির সঙ্গ
সাত-আটটি ছবি করছি। শু্যটিংয়র দুদিন আগ চিত্রনাট্য তাঁর দৃশ্যটি চয় পাঠাতন। ফ্লার
আসতন তৈরি হয়। 'আনন্দ আশ্রম' ছবিত উত্তমকুমারক অভিনয় কর দখিয় তাঁর
মতামত চয়ছিলন। এতটাই ছিল শখার আগ্রহ। </p>

<p> </p>

<p>শক্তি সামন্ত </p>

<p>
হোমিওপ্যাথি চিকিত্সা
নিয় মত থাকতন। ওষুধ দিতন। অসম্ভব আড্ডাবাজ মানুষ ছিলন। ওঁর অভিনয় আমাক মুগ্ধ
করত। তব আমার একটা ছবিতও ওঁক নিয় কাজ করত পারিনি। ভুবন সাম চরিত্রর জন্য ওঁক
ভবছিলাম। বিমল করর উপন্যাস 'ভুবনমাহিনী'-র প্রধান চরিত্রও ওঁক ভবছিলাম।
সাধ অপূর্ণ থক গল। </p>

<p> </p>

<p>মৃণাল সেন </p>

<p>
স্বাভাবিক অভিনয় করতন।
সময়র সঙ্গ বদলাতন নিজক। দিলীপকুমারর সঙ্গও যমন স্বাভাবিক, একালর শাহরুখর সঙ্গও তমনি।
বাড়িত একটা ছাটখাট রকর্ডিং ইউনিট তৈরি করছিলন। আমার দখা সরা সাউণ্ড ইঞ্জিনিয়ার।
খুব রসিক মানুষ ছিলন। হিন্দি, ইংরজি ছাড়াও উর্দু, জার্মান ভাষা জানতন। </p>

<p> </p>

<p>তপন সিংহ </p>

<p>
বাবার চল যাওয়ার পর
সব কাজই ওঁর পরামর্শ নিয়ছি। বন্ধুর মত মিশতন। য কানও কথা নিঃসঙ্কাচ বলত পারতাম।
</p>

<p> </p>

<p>অমিতকুমার </p>

<p>
'হাটবাজার'
ছবির আউটডার সন্ধ্যার আড্ডায় দাদামণি আর ছায়াদির দহাতি গান ছিল অনন্য এক অভিজ্ঞতা।
নানারকমর শখ ছিল। তলরঙ ছবি আঁকতন। শুভন্দুর কাছ এখনও আছ। </p>

<p> </p>

<p>চিন্ময়
রায়</p>

<p> </p>

<p>রাজ্য সরকার
স্কুলর ছুটি কমাত উদ্যাগী</p>

<p>সারা রাজ্য জুড়
বিদ্যালয়গুলিত পঠনপাঠনর জন্য এখন বরাদ্দ ২০০ দিনর মত। সাধারণত ৮০ দিন বিদ্যালয় বন্ধই থাক।
এছাড়া বছর ৫২টি রবিবার তা রয়ইছ। অর্থাত্ কমবশি ১৩২ দিন স্কুল পড়াশুনা হয় না।
রাজ্য সরকারি কর্মচারীদর ছুটি কমানার পর বিদ্যালয়ও ছুটি কমানার কথাও ভাবছ সরকার।
</p>

<p>রাজ্যর বিদ্যালয়
ছুটির সংখ্যা কমানা নিয় শিক্ষাদপ্তরর ভাবনার কথা বিধানসভায় জানিয়ছন বিদ্যালয়
শিক্ষামন্ত্রী কান্তি বিশ্বাস। বিদ্যালয় ছুটি কমানার বিষয় তাঁক সমর্থন করছ অল বঙ্গল
টিচার্স অ্যাসাসিয়শন (এ বি টি এ) এবং প্রধান শিক্ষক সমিতি। বর্তমান চালু প্রাথমিক এবং
মাধ্যমিকর সিলবাস পর্যালাচনার জন্য রঞ্জুগাপাল মুখাপাধ্যায়র নতৃত্ব গঠিত কমিশনর
কাছ এই ব্যাপার মতামত চাওয়া ছাড়াও সব শিক্ষক সংগঠন এবং শিক্ষার সঙ্গ যুক্ত ব্যক্তিদর সঙ্গ
সরকার আলাচনায় বসব। </p>

<p>গত
২৬ নভম্বর মধ্যশিক্ষা পর্ষদ এবং উচ্চমাধ্যমিক শিক্ষা সংসদর সভাপতি ও বিদ্যালয় শিক্ষামন্ত্রীর কাছ
স্মারকলিপি পশ করছ প্রধান শিক্ষক সমিতি এবং এ বি টি এ। এ বি টি এ-র পক্ষ প্রশান্ত ধর বলন,
এখন কানওভাবই ১৭৫ দিনর বশি বিদ্যালয় পড়াশুনার সুযাগ নই।
কৃষ্ণনগর-শিলিগুড়ি-কালনা এলাকায় রাজনৈতিক দলর ধর্মঘট ঘাষণায় আরও কিছু শিক্ষাদিবস নষ্ট
হয়। </p>

<p> </p>

<p>মামলার ভার
কমাত রাজ্য বহু ফাস্ট ট্র্যাক কোর্ট</p>

<p>রাজ্যর বিভিন্ন আদালত
জম থাকা মামলার ভার কমাত ১৫২টি ফাস্ট ট্র্যাক কার্ট গড় তালা হব। একাদশ অর্থ কমিশনর
সুপারিশ মন পাঁচ বছরর জন্য অস্হায়ীভাব এগুলি তৈরির কাজও শুরু হয় গছ। রাজ্যর
আইনমন্ত্রী নিশীথ অধিকারী একথা জানিয়ছন। প্রতিটি জলাতই একাধিক ফাস্ট ট্র্যাক কার্ট
গঠনর পরিকল্পনা রয়ছ রাজ্যর। এই ধরনর সাতটি আদালত ইতামধ্যই কাজ শুরু কর দিয়ছ।
আরও ছয়টি অর্থমন্ত্রকর অনুমাদনর অপক্ষায়। </p>

<p>কেন্দ্রীয়
সরকার এই আদালতগুলিত অবসরপ্রাপ্ত বিচারক এবং আদালত-কর্মীদর নিয়াগর পক্ষপাতী হলও
রাজ্য চায় স্হানীয় যুবকদর অস্হায়ী ভিত্তিত এগুলিত কাজ লাগাত। এই ব্যাপার নিশীথবাবুর
যুক্তি: অবসরপ্রাপ্ত সরকারি কর্মীদর নিয়ন্ত্রণ করা কঠিন। কিন্তু স্হানীয় যুবকদর নিয়াগ করল
এই সমস্যা নই। তাদর দায়বদ্ধতাও অনক বশি হব। তব বিচারকদর ক্ষত্র জলা আদালতর
বিচারপতিদর অস্হায়ী ভিত্তিত নিয়াগর কথা ভাবছ রাজ্য। </p>

<p> </p>

<p>কুম্বলর ৫
উইকট, বেকায়দায় ইংল্যান্ড</p>

<p>দ্বিতীয়
টস্টও কুম্বলক সামলাত ব্যর্থ ইংল্যান্ড। আমদাবাদ প্রথম দিনর চা বিরতির এক ঘণ্টার মধ্যই
পাঁচটি উইকট তুল নিলন কুম্বল। তার মধ্য অধিনায়ক নাসর হুসন ও মাইকল ভনর উইকট
অবশ্য জিম্বাবায়র আম্পায়ার ইয়ান রবিনসনর দাক্ষিণ্য পাওয়া। তা সত্ত্বও কুম্বলর কৃতিত্বক
খাটা কর দখার উপায় নই। মার্কাস ট্রসকাথিক (৯৯) ছাড়া কউই তাঁক সহজ খলত পারননি।
সঞ্চুরি বাঁধাই ছিল ট্রসকাথিকর। চা বিরতির পর প্রথম ওভার চারটি বল কা্ঙিখত রানটি
না পয় পঞ্চম বলটি ঝুঁকি নিয় স্টিয়ার করত গিয়ছিলন কাঁধ সমান উচ্চতায় চমত্কার ক্যাচ
ধরন দীপ দাশগুপ্ত। ষষ্ঠ উইকট চালিয় খল ৬০ রান যাগ করন রামপ্রকাশ ও ক্রগ হায়াইট।
রামপ্রকাশক বাল্ড কর জুটি ভাঙন শচীন। </p>

<p> </p>

<p>ভারতর
ফুটবল উন্নয়ন ফিফা দিচ্ছ সওয়া তিন কোটি</p>

<p>ভারত ফুটবলর
পরিকাঠামা আছ, কিন্তু তার ঠিকমতা রক্ষণাবক্ষণ হয় না। ফিফা তাই ভারতর ফুটবল উন্নয়ন
প্রতি বছর আড়াই লক্ষ ডলার (১ কাটি ২৫ লক্ষ টাকা) দব। ভারতর ফুটবল ফডারশনর নিজস্ব কানও
বাড়ি নই। ওই বাড়ির জন্য ফিফা ৪ লক্ষ ডলার (২ কাটি টাকা) দব। </p>

<p>অল ইন্ডিয়া ফুটবল ফডারশন
নয়াদিল্লির দ্বারকায় ওই 'ফুটবল ভবন' বানাব। ওটাই হব ভারতীয় ফুটবলর সদর দপ্তর।
এশিয়ান ফুটবল কনফডারশনর সহ-সভাপতি ডি মণিলাল এই খবর দিয় জানান, ফিফার গাল
প্রকল্প থক পুরা টাকাটাই দওয়া হব। </p>

<p>ভারত-সহ মাট ১১০টি
দশক গাল প্রকল্পর অন্তর্ভুক্ত করা হয়ছ। এ আই এফ এফ সচিব আলবার্তা কালাসা বলছন,
দিল্লিত দড় বিঘা জমিত 'ফুটবল ভবন' নির্মাণ করা হব। বাড়িটি হব সমবায় ভিত্তিত।
এ আই এফ এফ সভাপতি প্রিয়রঞ্জন দাশমুন্সী জানিয়ছন, ফিফার কাছ থক সরকারিভাব চিঠি এন
৩০ দিনর মধ্যই তাঁরা কাজ শুরু করবন। তিনি বলন, আমরা ফিফার কাছ নির্মাণ সংস্হার
নাম পাঠাবা। ফিফা সরাসরি তাদরই টাকা পয়সা দব। </p>

<p>ফিফা
য টাকা দব তাত ফুটবলারদর প্রশিক্ষণ কন্দ্র ও হস্টল গড়া হব বাঙ্গালার কর্নাটক ফুটবল
অ্যাসাসিয়শনর সহযাগিতায়। রাজ্য সরকাররও সাহায্য নওয়া হব। বাঙ্গালার স্টডিয়ামটিও
কাজ লাগানা হব, জানালন প্রিয়রঞ্জন। এর মালিক কর্নাটক ফুটবল অ্যাসাসিয়শন। </p>

<p> </p>

<p>বিশ্বদাবার
দ্বিতীয় গেমও আনন্দর ড্র</p>

<p>খুব আশা ছিল মস্কায়
বিশ্বদাবার সমিফাইনালর দ্বিতীয় গম সাদা ঘুঁটি নিয় জিতবন ভারতর প্রথম
গ্র্যান্ডমাস্টার বিশ্বনাথন আনন্দ। কিন্তু পারননি। সামবার ক্রমলিন হল ইভানচুকর সঙ্গ ১৮
চালর মাথায় উভয় ড্র-ত সম্মত হন। ভ্যাসিলি ইভানচুকর সিসিলিয়ান ডিফন্সর সামন
অনিন্দক অসহায় মন হয়ছ। </p>

<p>সমিফাইনাল
চার গম খলার পর যদি ২-২ হয়, তা হল টাইব্রকার নিষ্পত্তি হব। সমিফাইনাল আনন্দর
দুটি গম বাকি। পরর গম কালা ঘুঁটি, তিনি ড্র-র চষ্টাই করবন। তারপর বৃহস্পতিবার
সাদা ঘুঁটি নিয় জিতল ফাইনাল উঠবন। তব ইভানচুক পরর গম জতার জন্যই মরীয়া হবন
সাদা ঘুঁটি নিয়। কারণ বৃহস্পতিবারর জন্য ঝুঁকি নবন না। উপরন্ত্ত তিনি টাইব্রকারও যত
চান না। কারণ র্যাপিড গমস আনন্দ দুর্দান্ত খলন। </p>

<p> </p>

<p>ইংল্যান্ড ক্রিকট
বোর্ডর সভায় প্রধান আলাচনা ভারতক নিয়</p>

<p>আগামী বছর ইংল্যান্ড
ভারতীয় দলর সফর নিয় ইংল্যান্ড অ্যান্ড ওয়লস ক্রিকট বার্ড (ই সি বি) সামবার লন্ডন দুই দশর
মধ্য উদ্ভুত পরিস্হিতি নিয় দীর্ঘ আলাচনা করছন। ই সি বি-র কর্মাধ্যক্ষ টিম ল্যাম্ব সম্প্রতি
ভারত এস বার্ড প্রসিডন্ট জগমাহন ডালমিয়া ও সচিব নিরঞ্জন শাহ-র সঙ্গ দীর্ঘ আলাচনা
করন। ভারত ইংল্যান্ডক অতিরিক্ত একটি একদিনর ম্যাচ খলত বল। ভারত আরও একটি প্রস্তাব দয়
ই সি বি-ক। ভারত ২০০২-এ ইংল্যান্ড সফর চারটি টস্ট খলত রাজি। কিন্তু শর্ত হল ইংল্যান্ড
যখন ২০০৪-০৫এ ভারত সফর করব, তখন ছ'টি টস্ট ম্যাচ খলত হব। </p>

<p>জিম্বাবায় ভারত আসছ</p>

<p>জিম্বাবায় ফব্রুয়ারি-মার্চ
ভারত সফর এস দুটি টস্ট ও পাঁচটি একদিনর আন্তর্জাতিক ম্যাচ খলব। প্রথম টস্ট নাগপুর,
দ্বিতীয় টস্ট দিল্লিত। পূর্বাঞ্চলক একটি একদিনর আন্তর্জাতিক দওয়া হয়ছ। ওটি হব
গুয়াহাটিত। </p>

<p>জিম্বাবায় দল ১২ ফব্রুয়ারি
মুম্বই পৗঁছব। ১৫-১৭ ফব্রুয়ারি বিজয়ওয়াড়ায় খলা বার্ড প্রসিডন্ট একাদশর বিরুদ্ধ।
২১-২৫ ফব্রুয়ারি নাগপুর প্রথম টস্ট। ২৮ ফব্রুয়ারি-৪ মার্চ দিল্লিত দ্বিতীয় টস্ট। ৭
মার্চ প্রথম একদিনর আন্তর্জাতিক ফরিদাবাদ। ১০ মার্চ দ্বিতীয় একদিনর ম্যাচ মাহালিত।
১৩ মার্চ তৃতীয় একদিনর ম্যাচ কাচিত। ১৪ মার্চ চতুর্থ একদিনর ম্যাচ হায়দরাবাদ। ১৯
মার্চ পঞ্চম ও শষ একদিনর ম্যাচ গুয়াহাটিত। </p>

<p> </p>






</body></text></cesDoc>