<cesDoc id="ben-w-bengalnet-news-00-10-09" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-00-10-09.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 00-10-09</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>00-10-09</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>আজ বৈঠক
১৪৮৭ কাটি চাইবন অসীম</p>

<p>বন্যার জন্য ১৪৮৭ কাটি
টাকা কন্দ্রীয় সাহায্যর দাবি নিয় আজ পশ্চিমবঙ্গর অর্থমন্ত্রী অসীম দাশগুপ্ত কন্দ্রীয় অর্থমন্ত্রী
যশাবন্ত সিনহা ও কন্দ্রীয় কৃষিমন্ত্রী নীতীশকুমারর সঙ্গ দখা করবন। রবিবার অসীমবাবু
সাংবাদিকদর বলন, বন্যার ফল ১১৮৭ জনর প্রাণহানি হয়ছ। প্রায় ৭৪ হাজার গবাদি পশু
মারা গছ। শস্যর ক্ষতি হয়ছ ৩৮৬৮ কাটি টাকার। কার্যত, বন্যা-কবলিত আট জলার
খরিফশস্য পুরাটাই নষ্ট হয় গছ। ক্ষতিগ্রস্ত হয়ছন প্রায় ২ কাটি ১০ লক্ষ মানুষ। </p>

<p>বন্যার জল অধিকাংশ
স্হানই কমত শুরু করায় জলবাহিত রাগ মহামারীর আকার ধারণ করার সম্ভাবনা রয়ছ। রাগ
প্রতিরাধ প্রচুর হ্যালাজন ট্যাবলট বিলি করা হয়ছ। ও আর এস-এর প্যাকট নিয় জলায়
জলায় ঘুর বড়াচ্ছ ৫৯টি চিকিত্সক দল। </p>

<p>বন্যা
মাকাবিলায় রাজ্য সরকার ইতিমধ্যই ১০০ কাটি টাকা খরচ করছ। এর মধ্য গৃহ নির্মাণ ও
সংস্কারর জন্যই ৪৪ কাটি টাকা ব্যয় হব। অসীমবাবু জানান, রাজ্য সরকার হাডকার কাছ থক
২৫০ কাটি টাকা ঋণ চাইব। শস্যর বিরাট ক্ষয়ক্ষতি কৃষকরা যাত অন্তত কিছুটা সামলাত পারন,
এজন্য সমস্ত ব্যাঙ্কঋণ মকুব করার সিদ্ধান্ত নওয়া হয়ছ। </p>

<p>'ঠাকুর
গছ বিসর্জন'</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap>গতকাল বিজয়া দশমীর দিন ভার থকই
আকাশ ছিল মঘলীন বিষাদ ভরা। বিসর্জনর প্রস্তুতিতই যন। আকাশর সই বিষাদবাধ
প্রতিফলিত হচ্ছিল মানুষর মুখ। ঢাকর শষ কাঠিটি পড়ত তা আর দরি নই বশি। শুভ
বিজয়ার প্রণাম, আশীর্বাদ, শুভচ্ছা জ্ঞাপনর মধ্যই এ বছরর মত মাক বিদায় জানানার
প্রস্তুতি শষ। দশমীর সকাল থকই মঘ ভরা আকাশ। হাল্কা থক মাঝারি ধরনর বৃষ্টি হয়ছ
শহরর বিভিন্ন অঞ্চল। বন্যাভীত মানুষর আশঙ্কা ছিল, আবার নিম্নচাপ নয় তা! </p>

<p>আলিপুর আবহাওয়া
দপ্তরর অধিকর্তা রাজন গালদার জানিয়ছন এ বৃষ্টি নহাতই মৗসুমী বায়ুর জন্য। দু-একদিনর
মধ্যই বর্ষা বিদায় নব গাঙ্গয় পশ্চিমবঙ্গ থক। বৃষ্টির জন্য এদিন শহরর কয়কটি জায়গায়
যান চলাচল বিঘ্নিত হয়ছ। </p>

<p>দশমীর বিকল মণ্ডপ
মণ্ডপ প্রায় একই দৃশ্য। লাল পাড় শাড়ি পর মহিলারা মতছন সিঁদুর খলায়। বিসর্জনর পর
বিজয়ার শুভচ্ছা'র সাথ সাথ শুরু হয়ছ মিষ্টি মুখর পালা। </p>

<p>বিসর্জন উপলক্ষ শহরর
ন'টি ঘাট এবছর ছিল কড়া পুলিশি নিরাপত্তা। প্রতি ঘাটই সুরক্ষার কারণ অন্তত একটি কর
লাইফবাট রাখা ছিল, চার-পাঁচ জন ডুবুরি সমত। গঙ্গাবক্ষ বিসর্জনর জন্য চিহ্ণিত জায়গাগুলিত
ছিল জল-পুলিশর সতর্ক নজরদারি। বিসর্জনর প্রথম দিনটিত ভাসান হয়ছ মূলত বাড়ির
ঠাকুরগুলি। কয়কটি বারায়ারি পুজাও অবশ্য ভাসানর আয়াজন কর এই দিন। তব,
অন্যান্যবারর মতা সাড়া জাগানা পুজাগুলির প্রতিমা মণ্ডপই রয় গছ। থাকবও আর
কিছুদিন।  এত ব্যস্ততা আর প্রস্তুতির পর পাঁচটা দিনর আনন্দও য যথষ্ট মন হয় না।
</p>

<p>দাম বাড়ানার
কথা মমতা জানতন: প্রমাদ</p>

<p>
 
 
<gap desc="illustration"></gap>তৃণমূল-কংগ্রস নত্রী মমতা বন্দ্যাপাধ্যায়
সহ এন ডি এ-এর সমস্ত শরিক দলগুলির সঙ্গ আলাচনা করই পট্রাপণ্যর দাম বৃদ্ধির সিদ্ধান্ত হয়ছিল।
কন্দ্রীয় মন্ত্রিসভার মুখপাত্র প্রমাদ মহাজন একথা জানিয়ছন। তিনি বলন, পট্রাপণ্যর দাম য
বাড়াতই হব এ ব্যাপার কউই দ্বিমত ছিলন না। তব কান পণ্যর দাম কতটা বাড়ানা হব
তা নিয় প্রথম ঐকমত্য হয়নি। কউ চাইছিলন, করাসিনর দাম বাড়াত, আবার কারুর মত
ছিল রান্নার গ্যাসর দাম বাড়ানা হাক। 'কিন্তু মূল্যবৃদ্ধি য অনিবার্য সটা উপস্হিত
সকলই মন নিয়ছিলন। মমতাও এই সর্বসম্মত সিদ্ধান্তর শরিক ছিলন।' </p>

<p>মহাজন আরও বলন, তলর
দাম বাড়ানার বিরাধিতা করলও বর্ধিত বিক্রয়মূল্যর ফল অতিরিক্ত কর আদায়র সম্ভাবনায়
সমস্ত রাজ্যই খুশি। হাঁটুর অস্ত্রাপচারর পর প্রধানমন্ত্রী সুস্হ হয় উঠলই অয়ল বণ্ড ছাড়ার
ব্যাপার চূড়ান্ত সিদ্ধান্ত নওয়া হব বল মহাজন জানান।  </p>

<p>আমরা মমতা
নই! বি জ পি-ক ঠাকর</p>

<p>
 
 
<gap desc="illustration"></gap>অটলবিহারী বাজপয়ীর নতৃত্ব
এন ডি এ সরকারক পূর্ণ সমর্থন জানিয়ও বি জ পি-ক সতর্ক হত বললন শিব সনার সর্বময়
কর্তা বাল ঠাকর। 'বি জ পি সবসময় আমাদর সমর্থন পাবই এটা যন কউ ধর না নয়'
-জানিয়ছন তিনি। দশরা উপলক্ষ শিবাজি পার্ক এক জনসমাবশ ঠাকর বলছন, দশর স্বার্থর
খাতিরই তাঁরা বাজপয়ী সরকারক সমর্থন করছন। শিবসনার লক্ষ্য কবল মাত্র 'দশ-হিত'
ও 'মহারাষ্ট্র-হিত'। 'এ ব্যাপার বি জ পি যন আমাদর ধৈর্য পরীক্ষা করত চষ্টা
না কর। আমরা মমতা বন্দ্যাপাধ্যায় নই' -বলছন শিবসনা সর্বাধিনায়ক। তাঁর মত বাজপয়ী
সরকারর ত্রুটি রাষ্ট্র বিপ্লবর কারণ হয় দাঁড়াব। শিবাজি পার্কর ভাষণ তিনি মুসলমানদর
বিশষ সুযাগ দওয়ার ব্যাপার বঙ্গারু লক্ষ্মণর তীব্র নিন্দা কর বলন, 'এইভাব
মুসলমানদর দাবি মন নওয়াটা ঠিক নয়। তাহল অন্য ধর্মর মানুষরাই বা কী দাষ করল?'
তিনি আরও বলন, বআইনিভাব এ দশ বাংলাদশি অনুপ্রবশকারীদর সংখ্যা বাড়ছ। আমার
হাত সনা চালানার ক্ষমতা থাকল আমি এদর প্রত্যকক তাড়াব। কারগিল নিহত সৈনিকদর
পরিবার যথাপযুক্ত সরকারি সাহায্য পাচ্ছ না বল কন্দ্রীয় সরকারর তীব্র সমালাচনা কর
তিনি জানান, শিব সনাই এই সব শহিদদর পরিবারর পাশ এস দাঁড়াব।  </p>

<p> </p>






</body></text></cesDoc>