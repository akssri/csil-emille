<cesDoc id="ben-w-bengalnet-news2-01-03-16" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news2-01-03-16.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-03-16</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-03-16</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>পদত্যাগ করলন
মমতা ও পাঁজা</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>
 
 
<gap desc="illustration"></gap>মমতা, পাঁজা মন্ত্রিত্ব ছেড়ছন।
কেন্দ্রিয় মন্ত্রীসভা থক পদত্যাগ করলন মমতা ব্যানার্জি ও অজিত কুমার পাঁজা। তৃণমূল এন
ডি এ থকও সর যাচ্ছ তব বাইর থক সরকারক সমর্থন করব কিনা এ প্রশ্নর পরিষ্কার
জবাব পাওয়া যায়নি। যদিও তৃণমূলর সাংসদ মাত্র ৮, তারা সর যাওয়ায় সরকার পতনর
সম্ভাবনা নই। তব ভাটর মুখ মমতা একটা কৗশলি চাল দিলন। </p>

<p>পশ্চিমবঙ্গর নির্বাচন
তিনি হয়ত বিজপিক সঙ্গ নিয় লড়বন না। এত তাঁর গায়  কলঙ্কর কালি লাগার
সম্ভাবনা কম। এদিক মমতা এন ডি এ ছড় বড়িয় যাওয়ায় কংগ্রস তার সঙ্গ আঁতাত করত চয়ছ
তব মমতা হয়ত বিজপিত ফরার রাস্তাটা খালা রাখত এখনই কংগ্রসর সাথ হাত মলাবন
না। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>অস্ট্রলিয়া চূর্ণ, ভারত জিতল ১৭১ রান</p>

<p>
 
 
<gap desc="illustration"></gap>অস্ট্রলিয়াক ১৭১ রান হারিয়
প্রায় অবিশ্বাস্যভাব ইডন টস্ট জিতল ভারত। ফলা অন কর জয়র ঘটনা টস্ট ক্রিকটর
ইতিহাস এই নিয় তৃতীয়বার। প্রথম ইনিংসর মত দ্বিতীয় ইনিংসও অস্ট্রলিয়ার ব্যাটিংক ভাঙলন
হরভজন। প্রথম ইনিংস সাত ও দ্বিতীয় ইনিংস ছয়- মাট তর উইকট নিয় ম্যান অফ দ্য ম্যাচর
লড়াইয় লক্ষ্মণক কঠিন প্রতিদ্বন্দ্বিতার মুখ ফল দিয়ছিলন ২০ বছরর এই শিখ যুবক। ম্যাচ রফারি
ক্যাথি স্মিথ অবশ্য সরার পুরস্কার দিলন লক্ষ্মণকই। ২৮১ রানর ইনিংসর চয়ও য অসাধারণ
মানসিকতা ও স্নায়বিক দৃঢ়তা লক্ষ্মণ দখিয়ছন, তারই স্বীকৃতি এই পুরস্কার। সৗরভ এক যতই
দলগত সংহতির জয় বলুন না কন, অস্ট্রলিয়ার বিজয়রথ থামাত প্রধান ভূমিকা লক্ষ্মণ, দ্রাবিড় ও
হরভজনর। চন্নাইত এর মধ্যই ঘুর্ণি পিচ করার নির্দশ চল গছ। অভাবনীয় এই জয়র পর
ভারতীয়রাই সখান ফভারিট। </p>

<p>বিশ্বসরা অস্ট্রলিয়া
য এভাব আত্মসমর্পণ করব তা ভারতর অতিবড় সমর্থকও ভাবত পারননি। চার ঘন্টার মধ্যই
গুটিয় গল স্টিভদর দ্বিতীয় ইনিংস। লড়লন একা হডন (৬৭)। বল হাত হরভজনর পাশাপাশি
মানানসই হয় ওঠন শচীনও (তিন উইকট)। দ্বিতীয় ইনিংস চার অস্ট্রলিয় ব্যাটসম্যান লগ বিফার
হয়ছন। এর মধ্য ওয়ার্ন আর ম্যাকগ্রাথর আউট নিয় প্রশ্ন উঠবই। কাকতালীয় হলও প্রতিটি
ক্ষত্রই আঙুল তুলছন বনশল।  
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>জর্জর
ইস্তফা চাইল জেডি (ইউ)</p>

<p>তৃণমূল কংগ্রসর পর
জর্জ ফার্নাণ্ডজর ইস্তফা চাইল এনডিএ-এর অন্যতম শরিক সংযুক্ত জনতা দল। নিরপক্ষ তদন্তর স্বার্থ
ফার্নাণ্ডজর ইস্তফা দওয়া উচিত বল বৃহস্পতিবার জনতা দলর লাকসভার নতা দবন্দ্রপ্রসাদ
যাদব মন্তব্য করছন। তিনি বলন, বাজপয়ীর উচিত ছিল প্রতিরক্ষা মন্ত্রীর ইস্তফা স্বীকার করা।
জর্জ ইস্তফা দিল শুধু য সরকারর ভাবমূর্তিই উজ্জ্বল হত তা নয়, নিরপক্ষ তদন্তর স্বার্থও এটা
দরকার ছিল। </p>

<p>জর্জ পদত্যাগ না করল
সংযুক্ত জনতা দল সরকারর ওপর থক সমর্থন প্রত্যাহারর হুমকি দিয়ছ। যাদব জানিয়ছন,
শুক্র বা শনিবার দলর ১০ সাংসদ কর্মপদ্ধতি স্হির করত বৈঠক বসবন। দুর্নীতির ইসু্যত কানও
আপস করা হব না বল ইতিমধ্যই সংযুক্ত জনতা দলর সভাপতি শারদ যাদব জানিয়ছন। জর্জর
ইস্তফা গ্রহণ করার আর্জি জানিয় প্রধানমন্ত্রীক চিঠিও দিচ্ছন যাদব। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>প্রতিরক্ষা কেলঙ্কারি
: কে কী পেয়ছন </p>

<p>তহলকা ডট কমর অনুসন্ধান অনুযায়ী ক
কত ঘুষ নিয়ছন তার তালিকা যথাক্রম-</p>

<p>বঙ্গারু
লক্ষ্মণ (বিজপি সভাপতি) : ১ লাখ। 
জয়া জটলি (সমতা পার্টি
সভাপতি) : ২ লাখ। 
ব্রিগডিয়ার ইকবাল সিং
: ৫০ হাজার। 
ব্রিগডিয়ার অনিল সহগল
: ৪০ হাজার। 
শশী মনন : ৫২ হাজার।

ল. কর্নল সায়াল (মধ্যস্হতাকারী) : ৮০ হাজার। 
ম. জনারল এস পি মুরগাই : ১.২০ লাখ। 
নরন্দ্র সিং (অর্থনৈতিক
উপদষ্টা, প্রতিরক্ষা মন্ত্রক) : ১০ হাজার। 
আই আই সি পন্হ
(উপসচিব, প্রতিরক্ষামন্ত্রক) : ৫০ হাজার। 
সুরন্দ্র সুলখা (পাশাক
জাগানদার, প্রতিরক্ষা) : ১ লাখ। 
এল এন মহতা (অতিরিক্ত
প্রতিরক্ষা সচিব): সানার হার। 
রাজু বঙ্কটশ : ১০
হাজার। 
রঘুপতি : ১৬ হাজার।

সুরশ : ৭ হাজার</p>

<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>
 
<gap desc="illustration"></gap></p>

<p>অমৃত নয়,
গরল</p>

<p>তহলকা-র দ্বিতীয় দফা
ভিডিও টপ কষ একটা থাপ্পড় বসিয়ছ বাজপয়ীর মুখই। প্রশ্ন উঠছ তাঁর ভাবমূর্তির
পরিচ্ছন্নতা নিয়। প্রমাণ হয় গছ অস্ত্রচুক্তিত দীর্ঘদিন ধরই চলছ কাটমানির খলা। জর্জ
ফার্নাণ্ডজ শুধু ধুলা দিয়ছন দশবাসীর চাখ। ক্ষমতায় এস ৮৫ সালর পর সমস্ত প্রতিরক্ষা
চুক্তি নিয় তদন্তর নির্দশ দিয়ছিলন সন্ট্রাল ভিজিল্যান্স কমিশনার ভিট্টলক। ভিট্টল তাঁর
অন্তর্বর্তী রিপার্ট জমাও দিয়ছিলন। প্রশ্ন তুলছন একটার পর একটা চুক্তি নিয়। কিন্তু সই
রিপার্ট পশ করা হয়নি সংসদ। স্রফ চপ যাওয়া হয়ছিল। সাংবাদকিরা ফাঁস কর দিয়ছন
সই রিপার্ট। দখা যাচ্ছ, সমান চলছ ঘুষর খলা। দালালি অবৈধ হলও প্রতিরক্ষা সরঞ্জাম কনার
প্রথম স্তরটি থকই এই দুষ্টচক্র সক্রিয়। কম দর দিলও কন যাগ্য কাম্পানির সরঞ্জাম বাতিল হয়
যায়, কনা হয় বশি দামর জিনিস- সও এক রহস্য। সনাবাহিনী থক আমলা ও মন্ত্রী সর্বত্র
ঘুষর খলা। ইচ্ছ করল দশটাকই বুঝি কিন নওয়া যায়! </p>

<p>এই খলায় জর্জ ফার্নাণ্ডজ
কীভাব সামিল স কাহিনী সবিস্তার শুনিয়ছন অস্ত্র দালাল আর ক জৈন। জানিয়ছন কীভাব
তাঁক সমতা পার্টির কাষাধ্যক্ষ পদ বসিয় দিয়ছিলন জর্জ। আর জর্জ ফার্নাণ্ডজ কীভাব
অ্যাডমিরাল বিষ্ণু ভাগবতক নৗবাহিনীর নতৃত্ব থক সরিয়ছিলন- স কাহিনী আজ সকলর
জানা। এখন মন হচ্ছ মৗচাক ঢিল মরই এই পরিণতি ভাগবতর। </p>

<p>কদিন আগ জৈন
টিভি-ত দখানা হচ্ছিল প্রধানমন্ত্রীক ঘির দুষ্টচক্র কীভাব সক্রিয়। মন পড় যায় সই
গ্যাং অফ ফার-এর কথা। তাদর মূল লক্ষ্য ছিল ব্রজশ মিশ্র। প্রধানমন্ত্রীর প্রধান সচিব, জাতীয়
নিরাপত্তা উপদষ্টা। এবার অস্ত্র দালাল আর ক জৈন স্পষ্ট জানিয়ছন, 'প্রধানমন্ত্রীর দপ্তরক
চুক্তি অর্থর ৩ শতাংশ ঘুষ দওয়াটাই দস্তুর। এর কম কিছু হয় না। জর্জ ফার্নাণ্ডজর কাছ
ফাইল পাঠাত জয়া জটলিক ১০ লক্ষ টাকা দিত হয়। তিনি জর্জ ফার্নাণ্ডজর বিয় না করা
স্ত্রী। দশর দ্বিতীয় প্রতিরক্ষামন্ত্রী। কারগিল যুদ্ধর সময় বামা কনা হয় ১৪ লক্ষ টাকায়। অথচ
দাম ছিল ৬ লক্ষ টাকার। চুক্তি ছিল ২৭০ কাটি টাকার। কমিশন নওয়া হয় অন্তত ৫০ শতাংশ।'
</p>

<p>চাঞ্চল্যকর এইসব তথ্যর
ফিরিস্তি মহাভারতসমান। তব অমৃত নয়, সবই গরল। ব্রজশ মিশ্র আর জর্জ ফার্নাণ্ডজর হাত
বিপন্ন দশর নিরাপত্তা। ব্রজশর সঙ্গী প্রধানমন্ত্রীর জামাই রঞ্জন ভট্টাচার্য ও প্রমাদ মহাজন।
মধ্যপ্রদশর প্রাক্তন মুখ্যমন্ত্রী ডি পি মিশ্রর পুত্র ব্রজশ। প্রধানমন্ত্রীও মধ্যপ্রদশর লাক।
ব্রাহ্মণ সন্তান। এই দুই ব্রাহ্মণর ধূর্ততায় দশবাসী স্তম্ভিত।</p>

<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>দুর্নীতি
ফাঁস করত চাই : তরুণ তেজপাল</p>

<p>
 
 
<gap desc="illustration"></gap>অস্ত্র কনাবচায় অবৈধ লনদনর
ভিডিও টপ প্রকাশ কর আলাড়ন তুলছ তহলকা ডট কম। তার ম্যানজিং ডিরক্টর তরুণ তজপালর
সাক্ষাত্কার। </p>

<p>প্রশ্ন- মিঃ তজপাল, আপনার দল য
সব তথ্য উন্মাচন করছ তাত বাঝা যাচ্ছ দুর্নীতির শকড় বশ গভীর। আপনারা কি তাক
সমূল উত্পাটিত করত চান? 
তেজপাল- অবশ্যই। পুরা ব্যাপারটাই লাভর
পরিণাম। সাত মাসর অনুসন্ধান দখছি কী পরিমাণ লাভী লাকজন। ওরা অস্ত্রফস্ত্র বাঝ না, বাঝ
টাকা। </p>

<p>প্রশ্ন- কিন্তু জয়া জটলি এবং কয়কজন
সামরিক অফিসারর বক্তব্য, ভাল অস্ত্রর জন্য তিনটি দশ তক্ক তক্ক আছ। সুতরাং তা পাওয়ার
জন্য কিছু অতিরিক্ত তা দিতই হব। 
তেজপাল- বাজ কথা। পুরা টপটা দখলই
বুঝত পারবন। একদম রদ্দি অস্ত্র কিনত চলছিল। যা কনায় আবদুল কালামর আদৗ সায় ছিল
না। </p>

<p>প্রশ্ন- আপনাদর রিপার্টর কারণ
বঙ্গারু লক্ষ্মণক পদত্যাগ করত হল। এ নিয় আপনার প্রতিক্রিয়া কী? 
তেজপাল- ভয়ানক গালমল লাক। প্রথম
বলল, কিছু মন করত পারছ না। তারপর বলল ষড়যন্ত্র। ওদিক প্রধানমন্ত্রীক দখুন। সাংবাদিকরা
অজস্র প্রশ্ন করছ, তিনি নিরুত্তর।  
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>তহলকা
অভিযুক্তদর ঘির আইনি বিতর্ক</p>

<p>তহলকা-র গাপন ক্যামরায়
ধৃত প্রতিরক্ষা কলঙ্কারিত অভিযুক্তদর ঘির শুরু হয়ছ আইনি বিতর্ক। বিরাধী রাজনৈতিক
দলগুলির আক্রমণ ইতিমধ্যই কাণঠাসা হয় পড়ছ কন্দ্রীয় সরকার। তব আইনবিশারদরা কী
ভাবছন বিষয়টি নিয়? সুপ্রিমকার্টর আইনজীবী অশাক অরারার মত, বিষয়টি নিয় অবিলম্ব
পুলিস ও সিবিআইয়র তদন্ত শুরু করা উচিত। এই মন্তব্য সমর্থন জানিয় আরক আইনজীবী ডঃ
বি এল ওয়াদরার কথায়, প্রতিরক্ষা কলঙ্কারি সম্পর্ক যা প্রমাণ পাওয়া গছ তার ভিত্তিতই
সিবিআই তদন্ত শুরু হত পার। অন্যদিক, বহু আইনজীবীর ধারণা, তহলকা টপর ভিত্তিত
দুর্নীতি প্রমাণ হওয়া কঠিন। ওই টপ আদালত আদৗ গ্রাহ্য হব কি না, স বিষয়ও প্রশ্ন
তুলছন অনক। তব স্পষ্ট উত্তর নই কারুর কাছই। আইনজীবী পি এন লখির মত,
'আয়কর ফাঁকি দওয়া পর্যন্ত অপরাধ হিসব গণ্য হত পার অভিযুক্তদর। তব দুর্নীতি
প্রতিরাধ আইনর আওতায় বাধহয় অভিযুক্তদর আনা যাব না।' 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>গণ্ডগালর
জের সংসদ মুলতুবি</p>

<p>প্রতিরক্ষা চুক্তি
সংক্রান্ত লনদনক ঘির সৃষ্ট বিতর্কর জর বৃহস্পতিবার সংসদর উভয় কক্ষর অধিবশনই
মুলতুবি হয় যায়। লাকসভার অধিবশনর শুরুতই প্রতিরক্ষা কলঙ্কারি নিয় ট্রজারি বঞ্চ ও
বিরাধী সদস্যদর মধ্য বিতণ্ডা শুরু হয়। চলত থাক উত্তপ্ত স্লাগান বিনিময়। বিজপি বঞ্চ থক
কংগ্রসক লক্ষ্য কর আওয়াজ ওঠ- 'চার মাচায় শার।' কংগ্রস সদস্যরা জবাব দন-
'চার সরকার ওয়াপস যাও।' তুমুল হইচই-এর মধ্যই সভার কাজ মুলতুবি হয় যায়। পুরা
ঘটনার সময় বিরাধী নত্রী সানিয়া গান্ধী চুপচাপ বসছিলন। </p>

<p>রাজ্যসভাতও একই
ঘটনা ঘট। অধিবশন শুরুর সঙ্গ সঙ্গই উপরাষ্ট্রপতি ও সভার চয়ারম্যান কৃষ্ণকান্ত সভা মুলতুবি
ঘাষণা করন। সভাকক্ষর বাইর সরকার ও বিরাধীপক্ষর সদস্যদর মধ্য হাতাহাতির উপক্রম হল
নিরাপত্তারক্ষীরা হস্তক্ষপ করন। কিছুক্ষণ পর অবস্হা নিয়ন্ত্রণ আস।  
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>ইস্তফা দেব
কেন : জয়া জেটলি</p>

<p>
 
 
<gap desc="illustration"></gap>তাঁর বিরুদ্ধ আনা সমস্ত অভিযাগ
অস্বীকার করলন সমতা পার্টির সভানত্রী জয়া জটলি। ইস্তফা দওয়ার সম্ভাবনাও খারিজ কর দিয়ছন
জটলি। তাঁর কথায়, 'আমি এমন কিছুই বলিনি, প্রকাশ্য যা বলা যায় না, এমন কিছুই
করিনি, যাক অন্যায় বলা যায়।' তাই পদত্যাগর প্রশ্নই ওঠ না। </p>

<p>তাঁর দল
প্রতিরক্ষামন্ত্রক কন, কানও মন্ত্রকর কাজ কখনও হস্তক্ষপ করনি বল জটলি জানিয়ছন।
তহলকা-র প্রতিনিধিরা অস্ত্র-বিক্রয় সংস্হার প্রতিনিধি সজ তাঁর সঙ্গ দখা করছিলন। তখন
নাকি জটলি তাঁদর বলন, পার্টি তহবিল টাকা দিত। সই টাকা জাতীয় তহবিল খরচ করা হব।
আর ক জৈনর মন্তব্য দায়ও সমতার নয় বল জানিয়ছন জটলি। জানুয়ারি মাস দলীয়
নির্বাচন পরাজিত হওয়ার পর জৈন কাষাধ্যক্ষর পদ ছড়ছিলন, জানিয়ছন জয়া। 

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>তহলকা-র
সাংবাদিকক হত্যার হুমকি</p>

<p>প্রাণনাশর হুমকি দওয়া
হয়ছ তহলকা ডট কমর সাংবাদিক ম্যাথু স্যামুয়লক। স্যামুয়ল নিজই এই হুমকির কথা জানিয়ছন,
'অপারশন ওয়স্ট এণ্ড'-এর মাধ্যম তহলকার য দুই সাংবাদিক কন্দ্রীয় সরকারর ভিত নড়িয়
দিয়ছন, স্যামুয়ল তাঁদর মধ্য অন্যতম। </p>

<p>শুধু তাঁকই নয়,
তাঁর স্ত্রীকও হত্যার হুমকি দওয়া হচ্ছ বল স্যামুয়ল জানিয়ছন। বশ কয়কজন অপরিচিত
ব্যক্তি বাড়িত এস অত্যন্ত অশালীন উক্তি কর গছন। স্যামুয়ল আরও জানিয়ছন, প্রায় ১০
মাস আগই তাঁরা তদন্তর কাজ শুরু করছিলন। ট্রন চপ তিনি পুণ থক মুম্বই যাচ্ছিলন।
সখানই সামরিক বাহিনীর এক কর্মীর সঙ্গ তাঁর আলাপ হয়। সই তাঁক প্রতিরক্ষাবাহিনীত ডিলর
কথা বল। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>তহলকা :
বিপাক রাজনাথ সিং</p>

<p>উত্তরপ্রদশ মুখ্যমন্ত্রী
রাজনাথ সিংয়র বিরুদ্ধ তহলকা ডট কমর গাপন রিপার্টক ব্যবহার কর প্রচার চালাচ্ছন বিরাধীরা।
প্রচারর বক্তব্য: য দলর সর্বভারতীয় সভাপতি ঘুষ নন, কন সই দলর প্রার্থীক ভাট দবন?
</p>

<p>বিজপির অন্যতম শরিক
শিবসনাও রাজনাথর বিরুদ্ধ প্রার্থী দিচ্ছ। সনার রাজ্য শাখার সভাপতি বলছন, উত্তরপ্রদশ
সরকারর বিরুদ্ধ দুর্নীতির ভুরিভুরি অভিযাগ আছ। তার প্রতিবাদ হিসবই রাজনাথর
বিরুদ্ধ প্রার্থী দওয়া হব। গত চারবছর বিজপি উত্তরপ্রদশক লুটপুট খয়ছ বলও তাঁর
অভিযাগ। </p>

<p>পাঁচ মাস আগ
মুখ্যমন্ত্রী হওয়া রাজনাথ সিং এখনও পর্যন্ত রাজ্য আইনসভার কানও কক্ষরই সদস্য নন। হায়দরগড়
বিধানসভা আসন তাঁর প্রধান প্রতিদ্বন্দ্বী পার্টি ও কংগ্রস। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>মাধ্যমিকর
প্রথম লক্ষ্মণর রেকর্ড, এখন ভারত সেরা</p>

<p>নাম বঙ্গিপুরাপ্প বঙ্কট
সাই লক্ষ্মণ। অন্ধ্রর ছল। বুধবার ইডন একর পর এক রকর্ড ভাঙলন তিনি। মাধ্যমিক পরীক্ষায়
অন্ধ্র প্রথম হয়ছিলন লক্ষ্মণ। লিটল ফ্লাওয়ার স্কুল থক পরীক্ষা দিয়ছিলন। তাঁর বাবা
শান্তারাম পশায় চিকিত্সক। লক্ষ্মণ পর পড়ন সন্ট মরিজ কলজ। ৯৮ শতাংশ নম্বর পয়ছিলন
মাধ্যমিক। ১৩ বছর বয়স ক্রিকট খলা শুরু। প্রথম ম্যাচ ১৪০ রান করছিলন। </p>

<p>দ্বিশত রান করতই তাঁক
দু লাখ টাকা দওয়া হয়। এরপর ২৩৬ রান পর্যন্ত রান পিছু হাজার টাকা। ২৮১ পর্যন্ত প্রতিটি
রানর জন্য লক্ষ্মণ পাবন ২০০০ টাকা কর। </p>


 ১৬৮ কর পরালন ব্যক্তিগত সর্বাচ্চ ১৬৭
     (সিডনি ২০০০)। 
 ১৭৮ কর ঘরর মাটিত অস্ট্রলিয়ার বিরুদ্ধ সর্বাচ্চ
     শচীনর ১৭৭ পরিয় গলন। 
 ২০৭ কর অস্ট্রলিয়ার বিরুদ্ধ ভারতীয় হিসব
     সর্বাচ্চ রবি শাস্ত্রীর ২০৬ পরিয় যান। 
 ২৩৭ কর ভাঙলন ভারতীয় হিসব সর্বাচ্চ
     গাভাসকরর ২৩৭ রানর রকর্ড। 
 একদিন পুরা ব্যাট করার ক্ষত্র নতুন রকর্ড করন
     লক্ষ্মণ-দ্রাবিড়। 


<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>
 
<gap desc="illustration"></gap>
মৃদুল দাশগুপ্ত</p>

<p>শূর্পণখার
নাক কেটছ 
রামায়ণর কাল 
ক্যাঙারু-বধ করল তুমি

দু হাজার এক সাল।

দেখিয় দিল দাপট তুমি

রামচন্দ্রর ভ্রাতা 
ব্যাট হাত আজ ভাই
লক্ষ্মণ </p>

<p>এই ভারতর ত্রাতা।</p>

<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>লক্ষ্মণক
অভিনন্দন জানালন গাভাসকর</p>

<p>১৭ বছর লাগল গাভাসকরর ২৩৬ রানর রকর্ডটা ভাঙত। রকর্ডটা
লক্ষ্মণ ভাঙায় গাভাসকর ফান করছিলন সি এ বি সভাপতি জগমাহন ডালমিয়ার ঘর। কিন্তু তখন
স্যালাইন নিচ্ছন লক্ষ্মণ। তাই দুজনর কথা হয়নি। গাভাসকরর বাড়ির টলিফানর নম্বর লক্ষ্মণক
দওয়া হয়। আর লক্ষ্মণর হাটলর নম্বর দওয়া হয় গাভাসকরক। কিন্তু বুধবার রাত দুজনর
আর কথা হয়নি। গাভাসকর শষ পর্যন্ত আনসারিং মশিন তাঁর অভিনন্দনবার্তা রখ দন।
</p>

<p>গাভাসকর বলন, সবার ০ রান দুই উইকট পড়ছিল।
হ্যাটট্রিকর মুখ পড়ছিলাম। ভিভ বলছিল, আমিও ০ করব। আমি কিন্তু লক্ষ্মণক বশি
অভিনন্দন জানাব পরিস্হিতির প্রতিকূলতার জন্য। আশাকরি এরকম ইনিংস ওর কাছ আরও পাব।
</p>

<p>লক্ষ্মণক ২৩৭-এ পৗঁছত গাভাসকর দখন টলিভিশন।
গাভাসকরক সামলাত হয়ছিল মার্শাল, হার্ডিং, রবার্টসদর। আর লক্ষ্মণ সামলালন বিশ্বসরা
ম্যাকগ্রাথ, ওয়ার্ন, গিলসপিদর।  
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>মোহনবাগানর
সব আশা শেষ হয় গেল</p>

<p>তিরুবনন্তপুরম ছাটখাটা চহারার আসিফ সাহির মাহনবাগানক
চুরমার কর দিল। স্টট ব্যাঙ্ক অফ ত্রিবাঙ্কুর এই তারকার দৗড় আর গালক্ষুধার কাছ হার
মানল মাহনবাগান। কলকাতার বাইর গিয় ৪ গাল হজম লজ্জা মাহনবাগানর এই প্রথম। কদিন
আগ এই এস বি টি ১-৬ গাল হর এসছ এয়ার ইন্ডিয়ার কাছ। দশ মিনিটর মধ্য ২ গাল
হজম কর মাহনবাগান ডিফন্স। গাল করন সাহি ও হাকিম। ব্যবধান কমিয়ছিল ব্যারটা
কিন্তু ৩৮ মিনিট আবার গাল করন সাহির। আর সাবির আলি করন ৪-১। শুধু মান বাঁচাত
২-৪ করত পারন ব্যারটা। মাহনবাগানক এখন লিগ তিন নম্বরর জায়গাটার জন্য লড়ত হব।
খুব খারাপ খলছ মাহনবাগান। চাট-আঘাত দল চূড়ান্ত বিপর্যস্ত। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>বিশ্ব বাউল
বাপিদাস</p>

<p>
 
 
<gap desc="illustration"></gap>পূর্ণদাস বাউলর নাম সবার
জানা। বাংলার বাউল গান বিশ্ব মাতিয়ছিলন। তাঁরই মজা ছল বাপিদাস বাউল। বাপি
শুধু বাউল গাইছন না, বিদশি সুরর সঙ্গ বাউলর সুর মলাচ্ছন, তৈরি করছন মিশ্র গান।
তাঁর গান রীতিমত আকৃষ্ট হয়ছন পপ-সম্রাজ্ঞী ম্যাডানা। বাপির সুর গাইছনও। বাপি
শান্তিনিকতন পাঠ নিয়ছন। তারপর বরিয় পড়ছন একতারা হাত বিশ্বজয়। প্যারিস ৮
বছর ছিলন। জীবিকার জন্য খতখামার কাজ করছন, মুদির দাকান কাজ করছন, কিন্তু
গান ছাড়ননি। তাই শুধু প্যারিসই অনুষ্ঠান করছন ১৩০টি। গান শুনিয়ছন স্লাভানিয়া, জার্মানি,
বলজিয়াম, স্পন, ইতালিত। নিজর লখা ও সুর-করা গানর সংখ্যা গাটা পঞ্চাশক। স সব
গানর সিডি রম বরিয়ছ। জনপ্রিয়তাও পয়ছ। নপথ্য গলা দিয়ছন বশ কিছু তথ্যচিত্র।
শুধু নিজর গান নয় ফ্রান্স উত্তম্যাড ফস্টিভ্যাল এখান থক নিয় গছিলন নবনীদাস, ক্ষ্যাপা
বাউল, গাপীদাস, পূর্ণদাস প্রমুখক। বাপি চান বাংলার বাউল, ফকিরি, ভাটিয়ালির জয়ধ্বজা
তুল রাখত। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>মৌসুমী দ্বীপর
বাসিন্দারা কার্যত নির্বাসিত</p>

<p>সুন্দরবনর মৗসুমী দ্বীপর বাসিন্দার সংখ্যা ২৩০০০। এঁরা
কার্যত নির্বাসিত। জীবনযাপনর নূ্যনতম প্রয়াজনীয় জিনিসপত্রও এঁরা বঞ্চিত। যমন চিকিত্সা
ব্যবস্হা। ওই দ্বীপ একটি প্রাথমিক স্বাস্হ্য কন্দ্র আছ, তব তা না থাকারই মতা। সখান
ওষুধ নই, সরঞ্জাম নই, প্রয়াজনীয় চিকিত্সকও নই। বসরকারী দাকান ওষুধর চড়া দাম।
সাধারণ মানুষর আয়ত্তর বাইর। য চিকিত্সক এই বন্দাবস্তর বিরুদ্ধ মুখ খুলত চয়ছিলন,
সই দুলাল সর্দারর নামই দুর্নীতির কলঙ্ক লাগিয় দিয়ছ স্হানীয় রাজনৈতিক নতারা। গত
২০ বছর ধরই এটা চলছ। স্হানীয় বি ডি ও-র মত, এলাকার মানুষর জন্য চিকিত্সা ব্যবস্হায়
ঘাটতি আছ। তব স্হানীয় বিধায়ক প্রভঞ্জন মণ্ডলর কথায়, 'একটু আধটু অসুবিধা আছ
ঠিকই। সগুলা ঠিক কর দওয়ার ব্যবস্হা নিচ্ছি আমরা।' সাধারণ মানুষ অবশ্য এ সব স্তাক
মানত রাজি নন। তাঁদর বক্তব্য, নির্বাচনর আগ য সব প্রতিশ্রুতি দওয়া হয় তার কানওটাই
রাখা হয় না। তাই আসন্ন বিধানসভা নির্বাচন বয়কট করার সিদ্ধান্ত নিয়ছন মৗসুমী দ্বীপর
বাসিন্দারা। </p>

<p> </p>






</body></text></cesDoc>