<cesDoc id="ben-w-bengalnet-news-01-03-23" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-03-23.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-03-23</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-03-23</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>মমতাক
আক্রমণ শুরু বিজপি-র</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
 
<gap desc="illustration"></gap>মমতার সঙ্গ নির্বাচনী ঐক্য হব
না ধর নিয়ই তাঁক আক্রমণ শুরু করল বিজপি। বিজপি নতা মুখতার আব্বাস নকভি বলছন,
মমতাক আগ প্রকাশ্য এন ডি এ সরকারর ওপর সমর্থনর কথা ঘাষণা করত হব। তারপরই
আসন রফা নিয় আলাচনা শুরু হত পার। তৃণমূল নতৃত্ব এই প্রস্তাব নস্যাত্ কর দিলও
বৃহস্পতিবার পর্যন্ত জাট ভঙ গল' এমন কথা জানাননি। বাস্তব পরিস্হিতি বিচার কর দখত
গল অবশ্য আঁতাতর সম্ভাবনা ক্ষীণ। তবু জাট ভাঙার দায় কউ নিত চান না বলই প্রকাশ্য কানও
শিবিরই এই ব্যাপার মুখ খুলত নারাজ। </p>

<p>নকভির মত, তহলকা কলঙ্কারি
বাজপয়ী সরকারর ভাবমূর্তি নষ্ট করার একটি ষড়যন্ত্র মাত্র। বিরাধী দলগুলির পাশাপাশি মমতাকও
তিনি এই ষড়যন্ত্রর অংশীদার মন করন। তা সত্ত্বও সি পি এম ক ক্ষমতাচু্যত করত পশ্চিমবঙ্গ
তৃণমূলর সঙ্গ সমঝাতায় রাজি বিজপি। নকভি আরও বলন, জাট না হল ২০০-র বশি আসন
একাই লড়ব বিজপি। </p>

<p>বিধানসভা নির্বাচন জাট
গড়া নিয় তৃণমূল নত্রীর সঙ্গ সানিয়া গান্ধীর কথাবার্তা শুরু হয়ছ। দুজনর মধ্য টলিফান
কয়ক দফা আলাচনা হয়ছ। কয়কদিনর মধ্য দিল্লিত সানিয়া ও মমতার মুখামুখি বৈঠকও
হত পার। আসন রফা নিয় কথা বলার জন্য বৃহস্পতিবার রাত জরুরি তলব পয় দিল্লি গছন
প্রদশ কংগ্রস সভাপতি প্রণব মুখাপাধ্যায়। </p>

<p>তৃণমূল ও কংগ্রসর
মধ্য জাট গড়ার কাজ কব শষ হব তা পুরাপুরি অনিশ্চিত। মমতা গত দুদিন ধর সাংবাদিকদর
সঙ্গ দখাই করছন না। তাঁর ঘনিষ্ট শিবিরর একাংশ মন করছন, আগামী রবিবারর মধ্যই
একটা পরিষ্কার চিত্র পাওয়া যাব। অন্যরা অবশ্য এতটা আশাবাদী নন। কংগ্রসর কউ আবার
দিল্লির নির্দশ মুখ খুলছন না। তাঁদর পরিষ্কার জানিয় দওয়া হয়ছ, কথাবার্তা যা
হবার দিল্লিতই হব। ৩৩ জন কংগ্রস বিধায়কর আসন তৃণমূল প্রার্থীদর নাম ঘাষণা হয় গছ।
তাঁরা এখন মহা অনিশ্চয়তার মধ্য পড়ছন। আসন সংখ্যা নিয় অবশ্য বিরাধর সম্ভাবনা কম।
অন্তত ৬০টি আসন কংগ্রসক ছাড়ত তৃণমূলর আপত্তি নই। তব কংগ্রসর বর্তমান বিধায়কদর
সবাই টিকিট পাবন না। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p>প্রণব-গনিক
দেখ করুণা হয় : অনিল</p>

<p> </p>

<p>
 
<gap desc="illustration"></gap>দশর ধর্মনিরপক্ষতা রক্ষায় কংগ্রসর
দীর্ঘদিনর ঐতিহ্য কংগ্রস নতারা মমতা ব্যানার্জির পায়র নিচ সঁপ দিচ্ছন বল অভিযাগ
করলন সি পি এমর রাজ্য সম্পাদক অনিল বিশ্বাস। এজন্য ভবিষ্যত কংগ্রসক চরম মূল্য দিত হব
বল তিনি সতর্ক কর দিয়ছন। তৃণমূল ও কংগ্রসর মধ্য সম্ভাব্য আঁতাত নিয় প্রতিক্রিয়া
জানাত চাইল অনিলবাবু বলন: 'প্রণব মুখার্জি, গনিখান চৗধুরীর মতা নতাদর দখ
আমার করুণা হয়। এঁরা দীর্ঘদিন ধর একটা সর্বভারতীয় দলর নতা। এঁদর পিছন সুদীর্ঘ
রাজনৈতিক জীবনর ঐতিহ্য রয়ছ। কিন্তু সসব ভুল আজ তৃণমূলর সঙ্গ জাট বাঁধত এঁরা
মমতার পায়র তলায় বস পড়ছন।' </p>

<p>প্রস্তাবিত জাট সম্পর্ক
অনিলর প্রতিক্রিয়া: 'ভাটর ৪০ দিন আগ জাটর অর্থই হল সুবিধাবাদী মনাভাব নিয়
কাজ করা। তাছাড়া, এভাব জাট হয় না। সজন্য দীর্ঘদিনর রাজনৈতিক আন্দালন প্রয়াজন।'
মমতা-কংগ্রসর বাঝাপড়াক সি পি এম ও বামফ্রন্ট কানও গুরুত্ব দিচ্ছ না বলও জানিয়ছন
অনিল। মুখ একথা বললও বাঝাই যাচ্ছ কং-তৃণমূলর জাট সম্পর্ক যথষ্টই উদ্বিগ্ন বামফ্রন্ট।

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p>
 
<gap desc="illustration"></gap></p>

<p>মোহনবাগান
জিতল কষ্ট, ১ গোল</p>

<p>
 
 
<gap desc="illustration"></gap>জাতীয় ফুটবল লিগ ১৫টা ম্যাচর
পরও মাহনবাগান সমস্যা কাটল না। গাল কিছুতই আসছ না। ব্যক্তিগত স্বার্থটা বড় হল দল
বা ক্লাবর য ক্ষতি হত পার এ বাধটা সম্ভবত ফুটবলারদর নই। তাই ব্যারটা বল দিচ্ছন
না আর সি প্রকাশক, তমনি প্রকাশ দিচ্ছন না ব্যারটাক। দুর্ভাগ্য মাহনবাগানর।
</p>

<p>বৃহস্পতিবার কলকাতা
যুবভারতী ক্রীড়াঙ্গন টালিগঞ্জ অগ্রগামীর বিরুদ্ধ মাহনবাগান মাত্র  ১টি গালই করল। দিনর
একমাত্র গাল আর সি প্রকাশর। সংখ্যা বাড়ত পারত যদি প্রকাশ-ব্যারটা এক অপরক সাহায্য
করতন। প্রকাশক গাল করত হয়ছ বাসুদর মন্ডলর পাস থকই। </p>

<p>টালিগঞ্জ তিনটি সুযাগ
পয়ছিল। কিন্তু কখনও বল লক্ষ্যভ্রষ্ট হয়ছ, কখনও পাস্ট লগ ফিরছ। চার বিদশী খললন,
কিন্তু কউই কার্যকর নন। বিদশ থক আনলই ম্যাচ জতা যায় না। ভাল বিদশী চাই।
কলকাতার কানও দলই একথা উপলব্ধি করত পারছ না কন বাঝা দায়। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p>
 
<gap desc="illustration"></gap></p>

<p>
 
<gap desc="illustration"></gap></p>

<p>পূর্বাভাস
: শুক্রবার
প্রধানত মঘলা আকাশ। কানও কানও অঞ্চল হাল্কা বৃষ্টিপাতর সম্ভাবনা। দিনর সর্বাচ্চ
তাপমাত্রা ৩৪ ডিগ্রি এবং সর্বনিম্ন তাপমাত্রা ২৪ ডিগ্রি সলসিয়াসর কাছাকাছি থাকব।
</p>

<p>তাপমাত্রা : বৃহস্পতিবারর সর্বাচ্চ ৩৪.৭
এবং সর্বনিম্ন ২৫.৬(+৪) ডিগ্রি সলসিয়াস।
</p>

<p>আপক্ষিক আর্দ্রতা
: সর্বাধিক
৯৫ শতাংশ এবং নূ্যনতম ৪৫ শতাংশ। </p>

<p>বৃষ্টি : হাল্কা।</p>

<p> </p>






</body></text></cesDoc>