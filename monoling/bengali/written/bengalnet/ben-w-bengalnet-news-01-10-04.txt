<cesDoc id="ben-w-bengalnet-news-01-10-04" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-bengalnet-news-01-10-04.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Department of Linguistics, Lancaster University</respName>
<respType>text collected by</respType>
<respName>Paul Baker</respName>
<respType>transferred into Unicode by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>"Bengalnet" internet news (www.bengalnet.com), news stories collected on 01-10-04</h.title>
<h.author>Bengalnet</h.author>
<imprint>
<pubPlace>West Bengal, India</pubPlace>
<publisher>Bengalnet</publisher>
<pubDate>01-10-04</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-08-24</date>
</creation>
<langUsage>Bengali</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>
<body>




<p>সব যাত্রীই
মুক্ত ও সুস্হ, কোনও ছিনতাইকারী ছিল না 
রাতভর
বিমান ছিনতাই-নাটকর যবনিকা ভোর</p>

<p>মুম্বই থক দিল্লি
যাওয়ার পথ বুধবার মাঝরাত অ্যালায়ন্স এয়ারর সি ডি ৭৪৪৪ বিমান ছিনতাই হয়ছ- এই খবর
গাটা দশ তীব্র উত্তজনা ও আতঙ্ক দখা যায়। সবাই ধর নন, এটা সন্ত্রাসবাদীদর কাজ। বিশষত
১১ সপ্টম্বর আমরিকায় যা ঘটছ তা থক উদ্বগ আরও বাড়। ভাররাত বাঝা যায় পুরাটাই
গুজব। বিমান আদৗ কানও ছিনতাইকারী ছিল না। সমস্ত যাত্রীক সুস্হ অবস্হায় পাওয়া গছ।
মুম্বই থক বিমানটি ওড়ার কিছুক্ষণ পর তা ছিনতাই হয়ছ বল আমদাবাদর এয়ার ট্রাফিক কন্ট্রাল
য ভুয়া টলিফান কল এসছিল সটাই বিভ্রান্তির প্রধান কারণ বল মন করা হচ্ছ। </p>

<p>কীভাব রটল
ছিনতাইয়র গুজব : </p>

<p>রাত ১১ টা ১৫ মিনিট
মুম্বই থক বিমানটি দিল্লির উদ্দশ রওনা হয়। ঠিক তার পরই আমদাবাদ এ টি সি-ত ভুয়া টলিফান
আস। জানা যায়- দু'জন ছিনতাইকারী রয়ছ। তাদর কাছ অস্ত্রও রয়ছ। বিমানর পাইলট
ককপিট সিল কর দওয়ায় বিমানদসু্যরা যাত্রীদর মধ্যই থাকত বাধ্য হয়। পাইলট এ টি সি-ত
জানান, ছিনতাইকারীরা বিমানটি লখনউ নিয় যত চাইছ। এ টি সি থক পাইলটক বলা হয়- যকানওভাব
বিমান দসু্যদর বুঝিয় সুঝিয় বিমানটি দিল্লি আনত। এই অবস্হায় রাত আড়াইটা নাগাদ সি
ডি ৭৪৪৪ দিল্লি বিমানবন্দর নাম। ততক্ষণ ঘুম থক তালা হয়ছ প্রধানমন্ত্রী, স্বরাষ্ট্রমন্ত্রী ও
অসামরিক বিমান পরিবহন মন্ত্রী শাহনওয়াজ হুসনক। বস গছ ক্রাইসিস ম্যানজমন্ট গ্রুপর
বৈঠক। </p>

<p>বিমান সই সময়
মজুত মাত্র দড় ঘণ্টা ওড়ার মত জ্বালানি। জ্বালানি ভর্তি একটি ট্রাক বিমানর কাছ নিয়
যাওয়া হয়। মজুত ছিল অ্যাম্বুলন্স ও বম্ব স্কায়াডও। বিমানটি যাত কানওভাব দিল্লি ছাড়ত
না পার সজন্য ন্যাশনাল সিকিউরিটি গার্ডর কম্যাণ্ডারা চারদিক ঘির ফলন। </p>

<p>গুজবর অবসান
: </p>

<p>বহু আলাচনার পর
সরকার সিদ্ধান্ত নয় যাত্রীদর মুক্ত করত কম্যাণ্ডা হানা ছাড়া কানও উপায় নই। সইমত ভার
সাড় চারট নাগাদ আচমকা বিমানর দরজা ভঙ ঢুক পড় সশস্ত্র কম্যাণ্ডারা। দখা যায়, ৫২
জন যাত্রীই নিরাপদ। কানও ছিনতাইকারীর দখা মলনি। যাত্রীরা জানান- প্রথম তাঁদর
যান্ত্রিক গালাযাগর কথা বলা হয়ছিল। পর তাঁরা ছিনতাইয়র কথা জানত পারন।

</p>

<p>
 
 
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 

 
<gap desc="illustration"></gap></p>

<p>সরকার গড়ার
তোড়জাড় শুরু 
নতুন
কর নির্বাচনর দাবি হাসিনার</p>

<p>বাংলাদশ নতুন
সরকার গঠনর তাড়জাড় শুরু কর দিলন বি এন পি নত্রী খালদা জিয়া। বুধবার সারাদিন
বিভিন্ন শরিক নতাদর সঙ্গ আলাচনার পর রাত তদারকি সরকারর প্রধান বিচারপতি লতিফুর
রহমান ও রাষ্ট্রপতি সাহাবুদ্দিন আহমদর সঙ্গ সৗজন্য সাক্ষাত্কার করন খালদা। বৃহস্পতিবার
বিকল আনুষ্ঠানিকভাব সরকারি গজট জয়ী প্রার্থীদর নাম ঘাষণা করা হব। এরপর সংসদর
স্পিকার হামিদ নির্বাচিতদর শপথ বাক্য পাঠ করাবন। সম্ভবত পবিত্র জুম্বাবার (শুক্রবার)
প্রধানমন্ত্রী হিসব শপথ নবন খালদা। </p>

<p>এদিক পরাজয় মন
নিত আদৗ রাজি নন হাসিনা। গাটা নির্বাচন বাতিল কর ১০ অক্টাবরর মধ্য নতুন কর ভাট
গ্রহণর দাবি জানিয়ছন তিনি। এটা না করা হল দশজুড় অসহযাগ আন্দালন করা হব।
আওয়ামি লিগর কানও জয়ী প্রার্থী শপথ নবন না বলও ঘাষণা করছন হাসিনা। </p>

<p>বাংলাদশ নতুন
সরকারর সঙ্গ হাত মিলিয় চলত আগ্রহী ভারত। মঙ্গলবার গভীর রাত বাজপয়ীক টলিফান করন
খালদা। বুধবার জবাব খালদাক চিঠি লিখ অভিনন্দন জানিয়ছন বাজপয়ী। তিনি চিঠিত
লিখছন- দুই প্রতিবশী দশর মধ্য দৃঢ় সামাজিক, ঐতিহাসিক ও সাংস্কৃতিক বন্ধন রয়ছ।
অভিন্ন মূল্যবাধই স্হায়ী বন্ধুত্বপূর্ণ সম্পর্কর ভিত্তি রচনা করছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p>ছিনতাইয়র
গুজব : তদন্তর আশ্বাস মন্ত্রীর</p>

<p>কীভাব বিমান
ছিনতাইয়র গুজব রটল তা খতিয় দখত তদন্তর নির্দশ দিলন অসামরিক বিমান পরিবহন মন্ত্রী
শাহনওয়াজ হুসন। বৃহস্পতিবার ভার তিনি সাংবাদিকদর বলন, এটা অত্যন্ত উদ্বগর বিষয়।
এখান ৫২ জন যাত্রীদর নিরাপত্তাও জড়িত। তাই দখত হব কারা এই গুজব রটালা। দাষী
ব্যক্তিদর খুঁজ বর কর শাস্তি দওয়া হব। </p>

<p>শাহনওয়াজ বলন, আমদাবাদ
এয়ার ট্রাফিক কন্ট্রাল বনামী টলিফানই সব বিভ্রান্তর কারণ। এ টি সি-র উচিত ছিল তা
'ক্রসচক' করা। সটা তারা করনি। মন্ত্রীর কথায়: একজন যাত্রী নিজক বিমান পরিবহন
মন্ত্রকর অফিসার হিসব পরিচয় দিয় ককপিট ঢুকত চাইল বিমানকর্মীরা তাঁর পরিচয়পত্র দখত
চান। সটা ওই যাত্রী দখাত পারননি। তাঁকই সবাই ছিনতাইকারী বল ধর নন। এরপরই
পাইলট ভয় পয় ককপিট সিল কর দন। </p>

<p>শাহনওয়াজ আরও বলন,
পুরা ঘটনাই গুজব হলও এর থক অন্তত এটা প্রমাণ করা গল য, ছিনতাইয়র মাকাবিলায়
সরকারি ব্যবস্হা কতটা কার্যকর। সকলর অলক্ষ্য এন এস জি কম্যাণ্ডারা বিমানর মধ্য ঢুক পড়ত
পরছিলন। </p>

<p>পাকিস্তানর
বিরুদ্ধ সরাসরি যুদ্ধ চান ফারুক</p>

<p>পাকিস্তানর বিরুদ্ধ
সরাসরি যুদ্ধ ঘাষণার জন্য কন্দ্রর কাছ আর্জি জানালন জম্মু কাশ্মীরর মুখ্যমন্ত্রী ফারুক
আবদুল্লা। বুধবার রাজ্য বিধানসভার অধিবশন তিনি বলন, ধৈর্যর একটা সীমা আছ। আর
কতদিন আমরা সহ্য করব? সরকারর উচিত পাকিস্তানর বিরুদ্ধ যুদ্ধ ঘাষণা কর সখানকার
সমস্ত জঙ্গি ঘাঁটি গুঁড়িয় দওয়া। উগ্রপন্হার কবল পড় কাশ্মীরিরা য দুঃখ-কষ্ট-নির্যাতন ভাগ
করছন, তা থক রহাই পাওয়ার একমাত্র উপায় সন্ত্রাসবাদক সমূল বিনষ্ট করা। ফারুকর অভিযাগ:
ভারত ও পাকিস্তানর মাঝ পড় কাশ্মীরিদর চিঁড়চ্যাপ্টা হত হচ্ছ- 'এক দশ এখান
নিরীহ মানুষক খুন করছ আর অন্যদশ তার নীরব দর্শক।' </p>

<p>আমরিকার উদাহরণ টন
ফারুক বলন, নিউইয়র্ক ও পন্টাগন জঙ্গি হানার পর সন্ত্রাসবাদর বিরুদ্ধ যুদ্ধ ঘাষণা করত
বুশ একদিনও সময় নননি। আর ভারত কাশ্মীরর ক্ষত্র ১২ বছর ধর অপক্ষা করছ।  ফারুকর
কথায়: জঙ্গিরা আমাক মারত চাইল আমি সীমান্ত যত রাজি। সখান ওরা আমাক গুলি
করুক। তার বিনিময় রাজ্যর মানুষ যন শান্তিত বাঁচত পার। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>কাশ্মীরর
সন্ত্রাসর প্রতি নজর রাখছ আমরিকা : যশবন্তক পাওয়ল</p>

<p>কাশ্মীরর সন্ত্রাসবাদও
মার্কিন প্রসিডন্ট জর্জ বুশর তালিকায় আছ বল ফর জানাল আমরিকা। মার্কিন বিদশসচিব
কলিন পাওয়ল সফররত ভারতীয় বিদশন্ত্রী যশবন্ত সিংক এই আশ্বাস দন। কাশ্মীর বিধানসভা ভবন
জঙ্গিদর আক্রমণর ঘটনায় (মারা গছন অন্তত ৪০ জন) তীব্র ক্ষাভ প্রকাশ কর পাওয়ল বলন,
কাশ্মীর যা ঘট চলছ তা সন্ত্রাসবাদ ছাড়া কিছুই নয় এবং এই ধরনর সন্ত্রাসবাদ পৃথিবী থক
সমূল বিনাশ করাই মার্কিন যুক্তরাষ্ট্রর লক্ষ্য। 'আমরা সন্ত্রাসবাদক সামগ্রিক দৃষ্টিভঙ্গি থক
দখত চাই। এটা শুধু লাদন, আল কায়দা ও আফগানিস্তানর মধ্যই সীমাবদ্ধ নয়। ভারত-সহ বিশ্বর
বিভিন্ন দশ এর শিকার।' গণতন্ত্রর প্রাণকন্দ্র আইনসভাভবন আক্রমণক 'সন্ত্রাসর নির্লজ্জ
নিদর্শন' বলও বর্ণনা করছন কলিন পাওয়ল। </p>

<p>সন্ত্রাসর বিরুদ্ধ আমরিকার
ঘাষিত যুদ্ধ ভারতর সমর্থনর জন্য পাওয়ল যশবন্তক আন্তরিক ধন্যবাদ জানিয়ছন। যশবন্তও
সুযাগ বুঝ পাওয়ল এবং জর্জ বুশর কাছ উগ্রপন্হায় পাকিস্তানি মদতর ভুরিভুরি প্রমাণ পশ
করছন। এই ব্যাপার মার্কিন প্রসিডন্টর দৃষ্টি আকর্ষণ করত তাঁক একটি চিঠিও লিখছন
বাজপয়ী। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>তালিবানদর
বিরুদ্ধ মনস্তাত্ত্বিক যুদ্ধ শুরু করছ আমরিকা</p>

<p>আফগানিস্তানর বিরুদ্ধ
সামরিক অভিযান শুরুর প্রস্তুতির পাশাপাশি তালিবানদর বিরুদ্ধ মনস্তাত্ত্বিক যুদ্ধ শুরু কর দিয়ছন
বুশ। মনস্তাত্ত্বিক এই লড়াইয়র লক্ষ্য: তালিবান নতৃত্বর মধ্য ভুল বাঝাবুঝি সৃষ্টি করা, যাত
তালিবান যাদ্ধারা মনাবল হারিয় ফল। এজন্য একটি গাপন পরিকল্পনা অনুমাদন করছন
মার্কিন প্রসিডন্ট। এই পরিকল্পনার আওতায় নর্দার্ন অ্যালায়ন্স-সহ তালিবান-বিরাধী সমস্ত গাষ্ঠীক
অর্থ ও অন্যান্য সাহায্য দব আমরিকা। মার্কিন বিদশ মন্ত্রকর এক উচ্চপদস্হ মুখপাত্র বলছন-
তালিবানদর মধ্য বিরাধ সৃষ্টি করত আমরা সব কিছুই করব। </p>

<p>খুব শীঘ্রই রডিও
সম্প্রচারর মাধ্যম মার্কিন প্রশাসন আফগানদর বাঝাত চষ্টা করব য, তাদর লড়াই ইসলাম
বা মুসলিম দশগুলির বিরুদ্ধ। মার্কিন যুক্তরাষ্ট্রর লড়াই ওসামা বিন লাদন বা আরও সরাসরি
বলল সন্ত্রাসবাদর বিরুদ্ধ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>লাদনর
জড়িত থাকার প্রমাণ দিল আমরিকা</p>

<p>১১ সপ্টম্বর পন্টাগন
ও ওয়র্ল্ড ট্রড সন্টার জঙ্গি হানায় ওসামা বিন লাদন ও আল কায়দা সংগঠনর জড়িত থাকার
প্রমাণ আমরিকা ন্যাটা গাষ্ঠীভুক্ত দশগুলির হাত তুল দিয়ছ। ন্যাটার মহাসচিব
রবার্টসন বলছন, প্রমাণ পয় আমরা খুশি। এত লাদনর হাত থাকার ব্যাপার আর কানও
সন্দহই নই। প্রয়াজন প্রামাণ্য নথিপত্র ও সূত্র ভারত ও পাকিস্তানক দিত রাজি হলও
তালিবানদর কাছ পশ করার সম্ভাবনা উড়িয় দিয়ছ আমরিকা। </p>

<p>এদিক, বুধবার লাদন
ইসু্যত ফর আলাচনার প্রস্তাব দিয়ছন পাকিস্তান নিযুক্ত আফগান রাষ্ট্রদূত আবদুল সালাম
জঈফ। তাঁর কথায়: যুদ্ধর চয় শান্তির মূল্য আমাদর কাছও বশি। কিন্তু তা বল চাপর
কাছ নতি স্বীকার করার চয় যুদ্ধ মরাই আফগানরা শ্রয় মন করব। সুনির্দিষ্ট প্রমাণ ছাড়া
লাদনক আমরিকার হাত তুল দওয়া হব না বলও ফর জানান জঈফ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>হাসিনার
দায়িত্ব </p>

<p>বাংলাদশ সংসদীয়
নির্বাচনর ফলাফল প্রকাশিত হয়ছ। তীব্র প্রতিদ্বন্দ্বিতার সম্ভাবনা সমূল বিনাশ কর খালদা
জিয়া বিরাট ব্যবধান জয়ী হয়ছন। তাঁর দল বি এন পি একাই নিরুঙ্কুশ সংখ্যাগরিষ্ঠ। 

জাট-সঙ্গীদর ফলাফলও
ভাল। অন্যদিক পাঁচবছর শাসন ক্ষমতায় থাকা আওয়ামি লিগর ভাগ্য মাত্র ৬২ আসন। আরও আশ্চর্যর
কথা: রাজধানী ঢাকার কানও আসনই হাসিনার দল জিতত পারনি। ৪১ জন মন্ত্রীর মধ্য ৩০ জন
হরছন, খাদ নত্রীকও রংপুর পরাজিত হত হয়ছ।  </p>

<p>ভাট একদল জিতব,
আর একদল হারব- এটাই বিধি। কিন্তু উদ্বগর বিষয় হল- শাচনীয় পরাজয়র পরও হাসিনা এক
জনসাধারণর রায় বল মানত নারাজ। তাঁর অভিযাগ: তদারকি সরকার ও নির্বাচন কমিশন
চক্রান্ত কর তাঁদর হারিয় দিয়ছ। নবগঠিত সংসদ আওয়ামির নির্বাচিত সদস্য অংশ নবন
কি না তাও পরিষ্কার নয়।  </p>

<p>হাসিনার আচরণ কানওভাবই
সমর্থনযাগ্য নয়। সংবিধান অনুযায়ী তদারকি সরকারর তত্ত্বাবধান ভাট হয়ছ। দুই লক্ষরও বশি
নিরপক্ষ পর্যবক্ষক নির্বাচনর দিন বাংলাদশ ছিলন। তাঁদর কউই বড়রকম অনিয়মর অভিযাগ
করননি। রাষ্ট্রবিজ্ঞানর প্রাথমিক পাঠ থাকল এটুকু সবাই বুঝবন য, রিগিং বা
জাল-জুয়াচুরি কর ২৯৯ আসনর মধ্য ২০৫টি আসন কানও জাট পত পার না। বি এন পি
প্রার্থীরা অধিকাংশ আসনই বিরাট ভাটর ব্যবধান হরছন। পরাজয় মন নওয়া সংসদীয়
গণতন্ত্র যকানও রাজনৈতিক দলর অবশ্য কর্তব্য। হাসিনার কর্তব্য: এই পরাজয় থক শিক্ষা নিয়
আগামী পাঁচবছর দলক সাংগঠনিকভাব পাক্ত কর তালা। ক্ষমতার কাছাকাছি থাকায় আওয়ামি নতা
ও কর্মীদর মধ্য তৈরি হওয়া দম্ভ ও ঔদ্ধত্য দূর করার দায়ও হাসিনার।  </p>

<p>পরাজয়র কারণ বিশ্লষণ
ও সঠিক আন্দালনর মাধ্যম নতুন সরকারর বিরুদ্ধ জনমত গড় তালাই এখন মুজিব-কন্যার
লক্ষ্য হওয়া উচিত। সংসদ বয়কটর নতিবাচক পথ বছ নিল হাসিনা ও তাঁর দল জনসাধারণর
কাছ থক আরও বিচ্ছিন্ন হয় পড়ব। </p>

<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p>দূষণ! কালা
হয় যাচ্ছ ভিক্টারিয়া মেমারিয়াল</p>

<p>কলকাতার গর্বর ভিক্টারিয়া
মমারিয়াল কি জৗলুষ হারাচ্ছ? আর পাঁচজন সাধারণ মানুষ খালি চাখ দখ বুঝত না
পারলও বিশষজ্ঞদর সতর্ক পর্যবক্ষণ জানাচ্ছ: সাদা মার্বল পাথর তৈরি বিশ্ববিখ্যাত এই
স্মৃতিসৗধ ক্রমশ কালা হয় যাচ্ছ। প্রাথমিক কারণ হিসব চিহ্ণিত করা হয়ছ ধুলা-বালি
ও গাড়ির ধাঁয়াক। কন্দ্রীয় দূষণ নিয়ন্ত্রণ পর্ষদর চয়ারম্যান দিলীপ বিশ্বাসর আশঙ্কা:
অবিলম্ব ব্যবস্হা না নিল ভিক্টারিয়ার অবস্হা হব তাজমহলর মতাই। এখনই মার্বলর গায়
কালা রঙর একটা সূক্ষ্ন আস্তরণ পড় গছ। ধীর ধীর তা আরও গাঢ় হব। </p>

<p>দূষণর প্রধান কারণ,
ধুলা-বালি ও যানবাহনর কালা ধাঁয়া। জওহরলাল নহরু রাড ফ্লাইওভারর নির্মাণকাজ
চলায় গাটা ময়দান এলাকাতই বায়ুদূষণ বড়ছ উল্লখযাগ্য হার। শীতকাল ভিক্টারিয়ার পাশর
মাঠই বস পরপর মলার আসর। এই মলাগুলিত ধুলা কীরকম হয়, বইমলার অভিজ্ঞতা থকই তা
অনুমান করা যত পার। তার ওপর রয়ছ যানবাহনর কার্বন মনাক্সাইড-যুক্ত কালা ধাঁয়া।
ভিক্টারিয়ার দক্ষিণ প্রবশদ্বারর সামন দিয় সকাল ৮টা থক রাত ৮টা পর্যন্ত ২৬ হাজার গাড়ি
নিয়মিত যাতায়াত কর। </p>

<p>উড়ালপুল হবই,
যানবাহনর গতি আটকানাও অসম্ভব। তাহল ভিক্টারিয়ার কী হব? কয়কটি উপায় বাতলছন
দিলীপবাবুই। </p>


 কাছাকাছি বাসস্টপ রাখা যাব না। 
 ভিক্টারিয়ার সামনর গাটা রাস্তা গ্রিন চ্যানল
     কর দিত হব। 
 পণ্যবাহী যান নিষিদ্ধ করা দরকার। 
 সামনর রাস্তা ও ফুটপাথ নিয়মিত পরিষ্কার করত
     হব। 
 ফুটপাতগুলির ওপর ধাতব আস্তরণ দওয়া দরকার।
     
 প্রচুর গাছ লাগাত হব। 


<p>
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>৫০ হাজার
কোটি টাকার গোল্ডন কোয়াড্রাঙ্গল প্রোজক্ট 
হাওড়ায়
আবার জমি হারাত চলছন ৩ হাজার মানুষ</p>

<p>বিনিদ্র রজনী যাপন করছন হাওড়া জলার উত্তর নিবড়ার ৩
হাজার মানুষ। 'গাল্ডন কায়াড্রাঙ্গল প্রাজক্ট'-এর জন্য প্রস্তাবিত জাতীয় সড়ক তৈরি হল
জমি-বাড়ি হারাবন তাঁরা। বিপদর আশঙ্কায় ২৫টি কুটির শিল্পর কারখানাও। </p>

<p>দ্বিতীয় হুগলি সতু থক প্রলম্বিত প্রশস্ত কানা এক্সপ্রস হাইওয়
রাজ্যর পরিবহন নতুন মাত্রা যাগ করলও মুম্বই রাড গিয় মশার পর সখান আর যথষ্ট
গতিত গাড়ি চালানা যায় না। অত্যধিক লরি-ট্রাকর ভিড় যানজট প্রতিদিনর এক বড় সমস্যা।
প্রায় দড়বছর ধর ঝুল থাকার পর কলকাতা-মুম্বই-দিল্লি-চন্নাই- এই চার মহানগরীর সংযাগকারী
সড়ক নির্মাণ উদ্যাগী হয়ছ ন্যাশনাল হাইওয় অথরিটি অফ ইন্ডিয়া (এন এইচ এ আই)। এর জন্য
খরচ হব প্রায় ৫০,০০০ কাটি টাকা। এই উপলক্ষ গত মঙ্গলবার হাওড়া জলাশাসকর অফিস প্রকল্প
রূপায়ণর জন্য এক আলাচনা সভায় উপস্হিত ছিলন হাওড়া জলাশাসক বিবক কুমার, এন এইচ এ
আই-এর প্রধান প্রকল্প অধিকর্তা এস পি বক্সি, হাওড়া জলা পরিষদর সভাধিনত্রী ছায়া সন চৗধুরি,
স্হানীয় বিধায়ক পদ্মনিধি ধর ও হাওড়া ইমপ্রুভমন্ট ট্রাস্টর আধিকারিকবৃন্দ। এই প্রকল্প
রূপায়ণ জমি অধিগ্রহণর দরুণ কানা হাইওয় সন্নিহিত প্রায় ৩০০০ সাধারণ মানুষ ক্ষতিগ্রস্ত হত
পারন বল সন্দহ প্রকাশ করা হয়। কারণ, এর সঙ্গই উত্তর নিবড়ায় দুটি ফ্লাইওভার নির্মাণরও
কাজ হব। আগ অন্যভাব পরিকল্পনার কথা থাকলও এখন নতুন পরিকল্পনা অনুযায়ী কানা থক
বালি বরাবর দুটি ফ্লাইওভার জনবহুল এলাকার ওপর দিয়ই যাব। এত ক্ষতিগ্রস্ত হব ২৫টি
কুটির শিল্পর কারখানা। তাছাড়াও নষ্ট হব কয়ক শত একর চাষযাগ্য জমি। নতুনভাব তৈরি করত
হব একটি উচ্চ বিদ্যালয়ও। সরাত হব এন টি পি সি-র দুটি বড় ট্রান্সমিশন টাওয়ারও।

</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>কুলতলিত
গ্রামবাসীরা পিটিয় মারল বাঘক 
পশ্চিমবঙ্গ
বাড়ছ রয়্যাল বেঙ্গল টাইগারর সংখ্যা</p>

<p>গত মঙ্গলবার দুপুর কুলতলির কিশারীমাহনপুর ক্ষুধার্ত
রয়্যাল বঙ্গলর আচমকা হানায় ছ'জন গ্রামবাসী সাংঘাতিক আহত হয় রায়দিঘি এবং
ডায়মণ্ডহারবার হাসপাতাল ভর্তি হয় রয়ছন। বাঘটিক ঘুমপাড়ানি বন্দুকর গুলি মর বন
ফিরিয় নিয় যাওয়ার আগই ক্ষিপ্ত গ্রামবাসীরা তাক হত্যা কর। বিরল প্রজাতির একটি রয়্যাল বঙ্গল
টাইগারর মৃতু্যত প্রচণ্ড ক্ষুব্ধ বনমন্ত্রী যাগশ বর্মন। </p>

<p>তবুও পশ্চিমবাংলার বন্যপ্রাণী সংরক্ষণ খুশির খবর ছড়িয় পড়ছ
সব মহলই। 'বিগ ক্যাট' পরিবারর এই প্রজাতিটিক নিয় গর্বর শষ নই বাংলার।
এবছর বাঘ-পালনর প্রকল্প সাফল্য এসছ। ১৯৭২ সাল রয়্যাল বঙ্গলর সংখ্যা ছিল মাত্র ৭৩টি।
এবছর তার সংখ্যা বড় হয়ছ ২৮৪টি। সইসঙ্গ অন্যান্য প্রজাতি মিলিয় বাঘর সংখ্যা মাট
দাঁড়িয়ছ ৩৬৫। এই মুহূর্ত বাংলার বনাঞ্চল চিতাবাঘর সংখ্যা ১০৭টি। জলদাপাড়া এবং
গরুমারা অভয়ারণ্য থাকা মাট গন্ডারর সংখ্যা ৭৪টি। উত্তরবঙ্গর পার্বত্য-পাদদশ এলাকায়
ভারতীয় বাইসনর সংখ্যা এখন ৫৫৩-এর ওপর। হাতির সংখ্যাও বড় দাঁড়িয়ছ ৩২৭টি। পশ্চিমবঙ্গ
সরকারর উদ্যাগ ১৫টি অভয়ারণ্য পরিবশর সঙ্গ মানানসই কর পর্যটন ব্যবস্হা গড় তালা হচ্ছ।
সুন্দরবন এবং বক্সাতও নতুন কর গড় উঠছ ব্যাঘ্র প্রকল্প। সরকার দাবি করছ, পরিবশ উন্নয়নর
জন্য গঠিত বিভিন্ন কমিটির ঐক্যবদ্ধ প্রয়াস বৃদ্ধি পয়ছ এই রাজ্যর সবুজ বনাঞ্চল। উল্লখ্য,
পশ্চিমবঙ্গ এখন ২৩ শতাংশ বনভূমি আছ, যা সারা ভারতর বনভূমির হিসব ২.৭ শতাংশ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>পাঁচদিন কাজর
সপ্তাহ, প্রতিবাদ মুখর ছাত্রছাত্রী-অধ্যাপকরা</p>

<p>পশ্চিমবাংলার সরকারি অফিসর অনুকরণ যাদবপুর
বিশ্ববিদ্যালয় পঠন-পাঠন ও কাজর জন্য প্রতি সপ্তাহ ৫ দিন বরাদ্দ করার নাটিশ জারি হয়ছ।
একযাগ এই সিদ্ধান্তর বিরুদ্ধ ক্ষাভপ্রকাশ কর প্রতিবাদ মুখর হয়ছন অধ্যাপক এবং
ছাত্রছাত্রীরা। </p>

<p>গত সপ্তাহ বিশ্ববিদ্যালয়র প্রশাসন বিভাগ এক বিজ্ঞপ্তি জারি কর
জানায়, বিশ্ববিদ্যালয়র কাজর দিন এবার থক প্রতি সপ্তাহ ৫ দিন নামিয় আনা হব।
কিন্তু সক্ষত্র প্রতিদিন আটটি ক্লাসর বদল নওয়া হব ৯টি ক্লাস। কাজর সময়ও প্রতিদিন
বাড়ব ২০ মিনিট কর। কিন্তু এই বিজ্ঞপ্তির ফল নতুন ব্যবস্হাক কার্যত নাকচ কর দিয়ছ
বিশ্ববিদ্যালয়র কলা, বিজ্ঞান এবং প্রযুক্তিবিদ্যা বিভাগর ছাত্রছাত্রী ও অধ্যাপকবৃন্দ। যাদবপুর
বিশ্ববিদ্যালয়র অধ্যাপক সমিতির সাধারণ সম্পাদক তরুণ নস্কর জানান, 'এ ধরনর বিজ্ঞপ্তি কবলমাত্র
সিপিএমর পরিপাষিত অশিক্ষক কর্মচারী এবং আধিকারিকদর খুশি করার জন্যই প্রকাশ করা হয়ছ।
এই সিদ্ধান্ত সর্বতাভাবই শিক্ষাবিরাধী। এত ক্ষতিগ্রস্ত হব সমস্ত বিভাগর প্রশিক্ষণ-পদ্ধতি। কাজর
দিন কমিয় শিক্ষাদানর ক্ষত্র ক্ষতি স্বীকার মন নওয়া যায় না।' এমন বিজ্ঞপ্তি ইউ জি সি-র নির্দশিকা
বিরাধী বলও অনক উল্লখ করন। সকলই একমত য, এত ক্ষতিগ্রস্ত হব গবষণার কাজও।
ইঞ্জিনিয়ারিং বিভাগও ক্ষাভ দানা বঁধছ। বর্তমান সকাল ১০টা ২০মিনিটি থক ৫-১৫
মিনিট পর্যন্ত যাদবপুর ক্লাস হয়। শিক্ষাদানর জন্য ইউ জি সি নূ্যনতম পক্ষ একবছর ১৮০ দিনর
লক্ষ্যমাত্রা পূরণর নির্দশ দিয় রখছ। সপ্তাহ ৪০ ঘণ্টা পড়াতই হব বলও নির্দশিকা আছ।
নতুন নিয়ম সসব বিঘ্নিত হব বল অসন্তাষ প্রকাশ করছন বিশ্ববিদ্যালয়র সব মহলই।
</p>

<p>এদিক, স্টট কাউন্সিল ফর এডুকশনাল রিসার্চ অ্যান্ড ট্রনিং-এর
অধিকর্তা রিফায়াতুল্লাহ জানিয়ছন, 'হিন্দু এবং হয়ার স্কুলর ছাত্ররা একযাগ শিক্ষকদর
বিরুদ্ধ অভিযাগ এন বলছ য, শিক্ষকরা ছাত্রদর পড়াবার ক্ষত্র ততটা মনাযাগী নয়, যতটা
আগ্রহী গল্প করার ক্ষত্র।' তিনি জানান এ ব্যাপার বিশদ তদন্ত হব। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>গুজরাট
নতুন মুখ্যমন্ত্রী নরন্দ্র মোদি</p>

<p>বিদ্রাহর ধ্বজা উড়িয়ও তা গুটিয় নিলন কশুভাই প্যাটল।
বিজপি নতৃত্ব গুজরাটর মুখ্যমন্ত্রীর পদ থক সরিয় দওয়ার পর ক্ষুব্ধ প্যাটল জানিয়ছিলন,
নরন্দ্র মাদিক মুখ্যমন্ত্রী করা হল দলর সঙ্গ সম্পর্ক চুকিয় দবন। ১৬ ঘণ্টার মধ্য ঢাক
গিল নিজর মুখই মাদিক উত্তরসূরি হিসব ঘাষণা করলন কশুভাই। বুধবার সকাল তিনি
জানান: মাদিই মুখ্যমন্ত্রী হচ্ছন। বৃহস্পতিবার বিজপি পরিষদীয় দল আনুষ্ঠানিকভাব মাদিক
নতা নির্বাচিত করব। </p>

<p>গুজরাট ভয়াবহ ভূমিকম্পর পর ত্রাণ বন্টন নিয় দুর্নীতির
অভিযাগর মুখ পড় কশুভাই সরকার। ত্রাণ সামগ্রীর অভাব না থাকলও অধিকাংশ ক্ষত্রই সগুলি
যাদর দরকার তাদর কাছ পৗঁছয়নি। তাছাড়া, সদ্যসমাপ্ত পুরনির্বাচন কংগ্রসর কাছ শাচনীয়ভাব
হরছ বিজপি। সব মিলিয়ই বিজপি হাইকম্যাণ্ডর কাপ পড়ন কশুভাই। প্রাথমিকভাব বিদ্রাহর
কথা ভাবলও শুভানুধ্যায়ীরা বুঝিয় সুঝিয় তাঁক নিরস্ত করন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>খালদার জয়
খুশি মুক্তিযাদ্ধা</p>

<p>এত কাছ, এবং নিজর মাতৃভাষাতই খবরর কাগজ পড়ত
পারার এত সুবিধা, তবু এত দূর! বাংলাদশ মুক্তিযাদ্ধা সংসদর ফুটবলারদর কাছ
ব্যাপারটা বশ অস্বস্তিকর। বাংলাদশর ভাটর খবর পড়ত হচ্ছ কলকাতার কাগজ। </p>

<p>বি এন পি য একক সংখ্যাগরিষ্ঠতা পাব সটা বাধহয় ওঁরা কউ
ভাবত পারননি। ভবছিলন খুব বশি হল কায়ালিশন সরকার হব। কিন্তু বগম খালদা
জিয়া এভাব জিতবন ভাবতই পারননি। মুক্তিযাদ্ধার অধিনায়ক হাসান আল মামুন বলই ফললন,
'আমরা ভাবতই পারিনি এভাব জয় আসব। আসল লাক কাজ চায়! এখানই বি এন পি
বাজিমাত কর গল। আমরা সবাই মন মন ওঁদরই সমর্থন করছিলাম।' কথাটা য স্রফ বলার
জন্যই বলা নয়, সটা ওঁদর দখলই বাঝা যায়। বারবার খাঁজ নিচ্ছিলন সর্বশষ ফলাফল
কী? দলর ম্যানজার আবু হাসান চৗধুরি প্রিন্স তা বি এন পি-র জয়র খবর পয় দলর
সবাইক মিষ্টি খাইয়ছন। জার গলায় বলছন, 'আমি বি এন পি-র সমর্থক!' খালদা
জিয়ার ব্যক্তিগত সচিব মাসাব্বক হাসান ফালুর ঘনিষ্ঠ প্রিন্স। জয়র খবর পয়ই ঢাকায় তাঁক ফান
শুভচ্ছাবার্তা পাঠিয়ছন। জানাচ্ছন, এরমধ্যই প্রায় ১০ হাজার টাকার আই এস ডি কর ফলছন।
আসল মাসাব্বক হাসান ঢাকা মহমডান স্পার্টিংয়রও সভাপতি। আবার প্রিন্সও সই ক্লাবর
সঙ্গই যুক্ত। মুক্তিযাদ্ধা কলকাতায় আসার সময় তাঁক লিয়ন নিয় এসছ। সবমিলিয়ই বি এন
পি-র জয় মুক্তিযাদ্ধা শিবির খুশির হাওয়া বইছ। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>ডোনাল্ড নেই
প্রথম ২টি ম্যাচ</p>

<p>ত্রিদশীয় একদিনর সিরিজর প্রথম দুটি ম্যাচর জন্য ১২ জনর
দল ঘাষণা করল দক্ষিণ আফ্রিকা। সই দল নই অ্যালান ডানাল্ড। দল আছন: শন পালক
(অধিনায়ক), গ্যারি কারস্টন, হারশল গিবস, জ্যাক কালিস, নিল মকঞ্জি, জন্টি রাডস,
ল্যান্স ক্লুসনার, মার্ক বাউচার, জাস্টিন কম্প, ক্লদ হন্ডারসডন, মাখায়া এনতিনি, আন্দ্র নল।
এই ১২ জনই ছিলন সাম্প্রতিক জিম্বাবায় সফর। </p>

<p>উল্লখযাগ্য বাতিলর তালিকায় রয়ছন নিকি বায়ও। ডানাল্ড
ও বায়- দুজনই ছিলন জিম্বাবায় সফর। নির্বাচকদর আহ্বায়ক রুশদি মাজিয় বলন,
ম্যাচ অনুশীলন কম থাকার জন্যই ওদর বাইর রাখা হল। এদিক, প্রথম কৃষ্ণাঙ্গ ক্রিকটার হিসব
মাখায়া এনতিনি ও ফুনকা এনজাম দক্ষিণ আফ্রিকার বর্ষসরা ক্রিকটারর সম্মান পয়ছন। ৫
বর্ষসরা ক্রিকটারর নাম গতরাত ঘাষণা কর দক্ষিণ আফ্রিকা বার্ড। বাকি তিনজন হলন: শন
পালক, নিকি বায় ও নিল মকঞ্জি। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>ডালমিয়াক
স্বাগত জানাল পাকবার্ড</p>

<p>বার্ড সভাপতি পদ জগমাহন ডালমিয়ার নির্বাচনক স্বাগত
জানাল পাকবার্ডও। এই মুহূর্ত দুই দশর মধ্য ক্রিকটীয় সম্পর্ক যথষ্ট তিক্ততা দখা দিয়ছ
বল মন করছন পাকবার্ডর প্রধান জনারল তৗকির জিয়া। 'কিন্তু জগমাহন
ডালমিয়ার প্রয়াস সটা দূর হব'- জানিয়ছন জিয়া। তিনি আরও বলছন, ডালমিয়া আই সি
সি-র সভাপতি থাকাকালীন তাঁদর সঙ্গ দারুণ সম্পর্ক রখছিলন। আর এখন তাঁরা এশিয়ান বার্ডর
প্রধান হয় একই দৃষ্টিভঙ্গি নিয় এগাচ্ছন। এই অবস্হায় ডালমিয়ার মত ব্যক্তির ভারতীয় বার্ড
থাকাটা খুবই প্রয়াজন। কারণ তাঁর মত অভিজ্ঞ ব্যক্তি খুব কমই আছন। 'ডালমিয়া জানন
কি কর কঠিন পরিস্হিতির মাকাবিলা করত হয়!' নবনির্বাচিত সভাপতির প্রশংসা করার
পাশাপাশি জিয়া প্রশংসা করছন বিদায়ী সভাপতি এ সি মুথাইয়ারও। পাকবার্ড প্রকাশিত এক প্রস
বিজ্ঞপ্তিত জনারল জিয়া জানিয়ছন, 'প্রাক্তন বার্ড সভাপতি মুথাইয়া দু'দশর মধ্য
ক্রিকটীয় সম্পর্কক চাঙ্গা করার চষ্টা চালিয় গিয়ছিলন। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>এখন
গোল করান, ঝামলা থামান চিমা!</p>

<p>ছলর আব্দার, 'বাবা ইংল্যান্ড যাওয়ার আগ একবার
কলকাতায় যত চাই। আমাক নিয় চল।' ওকামার কথাতই তাঁক নিয় শনিবার সকাল
কলকাতায় পৗঁছচ্ছন চিমা ওকরি। </p>

<p>শনিবার কলকাতায় তাঁর গাওয়া ক্যাসটটির উদ্বাধন হব।
চিমার বক্তব্য: 'কলকাতার জন্যই ক্যাসট তৈরি করছি। কলকাতা ছাড়া আর অন্য কাথাও উদ্বাধনর
কথা ভাবত পারি না। কলকাতাক এই ক্যাসটটি আমার পুজার উপহার।' এদিক, মুম্বইয় চিমা
বঙ্গল-মুম্বই ফুটবল ক্লাবর হয় তিনটি ম্যাচও খল ফলছন স্হানীয় সুপার ডিভিশন লিগ।
চিমা মুম্বইয় সম্পূর্ণ অন্য ভূমিকায়। এখন গাল করার চয় করাত বশি ভালবাসছন। মাঠ
ঝামলা হল দৗড় গিয় থামান। চিমা শান্তিরক্ষকর ভূমিকায়। দু'দিন আগ সন্ট্রাল রলর
বিরুদ্ধ ২-০ গাল বঙ্গল-মুম্বই ফুটবল ক্লাব জিতছ। দুটি বিশ্বমানর গালরই কারিগর কালা
চিতা। একটু নিচ থক খলছন। মূলত স্কিমারর ভূমিকায়। 
</p>

<p>
 
<gap desc="illustration"></gap></p>

<p> </p>

<p>ভুল
বোঝাবুঝি ভুলই যেত চান বিজন</p>

<p>কাচি এফ সি-র বিরুদ্ধ ৬ গাল জতার আনন্দ ছাপিয় ইস্টবঙ্গল
সাজঘর মত উঠল হ্যাটট্রিককারী নায়ক বিজন সিং-ক নিয়। কাচ, কর্মকর্তা, ফুটবলারদর
পাশাপাশি সমর্থকদর মুখ মুখ ঘুরল মণিপুরি ফুটবলারটির নাম। অথচ ক্লাবর নির্দশ না মন
ভারতীয় দলর হয় ইংল্যান্ড সফর গিয় নির্বাসিত হয়ছিলন বিজন। এটাই কি জবাব? একটা
সফল প্রত্যাবর্তন? তিনটি গালর মতই পশাদারি জবাব বিজনর, 'না না ওসব
জবাব-টবাব নয়। স্ট্রাইকারর যা কাজ তা-ই করছি। ক্লাবর সঙ্গ যা ভুল বাঝাবুঝি ঘটছিল, তা চুক্তিবদ্ধ হওয়ার
আগ। এখন মন রাখত চাই না। ক্লাবর হয় সরাটাই দিত চাই। আমি তৈরি।' বিজন
জানান, কলকাতায় এটা তাঁর প্রথম হ্যাটট্রিক। এখন ইস্টবঙ্গল দলর যা শক্তি, তাত এই
হ্যাটট্রিক প্রথম একাদশ থাকত সাহায্য করব কি? বিজনর প্রতিক্রিয়া: 'এই ধরনর
সুস্হ প্রতিযাগিতা থাকা ভাল, ভাল খলার জদ বাড়। কাচই শষ কথা- তিনি যভাব
ব্যবহার করবন, সভাবই খলব।' </p>

<p> </p>






</body></text></cesDoc>