<cesDoc id="urd-w-science-engineer-en02.uns" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-science-engineer-en02.uns.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-01</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>K.K.T.</h.title>
<h.author>P.H.U.</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1963</pubDate>
</imprint>
<idno type="CIIL code">en02.uns</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 62\   .</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-01</date></creation>
<langUsage>Urdu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>﻿<body>
<p>
<w pos="JDNU">6</w> <w pos="(">(</w> <w pos="JDNU">63</w> <w pos=")">)</w> 
<w pos="JDNU">1</w> <w pos="FX">6چوتھا</w> <w pos="NNUM1O NNUM1N">باب</w> <w pos="FX">6خلا</w> 
<w pos="II PPM1N">مےں</w> <w pos="NNUM1O">سفر</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="RR VVYM2N VVYM1O VVYM2O">لےے</w> 
<w pos="JJU">ضروری</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">شرائط</w> <w pos="RR">آج</w> <w pos="RJ">جب</w> 
<w pos="PPM2N PPM2O">ہم</w> <w pos="PY1N PY2N">ےہ</w> <w pos="VVTM2N">سنتے</w> <w pos="VHHV2 VHHM2">ہےں</w> 
<w pos="CS">کہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">روس</w> <w pos="CC CCC">ےا</w> <w pos="NNUM1O">امرےکہ</w> 
<w pos="II">نے</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">مصنوعی</w> <w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">سےارے</w> <w pos="NNMM1N JJM1N VVYM1N">خلا</w> 
<w pos="II PPM1N">مےں</w> <w pos="NNMM1O NNMM2N JJM2N VVYM2N VVST1 VVSV1 RRJ">چھوڑے</w> <w pos="VHHV2 VHHM2">ہےں</w> <w pos=",">،</w> 
<w pos="XT">تو</w> <w pos="PGM2M2N PGM2M2O PGM2M1O">ہمارے</w> <w pos="NNUM1N JJU NNUM1O">دل</w> <w pos="NNMM1O NNMM2N JJM2N VVYM2N VVST1 VVSV1 RRJ">سےارے</w> 
<w pos="VVNM1O">بنانے</w> <w pos="JXVM2O">والوں</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1O NNUF1O RR">عظمت</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">قائل</w> <w pos="VH0">ہو</w> <w pos="VXTM2N">جاتے</w> 
<w pos="VHHV2 VHHM2">ہےں</w> <w pos="CC">اور</w> <w pos="PPM2N PPM2O">ہم</w> <w pos="PV2O PY2O">ان</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">دماغی</w> <w pos="CC RD">اور</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">علمی</w> 
<w pos="NNUF2O">صلاحےتوں</w> <w pos="IIM1N">کا</w> <w pos="LL">فراخ</w> <w pos="NNMF1O NNMF1N">دلی</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">اعتراف</w> <w pos="VVTM2N">کرتے</w> <w pos="VHHV2 VHHM2">ہےں</w> 
<w pos=".">۔</w> <w pos="PY1N PY2N">ےہ</w> <w pos="JDNU">اےک</w> <w pos="VVTF1N VVTF1O VVTF2N VVTF2O">قدرتی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">امر</w> <w pos="VHHV1">ہے</w> <w pos=",">،</w> <w pos="CC">لےکن</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ں</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUM1O">شخص</w> <w pos="II">کو</w> 
<w pos="RM">بھی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">فراموش</w> <w pos="RMN">نہےں</w> <w pos="VVNM1N">کرنا</w> 
<w pos=".">۔</w> <w pos="VC1">چاہےے</w> <w pos="PJ1O">جس</w> <w pos="II">نے</w> 
<w pos="RR">آج</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JDNU">تےن</w> <w pos="JDNU CC RR">سو</w> 
<w pos="NNUM2O NNUM2N NNUM1N NNUM1O">سال</w> <w pos="RRJ JDNM1O JDNM2N JDNM2O">پہلے</w> <w pos="NNMM1N JJM1N VVYM1N">خلا</w> <w pos="II PPM1N">مےں</w> 
<w pos="NNUM1O">سفر</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">امکانات</w> <w pos="CC RD">اور</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1O NNUF1O RR">شرائط</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">وضاحت</w> <w pos="VX0 VV0 VVIT1">کر</w> <w pos="VVYF1N VXYF1N">دی</w> <w pos="VHPF1">تھی</w> 
<w pos=".">۔</w> <w pos="PV2N PV1N">وہ</w> <w pos="NNUM1N NNUM1O">شخص</w> <w pos="NNUM1O">انگلستان</w> 
<w pos="IIM1N">کا</w> <w pos="JJU">مشہور</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">سائنس</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">دان</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">نےوٹن</w> <w pos="VHPM1">تھا</w> <w pos=".">۔</w> <w pos="PY1O PV1O">اس</w> 
<w pos="II">نے</w> <w pos="PGRF1N PGRF1O PGRF2O PGRF2N">اپنی</w> <w pos="JDNU">اےک</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تصنےف</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">رےاضےات</w> <w pos="NNUM2O">قوانےن</w> <w pos="II PPM1N">مےں</w> <w pos="JJF1N JJF1O JJF2N JJF2O">بڑی</w> 
<w pos="NNMF1N NNMF1O JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">صفائی</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="PV2O PY2O">ان</w> <w pos="JJU NNUM1O NNUF1O RR">شرائط</w> 
<w pos="IIM1N">کا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ذکر</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VHPM1">تھا</w> 
<w pos=",">،</w> <w pos="PJ2O">جن</w> <w pos="IIM1O">کے</w> <w pos="RR">مطابق</w> 
<w pos="PNO">کسی</w> <w pos="NNUF1O">چےز</w> <w pos="II">کو</w> <w pos="JDNU">اےک</w> 
<w pos="NNMM1N">مقررہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مدار</w> <w pos="II PPM1N">مےں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">قائم</w> 
<w pos="PK1N VVYM1N QQ PK2N">کےا</w> <w pos="PJ1O">جس</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VV0 VX0 VVIT1 VXIT1">جا</w> 
<w pos="VXTM1N VVTM1N">سکتا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> <w pos="PY1O PV1O">اس</w> 
<w pos="II">نے</w> <w pos="JDNU">2</w> <w pos="JDNU">1</w> <w pos=".">.</w> 
<w pos="FX">Principia</w> <w pos="FX">Mathematica</w> <w pos=".">.</w> <w pos="JDNU">6</w> 
<w pos="(">(</w> <w pos="JDNU">63</w> <w pos=")">)</w> <w pos="JDNU">2</w> 
<w pos="PY1O PV1O">اس</w> <w pos="NNMM1N">سلسلہ</w> <w pos="II PPM1N">مےں</w> <w pos="JJU NNUM1O NNUF1O RR">راکٹ</w> 
<w pos="IIM1O">کے</w> <w pos="NNMM1N">ذرےعہ</w> <w pos="NNMM1N JJM1N VVYM1N">دھکا</w> <w pos="VVNM1O">لگانے</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">امکان</w> <w pos="CC RD">اور</w> <w pos="PY1O PV1O">اس</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1O NNUF1O RR">اصول</w> <w pos="IIM1N">کا</w> <w pos="RM">بھی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ذکر</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIM1O">کے</w> <w pos="RR">علاوہ</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">دوری</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">حرکت</w> <w pos="NNUM1N NNUM1O">پےدا</w> <w pos="VVNM1O">کرنے</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="RR VVYM2N VVYM1O VVYM2O">لےے</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">حسالی</w> <w pos="NNMM2O NNUM2O NNUF2O">مقداروں</w> <w pos="IIM1N">کا</w> 
<w pos="JJF1O">بڑی</w> <w pos="NNUF1O">صحت</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1N RR NNUM1O">ساتھ</w> 
<w pos="JJU NNUM1N NNUM1O">اظہار</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
<w pos="PY1O PV1O">اس</w> <w pos="II">نے</w> <w pos="PY1O PV1O">اس</w> <w pos="II CC">پر</w> 
<w pos="XH">ہی</w> <w pos="NNMM1N JJM1N VVYM1N">اکتفا</w> <w pos="RMN">نہےں</w> <w pos="PK1N VVYM1N PK2N">کےا</w> 
<w pos="VHHV1">ہے</w> <w pos=",">،</w> <w pos="CS">بلکہ</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">دوری</w> 
<w pos="JJU NNUM1O NNUF1O RR">حرکت</w> <w pos="IIM1O">کے</w> <w pos="NNMM1N">سلسلہ</w> <w pos="II PPM1N">مےں</w> 
<w pos="NNUF1O">ہوا</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1O NNUF1O RR">مزاحمت</w> <w pos="IIM1N">کا</w> 
<w pos="RM">بھی</w> <w pos="NNMM1N">جائزہ</w> <w pos="VVYM1N">لےا</w> <w pos="VHHV1">ہے</w> 
<w pos="CC">اور</w> <w pos="RY">ےہاں</w> <w pos="II">تک</w> <w pos="VV0">لکھ</w> 
<w pos="VXYM1N">دےا</w> <w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> <w pos="CS">اگر</w> 
<w pos="PNN">کوئی</w> <w pos="NNUM1O NNUM2N NNUM1N">جسم</w> <w pos="NNUF1O">زمےن</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">گرد</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">گردش</w> <w pos="VVTM2N">کرتے</w> <w pos="VHYM2N VHYM2O">ہوئے</w> 
<w pos="PNO">کسی</w> <w pos="JJM2N JJM1O VVSV1 JJM2O">بڑے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ابھار</w> <w pos="(">(</w> 
<w pos="NNMF1O NNMF1N">پہاڑی</w> <w pos="NNMM1N">سلسلہ</w> <w pos="RR">وغےرہ</w> <w pos=")">)</w> 
<w pos="II">پر</w> <w pos="II">سے</w> <w pos="VVST1 VVSV1">گزرے</w> <w pos="VGM1">گا</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">توکشش</w> <w pos="JJU NNUM1O NNUF1O RR">ثقل</w> <w pos="IIF1O">کی</w> <w pos="NNMF1O">تبدےلی</w> 
<w pos="IIM1O">کے</w> <w pos="NNUM1O">باعث</w> <w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مدار</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ہٹ</w> <w pos="VVST1 VVSV1 VXST1 VXSV1">جائے</w> <w pos="VGM1">گا</w> 
<w pos=".">۔</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">غرض</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">مصنوعی</w> <w pos="NNMM2O NNUM2O NNUF2O VVSM1">سےاروں</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="RR">متعلق</w> <w pos="PY1O PV1O">اس</w> <w pos="II">نے</w> 
<w pos="JJU RR">قرےب</w> <w pos="JJU RR">قرےب</w> <w pos="PNN JD">سب</w> <w pos="JJU">ضروری</w> 
<w pos="NNUF2N">باتےں</w> <w pos="NNUM1O NNUM1N">بےان</w> <w pos="VV0 VVIT1">کر</w> <w pos="VXYF2N VVYF2N">دی</w> 
<w pos="VHPF2">تھےں</w> <w pos=".">۔</w> <w pos="RM">صرف</w> <w pos="PY1O PV1O">اس</w> 
<w pos="NNUF1N NNUF1O">قدر</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="JJU">باقی</w> <w pos="VHPM1">تھا</w> 
<w pos="CS">کہ</w> <w pos="NNMM1N JJM1N VVYM1N">عملاََ</w> <w pos="JDNU">اےک</w> <w pos="NNMM1N">سےارہ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مدار</w> <w pos="II PPM1N">مےں</w> <w pos="VV0">چھوڑ</w> <w pos="VXYM1N">دےا</w> 
<w pos="VXTM1N VVTM1N VXYM1N">جاتا</w> <w pos=".">۔</w> <w pos="RY">اب</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> 
<w pos="JDNU">تےن</w> <w pos="JDNU CC RR">سو</w> <w pos="NNUM2O NNUM2N NNUM1N NNUM1O">سال</w> <w pos="RRJ JDNM1O JDNM2N JDNM2O">پہلے</w> 
<w pos="PV2O PY2O">ان</w> <w pos="JD">تمام</w> <w pos="NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O">امور</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O VVYF1O VVYF2O">کی</w> 
<w pos="NNUM1N RR NNUM1O">پےش</w> <w pos="VVNF1 VVNF2">بےنی</w> <w pos="VVNM1N">کرنا</w> <w pos="JJU">ظاہر</w> 
<w pos="VVTM1N">کرتا</w> <w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> <w pos="JJU NNUM1O NNUF1O RR">نےوٹن</w> 
<w pos="II">کو</w> <w pos="RM">بھی</w> <w pos="JJU NNUM1O NNUF1O RR">قدرت</w> <w pos="II">نے</w> 
<w pos="RD JD">نہاےت</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">اعلٰی</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">دماغی</w> <w pos="NNUF2O">صلاحےتوں</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNMM1N JJM1N VVYM1N">نوازا</w> <w pos="VHPM1">تھا</w> <w pos=".">۔</w> 
<w pos="FX">0نےوٹن</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کلےات</w> <w pos="PPM2N PPM2O">ہم</w> 
<w pos="II PPM1N">مےں</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1O NNUF1O RR">اکثر</w> <w pos="II">نے</w> 
<w pos="PY1N PY2N">ےہ</w> <w pos="NNMM1N">مشاہدہ</w> <w pos="PK1N VVYM1N QQ PK2N">کےا</w> <w pos="NNMM1N JJM1N VVYM1N">ہوگا</w> 
<w pos="CS">کہ</w> <w pos="RJ">جب</w> <w pos="PNO">کسی</w> <w pos="NNMF1O">گاڑی</w> 
<w pos="II">کو</w> <w pos="NNMM1N JJM1N VVYM1N">دھکا</w> <w pos="VVNM1N">لگانا</w> <w pos="NNUM1N NNUM1O">مقصود</w> 
<w pos="VH0 VHSV1 VHHT2 VHST2 VHST1 AU VHIT1 VHIT2">ہو</w> <w pos=",">،</w> <w pos="XT">تو</w> <w pos="SPXYM">ہمےں</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ابتدائََ</w> <w pos="JJF1N JJF1O JJF2N JJF2O">بڑی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">قوت</w> <w pos="RM">صرف</w> 
<w pos="VVNF1 VVNF2">کرنی</w> <w pos="VVTF1N">پڑتی</w> <w pos="VHHV1 VHHT1">ہے</w> <w pos=".">۔</w> 
<w pos="CC">لےکن</w> <w pos="RJ">جب</w> <w pos="PV2N PV1N">وہ</w> <w pos="JDNU">اےک</w> 
<w pos="NNUF1N NNUF2N NNUF1O">دفعہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">حرکت</w> <w pos="II PPM1N">مےں</w> <w pos="NNUF1N NNUF1O NNUF1V">آجائے</w> 
<w pos=",">،</w> <w pos="XT">تو</w> <w pos="RM">پھر</w> <w pos="JJF1N JJF1O JJF2N JJF2O">تھوڑی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">طاقت</w> <w pos="RM">صرف</w> <w pos="JDNU">6</w> <w pos="(">(</w> 
<w pos="JDNU">64</w> <w pos=")">)</w> <w pos="JDNU">3</w> <w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">کرکے</w> 
<w pos="RM">بھی</w> <w pos="PPM2N PPM2O">ہم</w> <w pos="PY1O PV1O">اس</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="JJU NNUM1O NNUF1O RR">حرکت</w> <w pos="II">کو</w> <w pos="JJU">جاری</w> <w pos="VV0 VVIT1">رکھ</w> 
<w pos="VXTM2N VVTM2N">سکتے</w> <w pos="VHHV2 VHHM2">ہےں</w> <w pos=".">۔</w> <w pos="PNN PNO">کچھ</w> 
<w pos="RR NNUM1N NNUM2N NNUM1O">دور</w> <w pos="VVNM1O">چلنے</w> <w pos="IIM1O">کے</w> <w pos="RR">بعد</w> 
<w pos="CS">اگر</w> <w pos="PY1O PV1O">اس</w> <w pos="II">کو</w> <w pos="VVNM1N">روکنا</w> 
<w pos="NNUM1N NNUM1O">مقصود</w> </p>
<p>
<w pos="JDNU">0</w> </p>
</body>
</text>
</cesDoc>
