<cesDoc id="urd-w-media-mm07.uol" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-media-mm07.uol.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-07-02</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Ishtahariyat</h.title>
<h.author>Dilshad</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1991</pubDate>
</imprint>
<idno type="CIIL code">mm07.uol</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 148/.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-07-02</date></creation>
<langUsage>Urdu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>﻿<body>
<p>
<w pos="JDNU">60</w> <w pos="(">(</w> <w pos="JDNU">148</w> <w pos=")">)</w> 
<w pos="JDNU">1</w> </p>
<p>
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="RR VVYM2N VVYM1O VVYM2O">لےے</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VV0">کر</w> 
<w pos="VRM1">رہا</w> <w pos="VHHV1">ہے</w> <w pos="XT">تو</w> <w pos="PY1O PV1O">اس</w> 
<w pos="IIM1N">کا</w> <w pos="RM">بھی</w> <w pos="PY1O PV1O">اس</w> <w pos="NNMM1O">ادارے</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ملازمےن</w> <w pos="II CC">پر</w> <w pos="RD JD">بہت</w> 
<w pos="JJM1N">اچھا</w> <w pos="NNUM1N">اثر</w> <w pos="VHTM1N">ہوتا</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUF1N NNUM1O NNUM1N NNUF1O">طرح</w> <w pos="NNMM1N">ادارہ</w> 
<w pos="PGRM2O">اپنے</w> <w pos="NNMM2O NNUM2O">ملازموں</w> <w pos="IIF2N">کی</w> <w pos="NNMF2N">ہمدردےاں</w> 
<w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">بھرپور</w> <w pos="NNUM1N NNUM1O">تعاون</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> 
<w pos="VVNM1O">کرنے</w> <w pos="II PPM1N">مےں</w> <w pos="JJU">کامےاب</w> <w pos="VVTM1N">ہوجاتا</w> 
<w pos="VHHV1">ہے</w> <w pos="PJ2N PJ1N">جو</w> <w pos="CS">کہ</w> <w pos="NNMM1O">ادارے</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="RR NNUM1O">حال</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">اثاثہ</w> 
<w pos="CC RD">اور</w> <w pos="JJM2N RRJ JJM1O JJM2O">اچھے</w> <w pos="NNUM1O">مستقبل</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ضمانت</w> <w pos="VHTM1N">ہوتا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
</p>
<p>
<w pos="JDNU">0</w> <w pos="(">(</w> <w pos="JDNU">4</w> <w pos=")">)</w> 
<w pos="JJM2N RRJ JJM1O JJM2O">اچھے</w> <w pos="NNUM2O">کارکنوں</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اصول</w> 
<w pos=":">:</w> </p>
<p>
<w pos="JJU NNUM1O NNUF1O RR">تشہےر</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1O NNUF1N">وجہ</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> 
<w pos="NNMM2O NNUM2O NNUF2O">اداروں</w> <w pos="II">کو</w> <w pos="JJM2N RRJ JJM1O JJM2O">اچھے</w> <w pos="NNUM1O NNUM1N">کارکن</w> 
<w pos="CC RD">اور</w> <w pos="JJM2N RRJ JJM1O JJM2O">اچھے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">منظم</w> <w pos="VV0 NNUM1N NNUM1O">مل</w> 
<w pos="VVTM2N VXTM2N">جاتے</w> <w pos="VHHV2 VHHM2">ہےں</w> <w pos=".">۔</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> 
<w pos="VVNM1O">کرنے</w> <w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="CC RD">اور</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">کاروباری</w> 
<w pos="NNUM2N">لوگ</w> <w pos="NNMM1N JJM1N VVYM1N">عموماََ</w> <w pos="JJU">مشہور</w> <w pos="CC RD">اور</w> 
<w pos="VVNM1O VVSV1 VVYM2N VVNM2">جانے</w> <w pos="VVNM1O VVNM2">پہچانے</w> <w pos="NNMM2O NNUM2O NNUF2O">اداروں</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="NNUM1N RR NNUM1O">ساتھ</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VVNM1N">کرنا</w> <w pos="JJU NNUF1N NNUF1O">پسند</w> 
<w pos="VVTM2N">کرتے</w> <w pos="VHHV2 VHHM2">ہےں</w> <w pos=".">۔</w> <w pos="PY1N PY2N">ےہ</w> 
<w pos="VVTF1N VVTF1O VVTF2N VVTF2O">قدرتی</w> <w pos="NNUF1O NNUF1N">چےز</w> <w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> 
<w pos="JDYM1O RYJ JDYM2N JDYM2O">اےسے</w> <w pos="NNUM2N">لوگ</w> <w pos="RJ">جب</w> <w pos="RD JD RR">زےادہ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">تشہےر</w> <w pos="VVNM1O">کرنے</w> <w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="NNMM2O NNUM2O NNUF2O VVSM1">اداروں</w> 
<w pos="II PPM1N">مےں</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VVSM2 VVSV2">کرےں</w> <w pos="VGM2">گے</w> 
<w pos="XT">تو</w> <w pos="PV2O PY2O">ان</w> <w pos="IIC XHC">ھےں</w> <w pos="PY1O PV1O">اس</w> 
<w pos="IIM1O">کے</w> <w pos="NNUM1O">اشتہار</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دےکھ</w> <w pos="VX0 VV0 VVIT1">کر</w> 
<w pos="NNUM1N NNUM1O">فخر</w> <w pos="CC RD">اور</w> <w pos="NNMF1N NNMF1O">خوشی</w> <w pos="NNUM1N JJU NNUM1O">محسوس</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ہوگی</w> <w pos=".">۔</w> <w pos="RM">پھر</w> <w pos="PV2O PY2O">ان</w> 
<w pos="IIC XHC">ھےں</w> <w pos="PY1N PY2N">ےہ</w> <w pos="RM">بھی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اطمےنان</w> 
<w pos="NNMM1N JJM1N VVYM1N">ہوگا</w> <w pos="CS">کہ</w> <w pos="JDYM1O RYJ JDYM2N JDYM2O">اےسے</w> <w pos="NNMM1O">ادارے</w> 
<w pos="II PPM1N">مےں</w> <w pos="PJ2N PJ1N">جو</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">تشہےر</w> <w pos="VVTM1N">کرتا</w> 
<w pos="VHHV1">ہے</w> <w pos=",">،</w> <w pos="PV2O PY2O">ان</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ملازمت</w> <w pos="VVTM1N">نسبتاََ</w> <w pos="NNUM1O NNUM1N">مستقبل</w> <w pos="CC RD">اور</w> 
<w pos="PV2O PY2O">ان</w> <w pos="IIF1O">کی</w> <w pos="NNMF1O">ترقی</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">امکانات</w> <w pos="RD JD RR">زےادہ</w> <w pos="VHTM2N">ہوتے</w> <w pos="VHHV2 VHHM2">ہےں</w> 
<w pos=".">۔</w> </p>
<p>
<w pos="FX">0جنگ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">دوران</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تشہےر</w> 
<w pos=":">:</w> </p>
<p>
<w pos="JDNF1O JDNF1N JDNF2O JDNF2N">دوسری</w> <w pos="LL">جنگ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">عظےم</w> <w pos="CC RD">اور</w> 
<w pos="PY1O PV1O">اس</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1O NNUF1O RR">قبل</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="JJU">عالمی</w> <w pos="JJU NNUM1O NNUF1O RR">چپقلش</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">تارےخ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">مرتب</w> <w pos="VVNM1O">کرنے</w> <w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="JJU NNUM1O NNUF1O RR">مورخےن</w> 
<w pos="II">کو</w> <w pos="JJU">مختلف</w> <w pos="NNMF1N NNMF1O JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">دفاعی</w> <w pos="NNMM2O NNUM2O NNUF2O VVSM1">منصوبوں</w> 
<w pos="CC RD">اور</w> <w pos="JDNF1O JDNF1N JDNF2O JDNF2N">دوسری</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">حب</w> <w pos="NNUM1O NNUM1N">وطن</w> 
<w pos="NNMF2O">سرگرمےوں</w> <w pos="IIM1O">کے</w> <w pos="NNMM1O">سلسلے</w> <w pos="II PPM1N">مےں</w> 
<w pos="JJU NNUM1O NNUF1O RR">تشہےر</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU">اہم</w> <w pos="NNUM1O">کردار</w> 
<w pos="II">کو</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">فراموش</w> <w pos="RMN">نہےں</w> <w pos="VVNM1N">کرنا</w> 
<w pos="VC1">چاہےے</w> <w pos=".">۔</w> <w pos="LL">جنگ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">دوران</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">امرےکی</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">کاروباری</w> <w pos="NNMM2O NNUM2O NNUF2O">اداروں</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1O NNUF1O RR">تشہےر</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNMM1O RR">ذرےعے</w> 
<w pos="NNUM1O NNUM1N">رےڈےو</w> <w pos="CC RD">اور</w> <w pos="JDNM2O">دوسرے</w> <w pos="NNMM2O NNUM2O">اداروں</w> 
<w pos="II">کو</w> <w pos="JDNU">دس</w> <w pos="JDNU">کروڑ</w> <w pos="NNUM1O NNUM1N">ڈالر</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="RD JD RR">زےادہ</w> <w pos="VVNF1 VVNF2">آمدنی</w> <w pos="VHYF1N VHYF2N VHYF1O VHYF2O">ہوئی</w> 
<w pos="PY1O PV1O">اس</w> <w pos="NNUM1N NNUM1O">وقت</w> <w pos="NNUM1O NNUM1N">امرےکہ</w> <w pos="II PPM1N">مےں</w> 
<w pos="RR">تقرےباََ</w> <w pos="JDNU">اےک</w> <w pos="JDNU CC RR">سو</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">تشہےری</w> 
<w pos="NNUF2N VVSM2 VVSV2 VVYF2N">مہمےں</w> <w pos="NNUM1N JJU NNUM1O">شروع</w> <w pos="VHYF2N">ہوئےں</w> <w pos="PJ2O">جن</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JDNU">27</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">سرکاری</w> <w pos="NNMM2O NNUM2O NNUF2O">اداروں</w> 
<w pos="IIM1O">کے</w> <w pos="NNUM1O">تعاون</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUM1N JJU NNUM1O">شروع</w> 
<w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> <w pos="VVYF2N">گئےں</w> <w pos=".">۔</w> <w pos="PY1N PY2N">ےہ</w> 
<w pos="NNUF2N VVSM2 VVSV2 VVYF2N">مہمےں</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">جنگی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">بونڈ</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">فروخت</w> 
<w pos="VVNM1O VVNM2">کرنے</w> <w pos=",">،</w> <w pos="NNUM2O NNUF1O">عوام</w> <w pos="II">کو</w> 
<w pos="JJU">کم</w> <w pos="NNUM1O NNUM1N">راشن</w> <w pos="NNUM1N NNUM1O">استعمال</w> <w pos="VVNM1O">کرنے</w> 
<w pos="II CC">پر</w> <w pos="JJU">تےار</w> <w pos="VVNM1O VVNM2">کرنے</w> <w pos="CC RD">اور</w> 
<w pos="NNMM2O NNUM2O NNUF2O VVSM1">دھاتوں</w> <w pos=",">،</w> <w pos="NNUM1N NNUM1O">کاغذ</w> <w pos="CC RD">اور</w> 
<w pos="JDNF1O JDNF1N JDNF2O JDNF2N">دوسری</w> <w pos="JJU">ضروری</w> <w pos="NNMF1O VVYM1N">اشےا</w> <w pos="IIF1N IIF1O">کی</w> 
<w pos="NNUF1O NNUF1N">بچت</w> <w pos="VVNM1O VVNM2">کرنے</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1O NNUF1O RR">دشمن</w> 
<w pos="II">تک</w> <w pos="JJU">اہم</w> <w pos="NNUF2N NNUF2O NNUM2N NNUF1N NNUF1O">معلومات</w> <w pos="RMN JDNU">نہ</w> 
<w pos="VVNM1O">پہنچانے</w> <w pos="IIM1O">کے</w> <w pos="RR">متعلق</w> <w pos="VHPF2">تھےں</w> 
<w pos=".">۔</w> </p>
<p>
<w pos="JDNF1O JDNF1N JDNF2O JDNF2N">دوسری</w> <w pos="LL">جنگ</w> <w pos="JJU NNUM1O NNUF1O RR">عظےم</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">دوران</w> <w pos="RD JD">بہت</w> <w pos="JXSF2O">سی</w> <w pos="NNMF2O">کمپنےوں</w> 
<w pos="II">نے</w> <w pos="JJU">معمول</w> <w pos="IIM1O">کے</w> <w pos="RR">مطابق</w> 
<w pos="PGRF1N PGRF1O PGRF2O PGRF2N">اپنی</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">تشہےری</w> <w pos="NNUF2N VVSM2 VVSV2 VVYF2N">مہمےں</w> <w pos="JJU">جاری</w> 
<w pos="VVYF2N VVSV2 VVSM2">رکھےں</w> <w pos="NNMM1N">حالانکہ</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUM1N NNUM1O">وقت</w> 
<w pos="PV2O PY2O">ان</w> <w pos="IIM1O">کے</w> <w pos="RR">پاس</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">فروخت</w> 
<w pos="VVNM1O">کرنے</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="RR VVYM2N VVYM1O VVYM2O">لےے</w> <w pos="PNN PNO">کچھ</w> 
<w pos="RM">بھی</w> <w pos="RMN">نہےں</w> <w pos="VHPM1">تھا</w> <w pos=".">۔</w> 
<w pos="PY2E PV2E">انھوں</w> <w pos="II">نے</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUM1N NNUM1O">وقت</w> 
<w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اشتہارات</w> <w pos="JDNU">60</w> <w pos="(">(</w> 
<w pos="JDNU">154</w> <w pos=")">)</w> <w pos="JDNU">7</w> </p>
<p>
<w pos="II">کو</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">حقےقی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">تسکےن</w> <w pos="VVST1 VVSV1">بخشے</w> 
<w pos="VGF2 VGF1">گی</w> <w pos=".">۔</w> <w pos="VVNM1O">سننے</w> <w pos="JXVM2O">والوں</w> 
<w pos="II PPM1N">مےں</w> <w pos="NNUM1O NNUM1N">رےڈےو</w> <w pos="II CC">پر</w> <w pos="NNMF1O">موسےقی</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1O NNUF1O RR">اسباق</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">سلسلہ</w> 
<w pos="NNUM1N JJU NNUM1O">شروع</w> <w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">کرکے</w> <w pos="NNMF1O">موسےقی</w> <w pos="IIF2O">کی</w> 
<w pos="NNUF2O">کلاسوں</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNMM1O RR">ذرےعے</w> <w pos="NNMF1O">موسےقی</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سوجھ</w> <w pos="CC RD">اور</w> <w pos="NNUM1N NNUM1O">ذوق</w> 
<w pos="NNUM1N NNUM1O">پےدا</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VXTM1N VVTM1N VXYM1N">جاتا</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUF1N NNUM1O NNUM1N NNUF1O">طرح</w> <w pos="NNMF1O">موسےقی</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1O NNUF1O RR">خصوصےات</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU">مکمل</w> 
<w pos="NNUM1O">علم</w> <w pos="IIM1O">کے</w> <w pos="RR">بعد</w> <w pos="NNUM2O">لوگوں</w> 
<w pos="IIF1N IIF1O">کی</w> <w pos="NNMF1N NNMF1O">موسےقی</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="RD JD RR">زےادہ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تسکےن</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> <w pos="VVTF1N">جاسکتی</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> <w pos="PY1O JDNU SPHY">اسی</w> <w pos="NNUF1N NNUM1O NNUM1N NNUF1O">طرح</w> <w pos="JJU NNUM1O NNUF1O RR">تشہےر</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNMM1O RR">ذرےعے</w> <w pos="NNMF1N NNMF1O NNMF1V VVYM1N">اشےا</w> <w pos="II PPM1N">مےں</w> 
<w pos="JJU">موجود</w> <w pos="JJU NNUM1O NNUF1O RR">خصوصےات</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">نشاندہی</w> 
<w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">کرکے</w> <w pos="PV2O PY2O">ان</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تسکےن</w> 
<w pos="II PPM1N">مےں</w> <w pos="NNMM1N">اضافہ</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VVTM1N">جاسکتا</w> 
<w pos="VHHV1">ہے</w> <w pos=".">۔</w> </p>
<p>
<w pos="PY1O JDNU SPHY">اسی</w> <w pos="NNUF1N NNUM1O NNUM1N NNUF1O">طرح</w> <w pos="JDNU">اےک</w> <w pos="JDYF1N JDYF2N JDYF1O JDYF2O">اےسی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">معےشت</w> <w pos="II PPM1N">مےں</w> <w pos="RJ NNUF1O NNUF1N">جہاں</w> <w pos="JJF1N JJF1O JJF2N JJF2O">نئی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">مصنوعات</w> <w pos="CC RD">اور</w> <w pos="NNMM1N">پوشےدہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">خصوصےات</w> 
<w pos="JXVF1O JXVF1N JXVF2N JXVF2O">والی</w> <w pos="JJU NNUM1O NNUF1O RR">مصنوعات</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">کثرت</w> 
<w pos="VHHV1">ہے</w> <w pos=",">،</w> <w pos="RV">وہاں</w> <w pos="PY1N PY2N">ےہ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مصنوعات</w> <w pos="NNUM2O NNUF1O NNUF1N">عوام</w> <w pos="II PPM1N">مےں</w> <w pos="PY1O JDNU SPHY">اسی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">وقٹ</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">مقبول</w> <w pos="VVTF2N">ہوسکتی</w> <w pos="VHHV2 VHHM2">ہےں</w> 
<w pos="CS">اگر</w> <w pos="PV2O PY2O">ان</w> <w pos="IIC XHC">ھےں</w> <w pos="PV2O PY2O">ان</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1O NNUF1O RR">خواہشات</w> <w pos="II">کو</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">تسکےن</w> 
<w pos="VVNM1O">بخشنے</w> <w pos="JXVF1O JXVF1N JXVF2N JXVF2O">والی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">خصوصےات</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> 
<w pos="NNUM1N NNUM1O">آگاہ</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VVST1 VVSV1 VXST1 VXSV1">جائے</w> <w pos=".">۔</w> 
<w pos="PV2N PV1N">وہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مصنوعات</w> <w pos="PJ2N PJ1N">جو</w> <w pos="NNUM2O NNUF1O">عوام</w> 
<w pos="II">کو</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUF1N NNUF1O">صورت</w> <w pos="II PPM1N">مےں</w> 
<w pos="NNUM1N RR NNUM1O">پےش</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> <w pos="VVSV2 VVSM2">جائےں</w> <w pos="CS">کہ</w> 
<w pos="PV2O PY2O">ان</w> <w pos="IIC XHC">ھےں</w> <w pos="PV2O PY2O">ان</w> <w pos="JJU NNUM1O NNUF1O RR">خواہشات</w> 
<w pos="II">کو</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">تسکےن</w> <w pos="VVNM1O">بخشنے</w> <w pos="JXVF1O JXVF1N JXVF2N JXVF2O">والی</w> 
<w pos="JJU NNUM1O NNUF1O RR">خصوصےات</w> <w pos="IIM1N">کا</w> <w pos="NNUM1N">علم</w> <w pos="RMN JDNU">نہ</w> 
<w pos="VH0 VHSV1 VHHT2 VHST2 VHST1 AU VHIT1 VHIT2">ہو</w> <w pos="XT">تو</w> <w pos="NNUM2O NNUF1O NNUF1N">عوام</w> <w pos="II PPM1N">مےں</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مقبول</w> <w pos="RMN">نہےں</w> <w pos="VVTF2N">ہوسکتےں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تاہم</w> 
<w pos="JJU NNUM1O NNUF1O RR">تشہےر</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNMM1O RR">ذرےعے</w> <w pos="NNUM2O NNUF1O">عوام</w> 
<w pos="II">کو</w> <w pos="NNUF1N NNUF1O NNUF1V">اشےائے</w> <w pos="JJU NNUM1O NNUF1O RR">فروخت</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">خصوصےات</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUM1N NNUM1O">آگاہ</w> <w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">کرکے</w> 
<w pos="NNMF1O VVYM1N">اشےا</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O NNUF1V">افادےت</w> <w pos="II PPM1N">مےں</w> 
<w pos="NNMM1N">اضافہ</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VVTM1N">جاسکتا</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </p>
<p>
<w pos="FX">0صارفےن</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">خسارہ</w> <w pos=":">:</w> 
</p>
<p>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تشہےر</w> <w pos="II PPM1N">مےں</w> <w pos="VV0">بلا</w> <w pos="NNMM1N">شبہ</w> 
<w pos="RRJ JDNM1O JDNM2N JDNM2O">پہلے</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="IB">زےر</w> <w pos="NNUM1N NNUM1O">استعمال</w> 
<w pos="NNMF1N NNMF1O NNMF1V VVYM1N">اشےا</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VHNM1O">ہونے</w> 
<w pos="JXVF1O JXVF1N JXVF2N JXVF2O">والی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">تسکےن</w> <w pos="II PPM1N">مےں</w> <w pos="NNMM1N">اضافہ</w> 
<w pos="VVNM1O">کرنے</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">قوت</w> <w pos="JJU">موجود</w> 
<w pos="VHHV1">ہے</w> <w pos=".">۔</w> <w pos="PY1N PY2N">ےہ</w> <w pos="RD JD">بہت</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">صارفےن</w> <w pos="II PPM1N">مےں</w> <w pos="JDYF1N JDYF2N JDYF1O JDYF2O">اےسی</w> 
<w pos="NNMF1N NNMF1O VVYM1N">اشےا</w> <w pos="VVNM1O">خرےدنے</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">خواہش</w> 
<w pos="NNUM1N NNUM1O">پےدا</w> <w pos="VVNM1O">کرنے</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">طاقت</w> 
<w pos="RM">بھی</w> <w pos="VVTF1N VXTF1N">رکھتی</w> <w pos="VHHV1">ہے</w> <w pos="PJ2N PJ1N">جو</w> 
<w pos="PV2O PY2O">ان</w> <w pos="IIM1O">کے</w> <w pos="RR">پاس</w> <w pos="JJU">موجود</w> 
<w pos="RMN">نہےں</w> <w pos=",">،</w> <w pos="CC CCC">ےا</w> <w pos="NNUF2N VVSM2 VVSV2 VVYF2N">جنھےں</w> 
<w pos="PV2N PV1N">وہ</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ناکافی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">قوت</w> <w pos="VV0">خرےد</w> 
<w pos="VVYF1N VVYF2N VVYF1O VVYF2O">کی</w> <w pos="VV0 RR">بنا</w> <w pos="II CC">پر</w> <w pos="VVNM1O">خرےدنے</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پوزےشن</w> <w pos="II PPM1N">مےں</w> <w pos="RMN">نہےں</w> 
<w pos="VHHV2 VHHM2">ہےں</w> <w pos=".">۔</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUF1N NNUM1O NNUM1N NNUF1O">طرح</w> 
<w pos="PV2O PY2O">ان</w> <w pos="JJU NNUM1O NNUF1O RR">مصنوعات</w> <w pos="II">کو</w> <w pos="VVNM1O">خرےدنے</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="NNUM1O JJU NNUF1O NNUM1N NNUF1N">ہمت</w> <w pos="RMN JDNU">نہ</w> <w pos="VVNM1O">رکھنے</w> 
<w pos="JXVM2O">والوں</w> <w pos="II PPM1N">مےں</w> <w pos="IB">بے</w> <w pos="VVNF1 VVNF2">اطمےنانی</w> 
<w pos="NNUM1N NNUM1O">پےدا</w> <w pos="VVTF1N">ہوسکتی</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
</p>
<p>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تشہےر</w> <w pos="II CC">پر</w> <w pos="PK1N VVYM1N PK2N">کےا</w> <w pos="VXNM1O VVNM1O">جانے</w> 
<w pos="JXVM1N">والا</w> <w pos="JDNU">اےک</w> <w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">قدرے</w> <w pos="NNMM1N">سنجےدہ</w> 
<w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">حقےقت</w> <w pos="JJU NNUF1N NNUF1O">پسند</w> <w pos="NNMM1N">انہ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اعتراض</w> <w pos="PY1N PY2N">ےہ</w> <w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU">انسانی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">خواہشات</w> 
<w pos="II PPM1N">مےں</w> <w pos="LL">شدت</w> <w pos="NNUM1N NNUM1O">پےدا</w> <w pos="VVNM1O">کردےنے</w> 
<w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="NNUM1O">اثر</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1O NNUF1N">وجہ</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNMF1O VVYM1N">اشےا</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">مادی</w> 
<w pos="NNUF1N NNUF1O NNUF1V">افادےت</w> <w pos="NNUM1N JJU NNUM1O">ختم</w> <w pos="VHNM1O">ہونے</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> 
<w pos="RRJ JDNM1O JDNM2N JDNM2O">پہلے</w> <w pos="XH">ہی</w> <w pos="PV2O PY2O">ان</w> <w pos="IIF1N IIF1O">کی</w> 
<w pos="NNUF1N NNUF1O NNUF1V">افادےت</w> <w pos="NNUM1N JJU NNUM1O">ختم</w> <w pos="VVTF1N">ہوجاتی</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> <w pos="NNUF1O NNUF1N">رےاست</w> <w pos="AU">ہائے</w> <w pos="JJU">متحدہ</w> 
<w pos="NNUM1O">امرےکہ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JDNU">اےک</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">صنعت</w> 
<w pos="NNUM1O NNUF1O">کار</w> <w pos="II">نے</w> <w pos="PY1O PV1O">اس</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">خواہش</w> 
<w pos="II CC">پر</w> <w pos="NNUM1N NNUF1N NNUF1O NNUF2N NNUM1O">بار</w> <w pos="NNUM1N NNUF1N NNUF1O NNUF2N NNUM1O">بار</w> <w pos="NNUM1N NNUM1O">زور</w> 
<w pos="VVYM1N">دےا</w> <w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> <w pos="NNUM2O">لوگوں</w> 
<w pos="IIM1O">کے</w> <w pos="RR">پاس</w> <w pos="PJ2N PJ1N">جو</w> <w pos="PNN PNO">کچھ</w> 
<w pos="JJU">موجود</w> <w pos="VHHV1">ہے</w> </p>
<p>
<w pos="JDNU">60</w> <w pos=".">۔</w> <w pos=".">۔</w> <w pos=".">۔</w> 
<w pos=".">۔</w> <w pos=".">۔</w> <w pos=".">۔</w> <w pos=".">۔</w> 
<w pos=".">۔</w> <w pos=".">۔</w> <w pos=".">۔</w> <w pos=".">۔</w> 
<w pos=".">۔</w> <w pos=".">۔</w> </p>
<p>
</p>
<p>
</p>
<p>
<w pos="JDNU">0</w> </p>
</body>
</text>
</cesDoc>
