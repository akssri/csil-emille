<cesDoc id="urd-w-literature-drama-pl08.uss" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-literature-drama-pl08.uss.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Central Institute for Indian Languages, Mysore</respName>
<respType>transferred into Unicode and CES format by</respType>
<respName>"Unicodify" software by Andrew Hardie</respName>
</respStmt></titleStmt>
<publicationStmt>
<distributor>UCREL (on behalf of CIIL)</distributor>
<pubAddress>Department of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-08-01</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>T.K.K.</h.title>
<h.author>Anand.</h.author>
<imprint>
<pubPlace>India</pubPlace>
<publisher>Unknown - Book</publisher>
<pubDate>1983</pubDate>
</imprint>
<idno type="CIIL code">pl08.uss</idno>
</monogr></biblStruct></sourceDesc></fileDesc>
<encodingDesc>
<projectDesc>Text collected for the CIIL Corpus, subsequently integrated into the EMILLE/CIIL Monolingual Written Corpora.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted. Sampling begins at page 13/.</samplingDesc>
<editorialDecl><conformance level="1"></conformance></editorialDecl>
</encodingDesc>
<profileDesc>
<creation><date>03-08-01</date></creation>
<langUsage>Urdu</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<domain type="public"></domain>
<factuality type="fiction"></factuality>
</textClass>
<translations></translations>
</profileDesc>
<revisionDesc></revisionDesc>
</cesHeader>

<text>﻿<body>
<p>
<w pos="JDNU">60</w> <w pos="(">(</w> <w pos="JDNU">49</w> <w pos=")">)</w> 
<w pos="JDNU">1</w> </p>
<p>
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1O NNUF1O RR">قتل</w> <w pos="IIM1N">کا</w> <w pos="NNUM1N">الزام</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">چارودت</w> <w pos="II CC">پر</w> <w pos="VVTM1N">آجاتا</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> <w pos="CC">لےکن</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">وسنت</w> <w pos="VVNM1N">سےنا</w> 
<w pos="PJ2N PJ1N">جو</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">مری</w> <w pos="RMN">نہےں</w> <w pos="VHPF1">تھی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">صحتےاب</w> <w pos="VVTF1N">ہوجاتی</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
<w pos="CC RD">اور</w> <w pos="JJU NNUM1O NNUF1O RR">رقےب</w> <w pos="II">کو</w> <w pos="JJU">ظاہر</w> 
<w pos="VVTF1N">کردےتی</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">چارودت</w> 
<w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> <w pos="JJU NNUM1O NNUF1O RR">دشمن</w> <w pos="II">کو</w> <w pos="NNUM1N NNUM1O">معاف</w> 
<w pos="VVTM1N">کردےتا</w> <w pos="VHHV1">ہے</w> <w pos="NNMM1N JJM1N VVYM1N">سےسمتھا</w> <w pos="NNMM1N JJM1N VVYM1N">نکا</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">کورہائی</w> <w pos="VV0 NNUM1N NNUM1O">مل</w> <w pos="VXTF1N VVTF1N">جاتی</w> <w pos="VHHV1">ہے</w> 
<w pos="CC">اور</w> <w pos="JJU NNUM1O NNUF1O RR">چارودت</w> <w pos="II">کو</w> <w pos="PGRF1N PGRF1O PGRF2O PGRF2N">اپنی</w> 
<w pos="NNMF1N NNMF1O JJF1N JJF2N VVYF1N VVYF2N">کھوئی</w> <w pos="VHYF1N VHYF2N VHYF1O VHYF2O">ہوئی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">عزت</w> <w pos="CC RD">اور</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">دولت</w> <w pos="VV0 NNUM1N NNUM1O">مل</w> <w pos="VXTF1N VVTF1N">جاتی</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </p>
<p>
<w pos="PY1N PY2N">ےہ</w> <w pos="JJU NNUM1O NNUF1O RR">ناٹک</w> <w pos="IIM1N">کا</w> <w pos="RD JD">بہت</w> 
<w pos="XH">ہی</w> <w pos="JJU">مختصر</w> <w pos="NNMM1N">ساخاکہ</w> <w pos="VHHV1">ہے</w> 
<w pos="CS">چونکہ</w> <w pos="PY1O PV1O">اس</w> <w pos="IIM1N">کا</w> <w pos="JJM1N">پورا</w> 
<w pos="NNMM1N">خاکہ</w> <w pos="NNUM1O NNUM1N">بےان</w> <w pos="VVNM1N">کرنا</w> <w pos="XT">تو</w> 
<w pos="JJU">ممکن</w> <w pos="VHHV1 VHHT1">ہے</w> <w pos="PY1O PV1O">اس</w> <w pos="RR VVYM2N VVYM1O VVYM2O">لےے</w> 
<w pos="PPT2O">تم</w> <w pos="IIC XHC">ہےں</w> <w pos="PY1O PV1O">اس</w> <w pos="IIC">ے</w> 
<w pos="PRF">خود</w> <w pos="XH">ہی</w> <w pos="VVNM1N">پرھنا</w> <w pos="VC1">چاہےے</w> 
<w pos=".">۔</w> <w pos="JD">ہر</w> <w pos="FF">اےکٹ</w> <w pos="IIM1N">کا</w> 
<w pos="JDNU">اےک</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">عنوان</w> <w pos="VHHV1">ہے</w> <w pos="RJJ JDJM1O JDJM2N JDJM2O">جےسے</w> 
<w pos=",">،</w> <w pos="NNUM1N NNUM1O">طوفان</w> <w pos="CC CCC">ےا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">آزمائش</w> 
<w pos=",">،</w> <w pos="PY1O PV1O">اس</w> <w pos="II PPM1N">مےں</w> <w pos="JDNU">اےک</w> 
<w pos="FF">اےکٹ</w> <w pos="NNMM1N">مالشےہ</w> <w pos="NNMF1N NNMF1O JJF1N JJF2N VVYF1N">جواری</w> <w pos="VHHV1">ہے</w> 
<w pos="PJ1O">جس</w> <w pos="II PPM1N">مےں</w> <w pos="PY1N PY2N">ےہ</w> <w pos="NNUM1N NNUM1O">کردار</w> 
<w pos="JD">سب</w> <w pos="II">کو</w> <w pos="VVTM2N">ہنساتے</w> <w pos="VVTM1O VVTM2N VVTM2O">ہنساتے</w> 
<w pos="NNMM1N JJM1N VVYM1N">لٹا</w> <w pos="VVTM1N">دےتا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
<w pos="PY1N PY2N">ےہ</w> <w pos="JDNU">اےک</w> <w pos="JDYM1O RYJ JDYM2N JDYM2O">اےسے</w> <w pos="NNMF1O JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">جواری</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O RR NNUM1N">بارے</w> <w pos="II PPM1N">مےں</w> <w pos="VHHV1">ہے</w> 
<w pos="PJ2N PJ1N">جو</w> <w pos="PGRF1N PGRF1O PGRF2O PGRF2N">اپنی</w> <w pos="NNMM1N JJM1N VVYM1N">جوا</w> <w pos="VVNM1O">کھےلنے</w> 
<w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">عادت</w> <w pos="II CC">پر</w> <w pos="NNMM2V NNMF2V NNUM2V NNUF2V VVST2 VVIT2">قابو</w> 
<w pos="RMN">نہےں</w> <w pos="VVTM1N">پاسکتا</w> <w pos=".">۔</w> <w pos="PV2N PV1N">وہ</w> 
<w pos="JD">ہر</w> <w pos="JDNU">اےک</w> <w pos="IIM1N">کا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">قرض</w> 
<w pos="NNUM2N NNUM1N NNUM1O">دار</w> <w pos="VHHV1">ہے</w> <w pos="CC">اور</w> <w pos="RJ">جب</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">قرض</w> <w pos="JJU">خواہ</w> 
<w pos="PGRM1N">اپنا</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">قرض</w> <w pos="VVTM2N">مانگتے</w> <w pos="VHHV2 VHHM2">ہےں</w> 
<w pos="CC">اور</w> <w pos="PY1N PY2N">ےہ</w> <w pos="NNUF1N NNUF1O">ادا</w> <w pos="RMN">نہےں</w> 
<w pos="VVTM1N">کرپاتا</w> <w pos="XT">تو</w> <w pos="NNMM1N JJM1N VVYM1N">پےٹا</w> <w pos="VXTM1N VVTM1N VXYM1N">جاتا</w> 
<w pos="VHHV1 VHHT1">ہے</w> <w pos=".">۔</w> <w pos="PV2N PV1N">وہ</w> <w pos="VV0">بھاگ</w> 
<w pos="VX0">کر</w> <w pos="JDNU">اےک</w> <w pos="JJU NNUM1O NNUF1O RR">مندر</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">آنگن</w> <w pos="II PPM1N">مےں</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">چھپ</w> <w pos="VXTM1N VVTM1N VXYM1N">جاتا</w> 
<w pos="VHHV1">ہے</w> <w pos="CC">اور</w> <w pos="JJU NNUM1O NNUF1O RR">پتھر</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JDNU">اےک</w> <w pos="JJU NNUM1O NNUF1O RR">بت</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">طرح</w> 
<w pos="JDNU">اےک</w> <w pos="NNUF1O NNUF1N">جگہ</w> <w pos="NNMM1N JJM1N VVYM1N">کھڑا</w> <w pos="VVTM1N">ہوجاتا</w> 
<w pos="VHHV1">ہے</w> <w pos="CS">تاکہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">قرض</w> <w pos="NNMM2O NNUM2O NNUF2O">خواہوں</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">گرفت</w> <w pos="II JXSM2N">سے</w> <w pos="VV0">بچ</w> 
<w pos="VXSV1 VXYM2N VXYM1O VXYM2O VXST1">سکے</w> <w pos=".">۔</w> <w pos="CC">لےکن</w> <w pos="RJ">جب</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="VVNM1O">پکڑنے</w> <w pos="JXVM2N JXVM2O JXVM1O">والے</w> 
<w pos="XH">ہی</w> <w pos="RV">وہاں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">آکر</w> <w pos="NNMM1N JJM1N VVYM1N">جوا</w> 
<w pos="VVNM1O">کھےلنے</w> <w pos="VVTM2N">لگتے</w> <w pos="VHHV2 VHHM2">ہےں</w> <w pos="CC">اور</w> 
<w pos="NNUM1N NNUM1O">کھےل</w> <w pos="JJU RR">خوب</w> <w pos="NNMM2O NNUM2O NNUF2O VVSM1">زوروں</w> <w pos="II CC">پر</w> 
<w pos="VHNM1O">ہونے</w> <w pos="VVTM1N">لگتا</w> <w pos="VHHV1">ہے</w> <w pos=",">،</w> 
<w pos="XT">تو</w> <w pos="PY1N PY2N">ےہ</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">مالش</w> <w pos="VVNM1O">کرنے</w> 
<w pos="JXVM1N">والا</w> <w pos="NNMF1N NNMF1O JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">جواری</w> <w pos="PNN JD">سب</w> <w pos="PNN PNO">کچھ</w> 
<w pos="VV0">بھول</w> <w pos="VXTM1N VXYM1N">جاتا</w> <w pos="VHHV1">ہے</w> <w pos="CC">اور</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">لپک</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">شور</w> <w pos="VVTM1N">مچاتا</w> <w pos="VHYM1N">ہوا</w> 
<w pos="PY1O PV1O">اس</w> <w pos="II PPM1N">مےں</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">کود</w> <w pos="VVTM1N VXTM1N">پڑتا</w> 
<w pos="VHHV1">ہے</w> <w pos=",">،</w> <w pos="RY">اب</w> <w pos="PGM1F1N PGM1F1O">مےری</w> 
<w pos="NNMF1N NNMF1O">باری</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> <w pos="JJU">ظاہر</w> 
<w pos="VHHV1">ہے</w> <w pos="PY1O PV1O">اس</w> <w pos="IIC">ے</w> <w pos="VV0">پکڑ</w> 
<w pos="VXYM1N">لےا</w> <w pos="VXTM1N VVTM1N VXYM1N">جاتا</w> <w pos="VHHV1">ہے</w> <w pos="CC">اور</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU RR">خوب</w> <w pos="NNMF1N NNMF1O JJF1N JJF2N VVYF1N">پٹائی</w> 
<w pos="VHTF1N">ہوتی</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> </p>
<p>
<w pos="PY1O PV1O">اس</w> <w pos="II PPM1N">مےں</w> <w pos="JDNU">اےک</w> <w pos="CC RD">اور</w> 
<w pos="NNMM1N">مزاحےہ</w> <w pos="FF">اےکٹ</w> <w pos="VHHV1">ہے</w> <w pos="PJ1O">جس</w> 
<w pos="IIM1N">کا</w> <w pos="NNUM1N">نام</w> <w pos="VHHV1">ہے</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">نقب</w> 
<w pos="VVNF1 VVNF2">زنی</w> <w pos=".">۔</w> <w pos="JDNU">اےک</w> <w pos="JJU NNUM1O NNUF1O RR">چورچارودت</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O NNUM1N NNUM2N">گھر</w> <w pos="II PPM1N">مےں</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">نقب</w> 
<w pos="VVYM1N">لگاےا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> <w pos="PY1N PY2N">ےہ</w> 
<w pos="FF">اےکٹ</w> <w pos="PGRM1O">اپنے</w> <w pos="NNMM1N">مزاحےہ</w> <w pos="NNUM1O NNUM1N">انداز</w> 
<w pos="CC RD">اور</w> <w pos="JJU NNUM1O NNUF1O RR">چور</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">نقب</w> 
<w pos="VVNF2">زنی</w> <w pos="IIM1O">کے</w> <w pos="NNUM1O">فن</w> <w pos="II CC">پر</w> 
<w pos="PRF">خود</w> <w pos="NNMF1O JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">کلامی</w> <w pos="IIM2O">کے</w> <w pos="NNMM2O NNUM2O">مکالموں</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="RR VVYM2N VVYM1O VVYM2O">لےے</w> <w pos="RD">بہت</w> <w pos="JJU">مشہور</w> 
<w pos="VHHV1">ہے</w> <w pos=".">۔</w> </p>
<p>
<w pos="PY1N PY2N">ےہ</w> <w pos="CC RD">اور</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUF1O">قسم</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JDNM2N JDNM1O JDNM2O">دوسرے</w> <w pos="RD JD">بہت</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">دلچسپ</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پرلطف</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مناظر</w> 
<w pos=",">،</w> <w pos="NNMF1O">مٹی</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">چھوٹی</w> 
<w pos="JXSF1N JXSF1O">سی</w> <w pos="NNMF1N NNMF1O">گاڑی</w> <w pos=",">،</w> <w pos="II PPM1N">مےں</w> 
<w pos="VVNM1O">دےکھنے</w> <w pos="II">کو</w> <w pos="VVSV2">ملےں</w> <w pos="VGM2">گے</w> 
<w pos=".">۔</w> <w pos="PY1O PV1O">اس</w> <w pos="JJU NNUM1O NNUF1O RR">ناٹک</w> <w pos="IIM1N">کا</w> 
<w pos="NNUM1N">نام</w> <w pos="PNN PNO">کچھ</w> <w pos="JJU">عجےب</w> <w pos="JXSM1N">سا</w> 
<w pos="VVTM1N">لگتا</w> <w pos="VHHV1">ہے</w> <w pos="CC">اور</w> <w pos="JDNU">اےک</w> 
<w pos="JJM2N JJM1O JJM2O">چھوٹے</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JDNU">60</w> <w pos="(">(</w> 
<w pos="JDNU">50</w> <w pos=")">)</w> <w pos="JDNU">2</w> </p>
<p>
</p>
<p>
</p>
<p>
<w pos="JDNU">0</w> </p>
</body>
</text>
</cesDoc>
