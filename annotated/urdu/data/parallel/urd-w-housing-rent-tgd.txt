<cesDoc id="urd-w-housing-rent" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-housing-rent.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Miss Shehnaz Nawab)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-12-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Do you rent, or are you thinking of renting, from a private landlord?</h.title>
<h.author>Department for Transport, Local Government and the Regions</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department for Transport, Local Government and the Regions</publisher>
<pubDate>February 2002</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-11-23</date>
</creation>
<langUsage>Translated into Urdu from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-housing-rent.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
﻿<body>
<p>
<head>
<s>
<foreign lang="eng">
<w pos="FX">DTLR</w> </foreign>
</s>
<s>
<foreign lang="eng">
<w pos="FX">TRANSPORT</w> <w pos="FX">LOCAL</w> <w pos="FX">GOVERNMENT</w> <w pos="FX">REGIONS</w> 
</foreign>
</s>
</head>
</p>
</body>
</text>
</cesDoc>
