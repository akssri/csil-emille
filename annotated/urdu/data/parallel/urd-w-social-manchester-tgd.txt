<cesDoc id="urd-w-social-manchester" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-social-manchester.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Raheela Iqbal</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region= "WORLD"></availability>
<pubDate>01-13-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>How To Get Help From Social Services</h.title>
<h.author>Manchester City Council</h.author>
<imprint>
<pubPlace>Manchester</pubPlace>
<publisher>Manchester City Council</publisher>
<pubDate>Unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>00-12-19</date>
</creation>
<langUsage>Translated into Urdu from original English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646"> Universal Multiple-Ocet Coded Character Set (UCS)</writingSystem>
</wsdUsage> 
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality> 
</textClass>
<translations>
<translation trans.loc="parallel file identifier" lang="eng" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>
<text>
﻿<body>
<p>
<head>
<s>
<w pos="FF">سوشل</w> <w pos="FF">سروسز</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUF1N NNUF1O">مدد</w> 
<w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VVNM1O">کرنے</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNMM2N NNMM1O NNMM1V">طریقے</w> 
</s>
</head>
</p>
<p>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اپریل</w> <foreign lang="eng">
<w pos="JDNU">1993</w> </foreign>
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="FF">سوشل</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کیئر</w> <w pos=")">)</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">سماجی</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1N NNUM1N NNUF1O NNUM1O">بھال</w> <w pos="(">(</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">فراہمی</w> <w pos="CC RD">اور</w> <w pos="PY1O PV1O">اس</w> 
<w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="NNUF1N NNUF1O">رقم</w> <w pos="JJU">مہیا</w> 
<w pos="VVNM1O">کرنے</w> <w pos="IIM2O">کے</w> <w pos="NNMM2O NNUM2O">طریقوں</w> <w pos="II">میں</w> 
<w pos="NNMF2N">تبدیلیاں</w> <w pos="VVYF1N VVYF2N">لائی</w> <w pos="VV0 VX0">جا</w> <w pos="VRF2">رہی</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="RRJ JDNM1O JDNM2N JDNM2O">پہلے</w> <w pos="FF">سوشل</w> 
<w pos="NNMF1O">سکیورٹی</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1O NNUF1O RR">ڈیپارٹمنٹ</w> <w pos="IIF1N IIF1O">کی</w> 
<w pos="NNUF1O NNUF1N">جانب</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ریذیڈنشل</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ہومز</w> 
<w pos=")">)</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1N NNUM1N NNUF1O NNUM1O">بھال</w> <w pos="VVNM1O">کرنے</w> 
<w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="JJU">خصوصی</w> <w pos="NNUM2O">گھروں</w> 
<w pos="(">(</w> <w pos="IIM2N">کے</w> <w pos="NNUM2N">اخراجات</w> <w pos="IIM1O">کے</w> 
<w pos="RR">لئے</w> <w pos="NNUF1N NNUF1O">رقم</w> <w pos="JJU">فراہم</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> 
<w pos="VXTF1N VVTF1N">جاتی</w> <w pos="VHPF1">تھی</w> <w pos=".">۔</w> </s>
<s>
<w pos="CC">لیکن</w> <w pos="RY">اب</w> <w pos="PY1N PY2N">یہ</w> <w pos="FF">سٹی</w> 
<w pos="NNUM1O">کونسل</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="FF">سوشل</w> <w pos="FF">سروسز</w> 
<w pos="JJU NNUM1O NNUF1O RR">ڈیپارٹمنٹ</w> <w pos="II">کو</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">منتقل</w> <w pos="VX0 VV0 VVIT1">کر</w> 
<w pos="VVYF1N VXYF1N VXYF2N VVYF2N">دی</w> <w pos="VXYF1N VVYF1N">گئی</w> <w pos="VHHV1">ہے</w> <w pos="PJ2N PJ1N">جو</w> 
<w pos="RY">اب</w> <w pos="JD">تمام</w> <w pos="NNUM1O NNUF1O">کیئر</w> <w pos="IIM1O">کے</w> 
<w pos="NNUM1O">انتظام</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="LL">ذمہ</w> <w pos="NNUM2N NNUM1N NNUM1O">دار</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="NNMM1O">سلسلے</w> <w pos="II">میں</w> <w pos="PV2N PV1N">وہ</w> 
<w pos="PNN PNO">کچھ</w> <w pos="FF">سروسز</w> <w pos="XT">تو</w> <w pos="PRF">خود</w> 
<w pos="JJU">فراہم</w> <w pos="VVSM2 VVSV2">کریں</w> <w pos="VGM2">گے</w> <w pos="CC">اور</w> 
<w pos="PNN PNO">کچھ</w> <w pos="JJU">مختلف</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">پرائیویٹ</w> <w pos="CC RD">اور</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">رضاکار</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ذرائع</w> <w pos="II JXSM2N">سے</w> <w pos="VVSM2 VVSV2">خریدیں</w> 
<w pos="VGM2">گے</w> <w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="PV2O PY2O">ان</w> <w pos="NNUF2O">تبدیلیوں</w> <w pos="II">کو</w> <w pos="VVNM1O">لانے</w> 
<w pos="IIM1N">کا</w> <w pos="JDNU">ایک</w> <w pos="JJM1N RR">بڑا</w> <w pos="NNUM1N NNUM1O">مقصد</w> 
<w pos="PY1O PV1O">اس</w> <w pos="NNUF1O">بات</w> <w pos="IIM1N">کا</w> <w pos="NNUM1N">یقین</w> 
<w pos="VVNM1N">کرنا</w> <w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> <w pos="NNUF1N NNUF1O">مدد</w> 
<w pos="PV2O PY2O">ان</w> <w pos="NNUM2O">لوگوں</w> <w pos="II">کو</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> 
<w pos="VH0">ہو</w> <w pos="VXSV1 VXYM2N VXYM1O VXYM2O VXST1">سکے</w> <w pos="NNUF2N VVSM2 VVSV2 VVYF2N">جنھیں</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">اسکی</w> 
<w pos="PNN JD">سب</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JD RR">زیادہ</w> <w pos="NNUF1N NNUF1O">ضرورت</w> 
<w pos="VHHV1">ہے</w> <w pos=",">،</w> <w pos="CC RD">اور</w> <w pos="PY1N PY2N">یہ</w> 
<w pos="CS">کہ</w> <w pos="NNUM2O">لوگوں</w> <w pos="II">کو</w> <w pos="PV2O PY2O">ان</w> 
<w pos="IIF1N">کی</w> <w pos="NNMM1N">مطلوبہ</w> <w pos="FF">سروسز</w> <w pos="VV0 NNUM1N NNUM1O">مل</w> 
<w pos="VXSV2 VXSM2 VVYF2N VVSM2 VVSV2 VXYF2N">سکیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="JDYM1N">ایسا</w> <w pos="VVNM1O">کرنے</w> <w pos="IIM1O">کے</w> <w pos="RR">لئے</w> 
<w pos="FF">سوشل</w> <w pos="FF">سروسز</w> <w pos="II">کو</w> <w pos="PV2O PY2O">ان</w> 
<w pos="NNUM2O">افراد</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O RR">بارے</w> <w pos="II">میں</w> 
<w pos="RD JD">بہت</w> <w pos="JXSF1N JXSF2N JXSF1O JXSF2O">سی</w> <w pos="NNUF2N NNUF2O NNUF1N NNUF1O">معلومات</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> 
<w pos="VVNF1 VVNF2">کرنی</w> <w pos="VVTF2N">پڑتی</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="PJ2N PJ1N">جو</w> 
<w pos="PY1N PY2N">یہ</w> <w pos="NNUF1N NNUF1O">مدد</w> <w pos="VVTM2N">مانگتے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="IIC">ے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اسیسمنٹ</w> <w pos=")">)</w> 
<w pos="NNUF2O">ضروریات</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">اندازہ</w> <w pos="VVNM1O">لگانے</w> 
<w pos="IIM1N">کا</w> <w pos="NNUM1N">عمل</w> <w pos="(">(</w> <w pos="VVYM1N">کہا</w> 
<w pos="VXTM1N VVTM1N VXYM1N">جاتا</w> <w pos="VHHV1">ہے</w> <w pos="CC">اور</w> <w pos="PY1O PV1O">اس</w> 
<w pos="JJU NNUM1O NNUF1O RR">لیفلٹ</w> <w pos="II">میں</w> <w pos="PY1O PV1O">اس</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">طریق</w> <w pos="NNUM1O NNUF1O">کار</w> <w pos="IIF2N IIF2O">کی</w> <w pos="NNUF2N NNUF2O">تفصیلات</w> 
<w pos="JJU">شامل</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
</p>
<p>
<head>
<s>
<w pos="PY1N PY2N">یہ</w> <w pos="NNUF1N NNUF1O">مدد</w> <w pos="NNMM1O NNMM2N JJM2N VVYM2N VVST1 VVSV1 RRJ">کسے</w> <w pos="VVSV1">ملے</w> 
<w pos="VGF2 VGF1">گی</w> </s>
</head>
</p>
<p>
<s>
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> <w pos="NNUF2O">ضروریات</w> <w pos="IIM1N">کا</w> <w pos="JJU">تفصیلی</w> 
<w pos="NNMM1N">اندازہ</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">لگاۓ</w> <w pos="VXNM2O VXNM1O VVNM1O VVSV1 VVYM2N VVNM2 VVYM1O VVYM2O VXNM2">جانے</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">قبل</w> <w pos="JJU">ضروری</w> <w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> 
<w pos="PA">آپ</w> <w pos="JJU">مندرجہ</w> <w pos="JJU">ذیل</w> <w pos="NNMM2O NNUM2O NNUF2O">گروپوں</w> 
<w pos="II">میں</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JDNU">ایک</w> <w pos="CC CCC">یا</w> 
<w pos="RD JD RR">زیادہ</w> <w pos="II PPM1N">میں</w> <w pos="VVTM2N">آتے</w> <w pos="VHSV2">ہوں</w> 
<w pos=":">:</w> </s>
<s>
<w pos="PV2N PV1N">وہ</w> <w pos="NNUM2N">لوگ</w> <w pos="PJ2O">جن</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="NNUM1O RR">بارے</w> <w pos="II">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ثابت</w> <w pos="VH0">ہو</w> 
<w pos="VXYM1N">چکا</w> <w pos="VH0 VHSV1 VHST1 VHIT1">ہو</w> <w pos="CS">کہ</w> <w pos="PNO">کسی</w> 
<w pos="JJU NNUM1O NNUF1O RR">مداخلت</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">بغیر</w> <w pos="NNMF1N NNMF1O JJF1N JJF2N VVYF1N VVYF2N">انکی</w> 
<w pos="VVNF1 VVNF2">ذہنی</w> <w pos="CC RD">اور</w> <w pos="~">/</w> <w pos="VVNF1 VVNF2">جسمانی</w> 
<w pos="JJU NNUM1O NNUF1O RR">حالت</w> <w pos="II">میں</w> <w pos="NNMM1N">خاطرخواہ</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">خرابی</w> 
<w pos="NNUM1N NNUM1O">پیدا</w> <w pos="VHSV1 VHST2 VHST1">ہو</w> <w pos="VGF2 VGF1">گی</w> <w pos=",">،</w> 
<w pos="CC CCC">یا</w> <w pos="PV2O PY2O">ان</w> <w pos="IIC XHC">ھیں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">خاطر</w> 
<w pos="JJU">خواہ</w> <w pos="NNUM1N NNUM1O NNUM2N">نقصان</w> <w pos="VVNM1O">پہنچنے</w> <w pos="IIM1N">کا</w> 
<w pos="NNMM1N">اندیشہ</w> <w pos="VHSV1 VHST1">ہو</w> <w pos="VGM1">گا</w> <w pos=",">،</w> 
<w pos="CC CCC">یا</w> <w pos="PV2N PV1N">وہ</w> <w pos="JDNUO">دوسروں</w> <w pos="IIM1O">کے</w> 
<w pos="RR">لئے</w> <w pos="NNMM1O">خطرے</w> <w pos="IIM1N">کا</w> <w pos="NNUM1N">باعث</w> 
<w pos="VHSV2">ہوں</w> <w pos="VGM2">گے</w> <w pos=".">۔</w> </s>
<s>
<w pos="PV2N PV1N">وہ</w> <w pos="NNUM2N">لوگ</w> <w pos="NNUF2N VVSM2 VVSV2 VVYF2N">جنھیں</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ری</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ہیبلیٹیشن</w> <w pos=")">)</w> <w pos="NNMM1N">سابقہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">حالت</w> 
<w pos="II CC">پر</w> <w pos="VVNM1O">لانے</w> <w pos="JXVF1O JXVF1N JXVF2N JXVF2O">والی</w> <w pos="(">(</w> 
<w pos="FF">سروسز</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">ضرورت</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="PV2O PY2O">ان</w> <w pos="II PPM1N">میں</w> <w pos="PV2N PV1N">وہ</w> <w pos="NNUM2N">لوگ</w> 
<w pos="JJU">شامل</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="PJ2N PJ1N">جو</w> <w pos="PNO">کسی</w> 
<w pos="JJF1N JJF1O JJF2N JJF2O">بڑی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">علامت</w> <w pos="CC CCC">یا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">چوٹ</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNMM1N JJM1N VVYM1N">شفا</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VV0">کر</w> 
<w pos="VRM2">رہے</w> <w pos="VHSV2">ہوں</w> <w pos=",">،</w> <w pos="CC CCC">یا</w> 
<w pos="NNUF2N VVSM2 VVSV2 VVYF2N">جنھیں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">شراب</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">نوشی</w> <w pos="CC CCC">یا</w> 
<w pos="NNUF1O">منشیات</w> <w pos="IIM1O">کے</w> <w pos="NNUM1O">استعمال</w> <w pos="IIF1N IIF1O">کی</w> 
<w pos="NNUF1N NNUF1O">عادت</w> <w pos="VH0 VHSV1 VHHT2 VHST2 VHST1 AU VHIT1 VHIT2">ہو</w> <w pos=".">۔</w> </s>
<s>
<w pos="PV2N PV1N">وہ</w> <w pos="NNUM2N">لوگ</w> <w pos="PJ2N PJ1N">جو</w> <w pos="PNO">کسی</w> 
<w pos="JDNM1O">دوسرے</w> <w pos="NNUM1O">قانون</w> <w pos="IIM1O">کے</w> <w pos="RR">تحت</w> 
<w pos="JJU NNUM1O NNUF1O RR">اسیسمنٹ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="LL">اہل</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos=",">،</w> <w pos="RR">مثلاً</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">دائمی</w> <w pos="NNUM1O NNUM1N">طور</w> 
<w pos="II CC">پر</w> <w pos="JJU NNUM2N">بیمار</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">معذور</w> 
<w pos="NNUM2O">افراد</w> <w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <foreign lang="eng">
<w pos="JDNU">1970</w> </foreign>
<w pos="IIM1N">کا</w> <w pos="FF">ایکٹ</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مینٹل</w> 
<w pos="FF">ہیلتھ</w> <w pos="FF">ایکٹ</w> <foreign lang="eng">
<w pos="JDNU">1983</w> </foreign>
<w pos=".">۔</w> </s>
<s>
<w pos="PV2N PV1N">وہ</w> <w pos="NNUM2N">لوگ</w> <w pos="PJ2N PJ1N">جو</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اسوقت</w> 
<w pos="JDYF1N JDYF2N JDYF1O JDYF2O">ایسی</w> <w pos="FF">سروسز</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VV0">کر</w> 
<w pos="VRM2">رہے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="PJ2N PJ1N">جو</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">انکی</w> 
<w pos="NNUF2O">ضروریات</w> <w pos="II">کو</w> <w pos="JJM1N">پورا</w> <w pos="RMN">نہیں</w> 
<w pos="VVTF2N">کرتیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="II PPM1N">میں</w> <w pos="NNMF1N NNMF1O JJF1N JJF2N VVYF1N VVYF2N">انکی</w> <w pos="VVTF1N VVTF1O VVTF2N VVTF2O">ثقافتی</w> 
<w pos=",">،</w> <w pos="JJU">مذہبی</w> <w pos=",">،</w> <w pos="VVNF1 VVNF2">لسانی</w> 
<w pos="CC RD">اور</w> <w pos="NNMM1O JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">رابطے</w> <w pos="IIF2O">کی</w> <w pos="NNUF2O">ضروریات</w> 
<w pos="RM">بھی</w> <w pos="JJU">شامل</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="PV2N PV1N">وہ</w> <w pos="NNUM2N">لوگ</w> <w pos="PJ2N PJ1N">جو</w> <w pos="JJU">مندرجہ</w> 
<w pos="NNMM1N JJM1N VVYM1N">بالا</w> <w pos="NNUM2O">افراد</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> 
<w pos="NNUF1N NNUM1N NNUF1O NNUM1O">بھال</w> <w pos="VVTM2N">کرتے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
</p>
<p>
<s>
<w pos="FF">سوشل</w> <w pos="FF">سروسز</w> <w pos="JXVM2N">والے</w> <w pos="VVST2 VVIT2">آپکو</w> 
<w pos="NNMM1N">مذکورہ</w> <w pos="NNMM1N JJM1N VVYM1N">بالا</w> <w pos="JJU NNUM1O NNUF1O RR">اقسام</w> <w pos="IIF1N IIF1O">کی</w> 
<w pos="NNUF1N NNUF1O">تفصیل</w> <w pos="VV0 VVSV1 VVIT1 VVST1">دے</w> <w pos="VXTM2N VVTM2N">سکتے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="PNO">کسی</w> <w pos="NNMM1N">مذکورہ</w> 
<w pos="NNUF1O">قسم</w> <w pos="II">میں</w> <w pos="RMN">نہیں</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos="RV">تب</w> <w pos="RM">بھی</w> <w pos="PV2N PV1N">وہ</w> <w pos="NNMM2V NNMF2V NNUM2V NNUF2V VVST2 VVIT2">آپکو</w> 
<w pos="NNUM1N NNUM1O">مشورہ</w> <w pos="VV0 VVSV1 VVIT1 VVST1">دے</w> <w pos="VXTM2N VVTM2N">سکتے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos="CC">اور</w> <w pos="JJU">ممکن</w> <w pos="VHHV1">ہے</w> <w pos="NNMM2V NNMF2V NNUM2V NNUF2V VVST2 VVIT2">آپکو</w> 
<w pos="NNUF1O">مدد</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> <w pos="PNO">کسی</w> 
<w pos="JDNF1O JDNF1N">دوسری</w> <w pos="NNMF1O NNMF1N">ایجنسی</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUF1N NNUF1O">ملاقات</w> 
<w pos="NNMM1N JJM1N VVYM1N">کرا</w> <w pos="VVYF2N VVSM2 VVSV2">سکیں</w> <w pos=".">۔</w> </s>
</p>
<p>
<head>
<s>
<w pos="NNUF1N NNUF1O">مدد</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VVNM1O">کرنے</w> <w pos="IIM1N">کا</w> 
<w pos="NNMM1N">طریقہ</w> </s>
</head>
</p>
<p>
<s>
<w pos="PA">آپ</w> <w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> <w pos="JJU">مقامی</w> <w pos="FF">سوشل</w> 
<w pos="FF">سروسز</w> <w pos="IIM1O">کے</w> <w pos="NNUM1O">آفس</w> <w pos="II">میں</w> 
<w pos="VVIA NNMM1O NNMM1V NNMM2N">آیئے</w> <w pos=",">،</w> <w pos="NNUM1O NNUM1N NNUM2N RR">فون</w> <w pos="VVIA NNMM1O NNMM1V NNMM2N">کیجئے</w> 
<w pos="CC CCC">یا</w> <w pos="NNUM2N NNUM1N NNUM1O">خط</w> <w pos="VVIA NNMM1O NNMM1V NNMM2N">لکھئے</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="PA">آپ</w> <w pos="PNO">کسی</w> <w pos="CC RD">اور</w> <w pos="II">کو</w> 
<w pos="RM">بھی</w> <w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> <w pos="RR">لئے</w> <w pos="JDYM1N">ایسا</w> 
<w pos="VVNM1O">کرنے</w> <w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="VV0">کہہ</w> 
<w pos="VXTM2N">سکتے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="NNUM1O NNUM1N NNUM2N RR">فون</w> <w pos="NNUM1N NNUM1O">نمبر</w> <w pos="CC RD">اور</w> <w pos="NNMM2N NNMM1O NNMM1V">پتے</w> 
<w pos="PY1O PV1O">اس</w> <w pos="JJU NNUM1O NNUF1O RR">لیفلٹ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O JJU RR">آخر</w> 
<w pos="II">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">دۓ</w> <w pos="VXYM2N VVYM2N">گئے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="II">کو</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مترجم</w> 
<w pos="CC CCC">یا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سائز</w> <w pos=")">)</w> <w pos="NNMM2O NNUM2O NNUF2O VVSM1">اشاروں</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUF1O NNUF1N NNUF2N">بات</w> <w pos="VVNM1O">کرنے</w> <w pos="JXVM2N JXVM2O JXVM1O">والے</w> 
<w pos="(">(</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">مدد</w> <w pos="VC1 VVIA">چاہئے</w> 
<w pos=",">،</w> <w pos="CC CCC">یا</w> <w pos="PA">آپ</w> <w pos="PNO">کسی</w> 
<w pos="II">کو</w> <w pos="PGRF1N PGRF1O PGRF2O PGRF2N">اپنی</w> <w pos="NNMF1N NNMF1O JJF1N JJF2N VVYF1N VVYF2N">نمائندگی</w> <w pos="VVNM1O">کرنے</w> 
<w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="VVNM1N VVYM1N">کہنا</w> <w pos="VVTM2N">چاہتے</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos="XT">تو</w> <w pos="JJU NNUM1O NNUF1O RR">سٹاف</w> <w pos="II">کو</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="RRJ JDNM1O JDNM2N JDNM2O">پہلے</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> 
<w pos="NNUF1N NNUF1O">اطلاع</w> <w pos="VVIA NNMM1O NNMM1V NNMM2N">کیجئے</w> <w pos="IB">تا</w> <w pos="CS">کہ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مناسب</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">انتظامات</w> <w pos="VVIA NNMM1O NNMM2N">کئے</w> <w pos="VV0 VVIT1">جا</w> 
<w pos="VXSV2 VXSM2 VVYF2N VVSM2 VVSV2 VXYF2N">سکیں</w> <w pos=".">۔</w> </s>
</p>
<p>
<head>
<s>
<w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">اسکے</w> <w pos="RR">بعد</w> <w pos="PK1N VVYM1N PK2N">کیا</w> <w pos="VHSV1 VHST1">ہو</w> 
<w pos="VGM1">گا</w> </s>
</head>
</p>
<p>
<s>
<w pos="PA">آپ</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">ملاقات</w> <w pos="JDNU">ایک</w> 
<w pos="NNUF2N NNUF2O NNUM2N NNUF1N NNUF1O">معلومات</w> <w pos="CC RD">اور</w> <w pos="NNUM1N NNUM1O">مشورہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ورکر</w> 
<w pos="II JXSM2N">سے</w> <w pos="VHSV1 VHST2 VHST1">ہو</w> <w pos="VGF2 VGF1">گی</w> <w pos="PJ2N PJ1N">جو</w> 
<w pos="PA">آپ</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> <w pos="JJU NNUM1O NNUF1O RR">صورتحال</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O RR">بارے</w> <w pos="II">میں</w> <w pos="PNN PNO">کچھ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">سوالات</w> <w pos="VVST1 VVSV1">پوچھے</w> <w pos="VGM1">گا</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="IIM1N">کا</w> <w pos="NNUM1N">مقصد</w> <w pos="PY1O PV1O">اس</w> 
<w pos="NNUF1O">بات</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">پتہ</w> <w pos="VVNM1N">لگانا</w> 
<w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> <w pos="VVYM1N">آیا</w> <w pos="PA">آپ</w> 
<w pos="JJM2N JJM1O JJM2O">نئے</w> <w pos="JJU NNUM1O NNUF1O RR">قواعد</w> <w pos="IIM1O">کے</w> <w pos="RR">تحت</w> 
<w pos="NNUF1N NNUF1O">مدد</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VVNM1O">کرنے</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="LL">اہل</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="CC CCC">یا</w> <w pos="RMN">نہیں</w> 
<w pos=",">،</w> <w pos="CC RD">اور</w> <w pos="CS">اگر</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos="XT">تو</w> <w pos="PA">آپ</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">ضرورت</w> 
<w pos="PK1O PK1N">کس</w> <w pos="NNUF1N NNUF1O">قدر</w> <w pos="JJU">اہم</w> <w pos="VHHV1 VHHT1">ہے</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="PNN JD">سب</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JD RR">زیادہ</w> <w pos="NNUF1N NNUF1O">اہمیت</w> 
<w pos="PV2O PY2O">ان</w> <w pos="IIC XHC">ھیں</w> <w pos="VVYF1N VVYF2N">دی</w> <w pos="VXSM1">جاۓ</w> 
<w pos="VGF2 VGF1">گی</w> <w pos="NNUF2N VVSM2 VVSV2 VVYF2N">جنھیں</w> <w pos="NNUF1O">مدد</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اشد</w> <w pos="NNUF1N NNUF1O">ضرورت</w> <w pos="VHHV1">ہے</w> <w pos="RR">مثلاً</w> 
<w pos="PV2N PV1N">وہ</w> <w pos="NNUM2N">لوگ</w> <w pos="PJ2N PJ1N">جو</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">فوری</w> 
<w pos="NNMM1O">خطرے</w> <w pos="II">میں</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="CC CCC">یا</w> 
<w pos="PV2N PV1N">وہ</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">جنکی</w> <w pos="NNUF2O">ضروریات</w> <w pos="JD">بہت</w> 
<w pos="NNMM1N">پیچیدہ</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="LL">زیادہ</w> <w pos="JJU">تر</w> <w pos="NNUM2N">حالات</w> <w pos="II PPM1N">میں</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> <w pos="NNMM1N">مطلوبہ</w> <w pos="NNUF1N NNUF1O">مدد</w> <w pos="NNMF1O NNMF1N">آسانی</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU">مہیا</w> <w pos="VVYF1N VVYF2N">کی</w> <w pos="VV0 VX0 VVIT1 VXIT1">جا</w> 
<w pos="VXTF1N VVTF1N">سکتی</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> </s>
<s>
<w pos="CC">لیکن</w> <w pos="CS">اگر</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> <w pos="NNUF2O">ضروریات</w> 
<w pos="JJU">مختلف</w> <w pos="CC RD">اور</w> <w pos="NNMM1N">پیچیدہ</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos="XT">تو</w> <w pos="FF">سوشل</w> <w pos="FF">سروسز</w> <w pos="IIF1N IIF1O">کی</w> 
<w pos="NNUF1O NNUF1N">جانب</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="PNN">کوئی</w> <w pos="PA">آپ</w> 
<w pos="IIM1O">کے</w> <w pos="NNUM1O">گھر</w> <w pos="II">میں</w> <w pos=",">،</w> 
<w pos="CC CCC">یا</w> <w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="NNUM1O">ہسپتال</w> 
<w pos="II">میں</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="XT">تو</w> <w pos="RV">وہاں</w> 
<w pos="PA">آپ</w> <w pos="II JXSM2N">سے</w> <w pos="VVNM1O VVNM2">ملنے</w> <w pos="VVIT1">آۓ</w> 
<w pos="VGM1">گا</w> <w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="NNUF1O">ملاقات</w> <w pos="IIM1O">کے</w> <w pos="NNUM1O">مقصد</w> 
<w pos="JJU">مندرجہ</w> <w pos="JJU">ذیل</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=":">:</w> 
</s>
<s>
<w pos="PY1N PY2N">یہ</w> <w pos="VVNM1N">دیکھنا</w> <w pos="CS">کہ</w> <w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">آپکے</w> 
<w pos="NNUM1O">خیال</w> <w pos="II">میں</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> <w pos="PK1N VVYM1N QQ PK2N">کیا</w> 
<w pos="NNUF2O">ضروریات</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">آپکے</w> <w pos="NNUM2N">حالات</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O RR">بارے</w> 
<w pos="II">میں</w> <w pos="NNUF2N NNUF2O">تفصیلات</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VVNM1N">کرنا</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="PA">آپ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اسوقت</w> <w pos="PK1N VVYM1N QQ PK2N">کیا</w> <w pos="NNUF1N NNUF1O">مدد</w> 
<w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VV0">کر</w> <w pos="VRM2">رہے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos="?">؟</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="II PPM1N">میں</w> <w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">آپکے</w> <w pos="NNUM1O">خاندان</w> 
<w pos="IIM2N">کے</w> <w pos="NNUM2N">لوگ</w> <w pos="CC RD">اور</w> <w pos="NNUM2N NNUM1N NNUM1O">دوست</w> 
<w pos="RR">وغیرہ</w> <w pos="RM">بھی</w> <w pos="JJU">شامل</w> <w pos="VHHV2 VHHM2">ہیں</w> 
</s>
<s>
<w pos="PA">آپ</w> <w pos="NNUM1N RR NNUM1O">روز</w> <w pos="NNMM1N">مرہ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="NNMF1O NNMF1N">آسانی</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">انجام</w> 
<w pos="VV0 VX0">دے</w> <w pos="VRM2">رہے</w> <w pos="FX">ہیں یا</w> <w pos="RMN">نہیں</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="PA">آپ</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">رہائش</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> 
<w pos="NNUF2O">ضروریات</w> <w pos="IIM1O">کے</w> <w pos="RR">مطابق</w> <w pos="VHHV1">ہے</w> 
<w pos="CC CCC">یا</w> <w pos="RMN">نہیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="PA">آپ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">مزہبی</w> <w pos="CC RD">اور</w> 
<w pos="VVTF1N VVTF1O VVTF2N VVTF2O">ثقافتی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پس</w> <w pos="JJU NNUM1O NNUF1O RR">منظر</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="NNUM1O RR">بارے</w> <w pos="II">میں</w> <w pos="NNUF2N NNUF2O NNUM2N NNUF1N NNUF1O">معلومات</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> 
<w pos="VVNM1N">کرنا</w> <w pos=".">۔</w> </s>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="PNN">کوئی</w> 
<w pos="JJU">خاص</w> <w pos="NNMF2N">معذوریاں</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="CC CCC">یا</w> 
<w pos="PA">آپ</w> <w pos="II">کو</w> <w pos="PNO">کسی</w> <w pos="JJU">خاص</w> 
<w pos="NNMF1O">تبدیلی</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">ضرورت</w> <w pos="VHHV1">ہے</w> 
<w pos="XT">تو</w> <w pos="PNO">کسی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سپیشلسٹ</w> <w pos=")">)</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ماہر</w> <w pos="(">(</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="PA">آپ</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="VVNM1O">ملنے</w> <w pos="IIM1N">کا</w> <w pos="NNUM1N">انتظام</w> 
<w pos="VVNM1N">کرنا</w> <w pos=".">۔</w> </s>
<s>
<w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">آپکے</w> <w pos="AU FB">جی</w> <w pos="VVYF2N VV0 VVIT1 VVYF1N VVYF1O VVYF2O">پی</w> <w pos="CC CCC">یا</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1O NNUM1O">بھال</w> <w pos="II">میں</w> 
<w pos="JJU">شامل</w> <w pos="PNO">کسی</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">فرد</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUF1O NNUF1N NNUF2N">بات</w> <w pos="NNUF1N NNUF1O">چیت</w> <w pos="VVNM1N">کرنا</w> 
<w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="JJM1O">سارے</w> <w pos="NNUM1O">عمل</w> <w pos="II">میں</w> 
<w pos="RM">صرف</w> <w pos="JD">چند</w> <w pos="NNUM1N NNUM2N NNUM1O NNUM2O">دن</w> <w pos="VVSV2 VVSM2">لگیں</w> 
<w pos="VGM2">گے</w> <w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تحقیقات</w> <w pos="JJU">مکمل</w> <w pos="VHNM1O">ہونے</w> <w pos="IIM1O">کے</w> 
<w pos="RR">بعد</w> <w pos="PA">آپ</w> <w pos="CC RD">اور</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> 
<w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1O NNUM1O">بھال</w> <w pos="II">میں</w> <w pos="JJU">شامل</w> 
<w pos="NNUM2O">افراد</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1N RR NNUM1O">ساتھ</w> <w pos="NNUF1O NNUF1N NNUF2N">بات</w> 
<w pos="NNUF1O">چیت</w> <w pos="IIM1O">کے</w> <w pos="RR">بعد</w> <w pos="JDNU">ایک</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کیئر</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پلان</w> <w pos=")">)</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> 
<w pos="NNUF1O NNUM1O">بھال</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">منصوبہ</w> <w pos="(">(</w> 
<w pos="NNUF1N NNUF1O">ترتیب</w> <w pos="VVYM1N">دیا</w> <w pos="VXSM1">جاۓ</w> <w pos="VGM1">گا</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="RJ NNUF1O">جہاں</w> <w pos="II">تک</w> <w pos="JJU">ممکن</w> <w pos="VH0">ہو</w> 
<w pos="VXYM1N">سکا</w> <w pos="NNMM2V NNMF2V NNUM2V NNUF2V VVST2 VVIT2">آپکو</w> <w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> <w pos="NNUM1O NNUM1N NNUM2N">گھر</w> 
<w pos="CC CCC">یا</w> <w pos="NNUM1O NNMF1O">کمیونٹی</w> <w pos="II">میں</w> <w pos="XH">ہی</w> 
<w pos="FF">سروسز</w> <w pos="JJU">فراہم</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> <w pos="VXSV2 VVSV2 VVSM2 VXSM2">جائیں</w> 
<w pos="VGF2 VGF1">گی</w> <w pos=".">۔</w> </s>
<s>
<w pos="CC">لیکن</w> <w pos="JD">چند</w> <w pos="NNUM2N">حالات</w> <w pos="II PPM1N">میں</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">نرسنگ</w> <w pos="CC CCC">یا</w> <w pos="JJU NNUM1O NNUF1O RR">ریذیڈنشئل</w> <w pos="II">میں</w> 
<w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1N NNUM1N NNUF1O NNUM1O">بھال</w> <w pos="JJU">فراہم</w> <w pos="VVNM1N">کرنا</w> 
<w pos="JJU">بہترین</w> <w pos="VHSV1 VHST1">ہو</w> <w pos="VGM1">گا</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> <w pos="NNMF1O JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">مرضی</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">بغیر</w> 
<w pos="PNN">کوئی</w> <w pos="NNMM1N">فیصلہ</w> <w pos="RMN">نہیں</w> <w pos="PK1N VVYM1N PK2N">کیا</w> 
<w pos="VXSM1">جاۓ</w> <w pos="VGM1">گا</w> <w pos=",">،</w> <w pos="CC RD">اور</w> 
<w pos="RMN JDNU">نہ</w> <w pos="XH">ہی</w> <w pos="JDNUO">دوسروں</w> <w pos="II">کو</w> 
<w pos="NNMM1O NNMM1V NNMM2N JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">آپکے</w> <w pos="NNUM1O RR">بارے</w> <w pos="II">میں</w> <w pos="NNUF2N NNUF2O NNUM2N NNUF1N NNUF1O">معلومات</w> 
<w pos="JJU">فراہم</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> <w pos="VXSV2 VVSV2 VVSM2 VXSM2">جائیں</w> <w pos="VGF2 VGF1">گی</w> 
<w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos=")">)</w> <w pos="PY1O PV1O">اس</w> <w pos="NNMM1O">سلسلے</w> <w pos="II">میں</w> 
<w pos="JD">مزید</w> <w pos="NNUF2N NNUF2O NNUM2N NNUF1N NNUF1O">معلومات</w> <w pos="QUOTE">"</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ریذیڈنشئل</w> 
<w pos="CC CCC">یا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">نرسنگ</w> <w pos="FF">ہوم</w> <w pos="IIM1N">کا</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">انتخاب</w> <w pos="VVNM1N">کرنا</w> <w pos="QUOTE">"</w> <w pos="JJU">نامی</w> 
<w pos="JJU NNUM1O NNUF1O RR">لیفلٹ</w> <w pos="II">میں</w> <w pos="JJU">موجود</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos="(">(</w> <w pos=".">۔</w> </s>
</p>
<p>
<head>
<s>
<w pos="FF">سروسز</w> <w pos="IIF1O">کی</w> <w pos="NNUF1O">فیس</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ادائیگی</w> </s>
</head>
</p>
<p>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اسوقت</w> <w pos="NNUM1N NNUM1O">مانچسٹر</w> <w pos="FF">سٹی</w> <w pos="NNUM1O NNUM1N">کونسل</w> 
<w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O VVYF1O VVYF2O">کی</w> <w pos="LL">زیادہ</w> <w pos="JJU">تر</w> <w pos="FF">سوشل</w> 
<w pos="FF">سروسز</w> <w pos="JJU">مفت</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="II">کو</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ریذیڈنشئل</w> 
<w pos="FF">ہوم</w> <w pos="II PPM1N">میں</w> <w pos="VVNM1O VVSV1 VVYM2N VVNM2 VVYM1O VVYM2O">جانے</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">خواہش</w> <w pos="CC RD">اور</w> <w pos="NNUF1N NNUF1O">ضرورت</w> <w pos="VHHV1">ہے</w> 
<w pos="XT">تو</w> <w pos="PA">آپ</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">آمدن</w> <w pos=",">،</w> <w pos="NNUF1O NNUF1N">بچت</w> <w pos="CC RD">اور</w> 
<w pos="JJU NNUM1O NNUF1O RR">جائیداد</w> <w pos="IIF2N IIF2O">کی</w> <w pos="NNUF2N NNUF2O">تفصیلات</w> <w pos="NNMF1N NNMF1O JJF1N JJF2N VVYF1N VVYF2N">پوچھی</w> 
<w pos="VXSV2 VVSV2 VVSM2 VXSM2">جائیں</w> <w pos="VGF2 VGF1">گی</w> <w pos=".">۔</w> </s>
<s>
<w pos="PY1N PY2N">یہ</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">آپکی</w> <w pos="JJU NNUM1O NNUF1O RR">اسیسمنٹ</w> <w pos="IIM1N">کا</w> 
<w pos="NNMM1N">حصہ</w> <w pos="VHHV1">ہے</w> <w pos="CC">اور</w> <w pos="PA">آپ</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="PNN PNO">کچھ</w> <w pos="NNUM2N">اخراجات</w> <w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> 
<w pos="RR">پاس</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUF1N NNUF1O">ادا</w> <w pos="VVNM1O">کرنے</w> 
<w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O VVYF1O VVYF2O">کی</w> <w pos="NNUM1N NNUM1O">توقع</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> <w pos="VXSM1">جاۓ</w> 
<w pos="VGF2 VGF1">گی</w> <w pos=".">۔</w> </s>
</p>
</body>
</text>
</cesDoc>
