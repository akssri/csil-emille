<cesDoc id="urd-w-social-learning" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-social-learning.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Raheela Iqbal</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>01-12-03</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Services for people with learning disabilities</h.title>
<h.author>Manchester City Council</h.author>
<imprint>
<pubPlace>Manchester</pubPlace>
<publisher>Manchester City Council</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>00-12-01</date>
</creation>
<langUsage>Translated into Urdu from original English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS)</writingSystem>
</wsdUsage> 
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality> 
</textClass>
<translations>
<translation trans.loc="parallel file identifier" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>
<text>
﻿<body>
<p>
<head>
<s>
<w pos="VVNM1O">سیکھنے</w> <w pos="II PPM1N">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مشکلات</w> <w pos="JXVM2N JXVM2O JXVM1O">والے</w> 
<w pos="NNUM2O">لوگوں</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> <w pos="FF">سروسز</w> 
</s>
</head>
</p>
<p>
<head>
<s>
<w pos="NNUF1N NNUF1O">مدد</w> <w pos="RKJ JDKM1O JDKM2N JDKM2O">کیسے</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VVSM2 VVSV2">کریں</w> 
</s>
</head>
</p>
<p>
<s>
<w pos="NNUM1O">مانچسٹر</w> <w pos="II">میں</w> <w pos="VVNM1O">سیکھنے</w> <w pos="II PPM1N">میں</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">معذور</w> <w pos="NNUM2O">افراد</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="FF">سروسز</w> <w pos=",">،</w> <w pos="JJU">مقامی</w> <w pos="FF">اتھارٹی</w> 
<w pos=",">،</w> <w pos="FF">ہیلتھ</w> <w pos="FF">سروسز</w> <w pos=",">،</w> 
<w pos="NNMM1N">رضاکارانہ</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پرائیویٹ</w> <w pos="JJU NNUM1O NNUF1O RR">سیکٹر</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1N RR NNUM1O">ساتھ</w> <w pos="JDNU">ایک</w> <w pos="JJM2N RRJ JJM1O JJM2O">اچھے</w> 
<w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VVNM1O">کرنے</w> <w pos="IIF1O">کی</w> <w pos="NNUF1O">خاصیت</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">حامل</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="PY1N PY2N">یہ</w> <w pos="JD">تمام</w> <w pos="NNMM1O">ادارے</w> <w pos="JJU">انسانی</w> 
<w pos="NNUM2N">حقوق</w> <w pos="CC RD">اور</w> <w pos="VVNM1O">سیکھنے</w> <w pos="II PPM1N">میں</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">معذور</w> <w pos="NNUM2O">افراد</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مخصوص</w> 
<w pos="NNUF2O">ضروریات</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">پہچان</w> <w pos="II CC">پر</w> 
<w pos="FF">سروسز</w> <w pos="JJU">فراہم</w> <w pos="VVTM2N">کرتے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos="IB">تا</w> <w pos="CS">کہ</w> <w pos="PV2O PY2O">ان</w> <w pos="NNUM2O">افراد</w> 
<w pos="II">کو</w> <w pos="PGRF1N PGRF1O PGRF2O PGRF2N">اپنی</w> <w pos="FF">لوکل</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کمیونٹیز</w> 
<w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O VVYF1O VVYF2O">کی</w> <w pos="JJU">عام</w> <w pos="NNUM1O NNUM1N">سر</w> <w pos="NNMF2O">گرمیوں</w> 
<w pos="II">میں</w> <w pos="NNMM1N">حصہ</w> <w pos="VVNM1O">لینے</w> <w pos="IIM1N">کا</w> 
<w pos="NNUM1N">موقع</w> <w pos="VV0 NNUM1N NNUM1O">مل</w> <w pos="VXSV1 VXYM2N VVYM1O VVYM2N VVYM2O VVST1 VVSV1 VXYM1O VXYM2O VXST1">سکے</w> <w pos=".">۔</w> 
</s>
</p>
<p>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="CC CCC">یا</w> <w pos="PNN">کوئی</w> 
<w pos="CC RD">اور</w> <w pos="PNO">کسی</w> <w pos="JDYM1O RYJ JDYM2N JDYM2O">ایسے</w> <w pos="JJU NNUM1O NNUF1O RR">فرد</w> 
<w pos="II">کو</w> <w pos="VVTM2N">جانتے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="PJ1O">جس</w> 
<w pos="IIC">ے</w> <w pos="VVNM1O">سیکھنے</w> <w pos="II PPM1N">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مشکلات</w> 
<w pos="NNUM1N RR NNUM1O">پیش</w> <w pos="VVTF2N">آتی</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="CC">اور</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIC">ے</w> <w pos="NNUF1O">مدد</w> <w pos="IIF1N IIF1O">کی</w> 
<w pos="NNUF1N NNUF1O">ضرورت</w> <w pos="VHHV1">ہے</w> <w pos="XT">تو</w> <w pos="RM">پھر</w> 
<w pos="PA">آپ</w> <w pos="II">کو</w> <w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> <w pos="JJU">مقامی</w> 
<w pos="FF">سوشل</w> <w pos="FF">سروسز</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1O NNUF1O RR">ڈیپارٹمنٹ</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1N RR NNUM1O">ساتھ</w> <w pos="NNMM1N">رابطہ</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">قائم</w> 
<w pos="VVNM1N">کرنا</w> <w pos="VC1 VVIA VC2">چاہئے</w> <w pos=")">)</w> <w pos="PV2O PY2O">ان</w> 
<w pos="IIM1N">کا</w> <w pos="NNMM1N">پتہ</w> <w pos="PY1O PV1O">اس</w> <w pos="JJU NNUM1O NNUF1O RR">لیفلٹ</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O JJU RR NNUM1N">آخر</w> <w pos="II CC">پر</w> <w pos="VVYM1N">دیا</w> 
<w pos="VXYM1N VVYM1N">گیا</w> <w pos="VHHV1">ہے</w> <w pos="(">(</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="CS">اگر</w> <w pos="JJU">لازمی</w> <w pos="VHYM1N">ہوا</w> <w pos="XT">تو</w> 
<w pos="PV2N PV1N">وہ</w> <w pos="PA">آپ</w> <w pos="II">کو</w> <w pos="NNUM1N NNUM1O">مشورہ</w> 
<w pos="CC RD">اور</w> <w pos="NNUF2N NNUF2O NNUM2N NNUF1N NNUF1O">معلومات</w> <w pos="VVSV2 VVSM2">دیں</w> <w pos="VGM2">گے</w> 
<w pos="CC CCC">یا</w> <w pos="RM">پھر</w> <w pos="PA">آپ</w> <w pos="IIF1N IIF1O">کی</w> 
<w pos="NNUF1N NNUF1O">ضرورت</w> <w pos="VVNM1O">جانچنے</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="PA">آپ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1N RR NNUM1O">ساتھ</w> <w pos="NNUF1O NNUF1N NNUF2N">بات</w> 
<w pos="NNUF1N NNUF1O">چیت</w> <w pos="VVSM2 VVSV2">کریں</w> <w pos="VGM2">گے</w> <w pos=",">،</w> 
<w pos=")">)</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">لیفلٹ</w> <w pos="VVIA NNMM1O NNMM1V NNMM2N">دیکھیئے</w> <w pos=":">:</w> 
<w pos="FF">سوشل</w> <w pos="FF">سروسز</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUF1N NNUF1O">مدد</w> 
<w pos="RKJ JDKM1O JDKM2N JDKM2O">کیسے</w> <w pos="JJU NNUM1N NNUM1O">حاصل</w> <w pos="VVSM2 VVSV2">کریں</w> <w pos="(">(</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="NNUM1N JJU NNUM1O">محسوس</w> <w pos="VVTM2N">کرتے</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos="CS">کہ</w> <w pos="PA">آپ</w> <w pos="II">کو</w> 
<w pos="RM">صرف</w> <w pos="FF">ہیلتھ</w> <w pos=")">)</w> <w pos="NNUF1O NNUF1N">صحت</w> 
<w pos="(">(</w> <w pos="CC RD">اور</w> <w pos="NNMM1N">رضاکارانہ</w> <w pos="FF">سروسز</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">درکار</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="XT">تو</w> <w pos="RM">پھر</w> 
<w pos="PA">آپ</w> <w pos="II">کو</w> <w pos="NNMM1N">متعلقہ</w> <w pos="FF">سروسز</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNMM1N">براہ</w> <w pos="NNUM1O NNUM1N">راست</w> <w pos="NNMM1N">رابطہ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">قائم</w> <w pos="VVNM1N">کرنا</w> <w pos="VC1 VVIA VC2">چاہئے</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="PGRF1N PGRF1O">اپنی</w> <w pos="NNUF1N NNUF1O">ضرورت</w> 
<w pos="NNMM1O NNMM2N JJM2N VVYM2N VVST1 VVSV1 RRJ">جانچے</w> <w pos="VXNM2O VXNM1O VVNM1O VVSV1 VVYM2N VVNM2 VVYM1O VVYM2O VXNM2">جانے</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">اہلیت</w> 
<w pos="VVTM2N">رکھتے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="XT">تو</w> <w pos="NNMM1N">مطلوبہ</w> 
<w pos="NNUF1O">مدد</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> <w pos="PA">آپ</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JD PNN">کئی</w> <w pos="NNUF1O">قسم</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سوالات</w> <w pos="NNMM1O NNMM2N JJM2N VVYM2N VVST1 VVSV1 RRJ">پوچھے</w> <w pos="VXSV2 VVSV2 VVSM2 VXSM2">جائیں</w> <w pos="VGM2">گے</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="PA">آپ</w> <w pos="II">کو</w> <w pos="JDNU">ایک</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کیئر</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پلان</w> <w pos=")">)</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1O NNUM1O">بھال</w> 
<w pos="IIM1N">کا</w> <w pos="NNMM1N">منصوبہ</w> <w pos="(">(</w> <w pos="RM">بھی</w> 
<w pos="NNUM1N RR NNUM1O">پیش</w> <w pos="PK1N VVYM1N PK2N">کیا</w> <w pos="VV0 VX0 VVIT1 VXIT1">جا</w> <w pos="VXTM1N VVTM1N">سکتا</w> 
<w pos="VHHV1">ہے</w> <w pos="PJ1O">جس</w> <w pos="II PPM1N">میں</w> <w pos="JJU">مندرجہ</w> 
<w pos="JJU">ذیل</w> <w pos="II PPM1N">میں</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="PNN">کوئی</w> 
<w pos="JDNU">ایک</w> <w pos="FF">سروس</w> <w pos="JJU">شامل</w> <w pos="VH0">ہو</w> 
<w pos="VXTF1N">سکتی</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> </s>
</p>
<p>
<head>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">رہائش</w> <w pos="CC RD">اور</w> <w pos="NNMM1N">مددگارانہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">بقید</w> 
<w pos="NNUM1N NNUM1O">حیات</w> </s>
</head>
</p>
<p>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="NNUM1N NNUM1O">وقت</w> <w pos="NNUM1N NNUM1O">مانچسٹر</w> <w pos="FF">سوشل</w> 
<w pos="FF">سروسز</w> <w pos="JD PNN">کئی</w> <w pos="NNUM1O NNUF1O">طرح</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">رہائشی</w> <w pos="FF">سروسز</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="NNUM1N NNUM1O">فنڈ</w> <w pos="JJU">مہیا</w> <w pos="VVTF1N">کرتی</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="JDNU">گیارہ</w> <w pos="FF">نیٹ</w> <w pos="FF">ورک</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ہاؤسنگ</w> 
<w pos="NNUF2N VVSM2 VVSV2 VVYF2N">سکیمیں</w> <w pos="PJ2N PJ1N">جو</w> <w pos="CS">کہ</w> <w pos="JJM1O">پورے</w> 
<w pos="NNUM1O">شہر</w> <w pos="II">میں</w> <foreign lang="eng">
<w pos="JDNU">200</w> </foreign>
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">زائد</w> <w pos="NNUM2O">لوگوں</w> <w pos="IIM1N">کے</w> 
<w pos="RR">لئے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سٹاف</w> <w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">مکانات</w> 
<w pos="JJU">فراہم</w> <w pos="VVTF2N">کرتی</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="JDNU">ایک</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ایڈلٹ</w> <w pos="FF">ہوم</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">فائینڈنگ</w> 
<w pos="NNUF1N NNUF1O">سکیم</w> <w pos=")">)</w> <w pos="NNMM2O NNUM2O NNUF2O">بالغوں</w> <w pos="IIM1N">کے</w> 
<w pos="RR">لئے</w> <w pos="JJU NNUM1O NNUF1O RR">مکان</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1O NNUF1N">تلاش</w> 
<w pos="(">(</w> <w pos="VHHV1">ہے</w> <w pos="CC">اور</w> <w pos="PY1N PY2N">یہ</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">انفرادی</w> <w pos="NNUM2O">لوگوں</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="PJ2N PJ1N">جو</w> <w pos="JDNU">ایک</w> <w pos="NNUM1O">گھر</w> <w pos="II">میں</w> 
<w pos="VVNM1N">رہنا</w> <w pos="JJU NNUF1N NNUF1O">پسند</w> <w pos="VVTM2N">کرتے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos=",">،</w> <w pos="FF">سروسز</w> <w pos="JJU">مہیا</w> <w pos="VVTF1N">کرتی</w> 
<w pos="VHHV1">ہے</w> <w pos=".">۔</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="NNUF1O">سکیم</w> <w pos="II">نے</w> <w pos="RRJ JDNM1O JDNM2N JDNM2O">پہلے</w> 
<w pos="XH">ہی</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">بطور</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> 
<w pos="NNUF1N NNUM1N NNUF1O NNUM1O">بھال</w> <w pos="CC CCC">یا</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ایمرجنسی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پلیسمنٹ</w> 
<w pos=")">)</w> <w pos="JJM1O">تھوڑے</w> <w pos="NNMM1N">عرصہ</w> <w pos="IIM1N">کے</w> 
<w pos="RR">لئے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">رہائش</w> <w pos="(">(</w> <w pos="IIM1N">کا</w> 
<w pos="NNUM1N">کام</w> <w pos="NNUM1N JJU NNUM1O">شروع</w> <w pos="VV0 VVIT1">کر</w> <w pos="VVYM1N VXYM1N">رکھا</w> 
<w pos="VHHV1">ہے</w> <w pos=".">۔</w> </s>
<s>
<w pos="JJM1O">تھوڑے</w> <w pos="NNMM1O">عرصے</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1N NNUM1N NNUF1O NNUM1O">بھال</w> <w pos=")">)</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ریسپائیٹ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کیئر</w> <w pos="(">(</w> <w pos="JJM1O">پورے</w> <w pos="NNUM1O">شہر</w> 
<w pos="II">میں</w> <w pos="JDNU">پانچ</w> <w pos="JJU NNUM1O NNUF1O RR">مکانات</w> <w pos="II">میں</w> 
<w pos="JJU">فراہم</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> <w pos="VXTF1N VVTF1N">جاتی</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="FF">ساؤتھ</w> <w pos="NNUM1O">مانچسٹر</w> <w pos="II">میں</w> <w pos="JDNU">ایک</w> 
<w pos="JJU NNUM1O NNUF1O RR">مکان</w> <w pos="II">میں</w> <w pos="JJU">قابل</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">اعتراض</w> 
<w pos="NNUM1N NNUM1O">چال</w> <w pos="JJU NNUM1O NNUF1O RR">ڈھال</w> <w pos="IIM2O">کے</w> <w pos="NNUM2O">لوگوں</w> 
<w pos="IIM1N">کے</w> <w pos="RR">لئے</w> <w pos="RM">بھی</w> <w pos="FF">سروس</w> 
<w pos="JJU">مہیا</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O">کی</w> <w pos="VXTF1N VVTF1N">جاتی</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="FF">ہوم</w> <w pos="NNUF1O">سکیم</w> <w pos="IIM1O">کے</w> <w pos="RR">تحت</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">محتصر</w> <w pos="NNMM1O JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">وقفے</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="JDNM2O">دوسرے</w> <w pos="NNMM2O NNUM2O">خاندانوں</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1N RR NNUM1O">ساتھ</w> 
<w pos="NNMM2O">بچوں</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1O NNUM1O">بھال</w> 
<w pos="IIM1N">کا</w> <w pos="NNUM1N">انتظام</w> <w pos="RM">بھی</w> <w pos="PK1N VVYM1N PK2N">کیا</w> 
<w pos="VXTM1N VVTM1N VXYM1N">جاتا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> </s>
</p>
<p>
<head>
<s>
<w pos="FF">سٹی</w> <w pos="NNUM1O NNUM1N">کونسل</w> <w pos="CC RD">اور</w> <w pos="FF">ہیلتھ</w> 
<w pos="FF">سروس</w> </s>
</head>
</p>
<p>
<head>
<s>
<w pos="NNMM1N">مددگارانہ</w> <w pos="FF">سروس</w> </s>
</head>
</p>
<p>
<s>
<w pos="NNUM1O NNUM1N">نارتھ</w> <w pos="CC RD">اور</w> <w pos="FF">سنٹرل</w> <w pos="NNUM1O">مانچسٹر</w> 
<w pos="IIM1O">کے</w> <w pos="RR">پاس</w> <w pos="FF">سوشل</w> <w pos="FF">ورکرز</w> 
<w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مینیجرز</w> <w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ماہر</w> 
<w pos="JJU">خصوصی</w> <w pos="FF">ہوم</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کیئر</w> <w pos="FF">ورکرز</w> 
<w pos="CC RD">اور</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">انتظامی</w> <w pos="JJU NNUM1O NNUF1O RR">سٹاف</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="NNUF2N VVSM2 VVSV2 VVYF2N">ٹیمیں</w> <w pos="JJU">موجود</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="JD">ہر</w> <w pos="NNUM1O NNUF1O">ڈسٹرکٹ</w> <w pos="IIM1O">کے</w> <w pos="RR">پاس</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مینیجر</w> <w pos=",">،</w> <w pos="NNUF1N NNUF1O">نرس</w> <w pos=",">،</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">آکوپیشنل</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تھیراپسٹ</w> <w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">فزیوتھیراپسٹ</w> 
<w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سپیچ</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">لینگوئج</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تھیراپسٹ</w> <w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سائیکالوجسٹ</w> <w pos="CC RD">اور</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">انتظامی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سٹاف</w> <w pos="II CC">پر</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مشتمل</w> 
<w pos="NNUM1O NNMF1O NNMF1N NNMF2N NNMM1N NNUM1N">کمیونٹی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سپورٹ</w> <w pos="NNUM1N NNUF1N NNUF1O NNUM1O">ٹیم</w> <w pos="RM">بھی</w> 
<w pos="VHHV1">ہے</w> <w pos=".">۔</w> </s>
<s>
<w pos="JJU">مختلف</w> <w pos="NNUF2O">جگہوں</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> 
<w pos="VVNM1O">کرنے</w> <w pos="IIM1O">کے</w> <w pos="RR">باوجود</w> <w pos="FF">ہیلتھ</w> 
<w pos="CC RD">اور</w> <w pos="FF">سوشل</w> <w pos="FF">سروسز</w> <w pos="IIM1N">کا</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سٹاف</w> <w pos="PRC">آپس</w> <w pos="II PPM1N">میں</w> <w pos="VV0 NNUM1N NNUM1O">مل</w> 
<w pos="VX0 VV0 VVIT1">کر</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VVTM1N">کرتا</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="NNMM2N NNMM1O NNMM1V">پتے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">براۓ</w> <w pos="NNMM1N">رابطہ</w> <w pos=":">:</w> 
</s>
<s>
<w pos="FF">سنٹرل</w> <w pos="NNUM1N NNUM1O">مانچسٹر</w> <w pos="NNUM1O NNMF1O NNMF1N NNMF2N NNMM1N NNUM1N">کمیونٹی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سپورٹ</w> 
<w pos="NNUM1N NNUF1N NNUF1O NNUM1O">ٹیم</w> </s>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">راس</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پلیس</w> </s>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ایکڈ</w> <w pos="FF">کلوز</w> </s>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">آرڈوک</w> </s>
<s>
<w pos="NNUM1N NNUM1O">مانچسٹر</w> </s>
<foreign lang="eng">
<w pos="FX">M12</w> <w pos="FX">4AN</w> </foreign>
<s>
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ٹیلی</w> <w pos="NNUM1O NNUM1N NNUM2N RR">فون</w> <w pos=":">:</w> <foreign lang="eng">
<w pos="JDNU">273</w> <w pos="JDNU">5412</w> </foreign>
</s>
<s>
<w pos="NNUM1O NNUM1N">نارتھ</w> <w pos="NNUM1N NNUM1O">مانچسٹر</w> <w pos="NNUM1O NNMF1O NNMF1N NNMF2N NNMM1N NNUM1N">کمیونٹی</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سپورٹ</w> 
<w pos="NNUM1N NNUF1N NNUF1O NNUM1O">ٹیم</w> </s>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ریسورس</w> <w pos="NNUM1O NNUM1N">سنٹر</w> </s>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">بیچ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ماؤنٹ</w> </s>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ہار</w> <w pos="II CC">پر</w> <w pos="VHHV1">ہے</w> </s>
<s>
<w pos="NNUM1N NNUM1O">مانچسٹر</w> </s>
<foreign lang="eng">
<w pos="FX">M9</w> <w pos="FX">1XU</w> </foreign>
<s>
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ٹیلی</w> <w pos="NNUM1O NNUM1N NNUM2N RR">فون</w> <w pos=":">:</w> <foreign lang="eng">
<w pos="JDNU">205</w> <w pos="JDNU">1364</w> <w pos="~">/</w> <w pos="JDNU">4926</w> 
</foreign>
</s>
</p>
<p>
<s>
<w pos="FF">ساؤتھ</w> <w pos="NNUM1O">مانچسٹر</w> <w pos="II">میں</w> <w pos="JDNU">ایک</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مشترک</w> <w pos="FF">سروس</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="FF">ہیلتھ</w> <w pos="CC RD">اور</w> <w pos="FF">سوشل</w> <w pos="FF">سروسز</w> 
<w pos="IIM1N">کا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سٹاف</w> <w pos="JDNU">ایک</w> <w pos="XH">ہی</w> 
<w pos="NNUM1O NNUM1N">دفتر</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VVTM1N">کرتا</w> 
<w pos="VHHV1">ہے</w> <w pos=".">۔</w> </s>
<s>
<w pos="FF">ساؤتھ</w> <w pos="NNUM1O">مانچسٹر</w> <w pos="IIM1O">کے</w> <w pos="RR">پاس</w> 
<w pos="JDNU">ایک</w> <w pos="NNMM1N">کتابچہ</w> <w pos="RM">بھی</w> <w pos="JJU">دستیاب</w> 
<w pos="VHHV1">ہے</w> <w pos="PJ1O">جس</w> <w pos="II PPM1N">میں</w> <w pos="PV2O PY2O">ان</w> 
<w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="FF">سروسز</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJF2N JJF2O">پوری</w> 
<w pos="NNUF2N NNUF2O">تفصیلات</w> <w pos="VVYF1N VVYF2N">دی</w> <w pos="VXYF2N VVYF2N">گئی</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="FF">سروس</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O RR">بارے</w> 
<w pos="II">میں</w> <w pos="JD RR">زیادہ</w> <w pos="NNUF2O NNUF1O">معلومات</w> <w pos="IIM1N">کے</w> 
<w pos="RR">لئے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ویدن</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">شاء</w> <w pos="JJU NNUM1O NNUF1O RR">ڈسٹرکٹ</w> 
<w pos="IIM2O">کے</w> <w pos="NNUM2O">دفاتر</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNMM1N">رابطہ</w> 
<w pos="VVSM2 VVSV2">کریں</w> <w pos=")">)</w> <w pos="PV2O PY2O">ان</w> <w pos="IIM1N">کا</w> 
<w pos="NNMM1N">پتہ</w> <w pos="CC RD">اور</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ٹیلی</w> <w pos="NNUM1O NNUM1N NNUM2N RR">فون</w> 
<w pos="NNUM1N NNUM1O">نمبر</w> <w pos="PY1O PV1O">اس</w> <w pos="JJU NNUM1O NNUF1O RR">لیفلٹ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="NNUM1O JJU RR">آخر</w> <w pos="II">میں</w> <w pos="VVYM1N">دیا</w> <w pos="VXYM1N VVYM1N">گیا</w> 
<w pos="VHHV1">ہے</w> <w pos="(">(</w> <w pos=".">۔</w> </s>
</p>
<p>
<head>
<s>
<w pos="FF">ڈے</w> <w pos="FF">سروسز</w> </s>
</head>
</p>
<p>
<s>
<w pos="NNUM1O NNUM1N">شہر</w> <w pos="RR">بھر</w> <w pos="II PPM1N">میں</w> <w pos="JDNU">پانچ</w> 
<w pos="FF">ڈے</w> <w pos="FF">سنٹر</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="PJ2N PJ1N">جو</w> 
<w pos="JD PNN">کئی</w> <w pos="NNUF1O NNUF1N NNUM2N">قسم</w> <w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O VVYF1O VVYF2O">کی</w> <w pos="NNUM1O NNUM1N">سر</w> 
<w pos="NNMF2N">گرمیاں</w> <w pos="JJU">فراہم</w> <w pos="VVTM2N">کرتے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="JJU">مقامی</w> <w pos="NNUM1O NNUM1N">طور</w> <w pos="II CC">پر</w> <w pos="NNUF1N JDNU JJU NNUF1O VVST2 VVIT2">دو</w> 
<w pos="FF">ڈے</w> <w pos="FF">سروسز</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ہار</w> <w pos="II CC">پر</w> 
<w pos="VHHV1">ہے</w> <w pos="CC">اور</w> <w pos="NNMM2V NNMF2V NNUM2V NNUF2V VVST2 VVIT2">فیلو</w> <w pos="NNUM1O NNUM1N">فیلڈ</w> 
<w pos="~">/</w> <w pos="JJU NNUM1O NNUF1O RR">رشلم</w> <w pos="II">میں</w> <w pos="JJU">موجود</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos="PJ2N PJ1N">جو</w> <w pos="NNUM2O">لوگوں</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> 
<w pos="FF">ڈے</w> <w pos="CC RD">اور</w> <w pos="JJU">تفریحی</w> <w pos="NNUM1O NNUM1N">سر</w> 
<w pos="NNMF2O">گرمیوں</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1O NNUF1O RR">تکمیل</w> <w pos="II">میں</w> 
<w pos="NNUF1N NNUF1O">مدد</w> <w pos="VVTF2N">کرتی</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ایجوکیشن</w> <w pos="JJU NNUM1O NNUF1O RR">ڈیپارٹمنٹ</w> <w pos="IIM1O">کے</w> <w pos="RR">پاس</w> 
<w pos="JJU">شدید</w> <w pos="VVNM1O">سیکھنے</w> <w pos="II PPM1N">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مشکلات</w> 
<w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="NNMM2O">بچوں</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="JDNU">چھ</w> <w pos="NNUM1O NNUM2N NNUM1N">سکول</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ایڈلٹ</w> 
<w pos=")">)</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">بالغ</w> <w pos="(">(</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ایجوکیشن</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ڈیپارٹمنٹ</w> <w pos="VVNM1O">سیکھنے</w> <w pos="II PPM1N">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مشکلات</w> 
<w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="NNUM2O">لوگوں</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="JD PNN">کئی</w> <w pos="NNUF1O">قسم</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کورسز</w> 
<w pos="JJU">فراہم</w> <w pos="VVTM1N">کرتا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="FF">سوشل</w> <w pos="FF">سروسز</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ڈیپارٹمنٹ</w> <w pos="VVNM1O">سیکھنے</w> 
<w pos="II PPM1N">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مشکلات</w> <w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="NNUM2O">لوگوں</w> 
<w pos="II">کو</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VVNM1O VVNM2">ڈھونڈنے</w> <w pos="CC RD">اور</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIC">ے</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">قائم</w> <w pos="VVNM1O VXNM1O">رکھنے</w> 
<w pos="II PPM1N">میں</w> <w pos="NNUF1O">مدد</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="JDNU">ایک</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ایمپلائمنٹ</w> <w pos=")">)</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">نوکری</w> 
<w pos="(">(</w> <w pos="NNUF1N NNUF1O">سکیم</w> <w pos="II CC">پر</w> <w pos="RM">بھی</w> 
<w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VVTM1N">کرتا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
</s>
</p>
<p>
<head>
<s>
<w pos="NNMM1N">رضاکارانہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سیکٹر</w> </s>
</head>
</p>
<p>
<s>
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">والینٹری</w> <w pos=")">)</w> <w pos="NNMM1N">رضاکارانہ</w> <w pos="(">(</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">سیکٹر</w> <w pos="VVNM1O">سیکھنے</w> <w pos="II PPM1N">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مشکلات</w> 
<w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="NNUM2O">لوگوں</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="RD JD">بہت</w> <w pos="FF">سی</w> <w pos="FF">سروسز</w> <w pos="JJU">مہیا</w> 
<w pos="VVTM1N">کرتا</w> <w pos="VHHV1">ہے</w> <w pos="CC">اور</w> <w pos="PV2O PY2O">ان</w> 
<w pos="FF">سروسز</w> <w pos="II PPM1N">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سپورٹڈ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ہاؤسنگ</w> 
<w pos=")">)</w> <w pos="JDYM1O RYJ JDYM2N JDYM2O">ایسے</w> <w pos="NNUM1O NNUM1N NNUM2N">گھر</w> <w pos="RJ NNUF1O NNUF1N">جہاں</w> 
<w pos="NNUM1O NNUM1N">کارکن</w> <w pos="NNUF1N NNUF1O">مدد</w> <w pos="VVTM2N">کرتے</w> <w pos="VHHV2 VHHM2">ہیں</w> 
<w pos="(">(</w> <w pos=",">،</w> <w pos="FF">ڈے</w> <w pos="CC RD">اور</w> 
<w pos="JJU">تفریحی</w> <w pos="NNMF2N">سرگرمیاں</w> <w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سیلف</w> 
<w pos="NNMM2V NNMF2V NNUM2V NNUF2V VVST2 VVIT2">ایڈوو</w> <w pos="JDKF1N JDKF2N JDKF1O JDKF2O">کیسی</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">گراپس</w> <w pos=")">)</w> 
<w pos="PV2N PV1N">وہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">گروپس</w> <w pos="PJ1O">جس</w> <w pos="II PPM1N">میں</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ممبران</w> <w pos="JDNU">ایک</w> <w pos="JDNM2N JDNM1O JDNM2O">دوسرے</w> <w pos="II">کو</w> 
<w pos="NNMM1O NNMM2N JJM2N VVYM2N VVST1 VVSV1 RRJ">مشورے</w> <w pos="VVTM2N">دیتے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="(">(</w> 
<w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">سپورٹ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">گروپس</w> <w pos="FF">فار</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">پیرنٹس</w> <w pos=")">)</w> <w pos="NNUM2O">والدین</w> <w pos="IIM1N">کے</w> 
<w pos="RR">لئے</w> <w pos="NNMM1N">مددگارانہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">گروپس</w> <w pos="(">(</w> 
<w pos="RR">وغیرہ</w> <w pos="JJU">شامل</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
</p>
<p>
<s>
<w pos="JJU">مندرجہ</w> <w pos="NNMM1N JJM1N VVYM1N">بالا</w> <w pos="PNO">کسی</w> <w pos="RM">بھی</w> 
<w pos="FF">سروس</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O RR">بارے</w> <w pos="II">میں</w> 
<w pos="JD RR">زیادہ</w> <w pos="NNUF2O NNUF1O">معلومات</w> <w pos="IIM1N">کے</w> <w pos="RR">لئے</w> 
<w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> <w pos="JJU">مقامی</w> <w pos="FF">سوشل</w> <w pos="FF">سروسز</w> 
<w pos="IIM1O">کے</w> <w pos="NNUM1O">دفتر</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNMM1N">رابطہ</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">قائم</w> <w pos="VVSM2 VVSV2">کریں</w> <w pos=".">۔</w> </s>
<s>
<w pos="PV2O PY2O">ان</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNMM2N NNMM1O NNMM1V">پتے</w> <w pos="CC RD">اور</w> 
<w pos="NNUM1O NNUM1N NNUM2N RR">فون</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">نمبرز</w> <w pos="PY1O PV1O">اس</w> <w pos="NNMM1O JJM1O JJM2N JJM2O VVYM1O VVYM2N VVYM2O VVST1 VVSV1 RRJ">صفحے</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JDNF1O JDNF1N">دوسری</w> <w pos="NNUF1O NNUF1N">طرف</w> <w pos="VVIA NNMM1O NNMM2N">دیئے</w> 
<w pos="VVYM2N">گئے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
</p>
</body>
</text>
</cesDoc>
