<cesDoc id="urd-w-health-hiv" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-health-hiv.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Raheela Iqbal</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>01-12-11</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Services for people with HIV/AIDS</h.title>
<h.author>Manchester City Council</h.author>
<imprint>
<pubPlace>Manchester</pubPlace>
<publisher>Manchester City Council</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>01-01-11</date>
</creation>
<langUsage>Translated into Urdu from original English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646"> Universal Multiple-Octet Coded Character Set (UCS)</writingSystem>
</wsdUsage> 
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality> 
</textClass>
<translations>
<translation trans.loc="parallel file identifier" lang="ENG" wsd="ISO8859-1" n="1">
</translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>
<text>
﻿<body>
<p>
<head>
<s>
<w pos="NNMM2O NNUM2O NNUF2O">بیماروں</w> <w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="NNUF1N NNUF1O NNUF1V">لیفلیت</w> 
</s>
</head>
</p>
<p>
<head>
<s>
<w pos="CS">اگر</w> <w pos="NNUM1O">خون</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">نمونہ</w> 
<w pos="VVYM1N">لیا</w> <w pos="VV0 VX0">جا</w> <w pos="VRM1">رہا</w> <w pos="VH0 VHSV1 VHST1 VHIT1">ہو</w> 
<w pos=".">۔</w> <w pos=".">۔</w> <w pos=".">۔</w> 
</s>
</head>
</p>
<p>
<s>
<w pos="PPM2N PPM2O">ہم</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ایچ</w> <w pos="VVYF1N VVYF2N VVYF1O VVYF2O">آئی</w> <w pos="NNMF1N NNMF1O">وی</w> 
<w pos=")">)</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">وائرس</w> <w pos="PJ2N PJ1N">جو</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ایڈز</w> 
<w pos="NNUM1N NNUM1O">پیدا</w> <w pos="VVTM1N">کرتا</w> <w pos="VHHV1">ہے</w> <w pos="(">(</w> 
<w pos="IIF1N IIF2N IIF1O VVYF1N VVYF2N IIF2O VVYF1O VVYF2O">کی</w> <w pos="FF">انفیکشن</w> <w pos="CC RD">اور</w> <w pos="PNN PNO">کچھ</w> 
<w pos="JDNF1O JDNF1N JDNF2O JDNF2N">دوسری</w> <w pos="NNMF1N NNMF1O JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">متعدی</w> <w pos="NNMF2O">بیماریوں</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JJU NNUM1O NNUF1O RR">پھیلاؤ</w> <w pos="II">کو</w> <w pos="VV0">ماپ</w> <w pos="VRM2">رہے</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="PPM2N PPM2O">ہم</w> 
<w pos="JJM2O">چھوٹے</w> <w pos="NNMM2O">بچوں</w> <w pos="NNUF1N NNUF1O NNUF1V">سمیت</w> <w pos="PNN PNO">کچھ</w> 
<w pos="NNUM2O">مریضوں</w> <w pos="IIM1O">کے</w> <w pos="NNUM1O">خون</w> <w pos="NNUM1N NNUM1O">ٹیسٹ</w> 
<w pos="VV0">کر</w> <w pos="VRM2">رہے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
</p>
<p>
<s>
<w pos="CS">اگر</w> <w pos="NNUM1O">خون</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">نمونہ</w> 
<w pos="VVYM1N">لیا</w> <w pos="VV0 VX0">جا</w> <w pos="VRM1">رہا</w> <w pos="VH0 VHSV1 VHST1 VHIT1">ہو</w> 
<w pos="CC">اور</w> <w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="II">کو</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">اعتراض</w> <w pos="RMN JDNU">نہ</w> <w pos="VH0 VHSV1 VHHT2 VHST2 VHST1 AU VHIT1 VHIT2">ہو</w> <w pos="XT">تو</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ایچ</w> <w pos="VVYF1N VVYF2N VVYF1O VVYF2O">آئی</w> <w pos="NNMF1N NNMF1O">وی</w> <w pos="NNUF1N NNUF1O NNUF1V">سمیت</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">متعدی</w> <w pos="NNMF2O">بیماریوں</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O RR">بارے</w> 
<w pos="II">میں</w> <w pos="NNUF2N NNUF2O NNUM2N NNUF1N NNUF1O">معلومات</w> <w pos="JJU">مہیا</w> <w pos="VVNM1O">کرنے</w> 
<w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="PA">آپ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="NNMM2N NNMM1O NNMM2O">بچے</w> <w pos="VHYM2N VHYM2O">ہوۓ</w> <w pos="NNUM1O">خون</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="JDNU">ایک</w> <w pos="JJM1O">چھوٹے</w> <w pos="NNMM1O">حصے</w> <w pos="II CC">پر</w> 
<w pos="JJU">الگ</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUM1N NNUM1O">ٹیسٹ</w> <w pos="PK1N VVYM1N PK2N">کیا</w> 
<w pos="VV0 VX0 VVIT1 VXIT1">جا</w> <w pos="VXTM1N VVTM1N">سکتا</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="JJU">الگ</w> 
<w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUM1N NNUM1O">خون</w> <w pos="RMN">نہیں</w> <w pos="VVYM1N">لیا</w> 
<w pos="VXSM1">جاۓ</w> <w pos="VGM1">گا</w> <w pos=".">۔</w> </s>
<s>
<w pos="PY1O PV1O">اس</w> <w pos="VVNM1O">نمونے</w> <w pos="II CC">پر</w> <w pos="PNN">کوئی</w> 
<w pos="NNUM1N NNUM2N NNUM1O">نام</w> <w pos="RMN">نہیں</w> <w pos="VHSV1 VHST1">ہو</w> <w pos="VGM1">گا</w> 
<w pos="CC">اور</w> <w pos="CS">اگر</w> <w pos="PY1O PV1O">اس</w> <w pos="II">کو</w> 
<w pos="NNUM1N NNUM1O">ٹیسٹ</w> <w pos="PK1N VVYM1N PK2N">کیا</w> <w pos="VXSM1">جاۓ</w> <w pos="VGM1">گا</w> 
<w pos="XT">تو</w> <w pos="PY1O PV1O">اس</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNMM1O RR">ذریعے</w> 
<w pos="RR">واپس</w> <w pos="PA">آپ</w> <w pos="IIM1N">کا</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">سراغ</w> 
<w pos="VVNM1N">لگانا</w> <w pos="CC CCC">یا</w> <w pos="PA">آپ</w> <w pos="II">کو</w> 
<w pos="PY1O PV1O">اس</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">نتیجہ</w> <w pos="VVNM1N">بتانا</w> 
<w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ناممکن</w> <w pos="VHSV1 VHST1">ہو</w> <w pos="VGM1">گا</w> <w pos=".">۔</w> 
</s>
<s>
<w pos="NNMM2N NNMM1O NNMM2O">بچے</w> <w pos="VHYM2N VHYM2O">ہوۓ</w> <w pos="NNUM1N NNUM1O">خون</w> <w pos="II CC">پر</w> 
<w pos="NNUM1N NNUM1O">ٹیسٹ</w> <w pos="VVNM1O">کرنے</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="PA">آپ</w> 
<w pos="II">کو</w> <w pos="VVYF1N VVYF2N">دی</w> <w pos="VXNM1O VVNM1O">جانے</w> <w pos="JXVF1O JXVF1N JXVF2N JXVF2O">والی</w> 
<w pos="VV0 NNUF1N NNUM1N NNUF1O NNUM1O">دیکھ</w> <w pos="NNUF1N NNUM1N NNUF1O NNUM1O">بھال</w> <w pos="CC CCC">یا</w> <w pos="VHNM1O">ہونے</w> 
<w pos="JXVM2N JXVM2O JXVM1O">والے</w> <w pos="NNUM1O NNUM1N">علاج</w> <w pos="II CC">پر</w> <w pos="PNN">کوئی</w> 
<w pos="NNUM1N NNUM1O">اثر</w> <w pos="RMN">نہیں</w> <w pos="VVSV1 VVST1">پڑے</w> <w pos="VGM1">گا</w> 
<w pos="CC">اور</w> <w pos="RMN JDNU">نہ</w> <w pos="XH">ہی</w> <w pos="PY1O PV1O">اس</w> 
<w pos="IIM1N">کا</w> <w pos="PA">آپ</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> 
<w pos="CC CCC">یا</w> <w pos="FF">انشورنس</w> <w pos="II CC">پر</w> <w pos="PNN">کوئی</w> 
<w pos="NNUM1N NNUM1O">اثر</w> <w pos="VVSV1 VVST1">پڑے</w> <w pos="VGM1">گا</w> <w pos=".">۔</w> 
</s>
</p>
<p>
<s>
<w pos="SPXYM">ہمیں</w> <w pos="NNUF1N NNUF1O">امید</w> <w pos="VHHV1">ہے</w> <w pos="CS">کہ</w> 
<w pos="PA">آپ</w> <w pos="NNUF1N NNUF1O">مدد</w> <w pos="VVSM2 VVSV2">کریں</w> <w pos="VGM2">گے</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="PNN">کوئی</w> <w pos="NNUM1N NNUM1O">سوال</w> 
<w pos="VVNM1N">کرنا</w> <w pos="VVTM2N">چاہتے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="CC CCC">یا</w> 
<w pos="PA">آپ</w> <w pos="II">کو</w> <w pos="NNMM2N NNMM1O NNMM2O">بچے</w> <w pos="VHYM2N VHYM2O">ہوۓ</w> 
<w pos="NNUM1O">خون</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="PY1O PV1O">اس</w> <w pos="NNUM1O">مقصد</w> 
<w pos="IIM1O">کے</w> <w pos="RR">لئے</w> <w pos="NNUM1N NNUM1O">استعمال</w> <w pos="VVIA NNMM1O NNMM2N">کئے</w> 
<w pos="VVNM1O VVSV1 VVYM2N VVNM2 VVYM1O VVYM2O">جانے</w> <w pos="II CC">پر</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">اعتراض</w> <w pos="VHHV1">ہے</w> 
<w pos="XT">تو</w> <w pos="NNUM1V">ڈاکٹر</w> <w pos=",">،</w> <w pos="NNUF1N NNUF1O">نرس</w> 
<w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">مڈوائف</w> <w pos="CC CCC">یا</w> <w pos="FF">ہیلتھ</w> 
<w pos="JJU NNUM1O NNUF1O RR">وزیٹر</w> <w pos="II">کو</w> <w pos="PA">آپ</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> 
<w pos="NNMF1O JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">رازداری</w> <w pos="II">میں</w> <w pos="NNUF1O NNUF1N NNUF2N">بات</w> <w pos="VVTM2N">کرتے</w> 
<w pos="VHYM2N VHYM2O">ہوۓ</w> <w pos="NNMF1N NNMF1O">خوشی</w> <w pos="VHSV1 VHST2 VHST1">ہو</w> <w pos="VGF2 VGF1">گی</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="PA">آپ</w> <w pos="IIF1N IIF2N IIF1O IIF2O">کی</w> <w pos="JJU NNUM1O NNUF1O RR">خواہشات</w> <w pos="IIM1N">کا</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">احترام</w> <w pos="PK1N VVYM1N PK2N">کیا</w> <w pos="VXSM1">جاۓ</w> <w pos="VGM1">گا</w> 
<w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ایچ</w> <w pos="VVYF1N VVYF2N VVYF1O VVYF2O">آئی</w> 
<w pos="NNMF1N NNMF1O">وی</w> <w pos="CC CCC">یا</w> <w pos="PNO">کسی</w> <w pos="CC RD">اور</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">متعدی</w> <w pos="NNUM1O">مرض</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1O RR">بارے</w> 
<w pos="II">میں</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">فکرمند</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="CC">اور</w> 
<w pos="VVTF1N VVTF1O VVTF2N VVTF2O">ذاتی</w> <w pos="NNUM1N NNUM1O">ٹیسٹ</w> <w pos="VVNM1N">کرانا</w> <w pos="VVTM2N">چاہتے</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos="IB">تا</w> <w pos="CS">کہ</w> <w pos="PA">آپ</w> 
<w pos="II">کو</w> <w pos="PY1O PV1O">اس</w> <w pos="IIM1N">کا</w> <w pos="NNMM1N">نتیجہ</w> 
<w pos="NNMM1N">پتہ</w> <w pos="VV0 VVIT1">چل</w> <w pos="VXSV1 VXYM2N VVYM1O VVYM2N VVYM2O VVST1 VVSV1 VXYM1O VXYM2O VXST1">سکے</w> <w pos="XT">تو</w> 
<w pos="PGRM1O">اپنے</w> <w pos="NNUM1O">کلینک</w> <w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="NNUM1N NNUM2N NNUM1O NNUM2O NNUM1V">ڈاکٹر</w> 
<w pos="CC CCC">یا</w> <w pos="NNUF1N NNUF1O">نرس</w> <w pos="CC CCC">یا</w> <w pos="PGRM2N PGRM2O PGRM1O">اپنے</w> 
<w pos="AU FB">جی</w> <w pos="VVYF2N VV0 VVIT1 VVYF1N VVYF1O VVYF2O">پی</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="VVIA NNMM1O NNMM1V NNMM2N">کہیئے</w> 
<w pos="CC CCC">یا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">آلات</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">تناسل</w> <w pos="JJU NNUM1O NNUF1O RR">وبول</w> 
<w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">دوا</w> <w pos=")">)</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">جنیٹویوریزی</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">میڈیسن</w> <w pos="CC CCC">یا</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">گم</w> <w pos="(">(</w> 
<w pos="IIM2N IIM1O IIM2O">کے</w> <w pos="JJU">مقامی</w> <w pos="NNUM1O NNUM1N">کلینک</w> <w pos="VVIA NNMM1O NNMM1V NNMM2N">جایئے</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="NNMM2O NNUM2O NNUF2O">کلینکوں</w> <w pos="IIF1N IIF1O">کی</w> <w pos="NNUF1N NNUF1O">فہرست</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ٹیلی</w> 
<w pos="NNUM1O NNUM1N NNUM2N RR">فون</w> <w pos="NNMF2O">ڈائرکٹریوں</w> <w pos="II">میں</w> <w pos="VVYF1N">دی</w> 
<w pos="VHYF1N">ہوئی</w> <w pos="VHHV1">ہے</w> <w pos=".">۔</w> </s>
</p>
<p>
<s>
<w pos="CS">اگر</w> <w pos="PA">آپ</w> <w pos="JJU NNUM1N NNUM1O NNUM2N NNUF1N NNUF1O RR VV0 VVIT1">ایچ</w> <w pos="VVYF1N VVYF2N VVYF1O VVYF2O">آئی</w> 
<w pos="NNMF1N NNMF1O">وی</w> <w pos="CC RD">اور</w> <w pos="JJU NNUM1O NNUF1O RR">ایڈز</w> <w pos="IIM2N IIM1O IIM2O">کے</w> 
<w pos="NNUM1O RR">بارے</w> <w pos="II">میں</w> <w pos="NNUM1N NNUM1O">سوال</w> <w pos="VVTM2N">رکھتے</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos="CC CCC">یا</w> <w pos="PNO">کسی</w> <w pos="NNUF1O NNUF1N">تربیت</w> 
<w pos="NNMM1N">یافتہ</w> <w pos="NNUM1O NNUM1N">مشیر</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="NNUM1N NNUM1O">مشورہ</w> 
<w pos="VVNM1N">کرنا</w> <w pos="VVTM2N">چاہتے</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos="XT">تو</w> 
<w pos="PA">آپ</w> <w pos="FF">نیشنل</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ایڈز</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ہیلپ</w> 
<w pos="JJU NNUM1O NNUF1O RR">لائن</w> <w pos="II">کو</w> <foreign lang="eng">
<w pos="JDNU">0800</w> <w pos="JDNU">567</w> <w pos="JDNU">123</w> </foreign>
<w pos="II CC">پر</w> <w pos="NNUM1O NNUM1N NNUM2N RR">فون</w> <w pos="VV0 VVIT1">کر</w> <w pos="VXTM2N VVTM2N">سکتے</w> 
<w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> </s>
<s>
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ہیلپ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">لائن</w> <foreign lang="eng">
<w pos="JDNU">24</w> </foreign>
<w pos="NNMM1O">گھنٹے</w> <w pos="NNUM1N NNUM1O NNUM2N">کام</w> <w pos="VVTF1N">کرتی</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </s>
<s>
<w pos="FF FB">یو</w> <w pos="IIM2N IIM1O IIM2O FB">کے</w> <w pos="II PPM1N">میں</w> <w pos="PNO">کسی</w> 
<w pos="RM">بھی</w> <w pos="NNUF1O NNUF1N">جگہ</w> <w pos="II JXSM2N JXSM1O JXSM2O">سے</w> <w pos="JD">تمام</w> 
<w pos="NNUF2N">کالیں</w> <w pos="JJU">مفت</w> <w pos="VHHV2 VHHM2">ہیں</w> <w pos=".">۔</w> 
</s>
</p>
<p>
<s>
<w pos="PY1N PY2N">یہ</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">لیفلیٹ</w> <w pos="NNMF1O NNMF1N JJU">انگریزی</w> <w pos=",">،</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">ویلش</w> <w pos=",">،</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">عربی</w> <w pos=",">،</w> 
<w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">کینٹونیز</w> <w pos=",">،</w> <w pos="JJU NNUM1N NNUM1O NNUM1V NNUM2N NNUF1N NNUF1O NNUF1V RR VV0 VVIT1">گریک</w> <w pos=",">،</w> 
<w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">ترکی</w> <w pos=",">،</w> <w pos="NNMF1N NNMF1O NNMF1V JJF1N JJF1O JJF2N JJF2O VVYF1N VVYF1O VVYF2N VVYF2O">بنگالی</w> <w pos=",">،</w> 
<w pos="NNMF1O NNMF1N">ہندی</w> <w pos=",">،</w> <w pos="NNMF1O JJU NNMF1N">پنجابی</w> <w pos="CC RD">اور</w> 
<w pos="NNUF1O">اردو</w> <w pos="II">میں</w> <w pos="JJU">دستیاب</w> <w pos="VHHV1">ہے</w> 
<w pos=".">۔</w> </s>
</p>
</body>
</text>
</cesDoc>
