<cesDoc id="pun-w-social-county" lang="pun">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>pun-w-social-county.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Miss Ranjeet Kaur)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-12-14</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Here to help you</h.title>
<h.author>Lancashire County Council</h.author>
<imprint>
<pubPlace>Lancashire</pubPlace>
<publisher>Lancashire County Council</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-11-14</date>
</creation>
<langUsage>Translated into Punjabi from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-social-county.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s>ਤੁਹਾਡੀ</s>
<s>ਸੇਵਾ ਵਿਚ ਹਾਜ਼ਿਰ</s></head></p>

<p><head><s>ਕਾਉਂਟੀ ਦੇ</s>
<s>ਸੂਚਨਾ</s>
<s>ਕੇਂਦ੍ਰ</s></head></p>
<p><head><s><foreign lang="eng">Lancashire County Council</foreign></s></head></p>
<p><head><s>ਤੁਹਾਡਾ ਸੂਚਨਾ ਕੇਂਦ੍ਰ (ਇਨਫਰਮੇਸ਼ਨ ਸੈਂਟਰ)</s></head></p>

<p><head><s>ਕਾਉਂਟੀ ਦੇ ਸੂਚਨਾ ਕੇਂਦ੍ਰ ਕੀ ਹਨ?</s></head></p>
 
<p><s>ਕਾਉਂਟੀ ਦੇ ਸੂਚਨਾ ਕੇਂਦ੍ਰ ਲੰਕਾਸ਼ਾਇਰ ਕਾਉਂਟੀ ਕੌਸਲ ਬਾਰੇ ਅਤੇ ਉਸ ਦਵਾਰਾ ਦਿੱਤੀਆਂ ਜਾਣ ਵਾਲੀਆਂ ਸੇਵਾਵਾਂ ਬਾਰੇ ਜਾਣਕਾਰੀ ਲੈਣ ਦੇ ਪਹਿਲੇ ਸਥਾਨਿਕ ਥਾਂ ਹਨ -</s>
<s>ਸ਼ਿਕਸ਼ਾ (ਐਜੂਕੇਸ਼ਨ)</s> <s>ਸੋਸ਼ਲ ਸਰਵਿਸਿ</s>
<s>ਲਾਇਬ੍ਰੇਰੀਆਂ</s> <s>ਅੱਗ</s>
<s>ਸੜਕਾਂ</s> <s>ਵਿਉਂਤਬੰਦੀ</s>
<s>ਵੈਲਫੇਅਰ ਹੱਕ</s> <s>ਪਬਲਿਕ ਲਈ ਸਵਾਰੀ</s>
<s>ਸੜਕਾ ਉਪਰ ਬਚਾਉ</s> <s>ਵਿਉਪਾਰ ਸੰਬੰਧੀ ਮਾਪ-ਦੰਡ</s>
<s>ਮਿਉਜ਼ਿਅਮਾਂ</s> <s>ਕਲਾਸਾਂ</s>
<s>ਰਜਿਸਟਰਾਰ</s> <s>ਪੜਚੌਲ ਕਰਨ ਵਾਲੇ</s>
<s>ਰਿਕਾਰਡ ਦਫਤਰ</s> <s>ਐਮਰਜੈਂਸੀ ਲਈ ਯੋਜਨਾ ਬਣਾਉਣੀ</s></p>

<p><s>ਜੇ ਤੁਸੀਂ ਕੋਈ ਵੇਰਵੇ-ਸਹਿਤ ਪੁੱਛ-ਗਿੱਛ ਕਰਨੀ ਚਾਹੇ ਤਾਂ ਸਾਡੇ ਕਰਮਚਾਰੀ ਤੁਹਾਨੂੰ ਕਾਉਂਟੀ ਕੌਂਸਲ ਵਿਚ ਸੰਬੰਧਿਤ ਬੰਦੇ ਨਾਲ ਸੰਪਰਕ ਕਰਵਾ ਦੇਣਗੇ, ਜਿਵੇਂ ਕਿ, ਸ਼ਿਕਸ਼ਾ ਜਾਂ ਸੋਸ਼ਲ ਸੋਸ਼ਲ ਸਰਵਿਸਿਜ਼ ਬਾਰੇ ।</s> <s>ਉਹ ਸਿੱਧੀ-ਸਾਧੀ ਪੁੱਛ-ਗਿੱਛ ਦਾ ਜਵਾਬ ਖੁਦ ਦੇਣਗੇ ਜਿਵੇਂ ਕਿ ਯਾਤਰੀਆਂ ਜਾਂ ਪਬਲਿਕ ਸਵਾਰੀ ਬਾਰੇ ਜਾਣਕਾਰੀ ।</s> <s>ਤੁਸੀਂ ਆਪਣੇ ਸਥਾਨਿਕ ਕਾਉਂਟੀ ਕੌਸਲਰ ਦਾ ਨਾਂ ਅਤੇ ਪਤਾ ਲੈ ਸਕਦੇ ਹੋ ਜਾਂ ਉਸ ਨਾਲ ਮੁਲਾਕਾਤ ਕਰਨ ਲਈ ਸਮਾਂ ਤੈ ਕਰ ਸਕਦੇ ਹੋ ।</s></p>

<p><s>ਸਾਰੇ ਲੰਕਾਸ਼ਾਇਰ ਵਿਚ <foreign lang="eng">17</foreign> ਕੇਂਦ੍ਰ ਹਨ ਜੋਂ ਚੀਫ ਐਗਜ਼ੈਕਟਿਵ/ਕਲੱਰਕ ਦੇ ਮਹਿਕਮੇ ਦਾ ਹਿੱਸਾ ਹਨ ਅਤੇ ਕਾਉਂਟੀ ਪਬਲਿਕ ਰਿਲੇਸ਼ਨਜ਼ ਅਫਸਰ ਦੀ ਜ਼ਿੰਮੇਵਾਰੀ ਹਨ (ਟੈਲੀਫੋਨ - <foreign lang="eng">01772 263535</foreign>) ।</s> <s>ਇਹ ਸੈਂਟਰ ਮੋਰੀ <foreign lang="eng">(MORI)</foreign> ਦਵਾਰਾ ਕੀਤੀ ਕੌਮੀ ਪੱਧਰ ਦੀ ਖੋਜ ਤੋਂ ਬਾਅਦ ਖੋਲ੍ਹੇ ਗਏ ਸਨ ਜਿਸ ਵਿਚ ਇਹ ਸਾਫ-ਸਾਫ ਦਿਖਾਈ ਦਿੰਦਾ ਸੀ ਕਿ ਇਕ ਸੀ ਬਿਹਤਰੀ ਜਿਸ ਦੀ ਸਥਾਨਿਕ ਕੌਂਸਲ ਦੀਆਂ ਸੇਵਾਵਾਂ ਵਿਚ ਲਿਆਉਣ ਦੀ ਸਭ ਤੋਂ ਵੱਧ ਮੰਗ ਕੀਤੀ ਗਈ ਸੀ ਉਹ ਸੀ ਇਕ ਸਥਾਨਿਕ ਦਫਤਰ ਜਿਥੇ ਲੋਕ ਕਿਸੇ ਨਾਲ ਕੌਂਸਲ ਦੀਆਂ ਸੇਵਾਵਾਂ ਬਾਰੇ ਗੱਲ-ਬਾਤ ਕਰ ਸਕਣ ।</s> <s>ਲੰਕਾਸ਼ਾਇਰ ਵਿਚ <foreign lang="eng">1987</foreign> ਵਿਚ ਪਹਿਲਾ ਕੇਂਦ੍ ਖੋਲ੍ਹਣ ਤੋਂ ਹੁਣ ਤੱਕ <foreign lang="eng">8</foreign> ਮੀਲੀਅਨ ਤੋਂ ਵੱਧ ਲੋਕਾਂ ਦੀ ਮਦਦ ਕੀਤੀ ਜਾ ਚੁੱਕੀ ਹੈ ।</s></p> 
<p><head><s>ਕੀ ਕੁਝ ਮਿਲ ਸਕਦਾ ਹੈ?</s></head></p>

<p><s>ਕਾਉਂਟੀ ਦੇ ਸੂਚਨਾ ਕੇਂਦ੍ਰ ਤੋਂ ਕਾਉਂਟੀ ਕੌਂਸਲ ਦੀਆਂ ਸੇਵਾਵਾਂ ਬਾਰੇ ਕਈ ਤਰ੍ਹਾਂ ਦੇ ਮੁਫਤ ਪਰਚੇ, ਗੈਰ-ਮੁਲਕੀ ਅਤੇ ਨਸਲੀ ਘੱਟ-ਗਿਣਤੀ ਜ਼ੁਬਾਨਾਂ ਵਿਚ ਪਰਚੇ ਮਿਲ ਸਕਦੇ ਹਨ ਅਤੇ ਤੁਸੀਂ ਕਿਤਾਬਾਂ, ਸੁਵਨੀਅਰ, ਮਸ਼ਹੂਰੀ ਲਈ ਚੀਜ਼ਾਂ, ਬੱਸ ਟਿਕਟ, ਡਾਕ-ਟਿਕਟ ਅਤੇ ਟੈਲੀਫੋਨ ਕਾਰਡ ਖਰੀਦ ਸਕਦੇ ਹੋ ।</s></p>

<p><head><s>ਵੈਲਫੇਅਰ ਹੱਕਾਂ ਬਾਰੇ ਸਲਾਹ</s></head></p>

<p><s>ਬਲੈਕਪੂਲ ਅਤੇ ਲੇਲੈਂਡ ਦੇ ਸੂਚਨਾ ਕੇਂਦ੍ਰ ਵੈਲਫੇਅਰ ਹੱਕਾਂ ਸੰਬੰਧੀ ਸਲਾਹ ਦੀ ਬੜੀ ਕੀਮਤੀ ਸੇਵਾ ਪੇਸ਼ ਕਰਦੇ ਹਨ ਬਲੈਕਬਰਨ, ਬਰਨਲੇ, ਕਲਿਥਰੋ, ਫਲੀਟਵੁੱਡ, ਲਾਇਥਮ, ੳਮਜ਼ਕਰਕ ਅਤੇ ਸਕੈਲਮਰਜ਼ਡੇਲ ਸੈਂਟਰਾਂ ਵਿਚ ਵੈਲਫੇਅਰ ਹੱਕਾਂ ਦੀ ਪੁੱਛ-ਗਿੱਛ ਲਈ ਸਹੂਲਤਾਂ ਹਨ ।</s></p>

<p><head><s>ਵਿਉਪਾਰਕ ਮਾਪ-ਦੰਡਾਂ ਬਾਰੇ ਟੈਲੀਫੋਨ ਸੇਵਾ</s></head></p>

<p><s>ਲੰਕਾਸਟਰ ਅਤੇ ਬਲੈਕਬਰਨ ਦੇ ਸੂਚਨਾ ਕੇਂਦ੍ਰਾਂ ਵਿਚ ਟੈਲੀਫੋਨ ਰਾਹੀਂ ਤੁਸੀਂ ਸਿੱਧੇ ਵਿਉਪਾਰਕ ਮਾਪ-ਦੰਡਾਂ (ਟ੍ਰੈਡਿੰਗ ਸਟੈਂਡਰਡਜ਼) ਦੇ ਮਾਹਰਾਂ ਨਾਲ ਗਲੱ-ਬਾਤ ਕਰ ਸਕਦੇ ਹੋ ।</s></p>

<p><head><s>ਕੰਪਿਉਟਰ ਰਾਹੀਂ ਜਾਣਕਾਰੀ</s></head></p>

<p><s>ਜਦੋਂ ਸੈਂਟਰ ਬੰਦ ਵੀ ਹੁੰਦੇ ਹਨ ਤਾਂ ਅਸੀਂ ਸੇਵਾ ਦਿੰਦੇ ਹਾਂ ।</s> <s>ਟੈਲੀਫੋਨ ਦਾ ਜਵਾਬ ਦੇਣ ਦੀ ਮਸ਼ੀਨ ਤੋਂ ਇਲਾਵਾ ਬਹੁਤ ਸੈਂਟਰਾਂ ਵਿਚ <foreign lang="eng">24</foreign> ਘੰਟੇ ਕੰਪਿਉਟਰ ਵੀ ਚਾਲੂ ਹੁੰਦਾ ਹੈ ਜੋ ਟਾਈਮਤੇਬ ਅਤੇ ਯਾਤਰੀਆਂ ਲਈ ਮੁਫਤ ਸੇਵਾ ਦਿੰਦਾ ਹੈ ਜਿਸ ਵਿਚ ਜਸ਼ਨਾਂ, ਮਨੋਰੰਜਨ, ਰਹਿਣ ਲਈ ਥਾਂਵਾਂ ਅਤੇ ਕਾਉਂਟੀ ਕੌਸਲ ਦੀਆਂ ਸੇਵਾਵਾਂ ਦੀ ਪੂਰੀ (ਏ-ਜ਼ੈਡ) ਸੂਚਨਾ ਸ਼ਾਮਲ ਹੈ ।</s> <s>ਇਹ ਸਿਸਟਮ ਹੱਥ ਦੀ ਛੋਹ ਨਾਲ ਚਲਦਾ ਹੈ ਅਤੇ ਮੁਲਕ-ਭਰ ਵਿਚ ਸਭ ਤੋਂ ਵੱਡੀਆਂ ਕੰਪਿਉਟਰ ਸੂਚਨਾ ਸੇਵਾਵਾਂ ਵਿਚੋਂ ਇਕ ਪੇਸ਼ ਕਰਦਾ ਹੈ ।</s></p>

<p><head><s>ਯਾਤਰਿਆਂ ਲਈ ਮਦਦ</s></head></p>

<p><s>ਇਹ ਸੈਂਟਰ ਯਾਤਰਿਆਂ ਅਤੇ ਘੁੰਮਣ ਆਉਣ ਵਾਲਿਆਂ ਨੂੰ ਯਾਤਰੀਆਂ ਲਈ ਦਿਲਚਸਪ ਥਾਂਵਾਂ ਬਾਰੇ ਲਈ ਤਰ੍ਹਾਂ ਦਾ ਸਾਹਿਤ ਪੇਸ਼ ਕਰਦੇ ਹਨ ਅਤੇ ਰਹਿਣ ਲਈ ਥਾਂ ਬੁੱਕ ਕਰਨ ਅਤੇ ਕੁਝ ਥਾਂਵਾਂ ਨੂੰ ਜਾਣ ਲਈ ਟਿਕਟ ਵੇਚਣ ਦੀ ਸੇਵਾ ਪੇਸ਼ ਕਰਦੇ ਹਨ ।</s></p>

<p><s>ਚੋਰਲੀ ਦੇ ਕਾਉਂਟੀ ਸੂਚਨਾ ਕੇਂਦ੍ਰ ਨੂੰ <foreign lang="eng">01257-241693</foreign> ਨੰਬਰ ਤੇ ਟੈਲੀਫੋਨ ਕਰਨ ਨਾਲ ਉਚੱਾ ਸੁਣਨ ਵਾਲਿਆਂ ਲਈ "ਮਿਨੀਕਾਮ" ਦੀ ਸਹੂਲਤ ਤੋਂ ਮਦਦ ਮਿਲ ਸਕਦੀ ਹੈ ।</s> <s>ਨਜ਼ਰ ਵਿਚ ਨੁਕਸ ਵਾਲੇ ਲੋਕ <foreign lang="eng">01772-263535</foreign> ਨੰਬਰ ਤੇ ਟੈਲੀਫੋਨ ਕਰਕੇ ਮਦਦ ਹਾਸਿਲ ਕਰ ਸਕਦੇ ਹਨ ਜਾਂ ਕੰਪਿਉਟਰ ਉਪਰ ਸੂਚਨਾ ਸਿਸਟਮ ਤੇ ਵੱਡੇ ਅੱਖਰ ਫਾਇਦੇਮੰਦ ਹੋ ਸਕਦੇ ਹਨ ।</s> <s>ਅਧਿਕਤਰ ਸੈਂਟਰਾਂ ਵਿਚ ਅਪਾਹਿਜ ਲੋਕਾਂ ਦੇ ਆਉਣ-ਜਾਣ ਲਈ ਸਹੂਲਤਾਂ ਹਨ ।</s></p>

<p><s>ਮੋਰਕਮ, ਰੋਸੈਨਡੇਲ ਅਤੇ ਰਿਬਲ ਵੈਲੀ ਦੇ ਡਿਸਟ੍ਰਿਕਟ ਕਾਉਂਟੀ ਕੌਂਸਲ ਨਾਲ ਮਿਲ ਕੇ ਸੈਂਟਰ ਚਲਾਉਂਦੇ ਹਨ ।</s></p>

<p><s>ਸੋ ਚਾਹੇ ਤੁਸੀਂ ਇਹ ਜਾਣਨਾ ਚਾਹੁੰਦੇ ਹੋਵੋ ਕਿ ਤੁਹਾਡੇ ਬੱਚਿਆਂ ਲਈ ਕਿਹੜੇ ਸਕੂਲ ਉਪਲਬਧ ਹਨ, ਕਿਹੜੇ ਘਰ ਤੁਹਾਡੇ ਬਜ਼ੁਰਗ ਰਿਸ਼ਤੇਦਾਰਾਂ ਦੀ ਦੇਖ-ਭਾਲ ਕਰ ਸਕਦੇ ਹਨ ਜਾਂ ਤੁਹਾਡੇ ਘਰ ਜਾਣ ਲਈ ਅਗਲੀ ਬੱਸ ਕਿੰਨੇ ਵਜੇ ਹੈ, ਅਸੀਂ ਯਕੀਨ ਦਵਾਉਂਦੇ ਹੈ ਕਿ ਜਿਥੇ ਤੁਹਾਨੂੰ ਕਾਉਂਟੀ ਦੇ ਸੂਚਨਾ ਕੇਂਦ੍ਰ ਦਾ ਖਾਸ ਪੀਲਾ ਸਾਈਨ ਦਿਖਾਈ ਦਿੰਦਾ ਹੈ, ਉਥੇ ਤੁਹਾਨੂੰ ਆਦਰ-ਸਤਿਕਾਰ ਅਤੇ ਸਹਾਇਤਾ ਭਰੀ ਸੇਵਾ ਮਿਲੇਗੀ ।</s></p>

<p><s>ਇਹ ਪਰਚਾਂ ਅੰਗ੍ਰੇਜ਼ੀ, ਉਰਦੂ, ਗੁਜਰਾਤੀ ਅਤੇ ਬੰਗਾਲੀ ਵਿਚ ਵੀ ਮਿਲ ਸਕਦਾ ਹੈ ।</s> <s>ਸਾਰੇ ਕੇਂਦ੍ਰਾਂ ਦੇ ਪਤਿਆਂ, ਟੈਲੀਫੋਨ ਨੰਬਰਾਂ ਅਤੇ ਖੁੱਲ੍ਹਣ ਦੇ ਸਮਿਆਂ ਦੀ ਲਿਮਟ ਅਤੇ ਤੁਹਾਡੇ ਸਥਾਨਿਕ ਸੂਚਨਾ ਕੇਂਦ੍ਰ ਦਾ ਵੇਰਵਾ ਇਸ ਪਰਚੇ ਦੇ ਪਿੱਛੇ ਦਿੱਤਾ ਗਿਆ ਹੈ ।</s></p>

<p><head><s><foreign lang="eng">YOUR LOCAL INFORMATION CENTRES</foreign></s></head></p>

<p><s><foreign lang="eng">ACCRINGTON CIC</foreign></s>
<s><foreign lang="eng">The Bus Station,</foreign></s>
<s><foreign lang="eng">ACCRINGTON. BB5 1GR</foreign></s>
<s><foreign lang="eng">Tel: (01254) 872595</foreign></s></p>

<p><s><foreign lang="eng">BLACKBURN CIC</foreign></s>
<s><foreign lang="eng">15/17 Railway Road,</foreign></s>
<s><foreign lang="eng">BLACKBURN. BB1 5AX</foreign></s>
<s><foreign lang="eng">Tel: (01254) 681120</foreign></s></p>

<p><s><foreign lang="eng">BLACKPOOL CIC</foreign></s>
<s><foreign lang="eng">96/98 Talbot Road,</foreign></s>
<s><foreign lang="eng">BLACKPOOL. FY1 1LR</foreign></s>
<s><foreign lang="eng">Tel: (01253) 751485</foreign></s></p>

<p><s><foreign lang="eng">BURNLEY CIC</foreign></s>
<s><foreign lang="eng">The Bus Station,</foreign></s>
<s><foreign lang="eng">BURNLEY. BB11 2EJ</foreign></s>
<s><foreign lang="eng">Tel: (01282) 423125</foreign></s></p>

<p><s><foreign lang="eng">CHARNOCK RICHARD TIC</foreign></s>
<s><foreign lang="eng">M6 Motorway Service Area,</foreign></s>
<s><foreign lang="eng">Charnock Richard,</foreign></s>
<s><foreign lang="eng">CHORLEY. PR7 5NG</foreign></s>
<s><foreign lang="eng">Tel: (01257) 793773</foreign></s></p> 

<p><s><foreign lang="eng">CHORLEY CIC</foreign></s>
<s><foreign lang="eng">55/57 Union Street,</foreign></s>
<s><foreign lang="eng">CHORLEY. PR7 1EB</foreign></s>
<s><foreign lang="eng">Tel: (01257) 241693</foreign></s></p>

<p><s><foreign lang="eng">CLITHERORE CIC</foreign></s>
<s><foreign lang="eng">12/14 Market Place,</foreign></s>
<s><foreign lang="eng">CLITHEROE. BB7 2DA</foreign></s>
<s><foreign lang="eng">Tel: (01200) 442226</foreign></s></p>

<p><s><foreign lang="eng">FLEETWOOD CIC</foreign></s>
<s><foreign lang="eng">15 North Albert Street,</foreign></s>
<s><foreign lang="eng">FLEETWOOD. FY7 6AJ</foreign></s>
<s><foreign lang="eng">Tel: (01253) 772704</foreign></s></p>

<p><s><foreign lang="eng">LANCASTER CIC</foreign></s>
<s><foreign lang="eng">The Bus Station,</foreign></s>
<s><foreign lang="eng">Cable Street,</foreign></s>
<s><foreign lang="eng">LANCASTER. LA1 1HD</foreign></s>
<s><foreign lang="eng">Tel: (01524) 841656</foreign></s></p>

<p><s><foreign lang="eng">LEYLAND CIC</foreign></s>
<s><foreign lang="eng">116/118 Towngate,</foreign></s>
<s><foreign lang="eng">LEYLAND. PR5 1LQ</foreign></s>
<s><foreign lang="eng">Tel: (01772) 621857</foreign></s></p>

<p><s><foreign lang="eng">LYTHAM CIC</foreign></s>
<s><foreign lang="eng">4 Clifton Square,</foreign></s>
<s><foreign lang="eng">LYTHAM. FY8 5JP</foreign></s>
<s><foreign lang="eng">Tel: (01253) 794405</foreign></s></p>

<p><s><foreign lang="eng">MORECAMBE TIC</foreign></s>
<s><foreign lang="eng">Station Buildings,</foreign></s>
<s><foreign lang="eng">Central Promenade,</foreign></s>
<s><foreign lang="eng">MORECAMBE. LA4 4DB</foreign></s>
<s><foreign lang="eng">Tel: (01524) 582808</foreign></s></p>

<p><s><foreign lang="eng">NELSON CIC</foreign></s>
<s><foreign lang="eng">The Bus Station,</foreign></s>
<s><foreign lang="eng">Broadway,</foreign></s>
<s><foreign lang="eng">NELSON. BB9 9SJ</foreign></s>
<s><foreign lang="eng">Tel: (01282) 698533</foreign></s></p>

<p><s><foreign lang="eng">ORMSKIRK CIC</foreign></s>
<s><foreign lang="eng">The Bus Station,</foreign></s>
<s><foreign lang="eng">45 Moor Street,</foreign></s>
<s><foreign lang="eng">ORMSKIRK. L39 2AG</foreign></s>
<s><foreign lang="eng">Tel: (01695) 579062</foreign></s></p>

<p><s><foreign lang="eng">PRESTON CIC</foreign></s>
<s><foreign lang="eng">The Bus Station,</foreign></s>
<s><foreign lang="eng">PRESTON. PR1 1YT</foreign></s>
<s><foreign lang="eng">Tel: (01772) 556618</foreign></s></p>

<p><s><foreign lang="eng">RAWTENSTALL TIC</foreign></s>
<s><foreign lang="eng">43/45 Kay street,</foreign></s>
<s><foreign lang="eng">RAWTENSTALL. BB4 7LS</foreign></s>
<s><foreign lang="eng">Tel: (01706) 213677</foreign></s></p>

<p><s><foreign lang="eng">SKELMERSDALE CIC</foreign></s>
<s><foreign lang="eng">Unit 59c,</foreign></s>
<s><foreign lang="eng">The Concourse,</foreign></s>
<s><foreign lang="eng">SKELMERSDALE. WN8 6NT</foreign></s>
<s><foreign lang="eng">Tel: (01695) 50463</foreign></s></p>

<p><s><foreign lang="eng">For directions/further information, please contact Rukiyya, Gail or Gwen on 01772 263539/263535. Monday to Friday 9 am to 5 pm</foreign></s></p>

<p><s>ਹੋਰ ਜਾਣਕਾਰੀ ਲੈਣ ਲਈ ਅਤੇ ਰਸਤੇ ਬਾਰੇ ਪਤਾ ਕਰਨ ਲਈ ਕਿਰਪਾ ਕਰਕੇ ਰੁੱਕਿਆ, ਗੇਲ ਜਾਂ ਗਵੈਨ ਨਾਲ ਸੋਮਵਾਰ ਤੋਂ ਸ਼ੁਕਰਵਾਰ ਸਵੇਰੇ <foreign lang="eng">9</foreign> ਵਜੇ ਤੋਂ ਸ਼ਾਮ <foreign lang="eng">5</foreign> ਵਜੇ ਦੇ ਦਰਮਿਆਨ ਇਸ ਨੰਬਰ ਤੇ ਸੰਪਰਕ ਕਰੋ - <foreign lang="eng">01772 263539</foreign> ਜਾਂ <foreign lang="eng">263535</foreign></s></p>

</body>
</text>
</cesDoc>