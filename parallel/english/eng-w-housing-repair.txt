<cesDoc id="eng-w-housing-repair" lang="eng">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>eng-w-housing-repair.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Andrew Hardie</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-07-03</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Your new right to repair</h.title>
<h.author>Department for Environment, Transport and the Regions</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department for Environment, Transport and the Regions</publisher>
<pubDate>March 2000</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-07-03</date>
</creation>
<langUsage>English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="NULL" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s>A better deal for tenants</s></head></p>

<p><head><s>Your New Right to Repair</s></head></p>

<p><head><s>Your Right to Repair</s></head></p>

<p><s>As part of the Citizen's Charter scheme, a new Right to Repair scheme was introduced for council tenants from 1 April 1994.</s></p>

<p><s>The new Right to Repair is a scheme for council tenants.</s> <s>It will make sure that certain small urgent repairs which might affect your health, safety or security, are done quickly and easily.</s> <s>Councils will be told by law to carry out these repairs within a certain time.</s></p>

<p><s>If the council doesn't do your repair in time, you can tell it to get another contractor instead.</s> <s>If the second contractor doesn't do the repair in time, the council will pay you compensation.</s> <s>The council will tell you what repairs come under the new scheme and how long it has to carry them out.</s> <s>Repair times vary depending on the type of repair.</s> <s>For example, if your toilet isn't flushing, the council usually has one working day to come and repair it.</s> <s>It has three working days to mend a loose bannister rail and seven working days to mend a broken extractor fan in your bathroom or kitchen.</s></p>

<p><head><s>What repairs can you get done?</s></head></p>

<p><s>You can get certain small urgent repairs done (up to the value of £250) if they are likely to affect your health, safety or security.</s> <s>These are called qualifying repairs.</s></p>

<p><s>Qualifying repairs include:</s>
<s>unsafe power or lighting sockets or electrical fittings;</s>
<s>blocked flue to open fire or boiler;</s>
<s>leaking roof;</s>
<s>toilets which don't flush;</s>
<s>blocked sink, bath or basin;</s>
<s>leaking from a water or heating pipe, tank or cistern;</s>
<s>loose or broken bannisters or handrails.</s></p>

<p><s>Your council will have a full list of repairs which come under the scheme.</s> <s>It will be able to tell you if a repair you need is included in the scheme and how long it has to get the repair done.</s> <s>The council will also be able to tell you how it deals with repairs which aren't covered under this scheme.</s></p>

<p><head><s>How can you get your repairs done?</s></head></p>

<p><s>You should tell the council what repairs need to be done.</s> <s>The council may need to send someone to your home to check the problem first.</s> <s>If the repair comes under the Right to Repair scheme, the council will tell a contractor to do it in the set time.</s> <s>The council will also send you a copy of the repair notice it sends to the contractor.</s> <s>The notice will show you:</s>
<s>the name, address and telephone number of the contractor who will do the repair;</s>
<s>the arrangements made for the contractor to do the repair (the date and time);</s>
<s>what the repair is; and</s>
<s>when the repair should be done by.</s></p>

<p><s>You must let the council know when someone can be at home to let the contractor in.</s></p>

<p><head><s>How long has the council got to carry out these repairs?</s></head></p>

<p><s>This depends on the type of repair you need, but the council can always tell you how long it should take.</s> <s>Qualifying repair times are set by law - not the council.</s></p>

<p><head><s>What happens if the first contractor doesn't do your repair in time?</s></head></p>

<p><s>If the first contractor doesn't do your repair in time, you should phone the council and tell them to get a second contractor to carry out the work.</s></p>

<p><s>Unless there is a good reason why the work hasn't been done, the council will get a second contractor.</s> <s>You will get a copy of the second repair notice - which the council sends to the second contractor.</s> <s>The second contractor then has the same amount of time to do the repair as the first one had.</s></p>

<p><head><s>Compensation</s></head></p>

<p><s>If the second contractor doesn't do your repair in time, you will get £10 in compensation.</s> <s>For every extra day you wait, you will get another £2.</s> <s>The most compensation you can get for any one job is £50.</s> <s>The council will pay your compensation - unless you already owe it some money.</s> <s>If you do owe money to the council, it will take away the amount you owe from your compensation.</s></p>

<p><s>Sometimes there may be a good reason why a repair can't be done.</s> <s>For example, if you didn't keep your appointment to let the contractor in, and they therefore couldn't carry out the repair, the council won't have to pay you any compensation.</s></p>

<p><head><s>How to find out more</s></head></p>

<p><s>Contact your council's housing department.</s>
<s>This leaflet is one of a series of three dealing with the new tenants' rights (Your New Right to Manage, Your New Right to Repair, and Your New Right to Compensation for Improvements.</s></p>

<p><s>For further copies please write to:</s>
<s>Department of the Environment, Transport and the Regions</s>
<s>DETR Free Literature</s>
<s>PO Box No. 236</s>
<s>Wetherby</s>n<s>LS23 7NB</s>
<s>Tel: 0870 122 6236</s>
<s>Textphone 0870 120 7405</s>
<s>Fax: 0870 122 6237</s>
<s>email: detr@twoten.press.net</s>
<s>or</s>
<s>Housing Performance and Finance Division</s>
<s>National Assembly for Wales</s>
<s>Cathays Park, Cardiff CF10 3NQ</s>
<s>These leaflets are free and also available in Welsh, Bengali, Hindi, Urdu, Greek and Vietnamese.</s></p>

<p><s>Published by the Department of the Environment, Transport and the Regions and the National Assembly for Wales.</s></p>

<p><s>© Crown Copyright 2000.</s> <s>Printed in the UK.</s> <s>March 2000 on
paper comprising 75% post-consumer waste and 25% ECF pulp.</s></p>

<p><s>Product code GGHC 1096.</s></p>

</body>
</text>
</cesDoc>