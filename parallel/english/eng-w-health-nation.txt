<cesDoc id="eng-w-health-nation" lang="eng">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>eng-w-health-nation.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Paul Baker</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>01-07-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>The Health of the Nation and You</h.title>
<h.author>Department of Health</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department of Health</publisher>
<pubDate>July 1992</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>01-07-24</date>
</creation>
<langUsage>English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality> 
</textClass>
<translations>
<translation trans.loc="NULL" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>
<p><head><s>THE HEALTH OF THE NATION AND YOU</s></head></p>

<p><s>Towards a better state of health</s></p>

<p><head><s>Introduction</s></head></p> 

<p><s>No matter who we are or what we do, we all know how precious good health is.</s></p>

<p><s>After several months of wide consultation, the Government has produced "The Health of the Nation", a strategic plan aimed at achieving better health for everyone in England.</s></p>

<p><s>The plan sets out targets for the nation's health.</s> <s>This is the first time that such targets have been set for England.</s></p> 

<p><head><s>Promoting good health</s></head></p> 

<p><s>Promoting good health and preventing ill-health are at the heart of "The Health of the Nation".</s> <s>They are as important as treating illness and making sure that those who suffer a serious disease or illness can lead as full a life as possible.</s></p>

<p><s>The success of "The Health of the Nation" depends on getting the priorities right in all these important areas.</s></p> 

<p><s>This booklet tells you about the Government's aims and how, by working together, we can all help to achieve them.</s></p>

<p><s>It also includes some practical tips for better health, and many good ideas on how we can live healthier lives.</s></p> 

<p><head><s>England's health today</s></head></p> 

<p><s>It seems hard to believe that only a few generations ago thousands of people died of cholera and typhoid in England.</s></p>

<p><s>Half of the nation's children failed to reach adulthood, and of those who did, few could expect to live as long as we do today.</s></p> 

<p><s>Times have changed.</s> <s>The terrible diseases of Dicken's day, such as smallpox, diphtheria and polio, are no longer a threat.</s> <s>The number of deaths form tuberculosis has been reduced dramatically and deaths form illness such as whooping cough or measles are very rare.</s></p> 

<p><s>The great social developments of the last hundred years have played an important part bringing this about.</s> <s>Today, we have safer water, a wide variety of wholesome foods, proper sewerage and sanitation systems, better housing, and improved working conditions.</s> <s>Our National Health Service is recognised throughout the world for the excellent services it has been providing since 1948.</s></p> 

<p><s>Successful immunisation programmes have also played an important part in our fight against disease, as the diagrams here show.</s> <s>As a result of all these changes most of us now enjoy a better quality of life than ever before.</s></p> 

<p><s>However, there is still a lot we can do to improve our lives.</s> </p>

<p><s>Although we have said goodbye to many of those threats to our health, we still have to guard against their return.</s> <s>We also have to be aware that different diseases today pose new dangers.</s> <s>As we learn more about them and how they can be avoided, so we can take action to protect ourselves and others.</s></p>

<p><s>For example, heart disease, stroke, some cancers and accidents can often be prevented or avoided by changes in the way we live.</s></p> 

<p><s>HIV infection, which causes AIDS, is perhaps the greatest new public health challenge this century.</s> <s>But the ways in which the virus is passed, for example by sexual intercourse or through infected needles, are well understood and many of the risks can be eliminated or reduced by personal action and changes in behaviour.</s></p> 

<p><s>By most standards our health is already getting better.</s> <s>For example, as the diagrams show, we've seen a welcome drop in the number of people suffering form stroke, and in the number of men who die form lung cancer.</s> <s>These improvements are welcome, but there is still more that needs to be done.</s> <s>Further progress could be made by making simple changes in the way we live.</s></p> 

<p><s>"The Health of the Nation" builds on success we have already achieved.</s> <s>It aims to achieve even more --- by helping us to concentrate on today's problems in ways that will produce the right results, not only for us but for our children.</s></p> 

<p><head><s>A strategy for action.</s></head></p> 

<p><s>The main aim behind "The Health of the Nation" is simple:  to improve the health of people living in England.</s></p>

<p><s>The question is how and where do we begin?</s></p> 

<p><s>Our first aim must be to get our priorities right.</s> <s>If we don't, we won't achieve the success we want.</s></p>

<p><s>So we must: </s> <s>Concentrate on the major health problems.</s> <s>Focus on promoting good health and preventing disease as much as on care and treatment.</s> <s>Accept that if we are to succeed then we all have to work together.</s></p> 

<p><s>Concentrating on problems</s></p>

<p><s>While we all want to see improvements in every area of health, the government's new strategy is focused on the five key areas where the need for improvements in greatest, and where genuine long-lasting success is most likely to be achieved.</s> <s>It is in these areas where national targets are being set.</s></p> 

<p><s>The five key areas are: </s> <s>Coronary heart disease and stroke; </s> <s>Cancers; </s> <s>Mental illness; </s> <s>Accidents; </s> <s>HIV/AIDS and sexual health; </s></p> 

<p><s>They all</s> <s>are major causes of serious illness or early death;</s> <s>offer scope for effective action.</s></p> 

<p><s>The five initial choices were made form the many discussed during the last year.</s> <s>They will be reviewed on a regular basis, and the new key areas may be added or substituted in the future.</s></p> 

<p><s>The Government has set a series of challenging targets in each key area.</s> <s>These will help give a sense of direction to the efforts to improve health and focus attention on areas where we can make real progress.</s> <s>The most important targets are set out here.</s> <s>You will find a full list of the targets at the back of this booklet.</s></p> 

<p><head><s>The key areas</s></head></p> 

<p><head><s>Coronary heart disease and stroke.</s></head></p> 

<p><s>Targets: </s></p>

<p><s>By the year 2000 to: </s> <s>Reduce heart disease death rates in people under 65 by at least 40%, and among people between 65 and 74 by at least 30%; </s> <s>Reduce the death rate form stroke among people under 75 by at least 40%; </s> <s>Reduce the number of people smoking by about a third; </s></p>

<p><s>By 2005 to: </s> <s>Reduce the number of people aged 16 to 64 who are obese by at least a quarter for men and at least a third for women; </s> <s>Reduce average intake of fat by 12%, and saturates by 35%; </s> <s>Reduce the number of men drinking more than 21 units of alcohol per week and women drinking more than 14 units per week by a third.</s></p> 

<p><s>Coronary heart disease and stroke</s></p> 

<p><s>Coronary heart disease caused more than a quarter of all deaths in 1990, while stroke was responsible for more than one in ten.</s> <s>Together, they not only present the greatest threat of death, but also bring ill-health and disability to thousands of people every year.</s></p> 

<p><s>Yet many of the cases of heart disease and stroke are all things we can do something about.</s> <s>Smoking, raised blood pressure, raised blood cholesterol levels, being overweight, and lack of exercise can all contribute to these diseases.</s></p>

<p><s>If we all look at these problems in a sensible and positive way, we can reduce our risk of both heart disease and stroke.</s></p> 

<p><head><s>Cancers</s></head></p>

<p><s>Cancer targets: </s></p>

<p><s>By 2010 to: </s> <s>Reduce the rate of lung cancer deaths by at least 30% in men and by at least 15% in women under the age of 75;  </s></p>

<p><s>By 2000 to: </s> <s>Reduce the number of people smoking by about a third; </s> <s>Reduce the rate of breast cancer deaths among women invited for screening by at least 25%; </s> <s>Reduce the incidence of invasive cervical cancer by approximately 20%.</s></p>

<p><s>By 2005 to: </s> <s>Halt the increase in the incidence of skin cancer.</s></p> 

<p><s>After coronary heart disease, cancers are the most common cause of death in England.</s> <s>In 1990, they accounted for about a quarter of all deaths.</s></p>

<p><s>There are many types of cancers.</s> <s>We do not fully understand the causes of them all, but there are steps we can take avoid some of them.</s></p> 

<p><s>The Government has therefore set targets for reducing the levels of ill-health and death form four particular types of cancer:  breast cancer, cervical cancer, skin cancer and lung cancer.</s></p> 

<p><s>Breast cancer</s> <s>There is a national screening programme for breast cancer, and if women aged between 50-64 take up invitations for breast screening checks, the programme could save as many as 1,250 lives each year.</s></p> 

<p><s>Cervical cancer</s> <s>There is also a national screening programme for cervical cancer.</s> <s>This cancer can usually be prevented before it develops.</s> <s>More than 1,500 women still die of this disease each year.</s></p>

<p><s>By carrying out a simple smear test, doctors can identify abnormal cells which, if left untreated, might lead to cancer later in life.</s></p> 

<p><s>Skin cancer</s> <s>There are about 28,000 cases of skin cancer and 1,500 deaths a year.</s> <s>Skin cancer can mainly be prevented by a sensible approach sun bathing.</s></p> 

<p><s>Lung cancer</s> <s>It kills 26,000 people a year in England.</s> <s>At eight out of ten cases are caused by smoking, which is also known to be responsible for almost a third of all cancer deaths.</s> <s>It also causes heart disease and is recognised as one of the major causes of chest diseases such as bronchitis and emphysema.</s></p> 

<p><head><s>Mental illness</s></head></p>

<p><s>Mental illness targets</s> <s>Improve significantly the health, social functioning and quality of life of people who are mentally ill.</s></p>

<p><s>By the year 2000 to: </s> <s>Reduce the national suicide rate by 15%.</s></p> 

<p><s>Mental illness can take many forms, including depression and anxiety, as well as more serious disorders such as schizophrenia.</s></p>

<p><s>The cost is high in term of human misery and family suffering.</s> <s>Mental illness accounts for many days of sick absence form work and also leads to a considerable number of early deaths form higher levels of physical illness and form suicides.</s></p> 

<p><s>There is much that is known about mental illness.</s> <s>It is possible to help many people recover through effective treatment and care.</s> <s>It is also possible to improve the quality of life for those who remain mentally ill, by improving the availability of sensitive local health and social services.</s></p> 

<p><head><s>Accidents</s></head></p> 

<p><s>Targets for accident prevention</s></p>

<p> <s>By 2005 to: </s> <s>Reduce the rate of accidental deaths among children and elderly people by at least a third; </s> <s>Reduce the rate of accidental deaths among young people aged 15 to 24 by at least a quarter.</s></p> 

<p><s>Though England has one of the best records for safety in Europe, accidents are still the most common cause of death among people under thirty.</s></p> 

<p><s>Very few accidents are due solely to chance, and many could be avoided.</s> <s>Half of accidental deaths happen on the roads and over s third in the home.</s></p>

<p><s>Accidents account for many deaths and injury among children, young adults and elderly people.</s> <s>Because of this "The Health of the nation" includes specific targets for each of these age groups.</s></p> 

<p><head><s>HIV/AIDS and sexual health.</s></head></p>

<p><s>HIV/AIDS and sexual health targets to:  </s></p>

<p><s>Reduce the national incidence rate of gonorrhoea by at least 20% by 1995; </s> <s>Reduce the proportion of drug users who report sharing needles form a fifth in 1990 to no more than a tenth in 1997; </s> <s>Reduce by at least half the rate of conceptions amongst the under 16s by the year 2000.</s></p> 

<p><s>Many aspects of our general health are linked to our sexual behaviour.</s></p> 

<p><s>AIDS is one of those diseases about which we still have much to learn.</s> <s>HIV, the virus which causes AIDS, is transmitted in three main ways through sexual intercourse, by drug and substance users who share needles, syringes and other injecting equipment and form infected mothers to their babies, before or during childbirth, or through breast milk.</s></p>

<p><s>The more people with whom you have unprotected sexual intercourse, the greater the risk of infection with HIV and other sexually transmitted diseases.</s></p> 

<p><s>Many people do not realise that they have been infected with HIV until they develop symptoms of AIDS.</s> <s>This may not happen for many years.</s> <s>Other sexually transmitted diseases produce symptoms very soon after infection.</s> <s>Therefore by looking at trends in diseases such as gonorrhoea, we can see whether people are continuing to be at risk of HIV infection or whether patterns of behaviour are changing.</s> <s>Although the number of cases of gonorrhoea has been declining over the last ten years this has not been the case more recently.</s></p> 

<p><s>Of course, sexual health isn't only about controlling diseases.</s> <s>It also includes family planning, which can play an important part in the health of children and the well-being of families.</s> <s>For example, unwanted pregnancies especially amongst the under 16s always have serious personal and social consequences for both the mothers and their children.</s></p> 

<p><head><s>What we can do?</s></head></p> 

<p><s>The main aim of the "The Health of the Nation" is to improve the health of the people of England.</s></p>

<p><s>If we are going to create a healthier nation, we all have an important role to play.</s></p> 

<p><s>As individuals we can do much to look after our own health and that of our families.</s></p> 

<p><s>At work, employers can help by making factories, offices, shops and other work-places healthier and safer for everyone.</s></p>

<p><s>There is much they can also do to help improve the all-round general health of their employees by providing workplace health promotion activities and healthy choices in staff restaurants and canteens.</s></p>

<p><s>Hospitals, schools and colleges can also play a part in promoting good health.</s></p> 

<p><s>Throughout our communities in general we can all do more to pass on the good news about the real benefits of good health and healthy living.</s></p> 

<p><s>The government also has an important role to play.</s></p>

<p><s>It can take action when needed to protect out health.</s> <s>It spends thousands of millions of pounds of our money on the National Health Service each year.</s> </p>

<p><s>It provides money and guidance for others, such as voluntary organisations and research groups.</s></p> 

<p><s>It also publishes information on all aspects of health and health care.</s></p> 

<p><s>The contribution each individual or organisation makes is important.</s> <s>But if everyone works together, then the chance of this initiative making a real difference to our health is much better.</s></p> 

<p><head><s>A programme for the future</s></head></p> 

<p><s>"The Health of the Nation" is a programme for both the present and the future.</s> <s>It: </s> <s>sets out the key area for immediate action, and sets targets for the year 2000 and beyond; </s> <s>highlights those areas where considerable progress is already made such as the health of mothers and babies, immunisation, and food safety and proposes some new targets for the future.</s></p>

<p><s>"The health of the Nation" is not the last word on the project, but the beginning of new ways in which you can add years to life and life to our years.</s></p> 

<p><s>Further copies of this booklet are available form BAPS, Health Publications Unit, Storage and Distribution Centre, Heywood Stores, Manchester Road, Heywood, Lances OL 10 2PZ.</s></p> 

<p><head><s>You and your health</s></head></p> 

<p><s>Good health is to be valued and enjoyed.</s> <s>It can not be taken for granted.</s> <s>But it is a fact, that by making gradual changes to your everyday lifestyle and habits, you can look forward to a healthier and happier life.</s> <s>Because if you take care of your body, it will take care of you.</s> <s>All this need not be expensive and does not have to be a chore.</s> <s>Here are some tips to help you enjoy healthier living.</s></p> 

<p><head><s>Eat healthily</s></head></p> 

<p><s>It is the food we eat that gives us the energy and nourishment to live and enjoy life.</s> <s>If we eat too much or indulge in the wrong sort of diet, especially one with too much fatty food, we could have health problems.</s></p> 

<p><s>Eating healthily does not mean that we have to give up all the things we like.</s> <s>It means eating a variety of foods;  eating more fibre-rich starchy foods like bread, pasta, rice and potatoes and going easy on fatty, sugary and salty foods.</s></p> 

<p><s>Ten things you can eat more of:  </s> <s>Jacket Potatoes</s> <s>Chicken</s> <s>Fish</s> <s>Fruit</s> <s>Bread</s> <s>Beans on Toast</s> <s>Pasta</s> <s>Vegetables and Salads</s> <s>Rice</s> <s>Cereals.</s></p> 

<p><s>Here are eight simple guidelines for a healthy diet: </s> <s>Enjoy your food;  </s> <s>Eat a variety of different food;  </s> <s>Eat the right amount to be a healthy weight; </s> <s>Eat plenty of foods rich in starch and fibre; </s> <s>Don't eat too much fat; </s> <s>Don't eat sugary foods too often; </s> <s>Look after the vitamins and minerals in your food;  </s> <s>If you drink, keep within sensible limits.</s></p> 

<p><head><s>Tips about healthy eating.</s> </head></p>

<p><s>Grill food instead of frying it.</s> <s>Cut the fat off meat.</s> <s>Use lower-fat spreads and milks.</s> <s>Eat fish, poultry or the leaner cuts of meat.</s> <s>Go easy on cakes and biscuits try fruit instead.</s> <s>Try to eat at least four slices of bread a day.</s> <s>Eat plenty of vegetables, and salads.</s></p> 

<p><head><s>Drink sensibly</s></head></p> 

<p><s>One unit of Alcohol =</s><s>Half a pint of ordinary strength beer, lager or cider</s> <s>One small glass of sherry</s> <s>One small glass of wine</s> <s>One single measure of spirits</s></p>

<p><s>Many people enjoy drink.</s> <s>If you care about your health, then care about how much you drink.</s> <s>Too much alcohol can make you overweight, be bad for your circulation and your liver and cause problems at work and with family and friends.</s> <s>Remember that alcoholics also a major cause of accidents.</s> <s>Your driving ability can be affected by just one drink.</s> <s>Don't drink before driving, or before using dangerous machinery, or after taking medicine.</s></p> 

<p><s>You may find it helpful to keep track of the units of alcohol you drink in a week.</s> <s>Medical advice is that if men drink more than 21 units per week, and if women drink more than 14 units per week, on a regular basis, they increase the risk of damaging their health, especially if they drink alcohol everyday.</s> <s>Drinking regularly in excess of 50 units per week for men and 35 units per week for women is definitely dangerous.</s></p> 

<p><s>This is only a rough guide.</s> <s>It applies to measures that you would normally buy in pubs.</s> <s>Remember that home measures are usually much more generous.</s></p> 

<p><head><s>Tips about sensible drinking</s></head></p>

<p> <s>Keep count of how many units you drink each week.</s> <s>Spread your units over the week with one or two drink-free days.</s> <s>If you are thirsty, drink something non-alcoholic, or alternate alcoholic and non-alcoholic drinks.</s></p> 

<p><head><s>Exercise can be fun</s></head></p> 

<p><s>Regular physical activity can help control your weight, improve your blood circulation and help your heart work more efficiently.</s> <s>It helps you to relax and, not only will you feel better, you will look better.</s> <s>By regularly using your muscles, they will be firmer and in much better shape.</s></p> 

<p><head><s>Tips</s></head></p>

<p><s>Try walking more.</s> <s>If you normally take the car or use public transport for just a short journey, why not try walking instead.</s> <s>Cycle, jog, swim, dance or do anything else you enjoy and that makes you a bit out of breath.</s> <s>Go up stairs rather than use the escalator or lift.</s> <s>Try to exercise two or three times a week for about 20-30 minutes at a time.</s> <s>Don't overdo it start gently and build up gradually.</s></p> 

<p><head><s>Stopping smoking</s></head></p> 

<p><s>If you smoke, stopping is the most effective action you can take to improve your health.</s> <s>All form of smoking are bad for you;  it increases your risk of heart disease, lung disease (especially bronchitis and lung cancer) and in later life, osteoporosis (thinning of the bones).</s> <s>It also reduces your chance of survival after a heart attack.</s></p>

<p><s>One in every five fatal heart attacks is smoking related.</s> <s>Smoking is directly responsible every year for about 26,000 deaths in England from lung cancer and over three times this number from other diseases.</s></p> 

<p><s>Smoking can harm those close you, too;  those who breathe your smoke, including your children, can become ill as a result.</s></p> 

<p><s>You may think that stopping smoke will not be easy.</s> <s>On the other hand, you will never know until you try.</s> <s>In recent years, more than 11 million smokers in Britain have kicked the habit.</s> <s>Nine out of ten have done so without any medical advice.</s> <s>Lots of them tried several times before succeeding.</s> <s>And for most, the first few days were the worst!</s></p> 

<p><s>After that, the compensations make it all worthwhile.</s> <s>You are healthier and breathe more easily, you can taste food again, you don't smell of smoke and you feel like a winner!</s></p> 

<p><s>Telephone the Quitline on 071-487-3000.</s></p> 

<p><head><s>Tips on giving up.</s></head></p>

<p><s>Think of the reasons why you want to stop and keep reminding yourself of them.</s> <s>Pick a day for giving up.</s> <s>Don't be tempted to have a cigarette not even one.</s> <s>Tell yourself you are non-smoker.</s> <s>And, when you've stopped, tell other people you don't smoke.</s> <s>Take one day at a time.</s> <s>Every day without a cigarette is a success.</s> <s>Some people find it easier to give up with a friend or a group of work.</s> <s>You might be able to help each other.</s> <s>Think about the money you are saving and what you could use the additional money for.</s></p> 

<p><head><s>HIV.</s> <s>Be aware of the risk of HIV and other sexually transmitted diseases.</s></head></p> 

<p><s>Good personal and sexual relations can actively promote health and well-being.</s> <s>Like most things, however, sex is not risk-free and never has been.</s> <s>There has always been the risk of unplanned pregnancy and sexually transmitted infections (or "VD").</s> <s>Now added to these is HIV infection which causes AIDS.</s> <s>At present there is no vaccine and no cure.</s> <s>We need to be extra careful and may need to change our sexual behaviour.</s> <s>Remember, people with HIV infection and other sexually transmitted diseases may look and feel perfectly well and may not know they are infected.</s></p> 

<p><s>The safest way to avoid HIV infection is to remain faithful to one uninfected partner.</s> <s>Effective prevention will rely on practising safer sex by avoiding unprotected casual sex, multiple partners, and by using condom.</s></p> 

<p><s>It makes sense to use condoms;  they not only can help protect against pregnancy, but also against many sexually transmitted diseases including HIV.</s> <s>They may also protect against cancer of the cervix.</s></p> 

<p><s>For drug misusers, the safest path is to inject drugs.</s> <s>But if you do, never share needles, syringes or other injecting equipment with anyone, as they may be contaminated with HIV, Hepatitis viruses or other infections.</s> <s>Remember, drug taking is often against the law, and may well damage your health.</s></p> 

<p><s>Ordinary everyday social contact with someone who has HIV is perfectly safe.</s> <s>For example, the virus can not be passed through touching, sharing cutlery or cups, glasses or plates.</s></p> 

<p><head><s>Help</s></head></p> 

<p><s>If you need advice, your GP, family planning clinic or STD Clinic can help.</s> <s>For confidential advice and information on HIV infection and AIDS ring the National AIDS Helpline free of charge on 0800-567123.</s> <s>The service is available 24 hours a day.</s> <s>For free leaflets ring 0800-555777.</s></p> 

<p><head><s>Be safe at home and on the roads </s></head></p> 

<p><s>Accidents are a major cause of death and serious ill-health, particularly in the home.</s> <s>Each year in England around 4,000 people die and more than 2.5 million require medical attention following home accidents.</s> <s>Road accidents account for the deaths of more than a further 4,000 people, including about a quarter of all deaths among school children.</s></p> 

<p><s>In theory at least, all accidents are preventable.</s> <s>Here are some safety tips, which will help to reduce the risk of accidents occurring to members of your family: </s></p> 

<p><head><s>Tips for the home</s></head></p> 

<p><s>Install a smoke detector.</s> <s>They are not expensive and can save many lives and serious injuries.</s> <s>Beware of the cheap pan.</s> <s>Only fill it one-third full of oil and turn off the heat if you leave the room.</s> <s>Keep medicines, matches, lighters, knives, opened tins and sharp objects out of the reach of children and don't let electrical kettle flex overhang the work surface.</s> <s>Make sure all electrical equipment is safe and don't overload sockets.</s> <s>Keep dangling curtains, bed spreads and drying washing well away form fires and cookers.</s> <s>Use fireguards.</s> <s>Stop smoking.</s> <s>Until you do, ensure that you dispose of ash and stubs safely.</s> <s>Never smoke in bed.</s> <s>Beware of damaged carpets or loose rugs.</s> <s>Make sure there is good lighting in hallways and stairs.</s></p> 

<p><head><s>Tips for the roads</s></head></p> 

<p><s>Pedestrians.</s> <s>Stop and look before you cross.</s> <s>Use pedestrian crossings whenever possible.</s> <s>Cyclists.</s> <s>Always ware a safety helmet when out on your bike.</s> <s>Be safe - be seen.</s> <s>Wear bright colours/fluorescent clothing during the day and reflective clothing at night as a pedestrian, cyclist or motorcyclist.</s> <s>Seat belts can save your life.</s> <s>Always use a seatbelt or properly fitted child restraint when travelling in the car or taxi.</s> <s>The law requires everyone to ware seat belts (including rear seat belts) where they are available.</s> <s>Don't drink and drive.</s></p> 

<p><s>You can get more advice and information on health matters form the health promotion unit in your local health authority (find them in your telephone directory).</s> <s>You could also ring the Health Literature Line which is run by the Department of Health.</s> <s>Information on a wide variety of health related subjects is available, free of charge, by calling 0800-555777.</s></p> 

<p><s>The health Education Authority produce many leaflets and booklets on a wide range of topics, some of which are listed below.</s></p>

<p><s>Enjoy healthy eating.</s> <s>That's the limit - a guide to sensible drinking.</s> <s>Stopping smoking made easier.</s> <s>Passive smoking - questions and answers.</s> <s>Can you avoid cancer?</s> <s>Your guide to safe sex and the condom.</s> <s>these are available free from: </s> <s>The Distribution Department, Health Education Authority, Hamilton House, Mabledon Place, London WC1H 9XT</s></p> 

<p><s>Further copies of you and your health are available from: </s> <s>BAPS, Health Publications Unit, Storage and Distribution Centre, Heywood Stores, Manchester Road, Heywood, Lancashire, OL10 2PZ.</s></p>  

</body>
</text>
</cesDoc>