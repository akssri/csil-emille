<cesDoc id="eng-w-social-learning" lang="eng">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>eng-w-social-learning.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>translated and typed by</respType>
<respName>Winfocus Pvt Ltd (Soniya Malhotra)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-05-21</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Services for people with learning disabilities</h.title>
<h.author>Manchester City Council</h.author>
<imprint>
<pubPlace>Manchester</pubPlace>
<publisher>Manchester City Council</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-04-11</date>
</creation>
<langUsage>Translated into English from Hindi translation of original English (translation made on behalf of the University of Lancaster for the EMILLE Project)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="NULL" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s>Services for people with learning disabilities</s></head></p>
<p><head><s>Menchester Community Care</s></head></p>
<p><head><s>Services for people with learning disabilities</s></head></p>

<p><s>City Council Health Services and Volunteer and Private Organization provide special services to people with learning disabilities for their requirements.</s> <s>People with learning disabilities should also get every opportunity to take part in the social activities of the local community.</s></p>

<p><head><s>How to get help</s></head></p>

<p><s>If you yourself or anyone you know has a learning disability and needs help, then contact your local social services department (addresses of these are given at the end of the leaflet).</s> <s>They will try their best to provide you with advice, proper information or assessment (estimation of assessment required) for you (for this please see the leaflet "How to get help from social services").</s> <s>If you think you only need help from a health or social services welfare committee, then contact that organisation directly.</s> <s>You will be asked different questions if you are eligible.</s> <s>Probably a proposal will be put for your care, in which any of the services mentioned below may be included.</s></p>

<p><head><s>City Council and Health Services</s></head></p>

<p><head><s>Residence and Shelter</s></head></p>

<p><s>Manchester Social Services funds different types of residential services.</s>
<s>There are 11 housing schemes like this where provision is made for 200 and more than 200 employees in these houses.</s> <s>Schemes to find houses for adults, who help those individuals who want to live with their family.</s> <s>This scheme has already started rendering short term help to affected people with learning disabilities and their carers.</s> <s>A short term facility is provided to the physically handicapped along with their carers in 5 houses in the city.</s> <s>In a house in South Manchester people with behavioural difficulties are given services.</s> <s>Children are sent back along with other families to their homes by the home scheme.</s></p>

<p><head><s>Support Services</s></head></p>

<p><s>Nearby in North and Central Manchester, there is a team of a social worker, manager, specialist carers and management employees.</s> <s>There is a support team in every district, where a manager, nurses, occupational therapist, physiotherapist, speech and language therapist (doctors that help with speech), psychiatrist and management employees work.</s> <s>Although based in different places the Health and Social department employees work together.</s></p>

<p><s>Contacts:</s></p>

<p><s>Central Manchester</s>
<s>Community Support Support Team</s>
<s>Ross Place</s>
<s>Aked Close</s>
<s>Manchester M12 4AN</s>
<s>2735412</s></p>

<p><s>North Manchester Community Support Team</s>
<s>Ardwick Resource Centre</s>
<s>Beech Mount, Hapurhey</s>
<s>Manchester M9 1XU</s>
<s>205 1364/4926</s></p>

<p><s>Health and Social Services employees jointly provide services from the same office in South Manchester.</s></p>

<p><s>Leaflets with more detailed informaiton on these sevices are available.</s> <s>For these please contact Wythenshaw District Office (addresses and phone numbers are given at the end of this leaflet).</s></p>

<p><head><s>Day Services </s></head></p>

<p><s>There are five day centres in thecity, which provide various services for recreation and getting together.</s> <s>Day services are available at Harprey and Fallowfield/Rushholmes.</s></p>

<p><s>The Education Department has six schools for people with learning disabilities.</s> <s>The Education Department runs various courses for people with learning disabilities.</s></p> 

<p><s>The Department of Social Services directs the employment opportunities for the people with learning disabilities.</s></p>

<p><head><s>Self-help Organizations</s></head></p>

<p><s>Self-help Organizations provide services for people with learning disabilities in the areas such as cooperation in housing, recreation and daytime activities, self-advocacy groups and sports group for parents are included.</s></p>

<p><s>To obtain any kind of information related to these services please contact or ringyour local social services office, whose addresses and the telephone numbers are given at the end of this leaflet.</s></p>

</body>
</text>
</cesDoc>