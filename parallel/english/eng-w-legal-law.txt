<cesDoc id="eng-w-legal-law" lang="eng">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>eng-w-legal-law.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Paul Baker</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>01-12-06</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Health and Safety Law</h.title>
<h.author>Health and Safety Executive</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Health and Safety Executive</publisher>
<pubDate>October 1999</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>01-12-06</date>
</creation>
<langUsage>English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality> 
</textClass>
<translations>
<translation trans.loc="NULL" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s>Health and Safety Law</s></head></p>

<p><head><s>What you should know</s></head></p>

<p><s>Your health, safety and welfare at work are protected by law.</s></p>

<p><s>Your employer has a duty to protect you and keep you informed about health and safety.</s></p>

<p><s>You have a responsibility to look after yourself and others.</s></p>

<p><s>If there is a problem, discuss it with your employer or safety representative, if there is one.</s></p>

<p><s>This leaflet is a brief guide to health and safety law.</s> <s>It does not describe the law in detail, but it does list the key points.</s></p>

<p><s>Your employer has a duty under the law to ensure, so far as is reasonably practicable, your health, safety and welfare at work.</s></p>

<p><s>Your employer must consult you or your safety representative on matters relating to your health and safety at wok, including:</s>

<s>any change which may substantially affect your health and safety at work, eg in procedures, equipment or ways of working;</s>
<s>the employer's arrangments for getting competent people to help him/her satisfy health and safety laws;</s>
<s>the information you have to be given on the likely risks and dangers arising from your work, measures to reduce or get rid of these risks and what you should do if you have to deal with a risk or danger;</s>
<s>the planning of health and safety; and</s>
<s>the health and safety consequences of introducing new technology.</s></p>

<p><s>In general, your employer's duties include:</s>
<s>making your workplace safe and without risks to health;</s>
<s>ensuring plant and machinery are safe and that safe systems of work are set and followed;</s>
<s>ensuring articles and substances are moved, stored and used safely;</s>
<s>providing adequate welfare facilities;</s>
<s>giving you the information, instruction, training and supervision necessary for your health and safety.</s></p>

<p><s>In particular, your employer must:</s>
<s>assess the risk to your health and safety;</s>
<s>make arrangements for implementing the health and safety measures identified as being necessary by the assessment;</s>
<s>if there are five or more employees, record the significant findings of the risk assessment and the arrangements for health and safety measures;</s>
<s>if there are five or more employees, draw up a health and safety policy statement, including the health and safety organisation and arragements in force, and bring it to your attention;</s>
<s>appoint someone competent to assist with health and safet responsibilities, and consult you or your safety representative about this appointment;</s>
<s>co-operate on health and safety with other employers sharing the same workplace;</s>
<s>set up emergency procedures;</s>
<s>provide adequate first-aid facilities;</s>
<s>make sure that the workplace satisfies health, safety and welfare requirements, eg for ventilation, temperature, lighting and sanitary, washing and rest facilities;</s>
<s>make sure that work equipment is suitable for its intended use, so far as health and safetyis concerned, and that it is properly maintained and used;</s>
<s>prevent or adequately control exposure to substances which may damage your health;</s>
<s>take precautions against danger from flammable or explosive hazards, electrical equipment, noise and radiation;</s>
<s>avoid hazardous manual handling operations, and where they cannot be avoided, reduce the risk of injury</s>
<s>provide health surveillance as appropriate;</s>
<s>provide free any protective clothing and equipment, where risks are not adequately controlled by other means;</s>
<s>ensure that appropriate safety signs are provided and maintained;</s>
<s>report certain injuries, diseases and dangerous occurences to the appropriate health and safety enforcing authority (see box below for who this is).</s></p>

<p><s>As an employee you have legal duties too.</s> <s>They include:</s>
<s>taking reasonable care for your own health and safety and that of others who may be affected by what you do or do not do;</s>
<s>co-operating with your employer on health and safety;</s>
<s>correctly using work items provided by your employer, including personal protective equipment, in accordance with training or instructions; and</s>
<s>not interfering with or misuing anything provided for your health, safety or welfare.</s></p>

<p><s>If you think there is a health and safety problem in your workplace you should first discuss it with your employer, supervisor or manager.</s> <s>You may also wish to discuss it with your safety representative, if there is one.</s> <s>You, your employer or your safety representative can get information on health and safety in confidance by calling HSEs InfoLine telephone service on 0541 545500.</s></p>

<p><s>If you think your employer is exposing you to risks or is not carrying out legal duties, and you have pointed this out without getting a satisfactory answer, you can contact the enforcing authority for health and safety in your workplace.</s> <s>Health and safety inspectors can give advice on how to comply with the law.</s> <s>They also have powers to enforce it.</s> <s>HSE's Employment Advisory Service can give advice on health at work.</s> <s>Your employer can give you their names and addresses.</s></p>

<p><s>You can get advice on general fire precautions etc from the Fire Brigade or your fire officer.</s></p>

<p><s>More detailed guidance on health and safety law is set out in HSE priced publications, such as:</s>

<s>Essentials of health and safety at work HSE Books ISBN 0 7176 0715 X;</s>
<s>and in free leaflets such as:</s>
<s>An Introduction to health and safety INDG259 HSE Books.</s></p>

<p><s>Full details of HSE publications can be found on HSE's Web site at: http://www.open.gov.uk/hse/hsehome.htm</s></p>

<p><s>All HSE publications are available from HSE Books, PO Box 1999, Sudbury, Suffolk, CO10 2WA</s> <s>Tel 01787 881165</s> <s>FAX 01787 313995.</s></p>

<p><s>HSE priced publications are also available from good booksellers.</s></p>

</body>
</text>
</cesDoc>