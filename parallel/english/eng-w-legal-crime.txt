<cesDoc id="eng-w-legal-crime" lang="eng">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>eng-w-legal-crime.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Paul Baker</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress> 
<availability region="WORLD"></availability>
<pubDate>02-02-22</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Victims of Crime</h.title>
<h.author>The Home Office</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>The Home Office</publisher>
<pubDate>2002</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-02-07</date>
</creation>
<langUsage>English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type= "composite"></constitution>
<derivation type= "translation"></derivation>
<domain type= "public"></domain>
<factuality type= "fact"></factuality> 
</textClass>
<translations>
<translation trans.loc="NULL" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>
<p><head><s>Victims of Crime</s></head></p>

<p><s>This leaflet explains what will happen now you have reported a crime to the police.</s></p>

<p><s>It tells you:</s></p>

<p><s>How the crime will be investigated and what will happen if you have to go to court;</s></p>

<p><s>How you may be able to get compensation if you have been injured or if your property has been stolen or damaged;</s></p>

<p><s>How the police can help you;</s></p>

<p><s>That the police will pass information about you to Victim Support so that they can offer you help and support, unless you ask the police not to;</s></p>

<p><s>What other help and advice is available.</s></p>

<p><head><s>THE POLICE</s></head></p>

<p><s>If you need to talk to the police about the crime, you should contact the officer dealing with your case or, where there is one, the crime desk.</s> <s>The contact details are:</s></p>

<p><s>Police Force:</s></p>

<p><s>Police Station:</s></p>

<p><s>Telephone Number:</s></p>

<p><s>Name of officer dealing with your case:</s></p>

<p><s>Rank and Number:</s></p>

<p><s>Crime Reference Number:</s></p>

<p><s>Telephone number of crime desk (where relevant):</s></p>

<p><s>Whenever you report a crime to the police, they make enquiries to try to solve it.</s> <s>Wherever possible you should:</s>
<s>give them as much information as you can about the offence;</s>
<s>tell them if you are worried about your (or your family's) safety so they can give you appropriate advice.</s> <s>If, for instance, you are worried about the suspect being granted bail, they will tell the Crown Prosecution Service of your concern;</s>
<s>tell the police if you change your address or telephone number so that they can
tell you if someone has been arrested, charged or cautioned in connection with the offence;</s>
<s>tell them of any other changes: for example, you may have noticed further losses or damage since you first reported the offence, or you may be suffering further effects from an injury caused by the crime;</s>
<s>tell them if the offence was made worse by racial abuse or hatred directed at you.</s></p>

<p><head><s>WHAT HAPPENS NEXT?</s></head></p>

<p><s>The police will try to catch the criminal, but they may not always succeed.</s> <s>Even if they do have a suspect, there may not be enough evidence to charge the person or, if the suspect is young or mentally disordered, and the offence is not too serious, the police may decide to caution him or her instead.</s> <s>In some cases the suspect may be facing more serious charges for other offences and may be prosecuted for those offences instead.</s></p>

<p><s>If the police decide to charge someone, the case is taken over by the Crown Prosecution Service (CPS), an independent authority, which prosecutes in the name of the Queen.</s> <s>They decide whether there is enough evidence to provide a realistic prospect of conviction.</s> <s>This means they must be satisfied that it is more likely than not that the person accused will be convicted of the charge.</s> <s>They must then consider whether a prosecution would be in the public interest.</s></p>

<p><s>This means taking into account other factors, apart from the evidence, when deciding whether or not to prosecute.</s> <s>For example, it may not be necessary to prosecute an elderly person for shoplifting if the item stolen is of very small value.</s> <s>The CPS do not act directly on behalf of individual victims or represent them in criminal proceedings, but they do carefully consider the interests of victims when deciding where the public interest lies.</s></p>

<p><head><s>THE PRESS</s></head></p>

<p><s>The press can play an important role in tackling crime.</s> <s>For the purposes of investigating an offence, catching criminals or for crime prevention, the police may release details of a case to the press.</s> <s>Personal details are not normally released without the person's permission.</s> <s>But if you are concerned about the release of case details, tell the police officer dealing with your case.</s></p>

<p><head><s>GOING TO COURT</s></head></p>

<p><s>The majority of cases that reach court are dealt with by magistrates' courts.</s> <s>The most serious cases have to be sent to the Crown Court.</s> <s>If your case goes to court and you are needed as a witness:</s>
<s>You will be sent a copy of the leaflet 'Witness in Court' which will explain what is likely to happen.</s>
<s>You will be offered support by the Witness Service.</s>
<s>You should let the police know if there are any days when it would be difficult for you to attend court.</s> <s>If possible, these dates will then be avoided.</s> <s>But there may be times when the case has to go ahead even though this may not be convenient for individual witnesses.</s>
<s>If you are called to give evidence and are unable to give it in English, arrangements will be made to provide an interpreter.</s></p>

<p><s>If you are not needed as a witness, the police will try to tell you about hearing dates.</s> <s>There could be several such dates if, for example, cases are delayed or postponed.</s> <s>They will also try to tell you about the result of the case.</s></p>

<p><head><s>COMPENSATION</s></head></p>

<p><s>If you have been injured or your property has been damaged or stolen as a result of a crime, compensation may be available in some cases.</s> <s>If you think you may qualify:</s></p>

<p><head><s>Write Down</s></head></p>

<p><s>Any extra expenses that you have had as a result of the offence, for example medical expenses or the cost of repairing or replacing your property.</s>
<s>Any loss of earnings you have suffered.</s>
<s>Any income you have received as a result of the offence (for example Social Security benefits).</s></p>

<p><head><s>Keep</s></head></p>

<p><s>Any receipts, estimates or other documents about any of these things.</s></p>

<p><head><s>Criminal Injuries Compensation Scheme</s></head></p>

<p><s>If you have been injured because of a crime of violence you can apply for a payment under the Criminal Injuries Compensation Scheme.</s> <s>The responsibility for making a claim is yours.</s> <s>It does not matter whether the offender has been caught, but there are other rules which determine whether or not you receive any money.</s> <s>You can find out more from the leaflet 'Victims of Crimes of Violence, a guide to the Criminal Injuries Compensation Scheme'.</s> <s>You can get this from the police, from Victim Support Schemes, your Citizens Advice Bureau or from the Criminal Injuries Compensation Authority, Tay House, 300 Bath Street, Glasgow, G2 4JR (Telephone 0800 358 3601).</s></p>

<p><head><s>Compensation from the Offender</s></head></p>

<p><s>If someone is convicted, the criminal court may order the offender to pay you compensation for any injury, loss or damage which you have suffered because of the
offence.</s> <s>Unlike a claim under the Criminal Injuries Compensation Scheme (see previous section) you cannot apply for a compensation order yourself.</s> <s>It is therefore important that you tell the police if you would like to receive compensation.</s> <s>They will give you a form on which you should give accurate details of your losses.</s> <s>This should be supported by documentary evidence (e.g.</s> <s>receipts) where possible.</s> <s>The police will then pass this information to the Crown Prosecution Service who will make sure that the court knows about it.</s> <s>You can be compensated for personal injury; losses because of theft of, or damage to, property; losses because of fraud; loss of earnings while off work; medical expenses; travelling expenses; pain and suffering; loss, damage or injury caused to or by a stolen vehicle.</s></p>

<p><s>The dependants of a victim who has died (other than as the result of a road traffic incident) may also receive compensation (for example, through the courts or from the Criminal Injuries Compensation Scheme - see previous section).</s></p>

<p><head><s>The Role of the Court</s></head></p>

<p><s>The court has to consider compensation in every appropriate case and decide whether to order an offender to pay compensation and, if so, how much.</s> <s>The court must take account of the offender's circumstances and ability to pay.</s> <s>So if the court does decide to make an order, it may not be for the full amount of your loss.</s></p>

<p><s>If the court decides to make an order against the offender, he or she will be required to pay the money to the court, which will pass it on to you.</s> <s>If the offender has enough money, the compensation will normally be paid in a lump sum.</s> <s>In most cases, however, the court will allow time for the offender to pay, or may allow the offender to pay by instalments.</s></p>

<p><s>It is the job of the court to make sure that the offender pays the compensation.</s> <s>So if you have any questions about the compensation order or the way it will be paid to you, you should contact the clerk of the court.</s> <s>Do not contact the offender direct.</s></p>

<p><head><s>Civil Proceedings</s></head></p>

<p><s>Whether or not the offender is convicted in the criminal courts, you can sue him or her for damages in a civil court.</s> <s>You can find out more about this at your Citizens Advice Bureau or by asking a solicitor.</s></p>

<p><head><s>Motor Insurers' Bureau</s></head></p>

<p><s>If you suffer injury, loss or damage to property as a result of a road traffic incident involving a motor vehicle, compensation will normally be payable under insurance arrangements.</s> <s>Where the offender is uninsured, compensation for personal injury and damage to property may be available from the Motor Insurers' Bureau.</s> <s>Where the offender has not been traced, compensation for personal injury only may be available.</s> <s>To claim, you should contact the Motor Insurers' Bureau at 152 Silbury Boulevard, Central Milton Keynes, MK9 1NG.</s> <s>(Telephone: 01908 830001).</s></p>

<p><s>You can get further advice from the leaflet 'Victims of Uninsured and Untraced Drivers' which is available from the Department of Transport, Local Government and the Regions, Road Safety Division, Zone 2/14, Great Minster House, 76 Marsham Street, London SW1P 4DR.</s></p>

<p><s>If you should succeed in getting compensation in two or more ways, for example from a criminal court and through the Criminal Injuries Compensation Scheme or the Motor Insurers' Bureau, the award may be reduced to avoid double payment.</s> <s>You can't get compensation twice for the same thing from public funds.</s></p>

<p><head><s>CHECK LIST FOR ACTION</s></head></p>

<p><s>If you report a crime to the police you can expect:</s>
<s>the police to investigate the crime;</s>
<s>to be contacted by Victim Support, unlessyou ask the police not to pass on your details1;</s>
<s>;to be told by the police if someone is charged with, or cautioned for, the offence;
to be told (if you ask to be) by the police or the Crown Prosecution Service if the charge is later dropped or altered;</s>
<s>if you are needed as a witness, to be given the 'Witness in Court' leaflet, told the date of the trial, and offered support by the Witness Service;</s>
<s>if the case goes to court, consideration to be given to making a compensation
order in your favour;</s>
<s>to be told (if you ask to be) the result of the court case; in the more serious cases,</s></p>

<p><s>1 Victims of sexual crimes or domestic violence must give specific consent before their details are passed on to Victim Support.</s></p>

<p><s>to be told (if you ask to be) the result of any appeal against conviction or sentence;</s>
<s>to be given advice about applying for compensation for personal injury from the Criminal Injuries Compensation Authority; and</s>
<s>to be given crime prevention advice by the police if you ask for it.</s></p>

<p><s>To help make this possible, you should:</s>
<s>report the crime to the police promptly;</s>
<s>give the police full details of your injury or loss;</s>
<s>tell the police if you want compensation;</s>
<s>tell the police if you fear for your (or your family's) safety;</s>
<s>tell the police if you do not want the press to be informed;</s>
<s>tell the police if you do not want your name and contact details passed to Victim Support;</s>
<s>tell the police of any change of address while the case remains unresolved; and
contact Victim Support direct if you want to.</s></p>

<p><head><s>PROTECTING YOURSELF AGAINST CRIME</s></head></p>

<p><s>Unfortunately, some people who have been the victim of crime are more vulnerable, in the short term, to further crime.</s> <s>Whatever the circumstances which led to you becoming a victim of crime, it may be possible to take steps now to help reduce the risk of it happening again.</s> <s>The police can offer you free crime prevention advice, if you ask them.</s></p>

<p><s>You may also find useful the Home Office crime prevention guide 'Your Practical Guide to Crime Prevention'.</s> <s>It gives details of simple precautions that you can take to protect yourself, your family and your property.</s> <s>Copies are available from your local police station.</s></p>

<p><s>If there is a local Neighbourhood Watch Scheme, you may want to join it.</s> <s>If there is no scheme in your area, you might want to talk to your local crime prevention officer about starting one - there may be other people in your area who would be interested.</s></p>

<p><head><s>PROTECTION FROM HARASSMENT</s></head></p>

<p><s>The Protection From Harassment Act 1997 made it a criminal offence to make another person fear that violence will be used against them.</s> <s>It also made it an offence to cause harassment to somebody, for example, by 'stalking'.</s> <s>Those who commit the more serious offence of making someone fear violence can receive a fine and/or up to five years imprisonment.</s> <s>Those who cause harassment can receive a fine and/or up to six months imprisonment.</s></p>

<p><s>If you have suffered harassment or fear of violence and the offender has been caught and convicted, the criminal court can make a restraining order against the offender to stop the threats or harassment continuing.</s> <s>If the offender is convicted of breaching the restraining order he or she faces a penalty of up to five years in prison.</s></p>

<p><s>You can also seek an injunction from a civil court to put a stop to harassment.</s> <s>Breach of an injunction is an arrestable criminal offence, which carries a maximum penalty of five years imprisonment and/or an unlimited fine.</s></p>

<p><head><s>PROTECTION AGAINST ANTI-SOCIAL BEHAVIOUR</s></head></p>

<p><s>If someone's anti-social behaviour has been causing you harassment, alarm or distress, you can ask the police or local authority to apply to the magistrates' court for an Anti-Social Behaviour Order to stop this happening.</s> <s>You may not need to appear or be identified in court, because the court can ask to hear the evidence of a professional witness.</s> <s>This means that someone from the police or local authority can come out to see the behaviour for him or herself and give evidence on your behalf.</s> <s>The magistrates will decide if the grounds for an Order have been satisfied.</s></p>

<p><s>An Anti-Social Behaviour Order lasts at least two years.</s> <s>An offender who disobeys an Order can face a penalty of up to five years imprisonment.</s></p>

<p><head><s>PROTECTION AGAINST SEX OFFENDERS</s></head></p>

<p><s>If the behaviour of a known sex offender is causing concern in your community, the police can apply to the magistrates' court for a Sex Offender Order.</s> <s>This type of Order protects the public by prohibiting the defendant from doing specific things (e.g.</s> <s>lingering outside a school or in a playground).</s> <s>It lasts for at least five years.</s> <s>An offender who breaches the Order can face a penalty of up to five years imprisonment.</s></p>

<p><head><s>REMEMBER</s></head></p>

<p><s>If you are not sure about anything mentioned in this leaflet, ask your local police for advice.</s> <s>Victim Support are also willing to help whenever they can.</s></p>

<p><head><s>HELP FROM VICTIM SUPPORT SCHEMES</s></head></p>

<p><s>You may be feeling shocked, sad, distressed, or angry following the crime you have just reported.</s> <s>These feelings are common.</s> <s>Victim Support volunteers are specially trained to help you through this experience and to provide you with practical help and information.</s> <s>The police will normally tell Victim Support about all cases of burglary, theft, criminal damage, arson, assault (other than domestic violence), and racial harassment.</s> <s>If you do not want your name passed on as a victim of crime, tell the police officer dealing with your case.</s> <s>If you have suffered a sexual crime or domestic violence, you will be specifically asked if you wish to be referred to Victim Support.</s> <s>For any type of crime, you can also contact them yourself at a later date if you prefer.</s></p>

<p><s>Because resources are limited, victims whose cars are stolen or vandalised will not normally be referred to Victim Support.</s> <s>If you need help, tell the police officer in your case or contact Victim Support direct.</s></p>

<p><s>If you have been the female victim of rape or sexual assault or abuse you may alternatively wish to seek help through a local rape crisis group affiliated to the Rape Crisis Federation (Telephone 0115 934 8474).</s></p>

<p><head><s>DETAILS ABOUT VICTIM SUPPORT</s></head></p>

<p><s>Your local Victim Support Scheme is:</s></p>

<p><s>Or contact the Victim Supportline: 0845 30 30 900</s></p>

<p><s>Or, if you prefer, you can write to Victim Supportline at: PO Box 11431, London SW9 6ZH.</s></p>

<p><s>Produced by Home Office Communication Directorate 1/2002.</s> <s>CC1.1M</s></p>

<p><s>© Crown Copyright 1997, 1999, 2000, 2001, 2002</s></p>

</body>
</text>
</cesDoc>