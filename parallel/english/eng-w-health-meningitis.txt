<cesDoc id="eng-w-health-meningitis" lang="eng">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>eng-w-health-meningitis.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Paul Baker</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>01-07-24</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Knowing about Meningitis and Septicaemia</h.title>
<h.author>Department of Health</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department of Health</publisher>
<pubDate>February 1995</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>01-07-24</date>
</creation>
<langUsage>English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality> 
</textClass>
<translations>
<translation trans.loc="NULL" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s>Knowing about Meningitis and Septicaemia</s></head></p>

<p><head><s>Anyone, anywhere, can get meningitis.</s> <s>Knowing the symptoms of meningitis could mean the difference between life and death.</s></head></p> 

<p><s>Meningitis - inflammation of the lining of the brain - can be caused by several different germs.</s> <s>Some are bacteria and some are viruses.</s> <s>The bacterial germs can also cause septicaemia - blood poisoning or an infection of the blood.</s> <s>Meningitis caused by a virus (viral eningitis) is generally less serious.</s></p>

<p><s>Bacterial meningitis is quite rare, but can be very serious.</s> <s>There are three main types of bacterial meningitis.</s> <s>They are named after the germs that cause the infection.</s> <s>The three types are:</s></p>

<p><s>Menigococcal</s> <s>Pneumococcal</s> <s>Haemophilus influenzae type b (known as Hib).</s></p>

<p><s>Each year there are around 1,800 reported cases of bacterial meningitis in England and Wales, but this figure is now falling fast because of the success of the Hib vaccine.</s></p> 

<p><head><s>Is it only children who get the disease?</s></head></p>

<p><s>Anyone of any age can get meningitis or septicaemia.</s> <s>The most common type of meningitis in children under four used to be Hib.</s> <s>This germ could also cause septicaemia.</s></p>

<p><s>Immunisation against Hib infection is now possible.</s> <s>Children are immunised at two, three or four months - at the same time as diphtheria, whooping cough, tetanus and polio.</s> <s>If you do not remember your child being immunised, and he or she is under four, you should contact your doctor.</s></p> 

<p><s>Now, the meningococcal germ is the most common cause of meningitis and septicaemia in all age groups.</s> <s>Young people and adults also get the pneumococcal form of meningitis and septicaemia.</s> <s>Both forms can be very serious and must be treated immediately.</s></p>

<p> <s>If the illness is diagnosed early and treated quickly, most people recover completely.</s> <s>But in some cases the infection can kill or lead to a permanent disability such as deafness or brain damage.</s></p> 

<p><head><s>What are the symptoms of meningitis and septicaemia?</s></head></p>

<p><s>Recognising the symptoms could mean the difference between life and death.</s></p>

<p> <s>Meningitis is not easy to identify at first because the symptoms are similar to those of 'flu.</s></p>

<p> <s>But meningitis develops quickly, sometimes in just in few hours, and the patient will soon become seriously ill.</s></p>

<p> <s>The symptoms may not all appear at the same time, and they may be different in young babies, children and adults.</s></p>

<p> <s>Babies May have a staring expression and a fever.</s> <s>They may refuse feeds or they may vomit.</s> <s>They may be distressed and make a shrill or moaning cry when you pick them up.</s> <s>Babies with meningitis can be very difficult to wake up.</s> <s>The fontanelle - the soft spot on the top of the baby's head - may be tense or bulging, and the skin may be pale and blotchy.</s> <s>There may also be a rash of red or purple spots or bruises anywhere on body.</s></p>

<p> <s>Older children, young people and adults often have a fever, pains in their back or joints, and vomiting.</s> <s>They may have a severe headache or shy away from bright lights.</s> <s>They may develop a stiff neck, feel sleepy or become confused.</s> <s>Again, a rash of red or purple spots or bruises anywhere on the body is a very serious sign.</s></p> 

<p><head><s>What kinds of immunisation are there? </s></head></p>

<p><s>Researchers around the world are working to produce more effective vaccines against these infections.</s> <s>Hib vaccine is already in routine use and is very successful, but as yet there is no vaccine against the most common strain of the meningococcal germ.</s> <s>The Hib vaccine does not protect against the meningococcal germs.</s></p>

<p><s>There is a vaccine against rarer strains. </s> <s>when there are connected cases of meningococcal infection involving the rarer strain in a school, college or university, some people may be offered a vaccine.</s> <s>Unfortunately, the vaccine does not work in small children, who are most at risk.</s></p> 

<p><head><s>How is meningitis treated?</s></head></p>

<p> <s>Antibiotics are used to treat bacterial meningitis.</s> <s>They are also given to immediate family members and anyone else who is in close contact with the person who has the disease.</s> <s>Antibiotics are not used for viral meningitis.</s></p> 

<p><head><s>Can you get meningitis by coming into contact with someone who has it?</s></head></p>

<p><s>The germs that cause bacterial meningitis are very common and live in the back of the nose and throat.</s> <s>People of any age can carry these germs for weeks or months without becoming ill.</s> <s>It is only rarely that they overcome the body's defences and cause meningitis or septicaemia.</s> </p>

<p><s>Coughing, sneezing and kissing spread the germs, but they do not live for more than a few seconds outside the body and are not easily passed from one person to another.</s> <s>You can not get meningitis from water supplies, swimming pools, factories or buildings.</s></p> 

<p><head><s>What is septicaemia?</s></head></p>

<p><s>Septicaemia often occurs with meningococcal meningitis.</s> <s>The germ enters the body from the throat and travels through the blood.</s> <s>In some cases, the germs multiply in the bloodstream and cause blood poisoning.</s> <s>This may happen on its own or with an attack of meningitis.</s></p>

<p><s>People with septicaemia usually have a rash which can be anything from tiny red spots to large blotchy bruises.</s> <s>Their skin may be pale and clammy, even though they have a fever.</s></p> 

<p><head><s>Where do you get the rash?</s></head></p>

<p> <s>The rash can appear anywhere on the body and in the early stages may be a little more than a small red spots in the skin.</s> <s>It can develop very quickly, and in a matter of hours the spots can grow to red or purple bruises.</s> <s>It may be more difficult to see the rash if you have a dark skin.</s> <s>The spots or bruises must be taken seriously - call your doctor immediately.</s> <s>(A rash is not necessarily present in every case of meningitis)</s></p> 

<p><head><s>How soon should you call a doctor? </s></head></p>

<p><s>The meningococcal germ that cases meningitis or septicaemia can be very serious.</s> <s>But you can prevent the consequences by acting quickly.</s></p>

<p><s>If you think you or your child might have meningitis, contact your doctor immediately.</s> <s>Explain why you are concerned.</s> <s>Describe the symptoms carefully and ask for advice.</s> <s>If your doctor is not available, go straight to the nearest hospital casualty department.</s> <s>Bacterial meningitis or septicaemia must be treated early.</s></p>

<p><s>If you suspect meningitis or septicaemia, call your doctor immediately.</s> <s>Trust your instincts - a life could depend on it.</s></p>  

<p><s>For more information, contact the Nation Meningitis Trust, Fern House, Bath Road, Stroud, Gloucestershire GL5 3TH (tel. 01453 751738).</s> <s>Or phone their 24-hour helpline on 01453 755049.</s></p>

<p><s>Further copies of this booklet, and translations in Chinese, Vietnamese, Greek, Turkish, Bengali, Gujarati, Hindi and Urdu, can be obtained free by rining 0800 555777 or by writing to:</s></p>

<p><s>BAPS</s> <s>Health Publications Unit</s> <s>Heywood Stores,</s> <s>Manchester Road, Heywood,</s> <s>Lancashire OL10 2PZ</s></p>

</body>
</text>
</cesDoc>