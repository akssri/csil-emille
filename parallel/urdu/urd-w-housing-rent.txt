<cesDoc id="urd-w-housing-rent" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-housing-rent.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Miss Shehnaz Nawab)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-12-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Do you rent, or are you thinking of renting, from a private landlord?</h.title>
<h.author>Department for Transport, Local Government and the Regions</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department for Transport, Local Government and the Regions</publisher>
<pubDate>February 2002</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-11-23</date>
</creation>
<langUsage>Translated into Urdu from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="parallel file identifer" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s><foreign lang="eng">DTLR</foreign></s>
<s><foreign lang="eng">TRANSPORT LOCAL GOVERNMENT REGIONS</foreign></s></head></p>

<p><head><s>
کیا آپ پرائیویٹ مالک مکان کے کراۓ کے مکان میں رہ رہے ہیں یا ایسا کرنے کے بارے میں سوچ رہے ہیں ؟
</s></head></p>

<p><head><s>
ہاؤسنگ
</s></head></p>

<p><head><s>
قانون کیا ہے ؟
</s></head></p>

<p><s>
اگر آپ نے اب مکان کرایہ پر لیا ہے تو آپ کی کرایہ داری خودبخود ایشورڈ شارٹ ہولڈ کرایہ داری ہو جاۓ گی ۔
</s> <s>
)ماسواۓ اگر آپ کا مالک مکان تحریری طور پر کسی اور معاہدے پر متفق ہوا ہے( ۔
</s></p>

<p><s>
یہ آپ پر منحصر ہے کہ آپ مالک مکان سے کتنے عرصے کے لۓ کرایہ داری کا معاہدہ کریں ۔
</s> <s>
یہ کسی مخصوص مدت )جسے "فکسڈ ٹرم" کہتے ہیں( یا کسی مدت کے بغیر ہو سکتا ہے ۔
</s></p>

<p><head><s>
مجھے کب مکان خالی کرنے کے لۓ کہا جا سکتا ہے ؟
</s></head></p>

<p><s>
مالک مکان آپ کو     
<foreign lang="eng">6</foreign>
ماہ کے عرصے کے بعد کسی بھی وقت مکان خالی کرنے کے لۓ کہہ سکتا ہے، بشرطیکہ کوئی فکسڈ ٹرم جس کے لۓ آپ متفق ہوۓ ہوں ختم ہو گیا ہو ۔
</s> <s>
اگر مالک مکان آپ کو مکان خالی کرنے کا کہہ رہا ہے تو اسے   
<foreign lang="eng">2</foreign>
ماہ کا تحریری نوٹس دینا چاہۓ ۔
</s></p>

<p><s>
مالک مکان عدالت کی طرف سے کسی بھی وقت مخصوص "وجوہات" کی بناء پر جو قانونی طور پر طے کی گئ ہیں پر مکان خالی کرنے کے لۓ حکم نامہ جاری کر سکتا ہے ۔
</s></p>

<p><s>
ان میں کرایہ کے بقایا جات ـ آپ کا مالک مکان آپ کو مکان سے نکال سکتا ہے اگر آپ نے کم از کم     
<foreign lang="eng">2</foreign>
ماہ یا    
<foreign lang="eng">8</foreign>
ہفتوں کا کرایہ ادا نہیں کیا ہے؛ غیر اخلاقی رویہ ـ آپ کا مالک مکان آپ کو مکان سے نکال سکتا ہے اگر آپ مقامی لوگوں کے لۓ مشکلات پیدا کریں ۔
</s></p>

<p><head><s>
کیا مجھے مکان خالی کرنا ہوگا ؟
</s></head></p>

<p><s>
آپ کو نوٹس ختم ہونے کی تاریخ کو مکان چھوڑنا چاہۓ ۔
</s> <s>
البتہ اگر مالک مکان آپ کو مکان خالی کرنے کے لۓ مجبور نہیں کر سکتا تو وہ عدالت کی طرف سے مکان خالی کرنے کا حکم نامہ جاری کر سکتا ہے ۔
</s> <s>
اگر آپ نے حکم نامہ میں دی گئ تاریخ تک مکان خالی نہیں کیا تو مالک مکان عدالت سے آپ کو مکان سے نکالنے کے لۓ وارنٹ حاصل کرنے کی درخواست دے سکتا ہے ۔
</s> <s>
اس کے بعد عدالت بیلیف مقرر کر کے آپ کو مکان سے نکال دے گی ۔
</s></p>

<p><head><s>
کیا میں تحریری کرایہ داری کے معاہدے کا حقدار ہوں ؟
</s></head></p>

<p><s>
اگر آپ کو مالک مکان کی طرف سے تحریری معاہدہ نہیں دیا گیا تو آپ اس سے مندرجہ ذیل تفصیلات کے تحریری بیان کے لۓ کہہ سکتے ہیں :
</s> <s>
کرایہ داری شروع ہونے کی تاریخ 
</s> <s>
کرایہ اور اس کی ادائیگی کی تفصیلات 
</s> <s>
کسی کراۓ کے جائزے کی تفصیلات 
</s> <s>
کسی فکسڈٹرم کرایہ داری کی مدت
</s></p>

<p><head><s>
کیا میں کراۓ میں مدد حاصل کر سکتا ہوں ؟
</s></head></p>

<p><s>
اگر آپ دوسرے بینیفٹس حاصل کر رہے ہیں یا آپ کی کم آمدنی ہے تو آپ ہاؤسنگ بینیفٹ کے ذریعے کراۓ میں مدد حاصل کر سکتے ہیں ۔
</s> <s>
اس کے لۓ آپ کو اپنی لوکل اتھارٹی کو درخواست دینی چاہۓ تاکہ آپ یہ معلوم کر سکیں کہ آپ اس کے حقدار ہیں اور اگر ایسا ہے تو آپ کو کتنی رقم مل سکتی ہے ۔ 
</s></p>

<p><s>
کرایہ داری قبول کرنے سے پہلے آپ لوکل اتھارٹی سے درخواست دے کر یہ معلوم کر سکتے ہیں کہ ہاؤسنگ بینیفٹ کے تحت کتنا کرایہ ملے گا ۔
</s></p>

<p><head><s>
اگر میں کرایہ ادا نہ کرنے کی صورت میں بقایاجات میں چلا جاؤں تو کیا ہوگا ؟
</s></head></p>

<p><s>
مالک مکان آپ کو عدالت کی طرف سے مکان خالی کرنے کا حکم جاری کر سکتا ہے، اگر آپ نے کم از کم    
<foreign lang="eng">8</foreign>
ہفتوں )ہفتہ وار کرایہ دینے کی صورت میں(، یا     
<foreign lang="eng">2</foreign>
ماہ )ماہنامہ کرایہ دینے کی صورت میں( تک کا کرایہ ادا نہیں کر رہے ۔
</s> <s>
اگر عدالتی کاروائی کے وقت تک آپ کم از کم اتنے کراۓ کی رقم کے بقایاجات کے ذمہ دار ہیں تو جج مالک مکان کو آپ سے مکان خالی کروانے کا حکم جاری کر دے گا ۔
</s></p>

<p><head><s>
اگر میرے ہاؤسنگ بینیفٹ میں تاخیر ہو جاۓ تو کیا ہوگا ؟
</s></head></p>

<p><s>
فوری طور پر لوکل اتھارٹی کے آفیسر سے بات کریں جو آپ کے کلیم سے نبٹ رہا ہے اور اس بات کی وضاحت کریں کہ مالک مکان آپ کو مکان خالی کرنے کے لۓ کہہ رہا ہے کیونکے آپ کرایہ ادا کرنے میں پیچھے رہ گۓ ہیں ۔
</s></p>

<p><head><s>
اگر میں یہ سمجھوں کہ کرایہ بہت زیادہ ہے تو کیا کروں ؟
</s></head></p>

<p><s>
اگر آپ سمجھتے ہیں کہ آپ باقی اسی قسم کے دوسرے مکانات کے کرایوں کے مقابلے میں زیادہ کرایہ ادا کر رہے ہیں تو آپ کرایہ کا جائزہ لینے والی کمیٹی سے یہ فیصلہ لے سکتے ہیں کہ کرایہ کتنا ہونا چاہۓ ۔
</s> <s>
تفصیلات کے لۓ اپنے قریبی رینٹ ایسمنٹ پینل سے رابطہ قائم کریں ۔
</s> <s>
آپ کو کرایہ داری کا آغاز کرنے میں    
<foreign lang="eng">6</foreign>
ماہ کے اندر اندر اس کے لۓ درخواست دینی چاہۓ ۔
</s></p>

<p><head><s>
کرایہ دار ہونے کی حیثیت سے میری کیا ذمہ داریاں ہیں ؟
</s></head></p>

<p><s>
طے شدہ کرایہ کی ادائیگی اور مکان کی اچھی طرح دیکھ بھال کرنا
</s> <s>
بجلی، گیس، ٹیلی فون وغیرہ کے بلوں کی ادائیگی اگر آپ نے مالک مکان کے ساتھ اس کا معاہدہ کیا ہو 
</s> <s>
زیادہ تر صورتوں میں، کونسل ٹیکس اور پانی اور سیورتج کے چارجز
</s></p>

<p><head><s>
مالک مکان کی کیا ذمہ داریاں ہیں ؟
</s></head></p>

<p><s>
مکان کے ڈھانچے اور بیرونی حصے کی مرمت، ہیٹنگ اور گرم پانی کے آلات کی فٹنگز، بیسن اور دوسری سینیٹری کی فٹنگز 
</s> <s>
گیس اور بجلی کے آلات کے لۓ حفاظتی اقدامات 
</s> <s>
کرایہ داری کے تحت مہیا کۓ جانے والے فرنیچر اور فرنیشنگ کا فائرسیفٹی کے مطابق ہونا
</s></p>

<p><head><s>
اگر میرا مالک مکان مجھے غیر قانونی طور پر گھر سے نکالنے یا ہر اساں کرنے کی کوشش کرے تو میں کیا کر سکتا ہوں ؟
</s></head></p>

<p><s>
آپ کا مالک مکان آپ کو عدالت کی طرف سے جاری کردہ حکم نامہ کے بغیر گھر سے نکال نہیں سکتا ۔
</s> <s>
نہ ہی وہ یا اس کی طرف سے کوئی اور شخص آپ کو مکان کے اس حصے سے نکال سکتا ہے جس کے آپ قانونی طور پر حقدار ہیں ۔
</s> <s>
اگر آپ کو کوئی مشکل پیش آرہی ہوں تو لوکل اتھارٹی کے ٹیننسی ریلیشنز آفیسر سے رابطہ قائم کریں ۔
</s></p>

<p><head><s>
میری کرایہ داری    
<foreign lang="eng">28</foreign>
فروری     
<foreign lang="eng">1997</foreign>
سے پہلے شروع ہوئی ہے ۔
</s> <s>
اس سلسلے میں میری کیا پوزیشن ہے ؟
</s></head></p>

<p><s>
زیادہ تر کرایہ داریاں جو فروری      
<foreign lang="eng">1997</foreign>
سے پہلے شروع ہوئی ہیں وہ یا تو ایشورڈ شارٹ ہولڈ یا ایشورڈ کرایہ داریاں ہوں گی ۔
</s> <s>
اگر آپ ایشورڈ شارٹ ہولڈ کرایہ دار ہیں تو آپ کو اس مکان میں    
<foreign lang="eng">6</foreign>
ماہ کے لۓ رہنے کا گارنٹی شدہ حق ہوتا ہے ۔
</s> <s>
آپ کا مالک مکان آپ کو زیادہ عرصے کے لۓ رہنے کے لۓ راضی ہو سکتا ہے لیکن اسے ایسا کرنے کا قانونی حق نہیں ہے ۔
</s> <s>
آپ کا مالک مکان آپ کو صرف اسی صورت میں مکان خالی کرنے کے لۓ کہہ سکتا ہے اگر آپ نے کم از کم    
<foreign lang="eng">3</foreign>
ماہ یا     
<foreign lang="eng">13</foreign>
ہفتوں تک کا کرایہ ادا نہ کیا ہو ۔
</s> <s>
اگر آپ کا مالک مکان آپ کی کرایہ داری کو دوبارہ آغاز کرے تو وہ خودبخود شارٹ ہولڈ ہو جاۓ گی ماسواۓ اگر آپ کا مالک مکان تحریری طور پر یہ نوٹس دے کہ یہ ایشورڈ کرایہ داری ہے ۔
</s> <s>
اگر آپ کا مالک مکان آپ کی ایشورڈ کرایہ داری کو دوبارہ آغاز کرے تو وہ خودبخود ایشورڈ ہو جاۓ گی ۔
</s> <s>
مالک مکان کو اس کے لۓ ایشورڈ ہونے کا نوٹس دینے کی ضرورت نہیں ہوتی ۔
</s> <s><foreign lang="eng">15</foreign>
جنوری       
<foreign lang="eng">1989</foreign>
سے پہلے شروع ہونے والی کرایہ داریوں کے لۓ مختلف قوانین ہیں ۔
</s></p>

<p><head><s>
میں مزید معلومات کہاں سے حاصل کر سکتا ہوں ؟
</s></head></p>

<p><s>
ایک زیادہ تفصیلی کتابچہ "ایشورڈ اور ایشورڈ شارٹ ہولڈٹینسی ۔ کرایہ داروں کے لۓ راہنمائی" مندرجہ ذیل پتہ سے مفت دستیاب ہے:
</s> <s>
ڈیپارٹمنٹ فار ٹرانسپورٹ، لوکل گورنمنٹ اینڈ دی ریجنز 
</s> <s><foreign lang="eng">Department for Transport, Local Government and Regions</foreign></s> <s>
پبلیکیشنز ڈسپیچ سنٹر
</s> <s><foreign lang="eng">Publications Despatch Centre</foreign></s>
<s><foreign lang="eng">PO Box 236</foreign></s>
<s><foreign lang="eng">Wetherby</foreign></s>
<s><foreign lang="eng">West Yorkshire</foreign></s>
<s><foreign lang="eng">LS23 7NB</foreign></s></p>

<p><s>
فون:
<foreign lang="eng">0870 1226236</foreign></s> <s>
فیکس:
<foreign lang="eng">0870 1226237</foreign></s></p>

<p><s>
اس کے علاوہ آپ وکیل، لاءسنٹر، سیٹزن ایڈوائز بیورو یا لوکل اتھارٹی کے ہاؤسنگ ایڈوائز سنٹر سے مشورہ لے سکتے ہیں ۔
</s></p>

<p><s>
آپ مندرجہ ذیل ادارے سے رابطہ کرکے منظور شدہ کرایہ پر دینے والے ایجنٹوں کے بارے میں مشورہ لے سکتے ہیں:
</s></p>

<p><s>
دی نیشنل اپرویڈلیٹنگ سکیم
</s> <s><foreign lang="eng">The National Approved Letting Scheme</foreign></s>
<s><foreign lang="eng">Warwick Corner</foreign></s>
<s><foreign lang="eng">42 Warwick Road</foreign></s>
<s><foreign lang="eng">Kenilworth</foreign></s>
<s><foreign lang="eng">CV8 1HE</foreign></s></p>

<p><s>
فون: 
<foreign lang="eng">01926 866633</foreign></s>
<s><foreign lang="eng">www.nalscheme.co.uk</foreign></s></p>

<p><s><foreign lang="wel">Cynulliad Cenedlaethol Cymru</foreign></s>
<s><foreign lang="eng">The National Assembly for Wales</foreign></s></p>

<p><s><foreign lang="eng">Published by the Department for Transport, Local Government and the Regions.</foreign></s> <s><foreign lang="eng">© Crown Copyright 2002.</foreign></s> <s><foreign lang="eng">Printed in the UK February 2002 on material comprising 75% post consumer waste and 25% ECF pulp.</foreign></s> <s><foreign lang="eng">Product code 97HC0246/UR Urdu</foreign></s></p>

</body>
</text>
</cesDoc>