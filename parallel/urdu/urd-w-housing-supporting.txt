<cesDoc id="urd-w-housing-supporting" lang="urd">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>urd-w-housing-supporting.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Miss Shehnaz Nawab)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-03-06</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Supporting People and Sheltered Housing</h.title>
<h.author>Department for Transport, Local Goverment and the Regions</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department for Transport, Local Goverment and the Regions</publisher>
<pubDate>June 2001</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-12-13</date>
</creation>
<langUsage>Translated into Urdu from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-housing-supporting.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s><foreign lang="eng">DTLR</foreign></s>
<s><foreign lang="eng">TRANSPORT</foreign></s>
<s><foreign lang="eng">LOCAL GOVERNMENT</foreign></s>
<s><foreign lang="eng">REGIONS</foreign></s></head></p> 

<p><head><s>
لوگوں اور رہائشی پناہگاہوں
</s> <s>
کے لۓ مددگار
</s> <s>
کچھ سوالوں کے جوابات
</s></head></p>

<p><head><s>
سوال 
</s> <s>
یہ کتابچہ کن کے لۓ ہے ؟
</s></head></p>

<p><s>
جواب
</s> <s>
رجسٹرڈ شدہ لینڈ لارڈز )ہاؤسنگ ایسوسی ایشن( اور رضاکارانہ تنظیموں کے لۓ، جنمیں کہ ایبی فیلڈ سوسائٹی اور بوڑھے افراد کو رہائش فراہم کرنے والی تنظیمیں شامل ہیں ۔
</s> <s>
مقامی لوکل اتھارٹی کا معاملہ اسکے علاوہ ہے اور ٱس کا ذکر علیحدہ سے ہوگا ۔
</s></p>

<p><head><s>
سوال
</s> <s>
یہ کتابچہ کس بارے میں ہے ؟
</s></head></p>

<p><s>
جواب 
</s> <s>
یہ کتابچہ لوگوں کو مدد فراہم کرنے سے متعلق حکومت کے نۓ پروگرام کے بارے میں ہے جو کہ رہائش سے متعلق بہت سی امدادی خدمات کا احاطہ کرتا ہے جسمیں کہ رہائشی پناہ گاہ یعنی
<foreign lang="eng">(Sheltered housing)</foreign>
بھی شامل ہے ۔
</s> <s>
محفوظ رہائش گاہوں میں امدادی خدمات جو عام طور پر وارڈن )سکیم کے مینیجرز، محفوظ پناگاہوں کے افسران( فراہم کرتے ہیں مثلاً رہائشیوں سے مسلسل رابطہ، فوری روابط کے جوابات اور الارم سسٹم کی فراہمی ۔
</s></p>

<p><s>
فی الوقت ان خدمات کے لۓ رقومی کی فراہمی مرکزی حکومت کی جانب سے کی جاتی ہے ۔
</s> <s>
اپریل سہ۲۰۰۳ سے مقامی لوکل اتھارٹی کو ان خدمات کی فراہمی کے لۓ گرانٹ دی جاۓ گی ۔
</s> <s>
ایسی امدادی خدمات کا معاوضہ مثلاً وہ خدمات جو کہ وارڈن کے ذریعے مہیا کی جاتی ہیں کی ادائیگی آئندہ سے ہاؤسنگ بینیفٹ کے ذریعے نہیں کی جائیگی بلکہ مالک مکان کو یہ رقم لوکل اتھارٹی سے گرانٹ کی صورت میں موصول ہوگی ۔
</s></p>

<p><head><s>
سوال 
</s> <s>
لوگوں کی مدد کرنے کا مقصد کیا ہے ؟
</s></head></p>

<p><s>
جواب 
</s> <s>
محفوظ پناہ گاہوں اور دیگر امدادی پناہوں کے رہائشی افراد کے معیار خدمت کو بہتر بنانا ۔
</s> <s>
مدد فراہم کرنے والے افراد منصوبہ بندی میں ربط وضبط پیدا کرتے ہوۓ منضوبہ بندی کو بہتر بنائیں گے اور ضعیف افراد کو مقامی ہاؤسنگ اور امدادی خدمات کے بارے میں دی جانے والی معلومات کے معیار کو بہتر بنائیں گے ۔
</s></p>

<p><head><s>
سوال 
</s> <s>
کیا اس سے مُراد یہ ہے کہ تبدیلیاں واقع ہونگی ؟
</s></head></p>

<p><s>
جواب 
</s> <s>
موجودہ رہائشی افراد کو تو متواتر وہی خدمات میسر رہیں گی جو کہ ٱنکے معاہدہ کرایہ داری میں درج ہیں ۔
</s> <s>
ٱنہیں رہائشی پناہگاہ کیلۓ نۓ سرے سے درخواست دینے کی ضرورت بھی نہیں ہوگی اور نہ ہی ٱن کو دی جانے والی مددگار خدمات کے معاوضہ میں کوئی اضافہ ہوگا ۔
</s> <s>
مددگار خدمات فراہم کرنے والا بھی تبدیل نہیں ہوگا ۔
</s> <s>
سہ۲۰۰۳ اور سہ۲۰۰۶ کے دوران مقامی لوکل اتھارٹی تمام محفوظ پناگاہوں کے بارے میں اس بات کو یقینی بنانے کے لۓ نۓ سرے سے جائزہ لے گی کہ مددگار خدمات معیاری اور اچھی قدرو قیمت کی حامل ہیں ۔
</s> <s>
رہائشی افراد کو شامل کرتے ہوۓ ان سکیموں کا ازسرنو جائزہ لیا جاۓ گا اور ٱن میں کسی قسم کی تبدیلی کے لۓ ٱن کی راۓ لی جاۓ گی ۔
</s> <s>
لوگوں کو مدد کے پروگرام کا مقصد اس بات کو یقینی بنانا ہے کہ محفوظ پناہ گاہوں میں رہنے والے افراد کو یہ خدمات مسلسل جاری ہیں ۔
</s></p>

<p><head><s>
سوال 
</s> <s>
محفوظ رہائش گاہوں کے نۓ مُتلاشی افراد اور ایک سکیم سے دوسری سکیم میں منتقل ہونے والے افراد کے لۓ کیا طریقہ کار ہوگا ؟
</s></head></p>

<p><s>
جواب
</s> <s>
لوگوں کی مدد کے پروگرام کے نافد عمل ہونے کے نتیجہ میں ایک سکیم کے رہائشی افراد کے کسی دوسری سکیم میں منتقلی پر کوئی پابندی نہیں لگائی جاۓ گی ۔
</s> <s>
اسمیں وہ رہائشی افراد بھی شامل ہونگے جنکا مالک مکان ٱنکے ایک رہائش گاہ سے دوسری رہائشگاہ میں مُنتقل ہونے کی وجہ سے تبدیل ہوا ہوگا ۔
</s> <s>
سہ۲۰۰۳ کے بعد محفوظ رہائشگاہوں میں رہنے، ٱنکے الاٹ کۓ جانے اور کراۓ پر دۓ جانے میں ضعیف افراد کی خواہشات کا بڑا عمل دخل ہوگا ۔
</s> <s>
تمام اتھارٹیوں کو رہائش فراہم کرنے والے افراد سے یہ طے کرنے کی ضرورت ہوگی کہ ٱنکے علاقہ میں ضعیف افراد کی رسائی محفوظ پناہگاہوں تک کیسے ہوگی ۔
</s> <s>
اس معاملہ میں تفصیلی رہنمائی فراہم کی جاۓ گی ۔
</s></p>

<p><head><s>
سوال
</s> <s>
جو تبدیلیاں یا اصلاحات مالک مکان اپریل سہ۲۰۰۳ سے پہلے متعارف کرنا چاہتے ہیں ٱن کا کیا ہوگا ؟
</s></head></p>

<p><s>
جواب
</s> <s>
رہائش پزیر افراد کی خواہشات اور ٱنکی تبدیل ہوتی ہوئی ضروریات کو پورا کرنے کی غرض سے مالک مکان اور محفوظ رہائشگاہوں کے فراہم کرنے والے اپنی جانب سے دی جانے والی خدمات کا باقاعدگی سے جائزہ لیتے رہتے ہیں ۔
</s> <s>
مثلاً وارڈن کے اوقات کار میں تبدیلی یا وارڈن کے انتظامات میں مدد وغیرہ ۔
</s> <s>
خدمات میں کسی قسم کی تبدیلی صرف ٱسی صورت میں ممکن ہے جبکہ رہائشی افراد سے پہلے صلاح مشورہ کر لیا گیا ہو ۔
</s> <s>
لوگوں کے مددگار یعنی "سپورٹنگ پیپل" سکیم کے مُتعارف کۓ جانے کے بعد بھی یہ سلسلہ بدستور جاری رہیگا ۔ 
</s></p>

<p><head><s>
سوال
</s> <s>
مددگار خدمات کا معاوضہ کیا ہوگا ؟
</s></head></p>

<p><s>
جواب
</s> <s>
لوگوں کی مدد کو متعارف کراۓ جانے کے بعد مدد کی خدمات کے معاوضہ میں کوئی اضافہ نہیں ہونا چاہۓ ۔
</s> <s>
ان خدمات کا معاوضہ کراۓ اور خدمات کی طرح ہی قابل شناخت ہوگا جیسا کہ یہ اب ہے ۔
</s> <s>
ہماری راۓ یہ ہے کہ رہائشی افراد اپنی مقامی یعنی لوکل کونسل کو ہی ان خدمات کی ادائیگی کریں بجاۓ اس کے کہ وہ یہ ادائیگی اپنے مالک مکان کو کریں جیسا کہ وہ فی الوقت کر رہے ہیں ۔
</s></p>

<p><head><s>
سوال
</s> <s>
کیا مجھے اس سلسلے میں کچھ کرنے کی ضرورت ہے ؟
</s></head></p>

<p><s>
جواب
</s> <s>
نہیں ۔
</s> <s>
آئندہ آنے والے مہینوں میں مقامی یعنی لوکل اتھارٹیاں مالک مکانات سے یہ جاننے کے لۓ رابطہ کریں گی کہ وہ اپنے کرایہ داروں کو کس قسم کی خدمات مہیا کر رہے ہیں ۔
</s> <s>
لوکل اتھارٹی کے منصوبہ بندی کے ایک حصہ کے طور پر وہ ضعیف افراد سے یہ معلوم کریں گی کہ وہ مستقبل میں کس قسم کی خدمات کا حصول چاہتے ہیں ۔
</s> <s>
اس میں محفوظ پناہ گاہوں کے رہائشی افراد سے بحث وتمحیث بھی شامل ہوگی جسکے ذریعہ سے انہیں یہ موقع مُیسر آۓ گا کہ وہ اپنے خیالات کو بیان کر سکیں ۔
</s></p>

<p><head><s>
سوال 
</s> <s>
میں اس بارے میں مزید جانکاری کیسے حاصل کر سکتا ہوں ؟
</s></head></p>

<p><s>
جواب
</s> <s>
 اسکے لۓ آپ لوگوں کی مدد یعنی "سپورٹنگ پیپل" کی ہیلپ لائن سے مندرج ذیل نمبر پر رابطہ کریں:
<foreign lang="eng">020 7944 2556</foreign></s> <s>
سپورٹنگ پیپل کی
<foreign lang="eng">DTLR</foreign>
ویب سائٹ کو مندرجہ ذیل پتہ پر دیکھیں:
</s> <s><foreign lang="eng">www.supporting-people.dtlr.gov.uk</foreign></s> <s><foreign lang="eng">www.spkweb.org.uk</foreign></s> <s>
اگر آپ کو اس بارے میں کسی قسم کے تفکرات یا تشویش ہو اور آپ وارڈن یا سکیم کے مینیجر یا ہاؤسنگ آفیسر سے بات چیت کرنا چاہتے ہوں ۔
</s></p>

<p><s>
اکتوبر سہ۲۰۰۱ میں سپورٹنگ پیپل اور محفوظ رہائشگاہوں کے بارے میں مزید رہنمائی دستیاب ہوگی ۔
</s> <s>
محفوظ رہائشگاہوں کے فراہم کرنے والے تمام افراد کی حوصلہ افزائی کی جاۓ گی کہ وہ رہائش پذیر افراد سے اس بارے میں گفتگو کریں ۔
</s> <s>
اگر آپ کو اس کتابچہ کی مزید نقول درکار ہوں تو براۓ مہربانی مندرجہ ذیل سے رابطہ کریں:
</s> <s><foreign lang="eng">Department for Transport, Local Government and the Regions, Free Literature, PO Box No 236, Wetherby LS23 7NB.</foreign></s> <s>
فون نمبر:
<foreign lang="eng">0870 1226 236,</foreign>
فیکس نمبر:
<foreign lang="eng">0870 1226 237</foreign></s> <s>
ٹیکسٹ فون:
<foreign lang="eng">0870 1207 405</foreign></s> <s> 
ای میل:
<foreign lang="eng">dtlr@twoten.press.net</foreign></s> <s>
اوپر دۓ گۓ پتہ پر ہندی، ٱردو بنگالی ترجمے اور آڈیو کیسٹ بھی دستیاب ہیں ۔
</s></p>

<p><s><foreign lang="eng">Published by the Department for Transport, Local Government and the Regions.</foreign></s> <s><foreign lang="eng">© Crown Copyright 2001.</foreign></s> <s><foreign lang="eng">Printed in the UK June 2001 on material comprising 75% post-consumer waste and 25% pre-consumer waste.</foreign></s> <s><foreign lang="eng">Product code 01HC0254U (URDU)</foreign></s></p>
 
<p><s>
لوگوں کے مددگار کا ہیلپ نمبر:
<foreign lang="eng">020 7944 2556</foreign></s>
<s><foreign lang="eng">DTLR</foreign>
کی ویب سائٹ کا پتہ
<foreign lang="eng">www.supporting-people.dtlr.gov.uk</foreign></s></p>

</body>
</text>
</cesDoc>