<cesDoc id="guj-w-health-breast" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-health-breast.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Arpita Shah)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-09-18</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Be Breast Aware</h.title>
<h.author>Department of Health</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department of Health</publisher>
<pubDate>September 1995</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-09-02</date>
</creation>
<langUsage>Translated into Gujarati from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-health-breast.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s><foreign lang="eng">BE BREAST AWARE</foreign></s></head></p>

<p><head><s>સ્તન અંગે સભાનતા કેળવો</s></head></p>

<p><head><s>એનએચએસબીએસપી કેન્સર રિસર્ચ કેમ્પેઇન</s></head></p>

<p><head><s>સ્તન અંગે સભાનતા એ શંુ છે?</s></head></p>

<p><s>સ્તન અંગે સભાનતા એ શરીર વિષેની સાધારણ સભાનતાના હિસ્સારૂપ છે.</s> <s>તે તમારા પોતાના સ્તનો અંગેની સભાનતા અને તેના બાહ્ય દેખાવથી સુપરિચિત થવા માટેની પ્રક્રિયા છે.</s> <s>જુદા જુદા સમયે તમારાં સ્તનો સ્પર્શથી કેવાં લાગે છે તેની જાણકારીથી તમારા માટે સામાન્ય શું છે તે તમને જાણવામાં સહાયતા થશે.</s></p>

<p><s>જે કોઇપણ રીત તમારા માટે ઉત્તમ હોય તે પ્રમાને તમે જોઇને અને સ્પર્શ દ્વારા તમારાં સ્તનોની પેશીઓ વિષે પરિચિત બની શકો છો (દા.ત.નહાતી વખતે, શાવરમાં, તૈયાર થતાં હો ત્યારે).</s></p>    

<p><s>સ્તન અંગે સભાન બનવાથી અને તમારા માટે સ્વસ્થ/સામાન્ય શું ચે તે જાણવાથી એ પરિસ્થિતિમાં જો કોઇ પરિવર્તનો થાય તો તે વિષે તમને સભાન બનવામાં સહાયતા થશે.</s></p>

<p><s>તમારા પોતાના સ્વાસ્થયનું ધ્યાન રાખો</s></p>

<p><head><s>સામાન્ય સ્વસ્થ સ્તન</s></head></p>

<p><s>મેનોપોઝ (ઋતુનિવૃત્તિકાળ) પહેલાં સામાન્ય સ્તનો મહિનામાં જુદા જુદા સમયે સ્પર્શ કરવાથી જુદાં જુદાં લાગે છે.</s> <s>અટકાવની શરૂઆત થતાં પહેલાંના દિવસોમાં સ્તનોની અંદર દૂધ બનાવતી પેશીઓ સક્રિય બને છે.</s> <s>કેટલીક સ્ત્રીઓમાં આ સમયે સ્તનો આળાં, અને ગઠેદાર લાગે છે, ખાસ કરીને બગલની નજીક.</s></p>

<p><s>હિસ્ટરેકટમી બાદ તમારા માસિક અટકાવો બંધ થઇ ગયા હોવા જોઇએ ત્યાં સુધીના ગાળામાં સ્તનો સમાન્ય રીતે એક જ પ્રકારના માસિક ફેરફારો દર્શાવતાં રહે છે.</s></p>

<p><s>મેનોપોઝ બાદ દૂધ બનાવતી પેશીઓની સક્રિયાતા બંધ થાય છે.</s> <s>સામાન્ય સ્તનો પોચાં, ઓછા કઠણ અને ગાંઠા વગરનાં લાગે છે.</s></p>

<p><s>તમારા માટે સામાન્ય/સ્વસ્થ શું છે તે જાણો</s></p>

<p><head><s>જે પરિવર્તનો માટે જોતાં રહેવું જરૂરી છે</s></head></p>  

<p><s>બાહ્ય દેખાવઃ સ્તનના આકારમાં અથવા તેની રૂપરેખામાં કોઇપણ ફેરફારો, ખાસ કરીને જે ફેરફારો હાથનાં હલનચલનના કારણે, અથવા સ્તનોને ઊંચા કરવાથી થાય છે.</s> <s>ચામડીની અંદર ખંજનો, કરચલીઓ કે વાટા પડવાં.</s></p>

<p><s>સ્પર્શથી થતા અનુભવોઃ એક સ્તનની અંદર અસુવિધા અથવા અસાધારણ દુઃખાવો, ખાસ કરીને જો તે નવો અને સતત હોય.</s></p>

<p><s>દડબો (ગાંઠ)ઃ એક સ્તનની અંદર અથવા એક બગલની અંદર કોઇપણ પ્રકારની દડબો, જાડું થવું, અથવા ઉપસેલા ભાગો, જે બીજા સ્તન અથવા બીંજી બગલના તે જ ભાગોથી સ્પર્શ કરતાં જુદા લાગતા હોય.</s> <s>જો આ નવું જ હોય તો અત્યંત અગત્યનું છે.</s></p> 

<p><s>ડિટંડીમા આવતાં પરિવર્તનોઃ દૂધ જેવો ન હોય અને તમારા માટે નવો હોય તેવો ડિટંડીમાંથી થતો સ્ત્રાવ.</s> <s>લોહી નીકળવું અથવા ભીનાશ પડતા લાલ ભાગો કે જે સરળતાથી મટતા ન હોય.</s> <s>ડિટંડીની સ્થિતિમાં થતા ફેરફારો - અંદર જતી રહી હોય અથવા જુદી જ દિશામાં રહેતી હોય.</s> <s>ડિટંડીની ઉપર અથવા તેની આજુબાજુમાં ફોલ્લીઓ થવી.</s></p>

<p><s>શું જોવું અને સ્પર્શથી અનુભવવું તે જાણો</s></p>

<p><head><s>જો તમને કોઇ એક ફેરફાર જણાય તો શું કરવું?</s></head></p>

<p><s>સ્તનમાં ફેરફારો થવા માટે ઘણાં કારણો હોઇ શકે.</s> <s>તેમાંનાં ઘણાં ખરાં નિર્દોષ હોય છે, પણ તે બધાંની તપાસ કરાવવી જરૂરી છે કારણ કે તે કદાચ કેન્સરની પ્રથમ નિશાની હોવાની પણ અલ્પ શકયતા હોઇ શકે.</s></p>

<p><s>તમારા માટે જે સ્વસ્થ/સામાન્ય સ્થિતિ હોય તેનાથી તમારા સ્તનમાં તમને કંઇપણ ફેરફાર થયેલો જણાય, તો વિલંબ કર્યા વગર તમારા ડોકટરને જણાવો.</s> <s>યાદ રાખો તમે કોઇને પણ સમય વેડફતાં નથી.</s> <s>જો કેન્સર હશે તો જેટલી જલ્દીથી તેની જાણ કરવામાં આવશે તેટલી જ વધુ સરળ સારવાર થવાની સંભાવના રહેશે.</s> <s>આ જીવનની ગુણવત્તાની દ્રષ્ટિએ વધુ સારું ભવિષ્ય પ્રદાન કરે છે.</s></p>

<p><s><foreign lang="eng">40</foreign> વર્ષની વયથી હેઠળની સ્ત્રીઓમાં સ્તનનું કેન્સર ભાગ્યે જ થાય છે.</s> <s>ઉ ંમર વધવાની સાથે સાથે સ્તનના કેન્સર થવાની સંભાવના વધે છે.</s></p>

<p><s>કોઇપણ ફેરફારોની મોડું કર્યા વગર જાણ કરો</s></p>

<p><head><s>સ્તન પરીક્શણ</s></head></p>

<p><s>જો તમે <foreign lang="eng">50</foreign> વર્ષનાં અથવા તેથી વધુ વયનાં હો તો તમને નેશનલ હેલ્થ સર્વિસના સ્તન પરીક્શણ કાર્યક્રમનો ફાયદો ઉઠાવવા માટે ભારપૂર્વક ભલામણ કરવામાં આવે છે.</s> <s>જે ત્રિવર્ષિય મેમોગ્રાફી પ્રદાન કરે છે.</s> <s>આ એક એકમ રે પધ્દતિ છે જે તદન પ્રાથમિક તબકકામાં જ સ્તનોમાંના ફેરફારોને પકડી પાડે છે.</s> <s>બ્રેસ્ટ સ્ક્રિનીંગ પ્રોગ્રામ વિષે વધુ માહિતી મેળવવા મહેરબાની કરી તમારા ડોકટરને પૂછો.</s></p>

<p><s><foreign lang="eng">50</foreign> વર્ષની વયથી હેઠળથી મહિલાઓ માટે રાબેતા મુજબનું સ્તનોનું પરીક્શણ ઉપલબ્ધ નથી, કારણ કે તેનાથી ફાયદો થવાનું જણાયું નથી.</s> <s>જો તમરાં સ્તનો બારામાં તમને ચિંતા કરવા યોગ્ય કોઇપણ કારણ હોય તો મહેરબાની કરી તમારા ડોકટરને કહો.</s></p>

<p><head><s>સ્તન અંગેની સભાનતા પાંચ મુદાનો નિયમ</s></head></p>

<p><s>તમારા માટે સ્વસ્થ સામાન્ય સ્થિતિ શું છે તે જાણો</s>
<s>જુઓ અને સ્પર્શ કરો</s>
<s>કયા ફેરફારો માટે જોતાં રહેવું તે જાણો</s>
<s>વિલંબ કર્યા વગર કોઇપણ ફેરફારોની જાણ કરો</s>
<s>જો <foreign lang="eng">50</foreign> વર્ષ કે તેથી વધુ વયનાં હો તો સ્તન પરીક્શણો માટે હાજરી આપો.</s></p>

<p><s><foreign lang="eng">©Crown Copyright Produced by Deparment of Health G80/005 3144 3RP 10K Sept. 95 </foreign></s></p>

</body>
</text>
</cesDoc>