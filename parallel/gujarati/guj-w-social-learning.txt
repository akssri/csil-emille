<cesDoc id="guj-w-social-learning" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-social-learning.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>translated and typed by</respType>
<respName>Winfocus Pvt Ltd (Arpita Shah)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-12-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Services for people with learning disabilities</h.title>
<h.author>Manchester City Council</h.author>
<imprint>
<pubPlace>Manchester</pubPlace>
<publisher>Manchester City Council</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-12-02</date>
</creation>
<langUsage>Translated into Gujarati from Hindi translation of original English (translation made on behalf of the University of Lancaster for the EMILLE Project)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-social-learning.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>
<p><head><s>સિખવા મા અપાહિજ લોગો માટે સેવાઓ</s></head></p>

<p><head><s>માન્ચૈસ્ટર કમ્યૂનિટી લોગો માટે સેવાઓ</s></head></p>

<p><head><s>સિખવા મા અપાહિજ લોગો માટે સેવાઓ</s></head></p>

<p><s>સિટિ કાંઉસિલ હૈલ્થ સર્વિસિ઼જ અને સ્વયંસેવક અને પ્રાઈવેટ સસ્થાઓ, સિખવા મા અપાહિજ લોગો ની વિશેષ આવશ્કતાઓ ની પૂર્તી માટે સેવાઓ આપે છે.</s> <s>સિખવા માટે અપાહિજ વ્યકતિઓ ને સ્થાનીય સમુદાય ક્રિયાકલાપો મા ભાગ લેવા માટે પ્રત્યેક અવસર મળવા જોઇએ.</s></p>

<p><head><s>મદદ કેવીતે પ્રાપ્ત કરવી</s></head></p>

<p><s>કદાચ તમે સ્વયં યા તમારે કોઇ જાન ઓરખાન વ્યકિત ને સિખવા મા કોઈ અપાહિજતા છે, અને એમને મદદ ની આવશ્યકતા છે, તો તમે તમારી સ્થાનીય સોશલ સર્વિસિ઼જ ને વિભાગ મા જાઓ (એમના પતા આ પત્રિકા ને અન્ત મા આપેલા છે).</s> <s>એ તમને સલાહ-પરામર્શ અને જાનકારી આપીને યા તમારે માટે અસેસમેટ (આવશ્યકતાઓ ના મૂલ્યાકન) ના પ્રબન્ધ કરીને તમારી સહાયતા કરવાનુ પ્રયત્ન કરશે, (એના માટે કૃપયા સોશલ સર્વિસિ઼જ થી સહાયતા કેવીતે પ્રપ્ત કરો <foreign lang="eng">How to get help from your services deparment</foreign > નામની પત્રિકા જોવો.</s> <s>કદાચ તમારે વિચાર થી તમારે કેવલ હૈલ્થ યા સ્વયસેવી સંસ્થાઓ ની આવશ્યકતા છે, તો તમે આ સેવાઓ ને જોડે સ્વયં સીધો સમ્પર્ક સાધો.</s> <s>કદાચ તમે અસેસમેન્ટ માટે યોગ્ય હોય તો, તમને તમારી આવશ્યકતાઓ ને વિષે મા વિભિન્ન પ્રશ્ન પૂછશે.</s> <s>સમ્ભવત઼ તમારે માટે એક દેખભાળ ની યોજના ના પ્રસ્તાવ મુકાય, જેમા નિમ્નલિખિત સેવાઓ મા થી કોઈ પણ સમ્મલિત કરી જઇ સકે છે.</s></p>

<p><head><s>સિટિ કાઉસિલ અને હૈલ્થ ની સેવાઓ</s></head></p>   

<p><head><s>આવાસ અને આશ્રયસ્થાન</s></head></p>

<p><s>માનચૈસચ્ટર સોશલ સર્વિસિ઼જ વિભન્નિ પ્રકાર ની રિહાઇસી સેવાઓ માટે પૈસા આપે છે.</s> <s>એવી ગ્યારહ નેટવર્ક હાઉજિગ યોજનાઓ છે, જયાં સુધી કર્મચારિયો થી યુકત આ ઘરો મા ૨૦૦ થી વધાર લોગો માટે પ્રબન્ધ હોઇ સકે છે.</s> <s>વયસ્કો માટે ઘરે ગોતવા ની યોજના, જે એમને વ્યકિતગત લોગો ની સહાયતા કરે છે, જે પરિવાર ને જોડે રહવા માગે છે.</s> <s>આ યોજનાઓ ના કોઇ આપતિકાલ મા યા દેખભાલકર્તા ને વિશ્રામ આપવા માટે, અપાહિજ વ્યકિતયો ને અલ્પકાલિન વિશ્રામ ની વિશેષ વ્યવસ્થા કરવા આવે છે.</s> <s>સાઈથ માનચૈસ્ટર ના એક ઘર મા ઉત્તેજિત વ્યવહાર વારા સર્વિસિ઼જ ને સેવા આપે છે.</s> <s>હોમ સ્કીમ ની તરફ થી બાળકો ને અન્ય પરિવાર ને જોડે એમના ઘર મા કઇ દિવસો માટે મોકળે છે.</s></p>

<p><head><s>સપોર્ટ સર્વિસિજ઼ (સહાયતા આપવાની સેવાઓ)</s></head></p>

<p><s>નાર્થ અને સેન્ટ્રલ માનચૈસ્ટર ને જોડે સોશલ વર્કર, મેનેજર, ઘર મા દેખભાળ કરવા વારા વિશેષગ્ય અને પ્રબન્ધ કર્મચારિયો ની ટીમ છે.</s> <s>પ્રત્યેક ડિસટ્રિકણ મા કમ્યૂનિટિ સપોર્ટ ટીમ છે, જેમા મેનેજર, નર્સે, આકયુપેશનલ થેરપિસ્ટ, ફિજિઓથેરપિસ્ટ, સ્પીચ વ વારા ને થેરપિસ્ટ (બોલવા મા મદદ કરવા વારા ચિકિત્સક), મનોવિગ્યાંનિક વ પ્રબન્ધ કરવા વારા કર્મચારી કામ કરે છે.</s> <s>યધ્યપિ અલગ-અલગ સ્થાનો મા અધારિત હોવા ના પછી, હૈલ્થ અને સોશ્લ સર્વિસિ઼જ ને કર્મચારી એક બીજા ને જોડે મળીને કામ કરી કરે છે.</s></p> 

<p><s>સમ્પર્ક ઃ</s></p>

<p><s><foreign lang="eng">Central Manchester</foreign ></s> <s><foreign lang="eng">Community Support Support Team</foreign ></s> <s><foreign lang="eng">Ross Place</foreign ></s> <s><foreign lang="eng">Aked Close</foreign ></s> <s><foreign lang="eng">Manchester M12 4AN</foreign ></s> <s><foreign lang="eng">273 5412</foreign ></s></p>

<p><s><foreign lang="eng">North Manchester</foreign ></s> <s><foreign lang="eng">Community Support Team</foreign ></s> <s><foreign lang="eng">Ardwick Resource Centre</foreign ></s> <s><foreign lang="eng">Beech Mount, Hapurhey</foreign ></s> <s><foreign lang="eng">Mancheste M9 1XU</foreign ></s> <s><foreign lang="eng">205 1364/4926</foreign ></s></p>

<p><s>સાઉથ માનચૈસ્ટર મા એક જ આફિસ થી હૈલ્થ અને સોશલ સવર્સિજ઼ ને કર્મચારી સંયુકત રુપ થી સેવા આપે છે.</s></p>  

<p><s>આ સેવાઓ ને વિષે વિસ્તૃત જાનકારી માટે પત્રિકા ઉપલબ્ધ છે.</s> <s>એના માટે કૃપયા વિધિનશૉવ ડિસટ્રિકટ આફિસ થી સમ્પર્ક કરો) એમના પતા વ ફોન નમ્બર આ પત્રિકા ને અન્ત મા આપેલા છે).</s></p>

<p><head><s>ડે (સર્વિસિજ઼ દિવસ ની સેવાઓ)</s></head></p>

<p><s>આ શહર મા પાંચ ડે સેન્ટર છે, જે કિ વિભિન્ન પ્રકાર ની સેવાઓ આપે છે.</s> <s>આપસ મા મળવા જુલવા વ મનોરજંનાત્મક ક્રિયાકલાપો માટે હારપ્રેહ અને ફૈલોફિલ્ડ/રુશહોલ્મ મા દિવસ ની સેવાઓ ઉપલબ્ધ છે.</s></p>

<p><s>એજૂકેશન ડિપાર્ટમેન્ટ ને જોડે સિખવા મા અત્યન્ત અપાહિજ વ્યકિતઓ માટે છો સ્કૂલ છે</s> <s>એજૂયકેસન વિભાગ, સિખવા મા અપાહિજ વ્યકિતઓ માટે રોજ઼ગાર ની યોજના ના સંચાલન કરે છે.</s></p>

<p><head><s>સ્વયંસેવી સંસ્થાએ</s></head></p>

<p><s>સ્વયંસેવી સંસ્થાએ સિખવા મા અપાહિજ વ્યકિતયઓ માટે વિભન્ન પ્રકાર ની સેવાઓ આપે છે જેમા હાઉસિ઼ગ મા સહયોગ દિવસ મા મનોરંજન ના ક્રિયાકલાપ સેલ્ફ એડવોકસી ગ્રુપસ અને માં-બાપ માટે સપોર્ટ ગ્રુપ સમ્મલિત છે.</s></p>

<p><s>આમા થી કોઇ પણ સેવા ના વિષે મા વધાર જાનકારી પ્રાપ્ત કરવા માટે કૃપયા તમારે સ્થાનીય સોશલ સર્વિસિજ઼ આફિસ થી સમ્પર્ક કરો.</s> <s>આ પતા વ ફોન નમ્બર પાછળ આપવા મા આવયા છે.</s></p>

</body>
</text>
</cesDoc>