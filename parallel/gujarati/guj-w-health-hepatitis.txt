<cesDoc id="guj-w-health-hepatitis" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-health-hepatitis.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Arpita Shah)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-09-18</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Hepatitis B</h.title>
<h.author>Department of Health</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department of Health</publisher>
<pubDate>March 2000</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-09-02</date>
</creation>
<langUsage>Translated into Gujarati from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-health-hepatitis.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>
<p><head><s>હીપેટાઇટિસ બી</s>
<s>તમારા બાળકનું રક્શણ કેવી રીતે કરશો</s></head></p>

<p><head><s><foreign lang="eng">NHS</foreign></s></head></p>

<p><head><s>હીપેટાઇટિસ બી</s>
<s>તમારા બાળકનું રક્શણ કેવી રીતે કરશો</s></head></p> 

<p><s>હીપેટાઇટિસ બી નો ચેપ જન્મ દરમ્યાન માતામાંથી બાળકને લાગી શકે છે.</s> <s>મોટા ભાગની સ્ત્રીઓને ખબર નહિ હોય કે તેમને ચેપ લાગેલો છે.</s> <s>એટલા માટે હવેથી તમામ ગર્ભવતી સ્ત્રીઓને હીપેટાઇટિસ બી છે કે નહિ તે જોવા માટે તેમની સગર્ભાવસ્થાના શરૂઆતના સમયમાં બ્લડ ટેસ્ટ (લોહીનું પરીક્શણ) કરાવવામાં આવે છે.</s></p>

<p><head><s>હીપેટાઇટિસ બી શું છે?</s></head></p>

<p><s>હીપેટાઇટિસ બી એક વાઇરસ છે જે લિવર (પિત્તાશય)ને ચેપ લગાડે છે.</s> <s>હીપેટાઇટિસ બી નો ચેપ લાગેલા ઘણાં લોકોને કોણ પઇ ચિન્હો હોતાં નથી અને તેમને ખબર હોતી નથી કે તેમને ચેપ લાગેલો છે.</s> <s>બીજા કેટલાકને 'ફલૂ જેવાં' ચિન્હો દેખાય છે અને ચામડી તેમજ આંખો પીળી પડી જાય છે (જોન્ડિસ-કમળો).</s> <s>મોટા ભાગના કિસ્સાઓમાં, હીપેટાઇટિસ બી નો ચેપ લાગેલો છે કે નહિ તે માત્ર બ્લડ ટેસ્ટ દ્વારા જ ખબર પડી શકે છે.</s></p>  

<p><s>હીપેટાઇટિસ બીના વાઇરસની અસર પામેલાં મોટા ભાગનાં પુખ્ત લોકો આ રોગમાંથી સંપૂર્ણ સાજા થઇ જાય છે, પરંતુ <foreign lang="eng">10</foreign> માંથી <foreign lang="eng">1</foreign> પુખ્ત વ્યકિત આ વાઇરસને ફેલાવનાર બને છે અને બીજામાં ચેપ અને બીજામાં ચેપ ફેલાવી શકે છે.</s> <s>આ ચેપ ફેલાવનારા <foreign lang="eng">5</foreign> માંથી <foreign lang="eng">1</foreign> પુખ્ત વ્યકિત જીવનમાં પાછલા સમયે લિવરની ગંભીર બીમારીનો ભોગ બને છે.</s> <s>હીપેટાઇટિસ બી, હીપેટાઇટિસ એ કરતાં જુદો છે, જે જોન્ડિસનું વધુ સામાન્ય કારણ છે, પરંતુ તેનાથી આખી જિંદગીનો ચેપ લાગતો નથી અને તે ભાગ્યે જ લિવરની જંભીર બીમારીનું કારણ બને છે.</s></p> 

<p><head><s>હીપેટાઇટિસ બીનો ચેપ તમને કેવી રીતે લાગી શકે?</s></head></p>

<p><s>હીપેટાઇટિસ બીનાં વાઇરસ લોહીમાં અને શરીરના પ્રવાહીઓમાં વહેતા હોય છે.</s> <s>ચેપ લાગવાની ત્રણ મુખ્ય રીતો છેઃ</s>
<s>માતાને ચેપ લાગ્યો હોય તો જન્મ દરમ્યાન બાળકને તે લાગે છે;</s>
<s>ચેપ લાગ્યો હોય તેવી વ્યકિત સાથે નિરોધ (કોન્ડોમ) વગર જાતિય સંભોગ કરવાથી;</s>
<s>ચેપ લાગ્યો હોય તેવી વ્યકિત લોહી સાથે સીધા સંપર્કથી, દા.ત. એક જ ટૂથ બ્રશ કે રેઝર વહેંચાની વાપરવથી, ટાટૂ (છૂદણાં) બનાવવામાં અને શરીર પર કાણંા કરવામાં વપરાયેલા એક જ સાધનથી, અને ડ્રગ્સ વાપરનારાં જે લોકો સોયો અને બીજાં સાધનો વહેંચીને વાપરતાં હોય તેનાથી.</s></p>

<p><s>રોજ બ રોજના સાધારણ સંપર્કોથી વાઇરસ ફેલાતાં નથી, દા.ત. ઉધરસ ખાવાથી, છીંક ખાવાથી, ચુંબનથી, ભેટવાથી, હાથ પકડવાથી, એક જ બાથરૂમ, ટોઇલેટ વાપરવાથી કે ખોરાક વહેંચીને ખાવાથી, કપ, ચમચા-ચમચી, વાસણો અને ટાવેલ્સ વહેંચીને વાપરવાથી.</s></p>

<p><head><s>હીપેટાઇટિસ બીનો ચેપ બાળકો માટે ગંભીર શા માટે છે?</s></head></p>

<p><s>રસી આપવામાં ન આવે તો, હીપેટાઇટિસ બીનો ચેપ લાગેલી માતાનાં જન્મેલા ઘણાં બાળકોને ચેપ લાગશે.</s> <s>જન્મ સમયે ચેપ લાગેલાં <foreign lang="eng">10</foreign> માંથી <foreign lang="eng">9</foreign> જેટલાં બાળકોને લાંબા સમયની ચેપ લાગે છે અને આ બાળકો જેમ મોટાં થાય તેમ તેમને ગંભીર પ્રકારની લિવરની બીમારી થવાનું જોખમ ઊભું થાય છે.</s> <s>જો તેમને ચેપ લાગ્યો હોય, તો તેમનો ચેપ તેમનાં નજીકનાં સગાં અને ભવિષ્યમાં તેમના સંપર્કમાં આવનારાં બીજા લોકોને લાગી શકે છે.</s></p>

<p><head><s>હીપેટાઇટિસ બીના ચેપથી મારા બાળકનું રક્શણ કરી શકાય છે?</s></head></p>

<p><s>હા.</s> <s>તમારા બાળકને હીપેટાઇટિસ બીની રસીનો પૂરો કોર્સ આપવાથી તેનું ચેપ સામે રક્શણ કરી શકાય છે.</s></p> 

<p><head><s>મારા બાળકને હીપેટાઇટિસ બીની રસી કયારે આપવી જોઇએ?</s></head></p>

<p><s>તમારા બાળકને રસીનો પહેલો ડોઝ જન્મના ટંૂક સમય બાદ આપવો જોઇએ.</s> <s>તમારા ડોકટર અથવા મિડવાઇફ આની ગોઠવણ કરશે.</s> <s>તમારા બાળકને ચેપ સામે સંપુર્ણ રક્શણ માટે રસીના બીજા ડોઝની જરૂર પડશે.</s> <s>તમારું બાળક જયારે <foreign lang="eng">1, 2</foreign> મહિના અને <foreign lang="eng">12</foreign> મહિનાનું થાય ત્યારે તમારા ફેમિલિ ડોકટર અથવા બાળકોના ડોકટરે આ આપવું જોઇએ.</s></p>

<p><s>તમે હોસ્પિટલ છોડો તે પહેલાં તમને કહેવામાં આવવું જોઇએ કે આ ઇન્જેકશનો કયાંથી અને કયારે લેવાના છે અને તમારે ખાતરી કરવી જોઇએ કે તમને આની જાણ હોય.</s></p>

<p><head><s>રસીનો પૂરો કોર્સ આપવાનું શા માટે અગત્યનું છે?</s></head></p>

<p><s>રસી કામ કરે તે માટે તમારા બાળકને યોગ્ય ઉંમરે તેનો પૂરો કોર્સ આપવામાં આવે તે અગત્યનું છે.</s></p>

<p><head><s>હીપેટાઇટિસ બીની રસી સલામત છે?</s></head></p>

<p><s>રસી અત્યંત સલામત છે અને કોઇ પણ ગંભીર આડ-અસરો વિના સમગ્ર વિશ્વમાં લાખો બાળકોને તે આપવામાં આવી છે.</s></p>

<p><head><s>તેની કોઇ આડ-અસરો છે?</s></head></p>

<p><s>કેટલાક બાળકોમાં, જયાં ઇન્જેકશન આપવામાં આવ્યું હોય તે ભાગ લાલ થઇ જાય છે અને સૂજી જાય તેવું બની શકે, પરંતુ આ લાંબો સમય રહેતું નથી.</s></p>

<p><head><s>મારા બાળકને સ્તનપાન કરાવવાનું સુરક્શિત રહેશે?</s></head></p>

<p><s>હા - પરંતુ તમારા બાળકને રસીનો પૂરો કોર્સ તો આપવો જ જોઇએ.</s></p>

<p><head><s>હીપેટાઇટિસ બીનો ચેપ લાગવાથી મારી સગર્ભવસ્થા (પ્રેગ્નન્સી) અને પ્રસૂતિ (ડિલિવરી) ઉપર અસર પડશે?</s></head></p>

<p><s>ના.</s></p>

<p><head><s>મારા પાર્ટનર (જીવનસાથી) અને અન્ય બાળકોને હીપેટાઇટિસ બી થઇ શકે?</s></head></p>

<p><s>એન્ટિ નેટલ કિલનિક તમારા ફેમિલિ ડોકટરને જણાવશે કે તમને હીપેટાઇટિસ બી થયો છે, જેથી તેઓ તે વિશે તમારી અને તમારા જીવનસાથી સાથે ચર્ચા કરીને હીપેટાઇટિસ બીનું પરીક્શણ અને/અથવા જો જરૂરી હોય તો રસી આપવાની ગોઠવણ કરી શકે.</s></p>

<p><s>જો તમને ચિંતા હોય કે તમને હીપેટાઇટિસ બી થયો હોવાની બીજા લોકોને ખબર પડશે, તો તે વિશે તમારા મિડવાઇફ કે ડોકટર સાથે વાત કરો.</s></p>

<p><head><s>મારા હીપેટાઇટિસ બીના ચેપને કારણે મારે ડોકટરને મળવાની જરૂર છે?</s></head></p> 

<p><s>એન્ટિ નેટલ કિલનિક અથવા ફેમિલિ ડોકટરે તમને એસેસમેન્ટ (મૂલ્યાંકન) માટે નિષ્ણાત પાસે મોકલી આપવા જોઇએ અને તમારા પોતાના સારવાર કે સંભાળ વિશે સલાહ આપવી જોઇએ.તમારું બાળક જન્મી જાય ત્યાં સુધી આની જરૂર પડશે નહિ.</s></p>

<p><s>જો તમને બીજા કોઇ પ્રશ્નો કે ચિંતાઓ હોય તો તમારી મિડવાઇફ અથવા ડોકટર સાથે વાત કરો.</s></p>

<p><s>તમને હીપેટાઇટિસ બી વિશે વધુ માહિતી અહીંથી મળી શકેઃ</s></p>

<p><s>ચિલ્ડ્રન્સ લિવર ડિસીઝ ફાઉન્ડેશન</s>
<s><foreign lang="eng">Children's Liver Disease Foundation</foreign></s> 
<s><foreign lang="eng">36 Great Charles Street</foreign></s>
<s><foreign lang="eng">Birmingham B3 3JY</foreign></s>
<s>ફોનઃ <foreign lang="eng"> 0121-212-3839</foreign></s>        
<s>ફેકસઃ <foreign lang="eng">0121-212-4300</foreign></s>
<s>ઇ-મેઇલઃ <foreign lang="eng">info@childliverdisease.org</foreign></s> 
<s>વેબસાઇટઃ <foreign lang="eng">www.childliverdisease.org</foreign></s>
<s>(લિવરની બીમારી થયેલાં બાળકોનાં નિષ્ણાત)</s></p>

<p><s>ધ બ્રિટિશ લિવર ટ્રસ્ટ</s>
<s><foreign lang="eng">The British Liver Trust</foreign></s>
<s><foreign lang="eng">Ransomes Europark</foreign></s> 
<s><foreign lang="eng">Ipswich IP3 9QG</foreign></s>
<s>ફોનઃ <foreign lang="eng"> 0808-800-1000</foreign></s>
<s>(માહિતી આપતી ફોન લાઇન)</s>
<s><foreign lang="eng">01473-276326</foreign></s>        
<s>ફેકસઃ <foreign lang="eng">01473-276327</foreign></s>
<s>ઇ-મેઇલઃ <foreign lang="eng">info@britishlivertrust.org.uk</foreign></s> 
<s>વેબસાઇટઃ <foreign lang="eng">www.britishlivertrust.org.uk</foreign></s>
<s>(લિવરની બીમારી થયેલાં પુખ્ત લોકોનાં નિષ્ણાત)</s></p>

<p><s>આ પત્રિકા હાલમાં આ ભાષાઓમાં મળી શકે છેઃ અંગેજી; બંગાળી; કેન્ટનીઝ; હિન્દી; પંજાબી; ઉર્દૂ અને વિએટનામીઝ.</s> <s>ટૂંક સમયમાં જ તે અરેબિક; ફ્રેન્ચ; ગ્રીક; પોર્ટુગીઝ; સ્વાહિલી અને ટર્કિશ ભાષામાં પણ મળી શકશે.</s></p>

<p><s>તે મંગાવવા માટેઃ</s>
<s>તમારી વિનંતી પ્રોલોગને <foreign lang="eng">01623 724 524</foreign> ઉપર ફેકસ કરો અથવા <foreign lang="eng">doh.@prologistics.co.uk</foreign>  ઉપર ઇ-મેઇલ કરો.</s></p> 

<p><s><foreign lang="eng">© Crown Copyright</foreign></s>
<s><foreign lang="eng">Produced by the Department of Health</foreign></s>
<s><foreign lang="eng">LA4/022 21073 PH 1P10k Mar 00 (CTP)</foreign></s>
<s><foreign lang="eng">CHLORINE FREE PAPER</foreign></s></p>

</body>
</text>
</cesDoc>