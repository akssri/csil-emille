<cesDoc id="guj-w-housing-manage" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-housing-manage.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>translated and typed by</respType>
<respName>Winfocus Pvt Ltd (Arpita Shah)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-03-14</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>A better deal for tenants: Your new right to manage</h.title>
<h.author>Department of the Environment, Transport and the Regions</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department of the Environment, Transport and the Regions</publisher>
<pubDate>April 2000</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-12-20</date>
</creation>
<langUsage>Translated into Gujaratifrom original English (translation made on behalf of the University of Lancaster for the EMILLE Project)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-housing-manage.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><s><foreign lang="eng">(DETR Environment Transport Region</foreign></s></p>

<p><head><s>કિરાએદાર માટે વધારે સારી સુવિધા</s></head></p>

<p><head><s>તમારા નવા અધિકાર ના પ્રબંધ</s></head></p>

<p><head><s>તમારા અધિકાર ના પ્રબંધ</s></head></p>

<p><s>સિટિજન ચાર્ટર સ્કિમ ના અન્તર્ગત, <foreign lang="eng">1</foreign> અપ્રેલ <foreign lang="eng">2000</foreign> થી અનુસાર કિરાએદાર સગણો ને આપળા નિવાસ ના પ્રબંધ કરવાના અધિકાર છે આ પ્રબંધ ના અધિકાર કહેવા મા આવે છે.</s> <s>આ અધિકાર કેવળ કાઉસિલ - મા લાગુ થાય છે પઠ્ઠાધારી આમા શામિલ છે.</s></p>

<p><s>સામાન્યતઃ કાઇસિલ સ્ટેટસ લોકલ ઑથારિટી પ્રબંધ કરે છે લોકલ ઑથારિટી દ્વારા.</s> <s>એમનુ તાત્પર્ય આ છે કિ ઑથારિટી જિમ્મેદાર છે એમની આપેલી સેવાઓ માટે.</s></p>

<p><s>ભાળા એકત્રિત એવુ સેવા મુલ્ય;</s>
<s>સંસ્થા રિર્પેર અને રખરખાવ;</s>
<s>અને બનાવુ અને ઈમારતો ને સાફ સુન્દર રાખવુ.</s></p>

<p><s>અધિકાર ના પ્રબંધ કિરાએદાર ને અનુમતી આપે છે સર્વિસ ચલાવા માટે કાઇસિલ ની જગ્યા.</s> <s>કદાચ તમારી જોડે કિરાએદાર ની સંસ્થા નથી તમે કયા રહ્યશો, તમે બીજા કિરાએદાર મળી જુડી રહ્યશો તો તમારી એક સંસ્થા બનશે.</s> <s>તમારા કાઇસિલ ના ઘરેલુ ડિપાર્ટમેન્ટ આના વિષે મા સલાહ આપશે.</s></p>
L
<p><head><s>તમે તમારા અધિકાર ના પ્રબંધ કેવિતે કરશો?</s></head></p>

<p><s>તમારા કિરાએદાર સંસ્થા જોડે અધિકાર ના પ્રબંધ કરી સકે છે - કદાચ આ બતાવો, બધા સદસ્ય વિચાર બતાવે છે અને જેવુ ચાહો છો એવુ કરછે.</s> <s>બધા કિરાએદાર સંસ્થા માટે એક સવિધાન લાગુ કેવિતે કરવુ બતાવે છે:</s>
<s>અકસર સંસ્થા ના સદસ્ય ને મળતુ રહવુ જોઇએ,</s>
<s>સંસ્થા કે સદસ્ય ને ચુનવુ જોઇએ,</s>
<s>સંસ્થા ને નિર્ણય લેવુ જોઇએ.</s></p>

<p><s>કદાચ તમારા કિરાએદાર સંસ્થા ચાહે છે પ્રંબધ ના અધિકાર જોડો રાખવા માગે છે, આ જરુરી છે કાઇસિલ ને લિખિત મા કહો.</s> <s>આ પણ જરુરી છે કાઇસિલ ને કહો જે ઘર ના પ્રબંધ કરવા માગે છે.</s></p>

<p><s>જયારે તમે પ્રબંધ ના અધિકાર લો, તમે મફત સેવા મળી સકે અને સલાહ મળશે પ્રોફેશ્નલ એડવાઇસ એજસી.</s> <s>આ એજશી કામ કરશે તમારા માટે સમય ના છ મહીના સુધી.</s> <s>યે બતાવશે તમે કેવિતે વધાર થી વધાર ભાગ લઇ સકો તમારા ઘર ના પ્રબંધ મા શામિલ થવાને.</s> <s>જયારે તમને વધારે જાનકારી ની જરુરત હોય, તમે અને બીજા કિરાએદાર સંસ્થા સદસ્ય મત નો નિર્ણય લો કદાચ તમે ચાહો તો તમારા ઘર ના અધિકાર લઇ લો.</s></p>

<p><s>કદાચ તમારો મત હા છે, તમને મળશે મફત ટ્રેનિગ કેવિતે બરાબર સેવા પ્રબંધ કરશો.</s> <s>આ ટ્રેનિગ હોઇ સકે આખિર ના બે વર્ષ, આ સેવા ની સંખ્યા મા નિર્ભર કરે છે તમે ચાહો તો લઇ લો.</s> <s>તમે આપળા માટે કઇ લઇ સકો, યા પછી બધા ને, કાઇસિલ ને સેવા પ્રદાન કરો.</s></p>

<p><s>જયારે તમને ટ્રેનિગ મળી જશે ત્યારે તમારી એજશી પ્રબંધ કરવામા સહાયતા કરશે તમારા કાઉસિલ જોડે.</s> <s>પ્રબંધ કરવા માટે સુચી મળી જશે.</s> <s>એજશી મદદ કરશે કાઉસિલ સંસ્થા મા કેટલા પૈસા આપશે તમારા ધર ના પ્રબંધ માટે.</s></p>
L
<p><s>તમારા ફરીથી વિચારો છો મત બદલાવાનો કદાચ તમે ચાહો તો પ્રંબધ ના અધિકાર સ્વીકાર કરી સકો છો.</s> <s>કદાચ તમારો મત હૉ છે, તમને અને કાઉસિલ નિશ્ચય કરશે જયારે તમારા કિરાએદાર સંસ્થા લેશે તમારા ઘર ના પ્રબંધ.</s> <s>યાદ રાખો, કદાચ પ્રબંધ ના અધિકાર હોય તો પણ કાઉસિલ જોડે તમારા ઘર છે.</s></p>

<p><head><s>સુ તમે પ્રબંધ ની સર્વિસ કરી સકો છો?</s></head></p>

<p><s>તમે તમારા જાતે પ્રબંધ કરી સકો છો પ્રબંધ ની બધિ સેવાઓ.</s> <s>આમા થી કઇ પ્રકાર છેઃ</s>
<s>સુધાર કિરાએદાર સામાન્ય સુધાર; અદર અને બાર ની સજાવટ; પુરાની સજાવટ ને હટાવાનુ; બારિઓ ની બારી ને ફ્રેમ; ચમકાવુ; બાગળ; બારની લાઇટ ફીટીગ; અને બેકાર ની વસ્તુઓ;</s>
<s>સેવાઓ - સાફ કરવુ રહવાસી; બાગ બગીચા; ગર્મવાઓ;</s>
<s>વિત્તીય સેવાઓ - ભેગુ કરવુ કિરાએદાર અને સેવાઓ ચાર્જ; સેટિગ સર્વિસ ચાર્જ; અને જમા કરવુ બિલ;</s>
<s>કિરાઓ આપવારા માટે સેવા, ખાલી ઘરો ને કિરાય થી આપવુ, ગૈર કાનુની કાર્ય ને રોકવુ, પહેલા થી કિરાએ મા આપવાની મજુરી આપવી.</s></p>

<p><s>તમે નિશ્ચય કરી સકો તમે ચાહો પ્રબંધની સર્વિસ ને.</s> <s>તમે કદાચ ચાહો કોઇ એક સર્વિસ, યા તમે ચાહો આપળી બધી સર્વિસ લઇ લો.</s> <s>કદાચ તમે ચાહો, તમે શુરુવાત કરી સકો કિ પ્રબંધ દ્વારા એક યા બે સર્વિસ અને જલ્દી વધારે લઇ લો જયારે તમને વધારે અનુભવ મળી જાય.</s></p>

<p><s>તમે ફૈસલો કરી સકો કિ તમને પ્રબંધ માટે કઇ સેવા જોઇએ.</s> <s>થઇ સકે છે કોઇ એક જ સેવા નો પ્રબંધ કરવો પડે, થઇ સકે છે બધી સેવાઓ કરવા નો અવસર મળે.</s> <s>કદાચ તમને જોઇએ, તમે શુર કરી સકો છો પ્રબંધ નુ કામ એક યા બે થી વધાર અને ધીરે ધીરે વધાર અનુભવ થવા પર વધાર કામ કરી સકો છો.</s></p>
L  
<p><head><s>તમને વધારે કેવિતે મળે</s></head></p>

<p><s>તમે શરુ કરો પ્રબંધ ના અધિકાર ની પ્રક્રિયા, તમને મળી સકે ફ્રી સલાહ અને સહાયતા મળે કોઇ એક સલાહ એજસી જે સરકાર ની લિસ્ટ થી મળેલી હોય.</s> <s>તમને મળી સકે આ લિસ્ટ લખેલી ઃ.</s></p> 

<p><s><foreign lang="eng">Jerome john, Tenant Participation Branch, Deparment of the Enviroment, Transport, and the Region 1/J6, Bresseenden Place, London SW1E 5DU Telephone: 020-7944-3488.</foreign></s> <s><foreign lang="eng">Fax: 020-7944-3488</foreign>.</s>
<s>કદાચ <foreign lang="eng">Gwen Jones, Housing Services Branch The National Assembly for Wales, Cathays Park, Cardiff CF1 3NQ Tele. 01222-826942.</foreign></s> <s><foreign lang="eng">Fax: 01222-825136.</foreign></s></p> 

<p><s>તમે પણ કિરાએદાર ને કહી સકો છો સંસ્થા જે કિ પોતાનુ પ્રબંધ પહેલા થી કરી સકે છે.</s></p>   

<p><s>આ પર્ચા ઢેકેદારરોં ને નવા અધિકારોં થી સમ્બંધિત ચાર પર્ચો ની સંખ્યા (પ્રંબધ ના અધિકાર, મરમ્મત ના અધિકાર સુધાર માટે મરમ્મત ના અધિકાર) અને સી.ટી.ટી (કમ્પલસરી કમ્પૈટેટિવ ટેંડરિંગ આફ હાલસિંગ મૅનેજમેટં) ની એક કડ઼ી છે.</s> <s>તમારી પ્રતિયો માટે કૃપયા આને લખો</s></p>

<p><s><foreign lang="eng">DETR Free Literature, PO Box 236, Wetherby LS23 7NB</foreign> ટેલિફોન ૮૭ ૧૨૨૬ ૨૩૬</s>
<s>ફૈક્સ ૮૭ ૧૨૨૬ ૨૩૭</s>
<s>કદાચ</s>
<s><foreign lang="eng">Housing Services Branch</foreign></s>
<s><foreign lang="eng">The National Assembly for Wales,</foreign></s>
<s><foreign lang="eng">Cathays Park,</foreign></s>
<s><foreign lang="eng">Cardiff CF1 3NQ.</foreign></s></p>

<p><s>આ લી઼ફલેટ (પર્ચે) મફત મા મળી સકે છે અને એ વેલ્સ, બંગાલી, હિંદી, ઉર્દુ, ગ્રીક, અને વિએતનામીઝ઼ આ ભાર્ષાઓં મા ઉપલબ્ધ છે.</s></p>

<p><s><foreign lang="eng">Published by the Deparment of the Enviroment, Transport, and the Region and The National Assembly for Wales.</foreign></s> <s><foreign lang="eng"> Crown copyright 1995.</foreign></s> <s><foreign lang="eng">Reprinted in United Kingdom April 2000 (00FI0097) on paper comprising of 75% post-consumer waste and 25% pre-consumer waste.</foreign></s> <s><foreign lang="eng">Product code 94FICa 384.</foreign></s></p>

</body>
</text>
</cesDoc>