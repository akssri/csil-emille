<cesDoc id="guj-w-health-discharge" lang="guj">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>guj-w-health-discharge.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Winfocus Pvt Ltd (Arpita Shah)</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-09-18</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Discharge From Hospital</h.title>
<h.author>Manchester City Council</h.author>
<imprint>
<pubPlace>Manchester</pubPlace>
<publisher>Manchester City Council</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-09-02</date>
</creation>
<langUsage>Translated into Gujerati from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-health-discharge.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s>હોસ્પિટલમાંથી છૂટો ત્યારે</s></head></p>

<p><head><s>માનચેસ્ટર કમ્યુનીટી કેર</s></head></p>

<p><head><s>Discharge from Hospital</s></head></p>

<p><head><s>હોસ્પિટલમાંથી છૂટા કરવામાં આવે ત્યારે</s></head></p>

<p><s>એપ્રીલ ૧૯૯૩થી જ સામાજીક સંભાળ પૂરી પાડવાની રીત તથા તેના નાણાકીય ફંડમાં ફેરફારો આવશે.</s> <s>અગાઉ, રેસીડેન્શીયલ હોમમાં સભાંળ મેળવનારને સોશ્યલ સિકયુરીટી વિભાગ તરફથી પૈસા આપવામાં આવતાં હતાં.</s> <s>પરતું, હવે આ કામ કાઉન્સિલને સોંપવામાં આવેલ છે અને દરેક પ્રકારની સંબાળ સેવાની વ્યવસ્થા કરવાની જવાબદારી સીટી કાઉન્સિલના સોશ્યલ સર્વિસીસ ડીપાર્ટમેન્ટની છે.</s> <s>સંભાળ સેવા પુરુ કરવાનું કાર્ય તેઓ બે રીતે કરશે; એક તો અમુક સેવા તેઓ પોતે પૂરી પાડશે અને બીજુ, ખાનગી તથા સ્વૈચ્છિક ક્શેત્રેની સંસ્થાઓ સાથે કરાર કરીને (ખરીદીને) બીજી વિવિધ સેવાઓ પૂરી પાડશે.</s> <s>આનાથી સેવાનો ઉપયોગ કરનાર તથા તેઓની સંભાળ રાખનારાં સમક્શ સેવાની પસંદગીના ઘણા વિકલ્પો રહેશે.</s> <s>આ ફેરફારો પાછળનો એક મુખ્ય હેતુ એ છે કે જેને સેવાની ખાસ (તાતી) જરુર હોય તેને સેવા મળી રહે અને લોકોને તએઓને જરુરીયાતો મુજબની સેવા મળી રહે.</s></p>

<p><head><s>કોને મદદ મળે</s></head></p>

<p><s>જે લોકો હોસ્પિટલમાંથી છૂટા થવાની તૈયારી હોય, પરંતુ જો તેમને લાંબા ગાળા માટે કે, પછી સ્ટ્રોક કે મોટા ઓપરેશન જેવી બિમારી બાદ રોગમુકિત કે સુધારા માટે સખત મદદ અને સહાયની જરુર હોય, તેઓ મદદ મેળવી શકે.</s></p>

<p><head><s>મદદ કેવી રીતે મેળવવી</s></head></p>

<p><s>આપની જરૂરીયાતોના મૂલ્યાંકનની વ્યવસ્થા કરવા આપની નર્સ કે ડૉકટર સોશ્યલ સર્વિસીસ ડીપાર્ટમેન્ટનો સંપર્ક સાધશે.</s> <s>આપના અંગેની વધુ માહિતી એકત્રિત કરવા તથા આપના વિચારો અને મંતવ્યો જાણવા આપની મુલાકાતે કોઇ આવશે.</s> <s>તે વ્યકિત, જે લોકો આપના સંભાળ કાર્ય સાથે સંકળાયેલા હોય, તેઓ સાથે પણ વાતચિત કરશે, જેમ કે ઃ હોસ્પિટલના કાર્યકરો, આપના પરિવારના સભ્યો, મિત્રો તથા આપના જી.પી.</s></p>

<p><s>આપ માન્ચેસ્ટર શહેરના રહેવાસી ન હોય પણ શહેરની હોસ્પિટલમાં દાખલ થયા હોય તો આપના મૂલ્યાંકનની વ્યવસ્થા માન્ચેસ્ટર સોશ્યલ સર્વિસીસ ડીપાર્ટમેન્ટ કરશે.</s></p>

<p><s>(મૂલ્યાંકનની પ્રક્રિયાની વધુ વિગતો માટે, સોશ્યલ સર્વિસીસ ડીપાર્ટમેન્ટ પાસેથી મદદ કેવી રીતે મેળવવી, નામની પત્રિકા જુઓ).</s></p>

<p><head><s>આપની સંભાળનું આયોજન</s></head></p>

<p><s>બને ત્યાં સુધી તો આપની જરુરિયાતો પ્રમાણેની સંભાળ સેવા આપના પોતાના ઘરે જ કે પછી સમાજમાં પૂરી પાડવી જોઇએ.</s> <s>આમાં હેલ્થ અને સોશ્યલ સર્વિસીસ તરફથી ઉપલબ્ધ સહાય તેમજ આપના પરિવાર, મિત્રમંડળ અને સ્વૈચ્છિક સસ્થાંની મદદનો પણ સમાવેશ થાય છે.</s> <s>પરંતુ અમુક સંજોગોમાં રેસીડેન્શીયલ કે નર્સીંગ હોમમાં સંભાળ પૂરી પાડવી હિતાવહ હોય તેવું પણ લાગે આપની સંભાળ માટે જે પણ નિર્ણય લઇને સેવાનું આયોજન કરવામાં આવે તે આપની મંજૂરી તથા આપની મદદ કરનારા સાથે ચર્ચા-વિચારણા બાદ જ અમલમાં આવે.</s></p>

<p><s>સોશ્યલ સર્વિસીસ ડીપાર્ટમેન્ટ અને માન્ચેસ્ટર હેલ્થ ઓથોરિટીઝે સંયુકતપણે એવાં કરારો કર્યા છે કે જેના દ્વારા હોસ્પિટલમાંથી કોઇ પણ વ્યકિતનો છૂટકારો અસરકારક રીતે આયોજી શકાય.</s> <s>તબિબિ સારવાર માટે જરુરથી વધુ સમય માટે આપને હોસ્પિટલમાં રાખવા ન જોઇએ તેમજ યોગ્ય વ્યવસ્થા કર્યા વગર આપને હોસ્પિટલમાંથી આપને છૂટા કરવાના હોય તે અથવા તેથી અગાઉના દિવસમાં આપના જી.પી. ને જાણ કરવામાં આવશે.</s></p>

<p><s>(રેસીડેન્શીયલ કે નર્સીંગ હોમમાં પ્રવૈશ અંગેની વિશેષ માહિતી માટે, રેસીડેન્શીયલ કે નર્સીગ હોમની પસંદગી નામની, પત્રિકા જુઓ).</s></p>

<p><head><s>સેવાનો કેટલો ખર્ચ હોય</s></head></p>

<p><s>હાલમાં તો માન્ચેસ્ટર સીટી કાઉન્સિલની સોશ્યલ સર્વિસ (સમાજ સંભાળ સેવા) મફત છે.</s></p>

<p><s>જો રેસીડેન્શીયલ હોમ (સંભાળ સેવા પૂરી પાડે એવાં રહેવાના ધરો) માં રહેવા માટે આપ ઇચ્છો અથવા તો ત્યાં રહેવાની જરુર તો મૂલ્યાંકનના એક ભાગ રુપે આપની આવક, બચત અને મિલકત અંગેની વિગતો આપવાની રહેશે.</s> <s>જો આપની પાસે ૩૦૦૦ પાઉન્ડ ઓછી રકમ હોય તો આપે કશું ભરવાનું નહિ રહે; પરંતુ આપ ઇન્કમ સપોર્ટ કે અટેન્ડન્સ અલાઉન્સ મેળવતાં હશો તો તે રકમ ખર્ચ પેટે આપવી પડશે.</s> <s>આપના અંગત ખર્ચ માટે આપ અમુક પૈસા રાખી શકશો.</s> <s>જો આપની પાસે ૩૦૦૦ અને ૮૦૦૦ પાઉન્ડ વચ્ચેની રકમ હશે તો અમુક નકકી કરેલ ધોરણ પ્રમાણે જે રકમ બને તે ખર્ચ પેટે આપવાની રહેશે.</s></p>

<p><head><s>વધુ માહિતી</s></head></p>

<p><s>જો આપને વધુ માહિતી જોઇતી હોય તો વોર્ડ નર્સને પૂછો જે આપની મદદ કરવા સમર્થ હશે.</s> <s>માન્ચેસ્ટર સોશ્યલ સર્વિસીસ ડીપાર્ટમેન્ટની ઓફીસોના સરનામા અને ટેલીફોન નંબરો પાછળના પાને આપવામાં આવ્યાં છે.</s></p>

</body>
</text>
</cesDoc>