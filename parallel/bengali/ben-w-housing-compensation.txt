<cesDoc id="ben-w-housing-compensation" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-housing-compensation.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>translated and typed by</respType>
<respName>Sutapa Ghosh</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-02-14</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>A better deal for tenants: Your new right to compensation for improvements</h.title>
<h.author>Department of the Environment, Transport and the Regions</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department of the Environment, Transport and the Regions</publisher>
<pubDate>March 1994</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-12-22</date>
</creation>
<langUsage>Translated into Bengali from original English (translation made at University of Lancaster for the EMILLE Project)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-housing-compensation.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s>ভাড়াটেদের জন্য একটি অধিকতর উন্নত চুক্তি</s></head></p>

<p><head><s>উন্নতিসাধনের জন্য আপনার ক্ষতিপূরণ পাওয়ার নতুন অধিকার</s></head></p> 

<p><head><s>উন্নতিসাধনের জন্য আপনার ক্ষতিপূরণ পাওয়ার অধিকার</s></head></p> 

<p><s>কাউন্সিলের ভাড়াটেদের তাদের বাড়ির উন্নতিসাধনের জন্য ক্ষতিপূরণ পাওয়ার একটি নতুন অধিকার সিটিজেনস্ চার্টার স্কীম এর আওতায় নিয়ে আসা হয়েছে ১লা এপ্রিল ১৯৯৪ সাল থেকে ।</s> <s> আপনি যদি কাউন্সিল টেনেন্ট হন (একজন বসবাসকারী , একজন বাণিজ্যিক বসবাসকারী নন) এবং আপনার টেনেন্সী যদি শেষের দিকে হয় , তাহলে আপনি কাউন্সিলের কাছ থেকে আপনার বাড়ির উন্নতিসাধনের জন্য যে অর্থ ব্যয় করেছেন তার জন্য ক্ষতিপূরণ পেতে পারেন ।</s> <s> আপনি ক্ষতিপূরণের জন্য দরখাস্ত করতে পারেন যখন আপনার ভাড়ার চুক্তির মেয়াদ শেষ হবে - সাধারণতঃ যখন আপনি বাড়িটি ছেড়ে অন্যত্র চলে যাবেন ।</s> <s> যদি আপনি একজন নতুন বাড়িওয়ালা (ল্যান্ডলর্ড) পান তাহলেও ঐ ক্ষতিপূরণের জন্য আপনি আবেদন করতে পারেন ।</s></p> 

<p><head><s>কে ক্ষতিপূরণ পাবেন ?</s></head></p>

<p><s>প্রায় সমস্ত কাউন্সিল টেনেন্টরাই ক্ষতিপূরণ পাবার অধিকারী ।</s></p>

<p><s>যে ক্ষেত্রে ভাড়ার চুক্তি শেষ হয়ে যায় টেনেন্ট-এর মৃত্যুর জন্য , অথবা অন্যান্য বিশেষ ক্ষেত্রগুলিতেও আপনি ক্ষতিপূরণ দাবী করতে পারেন ।</s> <s> আপনার কাউন্সিল বলতে পারবে এর জন্য আপনি উপযুক্ত কি না ।</s></p>

<p><s>আপনি ক্ষতিপূরণ পাবেন না যদি আপনি বিক্রয়ের অধিকার  (<foreign lang="eng">Right to Buy</foreign>) অথবা বন্ধকের জন্য ভাড়া  (<foreign lang="eng"> Rent to Mortgage</foreign>) এর আওতায় বাড়ি ক্রয় করেন , কারণ উন্নতিসাধনের জন্য খরচগুলো ক্রয়মূল্যের মধ্যে ধরা হয় না ।</s></p> 

<p><head><s>উন্নতিসাধনের জন্য আপনার কি অনুমতি নেওয়ার প্রয়োজন আছে ?</s></head></p>

<p><s>হ্যাঁ । আপনাকে আগে অনুমতি নিতে হবে , আপনি এর জন্য আবেদন করতে পারেন যখন আপনি ক্ষতিপূরণের দাবী করবেন ।</s> <s> যদি আপনার কাউন্সিল অনুমতি দিতে অস্বীকার করেন , তাহলে আপনি কাউন্টি কোর্টে আবেদন করতে পারেন ।</s></p>

<p><s>কিন্তু মনে রাখবেন , আপনি ক্ষতিপূরণ পেতে পারেন না যদি আপনার কাউন্সিল আপনাকে অনুমতি না দেয় এবং কাউন্টি কোর্ট আপনার আবেদন সমর্থ না করে ।</s></p> 

<p><head><s>কি কি ধরণের উন্নতিসাধনের জন্য আপনি ক্ষতিপূরণ পেতে পারেন ?</s></head></p>

<p><s>ক্ষতিপূরণ পাবার অধিকার প্রযোজ্য হবে সেই সব উন্নতিসাধনগুলোর জন্য যেগুলো শুরু করা হয়েছিল ১লা এপ্রিল ১৯৯৪ বা তার পরে ।</s></p> 

<p><s>ক্ষতিপূরণ পাবার অধিকার প্রযোজ্য হয় নিম্নলিখিত উন্নতিসাধনগুলোর জন্য বাথ অথবা শাওয়ার , হাত ধোওয়ার বেসিন , এবং টয়লেট , রান্নাঘরের সিংক এবং খাবার তৈরীর জন্য প্রয়োজনীয় কাজের জায়গার উপরিভাগ ; রান্নাঘর এবং স্নানঘরে জিনিষপত্র রাখার জন্য প্রয়োজনীয় তাকযুক্ত আলমারী (কাপবোর্ড) সেন্ট্রাল হিটিং ; গরম জলের বয়লার এবং অন্যান্য গরম করার সামগ্রী ; থার্মোস্ট্যাটিক রেডিয়েটর ভাল্বস্ ; পাইপ , জলের ট্যাঙ্ক অথবা সিলিন্ডারের তাপ নিরোধক ; লফ্ট এবং ক্যাভিটি ওয়াল -এর তাপ নিরোধক ; বাইরের দিকের দরজা এবং জানালাগুলির জন্য ড্রট প্রুফিং (<foreign lang="eng">Draught-proofing</foreign>) ; ডবল গ্লেজিং অথবা জানালার অন্যান্য পরিবর্তন অথবা সেকেন্ডারী গ্লেজিং ; নতুন ওয়্যারিং ; অথবা আলো এবং পাওয়ারের বন্দোবস্ত অথবা অন্যান্য বৈদ্যুতিক সংযোগ (স্মোক ডিটেক্টর) ; নিরাপত্তা ব্যবস্থা (বার্গলার অ্যালার্ম ছাড়া) ।</s></p> 

<p><s>আভ্যন্তরীণ গৃহসজ্জা (<foreign lang="eng">Interior Decoration</foreign> )র সামগ্রী  (পেইন্টিং এবং ওয়াল পেপারিং) ক্ষতিপূরণযোগ্য নয় ।</s></p>

<p><head><s>আপনি কি ভাবে ক্ষতিপূরণ পেতে পারেন ?</s></head></p>

<p><s>আপনাকে ক্ষতিপূরণের জন্য একটি দাবী জানাতে হবে যখন আপনি কাউন্সিলকে জানাবেন যে আপনি বাড়ি ছেড়ে দিতে চান ।</s> <s> আপনাকে ভাড়ার চুক্তির শেষ হওয়ার ১৪ দিনের মধ্যে একটি দাবী জানাতে হবে ।</s> <s> আপনি যদি এ ব্যাপারে নিশ্চিত না হন , তাহলে আপনার কাউন্সিলকে জিজ্ঞাসা করুন কিভাবে দাবী জানাতে হবে ।</s></p>

<p><s>আপনি কত ক্ষতিপূরণ পাবেন সে ব্যাপারে সিদ্ধান্ত নেওয়ার জন্যে কাউন্সিলের অনেকগুলো তথ্যের প্রয়োজন হবে ।</s> <s> তাদের জানার প্রয়োজন হবে :</s>
 <s>আপনার নাম ও ঠিকানা ; আপনি কি কি উন্নতিসাধন করেছেন ; প্রতিটি উন্নতিসাধনের জন্য কত খরচ হয়েছে ; এবং কোন্ তারিখে উন্নতিসাধন শুরু এবং কোন্ তারিখে শেষ হয়েছিল ।</s></p>

<p><head><s>আপনার ক্ষতিপূরণের পরিমান কিভাবে স্থির করা হয় ?</s></head></p>

<p><s>কাউন্সিল আপনার উন্নতিসাধনের জন্য খরচগুলো খতিয়ে দেখবেন ।</s></p> 

<p><s>আপনি যদি আর্থিক সহায়তা পান (একটি গ্রান্ট) আপনার উন্নতিসাধনগুলোতে সহযোগিতার জন্য , তবে কাউন্সিল উন্নতিসাধনের খরচ থেকে আর্থিক সহায়তার সম পরিমান টাকা বাদ দিয়ে দেবে ।</s></p> 

<p><s>উন্নতিসাধনের মূল্য কম হতে থাকবে যত সেটা পুরাতন হবে এবং সেটা যত বেশী আপনি ব্যবহার করবেন ।</s> <s> ক্ষতিপূরণের পরিমান নির্ভর করবে উন্নতিসাধন কত পুরাতন হয়েছে তার ওপর যেদিন আপনি দাবী করবেন ।</s></p> 

<p><s>কাউন্সিল আপনাকে কম ক্ষতিপূরণ দিতে পারে যদি তারা মনে করে যে উন্নতিসাধনের খরচ অনেক বেশী অথবা গুণমান উচ্চতর হত যদি কাউন্সিল নিজে এই উন্নতিসাধনের কাজ করত ।</s></p> 

<p><s>কাউন্সিল আপনার ক্ষতিপূরনের পরিমান ঠিক করতে পারে - বেশী বা কম - সেটা নির্ভর করে উন্নতিসাধনের অবস্থার উপর যখন আপনি দাবী করবেন ।</s></p> 

<p><s>কাউন্সিল আপনার ক্ষতিপূরণ থেকে অর্থ কেটে নিতে পারে যদি আপনার কাছে তাদের কোন পাওনা বাকি থাকে যখন আপনার টেনেন্সী চুক্তি শেষ হবে ।</s></p> 

<p><s>আপনি কোন একটি উন্নতিসাধনের জন্য সর্বাধিক ৩০০০ পাউন্ড ক্ষতিপূরণ পেতে পারেন , তবে আপনি কোন ক্ষতিপূরণ পাবেন না যদি এই অর্থের পরিমাণ ৫০ পাউন্ডের কম হয় ।</s></p> 

<p><s>আপনি কিসের জন্য ক্ষতিপূরণ দাবি করতে পারেন</s>
<s>আপনি নিম্নলিখিতগুলির জন্য ক্ষতিপূরণ দাবি করতে পারেন :</s>
<s>কাঁচামালের মূল্য (কিন্তু যন্ত্রাদি যেমন কুকার এবং ফ্রীজের জন্য নয় ) ; এবং শ্রমের মূল্য (কিন্তু আপনার নিজের শ্রমের নয়) । </s></p>

<p><s>আপনাকে আপনার কাউন্সিলকে একটা চালান (<foreign lang="eng">Invoice</foreign>) দিতে হবে উন্নতিসাধনের খরচের পরিমান দেখানোর জন্যে ।</s> <s> যদি আপনার কাছে কোন চালান না থাকে , তাহলে কাউন্সিলকে সরাসরি বলুন এবং তাদেরকে খরচের পরিমান সম্পর্কে মোটামুটি একটা ধারণা দিন ।</s></p> 

<p><s>আপনি যদি মিথ্যা দাবি করেন (উদাহরণস্বরূপ যদি আপনি ক্ষতিপূরণের দাবি করেন এমন উন্নতিসাধনের জন্য যেটা) বাস্তবে আপনি করেননি অথবা আপনি বাস্তবে যা খরচ হয়েছে তার চেয়ে বেশী পরিমান দাবি করেন ) , তাহলে কাউন্সিল আপনাকে আদলতে নিয়ে যেতে পারে ।</s></p>

<p><head><s>যদি কাউন্সিল আপনার দাবির কোন অর্থ অথবা কিছু অংশ প্রদান না করে তাহলে কি করবেন ?</s></head></p>

<p><s>আপনি কাউন্সিলকে তাদের সিদ্ধান্ত পুর্নবিবেচনার জন্য বলতে পারেন , প্রায় সব কাউন্সিলেরই নিজস্ব পদ্ধতি আছে বিতর্কিত বিষয়গুলি নিরূপণের জন্য ।</s></p> 

<p><s>আপনারও কাউন্সিলকে আদালতে নিয়ে যাওয়ার অধিকার আছে ।</s> <s> কিন্তু প্রথমে আপনাকে পরামর্শ নিতে হবে কোন উকিল  (<foreign lang="eng">solicitor</foreign>) অথবা আপনার সিটিজেনস্ অ্যাডভাইস ব্যুরো (<foreign lang="eng">Citizens Advice Bureau</foreign>)-এর কাছ থেকে ।</s> <s> আপনি আইনী সাহায্যও পেতে পারেন ।</s></p>

<p><head><s>কিভাবে আরো তথ্য জানতে পারবো ?</s></head></p>

<p><s>আপনার কাউন্সিলের হাউজিং বিভাগের সঙ্গে যোগাযোগ করুন ।</s></p> 

<p><s>এই প্রচারপত্রটি চারটি সিরিসের একটি যেটি নতুন ভাড়াটিয়ার অধিকার নিয়ে ডিল করে (<foreign lang="eng">Right to Manage , Right to Repair , Right to Compensation for Improvements and CCT (compulsory Competitive tendering of Housing Management)</foreign> ) ।</s> 
<s>এই প্রচারপত্রগুলি বিনামূল্যে পাওয়া যায় এবং বাংলা , হিন্দি , উর্দু , গ্রীক এবং ভিয়েতনামিজ ভাষাতেও ।</s></p> 

<p><s>প্রচারপত্রের আরো কপির জন্য অনুগ্রহ করে এই ঠিকানায় লিখুন :</s>
 <s><foreign lang="eng">DETR Free Literature</foreign></s>
 <s><foreign lang="eng">P.O. Box 326</foreign></s>
 <s><foreign lang="eng">Wetherby</foreign></s>
 <s><foreign lang="eng">West Yorkshire</foreign></s>
 <s><foreign lang="eng">LS23 7NB</foreign></s>
 <s>টেলিফোন : ০৮৭০ ১২২৬ ২৩৬</s>
 <s>টেক্স্ট ফোন : ০৮৭০ ১২০ ৭৪০৫</s>
 <s>ফ্যাক্স : ০৮৭০ ১২২৬ ২৩৭</s>
 <s>ইমেইল: <foreign lang="eng">detr@twotenn.press.net</foreign></s></p>

<p><s>ডিপার্টমেন্ট অনেকগুলি সিরিসে হাউজিংয়ের বিষয়ে প্রচারপত্রগুলি প্রকাশ করে ।</s> <s> একটি পূর্ণ তালিকা , দি হাউজিং পাবলিকেশনস অর্ডার ফর্ম (<foreign lang="eng">The Housing Publications Order Form</foreign>) , প্রোডাক্ট কোড 96HC 0156 , এটিও পাওয়া যায় উপরের ডিইটিআর ফ্রি লিটারেচারের উপরের ঠিকানায় ।</s></p>                        
</body>
</text>
</cesDoc>