<cesDoc id="ben-w-health-looking" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-health-looking.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Sutapa Ghosh</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-05-16</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>How to get help in looking after someone</h.title>
<h.author>Department of Health</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department of Health</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-05-06</date>
</creation>
<langUsage>Translated into Bengali from English</langUsage>
<wsdUsage>
<writingSystem id= "ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="single"></constitution>
<derivation type="original"></derivation>
<domain type="public"></domain>
<factuality type="fact "></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-health-looking.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<p><head><s><foreign lang="eng">How to get help in looking after someone- bengali</foreign></s></head></p>

<p><head><s><foreign lang="eng">Department of Health</foreign></s></head></p>

<p><head><s>কারুর দেখাশোনা করায় কি করে সাহায্য পাওয়া যায়</s></head></p>

<gap desc="characters scrambled due to font corruption" extent="two paragraphs"></gap>

 <p><s>১। বাড়িতে বাস করতে সাহায্য দরকার হয় এরকম কোন আত্মীয় , বন্ধু অথবা প্রতিবন্ধী সন্তানকে যদি আপনি দেখাশোনা করেন , তাহলে আপনি একজন "কেয়ারার" ।</s> <s> আপনার স্থানীয় কাউন্সিলের সোশাল সার্ভিসেস্ ডিপার্টমেন্ট হয়ত আপনার ভার কমাতে সাহায্য করতে পারে ।</s></p>

<p><s>২। আপনি যার দেখাশোনা করেন , তার জন্য তারা সার্ভিস দিতে পারে ।</s> <s> তারা আপনার জন্যও সার্ভিস দিতে পারে । </s></p>

<p><s>৩। আপনার পরিস্থিতিতে কোন্ কোন্ সার্ভিস দিলে আপনার সুবিধা হবে তা ঠিক করার জন্য সোশাল সার্ভিসেস্ নিচের বিষয়গুলি সম্বন্ধে আলোচনা করতে চাইবে</s>
 <s>আপনি যাকে দেখাশোনা করেন তার কি সাহায্য দরকার</s>
 <s>বর্তমানে আপনি কি সাহায্য করেন</s>
 <s>যে যে সার্ভিসগুলি আপনার কাউন্সিল হয়ত দিতে পারে ।</s></p>

<p><s>যে ভাবে এটা ঠিক করা হয় , তাকে বলা হয় "এ্যাসেসমেন্ট" ।</s></p> 

<p><head><s>৪। কেয়ারার হিসেবে আপনি হয়ত বিভিন্নভাবে জড়িত থাকবেন :</s></head></p>

<p><s>আপনি যদি কোন প্রতিবন্ধী সন্তানের দেখাশোনা করেন , তাহলে আপনার ও আপনার পরিবারের অঙ্গ হিসাবে আপনার প্রয়োজনের কথা বিবেচনা করে দেখা হবে</s>
 <s> আপনি যদি কোন প্রাপ্তবয়স্কলোকের দেখাশোনা করেন , তলে আপনার কি সাহায্য দরকার সে সম্বন্ধে আলোচনা করার জন্য আপনি কেয়ারারস্ এ্যাসেসমেন্ট করাতে পারেন । </s> <s>আপনি যার দেখাশোনা করছেন , তিনি সাহায্য নিতে না চাইলেও আপনি এটা করাতে পারেন ।</s> <s> আপনি যার দেখাশোনা করছেন , তার প্রয়োজনের বিষয় সম্বন্ধে আলোচনায়ও আপনি অংশ নিতে পারেন (তাদের কমিউনিটি কেয়ার এ্যাসেসমেন্ট) ।</s></p> 

<p><head><s>৫। আপনার এ্যাসেসমেন্ট কিসের জন্য</s></head></p> 

<p><s>যে সব জিনিষ আপনার দেখাশোনার করার কাজটা অপেক্ষাকৃত সহজকরতে পারে , সেই সব বিষয় সোশাল সার্ভিসেসকে জানানোর একটা সুযোগ হচ্ছে আপনার কেয়ারারস্ এ্যাসেসমেন্ট ।</s></p>

<p><head><s>যে সব বিষয় সম্বন্ধে আপনি হয়ত ভাবেত চাইবেন :</s></head></p>

<p><s>আপনার ঘুম কি যথেষ্ট হচ্ছে ?</s>
 <s>অন্য কোন ভাবে কি আপনার স্বাস্থ্য নিয়ে সমস্যা হচ্ছে ?</s>
 <s>আপনি কি বাইরে বেড়াতে ও ঘুরোঘুরি করতে পারেন ?</s>
 <s>আপনি নিজের জন্য কোন সময় পান কি ?</s>
 <s>আপনার অন্যান্য সম্পর্কের উপর কি এর প্রভাব পড়ছে ?</s>
 <s>আপনি কি বেনিফিট সম্বন্ধে তথ্য চান ?</s>
 <s>আপনাকে কাজ ছেড়ে দিতে হবে বলে কি আপনি চিন্তিত ?</s>
 <s>আপনি যার দেখাশোনা করেন, তিনি কি যথেষ্ট সাহায্য পাচ্ছেন ?</s></p>

<p><head><s>৬। কি ধরণের সার্ভিস আপনাকে হয়ত সাহায্য করতে পারবে :</s></head></p>

<p><s>যে সব সার্ভিস আপনাকে একটু বিশ্রাম দেয়</s>
<s>অন্যান্য কেয়রার অথবা যারা বোঝেন , তাদের কাছ থেকে মানসিক সাহায্য</s>
<s>বাড়ির কাজকর্মে সাহায্য</s>
<s>দিনে / রাত্রে দেখাশোনার কাজে সাহায্য</s>
<s> বেনিফিট সম্বন্ধে উপদেশ</s>
<s>আপনি যার দেখাশোনা করেন ,তার জন্য কিছু ক্রিয়াকলাপ</s></p> 

<p><s>তবে , কেয়ারাররা সকলেই স্বতন্ত্র , এবং আপনি যে সার্ভিস পেলে আপনার দেখাশোনা করতে সুবিধা হবে , অথবা আপনার ভাল থাকায় সাহায্য হবে বলে তারা মনে করবে , আপনার স্থানীয় কাউন্সিল হয়ত আপনাকে সার্ভিস দিতে পারে ।</s> <s> কাজেই আপনি যদি ভেবে থাকেন যে কোন্ কোন্ সার্ভিস পেলে আপনার সুবিধা হবে , সেগুলি সম্বন্ধে আপনার সোশাল সার্ভিসেস্-এর সাথে আলোচনা করবেন ।</s> <s> আপনি যার দেখাশোনা করেন , তাকে যে সার্ভিসগুলি হয়ত সাহায্য করতে পারবে , সেই সব অন্যান্য সার্ভিসগুলি  আপনি হয়ত আলোচনা করতে চাইতে পারেন ।</s></p> 

<p><head><s>সোশাল সার্ভিসেস্-এর কাছে যে সব অন্যান্য বিষয় সম্বন্ধে আপনি হয়ত জানতে চাইতে পারেন :</s></head></p>

<p><s>যে সব স্থানীয় ও জাতীয় প্রতিষ্ঠানের সাথে আপনি যোগাযোগ করতে পারেন</s>
<s>অন্য কি সাহায্য আপনি পেতে পারেন</s>
<s> সার্ভিসের জন্য কিছু চার্জ (খরচ) দিতে হয় কিনা</s>
<s> অভিযোগ করতে চাইলে কি করতে হবে</s></p> 

<p><head><s>এ্যাসেসমেন্ট চাইবার জন্য সোশাল সার্ভিসেস্-এর সাথে যোগাযোগ করা ।</s></head></p>

<p><gap desc="characters scrambled due to font corruption" extent="one line"></gap><s><foreign lang="eng">Local Authority</foreign> <gap desc="characters scrambled due to font corruption" extent="one line"></gap><foreign lang="eng">Community Information</foreign> <gap desc="characters scrambled due to font corruption" extent="one line"></gap><foreign lang="eng">Social Services Department</foreign><gap desc="characters scrambled due to font corruption" extent="two lines"></gap></s></p>  

<p><head><s>অন্যান্য যে সব যোগাযোগের তথ্য কেয়ারারদের উপকারে লাগতে পারে</s></head></p> 

<p><s><gap desc="characters scrambled due to font corruption" extent="half a line"></gap>০৮০৮ ৮০৮ ৭৭৭৭</s>
 <s><foreign lang="eng">(CNA) www.carers.uk.demon.co.uk</foreign></s></p>

<p><s><gap desc="characters scrambled due to font corruption" extent="half a line"></gap>০২০ ৭৩৮৩ ৩৫৫৫৫</s>
 <s><foreign lang="eng">www.cafamily.org.uk</foreign></s></p>

<p><s><gap desc="characters scrambled due to font corruption" extent="half a line"></gap>০১৭৮৮ ৫৭৩৬৫৩</s>
 <s><foreign lang="eng">www.crossroads.org.uk</foreign></s></p>

<p><s><gap desc="characters scrambled due to font corruption" extent="half a line"></gap>০২০ ৭৪৮০ ৭৭৮৮</s>
 <s><foreign lang="eng">(PRTC) www.carers.org.</foreign></s></p>

<p><s><gap desc="characters scrambled due to font corruption" extent="three lines"></gap><foreign lang="eng">www.carers.gov.uk</foreign><gap desc="characters scrambled due to font corruption" extent="half a line"></gap></s></p>

</body>
</text>
</cesDoc>