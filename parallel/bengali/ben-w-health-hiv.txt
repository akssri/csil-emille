<cesDoc id="ben-w-health-hiv" lang="ben">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>ben-w-health-hiv.txt</h.title>
<respStmt>
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Samrat Roy</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT,UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>00-07-12</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.Title>চোServices for people with HIV/AIDS</h.Title>
<h.author>Manchester Citry Council</h.author>
<imprint>
<pubPlace>Manchester</pubPlace>
<publisher>Manchester Citry Council</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>unknown</date>
</creation>
<langUsage>translated into Bengali from original English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality> 
</textClass>
<translations>
<translation trans.loc="parallel file identifier" lang="eng" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>
<text>
<body>

<p><head><s>এইচ আই ভি/এইডস আক্রান্ত ব্যক্তিদের জন্য সার্ভিসসমূহ</s></head></p>

<p><s>এইচ আই ভি/এইডস আক্রান্ত ব্যক্তিদের সাহায্য ও সহায়তার জন্য ম্যানচেষ্টারে অনেক সার্ভিস রয়েছে যাতে লোকজন তাদের পছন্দমত সার্ভিস বেছে নিতে পারে । </s></p>

<p><head><s>কিভাবে সাহায্য পাবেন ?</s></head></p>

<p><s>আপনার বা আপনার পরিচিত কারো যদি এইচ আই ভি/এইডস থাকে এবং সাহায্যের প্রয়োজন হয় তবে আপনার স্থানীয় সোসাল সার্ভিসেস অফিসের সাথে যোগাযোগ করবেন । </s> <s> ঠিকানা ও টেলিফোন নম্বরসমূহ প্রচারপত্রের শেষের দিকে রয়েছে ।  তারা আপনাকে সাহায্য করার চেষ্টা করবেন, হয় আপনাকে উপদেশ ও তথ্য দেয়ার মাধ্যমে না হয় আপনার নিরূপণের ব্যবস্থা করে, (সোসাল সার্ভিস থেকে কিভাবে সাহায্য পাবেন নামক প্রচারপত্রটি দেখুন ।)</s> </p>

<p><s>আপনি যদি নিরূপণের যোগ্য হন তাহলে আপনার প্রয়োজনীয় সাহায্য সম্বন্ধে আপনাকে বিভিন্ন প্রশ্ন করা হবে । </s> <s> আপনার সেবা যত্নে নিয়োজিত অন্য লোকের সাথেও পরামর্শ করা হবে, যেমন আপনার আত্মীয় বা জিপি । </s> <s> তখন আপনার সম্মতি নিয়ে আপনার জন্য একটা কেয়ার প্ল্যান গ্রহণ করা হবে । </s> <s> তাতে নিম্নের এক বা একাধিক সার্ভিস অন্তর্ভুক্ত হতে পারে । </s> </p>

<p><head><s>সোসাল সার্ভিস</s></head></p>

<p><s>এইচ আই ভি/এইডস বিষয়ে বিশেষঞ্জ সোসাল ওয়ার্কাররা হাসপাতাল বা কমিউনিটি থেকে কাজ করেন । </s></p>

<p><s>ঘরে সহায়তাদান সার্ভিস যেটা ২৪ ঘন্টা জরুরী অবস্থার ও সাময়িক সেবাযত্ন প্রদান করে । </s></p>

<p><s>একজন ওয়েলফেয়ার রাইট্স বিশেষঞ্জ অফিসার যিনি বেনিফিটের উপর উপদেশ দিতে পারবেন । </s></p>

<p><s>শারীরিক এবং ইন্দ্রিয় অসমর্থতার সার্ভিসের কর্মচারীগণ যারা তাদের দায়িত্বের অংশ হিসেবে এইচ আই ভি/এইডস বিষয় নিয়েও কাজ করে । </s></p>

<p><head><s>হেল্থ সার্ভিস</s></head></p>

<p><s>বেশীরভাগ এইচ আই ভি/এইডস আক্রান্ত লোক তাদের নিজের ঘরে সেবাযত্ন পেয়ে থাকে । </s></p>

<p><s>মনসাল হাসপাতাল, উইদিংটন হাসপাতাল এবং ম্যানচেষ্টার রয়াল ইনফার্মারীতে ভর্তি হয়ে সেবাযত্ন পাওয়ার ব্যবস্থা রয়েছে । </s> <s> এই তিনটি হাসপাতাল রোগীদের জন্য বহির্বিভাগের সার্ভিসও প্রদান করে যেটাতে পরীক্ষা করানো ও কাউন্সেলিং ও অন্তর্ভুক্ত রয়েছে । </s> <s> মনসাল হাসপাতালে অবস্থিত এক নার্সিং টিম হাসপাতাল ও ঘরের সেবাযত্নের মধ্যে সমম্বয় সাধন করে । </s> </p>

<p><head><s>ভলান্টারী সেক্টর</s></head></p>

<p><s>বডি পজিটিভ নর্থ ওয়েস্ট এবং জর্জ হাউজ ট্রাস্টসহ শহরের মধ্যে ভলান্টারী সেক্টরের অনেকগুলো স্কিম রয়েছে যেগুলো কাউন্সেলিং, ড্রপ ইন (<s>যখন খুশী আসতে পারবেন</s>) সেন্টার ছাড়াও বন্ধুত্বপূর্ণ পরিবেশে সার্ভিস প্রদান করে থাকে । </s> <s> আপনার নিকটস্থ ভলান্টারী গ্রুপের সাথে স্থানীয় সোসাল সার্ভিস যোগাযোগ করিয়ে দিতে পারবে । </s> </p>

<p><head><s>আরো তথ্য</s></head></p>

<p><s>সোসাল সার্ভিস কি কি সেবা প্রদান করে সে সম্বন্ধে আপনি যদি আরো তথ্য জানতে চান তাহলে আপনার স্থানীয় অফিসের ঠিকানা ও টেলিফোন নম্বর পরের পৃষ্ঠায় রয়েছে । </s></p>   

</body>
</text>
</cesDoc>   