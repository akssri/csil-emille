<cesDoc id="hin-w-health-mmr" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-health-mmr.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>translated and typed by</respType>
<respName>Hrishikesh Arvind Rajhans</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-02-05</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>MMR The Facts</h.title>
<h.author>Department of Health</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department of Health</publisher>
<pubDate>unknown</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-02-01</date>
</creation>
<langUsage>Translated into Hindi from original English (translation made at University of Lancaster for the EMILLE Project)</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-health-mmr.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>
<p><head><s><foreign lang="eng"> MMR </foreign> - आरोग्य क्षेत्र में प्रवीण लोगों के लिए जानकारी देने वाला दस्तावेज ।</s></head></p>

<p><head><s><foreign lang="eng"> MMR </foreign> - कुछ बातें । </s></head></p>
 
<p><head><s>पुस्तिका का सारांश । </s></head></p>

<p><s>आप के बच्चे को रोग प्रतिबंधकारक करने संबंधी लेने वाला फैसला कभी भी आसान नही है । </s> <s>बहुत बार उस पे दी गई जानकारी बहुत ही हैरान कर देने वाली रहती है और कभी 
कभी वह उस से भी बुरी हालत में अखबार और दूरचित्रवाणी संच वाले लोग सादर करते है । </s> <s>इस पुस्तिका का मुख्य उद्देश्य आप को उन मुख्य समाचारों के पीछे रहने वाली सच्चाई बताना और उस 
में मदद करना है । </s> <s>अगर आप को इस के बारे में अधिक जनकारी चाहिये तो कृपया आप के
<foreign lang="eng"> GP </foreign> से , आरोग्य सेवा घर में लाने वाले से , या फिर प्रशिक्षित परिचारिका से पूछ सकते है । </s></p>

<p><head><s><foreign lang="eng"> MMR </foreign> क्या है ?</s></head></p>

<p><s><foreign lang="eng"> MMR </foreign> की रोग प्रतिबंधकारक टीका आप के बच्चे को <foreign lang="eng"> measles , mumps </foreign> और <foreign lang="eng"> rubella (German measles) </foreign> से बचाती है । </s> <s>यह बच्चे को <foreign lang="eng"> 13 </foreign> महीनों की आयु में दी जाती है और फिर से जब वह पाठशाला जाने से पहले दी जाती है । </s> <s>अगर पहली मात्रा से संरक्षण ना मिला तो दूसरी मात्रा किसी को भी बचा सकती है । </s> <s>जब से <foreign lang="eng"> 1988 </foreign> से <foreign lang="eng"> MMR </foreign> की पेहचान <foreign lang="eng"> UK </foreign> में बनी है तब से बच्चों की वह बीमारी की प्रतिशत संख्या कम हो गई है और अभी वह सब से नीचांक पर पहुँच चुकी है । </s></p> 

<p><s><foreign lang="eng"> Measles </foreign> एक बहुत गंभीर बीमारी है जो यह रोग प्रतिबंधकारक टीका आप को बचा सकत है ।</s> <s><foreign lang="eng"> Measles </foreign> से बहुत बार कठिनाई उपस्थित होती है और वह जान लेवा भी हो सकती है ।</s> <s><foreign lang="eng"> Mumps </foreign> के टीके से को रका जा सकता है  जो कि बच्चे में <foreign lang="eng"> viral meningitis </foreign> होने का सबसे बडा कारण था । </s> <s><foreign lang="eng"> Rubella </foreign> के टीके नवजात शिशु को बहुत गंभीर रुप से नुकसान पहुँचाने से बचाते है अगर उन की माताएं गर्भावस्था के समय  <foreign lang="eng"> Rubella </foreign> की शिकार हो गयी हो तो । </s></p>

<p><s><foreign lang="eng"> MMR </foreign> यह रोग एक ही सुई से मिटा सकता है । </s></p>
 
<p><head><s><foreign lang="eng"> MMR </foreign> से कोई अपाय हो सकता है?</s></head></p>

<p><s><foreign lang="eng"> MMR </foreign> में तीन अलग अलग टीके होते है जो कि एक ही सुई में रहते है । </s></p>

<p><s>टीकों का अलग अलग अपाय अलग अलग समय पर होते है । </s> <s><foreign lang="eng"> MMR </foreign> लेने के हफ्ते बाद से दस दिन तक कुछ बच्चों को अपना बदन गरम लगने लगता है और फिर इन के शरीर पर के <foreign lang="eng"> measles </foreign> समान एक रॅश आ सकता है और वह खाना भी वर्ज्य कर सकते है ।</s> <s>यह इसलिए होता है क्योंकि टीके में समा <foreign lang="eng"> measles </foreign> अपना काम शुरु करता है । </s> <s><foreign lang="eng"> MMR </foreign> लेने के छ हफ्ते बाद आप के बच्चे को बहुत ही कम आशंका है कि छोटे फोड आयेंगे जो कि टीके के <foreign lang="eng"> measles </foreign> या फिर <foreign lang="eng"> rubella </foreign> से प्रभावित हो सकते है ।</s> <s>वह सर्वसाधारण रुप से अपने आप ठीक होता है , लेकिन आप अगर ऐसे फोड देख रहे है तो डाक्टर को संपर्क कर सकते है । </s> <s>सुई देने के तीन या चार हफ्ते बाद बच्चे को <foreign lang="eng"> </foreign>mumps का माइल्ड प्रकार आ सकता है जो कि <foreign lang="eng"> MMR </foreign> से प्रभावित हो सकता है ।</s></p>

<p><s>बहुत कम बार , बच्चों को गंभीर एलर्जीक परिणामों का सामना कोई टिका लेने के बाद करना पडता है (तकरीबन <foreign lang="eng"> 100,000 MMR </foreign> के टिकों में <foreign lang="eng"> 1 </foreign> से) </s> <s>अगर बच्चे का इलाज जल्दी से किया जाये तो बच्चा या फिर बच्ची पूरी तरह से तंदुरुस्त हो सकते है । </s> <s>जो लोग टीके देते है उन्हें एलर्जीक परिणामों का सामना करने के लिए प्रशिक्षण दिया जाता है । </s></p>

<p><head><s>निम्नलिखित सूची बीमारीयों के गंभीर परिणाम और उस का <foreign lang="eng"> MMR </foreign> पर होने वाला असर इस के बीच में तुलना करती है । </s></head></p>

<p><s>स्थिती </s>
<s>बच्चे जो नैसर्गिक बीमारी से परिणामित हुए है । </s>
<s>बच्चे जो <foreign lang="eng"> MMR </foreign> के पहले टीके से परिणामित हुए है । </s>
<s>चक्कर</s>
<s><foreign lang="eng"> 200 </foreign> मैं से <foreign lang="eng"> 1 </foreign></s>
<s><foreign lang="eng"> 1000 </foreign>मैं से <foreign lang="eng"> 1 </foreign></s>
<s><foreign lang="eng"> Meningitis </foreign> या फिर <foreign lang="eng"> encephalitis </foreign></s> 
<s><foreign lang="eng"> 200 </foreign> मैं से <foreign lang="eng"> 1 </foreign> से लेकर <foreign lang="eng"> 5000 </foreign> मैं से <foreign lang="eng"> 1 </foreign></s>
<s>एक मिलियन में <foreign lang="eng"> 1 </foreign> से भी कम </s>
<s>स्थिती जिस से खुन में गाठ निर्माण हो सकती है ।</s>
<s><foreign lang="eng"> 3000 </foreign> मैं से <foreign lang="eng"> 1(rubella) </foreign></s>
<s><foreign lang="eng"> 6000 </foreign> मैं से <foreign lang="eng"> 1(measles) </foreign></s>
<s><foreign lang="eng"> </foreign>22,300 मैं से <foreign lang="eng"> 1 </foreign></s>
<s><foreign lang="eng"> SSPE </foreign> (<foreign lang="eng"> measles </foreign> का बहुत देरी से होने वाला परिणाम जिस से मेंदु को बुरी तरह से नुकसान हो सकता है और मौत भी आ सकती है । </s>
<s><foreign lang="eng"> 8000 </foreign>मैं से <foreign lang="eng"> 1 </foreign>(बच्चे जिन की आयु <foreign lang="eng"> 2 </foreign> साल से कम है ।)</s>
<s><foreign lang="eng"> 0 </foreign></s>
<s>मौतें</s>
<s><foreign lang="eng"> 2500 </foreign> मैं से <foreign lang="eng"> 1 </foreign> से लेकर <foreign lang="eng"> 5000 </foreign>मैं से <foreign lang="eng"> 1 </foreign>(यह आयु पर निर्भर करता है )</s>
<s><foreign lang="eng"> 0 </foreign></s></p>
 
<p><head><s>रिपोर्ट के अनुसार <foreign lang="eng"> Autism </foreign> और <foreign lang="eng"> MMR </foreign> में कोई संबंध है ? क्या यह सचमुच में धोकादायक है ?</s></head></p>

<p><s>कौनसी भी <foreign lang="eng"> autism </foreign> इतनी लंबे समय तक मालूम नही थी जब से इस देश में<foreign lang="eng"> MMR </foreign> का पहले बार उपयोग हुआ था ।</s> <s><foreign lang="eng"> Autism </foreign> एक प्रकार का रोग है जिस में बर्ताव और भाषा में कठिनाई आ सकती है, और वह पहले से अभी सामने आना चालु हो गया है और ऐसे बहुत सारे लोग <foreign lang="eng"> autism </foreign> के शिकार हो चुके है जिन के नंबर बढते ही जा रहे है जब तक <foreign lang="eng"> MMR </foreign> शुरु नही हुआ था । </s> <s><foreign lang="eng"> MMR </foreign> जब से शुरु हुआ है उस के पहले <foreign lang="eng"> autism </foreign> में अनपेक्षित बढाव नही आया था । </s> <s>सर्वसाधारण रुप से <foreign lang="eng"> MMR </foreign> देते समय पालक <foreign lang="eng"> autism </foreign> के पहले  चिन्ह देख लेते है । </s> <s>इस का अर्थ यह नही है कि एक से दूसरे का जन्म होता है । </s></p>

<p><s>बहुत संशोधन करने के बाद यह निष्कर्ष निकाला जा सकता है कि <foreign lang="eng"> </foreign>Autism और <foreign lang="eng"> MMR </foreign> में कोई संबंध नही है । </s> <s>यह संशोधन निम्नलिखित देशों में जिन में शामिल है <foreign lang="eng"> </foreign>USA, Sweden and Finland वहा के बच्चों पर प्रयोग कर के निकाला गया है । </s> <s>देश विदेश के जाने माने चिकित्सकों ने जिन में <foreign lang="eng"> World Health Organization </foreign> भी शामिल है यह माना है कि <foreign lang="eng"> Autism </foreign> और <foreign lang="eng"> MMR </foreign> में कोई संबंध नही है । </s></p>

<p><s>इस के बारे में अधिक जानकारी <foreign lang="eng"> Factsheet 2 </foreign> में मिलेगी । </s></p>
 
<p><head><s>रिपोर्ट के अनुसार <foreign lang="eng"> bowel disease </foreign> और <foreign lang="eng"> MMR </foreign> में कोई संबंध है ?</s></head></p>

<p><s>ऐसा सुझाव दिया गया है कि <foreign lang="eng"> measles </foreign> नैसर्गिक बीमारी या फिर टीकों से फैलता है , वह <foreign lang="eng"> bowel </foreign> में रह सकता है या फिर <foreign lang="eng"> bowel </foreign> की बीमारी दे सकता है । </s> <s>लेकिन <foreign lang="eng"> bowel </foreign>की बीमारी टिका लगवाए हुए लोगों से जो टिका नही लगवाते उन में आम बात है । </s> <s>फिर से, वहां पर बहुत सारे संशोधन मिलेंगे जिस से यह साबित होता है कि उस टीके से कोई संबंध नही है । </s></p>

<p><s>जग भर के तञों ने और <foreign lang="eng"> World Health Organization </foreign> ने भी यह निष्कर्ष निकाला है कि उन के पास आए हुए सबुतों से यह साबित होता है कि <foreign lang="eng"> measles </foreign> और <foreign lang="eng"> MMR </foreign> के टीके और <foreign lang="eng"> bowel </foreign> की बीमारी में कोई संबंध नही है ।  </s></p>

<p><head><s>क्या बच्चे बडे होने के बाद ही उन्हें दिया गया <foreign lang="eng"> MMR </foreign> का टीका सुरक्षित था या नही इस का अंदाजा लगाया जा सकता है ?</s></head></p> 

<p><s>अमरीका में <foreign lang="eng"> MMR </foreign> का टीका पिछले <foreign lang="eng"> 30 </foreign>सालों से दिया जा रहा है । </s> <s>जगभर में <foreign lang="eng"> 500 </foreign> मिलियन खुराक <foreign lang="eng"> 90 </foreign> से भी अधिक देशों में दिये गये है और टीके में बेहतरीन सुरक्षा प्रोफाइल है ।</s> <s>फिनलंड में, जहां पर बच्चों को <foreign lang="eng"> 1982 </foreign> के बाद दो खुराक<foreign lang="eng"> MMR </foreign> की दे दी थी <foreign lang="eng"> MMR </foreign> का परिणाम बाहर आते ही उस की जांच कर दी गई । </s> <s>संशोधन के बाद यह पता चला है कि टीके से संबंधित कोई मृत्यु या फिर कायमस्वरुपी नुकसान नही हुआ है । </s> <s><foreign lang="eng"> World Health Organization </foreign> के मुताबिक <foreign lang="eng"> MMR </foreign> एक अत्यंत बेहतरीन टीका है जिस का लाजवाब सुरक्षित रिकार्ड है । </s></p>
 
<p><head><s>बच्चों को टीके अलग रुप से देने से कोई फायदा होगा क्या ?</s></head></p>

<p><s>जी नही , टिके अलग रुप से देने से बच्चे <foreign lang="eng"> measles </foreign> या फिर <foreign lang="eng"> mumps </foreign> या फिर <foreign lang="eng"> rubella </foreign> जैसे बीमारीयों के शिकार हो सकते है । </s></p>

<p><s>यह बहुत ही घातक या फिर नुकसान देई रोग हो सकते है । </s> <s>ऐसे बताया जाता है 
कि टीके की तीन खुराके एकसाथ देने के बाद बच्चों की रोग प्रतिकार क्षमता पर बहुत जोर पड सकता है । </s> <s>लेकिन यह बात नही है । </s> <s>बच्चों की रोग प्रतिकार क्षमता बहुत अच्छे परतसद देती है , नैसर्गिक रुप से बच्चों को इन बीनारीयों से बचाती है । </s> <s>दुनिया में कोई भी देश इस 
का सुझाव नही देता कि <foreign lang="eng"> MMR </foreign> की तीन अलग अलग खुराके दी जाये । </s> <s><foreign lang="eng"> World Health Organization </foreign> तीन अलग अलग खुराके देने के खिलाफ सुझाव देती है क्योंकि उस से बच्चों को कोई फायदा नही पहुँचता उलटा 
नुकसान होने का खतरा हो सकता है । </s></p>

<p><head><s>मुख्य आरोग्य अधिकारी से सब को एक वैयक्तिक सलाह </s></head></p>

<p><s>"एक युवा बच्चे का कोई भी पालक <foreign lang="eng"> MMR </foreign> के बारे में दुःखद समाचार भुला नही सकता है जो कि कोई पिछले कुछ सालों से आ रहे है । </s> <s>मुझे कोई शंका नही लगती कि <foreign lang="eng"> MMR </foreign> एक बहुत ही सुरक्षित तरीका है जिस से पालक अपने बच्चे को बचा सकते है ।</s> <s>मुझे यह अंदाजा है कि यह पालकों के लिए बहुत कढीन है कि सच्चाई जानना जिस से वह सकारात्मक फैसले कर सकें । </s> <s>हमने यह पुस्तिका लोगों को एकत्र खीचने के लिए की है, जिस से हम उन्हें शास्त्रीय रुप से अच्छा मार्गदर्शन कर सकें और <foreign lang="eng"> MMR </foreign>पर सलाह भी दे सकते है ।</s> <s>पुस्तिका में यहां के और दुनिया भर के ताजा संशोधन शामिल है । </s> <s>और इस संशोधन में आप देखेंगे कि अभ्यास करने के बाद यह नतीजा निकला है कि <foreign lang="eng"> MMR </foreign> किसी भी तरह से <foreign lang="eng"> autism </foreign> या फिर <foreign lang="eng"> bowel disease </foreign> से जुडा हुआ नही है । </s></p>

<p><s>"सारे गवाहों को देखने के बाद , मुझे आशा है आप सबसे बेहतरीन फैसला लेने के लिए सक्षम बने होंगे - आप के बच्चे को <foreign lang="eng"> MMR </foreign> से बचाना "</s></p>

<p><s><foreign lang="eng"> Professor Liam Donaldson </foreign></s>
<s>इंग्लंड के मुख्य आरोग्य अधिकारी </s></p>

<p><s>सारी दुनिया से मिले हुए पुरावों से यह साबित होता है कि <foreign lang="eng"> MMR </foreign> किसी भी तरह से <foreign lang="eng"> autism </foreign> या फिर <foreign lang="eng"> bowel </foreign> के बीमारी से जुडा हुआ नही है ।</s>
<s><foreign lang="eng"> MMR </foreign> बच्चों को तीन बीमारीयों से बचाता है जो कि बहुत
खतरनाक हो सकते है । </s> <s>अलग अलग टीके देना शायद खतरनाक भी हो सकता है । </s>
<s>आप को इस के बारे में अधिक जानकारी आप के <foreign lang="eng"> GP </foreign>, आरोग्य अधिकारी या फिर प्रशिक्षित परिचारिका से मिलेगी । </s> <s>कृपया प्रश्न पूछने में बिल्कुल ना हिचकिचाइये । </s></p>
 
<p><head><s>सारांश </s></head></p>

<p><s><foreign lang="eng"> MMR </foreign> के टीके बच्चे को <foreign lang="eng"> measles, mumps </foreign> और <foreign lang="eng"> rubella </foreign> से बचाते है ।</s>
<s>पिछले <foreign lang="eng"> 30 </foreign> सालों से <foreign lang="eng"> 500 </foreign> मिलियन <foreign lang="eng"> MMR </foreign> के खुराक <foreign lang="eng"> 90 </foreign> से भी अधिक देशों में दिये गये है । </s> <s>और टीके में बेहतरीन सुरक्षा प्रोफाइल है ।</s>
<s>पुरावों से यह साबित होता है कि <foreign lang="eng"> MMR </foreign> किसी भी तरह से <foreign lang="eng"> autism </foreign> या फिर <foreign lang="eng"> bowel disease </foreign> से जुडा हुआ नही है ।</s>
<s>अलग अलग टीके देना शायद खतरनाक भी हो सकता है ।</s> <s>इस में बच्चों को <foreign lang="eng"> measles ,mumps </foreign> या फिर <foreign lang="eng"> rubella </foreign> होने का खतरा है । </s>
<s><foreign lang="eng"> MMR </foreign> जहां पर उपलब्ध है , कोई भी देश यह टीके अलग अलग देने की सलाह देगा । </s>
<s><foreign lang="eng"> Measles </foreign> एक बहुत ही संसर्ग जन्य बीमारी है और वह बच्चों और बुजुर्ग लोगों को विकलांग तथा मार भी सकती है । </s>
<s><foreign lang="eng"> England </foreign> मंे जब <foreign lang="eng"> MMR </foreign> की शुरुवात हुई थी तो उस के पहले एक साल <foreign lang="eng"> 86,000 </foreign>बच्चों को <foreign lang="eng"> measles </foreign> की लागत हुई थी और <foreign lang="eng"> 16 </foreign> बच्चे मरे थे । </s> <s>एक हाल ही में <foreign lang="eng"> Dublin </foreign> में पालकों ने बच्चों को टीके न लगवाने से दो बच्चों की मौत हो गयी थी । </s>
<s><foreign lang="eng"> MMR </foreign> के टिके उपलब्ध होने से पहले बच्चों में <foreign lang="eng"> viral meningitis </foreign> होने का प्रमुख कारण <foreign lang="eng"> </foreign>Mumps बीमारी थी । </s> <s>अभी यह बिल्कुल भी नही रही । </s> <s>न जन्मे हुए बच्चों के लिए <foreign lang="eng"> rubella </foreign> बहुत ही घातक साबित हो सकती है - बहुत सारे मामलों में गर्भावस्था की औरतों को उन के खुद के बच्चों से या फिर उन के मित्रों के बच्चों से <foreign lang="eng"> rubella </foreign> बीमारी का शिकार होना पडा है । </s>
<s><foreign lang="eng"> MMR </foreign> आप को और आप के परिवार को <foreign lang="eng"> measles, mumps</foreign> और <foreign lang="eng"> rubella </foreign> होने से बचाता है । </s> <s><foreign lang="eng"> MMR </foreign> के कारण यह तीन बीमारीयंो से अभी कोई खतरा नही । </s> <s>अगर बच्चों को इन बीमारीयों से ना बचाया तो बिमारीयां फिर से आ सकती है । </s></p>

<p><s>अगर आप ने आप की <foreign lang="eng"> MMR </foreign> का टीका लेने के समय को खो दिया है तो आप कभी भी उस का टीका लगवा सकते है । </s></p>

</body>
</text>
</cesDoc>