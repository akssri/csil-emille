<cesDoc id="hin-w-legal-service" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-legal-service.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Hrishikesh Arvind Rajhans</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>02-09-17</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Work permits : Service and Standards</h.title>
<h.author>The Home Office</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>The Home Office</publisher>
<pubDate>April 2002</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>02-09-09</date>
</creation>
<langUsage>Translated into Hindi from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-legal-service.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>
<text>
<body>

<p><head><s><foreign lang="eng">Work Permits (United Kingdom) </foreign></s></head></p>
<p><head><s><foreign lang="eng">serving business needs </foreign></s></head></p>

<p><head><s> सेवायें और मापदंड़ ।  </s></head></p>

<gap desc="Home Office logo" extent="3 cm x 3 cm"></gap>

<p><s> यह <foreign lang="eng"> 1/11/01 - 31/03/02</foreign> तक प्रभावशाली हैं ।  </s></p>

<gap desc="Work permits logo" extent="2 cm x 7 cm"></gap>
<gap desc="Home Office logo" extent="3 cm x 3 cm"></gap>

<p><head><s> सेवायें और मापदंड़ ।  </s></head></p>

<p><s> यह हमारी सेवाओं के मापदंड़ हैं जिन को हम वर्क परमिट के आवेदनपत्रों के बारे में सोचविचार करने के समय अपनाते हैं । </s> <s> यह वह न्यूनतम मापदंड़ है जिन की आप हम से अपेक्षा कर सकते है । </s> <s> हम आशा करते है कि आप हमें इन मापदंड़ों को पाने के लिए अपना सहयोग दे सकते है । </s> <s> साँझेदारी में काम करने से हमें उच्च गुणवत्ता, प्रभावशाली सेवा देने में सहायता मिल सकती है ।  </s></p>

<p><s> हमारा लक्ष एक दिन में <foreign lang="eng">50% </foreign> भरे हुए मिले आवेदनपत्रों और एक हफ्ते में <foreign lang="eng">90% </foreign> आवेदनपत्रों के बारे में निर्णय लेने का है ।  </s></p>

<p><head><s> विवरण ।  </s> <s>                                                                          पैराग्राफ ।  </s></head></p>

<p><s> आप हम से किन बातों की अपेक्षा कर सकते है ।</s> <s> <foreign lang="eng">1 </foreign></s></p>

<p><s> हम आप से क्या चाहते है ।</s> <s><foreign lang="eng">17 </foreign>

<p><s> किसी निर्णय के विरुद्ध कैसे अपील करनी है ।  </s></p> <foreign lang="eng">22 </foreign></s></p>

<p><s>शिकायत कैसे बनानी है । </s> <s><foreign lang="eng">24 </foreign></s></p>

<p><s>कृपया ध्यान देंः इन मार्गदर्शन करने वाले नोट्स की तारीख समाप्त होने से पहले वर्क परमिट (यू के ) के सारे इ-मेल के पते बदल सकते हैं ।  </s> <s>अधिक जानकारी के लिए हमारी वैबसाईट <foreign lang="eng"> www.workpermits.gov.uk </foreign> को देखें ।  </s></p>

<p><s> यह <foreign lang="eng">1/11/01 - 31/03/02 </foreign> तक लागू हैं ।  </s></p>

<p><head><s> आप हम से किन बातों की अपेक्षा कर सकते है ।  </s></head></p>

<p><s> <foreign lang="eng">1 </foreign> हमारा लक्ष आप को जहाँ तक संभव हो उच्च मापदंड़ों वाली सेवायें प्रदान करने का है ।  </s></p>

<p><s> <foreign lang="eng">2 </foreign>हमारा लक्ष नियोक्ता कुशल व्यक्तियों को विदेश से भर्ती करने और स्थलांतरित करने और निवासी कर्मचारियों के लिए रोज़गार के अवसरों को सुरक्षित रखने के बीच एक सही संतुलन बनाये रखने का है ।  </s></p>

<p><s> <foreign lang="eng">3 </foreign>हम आप को समझे जा सकने वाली , और आप की पहुँच में आने वाली सूचनायें और निर्दएश प्रदान करेंगे , जिन का कि हम समय-समय पर अवलोकन करेंगे ताकि इस को और बेहतरीन बनाने के लिए समय-समय पर अवलोकन करेंगे ताकि इस को और बेहतरीन बनाने के लिए हम इस में कुछ बदलावों को शामिल कर सकें । </s> <s> फॅार्म और पत्रक केवल उसी तारीख तक वैध हैं जो कि प्रत्येक प्रमाणपत्र पर छपी हुई है । </s> <s> अगर आप के पास कोई ऐसा फॅार्म है जिस की तारीख अभी हाल ही में समाप्त हुई है , आप उस को अभी भी प्रयोग में ला सकते हैं (लेकिन यह ई-मेल आवेदनपत्रों के मामलें में ठीक नही है , कृपया हमारे निर्देशों को हमारी वैबसाइटः <foreign lang="eng"> www.workpermits.gov.uk </foreign> पर देखें । </s> <s> ) </s></p>

<p><s> <foreign lang="eng">4 </foreign> हम सभी आवेदनपत्रों के मिलने की रसीद उस की प्राप्ती की तारीख से पाँच कार्यकारी दिनों के अंदर भेजेंगे , सिवाये तब जब की आवेदनपत्र पर उस समय के दौरान निर्णय लिया जाना है । </s> <s> मिलने की रसीट पर फोन और फैक्स नंबर , एक ई-मेल का पता और एक ऐसा नाम जिस पर आप अपनी जानकारियों के लिए बात कर सकते हैं , शामिल होगा । </s> <s> इ-मेल आवेदनपत्रों की मिलने की रसीट स्वतः ही उसी दिन मिल जायेगी ।  </s></p>

<p><s> <foreign lang="eng">5 </foreign>हम तरफ से भेजे जाने वाले पत्रों पर उस व्यक्ति का नाम और टेलीफोन ऐक्सटैंशन नंबर होगा जो कि आप के आवेदनपत्र को देख रहा है ।  </s></p>

<p><s> <foreign lang="eng">6 </foreign>अगर आप अपना आवेदनपत्र किसी प्रतिनिधी के द्वारा बनाये जाने की उच्छा करते है , जैसे की वकील या कोई अन्य दलाल , हम उन के साथ पत्राचार करेंगे और आवेदन के बारे में अपना निर्णय उन्हें भेजेंगे । </s> <s> हाँलाकि यह एक जरुरत नही है , और इस से इस बात पर कोई प्रभाव नही पड़ेगा कि हम आप के आवेदनपत्र को कितनी शीघ्रता से देखते हैं ।  </s></p>

<p><s> <foreign lang="eng">7 </foreign>हम आप के आवेदन पत्र पर वर्क परमिट के द्वारा उपयुक्त निर्देश नोट्स में निर्धारित मापदंड़ों के अनुसार , बहुत सावधानीपूर्वक विचार करेंगे । </s> <s>  </s></p>

<p><s> <foreign lang="eng">8 </foreign>हम आप के द्वारा पूछी हुई तारीख तक आप के आवेदन पत्र पर निर्णय लेने की भरपूर कोशिश करेंगे ।  </s></p>

<p><s> <foreign lang="eng">9 </foreign> हमारा लक्ष प्राप्त किये गये भरे हुए आवेदनपत्रों में से <foreign lang="eng">50% </foreign> का निर्णय एक दिन में तथा <foreign lang="eng">90% </foreign> आवेदन पत्रों का एक हफ्ते के अंदर निर्णय लेने का ह । </s> <s> उन व्यक्तियों के आवेदन पत्रों पर जो कि पहले से ही उस देश में रह रहे है , और जहां पर हम आप क किसी अतिरिक्त सूचना के लिए पूछते हैं , वह आवेदन पत्र थोड़ा ज्यादा समय ले सकते हैं । </s> <s> हम कोशिश करेंगे की देरी कम से कम हो , लेकिन कुछ मामले ऐसे होंगे , उदाहरण के लिए जहाँ हमें आप से या कसी दूसरे सरकारी विभागों से अतिरिक्त सूचना लेने कि ज़रुरत होगी । </s> <s> और उन व्यक्तियों के आवेदनों पर जो कि पहले से ही इस देश में रह रहे है , उन को हमारे द्वारा निर्णय लिये जाने के बाद हम अॅाफिस के प्रारंभिक कंसीड़रेशन यूनिट की स्वीकृति की ज़रुरत होगी , और यह कुछ जादा समय ले सकता है ।  </s></p>

<p><s> <foreign lang="eng">10 </foreign> अगर आप किसी ऐसे व्यक्ति का आवेदन बना रहे हैं जो कि पहले से ही इस देश में रह रहा है , तो उस में जहाँ तक संभव हो सके , व्यक्ति का पासपोर्ट और पुलिस में पंजीकरण का प्रमाणपत्र उन के आवेदन के साथ शामिल करें । </s> <s> इ-मेल वाले आवेदनपत्रों में कृपया यह सुनिश्चित कर लें कि यह ड़ाक द्वारा अलग से भेजे जायें ताकि यह आवेदन पत्र के मिलने के पाँच कार्यकारी दिनों के अंदर पहुँच जायें । </s> <s> जहाँ पर पासपोर्ट और पुलिस में पंजीकरण का प्रमाण पत्र प्रदान कर दिया गया है , हमारा वादा है कि हम वर्क परमिट के आवेदन पत्र निर्णय लेने के चार कार्यकारी दिनों के अंदर आप के आवेदन पत्र की प्रक्रिया में पासपोर्ट पर मुहर लगा देंगे ।  </s></p>

<p><s> <foreign lang="eng">11 </foreign> ऐसे व्यक्तियों के आवेदनपत्रों के बारे में जो कि पहले से ही इस देश में रह रहे हैं , अधिक समय लग सकता है जहाँ ः ।  </s></p>

<p><s> <foreign lang="eng">a </foreign> हमें देश के अंदर आवेदन पत्र के साथ पासपोर्ट और पुलिस में पंजीकरण का प्रमाणपत्र नही भेजा गया है ।  </s></p>

<p><s> <foreign lang="eng">b </foreign>हमें आप के इ-मेल के आवेदन पत्र के मिलने के पाँच कार्यकारी दोनों के अंदर वह नही मिले है ः या ।  </s></p>

<p><s> <foreign lang="eng">c </foreign> जहाँ पर हमें उन के पासपोर्टों को हम अॅाफिस के प्रारंभिक कंसीडरेशन यूनिट में उन का ध्यान दुबारा इस तरफ दिलाने के लिए दुबारा भेजना पडेगा ।  </s></p>

<p><s> <foreign lang="eng">12 </foreign> हम इस बात का अवलोकन करते रहेंगे कि हम किस सीमा तक आप की ज़रुरतों को पूरा कर पा रहे हैं और आप हमारे द्वारा प्रदान की जारही सेवाओं से कितने संतुष्ट हैं , और जहाँ पर आवश्यक होगा वहाँ पर सुधार लायेंगे ।  </s></p>

<p><s> <foreign lang="eng">13 </foreign> हम अपनी सेवाओं के मापदंड़ों का निरंतर अवलोकन करते रहेंगे ।  </s></p>

<p><s> <foreign lang="eng">14 </foreign> हम सभि पूछी गई जानकारियों और पत्रों का शीघ्रता से उत्तर देंगे , जिस में टेलीफोन और इ-मेल पूछताछ भी शामिल हैं । </s> <s> हमारा लक्ष है कि हम टेलीफोन की घंटी तीन बार बजने के अंदर जवाब दें और पत्रों और इ-मेलों का <foreign lang="eng">15 </foreign>  कार्यकारी दिनों के अंदर जवाब दें ।  </s></p>

<p><s> <foreign lang="eng">15 </foreign> सिविल सर्विस कोड़ और ड़ेटा प्रॅाटैक्शन ऐक्ट <foreign lang="eng">1998 </foreign>   के अंतर्गत सभी वर्क परमिट के आवेदन पत्रों को गोपनियता से रखा जायेगा । </s> <s> समय समय पर हम अन्य सरकारी विभागों को सूचनायें दे सकते हैं , उदाहरण के लिए जहाँ हमें हिम अॅाफिस के अन्य विभागों से या विदेशों में आप्रावासिय मामलों पर ब्रिटीश ड़िप्लोमैटिक पोस्ट्स से संपर्क करने की ज़रुरत होगी ।  </s></p> 

<p><s> <foreign lang="eng">16 </foreign> कुछ मौके ऐसे भी आते हैं जहाँ पर हमें किसी विशेष लेबर मार्केट के मामले के संबंध मे उस के अनुरुप संस्था से सलाह लेने की ज़रुरत होती है ।  </s></p>

<p><head><s> हमें आप से क्या चाहिये। </s></head></p>

<p><s> <foreign lang="eng">17 </foreign> कृपया यह सुनिश्चित कर ले कि आप सही आवेदन पत्र का प्रयोग कर रहे हैं और सभी ज़रुरी सवालों का उत्तर दे रहे हैं । </s> <s> निर्देश नोट्स आप की सहायता करेंगे ।  </s></p> 

<p><s> <foreign lang="eng">18 </foreign> कृपया हमारे द्वारा आवेदन पत्र के साथ माग गये किसी भी प्रमाण पत्र या सूचना को भेजें । </s> <s> अगर आप ऐसा नही करते हैं तो निर्णय लेने में देरी ह सकती है ।  </s></p>

<p><s> <foreign lang="eng">19 </foreign> अगर आप को कोई पत्र मिलता है जिस में किसी सूचना के बारे में पूचा गया है , तो कृपया पूछे गये सभी सवालों का शीघ्रता से जवाब दें । </s> <s> फोन , फेक्स और इ-मेल के द्वारा जवाब देने से काफी समय बच जाता है ।  </s></p> 

<p><s> <foreign lang="eng">20 </foreign> जब आप ऐसे व्यक्ति को खोज लें जिसे आप नौकरी देना चाहते है तो अपना आवेदन पत्र हमें भेज दें , आमतौर पर कम से कम एक महीना पहले , लेकिन परमिट लेने की तारीख से छः महीने पहले न भेजे ।  </s></p>

<p><s> <foreign lang="eng">21 </foreign> हम आप के द्वारा हमारी सेवाओं को सुधारने के बारे में दिये गये सभी सुझावों या तरीकों का स्वागत करते हैं । </s> <s> कृपया अपने सुझाव हमारी कस्टमर रिलेशन्ज़ टीम से संपर्क कर के उस पते पर बतायें ः ।  </s></p>

<p><s><foreign lang="eng">Customer Relations</foreign></s>
<s><foreign lang="eng">Work Permits (UK) </foreign></s>
<s><foreign lang="eng">Immigration and Nationality Directorate </foreign></s>
<s><foreign lang="eng">Home Office </foreign></s>
<s><foreign lang="eng">Level 5, Moorfoot </foreign></s>
<s><foreign lang="eng">Sheffield </foreign></s>
<s><foreign lang="eng">S1 4PQ </foreign></s></p>

<p><s>फोन ः <foreign lang="eng">0114 259 4074 </foreign> ।  </s></p>
<p><s>फैक्स ः <foreign lang="eng">0114 259 3776 </foreign> ।  </s> </p> 
<p><s>इ मेल ः <foreign lang="eng">customrel.workpermits@dfes.gsi.gov.uk </foreign>।  </s></p> 

<p><s> कृपया ध्यान देंः वर्क परमिट्स (यू के ) के सारे इ-मेल के पते इन निर्देश नोटों की अवधी समाप्त होने से पहले बदल सकते हैं ।  </s> <s>अधिक जानकारी के लिए हमारी वैबसाईट <foreign lang="eng"> www.workpermits.gov.uk </foreign> को देखें ।  </s></p> 

<p><head><s> किसी निर्णय के विरुद्ध कैसे अपील करनी है ।  </s></head></p>

<p><s> <foreign lang="eng">22 </foreign> अगर आप हमारे द्वारा आप के वर्क परमिट्स को अस्वीकृति देने के निर्णय के विरुद्ध अपील करना चाहते हैं तो ज़रुरी होने पर हमारा निर्णय बताने वाला पत्र इस के बारे में आप को बतायेगा । </s> <s> हम आप से इस बात को हमंे बताने के लिए पूछेंगे कि क्या आप <foreign lang="eng">28 </foreign>  दिनों के अंदर अपील करने की मंशा रखते हैं । </s> <s> अगर आप अपील कैसे करनी है के बारे में और अधिक सलाह लेना चाहते हैं , तो कृपया पत्र में बताये गये संपर्क नाम से बाते करें । </s> <s> हमारी अपील की प्रक्रिया आप के आवेदन पत्र के बारे में स्वतंत्र रुप से अवलोकन करेगी ।  </s></p>

<p><s><foreign lang="eng">23 </foreign> हमारा लक्ष है कि अपील प्राप्त होने के <foreign lang="eng">15 </foreign>  कार्यकारी दिनों के अंदर आप को निर्णय दे दें । </s> <s> हाँलाकि , इस में अधिक समय लग सकता है , अगर हम आप के द्वारा अपील भेजने के बाद आप से अधिक सूचनाओं के लिए संपर्क करने की ज़रुरत महसूस करें ।  </s></p>

<p><head><s> शिकायत खेसी करनी है । </s></head></p> 

<p><s><foreign lang="eng">24 </foreign>अगर आप प्राप्त की गई सेवाओं के मापदंड़ों से प्रसन्न नही है , और आप इस की शिकायत करना चाहते है , तो कृपया हमें इस पते पर संपर्क करेंः ।  </s></p>

<p><s><foreign lang="eng">Customer Relations </foreign></s>
<s><foreign lang="eng">Work Permits (Unite Kingdom) </foreign></s>
<s><foreign lang="eng">Immigration and Nationality Directorate </foreign></s>
<s><foreign lang="eng">Home Office </foreign></s>
<s><foreign lang="eng">Level 5, Moorfoot </foreign></s>
<s><foreign lang="eng">Sheffield </foreign></s>
<s><foreign lang="eng">S1 4PQ </foreign></s></p>

<p><s>फोन ः <foreign lang="eng">0114 259 4074 </foreign> ।  </s></p>
<p><s>फैक्स ः <foreign lang="eng">0114 259 3776 </foreign> ।  </s> </p> 
<p><s>इ मेल ः <foreign lang="eng">customrel.workpermits@dfes.gsi.gov.uk </foreign> ।  </s></p> 

<p><s> <foreign lang="eng">25 </foreign> 	हम आप की शिकायत मिलने के <foreign lang="eng"> 15 </foreign>  कार्यकारी दिनों के अंदर इस का जवाब देंगे । </s> <s> अगर इस समय के धोरान आप को उस का पूरा जवाब दे पाना संभव नही है , उदाहरण के लिए अगर एक विस्तृत जांच-पड़ताल की ज़रुरत है , तो हम आप को इस के बीच के समय में जवाब देंगे । </s> <s> यह आप को बतायेगा कि हम आप की शिकायत को किस प्रकार से देख रहे हैं और आप इस के पूरे जवाब की कब आशा कर सकते है ।  </s></p>

<p><s> <foreign lang="eng">26 </foreign> 	पूरे जवाब में उस व्यक्ति का विवरण भी शामिल होगा जिस से आप शिकायत कर सकते हैं कि आप की शिकायत को सही प्रकार से नही देखा गया है । </s> <s> आमतौर पर यह कोई उपयुक्त उच्च पदाधिकारी होता है ।  </s></p>

<p><s> <foreign lang="eng">27 </foreign> 	अगर आप अभी भी संतुष्ट नही हैं तो आप अपने स्थानीय संसद के सदस्य से स्वतंत्र पार्लिमैन्टरी कमीशनर फॅार एड़मिनिस्ट्रेशन (द ओम्बड़समैन) से संपर्क करने के लिए तथा आप की शिकायत का अवलोकन करने तथा यह देखने के लिए कि उसे किस प्रकार से देखा गया है , के लिए पूछ सकते हैं । </s> <s> अगर ओम्बड़समैन को यह संतुष्टी हो जाती है कि आप के आवेदन पत्र को निष्पक्ष रुप से देखा गया है , तो वह आप के मामले को बंद कर देगा / देगी और हम इस मामले के बारे में दुबारा पत्रचार नही करेंगे ।  </s></p>

<p><s>क्राउन कॅापीराईट <foreign lang="eng">2001 </foreign>ः सभी अधिकार सुरक्षित हैं ।  </s> <s>ग्रेट ब्रिटेन में में होम अॅाफिस , मूरकफुट , शैफील्ड एस <foreign lang="eng">1 </foreign> और <foreign lang="eng">4 </foreign> पी क्यू के द्वारा प्रकाशित की गई है ।  </s></p>

</body>
</text>
</cesDoc>