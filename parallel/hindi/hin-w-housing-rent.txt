<cesDoc id="hin-w-housing-rent" lang="hin">
<cesHeader type="text">
<fileDesc>
<titleStmt>
<h.title>hin-w-housing-rent.txt</h.title>
<respStmt> 
<respType>Electronic file created by</respType>
<respName>Dept of Linguistics, Lancaster University</respName>
<respType>transcribed by</respType>
<respName>Hrishikesh Arvind Rajhans</respName>
</respStmt>
</titleStmt>
<publicationStmt>
<distributor>UCREL</distributor>
<pubAddress>Dept of Linguistics, Lancaster University, Lancaster, LA1 4YT, UK</pubAddress>
<availability region="WORLD"></availability>
<pubDate>03-02-05</pubDate>
</publicationStmt>
<sourceDesc>
<biblStruct>
<monogr>
<h.title>Do you rent, or are you thinking of renting, from a private landlord?</h.title>
<h.author>Department for Transport, Local Government and the Regions</h.author>
<imprint>
<pubPlace>UK</pubPlace>
<publisher>Department for Transport, Local Government and the Regions</publisher>
<pubDate>February 2002</pubDate>
</imprint>
</monogr>
</biblStruct>
</sourceDesc>
</fileDesc>
<encodingDesc>
<projectDesc>Text collected for use in the EMILLE project, May 2000.</projectDesc>
<samplingDesc>Simple written text only has been transcribed. Diagrams, pictures and tables have been omitted and their place marked with a gap element.
</samplingDesc>
<editorialDecl>
<conformance level="1"></conformance>
</editorialDecl>
</encodingDesc>
<profileDesc>
<creation>
<date>03-02-01</date>
</creation>
<langUsage>Translated into Hindi from English</langUsage>
<wsdUsage>
<writingSystem id="ISO/IEC 10646">Universal Multiple-Octet Coded Character Set (UCS).</writingSystem>
</wsdUsage>
<textClass>
<channel mode="w">print</channel>
<constitution type="composite"></constitution>
<derivation type="translation"></derivation>
<domain type="public"></domain>
<factuality type="fact"></factuality>
</textClass>
<translations>
<translation trans.loc="eng-w-housing-rent.txt" lang="ENG" wsd="ISO8859-1" n="1"></translation>
</translations>
</profileDesc>
<revisionDesc>
</revisionDesc>
</cesHeader>

<text>
<body>

<foreign lang="eng">DTLR</foreign>
<foreign lang="eng">Transport Local Government Regions </foreign>

<p><head><s> क्या आप किराये पर मकान देते हैं , या क्या आप किसी निजी मकान मालिक से किराये पर मकान लेने की सोच रहे हैं? </s></head></p> 

<p><head><s> आवास (हाऊसिंग) </s></head></p>

<p><head><s> कानून क्या है ? </s></head></p>

<p><s> अगर आप अब किराये पर मकान लेना आरम्भ कर रहे हैं , तो , आपकी अभिघृती स्वतः ही , कुछ समय के लिए निश्चित अभिघृती (अशोयरड़ शार्टहोल्ड़ टीनैंसी) हो जायेगी (जब तक कि आपका मकान मालिक इसको लिखित में अलग ढंग से करने के लिए सहमत नही हो जाता है) । </s></p>

<p><s> मकान मालिक के साथ अभिघृती (किरायेदारी) की समय अवधी पर समझौता करना , आपके ऊपर है । </s> <s> यह एक निश्चित समय के लिए हो सकता है (जिसको फिक्सड़ टर्म से जाना जाता है) या इसको खुला छोड़ा जा सकता है (कोई समय तय किये बिना) ।</s></p>

<p><head><s> मुझे मका छोड़ने के लिए कब कहा जा सकता है ? </s></head></p>

<p><s> अगर आपके द्वारा सहमत निश्चित अवधी समाप्तहो गई है , तो मका मालिक आपसे छः महीने के बाद कभी भी मकान छोड़ने के लिए कह सकता है । </s> <s> उसको यह बताते हुये कि वह आपको निकालना चाहता है , के बारे में आपको कम से कम दो महीने पहले लिखित नोटिस देना चाहिए । </s></p>

<p><s> विधान में सम्पत्ती के अधिकार के बारे में बताये गये कुछ विशेष आधारों (उदाहरण के लिए कारणंो) पर , मकान मालिक किसी भी समय इस अभिघृती (किरायेदारी) को समाप्त करने के लिए अदालत में आवेदन कर सकता है । </s></p> 

<p><s> इसमें किराये की बकाया धनराशी शामिल है - आप का मकान मालिक आपसे मकान खाली करवाने के लिए कभी भी आवेदन कर सकता है , अगर आपने दो महीने का या <foreign lang="eng">  </foreign>८ हफ्तों का किराया नही दिया है ,और किसी असामाजिक व्यवहार को दर्शाया है - अगर आप स्थानीय व्यक्तियों के लिए मुसीबत बने हुये हैं तो , आपका मकान मालिक आपको घर से बाहर निकाल सकता है ।</s></p>

<p><head><s> क्या मुझे मकान छोड़ना होगा? </s></head></p>

<p><s> आपको नोटिस का समय समाप्त होने पर मकान छोड़ देना चाहिए । </s> <s> हालांकि , मकान मालिक आपको जबर्दस्ती मकान से बाहर निकाल सकता है , लेकिन उसके अधिकार के लिए अदालत में आवेदन कर सकता है । </s> <s> अगर आप अदालत के द्वारा बताई गई तारीख तक मकान खाली नही करते हैं तो , मकान मालिक खाली करवाने के लिए अदालत से हुक्मनामा जारी करने के लिए आवेदन कर सकता है । </s> <s> अदालत तब मकान खाली करवाने के लिए आपके लिए सहकारी अमीन (बेलिफ) की नियुक्ति कर सकती है । </s></p>

<p><head><s> क्या मैं एक लिखित अभिघृती (किरायेदारी) करवाने के लिए योग्य हूँ ? </s></head></p>

<p><s> अगर आपके मकान मालिक ने कोई लिखित समझौता नही दिया है , तो आप उनसे निम्नलिखित चीजों को बताते हुए एक लिखित टिपण्णी की माँग कर सकते हैंः </s></p>

<p><s> वह तारीख जिस से अभिघृती (किरायेदारी) आरम्भ हुई है </s></p>

<p><s> किराया , और यह कब देना है , </s></p>

<p><s> किराये के बारे में पुनर्विचार करने के प्रबन्ध ,</s></p>

<p><s> किसी निश्चित स्त्र की अवधी ।</s></p>

<p><head><s> क्या मुझे किराये में कुछ सहायता मिल सकती है? </s></head></p>

<p><s> अगर आप कोई बैनिफिटस (लाभ) ले रहे हैं या आपकी आय कम है , तो आप हाऊसिंग बैनिफिट के द्वारा सहायता लेने के योग्य हो सकते हैं । </s> <s> आपको अपने स्थानिय अथॅारिटी को यह जानने के लिए आवेदन करना चाहिए , कि क्या आप इसको लेने के योग्य हैं , अगर योग्य हैं , तो आप कितनी धनराशी का दावा कर सकते हैं ।</s></p>

<p><s> किसी किरायेदारी पर समझौता होने से पहले , आप अपने स्थानिय अथॅारिटी को यह जानने के लिए आवेदन कर सकते हैं , कि हाऊसिंग बैनिफिट के द्वारा कितना किराया पूरा किया जा सकता है ।</s></p>

<p><head><s> अगर मैं किराये को नही दे पाता / पाती हूँ तो क्या होगा ? </s></head></p>

<p><s> मकान मालिक आपको नोटिस दे सकता है कि , अगर आपने कम से कम आठ हफ्तों (अगर आप किराया हर हफ्ते देते हैं) या दो महीनों (अगर आप हर महीने देते हैं) से किराया नही दिया है , तो वह अदालत से मकान के कब्जे की आञा मांगेगा/माँगेगी । </s> <s> अगर आप अदालत में सुनवाई के समय भी इतने समय का किराया नही दे पाते हैं , तो न्यायाधीश मकान मालिक को अधिकार का आदेश दे देगा । </s></p>

<p><head><s> अगर मेरे हाईसिंग बैनिफिट को आने में देरी हो जाती है , तो क्या होता है? </s></head></p>

<p><s> अपने स्थानिय अथॅारिटी के अधिकारियों से जो कि आपके दावों के बारे में देखते हैं , से तुरंत सम्पर्क करें , और उन्हें बतायें कि किराया न दे पाने की वजह से मकान मालिक आपसे घर खाली करवाने की कोशिश कर रहा है ।</s></p>

<p><head><s> अगर मैं सोचता/सोचती हूँ कि किराया बहुत अधिक है , तो में क्या करूँ ? </s></head></p>

<p><s> अगर आप ऐसा सोचता हैं कि आपसे अन्य किरायेदारों की तुलना में , जो कि इसी प्रकार के घर में रहते हैं , अधिक पैसा लिया जा रहा है , तो आप किराये का मूल्यनिर्धारण करने वाली कमेटी से , किराये के बारे में निर्णय करने के बारे में कह सकते हैं । </s> <s> विस्तार से जानने के लिए अपने नजदीकी किराये का मूल्यनिर्धारण करने वाले पैनल से सम्पर्क करें । </s> <s> आपको इसके लिए अपनी अभिघृती (किरायेदारी) के आरम्भ होने के छः महीनों के अन्दर आवेदन करना चाहिए ।</s></p>

<p><head><s> किरायेदार के रुप में मेरी क्या जिम्मेदारियां है? </s></head></p>

<p><s> समझौते के अनुसार किराया दें और सम्पत्ती दें और सम्पत्ती को पूरी देखभाल के साथ रखें । </s>
<s> और इसी प्रकार की अन्य प्रसाधनों को लगाने के लिए । </s>
<s> गैस और बिजली के उपकरणों की सुरक्षा के लिए । </s>
<s> किरायेदारी के अंतर्गत प्रदान किये गये फर्नीचर और सजावट की वस्तुओं की आग से सुरक्षा के लिए । </s></p>

<p><head><s> अगर मेरा मकानमालिक मुझे गैरकानूनी तौर से बाहर निकलवाने या प्रताड़ित करने की कोशिश करे , तो मैं क्या करूँ ? </s></head></p>

<p><s> आपका मकनामालिक , बिना न्यायालय से निष्कासन के आदेशों के , आपको बाहर नही निकाल सकता है ।</s></p>

<p><s> अगर आपका वहाँ पर रहने का कानूनी अधिकार है , तो न तो वह व्यक्ति और न ही उसकी ओर से कोई व्यक्ति , आपको आपके घर से बाहर निकालने या इसके किसी भाग को प्रयोग में लाने से रोक सकते हैं ।</s></p>

<p><s> अगर आपको कोई मुश्किलें हैं , तो आपको अपने स्थानिय अथॅारिटी के किरायेदारी सम्बन्धित अॅाफिसर (टीनैंसी रेलेशन्ज़ अॅाफिसर) से सम्पर्क करना चाहिए ।</s></p>

<p><head><s> मेरा किरायेदार या अभिधारी के रुप में रहना <foreign lang="eng"> 28 </foreign> फरवरी <foreign lang="eng"> 1997 </foreign> से पहले आरम्भ हुआ था । </s> <s> मेरी क्या स्थिती है ? </s></head></p>

<p><s> अधिकतर अभिघृती (किरायेदारी) जो कि फरवरी <foreign lang="eng"> 1997 </foreign> से पहले आरम्भ हुई थी , उनको या तो कुछ समय के लिए निश्चित अभिघृती (किरायेदारी अशोयरड़ शॅार्टहोल्ड़ टीनैंसीज़ ) या निश्चित अभिघृती (अशोयरड़ टीनैंसीज़ ) माना जायेगा । </s></p> 

<p><s> अगर आप कुछ समय के लिए निश्चित अभिघृती (अशोयरड़ शॅार्टहोल्ड़ टीनैंसीज़ ) वाले किरायेदार हैं , तो हो सकता है कि आप उस सम्पत्ति पर दावे के साथ केवल छः महीनों तक ही रह सकते हैं । </s> <s> आपका मकानमालिक या मकान मालिक आपके और अधिक समय तक रहने के लिए सहमत हो सकता है , लेकिन वह यह नही भि कर सकते हैं । </s> <s> अगर आप निश्चित अभिघृती (अशोयरड़ टीनैंसीज़ ) वाले किरायेदार हैं तो , आप आमतौर पर तब तक रह सकते हैं जितना आप चाहें । </s> <s> आपका मकानमालिक केवल तभी आपसे मकान खाली करवा सकता / सकती हैं , अगर आपने कम से कम तीन महीनों या <foreign lang="eng"> 13 </foreign> हफ्तों से किराया नही दिया है । </s></p> 

<p><s> अगर आपका किरायेदार आपकी कुछ समय के लिए अभिघृती (अशोयरड़ टीनैंसीज़ )  को दुबारा से बना देता है , तो यह स्वतः ही कुछ समय के लिए अभिघृती हो जायेगी , जब तक कि वह आपको लिखित में यह नोटिस नही देता/देती है कि यह निश्चित है । </s></p>

<p><s> अगर आपका किरायेदार आपकी निश्चित अभिघृत (अशोयरड़ शॅार्टहोल्ड़ टीनैंसीज़ ) को दुबारा से बना देता है , तो यह स्वतः ही कुछ समय के लिए निश्चित अभिघृती हो जायेगी जब तक की वह आपको लिखित में यह नोटिस नही देता/देती है कि यह निश्चित है । </s></p>

<p><s> अभिघृती जो कि <foreign lang="eng"> 15 </foreign> जनवरी <foreign lang="eng"> 1989 </foreign> से पहले आरम्भ  हुई थी , वह भिन्न भिन्न विधानों के अंतर्गत है । </s></p> 

<p><head><s> मैं अधिक सुचना कहाँ से प्राप्त कर सकता/सकती हूँ? </s></head></p>

<p><s> अशोयरड़ और अशोयरड़ शॅार्टहोल्ड़ टीनैंसीज़ - किरायेदारों के लिए मार्ग निर्देश , एक विस्तृत पत्रिका इस पते पर बिल्कुल निःशुल्क उपलब्ध है । </s></p> 

<p><s> <foreign lang="eng"> Department of Transport , local Government and the Regions </foreign> </s>
<s><foreign lang="eng"> Publications Despatch Centre</foreign></s>
<s> <foreign lang="eng"> PO Box 236</foreign></s>
<s> <foreign lang="eng"> Wetherby </foreign></s>
<s> <foreign lang="eng"> West Yorkshire</foreign></s>
<s> <foreign lang="eng"> LS23 7NB</foreign></s></p>

<p><s> आप किसी वकील , लॅा सैन्टर , सिटिज़न एड़वाईस ब्यूरौ या स्थानिय अथॅारिटी हाऊसिंग  एड़वाईस सैन्टर से भी सलाह ले सकते है । </s></p> 

<p><s> आप इस पते पर , किसी किराये पर मकान देने वाले प्रमाणित ऐजेन्टों (दलालों) से सम्पर्क कर के भी सलाह ले सकते हैं ः </s></p>

<p><s><foreign lang="eng"> The National Approved Letting Scheme </foreign></s>
<s><foreign lang="eng"> Warwick Corner </foreign></s>
<s><foreign lang="eng"> 42 Warwick Road  </foreign></s>
<s><foreign lang="eng"> Kenilworth </foreign></s>
<s><foreign lang="eng"> CV 8 1HE  </foreign></s>
<s>टेलीफोन <foreign lang="eng"> 01926 866633 </foreign></s>
<s> <foreign lang="eng"> www. nalscheme.co.uk </foreign></s></p>

<p><s><foreign lang="eng"> Cynulliad Cenedlaethol Cymru </foreign></s></p>
<p><s><foreign lang="eng"> The National Assembly of Wales </foreign>  </s></p>
<p><s><foreign lang="eng"> Published by the Department of Transport , local Government and the Regions. Crown Copyright 2002.</foreign></s> <s><foreign lang="eng">Printed in th UK. February 2002 on material comprising 75% post consumer waste and 25% ECF pulp .</foreign></s> <s><foreign lang="eng"> Product code 97HCO246/HI Hindi </foreign></s></p>
</body>
</text>
</cesDoc>